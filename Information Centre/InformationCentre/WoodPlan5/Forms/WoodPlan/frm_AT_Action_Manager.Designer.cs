namespace WoodPlan5
{
    partial class frm_AT_Action_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Action_Manager));
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01334ATActionManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeContext = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeManagement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGroundType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeProtectionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeCrownDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePlantDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGroupNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSiteHazardClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePlantSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePlantSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePlantMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMapLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeTarget1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeTarget2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeTarget3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colTreeUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeAgeClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeAreaHa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplantCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeRiskFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionInspector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionIncidentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionIncidentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionAngleToVertical = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionGeneralCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionVitality = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostDifference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrderIssueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrderCompleteDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeOwnershipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserPickList1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserPickList2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserPickList3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl39 = new DevExpress.XtraGrid.GridControl();
            this.sp02066ATActionPicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView39 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn169 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn170 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn171 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn172 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn173 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn174 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn175 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn176 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn177 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn178 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn179 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShortLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditShowLastInspection = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEditSiteFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp01334_AT_Action_ManagerTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01334_AT_Action_ManagerTableAdapter();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp02066_AT_Action_Pictures_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp02066_AT_Action_Pictures_ListTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterData = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount = new DevExpress.XtraBars.BarStaticItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending39 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01334ATActionManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02066ATActionPicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowLastInspection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 659);
            this.barDockControlBottom.Size = new System.Drawing.Size(1060, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 659);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1060, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 659);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUndo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRedo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCut, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopy),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPaste),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectAll, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClear),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSpellChecker, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGrammarCheck)});
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.bciFilterData,
            this.bsiSelectedCount});
            this.barManager1.MaxItemId = 32;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1037, 659);
            this.splitContainerControl2.SplitterPosition = 408;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            this.splitContainerControl2.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl2_SplitGroupPanelCollapsed);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1037, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp01334ATActionManagerBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 42);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1037, 366);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01334ATActionManagerBindingSource
            // 
            this.sp01334ATActionManagerBindingSource.DataMember = "sp01334_AT_Action_Manager";
            this.sp01334ATActionManagerBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "refresh_16x16");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID,
            this.colTreeID,
            this.colSiteID,
            this.colClientID2,
            this.colSiteName,
            this.colClientName2,
            this.colTreeReference,
            this.colTreeMappingID,
            this.colTreeGridReference,
            this.colTreeDistance,
            this.colTreeHouseName,
            this.colTreeSpeciesName,
            this.colTreeAccess,
            this.colTreeVisibility,
            this.colTreeContext,
            this.colTreeManagement,
            this.colTreeLegalStatus,
            this.colTreeLastInspectionDate,
            this.colTreeInspectionCycle,
            this.colTreeInspectionUnit,
            this.colTreeNextInspectionDate,
            this.colTreeGroundType,
            this.colTreeDBH,
            this.colTreeDBHRange,
            this.colTreeHeight,
            this.colTreeHeightRange,
            this.colTreeProtectionType,
            this.colTreeCrownDiameter,
            this.colTreePlantDate,
            this.colTreeGroupNumber,
            this.colTreeRiskCategory,
            this.colTreeSiteHazardClass,
            this.colTreePlantSize,
            this.colTreePlantSource,
            this.colTreePlantMethod,
            this.colTreePostcode,
            this.colTreeMapLabel,
            this.colTreeStatus,
            this.colTreeSize,
            this.colTreeTarget1,
            this.colTreeTarget2,
            this.colTreeTarget3,
            this.colTreeRemarks,
            this.colTreeUser1,
            this.colTreeUser2,
            this.colTreeUser3,
            this.colTreeAgeClass,
            this.colTreeX,
            this.colTreeY,
            this.colTreeAreaHa,
            this.colTreeReplantCount,
            this.colTreeSurveyDate,
            this.colTreeType,
            this.colTreeRiskFactor,
            this.colTreePolygonXY,
            this.colTreeOwnershipName,
            this.colInspectionReference,
            this.colInspectionInspector,
            this.colInspectionDate,
            this.colInspectionIncidentReference,
            this.colInspectionIncidentDate,
            this.colIncidentID,
            this.colInspectionAngleToVertical,
            this.colInspectionUserDefined1,
            this.colInspectionUserDefined2,
            this.colInspectionUserDefined3,
            this.colInspectionRemarks,
            this.colInspectionStemPhysical1,
            this.colInspectionStemPhysical2,
            this.colInspectionStemPhysical3,
            this.colInspectionStemDisease1,
            this.colInspectionStemDisease2,
            this.colInspectionStemDisease3,
            this.colInspectionCrownPhysical1,
            this.colInspectionCrownPhysical2,
            this.colInspectionCrownPhysical3,
            this.colInspectionCrownDisease1,
            this.colInspectionCrownDisease2,
            this.colInspectionCrownDisease3,
            this.colInspectionCrownFoliation1,
            this.colInspectionCrownFoliation2,
            this.colInspectionCrownFoliation3,
            this.colInspectionRiskCategory,
            this.colInspectionGeneralCondition,
            this.colInspectionBasePhysical1,
            this.colInspectionBasePhysical2,
            this.colInspectionBasePhysical3,
            this.colInspectionVitality,
            this.colActionID,
            this.colActionJobNumber,
            this.colAction,
            this.colActionPriority,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colActionBy,
            this.colActionSupervisor,
            this.colActionOwnership,
            this.colActionCostCentre,
            this.colActionBudget,
            this.colActionWorkUnits,
            this.colActionScheduleOfRates,
            this.colActionBudgetedRateDescription,
            this.colActionBudgetedRate,
            this.colActionBudgetedCost,
            this.colActionActualRateDescription,
            this.colActionActualRate,
            this.colActionActualCost,
            this.colActionWorkOrder,
            this.colActionRemarks,
            this.colActionUserDefined1,
            this.colActionUserDefined2,
            this.colActionUserDefined3,
            this.colActionCostDifference,
            this.colActionCompletionStatus,
            this.colActionTimeliness,
            this.colActionWorkOrderIssueDate,
            this.colActionWorkOrderCompleteDate,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.colTreeOwnershipID,
            this.colActionUserPickList1,
            this.colActionUserPickList2,
            this.colActionUserPickList3,
            this.colSelected,
            this.colSiteCode,
            this.colSitePostcode});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 518, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 4;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDueDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            this.colInspectionID.Width = 85;
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            this.colTreeID.Width = 57;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Width = 71;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 68;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Width = 183;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 176;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.Width = 96;
            // 
            // colTreeMappingID
            // 
            this.colTreeMappingID.Caption = "Mapping ID";
            this.colTreeMappingID.FieldName = "TreeMappingID";
            this.colTreeMappingID.Name = "colTreeMappingID";
            this.colTreeMappingID.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID.OptionsColumn.ReadOnly = true;
            // 
            // colTreeGridReference
            // 
            this.colTreeGridReference.Caption = "Grid Reference";
            this.colTreeGridReference.FieldName = "TreeGridReference";
            this.colTreeGridReference.Name = "colTreeGridReference";
            this.colTreeGridReference.OptionsColumn.AllowEdit = false;
            this.colTreeGridReference.OptionsColumn.AllowFocus = false;
            this.colTreeGridReference.OptionsColumn.ReadOnly = true;
            this.colTreeGridReference.Width = 93;
            // 
            // colTreeDistance
            // 
            this.colTreeDistance.Caption = "Distance";
            this.colTreeDistance.FieldName = "TreeDistance";
            this.colTreeDistance.Name = "colTreeDistance";
            this.colTreeDistance.OptionsColumn.AllowEdit = false;
            this.colTreeDistance.OptionsColumn.AllowFocus = false;
            this.colTreeDistance.OptionsColumn.ReadOnly = true;
            this.colTreeDistance.Width = 74;
            // 
            // colTreeHouseName
            // 
            this.colTreeHouseName.Caption = "Nearest House";
            this.colTreeHouseName.FieldName = "TreeHouseName";
            this.colTreeHouseName.Name = "colTreeHouseName";
            this.colTreeHouseName.OptionsColumn.AllowEdit = false;
            this.colTreeHouseName.OptionsColumn.AllowFocus = false;
            this.colTreeHouseName.OptionsColumn.ReadOnly = true;
            this.colTreeHouseName.Width = 92;
            // 
            // colTreeSpeciesName
            // 
            this.colTreeSpeciesName.Caption = "Species";
            this.colTreeSpeciesName.FieldName = "TreeSpeciesName";
            this.colTreeSpeciesName.Name = "colTreeSpeciesName";
            this.colTreeSpeciesName.OptionsColumn.AllowEdit = false;
            this.colTreeSpeciesName.OptionsColumn.AllowFocus = false;
            this.colTreeSpeciesName.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesName.Visible = true;
            this.colTreeSpeciesName.VisibleIndex = 31;
            this.colTreeSpeciesName.Width = 171;
            // 
            // colTreeAccess
            // 
            this.colTreeAccess.Caption = "Access";
            this.colTreeAccess.FieldName = "TreeAccess";
            this.colTreeAccess.Name = "colTreeAccess";
            this.colTreeAccess.OptionsColumn.AllowEdit = false;
            this.colTreeAccess.OptionsColumn.AllowFocus = false;
            this.colTreeAccess.OptionsColumn.ReadOnly = true;
            this.colTreeAccess.Width = 54;
            // 
            // colTreeVisibility
            // 
            this.colTreeVisibility.Caption = "Visibility";
            this.colTreeVisibility.FieldName = "TreeVisibility";
            this.colTreeVisibility.Name = "colTreeVisibility";
            this.colTreeVisibility.OptionsColumn.AllowEdit = false;
            this.colTreeVisibility.OptionsColumn.AllowFocus = false;
            this.colTreeVisibility.OptionsColumn.ReadOnly = true;
            this.colTreeVisibility.Width = 58;
            // 
            // colTreeContext
            // 
            this.colTreeContext.Caption = "Context";
            this.colTreeContext.FieldName = "TreeContext";
            this.colTreeContext.Name = "colTreeContext";
            this.colTreeContext.OptionsColumn.AllowEdit = false;
            this.colTreeContext.OptionsColumn.AllowFocus = false;
            this.colTreeContext.OptionsColumn.ReadOnly = true;
            this.colTreeContext.Width = 60;
            // 
            // colTreeManagement
            // 
            this.colTreeManagement.Caption = "Management";
            this.colTreeManagement.FieldName = "TreeManagement";
            this.colTreeManagement.Name = "colTreeManagement";
            this.colTreeManagement.OptionsColumn.AllowEdit = false;
            this.colTreeManagement.OptionsColumn.AllowFocus = false;
            this.colTreeManagement.OptionsColumn.ReadOnly = true;
            this.colTreeManagement.Width = 83;
            // 
            // colTreeLegalStatus
            // 
            this.colTreeLegalStatus.Caption = "Legal Status";
            this.colTreeLegalStatus.FieldName = "TreeLegalStatus";
            this.colTreeLegalStatus.Name = "colTreeLegalStatus";
            this.colTreeLegalStatus.OptionsColumn.AllowEdit = false;
            this.colTreeLegalStatus.OptionsColumn.AllowFocus = false;
            this.colTreeLegalStatus.OptionsColumn.ReadOnly = true;
            this.colTreeLegalStatus.Width = 80;
            // 
            // colTreeLastInspectionDate
            // 
            this.colTreeLastInspectionDate.Caption = "Last Inspection Date";
            this.colTreeLastInspectionDate.FieldName = "TreeLastInspectionDate";
            this.colTreeLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTreeLastInspectionDate.Name = "colTreeLastInspectionDate";
            this.colTreeLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colTreeLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colTreeLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colTreeLastInspectionDate.Width = 120;
            // 
            // colTreeInspectionCycle
            // 
            this.colTreeInspectionCycle.Caption = "Inspection Cycle";
            this.colTreeInspectionCycle.FieldName = "TreeInspectionCycle";
            this.colTreeInspectionCycle.Name = "colTreeInspectionCycle";
            this.colTreeInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colTreeInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colTreeInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colTreeInspectionCycle.Width = 100;
            // 
            // colTreeInspectionUnit
            // 
            this.colTreeInspectionUnit.Caption = "Inspection Unit";
            this.colTreeInspectionUnit.FieldName = "TreeInspectionUnit";
            this.colTreeInspectionUnit.Name = "colTreeInspectionUnit";
            this.colTreeInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colTreeInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colTreeInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colTreeInspectionUnit.Width = 93;
            // 
            // colTreeNextInspectionDate
            // 
            this.colTreeNextInspectionDate.Caption = "Next Inspection Date";
            this.colTreeNextInspectionDate.FieldName = "TreeNextInspectionDate";
            this.colTreeNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTreeNextInspectionDate.Name = "colTreeNextInspectionDate";
            this.colTreeNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colTreeNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colTreeNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colTreeNextInspectionDate.Visible = true;
            this.colTreeNextInspectionDate.VisibleIndex = 32;
            this.colTreeNextInspectionDate.Width = 123;
            // 
            // colTreeGroundType
            // 
            this.colTreeGroundType.Caption = "Ground Type";
            this.colTreeGroundType.FieldName = "TreeGroundType";
            this.colTreeGroundType.Name = "colTreeGroundType";
            this.colTreeGroundType.OptionsColumn.AllowEdit = false;
            this.colTreeGroundType.OptionsColumn.AllowFocus = false;
            this.colTreeGroundType.OptionsColumn.ReadOnly = true;
            this.colTreeGroundType.Width = 83;
            // 
            // colTreeDBH
            // 
            this.colTreeDBH.Caption = "DBH";
            this.colTreeDBH.FieldName = "TreeDBH";
            this.colTreeDBH.Name = "colTreeDBH";
            this.colTreeDBH.OptionsColumn.AllowEdit = false;
            this.colTreeDBH.OptionsColumn.AllowFocus = false;
            this.colTreeDBH.OptionsColumn.ReadOnly = true;
            this.colTreeDBH.Width = 41;
            // 
            // colTreeDBHRange
            // 
            this.colTreeDBHRange.Caption = "DBH Band";
            this.colTreeDBHRange.FieldName = "TreeDBHRange";
            this.colTreeDBHRange.Name = "colTreeDBHRange";
            this.colTreeDBHRange.OptionsColumn.AllowEdit = false;
            this.colTreeDBHRange.OptionsColumn.AllowFocus = false;
            this.colTreeDBHRange.OptionsColumn.ReadOnly = true;
            this.colTreeDBHRange.Width = 68;
            // 
            // colTreeHeight
            // 
            this.colTreeHeight.Caption = "Height";
            this.colTreeHeight.FieldName = "TreeHeight";
            this.colTreeHeight.Name = "colTreeHeight";
            this.colTreeHeight.OptionsColumn.AllowEdit = false;
            this.colTreeHeight.OptionsColumn.AllowFocus = false;
            this.colTreeHeight.OptionsColumn.ReadOnly = true;
            this.colTreeHeight.Width = 52;
            // 
            // colTreeHeightRange
            // 
            this.colTreeHeightRange.Caption = "Height Band";
            this.colTreeHeightRange.FieldName = "TreeHeightRange";
            this.colTreeHeightRange.Name = "colTreeHeightRange";
            this.colTreeHeightRange.OptionsColumn.AllowEdit = false;
            this.colTreeHeightRange.OptionsColumn.AllowFocus = false;
            this.colTreeHeightRange.OptionsColumn.ReadOnly = true;
            this.colTreeHeightRange.Width = 79;
            // 
            // colTreeProtectionType
            // 
            this.colTreeProtectionType.Caption = "Protection Type";
            this.colTreeProtectionType.FieldName = "TreeProtectionType";
            this.colTreeProtectionType.Name = "colTreeProtectionType";
            this.colTreeProtectionType.OptionsColumn.AllowEdit = false;
            this.colTreeProtectionType.OptionsColumn.AllowFocus = false;
            this.colTreeProtectionType.OptionsColumn.ReadOnly = true;
            this.colTreeProtectionType.Width = 97;
            // 
            // colTreeCrownDiameter
            // 
            this.colTreeCrownDiameter.Caption = "Crown Diameter";
            this.colTreeCrownDiameter.FieldName = "TreeCrownDiameter";
            this.colTreeCrownDiameter.Name = "colTreeCrownDiameter";
            this.colTreeCrownDiameter.OptionsColumn.AllowEdit = false;
            this.colTreeCrownDiameter.OptionsColumn.AllowFocus = false;
            this.colTreeCrownDiameter.OptionsColumn.ReadOnly = true;
            this.colTreeCrownDiameter.Width = 98;
            // 
            // colTreePlantDate
            // 
            this.colTreePlantDate.Caption = "Plant Date";
            this.colTreePlantDate.FieldName = "TreePlantDate";
            this.colTreePlantDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTreePlantDate.Name = "colTreePlantDate";
            this.colTreePlantDate.OptionsColumn.AllowEdit = false;
            this.colTreePlantDate.OptionsColumn.AllowFocus = false;
            this.colTreePlantDate.OptionsColumn.ReadOnly = true;
            this.colTreePlantDate.Width = 71;
            // 
            // colTreeGroupNumber
            // 
            this.colTreeGroupNumber.Caption = "Gorup Number";
            this.colTreeGroupNumber.FieldName = "TreeGroupNumber";
            this.colTreeGroupNumber.Name = "colTreeGroupNumber";
            this.colTreeGroupNumber.OptionsColumn.AllowEdit = false;
            this.colTreeGroupNumber.OptionsColumn.AllowFocus = false;
            this.colTreeGroupNumber.OptionsColumn.ReadOnly = true;
            this.colTreeGroupNumber.Width = 90;
            // 
            // colTreeRiskCategory
            // 
            this.colTreeRiskCategory.Caption = "Risk Category";
            this.colTreeRiskCategory.FieldName = "TreeRiskCategory";
            this.colTreeRiskCategory.Name = "colTreeRiskCategory";
            this.colTreeRiskCategory.OptionsColumn.AllowEdit = false;
            this.colTreeRiskCategory.OptionsColumn.AllowFocus = false;
            this.colTreeRiskCategory.OptionsColumn.ReadOnly = true;
            this.colTreeRiskCategory.Visible = true;
            this.colTreeRiskCategory.VisibleIndex = 34;
            this.colTreeRiskCategory.Width = 88;
            // 
            // colTreeSiteHazardClass
            // 
            this.colTreeSiteHazardClass.Caption = "Site Hazard Class";
            this.colTreeSiteHazardClass.FieldName = "TreeSiteHazardClass";
            this.colTreeSiteHazardClass.Name = "colTreeSiteHazardClass";
            this.colTreeSiteHazardClass.OptionsColumn.AllowEdit = false;
            this.colTreeSiteHazardClass.OptionsColumn.AllowFocus = false;
            this.colTreeSiteHazardClass.OptionsColumn.ReadOnly = true;
            this.colTreeSiteHazardClass.Width = 104;
            // 
            // colTreePlantSize
            // 
            this.colTreePlantSize.Caption = "Plant Size";
            this.colTreePlantSize.FieldName = "TreePlantSize";
            this.colTreePlantSize.Name = "colTreePlantSize";
            this.colTreePlantSize.OptionsColumn.AllowEdit = false;
            this.colTreePlantSize.OptionsColumn.AllowFocus = false;
            this.colTreePlantSize.OptionsColumn.ReadOnly = true;
            this.colTreePlantSize.Width = 67;
            // 
            // colTreePlantSource
            // 
            this.colTreePlantSource.Caption = "Plant Source";
            this.colTreePlantSource.FieldName = "TreePlantSource";
            this.colTreePlantSource.Name = "colTreePlantSource";
            this.colTreePlantSource.OptionsColumn.AllowEdit = false;
            this.colTreePlantSource.OptionsColumn.AllowFocus = false;
            this.colTreePlantSource.OptionsColumn.ReadOnly = true;
            this.colTreePlantSource.Width = 81;
            // 
            // colTreePlantMethod
            // 
            this.colTreePlantMethod.Caption = "Plant Method";
            this.colTreePlantMethod.FieldName = "TreePlantMethod";
            this.colTreePlantMethod.Name = "colTreePlantMethod";
            this.colTreePlantMethod.OptionsColumn.AllowEdit = false;
            this.colTreePlantMethod.OptionsColumn.AllowFocus = false;
            this.colTreePlantMethod.OptionsColumn.ReadOnly = true;
            this.colTreePlantMethod.Width = 84;
            // 
            // colTreePostcode
            // 
            this.colTreePostcode.Caption = "Postcode";
            this.colTreePostcode.FieldName = "TreePostcode";
            this.colTreePostcode.Name = "colTreePostcode";
            this.colTreePostcode.OptionsColumn.AllowEdit = false;
            this.colTreePostcode.OptionsColumn.AllowFocus = false;
            this.colTreePostcode.OptionsColumn.ReadOnly = true;
            this.colTreePostcode.Width = 65;
            // 
            // colTreeMapLabel
            // 
            this.colTreeMapLabel.Caption = "Map Label";
            this.colTreeMapLabel.FieldName = "TreeMapLabel";
            this.colTreeMapLabel.Name = "colTreeMapLabel";
            this.colTreeMapLabel.OptionsColumn.AllowEdit = false;
            this.colTreeMapLabel.OptionsColumn.AllowFocus = false;
            this.colTreeMapLabel.OptionsColumn.ReadOnly = true;
            this.colTreeMapLabel.Width = 69;
            // 
            // colTreeStatus
            // 
            this.colTreeStatus.Caption = "Tree Status";
            this.colTreeStatus.FieldName = "TreeStatus";
            this.colTreeStatus.Name = "colTreeStatus";
            this.colTreeStatus.OptionsColumn.AllowEdit = false;
            this.colTreeStatus.OptionsColumn.AllowFocus = false;
            this.colTreeStatus.OptionsColumn.ReadOnly = true;
            this.colTreeStatus.Visible = true;
            this.colTreeStatus.VisibleIndex = 35;
            this.colTreeStatus.Width = 77;
            // 
            // colTreeSize
            // 
            this.colTreeSize.Caption = "Size";
            this.colTreeSize.FieldName = "TreeSize";
            this.colTreeSize.Name = "colTreeSize";
            this.colTreeSize.OptionsColumn.AllowEdit = false;
            this.colTreeSize.OptionsColumn.AllowFocus = false;
            this.colTreeSize.OptionsColumn.ReadOnly = true;
            this.colTreeSize.Width = 40;
            // 
            // colTreeTarget1
            // 
            this.colTreeTarget1.Caption = "Target 1";
            this.colTreeTarget1.FieldName = "TreeTarget1";
            this.colTreeTarget1.Name = "colTreeTarget1";
            this.colTreeTarget1.OptionsColumn.AllowEdit = false;
            this.colTreeTarget1.OptionsColumn.AllowFocus = false;
            this.colTreeTarget1.OptionsColumn.ReadOnly = true;
            this.colTreeTarget1.Width = 62;
            // 
            // colTreeTarget2
            // 
            this.colTreeTarget2.Caption = "Target 2";
            this.colTreeTarget2.FieldName = "TreeTarget2";
            this.colTreeTarget2.Name = "colTreeTarget2";
            this.colTreeTarget2.OptionsColumn.AllowEdit = false;
            this.colTreeTarget2.OptionsColumn.AllowFocus = false;
            this.colTreeTarget2.OptionsColumn.ReadOnly = true;
            this.colTreeTarget2.Width = 62;
            // 
            // colTreeTarget3
            // 
            this.colTreeTarget3.Caption = "Target 3";
            this.colTreeTarget3.FieldName = "TreeTarget3";
            this.colTreeTarget3.Name = "colTreeTarget3";
            this.colTreeTarget3.OptionsColumn.AllowEdit = false;
            this.colTreeTarget3.OptionsColumn.AllowFocus = false;
            this.colTreeTarget3.OptionsColumn.ReadOnly = true;
            this.colTreeTarget3.Width = 62;
            // 
            // colTreeRemarks
            // 
            this.colTreeRemarks.Caption = "Tree Remarks";
            this.colTreeRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colTreeRemarks.FieldName = "TreeRemarks";
            this.colTreeRemarks.Name = "colTreeRemarks";
            this.colTreeRemarks.OptionsColumn.ReadOnly = true;
            this.colTreeRemarks.Width = 87;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colTreeUser1
            // 
            this.colTreeUser1.Caption = "Tree User Defined 1";
            this.colTreeUser1.FieldName = "TreeUser1";
            this.colTreeUser1.Name = "colTreeUser1";
            this.colTreeUser1.OptionsColumn.AllowEdit = false;
            this.colTreeUser1.OptionsColumn.AllowFocus = false;
            this.colTreeUser1.OptionsColumn.ReadOnly = true;
            this.colTreeUser1.Width = 117;
            // 
            // colTreeUser2
            // 
            this.colTreeUser2.Caption = "Tree User Defined 2";
            this.colTreeUser2.FieldName = "TreeUser2";
            this.colTreeUser2.Name = "colTreeUser2";
            this.colTreeUser2.OptionsColumn.AllowEdit = false;
            this.colTreeUser2.OptionsColumn.AllowFocus = false;
            this.colTreeUser2.OptionsColumn.ReadOnly = true;
            this.colTreeUser2.Width = 117;
            // 
            // colTreeUser3
            // 
            this.colTreeUser3.Caption = "Tree User Defined 3";
            this.colTreeUser3.FieldName = "TreeUser3";
            this.colTreeUser3.Name = "colTreeUser3";
            this.colTreeUser3.OptionsColumn.AllowEdit = false;
            this.colTreeUser3.OptionsColumn.AllowFocus = false;
            this.colTreeUser3.OptionsColumn.ReadOnly = true;
            this.colTreeUser3.Width = 117;
            // 
            // colTreeAgeClass
            // 
            this.colTreeAgeClass.Caption = "Age Class";
            this.colTreeAgeClass.FieldName = "TreeAgeClass";
            this.colTreeAgeClass.Name = "colTreeAgeClass";
            this.colTreeAgeClass.OptionsColumn.AllowEdit = false;
            this.colTreeAgeClass.OptionsColumn.AllowFocus = false;
            this.colTreeAgeClass.OptionsColumn.ReadOnly = true;
            this.colTreeAgeClass.Width = 68;
            // 
            // colTreeX
            // 
            this.colTreeX.Caption = "X Coordinate";
            this.colTreeX.FieldName = "TreeX";
            this.colTreeX.Name = "colTreeX";
            this.colTreeX.OptionsColumn.AllowEdit = false;
            this.colTreeX.OptionsColumn.AllowFocus = false;
            this.colTreeX.OptionsColumn.ReadOnly = true;
            this.colTreeX.Width = 83;
            // 
            // colTreeY
            // 
            this.colTreeY.Caption = "Y Coordinate";
            this.colTreeY.FieldName = "TreeY";
            this.colTreeY.Name = "colTreeY";
            this.colTreeY.OptionsColumn.AllowEdit = false;
            this.colTreeY.OptionsColumn.AllowFocus = false;
            this.colTreeY.OptionsColumn.ReadOnly = true;
            this.colTreeY.Width = 83;
            // 
            // colTreeAreaHa
            // 
            this.colTreeAreaHa.Caption = "Area (M�)";
            this.colTreeAreaHa.FieldName = "TreeAreaHa";
            this.colTreeAreaHa.Name = "colTreeAreaHa";
            this.colTreeAreaHa.OptionsColumn.AllowEdit = false;
            this.colTreeAreaHa.OptionsColumn.AllowFocus = false;
            this.colTreeAreaHa.OptionsColumn.ReadOnly = true;
            this.colTreeAreaHa.Width = 61;
            // 
            // colTreeReplantCount
            // 
            this.colTreeReplantCount.Caption = "Replant Count";
            this.colTreeReplantCount.FieldName = "TreeReplantCount";
            this.colTreeReplantCount.Name = "colTreeReplantCount";
            this.colTreeReplantCount.OptionsColumn.AllowEdit = false;
            this.colTreeReplantCount.OptionsColumn.AllowFocus = false;
            this.colTreeReplantCount.OptionsColumn.ReadOnly = true;
            this.colTreeReplantCount.Width = 90;
            // 
            // colTreeSurveyDate
            // 
            this.colTreeSurveyDate.Caption = "Survey Date";
            this.colTreeSurveyDate.FieldName = "TreeSurveyDate";
            this.colTreeSurveyDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTreeSurveyDate.Name = "colTreeSurveyDate";
            this.colTreeSurveyDate.OptionsColumn.AllowEdit = false;
            this.colTreeSurveyDate.OptionsColumn.AllowFocus = false;
            this.colTreeSurveyDate.OptionsColumn.ReadOnly = true;
            this.colTreeSurveyDate.Width = 81;
            // 
            // colTreeType
            // 
            this.colTreeType.Caption = "Tree Type";
            this.colTreeType.FieldName = "TreeType";
            this.colTreeType.Name = "colTreeType";
            this.colTreeType.OptionsColumn.AllowEdit = false;
            this.colTreeType.OptionsColumn.AllowFocus = false;
            this.colTreeType.OptionsColumn.ReadOnly = true;
            this.colTreeType.Width = 70;
            // 
            // colTreeRiskFactor
            // 
            this.colTreeRiskFactor.Caption = "Risk Factor";
            this.colTreeRiskFactor.FieldName = "TreeRiskFactor";
            this.colTreeRiskFactor.Name = "colTreeRiskFactor";
            this.colTreeRiskFactor.OptionsColumn.AllowEdit = false;
            this.colTreeRiskFactor.OptionsColumn.AllowFocus = false;
            this.colTreeRiskFactor.OptionsColumn.ReadOnly = true;
            this.colTreeRiskFactor.Visible = true;
            this.colTreeRiskFactor.VisibleIndex = 33;
            this.colTreeRiskFactor.Width = 74;
            // 
            // colTreePolygonXY
            // 
            this.colTreePolygonXY.Caption = "Polygon XY";
            this.colTreePolygonXY.FieldName = "TreePolygonXY";
            this.colTreePolygonXY.Name = "colTreePolygonXY";
            this.colTreePolygonXY.OptionsColumn.AllowEdit = false;
            this.colTreePolygonXY.OptionsColumn.AllowFocus = false;
            this.colTreePolygonXY.OptionsColumn.ReadOnly = true;
            this.colTreePolygonXY.Width = 74;
            // 
            // colTreeOwnershipName
            // 
            this.colTreeOwnershipName.Caption = "Tree Ownership";
            this.colTreeOwnershipName.FieldName = "TreeOwnershipName";
            this.colTreeOwnershipName.Name = "colTreeOwnershipName";
            this.colTreeOwnershipName.OptionsColumn.AllowEdit = false;
            this.colTreeOwnershipName.OptionsColumn.AllowFocus = false;
            this.colTreeOwnershipName.OptionsColumn.ReadOnly = true;
            this.colTreeOwnershipName.Visible = true;
            this.colTreeOwnershipName.VisibleIndex = 36;
            this.colTreeOwnershipName.Width = 97;
            // 
            // colInspectionReference
            // 
            this.colInspectionReference.Caption = "Inspection Reference Number";
            this.colInspectionReference.FieldName = "InspectionReference";
            this.colInspectionReference.Name = "colInspectionReference";
            this.colInspectionReference.OptionsColumn.AllowEdit = false;
            this.colInspectionReference.OptionsColumn.AllowFocus = false;
            this.colInspectionReference.OptionsColumn.ReadOnly = true;
            this.colInspectionReference.Visible = true;
            this.colInspectionReference.VisibleIndex = 37;
            this.colInspectionReference.Width = 164;
            // 
            // colInspectionInspector
            // 
            this.colInspectionInspector.Caption = "Inspector";
            this.colInspectionInspector.FieldName = "InspectionInspector";
            this.colInspectionInspector.Name = "colInspectionInspector";
            this.colInspectionInspector.OptionsColumn.AllowEdit = false;
            this.colInspectionInspector.OptionsColumn.AllowFocus = false;
            this.colInspectionInspector.OptionsColumn.ReadOnly = true;
            this.colInspectionInspector.Visible = true;
            this.colInspectionInspector.VisibleIndex = 38;
            this.colInspectionInspector.Width = 67;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Width = 97;
            // 
            // colInspectionIncidentReference
            // 
            this.colInspectionIncidentReference.Caption = "Incident Reference";
            this.colInspectionIncidentReference.FieldName = "InspectionIncidentReference";
            this.colInspectionIncidentReference.Name = "colInspectionIncidentReference";
            this.colInspectionIncidentReference.OptionsColumn.AllowEdit = false;
            this.colInspectionIncidentReference.OptionsColumn.AllowFocus = false;
            this.colInspectionIncidentReference.OptionsColumn.ReadOnly = true;
            this.colInspectionIncidentReference.Width = 113;
            // 
            // colInspectionIncidentDate
            // 
            this.colInspectionIncidentDate.Caption = "Incident Date";
            this.colInspectionIncidentDate.FieldName = "InspectionIncidentDate";
            this.colInspectionIncidentDate.Name = "colInspectionIncidentDate";
            this.colInspectionIncidentDate.OptionsColumn.AllowEdit = false;
            this.colInspectionIncidentDate.OptionsColumn.AllowFocus = false;
            this.colInspectionIncidentDate.OptionsColumn.ReadOnly = true;
            this.colInspectionIncidentDate.Width = 86;
            // 
            // colIncidentID
            // 
            this.colIncidentID.Caption = "Incident ID";
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.Width = 74;
            // 
            // colInspectionAngleToVertical
            // 
            this.colInspectionAngleToVertical.Caption = "Angle To Vertical";
            this.colInspectionAngleToVertical.FieldName = "InspectionAngleToVertical";
            this.colInspectionAngleToVertical.Name = "colInspectionAngleToVertical";
            this.colInspectionAngleToVertical.OptionsColumn.AllowEdit = false;
            this.colInspectionAngleToVertical.OptionsColumn.AllowFocus = false;
            this.colInspectionAngleToVertical.OptionsColumn.ReadOnly = true;
            this.colInspectionAngleToVertical.Width = 101;
            // 
            // colInspectionUserDefined1
            // 
            this.colInspectionUserDefined1.Caption = "Inspection User Defined 1";
            this.colInspectionUserDefined1.FieldName = "InspectionUserDefined1";
            this.colInspectionUserDefined1.Name = "colInspectionUserDefined1";
            this.colInspectionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined1.Width = 145;
            // 
            // colInspectionUserDefined2
            // 
            this.colInspectionUserDefined2.Caption = "Inspection User Defined 2";
            this.colInspectionUserDefined2.FieldName = "InspectionUserDefined2";
            this.colInspectionUserDefined2.Name = "colInspectionUserDefined2";
            this.colInspectionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined2.Width = 145;
            // 
            // colInspectionUserDefined3
            // 
            this.colInspectionUserDefined3.Caption = "Inspection User Defined 3";
            this.colInspectionUserDefined3.FieldName = "InspectionUserDefined3";
            this.colInspectionUserDefined3.Name = "colInspectionUserDefined3";
            this.colInspectionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined3.Width = 145;
            // 
            // colInspectionRemarks
            // 
            this.colInspectionRemarks.Caption = "Inspection Remarks";
            this.colInspectionRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colInspectionRemarks.FieldName = "InspectionRemarks";
            this.colInspectionRemarks.Name = "colInspectionRemarks";
            this.colInspectionRemarks.OptionsColumn.ReadOnly = true;
            this.colInspectionRemarks.Width = 115;
            // 
            // colInspectionStemPhysical1
            // 
            this.colInspectionStemPhysical1.Caption = "Stem Physical 1";
            this.colInspectionStemPhysical1.FieldName = "InspectionStemPhysical1";
            this.colInspectionStemPhysical1.Name = "colInspectionStemPhysical1";
            this.colInspectionStemPhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical1.Width = 95;
            // 
            // colInspectionStemPhysical2
            // 
            this.colInspectionStemPhysical2.Caption = "Stem Physical 2";
            this.colInspectionStemPhysical2.FieldName = "InspectionStemPhysical2";
            this.colInspectionStemPhysical2.Name = "colInspectionStemPhysical2";
            this.colInspectionStemPhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical2.Width = 95;
            // 
            // colInspectionStemPhysical3
            // 
            this.colInspectionStemPhysical3.Caption = "Stem Physical 3";
            this.colInspectionStemPhysical3.FieldName = "InspectionStemPhysical3";
            this.colInspectionStemPhysical3.Name = "colInspectionStemPhysical3";
            this.colInspectionStemPhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical3.Width = 95;
            // 
            // colInspectionStemDisease1
            // 
            this.colInspectionStemDisease1.Caption = "Stem Disease 1";
            this.colInspectionStemDisease1.FieldName = "InspectionStemDisease1";
            this.colInspectionStemDisease1.Name = "colInspectionStemDisease1";
            this.colInspectionStemDisease1.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease1.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease1.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease1.Width = 94;
            // 
            // colInspectionStemDisease2
            // 
            this.colInspectionStemDisease2.Caption = "Stem Disease 2";
            this.colInspectionStemDisease2.FieldName = "InspectionStemDisease2";
            this.colInspectionStemDisease2.Name = "colInspectionStemDisease2";
            this.colInspectionStemDisease2.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease2.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease2.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease2.Width = 94;
            // 
            // colInspectionStemDisease3
            // 
            this.colInspectionStemDisease3.Caption = "Stem Disease 3";
            this.colInspectionStemDisease3.FieldName = "InspectionStemDisease3";
            this.colInspectionStemDisease3.Name = "colInspectionStemDisease3";
            this.colInspectionStemDisease3.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease3.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease3.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease3.Width = 94;
            // 
            // colInspectionCrownPhysical1
            // 
            this.colInspectionCrownPhysical1.Caption = "Crown Physical 1";
            this.colInspectionCrownPhysical1.FieldName = "InspectionCrownPhysical1";
            this.colInspectionCrownPhysical1.Name = "colInspectionCrownPhysical1";
            this.colInspectionCrownPhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical1.Width = 102;
            // 
            // colInspectionCrownPhysical2
            // 
            this.colInspectionCrownPhysical2.Caption = "Crown Physical 2";
            this.colInspectionCrownPhysical2.FieldName = "InspectionCrownPhysical2";
            this.colInspectionCrownPhysical2.Name = "colInspectionCrownPhysical2";
            this.colInspectionCrownPhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical2.Width = 102;
            // 
            // colInspectionCrownPhysical3
            // 
            this.colInspectionCrownPhysical3.Caption = "Crown Physical 3";
            this.colInspectionCrownPhysical3.FieldName = "InspectionCrownPhysical3";
            this.colInspectionCrownPhysical3.Name = "colInspectionCrownPhysical3";
            this.colInspectionCrownPhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical3.Width = 102;
            // 
            // colInspectionCrownDisease1
            // 
            this.colInspectionCrownDisease1.Caption = "Crown Physical 1";
            this.colInspectionCrownDisease1.FieldName = "InspectionCrownDisease1";
            this.colInspectionCrownDisease1.Name = "colInspectionCrownDisease1";
            this.colInspectionCrownDisease1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease1.Width = 102;
            // 
            // colInspectionCrownDisease2
            // 
            this.colInspectionCrownDisease2.Caption = "Crown Physical 2";
            this.colInspectionCrownDisease2.FieldName = "InspectionCrownDisease2";
            this.colInspectionCrownDisease2.Name = "colInspectionCrownDisease2";
            this.colInspectionCrownDisease2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease2.Width = 102;
            // 
            // colInspectionCrownDisease3
            // 
            this.colInspectionCrownDisease3.Caption = "Crown Physical 3";
            this.colInspectionCrownDisease3.FieldName = "InspectionCrownDisease3";
            this.colInspectionCrownDisease3.Name = "colInspectionCrownDisease3";
            this.colInspectionCrownDisease3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease3.Width = 102;
            // 
            // colInspectionCrownFoliation1
            // 
            this.colInspectionCrownFoliation1.Caption = "Crown Foliation 1";
            this.colInspectionCrownFoliation1.FieldName = "InspectionCrownFoliation1";
            this.colInspectionCrownFoliation1.Name = "colInspectionCrownFoliation1";
            this.colInspectionCrownFoliation1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation1.Width = 104;
            // 
            // colInspectionCrownFoliation2
            // 
            this.colInspectionCrownFoliation2.Caption = "Crown Foliation 2";
            this.colInspectionCrownFoliation2.FieldName = "InspectionCrownFoliation2";
            this.colInspectionCrownFoliation2.Name = "colInspectionCrownFoliation2";
            this.colInspectionCrownFoliation2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation2.Width = 104;
            // 
            // colInspectionCrownFoliation3
            // 
            this.colInspectionCrownFoliation3.Caption = "Crown Foliation 3";
            this.colInspectionCrownFoliation3.FieldName = "InspectionCrownFoliation3";
            this.colInspectionCrownFoliation3.Name = "colInspectionCrownFoliation3";
            this.colInspectionCrownFoliation3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation3.Width = 104;
            // 
            // colInspectionRiskCategory
            // 
            this.colInspectionRiskCategory.Caption = "Inspection Risk Category";
            this.colInspectionRiskCategory.FieldName = "InspectionRiskCategory";
            this.colInspectionRiskCategory.Name = "colInspectionRiskCategory";
            this.colInspectionRiskCategory.OptionsColumn.AllowEdit = false;
            this.colInspectionRiskCategory.OptionsColumn.AllowFocus = false;
            this.colInspectionRiskCategory.OptionsColumn.ReadOnly = true;
            this.colInspectionRiskCategory.Visible = true;
            this.colInspectionRiskCategory.VisibleIndex = 39;
            this.colInspectionRiskCategory.Width = 141;
            // 
            // colInspectionGeneralCondition
            // 
            this.colInspectionGeneralCondition.Caption = "General Condition";
            this.colInspectionGeneralCondition.FieldName = "InspectionGeneralCondition";
            this.colInspectionGeneralCondition.Name = "colInspectionGeneralCondition";
            this.colInspectionGeneralCondition.OptionsColumn.AllowEdit = false;
            this.colInspectionGeneralCondition.OptionsColumn.AllowFocus = false;
            this.colInspectionGeneralCondition.OptionsColumn.ReadOnly = true;
            this.colInspectionGeneralCondition.Width = 106;
            // 
            // colInspectionBasePhysical1
            // 
            this.colInspectionBasePhysical1.Caption = "Base Physical 1";
            this.colInspectionBasePhysical1.FieldName = "InspectionBasePhysical1";
            this.colInspectionBasePhysical1.Name = "colInspectionBasePhysical1";
            this.colInspectionBasePhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical1.Width = 94;
            // 
            // colInspectionBasePhysical2
            // 
            this.colInspectionBasePhysical2.Caption = "Base Physical 2";
            this.colInspectionBasePhysical2.FieldName = "InspectionBasePhysical2";
            this.colInspectionBasePhysical2.Name = "colInspectionBasePhysical2";
            this.colInspectionBasePhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical2.Width = 94;
            // 
            // colInspectionBasePhysical3
            // 
            this.colInspectionBasePhysical3.Caption = "Base Physical 3";
            this.colInspectionBasePhysical3.FieldName = "InspectionBasePhysical3";
            this.colInspectionBasePhysical3.Name = "colInspectionBasePhysical3";
            this.colInspectionBasePhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical3.Width = 94;
            // 
            // colInspectionVitality
            // 
            this.colInspectionVitality.Caption = "Vitality";
            this.colInspectionVitality.FieldName = "InspectionVitality";
            this.colInspectionVitality.Name = "colInspectionVitality";
            this.colInspectionVitality.OptionsColumn.AllowEdit = false;
            this.colInspectionVitality.OptionsColumn.AllowFocus = false;
            this.colInspectionVitality.OptionsColumn.ReadOnly = true;
            this.colInspectionVitality.Width = 53;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            this.colActionID.Width = 65;
            // 
            // colActionJobNumber
            // 
            this.colActionJobNumber.Caption = "Job Number";
            this.colActionJobNumber.FieldName = "ActionJobNumber";
            this.colActionJobNumber.Name = "colActionJobNumber";
            this.colActionJobNumber.OptionsColumn.AllowEdit = false;
            this.colActionJobNumber.OptionsColumn.AllowFocus = false;
            this.colActionJobNumber.OptionsColumn.ReadOnly = true;
            this.colActionJobNumber.Visible = true;
            this.colActionJobNumber.VisibleIndex = 3;
            this.colActionJobNumber.Width = 78;
            // 
            // colAction
            // 
            this.colAction.Caption = "Action";
            this.colAction.FieldName = "Action";
            this.colAction.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAction.Name = "colAction";
            this.colAction.OptionsColumn.AllowEdit = false;
            this.colAction.OptionsColumn.AllowFocus = false;
            this.colAction.OptionsColumn.ReadOnly = true;
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 2;
            this.colAction.Width = 242;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Action Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 4;
            this.colActionPriority.Width = 88;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Action Due Date";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 0;
            this.colActionDueDate.Width = 172;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Action Done Date";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 1;
            this.colActionDoneDate.Width = 105;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action Carried Out By";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            this.colActionBy.Visible = true;
            this.colActionBy.VisibleIndex = 5;
            this.colActionBy.Width = 125;
            // 
            // colActionSupervisor
            // 
            this.colActionSupervisor.Caption = "Action Supervisor";
            this.colActionSupervisor.FieldName = "ActionSupervisor";
            this.colActionSupervisor.Name = "colActionSupervisor";
            this.colActionSupervisor.OptionsColumn.AllowEdit = false;
            this.colActionSupervisor.OptionsColumn.AllowFocus = false;
            this.colActionSupervisor.OptionsColumn.ReadOnly = true;
            this.colActionSupervisor.Visible = true;
            this.colActionSupervisor.VisibleIndex = 6;
            this.colActionSupervisor.Width = 105;
            // 
            // colActionOwnership
            // 
            this.colActionOwnership.Caption = "Action Ownership";
            this.colActionOwnership.FieldName = "ActionOwnership";
            this.colActionOwnership.Name = "colActionOwnership";
            this.colActionOwnership.OptionsColumn.AllowEdit = false;
            this.colActionOwnership.OptionsColumn.AllowFocus = false;
            this.colActionOwnership.OptionsColumn.ReadOnly = true;
            this.colActionOwnership.Visible = true;
            this.colActionOwnership.VisibleIndex = 7;
            this.colActionOwnership.Width = 105;
            // 
            // colActionCostCentre
            // 
            this.colActionCostCentre.Caption = "Action Cost Centre";
            this.colActionCostCentre.FieldName = "ActionCostCentre";
            this.colActionCostCentre.Name = "colActionCostCentre";
            this.colActionCostCentre.OptionsColumn.AllowEdit = false;
            this.colActionCostCentre.OptionsColumn.AllowFocus = false;
            this.colActionCostCentre.OptionsColumn.ReadOnly = true;
            this.colActionCostCentre.Visible = true;
            this.colActionCostCentre.VisibleIndex = 13;
            this.colActionCostCentre.Width = 112;
            // 
            // colActionBudget
            // 
            this.colActionBudget.Caption = "Action Budget";
            this.colActionBudget.FieldName = "ActionBudget";
            this.colActionBudget.Name = "colActionBudget";
            this.colActionBudget.OptionsColumn.AllowEdit = false;
            this.colActionBudget.OptionsColumn.AllowFocus = false;
            this.colActionBudget.OptionsColumn.ReadOnly = true;
            this.colActionBudget.Visible = true;
            this.colActionBudget.VisibleIndex = 12;
            this.colActionBudget.Width = 91;
            // 
            // colActionWorkUnits
            // 
            this.colActionWorkUnits.Caption = "Action Work Units";
            this.colActionWorkUnits.FieldName = "ActionWorkUnits";
            this.colActionWorkUnits.Name = "colActionWorkUnits";
            this.colActionWorkUnits.OptionsColumn.AllowEdit = false;
            this.colActionWorkUnits.OptionsColumn.AllowFocus = false;
            this.colActionWorkUnits.OptionsColumn.ReadOnly = true;
            this.colActionWorkUnits.Visible = true;
            this.colActionWorkUnits.VisibleIndex = 10;
            this.colActionWorkUnits.Width = 106;
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Action Schedule of Rates";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 11;
            this.colActionScheduleOfRates.Width = 141;
            // 
            // colActionBudgetedRateDescription
            // 
            this.colActionBudgetedRateDescription.Caption = "Action Budgeted Rate Description";
            this.colActionBudgetedRateDescription.FieldName = "ActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.Name = "colActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRateDescription.Visible = true;
            this.colActionBudgetedRateDescription.VisibleIndex = 15;
            this.colActionBudgetedRateDescription.Width = 182;
            // 
            // colActionBudgetedRate
            // 
            this.colActionBudgetedRate.Caption = "Action Budgeted Rate";
            this.colActionBudgetedRate.FieldName = "ActionBudgetedRate";
            this.colActionBudgetedRate.Name = "colActionBudgetedRate";
            this.colActionBudgetedRate.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRate.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRate.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRate.Visible = true;
            this.colActionBudgetedRate.VisibleIndex = 14;
            this.colActionBudgetedRate.Width = 126;
            // 
            // colActionBudgetedCost
            // 
            this.colActionBudgetedCost.Caption = "Action Budgeted Cost";
            this.colActionBudgetedCost.FieldName = "ActionBudgetedCost";
            this.colActionBudgetedCost.Name = "colActionBudgetedCost";
            this.colActionBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedCost.Visible = true;
            this.colActionBudgetedCost.VisibleIndex = 16;
            this.colActionBudgetedCost.Width = 125;
            // 
            // colActionActualRateDescription
            // 
            this.colActionActualRateDescription.Caption = "Action Actual Rate Description";
            this.colActionActualRateDescription.FieldName = "ActionActualRateDescription";
            this.colActionActualRateDescription.Name = "colActionActualRateDescription";
            this.colActionActualRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionActualRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionActualRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionActualRateDescription.Visible = true;
            this.colActionActualRateDescription.VisibleIndex = 18;
            this.colActionActualRateDescription.Width = 166;
            // 
            // colActionActualRate
            // 
            this.colActionActualRate.Caption = "Action Actual Rate";
            this.colActionActualRate.FieldName = "ActionActualRate";
            this.colActionActualRate.Name = "colActionActualRate";
            this.colActionActualRate.OptionsColumn.AllowEdit = false;
            this.colActionActualRate.OptionsColumn.AllowFocus = false;
            this.colActionActualRate.OptionsColumn.ReadOnly = true;
            this.colActionActualRate.Visible = true;
            this.colActionActualRate.VisibleIndex = 17;
            this.colActionActualRate.Width = 105;
            // 
            // colActionActualCost
            // 
            this.colActionActualCost.Caption = "Action Actual Cost";
            this.colActionActualCost.FieldName = "ActionActualCost";
            this.colActionActualCost.Name = "colActionActualCost";
            this.colActionActualCost.OptionsColumn.AllowEdit = false;
            this.colActionActualCost.OptionsColumn.AllowFocus = false;
            this.colActionActualCost.OptionsColumn.ReadOnly = true;
            this.colActionActualCost.Visible = true;
            this.colActionActualCost.VisibleIndex = 19;
            this.colActionActualCost.Width = 109;
            // 
            // colActionWorkOrder
            // 
            this.colActionWorkOrder.Caption = "Action Work Order";
            this.colActionWorkOrder.FieldName = "ActionWorkOrder";
            this.colActionWorkOrder.Name = "colActionWorkOrder";
            this.colActionWorkOrder.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrder.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrder.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrder.Visible = true;
            this.colActionWorkOrder.VisibleIndex = 21;
            this.colActionWorkOrder.Width = 110;
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.Caption = "Action Remarks";
            this.colActionRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 24;
            this.colActionRemarks.Width = 95;
            // 
            // colActionUserDefined1
            // 
            this.colActionUserDefined1.Caption = "Action User Defined 1";
            this.colActionUserDefined1.FieldName = "ActionUserDefined1";
            this.colActionUserDefined1.Name = "colActionUserDefined1";
            this.colActionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined1.Visible = true;
            this.colActionUserDefined1.VisibleIndex = 25;
            this.colActionUserDefined1.Width = 125;
            // 
            // colActionUserDefined2
            // 
            this.colActionUserDefined2.Caption = "Action User Defined 2";
            this.colActionUserDefined2.FieldName = "ActionUserDefined2";
            this.colActionUserDefined2.Name = "colActionUserDefined2";
            this.colActionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined2.Visible = true;
            this.colActionUserDefined2.VisibleIndex = 26;
            this.colActionUserDefined2.Width = 125;
            // 
            // colActionUserDefined3
            // 
            this.colActionUserDefined3.Caption = "Action User Defined 3";
            this.colActionUserDefined3.FieldName = "ActionUserDefined3";
            this.colActionUserDefined3.Name = "colActionUserDefined3";
            this.colActionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined3.Visible = true;
            this.colActionUserDefined3.VisibleIndex = 27;
            this.colActionUserDefined3.Width = 125;
            // 
            // colActionCostDifference
            // 
            this.colActionCostDifference.Caption = "Action Cost Difference";
            this.colActionCostDifference.FieldName = "ActionCostDifference";
            this.colActionCostDifference.Name = "colActionCostDifference";
            this.colActionCostDifference.OptionsColumn.AllowEdit = false;
            this.colActionCostDifference.OptionsColumn.AllowFocus = false;
            this.colActionCostDifference.OptionsColumn.ReadOnly = true;
            this.colActionCostDifference.Visible = true;
            this.colActionCostDifference.VisibleIndex = 20;
            this.colActionCostDifference.Width = 129;
            // 
            // colActionCompletionStatus
            // 
            this.colActionCompletionStatus.Caption = "Action Completion Status";
            this.colActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.colActionCompletionStatus.Name = "colActionCompletionStatus";
            this.colActionCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colActionCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colActionCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colActionCompletionStatus.Visible = true;
            this.colActionCompletionStatus.VisibleIndex = 8;
            this.colActionCompletionStatus.Width = 141;
            // 
            // colActionTimeliness
            // 
            this.colActionTimeliness.Caption = "Action Timeliness";
            this.colActionTimeliness.FieldName = "ActionTimeliness";
            this.colActionTimeliness.Name = "colActionTimeliness";
            this.colActionTimeliness.OptionsColumn.AllowEdit = false;
            this.colActionTimeliness.OptionsColumn.AllowFocus = false;
            this.colActionTimeliness.OptionsColumn.ReadOnly = true;
            this.colActionTimeliness.Visible = true;
            this.colActionTimeliness.VisibleIndex = 9;
            this.colActionTimeliness.Width = 102;
            // 
            // colActionWorkOrderIssueDate
            // 
            this.colActionWorkOrderIssueDate.Caption = "Action Work Order Issue Date";
            this.colActionWorkOrderIssueDate.FieldName = "ActionWorkOrderIssueDate";
            this.colActionWorkOrderIssueDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActionWorkOrderIssueDate.Name = "colActionWorkOrderIssueDate";
            this.colActionWorkOrderIssueDate.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrderIssueDate.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrderIssueDate.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrderIssueDate.Visible = true;
            this.colActionWorkOrderIssueDate.VisibleIndex = 22;
            this.colActionWorkOrderIssueDate.Width = 165;
            // 
            // colActionWorkOrderCompleteDate
            // 
            this.colActionWorkOrderCompleteDate.Caption = "Action Work Order Complete Date";
            this.colActionWorkOrderCompleteDate.FieldName = "ActionWorkOrderCompleteDate";
            this.colActionWorkOrderCompleteDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActionWorkOrderCompleteDate.Name = "colActionWorkOrderCompleteDate";
            this.colActionWorkOrderCompleteDate.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrderCompleteDate.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrderCompleteDate.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrderCompleteDate.Visible = true;
            this.colActionWorkOrderCompleteDate.VisibleIndex = 23;
            this.colActionWorkOrderCompleteDate.Width = 184;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "<b>Calculated 1<b>";
            this.gridColumn1.FieldName = "Calculated1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.ShowUnboundExpressionMenu = true;
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 40;
            this.gridColumn1.Width = 90;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "<b>Calculated 2<b>";
            this.gridColumn2.FieldName = "Calculated2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.ShowUnboundExpressionMenu = true;
            this.gridColumn2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 41;
            this.gridColumn2.Width = 90;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "<b>Calculated 3<b>";
            this.gridColumn3.FieldName = "Calculated3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.ShowUnboundExpressionMenu = true;
            this.gridColumn3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 42;
            this.gridColumn3.Width = 90;
            // 
            // colTreeOwnershipID
            // 
            this.colTreeOwnershipID.FieldName = "TreeOwnershipID";
            this.colTreeOwnershipID.Name = "colTreeOwnershipID";
            this.colTreeOwnershipID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colActionUserPickList1
            // 
            this.colActionUserPickList1.Caption = "Action User Defined Picklist 1";
            this.colActionUserPickList1.FieldName = "ActionUserPickList1";
            this.colActionUserPickList1.Name = "colActionUserPickList1";
            this.colActionUserPickList1.OptionsColumn.AllowEdit = false;
            this.colActionUserPickList1.OptionsColumn.AllowFocus = false;
            this.colActionUserPickList1.OptionsColumn.ReadOnly = true;
            this.colActionUserPickList1.Visible = true;
            this.colActionUserPickList1.VisibleIndex = 28;
            this.colActionUserPickList1.Width = 157;
            // 
            // colActionUserPickList2
            // 
            this.colActionUserPickList2.Caption = "Action User Defined Picklist 2";
            this.colActionUserPickList2.FieldName = "ActionUserPickList2";
            this.colActionUserPickList2.Name = "colActionUserPickList2";
            this.colActionUserPickList2.OptionsColumn.AllowEdit = false;
            this.colActionUserPickList2.OptionsColumn.AllowFocus = false;
            this.colActionUserPickList2.OptionsColumn.ReadOnly = true;
            this.colActionUserPickList2.Visible = true;
            this.colActionUserPickList2.VisibleIndex = 30;
            this.colActionUserPickList2.Width = 157;
            // 
            // colActionUserPickList3
            // 
            this.colActionUserPickList3.Caption = "Action User Defined Picklist 3";
            this.colActionUserPickList3.FieldName = "ActionUserPickList3";
            this.colActionUserPickList3.Name = "colActionUserPickList3";
            this.colActionUserPickList3.OptionsColumn.AllowEdit = false;
            this.colActionUserPickList3.OptionsColumn.AllowFocus = false;
            this.colActionUserPickList3.OptionsColumn.ReadOnly = true;
            this.colActionUserPickList3.Visible = true;
            this.colActionUserPickList3.VisibleIndex = 29;
            this.colActionUserPickList3.Width = 157;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Visible = true;
            this.colSelected.VisibleIndex = 45;
            this.colSelected.Width = 60;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 43;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Visible = true;
            this.colSitePostcode.VisibleIndex = 44;
            this.colSitePostcode.Width = 84;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(1037, 245);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage1});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl39);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1032, 219);
            this.xtraTabPage2.Text = "Linked Pictures";
            // 
            // gridControl39
            // 
            this.gridControl39.DataSource = this.sp02066ATActionPicturesListBindingSource;
            this.gridControl39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl39.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl39.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl39_EmbeddedNavigator_ButtonClick);
            this.gridControl39.Location = new System.Drawing.Point(0, 0);
            this.gridControl39.MainView = this.gridView39;
            this.gridControl39.MenuManager = this.barManager1;
            this.gridControl39.Name = "gridControl39";
            this.gridControl39.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextEditDateTime});
            this.gridControl39.Size = new System.Drawing.Size(1032, 219);
            this.gridControl39.TabIndex = 83;
            this.gridControl39.UseEmbeddedNavigator = true;
            this.gridControl39.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView39});
            // 
            // sp02066ATActionPicturesListBindingSource
            // 
            this.sp02066ATActionPicturesListBindingSource.DataMember = "sp02066_AT_Action_Pictures_List";
            this.sp02066ATActionPicturesListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView39
            // 
            this.gridView39.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn169,
            this.gridColumn170,
            this.gridColumn171,
            this.gridColumn172,
            this.gridColumn173,
            this.gridColumn174,
            this.gridColumn175,
            this.gridColumn176,
            this.gridColumn177,
            this.gridColumn178,
            this.gridColumn179,
            this.colShortLinkedRecordDescription});
            this.gridView39.GridControl = this.gridControl39;
            this.gridView39.GroupCount = 1;
            this.gridView39.Name = "gridView39";
            this.gridView39.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView39.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView39.OptionsLayout.StoreAppearance = true;
            this.gridView39.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView39.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView39.OptionsSelection.MultiSelect = true;
            this.gridView39.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView39.OptionsView.ColumnAutoWidth = false;
            this.gridView39.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView39.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView39.OptionsView.ShowGroupPanel = false;
            this.gridView39.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn178, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn174, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView39.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView39_CustomDrawCell);
            this.gridView39.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView39.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView39.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView39.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView39.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView39.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView39_MouseUp);
            this.gridView39.DoubleClick += new System.EventHandler(this.gridView39_DoubleClick);
            this.gridView39.GotFocus += new System.EventHandler(this.gridView39_GotFocus);
            // 
            // gridColumn169
            // 
            this.gridColumn169.Caption = "Survey Picture ID";
            this.gridColumn169.FieldName = "SurveyPictureID";
            this.gridColumn169.Name = "gridColumn169";
            this.gridColumn169.OptionsColumn.AllowEdit = false;
            this.gridColumn169.OptionsColumn.AllowFocus = false;
            this.gridColumn169.OptionsColumn.ReadOnly = true;
            this.gridColumn169.Width = 105;
            // 
            // gridColumn170
            // 
            this.gridColumn170.Caption = "Linked To Record ID";
            this.gridColumn170.FieldName = "LinkedToRecordID";
            this.gridColumn170.Name = "gridColumn170";
            this.gridColumn170.OptionsColumn.AllowEdit = false;
            this.gridColumn170.OptionsColumn.AllowFocus = false;
            this.gridColumn170.OptionsColumn.ReadOnly = true;
            this.gridColumn170.Width = 117;
            // 
            // gridColumn171
            // 
            this.gridColumn171.Caption = "Linked To Record Type ID";
            this.gridColumn171.FieldName = "LinkedToRecordTypeID";
            this.gridColumn171.Name = "gridColumn171";
            this.gridColumn171.OptionsColumn.AllowEdit = false;
            this.gridColumn171.OptionsColumn.AllowFocus = false;
            this.gridColumn171.OptionsColumn.ReadOnly = true;
            this.gridColumn171.Width = 144;
            // 
            // gridColumn172
            // 
            this.gridColumn172.Caption = "Picture Type ID";
            this.gridColumn172.FieldName = "PictureTypeID";
            this.gridColumn172.Name = "gridColumn172";
            this.gridColumn172.OptionsColumn.AllowEdit = false;
            this.gridColumn172.OptionsColumn.AllowFocus = false;
            this.gridColumn172.OptionsColumn.ReadOnly = true;
            this.gridColumn172.Width = 95;
            // 
            // gridColumn173
            // 
            this.gridColumn173.Caption = "Picture Path";
            this.gridColumn173.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.gridColumn173.FieldName = "PicturePath";
            this.gridColumn173.Name = "gridColumn173";
            this.gridColumn173.OptionsColumn.ReadOnly = true;
            this.gridColumn173.Visible = true;
            this.gridColumn173.VisibleIndex = 2;
            this.gridColumn173.Width = 472;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // gridColumn174
            // 
            this.gridColumn174.Caption = "Date Taken";
            this.gridColumn174.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.gridColumn174.FieldName = "DateTimeTaken";
            this.gridColumn174.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn174.Name = "gridColumn174";
            this.gridColumn174.OptionsColumn.AllowEdit = false;
            this.gridColumn174.OptionsColumn.AllowFocus = false;
            this.gridColumn174.OptionsColumn.ReadOnly = true;
            this.gridColumn174.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn174.Visible = true;
            this.gridColumn174.VisibleIndex = 0;
            this.gridColumn174.Width = 99;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // gridColumn175
            // 
            this.gridColumn175.Caption = "Added By Staff ID";
            this.gridColumn175.FieldName = "AddedByStaffID";
            this.gridColumn175.Name = "gridColumn175";
            this.gridColumn175.OptionsColumn.AllowEdit = false;
            this.gridColumn175.OptionsColumn.AllowFocus = false;
            this.gridColumn175.OptionsColumn.ReadOnly = true;
            this.gridColumn175.Width = 108;
            // 
            // gridColumn176
            // 
            this.gridColumn176.Caption = "GUID";
            this.gridColumn176.FieldName = "GUID";
            this.gridColumn176.Name = "gridColumn176";
            this.gridColumn176.OptionsColumn.AllowEdit = false;
            this.gridColumn176.OptionsColumn.AllowFocus = false;
            this.gridColumn176.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn177
            // 
            this.gridColumn177.Caption = "Remarks";
            this.gridColumn177.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn177.FieldName = "Remarks";
            this.gridColumn177.Name = "gridColumn177";
            this.gridColumn177.OptionsColumn.ReadOnly = true;
            this.gridColumn177.Visible = true;
            this.gridColumn177.VisibleIndex = 3;
            this.gridColumn177.Width = 252;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // gridColumn178
            // 
            this.gridColumn178.Caption = "Linked To Action";
            this.gridColumn178.FieldName = "LinkedRecordDescription";
            this.gridColumn178.Name = "gridColumn178";
            this.gridColumn178.OptionsColumn.AllowEdit = false;
            this.gridColumn178.OptionsColumn.AllowFocus = false;
            this.gridColumn178.OptionsColumn.ReadOnly = true;
            this.gridColumn178.Visible = true;
            this.gridColumn178.VisibleIndex = 4;
            this.gridColumn178.Width = 303;
            // 
            // gridColumn179
            // 
            this.gridColumn179.Caption = "Added By";
            this.gridColumn179.FieldName = "AddedByStaffName";
            this.gridColumn179.Name = "gridColumn179";
            this.gridColumn179.OptionsColumn.AllowEdit = false;
            this.gridColumn179.OptionsColumn.AllowFocus = false;
            this.gridColumn179.OptionsColumn.ReadOnly = true;
            this.gridColumn179.Visible = true;
            this.gridColumn179.VisibleIndex = 1;
            this.gridColumn179.Width = 108;
            // 
            // colShortLinkedRecordDescription
            // 
            this.colShortLinkedRecordDescription.Caption = "Linked To Action (Short Desc)";
            this.colShortLinkedRecordDescription.FieldName = "ShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.Name = "colShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colShortLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colShortLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colShortLinkedRecordDescription.Width = 231;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1032, 219);
            this.xtraTabPage1.Text = "Linked Documents";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl4;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1032, 219);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl4.Size = new System.Drawing.Size(1032, 219);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // checkEditShowLastInspection
            // 
            this.checkEditShowLastInspection.Location = new System.Drawing.Point(7, 55);
            this.checkEditShowLastInspection.MenuManager = this.barManager1;
            this.checkEditShowLastInspection.Name = "checkEditShowLastInspection";
            this.checkEditShowLastInspection.Properties.Caption = "Show Last Inspection Only";
            this.checkEditShowLastInspection.Size = new System.Drawing.Size(314, 19);
            this.checkEditShowLastInspection.StyleController = this.layoutControl2;
            this.checkEditShowLastInspection.TabIndex = 8;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btnLoad);
            this.layoutControl2.Controls.Add(this.buttonEditSiteFilter);
            this.layoutControl2.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl2.Controls.Add(this.checkEditShowLastInspection);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(853, 456, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(328, 627);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.ImageIndex = 5;
            this.btnLoad.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoad.Location = new System.Drawing.Point(239, 180);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(82, 22);
            this.btnLoad.StyleController = this.layoutControl2;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Load Data - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to Load Action Data.\r\n\r\nOnly data matching the specified Filter Criteria" +
    " will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnLoad.SuperTip = superToolTip2;
            this.btnLoad.TabIndex = 15;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // buttonEditSiteFilter
            // 
            this.buttonEditSiteFilter.Location = new System.Drawing.Point(64, 31);
            this.buttonEditSiteFilter.MenuManager = this.barManager1;
            this.buttonEditSiteFilter.Name = "buttonEditSiteFilter";
            this.buttonEditSiteFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Choose Site Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditSiteFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSiteFilter.Size = new System.Drawing.Size(257, 20);
            this.buttonEditSiteFilter.StyleController = this.layoutControl2;
            this.buttonEditSiteFilter.TabIndex = 14;
            this.buttonEditSiteFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSiteFilter_ButtonClick);
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.Location = new System.Drawing.Point(64, 7);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(257, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl2;
            this.buttonEditClientFilter.TabIndex = 7;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(76, 120);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Clear Date - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, superToolTip3, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(233, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(76, 144);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Clear Date - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, superToolTip4, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(233, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 10;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlGroup3,
            this.emptySpaceItem3,
            this.layoutControlItem4,
            this.emptySpaceItem4});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(328, 627);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonEditClientFilter;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem6.Text = "Client:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonEditSiteFilter;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem7.Text = "Site:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(54, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 199);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(318, 418);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkEditShowLastInspection;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(318, 23);
            this.layoutControlItem1.Text = "Last Inspection Only:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 81);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(318, 92);
            this.layoutControlGroup3.Text = "Action Due Date:";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditFromDate;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem2.Text = "From Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dateEditToDate;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem3.Text = "To Date:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(54, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(318, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnLoad;
            this.layoutControlItem4.Location = new System.Drawing.Point(232, 173);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(86, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(86, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 173);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(232, 26);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp01334_AT_Action_ManagerTableAdapter
            // 
            this.sp01334_AT_Action_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp02066_AT_Action_Pictures_ListTableAdapter
            // 
            this.sp02066_AT_Action_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 659);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.ID = new System.Guid("cfa6aa04-7a42-43c1-9b83-5fe8dca34d55");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(334, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(334, 659);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(328, 627);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // bar1
            // 
            this.bar1.BarName = "Filter";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(895, 171);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterData),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 29;
            this.bbiRefresh.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bciFilterData
            // 
            this.bciFilterData.Caption = "Filter Selected";
            this.bciFilterData.Id = 30;
            this.bciFilterData.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Filter_32x32;
            this.bciFilterData.Name = "bciFilterData";
            this.bciFilterData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Filter Selected - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciFilterData.SuperTip = superToolTip1;
            this.bciFilterData.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterData_CheckedChanged);
            // 
            // bsiSelectedCount
            // 
            this.bsiSelectedCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount.Caption = "0 Actions Selected";
            this.bsiSelectedCount.Id = 31;
            this.bsiSelectedCount.ImageOptions.ImageIndex = 4;
            this.bsiSelectedCount.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.Name = "bsiSelectedCount";
            this.bsiSelectedCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending39
            // 
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending39.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending39.GridControl = this.gridControl39;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // frm_AT_Action_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1060, 659);
            this.Controls.Add(this.splitContainerControl2);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Action_Manager";
            this.Text = "Action Manager";
            this.Activated += new System.EventHandler(this.frm_AT_Action_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Action_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Action_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01334ATActionManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02066ATActionPicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowLastInspection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.CheckEdit checkEditShowLastInspection;
        private System.Windows.Forms.BindingSource sp01334ATActionManagerBindingSource;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeVisibility;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeContext;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeManagement;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGroundType;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeProtectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeCrownDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePlantDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGroupNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSiteHazardClass;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePlantSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePlantSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePlantMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMapLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeTarget1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeTarget2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeTarget3;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeAgeClass;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeX;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeY;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeAreaHa;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplantCount;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeType;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeRiskFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionInspector;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionIncidentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionIncidentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionAngleToVertical;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionGeneralCondition;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionVitality;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colActionOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudget;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostDifference;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTimeliness;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrderIssueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrderCompleteDate;
        private WoodPlan5.DataSet_ATTableAdapters.sp01334_AT_Action_ManagerTableAdapter sp01334_AT_Action_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeOwnershipID;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl39;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn169;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn170;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn171;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn172;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn173;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn174;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn175;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn176;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn177;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn178;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn179;
        private DevExpress.XtraGrid.Columns.GridColumn colShortLinkedRecordDescription;
        private System.Windows.Forms.BindingSource sp02066ATActionPicturesListBindingSource;
        private DataSet_ATTableAdapters.sp02066_AT_Action_Pictures_ListTableAdapter sp02066_AT_Action_Pictures_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserPickList1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserPickList2;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserPickList3;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSiteFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarCheckItem bciFilterData;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending39;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
    }
}

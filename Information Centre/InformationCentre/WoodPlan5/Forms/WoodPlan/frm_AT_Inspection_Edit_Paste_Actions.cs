using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_AT_Inspection_Edit_Paste_Actions : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        BaseObjects.GridCheckMarksSelection selection1;

        public string strInspectionIDs = "";
        public string strLastUsedInspectRef = "";
        public string strActionIDs = "";
        public string strSelectedIDs = "";
        public DateTime dtSelectedDate;
        public string strSelectedSequence = "";
        public int intSelectedContractor = 0;
        
        #endregion

        public frm_AT_Inspection_Edit_Paste_Actions()
        {
            InitializeComponent();
        }

        private void frm_AT_Inspection_Edit_Paste_Actions_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 20012100;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Connection.ConnectionString = strConnectionString;

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp00190_Contractor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp00190_Contractor_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00190_Contractor_List_With_Blank);

            buttonEditSequence.EditValue = strLastUsedInspectRef;
            dateEditDueDate.DateTime = DateTime.Today;

            if (!string.IsNullOrEmpty(strInspectionIDs))
            {
                GridView view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Fill(dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections, strInspectionIDs, 0, null, null, 0);
                view.EndUpdate();
                gridControl1.ForceInitialize();
                selection1.SelectAll();
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            Set_btnGetActionsStatus();
            dxErrorProvider1.ClearErrors();
            this.ValidateChildren();
        }

        public override void PostLoadView(object objParameter)
        {
            Set_btnGetActionsStatus();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            string message = (checkEdit1.Checked ? "No Actions Stored - Open Edit Inspection screen and click Copy Actions or select Actions From List on this screen" : "No Actions - click Get Actions button");
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /*               bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                               bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                               if (view.RowCount > 0)
                               {
                                   bbiDatasetSelection.Enabled = true;
                                   bsiDataset.Enabled = true;
                               }
                               else
                               {
                                   bbiDatasetSelection.Enabled = false;
                                   bsiDataset.Enabled = false;
                               }
                               bsiDataset.Enabled = true;
                               bbiDatasetManager.Enabled = true;
                               bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);

                               pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));*/
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            isRunning = false;
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        #endregion


        #region Due Date Used

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
            Set_dateEditDueDate_Visibility();
        }

        private void checkEdit4_CheckedChanged(object sender, EventArgs e)
        {
            Set_dateEditDueDate_Visibility();
        }

        private void Set_dateEditDueDate_Visibility()
        {
            dateEditDueDate.Visible = checkEdit4.Checked;
            dateEditDueDate.Enabled = checkEdit4.Checked;
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
        }

        private void dateEditDueDate_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (checkEdit4.Checked && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(dateEditDueDate, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(dateEditDueDate, "");
            }
        }

        #endregion


        #region Carried Out By Used

        private void checkEdit5_CheckedChanged(object sender, EventArgs e)
        {
            Set_gridLookUpEditCarriedOutBy_Visibility();
        }

        private void checkEdit6_CheckedChanged(object sender, EventArgs e)
        {
            Set_gridLookUpEditCarriedOutBy_Visibility();
        }

        private void Set_gridLookUpEditCarriedOutBy_Visibility()
        {
            gridLookUpEditCarriedOutBy.Visible = checkEdit6.Checked;
            gridLookUpEditCarriedOutBy.Enabled = checkEdit6.Checked;
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
        }

        private void gridLookUpEditCarriedOutBy_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (checkEdit6.Checked && (glue.EditValue == null || string.IsNullOrEmpty(glue.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(gridLookUpEditCarriedOutBy, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(gridLookUpEditCarriedOutBy, "");
            }
        }



        #endregion


        #region Set_btnGetActions

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            Set_btnGetActionsStatus();
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                GridView view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                if (!string.IsNullOrEmpty(strInspectionIDs))
                {
                    sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Fill(dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections, strInspectionIDs, 0, null, null, 0);
                    selection1.SelectAll();
                }
                else
                {
                    this.dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections.Clear();
                }
                view.EndUpdate();
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            Set_btnGetActionsStatus();
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                GridView view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                if (!string.IsNullOrEmpty(strActionIDs))
                {
                    sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Fill(dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections, strActionIDs, 2, null, null, 0);
                    selection1.SelectAll();
                }
                else
                {
                    this.dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections.Clear();
                }
                view.EndUpdate();
            }
        }

        private void Set_btnGetActionsStatus()
        {
            btnGetActions.Enabled = checkEdit2.Checked;
            btnGetActions.Visible = checkEdit2.Checked;
        }

        private void btnGetActions_Click(object sender, EventArgs e)
        {
            frm_AT_Select_Action_Multiple fChildForm = new frm_AT_Select_Action_Multiple();
            this.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intWinterMaintenanceClient = 1;
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                strActionIDs = fChildForm.strSelectedValues;
                GridView view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                if (!string.IsNullOrEmpty(strActionIDs))
                {
                    sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Fill(dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections, strActionIDs, 2, null, null, 0);
                    selection1.SelectAll();
                }
                else
                {
                    this.dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections.Clear();
                }
                view.EndUpdate();
            }
        }


        #endregion


        #region Job_Number

        private void buttonEditSequence_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            string strTableName = "tblAction";
            string strFieldName = "strJobNumber";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                this.AddOwnedForm(fChildForm);
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK) buttonEditSequence.EditValue = fChildForm.SelectedSequence;
            }
        }

        private void buttonEditSequence_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (be.EditValue == null)
            {
                dxErrorProvider1.SetError(buttonEditSequence, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(buttonEditSequence, "");
            }

        }

        #endregion


        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, LayoutControl Layoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                //layoutControl1.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = Layoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        Layoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        Layoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, layoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the screen!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Paste Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (selection1.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Tick one or more actions to paste before proceeding!", "Paste Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            GridView view = (GridView)gridControl1.MainView;

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strSelectedIDs += view.GetRowCellValue(i, "ActionID").ToString() + ',';
                }
            }
            strSelectedSequence = buttonEditSequence.EditValue.ToString();
            dtSelectedDate = (checkEdit4.Checked ? dateEditDueDate.DateTime : Convert.ToDateTime("1900-01-01"));
            intSelectedContractor = (checkEdit6.Checked ? Convert.ToInt32(gridLookUpEditCarriedOutBy.EditValue) : -1);
            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }




    }
}


namespace WoodPlan5
{
    partial class frm_AT_Analysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Analysis));
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup1 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup2 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup3 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup4 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup5 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup6 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup7 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup8 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup9 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup10 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup11 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup12 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup13 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.fieldLastInspectionYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPlantYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPlantQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPlantMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeLastInspectionYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeLastInspectionQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeLastInspectionMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeNextInspectionYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeNextInspectionQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeNextInspectionMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedInspectionYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedInspectionQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedInspectionMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeLastInspectionYear3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeLastInspectionQuarter3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeLastInspectionMonth3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeNextInspectionYear3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeNextInspectionQuarter3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedTreeNextInspectionMonth3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.InspectionYear2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.InspectionQuarter2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.InspectionMonth2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.ActionDueYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.ActionDueQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.ActionDueMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.ActionDoneYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.ActionDoneQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.ActionDoneMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedActionWorkOrderCompleteYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedActionWorkOrderCompleteQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedActionWorkOrderCompleteMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedActionWorkOrderIssueYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedActionWorkOrderIssueQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.calculatedActionWorkOrderIssueMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.sp01234ATDataAnalysisTreesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_Reports = new WoodPlan5.DataSet_AT_Reports();
            this.fieldTreeID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReference = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMappingID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldGridReference = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDistance = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHouseName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSpeciesName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccess = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldVisibility = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldContext = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldManagement = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLegalStatus = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCycle = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUnit = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldGroundType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDBH = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDBHRange = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHeight = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHeightRange = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldProtectionType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCrownDiameter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPlantDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldGroupNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRiskCategory = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteHazardClass = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPlantSize = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPlantSource = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPlantMethod = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPostcode = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMapLabel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStatus = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSize = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTarget1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTarget2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTarget3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastModifiedDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRemarks = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldUser1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldUser2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldUser3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAgeClass = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldX = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldY = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAreaHa = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldReplantCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHedgeOwner = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHedgeLength = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldGardenSize = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPropertyProximity = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteLevel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSituation = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRiskFactor = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPolygonXY = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldcolor = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOwnershipName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLinkedInspectionCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCavat = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeStemCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownNorth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownSouth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownEast = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownWest = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeRetentionCategory = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSULE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSpeciesVariety = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUserPicklist1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUserPicklist2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUserPicklist3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeObjectLength = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeObjectWidth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp01236_AT_Data_Analysis_ActionsTableAdapter = new WoodPlan5.DataSet_AT_ReportsTableAdapters.sp01236_AT_Data_Analysis_ActionsTableAdapter();
            this.sp01232_AT_Reports_Listings_Action_FilterTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01232_AT_Reports_Listings_Action_FilterTableAdapter();
            this.sp01235_AT_Data_Analysis_InspectionsTableAdapter = new WoodPlan5.DataSet_AT_ReportsTableAdapters.sp01235_AT_Data_Analysis_InspectionsTableAdapter();
            this.sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter();
            this.sp01234_AT_Data_Analysis_TreesTableAdapter = new WoodPlan5.DataSet_AT_ReportsTableAdapters.sp01234_AT_Data_Analysis_TreesTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.btnAnalyze = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.btnLoadTrees = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01220ATReportsListingsTreeFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContext = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroundType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProtectionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteHazardClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgeClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaHa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReplantCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGardenSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPropertyProximity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSituation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcolor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedInspectionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCavat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStemCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownNorth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownSouth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownEast = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownWest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetentionCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSULE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesVariety = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditClientFilter2 = new DevExpress.XtraEditors.ButtonEdit();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp01230ATReportsListingsInspectionFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAngleToVertical = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownFoliation1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownFoliation2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownFoliation3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskCategory1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGeneralCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRootHeave1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRootHeave2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRootHeave3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUser11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcolor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedOutstandingActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoFurtherActionRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1b = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2b = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3b = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnLoadInspections = new DevExpress.XtraEditors.SimpleButton();
            this.ceLastInspectionOnly = new DevExpress.XtraEditors.CheckEdit();
            this.deInspectionToDate = new DevExpress.XtraEditors.DateEdit();
            this.deInspectionFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl7 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditClientFilter3 = new DevExpress.XtraEditors.ButtonEdit();
            this.deActionToDate = new DevExpress.XtraEditors.DateEdit();
            this.deActionFromDate = new DevExpress.XtraEditors.DateEdit();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01232ATReportsListingsActionFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientNameName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGridReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDistance1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHouseName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentReferenceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActionBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateLastModified1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUser12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcolor2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostDifference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrderCompleteDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrderIssueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnLoadActions = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01220_AT_Reports_Listings_Tree_FilterTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01220_AT_Reports_Listings_Tree_FilterTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pivotGridControl2 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.sp01235ATDataAnalysisInspectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldInspectionID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReference1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeMappingID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeGridReference = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeDistance = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHouseName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSpeciesName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeAccess = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeVisibility = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeContext = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeManagement = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeLegalStatus = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeLastInspectionDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeInspectionCycle = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeInspectionUnit = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeNextInspectionDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeGroundType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeDBH = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeDBHRange = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHeight = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHeightRange = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeProtectionType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownDiameter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePlantDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeGroupNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeRiskCategory = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSiteHazardClass = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePlantSize = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePlantSource = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePlantMethod = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePostcode = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeMapLabel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeStatus = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSize = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeTarget1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeTarget2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeTarget3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeLastModifiedDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeRemarks = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUser1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUser2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUser3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeAgeClass = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeX = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeY = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeAreaHa = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReplantCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSurveyDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHedgeOwner = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHedgeLength = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeGardenSize = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePropertyProximity = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSiteLevel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSituation = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeRiskFactor = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePolygonXY = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldcolor1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeOwnershipName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionReference = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionInspector = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionIncidentReference = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionIncidentDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIncidentID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionAngleToVertical = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionLastModified = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserDefined1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserDefined2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserDefined3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionRemarks = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemPhysical1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemPhysical2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemPhysical3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemDisease1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemDisease2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemDisease3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownPhysical1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownPhysical2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownPhysical3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownDisease1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownDisease2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownDisease3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownFoliation1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownFoliation2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownFoliation3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionRiskCategory = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionGeneralCondition = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionBasePhysical1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionBasePhysical2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionBasePhysical3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionVitality = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLinkedActionCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLinkedOutstandingActionCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNoFurtherActionRequired = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCavat1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeStemCount1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownNorth1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownSouth1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownEast1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownWest1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeRetentionCategory1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSULE1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSpeciesVariety1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUserPicklist11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUserPicklist21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUserPicklist31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeObjectLength1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeObjectWidth1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserPicklist1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserPicklist2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserPicklist3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.chartControl2 = new DevExpress.XtraCharts.ChartControl();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pivotGridControl3 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.sp01236ATDataAnalysisActionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldInspectionID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeID2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteID2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientID2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteName2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientName3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReference2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeMappingID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeGridReference1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeDistance1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHouseName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSpeciesName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeAccess1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeVisibility1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeContext1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeManagement1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeLegalStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeLastInspectionDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeInspectionCycle1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeInspectionUnit1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeNextInspectionDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeGroundType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeDBH1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeDBHRange1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHeight1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHeightRange1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeProtectionType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownDiameter1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePlantDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeGroupNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeRiskCategory1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSiteHazardClass1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePlantSize1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePlantSource1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePlantMethod1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePostcode1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeMapLabel1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSize1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeTarget11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeTarget21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeTarget31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeLastModifiedDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeRemarks1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUser11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUser21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUser31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeAgeClass1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeX1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeY1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeAreaHa1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReplantCount1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSurveyDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeType2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHedgeOwner1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeHedgeLength1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeGardenSize1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePropertyProximity1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSiteLevel1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSituation1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeRiskFactor1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreePolygonXY1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldcolor2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeOwnershipName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionReference1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionInspector1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionIncidentReference1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionIncidentDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIncidentID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionAngleToVertical1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionLastModified1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserDefined11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserDefined21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserDefined31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionRemarks1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemPhysical11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemPhysical21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemPhysical31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemDisease11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemDisease21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionStemDisease31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownPhysical11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownPhysical21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownPhysical31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownDisease11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownDisease21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownDisease31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownFoliation11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownFoliation21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCrownFoliation31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionRiskCategory1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionGeneralCondition1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionBasePhysical11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionBasePhysical21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionBasePhysical31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionVitality1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionJobNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAction = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionPriority = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionDueDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionDoneDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionBy = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionSupervisor = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionOwnership = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCostCentre = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionBudget = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionWorkUnits = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionScheduleOfRates = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionBudgetedRateDescription = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionBudgetedRate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionBudgetedCost = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionActualRateDescription = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionActualRate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionActualCost = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionDateLastModified = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionWorkOrder = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionRemarks = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionUserDefined1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionUserDefined2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionUserDefined3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCostDifference = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCompletionStatus = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionTimeliness = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionWorkOrderCompleteDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionWorkOrderIssueDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCavat2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeStemCount2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownNorth2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownSouth2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownEast2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeCrownWest2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeRetentionCategory2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSULE2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeSpeciesVariety2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeObjectLength2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeObjectWidth2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUserPicklist12 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUserPicklist22 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeUserPicklist32 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserPicklist11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserPicklist21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUserPicklist31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionUserPicklist1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionUserPicklist2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionUserPicklist3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.chartControl3 = new DevExpress.XtraCharts.ChartControl();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pmChart = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiRotateAxis = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChartWizard = new DevExpress.XtraBars.BarButtonItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01234ATDataAnalysisTreesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01220ATReportsListingsTreeFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01230ATReportsListingsInspectionFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceLastInspectionOnly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl7)).BeginInit();
            this.layoutControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01232ATReportsListingsActionFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage4.SuspendLayout();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01235ATDataAnalysisInspectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01236ATDataAnalysisActionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 850);
            this.barDockControlBottom.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 850);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1293, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 850);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.bbiChartWizard,
            this.bbiRotateAxis});
            this.barManager1.MaxItemId = 28;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // fieldLastInspectionYear
            // 
            this.fieldLastInspectionYear.AreaIndex = 35;
            this.fieldLastInspectionYear.Caption = "Last Inspection Year";
            this.fieldLastInspectionYear.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionYear.FieldName = "LastInspectionDate";
            this.fieldLastInspectionYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldLastInspectionYear.Name = "fieldLastInspectionYear";
            this.fieldLastInspectionYear.Options.AllowRunTimeSummaryChange = true;
            this.fieldLastInspectionYear.UnboundFieldName = "fieldLastInspectionYear";
            // 
            // fieldLastInspectionQuarter
            // 
            this.fieldLastInspectionQuarter.AreaIndex = 33;
            this.fieldLastInspectionQuarter.Caption = "Last Inspection Quarter";
            this.fieldLastInspectionQuarter.FieldName = "LastInspectionDate";
            this.fieldLastInspectionQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldLastInspectionQuarter.Name = "fieldLastInspectionQuarter";
            this.fieldLastInspectionQuarter.Options.AllowRunTimeSummaryChange = true;
            this.fieldLastInspectionQuarter.UnboundFieldName = "fieldLastInspectionQuarter";
            this.fieldLastInspectionQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldLastInspectionQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLastInspectionQuarter.Visible = false;
            // 
            // fieldLastInspectionMonth
            // 
            this.fieldLastInspectionMonth.AreaIndex = 34;
            this.fieldLastInspectionMonth.Caption = "Last Inspection Month";
            this.fieldLastInspectionMonth.FieldName = "LastInspectionDate";
            this.fieldLastInspectionMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldLastInspectionMonth.Name = "fieldLastInspectionMonth";
            this.fieldLastInspectionMonth.Options.AllowRunTimeSummaryChange = true;
            this.fieldLastInspectionMonth.UnboundFieldName = "fieldLastInspectionMonth";
            this.fieldLastInspectionMonth.Visible = false;
            // 
            // fieldNextInspectionYear
            // 
            this.fieldNextInspectionYear.AreaIndex = 36;
            this.fieldNextInspectionYear.Caption = "Next Inspection Year";
            this.fieldNextInspectionYear.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionYear.FieldName = "NextInspectionDate";
            this.fieldNextInspectionYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldNextInspectionYear.Name = "fieldNextInspectionYear";
            this.fieldNextInspectionYear.Options.AllowRunTimeSummaryChange = true;
            this.fieldNextInspectionYear.UnboundFieldName = "fieldNextInspectionYear";
            // 
            // fieldNextInspectionQuarter
            // 
            this.fieldNextInspectionQuarter.AreaIndex = 36;
            this.fieldNextInspectionQuarter.Caption = "Next Inspection Quarter";
            this.fieldNextInspectionQuarter.FieldName = "NextInspectionDate";
            this.fieldNextInspectionQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldNextInspectionQuarter.Name = "fieldNextInspectionQuarter";
            this.fieldNextInspectionQuarter.Options.AllowRunTimeSummaryChange = true;
            this.fieldNextInspectionQuarter.UnboundFieldName = "fieldNextInspectionQuarter";
            this.fieldNextInspectionQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldNextInspectionQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNextInspectionQuarter.Visible = false;
            // 
            // fieldNextInspectionMonth
            // 
            this.fieldNextInspectionMonth.AreaIndex = 37;
            this.fieldNextInspectionMonth.Caption = "Next Inspection Month";
            this.fieldNextInspectionMonth.FieldName = "NextInspectionDate";
            this.fieldNextInspectionMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldNextInspectionMonth.Name = "fieldNextInspectionMonth";
            this.fieldNextInspectionMonth.Options.AllowRunTimeSummaryChange = true;
            this.fieldNextInspectionMonth.UnboundFieldName = "fieldNextInspectionMonth";
            this.fieldNextInspectionMonth.Visible = false;
            // 
            // fieldPlantYear
            // 
            this.fieldPlantYear.AreaIndex = 37;
            this.fieldPlantYear.Caption = "Plant Year";
            this.fieldPlantYear.ExpandedInFieldsGroup = false;
            this.fieldPlantYear.FieldName = "PlantDate";
            this.fieldPlantYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldPlantYear.Name = "fieldPlantYear";
            this.fieldPlantYear.Options.AllowRunTimeSummaryChange = true;
            this.fieldPlantYear.UnboundFieldName = "fieldPlantYear";
            // 
            // fieldPlantQuarter
            // 
            this.fieldPlantQuarter.AreaIndex = 1;
            this.fieldPlantQuarter.Caption = "Plant Quarter";
            this.fieldPlantQuarter.FieldName = "PlantDate";
            this.fieldPlantQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldPlantQuarter.Name = "fieldPlantQuarter";
            this.fieldPlantQuarter.Options.AllowRunTimeSummaryChange = true;
            this.fieldPlantQuarter.UnboundFieldName = "fieldPlantQuarter";
            this.fieldPlantQuarter.Visible = false;
            // 
            // fieldPlantMonth
            // 
            this.fieldPlantMonth.AreaIndex = 2;
            this.fieldPlantMonth.Caption = "Plant Month";
            this.fieldPlantMonth.FieldName = "PlantDate";
            this.fieldPlantMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldPlantMonth.Name = "fieldPlantMonth";
            this.fieldPlantMonth.Options.AllowRunTimeSummaryChange = true;
            this.fieldPlantMonth.UnboundFieldName = "fieldPlantMonth";
            this.fieldPlantMonth.Visible = false;
            // 
            // calculatedTreeLastInspectionYear
            // 
            this.calculatedTreeLastInspectionYear.AreaIndex = 17;
            this.calculatedTreeLastInspectionYear.Caption = "Tree Last Inspection Year";
            this.calculatedTreeLastInspectionYear.ExpandedInFieldsGroup = false;
            this.calculatedTreeLastInspectionYear.FieldName = "TreeLastInspectionDate";
            this.calculatedTreeLastInspectionYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.calculatedTreeLastInspectionYear.Name = "calculatedTreeLastInspectionYear";
            this.calculatedTreeLastInspectionYear.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeLastInspectionYear.UnboundFieldName = "calculatedTreeLastInspectionYear";
            // 
            // calculatedTreeLastInspectionQuarter
            // 
            this.calculatedTreeLastInspectionQuarter.AreaIndex = 42;
            this.calculatedTreeLastInspectionQuarter.Caption = "Tree Last Inspection Quarter";
            this.calculatedTreeLastInspectionQuarter.FieldName = "TreeLastInspectionDate";
            this.calculatedTreeLastInspectionQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.calculatedTreeLastInspectionQuarter.Name = "calculatedTreeLastInspectionQuarter";
            this.calculatedTreeLastInspectionQuarter.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeLastInspectionQuarter.UnboundFieldName = "calculatedTreeLastInspectionQuarter";
            this.calculatedTreeLastInspectionQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.calculatedTreeLastInspectionQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calculatedTreeLastInspectionQuarter.Visible = false;
            // 
            // calculatedTreeLastInspectionMonth
            // 
            this.calculatedTreeLastInspectionMonth.AreaIndex = 43;
            this.calculatedTreeLastInspectionMonth.Caption = "Tree Last Inspection Month";
            this.calculatedTreeLastInspectionMonth.FieldName = "TreeLastInspectionDate";
            this.calculatedTreeLastInspectionMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.calculatedTreeLastInspectionMonth.Name = "calculatedTreeLastInspectionMonth";
            this.calculatedTreeLastInspectionMonth.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeLastInspectionMonth.UnboundFieldName = "calculatedTreeLastInspectionMonth";
            this.calculatedTreeLastInspectionMonth.Visible = false;
            // 
            // calculatedTreeNextInspectionYear
            // 
            this.calculatedTreeNextInspectionYear.AreaIndex = 18;
            this.calculatedTreeNextInspectionYear.Caption = "Tree Next Inspection Year";
            this.calculatedTreeNextInspectionYear.ExpandedInFieldsGroup = false;
            this.calculatedTreeNextInspectionYear.FieldName = "TreeNextInspectionDate";
            this.calculatedTreeNextInspectionYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.calculatedTreeNextInspectionYear.Name = "calculatedTreeNextInspectionYear";
            this.calculatedTreeNextInspectionYear.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeNextInspectionYear.UnboundFieldName = "calculatedTreeNextInspectionYear";
            // 
            // calculatedTreeNextInspectionQuarter
            // 
            this.calculatedTreeNextInspectionQuarter.AreaIndex = 45;
            this.calculatedTreeNextInspectionQuarter.Caption = "Tree Next Inspection Quarter";
            this.calculatedTreeNextInspectionQuarter.FieldName = "TreeNextInspectionDate";
            this.calculatedTreeNextInspectionQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.calculatedTreeNextInspectionQuarter.Name = "calculatedTreeNextInspectionQuarter";
            this.calculatedTreeNextInspectionQuarter.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeNextInspectionQuarter.UnboundFieldName = "calculatedTreeNextInspectionQuarter";
            this.calculatedTreeNextInspectionQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.calculatedTreeNextInspectionQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calculatedTreeNextInspectionQuarter.Visible = false;
            // 
            // calculatedTreeNextInspectionMonth
            // 
            this.calculatedTreeNextInspectionMonth.AreaIndex = 46;
            this.calculatedTreeNextInspectionMonth.Caption = "Tree Next Inspection Month";
            this.calculatedTreeNextInspectionMonth.FieldName = "TreeNextInspectionDate";
            this.calculatedTreeNextInspectionMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.calculatedTreeNextInspectionMonth.Name = "calculatedTreeNextInspectionMonth";
            this.calculatedTreeNextInspectionMonth.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeNextInspectionMonth.UnboundFieldName = "calculatedTreeNextInspectionMonth";
            this.calculatedTreeNextInspectionMonth.Visible = false;
            // 
            // calculatedInspectionYear
            // 
            this.calculatedInspectionYear.AreaIndex = 22;
            this.calculatedInspectionYear.Caption = "Inspection Year";
            this.calculatedInspectionYear.ExpandedInFieldsGroup = false;
            this.calculatedInspectionYear.FieldName = "InspectionDate";
            this.calculatedInspectionYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.calculatedInspectionYear.Name = "calculatedInspectionYear";
            this.calculatedInspectionYear.Options.AllowRunTimeSummaryChange = true;
            this.calculatedInspectionYear.UnboundFieldName = "calculatedInspectionYear";
            // 
            // calculatedInspectionQuarter
            // 
            this.calculatedInspectionQuarter.AreaIndex = 48;
            this.calculatedInspectionQuarter.Caption = "Inspection Quarter";
            this.calculatedInspectionQuarter.FieldName = "InspectionDate";
            this.calculatedInspectionQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.calculatedInspectionQuarter.Name = "calculatedInspectionQuarter";
            this.calculatedInspectionQuarter.Options.AllowRunTimeSummaryChange = true;
            this.calculatedInspectionQuarter.UnboundFieldName = "calculatedInspectionQuarter";
            this.calculatedInspectionQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.calculatedInspectionQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calculatedInspectionQuarter.Visible = false;
            // 
            // calculatedInspectionMonth
            // 
            this.calculatedInspectionMonth.AreaIndex = 49;
            this.calculatedInspectionMonth.Caption = "Inspection Month";
            this.calculatedInspectionMonth.FieldName = "InspectionDate";
            this.calculatedInspectionMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.calculatedInspectionMonth.Name = "calculatedInspectionMonth";
            this.calculatedInspectionMonth.Options.AllowRunTimeSummaryChange = true;
            this.calculatedInspectionMonth.UnboundFieldName = "calculatedInspectionMonth";
            this.calculatedInspectionMonth.Visible = false;
            // 
            // calculatedTreeLastInspectionYear3
            // 
            this.calculatedTreeLastInspectionYear3.AreaIndex = 81;
            this.calculatedTreeLastInspectionYear3.Caption = "Tree Last Inspection Year";
            this.calculatedTreeLastInspectionYear3.ExpandedInFieldsGroup = false;
            this.calculatedTreeLastInspectionYear3.FieldName = "TreeLastInspectionDate";
            this.calculatedTreeLastInspectionYear3.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.calculatedTreeLastInspectionYear3.Name = "calculatedTreeLastInspectionYear3";
            this.calculatedTreeLastInspectionYear3.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeLastInspectionYear3.UnboundFieldName = "calculatedTreeLastInspectionYear3";
            // 
            // calculatedTreeLastInspectionQuarter3
            // 
            this.calculatedTreeLastInspectionQuarter3.AreaIndex = 84;
            this.calculatedTreeLastInspectionQuarter3.Caption = "Tree Last Inspection Quarter";
            this.calculatedTreeLastInspectionQuarter3.FieldName = "TreeLastInspectionDate";
            this.calculatedTreeLastInspectionQuarter3.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.calculatedTreeLastInspectionQuarter3.Name = "calculatedTreeLastInspectionQuarter3";
            this.calculatedTreeLastInspectionQuarter3.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeLastInspectionQuarter3.UnboundFieldName = "calculatedTreeLastInspectionQuarter3";
            this.calculatedTreeLastInspectionQuarter3.ValueFormat.FormatString = "Qtr {0}";
            this.calculatedTreeLastInspectionQuarter3.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calculatedTreeLastInspectionQuarter3.Visible = false;
            // 
            // calculatedTreeLastInspectionMonth3
            // 
            this.calculatedTreeLastInspectionMonth3.AreaIndex = 85;
            this.calculatedTreeLastInspectionMonth3.Caption = "Tree Last Inspection Month";
            this.calculatedTreeLastInspectionMonth3.FieldName = "TreeLastInspectionDate";
            this.calculatedTreeLastInspectionMonth3.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.calculatedTreeLastInspectionMonth3.Name = "calculatedTreeLastInspectionMonth3";
            this.calculatedTreeLastInspectionMonth3.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeLastInspectionMonth3.UnboundFieldName = "calculatedTreeLastInspectionMonth3";
            this.calculatedTreeLastInspectionMonth3.Visible = false;
            // 
            // calculatedTreeNextInspectionYear3
            // 
            this.calculatedTreeNextInspectionYear3.AreaIndex = 82;
            this.calculatedTreeNextInspectionYear3.Caption = "Tree Next Inspection Year";
            this.calculatedTreeNextInspectionYear3.ExpandedInFieldsGroup = false;
            this.calculatedTreeNextInspectionYear3.FieldName = "TreeNextInspectionDate";
            this.calculatedTreeNextInspectionYear3.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.calculatedTreeNextInspectionYear3.Name = "calculatedTreeNextInspectionYear3";
            this.calculatedTreeNextInspectionYear3.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeNextInspectionYear3.UnboundFieldName = "calculatedTreeNextInspectionYear3";
            // 
            // calculatedTreeNextInspectionQuarter3
            // 
            this.calculatedTreeNextInspectionQuarter3.AreaIndex = 4;
            this.calculatedTreeNextInspectionQuarter3.Caption = "Tree Next Inspection Quarter";
            this.calculatedTreeNextInspectionQuarter3.FieldName = "TreeNextInspectionDate";
            this.calculatedTreeNextInspectionQuarter3.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.calculatedTreeNextInspectionQuarter3.Name = "calculatedTreeNextInspectionQuarter3";
            this.calculatedTreeNextInspectionQuarter3.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeNextInspectionQuarter3.UnboundFieldName = "calculatedTreeNextInspectionQuarter3";
            this.calculatedTreeNextInspectionQuarter3.ValueFormat.FormatString = "Qtr {0}";
            this.calculatedTreeNextInspectionQuarter3.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calculatedTreeNextInspectionQuarter3.Visible = false;
            // 
            // calculatedTreeNextInspectionMonth3
            // 
            this.calculatedTreeNextInspectionMonth3.AreaIndex = 5;
            this.calculatedTreeNextInspectionMonth3.Caption = "Tree Next Inspection Month";
            this.calculatedTreeNextInspectionMonth3.FieldName = "TreeNextInspectionDate";
            this.calculatedTreeNextInspectionMonth3.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.calculatedTreeNextInspectionMonth3.Name = "calculatedTreeNextInspectionMonth3";
            this.calculatedTreeNextInspectionMonth3.Options.AllowRunTimeSummaryChange = true;
            this.calculatedTreeNextInspectionMonth3.UnboundFieldName = "calculatedTreeNextInspectionMonth3";
            this.calculatedTreeNextInspectionMonth3.Visible = false;
            // 
            // InspectionYear2
            // 
            this.InspectionYear2.AreaIndex = 83;
            this.InspectionYear2.Caption = "Inspection Year";
            this.InspectionYear2.ExpandedInFieldsGroup = false;
            this.InspectionYear2.FieldName = "InspectionDate";
            this.InspectionYear2.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.InspectionYear2.Name = "InspectionYear2";
            this.InspectionYear2.Options.AllowRunTimeSummaryChange = true;
            this.InspectionYear2.UnboundFieldName = "InspectionYear2";
            // 
            // InspectionQuarter2
            // 
            this.InspectionQuarter2.AreaIndex = 7;
            this.InspectionQuarter2.Caption = "Inspection Quarter";
            this.InspectionQuarter2.FieldName = "InspectionDate";
            this.InspectionQuarter2.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.InspectionQuarter2.Name = "InspectionQuarter2";
            this.InspectionQuarter2.Options.AllowRunTimeSummaryChange = true;
            this.InspectionQuarter2.UnboundFieldName = "InspectionQuarter2";
            this.InspectionQuarter2.ValueFormat.FormatString = "Qtr {0}";
            this.InspectionQuarter2.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.InspectionQuarter2.Visible = false;
            // 
            // InspectionMonth2
            // 
            this.InspectionMonth2.AreaIndex = 8;
            this.InspectionMonth2.Caption = "Inspection Month";
            this.InspectionMonth2.FieldName = "InspectionDate";
            this.InspectionMonth2.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.InspectionMonth2.Name = "InspectionMonth2";
            this.InspectionMonth2.Options.AllowRunTimeSummaryChange = true;
            this.InspectionMonth2.UnboundFieldName = "InspectionMonth2";
            this.InspectionMonth2.Visible = false;
            // 
            // ActionDueYear
            // 
            this.ActionDueYear.AreaIndex = 84;
            this.ActionDueYear.Caption = "Action Due Year";
            this.ActionDueYear.ExpandedInFieldsGroup = false;
            this.ActionDueYear.FieldName = "ActionDueDate";
            this.ActionDueYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.ActionDueYear.Name = "ActionDueYear";
            this.ActionDueYear.Options.AllowRunTimeSummaryChange = true;
            this.ActionDueYear.UnboundFieldName = "ActionDueYear";
            // 
            // ActionDueQuarter
            // 
            this.ActionDueQuarter.AreaIndex = 10;
            this.ActionDueQuarter.Caption = "Action Due Quarter";
            this.ActionDueQuarter.FieldName = "ActionDueDate";
            this.ActionDueQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.ActionDueQuarter.Name = "ActionDueQuarter";
            this.ActionDueQuarter.Options.AllowRunTimeSummaryChange = true;
            this.ActionDueQuarter.UnboundFieldName = "ActionDueQuarter";
            this.ActionDueQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.ActionDueQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ActionDueQuarter.Visible = false;
            // 
            // ActionDueMonth
            // 
            this.ActionDueMonth.AreaIndex = 11;
            this.ActionDueMonth.Caption = "Action Due Month";
            this.ActionDueMonth.FieldName = "ActionDueDate";
            this.ActionDueMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.ActionDueMonth.Name = "ActionDueMonth";
            this.ActionDueMonth.Options.AllowRunTimeSummaryChange = true;
            this.ActionDueMonth.UnboundFieldName = "ActionDueMonth";
            this.ActionDueMonth.Visible = false;
            // 
            // ActionDoneYear
            // 
            this.ActionDoneYear.AreaIndex = 85;
            this.ActionDoneYear.Caption = "Action Done Year";
            this.ActionDoneYear.ExpandedInFieldsGroup = false;
            this.ActionDoneYear.FieldName = "ActionDoneDate";
            this.ActionDoneYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.ActionDoneYear.Name = "ActionDoneYear";
            this.ActionDoneYear.Options.AllowRunTimeSummaryChange = true;
            this.ActionDoneYear.UnboundFieldName = "ActionDoneYear";
            // 
            // ActionDoneQuarter
            // 
            this.ActionDoneQuarter.AreaIndex = 13;
            this.ActionDoneQuarter.Caption = "Action Done Quarter";
            this.ActionDoneQuarter.FieldName = "ActionDoneDate";
            this.ActionDoneQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.ActionDoneQuarter.Name = "ActionDoneQuarter";
            this.ActionDoneQuarter.Options.AllowRunTimeSummaryChange = true;
            this.ActionDoneQuarter.UnboundFieldName = "ActionDoneQuarter";
            this.ActionDoneQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.ActionDoneQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ActionDoneQuarter.Visible = false;
            // 
            // ActionDoneMonth
            // 
            this.ActionDoneMonth.AreaIndex = 14;
            this.ActionDoneMonth.Caption = "Action Done Month";
            this.ActionDoneMonth.FieldName = "ActionDoneDate";
            this.ActionDoneMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.ActionDoneMonth.Name = "ActionDoneMonth";
            this.ActionDoneMonth.Options.AllowRunTimeSummaryChange = true;
            this.ActionDoneMonth.UnboundFieldName = "ActionDoneMonth";
            this.ActionDoneMonth.Visible = false;
            // 
            // calculatedActionWorkOrderCompleteYear
            // 
            this.calculatedActionWorkOrderCompleteYear.AreaIndex = 87;
            this.calculatedActionWorkOrderCompleteYear.Caption = "Action Work Order Complete Year";
            this.calculatedActionWorkOrderCompleteYear.ExpandedInFieldsGroup = false;
            this.calculatedActionWorkOrderCompleteYear.FieldName = "ActionWorkOrderCompleteDate";
            this.calculatedActionWorkOrderCompleteYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.calculatedActionWorkOrderCompleteYear.Name = "calculatedActionWorkOrderCompleteYear";
            this.calculatedActionWorkOrderCompleteYear.UnboundFieldName = "calculatedActionWorkOrderCompleteYear";
            // 
            // calculatedActionWorkOrderCompleteQuarter
            // 
            this.calculatedActionWorkOrderCompleteQuarter.AreaIndex = 90;
            this.calculatedActionWorkOrderCompleteQuarter.Caption = "Action Work Order Complete Quarter";
            this.calculatedActionWorkOrderCompleteQuarter.FieldName = "ActionWorkOrderCompleteDate";
            this.calculatedActionWorkOrderCompleteQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.calculatedActionWorkOrderCompleteQuarter.Name = "calculatedActionWorkOrderCompleteQuarter";
            this.calculatedActionWorkOrderCompleteQuarter.UnboundFieldName = "calculatedActionWorkOrderCompleteQuarter";
            this.calculatedActionWorkOrderCompleteQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.calculatedActionWorkOrderCompleteQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calculatedActionWorkOrderCompleteQuarter.Visible = false;
            // 
            // calculatedActionWorkOrderCompleteMonth
            // 
            this.calculatedActionWorkOrderCompleteMonth.AreaIndex = 91;
            this.calculatedActionWorkOrderCompleteMonth.Caption = "Action Work Order Complete Month";
            this.calculatedActionWorkOrderCompleteMonth.FieldName = "ActionWorkOrderCompleteDate";
            this.calculatedActionWorkOrderCompleteMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.calculatedActionWorkOrderCompleteMonth.Name = "calculatedActionWorkOrderCompleteMonth";
            this.calculatedActionWorkOrderCompleteMonth.UnboundFieldName = "calculatedActionWorkOrderCompleteMonth";
            this.calculatedActionWorkOrderCompleteMonth.Visible = false;
            // 
            // calculatedActionWorkOrderIssueYear
            // 
            this.calculatedActionWorkOrderIssueYear.AreaIndex = 86;
            this.calculatedActionWorkOrderIssueYear.Caption = "Action Work Order Issue Year";
            this.calculatedActionWorkOrderIssueYear.ExpandedInFieldsGroup = false;
            this.calculatedActionWorkOrderIssueYear.FieldName = "ActionWorkOrderIssueDate";
            this.calculatedActionWorkOrderIssueYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.calculatedActionWorkOrderIssueYear.Name = "calculatedActionWorkOrderIssueYear";
            this.calculatedActionWorkOrderIssueYear.UnboundFieldName = "calculatedActionWorkOrderIssueYear";
            // 
            // calculatedActionWorkOrderIssueQuarter
            // 
            this.calculatedActionWorkOrderIssueQuarter.AreaIndex = 2;
            this.calculatedActionWorkOrderIssueQuarter.Caption = "Action Work Order Issue Quarter";
            this.calculatedActionWorkOrderIssueQuarter.FieldName = "ActionWorkOrderIssueDate";
            this.calculatedActionWorkOrderIssueQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.calculatedActionWorkOrderIssueQuarter.Name = "calculatedActionWorkOrderIssueQuarter";
            this.calculatedActionWorkOrderIssueQuarter.UnboundFieldName = "calculatedActionWorkOrderIssueQuarter";
            this.calculatedActionWorkOrderIssueQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.calculatedActionWorkOrderIssueQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calculatedActionWorkOrderIssueQuarter.Visible = false;
            // 
            // calculatedActionWorkOrderIssueMonth
            // 
            this.calculatedActionWorkOrderIssueMonth.AreaIndex = 2;
            this.calculatedActionWorkOrderIssueMonth.Caption = " Action Work Order Issue Month ";
            this.calculatedActionWorkOrderIssueMonth.FieldName = "ActionWorkOrderIssueDate";
            this.calculatedActionWorkOrderIssueMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.calculatedActionWorkOrderIssueMonth.Name = "calculatedActionWorkOrderIssueMonth";
            this.calculatedActionWorkOrderIssueMonth.UnboundFieldName = "calculatedActionWorkOrderIssueMonth";
            this.calculatedActionWorkOrderIssueMonth.Visible = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.pivotGridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.chartControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(949, 824);
            this.splitContainerControl1.SplitterPosition = 439;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.ActiveFilterString = "";
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.DataSource = this.sp01234ATDataAnalysisTreesBindingSource;
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldTreeID,
            this.fieldSiteID,
            this.fieldClientID,
            this.fieldSiteName,
            this.fieldClientName,
            this.fieldTreeReference,
            this.fieldMappingID,
            this.fieldGridReference,
            this.fieldDistance,
            this.fieldHouseName,
            this.fieldSpeciesName,
            this.fieldAccess,
            this.fieldVisibility,
            this.fieldContext,
            this.fieldManagement,
            this.fieldLegalStatus,
            this.fieldLastInspectionDate,
            this.fieldInspectionCycle,
            this.fieldInspectionUnit,
            this.fieldNextInspectionDate,
            this.fieldGroundType,
            this.fieldDBH,
            this.fieldDBHRange,
            this.fieldHeight,
            this.fieldHeightRange,
            this.fieldProtectionType,
            this.fieldCrownDiameter,
            this.fieldPlantDate,
            this.fieldGroupNumber,
            this.fieldRiskCategory,
            this.fieldSiteHazardClass,
            this.fieldPlantSize,
            this.fieldPlantSource,
            this.fieldPlantMethod,
            this.fieldPostcode,
            this.fieldMapLabel,
            this.fieldStatus,
            this.fieldSize,
            this.fieldTarget1,
            this.fieldTarget2,
            this.fieldTarget3,
            this.fieldLastModifiedDate,
            this.fieldRemarks,
            this.fieldUser1,
            this.fieldUser2,
            this.fieldUser3,
            this.fieldAgeClass,
            this.fieldX,
            this.fieldY,
            this.fieldAreaHa,
            this.fieldReplantCount,
            this.fieldSurveyDate,
            this.fieldTreeType,
            this.fieldHedgeOwner,
            this.fieldHedgeLength,
            this.fieldGardenSize,
            this.fieldPropertyProximity,
            this.fieldSiteLevel,
            this.fieldSituation,
            this.fieldRiskFactor,
            this.fieldPolygonXY,
            this.fieldcolor,
            this.fieldOwnershipName,
            this.fieldTreeCount,
            this.fieldLastInspectionYear,
            this.fieldLastInspectionQuarter,
            this.fieldLastInspectionMonth,
            this.fieldNextInspectionYear,
            this.fieldNextInspectionQuarter,
            this.fieldNextInspectionMonth,
            this.fieldLinkedInspectionCount,
            this.fieldPlantYear,
            this.fieldPlantQuarter,
            this.fieldPlantMonth,
            this.fieldTreeCavat,
            this.fieldTreeStemCount,
            this.fieldTreeCrownNorth,
            this.fieldTreeCrownSouth,
            this.fieldTreeCrownEast,
            this.fieldTreeCrownWest,
            this.fieldTreeRetentionCategory,
            this.fieldTreeSULE,
            this.fieldTreeSpeciesVariety,
            this.fieldTreeUserPicklist1,
            this.fieldTreeUserPicklist2,
            this.fieldTreeUserPicklist3,
            this.fieldTreeObjectLength,
            this.fieldTreeObjectWidth});
            pivotGridGroup1.Caption = "Last Inspection Year - Quarter - Month";
            pivotGridGroup1.Fields.Add(this.fieldLastInspectionYear);
            pivotGridGroup1.Fields.Add(this.fieldLastInspectionQuarter);
            pivotGridGroup1.Fields.Add(this.fieldLastInspectionMonth);
            pivotGridGroup1.Hierarchy = null;
            pivotGridGroup1.ShowNewValues = true;
            pivotGridGroup2.Caption = "Next Inspection Year - Quarter - Month";
            pivotGridGroup2.Fields.Add(this.fieldNextInspectionYear);
            pivotGridGroup2.Fields.Add(this.fieldNextInspectionQuarter);
            pivotGridGroup2.Fields.Add(this.fieldNextInspectionMonth);
            pivotGridGroup2.Hierarchy = null;
            pivotGridGroup2.ShowNewValues = true;
            pivotGridGroup3.Caption = "Plant Year - Quarter - Month";
            pivotGridGroup3.Fields.Add(this.fieldPlantYear);
            pivotGridGroup3.Fields.Add(this.fieldPlantQuarter);
            pivotGridGroup3.Fields.Add(this.fieldPlantMonth);
            pivotGridGroup3.Hierarchy = null;
            pivotGridGroup3.ShowNewValues = true;
            this.pivotGridControl1.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup1,
            pivotGridGroup2,
            pivotGridGroup3});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl1.MenuManager = this.barManager1;
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pivotGridControl1.Size = new System.Drawing.Size(949, 439);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.PopupMenuShowing += new DevExpress.XtraPivotGrid.PopupMenuShowingEventHandler(this.pivotGridControl1_PopupMenuShowing);
            this.pivotGridControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pivotGridControl1_MouseUp);
            // 
            // sp01234ATDataAnalysisTreesBindingSource
            // 
            this.sp01234ATDataAnalysisTreesBindingSource.DataMember = "sp01234_AT_Data_Analysis_Trees";
            this.sp01234ATDataAnalysisTreesBindingSource.DataSource = this.dataSet_AT_Reports;
            // 
            // dataSet_AT_Reports
            // 
            this.dataSet_AT_Reports.DataSetName = "DataSet_AT_Reports";
            this.dataSet_AT_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fieldTreeID
            // 
            this.fieldTreeID.AreaIndex = 0;
            this.fieldTreeID.Caption = "Tree ID";
            this.fieldTreeID.FieldName = "TreeID";
            this.fieldTreeID.Name = "fieldTreeID";
            this.fieldTreeID.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeID.Visible = false;
            // 
            // fieldSiteID
            // 
            this.fieldSiteID.AreaIndex = 0;
            this.fieldSiteID.Caption = "Site ID";
            this.fieldSiteID.FieldName = "SiteID";
            this.fieldSiteID.Name = "fieldSiteID";
            this.fieldSiteID.Options.AllowRunTimeSummaryChange = true;
            this.fieldSiteID.Visible = false;
            // 
            // fieldClientID
            // 
            this.fieldClientID.AreaIndex = 1;
            this.fieldClientID.Caption = "Client ID";
            this.fieldClientID.FieldName = "ClientID";
            this.fieldClientID.Name = "fieldClientID";
            this.fieldClientID.Options.AllowRunTimeSummaryChange = true;
            this.fieldClientID.Visible = false;
            // 
            // fieldSiteName
            // 
            this.fieldSiteName.AreaIndex = 1;
            this.fieldSiteName.Caption = "Site Name";
            this.fieldSiteName.FieldName = "SiteName";
            this.fieldSiteName.Name = "fieldSiteName";
            this.fieldSiteName.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldClientName
            // 
            this.fieldClientName.AreaIndex = 0;
            this.fieldClientName.Caption = "Client Name";
            this.fieldClientName.FieldName = "ClientName";
            this.fieldClientName.Name = "fieldClientName";
            this.fieldClientName.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeReference
            // 
            this.fieldTreeReference.AreaIndex = 2;
            this.fieldTreeReference.Caption = "Tree Reference";
            this.fieldTreeReference.FieldName = "TreeReference";
            this.fieldTreeReference.Name = "fieldTreeReference";
            this.fieldTreeReference.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldMappingID
            // 
            this.fieldMappingID.AreaIndex = 3;
            this.fieldMappingID.Caption = "Mapping ID";
            this.fieldMappingID.FieldName = "MappingID";
            this.fieldMappingID.Name = "fieldMappingID";
            this.fieldMappingID.Options.AllowRunTimeSummaryChange = true;
            this.fieldMappingID.Visible = false;
            // 
            // fieldGridReference
            // 
            this.fieldGridReference.AreaIndex = 3;
            this.fieldGridReference.Caption = "Grid Reference";
            this.fieldGridReference.FieldName = "GridReference";
            this.fieldGridReference.Name = "fieldGridReference";
            this.fieldGridReference.Options.AllowRunTimeSummaryChange = true;
            this.fieldGridReference.Visible = false;
            // 
            // fieldDistance
            // 
            this.fieldDistance.AreaIndex = 3;
            this.fieldDistance.Caption = "Distance";
            this.fieldDistance.FieldName = "Distance";
            this.fieldDistance.Name = "fieldDistance";
            this.fieldDistance.Options.AllowRunTimeSummaryChange = true;
            this.fieldDistance.Visible = false;
            // 
            // fieldHouseName
            // 
            this.fieldHouseName.AreaIndex = 3;
            this.fieldHouseName.Caption = "House Name";
            this.fieldHouseName.FieldName = "HouseName";
            this.fieldHouseName.Name = "fieldHouseName";
            this.fieldHouseName.Options.AllowRunTimeSummaryChange = true;
            this.fieldHouseName.Visible = false;
            // 
            // fieldSpeciesName
            // 
            this.fieldSpeciesName.AreaIndex = 4;
            this.fieldSpeciesName.Caption = "Species Name";
            this.fieldSpeciesName.FieldName = "SpeciesName";
            this.fieldSpeciesName.Name = "fieldSpeciesName";
            this.fieldSpeciesName.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldAccess
            // 
            this.fieldAccess.AreaIndex = 10;
            this.fieldAccess.Caption = "Access";
            this.fieldAccess.FieldName = "Access";
            this.fieldAccess.Name = "fieldAccess";
            this.fieldAccess.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldVisibility
            // 
            this.fieldVisibility.AreaIndex = 11;
            this.fieldVisibility.Caption = "Visibility";
            this.fieldVisibility.FieldName = "Visibility";
            this.fieldVisibility.Name = "fieldVisibility";
            this.fieldVisibility.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldContext
            // 
            this.fieldContext.AreaIndex = 12;
            this.fieldContext.Caption = "Context";
            this.fieldContext.FieldName = "Context";
            this.fieldContext.Name = "fieldContext";
            this.fieldContext.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldManagement
            // 
            this.fieldManagement.AreaIndex = 13;
            this.fieldManagement.Caption = "Management";
            this.fieldManagement.FieldName = "Management";
            this.fieldManagement.Name = "fieldManagement";
            this.fieldManagement.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldLegalStatus
            // 
            this.fieldLegalStatus.AreaIndex = 14;
            this.fieldLegalStatus.Caption = "Legal Status";
            this.fieldLegalStatus.FieldName = "LegalStatus";
            this.fieldLegalStatus.Name = "fieldLegalStatus";
            this.fieldLegalStatus.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldLastInspectionDate
            // 
            this.fieldLastInspectionDate.AreaIndex = 15;
            this.fieldLastInspectionDate.Caption = "Last Inspection Date";
            this.fieldLastInspectionDate.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDate.Name = "fieldLastInspectionDate";
            this.fieldLastInspectionDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCycle
            // 
            this.fieldInspectionCycle.AreaIndex = 10;
            this.fieldInspectionCycle.Caption = "Inspection Cycle";
            this.fieldInspectionCycle.FieldName = "InspectionCycle";
            this.fieldInspectionCycle.Name = "fieldInspectionCycle";
            this.fieldInspectionCycle.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionCycle.Visible = false;
            // 
            // fieldInspectionUnit
            // 
            this.fieldInspectionUnit.AreaIndex = 10;
            this.fieldInspectionUnit.Caption = "Inspection Unit";
            this.fieldInspectionUnit.FieldName = "InspectionUnit";
            this.fieldInspectionUnit.Name = "fieldInspectionUnit";
            this.fieldInspectionUnit.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionUnit.Visible = false;
            // 
            // fieldNextInspectionDate
            // 
            this.fieldNextInspectionDate.AreaIndex = 16;
            this.fieldNextInspectionDate.Caption = "Next Inspection Date";
            this.fieldNextInspectionDate.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDate.Name = "fieldNextInspectionDate";
            this.fieldNextInspectionDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldGroundType
            // 
            this.fieldGroundType.AreaIndex = 17;
            this.fieldGroundType.Caption = "Ground Type";
            this.fieldGroundType.FieldName = "GroundType";
            this.fieldGroundType.Name = "fieldGroundType";
            this.fieldGroundType.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldDBH
            // 
            this.fieldDBH.AreaIndex = 18;
            this.fieldDBH.Caption = "DBH";
            this.fieldDBH.CellFormat.FormatString = "#";
            this.fieldDBH.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDBH.FieldName = "DBH";
            this.fieldDBH.GrandTotalCellFormat.FormatString = "#";
            this.fieldDBH.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDBH.Name = "fieldDBH";
            this.fieldDBH.Options.AllowRunTimeSummaryChange = true;
            this.fieldDBH.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldDBH.TotalCellFormat.FormatString = "#";
            this.fieldDBH.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDBH.TotalValueFormat.FormatString = "#";
            this.fieldDBH.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDBH.ValueFormat.FormatString = "#";
            this.fieldDBH.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldDBHRange
            // 
            this.fieldDBHRange.AreaIndex = 19;
            this.fieldDBHRange.Caption = "DBH Range";
            this.fieldDBHRange.FieldName = "DBHRange";
            this.fieldDBHRange.Name = "fieldDBHRange";
            this.fieldDBHRange.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldHeight
            // 
            this.fieldHeight.AreaIndex = 20;
            this.fieldHeight.Caption = "Height";
            this.fieldHeight.CellFormat.FormatString = "#.##";
            this.fieldHeight.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHeight.FieldName = "Height";
            this.fieldHeight.GrandTotalCellFormat.FormatString = "#.##";
            this.fieldHeight.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHeight.Name = "fieldHeight";
            this.fieldHeight.Options.AllowRunTimeSummaryChange = true;
            this.fieldHeight.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldHeight.TotalCellFormat.FormatString = "#.##";
            this.fieldHeight.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHeight.TotalValueFormat.FormatString = "#.##";
            this.fieldHeight.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHeight.ValueFormat.FormatString = "#.##";
            this.fieldHeight.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldHeightRange
            // 
            this.fieldHeightRange.AreaIndex = 21;
            this.fieldHeightRange.Caption = "Height Range";
            this.fieldHeightRange.FieldName = "HeightRange";
            this.fieldHeightRange.Name = "fieldHeightRange";
            this.fieldHeightRange.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldProtectionType
            // 
            this.fieldProtectionType.AreaIndex = 22;
            this.fieldProtectionType.Caption = "Protection Type";
            this.fieldProtectionType.FieldName = "ProtectionType";
            this.fieldProtectionType.Name = "fieldProtectionType";
            this.fieldProtectionType.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldCrownDiameter
            // 
            this.fieldCrownDiameter.AreaIndex = 23;
            this.fieldCrownDiameter.Caption = "Crown Diameter";
            this.fieldCrownDiameter.CellFormat.FormatString = "#";
            this.fieldCrownDiameter.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCrownDiameter.FieldName = "CrownDiameter";
            this.fieldCrownDiameter.GrandTotalCellFormat.FormatString = "#";
            this.fieldCrownDiameter.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCrownDiameter.Name = "fieldCrownDiameter";
            this.fieldCrownDiameter.Options.AllowRunTimeSummaryChange = true;
            this.fieldCrownDiameter.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldCrownDiameter.TotalCellFormat.FormatString = "#";
            this.fieldCrownDiameter.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCrownDiameter.TotalValueFormat.FormatString = "#";
            this.fieldCrownDiameter.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCrownDiameter.ValueFormat.FormatString = "#";
            this.fieldCrownDiameter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldPlantDate
            // 
            this.fieldPlantDate.AreaIndex = 19;
            this.fieldPlantDate.Caption = "Plant Date";
            this.fieldPlantDate.FieldName = "PlantDate";
            this.fieldPlantDate.Name = "fieldPlantDate";
            this.fieldPlantDate.Options.AllowRunTimeSummaryChange = true;
            this.fieldPlantDate.Visible = false;
            // 
            // fieldGroupNumber
            // 
            this.fieldGroupNumber.AreaIndex = 19;
            this.fieldGroupNumber.Caption = "Group Number";
            this.fieldGroupNumber.FieldName = "GroupNumber";
            this.fieldGroupNumber.Name = "fieldGroupNumber";
            this.fieldGroupNumber.Options.AllowRunTimeSummaryChange = true;
            this.fieldGroupNumber.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldGroupNumber.Visible = false;
            // 
            // fieldRiskCategory
            // 
            this.fieldRiskCategory.AreaIndex = 6;
            this.fieldRiskCategory.Caption = "Risk Category";
            this.fieldRiskCategory.FieldName = "RiskCategory";
            this.fieldRiskCategory.Name = "fieldRiskCategory";
            this.fieldRiskCategory.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldSiteHazardClass
            // 
            this.fieldSiteHazardClass.AreaIndex = 24;
            this.fieldSiteHazardClass.Caption = "Site Hazard Class";
            this.fieldSiteHazardClass.FieldName = "SiteHazardClass";
            this.fieldSiteHazardClass.Name = "fieldSiteHazardClass";
            this.fieldSiteHazardClass.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldPlantSize
            // 
            this.fieldPlantSize.AreaIndex = 24;
            this.fieldPlantSize.Caption = "Plant Size";
            this.fieldPlantSize.FieldName = "PlantSize";
            this.fieldPlantSize.Name = "fieldPlantSize";
            this.fieldPlantSize.Options.AllowRunTimeSummaryChange = true;
            this.fieldPlantSize.Visible = false;
            // 
            // fieldPlantSource
            // 
            this.fieldPlantSource.AreaIndex = 25;
            this.fieldPlantSource.Caption = "Plant Source";
            this.fieldPlantSource.FieldName = "PlantSource";
            this.fieldPlantSource.Name = "fieldPlantSource";
            this.fieldPlantSource.Options.AllowRunTimeSummaryChange = true;
            this.fieldPlantSource.Visible = false;
            // 
            // fieldPlantMethod
            // 
            this.fieldPlantMethod.AreaIndex = 25;
            this.fieldPlantMethod.Caption = "Plant Method";
            this.fieldPlantMethod.FieldName = "PlantMethod";
            this.fieldPlantMethod.Name = "fieldPlantMethod";
            this.fieldPlantMethod.Options.AllowRunTimeSummaryChange = true;
            this.fieldPlantMethod.Visible = false;
            // 
            // fieldPostcode
            // 
            this.fieldPostcode.AreaIndex = 27;
            this.fieldPostcode.Caption = "Postcode";
            this.fieldPostcode.FieldName = "Postcode";
            this.fieldPostcode.Name = "fieldPostcode";
            this.fieldPostcode.Options.AllowRunTimeSummaryChange = true;
            this.fieldPostcode.Visible = false;
            // 
            // fieldMapLabel
            // 
            this.fieldMapLabel.AreaIndex = 28;
            this.fieldMapLabel.Caption = "MapLabel";
            this.fieldMapLabel.FieldName = "MapLabel";
            this.fieldMapLabel.Name = "fieldMapLabel";
            this.fieldMapLabel.Options.AllowRunTimeSummaryChange = true;
            this.fieldMapLabel.Visible = false;
            // 
            // fieldStatus
            // 
            this.fieldStatus.AreaIndex = 33;
            this.fieldStatus.Caption = "Status";
            this.fieldStatus.FieldName = "Status";
            this.fieldStatus.Name = "fieldStatus";
            this.fieldStatus.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldSize
            // 
            this.fieldSize.AreaIndex = 25;
            this.fieldSize.Caption = "Size";
            this.fieldSize.FieldName = "Size";
            this.fieldSize.Name = "fieldSize";
            this.fieldSize.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTarget1
            // 
            this.fieldTarget1.AreaIndex = 27;
            this.fieldTarget1.Caption = "Target 1";
            this.fieldTarget1.FieldName = "Target1";
            this.fieldTarget1.Name = "fieldTarget1";
            this.fieldTarget1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTarget2
            // 
            this.fieldTarget2.AreaIndex = 28;
            this.fieldTarget2.Caption = "Target 2";
            this.fieldTarget2.FieldName = "Target2";
            this.fieldTarget2.Name = "fieldTarget2";
            this.fieldTarget2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTarget3
            // 
            this.fieldTarget3.AreaIndex = 29;
            this.fieldTarget3.Caption = "Target 3";
            this.fieldTarget3.FieldName = "Target3";
            this.fieldTarget3.Name = "fieldTarget3";
            this.fieldTarget3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldLastModifiedDate
            // 
            this.fieldLastModifiedDate.AreaIndex = 34;
            this.fieldLastModifiedDate.Caption = "Last Modified Date";
            this.fieldLastModifiedDate.FieldName = "LastModifiedDate";
            this.fieldLastModifiedDate.Name = "fieldLastModifiedDate";
            this.fieldLastModifiedDate.Options.AllowRunTimeSummaryChange = true;
            this.fieldLastModifiedDate.Visible = false;
            // 
            // fieldRemarks
            // 
            this.fieldRemarks.AreaIndex = 33;
            this.fieldRemarks.Caption = "Remarks";
            this.fieldRemarks.FieldName = "Remarks";
            this.fieldRemarks.Name = "fieldRemarks";
            this.fieldRemarks.Options.AllowRunTimeSummaryChange = true;
            this.fieldRemarks.Visible = false;
            // 
            // fieldUser1
            // 
            this.fieldUser1.AreaIndex = 38;
            this.fieldUser1.Caption = "User 1";
            this.fieldUser1.FieldName = "User1";
            this.fieldUser1.Name = "fieldUser1";
            this.fieldUser1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldUser2
            // 
            this.fieldUser2.AreaIndex = 39;
            this.fieldUser2.Caption = "User 2";
            this.fieldUser2.FieldName = "User2";
            this.fieldUser2.Name = "fieldUser2";
            this.fieldUser2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldUser3
            // 
            this.fieldUser3.AreaIndex = 40;
            this.fieldUser3.Caption = "User 3";
            this.fieldUser3.FieldName = "User3";
            this.fieldUser3.Name = "fieldUser3";
            this.fieldUser3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldAgeClass
            // 
            this.fieldAgeClass.AreaIndex = 9;
            this.fieldAgeClass.Caption = "Age Class";
            this.fieldAgeClass.FieldName = "AgeClass";
            this.fieldAgeClass.Name = "fieldAgeClass";
            this.fieldAgeClass.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldX
            // 
            this.fieldX.AreaIndex = 37;
            this.fieldX.Caption = "X";
            this.fieldX.FieldName = "X";
            this.fieldX.Name = "fieldX";
            this.fieldX.Options.AllowRunTimeSummaryChange = true;
            this.fieldX.Visible = false;
            // 
            // fieldY
            // 
            this.fieldY.AreaIndex = 37;
            this.fieldY.Caption = "Y";
            this.fieldY.FieldName = "Y";
            this.fieldY.Name = "fieldY";
            this.fieldY.Options.AllowRunTimeSummaryChange = true;
            this.fieldY.Visible = false;
            // 
            // fieldAreaHa
            // 
            this.fieldAreaHa.AreaIndex = 37;
            this.fieldAreaHa.Caption = "Area [M�]";
            this.fieldAreaHa.FieldName = "AreaHa";
            this.fieldAreaHa.Name = "fieldAreaHa";
            this.fieldAreaHa.Options.AllowRunTimeSummaryChange = true;
            this.fieldAreaHa.Visible = false;
            // 
            // fieldReplantCount
            // 
            this.fieldReplantCount.AreaIndex = 38;
            this.fieldReplantCount.Caption = "Replant Count";
            this.fieldReplantCount.FieldName = "ReplantCount";
            this.fieldReplantCount.Name = "fieldReplantCount";
            this.fieldReplantCount.Options.AllowRunTimeSummaryChange = true;
            this.fieldReplantCount.Visible = false;
            // 
            // fieldSurveyDate
            // 
            this.fieldSurveyDate.AreaIndex = 37;
            this.fieldSurveyDate.Caption = "Survey Date";
            this.fieldSurveyDate.FieldName = "SurveyDate";
            this.fieldSurveyDate.Name = "fieldSurveyDate";
            this.fieldSurveyDate.Options.AllowRunTimeSummaryChange = true;
            this.fieldSurveyDate.Visible = false;
            // 
            // fieldTreeType
            // 
            this.fieldTreeType.AreaIndex = 37;
            this.fieldTreeType.Caption = "Tree Type";
            this.fieldTreeType.FieldName = "TreeType";
            this.fieldTreeType.Name = "fieldTreeType";
            this.fieldTreeType.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeType.Visible = false;
            // 
            // fieldHedgeOwner
            // 
            this.fieldHedgeOwner.AreaIndex = 37;
            this.fieldHedgeOwner.Caption = "Hedge Owner";
            this.fieldHedgeOwner.FieldName = "HedgeOwner";
            this.fieldHedgeOwner.Name = "fieldHedgeOwner";
            this.fieldHedgeOwner.Options.AllowRunTimeSummaryChange = true;
            this.fieldHedgeOwner.Visible = false;
            // 
            // fieldHedgeLength
            // 
            this.fieldHedgeLength.AreaIndex = 37;
            this.fieldHedgeLength.Caption = "Hedge Length";
            this.fieldHedgeLength.FieldName = "HedgeLength";
            this.fieldHedgeLength.Name = "fieldHedgeLength";
            this.fieldHedgeLength.Options.AllowRunTimeSummaryChange = true;
            this.fieldHedgeLength.Visible = false;
            // 
            // fieldGardenSize
            // 
            this.fieldGardenSize.AreaIndex = 37;
            this.fieldGardenSize.Caption = "Garden Size";
            this.fieldGardenSize.FieldName = "GardenSize";
            this.fieldGardenSize.Name = "fieldGardenSize";
            this.fieldGardenSize.Options.AllowRunTimeSummaryChange = true;
            this.fieldGardenSize.Visible = false;
            // 
            // fieldPropertyProximity
            // 
            this.fieldPropertyProximity.AreaIndex = 37;
            this.fieldPropertyProximity.Caption = "Property Proximity";
            this.fieldPropertyProximity.FieldName = "PropertyProximity";
            this.fieldPropertyProximity.Name = "fieldPropertyProximity";
            this.fieldPropertyProximity.Options.AllowRunTimeSummaryChange = true;
            this.fieldPropertyProximity.Visible = false;
            // 
            // fieldSiteLevel
            // 
            this.fieldSiteLevel.AreaIndex = 37;
            this.fieldSiteLevel.Caption = "Site Level";
            this.fieldSiteLevel.FieldName = "SiteLevel";
            this.fieldSiteLevel.Name = "fieldSiteLevel";
            this.fieldSiteLevel.Options.AllowRunTimeSummaryChange = true;
            this.fieldSiteLevel.Visible = false;
            // 
            // fieldSituation
            // 
            this.fieldSituation.AreaIndex = 8;
            this.fieldSituation.Caption = "Situation";
            this.fieldSituation.FieldName = "Situation";
            this.fieldSituation.Name = "fieldSituation";
            this.fieldSituation.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldRiskFactor
            // 
            this.fieldRiskFactor.AreaIndex = 7;
            this.fieldRiskFactor.Caption = "Risk Factor";
            this.fieldRiskFactor.CellFormat.FormatString = "#.##";
            this.fieldRiskFactor.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldRiskFactor.FieldName = "RiskFactor";
            this.fieldRiskFactor.GrandTotalCellFormat.FormatString = "#.##";
            this.fieldRiskFactor.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldRiskFactor.Name = "fieldRiskFactor";
            this.fieldRiskFactor.Options.AllowRunTimeSummaryChange = true;
            this.fieldRiskFactor.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldRiskFactor.TotalCellFormat.FormatString = "#.##";
            this.fieldRiskFactor.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldRiskFactor.TotalValueFormat.FormatString = "#.##";
            this.fieldRiskFactor.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldRiskFactor.ValueFormat.FormatString = "#.##";
            this.fieldRiskFactor.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldPolygonXY
            // 
            this.fieldPolygonXY.AreaIndex = 39;
            this.fieldPolygonXY.Caption = "Polygon XY";
            this.fieldPolygonXY.FieldName = "PolygonXY";
            this.fieldPolygonXY.Name = "fieldPolygonXY";
            this.fieldPolygonXY.Options.AllowRunTimeSummaryChange = true;
            this.fieldPolygonXY.Visible = false;
            // 
            // fieldcolor
            // 
            this.fieldcolor.AreaIndex = 51;
            this.fieldcolor.Caption = "color";
            this.fieldcolor.FieldName = "color";
            this.fieldcolor.Name = "fieldcolor";
            this.fieldcolor.Options.AllowRunTimeSummaryChange = true;
            this.fieldcolor.Visible = false;
            // 
            // fieldOwnershipName
            // 
            this.fieldOwnershipName.AreaIndex = 3;
            this.fieldOwnershipName.Caption = "Ownership Name";
            this.fieldOwnershipName.FieldName = "OwnershipName";
            this.fieldOwnershipName.Name = "fieldOwnershipName";
            this.fieldOwnershipName.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeCount
            // 
            this.fieldTreeCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldTreeCount.AreaIndex = 0;
            this.fieldTreeCount.Caption = "Tree Count";
            this.fieldTreeCount.FieldName = "TreeCount";
            this.fieldTreeCount.Name = "fieldTreeCount";
            this.fieldTreeCount.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldLinkedInspectionCount
            // 
            this.fieldLinkedInspectionCount.AreaIndex = 34;
            this.fieldLinkedInspectionCount.Caption = "Inspection Count";
            this.fieldLinkedInspectionCount.FieldName = "LinkedInspectionCount";
            this.fieldLinkedInspectionCount.Name = "fieldLinkedInspectionCount";
            this.fieldLinkedInspectionCount.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeCavat
            // 
            this.fieldTreeCavat.AreaIndex = 30;
            this.fieldTreeCavat.Caption = "Cavat";
            this.fieldTreeCavat.CellFormat.FormatString = "c";
            this.fieldTreeCavat.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat.FieldName = "TreeCavat";
            this.fieldTreeCavat.GrandTotalCellFormat.FormatString = "c";
            this.fieldTreeCavat.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat.Name = "fieldTreeCavat";
            this.fieldTreeCavat.TotalCellFormat.FormatString = "c";
            this.fieldTreeCavat.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat.TotalValueFormat.FormatString = "c";
            this.fieldTreeCavat.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat.ValueFormat.FormatString = "c";
            this.fieldTreeCavat.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreeStemCount
            // 
            this.fieldTreeStemCount.AreaIndex = 32;
            this.fieldTreeStemCount.Caption = "Stem Count";
            this.fieldTreeStemCount.FieldName = "TreeStemCount";
            this.fieldTreeStemCount.Name = "fieldTreeStemCount";
            // 
            // fieldTreeCrownNorth
            // 
            this.fieldTreeCrownNorth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownNorth.AreaIndex = 0;
            this.fieldTreeCrownNorth.Caption = "Crown North";
            this.fieldTreeCrownNorth.FieldName = "TreeCrownNorth";
            this.fieldTreeCrownNorth.Name = "fieldTreeCrownNorth";
            this.fieldTreeCrownNorth.Visible = false;
            // 
            // fieldTreeCrownSouth
            // 
            this.fieldTreeCrownSouth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownSouth.AreaIndex = 0;
            this.fieldTreeCrownSouth.Caption = "Crown South";
            this.fieldTreeCrownSouth.FieldName = "TreeCrownSouth";
            this.fieldTreeCrownSouth.Name = "fieldTreeCrownSouth";
            this.fieldTreeCrownSouth.Visible = false;
            // 
            // fieldTreeCrownEast
            // 
            this.fieldTreeCrownEast.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownEast.AreaIndex = 0;
            this.fieldTreeCrownEast.Caption = "Crown East";
            this.fieldTreeCrownEast.FieldName = "TreeCrownEast";
            this.fieldTreeCrownEast.Name = "fieldTreeCrownEast";
            this.fieldTreeCrownEast.Visible = false;
            // 
            // fieldTreeCrownWest
            // 
            this.fieldTreeCrownWest.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownWest.AreaIndex = 0;
            this.fieldTreeCrownWest.Caption = "Crown West";
            this.fieldTreeCrownWest.FieldName = "TreeCrownWest";
            this.fieldTreeCrownWest.Name = "fieldTreeCrownWest";
            this.fieldTreeCrownWest.Visible = false;
            // 
            // fieldTreeRetentionCategory
            // 
            this.fieldTreeRetentionCategory.AreaIndex = 26;
            this.fieldTreeRetentionCategory.Caption = "Retention Category";
            this.fieldTreeRetentionCategory.FieldName = "TreeRetentionCategory";
            this.fieldTreeRetentionCategory.Name = "fieldTreeRetentionCategory";
            // 
            // fieldTreeSULE
            // 
            this.fieldTreeSULE.AreaIndex = 31;
            this.fieldTreeSULE.Caption = "SULE";
            this.fieldTreeSULE.FieldName = "TreeSULE";
            this.fieldTreeSULE.Name = "fieldTreeSULE";
            // 
            // fieldTreeSpeciesVariety
            // 
            this.fieldTreeSpeciesVariety.AreaIndex = 5;
            this.fieldTreeSpeciesVariety.Caption = "Species Variety";
            this.fieldTreeSpeciesVariety.FieldName = "TreeSpeciesVariety";
            this.fieldTreeSpeciesVariety.Name = "fieldTreeSpeciesVariety";
            // 
            // fieldTreeUserPicklist1
            // 
            this.fieldTreeUserPicklist1.AreaIndex = 41;
            this.fieldTreeUserPicklist1.Caption = "User Picklist 1";
            this.fieldTreeUserPicklist1.FieldName = "TreeUserPicklist1";
            this.fieldTreeUserPicklist1.Name = "fieldTreeUserPicklist1";
            // 
            // fieldTreeUserPicklist2
            // 
            this.fieldTreeUserPicklist2.AreaIndex = 42;
            this.fieldTreeUserPicklist2.Caption = "User Picklist 2";
            this.fieldTreeUserPicklist2.FieldName = "TreeUserPicklist2";
            this.fieldTreeUserPicklist2.Name = "fieldTreeUserPicklist2";
            // 
            // fieldTreeUserPicklist3
            // 
            this.fieldTreeUserPicklist3.AreaIndex = 43;
            this.fieldTreeUserPicklist3.Caption = "User Picklist 3";
            this.fieldTreeUserPicklist3.FieldName = "TreeUserPicklist3";
            this.fieldTreeUserPicklist3.Name = "fieldTreeUserPicklist3";
            // 
            // fieldTreeObjectLength
            // 
            this.fieldTreeObjectLength.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeObjectLength.AreaIndex = 12;
            this.fieldTreeObjectLength.Caption = "Object Length";
            this.fieldTreeObjectLength.FieldName = "TreeObjectLength";
            this.fieldTreeObjectLength.Name = "fieldTreeObjectLength";
            this.fieldTreeObjectLength.Visible = false;
            // 
            // fieldTreeObjectWidth
            // 
            this.fieldTreeObjectWidth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeObjectWidth.AreaIndex = 13;
            this.fieldTreeObjectWidth.Caption = "Object Width";
            this.fieldTreeObjectWidth.FieldName = "TreeObjectWidth";
            this.fieldTreeObjectWidth.Name = "fieldTreeObjectWidth";
            this.fieldTreeObjectWidth.Visible = false;
            // 
            // chartControl1
            // 
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl1.EmptyChartText.Text = "No Data To Graph - Try Selecting Data from the Analysis Grid.";
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            sideBySideBarSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl1.SeriesTemplate.Label = sideBySideBarSeriesLabel1;
            this.chartControl1.Size = new System.Drawing.Size(949, 379);
            this.chartControl1.TabIndex = 1;
            this.chartControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chartControl1_MouseUp);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "ProjectManDataSet";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp01236_AT_Data_Analysis_ActionsTableAdapter
            // 
            this.sp01236_AT_Data_Analysis_ActionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01232_AT_Reports_Listings_Action_FilterTableAdapter
            // 
            this.sp01232_AT_Reports_Listings_Action_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp01235_AT_Data_Analysis_InspectionsTableAdapter
            // 
            this.sp01235_AT_Data_Analysis_InspectionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter
            // 
            this.sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp01234_AT_Data_Analysis_TreesTableAdapter
            // 
            this.sp01234_AT_Data_Analysis_TreesTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("eca7f263-f03a-476e-be2c-7e2164986cdc");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(339, 200);
            this.dockPanel1.Size = new System.Drawing.Size(339, 850);
            this.dockPanel1.Text = "Data Source";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.btnAnalyze);
            this.dockPanel1_Container.Controls.Add(this.xtraTabControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(332, 818);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // btnAnalyze
            // 
            this.btnAnalyze.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnalyze.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAnalyze.ImageOptions.Image")));
            this.btnAnalyze.Location = new System.Drawing.Point(236, 795);
            this.btnAnalyze.Name = "btnAnalyze";
            this.btnAnalyze.Size = new System.Drawing.Size(95, 22);
            this.btnAnalyze.TabIndex = 4;
            this.btnAnalyze.Text = "Analyze Data";
            this.btnAnalyze.Click += new System.EventHandler(this.btnAnalyze_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(3, 3);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(329, 791);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.layoutControl3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(324, 765);
            this.xtraTabPage1.Text = "Trees";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl3.Controls.Add(this.btnLoadTrees);
            this.layoutControl3.Controls.Add(this.gridSplitContainer1);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.MenuManager = this.barManager1;
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(854, 204, 250, 350);
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(324, 765);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.Location = new System.Drawing.Point(41, 7);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(187, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl3;
            this.buttonEditClientFilter.TabIndex = 8;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // btnLoadTrees
            // 
            this.btnLoadTrees.ImageOptions.ImageIndex = 0;
            this.btnLoadTrees.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadTrees.Location = new System.Drawing.Point(232, 7);
            this.btnLoadTrees.Name = "btnLoadTrees";
            this.btnLoadTrees.Size = new System.Drawing.Size(85, 22);
            this.btnLoadTrees.StyleController = this.layoutControl3;
            this.btnLoadTrees.TabIndex = 1;
            this.btnLoadTrees.Text = "Load Trees";
            this.btnLoadTrees.Click += new System.EventHandler(this.btnLoadTrees_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "refresh_16x16");
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(7, 33);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(310, 725);
            this.gridSplitContainer1.TabIndex = 4;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01220ATReportsListingsTreeFilterBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(310, 725);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01220ATReportsListingsTreeFilterBindingSource
            // 
            this.sp01220ATReportsListingsTreeFilterBindingSource.DataMember = "sp01220_AT_Reports_Listings_Tree_Filter";
            this.sp01220ATReportsListingsTreeFilterBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeID,
            this.colSiteID,
            this.colClientID1,
            this.colSiteName,
            this.colClientName1,
            this.colTreeReference,
            this.colMappingID,
            this.colGridReference,
            this.colDistance,
            this.colHouseName,
            this.colSpeciesName,
            this.colAccess,
            this.colVisibility,
            this.colContext,
            this.colManagement,
            this.colLegalStatus,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnit,
            this.colNextInspectionDate,
            this.colGroundType,
            this.colDBH,
            this.colDBHRange,
            this.colHeight,
            this.colHeightRange,
            this.colProtectionType,
            this.colCrownDiameter,
            this.colPlantDate,
            this.colGroupNumber,
            this.colRiskCategory,
            this.colSiteHazardClass,
            this.colPlantSize,
            this.colPlantSource,
            this.colPlantMethod,
            this.colPostcode,
            this.colMapLabel,
            this.colStatus,
            this.colSize,
            this.colTarget1,
            this.colTarget2,
            this.colTarget3,
            this.colLastModifiedDate,
            this.colRemarks,
            this.colUser1,
            this.colUser2,
            this.colUser3,
            this.colAgeClass,
            this.colX,
            this.colY,
            this.colAreaHa,
            this.colReplantCount,
            this.colSurveyDate,
            this.colTreeType,
            this.colHedgeOwner,
            this.colHedgeLength,
            this.colGardenSize,
            this.colPropertyProximity,
            this.colSiteLevel,
            this.colSituation,
            this.colRiskFactor,
            this.colPolygonXY,
            this.colcolor,
            this.colLinkedInspectionCount,
            this.colOwnershipName,
            this.Calculated1,
            this.Calculated2,
            this.Calculated3,
            this.colCavat,
            this.colStemCount,
            this.colCrownNorth,
            this.colCrownSouth,
            this.colCrownEast,
            this.colCrownWest,
            this.colRetentionCategory,
            this.colSULE,
            this.colUserPicklist1,
            this.colUserPicklist2,
            this.colUserPicklist3,
            this.colSpeciesVariety,
            this.colObjectLength,
            this.colObjectWidth});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 513, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 0;
            this.colTreeReference.Width = 112;
            // 
            // colMappingID
            // 
            this.colMappingID.Caption = "Map ID";
            this.colMappingID.FieldName = "MappingID";
            this.colMappingID.Name = "colMappingID";
            this.colMappingID.OptionsColumn.AllowEdit = false;
            this.colMappingID.OptionsColumn.AllowFocus = false;
            this.colMappingID.OptionsColumn.ReadOnly = true;
            // 
            // colGridReference
            // 
            this.colGridReference.Caption = "Grid Reference";
            this.colGridReference.FieldName = "GridReference";
            this.colGridReference.Name = "colGridReference";
            this.colGridReference.OptionsColumn.AllowEdit = false;
            this.colGridReference.OptionsColumn.AllowFocus = false;
            this.colGridReference.OptionsColumn.ReadOnly = true;
            this.colGridReference.Visible = true;
            this.colGridReference.VisibleIndex = 5;
            this.colGridReference.Width = 113;
            // 
            // colDistance
            // 
            this.colDistance.Caption = "Distance";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            // 
            // colHouseName
            // 
            this.colHouseName.Caption = "House Name";
            this.colHouseName.FieldName = "HouseName";
            this.colHouseName.Name = "colHouseName";
            this.colHouseName.OptionsColumn.AllowEdit = false;
            this.colHouseName.OptionsColumn.AllowFocus = false;
            this.colHouseName.OptionsColumn.ReadOnly = true;
            this.colHouseName.Visible = true;
            this.colHouseName.VisibleIndex = 3;
            this.colHouseName.Width = 99;
            // 
            // colSpeciesName
            // 
            this.colSpeciesName.Caption = "Species";
            this.colSpeciesName.FieldName = "SpeciesName";
            this.colSpeciesName.Name = "colSpeciesName";
            this.colSpeciesName.OptionsColumn.AllowEdit = false;
            this.colSpeciesName.OptionsColumn.AllowFocus = false;
            this.colSpeciesName.OptionsColumn.ReadOnly = true;
            this.colSpeciesName.Visible = true;
            this.colSpeciesName.VisibleIndex = 14;
            // 
            // colAccess
            // 
            this.colAccess.Caption = "Access";
            this.colAccess.FieldName = "Access";
            this.colAccess.Name = "colAccess";
            this.colAccess.OptionsColumn.AllowEdit = false;
            this.colAccess.OptionsColumn.AllowFocus = false;
            this.colAccess.OptionsColumn.ReadOnly = true;
            // 
            // colVisibility
            // 
            this.colVisibility.Caption = "Visibility";
            this.colVisibility.FieldName = "Visibility";
            this.colVisibility.Name = "colVisibility";
            this.colVisibility.OptionsColumn.AllowEdit = false;
            this.colVisibility.OptionsColumn.AllowFocus = false;
            this.colVisibility.OptionsColumn.ReadOnly = true;
            // 
            // colContext
            // 
            this.colContext.Caption = "Context";
            this.colContext.FieldName = "Context";
            this.colContext.Name = "colContext";
            this.colContext.OptionsColumn.AllowEdit = false;
            this.colContext.OptionsColumn.AllowFocus = false;
            this.colContext.OptionsColumn.ReadOnly = true;
            // 
            // colManagement
            // 
            this.colManagement.Caption = "Management";
            this.colManagement.FieldName = "Management";
            this.colManagement.Name = "colManagement";
            this.colManagement.OptionsColumn.AllowEdit = false;
            this.colManagement.OptionsColumn.AllowFocus = false;
            this.colManagement.OptionsColumn.ReadOnly = true;
            this.colManagement.Width = 88;
            // 
            // colLegalStatus
            // 
            this.colLegalStatus.Caption = "Legal Status";
            this.colLegalStatus.FieldName = "LegalStatus";
            this.colLegalStatus.Name = "colLegalStatus";
            this.colLegalStatus.OptionsColumn.AllowEdit = false;
            this.colLegalStatus.OptionsColumn.AllowFocus = false;
            this.colLegalStatus.OptionsColumn.ReadOnly = true;
            this.colLegalStatus.Visible = true;
            this.colLegalStatus.VisibleIndex = 24;
            this.colLegalStatus.Width = 92;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection";
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 10;
            this.colLastInspectionDate.Width = 98;
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Inspection Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.Width = 99;
            // 
            // colInspectionUnit
            // 
            this.colInspectionUnit.Caption = "Inspection Unit";
            this.colInspectionUnit.FieldName = "InspectionUnit";
            this.colInspectionUnit.Name = "colInspectionUnit";
            this.colInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colInspectionUnit.Width = 96;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection";
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 11;
            this.colNextInspectionDate.Width = 99;
            // 
            // colGroundType
            // 
            this.colGroundType.Caption = "Ground Type";
            this.colGroundType.FieldName = "GroundType";
            this.colGroundType.Name = "colGroundType";
            this.colGroundType.OptionsColumn.AllowEdit = false;
            this.colGroundType.OptionsColumn.AllowFocus = false;
            this.colGroundType.OptionsColumn.ReadOnly = true;
            this.colGroundType.Visible = true;
            this.colGroundType.VisibleIndex = 22;
            this.colGroundType.Width = 91;
            // 
            // colDBH
            // 
            this.colDBH.Caption = "DBH";
            this.colDBH.FieldName = "DBH";
            this.colDBH.Name = "colDBH";
            this.colDBH.OptionsColumn.AllowEdit = false;
            this.colDBH.OptionsColumn.AllowFocus = false;
            this.colDBH.OptionsColumn.ReadOnly = true;
            this.colDBH.Visible = true;
            this.colDBH.VisibleIndex = 15;
            // 
            // colDBHRange
            // 
            this.colDBHRange.Caption = "DBH Range";
            this.colDBHRange.FieldName = "DBHRange";
            this.colDBHRange.Name = "colDBHRange";
            this.colDBHRange.OptionsColumn.AllowEdit = false;
            this.colDBHRange.OptionsColumn.AllowFocus = false;
            this.colDBHRange.OptionsColumn.ReadOnly = true;
            this.colDBHRange.Visible = true;
            this.colDBHRange.VisibleIndex = 16;
            // 
            // colHeight
            // 
            this.colHeight.Caption = "Height";
            this.colHeight.FieldName = "Height";
            this.colHeight.Name = "colHeight";
            this.colHeight.OptionsColumn.AllowEdit = false;
            this.colHeight.OptionsColumn.AllowFocus = false;
            this.colHeight.OptionsColumn.ReadOnly = true;
            this.colHeight.Visible = true;
            this.colHeight.VisibleIndex = 17;
            // 
            // colHeightRange
            // 
            this.colHeightRange.Caption = "Height Range";
            this.colHeightRange.FieldName = "HeightRange";
            this.colHeightRange.Name = "colHeightRange";
            this.colHeightRange.OptionsColumn.AllowEdit = false;
            this.colHeightRange.OptionsColumn.AllowFocus = false;
            this.colHeightRange.OptionsColumn.ReadOnly = true;
            this.colHeightRange.Visible = true;
            this.colHeightRange.VisibleIndex = 18;
            this.colHeightRange.Width = 91;
            // 
            // colProtectionType
            // 
            this.colProtectionType.Caption = "Protection Type";
            this.colProtectionType.FieldName = "ProtectionType";
            this.colProtectionType.Name = "colProtectionType";
            this.colProtectionType.OptionsColumn.AllowEdit = false;
            this.colProtectionType.OptionsColumn.AllowFocus = false;
            this.colProtectionType.OptionsColumn.ReadOnly = true;
            this.colProtectionType.Width = 105;
            // 
            // colCrownDiameter
            // 
            this.colCrownDiameter.Caption = "Crown Diameter";
            this.colCrownDiameter.FieldName = "CrownDiameter";
            this.colCrownDiameter.Name = "colCrownDiameter";
            this.colCrownDiameter.OptionsColumn.AllowEdit = false;
            this.colCrownDiameter.OptionsColumn.AllowFocus = false;
            this.colCrownDiameter.OptionsColumn.ReadOnly = true;
            this.colCrownDiameter.Visible = true;
            this.colCrownDiameter.VisibleIndex = 20;
            this.colCrownDiameter.Width = 111;
            // 
            // colPlantDate
            // 
            this.colPlantDate.Caption = "Plant Date";
            this.colPlantDate.FieldName = "PlantDate";
            this.colPlantDate.Name = "colPlantDate";
            this.colPlantDate.OptionsColumn.AllowEdit = false;
            this.colPlantDate.OptionsColumn.AllowFocus = false;
            this.colPlantDate.OptionsColumn.ReadOnly = true;
            // 
            // colGroupNumber
            // 
            this.colGroupNumber.Caption = "Group Number";
            this.colGroupNumber.FieldName = "GroupNumber";
            this.colGroupNumber.Name = "colGroupNumber";
            this.colGroupNumber.OptionsColumn.AllowEdit = false;
            this.colGroupNumber.OptionsColumn.AllowFocus = false;
            this.colGroupNumber.OptionsColumn.ReadOnly = true;
            this.colGroupNumber.Visible = true;
            this.colGroupNumber.VisibleIndex = 12;
            this.colGroupNumber.Width = 96;
            // 
            // colRiskCategory
            // 
            this.colRiskCategory.Caption = "Risk Category";
            this.colRiskCategory.FieldName = "RiskCategory";
            this.colRiskCategory.Name = "colRiskCategory";
            this.colRiskCategory.OptionsColumn.AllowEdit = false;
            this.colRiskCategory.OptionsColumn.AllowFocus = false;
            this.colRiskCategory.OptionsColumn.ReadOnly = true;
            this.colRiskCategory.Visible = true;
            this.colRiskCategory.VisibleIndex = 2;
            this.colRiskCategory.Width = 95;
            // 
            // colSiteHazardClass
            // 
            this.colSiteHazardClass.Caption = "Site Hazard Class";
            this.colSiteHazardClass.FieldName = "SiteHazardClass";
            this.colSiteHazardClass.Name = "colSiteHazardClass";
            this.colSiteHazardClass.OptionsColumn.AllowEdit = false;
            this.colSiteHazardClass.OptionsColumn.AllowFocus = false;
            this.colSiteHazardClass.OptionsColumn.ReadOnly = true;
            this.colSiteHazardClass.Width = 115;
            // 
            // colPlantSize
            // 
            this.colPlantSize.Caption = "Plant Size";
            this.colPlantSize.FieldName = "PlantSize";
            this.colPlantSize.Name = "colPlantSize";
            this.colPlantSize.OptionsColumn.AllowEdit = false;
            this.colPlantSize.OptionsColumn.AllowFocus = false;
            this.colPlantSize.OptionsColumn.ReadOnly = true;
            // 
            // colPlantSource
            // 
            this.colPlantSource.Caption = "Plant Source";
            this.colPlantSource.FieldName = "PlantSource";
            this.colPlantSource.Name = "colPlantSource";
            this.colPlantSource.OptionsColumn.AllowEdit = false;
            this.colPlantSource.OptionsColumn.AllowFocus = false;
            this.colPlantSource.OptionsColumn.ReadOnly = true;
            this.colPlantSource.Width = 107;
            // 
            // colPlantMethod
            // 
            this.colPlantMethod.Caption = "Plant Method";
            this.colPlantMethod.FieldName = "PlantMethod";
            this.colPlantMethod.Name = "colPlantMethod";
            this.colPlantMethod.OptionsColumn.AllowEdit = false;
            this.colPlantMethod.OptionsColumn.AllowFocus = false;
            this.colPlantMethod.OptionsColumn.ReadOnly = true;
            this.colPlantMethod.Width = 110;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 4;
            // 
            // colMapLabel
            // 
            this.colMapLabel.Caption = "Map Label";
            this.colMapLabel.FieldName = "MapLabel";
            this.colMapLabel.Name = "colMapLabel";
            this.colMapLabel.OptionsColumn.AllowEdit = false;
            this.colMapLabel.OptionsColumn.AllowFocus = false;
            this.colMapLabel.OptionsColumn.ReadOnly = true;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 13;
            // 
            // colSize
            // 
            this.colSize.Caption = "Size";
            this.colSize.FieldName = "Size";
            this.colSize.Name = "colSize";
            this.colSize.OptionsColumn.AllowEdit = false;
            this.colSize.OptionsColumn.AllowFocus = false;
            this.colSize.OptionsColumn.ReadOnly = true;
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 19;
            // 
            // colTarget1
            // 
            this.colTarget1.Caption = "Target 1";
            this.colTarget1.FieldName = "Target1";
            this.colTarget1.Name = "colTarget1";
            this.colTarget1.OptionsColumn.AllowEdit = false;
            this.colTarget1.OptionsColumn.AllowFocus = false;
            this.colTarget1.OptionsColumn.ReadOnly = true;
            this.colTarget1.Visible = true;
            this.colTarget1.VisibleIndex = 7;
            // 
            // colTarget2
            // 
            this.colTarget2.Caption = "Target 2";
            this.colTarget2.FieldName = "Target2";
            this.colTarget2.Name = "colTarget2";
            this.colTarget2.OptionsColumn.AllowEdit = false;
            this.colTarget2.OptionsColumn.AllowFocus = false;
            this.colTarget2.OptionsColumn.ReadOnly = true;
            this.colTarget2.Visible = true;
            this.colTarget2.VisibleIndex = 8;
            // 
            // colTarget3
            // 
            this.colTarget3.Caption = "Target 3";
            this.colTarget3.FieldName = "Target3";
            this.colTarget3.Name = "colTarget3";
            this.colTarget3.OptionsColumn.AllowEdit = false;
            this.colTarget3.OptionsColumn.AllowFocus = false;
            this.colTarget3.OptionsColumn.ReadOnly = true;
            this.colTarget3.Visible = true;
            this.colTarget3.VisibleIndex = 9;
            // 
            // colLastModifiedDate
            // 
            this.colLastModifiedDate.Caption = "Last Modified";
            this.colLastModifiedDate.FieldName = "LastModifiedDate";
            this.colLastModifiedDate.Name = "colLastModifiedDate";
            this.colLastModifiedDate.OptionsColumn.AllowEdit = false;
            this.colLastModifiedDate.OptionsColumn.AllowFocus = false;
            this.colLastModifiedDate.OptionsColumn.ReadOnly = true;
            this.colLastModifiedDate.Width = 112;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 27;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            // 
            // colUser1
            // 
            this.colUser1.Caption = "User 1";
            this.colUser1.FieldName = "User1";
            this.colUser1.Name = "colUser1";
            this.colUser1.OptionsColumn.AllowEdit = false;
            this.colUser1.OptionsColumn.AllowFocus = false;
            this.colUser1.OptionsColumn.ReadOnly = true;
            // 
            // colUser2
            // 
            this.colUser2.Caption = "User 2";
            this.colUser2.FieldName = "User2";
            this.colUser2.Name = "colUser2";
            this.colUser2.OptionsColumn.AllowEdit = false;
            this.colUser2.OptionsColumn.AllowFocus = false;
            this.colUser2.OptionsColumn.ReadOnly = true;
            // 
            // colUser3
            // 
            this.colUser3.Caption = "User 3";
            this.colUser3.FieldName = "User3";
            this.colUser3.Name = "colUser3";
            this.colUser3.OptionsColumn.AllowEdit = false;
            this.colUser3.OptionsColumn.AllowFocus = false;
            this.colUser3.OptionsColumn.ReadOnly = true;
            // 
            // colAgeClass
            // 
            this.colAgeClass.Caption = "Age Class";
            this.colAgeClass.FieldName = "AgeClass";
            this.colAgeClass.Name = "colAgeClass";
            this.colAgeClass.OptionsColumn.AllowEdit = false;
            this.colAgeClass.OptionsColumn.AllowFocus = false;
            this.colAgeClass.OptionsColumn.ReadOnly = true;
            this.colAgeClass.Visible = true;
            this.colAgeClass.VisibleIndex = 21;
            // 
            // colX
            // 
            this.colX.Caption = "X";
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.OptionsColumn.AllowEdit = false;
            this.colX.OptionsColumn.AllowFocus = false;
            this.colX.OptionsColumn.ReadOnly = true;
            // 
            // colY
            // 
            this.colY.Caption = "Y";
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.OptionsColumn.AllowEdit = false;
            this.colY.OptionsColumn.AllowFocus = false;
            this.colY.OptionsColumn.ReadOnly = true;
            // 
            // colAreaHa
            // 
            this.colAreaHa.Caption = "Area [M�]";
            this.colAreaHa.FieldName = "AreaHa";
            this.colAreaHa.Name = "colAreaHa";
            this.colAreaHa.OptionsColumn.AllowEdit = false;
            this.colAreaHa.OptionsColumn.AllowFocus = false;
            this.colAreaHa.OptionsColumn.ReadOnly = true;
            // 
            // colReplantCount
            // 
            this.colReplantCount.Caption = "Replant Count";
            this.colReplantCount.FieldName = "ReplantCount";
            this.colReplantCount.Name = "colReplantCount";
            this.colReplantCount.OptionsColumn.AllowEdit = false;
            this.colReplantCount.OptionsColumn.AllowFocus = false;
            this.colReplantCount.OptionsColumn.ReadOnly = true;
            this.colReplantCount.Width = 106;
            // 
            // colSurveyDate
            // 
            this.colSurveyDate.Caption = "Survey Date";
            this.colSurveyDate.FieldName = "SurveyDate";
            this.colSurveyDate.Name = "colSurveyDate";
            this.colSurveyDate.OptionsColumn.AllowEdit = false;
            this.colSurveyDate.OptionsColumn.AllowFocus = false;
            this.colSurveyDate.OptionsColumn.ReadOnly = true;
            this.colSurveyDate.Width = 115;
            // 
            // colTreeType
            // 
            this.colTreeType.Caption = "Tree Type";
            this.colTreeType.FieldName = "TreeType";
            this.colTreeType.Name = "colTreeType";
            this.colTreeType.OptionsColumn.AllowEdit = false;
            this.colTreeType.OptionsColumn.AllowFocus = false;
            this.colTreeType.OptionsColumn.ReadOnly = true;
            this.colTreeType.Visible = true;
            this.colTreeType.VisibleIndex = 23;
            // 
            // colHedgeOwner
            // 
            this.colHedgeOwner.Caption = "Hedge Owner";
            this.colHedgeOwner.FieldName = "HedgeOwner";
            this.colHedgeOwner.Name = "colHedgeOwner";
            this.colHedgeOwner.OptionsColumn.AllowEdit = false;
            this.colHedgeOwner.OptionsColumn.AllowFocus = false;
            this.colHedgeOwner.OptionsColumn.ReadOnly = true;
            this.colHedgeOwner.Width = 115;
            // 
            // colHedgeLength
            // 
            this.colHedgeLength.Caption = "Hedge Length";
            this.colHedgeLength.FieldName = "HedgeLength";
            this.colHedgeLength.Name = "colHedgeLength";
            this.colHedgeLength.OptionsColumn.AllowEdit = false;
            this.colHedgeLength.OptionsColumn.AllowFocus = false;
            this.colHedgeLength.OptionsColumn.ReadOnly = true;
            this.colHedgeLength.Width = 126;
            // 
            // colGardenSize
            // 
            this.colGardenSize.Caption = "Garden Size";
            this.colGardenSize.FieldName = "GardenSize";
            this.colGardenSize.Name = "colGardenSize";
            this.colGardenSize.OptionsColumn.AllowEdit = false;
            this.colGardenSize.OptionsColumn.AllowFocus = false;
            this.colGardenSize.OptionsColumn.ReadOnly = true;
            this.colGardenSize.Width = 125;
            // 
            // colPropertyProximity
            // 
            this.colPropertyProximity.Caption = "Property Proximity";
            this.colPropertyProximity.FieldName = "PropertyProximity";
            this.colPropertyProximity.Name = "colPropertyProximity";
            this.colPropertyProximity.OptionsColumn.AllowEdit = false;
            this.colPropertyProximity.OptionsColumn.AllowFocus = false;
            this.colPropertyProximity.OptionsColumn.ReadOnly = true;
            this.colPropertyProximity.Width = 117;
            // 
            // colSiteLevel
            // 
            this.colSiteLevel.Caption = "Site Level";
            this.colSiteLevel.FieldName = "SiteLevel";
            this.colSiteLevel.Name = "colSiteLevel";
            this.colSiteLevel.OptionsColumn.AllowEdit = false;
            this.colSiteLevel.OptionsColumn.AllowFocus = false;
            this.colSiteLevel.OptionsColumn.ReadOnly = true;
            // 
            // colSituation
            // 
            this.colSituation.Caption = "Situation";
            this.colSituation.FieldName = "Situation";
            this.colSituation.Name = "colSituation";
            this.colSituation.OptionsColumn.AllowEdit = false;
            this.colSituation.OptionsColumn.AllowFocus = false;
            this.colSituation.OptionsColumn.ReadOnly = true;
            this.colSituation.Visible = true;
            this.colSituation.VisibleIndex = 6;
            this.colSituation.Width = 105;
            // 
            // colRiskFactor
            // 
            this.colRiskFactor.Caption = "Risk Factor";
            this.colRiskFactor.FieldName = "RiskFactor";
            this.colRiskFactor.Name = "colRiskFactor";
            this.colRiskFactor.OptionsColumn.AllowEdit = false;
            this.colRiskFactor.OptionsColumn.AllowFocus = false;
            this.colRiskFactor.OptionsColumn.ReadOnly = true;
            this.colRiskFactor.Visible = true;
            this.colRiskFactor.VisibleIndex = 1;
            // 
            // colPolygonXY
            // 
            this.colPolygonXY.Caption = "Polygon X/Y";
            this.colPolygonXY.FieldName = "PolygonXY";
            this.colPolygonXY.Name = "colPolygonXY";
            this.colPolygonXY.OptionsColumn.AllowEdit = false;
            this.colPolygonXY.OptionsColumn.AllowFocus = false;
            this.colPolygonXY.OptionsColumn.ReadOnly = true;
            // 
            // colcolor
            // 
            this.colcolor.Caption = "Color";
            this.colcolor.FieldName = "color";
            this.colcolor.Name = "colcolor";
            this.colcolor.OptionsColumn.AllowEdit = false;
            this.colcolor.OptionsColumn.AllowFocus = false;
            this.colcolor.OptionsColumn.ReadOnly = true;
            this.colcolor.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colLinkedInspectionCount
            // 
            this.colLinkedInspectionCount.Caption = "Inspection Count";
            this.colLinkedInspectionCount.FieldName = "LinkedInspectionCount";
            this.colLinkedInspectionCount.Name = "colLinkedInspectionCount";
            this.colLinkedInspectionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedInspectionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedInspectionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedInspectionCount.Visible = true;
            this.colLinkedInspectionCount.VisibleIndex = 26;
            this.colLinkedInspectionCount.Width = 104;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.OptionsColumn.AllowEdit = false;
            this.colOwnershipName.OptionsColumn.AllowFocus = false;
            this.colOwnershipName.OptionsColumn.ReadOnly = true;
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 25;
            this.colOwnershipName.Width = 103;
            // 
            // Calculated1
            // 
            this.Calculated1.Caption = "<b>Calculated 1</b>";
            this.Calculated1.FieldName = "Calculated1";
            this.Calculated1.Name = "Calculated1";
            this.Calculated1.OptionsColumn.AllowEdit = false;
            this.Calculated1.OptionsColumn.AllowFocus = false;
            this.Calculated1.OptionsColumn.ReadOnly = true;
            this.Calculated1.ShowUnboundExpressionMenu = true;
            this.Calculated1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1.Visible = true;
            this.Calculated1.VisibleIndex = 28;
            this.Calculated1.Width = 90;
            // 
            // Calculated2
            // 
            this.Calculated2.Caption = "<b>Calculated 2</b>";
            this.Calculated2.FieldName = "Calculated2";
            this.Calculated2.Name = "Calculated2";
            this.Calculated2.OptionsColumn.AllowEdit = false;
            this.Calculated2.OptionsColumn.AllowFocus = false;
            this.Calculated2.OptionsColumn.ReadOnly = true;
            this.Calculated2.ShowUnboundExpressionMenu = true;
            this.Calculated2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2.Visible = true;
            this.Calculated2.VisibleIndex = 29;
            this.Calculated2.Width = 90;
            // 
            // Calculated3
            // 
            this.Calculated3.Caption = "<b>Calculated 3</b>";
            this.Calculated3.FieldName = "Calculated3";
            this.Calculated3.Name = "Calculated3";
            this.Calculated3.OptionsColumn.AllowEdit = false;
            this.Calculated3.OptionsColumn.AllowFocus = false;
            this.Calculated3.OptionsColumn.ReadOnly = true;
            this.Calculated3.ShowUnboundExpressionMenu = true;
            this.Calculated3.Visible = true;
            this.Calculated3.VisibleIndex = 30;
            this.Calculated3.Width = 90;
            // 
            // colCavat
            // 
            this.colCavat.Caption = "Cavat";
            this.colCavat.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCavat.FieldName = "Cavat";
            this.colCavat.Name = "colCavat";
            this.colCavat.OptionsColumn.AllowEdit = false;
            this.colCavat.OptionsColumn.AllowFocus = false;
            this.colCavat.OptionsColumn.ReadOnly = true;
            this.colCavat.Width = 50;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colStemCount
            // 
            this.colStemCount.Caption = "Stem Count";
            this.colStemCount.FieldName = "StemCount";
            this.colStemCount.Name = "colStemCount";
            this.colStemCount.OptionsColumn.AllowEdit = false;
            this.colStemCount.OptionsColumn.AllowFocus = false;
            this.colStemCount.OptionsColumn.ReadOnly = true;
            this.colStemCount.Width = 77;
            // 
            // colCrownNorth
            // 
            this.colCrownNorth.Caption = "Crown North";
            this.colCrownNorth.FieldName = "CrownNorth";
            this.colCrownNorth.Name = "colCrownNorth";
            this.colCrownNorth.OptionsColumn.AllowEdit = false;
            this.colCrownNorth.OptionsColumn.AllowFocus = false;
            this.colCrownNorth.OptionsColumn.ReadOnly = true;
            this.colCrownNorth.Width = 82;
            // 
            // colCrownSouth
            // 
            this.colCrownSouth.Caption = "Crown South";
            this.colCrownSouth.FieldName = "CrownSouth";
            this.colCrownSouth.Name = "colCrownSouth";
            this.colCrownSouth.OptionsColumn.AllowEdit = false;
            this.colCrownSouth.OptionsColumn.AllowFocus = false;
            this.colCrownSouth.OptionsColumn.ReadOnly = true;
            this.colCrownSouth.Width = 83;
            // 
            // colCrownEast
            // 
            this.colCrownEast.Caption = "Crown East";
            this.colCrownEast.FieldName = "CrownEast";
            this.colCrownEast.Name = "colCrownEast";
            this.colCrownEast.OptionsColumn.AllowEdit = false;
            this.colCrownEast.OptionsColumn.AllowFocus = false;
            this.colCrownEast.OptionsColumn.ReadOnly = true;
            this.colCrownEast.Width = 76;
            // 
            // colCrownWest
            // 
            this.colCrownWest.Caption = "Crown West";
            this.colCrownWest.FieldName = "CrownWest";
            this.colCrownWest.Name = "colCrownWest";
            this.colCrownWest.OptionsColumn.AllowEdit = false;
            this.colCrownWest.OptionsColumn.AllowFocus = false;
            this.colCrownWest.OptionsColumn.ReadOnly = true;
            this.colCrownWest.Width = 80;
            // 
            // colRetentionCategory
            // 
            this.colRetentionCategory.Caption = "Retention Category";
            this.colRetentionCategory.FieldName = "RetentionCategory";
            this.colRetentionCategory.Name = "colRetentionCategory";
            this.colRetentionCategory.OptionsColumn.AllowEdit = false;
            this.colRetentionCategory.OptionsColumn.AllowFocus = false;
            this.colRetentionCategory.OptionsColumn.ReadOnly = true;
            this.colRetentionCategory.Width = 116;
            // 
            // colSULE
            // 
            this.colSULE.Caption = "SULE";
            this.colSULE.FieldName = "SULE";
            this.colSULE.Name = "colSULE";
            this.colSULE.OptionsColumn.AllowEdit = false;
            this.colSULE.OptionsColumn.AllowFocus = false;
            this.colSULE.OptionsColumn.ReadOnly = true;
            this.colSULE.Width = 45;
            // 
            // colUserPicklist1
            // 
            this.colUserPicklist1.Caption = "User Picklist 1";
            this.colUserPicklist1.FieldName = "UserPicklist1";
            this.colUserPicklist1.Name = "colUserPicklist1";
            this.colUserPicklist1.OptionsColumn.AllowEdit = false;
            this.colUserPicklist1.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist2
            // 
            this.colUserPicklist2.Caption = "User Picklist 2";
            this.colUserPicklist2.FieldName = "UserPicklist2";
            this.colUserPicklist2.Name = "colUserPicklist2";
            this.colUserPicklist2.OptionsColumn.AllowEdit = false;
            this.colUserPicklist2.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2.OptionsColumn.ReadOnly = true;
            this.colUserPicklist2.Width = 86;
            // 
            // colUserPicklist3
            // 
            this.colUserPicklist3.Caption = "User Picklist 3";
            this.colUserPicklist3.FieldName = "UserPicklist3";
            this.colUserPicklist3.Name = "colUserPicklist3";
            this.colUserPicklist3.OptionsColumn.AllowEdit = false;
            this.colUserPicklist3.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3.OptionsColumn.ReadOnly = true;
            this.colUserPicklist3.Width = 86;
            // 
            // colSpeciesVariety
            // 
            this.colSpeciesVariety.Caption = "Species Variety";
            this.colSpeciesVariety.FieldName = "SpeciesVariety";
            this.colSpeciesVariety.Name = "colSpeciesVariety";
            this.colSpeciesVariety.OptionsColumn.AllowEdit = false;
            this.colSpeciesVariety.OptionsColumn.AllowFocus = false;
            this.colSpeciesVariety.OptionsColumn.ReadOnly = true;
            this.colSpeciesVariety.Width = 94;
            // 
            // colObjectLength
            // 
            this.colObjectLength.Caption = "Object Length";
            this.colObjectLength.FieldName = "ObjectLength";
            this.colObjectLength.Name = "colObjectLength";
            this.colObjectLength.OptionsColumn.AllowEdit = false;
            this.colObjectLength.OptionsColumn.AllowFocus = false;
            this.colObjectLength.OptionsColumn.ReadOnly = true;
            this.colObjectLength.Width = 89;
            // 
            // colObjectWidth
            // 
            this.colObjectWidth.Caption = "Object Width";
            this.colObjectWidth.FieldName = "ObjectWidth";
            this.colObjectWidth.Name = "colObjectWidth";
            this.colObjectWidth.OptionsColumn.AllowEdit = false;
            this.colObjectWidth.OptionsColumn.AllowFocus = false;
            this.colObjectWidth.OptionsColumn.ReadOnly = true;
            this.colObjectWidth.Width = 84;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem1});
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlGroup3.Size = new System.Drawing.Size(324, 765);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridSplitContainer1;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(314, 729);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnLoadTrees;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(225, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(89, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonEditClientFilter;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(225, 26);
            this.layoutControlItem1.Text = "Client:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(31, 13);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.layoutControl4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(324, 765);
            this.xtraTabPage2.Text = "Inspections";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.buttonEditClientFilter2);
            this.layoutControl4.Controls.Add(this.gridSplitContainer2);
            this.layoutControl4.Controls.Add(this.btnLoadInspections);
            this.layoutControl4.Controls.Add(this.ceLastInspectionOnly);
            this.layoutControl4.Controls.Add(this.deInspectionToDate);
            this.layoutControl4.Controls.Add(this.deInspectionFromDate);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.MenuManager = this.barManager1;
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1132, 164, 250, 350);
            this.layoutControl4.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(324, 765);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // buttonEditClientFilter2
            // 
            this.buttonEditClientFilter2.Location = new System.Drawing.Point(41, 31);
            this.buttonEditClientFilter2.MenuManager = this.barManager1;
            this.buttonEditClientFilter2.Name = "buttonEditClientFilter2";
            this.buttonEditClientFilter2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter2.Size = new System.Drawing.Size(276, 20);
            this.buttonEditClientFilter2.StyleController = this.layoutControl4;
            this.buttonEditClientFilter2.TabIndex = 9;
            this.buttonEditClientFilter2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter2_ButtonClick);
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(7, 81);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(310, 677);
            this.gridSplitContainer2.TabIndex = 5;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp01230ATReportsListingsInspectionFilterBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl2.Size = new System.Drawing.Size(310, 677);
            this.gridControl2.TabIndex = 10;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp01230ATReportsListingsInspectionFilterBindingSource
            // 
            this.sp01230ATReportsListingsInspectionFilterBindingSource.DataMember = "sp01230_AT_Reports_Listings_Inspection_Filter";
            this.sp01230ATReportsListingsInspectionFilterBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID,
            this.colTreeID1,
            this.colSiteID1,
            this.colClientID2,
            this.colClientName2,
            this.colSiteName1,
            this.colTreeReference1,
            this.colTreeMappingID,
            this.colTreeGridReference,
            this.colTreeDistance,
            this.colTreeHouseName,
            this.colTreePostcode,
            this.colInspectionReference,
            this.colInspectorName,
            this.colInspectionDate,
            this.colIncidentReferenceNumber,
            this.colIncidentID,
            this.colStemPhysical1,
            this.colStemPhysical2,
            this.colStemPhysical3,
            this.colStemDisease1,
            this.colStemDisease2,
            this.colStemDisease3,
            this.colAngleToVertical,
            this.colCrownPhysical1,
            this.colCrownPhysical2,
            this.colCrownPhysical3,
            this.colCrownDisease1,
            this.colCrownDisease2,
            this.colCrownDisease3,
            this.colCrownFoliation1,
            this.colCrownFoliation2,
            this.colCrownFoliation3,
            this.colRiskCategory1,
            this.colGeneralCondition,
            this.colRootHeave1,
            this.colRootHeave2,
            this.colRootHeave3,
            this.colDateLastModified,
            this.colRemarks1,
            this.colUser11,
            this.colUser21,
            this.colUser31,
            this.colcolor1,
            this.colLinkedActionCount,
            this.colLinkedOutstandingActionCount,
            this.colNoFurtherActionRequired,
            this.Calculated1b,
            this.Calculated2b,
            this.Calculated3b,
            this.colUserPicklist11,
            this.colUserPicklist21,
            this.colUserPicklist31});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedOutstandingActionCount, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseDown);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            this.colInspectionID.Width = 86;
            // 
            // colTreeID1
            // 
            this.colTreeID1.Caption = "Tree ID";
            this.colTreeID1.FieldName = "TreeID";
            this.colTreeID1.Name = "colTreeID1";
            this.colTreeID1.OptionsColumn.AllowEdit = false;
            this.colTreeID1.OptionsColumn.AllowFocus = false;
            this.colTreeID1.OptionsColumn.ReadOnly = true;
            this.colTreeID1.Width = 58;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Width = 72;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 69;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 85;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Width = 88;
            // 
            // colTreeReference1
            // 
            this.colTreeReference1.Caption = "Tree Reference";
            this.colTreeReference1.FieldName = "TreeReference";
            this.colTreeReference1.Name = "colTreeReference1";
            this.colTreeReference1.OptionsColumn.AllowEdit = false;
            this.colTreeReference1.OptionsColumn.AllowFocus = false;
            this.colTreeReference1.OptionsColumn.ReadOnly = true;
            this.colTreeReference1.Visible = true;
            this.colTreeReference1.VisibleIndex = 0;
            this.colTreeReference1.Width = 110;
            // 
            // colTreeMappingID
            // 
            this.colTreeMappingID.Caption = "Tree Mapping ID";
            this.colTreeMappingID.FieldName = "TreeMappingID";
            this.colTreeMappingID.Name = "colTreeMappingID";
            this.colTreeMappingID.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID.Width = 101;
            // 
            // colTreeGridReference
            // 
            this.colTreeGridReference.Caption = "Tree Grid Reference";
            this.colTreeGridReference.FieldName = "TreeGridReference";
            this.colTreeGridReference.Name = "colTreeGridReference";
            this.colTreeGridReference.OptionsColumn.AllowEdit = false;
            this.colTreeGridReference.OptionsColumn.AllowFocus = false;
            this.colTreeGridReference.OptionsColumn.ReadOnly = true;
            this.colTreeGridReference.Width = 119;
            // 
            // colTreeDistance
            // 
            this.colTreeDistance.Caption = "Tree Distance";
            this.colTreeDistance.FieldName = "TreeDistance";
            this.colTreeDistance.Name = "colTreeDistance";
            this.colTreeDistance.OptionsColumn.AllowEdit = false;
            this.colTreeDistance.OptionsColumn.AllowFocus = false;
            this.colTreeDistance.OptionsColumn.ReadOnly = true;
            this.colTreeDistance.Width = 88;
            // 
            // colTreeHouseName
            // 
            this.colTreeHouseName.Caption = "Tree House Name";
            this.colTreeHouseName.FieldName = "TreeHouseName";
            this.colTreeHouseName.Name = "colTreeHouseName";
            this.colTreeHouseName.OptionsColumn.AllowEdit = false;
            this.colTreeHouseName.OptionsColumn.AllowFocus = false;
            this.colTreeHouseName.OptionsColumn.ReadOnly = true;
            this.colTreeHouseName.Visible = true;
            this.colTreeHouseName.VisibleIndex = 1;
            this.colTreeHouseName.Width = 107;
            // 
            // colTreePostcode
            // 
            this.colTreePostcode.Caption = "Tree Postcode";
            this.colTreePostcode.FieldName = "TreePostcode";
            this.colTreePostcode.Name = "colTreePostcode";
            this.colTreePostcode.OptionsColumn.AllowEdit = false;
            this.colTreePostcode.OptionsColumn.AllowFocus = false;
            this.colTreePostcode.OptionsColumn.ReadOnly = true;
            this.colTreePostcode.Visible = true;
            this.colTreePostcode.VisibleIndex = 2;
            this.colTreePostcode.Width = 91;
            // 
            // colInspectionReference
            // 
            this.colInspectionReference.Caption = "Inspection Reference";
            this.colInspectionReference.FieldName = "InspectionReference";
            this.colInspectionReference.Name = "colInspectionReference";
            this.colInspectionReference.OptionsColumn.AllowEdit = false;
            this.colInspectionReference.OptionsColumn.AllowFocus = false;
            this.colInspectionReference.OptionsColumn.ReadOnly = true;
            this.colInspectionReference.Visible = true;
            this.colInspectionReference.VisibleIndex = 3;
            this.colInspectionReference.Width = 125;
            // 
            // colInspectorName
            // 
            this.colInspectorName.Caption = "Inspector Name";
            this.colInspectorName.FieldName = "InspectorName";
            this.colInspectorName.Name = "colInspectorName";
            this.colInspectorName.OptionsColumn.AllowEdit = false;
            this.colInspectorName.OptionsColumn.AllowFocus = false;
            this.colInspectorName.OptionsColumn.ReadOnly = true;
            this.colInspectorName.Visible = true;
            this.colInspectorName.VisibleIndex = 4;
            this.colInspectorName.Width = 98;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Visible = true;
            this.colInspectionDate.VisibleIndex = 5;
            this.colInspectionDate.Width = 111;
            // 
            // colIncidentReferenceNumber
            // 
            this.colIncidentReferenceNumber.Caption = "Incident Ref No";
            this.colIncidentReferenceNumber.FieldName = "IncidentReferenceNumber";
            this.colIncidentReferenceNumber.Name = "colIncidentReferenceNumber";
            this.colIncidentReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colIncidentReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colIncidentReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colIncidentReferenceNumber.Visible = true;
            this.colIncidentReferenceNumber.VisibleIndex = 7;
            this.colIncidentReferenceNumber.Width = 97;
            // 
            // colIncidentID
            // 
            this.colIncidentID.Caption = "Incident ID";
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            // 
            // colStemPhysical1
            // 
            this.colStemPhysical1.Caption = "Stem Physical 1";
            this.colStemPhysical1.FieldName = "StemPhysical1";
            this.colStemPhysical1.Name = "colStemPhysical1";
            this.colStemPhysical1.OptionsColumn.AllowEdit = false;
            this.colStemPhysical1.OptionsColumn.AllowFocus = false;
            this.colStemPhysical1.OptionsColumn.ReadOnly = true;
            this.colStemPhysical1.Visible = true;
            this.colStemPhysical1.VisibleIndex = 9;
            this.colStemPhysical1.Width = 96;
            // 
            // colStemPhysical2
            // 
            this.colStemPhysical2.Caption = "Stem Physical 2";
            this.colStemPhysical2.FieldName = "StemPhysical2";
            this.colStemPhysical2.Name = "colStemPhysical2";
            this.colStemPhysical2.OptionsColumn.AllowEdit = false;
            this.colStemPhysical2.OptionsColumn.AllowFocus = false;
            this.colStemPhysical2.OptionsColumn.ReadOnly = true;
            this.colStemPhysical2.Visible = true;
            this.colStemPhysical2.VisibleIndex = 10;
            this.colStemPhysical2.Width = 96;
            // 
            // colStemPhysical3
            // 
            this.colStemPhysical3.Caption = "Stem Physical 3";
            this.colStemPhysical3.FieldName = "StemPhysical3";
            this.colStemPhysical3.Name = "colStemPhysical3";
            this.colStemPhysical3.OptionsColumn.AllowEdit = false;
            this.colStemPhysical3.OptionsColumn.AllowFocus = false;
            this.colStemPhysical3.OptionsColumn.ReadOnly = true;
            this.colStemPhysical3.Visible = true;
            this.colStemPhysical3.VisibleIndex = 11;
            this.colStemPhysical3.Width = 96;
            // 
            // colStemDisease1
            // 
            this.colStemDisease1.Caption = "Stem Disease 1";
            this.colStemDisease1.FieldName = "StemDisease1";
            this.colStemDisease1.Name = "colStemDisease1";
            this.colStemDisease1.OptionsColumn.AllowEdit = false;
            this.colStemDisease1.OptionsColumn.AllowFocus = false;
            this.colStemDisease1.OptionsColumn.ReadOnly = true;
            this.colStemDisease1.Visible = true;
            this.colStemDisease1.VisibleIndex = 12;
            this.colStemDisease1.Width = 95;
            // 
            // colStemDisease2
            // 
            this.colStemDisease2.Caption = "Stem Disease 2";
            this.colStemDisease2.FieldName = "StemDisease2";
            this.colStemDisease2.Name = "colStemDisease2";
            this.colStemDisease2.OptionsColumn.AllowEdit = false;
            this.colStemDisease2.OptionsColumn.AllowFocus = false;
            this.colStemDisease2.OptionsColumn.ReadOnly = true;
            this.colStemDisease2.Visible = true;
            this.colStemDisease2.VisibleIndex = 13;
            this.colStemDisease2.Width = 95;
            // 
            // colStemDisease3
            // 
            this.colStemDisease3.Caption = "Stem Disease 3";
            this.colStemDisease3.FieldName = "StemDisease3";
            this.colStemDisease3.Name = "colStemDisease3";
            this.colStemDisease3.OptionsColumn.AllowEdit = false;
            this.colStemDisease3.OptionsColumn.AllowFocus = false;
            this.colStemDisease3.OptionsColumn.ReadOnly = true;
            this.colStemDisease3.Visible = true;
            this.colStemDisease3.VisibleIndex = 14;
            this.colStemDisease3.Width = 95;
            // 
            // colAngleToVertical
            // 
            this.colAngleToVertical.Caption = "Angle To Vertical";
            this.colAngleToVertical.FieldName = "AngleToVertical";
            this.colAngleToVertical.Name = "colAngleToVertical";
            this.colAngleToVertical.OptionsColumn.AllowEdit = false;
            this.colAngleToVertical.OptionsColumn.AllowFocus = false;
            this.colAngleToVertical.OptionsColumn.ReadOnly = true;
            this.colAngleToVertical.Visible = true;
            this.colAngleToVertical.VisibleIndex = 15;
            this.colAngleToVertical.Width = 102;
            // 
            // colCrownPhysical1
            // 
            this.colCrownPhysical1.Caption = "Crown Physical 1";
            this.colCrownPhysical1.FieldName = "CrownPhysical1";
            this.colCrownPhysical1.Name = "colCrownPhysical1";
            this.colCrownPhysical1.OptionsColumn.AllowEdit = false;
            this.colCrownPhysical1.OptionsColumn.AllowFocus = false;
            this.colCrownPhysical1.OptionsColumn.ReadOnly = true;
            this.colCrownPhysical1.Visible = true;
            this.colCrownPhysical1.VisibleIndex = 16;
            this.colCrownPhysical1.Width = 103;
            // 
            // colCrownPhysical2
            // 
            this.colCrownPhysical2.Caption = "Crown Physical 2";
            this.colCrownPhysical2.FieldName = "CrownPhysical2";
            this.colCrownPhysical2.Name = "colCrownPhysical2";
            this.colCrownPhysical2.OptionsColumn.AllowEdit = false;
            this.colCrownPhysical2.OptionsColumn.AllowFocus = false;
            this.colCrownPhysical2.OptionsColumn.ReadOnly = true;
            this.colCrownPhysical2.Visible = true;
            this.colCrownPhysical2.VisibleIndex = 17;
            this.colCrownPhysical2.Width = 103;
            // 
            // colCrownPhysical3
            // 
            this.colCrownPhysical3.Caption = "Crown Physical 3";
            this.colCrownPhysical3.FieldName = "CrownPhysical3";
            this.colCrownPhysical3.Name = "colCrownPhysical3";
            this.colCrownPhysical3.OptionsColumn.AllowEdit = false;
            this.colCrownPhysical3.OptionsColumn.AllowFocus = false;
            this.colCrownPhysical3.OptionsColumn.ReadOnly = true;
            this.colCrownPhysical3.Visible = true;
            this.colCrownPhysical3.VisibleIndex = 18;
            this.colCrownPhysical3.Width = 103;
            // 
            // colCrownDisease1
            // 
            this.colCrownDisease1.Caption = "Crown Disease 1";
            this.colCrownDisease1.FieldName = "CrownDisease1";
            this.colCrownDisease1.Name = "colCrownDisease1";
            this.colCrownDisease1.OptionsColumn.AllowEdit = false;
            this.colCrownDisease1.OptionsColumn.AllowFocus = false;
            this.colCrownDisease1.OptionsColumn.ReadOnly = true;
            this.colCrownDisease1.Visible = true;
            this.colCrownDisease1.VisibleIndex = 19;
            this.colCrownDisease1.Width = 102;
            // 
            // colCrownDisease2
            // 
            this.colCrownDisease2.Caption = "Crown Disease 2";
            this.colCrownDisease2.FieldName = "CrownDisease2";
            this.colCrownDisease2.Name = "colCrownDisease2";
            this.colCrownDisease2.OptionsColumn.AllowEdit = false;
            this.colCrownDisease2.OptionsColumn.AllowFocus = false;
            this.colCrownDisease2.OptionsColumn.ReadOnly = true;
            this.colCrownDisease2.Visible = true;
            this.colCrownDisease2.VisibleIndex = 20;
            this.colCrownDisease2.Width = 102;
            // 
            // colCrownDisease3
            // 
            this.colCrownDisease3.Caption = "Crown Disease 3";
            this.colCrownDisease3.FieldName = "CrownDisease3";
            this.colCrownDisease3.Name = "colCrownDisease3";
            this.colCrownDisease3.OptionsColumn.AllowEdit = false;
            this.colCrownDisease3.OptionsColumn.AllowFocus = false;
            this.colCrownDisease3.OptionsColumn.ReadOnly = true;
            this.colCrownDisease3.Visible = true;
            this.colCrownDisease3.VisibleIndex = 21;
            this.colCrownDisease3.Width = 102;
            // 
            // colCrownFoliation1
            // 
            this.colCrownFoliation1.Caption = "Crown Foliation 1";
            this.colCrownFoliation1.FieldName = "CrownFoliation1";
            this.colCrownFoliation1.Name = "colCrownFoliation1";
            this.colCrownFoliation1.OptionsColumn.AllowEdit = false;
            this.colCrownFoliation1.OptionsColumn.AllowFocus = false;
            this.colCrownFoliation1.OptionsColumn.ReadOnly = true;
            this.colCrownFoliation1.Visible = true;
            this.colCrownFoliation1.VisibleIndex = 22;
            this.colCrownFoliation1.Width = 105;
            // 
            // colCrownFoliation2
            // 
            this.colCrownFoliation2.Caption = "Crown Foliation 2";
            this.colCrownFoliation2.FieldName = "CrownFoliation2";
            this.colCrownFoliation2.Name = "colCrownFoliation2";
            this.colCrownFoliation2.OptionsColumn.AllowEdit = false;
            this.colCrownFoliation2.OptionsColumn.AllowFocus = false;
            this.colCrownFoliation2.OptionsColumn.ReadOnly = true;
            this.colCrownFoliation2.Visible = true;
            this.colCrownFoliation2.VisibleIndex = 23;
            this.colCrownFoliation2.Width = 105;
            // 
            // colCrownFoliation3
            // 
            this.colCrownFoliation3.Caption = "Crown Foliation 3";
            this.colCrownFoliation3.FieldName = "CrownFoliation3";
            this.colCrownFoliation3.Name = "colCrownFoliation3";
            this.colCrownFoliation3.OptionsColumn.AllowEdit = false;
            this.colCrownFoliation3.OptionsColumn.AllowFocus = false;
            this.colCrownFoliation3.OptionsColumn.ReadOnly = true;
            this.colCrownFoliation3.Visible = true;
            this.colCrownFoliation3.VisibleIndex = 24;
            this.colCrownFoliation3.Width = 105;
            // 
            // colRiskCategory1
            // 
            this.colRiskCategory1.Caption = "Risk Category";
            this.colRiskCategory1.FieldName = "RiskCategory";
            this.colRiskCategory1.Name = "colRiskCategory1";
            this.colRiskCategory1.OptionsColumn.AllowEdit = false;
            this.colRiskCategory1.OptionsColumn.AllowFocus = false;
            this.colRiskCategory1.OptionsColumn.ReadOnly = true;
            this.colRiskCategory1.Visible = true;
            this.colRiskCategory1.VisibleIndex = 6;
            this.colRiskCategory1.Width = 89;
            // 
            // colGeneralCondition
            // 
            this.colGeneralCondition.Caption = "General Condition";
            this.colGeneralCondition.FieldName = "GeneralCondition";
            this.colGeneralCondition.Name = "colGeneralCondition";
            this.colGeneralCondition.OptionsColumn.AllowEdit = false;
            this.colGeneralCondition.OptionsColumn.AllowFocus = false;
            this.colGeneralCondition.OptionsColumn.ReadOnly = true;
            this.colGeneralCondition.Visible = true;
            this.colGeneralCondition.VisibleIndex = 8;
            this.colGeneralCondition.Width = 107;
            // 
            // colRootHeave1
            // 
            this.colRootHeave1.Caption = "Root Heave 1";
            this.colRootHeave1.FieldName = "RootHeave1";
            this.colRootHeave1.Name = "colRootHeave1";
            this.colRootHeave1.OptionsColumn.AllowEdit = false;
            this.colRootHeave1.OptionsColumn.AllowFocus = false;
            this.colRootHeave1.OptionsColumn.ReadOnly = true;
            this.colRootHeave1.Visible = true;
            this.colRootHeave1.VisibleIndex = 25;
            this.colRootHeave1.Width = 88;
            // 
            // colRootHeave2
            // 
            this.colRootHeave2.Caption = "Root Heave 2";
            this.colRootHeave2.FieldName = "RootHeave2";
            this.colRootHeave2.Name = "colRootHeave2";
            this.colRootHeave2.OptionsColumn.AllowEdit = false;
            this.colRootHeave2.OptionsColumn.AllowFocus = false;
            this.colRootHeave2.OptionsColumn.ReadOnly = true;
            this.colRootHeave2.Visible = true;
            this.colRootHeave2.VisibleIndex = 26;
            this.colRootHeave2.Width = 88;
            // 
            // colRootHeave3
            // 
            this.colRootHeave3.Caption = "Root Heave 3";
            this.colRootHeave3.FieldName = "RootHeave3";
            this.colRootHeave3.Name = "colRootHeave3";
            this.colRootHeave3.OptionsColumn.AllowEdit = false;
            this.colRootHeave3.OptionsColumn.AllowFocus = false;
            this.colRootHeave3.OptionsColumn.ReadOnly = true;
            this.colRootHeave3.Visible = true;
            this.colRootHeave3.VisibleIndex = 27;
            this.colRootHeave3.Width = 88;
            // 
            // colDateLastModified
            // 
            this.colDateLastModified.Caption = "Last Modified";
            this.colDateLastModified.FieldName = "DateLastModified";
            this.colDateLastModified.Name = "colDateLastModified";
            this.colDateLastModified.OptionsColumn.AllowEdit = false;
            this.colDateLastModified.OptionsColumn.AllowFocus = false;
            this.colDateLastModified.OptionsColumn.ReadOnly = true;
            this.colDateLastModified.Width = 85;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 34;
            this.colRemarks1.Width = 63;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            // 
            // colUser11
            // 
            this.colUser11.Caption = "User 1";
            this.colUser11.FieldName = "User1";
            this.colUser11.Name = "colUser11";
            this.colUser11.OptionsColumn.AllowEdit = false;
            this.colUser11.OptionsColumn.AllowFocus = false;
            this.colUser11.OptionsColumn.ReadOnly = true;
            this.colUser11.Visible = true;
            this.colUser11.VisibleIndex = 28;
            this.colUser11.Width = 53;
            // 
            // colUser21
            // 
            this.colUser21.Caption = "User 2";
            this.colUser21.FieldName = "User2";
            this.colUser21.Name = "colUser21";
            this.colUser21.OptionsColumn.AllowEdit = false;
            this.colUser21.OptionsColumn.AllowFocus = false;
            this.colUser21.OptionsColumn.ReadOnly = true;
            this.colUser21.Visible = true;
            this.colUser21.VisibleIndex = 29;
            this.colUser21.Width = 53;
            // 
            // colUser31
            // 
            this.colUser31.Caption = "User 3";
            this.colUser31.FieldName = "User3";
            this.colUser31.Name = "colUser31";
            this.colUser31.OptionsColumn.AllowEdit = false;
            this.colUser31.OptionsColumn.AllowFocus = false;
            this.colUser31.OptionsColumn.ReadOnly = true;
            this.colUser31.Visible = true;
            this.colUser31.VisibleIndex = 30;
            this.colUser31.Width = 53;
            // 
            // colcolor1
            // 
            this.colcolor1.Caption = "color";
            this.colcolor1.FieldName = "color";
            this.colcolor1.Name = "colcolor1";
            this.colcolor1.OptionsColumn.AllowEdit = false;
            this.colcolor1.OptionsColumn.AllowFocus = false;
            this.colcolor1.OptionsColumn.ReadOnly = true;
            this.colcolor1.OptionsColumn.ShowInCustomizationForm = false;
            this.colcolor1.Width = 35;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Action Count";
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 31;
            this.colLinkedActionCount.Width = 84;
            // 
            // colLinkedOutstandingActionCount
            // 
            this.colLinkedOutstandingActionCount.Caption = "Outstanding Action Count";
            this.colLinkedOutstandingActionCount.FieldName = "LinkedOutstandingActionCount";
            this.colLinkedOutstandingActionCount.Name = "colLinkedOutstandingActionCount";
            this.colLinkedOutstandingActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedOutstandingActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedOutstandingActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedOutstandingActionCount.Visible = true;
            this.colLinkedOutstandingActionCount.VisibleIndex = 32;
            this.colLinkedOutstandingActionCount.Width = 159;
            // 
            // colNoFurtherActionRequired
            // 
            this.colNoFurtherActionRequired.FieldName = "NoFurtherActionRequired";
            this.colNoFurtherActionRequired.Name = "colNoFurtherActionRequired";
            this.colNoFurtherActionRequired.OptionsColumn.AllowEdit = false;
            this.colNoFurtherActionRequired.OptionsColumn.AllowFocus = false;
            this.colNoFurtherActionRequired.OptionsColumn.ReadOnly = true;
            this.colNoFurtherActionRequired.Visible = true;
            this.colNoFurtherActionRequired.VisibleIndex = 33;
            this.colNoFurtherActionRequired.Width = 153;
            // 
            // Calculated1b
            // 
            this.Calculated1b.Caption = "<b>Calculated 1</b>";
            this.Calculated1b.FieldName = "Calculated1";
            this.Calculated1b.Name = "Calculated1b";
            this.Calculated1b.OptionsColumn.AllowEdit = false;
            this.Calculated1b.OptionsColumn.AllowFocus = false;
            this.Calculated1b.OptionsColumn.ReadOnly = true;
            this.Calculated1b.ShowUnboundExpressionMenu = true;
            this.Calculated1b.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1b.Visible = true;
            this.Calculated1b.VisibleIndex = 35;
            this.Calculated1b.Width = 90;
            // 
            // Calculated2b
            // 
            this.Calculated2b.Caption = "<b>Calculated 2</b>";
            this.Calculated2b.FieldName = "Calculated2";
            this.Calculated2b.Name = "Calculated2b";
            this.Calculated2b.OptionsColumn.AllowEdit = false;
            this.Calculated2b.OptionsColumn.AllowFocus = false;
            this.Calculated2b.OptionsColumn.ReadOnly = true;
            this.Calculated2b.ShowUnboundExpressionMenu = true;
            this.Calculated2b.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2b.Visible = true;
            this.Calculated2b.VisibleIndex = 36;
            this.Calculated2b.Width = 90;
            // 
            // Calculated3b
            // 
            this.Calculated3b.Caption = "<b>Calculated 3</b>";
            this.Calculated3b.FieldName = "Calculated3";
            this.Calculated3b.Name = "Calculated3b";
            this.Calculated3b.OptionsColumn.AllowEdit = false;
            this.Calculated3b.OptionsColumn.AllowFocus = false;
            this.Calculated3b.OptionsColumn.ReadOnly = true;
            this.Calculated3b.ShowUnboundExpressionMenu = true;
            this.Calculated3b.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3b.Visible = true;
            this.Calculated3b.VisibleIndex = 37;
            this.Calculated3b.Width = 90;
            // 
            // colUserPicklist11
            // 
            this.colUserPicklist11.Caption = "User Picklist 1";
            this.colUserPicklist11.FieldName = "UserPicklist1";
            this.colUserPicklist11.Name = "colUserPicklist11";
            this.colUserPicklist11.OptionsColumn.AllowEdit = false;
            this.colUserPicklist11.OptionsColumn.AllowFocus = false;
            this.colUserPicklist11.OptionsColumn.ReadOnly = true;
            this.colUserPicklist11.Width = 86;
            // 
            // colUserPicklist21
            // 
            this.colUserPicklist21.Caption = "User Picklist 2";
            this.colUserPicklist21.FieldName = "UserPicklist2";
            this.colUserPicklist21.Name = "colUserPicklist21";
            this.colUserPicklist21.OptionsColumn.AllowEdit = false;
            this.colUserPicklist21.OptionsColumn.AllowFocus = false;
            this.colUserPicklist21.OptionsColumn.ReadOnly = true;
            this.colUserPicklist21.Width = 86;
            // 
            // colUserPicklist31
            // 
            this.colUserPicklist31.Caption = "User Picklist 3";
            this.colUserPicklist31.FieldName = "UserPicklist3";
            this.colUserPicklist31.Name = "colUserPicklist31";
            this.colUserPicklist31.OptionsColumn.AllowEdit = false;
            this.colUserPicklist31.OptionsColumn.AllowFocus = false;
            this.colUserPicklist31.OptionsColumn.ReadOnly = true;
            this.colUserPicklist31.Width = 86;
            // 
            // btnLoadInspections
            // 
            this.btnLoadInspections.ImageOptions.ImageIndex = 0;
            this.btnLoadInspections.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadInspections.Location = new System.Drawing.Point(209, 55);
            this.btnLoadInspections.Name = "btnLoadInspections";
            this.btnLoadInspections.Size = new System.Drawing.Size(108, 22);
            this.btnLoadInspections.StyleController = this.layoutControl4;
            this.btnLoadInspections.TabIndex = 4;
            this.btnLoadInspections.Text = "Load Inspections";
            this.btnLoadInspections.Click += new System.EventHandler(this.btnLoadInspections_Click);
            // 
            // ceLastInspectionOnly
            // 
            this.ceLastInspectionOnly.EditValue = true;
            this.ceLastInspectionOnly.Location = new System.Drawing.Point(7, 55);
            this.ceLastInspectionOnly.Name = "ceLastInspectionOnly";
            this.ceLastInspectionOnly.Properties.Caption = "Last Inspection for Each Tree Only";
            this.ceLastInspectionOnly.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.ceLastInspectionOnly.Size = new System.Drawing.Size(198, 19);
            this.ceLastInspectionOnly.StyleController = this.layoutControl4;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Information - Only show last Inspection tick box";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Tick to return only one inspection (last recorded) per tree.\r\nLeave unticked to r" +
    "eturn all inspections for each tree.";
            toolTipTitleItem2.LeftIndent = 6;
            toolTipTitleItem2.Text = "Trees without an inspection are NOT shown.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipSeparatorItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.ceLastInspectionOnly.SuperTip = superToolTip1;
            this.ceLastInspectionOnly.TabIndex = 3;
            // 
            // deInspectionToDate
            // 
            this.deInspectionToDate.EditValue = null;
            this.deInspectionToDate.Location = new System.Drawing.Point(214, 7);
            this.deInspectionToDate.Name = "deInspectionToDate";
            this.deInspectionToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deInspectionToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deInspectionToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deInspectionToDate.Properties.NullText = "Not Used";
            this.deInspectionToDate.Size = new System.Drawing.Size(103, 20);
            this.deInspectionToDate.StyleController = this.layoutControl4;
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem3.Text = "Information - To Date";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "All inspections with an <b>Inspection Date</b> which fall between the From Date a" +
    "nd To Date will be returned.";
            superToolTip2.Items.Add(toolTipTitleItem3);
            superToolTip2.Items.Add(toolTipItem2);
            this.deInspectionToDate.SuperTip = superToolTip2;
            this.deInspectionToDate.TabIndex = 1;
            // 
            // deInspectionFromDate
            // 
            this.deInspectionFromDate.EditValue = null;
            this.deInspectionFromDate.Location = new System.Drawing.Point(64, 7);
            this.deInspectionFromDate.Name = "deInspectionFromDate";
            this.deInspectionFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deInspectionFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deInspectionFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deInspectionFromDate.Properties.NullText = "Not Used";
            this.deInspectionFromDate.Size = new System.Drawing.Size(101, 20);
            this.deInspectionFromDate.StyleController = this.layoutControl4;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem4.Text = "Information - From Date";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "All inspections with an <b>Inspection Date</b> which fall between the From Date a" +
    "nd To Date will be returned.\r\n";
            superToolTip3.Items.Add(toolTipTitleItem4);
            superToolTip3.Items.Add(toolTipItem3);
            this.deInspectionFromDate.SuperTip = superToolTip3;
            this.deInspectionFromDate.TabIndex = 0;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem2});
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlGroup4.Size = new System.Drawing.Size(324, 765);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.ceLastInspectionOnly;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(202, 26);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btnLoadInspections;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(202, 48);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(112, 26);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(112, 26);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(112, 26);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.gridSplitContainer2;
            this.layoutControlItem14.CustomizationFormText = "Inspection Grid:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(314, 681);
            this.layoutControlItem14.Text = "Inspection Grid:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AllowHtmlStringInCaption = true;
            this.layoutControlItem10.Control = this.deInspectionFromDate;
            this.layoutControlItem10.CustomizationFormText = "From Date:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(162, 24);
            this.layoutControlItem10.Text = "From Date:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AllowHtmlStringInCaption = true;
            this.layoutControlItem11.Control = this.deInspectionToDate;
            this.layoutControlItem11.CustomizationFormText = "To Date:";
            this.layoutControlItem11.Location = new System.Drawing.Point(162, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(152, 24);
            this.layoutControlItem11.Text = "To Date:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(42, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonEditClientFilter2;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(314, 24);
            this.layoutControlItem2.Text = "Client:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(31, 13);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.layoutControl7);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(324, 765);
            this.xtraTabPage3.Text = "Actions";
            // 
            // layoutControl7
            // 
            this.layoutControl7.Controls.Add(this.buttonEditClientFilter3);
            this.layoutControl7.Controls.Add(this.deActionToDate);
            this.layoutControl7.Controls.Add(this.deActionFromDate);
            this.layoutControl7.Controls.Add(this.gridSplitContainer3);
            this.layoutControl7.Controls.Add(this.btnLoadActions);
            this.layoutControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl7.Location = new System.Drawing.Point(0, 0);
            this.layoutControl7.MenuManager = this.barManager1;
            this.layoutControl7.Name = "layoutControl7";
            this.layoutControl7.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(854, 204, 250, 350);
            this.layoutControl7.Root = this.layoutControlGroup7;
            this.layoutControl7.Size = new System.Drawing.Size(324, 765);
            this.layoutControl7.TabIndex = 0;
            this.layoutControl7.Text = "layoutControl7";
            // 
            // buttonEditClientFilter3
            // 
            this.buttonEditClientFilter3.Location = new System.Drawing.Point(41, 31);
            this.buttonEditClientFilter3.MenuManager = this.barManager1;
            this.buttonEditClientFilter3.Name = "buttonEditClientFilter3";
            this.buttonEditClientFilter3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter3.Size = new System.Drawing.Size(181, 20);
            this.buttonEditClientFilter3.StyleController = this.layoutControl7;
            this.buttonEditClientFilter3.TabIndex = 10;
            this.buttonEditClientFilter3.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter3_ButtonClick);
            // 
            // deActionToDate
            // 
            this.deActionToDate.EditValue = null;
            this.deActionToDate.Location = new System.Drawing.Point(217, 7);
            this.deActionToDate.MenuManager = this.barManager1;
            this.deActionToDate.Name = "deActionToDate";
            this.deActionToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deActionToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deActionToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deActionToDate.Properties.NullText = "Not Used";
            this.deActionToDate.Size = new System.Drawing.Size(100, 20);
            this.deActionToDate.StyleController = this.layoutControl7;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem5.Text = "Information - To Date";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "All actions with an <b>Action Due Date</b> which fall between the From Date and T" +
    "o Date will be returned.\r\n\r\n";
            superToolTip4.Items.Add(toolTipTitleItem5);
            superToolTip4.Items.Add(toolTipItem4);
            this.deActionToDate.SuperTip = superToolTip4;
            this.deActionToDate.TabIndex = 1;
            // 
            // deActionFromDate
            // 
            this.deActionFromDate.EditValue = null;
            this.deActionFromDate.Location = new System.Drawing.Point(64, 7);
            this.deActionFromDate.MenuManager = this.barManager1;
            this.deActionFromDate.Name = "deActionFromDate";
            this.deActionFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deActionFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deActionFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deActionFromDate.Properties.NullText = "Not Used";
            this.deActionFromDate.Size = new System.Drawing.Size(104, 20);
            this.deActionFromDate.StyleController = this.layoutControl7;
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem6.Text = "Information - From Date";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "All actions with an <b>Action Due Date</b> which fall between the From Date and T" +
    "o Date will be returned.\r\n";
            superToolTip5.Items.Add(toolTipTitleItem6);
            superToolTip5.Items.Add(toolTipItem5);
            this.deActionFromDate.SuperTip = superToolTip5;
            this.deActionFromDate.TabIndex = 0;
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(7, 57);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(310, 701);
            this.gridSplitContainer3.TabIndex = 4;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp01232ATReportsListingsActionFilterBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit2});
            this.gridControl3.Size = new System.Drawing.Size(310, 701);
            this.gridControl3.TabIndex = 7;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp01232ATReportsListingsActionFilterBindingSource
            // 
            this.sp01232ATReportsListingsActionFilterBindingSource.DataMember = "sp01232_AT_Reports_Listings_Action_Filter";
            this.sp01232ATReportsListingsActionFilterBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colInspectionID1,
            this.colTreeID2,
            this.colSiteID2,
            this.colClientID3,
            this.colClientNameName3,
            this.colSiteName2,
            this.colTreeReference2,
            this.colTreeMappingID1,
            this.colTreeGridReference1,
            this.colTreeDistance1,
            this.colTreeHouseName1,
            this.colTreePostcode1,
            this.colInspectionReference1,
            this.colInspectorName1,
            this.colInspectionDate1,
            this.colIncidentReferenceNumber1,
            this.colIncidentID1,
            this.colActionJobNumber,
            this.colAction,
            this.colActionPriority,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colActionBy,
            this.colActionSupervisor,
            this.colActionOwnership,
            this.colActionCostCentre,
            this.colActionBudget,
            this.colActionWorkUnits,
            this.colActionScheduleOfRates,
            this.colActionBudgetedRateDescription,
            this.colActionBudgetedRate,
            this.colActionBudgetedCost,
            this.colActionActualRateDescription,
            this.colActionActualRate,
            this.colActionActualCost,
            this.colDateLastModified1,
            this.colActionWorkOrder,
            this.colRemarks2,
            this.colUser12,
            this.colUser22,
            this.colUser32,
            this.colcolor2,
            this.colActionCostDifference,
            this.colActionCompletionStatus,
            this.colActionTimeliness,
            this.Calculated1c,
            this.Calculated2c,
            this.Calculated3c,
            this.colActionWorkOrderCompleteDate,
            this.colActionWorkOrderIssueDate,
            this.colUserPicklist12,
            this.colUserPicklist22,
            this.colUserPicklist32});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientNameName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionReference1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionJobNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseDown);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            this.colActionID.Width = 56;
            // 
            // colInspectionID1
            // 
            this.colInspectionID1.Caption = "Inspection ID";
            this.colInspectionID1.FieldName = "InspectionID";
            this.colInspectionID1.Name = "colInspectionID1";
            this.colInspectionID1.OptionsColumn.AllowEdit = false;
            this.colInspectionID1.OptionsColumn.AllowFocus = false;
            this.colInspectionID1.OptionsColumn.ReadOnly = true;
            this.colInspectionID1.Width = 76;
            // 
            // colTreeID2
            // 
            this.colTreeID2.Caption = "Tree ID";
            this.colTreeID2.FieldName = "TreeID";
            this.colTreeID2.Name = "colTreeID2";
            this.colTreeID2.OptionsColumn.AllowEdit = false;
            this.colTreeID2.OptionsColumn.AllowFocus = false;
            this.colTreeID2.OptionsColumn.ReadOnly = true;
            this.colTreeID2.Width = 48;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.Width = 62;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            this.colClientID3.Width = 59;
            // 
            // colClientNameName3
            // 
            this.colClientNameName3.Caption = "client Name";
            this.colClientNameName3.FieldName = "ClientName";
            this.colClientNameName3.Name = "colClientNameName3";
            this.colClientNameName3.OptionsColumn.AllowEdit = false;
            this.colClientNameName3.OptionsColumn.AllowFocus = false;
            this.colClientNameName3.OptionsColumn.ReadOnly = true;
            this.colClientNameName3.Width = 98;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Width = 101;
            // 
            // colTreeReference2
            // 
            this.colTreeReference2.Caption = "Tree Ref";
            this.colTreeReference2.FieldName = "TreeReference";
            this.colTreeReference2.Name = "colTreeReference2";
            this.colTreeReference2.OptionsColumn.AllowEdit = false;
            this.colTreeReference2.OptionsColumn.AllowFocus = false;
            this.colTreeReference2.OptionsColumn.ReadOnly = true;
            this.colTreeReference2.Width = 96;
            // 
            // colTreeMappingID1
            // 
            this.colTreeMappingID1.Caption = "Mapping ID";
            this.colTreeMappingID1.FieldName = "TreeMappingID";
            this.colTreeMappingID1.Name = "colTreeMappingID1";
            this.colTreeMappingID1.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID1.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID1.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID1.Width = 66;
            // 
            // colTreeGridReference1
            // 
            this.colTreeGridReference1.Caption = "Grid Reference";
            this.colTreeGridReference1.FieldName = "TreeGridReference";
            this.colTreeGridReference1.Name = "colTreeGridReference1";
            this.colTreeGridReference1.OptionsColumn.AllowEdit = false;
            this.colTreeGridReference1.OptionsColumn.AllowFocus = false;
            this.colTreeGridReference1.OptionsColumn.ReadOnly = true;
            this.colTreeGridReference1.Width = 96;
            // 
            // colTreeDistance1
            // 
            this.colTreeDistance1.Caption = "Distance";
            this.colTreeDistance1.FieldName = "TreeDistance";
            this.colTreeDistance1.Name = "colTreeDistance1";
            this.colTreeDistance1.OptionsColumn.AllowEdit = false;
            this.colTreeDistance1.OptionsColumn.AllowFocus = false;
            this.colTreeDistance1.OptionsColumn.ReadOnly = true;
            this.colTreeDistance1.Width = 63;
            // 
            // colTreeHouseName1
            // 
            this.colTreeHouseName1.Caption = "Nearest House";
            this.colTreeHouseName1.FieldName = "TreeHouseName";
            this.colTreeHouseName1.Name = "colTreeHouseName1";
            this.colTreeHouseName1.OptionsColumn.AllowEdit = false;
            this.colTreeHouseName1.OptionsColumn.AllowFocus = false;
            this.colTreeHouseName1.OptionsColumn.ReadOnly = true;
            this.colTreeHouseName1.Visible = true;
            this.colTreeHouseName1.VisibleIndex = 4;
            this.colTreeHouseName1.Width = 93;
            // 
            // colTreePostcode1
            // 
            this.colTreePostcode1.Caption = "Postcode";
            this.colTreePostcode1.FieldName = "TreePostcode";
            this.colTreePostcode1.Name = "colTreePostcode1";
            this.colTreePostcode1.OptionsColumn.AllowEdit = false;
            this.colTreePostcode1.OptionsColumn.AllowFocus = false;
            this.colTreePostcode1.OptionsColumn.ReadOnly = true;
            this.colTreePostcode1.Width = 66;
            // 
            // colInspectionReference1
            // 
            this.colInspectionReference1.Caption = "Inspection Ref";
            this.colInspectionReference1.FieldName = "InspectionReference";
            this.colInspectionReference1.Name = "colInspectionReference1";
            this.colInspectionReference1.OptionsColumn.AllowEdit = false;
            this.colInspectionReference1.OptionsColumn.AllowFocus = false;
            this.colInspectionReference1.OptionsColumn.ReadOnly = true;
            this.colInspectionReference1.Visible = true;
            this.colInspectionReference1.VisibleIndex = 0;
            this.colInspectionReference1.Width = 105;
            // 
            // colInspectorName1
            // 
            this.colInspectorName1.Caption = "Inspector";
            this.colInspectorName1.FieldName = "InspectorName";
            this.colInspectorName1.Name = "colInspectorName1";
            this.colInspectorName1.OptionsColumn.AllowEdit = false;
            this.colInspectorName1.OptionsColumn.AllowFocus = false;
            this.colInspectorName1.OptionsColumn.ReadOnly = true;
            this.colInspectorName1.Visible = true;
            this.colInspectorName1.VisibleIndex = 2;
            this.colInspectorName1.Width = 94;
            // 
            // colInspectionDate1
            // 
            this.colInspectionDate1.Caption = "Inspection Date";
            this.colInspectionDate1.FieldName = "InspectionDate";
            this.colInspectionDate1.Name = "colInspectionDate1";
            this.colInspectionDate1.OptionsColumn.AllowEdit = false;
            this.colInspectionDate1.OptionsColumn.AllowFocus = false;
            this.colInspectionDate1.OptionsColumn.ReadOnly = true;
            this.colInspectionDate1.Visible = true;
            this.colInspectionDate1.VisibleIndex = 3;
            this.colInspectionDate1.Width = 98;
            // 
            // colIncidentReferenceNumber1
            // 
            this.colIncidentReferenceNumber1.Caption = "Incident Ref";
            this.colIncidentReferenceNumber1.FieldName = "IncidentReferenceNumber";
            this.colIncidentReferenceNumber1.Name = "colIncidentReferenceNumber1";
            this.colIncidentReferenceNumber1.OptionsColumn.AllowEdit = false;
            this.colIncidentReferenceNumber1.OptionsColumn.AllowFocus = false;
            this.colIncidentReferenceNumber1.OptionsColumn.ReadOnly = true;
            this.colIncidentReferenceNumber1.Visible = true;
            this.colIncidentReferenceNumber1.VisibleIndex = 1;
            this.colIncidentReferenceNumber1.Width = 88;
            // 
            // colIncidentID1
            // 
            this.colIncidentID1.Caption = "Incident ID";
            this.colIncidentID1.FieldName = "IncidentID";
            this.colIncidentID1.Name = "colIncidentID1";
            this.colIncidentID1.OptionsColumn.AllowEdit = false;
            this.colIncidentID1.OptionsColumn.AllowFocus = false;
            this.colIncidentID1.OptionsColumn.ReadOnly = true;
            this.colIncidentID1.Width = 65;
            // 
            // colActionJobNumber
            // 
            this.colActionJobNumber.Caption = "Job Number";
            this.colActionJobNumber.FieldName = "ActionJobNumber";
            this.colActionJobNumber.Name = "colActionJobNumber";
            this.colActionJobNumber.OptionsColumn.AllowEdit = false;
            this.colActionJobNumber.OptionsColumn.AllowFocus = false;
            this.colActionJobNumber.OptionsColumn.ReadOnly = true;
            this.colActionJobNumber.Visible = true;
            this.colActionJobNumber.VisibleIndex = 5;
            this.colActionJobNumber.Width = 90;
            // 
            // colAction
            // 
            this.colAction.Caption = "Action";
            this.colAction.FieldName = "Action";
            this.colAction.Name = "colAction";
            this.colAction.OptionsColumn.AllowEdit = false;
            this.colAction.OptionsColumn.AllowFocus = false;
            this.colAction.OptionsColumn.ReadOnly = true;
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 6;
            this.colAction.Width = 125;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 7;
            this.colActionPriority.Width = 56;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Action Due";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 8;
            this.colActionDueDate.Width = 74;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Action Done";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 9;
            this.colActionDoneDate.Width = 80;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action By";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            this.colActionBy.Visible = true;
            this.colActionBy.VisibleIndex = 12;
            this.colActionBy.Width = 97;
            // 
            // colActionSupervisor
            // 
            this.colActionSupervisor.Caption = "Supervisor";
            this.colActionSupervisor.FieldName = "ActionSupervisor";
            this.colActionSupervisor.Name = "colActionSupervisor";
            this.colActionSupervisor.OptionsColumn.AllowEdit = false;
            this.colActionSupervisor.OptionsColumn.AllowFocus = false;
            this.colActionSupervisor.OptionsColumn.ReadOnly = true;
            this.colActionSupervisor.Visible = true;
            this.colActionSupervisor.VisibleIndex = 13;
            this.colActionSupervisor.Width = 104;
            // 
            // colActionOwnership
            // 
            this.colActionOwnership.Caption = "Ownership";
            this.colActionOwnership.FieldName = "ActionOwnership";
            this.colActionOwnership.Name = "colActionOwnership";
            this.colActionOwnership.OptionsColumn.AllowEdit = false;
            this.colActionOwnership.OptionsColumn.AllowFocus = false;
            this.colActionOwnership.OptionsColumn.ReadOnly = true;
            this.colActionOwnership.Visible = true;
            this.colActionOwnership.VisibleIndex = 15;
            this.colActionOwnership.Width = 108;
            // 
            // colActionCostCentre
            // 
            this.colActionCostCentre.Caption = "Cost Centre";
            this.colActionCostCentre.FieldName = "ActionCostCentre";
            this.colActionCostCentre.Name = "colActionCostCentre";
            this.colActionCostCentre.OptionsColumn.AllowEdit = false;
            this.colActionCostCentre.OptionsColumn.AllowFocus = false;
            this.colActionCostCentre.OptionsColumn.ReadOnly = true;
            this.colActionCostCentre.Visible = true;
            this.colActionCostCentre.VisibleIndex = 16;
            this.colActionCostCentre.Width = 117;
            // 
            // colActionBudget
            // 
            this.colActionBudget.Caption = "Budget";
            this.colActionBudget.FieldName = "ActionBudget";
            this.colActionBudget.Name = "colActionBudget";
            this.colActionBudget.OptionsColumn.AllowEdit = false;
            this.colActionBudget.OptionsColumn.AllowFocus = false;
            this.colActionBudget.OptionsColumn.ReadOnly = true;
            this.colActionBudget.Visible = true;
            this.colActionBudget.VisibleIndex = 17;
            this.colActionBudget.Width = 93;
            // 
            // colActionWorkUnits
            // 
            this.colActionWorkUnits.Caption = "Work Units";
            this.colActionWorkUnits.FieldName = "ActionWorkUnits";
            this.colActionWorkUnits.Name = "colActionWorkUnits";
            this.colActionWorkUnits.OptionsColumn.AllowEdit = false;
            this.colActionWorkUnits.OptionsColumn.AllowFocus = false;
            this.colActionWorkUnits.OptionsColumn.ReadOnly = true;
            this.colActionWorkUnits.Visible = true;
            this.colActionWorkUnits.VisibleIndex = 18;
            this.colActionWorkUnits.Width = 74;
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Schedule Of Rates";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 19;
            this.colActionScheduleOfRates.Width = 111;
            // 
            // colActionBudgetedRateDescription
            // 
            this.colActionBudgetedRateDescription.Caption = "Budgeted Rate Desc";
            this.colActionBudgetedRateDescription.FieldName = "ActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.Name = "colActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRateDescription.Visible = true;
            this.colActionBudgetedRateDescription.VisibleIndex = 20;
            this.colActionBudgetedRateDescription.Width = 120;
            // 
            // colActionBudgetedRate
            // 
            this.colActionBudgetedRate.Caption = "Budgeted Rate";
            this.colActionBudgetedRate.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionBudgetedRate.FieldName = "ActionBudgetedRate";
            this.colActionBudgetedRate.Name = "colActionBudgetedRate";
            this.colActionBudgetedRate.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRate.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRate.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRate.Visible = true;
            this.colActionBudgetedRate.VisibleIndex = 21;
            this.colActionBudgetedRate.Width = 94;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colActionBudgetedCost
            // 
            this.colActionBudgetedCost.Caption = "Budgeted Cost";
            this.colActionBudgetedCost.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionBudgetedCost.FieldName = "ActionBudgetedCost";
            this.colActionBudgetedCost.Name = "colActionBudgetedCost";
            this.colActionBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedCost.Visible = true;
            this.colActionBudgetedCost.VisibleIndex = 24;
            this.colActionBudgetedCost.Width = 93;
            // 
            // colActionActualRateDescription
            // 
            this.colActionActualRateDescription.Caption = "Actual Rate Desc";
            this.colActionActualRateDescription.FieldName = "ActionActualRateDescription";
            this.colActionActualRateDescription.Name = "colActionActualRateDescription";
            this.colActionActualRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionActualRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionActualRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionActualRateDescription.Visible = true;
            this.colActionActualRateDescription.VisibleIndex = 22;
            this.colActionActualRateDescription.Width = 104;
            // 
            // colActionActualRate
            // 
            this.colActionActualRate.Caption = "Actual Rate";
            this.colActionActualRate.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionActualRate.FieldName = "ActionActualRate";
            this.colActionActualRate.Name = "colActionActualRate";
            this.colActionActualRate.OptionsColumn.AllowEdit = false;
            this.colActionActualRate.OptionsColumn.AllowFocus = false;
            this.colActionActualRate.OptionsColumn.ReadOnly = true;
            this.colActionActualRate.Visible = true;
            this.colActionActualRate.VisibleIndex = 23;
            this.colActionActualRate.Width = 78;
            // 
            // colActionActualCost
            // 
            this.colActionActualCost.Caption = "Actual Cost";
            this.colActionActualCost.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionActualCost.FieldName = "ActionActualCost";
            this.colActionActualCost.Name = "colActionActualCost";
            this.colActionActualCost.OptionsColumn.AllowEdit = false;
            this.colActionActualCost.OptionsColumn.AllowFocus = false;
            this.colActionActualCost.OptionsColumn.ReadOnly = true;
            this.colActionActualCost.Visible = true;
            this.colActionActualCost.VisibleIndex = 25;
            this.colActionActualCost.Width = 77;
            // 
            // colDateLastModified1
            // 
            this.colDateLastModified1.Caption = "Last Modified";
            this.colDateLastModified1.FieldName = "DateLastModified";
            this.colDateLastModified1.Name = "colDateLastModified1";
            this.colDateLastModified1.OptionsColumn.AllowEdit = false;
            this.colDateLastModified1.OptionsColumn.AllowFocus = false;
            this.colDateLastModified1.OptionsColumn.ReadOnly = true;
            // 
            // colActionWorkOrder
            // 
            this.colActionWorkOrder.Caption = "Work Order";
            this.colActionWorkOrder.FieldName = "ActionWorkOrder";
            this.colActionWorkOrder.Name = "colActionWorkOrder";
            this.colActionWorkOrder.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrder.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrder.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrder.Visible = true;
            this.colActionWorkOrder.VisibleIndex = 14;
            this.colActionWorkOrder.Width = 78;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Width = 53;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            // 
            // colUser12
            // 
            this.colUser12.Caption = "User 1";
            this.colUser12.FieldName = "User1";
            this.colUser12.Name = "colUser12";
            this.colUser12.OptionsColumn.AllowEdit = false;
            this.colUser12.OptionsColumn.AllowFocus = false;
            this.colUser12.OptionsColumn.ReadOnly = true;
            this.colUser12.Width = 43;
            // 
            // colUser22
            // 
            this.colUser22.Caption = "User 2";
            this.colUser22.FieldName = "User2";
            this.colUser22.Name = "colUser22";
            this.colUser22.OptionsColumn.AllowEdit = false;
            this.colUser22.OptionsColumn.AllowFocus = false;
            this.colUser22.OptionsColumn.ReadOnly = true;
            this.colUser22.Width = 43;
            // 
            // colUser32
            // 
            this.colUser32.Caption = "Use r3";
            this.colUser32.FieldName = "User3";
            this.colUser32.Name = "colUser32";
            this.colUser32.OptionsColumn.AllowEdit = false;
            this.colUser32.OptionsColumn.AllowFocus = false;
            this.colUser32.OptionsColumn.ReadOnly = true;
            this.colUser32.Width = 43;
            // 
            // colcolor2
            // 
            this.colcolor2.Caption = "color";
            this.colcolor2.FieldName = "color";
            this.colcolor2.Name = "colcolor2";
            this.colcolor2.OptionsColumn.AllowEdit = false;
            this.colcolor2.OptionsColumn.AllowFocus = false;
            this.colcolor2.OptionsColumn.ReadOnly = true;
            this.colcolor2.OptionsColumn.ShowInCustomizationForm = false;
            this.colcolor2.Width = 35;
            // 
            // colActionCostDifference
            // 
            this.colActionCostDifference.Caption = "Cost Diff";
            this.colActionCostDifference.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionCostDifference.FieldName = "ActionCostDifference";
            this.colActionCostDifference.Name = "colActionCostDifference";
            this.colActionCostDifference.OptionsColumn.AllowEdit = false;
            this.colActionCostDifference.OptionsColumn.AllowFocus = false;
            this.colActionCostDifference.OptionsColumn.ReadOnly = true;
            this.colActionCostDifference.Visible = true;
            this.colActionCostDifference.VisibleIndex = 26;
            // 
            // colActionCompletionStatus
            // 
            this.colActionCompletionStatus.Caption = "Complation Status";
            this.colActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.colActionCompletionStatus.Name = "colActionCompletionStatus";
            this.colActionCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colActionCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colActionCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colActionCompletionStatus.Visible = true;
            this.colActionCompletionStatus.VisibleIndex = 10;
            this.colActionCompletionStatus.Width = 109;
            // 
            // colActionTimeliness
            // 
            this.colActionTimeliness.Caption = "Timeliness";
            this.colActionTimeliness.FieldName = "ActionTimeliness";
            this.colActionTimeliness.Name = "colActionTimeliness";
            this.colActionTimeliness.OptionsColumn.AllowEdit = false;
            this.colActionTimeliness.OptionsColumn.AllowFocus = false;
            this.colActionTimeliness.OptionsColumn.ReadOnly = true;
            this.colActionTimeliness.Visible = true;
            this.colActionTimeliness.VisibleIndex = 11;
            this.colActionTimeliness.Width = 70;
            // 
            // Calculated1c
            // 
            this.Calculated1c.Caption = "<b>Calculated 1</b>";
            this.Calculated1c.FieldName = "Calculated1";
            this.Calculated1c.Name = "Calculated1c";
            this.Calculated1c.OptionsColumn.AllowEdit = false;
            this.Calculated1c.OptionsColumn.AllowFocus = false;
            this.Calculated1c.OptionsColumn.ReadOnly = true;
            this.Calculated1c.ShowUnboundExpressionMenu = true;
            this.Calculated1c.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1c.Visible = true;
            this.Calculated1c.VisibleIndex = 27;
            this.Calculated1c.Width = 90;
            // 
            // Calculated2c
            // 
            this.Calculated2c.Caption = "<b>Calculated 2</b>";
            this.Calculated2c.FieldName = "Calculated2";
            this.Calculated2c.Name = "Calculated2c";
            this.Calculated2c.OptionsColumn.AllowEdit = false;
            this.Calculated2c.OptionsColumn.AllowFocus = false;
            this.Calculated2c.OptionsColumn.ReadOnly = true;
            this.Calculated2c.ShowUnboundExpressionMenu = true;
            this.Calculated2c.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2c.Visible = true;
            this.Calculated2c.VisibleIndex = 28;
            this.Calculated2c.Width = 90;
            // 
            // Calculated3c
            // 
            this.Calculated3c.Caption = "<b>Calculated 3</b>";
            this.Calculated3c.FieldName = "Calculated3";
            this.Calculated3c.Name = "Calculated3c";
            this.Calculated3c.OptionsColumn.AllowEdit = false;
            this.Calculated3c.OptionsColumn.AllowFocus = false;
            this.Calculated3c.OptionsColumn.ReadOnly = true;
            this.Calculated3c.ShowUnboundExpressionMenu = true;
            this.Calculated3c.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3c.Visible = true;
            this.Calculated3c.VisibleIndex = 29;
            this.Calculated3c.Width = 90;
            // 
            // colActionWorkOrderCompleteDate
            // 
            this.colActionWorkOrderCompleteDate.Caption = "Work Order Complete Date";
            this.colActionWorkOrderCompleteDate.FieldName = "ActionWorkOrderCompleteDate";
            this.colActionWorkOrderCompleteDate.Name = "colActionWorkOrderCompleteDate";
            this.colActionWorkOrderCompleteDate.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrderCompleteDate.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrderCompleteDate.OptionsColumn.ReadOnly = true;
            // 
            // colActionWorkOrderIssueDate
            // 
            this.colActionWorkOrderIssueDate.Caption = "Work Order Issue Date";
            this.colActionWorkOrderIssueDate.FieldName = "ActionWorkOrderIssueDate";
            this.colActionWorkOrderIssueDate.Name = "colActionWorkOrderIssueDate";
            this.colActionWorkOrderIssueDate.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrderIssueDate.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrderIssueDate.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist12
            // 
            this.colUserPicklist12.Caption = "User Picklist 1";
            this.colUserPicklist12.FieldName = "UserPicklist1";
            this.colUserPicklist12.Name = "colUserPicklist12";
            this.colUserPicklist12.OptionsColumn.AllowEdit = false;
            this.colUserPicklist12.OptionsColumn.AllowFocus = false;
            this.colUserPicklist12.OptionsColumn.ReadOnly = true;
            this.colUserPicklist12.Width = 86;
            // 
            // colUserPicklist22
            // 
            this.colUserPicklist22.Caption = "User Picklist 2";
            this.colUserPicklist22.FieldName = "UserPicklist2";
            this.colUserPicklist22.Name = "colUserPicklist22";
            this.colUserPicklist22.OptionsColumn.AllowEdit = false;
            this.colUserPicklist22.OptionsColumn.AllowFocus = false;
            this.colUserPicklist22.OptionsColumn.ReadOnly = true;
            this.colUserPicklist22.Width = 86;
            // 
            // colUserPicklist32
            // 
            this.colUserPicklist32.Caption = "User Picklist 3";
            this.colUserPicklist32.FieldName = "UserPicklist3";
            this.colUserPicklist32.Name = "colUserPicklist32";
            this.colUserPicklist32.OptionsColumn.AllowEdit = false;
            this.colUserPicklist32.OptionsColumn.AllowFocus = false;
            this.colUserPicklist32.OptionsColumn.ReadOnly = true;
            this.colUserPicklist32.Width = 86;
            // 
            // btnLoadActions
            // 
            this.btnLoadActions.ImageOptions.ImageIndex = 0;
            this.btnLoadActions.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadActions.Location = new System.Drawing.Point(226, 31);
            this.btnLoadActions.Name = "btnLoadActions";
            this.btnLoadActions.Size = new System.Drawing.Size(91, 22);
            this.btnLoadActions.StyleController = this.layoutControl7;
            this.btnLoadActions.TabIndex = 3;
            this.btnLoadActions.Text = "Load Actions";
            this.btnLoadActions.Click += new System.EventHandler(this.btnLoadActions_Click);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem3});
            this.layoutControlGroup7.Name = "Root";
            this.layoutControlGroup7.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlGroup7.Size = new System.Drawing.Size(324, 765);
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.btnLoadActions;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(219, 24);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(95, 26);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(95, 26);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(95, 26);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.gridSplitContainer3;
            this.layoutControlItem25.CustomizationFormText = "Action Grid:";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(314, 705);
            this.layoutControlItem25.Text = "Action Grid:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AllowHtmlStringInCaption = true;
            this.layoutControlItem27.Control = this.deActionFromDate;
            this.layoutControlItem27.CustomizationFormText = "From Date:";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(165, 24);
            this.layoutControlItem27.Text = "From Date:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AllowHtmlStringInCaption = true;
            this.layoutControlItem28.Control = this.deActionToDate;
            this.layoutControlItem28.CustomizationFormText = "To Date:";
            this.layoutControlItem28.Location = new System.Drawing.Point(165, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(149, 24);
            this.layoutControlItem28.Text = "To Date:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(42, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonEditClientFilter3;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(219, 26);
            this.layoutControlItem3.Text = "Client:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(31, 13);
            // 
            // sp01220_AT_Reports_Listings_Tree_FilterTableAdapter
            // 
            this.sp01220_AT_Reports_Listings_Tree_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(339, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage4;
            this.xtraTabControl2.Size = new System.Drawing.Size(954, 850);
            this.xtraTabControl2.TabIndex = 6;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6});
            this.xtraTabControl2.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl2_SelectedPageChanged);
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(949, 824);
            this.xtraTabPage4.Text = "Tree Analysis";
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(949, 824);
            this.xtraTabPage5.Text = "Inspection Analysis";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.pivotGridControl2);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.chartControl2);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(949, 824);
            this.splitContainerControl2.SplitterPosition = 439;
            this.splitContainerControl2.TabIndex = 8;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // pivotGridControl2
            // 
            this.pivotGridControl2.ActiveFilterString = "";
            this.pivotGridControl2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl2.DataSource = this.sp01235ATDataAnalysisInspectionsBindingSource;
            this.pivotGridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl2.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldInspectionID,
            this.fieldTreeID1,
            this.fieldSiteID1,
            this.fieldClientID1,
            this.fieldSiteName1,
            this.fieldClientName1,
            this.fieldTreeReference1,
            this.fieldTreeMappingID,
            this.fieldTreeGridReference,
            this.fieldTreeDistance,
            this.fieldTreeHouseName,
            this.fieldTreeSpeciesName,
            this.fieldTreeAccess,
            this.fieldTreeVisibility,
            this.fieldTreeContext,
            this.fieldTreeManagement,
            this.fieldTreeLegalStatus,
            this.fieldTreeLastInspectionDate,
            this.fieldTreeInspectionCycle,
            this.fieldTreeInspectionUnit,
            this.fieldTreeNextInspectionDate,
            this.fieldTreeGroundType,
            this.fieldTreeDBH,
            this.fieldTreeDBHRange,
            this.fieldTreeHeight,
            this.fieldTreeHeightRange,
            this.fieldTreeProtectionType,
            this.fieldTreeCrownDiameter,
            this.fieldTreePlantDate,
            this.fieldTreeGroupNumber,
            this.fieldTreeRiskCategory,
            this.fieldTreeSiteHazardClass,
            this.fieldTreePlantSize,
            this.fieldTreePlantSource,
            this.fieldTreePlantMethod,
            this.fieldTreePostcode,
            this.fieldTreeMapLabel,
            this.fieldTreeStatus,
            this.fieldTreeSize,
            this.fieldTreeTarget1,
            this.fieldTreeTarget2,
            this.fieldTreeTarget3,
            this.fieldTreeLastModifiedDate,
            this.fieldTreeRemarks,
            this.fieldTreeUser1,
            this.fieldTreeUser2,
            this.fieldTreeUser3,
            this.fieldTreeAgeClass,
            this.fieldTreeX,
            this.fieldTreeY,
            this.fieldTreeAreaHa,
            this.fieldTreeReplantCount,
            this.fieldTreeSurveyDate,
            this.fieldTreeType1,
            this.fieldTreeHedgeOwner,
            this.fieldTreeHedgeLength,
            this.fieldTreeGardenSize,
            this.fieldTreePropertyProximity,
            this.fieldTreeSiteLevel,
            this.fieldTreeSituation,
            this.fieldTreeRiskFactor,
            this.fieldTreePolygonXY,
            this.fieldcolor1,
            this.fieldTreeOwnershipName,
            this.fieldInspectionReference,
            this.fieldInspectionInspector,
            this.fieldInspectionDate,
            this.fieldInspectionIncidentReference,
            this.fieldInspectionIncidentDate,
            this.fieldIncidentID,
            this.fieldInspectionAngleToVertical,
            this.fieldInspectionLastModified,
            this.fieldInspectionUserDefined1,
            this.fieldInspectionUserDefined2,
            this.fieldInspectionUserDefined3,
            this.fieldInspectionRemarks,
            this.fieldInspectionStemPhysical1,
            this.fieldInspectionStemPhysical2,
            this.fieldInspectionStemPhysical3,
            this.fieldInspectionStemDisease1,
            this.fieldInspectionStemDisease2,
            this.fieldInspectionStemDisease3,
            this.fieldInspectionCrownPhysical1,
            this.fieldInspectionCrownPhysical2,
            this.fieldInspectionCrownPhysical3,
            this.fieldInspectionCrownDisease1,
            this.fieldInspectionCrownDisease2,
            this.fieldInspectionCrownDisease3,
            this.fieldInspectionCrownFoliation1,
            this.fieldInspectionCrownFoliation2,
            this.fieldInspectionCrownFoliation3,
            this.fieldInspectionRiskCategory,
            this.fieldInspectionGeneralCondition,
            this.fieldInspectionBasePhysical1,
            this.fieldInspectionBasePhysical2,
            this.fieldInspectionBasePhysical3,
            this.fieldInspectionVitality,
            this.fieldInspectionCount,
            this.calculatedTreeLastInspectionYear,
            this.calculatedTreeLastInspectionQuarter,
            this.calculatedTreeLastInspectionMonth,
            this.calculatedTreeNextInspectionYear,
            this.calculatedTreeNextInspectionQuarter,
            this.calculatedTreeNextInspectionMonth,
            this.calculatedInspectionYear,
            this.calculatedInspectionQuarter,
            this.calculatedInspectionMonth,
            this.fieldLinkedActionCount,
            this.fieldLinkedOutstandingActionCount,
            this.fieldNoFurtherActionRequired,
            this.fieldTreeCavat1,
            this.fieldTreeStemCount1,
            this.fieldTreeCrownNorth1,
            this.fieldTreeCrownSouth1,
            this.fieldTreeCrownEast1,
            this.fieldTreeCrownWest1,
            this.fieldTreeRetentionCategory1,
            this.fieldTreeSULE1,
            this.fieldTreeSpeciesVariety1,
            this.fieldTreeUserPicklist11,
            this.fieldTreeUserPicklist21,
            this.fieldTreeUserPicklist31,
            this.fieldTreeObjectLength1,
            this.fieldTreeObjectWidth1,
            this.fieldInspectionUserPicklist1,
            this.fieldInspectionUserPicklist2,
            this.fieldInspectionUserPicklist3});
            pivotGridGroup4.Caption = "Tree Last Inspection Year - Quarter - Month";
            pivotGridGroup4.Fields.Add(this.calculatedTreeLastInspectionYear);
            pivotGridGroup4.Fields.Add(this.calculatedTreeLastInspectionQuarter);
            pivotGridGroup4.Fields.Add(this.calculatedTreeLastInspectionMonth);
            pivotGridGroup4.Hierarchy = null;
            pivotGridGroup4.ShowNewValues = true;
            pivotGridGroup5.Caption = "Tree Next Inspection Year - Quarter - Month";
            pivotGridGroup5.Fields.Add(this.calculatedTreeNextInspectionYear);
            pivotGridGroup5.Fields.Add(this.calculatedTreeNextInspectionQuarter);
            pivotGridGroup5.Fields.Add(this.calculatedTreeNextInspectionMonth);
            pivotGridGroup5.Hierarchy = null;
            pivotGridGroup5.ShowNewValues = true;
            pivotGridGroup6.Caption = "Inspection Year - Quarter - Month";
            pivotGridGroup6.Fields.Add(this.calculatedInspectionYear);
            pivotGridGroup6.Fields.Add(this.calculatedInspectionQuarter);
            pivotGridGroup6.Fields.Add(this.calculatedInspectionMonth);
            pivotGridGroup6.Hierarchy = null;
            pivotGridGroup6.ShowNewValues = true;
            this.pivotGridControl2.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup4,
            pivotGridGroup5,
            pivotGridGroup6});
            this.pivotGridControl2.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl2.MenuManager = this.barManager1;
            this.pivotGridControl2.Name = "pivotGridControl2";
            this.pivotGridControl2.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pivotGridControl2.Size = new System.Drawing.Size(949, 439);
            this.pivotGridControl2.TabIndex = 0;
            this.pivotGridControl2.PopupMenuShowing += new DevExpress.XtraPivotGrid.PopupMenuShowingEventHandler(this.pivotGridControl2_PopupMenuShowing);
            this.pivotGridControl2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pivotGridControl2_MouseUp);
            // 
            // sp01235ATDataAnalysisInspectionsBindingSource
            // 
            this.sp01235ATDataAnalysisInspectionsBindingSource.DataMember = "sp01235_AT_Data_Analysis_Inspections";
            this.sp01235ATDataAnalysisInspectionsBindingSource.DataSource = this.dataSet_AT_Reports;
            // 
            // fieldInspectionID
            // 
            this.fieldInspectionID.AreaIndex = 0;
            this.fieldInspectionID.Caption = "Inspection ID";
            this.fieldInspectionID.FieldName = "InspectionID";
            this.fieldInspectionID.Name = "fieldInspectionID";
            this.fieldInspectionID.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionID.Visible = false;
            // 
            // fieldTreeID1
            // 
            this.fieldTreeID1.AreaIndex = 0;
            this.fieldTreeID1.Caption = "Tree ID";
            this.fieldTreeID1.FieldName = "TreeID";
            this.fieldTreeID1.Name = "fieldTreeID1";
            this.fieldTreeID1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeID1.Visible = false;
            // 
            // fieldSiteID1
            // 
            this.fieldSiteID1.AreaIndex = 0;
            this.fieldSiteID1.Caption = "Site ID";
            this.fieldSiteID1.FieldName = "SiteID";
            this.fieldSiteID1.Name = "fieldSiteID1";
            this.fieldSiteID1.Options.AllowRunTimeSummaryChange = true;
            this.fieldSiteID1.Visible = false;
            // 
            // fieldClientID1
            // 
            this.fieldClientID1.AreaIndex = 0;
            this.fieldClientID1.Caption = "Client ID";
            this.fieldClientID1.FieldName = "ClientID";
            this.fieldClientID1.Name = "fieldClientID1";
            this.fieldClientID1.Options.AllowRunTimeSummaryChange = true;
            this.fieldClientID1.Visible = false;
            // 
            // fieldSiteName1
            // 
            this.fieldSiteName1.AreaIndex = 1;
            this.fieldSiteName1.Caption = "Site Name";
            this.fieldSiteName1.FieldName = "SiteName";
            this.fieldSiteName1.Name = "fieldSiteName1";
            this.fieldSiteName1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldClientName1
            // 
            this.fieldClientName1.AreaIndex = 0;
            this.fieldClientName1.Caption = "Client Name";
            this.fieldClientName1.FieldName = "ClientName";
            this.fieldClientName1.Name = "fieldClientName1";
            this.fieldClientName1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeReference1
            // 
            this.fieldTreeReference1.AreaIndex = 2;
            this.fieldTreeReference1.Caption = "Tree Reference";
            this.fieldTreeReference1.FieldName = "TreeReference";
            this.fieldTreeReference1.Name = "fieldTreeReference1";
            this.fieldTreeReference1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeMappingID
            // 
            this.fieldTreeMappingID.AreaIndex = 3;
            this.fieldTreeMappingID.Caption = "Tree Mapping ID";
            this.fieldTreeMappingID.FieldName = "TreeMappingID";
            this.fieldTreeMappingID.Name = "fieldTreeMappingID";
            this.fieldTreeMappingID.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeMappingID.Visible = false;
            // 
            // fieldTreeGridReference
            // 
            this.fieldTreeGridReference.AreaIndex = 4;
            this.fieldTreeGridReference.Caption = "Tree Grid Reference";
            this.fieldTreeGridReference.FieldName = "TreeGridReference";
            this.fieldTreeGridReference.Name = "fieldTreeGridReference";
            this.fieldTreeGridReference.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeGridReference.Visible = false;
            // 
            // fieldTreeDistance
            // 
            this.fieldTreeDistance.AreaIndex = 4;
            this.fieldTreeDistance.Caption = "Tree Distance";
            this.fieldTreeDistance.FieldName = "TreeDistance";
            this.fieldTreeDistance.Name = "fieldTreeDistance";
            this.fieldTreeDistance.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeDistance.Visible = false;
            // 
            // fieldTreeHouseName
            // 
            this.fieldTreeHouseName.AreaIndex = 4;
            this.fieldTreeHouseName.Caption = "Tree House Name";
            this.fieldTreeHouseName.FieldName = "TreeHouseName";
            this.fieldTreeHouseName.Name = "fieldTreeHouseName";
            this.fieldTreeHouseName.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeHouseName.Visible = false;
            // 
            // fieldTreeSpeciesName
            // 
            this.fieldTreeSpeciesName.AreaIndex = 6;
            this.fieldTreeSpeciesName.Caption = "Tree Species Name";
            this.fieldTreeSpeciesName.FieldName = "TreeSpeciesName";
            this.fieldTreeSpeciesName.Name = "fieldTreeSpeciesName";
            this.fieldTreeSpeciesName.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeAccess
            // 
            this.fieldTreeAccess.AreaIndex = 5;
            this.fieldTreeAccess.Caption = "Tree Access";
            this.fieldTreeAccess.FieldName = "TreeAccess";
            this.fieldTreeAccess.Name = "fieldTreeAccess";
            this.fieldTreeAccess.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeAccess.Visible = false;
            // 
            // fieldTreeVisibility
            // 
            this.fieldTreeVisibility.AreaIndex = 5;
            this.fieldTreeVisibility.Caption = "Tree Visibility";
            this.fieldTreeVisibility.FieldName = "TreeVisibility";
            this.fieldTreeVisibility.Name = "fieldTreeVisibility";
            this.fieldTreeVisibility.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeVisibility.Visible = false;
            // 
            // fieldTreeContext
            // 
            this.fieldTreeContext.AreaIndex = 5;
            this.fieldTreeContext.Caption = "Tree Context";
            this.fieldTreeContext.FieldName = "TreeContext";
            this.fieldTreeContext.Name = "fieldTreeContext";
            this.fieldTreeContext.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeContext.Visible = false;
            // 
            // fieldTreeManagement
            // 
            this.fieldTreeManagement.AreaIndex = 5;
            this.fieldTreeManagement.Caption = "Tree Management";
            this.fieldTreeManagement.FieldName = "TreeManagement";
            this.fieldTreeManagement.Name = "fieldTreeManagement";
            this.fieldTreeManagement.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeManagement.Visible = false;
            // 
            // fieldTreeLegalStatus
            // 
            this.fieldTreeLegalStatus.AreaIndex = 5;
            this.fieldTreeLegalStatus.Caption = "Tree Legal Status";
            this.fieldTreeLegalStatus.FieldName = "TreeLegalStatus";
            this.fieldTreeLegalStatus.Name = "fieldTreeLegalStatus";
            this.fieldTreeLegalStatus.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeLegalStatus.Visible = false;
            // 
            // fieldTreeLastInspectionDate
            // 
            this.fieldTreeLastInspectionDate.AreaIndex = 10;
            this.fieldTreeLastInspectionDate.Caption = "Tree Last Inspection Date";
            this.fieldTreeLastInspectionDate.FieldName = "TreeLastInspectionDate";
            this.fieldTreeLastInspectionDate.Name = "fieldTreeLastInspectionDate";
            this.fieldTreeLastInspectionDate.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeLastInspectionDate.Visible = false;
            // 
            // fieldTreeInspectionCycle
            // 
            this.fieldTreeInspectionCycle.AreaIndex = 10;
            this.fieldTreeInspectionCycle.Caption = "Tree Inspection Cycle";
            this.fieldTreeInspectionCycle.FieldName = "TreeInspectionCycle";
            this.fieldTreeInspectionCycle.Name = "fieldTreeInspectionCycle";
            this.fieldTreeInspectionCycle.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeInspectionCycle.Visible = false;
            // 
            // fieldTreeInspectionUnit
            // 
            this.fieldTreeInspectionUnit.AreaIndex = 10;
            this.fieldTreeInspectionUnit.Caption = "Tree Inspection Unit";
            this.fieldTreeInspectionUnit.FieldName = "TreeInspectionUnit";
            this.fieldTreeInspectionUnit.Name = "fieldTreeInspectionUnit";
            this.fieldTreeInspectionUnit.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeInspectionUnit.Visible = false;
            // 
            // fieldTreeNextInspectionDate
            // 
            this.fieldTreeNextInspectionDate.AreaIndex = 10;
            this.fieldTreeNextInspectionDate.Caption = "Tree Next Inspection Date";
            this.fieldTreeNextInspectionDate.FieldName = "TreeNextInspectionDate";
            this.fieldTreeNextInspectionDate.Name = "fieldTreeNextInspectionDate";
            this.fieldTreeNextInspectionDate.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeNextInspectionDate.Visible = false;
            // 
            // fieldTreeGroundType
            // 
            this.fieldTreeGroundType.AreaIndex = 5;
            this.fieldTreeGroundType.Caption = "Tree Ground Type";
            this.fieldTreeGroundType.FieldName = "TreeGroundType";
            this.fieldTreeGroundType.Name = "fieldTreeGroundType";
            this.fieldTreeGroundType.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeGroundType.Visible = false;
            // 
            // fieldTreeDBH
            // 
            this.fieldTreeDBH.AreaIndex = 8;
            this.fieldTreeDBH.Caption = "Tree DBH";
            this.fieldTreeDBH.CellFormat.FormatString = "#";
            this.fieldTreeDBH.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeDBH.FieldName = "TreeDBH";
            this.fieldTreeDBH.GrandTotalCellFormat.FormatString = "#";
            this.fieldTreeDBH.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeDBH.Name = "fieldTreeDBH";
            this.fieldTreeDBH.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeDBH.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldTreeDBH.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeDBH.TotalValueFormat.FormatString = "#";
            this.fieldTreeDBH.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeDBH.ValueFormat.FormatString = "#";
            this.fieldTreeDBH.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreeDBHRange
            // 
            this.fieldTreeDBHRange.AreaIndex = 9;
            this.fieldTreeDBHRange.Caption = "Tree DBH Range";
            this.fieldTreeDBHRange.FieldName = "TreeDBHRange";
            this.fieldTreeDBHRange.Name = "fieldTreeDBHRange";
            this.fieldTreeDBHRange.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeHeight
            // 
            this.fieldTreeHeight.AreaIndex = 10;
            this.fieldTreeHeight.Caption = "Tree Height";
            this.fieldTreeHeight.CellFormat.FormatString = "#.##";
            this.fieldTreeHeight.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeHeight.FieldName = "TreeHeight";
            this.fieldTreeHeight.GrandTotalCellFormat.FormatString = "#.##";
            this.fieldTreeHeight.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeHeight.Name = "fieldTreeHeight";
            this.fieldTreeHeight.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeHeight.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldTreeHeight.TotalCellFormat.FormatString = "#.##";
            this.fieldTreeHeight.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeHeight.TotalValueFormat.FormatString = "#.##";
            this.fieldTreeHeight.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeHeight.ValueFormat.FormatString = "#.##";
            this.fieldTreeHeight.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreeHeightRange
            // 
            this.fieldTreeHeightRange.AreaIndex = 11;
            this.fieldTreeHeightRange.Caption = "Tree Height Range";
            this.fieldTreeHeightRange.FieldName = "TreeHeightRange";
            this.fieldTreeHeightRange.Name = "fieldTreeHeightRange";
            this.fieldTreeHeightRange.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeProtectionType
            // 
            this.fieldTreeProtectionType.AreaIndex = 9;
            this.fieldTreeProtectionType.Caption = "Tree Protection Type";
            this.fieldTreeProtectionType.FieldName = "TreeProtectionType";
            this.fieldTreeProtectionType.Name = "fieldTreeProtectionType";
            this.fieldTreeProtectionType.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeProtectionType.Visible = false;
            // 
            // fieldTreeCrownDiameter
            // 
            this.fieldTreeCrownDiameter.AreaIndex = 12;
            this.fieldTreeCrownDiameter.Caption = "Tree Crown Diameter";
            this.fieldTreeCrownDiameter.CellFormat.FormatString = "#";
            this.fieldTreeCrownDiameter.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCrownDiameter.FieldName = "TreeCrownDiameter";
            this.fieldTreeCrownDiameter.GrandTotalCellFormat.FormatString = "#";
            this.fieldTreeCrownDiameter.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCrownDiameter.Name = "fieldTreeCrownDiameter";
            this.fieldTreeCrownDiameter.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeCrownDiameter.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldTreeCrownDiameter.TotalCellFormat.FormatString = "#";
            this.fieldTreeCrownDiameter.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCrownDiameter.TotalValueFormat.FormatString = "#";
            this.fieldTreeCrownDiameter.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCrownDiameter.ValueFormat.FormatString = "#";
            this.fieldTreeCrownDiameter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreePlantDate
            // 
            this.fieldTreePlantDate.AreaIndex = 10;
            this.fieldTreePlantDate.Caption = "Tree Plant Date";
            this.fieldTreePlantDate.FieldName = "TreePlantDate";
            this.fieldTreePlantDate.Name = "fieldTreePlantDate";
            this.fieldTreePlantDate.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePlantDate.Visible = false;
            // 
            // fieldTreeGroupNumber
            // 
            this.fieldTreeGroupNumber.AreaIndex = 10;
            this.fieldTreeGroupNumber.Caption = "Tree Group Number";
            this.fieldTreeGroupNumber.FieldName = "TreeGroupNumber";
            this.fieldTreeGroupNumber.Name = "fieldTreeGroupNumber";
            this.fieldTreeGroupNumber.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeGroupNumber.Visible = false;
            // 
            // fieldTreeRiskCategory
            // 
            this.fieldTreeRiskCategory.AreaIndex = 4;
            this.fieldTreeRiskCategory.Caption = "Tree Risk Category";
            this.fieldTreeRiskCategory.FieldName = "TreeRiskCategory";
            this.fieldTreeRiskCategory.Name = "fieldTreeRiskCategory";
            this.fieldTreeRiskCategory.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeSiteHazardClass
            // 
            this.fieldTreeSiteHazardClass.AreaIndex = 13;
            this.fieldTreeSiteHazardClass.Caption = "Tree Site Hazard Class";
            this.fieldTreeSiteHazardClass.FieldName = "TreeSiteHazardClass";
            this.fieldTreeSiteHazardClass.Name = "fieldTreeSiteHazardClass";
            this.fieldTreeSiteHazardClass.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreePlantSize
            // 
            this.fieldTreePlantSize.AreaIndex = 12;
            this.fieldTreePlantSize.Caption = "Tree Plant Size";
            this.fieldTreePlantSize.FieldName = "TreePlantSize";
            this.fieldTreePlantSize.Name = "fieldTreePlantSize";
            this.fieldTreePlantSize.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePlantSize.Visible = false;
            // 
            // fieldTreePlantSource
            // 
            this.fieldTreePlantSource.AreaIndex = 12;
            this.fieldTreePlantSource.Caption = "Tree Plant Source";
            this.fieldTreePlantSource.FieldName = "TreePlantSource";
            this.fieldTreePlantSource.Name = "fieldTreePlantSource";
            this.fieldTreePlantSource.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePlantSource.Visible = false;
            // 
            // fieldTreePlantMethod
            // 
            this.fieldTreePlantMethod.AreaIndex = 12;
            this.fieldTreePlantMethod.Caption = "Tree Plant Method";
            this.fieldTreePlantMethod.FieldName = "TreePlantMethod";
            this.fieldTreePlantMethod.Name = "fieldTreePlantMethod";
            this.fieldTreePlantMethod.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePlantMethod.Visible = false;
            // 
            // fieldTreePostcode
            // 
            this.fieldTreePostcode.AreaIndex = 12;
            this.fieldTreePostcode.Caption = "Tree Postcode";
            this.fieldTreePostcode.FieldName = "TreePostcode";
            this.fieldTreePostcode.Name = "fieldTreePostcode";
            this.fieldTreePostcode.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePostcode.Visible = false;
            // 
            // fieldTreeMapLabel
            // 
            this.fieldTreeMapLabel.AreaIndex = 12;
            this.fieldTreeMapLabel.Caption = "Tree Map Label";
            this.fieldTreeMapLabel.FieldName = "TreeMapLabel";
            this.fieldTreeMapLabel.Name = "fieldTreeMapLabel";
            this.fieldTreeMapLabel.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeMapLabel.Visible = false;
            // 
            // fieldTreeStatus
            // 
            this.fieldTreeStatus.AreaIndex = 12;
            this.fieldTreeStatus.Caption = "Tree Status";
            this.fieldTreeStatus.FieldName = "TreeStatus";
            this.fieldTreeStatus.Name = "fieldTreeStatus";
            this.fieldTreeStatus.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeStatus.Visible = false;
            // 
            // fieldTreeSize
            // 
            this.fieldTreeSize.AreaIndex = 14;
            this.fieldTreeSize.Caption = "Tree Size";
            this.fieldTreeSize.FieldName = "TreeSize";
            this.fieldTreeSize.Name = "fieldTreeSize";
            this.fieldTreeSize.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeTarget1
            // 
            this.fieldTreeTarget1.AreaIndex = 13;
            this.fieldTreeTarget1.Caption = "Tree Target 1";
            this.fieldTreeTarget1.FieldName = "TreeTarget1";
            this.fieldTreeTarget1.Name = "fieldTreeTarget1";
            this.fieldTreeTarget1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeTarget1.Visible = false;
            // 
            // fieldTreeTarget2
            // 
            this.fieldTreeTarget2.AreaIndex = 13;
            this.fieldTreeTarget2.Caption = "Tree Target 2";
            this.fieldTreeTarget2.FieldName = "TreeTarget2";
            this.fieldTreeTarget2.Name = "fieldTreeTarget2";
            this.fieldTreeTarget2.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeTarget2.Visible = false;
            // 
            // fieldTreeTarget3
            // 
            this.fieldTreeTarget3.AreaIndex = 13;
            this.fieldTreeTarget3.Caption = "Tree Target 3";
            this.fieldTreeTarget3.FieldName = "TreeTarget3";
            this.fieldTreeTarget3.Name = "fieldTreeTarget3";
            this.fieldTreeTarget3.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeTarget3.Visible = false;
            // 
            // fieldTreeLastModifiedDate
            // 
            this.fieldTreeLastModifiedDate.AreaIndex = 37;
            this.fieldTreeLastModifiedDate.Caption = "Tree Last Modified Date";
            this.fieldTreeLastModifiedDate.FieldName = "TreeLastModifiedDate";
            this.fieldTreeLastModifiedDate.Name = "fieldTreeLastModifiedDate";
            this.fieldTreeLastModifiedDate.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeLastModifiedDate.Visible = false;
            // 
            // fieldTreeRemarks
            // 
            this.fieldTreeRemarks.AreaIndex = 37;
            this.fieldTreeRemarks.Caption = "Tree Remarks";
            this.fieldTreeRemarks.FieldName = "TreeRemarks";
            this.fieldTreeRemarks.Name = "fieldTreeRemarks";
            this.fieldTreeRemarks.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeRemarks.Visible = false;
            // 
            // fieldTreeUser1
            // 
            this.fieldTreeUser1.AreaIndex = 31;
            this.fieldTreeUser1.Caption = "Tree User 1";
            this.fieldTreeUser1.FieldName = "TreeUser1";
            this.fieldTreeUser1.Name = "fieldTreeUser1";
            this.fieldTreeUser1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeUser1.Visible = false;
            // 
            // fieldTreeUser2
            // 
            this.fieldTreeUser2.AreaIndex = 31;
            this.fieldTreeUser2.Caption = "Tree User 2";
            this.fieldTreeUser2.FieldName = "TreeUser2";
            this.fieldTreeUser2.Name = "fieldTreeUser2";
            this.fieldTreeUser2.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeUser2.Visible = false;
            // 
            // fieldTreeUser3
            // 
            this.fieldTreeUser3.AreaIndex = 31;
            this.fieldTreeUser3.Caption = "Tree User 3";
            this.fieldTreeUser3.FieldName = "TreeUser3";
            this.fieldTreeUser3.Name = "fieldTreeUser3";
            this.fieldTreeUser3.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeUser3.Visible = false;
            // 
            // fieldTreeAgeClass
            // 
            this.fieldTreeAgeClass.AreaIndex = 13;
            this.fieldTreeAgeClass.Caption = "Tree Age Class";
            this.fieldTreeAgeClass.FieldName = "TreeAgeClass";
            this.fieldTreeAgeClass.Name = "fieldTreeAgeClass";
            this.fieldTreeAgeClass.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeAgeClass.Visible = false;
            // 
            // fieldTreeX
            // 
            this.fieldTreeX.AreaIndex = 13;
            this.fieldTreeX.Caption = "Tree X";
            this.fieldTreeX.FieldName = "TreeX";
            this.fieldTreeX.Name = "fieldTreeX";
            this.fieldTreeX.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeX.Visible = false;
            // 
            // fieldTreeY
            // 
            this.fieldTreeY.AreaIndex = 13;
            this.fieldTreeY.Caption = "Tree Y";
            this.fieldTreeY.FieldName = "TreeY";
            this.fieldTreeY.Name = "fieldTreeY";
            this.fieldTreeY.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeY.Visible = false;
            // 
            // fieldTreeAreaHa
            // 
            this.fieldTreeAreaHa.AreaIndex = 13;
            this.fieldTreeAreaHa.Caption = "Tree Area [M�]";
            this.fieldTreeAreaHa.FieldName = "TreeAreaHa";
            this.fieldTreeAreaHa.Name = "fieldTreeAreaHa";
            this.fieldTreeAreaHa.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeAreaHa.Visible = false;
            // 
            // fieldTreeReplantCount
            // 
            this.fieldTreeReplantCount.AreaIndex = 13;
            this.fieldTreeReplantCount.Caption = "Tree Replant Count";
            this.fieldTreeReplantCount.FieldName = "TreeReplantCount";
            this.fieldTreeReplantCount.Name = "fieldTreeReplantCount";
            this.fieldTreeReplantCount.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeReplantCount.Visible = false;
            // 
            // fieldTreeSurveyDate
            // 
            this.fieldTreeSurveyDate.AreaIndex = 13;
            this.fieldTreeSurveyDate.Caption = "Tree Survey Date";
            this.fieldTreeSurveyDate.FieldName = "TreeSurveyDate";
            this.fieldTreeSurveyDate.Name = "fieldTreeSurveyDate";
            this.fieldTreeSurveyDate.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeSurveyDate.Visible = false;
            // 
            // fieldTreeType1
            // 
            this.fieldTreeType1.AreaIndex = 13;
            this.fieldTreeType1.Caption = "Tree Type";
            this.fieldTreeType1.FieldName = "TreeType";
            this.fieldTreeType1.Name = "fieldTreeType1";
            this.fieldTreeType1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeType1.Visible = false;
            // 
            // fieldTreeHedgeOwner
            // 
            this.fieldTreeHedgeOwner.AreaIndex = 13;
            this.fieldTreeHedgeOwner.Caption = "Tree Hedge Owner";
            this.fieldTreeHedgeOwner.FieldName = "TreeHedgeOwner";
            this.fieldTreeHedgeOwner.Name = "fieldTreeHedgeOwner";
            this.fieldTreeHedgeOwner.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeHedgeOwner.Visible = false;
            // 
            // fieldTreeHedgeLength
            // 
            this.fieldTreeHedgeLength.AreaIndex = 13;
            this.fieldTreeHedgeLength.Caption = "Tree Hedge Length";
            this.fieldTreeHedgeLength.FieldName = "TreeHedgeLength";
            this.fieldTreeHedgeLength.Name = "fieldTreeHedgeLength";
            this.fieldTreeHedgeLength.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeHedgeLength.Visible = false;
            // 
            // fieldTreeGardenSize
            // 
            this.fieldTreeGardenSize.AreaIndex = 13;
            this.fieldTreeGardenSize.Caption = "Tree Garden Size";
            this.fieldTreeGardenSize.FieldName = "TreeGardenSize";
            this.fieldTreeGardenSize.Name = "fieldTreeGardenSize";
            this.fieldTreeGardenSize.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeGardenSize.Visible = false;
            // 
            // fieldTreePropertyProximity
            // 
            this.fieldTreePropertyProximity.AreaIndex = 13;
            this.fieldTreePropertyProximity.Caption = "Tree Property Proximity";
            this.fieldTreePropertyProximity.FieldName = "TreePropertyProximity";
            this.fieldTreePropertyProximity.Name = "fieldTreePropertyProximity";
            this.fieldTreePropertyProximity.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePropertyProximity.Visible = false;
            // 
            // fieldTreeSiteLevel
            // 
            this.fieldTreeSiteLevel.AreaIndex = 13;
            this.fieldTreeSiteLevel.Caption = "Tree Site Level";
            this.fieldTreeSiteLevel.FieldName = "TreeSiteLevel";
            this.fieldTreeSiteLevel.Name = "fieldTreeSiteLevel";
            this.fieldTreeSiteLevel.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeSiteLevel.Visible = false;
            // 
            // fieldTreeSituation
            // 
            this.fieldTreeSituation.AreaIndex = 13;
            this.fieldTreeSituation.Caption = "Tree Situation";
            this.fieldTreeSituation.FieldName = "TreeSituation";
            this.fieldTreeSituation.Name = "fieldTreeSituation";
            this.fieldTreeSituation.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeSituation.Visible = false;
            // 
            // fieldTreeRiskFactor
            // 
            this.fieldTreeRiskFactor.AreaIndex = 5;
            this.fieldTreeRiskFactor.Caption = "Tree Risk Factor";
            this.fieldTreeRiskFactor.CellFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeRiskFactor.FieldName = "TreeRiskFactor";
            this.fieldTreeRiskFactor.GrandTotalCellFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeRiskFactor.Name = "fieldTreeRiskFactor";
            this.fieldTreeRiskFactor.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeRiskFactor.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldTreeRiskFactor.TotalCellFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeRiskFactor.TotalValueFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeRiskFactor.ValueFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreePolygonXY
            // 
            this.fieldTreePolygonXY.AreaIndex = 45;
            this.fieldTreePolygonXY.Caption = "Tree Polygon XY";
            this.fieldTreePolygonXY.FieldName = "TreePolygonXY";
            this.fieldTreePolygonXY.Name = "fieldTreePolygonXY";
            this.fieldTreePolygonXY.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePolygonXY.Visible = false;
            // 
            // fieldcolor1
            // 
            this.fieldcolor1.AreaIndex = 46;
            this.fieldcolor1.Caption = "Color";
            this.fieldcolor1.FieldName = "color";
            this.fieldcolor1.Name = "fieldcolor1";
            this.fieldcolor1.Options.AllowRunTimeSummaryChange = true;
            this.fieldcolor1.Visible = false;
            // 
            // fieldTreeOwnershipName
            // 
            this.fieldTreeOwnershipName.AreaIndex = 3;
            this.fieldTreeOwnershipName.Caption = "Tree Ownership Name";
            this.fieldTreeOwnershipName.FieldName = "TreeOwnershipName";
            this.fieldTreeOwnershipName.Name = "fieldTreeOwnershipName";
            this.fieldTreeOwnershipName.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionReference
            // 
            this.fieldInspectionReference.AreaIndex = 19;
            this.fieldInspectionReference.Caption = "Inspection Reference";
            this.fieldInspectionReference.FieldName = "InspectionReference";
            this.fieldInspectionReference.Name = "fieldInspectionReference";
            this.fieldInspectionReference.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionInspector
            // 
            this.fieldInspectionInspector.AreaIndex = 20;
            this.fieldInspectionInspector.Caption = "Inspection Inspector";
            this.fieldInspectionInspector.FieldName = "InspectionInspector";
            this.fieldInspectionInspector.Name = "fieldInspectionInspector";
            this.fieldInspectionInspector.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionDate
            // 
            this.fieldInspectionDate.AreaIndex = 21;
            this.fieldInspectionDate.Caption = "Inspection Date";
            this.fieldInspectionDate.FieldName = "InspectionDate";
            this.fieldInspectionDate.Name = "fieldInspectionDate";
            this.fieldInspectionDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionIncidentReference
            // 
            this.fieldInspectionIncidentReference.AreaIndex = 23;
            this.fieldInspectionIncidentReference.Caption = "Inspection Incident Reference";
            this.fieldInspectionIncidentReference.FieldName = "InspectionIncidentReference";
            this.fieldInspectionIncidentReference.Name = "fieldInspectionIncidentReference";
            this.fieldInspectionIncidentReference.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionIncidentDate
            // 
            this.fieldInspectionIncidentDate.AreaIndex = 24;
            this.fieldInspectionIncidentDate.Caption = "Inspection Incident Date";
            this.fieldInspectionIncidentDate.FieldName = "InspectionIncidentDate";
            this.fieldInspectionIncidentDate.Name = "fieldInspectionIncidentDate";
            this.fieldInspectionIncidentDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldIncidentID
            // 
            this.fieldIncidentID.AreaIndex = 51;
            this.fieldIncidentID.Caption = "Incident ID";
            this.fieldIncidentID.FieldName = "IncidentID";
            this.fieldIncidentID.Name = "fieldIncidentID";
            this.fieldIncidentID.Options.AllowRunTimeSummaryChange = true;
            this.fieldIncidentID.Visible = false;
            // 
            // fieldInspectionAngleToVertical
            // 
            this.fieldInspectionAngleToVertical.AreaIndex = 25;
            this.fieldInspectionAngleToVertical.Caption = "Inspection Angle To Vertical";
            this.fieldInspectionAngleToVertical.FieldName = "InspectionAngleToVertical";
            this.fieldInspectionAngleToVertical.Name = "fieldInspectionAngleToVertical";
            this.fieldInspectionAngleToVertical.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionLastModified
            // 
            this.fieldInspectionLastModified.AreaIndex = 64;
            this.fieldInspectionLastModified.Caption = "Inspection Last Modified";
            this.fieldInspectionLastModified.FieldName = "InspectionLastModified";
            this.fieldInspectionLastModified.Name = "fieldInspectionLastModified";
            this.fieldInspectionLastModified.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionLastModified.Visible = false;
            // 
            // fieldInspectionUserDefined1
            // 
            this.fieldInspectionUserDefined1.AreaIndex = 52;
            this.fieldInspectionUserDefined1.Caption = "Inspection User Defined 1";
            this.fieldInspectionUserDefined1.FieldName = "InspectionUserDefined1";
            this.fieldInspectionUserDefined1.Name = "fieldInspectionUserDefined1";
            this.fieldInspectionUserDefined1.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionUserDefined1.Visible = false;
            // 
            // fieldInspectionUserDefined2
            // 
            this.fieldInspectionUserDefined2.AreaIndex = 52;
            this.fieldInspectionUserDefined2.Caption = "Inspection User Defined 2";
            this.fieldInspectionUserDefined2.FieldName = "InspectionUserDefined2";
            this.fieldInspectionUserDefined2.Name = "fieldInspectionUserDefined2";
            this.fieldInspectionUserDefined2.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionUserDefined2.Visible = false;
            // 
            // fieldInspectionUserDefined3
            // 
            this.fieldInspectionUserDefined3.AreaIndex = 52;
            this.fieldInspectionUserDefined3.Caption = "Inspection User Defined 3";
            this.fieldInspectionUserDefined3.FieldName = "InspectionUserDefined3";
            this.fieldInspectionUserDefined3.Name = "fieldInspectionUserDefined3";
            this.fieldInspectionUserDefined3.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionUserDefined3.Visible = false;
            // 
            // fieldInspectionRemarks
            // 
            this.fieldInspectionRemarks.AreaIndex = 68;
            this.fieldInspectionRemarks.Caption = "Inspection Remarks";
            this.fieldInspectionRemarks.FieldName = "InspectionRemarks";
            this.fieldInspectionRemarks.Name = "fieldInspectionRemarks";
            this.fieldInspectionRemarks.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionRemarks.Visible = false;
            // 
            // fieldInspectionStemPhysical1
            // 
            this.fieldInspectionStemPhysical1.AreaIndex = 26;
            this.fieldInspectionStemPhysical1.Caption = "Inspection Stem Physical 1";
            this.fieldInspectionStemPhysical1.FieldName = "InspectionStemPhysical1";
            this.fieldInspectionStemPhysical1.Name = "fieldInspectionStemPhysical1";
            this.fieldInspectionStemPhysical1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemPhysical2
            // 
            this.fieldInspectionStemPhysical2.AreaIndex = 27;
            this.fieldInspectionStemPhysical2.Caption = "Inspection Stem Physical 2";
            this.fieldInspectionStemPhysical2.FieldName = "InspectionStemPhysical2";
            this.fieldInspectionStemPhysical2.Name = "fieldInspectionStemPhysical2";
            this.fieldInspectionStemPhysical2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemPhysical3
            // 
            this.fieldInspectionStemPhysical3.AreaIndex = 28;
            this.fieldInspectionStemPhysical3.Caption = "Inspection Stem Physical 3";
            this.fieldInspectionStemPhysical3.FieldName = "InspectionStemPhysical3";
            this.fieldInspectionStemPhysical3.Name = "fieldInspectionStemPhysical3";
            this.fieldInspectionStemPhysical3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemDisease1
            // 
            this.fieldInspectionStemDisease1.AreaIndex = 29;
            this.fieldInspectionStemDisease1.Caption = "Inspection Stem Disease 1";
            this.fieldInspectionStemDisease1.FieldName = "InspectionStemDisease1";
            this.fieldInspectionStemDisease1.Name = "fieldInspectionStemDisease1";
            this.fieldInspectionStemDisease1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemDisease2
            // 
            this.fieldInspectionStemDisease2.AreaIndex = 30;
            this.fieldInspectionStemDisease2.Caption = "Inspection Stem Disease 2";
            this.fieldInspectionStemDisease2.FieldName = "InspectionStemDisease2";
            this.fieldInspectionStemDisease2.Name = "fieldInspectionStemDisease2";
            this.fieldInspectionStemDisease2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemDisease3
            // 
            this.fieldInspectionStemDisease3.AreaIndex = 31;
            this.fieldInspectionStemDisease3.Caption = "Inspection Stem Disease 3";
            this.fieldInspectionStemDisease3.FieldName = "InspectionStemDisease3";
            this.fieldInspectionStemDisease3.Name = "fieldInspectionStemDisease3";
            this.fieldInspectionStemDisease3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownPhysical1
            // 
            this.fieldInspectionCrownPhysical1.AreaIndex = 32;
            this.fieldInspectionCrownPhysical1.Caption = "Inspection Crown Physical 1";
            this.fieldInspectionCrownPhysical1.FieldName = "InspectionCrownPhysical1";
            this.fieldInspectionCrownPhysical1.Name = "fieldInspectionCrownPhysical1";
            this.fieldInspectionCrownPhysical1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownPhysical2
            // 
            this.fieldInspectionCrownPhysical2.AreaIndex = 33;
            this.fieldInspectionCrownPhysical2.Caption = "Inspection Crown Physical 2";
            this.fieldInspectionCrownPhysical2.FieldName = "InspectionCrownPhysical2";
            this.fieldInspectionCrownPhysical2.Name = "fieldInspectionCrownPhysical2";
            this.fieldInspectionCrownPhysical2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownPhysical3
            // 
            this.fieldInspectionCrownPhysical3.AreaIndex = 34;
            this.fieldInspectionCrownPhysical3.Caption = "Inspection Crown Physical 3";
            this.fieldInspectionCrownPhysical3.FieldName = "InspectionCrownPhysical3";
            this.fieldInspectionCrownPhysical3.Name = "fieldInspectionCrownPhysical3";
            this.fieldInspectionCrownPhysical3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownDisease1
            // 
            this.fieldInspectionCrownDisease1.AreaIndex = 35;
            this.fieldInspectionCrownDisease1.Caption = "Inspection Crown Disease 1";
            this.fieldInspectionCrownDisease1.FieldName = "InspectionCrownDisease1";
            this.fieldInspectionCrownDisease1.Name = "fieldInspectionCrownDisease1";
            this.fieldInspectionCrownDisease1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownDisease2
            // 
            this.fieldInspectionCrownDisease2.AreaIndex = 36;
            this.fieldInspectionCrownDisease2.Caption = "Inspection Crown Disease 2";
            this.fieldInspectionCrownDisease2.FieldName = "InspectionCrownDisease2";
            this.fieldInspectionCrownDisease2.Name = "fieldInspectionCrownDisease2";
            this.fieldInspectionCrownDisease2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownDisease3
            // 
            this.fieldInspectionCrownDisease3.AreaIndex = 37;
            this.fieldInspectionCrownDisease3.Caption = "Inspection Crown Disease 3";
            this.fieldInspectionCrownDisease3.FieldName = "InspectionCrownDisease3";
            this.fieldInspectionCrownDisease3.Name = "fieldInspectionCrownDisease3";
            this.fieldInspectionCrownDisease3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownFoliation1
            // 
            this.fieldInspectionCrownFoliation1.AreaIndex = 38;
            this.fieldInspectionCrownFoliation1.Caption = "Inspection Crown Foliation 1";
            this.fieldInspectionCrownFoliation1.FieldName = "InspectionCrownFoliation1";
            this.fieldInspectionCrownFoliation1.Name = "fieldInspectionCrownFoliation1";
            this.fieldInspectionCrownFoliation1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownFoliation2
            // 
            this.fieldInspectionCrownFoliation2.AreaIndex = 39;
            this.fieldInspectionCrownFoliation2.Caption = "Inspection Crown Foliation 2";
            this.fieldInspectionCrownFoliation2.FieldName = "InspectionCrownFoliation2";
            this.fieldInspectionCrownFoliation2.Name = "fieldInspectionCrownFoliation2";
            this.fieldInspectionCrownFoliation2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownFoliation3
            // 
            this.fieldInspectionCrownFoliation3.AreaIndex = 40;
            this.fieldInspectionCrownFoliation3.Caption = "Inspection Crown Foliation 3";
            this.fieldInspectionCrownFoliation3.FieldName = "InspectionCrownFoliation3";
            this.fieldInspectionCrownFoliation3.Name = "fieldInspectionCrownFoliation3";
            this.fieldInspectionCrownFoliation3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionRiskCategory
            // 
            this.fieldInspectionRiskCategory.AreaIndex = 41;
            this.fieldInspectionRiskCategory.Caption = "Inspection Risk Category";
            this.fieldInspectionRiskCategory.FieldName = "InspectionRiskCategory";
            this.fieldInspectionRiskCategory.Name = "fieldInspectionRiskCategory";
            this.fieldInspectionRiskCategory.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionGeneralCondition
            // 
            this.fieldInspectionGeneralCondition.AreaIndex = 42;
            this.fieldInspectionGeneralCondition.Caption = "Inspection General Condition";
            this.fieldInspectionGeneralCondition.FieldName = "InspectionGeneralCondition";
            this.fieldInspectionGeneralCondition.Name = "fieldInspectionGeneralCondition";
            this.fieldInspectionGeneralCondition.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionBasePhysical1
            // 
            this.fieldInspectionBasePhysical1.AreaIndex = 43;
            this.fieldInspectionBasePhysical1.Caption = "Inspection Base Physical 1";
            this.fieldInspectionBasePhysical1.FieldName = "InspectionBasePhysical1";
            this.fieldInspectionBasePhysical1.Name = "fieldInspectionBasePhysical1";
            this.fieldInspectionBasePhysical1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionBasePhysical2
            // 
            this.fieldInspectionBasePhysical2.AreaIndex = 44;
            this.fieldInspectionBasePhysical2.Caption = "Inspection Base Physical 2";
            this.fieldInspectionBasePhysical2.FieldName = "InspectionBasePhysical2";
            this.fieldInspectionBasePhysical2.Name = "fieldInspectionBasePhysical2";
            this.fieldInspectionBasePhysical2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionBasePhysical3
            // 
            this.fieldInspectionBasePhysical3.AreaIndex = 45;
            this.fieldInspectionBasePhysical3.Caption = "Inspection Base Physical 3";
            this.fieldInspectionBasePhysical3.FieldName = "InspectionBasePhysical3";
            this.fieldInspectionBasePhysical3.Name = "fieldInspectionBasePhysical3";
            this.fieldInspectionBasePhysical3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionVitality
            // 
            this.fieldInspectionVitality.AreaIndex = 46;
            this.fieldInspectionVitality.Caption = "Inspection Vitality";
            this.fieldInspectionVitality.FieldName = "InspectionVitality";
            this.fieldInspectionVitality.Name = "fieldInspectionVitality";
            this.fieldInspectionVitality.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCount
            // 
            this.fieldInspectionCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldInspectionCount.AreaIndex = 0;
            this.fieldInspectionCount.Caption = "Inspection Count";
            this.fieldInspectionCount.FieldName = "InspectionCount";
            this.fieldInspectionCount.Name = "fieldInspectionCount";
            this.fieldInspectionCount.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldLinkedActionCount
            // 
            this.fieldLinkedActionCount.AreaIndex = 41;
            this.fieldLinkedActionCount.Caption = "Inspection Action Count";
            this.fieldLinkedActionCount.FieldName = "LinkedActionCount";
            this.fieldLinkedActionCount.Name = "fieldLinkedActionCount";
            this.fieldLinkedActionCount.Options.AllowRunTimeSummaryChange = true;
            this.fieldLinkedActionCount.Visible = false;
            // 
            // fieldLinkedOutstandingActionCount
            // 
            this.fieldLinkedOutstandingActionCount.AreaIndex = 41;
            this.fieldLinkedOutstandingActionCount.Caption = "Inspection Outstanding Action Count";
            this.fieldLinkedOutstandingActionCount.FieldName = "LinkedOutstandingActionCount";
            this.fieldLinkedOutstandingActionCount.Name = "fieldLinkedOutstandingActionCount";
            this.fieldLinkedOutstandingActionCount.Options.AllowRunTimeSummaryChange = true;
            this.fieldLinkedOutstandingActionCount.Visible = false;
            // 
            // fieldNoFurtherActionRequired
            // 
            this.fieldNoFurtherActionRequired.AreaIndex = 41;
            this.fieldNoFurtherActionRequired.Caption = "Inspection No Further Action Required";
            this.fieldNoFurtherActionRequired.FieldName = "NoFurtherActionRequired";
            this.fieldNoFurtherActionRequired.Name = "fieldNoFurtherActionRequired";
            this.fieldNoFurtherActionRequired.Options.AllowRunTimeSummaryChange = true;
            this.fieldNoFurtherActionRequired.Visible = false;
            // 
            // fieldTreeCavat1
            // 
            this.fieldTreeCavat1.AreaIndex = 15;
            this.fieldTreeCavat1.Caption = "Tree Cavat";
            this.fieldTreeCavat1.CellFormat.FormatString = "c";
            this.fieldTreeCavat1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat1.FieldName = "TreeCavat";
            this.fieldTreeCavat1.GrandTotalCellFormat.FormatString = "c";
            this.fieldTreeCavat1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat1.Name = "fieldTreeCavat1";
            this.fieldTreeCavat1.TotalCellFormat.FormatString = "c";
            this.fieldTreeCavat1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat1.TotalValueFormat.FormatString = "c";
            this.fieldTreeCavat1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat1.ValueFormat.FormatString = "c";
            this.fieldTreeCavat1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreeStemCount1
            // 
            this.fieldTreeStemCount1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeStemCount1.AreaIndex = 0;
            this.fieldTreeStemCount1.Caption = "Tree Stem Count";
            this.fieldTreeStemCount1.FieldName = "TreeStemCount";
            this.fieldTreeStemCount1.Name = "fieldTreeStemCount1";
            this.fieldTreeStemCount1.Visible = false;
            // 
            // fieldTreeCrownNorth1
            // 
            this.fieldTreeCrownNorth1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownNorth1.AreaIndex = 0;
            this.fieldTreeCrownNorth1.Caption = "Tree Crown North";
            this.fieldTreeCrownNorth1.FieldName = "TreeCrownNorth";
            this.fieldTreeCrownNorth1.Name = "fieldTreeCrownNorth1";
            this.fieldTreeCrownNorth1.Visible = false;
            // 
            // fieldTreeCrownSouth1
            // 
            this.fieldTreeCrownSouth1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownSouth1.AreaIndex = 0;
            this.fieldTreeCrownSouth1.Caption = "Tree Crown South";
            this.fieldTreeCrownSouth1.FieldName = "TreeCrownSouth";
            this.fieldTreeCrownSouth1.Name = "fieldTreeCrownSouth1";
            this.fieldTreeCrownSouth1.Visible = false;
            // 
            // fieldTreeCrownEast1
            // 
            this.fieldTreeCrownEast1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownEast1.AreaIndex = 0;
            this.fieldTreeCrownEast1.Caption = "Tree Crown East";
            this.fieldTreeCrownEast1.FieldName = "TreeCrownEast";
            this.fieldTreeCrownEast1.Name = "fieldTreeCrownEast1";
            this.fieldTreeCrownEast1.Visible = false;
            // 
            // fieldTreeCrownWest1
            // 
            this.fieldTreeCrownWest1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownWest1.AreaIndex = 0;
            this.fieldTreeCrownWest1.Caption = "Tree Crown West";
            this.fieldTreeCrownWest1.FieldName = "TreeCrownWest";
            this.fieldTreeCrownWest1.Name = "fieldTreeCrownWest1";
            this.fieldTreeCrownWest1.Visible = false;
            // 
            // fieldTreeRetentionCategory1
            // 
            this.fieldTreeRetentionCategory1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeRetentionCategory1.AreaIndex = 1;
            this.fieldTreeRetentionCategory1.Caption = "Tree Retention Category";
            this.fieldTreeRetentionCategory1.FieldName = "TreeRetentionCategory";
            this.fieldTreeRetentionCategory1.Name = "fieldTreeRetentionCategory1";
            this.fieldTreeRetentionCategory1.Visible = false;
            // 
            // fieldTreeSULE1
            // 
            this.fieldTreeSULE1.AreaIndex = 16;
            this.fieldTreeSULE1.Caption = "Tree SULE";
            this.fieldTreeSULE1.FieldName = "TreeSULE";
            this.fieldTreeSULE1.Name = "fieldTreeSULE1";
            // 
            // fieldTreeSpeciesVariety1
            // 
            this.fieldTreeSpeciesVariety1.AreaIndex = 7;
            this.fieldTreeSpeciesVariety1.Caption = "Tree Species Variety";
            this.fieldTreeSpeciesVariety1.FieldName = "TreeSpeciesVariety";
            this.fieldTreeSpeciesVariety1.Name = "fieldTreeSpeciesVariety1";
            // 
            // fieldTreeUserPicklist11
            // 
            this.fieldTreeUserPicklist11.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeUserPicklist11.AreaIndex = 0;
            this.fieldTreeUserPicklist11.Caption = "Tree User Picklist 1";
            this.fieldTreeUserPicklist11.FieldName = "TreeUserPicklist1";
            this.fieldTreeUserPicklist11.Name = "fieldTreeUserPicklist11";
            this.fieldTreeUserPicklist11.Visible = false;
            // 
            // fieldTreeUserPicklist21
            // 
            this.fieldTreeUserPicklist21.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeUserPicklist21.AreaIndex = 0;
            this.fieldTreeUserPicklist21.Caption = "Tree User Picklist 2";
            this.fieldTreeUserPicklist21.FieldName = "TreeUserPicklist2";
            this.fieldTreeUserPicklist21.Name = "fieldTreeUserPicklist21";
            this.fieldTreeUserPicklist21.Visible = false;
            // 
            // fieldTreeUserPicklist31
            // 
            this.fieldTreeUserPicklist31.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeUserPicklist31.AreaIndex = 0;
            this.fieldTreeUserPicklist31.Caption = "Tree User Picklist 3";
            this.fieldTreeUserPicklist31.FieldName = "TreeUserPicklist3";
            this.fieldTreeUserPicklist31.Name = "fieldTreeUserPicklist31";
            this.fieldTreeUserPicklist31.Visible = false;
            // 
            // fieldTreeObjectLength1
            // 
            this.fieldTreeObjectLength1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeObjectLength1.AreaIndex = 0;
            this.fieldTreeObjectLength1.Caption = "Tree Object Length";
            this.fieldTreeObjectLength1.FieldName = "TreeObjectLength";
            this.fieldTreeObjectLength1.Name = "fieldTreeObjectLength1";
            this.fieldTreeObjectLength1.Visible = false;
            // 
            // fieldTreeObjectWidth1
            // 
            this.fieldTreeObjectWidth1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeObjectWidth1.AreaIndex = 0;
            this.fieldTreeObjectWidth1.Caption = "Tree Object Width";
            this.fieldTreeObjectWidth1.FieldName = "TreeObjectWidth";
            this.fieldTreeObjectWidth1.Name = "fieldTreeObjectWidth1";
            this.fieldTreeObjectWidth1.Visible = false;
            // 
            // fieldInspectionUserPicklist1
            // 
            this.fieldInspectionUserPicklist1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldInspectionUserPicklist1.AreaIndex = 0;
            this.fieldInspectionUserPicklist1.Caption = "Inspection User Picklist 1";
            this.fieldInspectionUserPicklist1.FieldName = "InspectionUserPicklist1";
            this.fieldInspectionUserPicklist1.Name = "fieldInspectionUserPicklist1";
            this.fieldInspectionUserPicklist1.Visible = false;
            // 
            // fieldInspectionUserPicklist2
            // 
            this.fieldInspectionUserPicklist2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldInspectionUserPicklist2.AreaIndex = 0;
            this.fieldInspectionUserPicklist2.Caption = "Inspection User Picklist 2";
            this.fieldInspectionUserPicklist2.FieldName = "InspectionUserPicklist2";
            this.fieldInspectionUserPicklist2.Name = "fieldInspectionUserPicklist2";
            this.fieldInspectionUserPicklist2.Visible = false;
            // 
            // fieldInspectionUserPicklist3
            // 
            this.fieldInspectionUserPicklist3.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldInspectionUserPicklist3.AreaIndex = 0;
            this.fieldInspectionUserPicklist3.Caption = "Inspection User Picklist 3";
            this.fieldInspectionUserPicklist3.FieldName = "InspectionUserPicklist3";
            this.fieldInspectionUserPicklist3.Name = "fieldInspectionUserPicklist3";
            this.fieldInspectionUserPicklist3.Visible = false;
            // 
            // chartControl2
            // 
            this.chartControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl2.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl2.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl2.EmptyChartText.Text = "No Data To Graph - Try Selecting Data from the Analysis Grid.";
            this.chartControl2.Legend.Name = "Default Legend";
            this.chartControl2.Location = new System.Drawing.Point(0, 0);
            this.chartControl2.Name = "chartControl2";
            this.chartControl2.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            sideBySideBarSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl2.SeriesTemplate.Label = sideBySideBarSeriesLabel2;
            this.chartControl2.Size = new System.Drawing.Size(949, 379);
            this.chartControl2.TabIndex = 1;
            this.chartControl2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chartControl2_MouseUp);
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.splitContainerControl3);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(949, 824);
            this.xtraTabPage6.Text = "Action Analysis";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.pivotGridControl3);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.chartControl3);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(949, 824);
            this.splitContainerControl3.SplitterPosition = 439;
            this.splitContainerControl3.TabIndex = 9;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // pivotGridControl3
            // 
            this.pivotGridControl3.ActiveFilterString = "";
            this.pivotGridControl3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl3.DataSource = this.sp01236ATDataAnalysisActionsBindingSource;
            this.pivotGridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl3.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldInspectionID1,
            this.fieldTreeID2,
            this.fieldSiteID2,
            this.fieldClientID2,
            this.fieldSiteName2,
            this.fieldClientName3,
            this.fieldTreeReference2,
            this.fieldTreeMappingID1,
            this.fieldTreeGridReference1,
            this.fieldTreeDistance1,
            this.fieldTreeHouseName1,
            this.fieldTreeSpeciesName1,
            this.fieldTreeAccess1,
            this.fieldTreeVisibility1,
            this.fieldTreeContext1,
            this.fieldTreeManagement1,
            this.fieldTreeLegalStatus1,
            this.fieldTreeLastInspectionDate1,
            this.fieldTreeInspectionCycle1,
            this.fieldTreeInspectionUnit1,
            this.fieldTreeNextInspectionDate1,
            this.fieldTreeGroundType1,
            this.fieldTreeDBH1,
            this.fieldTreeDBHRange1,
            this.fieldTreeHeight1,
            this.fieldTreeHeightRange1,
            this.fieldTreeProtectionType1,
            this.fieldTreeCrownDiameter1,
            this.fieldTreePlantDate1,
            this.fieldTreeGroupNumber1,
            this.fieldTreeRiskCategory1,
            this.fieldTreeSiteHazardClass1,
            this.fieldTreePlantSize1,
            this.fieldTreePlantSource1,
            this.fieldTreePlantMethod1,
            this.fieldTreePostcode1,
            this.fieldTreeMapLabel1,
            this.fieldTreeStatus1,
            this.fieldTreeSize1,
            this.fieldTreeTarget11,
            this.fieldTreeTarget21,
            this.fieldTreeTarget31,
            this.fieldTreeLastModifiedDate1,
            this.fieldTreeRemarks1,
            this.fieldTreeUser11,
            this.fieldTreeUser21,
            this.fieldTreeUser31,
            this.fieldTreeAgeClass1,
            this.fieldTreeX1,
            this.fieldTreeY1,
            this.fieldTreeAreaHa1,
            this.fieldTreeReplantCount1,
            this.fieldTreeSurveyDate1,
            this.fieldTreeType2,
            this.fieldTreeHedgeOwner1,
            this.fieldTreeHedgeLength1,
            this.fieldTreeGardenSize1,
            this.fieldTreePropertyProximity1,
            this.fieldTreeSiteLevel1,
            this.fieldTreeSituation1,
            this.fieldTreeRiskFactor1,
            this.fieldTreePolygonXY1,
            this.fieldcolor2,
            this.fieldTreeOwnershipName1,
            this.fieldInspectionReference1,
            this.fieldInspectionInspector1,
            this.fieldInspectionDate1,
            this.fieldInspectionIncidentReference1,
            this.fieldInspectionIncidentDate1,
            this.fieldIncidentID1,
            this.fieldInspectionAngleToVertical1,
            this.fieldInspectionLastModified1,
            this.fieldInspectionUserDefined11,
            this.fieldInspectionUserDefined21,
            this.fieldInspectionUserDefined31,
            this.fieldInspectionRemarks1,
            this.fieldInspectionStemPhysical11,
            this.fieldInspectionStemPhysical21,
            this.fieldInspectionStemPhysical31,
            this.fieldInspectionStemDisease11,
            this.fieldInspectionStemDisease21,
            this.fieldInspectionStemDisease31,
            this.fieldInspectionCrownPhysical11,
            this.fieldInspectionCrownPhysical21,
            this.fieldInspectionCrownPhysical31,
            this.fieldInspectionCrownDisease11,
            this.fieldInspectionCrownDisease21,
            this.fieldInspectionCrownDisease31,
            this.fieldInspectionCrownFoliation11,
            this.fieldInspectionCrownFoliation21,
            this.fieldInspectionCrownFoliation31,
            this.fieldInspectionRiskCategory1,
            this.fieldInspectionGeneralCondition1,
            this.fieldInspectionBasePhysical11,
            this.fieldInspectionBasePhysical21,
            this.fieldInspectionBasePhysical31,
            this.fieldInspectionVitality1,
            this.fieldActionID,
            this.fieldActionJobNumber,
            this.fieldAction,
            this.fieldActionPriority,
            this.fieldActionDueDate,
            this.fieldActionDoneDate,
            this.fieldActionBy,
            this.fieldActionSupervisor,
            this.fieldActionOwnership,
            this.fieldActionCostCentre,
            this.fieldActionBudget,
            this.fieldActionWorkUnits,
            this.fieldActionScheduleOfRates,
            this.fieldActionBudgetedRateDescription,
            this.fieldActionBudgetedRate,
            this.fieldActionBudgetedCost,
            this.fieldActionActualRateDescription,
            this.fieldActionActualRate,
            this.fieldActionActualCost,
            this.fieldActionDateLastModified,
            this.fieldActionWorkOrder,
            this.fieldActionRemarks,
            this.fieldActionUserDefined1,
            this.fieldActionUserDefined2,
            this.fieldActionUserDefined3,
            this.fieldActionCostDifference,
            this.fieldActionCount,
            this.calculatedTreeLastInspectionYear3,
            this.calculatedTreeLastInspectionQuarter3,
            this.calculatedTreeLastInspectionMonth3,
            this.calculatedTreeNextInspectionYear3,
            this.calculatedTreeNextInspectionQuarter3,
            this.calculatedTreeNextInspectionMonth3,
            this.InspectionYear2,
            this.InspectionQuarter2,
            this.InspectionMonth2,
            this.ActionDueYear,
            this.ActionDueQuarter,
            this.ActionDueMonth,
            this.ActionDoneYear,
            this.ActionDoneQuarter,
            this.ActionDoneMonth,
            this.fieldActionCompletionStatus,
            this.fieldActionTimeliness,
            this.fieldActionWorkOrderCompleteDate,
            this.fieldActionWorkOrderIssueDate,
            this.calculatedActionWorkOrderCompleteYear,
            this.calculatedActionWorkOrderCompleteQuarter,
            this.calculatedActionWorkOrderCompleteMonth,
            this.calculatedActionWorkOrderIssueYear,
            this.calculatedActionWorkOrderIssueQuarter,
            this.calculatedActionWorkOrderIssueMonth,
            this.fieldTreeCavat2,
            this.fieldTreeStemCount2,
            this.fieldTreeCrownNorth2,
            this.fieldTreeCrownSouth2,
            this.fieldTreeCrownEast2,
            this.fieldTreeCrownWest2,
            this.fieldTreeRetentionCategory2,
            this.fieldTreeSULE2,
            this.fieldTreeSpeciesVariety2,
            this.fieldTreeObjectLength2,
            this.fieldTreeObjectWidth2,
            this.fieldTreeUserPicklist12,
            this.fieldTreeUserPicklist22,
            this.fieldTreeUserPicklist32,
            this.fieldInspectionUserPicklist11,
            this.fieldInspectionUserPicklist21,
            this.fieldInspectionUserPicklist31,
            this.fieldActionUserPicklist1,
            this.fieldActionUserPicklist2,
            this.fieldActionUserPicklist3});
            pivotGridGroup7.Caption = "Tree Last Inspection Year - Quarter - Month";
            pivotGridGroup7.Fields.Add(this.calculatedTreeLastInspectionYear3);
            pivotGridGroup7.Fields.Add(this.calculatedTreeLastInspectionQuarter3);
            pivotGridGroup7.Fields.Add(this.calculatedTreeLastInspectionMonth3);
            pivotGridGroup7.Hierarchy = null;
            pivotGridGroup7.ShowNewValues = true;
            pivotGridGroup8.Caption = "Tree Next Inspection Year - Quarter - Month";
            pivotGridGroup8.Fields.Add(this.calculatedTreeNextInspectionYear3);
            pivotGridGroup8.Fields.Add(this.calculatedTreeNextInspectionQuarter3);
            pivotGridGroup8.Fields.Add(this.calculatedTreeNextInspectionMonth3);
            pivotGridGroup8.Hierarchy = null;
            pivotGridGroup8.ShowNewValues = true;
            pivotGridGroup9.Caption = "Inspection Year - Quarter - Month";
            pivotGridGroup9.Fields.Add(this.InspectionYear2);
            pivotGridGroup9.Fields.Add(this.InspectionQuarter2);
            pivotGridGroup9.Fields.Add(this.InspectionMonth2);
            pivotGridGroup9.Hierarchy = null;
            pivotGridGroup9.ShowNewValues = true;
            pivotGridGroup10.Caption = "Action Due Year - Quarter - Month";
            pivotGridGroup10.Fields.Add(this.ActionDueYear);
            pivotGridGroup10.Fields.Add(this.ActionDueQuarter);
            pivotGridGroup10.Fields.Add(this.ActionDueMonth);
            pivotGridGroup10.Hierarchy = null;
            pivotGridGroup10.ShowNewValues = true;
            pivotGridGroup11.Caption = "Action Done Year - Quarter - Month";
            pivotGridGroup11.Fields.Add(this.ActionDoneYear);
            pivotGridGroup11.Fields.Add(this.ActionDoneQuarter);
            pivotGridGroup11.Fields.Add(this.ActionDoneMonth);
            pivotGridGroup11.Hierarchy = null;
            pivotGridGroup11.ShowNewValues = true;
            pivotGridGroup12.Caption = "Action Work Order Complete Year - Quarter - Month";
            pivotGridGroup12.Fields.Add(this.calculatedActionWorkOrderCompleteYear);
            pivotGridGroup12.Fields.Add(this.calculatedActionWorkOrderCompleteQuarter);
            pivotGridGroup12.Fields.Add(this.calculatedActionWorkOrderCompleteMonth);
            pivotGridGroup12.Hierarchy = null;
            pivotGridGroup12.ShowNewValues = true;
            pivotGridGroup13.Caption = "Action Work Order Issue Year - Quarter - Month";
            pivotGridGroup13.Fields.Add(this.calculatedActionWorkOrderIssueYear);
            pivotGridGroup13.Fields.Add(this.calculatedActionWorkOrderIssueQuarter);
            pivotGridGroup13.Fields.Add(this.calculatedActionWorkOrderIssueMonth);
            pivotGridGroup13.Hierarchy = null;
            pivotGridGroup13.ShowNewValues = true;
            this.pivotGridControl3.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup7,
            pivotGridGroup8,
            pivotGridGroup9,
            pivotGridGroup10,
            pivotGridGroup11,
            pivotGridGroup12,
            pivotGridGroup13});
            this.pivotGridControl3.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl3.MenuManager = this.barManager1;
            this.pivotGridControl3.Name = "pivotGridControl3";
            this.pivotGridControl3.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pivotGridControl3.Size = new System.Drawing.Size(949, 439);
            this.pivotGridControl3.TabIndex = 0;
            this.pivotGridControl3.PopupMenuShowing += new DevExpress.XtraPivotGrid.PopupMenuShowingEventHandler(this.pivotGridControl3_PopupMenuShowing);
            this.pivotGridControl3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pivotGridControl3_MouseUp);
            // 
            // sp01236ATDataAnalysisActionsBindingSource
            // 
            this.sp01236ATDataAnalysisActionsBindingSource.DataMember = "sp01236_AT_Data_Analysis_Actions";
            this.sp01236ATDataAnalysisActionsBindingSource.DataSource = this.dataSet_AT_Reports;
            // 
            // fieldInspectionID1
            // 
            this.fieldInspectionID1.AreaIndex = 0;
            this.fieldInspectionID1.Caption = "Inspection ID";
            this.fieldInspectionID1.FieldName = "InspectionID";
            this.fieldInspectionID1.Name = "fieldInspectionID1";
            this.fieldInspectionID1.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionID1.Visible = false;
            // 
            // fieldTreeID2
            // 
            this.fieldTreeID2.AreaIndex = 0;
            this.fieldTreeID2.Caption = "Tree ID";
            this.fieldTreeID2.FieldName = "TreeID";
            this.fieldTreeID2.Name = "fieldTreeID2";
            this.fieldTreeID2.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeID2.Visible = false;
            // 
            // fieldSiteID2
            // 
            this.fieldSiteID2.AreaIndex = 0;
            this.fieldSiteID2.Caption = "Site ID";
            this.fieldSiteID2.FieldName = "SiteID";
            this.fieldSiteID2.Name = "fieldSiteID2";
            this.fieldSiteID2.Options.AllowRunTimeSummaryChange = true;
            this.fieldSiteID2.Visible = false;
            // 
            // fieldClientID2
            // 
            this.fieldClientID2.AreaIndex = 0;
            this.fieldClientID2.Caption = "Client ID";
            this.fieldClientID2.FieldName = "ClientID";
            this.fieldClientID2.Name = "fieldClientID2";
            this.fieldClientID2.Options.AllowRunTimeSummaryChange = true;
            this.fieldClientID2.Visible = false;
            // 
            // fieldSiteName2
            // 
            this.fieldSiteName2.AreaIndex = 1;
            this.fieldSiteName2.Caption = "Site Name";
            this.fieldSiteName2.FieldName = "SiteName";
            this.fieldSiteName2.Name = "fieldSiteName2";
            this.fieldSiteName2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldClientName3
            // 
            this.fieldClientName3.AreaIndex = 0;
            this.fieldClientName3.Caption = "Client Name";
            this.fieldClientName3.FieldName = "ClientName";
            this.fieldClientName3.Name = "fieldClientName3";
            this.fieldClientName3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeReference2
            // 
            this.fieldTreeReference2.AreaIndex = 4;
            this.fieldTreeReference2.Caption = "Tree Reference";
            this.fieldTreeReference2.FieldName = "TreeReference";
            this.fieldTreeReference2.Name = "fieldTreeReference2";
            this.fieldTreeReference2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeMappingID1
            // 
            this.fieldTreeMappingID1.AreaIndex = 3;
            this.fieldTreeMappingID1.Caption = "Tree Mapping ID";
            this.fieldTreeMappingID1.FieldName = "TreeMappingID";
            this.fieldTreeMappingID1.Name = "fieldTreeMappingID1";
            this.fieldTreeMappingID1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeMappingID1.Visible = false;
            // 
            // fieldTreeGridReference1
            // 
            this.fieldTreeGridReference1.AreaIndex = 3;
            this.fieldTreeGridReference1.Caption = "Tree Grid Reference";
            this.fieldTreeGridReference1.FieldName = "TreeGridReference";
            this.fieldTreeGridReference1.Name = "fieldTreeGridReference1";
            this.fieldTreeGridReference1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeGridReference1.Visible = false;
            // 
            // fieldTreeDistance1
            // 
            this.fieldTreeDistance1.AreaIndex = 3;
            this.fieldTreeDistance1.Caption = "Tree Distance";
            this.fieldTreeDistance1.FieldName = "TreeDistance";
            this.fieldTreeDistance1.Name = "fieldTreeDistance1";
            this.fieldTreeDistance1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeDistance1.Visible = false;
            // 
            // fieldTreeHouseName1
            // 
            this.fieldTreeHouseName1.AreaIndex = 5;
            this.fieldTreeHouseName1.Caption = "Tree House Name";
            this.fieldTreeHouseName1.FieldName = "TreeHouseName";
            this.fieldTreeHouseName1.Name = "fieldTreeHouseName1";
            this.fieldTreeHouseName1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeHouseName1.Visible = false;
            // 
            // fieldTreeSpeciesName1
            // 
            this.fieldTreeSpeciesName1.AreaIndex = 6;
            this.fieldTreeSpeciesName1.Caption = "Tree Species Name";
            this.fieldTreeSpeciesName1.FieldName = "TreeSpeciesName";
            this.fieldTreeSpeciesName1.Name = "fieldTreeSpeciesName1";
            this.fieldTreeSpeciesName1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeAccess1
            // 
            this.fieldTreeAccess1.AreaIndex = 8;
            this.fieldTreeAccess1.Caption = "Tree Access";
            this.fieldTreeAccess1.FieldName = "TreeAccess";
            this.fieldTreeAccess1.Name = "fieldTreeAccess1";
            this.fieldTreeAccess1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeVisibility1
            // 
            this.fieldTreeVisibility1.AreaIndex = 9;
            this.fieldTreeVisibility1.Caption = "Tree Visibility";
            this.fieldTreeVisibility1.FieldName = "TreeVisibility";
            this.fieldTreeVisibility1.Name = "fieldTreeVisibility1";
            this.fieldTreeVisibility1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeContext1
            // 
            this.fieldTreeContext1.AreaIndex = 10;
            this.fieldTreeContext1.Caption = "Tree Context";
            this.fieldTreeContext1.FieldName = "TreeContext";
            this.fieldTreeContext1.Name = "fieldTreeContext1";
            this.fieldTreeContext1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeContext1.Visible = false;
            // 
            // fieldTreeManagement1
            // 
            this.fieldTreeManagement1.AreaIndex = 10;
            this.fieldTreeManagement1.Caption = "Tree Management";
            this.fieldTreeManagement1.FieldName = "TreeManagement";
            this.fieldTreeManagement1.Name = "fieldTreeManagement1";
            this.fieldTreeManagement1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeManagement1.Visible = false;
            // 
            // fieldTreeLegalStatus1
            // 
            this.fieldTreeLegalStatus1.AreaIndex = 10;
            this.fieldTreeLegalStatus1.Caption = "Tree Legal Status";
            this.fieldTreeLegalStatus1.FieldName = "TreeLegalStatus";
            this.fieldTreeLegalStatus1.Name = "fieldTreeLegalStatus1";
            this.fieldTreeLegalStatus1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeLastInspectionDate1
            // 
            this.fieldTreeLastInspectionDate1.AreaIndex = 11;
            this.fieldTreeLastInspectionDate1.Caption = "Tree Last Inspection Date";
            this.fieldTreeLastInspectionDate1.FieldName = "TreeLastInspectionDate";
            this.fieldTreeLastInspectionDate1.Name = "fieldTreeLastInspectionDate1";
            this.fieldTreeLastInspectionDate1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeInspectionCycle1
            // 
            this.fieldTreeInspectionCycle1.AreaIndex = 11;
            this.fieldTreeInspectionCycle1.Caption = "Tree Inspection Cycle";
            this.fieldTreeInspectionCycle1.FieldName = "TreeInspectionCycle";
            this.fieldTreeInspectionCycle1.Name = "fieldTreeInspectionCycle1";
            this.fieldTreeInspectionCycle1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeInspectionCycle1.Visible = false;
            // 
            // fieldTreeInspectionUnit1
            // 
            this.fieldTreeInspectionUnit1.AreaIndex = 11;
            this.fieldTreeInspectionUnit1.Caption = "Tree Inspection Unit";
            this.fieldTreeInspectionUnit1.FieldName = "TreeInspectionUnit";
            this.fieldTreeInspectionUnit1.Name = "fieldTreeInspectionUnit1";
            this.fieldTreeInspectionUnit1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeInspectionUnit1.Visible = false;
            // 
            // fieldTreeNextInspectionDate1
            // 
            this.fieldTreeNextInspectionDate1.AreaIndex = 12;
            this.fieldTreeNextInspectionDate1.Caption = "Tree Next Inspection Date";
            this.fieldTreeNextInspectionDate1.FieldName = "TreeNextInspectionDate";
            this.fieldTreeNextInspectionDate1.Name = "fieldTreeNextInspectionDate1";
            this.fieldTreeNextInspectionDate1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeGroundType1
            // 
            this.fieldTreeGroundType1.AreaIndex = 13;
            this.fieldTreeGroundType1.Caption = "Tree Ground Type";
            this.fieldTreeGroundType1.FieldName = "TreeGroundType";
            this.fieldTreeGroundType1.Name = "fieldTreeGroundType1";
            this.fieldTreeGroundType1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeDBH1
            // 
            this.fieldTreeDBH1.AreaIndex = 14;
            this.fieldTreeDBH1.Caption = "Tree DBH";
            this.fieldTreeDBH1.CellFormat.FormatString = "#";
            this.fieldTreeDBH1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeDBH1.FieldName = "TreeDBH";
            this.fieldTreeDBH1.GrandTotalCellFormat.FormatString = "#";
            this.fieldTreeDBH1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeDBH1.Name = "fieldTreeDBH1";
            this.fieldTreeDBH1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeDBH1.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldTreeDBH1.TotalCellFormat.FormatString = "#";
            this.fieldTreeDBH1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeDBH1.TotalValueFormat.FormatString = "#";
            this.fieldTreeDBH1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeDBH1.ValueFormat.FormatString = "#";
            this.fieldTreeDBH1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreeDBHRange1
            // 
            this.fieldTreeDBHRange1.AreaIndex = 15;
            this.fieldTreeDBHRange1.Caption = "Tree DBH Range";
            this.fieldTreeDBHRange1.FieldName = "TreeDBHRange";
            this.fieldTreeDBHRange1.Name = "fieldTreeDBHRange1";
            this.fieldTreeDBHRange1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeHeight1
            // 
            this.fieldTreeHeight1.AreaIndex = 16;
            this.fieldTreeHeight1.Caption = "Tree Height";
            this.fieldTreeHeight1.CellFormat.FormatString = "#.##";
            this.fieldTreeHeight1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeHeight1.FieldName = "TreeHeight";
            this.fieldTreeHeight1.GrandTotalCellFormat.FormatString = "#.##";
            this.fieldTreeHeight1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeHeight1.Name = "fieldTreeHeight1";
            this.fieldTreeHeight1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeHeight1.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldTreeHeight1.TotalCellFormat.FormatString = "#.##";
            this.fieldTreeHeight1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeHeight1.TotalValueFormat.FormatString = "#.##";
            this.fieldTreeHeight1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeHeight1.ValueFormat.FormatString = "#.##";
            this.fieldTreeHeight1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreeHeightRange1
            // 
            this.fieldTreeHeightRange1.AreaIndex = 17;
            this.fieldTreeHeightRange1.Caption = "Tree Height Range";
            this.fieldTreeHeightRange1.FieldName = "TreeHeightRange";
            this.fieldTreeHeightRange1.Name = "fieldTreeHeightRange1";
            this.fieldTreeHeightRange1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeProtectionType1
            // 
            this.fieldTreeProtectionType1.AreaIndex = 18;
            this.fieldTreeProtectionType1.Caption = "Tree Protection Type";
            this.fieldTreeProtectionType1.FieldName = "TreeProtectionType";
            this.fieldTreeProtectionType1.Name = "fieldTreeProtectionType1";
            this.fieldTreeProtectionType1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeCrownDiameter1
            // 
            this.fieldTreeCrownDiameter1.AreaIndex = 19;
            this.fieldTreeCrownDiameter1.Caption = "Tree Crown Diameter";
            this.fieldTreeCrownDiameter1.CellFormat.FormatString = "#";
            this.fieldTreeCrownDiameter1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCrownDiameter1.FieldName = "TreeCrownDiameter";
            this.fieldTreeCrownDiameter1.GrandTotalCellFormat.FormatString = "#";
            this.fieldTreeCrownDiameter1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCrownDiameter1.Name = "fieldTreeCrownDiameter1";
            this.fieldTreeCrownDiameter1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeCrownDiameter1.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldTreeCrownDiameter1.TotalCellFormat.FormatString = "#";
            this.fieldTreeCrownDiameter1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCrownDiameter1.TotalValueFormat.FormatString = "#";
            this.fieldTreeCrownDiameter1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCrownDiameter1.ValueFormat.FormatString = "#";
            this.fieldTreeCrownDiameter1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreePlantDate1
            // 
            this.fieldTreePlantDate1.AreaIndex = 20;
            this.fieldTreePlantDate1.Caption = "Tree Plant Date";
            this.fieldTreePlantDate1.FieldName = "TreePlantDate";
            this.fieldTreePlantDate1.Name = "fieldTreePlantDate1";
            this.fieldTreePlantDate1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeGroupNumber1
            // 
            this.fieldTreeGroupNumber1.AreaIndex = 20;
            this.fieldTreeGroupNumber1.Caption = "Tree Group Number";
            this.fieldTreeGroupNumber1.FieldName = "TreeGroupNumber";
            this.fieldTreeGroupNumber1.Name = "fieldTreeGroupNumber1";
            this.fieldTreeGroupNumber1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeGroupNumber1.Visible = false;
            // 
            // fieldTreeRiskCategory1
            // 
            this.fieldTreeRiskCategory1.AreaIndex = 5;
            this.fieldTreeRiskCategory1.Caption = "Tree Risk Category";
            this.fieldTreeRiskCategory1.FieldName = "TreeRiskCategory";
            this.fieldTreeRiskCategory1.Name = "fieldTreeRiskCategory1";
            this.fieldTreeRiskCategory1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeSiteHazardClass1
            // 
            this.fieldTreeSiteHazardClass1.AreaIndex = 21;
            this.fieldTreeSiteHazardClass1.Caption = "Tree Site Hazard Class";
            this.fieldTreeSiteHazardClass1.FieldName = "TreeSiteHazardClass";
            this.fieldTreeSiteHazardClass1.Name = "fieldTreeSiteHazardClass1";
            this.fieldTreeSiteHazardClass1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreePlantSize1
            // 
            this.fieldTreePlantSize1.AreaIndex = 22;
            this.fieldTreePlantSize1.Caption = "Tree Plant Size";
            this.fieldTreePlantSize1.FieldName = "TreePlantSize";
            this.fieldTreePlantSize1.Name = "fieldTreePlantSize1";
            this.fieldTreePlantSize1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreePlantSource1
            // 
            this.fieldTreePlantSource1.AreaIndex = 23;
            this.fieldTreePlantSource1.Caption = "Tree Plant Source";
            this.fieldTreePlantSource1.FieldName = "TreePlantSource";
            this.fieldTreePlantSource1.Name = "fieldTreePlantSource1";
            this.fieldTreePlantSource1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreePlantMethod1
            // 
            this.fieldTreePlantMethod1.AreaIndex = 24;
            this.fieldTreePlantMethod1.Caption = "Tree Plant Method";
            this.fieldTreePlantMethod1.FieldName = "TreePlantMethod";
            this.fieldTreePlantMethod1.Name = "fieldTreePlantMethod1";
            this.fieldTreePlantMethod1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreePostcode1
            // 
            this.fieldTreePostcode1.AreaIndex = 28;
            this.fieldTreePostcode1.Caption = "Tree Postcode";
            this.fieldTreePostcode1.FieldName = "TreePostcode";
            this.fieldTreePostcode1.Name = "fieldTreePostcode1";
            this.fieldTreePostcode1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePostcode1.Visible = false;
            // 
            // fieldTreeMapLabel1
            // 
            this.fieldTreeMapLabel1.AreaIndex = 29;
            this.fieldTreeMapLabel1.Caption = "Tree Map Label";
            this.fieldTreeMapLabel1.FieldName = "TreeMapLabel";
            this.fieldTreeMapLabel1.Name = "fieldTreeMapLabel1";
            this.fieldTreeMapLabel1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeMapLabel1.Visible = false;
            // 
            // fieldTreeStatus1
            // 
            this.fieldTreeStatus1.AreaIndex = 28;
            this.fieldTreeStatus1.Caption = "Tree Status";
            this.fieldTreeStatus1.FieldName = "TreeStatus";
            this.fieldTreeStatus1.Name = "fieldTreeStatus1";
            this.fieldTreeStatus1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeStatus1.Visible = false;
            // 
            // fieldTreeSize1
            // 
            this.fieldTreeSize1.AreaIndex = 25;
            this.fieldTreeSize1.Caption = "Tree Size";
            this.fieldTreeSize1.FieldName = "TreeSize";
            this.fieldTreeSize1.Name = "fieldTreeSize1";
            this.fieldTreeSize1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeTarget11
            // 
            this.fieldTreeTarget11.AreaIndex = 26;
            this.fieldTreeTarget11.Caption = "Tree Target 1";
            this.fieldTreeTarget11.FieldName = "TreeTarget1";
            this.fieldTreeTarget11.Name = "fieldTreeTarget11";
            this.fieldTreeTarget11.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeTarget21
            // 
            this.fieldTreeTarget21.AreaIndex = 27;
            this.fieldTreeTarget21.Caption = "Tree Target 2";
            this.fieldTreeTarget21.FieldName = "TreeTarget2";
            this.fieldTreeTarget21.Name = "fieldTreeTarget21";
            this.fieldTreeTarget21.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeTarget31
            // 
            this.fieldTreeTarget31.AreaIndex = 28;
            this.fieldTreeTarget31.Caption = "Tree Target 3";
            this.fieldTreeTarget31.FieldName = "TreeTarget3";
            this.fieldTreeTarget31.Name = "fieldTreeTarget31";
            this.fieldTreeTarget31.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeLastModifiedDate1
            // 
            this.fieldTreeLastModifiedDate1.AreaIndex = 35;
            this.fieldTreeLastModifiedDate1.Caption = "Tree Last Modified Date";
            this.fieldTreeLastModifiedDate1.FieldName = "TreeLastModifiedDate";
            this.fieldTreeLastModifiedDate1.Name = "fieldTreeLastModifiedDate1";
            this.fieldTreeLastModifiedDate1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeLastModifiedDate1.Visible = false;
            // 
            // fieldTreeRemarks1
            // 
            this.fieldTreeRemarks1.AreaIndex = 35;
            this.fieldTreeRemarks1.Caption = "Tree Remarks";
            this.fieldTreeRemarks1.FieldName = "TreeRemarks";
            this.fieldTreeRemarks1.Name = "fieldTreeRemarks1";
            this.fieldTreeRemarks1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeRemarks1.Visible = false;
            // 
            // fieldTreeUser11
            // 
            this.fieldTreeUser11.AreaIndex = 35;
            this.fieldTreeUser11.Caption = "Tree User Defined 1";
            this.fieldTreeUser11.FieldName = "TreeUser1";
            this.fieldTreeUser11.Name = "fieldTreeUser11";
            this.fieldTreeUser11.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeUser11.Visible = false;
            // 
            // fieldTreeUser21
            // 
            this.fieldTreeUser21.AreaIndex = 35;
            this.fieldTreeUser21.Caption = "Tree User Defined 2";
            this.fieldTreeUser21.FieldName = "TreeUser2";
            this.fieldTreeUser21.Name = "fieldTreeUser21";
            this.fieldTreeUser21.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeUser21.Visible = false;
            // 
            // fieldTreeUser31
            // 
            this.fieldTreeUser31.AreaIndex = 35;
            this.fieldTreeUser31.Caption = "Tree User Defined 3";
            this.fieldTreeUser31.FieldName = "TreeUser3";
            this.fieldTreeUser31.Name = "fieldTreeUser31";
            this.fieldTreeUser31.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeUser31.Visible = false;
            // 
            // fieldTreeAgeClass1
            // 
            this.fieldTreeAgeClass1.AreaIndex = 29;
            this.fieldTreeAgeClass1.Caption = "Tree Age Class";
            this.fieldTreeAgeClass1.FieldName = "TreeAgeClass";
            this.fieldTreeAgeClass1.Name = "fieldTreeAgeClass1";
            this.fieldTreeAgeClass1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeX1
            // 
            this.fieldTreeX1.AreaIndex = 36;
            this.fieldTreeX1.Caption = "Tree X";
            this.fieldTreeX1.FieldName = "TreeX";
            this.fieldTreeX1.Name = "fieldTreeX1";
            this.fieldTreeX1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeX1.Visible = false;
            // 
            // fieldTreeY1
            // 
            this.fieldTreeY1.AreaIndex = 36;
            this.fieldTreeY1.Caption = "Tree Y";
            this.fieldTreeY1.FieldName = "TreeY";
            this.fieldTreeY1.Name = "fieldTreeY1";
            this.fieldTreeY1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeY1.Visible = false;
            // 
            // fieldTreeAreaHa1
            // 
            this.fieldTreeAreaHa1.AreaIndex = 36;
            this.fieldTreeAreaHa1.Caption = "Tree Area [M�]";
            this.fieldTreeAreaHa1.FieldName = "TreeAreaHa";
            this.fieldTreeAreaHa1.Name = "fieldTreeAreaHa1";
            this.fieldTreeAreaHa1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeAreaHa1.Visible = false;
            // 
            // fieldTreeReplantCount1
            // 
            this.fieldTreeReplantCount1.AreaIndex = 36;
            this.fieldTreeReplantCount1.Caption = "Tree Replant Count";
            this.fieldTreeReplantCount1.FieldName = "TreeReplantCount";
            this.fieldTreeReplantCount1.Name = "fieldTreeReplantCount1";
            this.fieldTreeReplantCount1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeReplantCount1.Visible = false;
            // 
            // fieldTreeSurveyDate1
            // 
            this.fieldTreeSurveyDate1.AreaIndex = 36;
            this.fieldTreeSurveyDate1.Caption = "Tree Survey Date";
            this.fieldTreeSurveyDate1.FieldName = "TreeSurveyDate";
            this.fieldTreeSurveyDate1.Name = "fieldTreeSurveyDate1";
            this.fieldTreeSurveyDate1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeSurveyDate1.Visible = false;
            // 
            // fieldTreeType2
            // 
            this.fieldTreeType2.AreaIndex = 30;
            this.fieldTreeType2.Caption = "Tree Type";
            this.fieldTreeType2.FieldName = "TreeType";
            this.fieldTreeType2.Name = "fieldTreeType2";
            this.fieldTreeType2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeHedgeOwner1
            // 
            this.fieldTreeHedgeOwner1.AreaIndex = 37;
            this.fieldTreeHedgeOwner1.Caption = "Tree Hedge Owner";
            this.fieldTreeHedgeOwner1.FieldName = "TreeHedgeOwner";
            this.fieldTreeHedgeOwner1.Name = "fieldTreeHedgeOwner1";
            this.fieldTreeHedgeOwner1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeHedgeOwner1.Visible = false;
            // 
            // fieldTreeHedgeLength1
            // 
            this.fieldTreeHedgeLength1.AreaIndex = 37;
            this.fieldTreeHedgeLength1.Caption = "Tree Hedge Length";
            this.fieldTreeHedgeLength1.FieldName = "TreeHedgeLength";
            this.fieldTreeHedgeLength1.Name = "fieldTreeHedgeLength1";
            this.fieldTreeHedgeLength1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeHedgeLength1.Visible = false;
            // 
            // fieldTreeGardenSize1
            // 
            this.fieldTreeGardenSize1.AreaIndex = 37;
            this.fieldTreeGardenSize1.Caption = "Tree Garden Size";
            this.fieldTreeGardenSize1.FieldName = "TreeGardenSize";
            this.fieldTreeGardenSize1.Name = "fieldTreeGardenSize1";
            this.fieldTreeGardenSize1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeGardenSize1.Visible = false;
            // 
            // fieldTreePropertyProximity1
            // 
            this.fieldTreePropertyProximity1.AreaIndex = 37;
            this.fieldTreePropertyProximity1.Caption = "Tree Property Proximity";
            this.fieldTreePropertyProximity1.FieldName = "TreePropertyProximity";
            this.fieldTreePropertyProximity1.Name = "fieldTreePropertyProximity1";
            this.fieldTreePropertyProximity1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePropertyProximity1.Visible = false;
            // 
            // fieldTreeSiteLevel1
            // 
            this.fieldTreeSiteLevel1.AreaIndex = 34;
            this.fieldTreeSiteLevel1.Caption = "Tree Site Level";
            this.fieldTreeSiteLevel1.FieldName = "TreeSiteLevel";
            this.fieldTreeSiteLevel1.Name = "fieldTreeSiteLevel1";
            this.fieldTreeSiteLevel1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeSiteLevel1.Visible = false;
            // 
            // fieldTreeSituation1
            // 
            this.fieldTreeSituation1.AreaIndex = 32;
            this.fieldTreeSituation1.Caption = "Tree Situation";
            this.fieldTreeSituation1.FieldName = "TreeSituation";
            this.fieldTreeSituation1.Name = "fieldTreeSituation1";
            this.fieldTreeSituation1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldTreeRiskFactor1
            // 
            this.fieldTreeRiskFactor1.AreaIndex = 33;
            this.fieldTreeRiskFactor1.Caption = "Tree Risk Factor";
            this.fieldTreeRiskFactor1.CellFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeRiskFactor1.FieldName = "TreeRiskFactor";
            this.fieldTreeRiskFactor1.GrandTotalCellFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeRiskFactor1.Name = "fieldTreeRiskFactor1";
            this.fieldTreeRiskFactor1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreeRiskFactor1.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average;
            this.fieldTreeRiskFactor1.TotalCellFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeRiskFactor1.TotalValueFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeRiskFactor1.ValueFormat.FormatString = "#.##";
            this.fieldTreeRiskFactor1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreePolygonXY1
            // 
            this.fieldTreePolygonXY1.AreaIndex = 54;
            this.fieldTreePolygonXY1.Caption = "Tree Polygon XY";
            this.fieldTreePolygonXY1.FieldName = "TreePolygonXY";
            this.fieldTreePolygonXY1.Name = "fieldTreePolygonXY1";
            this.fieldTreePolygonXY1.Options.AllowRunTimeSummaryChange = true;
            this.fieldTreePolygonXY1.Visible = false;
            // 
            // fieldcolor2
            // 
            this.fieldcolor2.AreaIndex = 54;
            this.fieldcolor2.Caption = "Color";
            this.fieldcolor2.FieldName = "color";
            this.fieldcolor2.Name = "fieldcolor2";
            this.fieldcolor2.Options.AllowRunTimeSummaryChange = true;
            this.fieldcolor2.Visible = false;
            // 
            // fieldTreeOwnershipName1
            // 
            this.fieldTreeOwnershipName1.AreaIndex = 34;
            this.fieldTreeOwnershipName1.Caption = "Tree Ownership Name";
            this.fieldTreeOwnershipName1.FieldName = "TreeOwnershipName";
            this.fieldTreeOwnershipName1.Name = "fieldTreeOwnershipName1";
            this.fieldTreeOwnershipName1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionReference1
            // 
            this.fieldInspectionReference1.AreaIndex = 35;
            this.fieldInspectionReference1.Caption = "Inspection Reference";
            this.fieldInspectionReference1.FieldName = "InspectionReference";
            this.fieldInspectionReference1.Name = "fieldInspectionReference1";
            this.fieldInspectionReference1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionInspector1
            // 
            this.fieldInspectionInspector1.AreaIndex = 36;
            this.fieldInspectionInspector1.Caption = "Inspection Inspector";
            this.fieldInspectionInspector1.FieldName = "InspectionInspector";
            this.fieldInspectionInspector1.Name = "fieldInspectionInspector1";
            this.fieldInspectionInspector1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionDate1
            // 
            this.fieldInspectionDate1.AreaIndex = 37;
            this.fieldInspectionDate1.Caption = "Inspection Date";
            this.fieldInspectionDate1.FieldName = "InspectionDate";
            this.fieldInspectionDate1.Name = "fieldInspectionDate1";
            this.fieldInspectionDate1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionIncidentReference1
            // 
            this.fieldInspectionIncidentReference1.AreaIndex = 38;
            this.fieldInspectionIncidentReference1.Caption = "Inspection Incident Reference";
            this.fieldInspectionIncidentReference1.FieldName = "InspectionIncidentReference";
            this.fieldInspectionIncidentReference1.Name = "fieldInspectionIncidentReference1";
            this.fieldInspectionIncidentReference1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionIncidentDate1
            // 
            this.fieldInspectionIncidentDate1.AreaIndex = 39;
            this.fieldInspectionIncidentDate1.Caption = "Inspection Incident Date";
            this.fieldInspectionIncidentDate1.FieldName = "InspectionIncidentDate";
            this.fieldInspectionIncidentDate1.Name = "fieldInspectionIncidentDate1";
            this.fieldInspectionIncidentDate1.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionIncidentDate1.Visible = false;
            // 
            // fieldIncidentID1
            // 
            this.fieldIncidentID1.AreaIndex = 46;
            this.fieldIncidentID1.Caption = "Incident ID";
            this.fieldIncidentID1.FieldName = "IncidentID";
            this.fieldIncidentID1.Name = "fieldIncidentID1";
            this.fieldIncidentID1.Options.AllowRunTimeSummaryChange = true;
            this.fieldIncidentID1.Visible = false;
            // 
            // fieldInspectionAngleToVertical1
            // 
            this.fieldInspectionAngleToVertical1.AreaIndex = 39;
            this.fieldInspectionAngleToVertical1.Caption = "Inspection Angle To Vertical";
            this.fieldInspectionAngleToVertical1.FieldName = "InspectionAngleToVertical";
            this.fieldInspectionAngleToVertical1.Name = "fieldInspectionAngleToVertical1";
            this.fieldInspectionAngleToVertical1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionLastModified1
            // 
            this.fieldInspectionLastModified1.AreaIndex = 47;
            this.fieldInspectionLastModified1.Caption = "Inspection Last Modified";
            this.fieldInspectionLastModified1.FieldName = "InspectionLastModified";
            this.fieldInspectionLastModified1.Name = "fieldInspectionLastModified1";
            this.fieldInspectionLastModified1.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionLastModified1.Visible = false;
            // 
            // fieldInspectionUserDefined11
            // 
            this.fieldInspectionUserDefined11.AreaIndex = 47;
            this.fieldInspectionUserDefined11.Caption = "Inspection User Defined 1";
            this.fieldInspectionUserDefined11.FieldName = "InspectionUserDefined1";
            this.fieldInspectionUserDefined11.Name = "fieldInspectionUserDefined11";
            this.fieldInspectionUserDefined11.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionUserDefined11.Visible = false;
            // 
            // fieldInspectionUserDefined21
            // 
            this.fieldInspectionUserDefined21.AreaIndex = 47;
            this.fieldInspectionUserDefined21.Caption = "Inspection User Defined 2";
            this.fieldInspectionUserDefined21.FieldName = "InspectionUserDefined2";
            this.fieldInspectionUserDefined21.Name = "fieldInspectionUserDefined21";
            this.fieldInspectionUserDefined21.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionUserDefined21.Visible = false;
            // 
            // fieldInspectionUserDefined31
            // 
            this.fieldInspectionUserDefined31.AreaIndex = 47;
            this.fieldInspectionUserDefined31.Caption = "Inspection User Defined 3";
            this.fieldInspectionUserDefined31.FieldName = "InspectionUserDefined3";
            this.fieldInspectionUserDefined31.Name = "fieldInspectionUserDefined31";
            this.fieldInspectionUserDefined31.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionUserDefined31.Visible = false;
            // 
            // fieldInspectionRemarks1
            // 
            this.fieldInspectionRemarks1.AreaIndex = 47;
            this.fieldInspectionRemarks1.Caption = "Inspection Remarks";
            this.fieldInspectionRemarks1.FieldName = "InspectionRemarks";
            this.fieldInspectionRemarks1.Name = "fieldInspectionRemarks1";
            this.fieldInspectionRemarks1.Options.AllowRunTimeSummaryChange = true;
            this.fieldInspectionRemarks1.Visible = false;
            // 
            // fieldInspectionStemPhysical11
            // 
            this.fieldInspectionStemPhysical11.AreaIndex = 40;
            this.fieldInspectionStemPhysical11.Caption = "Inspection Stem Physical 1";
            this.fieldInspectionStemPhysical11.FieldName = "InspectionStemPhysical1";
            this.fieldInspectionStemPhysical11.Name = "fieldInspectionStemPhysical11";
            this.fieldInspectionStemPhysical11.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemPhysical21
            // 
            this.fieldInspectionStemPhysical21.AreaIndex = 41;
            this.fieldInspectionStemPhysical21.Caption = "Inspection Stem Physical 2";
            this.fieldInspectionStemPhysical21.FieldName = "InspectionStemPhysical2";
            this.fieldInspectionStemPhysical21.Name = "fieldInspectionStemPhysical21";
            this.fieldInspectionStemPhysical21.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemPhysical31
            // 
            this.fieldInspectionStemPhysical31.AreaIndex = 42;
            this.fieldInspectionStemPhysical31.Caption = "Inspection Stem Physical 3";
            this.fieldInspectionStemPhysical31.FieldName = "InspectionStemPhysical3";
            this.fieldInspectionStemPhysical31.Name = "fieldInspectionStemPhysical31";
            this.fieldInspectionStemPhysical31.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemDisease11
            // 
            this.fieldInspectionStemDisease11.AreaIndex = 43;
            this.fieldInspectionStemDisease11.Caption = "Inspection Stem Disease 1";
            this.fieldInspectionStemDisease11.FieldName = "InspectionStemDisease1";
            this.fieldInspectionStemDisease11.Name = "fieldInspectionStemDisease11";
            this.fieldInspectionStemDisease11.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemDisease21
            // 
            this.fieldInspectionStemDisease21.AreaIndex = 44;
            this.fieldInspectionStemDisease21.Caption = "Inspection Stem Disease 2";
            this.fieldInspectionStemDisease21.FieldName = "InspectionStemDisease2";
            this.fieldInspectionStemDisease21.Name = "fieldInspectionStemDisease21";
            this.fieldInspectionStemDisease21.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionStemDisease31
            // 
            this.fieldInspectionStemDisease31.AreaIndex = 45;
            this.fieldInspectionStemDisease31.Caption = "Inspection Stem Disease 3";
            this.fieldInspectionStemDisease31.FieldName = "InspectionStemDisease3";
            this.fieldInspectionStemDisease31.Name = "fieldInspectionStemDisease31";
            this.fieldInspectionStemDisease31.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownPhysical11
            // 
            this.fieldInspectionCrownPhysical11.AreaIndex = 46;
            this.fieldInspectionCrownPhysical11.Caption = "Inspection Crown Physical 1";
            this.fieldInspectionCrownPhysical11.FieldName = "InspectionCrownPhysical1";
            this.fieldInspectionCrownPhysical11.Name = "fieldInspectionCrownPhysical11";
            this.fieldInspectionCrownPhysical11.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownPhysical21
            // 
            this.fieldInspectionCrownPhysical21.AreaIndex = 47;
            this.fieldInspectionCrownPhysical21.Caption = "Inspection Crown Physical 2";
            this.fieldInspectionCrownPhysical21.FieldName = "InspectionCrownPhysical2";
            this.fieldInspectionCrownPhysical21.Name = "fieldInspectionCrownPhysical21";
            this.fieldInspectionCrownPhysical21.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownPhysical31
            // 
            this.fieldInspectionCrownPhysical31.AreaIndex = 48;
            this.fieldInspectionCrownPhysical31.Caption = "Inspection Crown Physical 3";
            this.fieldInspectionCrownPhysical31.FieldName = "InspectionCrownPhysical3";
            this.fieldInspectionCrownPhysical31.Name = "fieldInspectionCrownPhysical31";
            this.fieldInspectionCrownPhysical31.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownDisease11
            // 
            this.fieldInspectionCrownDisease11.AreaIndex = 49;
            this.fieldInspectionCrownDisease11.Caption = "Inspection Crown Disease 1";
            this.fieldInspectionCrownDisease11.FieldName = "InspectionCrownDisease1";
            this.fieldInspectionCrownDisease11.Name = "fieldInspectionCrownDisease11";
            this.fieldInspectionCrownDisease11.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownDisease21
            // 
            this.fieldInspectionCrownDisease21.AreaIndex = 50;
            this.fieldInspectionCrownDisease21.Caption = "Inspection Crown Disease 2";
            this.fieldInspectionCrownDisease21.FieldName = "InspectionCrownDisease2";
            this.fieldInspectionCrownDisease21.Name = "fieldInspectionCrownDisease21";
            this.fieldInspectionCrownDisease21.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownDisease31
            // 
            this.fieldInspectionCrownDisease31.AreaIndex = 51;
            this.fieldInspectionCrownDisease31.Caption = "Inspection Crown Disease 3";
            this.fieldInspectionCrownDisease31.FieldName = "InspectionCrownDisease3";
            this.fieldInspectionCrownDisease31.Name = "fieldInspectionCrownDisease31";
            this.fieldInspectionCrownDisease31.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownFoliation11
            // 
            this.fieldInspectionCrownFoliation11.AreaIndex = 52;
            this.fieldInspectionCrownFoliation11.Caption = "Inspection Crown Foliation 1";
            this.fieldInspectionCrownFoliation11.FieldName = "InspectionCrownFoliation1";
            this.fieldInspectionCrownFoliation11.Name = "fieldInspectionCrownFoliation11";
            this.fieldInspectionCrownFoliation11.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownFoliation21
            // 
            this.fieldInspectionCrownFoliation21.AreaIndex = 53;
            this.fieldInspectionCrownFoliation21.Caption = "Inspection Crown Foliation 2";
            this.fieldInspectionCrownFoliation21.FieldName = "InspectionCrownFoliation2";
            this.fieldInspectionCrownFoliation21.Name = "fieldInspectionCrownFoliation21";
            this.fieldInspectionCrownFoliation21.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionCrownFoliation31
            // 
            this.fieldInspectionCrownFoliation31.AreaIndex = 54;
            this.fieldInspectionCrownFoliation31.Caption = "Inspection Crown Foliation 3";
            this.fieldInspectionCrownFoliation31.FieldName = "InspectionCrownFoliation3";
            this.fieldInspectionCrownFoliation31.Name = "fieldInspectionCrownFoliation31";
            this.fieldInspectionCrownFoliation31.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionRiskCategory1
            // 
            this.fieldInspectionRiskCategory1.AreaIndex = 55;
            this.fieldInspectionRiskCategory1.Caption = "Inspection Risk Category";
            this.fieldInspectionRiskCategory1.FieldName = "InspectionRiskCategory";
            this.fieldInspectionRiskCategory1.Name = "fieldInspectionRiskCategory1";
            this.fieldInspectionRiskCategory1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionGeneralCondition1
            // 
            this.fieldInspectionGeneralCondition1.AreaIndex = 56;
            this.fieldInspectionGeneralCondition1.Caption = "Inspection General Condition";
            this.fieldInspectionGeneralCondition1.FieldName = "InspectionGeneralCondition";
            this.fieldInspectionGeneralCondition1.Name = "fieldInspectionGeneralCondition1";
            this.fieldInspectionGeneralCondition1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionBasePhysical11
            // 
            this.fieldInspectionBasePhysical11.AreaIndex = 57;
            this.fieldInspectionBasePhysical11.Caption = "Inspection Base Physical 1";
            this.fieldInspectionBasePhysical11.FieldName = "InspectionBasePhysical1";
            this.fieldInspectionBasePhysical11.Name = "fieldInspectionBasePhysical11";
            this.fieldInspectionBasePhysical11.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionBasePhysical21
            // 
            this.fieldInspectionBasePhysical21.AreaIndex = 58;
            this.fieldInspectionBasePhysical21.Caption = "Inspection Base Physical 2";
            this.fieldInspectionBasePhysical21.FieldName = "InspectionBasePhysical2";
            this.fieldInspectionBasePhysical21.Name = "fieldInspectionBasePhysical21";
            this.fieldInspectionBasePhysical21.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionBasePhysical31
            // 
            this.fieldInspectionBasePhysical31.AreaIndex = 59;
            this.fieldInspectionBasePhysical31.Caption = "Inspection Base Physical 3";
            this.fieldInspectionBasePhysical31.FieldName = "InspectionBasePhysical3";
            this.fieldInspectionBasePhysical31.Name = "fieldInspectionBasePhysical31";
            this.fieldInspectionBasePhysical31.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldInspectionVitality1
            // 
            this.fieldInspectionVitality1.AreaIndex = 60;
            this.fieldInspectionVitality1.Caption = "Inspection Vitality";
            this.fieldInspectionVitality1.FieldName = "InspectionVitality";
            this.fieldInspectionVitality1.Name = "fieldInspectionVitality1";
            this.fieldInspectionVitality1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionID
            // 
            this.fieldActionID.AreaIndex = 68;
            this.fieldActionID.Caption = "Action ID";
            this.fieldActionID.FieldName = "ActionID";
            this.fieldActionID.Name = "fieldActionID";
            this.fieldActionID.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionID.Visible = false;
            // 
            // fieldActionJobNumber
            // 
            this.fieldActionJobNumber.AreaIndex = 61;
            this.fieldActionJobNumber.Caption = "Action Job Number";
            this.fieldActionJobNumber.FieldName = "ActionJobNumber";
            this.fieldActionJobNumber.Name = "fieldActionJobNumber";
            this.fieldActionJobNumber.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldAction
            // 
            this.fieldAction.AreaIndex = 62;
            this.fieldAction.Caption = "Action";
            this.fieldAction.FieldName = "Action";
            this.fieldAction.Name = "fieldAction";
            this.fieldAction.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionPriority
            // 
            this.fieldActionPriority.AreaIndex = 63;
            this.fieldActionPriority.Caption = "Action Priority";
            this.fieldActionPriority.FieldName = "ActionPriority";
            this.fieldActionPriority.Name = "fieldActionPriority";
            this.fieldActionPriority.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionDueDate
            // 
            this.fieldActionDueDate.AreaIndex = 64;
            this.fieldActionDueDate.Caption = "Action Due Date";
            this.fieldActionDueDate.FieldName = "ActionDueDate";
            this.fieldActionDueDate.Name = "fieldActionDueDate";
            this.fieldActionDueDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionDoneDate
            // 
            this.fieldActionDoneDate.AreaIndex = 65;
            this.fieldActionDoneDate.Caption = "Action Done Date";
            this.fieldActionDoneDate.FieldName = "ActionDoneDate";
            this.fieldActionDoneDate.Name = "fieldActionDoneDate";
            this.fieldActionDoneDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionBy
            // 
            this.fieldActionBy.AreaIndex = 66;
            this.fieldActionBy.Caption = "Action By";
            this.fieldActionBy.FieldName = "ActionBy";
            this.fieldActionBy.Name = "fieldActionBy";
            this.fieldActionBy.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionSupervisor
            // 
            this.fieldActionSupervisor.AreaIndex = 67;
            this.fieldActionSupervisor.Caption = "Action Supervisor";
            this.fieldActionSupervisor.FieldName = "ActionSupervisor";
            this.fieldActionSupervisor.Name = "fieldActionSupervisor";
            this.fieldActionSupervisor.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionOwnership
            // 
            this.fieldActionOwnership.AreaIndex = 68;
            this.fieldActionOwnership.Caption = "Action Ownership";
            this.fieldActionOwnership.FieldName = "ActionOwnership";
            this.fieldActionOwnership.Name = "fieldActionOwnership";
            this.fieldActionOwnership.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionCostCentre
            // 
            this.fieldActionCostCentre.AreaIndex = 69;
            this.fieldActionCostCentre.Caption = "Action Cost Centre";
            this.fieldActionCostCentre.FieldName = "ActionCostCentre";
            this.fieldActionCostCentre.Name = "fieldActionCostCentre";
            this.fieldActionCostCentre.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionBudget
            // 
            this.fieldActionBudget.AreaIndex = 70;
            this.fieldActionBudget.Caption = "Action Budget";
            this.fieldActionBudget.FieldName = "ActionBudget";
            this.fieldActionBudget.Name = "fieldActionBudget";
            this.fieldActionBudget.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionWorkUnits
            // 
            this.fieldActionWorkUnits.AreaIndex = 71;
            this.fieldActionWorkUnits.Caption = "Action Work Units";
            this.fieldActionWorkUnits.FieldName = "ActionWorkUnits";
            this.fieldActionWorkUnits.Name = "fieldActionWorkUnits";
            this.fieldActionWorkUnits.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionScheduleOfRates
            // 
            this.fieldActionScheduleOfRates.AreaIndex = 72;
            this.fieldActionScheduleOfRates.Caption = "Action Schedule Of Rates";
            this.fieldActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.fieldActionScheduleOfRates.Name = "fieldActionScheduleOfRates";
            this.fieldActionScheduleOfRates.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionBudgetedRateDescription
            // 
            this.fieldActionBudgetedRateDescription.AreaIndex = 73;
            this.fieldActionBudgetedRateDescription.Caption = "Action Budgeted Rate Description";
            this.fieldActionBudgetedRateDescription.FieldName = "ActionBudgetedRateDescription";
            this.fieldActionBudgetedRateDescription.Name = "fieldActionBudgetedRateDescription";
            this.fieldActionBudgetedRateDescription.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionBudgetedRate
            // 
            this.fieldActionBudgetedRate.AreaIndex = 74;
            this.fieldActionBudgetedRate.Caption = "Action Budgeted Rate";
            this.fieldActionBudgetedRate.CellFormat.FormatString = "c";
            this.fieldActionBudgetedRate.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionBudgetedRate.FieldName = "ActionBudgetedRate";
            this.fieldActionBudgetedRate.GrandTotalCellFormat.FormatString = "c";
            this.fieldActionBudgetedRate.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionBudgetedRate.Name = "fieldActionBudgetedRate";
            this.fieldActionBudgetedRate.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionBudgetedRate.TotalCellFormat.FormatString = "c";
            this.fieldActionBudgetedRate.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionBudgetedRate.TotalValueFormat.FormatString = "c";
            this.fieldActionBudgetedRate.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionBudgetedRate.ValueFormat.FormatString = "c";
            this.fieldActionBudgetedRate.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActionBudgetedCost
            // 
            this.fieldActionBudgetedCost.AreaIndex = 75;
            this.fieldActionBudgetedCost.Caption = "Action Budgeted Cost";
            this.fieldActionBudgetedCost.CellFormat.FormatString = "c";
            this.fieldActionBudgetedCost.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionBudgetedCost.FieldName = "ActionBudgetedCost";
            this.fieldActionBudgetedCost.GrandTotalCellFormat.FormatString = "c";
            this.fieldActionBudgetedCost.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionBudgetedCost.Name = "fieldActionBudgetedCost";
            this.fieldActionBudgetedCost.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionBudgetedCost.TotalCellFormat.FormatString = "c";
            this.fieldActionBudgetedCost.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionBudgetedCost.TotalValueFormat.FormatString = "c";
            this.fieldActionBudgetedCost.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionBudgetedCost.ValueFormat.FormatString = "c";
            this.fieldActionBudgetedCost.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActionActualRateDescription
            // 
            this.fieldActionActualRateDescription.AreaIndex = 76;
            this.fieldActionActualRateDescription.Caption = "Action Actual Rate Description";
            this.fieldActionActualRateDescription.FieldName = "ActionActualRateDescription";
            this.fieldActionActualRateDescription.Name = "fieldActionActualRateDescription";
            this.fieldActionActualRateDescription.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionActualRate
            // 
            this.fieldActionActualRate.AreaIndex = 77;
            this.fieldActionActualRate.Caption = "Action Actual Rate";
            this.fieldActionActualRate.CellFormat.FormatString = "c";
            this.fieldActionActualRate.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionActualRate.FieldName = "ActionActualRate";
            this.fieldActionActualRate.GrandTotalCellFormat.FormatString = "c";
            this.fieldActionActualRate.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionActualRate.Name = "fieldActionActualRate";
            this.fieldActionActualRate.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionActualRate.TotalCellFormat.FormatString = "c";
            this.fieldActionActualRate.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionActualRate.TotalValueFormat.FormatString = "c";
            this.fieldActionActualRate.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionActualRate.ValueFormat.FormatString = "c";
            this.fieldActionActualRate.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActionActualCost
            // 
            this.fieldActionActualCost.AreaIndex = 78;
            this.fieldActionActualCost.Caption = "Action Actual Cost";
            this.fieldActionActualCost.CellFormat.FormatString = "c";
            this.fieldActionActualCost.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionActualCost.FieldName = "ActionActualCost";
            this.fieldActionActualCost.GrandTotalCellFormat.FormatString = "c";
            this.fieldActionActualCost.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionActualCost.Name = "fieldActionActualCost";
            this.fieldActionActualCost.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionActualCost.TotalCellFormat.FormatString = "c";
            this.fieldActionActualCost.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionActualCost.TotalValueFormat.FormatString = "c";
            this.fieldActionActualCost.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionActualCost.ValueFormat.FormatString = "c";
            this.fieldActionActualCost.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActionDateLastModified
            // 
            this.fieldActionDateLastModified.AreaIndex = 86;
            this.fieldActionDateLastModified.Caption = "Action Date Last Modified";
            this.fieldActionDateLastModified.FieldName = "ActionDateLastModified";
            this.fieldActionDateLastModified.Name = "fieldActionDateLastModified";
            this.fieldActionDateLastModified.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionDateLastModified.Visible = false;
            // 
            // fieldActionWorkOrder
            // 
            this.fieldActionWorkOrder.AreaIndex = 79;
            this.fieldActionWorkOrder.Caption = "Action Work Order";
            this.fieldActionWorkOrder.FieldName = "ActionWorkOrder";
            this.fieldActionWorkOrder.Name = "fieldActionWorkOrder";
            this.fieldActionWorkOrder.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionRemarks
            // 
            this.fieldActionRemarks.AreaIndex = 87;
            this.fieldActionRemarks.Caption = "Action Remarks";
            this.fieldActionRemarks.FieldName = "ActionRemarks";
            this.fieldActionRemarks.Name = "fieldActionRemarks";
            this.fieldActionRemarks.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionRemarks.Visible = false;
            // 
            // fieldActionUserDefined1
            // 
            this.fieldActionUserDefined1.AreaIndex = 87;
            this.fieldActionUserDefined1.Caption = "Action User Defined 1";
            this.fieldActionUserDefined1.FieldName = "ActionUserDefined1";
            this.fieldActionUserDefined1.Name = "fieldActionUserDefined1";
            this.fieldActionUserDefined1.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionUserDefined1.Visible = false;
            // 
            // fieldActionUserDefined2
            // 
            this.fieldActionUserDefined2.AreaIndex = 87;
            this.fieldActionUserDefined2.Caption = "Action User Defined 2";
            this.fieldActionUserDefined2.FieldName = "ActionUserDefined2";
            this.fieldActionUserDefined2.Name = "fieldActionUserDefined2";
            this.fieldActionUserDefined2.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionUserDefined2.Visible = false;
            // 
            // fieldActionUserDefined3
            // 
            this.fieldActionUserDefined3.AreaIndex = 87;
            this.fieldActionUserDefined3.Caption = "Action User Defined 3";
            this.fieldActionUserDefined3.FieldName = "ActionUserDefined3";
            this.fieldActionUserDefined3.Name = "fieldActionUserDefined3";
            this.fieldActionUserDefined3.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionUserDefined3.Visible = false;
            // 
            // fieldActionCostDifference
            // 
            this.fieldActionCostDifference.AreaIndex = 80;
            this.fieldActionCostDifference.Caption = "Action Cost Difference";
            this.fieldActionCostDifference.CellFormat.FormatString = "c";
            this.fieldActionCostDifference.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionCostDifference.FieldName = "ActionCostDifference";
            this.fieldActionCostDifference.GrandTotalCellFormat.FormatString = "c";
            this.fieldActionCostDifference.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionCostDifference.Name = "fieldActionCostDifference";
            this.fieldActionCostDifference.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionCostDifference.TotalCellFormat.FormatString = "c";
            this.fieldActionCostDifference.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionCostDifference.TotalValueFormat.FormatString = "c";
            this.fieldActionCostDifference.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActionCostDifference.ValueFormat.FormatString = "c";
            this.fieldActionCostDifference.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActionCount
            // 
            this.fieldActionCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldActionCount.AreaIndex = 0;
            this.fieldActionCount.Caption = "Action Count";
            this.fieldActionCount.FieldName = "ActionCount";
            this.fieldActionCount.Name = "fieldActionCount";
            this.fieldActionCount.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldActionCompletionStatus
            // 
            this.fieldActionCompletionStatus.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldActionCompletionStatus.AreaIndex = 0;
            this.fieldActionCompletionStatus.Caption = "Action Completion Status";
            this.fieldActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.fieldActionCompletionStatus.Name = "fieldActionCompletionStatus";
            this.fieldActionCompletionStatus.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionCompletionStatus.Visible = false;
            // 
            // fieldActionTimeliness
            // 
            this.fieldActionTimeliness.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldActionTimeliness.AreaIndex = 1;
            this.fieldActionTimeliness.Caption = "Action Timeliness";
            this.fieldActionTimeliness.FieldName = "ActionTimeliness";
            this.fieldActionTimeliness.Name = "fieldActionTimeliness";
            this.fieldActionTimeliness.Options.AllowRunTimeSummaryChange = true;
            this.fieldActionTimeliness.Visible = false;
            // 
            // fieldActionWorkOrderCompleteDate
            // 
            this.fieldActionWorkOrderCompleteDate.AreaIndex = 3;
            this.fieldActionWorkOrderCompleteDate.Caption = "Action Work Order Complete Date";
            this.fieldActionWorkOrderCompleteDate.FieldName = "ActionWorkOrderCompleteDate";
            this.fieldActionWorkOrderCompleteDate.Name = "fieldActionWorkOrderCompleteDate";
            // 
            // fieldActionWorkOrderIssueDate
            // 
            this.fieldActionWorkOrderIssueDate.AreaIndex = 2;
            this.fieldActionWorkOrderIssueDate.Caption = "Action Work Order Issue Date";
            this.fieldActionWorkOrderIssueDate.FieldName = "ActionWorkOrderIssueDate";
            this.fieldActionWorkOrderIssueDate.Name = "fieldActionWorkOrderIssueDate";
            // 
            // fieldTreeCavat2
            // 
            this.fieldTreeCavat2.AreaIndex = 31;
            this.fieldTreeCavat2.Caption = "Tree Cavat";
            this.fieldTreeCavat2.CellFormat.FormatString = "c";
            this.fieldTreeCavat2.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat2.FieldName = "TreeCavat";
            this.fieldTreeCavat2.GrandTotalCellFormat.FormatString = "c";
            this.fieldTreeCavat2.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat2.Name = "fieldTreeCavat2";
            this.fieldTreeCavat2.TotalCellFormat.FormatString = "c";
            this.fieldTreeCavat2.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat2.TotalValueFormat.FormatString = "c";
            this.fieldTreeCavat2.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTreeCavat2.ValueFormat.FormatString = "c";
            this.fieldTreeCavat2.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTreeStemCount2
            // 
            this.fieldTreeStemCount2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeStemCount2.AreaIndex = 0;
            this.fieldTreeStemCount2.Caption = "Tree Stem Count";
            this.fieldTreeStemCount2.FieldName = "TreeStemCount";
            this.fieldTreeStemCount2.Name = "fieldTreeStemCount2";
            this.fieldTreeStemCount2.Visible = false;
            // 
            // fieldTreeCrownNorth2
            // 
            this.fieldTreeCrownNorth2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownNorth2.AreaIndex = 0;
            this.fieldTreeCrownNorth2.Caption = "Tree Crown North";
            this.fieldTreeCrownNorth2.FieldName = "TreeCrownNorth";
            this.fieldTreeCrownNorth2.Name = "fieldTreeCrownNorth2";
            this.fieldTreeCrownNorth2.Visible = false;
            // 
            // fieldTreeCrownSouth2
            // 
            this.fieldTreeCrownSouth2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownSouth2.AreaIndex = 0;
            this.fieldTreeCrownSouth2.Caption = "Tree Crown South";
            this.fieldTreeCrownSouth2.FieldName = "TreeCrownSouth";
            this.fieldTreeCrownSouth2.Name = "fieldTreeCrownSouth2";
            this.fieldTreeCrownSouth2.Visible = false;
            // 
            // fieldTreeCrownEast2
            // 
            this.fieldTreeCrownEast2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownEast2.AreaIndex = 0;
            this.fieldTreeCrownEast2.Caption = "Tree Crown East";
            this.fieldTreeCrownEast2.FieldName = "TreeCrownEast";
            this.fieldTreeCrownEast2.Name = "fieldTreeCrownEast2";
            this.fieldTreeCrownEast2.Visible = false;
            // 
            // fieldTreeCrownWest2
            // 
            this.fieldTreeCrownWest2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeCrownWest2.AreaIndex = 0;
            this.fieldTreeCrownWest2.Caption = "Tree Crown West";
            this.fieldTreeCrownWest2.FieldName = "TreeCrownWest";
            this.fieldTreeCrownWest2.Name = "fieldTreeCrownWest2";
            this.fieldTreeCrownWest2.Visible = false;
            // 
            // fieldTreeRetentionCategory2
            // 
            this.fieldTreeRetentionCategory2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeRetentionCategory2.AreaIndex = 0;
            this.fieldTreeRetentionCategory2.Caption = "Tree Retention Category";
            this.fieldTreeRetentionCategory2.FieldName = "TreeRetentionCategory";
            this.fieldTreeRetentionCategory2.Name = "fieldTreeRetentionCategory2";
            this.fieldTreeRetentionCategory2.Visible = false;
            // 
            // fieldTreeSULE2
            // 
            this.fieldTreeSULE2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeSULE2.AreaIndex = 0;
            this.fieldTreeSULE2.Caption = "Tree SULE";
            this.fieldTreeSULE2.FieldName = "TreeSULE";
            this.fieldTreeSULE2.Name = "fieldTreeSULE2";
            this.fieldTreeSULE2.Visible = false;
            // 
            // fieldTreeSpeciesVariety2
            // 
            this.fieldTreeSpeciesVariety2.AreaIndex = 7;
            this.fieldTreeSpeciesVariety2.Caption = "Tree Species Variety";
            this.fieldTreeSpeciesVariety2.FieldName = "TreeSpeciesVariety";
            this.fieldTreeSpeciesVariety2.Name = "fieldTreeSpeciesVariety2";
            // 
            // fieldTreeObjectLength2
            // 
            this.fieldTreeObjectLength2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeObjectLength2.AreaIndex = 1;
            this.fieldTreeObjectLength2.Caption = "Tree Object Length";
            this.fieldTreeObjectLength2.FieldName = "TreeObjectLength";
            this.fieldTreeObjectLength2.Name = "fieldTreeObjectLength2";
            this.fieldTreeObjectLength2.Visible = false;
            // 
            // fieldTreeObjectWidth2
            // 
            this.fieldTreeObjectWidth2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeObjectWidth2.AreaIndex = 1;
            this.fieldTreeObjectWidth2.Caption = "Tree Object Width";
            this.fieldTreeObjectWidth2.FieldName = "TreeObjectWidth";
            this.fieldTreeObjectWidth2.Name = "fieldTreeObjectWidth2";
            this.fieldTreeObjectWidth2.Visible = false;
            // 
            // fieldTreeUserPicklist12
            // 
            this.fieldTreeUserPicklist12.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeUserPicklist12.AreaIndex = 1;
            this.fieldTreeUserPicklist12.Caption = "Tree User Picklist 1";
            this.fieldTreeUserPicklist12.FieldName = "TreeUserPicklist1";
            this.fieldTreeUserPicklist12.Name = "fieldTreeUserPicklist12";
            this.fieldTreeUserPicklist12.Visible = false;
            // 
            // fieldTreeUserPicklist22
            // 
            this.fieldTreeUserPicklist22.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeUserPicklist22.AreaIndex = 1;
            this.fieldTreeUserPicklist22.Caption = "Tree User Picklist 2";
            this.fieldTreeUserPicklist22.FieldName = "TreeUserPicklist2";
            this.fieldTreeUserPicklist22.Name = "fieldTreeUserPicklist22";
            this.fieldTreeUserPicklist22.Visible = false;
            // 
            // fieldTreeUserPicklist32
            // 
            this.fieldTreeUserPicklist32.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldTreeUserPicklist32.AreaIndex = 1;
            this.fieldTreeUserPicklist32.Caption = "Tree User Picklist 3";
            this.fieldTreeUserPicklist32.FieldName = "TreeUserPicklist3";
            this.fieldTreeUserPicklist32.Name = "fieldTreeUserPicklist32";
            this.fieldTreeUserPicklist32.Visible = false;
            // 
            // fieldInspectionUserPicklist11
            // 
            this.fieldInspectionUserPicklist11.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldInspectionUserPicklist11.AreaIndex = 1;
            this.fieldInspectionUserPicklist11.Caption = "Inspection User Picklist 1";
            this.fieldInspectionUserPicklist11.FieldName = "InspectionUserPicklist1";
            this.fieldInspectionUserPicklist11.Name = "fieldInspectionUserPicklist11";
            this.fieldInspectionUserPicklist11.Visible = false;
            // 
            // fieldInspectionUserPicklist21
            // 
            this.fieldInspectionUserPicklist21.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldInspectionUserPicklist21.AreaIndex = 1;
            this.fieldInspectionUserPicklist21.Caption = "Inspection User Picklist 2";
            this.fieldInspectionUserPicklist21.FieldName = "InspectionUserPicklist2";
            this.fieldInspectionUserPicklist21.Name = "fieldInspectionUserPicklist21";
            this.fieldInspectionUserPicklist21.Visible = false;
            // 
            // fieldInspectionUserPicklist31
            // 
            this.fieldInspectionUserPicklist31.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldInspectionUserPicklist31.AreaIndex = 1;
            this.fieldInspectionUserPicklist31.Caption = "Inspection User Picklist 3";
            this.fieldInspectionUserPicklist31.FieldName = "InspectionUserPicklist3";
            this.fieldInspectionUserPicklist31.Name = "fieldInspectionUserPicklist31";
            this.fieldInspectionUserPicklist31.Visible = false;
            // 
            // fieldActionUserPicklist1
            // 
            this.fieldActionUserPicklist1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldActionUserPicklist1.AreaIndex = 1;
            this.fieldActionUserPicklist1.Caption = "Action User Picklist 1";
            this.fieldActionUserPicklist1.FieldName = "ActionUserPicklist1";
            this.fieldActionUserPicklist1.Name = "fieldActionUserPicklist1";
            this.fieldActionUserPicklist1.Visible = false;
            // 
            // fieldActionUserPicklist2
            // 
            this.fieldActionUserPicklist2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldActionUserPicklist2.AreaIndex = 1;
            this.fieldActionUserPicklist2.Caption = "Action User Picklist 2";
            this.fieldActionUserPicklist2.FieldName = "ActionUserPicklist2";
            this.fieldActionUserPicklist2.Name = "fieldActionUserPicklist2";
            this.fieldActionUserPicklist2.Visible = false;
            // 
            // fieldActionUserPicklist3
            // 
            this.fieldActionUserPicklist3.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldActionUserPicklist3.AreaIndex = 1;
            this.fieldActionUserPicklist3.Caption = "Action User Picklist 3";
            this.fieldActionUserPicklist3.FieldName = "ActionUserPicklist3";
            this.fieldActionUserPicklist3.Name = "fieldActionUserPicklist3";
            this.fieldActionUserPicklist3.Visible = false;
            // 
            // chartControl3
            // 
            this.chartControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl3.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl3.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl3.EmptyChartText.Text = "No Data To Graph - Try Selecting Data from the Analysis Grid.";
            this.chartControl3.Legend.Name = "Default Legend";
            this.chartControl3.Location = new System.Drawing.Point(0, 0);
            this.chartControl3.Name = "chartControl3";
            this.chartControl3.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            sideBySideBarSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl3.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.chartControl3.Size = new System.Drawing.Size(949, 379);
            this.chartControl3.TabIndex = 1;
            this.chartControl3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chartControl3_MouseUp);
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Copy to Clipboard";
            this.barButtonItem1.Hint = "Copy the selected cells to the Clipboard for pasting to external application.";
            this.barButtonItem1.Id = 25;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.deInspectionFromDate;
            this.layoutControlItem26.CustomizationFormText = "From Date:";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem26.Name = "layoutControlItem10";
            this.layoutControlItem26.Size = new System.Drawing.Size(182, 31);
            this.layoutControlItem26.Text = "From Date:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(54, 13);
            // 
            // pmChart
            // 
            this.pmChart.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateAxis),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChartWizard, true)});
            this.pmChart.Manager = this.barManager1;
            this.pmChart.MenuCaption = "Chart Menu";
            this.pmChart.Name = "pmChart";
            // 
            // bbiRotateAxis
            // 
            this.bbiRotateAxis.Caption = "Rotate Axis";
            this.bbiRotateAxis.Hint = "Rotate Chart Axis";
            this.bbiRotateAxis.Id = 27;
            this.bbiRotateAxis.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRotateAxis.ImageOptions.Image")));
            this.bbiRotateAxis.Name = "bbiRotateAxis";
            this.bbiRotateAxis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAxis_ItemClick);
            // 
            // bbiChartWizard
            // 
            this.bbiChartWizard.Caption = "Chart Wizard";
            this.bbiChartWizard.Hint = "Open Chart Wizard";
            this.bbiChartWizard.Id = 26;
            this.bbiChartWizard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiChartWizard.ImageOptions.Image")));
            this.bbiChartWizard.Name = "bbiChartWizard";
            this.bbiChartWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChartWizard_ItemClick);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // frm_AT_Analysis
            // 
            this.ClientSize = new System.Drawing.Size(1293, 850);
            this.Controls.Add(this.xtraTabControl2);
            this.Controls.Add(this.dockPanel1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Analysis";
            this.Text = "Amenity Trees - Data Analysis";
            this.Activated += new System.EventHandler(this.frm_AT_Analysis_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_AT_Analysis_FormClosed);
            this.Load += new System.EventHandler(this.frm_AT_Analysis_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.Controls.SetChildIndex(this.xtraTabControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01234ATDataAnalysisTreesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01220ATReportsListingsTreeFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01230ATReportsListingsInspectionFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceLastInspectionOnly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl7)).EndInit();
            this.layoutControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01232ATReportsListingsActionFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01235ATDataAnalysisInspectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01236ATDataAnalysisActionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.SimpleButton btnAnalyze;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SimpleButton btnLoadTrees;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp01220ATReportsListingsTreeFilterBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colVisibility;
        private DevExpress.XtraGrid.Columns.GridColumn colContext;
        private DevExpress.XtraGrid.Columns.GridColumn colManagement;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroundType;
        private DevExpress.XtraGrid.Columns.GridColumn colDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colProtectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteHazardClass;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colMapLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget1;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget2;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget3;
        private DevExpress.XtraGrid.Columns.GridColumn colLastModifiedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colAgeClass;
        private DevExpress.XtraGrid.Columns.GridColumn colX;
        private DevExpress.XtraGrid.Columns.GridColumn colY;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaHa;
        private DevExpress.XtraGrid.Columns.GridColumn colReplantCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeType;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeLength;
        private DevExpress.XtraGrid.Columns.GridColumn colGardenSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPropertyProximity;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSituation;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colcolor;
        private WoodPlan5.DataSet_ATTableAdapters.sp01220_AT_Reports_Listings_Tree_FilterTableAdapter sp01220_AT_Reports_Listings_Tree_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT_Reports dataSet_AT_Reports;
        private WoodPlan5.DataSet_AT_ReportsTableAdapters.sp01234_AT_Data_Analysis_TreesTableAdapter sp01234_AT_Data_Analysis_TreesTableAdapter;
        private System.Windows.Forms.BindingSource sp01234ATDataAnalysisTreesBindingSource;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReference;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMappingID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldGridReference;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDistance;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHouseName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSpeciesName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccess;
        private DevExpress.XtraPivotGrid.PivotGridField fieldVisibility;
        private DevExpress.XtraPivotGrid.PivotGridField fieldContext;
        private DevExpress.XtraPivotGrid.PivotGridField fieldManagement;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLegalStatus;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCycle;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUnit;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldGroundType;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDBH;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDBHRange;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHeight;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHeightRange;
        private DevExpress.XtraPivotGrid.PivotGridField fieldProtectionType;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCrownDiameter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPlantDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldGroupNumber;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRiskCategory;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteHazardClass;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPlantSize;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPlantSource;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPlantMethod;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPostcode;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMapLabel;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStatus;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSize;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTarget1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTarget2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTarget3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastModifiedDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRemarks;
        private DevExpress.XtraPivotGrid.PivotGridField fieldUser1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldUser2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldUser3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAgeClass;
        private DevExpress.XtraPivotGrid.PivotGridField fieldX;
        private DevExpress.XtraPivotGrid.PivotGridField fieldY;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAreaHa;
        private DevExpress.XtraPivotGrid.PivotGridField fieldReplantCount;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeType;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHedgeOwner;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHedgeLength;
        private DevExpress.XtraPivotGrid.PivotGridField fieldGardenSize;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPropertyProximity;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteLevel;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSituation;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRiskFactor;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPolygonXY;
        private DevExpress.XtraPivotGrid.PivotGridField fieldcolor;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOwnershipName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCount;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.DateEdit deInspectionFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.DateEdit deInspectionToDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.CheckEdit ceLastInspectionOnly;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.SimpleButton btnLoadInspections;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private WoodPlan5.DataSet_ATTableAdapters.sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp01230ATReportsListingsInspectionFilterBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectorName;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colStemPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colStemPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colStemPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colStemDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colStemDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colStemDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colAngleToVertical;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownFoliation1;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownFoliation2;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownFoliation3;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskCategory1;
        private DevExpress.XtraGrid.Columns.GridColumn colGeneralCondition;
        private DevExpress.XtraGrid.Columns.GridColumn colRootHeave1;
        private DevExpress.XtraGrid.Columns.GridColumn colRootHeave2;
        private DevExpress.XtraGrid.Columns.GridColumn colRootHeave3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateLastModified;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser11;
        private DevExpress.XtraGrid.Columns.GridColumn colUser21;
        private DevExpress.XtraGrid.Columns.GridColumn colUser31;
        private DevExpress.XtraGrid.Columns.GridColumn colcolor1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl2;
        private DevExpress.XtraCharts.ChartControl chartControl2;
        private WoodPlan5.DataSet_AT_ReportsTableAdapters.sp01235_AT_Data_Analysis_InspectionsTableAdapter sp01235_AT_Data_Analysis_InspectionsTableAdapter;
        private System.Windows.Forms.BindingSource sp01235ATDataAnalysisInspectionsBindingSource;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReference1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeMappingID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeGridReference;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeDistance;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHouseName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSpeciesName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeAccess;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeVisibility;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeContext;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeManagement;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeLegalStatus;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeLastInspectionDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeInspectionCycle;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeInspectionUnit;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeNextInspectionDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeGroundType;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeDBH;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeDBHRange;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHeight;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHeightRange;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeProtectionType;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownDiameter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePlantDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeGroupNumber;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeRiskCategory;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSiteHazardClass;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePlantSize;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePlantSource;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePlantMethod;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePostcode;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeMapLabel;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeStatus;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSize;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeTarget1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeTarget2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeTarget3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeLastModifiedDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeRemarks;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUser1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUser2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUser3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeAgeClass;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeX;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeY;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeAreaHa;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReplantCount;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSurveyDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHedgeOwner;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHedgeLength;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeGardenSize;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePropertyProximity;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSiteLevel;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSituation;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeRiskFactor;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePolygonXY;
        private DevExpress.XtraPivotGrid.PivotGridField fieldcolor1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeOwnershipName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionReference;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionInspector;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionIncidentReference;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionIncidentDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIncidentID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionAngleToVertical;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionLastModified;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserDefined1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserDefined2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserDefined3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionRemarks;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemPhysical1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemPhysical2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemPhysical3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemDisease1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemDisease2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemDisease3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownPhysical1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownPhysical2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownPhysical3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownDisease1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownDisease2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownDisease3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownFoliation1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownFoliation2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownFoliation3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionRiskCategory;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionGeneralCondition;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionBasePhysical1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionBasePhysical2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionBasePhysical3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionVitality;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCount;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl3;
        private DevExpress.XtraCharts.ChartControl chartControl3;
        private DevExpress.XtraLayout.LayoutControl layoutControl7;
        private DevExpress.XtraEditors.SimpleButton btnLoadActions;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private WoodPlan5.DataSet_ATTableAdapters.sp01232_AT_Reports_Listings_Action_FilterTableAdapter sp01232_AT_Reports_Listings_Action_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp01232ATReportsListingsActionFilterBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientNameName3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGridReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDistance1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHouseName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectorName1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentReferenceNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colActionOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudget;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colDateLastModified1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colUser12;
        private DevExpress.XtraGrid.Columns.GridColumn colUser22;
        private DevExpress.XtraGrid.Columns.GridColumn colUser32;
        private DevExpress.XtraGrid.Columns.GridColumn colcolor2;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostDifference;
        private WoodPlan5.DataSet_AT_ReportsTableAdapters.sp01236_AT_Data_Analysis_ActionsTableAdapter sp01236_AT_Data_Analysis_ActionsTableAdapter;
        private System.Windows.Forms.BindingSource sp01236ATDataAnalysisActionsBindingSource;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeID2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteID2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientID2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteName2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientName3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReference2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeMappingID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeGridReference1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeDistance1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHouseName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSpeciesName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeAccess1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeVisibility1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeContext1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeManagement1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeLegalStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeLastInspectionDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeInspectionCycle1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeInspectionUnit1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeNextInspectionDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeGroundType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeDBH1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeDBHRange1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHeight1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHeightRange1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeProtectionType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownDiameter1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePlantDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeGroupNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeRiskCategory1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSiteHazardClass1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePlantSize1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePlantSource1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePlantMethod1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePostcode1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeMapLabel1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSize1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeTarget11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeTarget21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeTarget31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeLastModifiedDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeRemarks1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUser11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUser21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUser31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeAgeClass1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeX1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeY1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeAreaHa1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReplantCount1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSurveyDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeType2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHedgeOwner1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeHedgeLength1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeGardenSize1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePropertyProximity1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSiteLevel1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSituation1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeRiskFactor1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreePolygonXY1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldcolor2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeOwnershipName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionReference1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionInspector1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionIncidentReference1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionIncidentDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIncidentID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionAngleToVertical1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionLastModified1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserDefined11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserDefined21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserDefined31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionRemarks1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemPhysical11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemPhysical21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemPhysical31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemDisease11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemDisease21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionStemDisease31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownPhysical11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownPhysical21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownPhysical31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownDisease11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownDisease21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownDisease31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownFoliation11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownFoliation21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCrownFoliation31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionRiskCategory1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionGeneralCondition1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionBasePhysical11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionBasePhysical21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionBasePhysical31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionVitality1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionJobNumber;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAction;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionPriority;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionDueDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionDoneDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionBy;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionSupervisor;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionOwnership;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCostCentre;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionBudget;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionWorkUnits;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionScheduleOfRates;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionBudgetedRateDescription;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionBudgetedRate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionBudgetedCost;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionActualRateDescription;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionActualRate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionActualCost;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionDateLastModified;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionWorkOrder;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionRemarks;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionUserDefined1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionUserDefined2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionUserDefined3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCostDifference;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCount;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionMonth;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeLastInspectionYear;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeLastInspectionQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeLastInspectionMonth;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeNextInspectionYear;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeNextInspectionQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeNextInspectionMonth;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedInspectionYear;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedInspectionQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedInspectionMonth;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeLastInspectionYear3;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeLastInspectionQuarter3;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeLastInspectionMonth3;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeNextInspectionYear3;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeNextInspectionQuarter3;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedTreeNextInspectionMonth3;
        private DevExpress.XtraPivotGrid.PivotGridField InspectionYear2;
        private DevExpress.XtraPivotGrid.PivotGridField InspectionQuarter2;
        private DevExpress.XtraPivotGrid.PivotGridField InspectionMonth2;
        private DevExpress.XtraPivotGrid.PivotGridField ActionDueYear;
        private DevExpress.XtraPivotGrid.PivotGridField ActionDueQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField ActionDueMonth;
        private DevExpress.XtraPivotGrid.PivotGridField ActionDoneYear;
        private DevExpress.XtraPivotGrid.PivotGridField ActionDoneQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField ActionDoneMonth;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedInspectionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLinkedInspectionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedOutstandingActionCount;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLinkedActionCount;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLinkedOutstandingActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTimeliness;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCompletionStatus;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionTimeliness;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colNoFurtherActionRequired;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNoFurtherActionRequired;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.DateEdit deActionToDate;
        private DevExpress.XtraEditors.DateEdit deActionFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1b;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2b;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3b;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1c;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2c;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3c;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrderCompleteDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrderIssueDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionWorkOrderCompleteDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionWorkOrderIssueDate;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedActionWorkOrderCompleteYear;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedActionWorkOrderCompleteQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedActionWorkOrderCompleteMonth;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedActionWorkOrderIssueYear;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedActionWorkOrderIssueQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField calculatedActionWorkOrderIssueMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPlantYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPlantQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPlantMonth;
        private DevExpress.XtraGrid.Columns.GridColumn colCavat;
        private DevExpress.XtraGrid.Columns.GridColumn colStemCount;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownNorth;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownSouth;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownEast;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownWest;
        private DevExpress.XtraGrid.Columns.GridColumn colRetentionCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colSULE;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesVariety;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectLength;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist11;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist21;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist31;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist12;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist22;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist32;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCavat;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeStemCount;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownNorth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownSouth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownEast;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownWest;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeRetentionCategory;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSULE;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSpeciesVariety;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUserPicklist1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUserPicklist2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUserPicklist3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeObjectLength;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeObjectWidth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCavat1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeStemCount1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownNorth1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownSouth1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownEast1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownWest1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeRetentionCategory1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSULE1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSpeciesVariety1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUserPicklist11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUserPicklist21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUserPicklist31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeObjectLength1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeObjectWidth1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserPicklist1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserPicklist2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserPicklist3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCavat2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeStemCount2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownNorth2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownSouth2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownEast2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeCrownWest2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeRetentionCategory2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSULE2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeSpeciesVariety2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeObjectLength2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeObjectWidth2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUserPicklist12;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUserPicklist22;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeUserPicklist32;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserPicklist11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserPicklist21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUserPicklist31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionUserPicklist1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionUserPicklist2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionUserPicklist3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.BarButtonItem bbiChartWizard;
        private DevExpress.XtraBars.PopupMenu pmChart;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAxis;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
    }
}

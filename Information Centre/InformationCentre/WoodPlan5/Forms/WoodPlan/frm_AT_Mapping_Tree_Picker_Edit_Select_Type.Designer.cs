﻿namespace WoodPlan5
{
    partial class frm_AT_Mapping_Tree_Picker_Edit_Select_Type
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Mapping_Tree_Picker_Edit_Select_Type));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.btnBoth = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnAssets = new DevExpress.XtraEditors.SimpleButton();
            this.btnAmenityTrees = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(51, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(338, 86);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = resources.GetString("labelControl1.Text");
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.Info_32x32;
            this.pictureEdit1.Location = new System.Drawing.Point(12, 12);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(30, 33);
            this.pictureEdit1.TabIndex = 15;
            // 
            // btnBoth
            // 
            this.btnBoth.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            this.btnBoth.Location = new System.Drawing.Point(217, 141);
            this.btnBoth.Name = "btnBoth";
            this.btnBoth.Size = new System.Drawing.Size(96, 23);
            this.btnBoth.TabIndex = 13;
            this.btnBoth.Text = "Edit Both";
            this.btnBoth.Click += new System.EventHandler(this.btnBoth_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.btnCancel.Location = new System.Drawing.Point(319, 141);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(70, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAssets
            // 
            this.btnAssets.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            this.btnAssets.Location = new System.Drawing.Point(12, 141);
            this.btnAssets.Name = "btnAssets";
            this.btnAssets.Size = new System.Drawing.Size(199, 23);
            this.btnAssets.TabIndex = 11;
            this.btnAssets.Text = "Edit Selected Asset Objects";
            this.btnAssets.Click += new System.EventHandler(this.btnAssets_Click);
            // 
            // btnAmenityTrees
            // 
            this.btnAmenityTrees.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAmenityTrees.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            this.btnAmenityTrees.Location = new System.Drawing.Point(12, 112);
            this.btnAmenityTrees.Name = "btnAmenityTrees";
            this.btnAmenityTrees.Size = new System.Drawing.Size(199, 23);
            this.btnAmenityTrees.TabIndex = 10;
            this.btnAmenityTrees.Text = "Edit Selected Amenity Tree Objects";
            this.btnAmenityTrees.Click += new System.EventHandler(this.btnAmenityTrees_Click);
            // 
            // frm_AT_Mapping_Tree_Picker_Edit_Select_Type
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(401, 174);
            this.ControlBox = false;
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnBoth);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAssets);
            this.Controls.Add(this.btnAmenityTrees);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_AT_Mapping_Tree_Picker_Edit_Select_Type";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mapping - Select Object Type(s) To Edit";
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnBoth;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnAssets;
        private DevExpress.XtraEditors.SimpleButton btnAmenityTrees;
    }
}
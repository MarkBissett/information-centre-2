using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.LookAndFeel;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using System.IO;  // Used by File.Delete //
using System.Data.SqlClient;  // Used by Generate Map process //
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_AT_Report_Tree_Listing : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;

        WaitDialogForm loadingForm;
        private int i_int_SelectedLayoutID = 0;
        
        public string i_str_SavedDirectoryName = "";
        
        private string i_str_SelectedFilename = "";
        private int i_int_FocusedGrid = 1;
        private string i_str_selected_district_ids = "";
        private string i_str_selected_district_names = "";
        private string i_str_selected_district_ids2 = "";
        private string i_str_selected_district_names2 = "";
        private string i_str_selected_district_ids3 = "";
        private string i_str_selected_district_names3 = "";


        string i_str_selected_client_ids = "";
        string i_str_selected_client_names = "";

        string i_str_selected_client_ids2 = "";
        string i_str_selected_client_names2 = "";

        string i_str_selected_client_ids3 = "";
        string i_str_selected_client_names3 = "";

        string i_str_selected_trees = "";
        string i_str_selected_inspections = "";
        string i_str_selected_actions = "";

        private int i_intReportType = 2;

        private DataSet_Selection DS_Selection1;
        private DataSet_Selection DS_Selection2;
        private DataSet_Selection DS_Selection3;

        rpt_AT_Report_Layout_Listing_Blank rptReport;
        #endregion

        public frm_AT_Report_Tree_Listing()
        {
            InitializeComponent();
        }

        private void frm_AT_Report_Tree_Listing_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 92001;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            printPreviewBarItem44.Enabled = false;  // Switch off Open button //

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            sp01206_AT_Report_Available_LayoutsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, 2, "AmenityTreesReportLayoutLocation");
            
            sp01220_AT_Reports_Listings_Tree_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01232_AT_Reports_Listings_Action_FilterTableAdapter.Connection.ConnectionString = strConnectionString;

            // Get default layout file directory from System_Settings table //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            i_str_SavedDirectoryName = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesReportLayoutLocation")) + "\\";

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridView1, strConnectionString);  // Trees List //
            DS_Selection2 = new Utilities.DataSet_Selection((GridView)gridView4, strConnectionString);  // Inspections List //
            DS_Selection3 = new Utilities.DataSet_Selection((GridView)gridView7, strConnectionString);  // Actions List //

            btnLayoutAddNew.Enabled = iBool_AllowEdit;
        }

        public override void PostOpen(object objParameter)
        {
            dockPanel1.Width = 450;
            
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            this.Refresh();

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }
        
        private void frm_AT_Report_Tree_Listing_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            bbiPublishToWeb.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            bbiPublishToWeb.Enabled = false;
        
            alItems.AddRange(new string[] { "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            if (i_int_FocusedGrid == 99)
            {
                bbiPublishToWeb.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                bbiPublishToWeb.Enabled = true;

                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                    iBool_AllowDelete = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                    iBool_AllowDelete = false;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void LoadFilterList()
        {
            //GridView view = (GridView)sp01205_AT_Report_Workorder_ListGridControl.MainView;

            //view.BeginUpdate();
            //sp01205_AT_Report_Workorder_ListTableAdapter.Fill(this.dataSet_WorkOrders.sp01205_AT_Report_Workorder_List, Convert.ToDateTime(FromDate.Text), Convert.ToDateTime(ToDate.Text));
            //view.EndUpdate();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (popupContainerEdit1.Text == "No Report Layout Selected")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, no layout specified.\n\nSelect a layout to use from the Layout list then try again.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (printControl1.PrintingSystem.Document.Name != null)  // Clear any report currently being viewed //
            {
                printControl1.PrintingSystem.ClearContent();
                rptReport = null;
            }

            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            switch (xtraTabControl1.SelectedTabPage.Text)
            {
                case "Trees":
                    i_str_selected_trees = "";
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to report on before proceeding!", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        i_str_selected_trees += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                    }
                    rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, i_str_selected_trees, i_intReportType, checkEdit_LoadLinkedData.Checked, checkEdit_LoadLinkedPictures.Checked);
                    break;
                case "Inspections":
                    i_str_selected_inspections = "";
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to report on before proceeding!", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        i_str_selected_inspections += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                    }
                    rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, i_str_selected_inspections, i_intReportType, checkEdit_LoadLinkedData.Checked, checkEdit_LoadLinkedPictures.Checked);
                    break;
                case "Actions":
                    i_str_selected_actions = "";
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to report on before proceeding!", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        i_str_selected_actions += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ActionID"])) + ',';
                    }
                    rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, i_str_selected_actions, i_intReportType, checkEdit_LoadLinkedData.Checked, checkEdit_LoadLinkedPictures.Checked);
                    break;
                case "Work Orders":
                    break;
                case "Incidents":
                    break;
                case "Import History":
                    break;
                default:
                    return;
            }
            loadingForm = new WaitDialogForm("Loading Report...", "Reporting");
            loadingForm.Show();

            try
            {
                rptReport.LoadLayout(i_str_SavedDirectoryName + i_str_SelectedFilename);
            }
            catch (Exception Ex)
            {
                loadingForm.Close();
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            AdjustEventHandlers(rptReport);  // Tie in custom Sorting //
            
            printControl1.PrintingSystem = rptReport.PrintingSystem;
            printingSystem1.Begin();
            rptReport.CreateDocument();
            printingSystem1.End();
            loadingForm.Close();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            DeleteRecord();
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
            string strReportLayoutName = Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
            string strReportFileName = Convert.ToString(intReportLayoutID) + ".repx";
            if (intReportLayoutID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout, no layout selected for deletion!\n\nSelect a layout before proceeding.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to delete the selected layout: " + strReportLayoutName + "?\n\nIf you proceed, the layout will no longer be available for use!", "Delete Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                loadingForm = new WaitDialogForm("Deleting Layout...", "Reporting");
                loadingForm.Show();

                // Delete the record then the physical file layout //
                DataSet_ATTableAdapters.QueriesTableAdapter DeleteLayout = new DataSet_ATTableAdapters.QueriesTableAdapter();
                DeleteLayout.ChangeConnectionString(strConnectionString);
                DeleteLayout.sp01213_AT_Report_Layouts_Delete_Layout_Record(intReportLayoutID);


                try
                {
                    File.Delete(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout, it may no longer exist or you may not have the necessary permissions to delete it - contact Technical Support.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                // Reload list of available layouts //
                sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, i_intReportType, "AmenityTreesReportLayoutLocation");          
                popupContainerEdit1.Text = "No Report Layout Selected";
                i_int_SelectedLayoutID = 0;
                loadingForm.Close();
            }

        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            EditRecord();
        }

        private void EditRecord()
        {
            GridView View = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(View.GetFocusedRowCellValue("ReportLayoutID"));
            string strReportLayoutName = Convert.ToString(View.GetFocusedRowCellValue("ReportLayoutName"));
            string strReportFileName = Convert.ToString(intReportLayoutID) + ".repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                switch (xtraTabControl1.SelectedTabPage.Text)
                {
                    case "Trees":
                        rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, i_str_selected_trees, i_intReportType, checkEdit_LoadLinkedData.Checked, checkEdit_LoadLinkedPictures.Checked);
                        break;
                    case "Inspections":
                        rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, i_str_selected_inspections, i_intReportType, checkEdit_LoadLinkedData.Checked, checkEdit_LoadLinkedPictures.Checked);
                        break;
                    case "Actions":
                        rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, i_str_selected_actions, i_intReportType, checkEdit_LoadLinkedData.Checked, checkEdit_LoadLinkedPictures.Checked);
                        break;
                    case "Work Orders":
                        break;
                    case "Incidents":
                        break;
                    case "Import History":
                        break;
                    default:
                        return;
                }
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                loadingForm = new WaitDialogForm("Loading Report Builder...", "Reporting");
                loadingForm.Show();

                // Open the report in the Report Builder //
                // Create a design form and get its panel.
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);                

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                loadingForm.Close();
                form.ShowDialog();
                panel.CloseReport();
            }
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            if (!iBool_AllowAdd) return;
            AddLayout();
        }
        
        void form_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Text)
            {
                case "Trees":
                    i_intReportType = 2;
                    break;
                case "Inspections":
                    i_intReportType = 3;
                    break;
                case "Actions":
                    i_intReportType = 4;
                    break;
                case "Work Orders":
                    i_intReportType = 5;
                    break;
                case "Incidents":
                    i_intReportType = 6;
                    break;
                case "Import History":
                    i_intReportType = 7;
                    break;
                default:
                    i_intReportType = 0;
                    break;
            }
            sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, i_intReportType, "AmenityTreesReportLayoutLocation");
            popupContainerEdit1.Text = "No Report Layout Selected";
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 2 || i_int_FocusedGrid == 3)
            {
                string strType = "";
                string strFieldName = "";
                GridView view = null;
                switch (i_int_FocusedGrid)
                {
                    case 1:
                        view = (GridView)gridControl1.MainView;
                        strFieldName = "TreeID";
                        strType = "tree";
                        break;
                    case 2:
                        view = (GridView)gridControl2.MainView;
                        strFieldName = "InspectionID";
                        strType = "inspection";
                        break;
                    case 3:
                        view = (GridView)gridControl3.MainView;
                        strFieldName = "ActionID";
                        strType = "action";
                        break;
                    default:
                        return;
                }
                view.PostEditor();
                string strSelectedIDs = "";
                int[] intRowHandles;
                int intCount = 0;
                intRowHandles = view.GetSelectedRows();
                intCount = intRowHandles.Length;
                if (intCount <= 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                foreach (int intRowHandle in intRowHandles)
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, strFieldName)) + ',';
                }
                try
                {
                    Mapping_Functions MapFunctions = new Mapping_Functions();
                    MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, strType);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = "";
            string strSelectedTrees = "";
            string strSelectedTree = "";
            int intSelectedTreeCount = 0;
            string strSelectedInspections = "";
            string strSelectedInspection = "";
            int intSelectedInspectionCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Trees Grid //
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Trees //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                    }
                    CreateDataset("Tree", intCount, strSelectedIDs, 0, "", 0, "");
                    break;
                case 2:  // Available Inspections Grid //
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Inspections //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                    }
                    CreateDataset("Inspection", intSelectedTreeCount, strSelectedTrees, intCount, strSelectedIDs, 0, "");
                    break;
                case 3:  // Available Action Grid //
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Actions //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ActionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ActionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                        // Inspections //
                        if (strSelectedInspections == "")
                        {
                            strSelectedInspections = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                            intSelectedInspectionCount++;
                        }
                        else
                        {
                            strSelectedInspection = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedInspections.Contains("," + strSelectedInspection))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedInspections += strSelectedInspection;
                                intSelectedInspectionCount++;
                            }
                        }
                    }
                    CreateDataset("Action", intSelectedTreeCount, strSelectedTrees, intSelectedInspectionCount, strSelectedInspections, intCount, strSelectedIDs);
                    break;
                default:
                    return;
            }
        }

        private void CreateDataset(string strType, int intTreeCount, string strSelectedTreeIDs, int intInspectionCount, string strSelectedInspectionIDs, int intActionCount, string strSelectedActionIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            //Form frmMain = this.MdiParent;
            //frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_tree_count = intTreeCount;
            fChildForm.i_int_selected_inspection_count = intInspectionCount;
            fChildForm.i_int_selected_action_count = intActionCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                string strSelectedIDs = "";
                switch (strDatasetType.ToUpper())
                {
                    case "TREE":
                        strSelectedIDs = strSelectedTreeIDs;
                        break;
                    case "INSPECTION":
                        strSelectedIDs = strSelectedInspectionIDs;
                        break;
                    case "ACTION":
                        strSelectedIDs = strSelectedActionIDs;
                        break;
                    default:
                        strSelectedIDs = "";
                        break;
                }
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }

        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Trees Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 2:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 3:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Trees Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 2:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 3:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region PopupContainerLayout

        private void btnLayoutOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void btnLayoutAddNew_Click(object sender, EventArgs e)
        {
            if (!iBool_AllowAdd) return;
            AddLayout();
        }

        private void AddLayout()
        {
            Form frmMain = this.MdiParent;
            frm_AT_Report_Add_Layout fChildForm = new frm_AT_Report_Add_Layout();
            frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intReportType = i_intReportType;
            fChildForm.boolPublishedToWebEnabled = true;
            int intNewLayoutID = 0;
            string strNewLayoutName = "";
            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                return;
            }
            intNewLayoutID = fChildForm.intReturnedValue;
            strNewLayoutName = fChildForm.strReturnedLayoutName;
            int intPublishedToWeb = fChildForm.intPublishedToWeb;

            // Save the new Layout and get back it's ID for the physical filename //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intNewID = 0;
            intNewID = Convert.ToInt32(GetSetting.sp01212_AT_Report_Add_Layouts_Create_Layout_Record(1, i_intReportType, strNewLayoutName, this.GlobalSettings.UserID, intPublishedToWeb));
            if (intNewID <= 0 || i_intReportType <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create new layout, a problem occurred - contact Technical Support.", "Create Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // Load the report - Create a design form and get its panel.
            XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
            //XRDesignFormEx form = new XRDesignFormEx();

            XRDesignPanel panel = form.DesignPanel;
            panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);                

            switch (xtraTabControl1.SelectedTabPage.Text)
            {
                case "Trees":
                    rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, i_str_selected_trees, i_intReportType, checkEdit_LoadLinkedData.Checked, checkEdit_LoadLinkedPictures.Checked);
                    //rptReport.Name = "Tree Listing";  // Set Document Root Node to meaningful text //
                    break;
                case "Inspections":
                    rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, i_str_selected_inspections, i_intReportType, checkEdit_LoadLinkedData.Checked, checkEdit_LoadLinkedPictures.Checked);
                    //rptReport.Name = "Tree Listing";  // Set Document Root Node to meaningful text //
                    break;
                case "Actions":
                    rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, i_str_selected_actions, i_intReportType, checkEdit_LoadLinkedData.Checked, checkEdit_LoadLinkedPictures.Checked);
                    //rptReport.Name = "Tree Listing";  // Set Document Root Node to meaningful text //
                    break;
                case "Work Orders":
                    break;
                case "Incidents":
                    break;
                case "Import History":
                    break;
                default:
                    return;
            }

            if (intNewLayoutID > 0)
            {
                i_str_SelectedFilename = Convert.ToInt32(intNewLayoutID) + ".repx";
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + i_str_SelectedFilename);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            // Set filename to correct one so that when save fires from the report designer, the correct file is created/updated //
            i_str_SelectedFilename = Convert.ToInt32(intNewID) + ".repx";

            // Add a new command handler to the Report Designer which saves the report in a custom way.
            panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, i_str_SelectedFilename));

            // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
            panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

            loadingForm = new WaitDialogForm("Loading Report Builder...", "Reporting");
            loadingForm.Show();

            // Load the report into the design form and show the form.
            panel.OpenReport(rptReport);
            form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
            form.WindowState = FormWindowState.Maximized;
            loadingForm.Close();

            form.ShowDialog();
            panel.CloseReport();

            // Reload list of available layout and pre-select new one //
            sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, i_intReportType, "AmenityTreesReportLayoutLocation");
            GridView view = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.MainView;
            Int32 intFoundRow = view.LocateByValue(0, view.Columns["ReportLayoutID"], intNewID);
            if (intFoundRow > -1)
            {
                view.FocusedRowHandle = intFoundRow;
            }
            popupContainerEdit1.Text = PopupContainerEdit1_Get_Layout_Selected();  // Set Selected Layout Text //
        }

        private void popupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Layout_Selected();
        }

        private string PopupContainerEdit1_Get_Layout_Selected()
        {
            int[] intRowHandles;
            int intCount = 0;

            GridView view = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                i_int_SelectedLayoutID = 0;
                return "No Report Layout Selected";
            }
            else
            {
                i_int_SelectedLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
                i_str_SelectedFilename = Convert.ToString(i_int_SelectedLayoutID) + ".repx";
                return Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
            }

        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 99;
            SetMenuStatus();
        }

        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {

        }

        private void sp01206_AT_Report_Available_LayoutsGridControl_DoubleClick(object sender, EventArgs e)
        {
            if (!iBool_AllowEdit) return;
            EditRecord();
        }

        #endregion


        #region Page_1_Trees

        private void btnLoadTrees_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Trees...");
            }

            if (i_str_selected_client_ids == null) i_str_selected_client_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01220_AT_Reports_Listings_Tree_FilterTableAdapter.Fill(this.dataSet_AT.sp01220_AT_Reports_Listings_Tree_Filter, i_str_selected_client_ids);
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 2 || i_int_FocusedGrid == 3)
                {
                    if (view.SelectedRowsCount > 0)
                    {
                        bbiDatasetCreate.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetCreate.Enabled = false;
                    }
                    if (view.RowCount > 0)
                    {
                        bbiDatasetSelection.Enabled = true;
                        bsiDataset.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetSelection.Enabled = false;
                        bsiDataset.Enabled = false;
                    }
                    bsiDataset.Enabled = true;
                    bbiDatasetManager.Enabled = true;
                    bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        /*private void PopupMenuAddToGroup(Object sender, EventArgs e)
        {
            // Handle the Expand and Select All menu item's click event //
            DXMenuItem Item = sender as DXMenuItem;
            //GridViewColumnMenu menu = Item.Tag as GridViewColumnMenu;
        }*/

        private void buttonEditClientFilter_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids = "";
                i_str_selected_client_names = "";
                buttonEditClientFilter.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;
                }
            }
        }

        #endregion


        #region Page_2_Inspections

        private void btnLoadInspections_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Inspections...");
            }
            int intLastInspectionOnly = 0;
            if (ceLastInspectionOnly.Checked) intLastInspectionOnly = 1;

            DateTime? dtFromDate = Convert.ToDateTime(deInspectionFromDate.DateTime);
            if (dtFromDate <= Convert.ToDateTime("2001-01-01")) dtFromDate = null;
            DateTime? dtToDate = Convert.ToDateTime(deInspectionToDate.DateTime);
            if (dtToDate <= Convert.ToDateTime("2001-01-01")) dtToDate = null;

            if (i_str_selected_client_ids2 == null) i_str_selected_client_ids2 = "";
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter.Fill(this.dataSet_AT.sp01230_AT_Reports_Listings_Inspection_Filter, i_str_selected_client_ids2, dtFromDate, dtToDate, intLastInspectionOnly);
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }
        
        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView4_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 2 || i_int_FocusedGrid == 3)
                {
                    if (view.SelectedRowsCount > 0)
                    {
                        bbiDatasetCreate.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetCreate.Enabled = false;
                    }
                    if (view.RowCount > 0)
                    {
                        bbiDatasetSelection.Enabled = true;
                        bsiDataset.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetSelection.Enabled = false;
                        bsiDataset.Enabled = false;
                    }
                    bsiDataset.Enabled = true;
                    bbiDatasetManager.Enabled = true;
                    bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void buttonEditClientFilter2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids2 = "";
                i_str_selected_client_names2 = "";
                buttonEditClientFilter2.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids2;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids2 = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names2 = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter2.Text = i_str_selected_client_names;
                }
            }
        }

        #endregion


        #region Page_3_Actions

        private void btnLoadActions_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Actions...");
            }

            DateTime? dtFromDate = Convert.ToDateTime(deActionFromDate.DateTime);
            if (dtFromDate <= Convert.ToDateTime("1900-01-01")) dtFromDate = null;
            DateTime? dtToDate = Convert.ToDateTime(deActionToDate.DateTime);
            if (dtToDate <= Convert.ToDateTime("1900-01-01")) dtToDate = null;

            if (i_str_selected_client_ids3 == null) i_str_selected_client_ids3 = "";
            GridView view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            sp01232_AT_Reports_Listings_Action_FilterTableAdapter.Fill(this.dataSet_AT.sp01232_AT_Reports_Listings_Action_Filter, i_str_selected_client_ids3, dtFromDate, dtToDate);
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView7_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 2 || i_int_FocusedGrid == 3)
                {
                    if (view.SelectedRowsCount > 0)
                    {
                        bbiDatasetCreate.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetCreate.Enabled = false;
                    }
                    if (view.RowCount > 0)
                    {
                        bbiDatasetSelection.Enabled = true;
                        bsiDataset.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetSelection.Enabled = false;
                        bsiDataset.Enabled = false;
                    }
                    bsiDataset.Enabled = true;
                    bbiDatasetManager.Enabled = true;
                    bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void buttonEditClientFilter3_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids3 = "";
                i_str_selected_client_names3 = "";
                buttonEditClientFilter3.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids3;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids3 = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names3 = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter3.Text = i_str_selected_client_names;
                }
            }
        }

        #endregion


        #region Page_4_WorkOrders

        #endregion


        #region Page_5_Incidents

        #endregion

        
        #region Page_6_Import_History

        #endregion


        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Report...", "Reporting");
                loadingForm.Show();
                
                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }

        private void bbiPublishToWeb_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView View = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(View.GetFocusedRowCellValue("ReportLayoutID"));
            int intPublished = Convert.ToInt32(View.GetFocusedRowCellValue("PublishedToWeb"));
            string strMessage = "";
            if (intReportLayoutID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Toggle Published Status of Layout, no layout selected.\n\nSelect a layout before proceeding.", "Toggle Web Published Status of Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intPublished == 1)
            {
                strMessage = "You are about to hide the selected report layout from the WoodPlan Web Interface.\n\nAre you sure you wish to proceed?";
            }
            else
            {
                strMessage = "You are about to make the selected report layout available to the WoodPlan Web Interface?\n\nAre you sure you wish to proceed?";
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Toggle Web Published Status of Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                loadingForm = new WaitDialogForm("Toggling Published Status...", "Reporting");
                loadingForm.Show();

                // Delete the record then the physical file layout //
                DataSet_ATTableAdapters.QueriesTableAdapter TogglePublishedStatus = new DataSet_ATTableAdapters.QueriesTableAdapter();
                TogglePublishedStatus.ChangeConnectionString(strConnectionString);
                try
                {
                    TogglePublishedStatus.sp01453_AT_Report_Layouts_Toggle_Web_Availibility(intReportLayoutID, (intPublished == 1 ? 0 : 1));
                }
                catch (Exception Ex)
                {
                    loadingForm.Close();
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to toggle published status - an error occurred [" + Ex.Message + "]. Please try again. If the problem persists contact Technical Support.", "Toggle Web Published Status of Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                // Reload list of available layouts to reflect changed Published Status of layout //
                sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, i_intReportType, "AmenityTreesReportLayoutLocation");
                loadingForm.Close();
                if (intPublished == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Report Layout set as Unpublished.\n\nThis layout will no longer be available to the WoodPlan Web Interface.\n\nIf you currently have the WoodPlan Web Interface open, you may need to refresh the page to reflect this change.", "Toggle Web Published Status of Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Report Layout set Published.\n\nThis layout is now available to the WoodPlan Web Interface.\n\nIf you currently have the WoodPlan Web Interface open, you may need to refresh the page to reflect this change.", "Toggle Web Published Status of Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }




    }
}


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Properties_Layer_Raster : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intTransparency = 0;
        public int intGreyScale = 0;

        #endregion

        public frm_AT_Mapping_Properties_Layer_Raster()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Properties_Layer_Raster_Load(object sender, EventArgs e)
        {
            this.FormID = 20045;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked at end of event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            tbcTransparency.EditValue = intTransparency;
            ceGreyScale.EditValue = intGreyScale;

            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            intTransparency = Convert.ToInt32(tbcTransparency.EditValue);
            intGreyScale = Convert.ToInt32(ceGreyScale.EditValue);
            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails any validation tests //
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

    }
}


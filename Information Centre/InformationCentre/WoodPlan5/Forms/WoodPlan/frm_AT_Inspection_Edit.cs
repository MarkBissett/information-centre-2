using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Skins;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_AT_Inspection_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";

        public bool ibool_CalledByTreePicker = false;
        public int intObjectType = 0;
        public int intX = 0;
        public int intY = 0;
        public string strPolygonXY = "";
        public decimal decArea = (decimal)0.00;
        public decimal decLength = (decimal)0.00;
        public decimal decWidth = (decimal)0.00;
        public string strID = "";
        public string strMapIDs = "";  // Only required when block editing from the map - used to tell the tree picker which objects to reload //

        ArrayList ArrayListFilteredPicklists;  // Holds all the picklists bound to sp01372 - used for iterating round to bind and unbind generic Enter, Leave and ButtonClick events //
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        private string i_strLastUsedSequencePrefix = "";

        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState39;  // Used by Grid View State Facilities //
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs39 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultPicturesPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        private DataSet_Selection DS_Selection3;

        #endregion

        public frm_AT_Inspection_Edit()
        {
            InitializeComponent();
        }

        private void frm_AT_Inspection_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 20012;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            // Set form Permissions //
            stcFormPermissions sfpPermissions = (stcFormPermissions)this.FormPermissions[0];
            if (this.FormPermissions.Count > 0)
            {
                iBool_AllowAdd = sfpPermissions.blCreate;
                iBool_AllowEdit = sfpPermissions.blUpdate;
                iBool_AllowDelete = sfpPermissions.blDelete;
            }

            ArrayListFilteredPicklists = new ArrayList();  // Note: Each GridLookUpEdit within the array should have it's tag value set to it's value in the SQL PicklistHeader Table //
            ArrayListFilteredPicklists.Add(intSafetyGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intGeneralConditionGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intVitalityGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intRootHeaveGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intRootHeave2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intRootHeave3GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intStemPhysicalGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intStemPhysical2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intStemPhysical3GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intStemDiseaseGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intStemDisease2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intStemDisease3GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intCrownPhysicalGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intCrownPhysical2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intCrownPhysical3GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intCrownDiseaseGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intCrownDisease2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intCrownDisease3GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intCrownFoliationGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intCrownFoliation2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intCrownFoliation3GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList1GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList3GridLookUpEdit);

            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);

            sp00190_Contractor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00190_Contractor_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00190_Contractor_List_With_Blank);

            // Last Saved Record Details //
            sp01375_AT_User_Screen_SettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01375_AT_User_Screen_SettingsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings, this.GlobalSettings.UserID, FormID);

            sp01384_AT_Inspection_Edit_Previous_InspectionsTableAdapter.Connection.ConnectionString = strConnectionString;

            sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "ActionID");

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView26, "LinkedDocumentID");

            sp02065_AT_Inspection_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState39 = new RefreshGridState(gridView39, "SurveyPictureID");

            DS_Selection3 = new Utilities.DataSet_Selection((GridView)gridView1, strConnectionString);  // Actions List //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPicturesPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Pictures path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Pictures Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Populate Main Dataset //
            sp01381_AT_Edit_Inspection_DetailsTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["intTreeID"] = intLinkedToRecordID;
                        drNewRow["TreeReference"] = strLinkedToRecordDesc;
                        drNewRow["dtInspectDate"] = DateTime.Today;
                        this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.Rows.Add(drNewRow);

                        // Get next sequence //
                        DataRow[] drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = 'strInspectRefButtonEdit'");
                        if (drFiltered.Length != 0)  // Matching row found //
                        {
                            DataRow dr = drFiltered[0];
                            strInspectRefButtonEdit.EditValue = dr["ItemValue"].ToString();
                            // Calculate Next Sequence Number by triggering button click event of strTreeRefButtonEdit //
                            if (!string.IsNullOrEmpty(strInspectRefButtonEdit.EditValue.ToString())) strInspectRefButtonEdit_ButtonClick(strInspectRefButtonEdit, new DevExpress.XtraEditors.Controls.ButtonPressedEventArgs(strInspectRefButtonEdit.Properties.Buttons[1]));
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.NewRow();
                        drNewRow["strMode"] = "blockadd";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        //drNewRow["intTreeID"] = intLinkedToRecordID;
                        //drNewRow["TreeReference"] = strLinkedToRecordDesc;
                        drNewRow["dtInspectDate"] = DateTime.Today;
                        this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.Rows.Add(drNewRow);

                        // Get next sequence //
                        DataRow[] drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = 'strInspectRefButtonEdit'");
                        if (drFiltered.Length != 0)  // Matching row found //
                        {
                            DataRow dr = drFiltered[0];
                            strInspectRefButtonEdit.EditValue = dr["ItemValue"].ToString();
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.Rows.Add(drNewRow);
                        this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp01381_AT_Edit_Inspection_DetailsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    Load_Linked_Pictures();
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
            Attach_Enter_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach Enter Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //
            Attach_ButtonClick_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach ButtonClick Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //

            bbiCopyAllValues.Enabled = (this.strFormMode == "add" || ibool_CalledByTreePicker ? true : false);
            bbiCopySelectedValues.Enabled = (this.strFormMode == "add" || ibool_CalledByTreePicker ? true : false);
            bbiReload.Enabled = (this.strFormMode == "add" || ibool_CalledByTreePicker ? true : false);
            bbiRememberDetails.Enabled = (this.strFormMode == "add" || this.strFormMode == "edit" || ibool_CalledByTreePicker ? true : false);
            bbiCopyActions.Enabled = (this.strFormMode == "add" || this.strFormMode == "edit" || ibool_CalledByTreePicker ? true : false); 
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).Enter += new EventHandler(Generic_Enter);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).Enter -= new EventHandler(Generic_Enter);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void Generic_Enter(object sender, EventArgs e)
        {
            if (!checkEditSychronise.Checked) return;

            // Focus VerticalGrid Row with name same as current Control //
            DevExpress.XtraEditors.BaseEdit be = (DevExpress.XtraEditors.BaseEdit)sender;
            string strName = "row" + be.Name;

            try
            {
                BaseRow br = vGridControl1.FocusedRow;
                if (br.Name != strName)
                {
                    BaseRow br2 = vGridControl1.Rows[strName];
                    if (br2 == null) return;
                    if (br2.Visible)
                    {
                        vGridControl1.FocusedRow = br2;
                    }
                    else
                    {
                        vGridControl1.FocusFirst();
                    }
                }
            }
            catch (Exception)
            {
            }
        }


        DataTable data;  // Used for holding filtered dataset for picklists //
        private void Attach_Enter_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter += new EventHandler(Filter_GridView);
            }
        }

        private void Detach_Enter_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter -= new EventHandler(Filter_GridView);
            }
        }

        private void Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            int intHeaderID = Convert.ToInt32(glue.Tag);
            data = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks;
            DataView newView = new DataView(data);
            newView.RowFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            glue.Properties.DataSource = newView;
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        private void Attach_Leave_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave += new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Detach_Leave_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave -= new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Clear_Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            glue.Properties.DataSource = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks.DefaultView;
            view.EndUpdate();
        }


        private void Attach_ButtonClick_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void Detach_ButtonClick_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick -= new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void ButtonClick_GridView(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    int intHeaderID = Convert.ToInt32(glue.Tag);
                    String strPickListName = "";
                    switch (glue.Name)
                    {
                        case "intSafetyGridLookUpEdit":
                            strPickListName = "Risk Categories";
                            break;
                        case "intGeneralConditionGridLookUpEdit":
                            strPickListName = "General Conditions";
                            break;
                        case "intVitalityGridLookUpEdit":
                            strPickListName = "Vitality";
                            break;
                        case "intRootHeaveGridLookUpEdit":
                        case "intRootHeave2GridLookUpEdit":
                        case "intRootHeave3GridLookUpEdit":
                            strPickListName = "Root Heave";
                            break;
                        case "intStemPhysicalGridLookUpEdit":
                        case "intStemPhysical2GridLookUpEdit":
                        case "intStemPhysical3GridLookUpEdit":
                            strPickListName = "Stem Physical Conditions";
                            break;
                        case "intStemDiseaseGridLookUpEdit":
                        case "intStemDisease2GridLookUpEdit":
                        case "intStemDisease3GridLookUpEdit":
                            strPickListName = "Stem Diseases";
                            break;
                        case "intCrownPhysicalGridLookUpEdit":
                        case "intCrownPhysical2GridLookUpEdit":
                        case "intCrownPhysical3GridLookUpEdit":
                            strPickListName = "Crown Physical Conditions";
                            break;
                        case "intCrownDiseaseGridLookUpEdit":
                        case "intCrownDisease2GridLookUpEdit":
                        case "intCrownDisease3GridLookUpEdit":
                            strPickListName = "Crown Diseases";
                            break;
                        case "intCrownFoliationGridLookUpEdit":
                        case "intCrownFoliation2GridLookUpEdit":
                        case "intCrownFoliation3GridLookUpEdit":
                            strPickListName = "Crown Foliation";
                            break;
                        case "UserPickList1GridLookUpEdit":
                            strPickListName = "Inspection User Defined 1";
                            break;
                        case "UserPickList2GridLookUpEdit":
                            strPickListName = "Inspection User Defined 2";
                            break;
                        case "UserPickList3GridLookUpEdit":
                            strPickListName = "Inspection User Defined 3";
                            break;
                        default:
                            break;
                    }
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, intHeaderID, strPickListName);
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
                    this.ValidateChildren();
                }
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Inspection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        strInspectRefButtonEdit.Focus();

                        TreeReferenceButtonEdit.Properties.Buttons[0].Enabled = true;
                        TreeReferenceButtonEdit.Properties.Buttons[1].Enabled = true;

                        strInspectRefButtonEdit.Properties.ReadOnly = false;
                        strInspectRefButtonEdit.Properties.Buttons[0].Enabled = true;
                        strInspectRefButtonEdit.Properties.Buttons[1].Enabled = true;
                        gridControl1.Enabled = true;
                        gridControl2.Enabled = true;
                        gridControl39.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        intInspectorGridLookUpEdit.Focus();

                        TreeReferenceButtonEdit.Properties.Buttons[0].Enabled = false;
                        TreeReferenceButtonEdit.Properties.Buttons[1].Enabled = false;
                        
                        strInspectRefButtonEdit.Properties.ReadOnly = true;
                        strInspectRefButtonEdit.Properties.Buttons[0].Enabled = true;
                        strInspectRefButtonEdit.Properties.Buttons[1].Enabled = false;
                        gridControl1.Enabled = false;
                        gridControl2.Enabled = false;
                        gridControl39.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        intInspectorGridLookUpEdit.Focus();

                        TreeReferenceButtonEdit.Properties.Buttons[0].Enabled = true;
                        TreeReferenceButtonEdit.Properties.Buttons[1].Enabled = true;                    

                        strInspectRefButtonEdit.Properties.ReadOnly = false;
                        strInspectRefButtonEdit.Properties.Buttons[0].Enabled = true;
                        strInspectRefButtonEdit.Properties.Buttons[1].Enabled = true;
                        gridControl1.Enabled = true;
                        gridControl2.Enabled = true;
                        gridControl39.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        intInspectorGridLookUpEdit.Focus();

                        TreeReferenceButtonEdit.Properties.Buttons[0].Enabled = true;
                        TreeReferenceButtonEdit.Properties.Buttons[1].Enabled = true;

                        strInspectRefButtonEdit.Properties.ReadOnly = true;
                        strInspectRefButtonEdit.Properties.Buttons[0].Enabled = true;
                        strInspectRefButtonEdit.Properties.Buttons[1].Enabled = false;
                        gridControl1.Enabled = false;
                        gridControl2.Enabled = false;
                        gridControl39.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            ibool_FireLayoutUpdateCode = true;  // Refresh Vertical Grid Row Labels to match those of the Layout control //
            Refresh_Vertical_Grid_Row_Labels(dataLayoutControl1);

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            intInspectorGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intInspectorGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intInspectorGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intInspectorGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intSafetyGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intSafetyGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intSafetyGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intSafetyGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intGeneralConditionGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intGeneralConditionGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intGeneralConditionGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intGeneralConditionGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intVitalityGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intVitalityGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intVitalityGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intVitalityGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intRootHeaveGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intRootHeaveGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intRootHeaveGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intRootHeaveGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intRootHeave2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intRootHeave2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intRootHeave2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intRootHeave2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intRootHeave3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intRootHeave3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intRootHeave3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intRootHeave3GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intStemPhysicalGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intStemPhysicalGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intStemPhysicalGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intStemPhysicalGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intStemPhysical2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intStemPhysical2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intStemPhysical2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intStemPhysical2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intStemPhysical3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intStemPhysical3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intStemPhysical3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intStemPhysical3GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intStemDiseaseGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intStemDiseaseGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intStemDiseaseGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intStemDiseaseGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intStemDisease2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intStemDisease2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intStemDisease2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intStemDisease2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intStemDisease3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intStemDisease3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intStemDisease3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intStemDisease3GridLookUpEdit.Properties.Buttons[2].Visible = false;           
            intCrownPhysicalGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intCrownPhysicalGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intCrownPhysicalGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intCrownPhysicalGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intCrownPhysical2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intCrownPhysical2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intCrownPhysical2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intCrownPhysical2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intCrownPhysical3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intCrownPhysical3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intCrownPhysical3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intCrownPhysical3GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intCrownDiseaseGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intCrownDiseaseGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intCrownDiseaseGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intCrownDiseaseGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intCrownDisease2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intCrownDisease2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intCrownDisease2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intCrownDisease2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intCrownDisease3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intCrownDisease3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intCrownDisease3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intCrownDisease3GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "8001,9003,9015,9038,9033,9005,9006,9007,9008,9044,9045,9046", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 8001:  // Contractor Manager //    
                        {
                            intInspectorGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intInspectorGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intInspectorGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intInspectorGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9003:  // Risk Categories //    
                        {
                            intSafetyGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intSafetyGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intSafetyGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intSafetyGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9015:  // General Conditions //    
                        {
                            intGeneralConditionGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intGeneralConditionGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intGeneralConditionGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intGeneralConditionGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9038:  // Vitality //    
                        {
                            intVitalityGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intVitalityGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intVitalityGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intVitalityGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9033:  // Root Heave //    
                        {
                            intRootHeaveGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intRootHeaveGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intRootHeaveGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intRootHeaveGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intRootHeave2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intRootHeave2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intRootHeave2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intRootHeave2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intRootHeave3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intRootHeave3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intRootHeave3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intRootHeave3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9005:  // Stem Physical Condition //    
                        {
                            intStemPhysicalGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intStemPhysicalGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intStemPhysicalGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intStemPhysicalGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intStemPhysical2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intStemPhysical2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intStemPhysical2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intStemPhysical2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intStemPhysical3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intStemPhysical3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intStemPhysical3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intStemPhysical3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9006:  // Stem Disease //    
                        {
                            intStemDiseaseGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intStemDiseaseGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intStemDiseaseGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intStemDiseaseGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intStemDisease2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intStemDisease2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intStemDisease2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intStemDisease2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intStemDisease3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intStemDisease3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intStemDisease3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intStemDisease3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9007:  // Crown Physical Condition //    
                        {
                            intCrownPhysicalGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intCrownPhysicalGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intCrownPhysicalGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intCrownPhysicalGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intCrownPhysical2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intCrownPhysical2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intCrownPhysical2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intCrownPhysical2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intCrownPhysical3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intCrownPhysical3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intCrownPhysical3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intCrownPhysical3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9008:  // Crown Disease //    
                        {
                            intCrownDiseaseGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intCrownDiseaseGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intCrownDiseaseGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intCrownDiseaseGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intCrownDisease2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intCrownDisease2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intCrownDisease2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intCrownDisease2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intCrownDisease3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intCrownDisease3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intCrownDisease3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intCrownDisease3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9044:  // User Picklist 1 //    
                        {
                            UserPickList1GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9045:  // User Picklist 2 //    
                        {
                            UserPickList2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9046:  // User Picklist 3 //    
                        {
                            UserPickList3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            }
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_AT_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intInspectionID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
            if (currentRow != null) intInspectionID = (currentRow["intInspectionID"] == null ? 0 : Convert.ToInt32(currentRow["intInspectionID"]));

            bbiPasteActions.Enabled = (intInspectionID > 0 ? true : false);
            
            if (i_int_FocusedGrid == 1)  // Inspections //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intInspectionID > 0 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Linked Documents //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intInspectionID > 0 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 39)  // Linked Pictures //
            {
                view = (GridView)gridControl39.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intInspectionID > 0 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd && intInspectionID > 0 && strFormMode != "view")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit && strFormMode != "view")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete && strFormMode != "view")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
            // Set enabled status of GridControl2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd && intInspectionID > 0 && strFormMode != "view")
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit && strFormMode != "view")
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete && strFormMode != "view")
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
            // Set enabled status of GridView39 navigator custom buttons //
            view = (GridView)gridControl39.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intInspectionID > 0 && strFormMode != "view" ? true : false);
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && strFormMode != "view" && intRowHandles.Length == 1 ? true : false);
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }


        private void frm_AT_Inspection_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                LoadLinkedRecords();
            }
            SetMenuStatus();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in arraylistFloatingPanels)
            {
                dp.Show();
            }
        }

        private void frm_AT_Inspection_Edit_Deactivate(object sender, EventArgs e)
        {
            arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }
        }

        private void frm_AT_Inspection_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        Detach_Enter_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Enter Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_Leave_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Leave Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_ButtonClick_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach ButtonClick Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        private void frm_AT_Tree_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_AT_Tree_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        Detach_Enter_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Enter Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_Leave_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Leave Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_ButtonClick_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach ButtonClick Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp01381ATEditInspectionDetailsBindingSource.EndEdit();
            try
            {
                this.sp01381_AT_Edit_Inspection_DetailsTableAdapter.Update(dataSet_AT_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["intInspectionID"]) + ";";
                
                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //
            }

            // Notify any open forms which reference this data that they will need to refresh their data on activating //
            string strTreeIDs = "";
            if (string.IsNullOrEmpty(strRecordIDs))
            {
                DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                if (currentRow != null) strTreeIDs = Convert.ToString(currentRow["intTreeID"]) + ",";
            }
            else
            {
                strTreeIDs = strRecordIDs;
            }
            Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
            Broadcast.Inspection_Refresh(this.ParentForm, this.Name, strNewIDs, strTreeIDs);

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

        private void Store_Last_Saved_Record_Details(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit)
                {
                    string strName = ((DevExpress.XtraEditors.BaseEdit)item2).Name;
                    string strText = "";
                    if (strName == "strInspectRefButtonEdit")
                    {
                        if (i_strLastUsedSequencePrefix != "")
                        {
                            strText = i_strLastUsedSequencePrefix;
                        }
                        else
                        {
                            continue;  // Ignore Sequence if empty //
                        }
                    }
                    else if (strName == "DateAddedDateEdit" || strName == "GUIDTextEdit" || strName == "intInspectionIDTextEdit" || strName == "dtInspectDateDateEdit")
                    {
                        continue;  // Ignore Record and Mapping Fields //
                    }
                    else
                    {
                        strText = (((DevExpress.XtraEditors.BaseEdit)item2).EditValue == null ? null : ((DevExpress.XtraEditors.BaseEdit)item2).EditValue.ToString());
                    }
                    DataRow[] drFiltered;
                    drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = '" + strName + "'");
                    if (drFiltered.Length != 0)  // Matching row found so Update it //
                    {
                        DataRow drExistingRow = drFiltered[0];
                        drExistingRow["ItemValue"] = strText;
                    }
                    else  // No Matching row so Add it to DataSet //
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.NewRow();
                        drNewRow["ScreenID"] = FormID;
                        drNewRow["UserID"] = this.GlobalSettings.UserID;
                        drNewRow["ItemName"] = strName;
                        drNewRow["ItemValue"] = strText;
                        this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Rows.Add(drNewRow);
                    }
                }
                if (item2 is ContainerControl) Store_Last_Saved_Record_Details(item.Controls);
            }
            try
            {
                this.sp01375_AT_User_Screen_SettingsTableAdapter.Update(dataSet_AT_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while copying the inspection details [" + ex.Message + "]!\n\nTry copying again - if the problem persists, contact Technical Support.", "Copy Inspection Details", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.Rows.Count; i++)
            {
                switch (this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiCopyAllValues_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Control ctrlOnForm = null;
            string strName = "";
            string strText = "";
            bool boolSequenceUpdated = false;
            dataLayoutControl1.BeginUpdate();
            foreach (DataRow dr in dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Rows)
            {
                strName = dr["ItemName"].ToString();
                if (strName == "intInspectionIDTextEdit") continue;  // Ignore this value [only stored when user clicks Copy Actions]

                strText = dr["ItemValue"].ToString();
                Control[] controls = this.Controls.Find(strName, true);
                if (controls.Length == 1)  // Only proceed if just one control was found matching the name // 
                {
                    ctrlOnForm = controls[0] as Control;
                    if (ctrlOnForm is DevExpress.XtraEditors.BaseEdit)
                    {
                        if (strName == "strInspectRefButtonEdit")
                        {
                            boolSequenceUpdated = true;
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = "";  // Clear any sequence first //
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                        }
                        if (string.IsNullOrEmpty(strText))
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = DBNull.Value;
                        }
                        else
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = strText;
                        }
                    }
                }
            }
            // Calculate Next Sequence Number by triggering button click event of strTreeRefButtonEdit //
            if (!string.IsNullOrEmpty(strInspectRefButtonEdit.EditValue.ToString()) && boolSequenceUpdated) strInspectRefButtonEdit_ButtonClick(strInspectRefButtonEdit, new DevExpress.XtraEditors.Controls.ButtonPressedEventArgs(strInspectRefButtonEdit.Properties.Buttons[1]));

            dataLayoutControl1.EndUpdate();
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
        }

        private void bbiCopySelectedValues_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Loop round all objects in DataLayout Control and store the child objects and the Layout Captions in HashTable //
            Hashtable htObjects = new Hashtable();
            foreach (Control item in this.Controls)
            {
                if (item is DevExpress.XtraLayout.LayoutControl)
                {
                    DevExpress.XtraLayout.LayoutControl item2 = (DevExpress.XtraLayout.LayoutControl)item;
                    foreach (object obj in item2.Items)
                    {
                        if (obj is DevExpress.XtraLayout.LayoutControlItem)
                        {
                            DevExpress.XtraLayout.LayoutControlItem lci = (DevExpress.XtraLayout.LayoutControlItem)obj;
                            if (lci.Control != null)
                            {
                                if (lci.Control.Name == "GUIDTextEdit" || lci.Control.Name == "intInspectionIDTextEdit") continue;  // Ignore this value [only stored when user clicks Copy Actions] //
                                htObjects.Add(lci.Control.Name, lci.Text);
                            }
                        }
                    }
                }
            }
            frm_Core_Recall_Selected_Values fChildForm = new frm_Core_Recall_Selected_Values();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intPassedInScreenID = this.FormID;
            fChildForm.htPassedInObjects = htObjects;
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                htObjects = fChildForm.htPassedInObjects;
            }

            // Process the updated HashTable //
            Control ctrlOnForm = null;
            string strName = "";
            string strText = "";
            bool boolSequenceUpdated = false;
            dataLayoutControl1.BeginUpdate();
            IDictionaryEnumerator enumerator = htObjects.GetEnumerator();
            while (enumerator.MoveNext())
            {
                strName = enumerator.Key.ToString();
                if (strName == "intInspectionIDTextEdit") continue;  // Ignore this value [only stored when user clicks Copy Actions] //
                strText = enumerator.Value.ToString();
                Control[] controls = this.Controls.Find(strName, true);
                if (controls.Length == 1)  // Only proceed if just one control was found matching the name // 
                {
                    ctrlOnForm = controls[0] as Control;
                    if (ctrlOnForm is DevExpress.XtraEditors.BaseEdit)
                    {
                        if (strName == "strInspectRefButtonEdit")
                        {
                            boolSequenceUpdated = true;
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = "";  // Clear any sequence first //
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                        }
                        if (string.IsNullOrEmpty(strText))
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = DBNull.Value;
                        }
                        else
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = strText;
                        }
                    }
                }
            }
            // Calculate Next Sequence Number by triggering button click event of strTreeRefButtonEdit //
            if (!string.IsNullOrEmpty(strInspectRefButtonEdit.EditValue.ToString()) && boolSequenceUpdated) strInspectRefButtonEdit_ButtonClick(strInspectRefButtonEdit, new DevExpress.XtraEditors.Controls.ButtonPressedEventArgs(strInspectRefButtonEdit.Properties.Buttons[1]));

            dataLayoutControl1.EndUpdate();
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //


        }

        private void bbiReload_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Reload Last Saved Record Details into memory//
            sp01375_AT_User_Screen_SettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01375_AT_User_Screen_SettingsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings, this.GlobalSettings.UserID, FormID);
        }

        private void bbiRememberDetails_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save records attributes to User_Settings table for later recall //
            DevExpress.Utils.WaitDialogForm loading = new DevExpress.Utils.WaitDialogForm("Storing Details to Memory...", "Inspections");
            loading.Show();
            Store_Last_Saved_Record_Details(this.Controls);
            XtraMessageBox.Show("Details Copied.", "Copy Inspection Details", MessageBoxButtons.OK, MessageBoxIcon.Information);

            loading.Hide();
            loading.Dispose();
        }

        private void bbiCopyActions_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strName = "intInspectionIDTextEdit";
            string strText = intInspectionIDTextEdit.EditValue.ToString();

            DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
            if (currentRow != null)
            {
                strText = (currentRow["intInspectionID"] == null ? "" : currentRow["intInspectionID"].ToString());
                if (string.IsNullOrEmpty(strText) || gridControl1.MainView.DataRowCount <= 0)
                {
                    XtraMessageBox.Show("Unable to copy actions, no Actions are linked to the current Inspection!", "Copy Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            
            DataRow[] drFiltered;
            drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = '" + strName + "'");
            if (drFiltered.Length != 0)  // Matching row found so Update it //
            {
                DataRow drExistingRow = drFiltered[0];
                drExistingRow["ItemValue"] = strText;
            }
            else  // No Matching row so Add it to DataSet //
            {
                DataRow drNewRow;
                drNewRow = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.NewRow();
                drNewRow["ScreenID"] = FormID;
                drNewRow["UserID"] = this.GlobalSettings.UserID;
                drNewRow["ItemName"] = strName;
                drNewRow["ItemValue"] = strText;
                this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Rows.Add(drNewRow);
            }
            try
            {
                this.sp01375_AT_User_Screen_SettingsTableAdapter.Update(dataSet_AT_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while copying the actions [" + ex.Message + "]!\n\nTry copying again - if the problem persists, contact Technical Support.", "Copy Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            XtraMessageBox.Show("Details Copied.", "Copy Action Details", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void bbiPasteActions_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {     
            int intCurrentInspectionID = 0;
            DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
            if (currentRow != null)
            {
                intCurrentInspectionID = (currentRow["intInspectionID"] == null ? 0 : Convert.ToInt32(currentRow["intInspectionID"]));
                string strText = intCurrentInspectionID.ToString();
                if (string.IsNullOrEmpty(strText))
                {
                    XtraMessageBox.Show("Unable to copy actions - Save the current Inspection first before proceeding.", "Copy Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else return;
          
            frm_AT_Inspection_Edit_Paste_Actions fChildForm = new frm_AT_Inspection_Edit_Paste_Actions();
            fChildForm.GlobalSettings = this.GlobalSettings;

            // Get Values from stored user settings for passing through to screen //
            string strInspectionID = "";
            string strLastInspectRef = "";
            DataRow[] drFiltered;
            drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = 'intInspectionIDTextEdit'");
            if (drFiltered.Length != 0)  // Matching row found so Update it //
            {
                DataRow drExistingRow = drFiltered[0];
                strInspectionID = drExistingRow["ItemValue"].ToString() + ",";
            }

            try
            {
                DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter GetSetting = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strLastInspectRef = GetSetting.sp01386_AT_User_Screen_Settings(this.GlobalSettings.UserID, 20013, "strJobNumberButtonEdit" ).ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while getting the last used Action Job Number [" + ex.Message + "]!\n\nTry again. If the problem persists, contact Technical Support.", "Paste Copied Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            fChildForm.strInspectionIDs = strInspectionID;
            fChildForm.strLastUsedInspectRef = strLastInspectRef;
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                string strSelectedActionIDs = fChildForm.strSelectedIDs;
                DateTime dtSelectedDate = fChildForm.dtSelectedDate;
                string strSelectedSequence = fChildForm.strSelectedSequence;
                int intSelectedContractor = fChildForm.intSelectedContractor;
                if (string.IsNullOrEmpty(strSelectedActionIDs)) return;
                
                // Add Selected Actions to current Inspection //
                string strNewActionIDs = "";
                try
                {
                    DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter CopyActions = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                    CopyActions.ChangeConnectionString(strConnectionString);
                    strNewActionIDs = CopyActions.sp01385_AT_Copy_Inspection_Actions(strSelectedActionIDs, intCurrentInspectionID, strSelectedSequence, intSelectedContractor, dtSelectedDate).ToString() + ";";

                    // Load new records into Actions grid then broadcast the new records to any other relevent screens //
                    i_str_AddedRecordIDs1 = strNewActionIDs;
                    LoadLinkedRecords();

                    // Notify any open forms which reference this data that they will need to refresh their data on activating //
                    Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                    Broadcast.Action_Refresh(this.ParentForm, this.Name, strNewActionIDs, "");

                    char[] delimiters = new char[] { ';' };
                    string[] strArray;
                    strArray = strNewActionIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    int intNewRecordCount = strArray.Length;
                    DevExpress.XtraEditors.XtraMessageBox.Show(Convert.ToString(intNewRecordCount) + (intNewRecordCount == 1 ? " action" : " actions") + " pasted.", "Paste Copied Actions", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while pasting the copied actions [" + ex.Message + "]!\n\nTry again. If the problem persists, contact Technical Support.", "Paste Copied Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void LoadPriorInspectionRecords()
        {           
            string strTreeID = "";
            string strCurrentInspectionID = "";
            DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
            if (currentRow != null)
            {
                strTreeID = (currentRow["intTreeID"] == null ? "" : currentRow["intTreeID"].ToString() + ",");
                strCurrentInspectionID = (currentRow["intInspectionID"] == null ? "" : currentRow["intInspectionID"].ToString() + ",");
            }
            vGridControl1.BeginUpdate();
            sp01384_AT_Inspection_Edit_Previous_InspectionsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01384_AT_Inspection_Edit_Previous_Inspections, strTreeID, strCurrentInspectionID);
            vGridControl1.EndUpdate();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strInspectionID = "";
            DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
            if (currentRow != null)
            {
                strInspectionID = (currentRow["intInspectionID"] == null ? "" : currentRow["intInspectionID"].ToString() + ",");
            }

            // Inspections //
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections, strInspectionID, 0,  null, null, 0);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            // Linked Documents //
            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
            gridControl2.BeginUpdate();
            sp00220_Linked_Documents_ListTableAdapter.Fill(this.dataSet_AT.sp00220_Linked_Documents_List, strInspectionID, 4, strDefaultPath);
            this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl2.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl2.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }
        }

        private void Load_Linked_Pictures()
        {
            string strInspectionID = "";
            DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
            if (currentRow != null)
            {
                strInspectionID = (currentRow["intInspectionID"] == null ? "" : currentRow["intInspectionID"].ToString() + ",");
            }
            gridControl39.BeginUpdate();
            this.RefreshGridViewState39.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp02065_AT_Inspection_Pictures_ListTableAdapter.Fill(dataSet_AT.sp02065_AT_Inspection_Pictures_List, strInspectionID, strDefaultPicturesPath);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState39.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl39.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs39 != "")
            {
                strArray = i_str_AddedRecordIDs39.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl39.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyPictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs39 = "";
            }
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else  // Load in Past Inspections... //
                {
                    if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd")
                    {
                        LoadPriorInspectionRecords();
                        LoadLinkedRecords();
                        GridView view = (GridView)gridControl1.MainView;
                        view.ExpandAllGroups();
                        view = (GridView)gridControl2.MainView;
                        view.ExpandAllGroups();
                        Load_Linked_Pictures();
                        view = (GridView)gridControl39.MainView;
                        view.ExpandAllGroups();
                    }
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void TreeReferenceButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Tree Button //
            {
                frm_AT_Select_Tree fChildForm = new frm_AT_Select_Tree();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                {
                    DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["intTreeID"] = fChildForm.intSelectedID;
                        currentRow["TreeReference"] = fChildForm.strSelectedValue;

                        if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd") LoadPriorInspectionRecords();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Tree` Button //
            {
                DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                if (currentRow != null)
                {
                    int intTreeID = (string.IsNullOrEmpty(currentRow["intTreeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["intTreeID"]));
                    if (intTreeID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display parent tree - no tree set as parent.", "View Parent Tree", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    frm_AT_Tree_Edit fChildForm = new frm_AT_Tree_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intTreeID + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = "frm_AT_Inspection_Edit";
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void TreeReferenceButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "edit" || this.strFormMode == "add") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(TreeReferenceButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(TreeReferenceButtonEdit, "");
            }
        }

        private void strInspectRefButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            string strTableName = "tblInspection";
            string strFieldName = "strInspectRef";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    strInspectRefButtonEdit.EditValue = fChildForm.SelectedSequence;
                    i_strLastUsedSequencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = strInspectRefButtonEdit.EditValue.ToString() ?? "";

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Site Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";


                // Check if value is already present within dataset //
                string strCurrentInspectionID = intInspectionIDTextEdit.EditValue.ToString();
                if (string.IsNullOrEmpty(strCurrentInspectionID)) strCurrentInspectionID = "";
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_AT_DataEntry.sp01381_AT_Edit_Inspection_Details.Rows)
                    {
                        if (dr["strInspectRef"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                strInspectRefButtonEdit.EditValue = strSequence + strTempNumber;
            }
        }

        private void strInspectRefButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            /*ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(strInspectRefButtonEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(strInspectRefButtonEdit, "");
            }*/
        }

        private void intInspectorGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 1, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00190_Contractor_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00190_Contractor_List_With_Blank);
                }
            }
        }

        private void dtInspectDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(dtInspectDateDateEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(dtInspectDateDateEdit, "");
            }
        }

        private void IncidentDescriptionButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if ("choose".Equals(e.Button.Tag))
            {
                frm_AT_Select_Incident fChildForm = new frm_AT_Select_Incident();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                {
                    DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["intIncidentID"] = fChildForm.intSelectedID;
                        currentRow["IncidentDescription"] = fChildForm.strSelectedValue;
                    }
                }
            }
            else if ("clear".Equals(e.Button.Tag))
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the selected incident.\n\nAre you sure you wish to proceed?", "Clear Incident", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                     DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                     if (currentRow != null)
                     {
                         currentRow["intIncidentID"] = 0;
                         currentRow["IncidentDescription"] = "";
                     }
                }
            }
            else if ("view".Equals(e.Button.Tag))
            {
                DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                if (currentRow != null)
                {
                    int intIncidentID = (string.IsNullOrEmpty(currentRow["intIncidentID"].ToString()) ? 0 : Convert.ToInt32(currentRow["intIncidentID"]));
                    if (intIncidentID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked incident - no incident linked.", "View Linked Incident", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_AT_Incident_Edit fChildForm = new frm_AT_Incident_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intIncidentID + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = "frm_AT_Inspection_Edit";
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void intInspectorGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[Disabled] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void intInspectorGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    if (this.strFormMode.ToLower() == "add")
                    {
                        DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            if (String.IsNullOrEmpty(currentRow["intInspectionID"].ToString()) || currentRow["intInspectionID"].ToString() == "0")
                            {
                                message = "No Actions - Save New Inspection Record Before Adding Actions";
                            }
                        }
                    }
                    else message = "No Actions";
                    break;
                case "gridView26":
                    if (this.strFormMode.ToLower() == "add")
                    {
                        DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            if (String.IsNullOrEmpty(currentRow["intTreeID"].ToString()) || currentRow["intTreeID"].ToString() == "0")
                            {
                                message = "No Linked Documents - Save New Inspection Record Before Adding Linked Documents";
                            }
                        }
                    }
                    else message = "No Linked Documents";
                    break;
                case "gridView239":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Pictures NOT Shown When Block Adding and Block Editing" : "No Linked Pictures Available - Click the Add button to Create Linked Pictures");
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region Grid - Actions

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit && strFormMode != "view")
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }




        #endregion


        #region Grid - Linked Documents

        private void gridView26_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit && strFormMode != "view")
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView26_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView26_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region Grid - Linked Pictures

        private void gridView39_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            //if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView39_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView39_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 39;
            SetMenuStatus();
        }

        private void gridView39_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl39_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl39.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit3_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region Toolbar

        private void bciShowPrevious_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciShowPrevious.Checked)
            {
                dockPanelLastInspection.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelLastInspection.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void dockPanelLastInspection_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciShowPrevious.Checked) bciShowPrevious.Checked = false;
            //this.dataSet_AT_DataEntry.sp01367_AT_Inspections_For_Trees.Clear();  // Empty Inspection grid to conserve memory //
        }

 
        #endregion


        #region DataLayoutControl1

        bool ibool_FireLayoutUpdateCode = false;  // Used to stop firing everytime a programatic change is made to the Layout Control //
        private void dataLayoutControl1_LayoutUpdate(object sender, EventArgs e)
        {
            if (!ibool_FireLayoutUpdateCode) return;
            DevExpress.XtraLayout.LayoutControl lc = (DevExpress.XtraLayout.LayoutControl)sender;
            Refresh_Vertical_Grid_Row_Labels(lc);
        }

        private void dataLayoutControl1_HideCustomization(object sender, EventArgs e)
        {
            ibool_FireLayoutUpdateCode = true;
        }

        private void Refresh_Vertical_Grid_Row_Labels(DevExpress.XtraLayout.LayoutControl lc)
        {
            vGridControl1.BeginUpdate();
            foreach (object obj in lc.Items)
            {
                if (obj is DevExpress.XtraLayout.LayoutControlItem)
                {
                    DevExpress.XtraLayout.LayoutControlItem lci = (DevExpress.XtraLayout.LayoutControlItem)obj;
                    if (lci.TypeName != "LayoutControlItem") continue;
                    string strName = "row" + lci.Control.Name;
                    BaseRow br2 = vGridControl1.Rows[strName];
                    if (br2 == null) continue;
                    if (br2.Properties.Caption != lci.Text) br2.Properties.Caption = lci.Text;
                }
            }
            vGridControl1.EndUpdate();
            ibool_FireLayoutUpdateCode = false;

        }

        #endregion


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = "";
            string strSelectedTrees = "";
            string strSelectedTree = "";
            int intSelectedTreeCount = 0;
            string strSelectedInspections = "";
            string strSelectedInspection = "";
            int intSelectedInspectionCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Action Grid //
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Actions //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                        // Inspections //
                        if (strSelectedInspections == "")
                        {
                            strSelectedInspections = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                            intSelectedInspectionCount++;
                        }
                        else
                        {
                            strSelectedInspection = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedInspections.Contains("," + strSelectedInspection))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedInspections += strSelectedInspection;
                                intSelectedInspectionCount++;
                            }
                        }
                    }
                    CreateDataset("Action", intSelectedTreeCount, strSelectedTrees, intSelectedInspectionCount, strSelectedInspections, intCount, strSelectedIDs);
                    break;
                default:
                    return;
            }
        }

        private void CreateDataset(string strType, int intTreeCount, string strSelectedTreeIDs, int intInspectionCount, string strSelectedInspectionIDs, int intActionCount, string strSelectedActionIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            //Form frmMain = this.MdiParent;
            //frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_tree_count = intTreeCount;
            fChildForm.i_int_selected_inspection_count = intInspectionCount;
            fChildForm.i_int_selected_action_count = intActionCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                string strSelectedIDs = "";
                switch (strDatasetType.ToUpper())
                {
                    case "TREE":
                        strSelectedIDs = strSelectedTreeIDs;
                        break;
                    case "INSPECTION":
                        strSelectedIDs = strSelectedInspectionIDs;
                        break;
                    case "ACTION":
                        strSelectedIDs = strSelectedActionIDs;
                        break;
                    default:
                        strSelectedIDs = "";
                        break;
                }
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            int intInspectionID = 0;
            string strInspectionReference = "";
            switch (i_int_FocusedGrid)
            {
                case 1:     // Actions //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_AT_Inspection_Edit";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        int intParentOwnershipID = 0;
                        string strParentNearestHouse = "";
                        DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            intInspectionID = (currentRow["intInspectionID"] == null ? 0 : Convert.ToInt32(currentRow["intInspectionID"]));
                            strInspectionReference = (currentRow["strInspectRef"] == null ? "" : currentRow["strInspectRef"].ToString());
                            if (intInspectionID > 0)
                            {
                                try
                                {

                                    DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter GetValue = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                                    GetValue.ChangeConnectionString(strConnectionString);
                                    string strReturnValue = GetValue.sp01395_AT_Get_Inspection_Ownership_And_Nearest_House(intInspectionID).ToString();
                                    char[] delimiters = new char[] { '|' };
                                    string[] strArray = strReturnValue.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                    if (strArray.Length == 2)
                                    {
                                        intParentOwnershipID = Convert.ToInt32(strArray[0]);
                                        strParentNearestHouse = strArray[1];
                                    }
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }
                        fChildForm.intLinkedToRecordID = intInspectionID;
                        fChildForm.strLinkedToRecordDesc = strInspectionReference;
                        fChildForm.intParentOwnershipID = intParentOwnershipID;
                        fChildForm.strParentNearestHouse = strParentNearestHouse;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_AT_Inspection_Edit";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 4;  // Inspections //

                        DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            intInspectionID = (currentRow["intInspectionID"] == null ? 0 : Convert.ToInt32(currentRow["intInspectionID"]));
                            strInspectionReference = (currentRow["strInspectRef"] == null ? "" : currentRow["strInspectRef"].ToString());
                        }
                        fChildForm.intLinkedToRecordID = intInspectionID;
                        fChildForm.strLinkedToRecordDesc = strInspectionReference;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 39:  // Linked Pictures //
                    {
                        int intID = 0;
                        string strTreeRef = "";
                        DataRowView currentRow = (DataRowView)sp01381ATEditInspectionDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            intID = (currentRow["intInspectionID"] == null ? 0 : Convert.ToInt32(currentRow["intInspectionID"]));
                            strTreeRef = (currentRow["TreeReference"] == null ? "" : currentRow["TreeReference"].ToString());
                        }
                        if (intID == 0) return;
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "add";
                        fChildForm.intAddToRecordID = intID;
                        fChildForm.strSaveNameValue = strTreeRef;
                        fChildForm.intAddToRecordTypeID = 102;  // 102 = AT Inspection //
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.strDefaultPath = strDefaultPicturesPath;
                        fChildForm._PassedInFileNamePrefix = "Inspection";
                        fChildForm.ShowDialog();
                        int intNewPictureID = fChildForm._AddedPolePictureID;
                        if (intNewPictureID != 0)  // At least 1 picture added so refresh picture grid //
                        {
                            i_str_AddedRecordIDs39 = intNewPictureID.ToString() + ";";
                            this.RefreshGridViewState39.SaveViewInfo();
                            Load_Linked_Pictures();
                            SetMenuStatus();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Actions //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                    }
                    var fChildForm = new frm_AT_Action_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "blockedit";
                    fChildForm.strCaller = "frm_AT_Inspection_Edit";
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_AT_Inspection_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_AT_Inspection_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 4;  // Inspections //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 39:     // Picture //
                    {
                        view = (GridView)gridControl39.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Picture Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intPictureID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyPictureID"));
                        int intAddToRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordID"));
                        int intLinkedToRecordTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordTypeID"));

                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "edit";
                        fChildForm._PassedInEditImageID = intPictureID;
                        fChildForm.intAddToRecordID = intAddToRecordID;
                        fChildForm.intAddToRecordTypeID = intLinkedToRecordTypeID;
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.strDefaultPath = strDefaultPicturesPath;
                        fChildForm._PassedInFileNamePrefix = "Inspection";
                        fChildForm.ShowDialog();
                        this.RefreshGridViewState39.SaveViewInfo();
                        Load_Linked_Pictures();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Action //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Actions to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Action" : Convert.ToString(intRowHandles.Length) + " Actions") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this action" : "these actions") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ActionID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp01387_AT_Action_Manager_Delete("action", strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Action_Refresh(this.ParentForm, this.Name, "", "");

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:  // Linked Documents //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();
                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.LinkedDocument_Refresh(this.ParentForm, this.Name, "");

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 39:  // Linked Inspection Pictures //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl39.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Inspection Picture" : Convert.ToString(intRowHandles.Length) + " Inspection Pictures") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Inspection Picture" : "these Inspection Pictures") + " will no longer be available for selection and any associated images will be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            string strImageName = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strImageName = view.GetRowCellValue(intRowHandle, "PicturePath").ToString();
                                try
                                {
                                    base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                                    GC.GetTotalMemory(true);
                                    System.IO.File.Delete(strImageName);
                                }
                                catch (Exception ex)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete a linked picture [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete Inspection Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    continue;
                                }
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyPictureID")) + ",";
                            }
                            DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState39.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp01387_AT_Action_Manager_Delete("picture", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Linked_Pictures();
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Actions //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = "frm_AT_Inspection_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }










    }
}


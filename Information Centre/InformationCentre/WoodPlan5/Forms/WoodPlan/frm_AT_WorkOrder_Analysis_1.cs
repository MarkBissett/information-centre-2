using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraReports;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Wizard;
using DevExpress.XtraCharts.Native;


namespace WoodPlan5
{
    public partial class frm_AT_WorkOrder_Analysis_1 : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        private ExtendedPivotGridMenu epgmMenu;  // Used to extend Pivot Grid Field Right-click Menu //
        GridHitInfo downHitInfo = null;

        #endregion      
       
        public frm_AT_WorkOrder_Analysis_1()
        {
            InitializeComponent();
        }

        private void frm_AT_WorkOrder_Analysis_1_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 92004;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
          
            strConnectionString = this.GlobalSettings.ConnectionString;
            sp01209_AT_WorkOrders_Analysis_Jobs_AvailableTableAdapter.Connection.ConnectionString = strConnectionString;
            
            sp01218_AT_WorkOrders_Analysis_Jobs_AnalyseTableAdapter.Connection.ConnectionString = strConnectionString;
            
            dateEditFrom.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditTo.DateTime = this.GlobalSettings.ViewedEndDate;

            LinkChartsToPivotGrids();         

            LinkChartToRibbonBar();  // Link Chart to Chart Gallery on Ribbon Bar //
            
            Load_Jobs_List();
        }

        private void Chart_Preserve_User_Interaction(ChartControl chart)
        {
            // Ensure end-user rotation working if 3D chart //
            try
            {
                SimpleDiagram3D diagram = (SimpleDiagram3D)chart.Diagram;
                if (diagram != null)
                {
                    diagram.RuntimeRotation = true;
                    diagram.RuntimeScrolling = true;
                    diagram.RuntimeZooming = true;
                }
            }
            catch
            {
            }
        }


        private void LinkChartsToPivotGrids()
        {
            chartControl1.DataSource = pivotGridControl1;
            chartControl1.SeriesDataMember = "Series";
            chartControl1.SeriesTemplate.ArgumentDataMember = "Arguments";
            chartControl1.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "Values" });
            Chart_Preserve_User_Interaction(chartControl1);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            this.Refresh();

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            LinkChartsToPivotGrids();
        }

        private void Load_Jobs_List()
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Jobs...");
            }

            DateTime dtStart = new DateTime(1900, 1, 1);
            DateTime dtEnd = new DateTime(2500, 1, 1);
            if (dateEditFrom.Text != "") dtStart = Convert.ToDateTime(dateEditFrom.Text);
            if (dateEditTo.Text != "") dtEnd = Convert.ToDateTime(dateEditTo.Text);
            sp01209_AT_WorkOrders_Analysis_Jobs_AvailableTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01209_AT_WorkOrders_Analysis_Jobs_Available, dtStart, dtEnd);

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void btnRefreshList_Click(object sender, EventArgs e)
        {
            Load_Jobs_List();
        }


        #region GridView1

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void frm_AT_WorkOrder_Analysis_1_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "rgbiChartTypes", "bbiChartPalette", "rgbiChartAppearance", "bbiRotateAxis", "bbiChartWizard", "bbiLayoutFlip", "bbiLayoutRotate", "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            /*foreach (stcFormPermissions sfpPermissions in FormPermissions)
            {
                if (sfpPermissions.intSubPartID == 0)  // 0 = Classes (whole Form) //
                {
                    if (sfpPermissions.blDelete == true)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                        iBool_AllowDelete = true;
                    }
                    if (sfpPermissions.blCreate == true)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        iBool_AllowAdd = true;
                    }
                    if (sfpPermissions.blUpdate == true)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        iBool_AllowEdit = true;
                    }
                    GridView gvClass = (GridView)sp01200_TreeListALLGridControl.MainView;
                    int[] intRowHandles;
                    intRowHandles = gvClass.GetSelectedRows();
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    break;
                }
            }*/
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
            LinkChartToRibbonBar();  // Ensure Charting items on Ribbon Bar are set correctly //
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;

            GridView view = (GridView)sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to analyse before proceeding!", "Analyse Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobActionID"])) + ',';
            }
            sp01218_AT_WorkOrders_Analysis_Jobs_AnalyseTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01218_AT_WorkOrders_Analysis_Jobs_Analyse, strRecordIDs);
            if (chartControl1.DataSource == null) LinkChartsToPivotGrids();
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
         /*   if (checkEdit1.Checked)
            {
                chartControl1.DataSource = pivotGridControl1;
                chartControl1.SeriesDataMember = "Series";
                chartControl1.SeriesTemplate.ArgumentDataMember = "Arguments";
                chartControl1.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "Values" });
                pivotGridControl1.OptionsChartDataSource.ProvideDataByColumns = comboBoxEdit1.SelectedIndex == 1;
                splitContainerControl4.PanelVisibility = SplitPanelVisibility.Both;
            }
            else
            {
                chartControl1.DataSource = null;
                splitContainerControl4.PanelVisibility = SplitPanelVisibility.Panel1;
            }*/
        }

        private void pivotGridControl1_FieldAreaChanged(object sender, DevExpress.XtraPivotGrid.PivotFieldEventArgs e)
        {
            string fieldName = e.Field.FieldName;
        }

        private void pivotGridControl1_MouseUp(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            if (e.Button != MouseButtons.Right) return;
            if (pivotGridControl1.CalcHitInfo(pt).HitTest != PivotGridHitTest.Cell) return;
            // Shows the context menu.
            popupMenu1.ShowPopup(barManager1, pivotGridControl1.PointToScreen(pt));
        }

        private void pivotGridControl1_PopupMenuShowing(object sender, DevExpress.XtraPivotGrid.PopupMenuShowingEventArgs e)
        {
            PivotGridControl pivotGrid = (PivotGridControl)sender;
            epgmMenu = new ExtendedPivotGridMenu(pivotGrid);
            epgmMenu.ShowPivotGridMenu(sender, e);
        }

        private void barButtonItem1_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Right click menu from Cells of Pivot Grid //
            pivotGridControl1.Cells.CopySelectionToClipboard();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            // Create a new Wizard.
            ChartWizard wizard = new ChartWizard(this.chartControl1);

            // Obtain a Data page.
            WizardDataPage page = wizard.DataPage;

            // Hide datasource-related tabs on the Data page.
            page.HiddenPageTabs.Add(DataPageTab.AutoCreatedSeries);
            page.HiddenPageTabs.Add(DataPageTab.SeriesBinding);

            // Invoke the Wizard window.
            wizard.ShowDialog();
            Chart_Preserve_User_Interaction(chartControl1);  // Ensure end-user rotation still working if user converted from 2D to 3D chart //

            // Update Main Forms Ribbon (Chart Styling) //
            frmMain2 MainForm = (frmMain2)this.ParentForm;

            string strViewType = chartControl1.SeriesTemplate.View.ToString();
            ViewType viewType;
            switch (strViewType)
            {
                case "Bar":
                    viewType = ViewType.Bar;
                    break;
                case "StackedBar":
                    viewType = ViewType.StackedBar;
                    break;
                case "FullStackedBar":
                    viewType = ViewType.FullStackedBar;
                    break;
                case "SideBySideStackedBar":
                    viewType = ViewType.SideBySideStackedBar;
                    break;
                case "SideBySideFullStackedBar":
                    viewType = ViewType.SideBySideFullStackedBar;
                    break;
                case "Pie":
                    viewType = ViewType.Pie;
                    break;
                case "Doughnut":
                    viewType = ViewType.Doughnut;
                    break;
                case "Funnel":
                    viewType = ViewType.Funnel;
                    break;
                case "Bubble":
                    viewType = ViewType.Bubble;
                    break;
                case "Line":
                    viewType = ViewType.Line;
                    break;
                case "StepLine":
                    viewType = ViewType.StepLine;
                    break;
                case "Spline":
                    viewType = ViewType.Spline;
                    break;
                case "ScatterLine":
                    viewType = ViewType.ScatterLine;
                    break;
                case "SwiftPlot":
                    viewType = ViewType.SwiftPlot;
                    break;
                case "Area":
                    viewType = ViewType.Area;
                    break;
                case "SplineArea":
                    viewType = ViewType.SplineArea;
                    break;
                case "StackedArea":
                    viewType = ViewType.StackedArea;
                    break;
                case "StackedSplineArea":
                    viewType = ViewType.StackedSplineArea;
                    break;
                case "FullStackedArea":
                    viewType = ViewType.FullStackedArea;
                    break;
                case "FullStackedSplineArea":
                    viewType = ViewType.FullStackedSplineArea;
                    break;
                case "Stock":
                    viewType = ViewType.Stock;
                    break;
                case "CandleStick":
                    viewType = ViewType.CandleStick;
                    break;
                case "SideBySideRangeBar":
                    viewType = ViewType.SideBySideRangeBar;
                    break;
                case "RangeBar":
                    viewType = ViewType.RangeBar;
                    break;
                case "SideBySideGantt":
                    viewType = ViewType.SideBySideGantt;
                    break;
                case "Gantt":
                    viewType = ViewType.Gantt;
                    break;
                case "PolarPoint":
                    viewType = ViewType.PolarPoint;
                    break;
                case "PolarLine":
                    viewType = ViewType.PolarLine;
                    break;
                case "PolarArea":
                    viewType = ViewType.PolarArea;
                    break;
                case "RadarPoint":
                    viewType = ViewType.RadarPoint;
                    break;
                case "RadarLine":
                    viewType = ViewType.RadarLine;
                    break;
                case "RadarArea":
                    viewType = ViewType.RadarArea;
                    break;
                case "Bar3D":
                    viewType = ViewType.Bar3D;
                    break;
                case "StackedBar3D":
                    viewType = ViewType.StackedBar3D;
                    break;
                case "FullStackedBar3D":
                    viewType = ViewType.FullStackedBar3D;
                    break;
                case "ManhattanBar":
                    viewType = ViewType.ManhattanBar;
                    break;
                case "SideBySideStackedBar3D":
                    viewType = ViewType.SideBySideStackedBar3D;
                    break;
                case "SideBySideFullStackedBar3D":
                    viewType = ViewType.SideBySideFullStackedBar3D;
                    break;
                case "Pie3D":
                    viewType = ViewType.Pie3D;
                    break;
                case "Doughnut3D":
                    viewType = ViewType.Doughnut3D;
                    break;
                case "Funnel3D":
                    viewType = ViewType.Funnel3D;
                    break;
                case "Line3D":
                    viewType = ViewType.Line3D;
                    break;
                case "StepLine3D":
                    viewType = ViewType.StepLine3D;
                    break;
                case "Area3D":
                    viewType = ViewType.Area3D;
                    break;
                case "StackedArea3D":
                    viewType = ViewType.StackedArea3D;
                    break;
                case "FullStackedArea3D":
                    viewType = ViewType.FullStackedArea3D;
                    break;
                case "Spline3D":
                    viewType = ViewType.Spline3D;
                    break;
                case "SplineArea3D":
                    viewType = ViewType.SplineArea3D;
                    break;
                case "StackedSplineArea3D":
                    viewType = ViewType.StackedSplineArea3D;
                    break;
                case "FullStackedSplineArea3D":
                    viewType = ViewType.FullStackedSplineArea3D;
                    break;
                default:
                     viewType = ViewType.Line;
                   break;
            }
            MainForm.ViewType = viewType;
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Copy to Clipboard popup menu item - fired from pivotGridControl1_MouseUp event //
            pivotGridControl1.Cells.CopySelectionToClipboard();
        }


        private void frm_AT_WorkOrder_Analysis_1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                // Update Main Forms Ribbon (Chart Styling) - Remove link from current chart //
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.Kill_Chart_Styling();
            }
            catch (Exception)
            {
            }
        }

        public override void OnLayoutRotateEvent(object sender, EventArgs e)
        {
            splitContainerControl3.Horizontal = !splitContainerControl3.Horizontal;
        }

        public override void OnLayoutFlipEvent(object sender, EventArgs e)
        {
            splitContainerControl3.Visible = false;
            Control parent1 = (Control)pivotGridControl1.Parent;
            Control parent2 = (Control)chartControl1.Parent;
            Control child1 = (Control)pivotGridControl1;
            Control child2 = (Control)chartControl1;
            parent1.Controls.Add(child2);
            parent2.Controls.Add(child1);
            splitContainerControl3.Visible = true;
        }

        private void LinkChartToRibbonBar()
        {
            ChartControl chart = chartControl1;
            PivotGridControl pivotGrid = pivotGridControl1;

            // Link Chart to Chart Gallery on Ribbon Bar //
            try
            {
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.Set_Chart(chart, pivotGrid);
                MainForm.InitChartPaletteGallery(MainForm.gddChartPalette);  // Analysis Drop Down list of Colour schemes for charting //
                MainForm.InitChartAppearanceGallery(MainForm.rgbiChartAppearance, chart.PaletteName);
                MainForm.ViewType = DevExpress.XtraCharts.Native.SeriesViewFactory.GetViewType(chart.Series[0].View);
            }
            catch { }
        }

        private void chartControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                pmChart.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void bbiChartWizard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.RunChartWizard();
            }
            catch { }

        }

        private void bbiRotateAxis_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.RotateChartAxis();
            }
            catch { }
        }

    }
}


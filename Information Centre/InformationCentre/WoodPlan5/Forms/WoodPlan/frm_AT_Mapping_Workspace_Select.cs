using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;  // Mouse down Args for GridView Mouse Down //
using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Workspace_Select : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        GridHitInfo downHitInfo = null;
        public int intWorkspaceID = 0;
        public int intWorkspaceOwner = 0;
        public string strWorkspaceName = "";
        public int intCurrentlyLoadedWorkspaceID = 0; // This is the workspace loaded [so can't delete it] if called from an open Mapping Tree Picker screen //

        #endregion

        public frm_AT_Mapping_Workspace_Select()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Workspace_Select_Load(object sender, EventArgs e)
        {
            this.FormID = 20041;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked at end of event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;
           
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01264_AT_Tree_Picker_Available_WorkspacesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01264_AT_Tree_Picker_Available_WorkspacesTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01264_AT_Tree_Picker_Available_Workspaces, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);
            view.EndUpdate();
            gridControl1.ForceInitialize();
            RefreshGridViewState1 = new RefreshGridState(view, "WorkspaceID");

            if (intCurrentlyLoadedWorkspaceID > 0)   // Highlight currently selected row //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["WorkspaceID"], intCurrentlyLoadedWorkspaceID);
                if (intFoundRow != GridControl.InvalidRowHandle) view.FocusedRowHandle = intFoundRow;
            }         
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }  

        private void btnOK_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                XtraMessageBox.Show("Select the Mapping Workspace to use before proceeding.", "Select Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intWorkspaceID = Convert.ToInt32(view.GetFocusedRowCellValue("WorkspaceID"));
            intWorkspaceOwner = Convert.ToInt32(view.GetFocusedRowCellValue("CreatedByID"));
            strWorkspaceName = Convert.ToString(view.GetFocusedRowCellValue("WorkspaceName"));
            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails any validation tests //
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddRecord();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditRecord();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeleteRecord();
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            AddRecord();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            EditRecord();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            DeleteRecord();
        }

        private void AddRecord()
        {
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter AddRecord = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
            AddRecord.ChangeConnectionString(strConnectionString);
            int intWorkspaceID = 0;
            try
            {
                intWorkspaceID = Convert.ToInt32(AddRecord.sp01272_AT_Tree_Picker_Workspace_Add(this.GlobalSettings.UserID));
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Unable to create new workspace - an error occurred [" + ex.Message + "]!\n\nTry adding again - if the problem persists, contact Technical Support.", "Create Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intWorkspaceID == 0)
            {
                XtraMessageBox.Show("Unable to create new workspace - an error occurred!\n\nTry adding again - if the problem persists, contact Technical Support.", "Create Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frm_AT_Mapping_Workspace_Edit fChildForm = new frm_AT_Mapping_Workspace_Edit();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = "frm_AT_Mapping_Workspace_Select";
            fChildForm.intRecordCount = 0;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.fProgress = fProgress;
            fChildForm.intWorkSpaceID = intWorkspaceID;
            if (fChildForm.ShowDialog() != DialogResult.OK) // User Aborted New Workspace //
            {
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter DeleteRecord = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                DeleteRecord.ChangeConnectionString(strConnectionString);
                try
                {
                    DeleteRecord.sp01273_AT_Tree_Picker_Workspace_Delete(intWorkspaceID);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while removing the new workspace [" + ex.Message + "]!", "Create Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                return;
            }
            else
            {
                // Refresh Workspace list //
                GridView view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                sp01264_AT_Tree_Picker_Available_WorkspacesTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01264_AT_Tree_Picker_Available_Workspaces, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);
                this.RefreshGridViewState1.LoadViewInfo();
                view.EndUpdate();
            }
        }

        private void EditRecord()
        {
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the workspace to edit before proceeding.", "Edit Mapping Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetFocusedRowCellValue("CreatedByID")) != GlobalSettings.UserID && GlobalSettings.PersonType != 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You cannot edit the currently selected workspace - only the user who created the workspace can edit it.\n\nSelect a different workspace to edit before proceeding.", "Edit Mapping Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intWorkspaceID = Convert.ToInt32(view.GetFocusedRowCellValue("WorkspaceID"));

            frm_AT_Mapping_Workspace_Edit fChildForm = new frm_AT_Mapping_Workspace_Edit();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = "frm_AT_Mapping_Workspace_Select";
            fChildForm.intRecordCount = 0;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.fProgress = fProgress;
            fChildForm.intWorkSpaceID = intWorkspaceID;
            if (fChildForm.ShowDialog() == DialogResult.OK) // User Saved changes //
            {
                // Refresh Workspace list //
                view.BeginUpdate();
                sp01264_AT_Tree_Picker_Available_WorkspacesTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01264_AT_Tree_Picker_Available_Workspaces, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);
                this.RefreshGridViewState1.LoadViewInfo();
                view.EndUpdate();
           }
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the workspace to delete before proceeding.", "Delete Mapping Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetFocusedRowCellValue("CreatedByID")) != GlobalSettings.UserID && GlobalSettings.PersonType != 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You cannot delete the currently selected workspace - only the user who created the workspace can delete it.\n\nSelect a different workspace to delete before proceeding.", "Delete Mapping Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intWorkspaceID = Convert.ToInt32(view.GetFocusedRowCellValue("WorkspaceID"));
            if (intWorkspaceID == intCurrentlyLoadedWorkspaceID)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You cannot delete the currently selected workspace - it is in use in the Mapping screen.\n\nSelect a different Workspace in the Mapping screen before trying to delete this Workspace.", "Delete Mapping Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Warning you are about to delete a Mapping Workspace!\n\nAre you sure you wish to proceed?", "Delete Workspace", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {

                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter DeleteWorkspace = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                DeleteWorkspace.ChangeConnectionString(strConnectionString);
                try
                {
                    DeleteWorkspace.sp01273_AT_Tree_Picker_Workspace_Delete(intWorkspaceID);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete Workspace - An Error occurred [" + Ex.Message + "]!\n\nDelete Aborted. Try deleting again, if the problem persists, contact Technical Support.", "Delete Mapping Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // Refresh Workspace list //
                view.BeginUpdate();
                sp01264_AT_Tree_Picker_Available_WorkspacesTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01264_AT_Tree_Picker_Available_Workspaces, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);
                view.EndUpdate();
            }
        }


        #region Grid Control 1
        
        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                EditRecord();
            }

        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiBlockAdd.Enabled = false;
                bbiSave.Enabled = false;
                bsiDataset.Enabled = false;
                if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                    bbiBlockEdit.Enabled = false;
                    bbiDelete.Enabled = false;
                }
                else if (Convert.ToInt32(view.GetFocusedRowCellValue("CreatedByID")) == this.GlobalSettings.UserID || this.GlobalSettings.PersonType == 1)
                {
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    bbiBlockEdit.Enabled = false;
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                    bbiBlockEdit.Enabled = false;
                    bbiDelete.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    AddRecord();
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Edit:
                    EditRecord();
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    DeleteRecord();
                    e.Handled = true;
                    break;
            }
        }




        #endregion

    }
}


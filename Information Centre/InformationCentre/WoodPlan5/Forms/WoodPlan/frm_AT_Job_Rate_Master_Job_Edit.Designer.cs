namespace WoodPlan5
{
    partial class frm_AT_Job_Rate_Master_Job_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Job_Rate_Master_Job_Edit));
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.IsUtilityArbJobCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.sp00207JobRateMasterJobItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.IsAmenityArbJobCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsOperationsJobCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.JobDisabledCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.strJobCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strJobDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DefaultWorkUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intJobIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForintJobID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrJobCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultWorkUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrJobDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIsOperationsJob = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsAmenityArbJob = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsUtilityArbJob = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00207_Job_Rate_Master_Job_ItemTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00207_Job_Rate_Master_Job_ItemTableAdapter();
            this.IsExtraWorksJobCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ItemForIsExtraWorksJob = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IsUtilityArbJobCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00207JobRateMasterJobItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsAmenityArbJobCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsOperationsJobCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobDisabledCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strJobCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strJobDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultWorkUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intJobIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintJobID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrJobCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultWorkUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrJobDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsOperationsJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsAmenityArbJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsUtilityArbJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsExtraWorksJobCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsExtraWorksJob)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.IsExtraWorksJobCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsUtilityArbJobCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsAmenityArbJobCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsOperationsJobCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.JobDisabledCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.strJobCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strJobDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultWorkUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intJobIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.DataSource = this.sp00207JobRateMasterJobItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintJobID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1292, 293, 457, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // IsUtilityArbJobCheckEdit
            // 
            this.IsUtilityArbJobCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "IsUtilityArbJob", true));
            this.IsUtilityArbJobCheckEdit.Location = new System.Drawing.Point(109, 176);
            this.IsUtilityArbJobCheckEdit.MenuManager = this.barManager1;
            this.IsUtilityArbJobCheckEdit.Name = "IsUtilityArbJobCheckEdit";
            this.IsUtilityArbJobCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.IsUtilityArbJobCheckEdit.Properties.ValueChecked = 1;
            this.IsUtilityArbJobCheckEdit.Properties.ValueUnchecked = 0;
            this.IsUtilityArbJobCheckEdit.Size = new System.Drawing.Size(139, 19);
            this.IsUtilityArbJobCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsUtilityArbJobCheckEdit.TabIndex = 11;
            // 
            // sp00207JobRateMasterJobItemBindingSource
            // 
            this.sp00207JobRateMasterJobItemBindingSource.DataMember = "sp00207_Job_Rate_Master_Job_Item";
            this.sp00207JobRateMasterJobItemBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // IsAmenityArbJobCheckEdit
            // 
            this.IsAmenityArbJobCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "IsAmenityArbJob", true));
            this.IsAmenityArbJobCheckEdit.Location = new System.Drawing.Point(109, 153);
            this.IsAmenityArbJobCheckEdit.MenuManager = this.barManager1;
            this.IsAmenityArbJobCheckEdit.Name = "IsAmenityArbJobCheckEdit";
            this.IsAmenityArbJobCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.IsAmenityArbJobCheckEdit.Properties.ValueChecked = 1;
            this.IsAmenityArbJobCheckEdit.Properties.ValueUnchecked = 0;
            this.IsAmenityArbJobCheckEdit.Size = new System.Drawing.Size(139, 19);
            this.IsAmenityArbJobCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsAmenityArbJobCheckEdit.TabIndex = 10;
            // 
            // IsOperationsJobCheckEdit
            // 
            this.IsOperationsJobCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "IsOperationsJob", true));
            this.IsOperationsJobCheckEdit.Location = new System.Drawing.Point(109, 130);
            this.IsOperationsJobCheckEdit.MenuManager = this.barManager1;
            this.IsOperationsJobCheckEdit.Name = "IsOperationsJobCheckEdit";
            this.IsOperationsJobCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsOperationsJobCheckEdit.Properties.ValueChecked = 1;
            this.IsOperationsJobCheckEdit.Properties.ValueUnchecked = 0;
            this.IsOperationsJobCheckEdit.Size = new System.Drawing.Size(139, 19);
            this.IsOperationsJobCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsOperationsJobCheckEdit.TabIndex = 4;
            this.IsOperationsJobCheckEdit.ToolTip = "Tick me to make this job available to Operations Manager";
            // 
            // JobDisabledCheckEdit
            // 
            this.JobDisabledCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "JobDisabled", true));
            this.JobDisabledCheckEdit.Location = new System.Drawing.Point(109, 107);
            this.JobDisabledCheckEdit.MenuManager = this.barManager1;
            this.JobDisabledCheckEdit.Name = "JobDisabledCheckEdit";
            this.JobDisabledCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.JobDisabledCheckEdit.Properties.ValueChecked = 1;
            this.JobDisabledCheckEdit.Properties.ValueUnchecked = 0;
            this.JobDisabledCheckEdit.Size = new System.Drawing.Size(139, 19);
            this.JobDisabledCheckEdit.StyleController = this.dataLayoutControl1;
            this.JobDisabledCheckEdit.TabIndex = 3;
            this.JobDisabledCheckEdit.ToolTip = "Tick me to hide job from Job Selection list on Edit Action screen.";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00207JobRateMasterJobItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(109, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(161, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 9;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // strJobCodeTextEdit
            // 
            this.strJobCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "strJobCode", true));
            this.strJobCodeTextEdit.Location = new System.Drawing.Point(109, 59);
            this.strJobCodeTextEdit.MenuManager = this.barManager1;
            this.strJobCodeTextEdit.Name = "strJobCodeTextEdit";
            this.strJobCodeTextEdit.Properties.MaxLength = 15;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strJobCodeTextEdit, true);
            this.strJobCodeTextEdit.Size = new System.Drawing.Size(507, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strJobCodeTextEdit, optionsSpelling1);
            this.strJobCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strJobCodeTextEdit.TabIndex = 1;
            this.strJobCodeTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strJobCodeTextEdit_Validating);
            // 
            // strJobDescriptionTextEdit
            // 
            this.strJobDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "strJobDescription", true));
            this.strJobDescriptionTextEdit.Location = new System.Drawing.Point(109, 35);
            this.strJobDescriptionTextEdit.MenuManager = this.barManager1;
            this.strJobDescriptionTextEdit.Name = "strJobDescriptionTextEdit";
            this.strJobDescriptionTextEdit.Properties.MaxLength = 50;
            this.strJobDescriptionTextEdit.Properties.NullValuePrompt = "Enter a Value";
            this.strJobDescriptionTextEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strJobDescriptionTextEdit, true);
            this.strJobDescriptionTextEdit.Size = new System.Drawing.Size(507, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strJobDescriptionTextEdit, optionsSpelling2);
            this.strJobDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.strJobDescriptionTextEdit.TabIndex = 0;
            this.strJobDescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strJobDescriptionTextEdit_Validating);
            // 
            // DefaultWorkUnitsSpinEdit
            // 
            this.DefaultWorkUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "DefaultWorkUnits", true));
            this.DefaultWorkUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DefaultWorkUnitsSpinEdit.Location = new System.Drawing.Point(109, 83);
            this.DefaultWorkUnitsSpinEdit.MenuManager = this.barManager1;
            this.DefaultWorkUnitsSpinEdit.Name = "DefaultWorkUnitsSpinEdit";
            this.DefaultWorkUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DefaultWorkUnitsSpinEdit.Properties.Mask.EditMask = "f2";
            this.DefaultWorkUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DefaultWorkUnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.DefaultWorkUnitsSpinEdit.Size = new System.Drawing.Size(139, 20);
            this.DefaultWorkUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.DefaultWorkUnitsSpinEdit.TabIndex = 2;
            // 
            // intJobIDTextEdit
            // 
            this.intJobIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "intJobID", true));
            this.intJobIDTextEdit.Location = new System.Drawing.Point(106, 12);
            this.intJobIDTextEdit.MenuManager = this.barManager1;
            this.intJobIDTextEdit.Name = "intJobIDTextEdit";
            this.intJobIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intJobIDTextEdit, true);
            this.intJobIDTextEdit.Size = new System.Drawing.Size(510, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intJobIDTextEdit, optionsSpelling3);
            this.intJobIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intJobIDTextEdit.TabIndex = 4;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 302);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(556, 143);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling4);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 5;
            this.strRemarksMemoEdit.TabStop = false;
            // 
            // ItemForintJobID
            // 
            this.ItemForintJobID.Control = this.intJobIDTextEdit;
            this.ItemForintJobID.CustomizationFormText = "Job ID:";
            this.ItemForintJobID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintJobID.Name = "ItemForintJobID";
            this.ItemForintJobID.Size = new System.Drawing.Size(608, 24);
            this.ItemForintJobID.Text = "Job ID:";
            this.ItemForintJobID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 481);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrJobCode,
            this.layoutControlGroup4,
            this.ItemForDefaultWorkUnits,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItem2,
            this.emptySpaceItem5,
            this.ItemForstrJobDescription,
            this.emptySpaceItem6,
            this.ItemForIsOperationsJob,
            this.ItemForIsAmenityArbJob,
            this.ItemForIsUtilityArbJob,
            this.ItemForIsExtraWorksJob});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 461);
            // 
            // ItemForstrJobCode
            // 
            this.ItemForstrJobCode.Control = this.strJobCodeTextEdit;
            this.ItemForstrJobCode.CustomizationFormText = "Job Code:";
            this.ItemForstrJobCode.Location = new System.Drawing.Point(0, 47);
            this.ItemForstrJobCode.Name = "ItemForstrJobCode";
            this.ItemForstrJobCode.Size = new System.Drawing.Size(608, 24);
            this.ItemForstrJobCode.Text = "Job Code:";
            this.ItemForstrJobCode.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details:";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 220);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(608, 241);
            this.layoutControlGroup4.Text = "Details:";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 195);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImage")));
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(560, 147);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(560, 147);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // ItemForDefaultWorkUnits
            // 
            this.ItemForDefaultWorkUnits.Control = this.DefaultWorkUnitsSpinEdit;
            this.ItemForDefaultWorkUnits.CustomizationFormText = "Default Work Units:";
            this.ItemForDefaultWorkUnits.Location = new System.Drawing.Point(0, 71);
            this.ItemForDefaultWorkUnits.MaxSize = new System.Drawing.Size(240, 24);
            this.ItemForDefaultWorkUnits.MinSize = new System.Drawing.Size(240, 24);
            this.ItemForDefaultWorkUnits.Name = "ItemForDefaultWorkUnits";
            this.ItemForDefaultWorkUnits.Size = new System.Drawing.Size(240, 24);
            this.ItemForDefaultWorkUnits.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDefaultWorkUnits.Text = "Default Work Units:";
            this.ItemForDefaultWorkUnits.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 210);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(97, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(165, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(97, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(97, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(97, 23);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(262, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(346, 23);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.JobDisabledCheckEdit;
            this.layoutControlItem2.CustomizationFormText = "Disabled:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 95);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(240, 23);
            this.layoutControlItem2.Text = "Disabled:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(240, 71);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(368, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrJobDescription
            // 
            this.ItemForstrJobDescription.Control = this.strJobDescriptionTextEdit;
            this.ItemForstrJobDescription.CustomizationFormText = "Job Description:";
            this.ItemForstrJobDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForstrJobDescription.Name = "ItemForstrJobDescription";
            this.ItemForstrJobDescription.Size = new System.Drawing.Size(608, 24);
            this.ItemForstrJobDescription.Text = "Job Description:";
            this.ItemForstrJobDescription.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(240, 95);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(368, 115);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIsOperationsJob
            // 
            this.ItemForIsOperationsJob.Control = this.IsOperationsJobCheckEdit;
            this.ItemForIsOperationsJob.CustomizationFormText = "Operations Manager Job:";
            this.ItemForIsOperationsJob.Location = new System.Drawing.Point(0, 118);
            this.ItemForIsOperationsJob.Name = "ItemForIsOperationsJob";
            this.ItemForIsOperationsJob.Size = new System.Drawing.Size(240, 23);
            this.ItemForIsOperationsJob.Text = "Operations Job:";
            this.ItemForIsOperationsJob.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForIsAmenityArbJob
            // 
            this.ItemForIsAmenityArbJob.Control = this.IsAmenityArbJobCheckEdit;
            this.ItemForIsAmenityArbJob.Location = new System.Drawing.Point(0, 141);
            this.ItemForIsAmenityArbJob.Name = "ItemForIsAmenityArbJob";
            this.ItemForIsAmenityArbJob.Size = new System.Drawing.Size(240, 23);
            this.ItemForIsAmenityArbJob.Text = "Amenity Arb Job:";
            this.ItemForIsAmenityArbJob.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForIsUtilityArbJob
            // 
            this.ItemForIsUtilityArbJob.Control = this.IsUtilityArbJobCheckEdit;
            this.ItemForIsUtilityArbJob.Location = new System.Drawing.Point(0, 164);
            this.ItemForIsUtilityArbJob.Name = "ItemForIsUtilityArbJob";
            this.ItemForIsUtilityArbJob.Size = new System.Drawing.Size(240, 23);
            this.ItemForIsUtilityArbJob.Text = "Utlity Arb Job:";
            this.ItemForIsUtilityArbJob.TextSize = new System.Drawing.Size(94, 13);
            // 
            // sp00207_Job_Rate_Master_Job_ItemTableAdapter
            // 
            this.sp00207_Job_Rate_Master_Job_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // IsExtraWorksJobCheckEdit
            // 
            this.IsExtraWorksJobCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00207JobRateMasterJobItemBindingSource, "IsExtraWorksJob", true));
            this.IsExtraWorksJobCheckEdit.Location = new System.Drawing.Point(109, 199);
            this.IsExtraWorksJobCheckEdit.MenuManager = this.barManager1;
            this.IsExtraWorksJobCheckEdit.Name = "IsExtraWorksJobCheckEdit";
            this.IsExtraWorksJobCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.IsExtraWorksJobCheckEdit.Properties.ValueChecked = 1;
            this.IsExtraWorksJobCheckEdit.Properties.ValueUnchecked = 0;
            this.IsExtraWorksJobCheckEdit.Size = new System.Drawing.Size(139, 19);
            this.IsExtraWorksJobCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsExtraWorksJobCheckEdit.TabIndex = 13;
            // 
            // ItemForIsExtraWorksJob
            // 
            this.ItemForIsExtraWorksJob.Control = this.IsExtraWorksJobCheckEdit;
            this.ItemForIsExtraWorksJob.Location = new System.Drawing.Point(0, 187);
            this.ItemForIsExtraWorksJob.Name = "ItemForIsExtraWorksJob";
            this.ItemForIsExtraWorksJob.Size = new System.Drawing.Size(240, 23);
            this.ItemForIsExtraWorksJob.Text = "Extra Works Job:";
            this.ItemForIsExtraWorksJob.TextSize = new System.Drawing.Size(94, 13);
            // 
            // frm_AT_Job_Rate_Master_Job_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Job_Rate_Master_Job_Edit";
            this.Text = "Edit Master Job";
            this.Activated += new System.EventHandler(this.frm_AT_Job_Rate_Master_Job_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Job_Rate_Master_Job_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Job_Rate_Master_Job_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IsUtilityArbJobCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00207JobRateMasterJobItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsAmenityArbJobCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsOperationsJobCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobDisabledCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strJobCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strJobDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultWorkUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intJobIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintJobID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrJobCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultWorkUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrJobDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsOperationsJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsAmenityArbJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsUtilityArbJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsExtraWorksJobCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsExtraWorksJob)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit strJobCodeTextEdit;
        private System.Windows.Forms.BindingSource sp00207JobRateMasterJobItemBindingSource;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.TextEdit strJobDescriptionTextEdit;
        private DevExpress.XtraEditors.SpinEdit DefaultWorkUnitsSpinEdit;
        private DevExpress.XtraEditors.TextEdit intJobIDTextEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintJobID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrJobCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrJobDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultWorkUnits;
        private WoodPlan5.DataSet_ATTableAdapters.sp00207_Job_Rate_Master_Job_ItemTableAdapter sp00207_Job_Rate_Master_Job_ItemTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.CheckEdit JobDisabledCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CheckEdit IsOperationsJobCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsOperationsJob;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit IsUtilityArbJobCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsAmenityArbJobCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsAmenityArbJob;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsUtilityArbJob;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.CheckEdit IsExtraWorksJobCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsExtraWorksJob;
    }
}

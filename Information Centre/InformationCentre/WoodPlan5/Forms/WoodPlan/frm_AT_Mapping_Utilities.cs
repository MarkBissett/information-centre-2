using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using DevExpress.XtraEditors;

using MapInfo.Engine;
using MapInfo.Geometry;
using MapInfo.Mapping;

using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Utilities : BaseObjects.frmBase
    {

        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        private string strDefaultMappingProjection = "";
        #endregion


        public frm_AT_Mapping_Utilities()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Utilities_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;
            this.sp01325_AT_Mapping_Utilities_Register_Files_StructureTableAdapter.Connection.ConnectionString = strConnectionString;
            AdjustEnabledStatus();

            // Get Default Mapping Projection //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            strDefaultMappingProjection = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultMappingProjection").ToString();
            if (string.IsNullOrEmpty(strDefaultMappingProjection)) strDefaultMappingProjection = "CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000";
        }


        #region GridView 1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No files selected for registering - click Select File(s) button");
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            //if (e.Clicks < 2)  // Don't fire if double clicking //
            //{
            //    //SetMenuStatus();
            //}
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator) downHitInfo = hitInfo;
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                int[] intRowHandles = view.GetSelectedRows();
                bbiRemove.Enabled = (intRowHandles.Length > 0 ? true : false);
                bbiClearGrid.Enabled = (view.DataRowCount > 0 ? true : false);
                popupMenuGrid1.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    DeleteRecord();
                    e.Handled = true;
                    break;
            }
        }

        private void DeleteRecord()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or files to remove from the register list by clicking on them then try again.", "Remove files from Register List", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            frmProgress fProgress = new frmProgress(20);
            fProgress.UpdateCaption("Removing files...");
            fProgress.Show();
            Application.DoEvents();
            int intUpdateProgressThreshhold = intCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            //view.BeginDataUpdate();
            //view.BeginSort();
            for (int i = intRowHandles.Length - 1; i >= 0; i--)
            {
                view.DeleteRow(intRowHandles[i]);
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            //view.EndSort();
            //view.EndDataUpdate();
            view.EndUpdate();
            AdjustEnabledStatus();

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void bbiRemove_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeleteRecord();
        }

        private void bbiClearGrid_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int intCount = view.DataRowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or files to remove from the register list by clicking on them then try again.", "Remove files from Register List", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            view.BeginUpdate();
            dataSet_AT_TreePicker.sp01325_AT_Mapping_Utilities_Register_Files_Structure.Rows.Clear();
            view.EndUpdate();
            AdjustEnabledStatus();
        }

        #endregion


        private void btnSelectFiles_Click(object sender, EventArgs e)
        {
            // Get Default Mapping Path //
            string strDefaultPath = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                //strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesMapBackgroundFolder").ToString().ToLower();
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, (this.GlobalSettings.SystemDataTransferMode != "Tablet" ? "AmenityTreesMapBackgroundFolder" : "AmenityTreesMapBackgroundFolderTablet")).ToString().ToLower();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Select Mapping File(s) - No Mapping Background Folder Location has been specified in the System Settings table.", "Select Files(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Process all selected files //
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.DefaultExt = "tab";
                dlg.Filter = "ECW Files (*.ecw)|*.ecw|Bitmap Files (*.bmp)|*.bmp|Jpeg Files (*.jpg)|*.jpg|MrSID Files (*.sid)|*.sid|Tiff Files (*.tif)|*.tif";
                dlg.DefaultExt = "ecw";
                if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                dlg.Multiselect = true;
                dlg.ShowDialog();
                if (dlg.FileNames.Length > 0)
                {
                    DataRow drNewRow;
                    GridView view = (GridView)gridControl1.MainView;
                    bool boolFirstFilenameProcessed = false;
                    foreach (string filename in dlg.FileNames)
                    {                         
                        FileInfo file = new FileInfo(filename);
                        drNewRow = dataSet_AT_TreePicker.sp01325_AT_Mapping_Utilities_Register_Files_Structure.NewRow();
                        drNewRow["Status"] = "";
                        drNewRow["FileName"] = file.Name.Substring(0, file.Name.Length - 4);
                        drNewRow["FilePath"] = file.FullName; ;
                        dataSet_AT_TreePicker.sp01325_AT_Mapping_Utilities_Register_Files_Structure.Rows.Add(drNewRow);

                        if (file.Name.Substring(0, file.Name.Length - 4).Length == 4 && ! boolFirstFilenameProcessed)
                        {
                            spinEditTileWidth.Value = 20000;
                            spinEditTileHeight.Value = 20000;
                            spinEditWidthInPixels.Value = 4000;
                            spinEditHeightInPixels.Value = 4000;
                        }
                        boolFirstFilenameProcessed = true;
                    }
                    if (view.DataRowCount > 1)
                    {
                        checkEditUseFilename.Checked = true;
                        checkEditUseCoords.Enabled = false;
                    }
                }
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int intRowCount = view.DataRowCount;
            if (intRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or files to register by clicking the Select File(s) button before proceeding.", "Register Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            frmProgress fProgress = new frmProgress(20);
            fProgress.UpdateCaption("Registering files...");
            fProgress.Show();
            Application.DoEvents();
            int intUpdateProgressThreshhold = intRowCount / 10;
            int intUpdateProgressTempCount = 0;

            string strFileName = "";
            string strFullPath = "";
            int intTileWidth = 0;
            int intTileHeight = 0;
            int intWidthInPixels = Convert.ToInt32(spinEditWidthInPixels.EditValue);
            int intHeightInPixels = Convert.ToInt32(spinEditHeightInPixels.EditValue);

            view.BeginUpdate();
            for (int i = 0; i < intRowCount; i++)
            {
                intTileWidth = Convert.ToInt32(spinEditTileWidth.EditValue);  // Reset value back to user inputed, ready for next file to be processed just in case following code temporarily changed it //
                intTileHeight = Convert.ToInt32(spinEditTileHeight.EditValue);  // Reset value back to user inputed, ready for next file to be processed just in case following code temporarily changed it //

                strFileName = view.GetRowCellValue(i, "FileName").ToString();
                strFullPath = view.GetRowCellValue(i, "FilePath").ToString();

                FileInfo file = new FileInfo(strFullPath);
                string strNewFileName = file.DirectoryName + "\\" + strFileName + ".tab";

                try
                {
                    StreamWriter sw = new StreamWriter(strNewFileName, false);
                    sw.Write("!table"); sw.Write(sw.NewLine);
                    sw.Write("!version 300"); sw.Write(sw.NewLine);
                    sw.Write("!charset WindowsLatin1"); sw.Write(sw.NewLine);
                    sw.Write(sw.NewLine);
                    sw.Write("Definition Table"); sw.Write(sw.NewLine);
                    sw.Write("  File \"" + file.Name + "\""); sw.Write(sw.NewLine);
                    sw.Write("  Type \"RASTER\""); sw.Write(sw.NewLine);
                    string strX = "";
                    string strY = "";
                    if (checkEditUseFilename.Checked)
                    {
                        // Calculate Data to be written based on file's filename... //
                        char strFirstChar = strFileName[0];
                        char strSecondChar = strFileName[1];
                        if (CheckingFunctions.IsNumeric(strFirstChar) && CheckingFunctions.IsNumeric(strSecondChar))
                        {
                            // Filename starts with 2 numbers //
                            strX = strFileName.Substring(0, 4).ToString() + "00";
                            strY = strFileName.Substring(4, 4).ToString() + "00";


                        }
                        else // Filename does not start with 2 numbers so look it up //
                        {
                            string strValue = "";

                            #region Switch Clause for Letters

                            switch (strFirstChar.ToString().ToUpper() + strSecondChar.ToString().ToUpper())
                            {
                                case "SV":
                                    strValue = "00";
                                    break;
                                case "NL":
                                    strValue = "07";
                                    break;
                                case "NF":
                                    strValue = "08";
                                    break;
                                case "NA":
                                    strValue = "09";
                                    break;
                                case "SW":
                                    strValue = "10";
                                    break;
                                case "SR":
                                    strValue = "11";
                                    break;
                                case "SM":
                                    strValue = "12";
                                    break;
                                case "NW":
                                    strValue = "15";
                                    break;
                                case "NR":
                                    strValue = "16";
                                    break;
                                case "NM":
                                    strValue = "17";
                                    break;
                                case "NG":
                                    strValue = "18";
                                    break;
                                case "NB":
                                    strValue = "19";
                                    break;
                                case "SX":
                                    strValue = "20";
                                    break;
                                case "SS":
                                    strValue = "21";
                                    break;
                                case "SN":
                                    strValue = "22";
                                    break;
                                case "SH":
                                    strValue = "23";
                                    break;
                                case "SC":
                                    strValue = "24";
                                    break;
                                case "NX":
                                    strValue = "25";
                                    break;
                                case "NS":
                                    strValue = "26";
                                    break;
                                case "NN":
                                    strValue = "27";
                                    break;
                                case "NH":
                                    strValue = "28";
                                    break;
                                case "NC":
                                    strValue = "29";
                                    break;
                                case "SY":
                                    strValue = "30";
                                    break;
                                case "ST":
                                    strValue = "31";
                                    break;
                                case "SO":
                                    strValue = "32";
                                    break;
                                case "SJ":
                                    strValue = "33";
                                    break;
                                case "SD":
                                    strValue = "34";
                                    break;
                                case "NY":
                                    strValue = "35";
                                    break;
                                case "NT":
                                    strValue = "36";
                                    break;
                                case "NO":
                                    strValue = "37";
                                    break;
                                case "NJ":
                                    strValue = "38";
                                    break;
                                case "ND":
                                    strValue = "39";
                                    break;
                                case "SZ":
                                    strValue = "40";
                                    break;
                                case "SU":
                                    strValue = "41";
                                    break;
                                case "SP":
                                    strValue = "42";
                                    break;
                                case "SK":
                                    strValue = "43";
                                    break;
                                case "SE":
                                    strValue = "44";
                                    break;
                                case "NZ":
                                    strValue = "45";
                                    break;
                                case "NU":
                                    strValue = "46";
                                    break;
                                case "NK":
                                    strValue = "48";
                                    break;
                                case "NE":
                                    strValue = "49";
                                    break;
                                case "TV":
                                    strValue = "50";
                                    break;
                                case "TQ":
                                    strValue = "51";
                                    break;
                                case "TL":
                                    strValue = "52";
                                    break;
                                case "TF":
                                    strValue = "53";
                                    break;
                                case "TA":
                                    strValue = "54";
                                    break;
                                case "TR":
                                    strValue = "61";
                                    break;
                                case "TM":
                                    strValue = "62";
                                    break;
                                case "TG":
                                    strValue = "63";
                                    break;
                                default:
                                    strValue = "00";
                                    break;
                            }

                            #endregion


                            if (strFileName.Length == 4)  // Using Format Leter Letter Number Number eg "TQ24" //
                            {
                                strFirstChar = strFileName[2];
                                strSecondChar = strFileName[3];
                                strX = strValue[0].ToString() + strFileName[2].ToString() + "0000";
                                strY = strValue[1].ToString() + strFileName[3].ToString() + "0000";
                                intTileWidth = 20000;  // Hardcoded for this file type //
                                intTileHeight = 20000;  // Hardcoded for this file type //

                            }
                            else
                            {
                                strFirstChar = strFileName[4];
                                strSecondChar = strFileName[5];

                                if (CheckingFunctions.IsNumeric(strFirstChar) && CheckingFunctions.IsNumeric(strSecondChar))  // Last 2 digits are numbers //
                                {
                                    strX = strValue[0].ToString() + strFileName[2].ToString() + strFileName[3].ToString() + "000";
                                    strY = strValue[1].ToString() + strFileName[4].ToString() + strFileName[5].ToString() + "000";
                                }
                                else  // Last 2 digits are letters //
                                {
                                    intTileWidth = 5000;  // Hardcoded for this file type //
                                    intTileHeight = 5000;  // Hardcoded for this file type //
                                    strX = strValue[0].ToString() + strFileName[2].ToString() + "0000";
                                    strY = strValue[1].ToString() + strFileName[3].ToString() + "0000";
                                    switch (strFileName[4].ToString().ToUpper() + strFileName[5].ToString().ToUpper())
                                    {
                                        case "SW":
                                            break;
                                        case "NW":
                                            strY = Convert.ToString((Convert.ToInt32(strY) + 5000));
                                            break;
                                        case "SE":
                                            strX = Convert.ToString((Convert.ToInt32(strX) + 5000));
                                            break;
                                        case "NE":
                                            strX = Convert.ToString((Convert.ToInt32(strX) + 5000));
                                            strY = Convert.ToString((Convert.ToInt32(strY) + 5000));
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    else  // Using user specified minimum X and Y values //
                    {
                        strX = spinEdit1.EditValue.ToString();
                        strY = spinEdit2.EditValue.ToString();
                    }
                    sw.Write("  (" + strX + "," + Convert.ToString(Convert.ToInt32(strY) + intTileHeight) + ") (0,0) Label \"Pt 1\","); sw.Write(sw.NewLine);
                    sw.Write("  (" + Convert.ToString(Convert.ToInt32(strX) + intTileWidth) + "," + Convert.ToString(Convert.ToInt32(strY) + intTileHeight) + ") (" + intWidthInPixels.ToString() + ",0) Label \"Pt 2\","); sw.Write(sw.NewLine);
                    sw.Write("  (" + strX + "," + strY + ") (0," + intHeightInPixels.ToString() + ") Label \"Pt 3\""); sw.Write(sw.NewLine);
                    sw.Write("  " + strDefaultMappingProjection); sw.Write(sw.NewLine);
                    sw.Write("  Units \"m\"");
                    sw.Close();
                    view.SetRowCellValue(i, "Status", "Registered");
                }
                catch (Exception)
                {
                    view.SetRowCellValue(i, "Status", "Filename Error!");
                }

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndUpdate();

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkEditUseFilename_CheckedChanged(object sender, EventArgs e)
        {
            AdjustEnabledStatus();
        }

        private void checkEditUseCoords_CheckedChanged(object sender, EventArgs e)
        {
            AdjustEnabledStatus();
        }

        public void AdjustEnabledStatus()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (checkEditUseFilename.Checked || view.DataRowCount > 1)
            {
                spinEdit1.Enabled = false;
                spinEdit2.Enabled = false;
            }
            else
            {
                spinEdit1.Enabled = true;
                spinEdit2.Enabled = true;
            }
            checkEditUseCoords.Enabled = (view.DataRowCount > 1 ? false : true);
        }


        #region Synchronise Page

        private void btnClose2_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnSynchronise_Click(object sender, EventArgs e)
        {
            bool boolTrees = checkEditTrees.Checked;
            bool boolAssets = checkEditAssets.Checked;
            int intDataAmount = (checkEditAllData.Checked ? 0 : 1);

            if (!boolTrees && !boolAssets)
            {
                XtraMessageBox.Show("Select one or more Update Data Types before proceeding.", "Synchronise Lat \\ Long Values", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You are about to Synchronise the Lat \\ Long Values with the Easting and Northing Values.\n\nNote that this process may take some time to complete.\n\nAre you sure you wish to proceed?", "Synchronise Lat \\ Long Values", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No) return;

            // Checks passed so delete selected record(s) //
            int intUpdateProgressThreshhold = 0;
            int intUpdateProgressTempCount = 0;
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Synchronising Lat \\ Long...");
            fProgress.Show();
            Application.DoEvents();

            if (boolTrees)
            {
                fProgress.UpdateCaption("Sync Lat \\ Long - Trees...");
                int intDataType = 0;
                SqlDataAdapter sdaRecords = new SqlDataAdapter();
                DataSet dsRecords = new DataSet("NewDataSet");
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp01451_AT_Mapping_Utilities_Get_Required_Lat_Long_Sync", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@intDataAmount", intDataAmount));
                cmd.Parameters.Add(new SqlParameter("@intDataType", intDataType));
                dsRecords.Clear();  // Remove old values first //
                sdaRecords = new SqlDataAdapter(cmd);
                try
                {
                    sdaRecords.Fill(dsRecords, "Table");
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while loading the Tree records for synchronisation - [" + ex.Message + "]. Process aborted.", "Synchronise Lat \\ Long Values", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                intUpdateProgressThreshhold = dsRecords.Tables[0].Rows.Count / 10;
                intUpdateProgressTempCount = 0;

                string strType = "";
                float dX = (float)0.00;
                float dY = (float)0.00;
                string strNewPolyXY = "";

                Mapping_Functions MapFunctions = new Mapping_Functions();
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter SyncData = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                SyncData.ChangeConnectionString(strConnectionString);

                foreach (DataRow dr in dsRecords.Tables[0].Rows)
                {
                    dX = (float)(double)dr["XCoordinate"];
                    dY = (float)(double)dr["YCoordinate"];
                    strNewPolyXY = dr["strPolygonXY"].ToString();
                    MapInfo.Geometry.FeatureGeometry ftr = MapFunctions.CreateLatLongFeatureFromNonLatLongCoords(dX, dY, strNewPolyXY);
                    if (ftr == null) continue; // Skip to next row //
                    MapFunctions.GetLatLongCoordsFromLatLongFeature(ftr, ref dX, ref dY, ref strNewPolyXY);
                    // Update Back-End //
                    try
                    {
                        SyncData.sp01452_AT_Mapping_Utilities_Set_Lat_Long(0, Convert.ToInt32(dr["RecordID"]), (double)dX, (double)dY, strNewPolyXY);
                    }
                    catch (Exception ex)
                    {
                    }

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
               }
            }
            if (boolAssets)
            {
                fProgress.UpdateCaption("Sync Lat \\ Long - Assets...");
                fProgress.SetProgressValue(0);
                int intDataType = 1;
                SqlDataAdapter sdaRecords = new SqlDataAdapter();
                DataSet dsRecords = new DataSet("NewDataSet");
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp01451_AT_Mapping_Utilities_Get_Required_Lat_Long_Sync", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@intDataAmount", intDataAmount));
                cmd.Parameters.Add(new SqlParameter("@intDataType", intDataType));
                dsRecords.Clear();  // Remove old values first //
                sdaRecords = new SqlDataAdapter(cmd);
                try
                {
                    sdaRecords.Fill(dsRecords, "Table");
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while loading the Asset records for synchronisation - [" + ex.Message + "]. Process aborted.", "Synchronise Lat \\ Long Values", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                intUpdateProgressThreshhold = dsRecords.Tables[0].Rows.Count / 10;
                intUpdateProgressTempCount = 0;

                string strType = "";
                float dX = (float)0.00;
                float dY = (float)0.00;
                string strNewPolyXY = "";

                Mapping_Functions MapFunctions = new Mapping_Functions();
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter SyncData = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                SyncData.ChangeConnectionString(strConnectionString);

                foreach (DataRow dr in dsRecords.Tables[0].Rows)
                {

                    dX = (float)Convert.ToDecimal(dr["XCoordinate"]);
                    dY = (float)Convert.ToDecimal(dr["YCoordinate"]);
                    strNewPolyXY = dr["strPolygonXY"].ToString();
                    MapInfo.Geometry.FeatureGeometry ftr = MapFunctions.CreateLatLongFeatureFromNonLatLongCoords(dX, dY, strNewPolyXY);
                    if (ftr == null) continue; // Skip to next row //
                    MapFunctions.GetLatLongCoordsFromLatLongFeature(ftr, ref dX, ref dY, ref strNewPolyXY);
                    // Update Back-End //
                    try
                    {
                        SyncData.sp01452_AT_Mapping_Utilities_Set_Lat_Long(1, Convert.ToInt32(dr["RecordID"]), (double)dX, (double)dY, strNewPolyXY);
                    }
                    catch (Exception ex)
                    {
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
               }
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            XtraMessageBox.Show("Syncronising of Lat \\ Long Data Completed Successfully.", "Synchronise Lat \\ Long Values", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion




    }
}


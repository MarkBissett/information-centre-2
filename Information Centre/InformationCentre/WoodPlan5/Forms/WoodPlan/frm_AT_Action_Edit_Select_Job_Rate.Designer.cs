namespace WoodPlan5
{
    partial class frm_AT_Action_Edit_Select_Job_Rate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Action_Edit_Select_Job_Rate));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colMatch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01392ATJobRatesForActionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRateTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRateDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp01392_AT_Job_Rates_For_ActionTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01392_AT_Job_Rates_For_ActionTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiGridHighlightColour = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp00206JobRateManagerJobRateParametersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrJobcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobdescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRateTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRateDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldecRateUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrCriteriaDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCriteriaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintParameterID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintRateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintRateType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01392ATJobRatesForActionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00206JobRateManagerJobRateParametersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(633, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 531);
            this.barDockControlBottom.Size = new System.Drawing.Size(633, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 505);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(633, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 505);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.beiGridHighlightColour});
            this.barManager1.MaxItemId = 31;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemColorEdit1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colMatch
            // 
            this.colMatch.Caption = "Status";
            this.colMatch.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colMatch.FieldName = "Match";
            this.colMatch.Name = "colMatch";
            this.colMatch.OptionsColumn.AllowEdit = false;
            this.colMatch.OptionsColumn.AllowFocus = false;
            this.colMatch.OptionsColumn.ReadOnly = true;
            this.colMatch.Visible = true;
            this.colMatch.VisibleIndex = 0;
            this.colMatch.Width = 63;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01392ATJobRatesForActionBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(629, 437);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01392ATJobRatesForActionBindingSource
            // 
            this.sp01392ATJobRatesForActionBindingSource.DataMember = "sp01392_AT_Job_Rates_For_Action";
            this.sp01392ATJobRatesForActionBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRateID,
            this.colJobID,
            this.colValidFrom,
            this.colValidTo,
            this.colRate,
            this.colJobCode,
            this.colJobDescription,
            this.colType,
            this.colUnits,
            this.colRateTypeDescription,
            this.colRateDesc,
            this.colMatch});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.LightGreen;
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colMatch;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 1;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMatch, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRateDesc, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridView1_CustomColumnDisplayText);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colRateID
            // 
            this.colRateID.Caption = "Rate ID";
            this.colRateID.FieldName = "RateID";
            this.colRateID.Name = "colRateID";
            this.colRateID.OptionsColumn.AllowEdit = false;
            this.colRateID.OptionsColumn.AllowFocus = false;
            this.colRateID.OptionsColumn.ReadOnly = true;
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // colValidFrom
            // 
            this.colValidFrom.Caption = "Valid From";
            this.colValidFrom.FieldName = "ValidFrom";
            this.colValidFrom.Name = "colValidFrom";
            this.colValidFrom.OptionsColumn.AllowEdit = false;
            this.colValidFrom.OptionsColumn.AllowFocus = false;
            this.colValidFrom.OptionsColumn.ReadOnly = true;
            this.colValidFrom.Visible = true;
            this.colValidFrom.VisibleIndex = 2;
            // 
            // colValidTo
            // 
            this.colValidTo.Caption = "Valid To";
            this.colValidTo.FieldName = "ValidTo";
            this.colValidTo.Name = "colValidTo";
            this.colValidTo.OptionsColumn.AllowEdit = false;
            this.colValidTo.OptionsColumn.AllowFocus = false;
            this.colValidTo.OptionsColumn.ReadOnly = true;
            this.colValidTo.Visible = true;
            this.colValidTo.VisibleIndex = 3;
            // 
            // colRate
            // 
            this.colRate.Caption = "Rate";
            this.colRate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colRate.FieldName = "Rate";
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowEdit = false;
            this.colRate.OptionsColumn.AllowFocus = false;
            this.colRate.OptionsColumn.ReadOnly = true;
            this.colRate.Visible = true;
            this.colRate.VisibleIndex = 1;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colJobCode
            // 
            this.colJobCode.Caption = "Job Code";
            this.colJobCode.FieldName = "JobCode";
            this.colJobCode.Name = "colJobCode";
            this.colJobCode.OptionsColumn.AllowEdit = false;
            this.colJobCode.OptionsColumn.AllowFocus = false;
            this.colJobCode.OptionsColumn.ReadOnly = true;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Width = 94;
            // 
            // colType
            // 
            this.colType.Caption = "Rate Type";
            this.colType.FieldName = "Type";
            this.colType.Name = "colType";
            this.colType.OptionsColumn.AllowEdit = false;
            this.colType.OptionsColumn.AllowFocus = false;
            this.colType.OptionsColumn.ReadOnly = true;
            // 
            // colUnits
            // 
            this.colUnits.Caption = "Units";
            this.colUnits.FieldName = "Units";
            this.colUnits.Name = "colUnits";
            this.colUnits.OptionsColumn.AllowEdit = false;
            this.colUnits.OptionsColumn.AllowFocus = false;
            this.colUnits.OptionsColumn.ReadOnly = true;
            // 
            // colRateTypeDescription
            // 
            this.colRateTypeDescription.Caption = "Rate Type Description";
            this.colRateTypeDescription.FieldName = "RateTypeDescription";
            this.colRateTypeDescription.Name = "colRateTypeDescription";
            this.colRateTypeDescription.OptionsColumn.AllowEdit = false;
            this.colRateTypeDescription.OptionsColumn.AllowFocus = false;
            this.colRateTypeDescription.OptionsColumn.ReadOnly = true;
            this.colRateTypeDescription.Width = 127;
            // 
            // colRateDesc
            // 
            this.colRateDesc.Caption = "Rate Description";
            this.colRateDesc.FieldName = "RateDesc";
            this.colRateDesc.Name = "colRateDesc";
            this.colRateDesc.OptionsColumn.AllowEdit = false;
            this.colRateDesc.OptionsColumn.AllowFocus = false;
            this.colRateDesc.OptionsColumn.ReadOnly = true;
            this.colRateDesc.Visible = true;
            this.colRateDesc.VisibleIndex = 0;
            this.colRateDesc.Width = 369;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(472, 502);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(553, 502);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp01392_AT_Job_Rates_For_ActionTableAdapter
            // 
            this.sp01392_AT_Job_Rates_For_ActionTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "GridHighlight";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(391, 195);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiGridHighlightColour, "", false, true, true, 131)});
            this.bar1.Text = "Grid Highlighting";
            // 
            // beiGridHighlightColour
            // 
            this.beiGridHighlightColour.Caption = "Best Match Rates:";
            this.beiGridHighlightColour.Edit = this.repositoryItemColorEdit1;
            this.beiGridHighlightColour.EditValue = "LightGreen";
            this.beiGridHighlightColour.EditWidth = 147;
            this.beiGridHighlightColour.Id = 30;
            this.beiGridHighlightColour.Name = "beiGridHighlightColour";
            this.beiGridHighlightColour.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Grid Highlight Colour - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I store the colour used as a background highlight colour for Best Match rates.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.beiGridHighlightColour.SuperTip = superToolTip1;
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "DropDown", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Delete", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Highlighting", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            this.repositoryItemColorEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemColorEdit1_ButtonClick);
            this.repositoryItemColorEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemColorEdit1_EditValueChanged);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Collapsed = true;
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 29);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Job Rates - Selected one";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Job Rate Parameters Linked to Selected Job Rate  [Read Only]";
            this.splitContainerControl1.Size = new System.Drawing.Size(633, 467);
            this.splitContainerControl1.SplitterPosition = 184;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(629, 437);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp00206JobRateManagerJobRateParametersBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(0, 0);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp00206JobRateManagerJobRateParametersBindingSource
            // 
            this.sp00206JobRateManagerJobRateParametersBindingSource.DataMember = "sp00206_Job_Rate_Manager_Job_Rate_Parameters";
            this.sp00206JobRateManagerJobRateParametersBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colstrJobcode,
            this.colstrJobdescription,
            this.colstrRateTypeDescription,
            this.colstrRateDesc,
            this.colmonRate,
            this.coldecRateUnits,
            this.colstrContractorName,
            this.colstrCriteriaDescription,
            this.colintCriteriaID,
            this.colintContractorID,
            this.colintParameterID,
            this.colintRateID,
            this.colintJobID,
            this.colintRateType});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrRateTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            // 
            // colstrJobcode
            // 
            this.colstrJobcode.Caption = "Job Code";
            this.colstrJobcode.FieldName = "strJobcode";
            this.colstrJobcode.Name = "colstrJobcode";
            this.colstrJobcode.OptionsColumn.AllowEdit = false;
            this.colstrJobcode.OptionsColumn.AllowFocus = false;
            this.colstrJobcode.OptionsColumn.ReadOnly = true;
            this.colstrJobcode.Width = 66;
            // 
            // colstrJobdescription
            // 
            this.colstrJobdescription.Caption = "Job Name";
            this.colstrJobdescription.FieldName = "strJobdescription";
            this.colstrJobdescription.Name = "colstrJobdescription";
            this.colstrJobdescription.OptionsColumn.AllowEdit = false;
            this.colstrJobdescription.OptionsColumn.AllowFocus = false;
            this.colstrJobdescription.OptionsColumn.ReadOnly = true;
            this.colstrJobdescription.Width = 248;
            // 
            // colstrRateTypeDescription
            // 
            this.colstrRateTypeDescription.Caption = "Rate Type";
            this.colstrRateTypeDescription.FieldName = "strRateTypeDescription";
            this.colstrRateTypeDescription.Name = "colstrRateTypeDescription";
            this.colstrRateTypeDescription.OptionsColumn.AllowEdit = false;
            this.colstrRateTypeDescription.OptionsColumn.AllowFocus = false;
            this.colstrRateTypeDescription.OptionsColumn.ReadOnly = true;
            this.colstrRateTypeDescription.Width = 106;
            // 
            // colstrRateDesc
            // 
            this.colstrRateDesc.Caption = "Rate Description";
            this.colstrRateDesc.FieldName = "strRateDesc";
            this.colstrRateDesc.Name = "colstrRateDesc";
            this.colstrRateDesc.OptionsColumn.AllowEdit = false;
            this.colstrRateDesc.OptionsColumn.AllowFocus = false;
            this.colstrRateDesc.OptionsColumn.ReadOnly = true;
            this.colstrRateDesc.Width = 272;
            // 
            // colmonRate
            // 
            this.colmonRate.Caption = "Job Rate";
            this.colmonRate.FieldName = "monRate";
            this.colmonRate.Name = "colmonRate";
            this.colmonRate.OptionsColumn.AllowEdit = false;
            this.colmonRate.OptionsColumn.AllowFocus = false;
            this.colmonRate.OptionsColumn.ReadOnly = true;
            this.colmonRate.Width = 65;
            // 
            // coldecRateUnits
            // 
            this.coldecRateUnits.Caption = "Applied To";
            this.coldecRateUnits.FieldName = "decRateUnits";
            this.coldecRateUnits.Name = "coldecRateUnits";
            this.coldecRateUnits.OptionsColumn.AllowEdit = false;
            this.coldecRateUnits.OptionsColumn.AllowFocus = false;
            this.coldecRateUnits.OptionsColumn.ReadOnly = true;
            this.coldecRateUnits.Width = 110;
            // 
            // colstrContractorName
            // 
            this.colstrContractorName.Caption = "Contractor";
            this.colstrContractorName.FieldName = "strContractorName";
            this.colstrContractorName.Name = "colstrContractorName";
            this.colstrContractorName.OptionsColumn.AllowEdit = false;
            this.colstrContractorName.OptionsColumn.AllowFocus = false;
            this.colstrContractorName.OptionsColumn.ReadOnly = true;
            this.colstrContractorName.Visible = true;
            this.colstrContractorName.VisibleIndex = 0;
            this.colstrContractorName.Width = 187;
            // 
            // colstrCriteriaDescription
            // 
            this.colstrCriteriaDescription.Caption = "Criteria";
            this.colstrCriteriaDescription.FieldName = "strCriteriaDescription";
            this.colstrCriteriaDescription.Name = "colstrCriteriaDescription";
            this.colstrCriteriaDescription.OptionsColumn.AllowEdit = false;
            this.colstrCriteriaDescription.OptionsColumn.AllowFocus = false;
            this.colstrCriteriaDescription.OptionsColumn.ReadOnly = true;
            this.colstrCriteriaDescription.Visible = true;
            this.colstrCriteriaDescription.VisibleIndex = 1;
            this.colstrCriteriaDescription.Width = 213;
            // 
            // colintCriteriaID
            // 
            this.colintCriteriaID.Caption = "Criteria ID";
            this.colintCriteriaID.FieldName = "intCriteriaID";
            this.colintCriteriaID.Name = "colintCriteriaID";
            this.colintCriteriaID.OptionsColumn.AllowEdit = false;
            this.colintCriteriaID.OptionsColumn.AllowFocus = false;
            this.colintCriteriaID.OptionsColumn.ReadOnly = true;
            this.colintCriteriaID.Width = 70;
            // 
            // colintContractorID
            // 
            this.colintContractorID.Caption = "Contractor ID";
            this.colintContractorID.FieldName = "intContractorID";
            this.colintContractorID.Name = "colintContractorID";
            this.colintContractorID.OptionsColumn.AllowEdit = false;
            this.colintContractorID.OptionsColumn.AllowFocus = false;
            this.colintContractorID.OptionsColumn.ReadOnly = true;
            this.colintContractorID.Width = 87;
            // 
            // colintParameterID
            // 
            this.colintParameterID.Caption = "Parameter ID";
            this.colintParameterID.FieldName = "intParameterID";
            this.colintParameterID.Name = "colintParameterID";
            this.colintParameterID.OptionsColumn.AllowEdit = false;
            this.colintParameterID.OptionsColumn.AllowFocus = false;
            this.colintParameterID.OptionsColumn.ReadOnly = true;
            this.colintParameterID.Width = 85;
            // 
            // colintRateID
            // 
            this.colintRateID.Caption = "Rate ID";
            this.colintRateID.FieldName = "intRateID";
            this.colintRateID.Name = "colintRateID";
            this.colintRateID.OptionsColumn.AllowEdit = false;
            this.colintRateID.OptionsColumn.AllowFocus = false;
            this.colintRateID.OptionsColumn.ReadOnly = true;
            this.colintRateID.Width = 58;
            // 
            // colintJobID
            // 
            this.colintJobID.Caption = "Job ID";
            this.colintJobID.FieldName = "intJobID";
            this.colintJobID.Name = "colintJobID";
            this.colintJobID.OptionsColumn.AllowEdit = false;
            this.colintJobID.OptionsColumn.AllowFocus = false;
            this.colintJobID.OptionsColumn.ReadOnly = true;
            this.colintJobID.Width = 52;
            // 
            // colintRateType
            // 
            this.colintRateType.Caption = "Rate Type ID";
            this.colintRateType.FieldName = "intRateType";
            this.colintRateType.Name = "colintRateType";
            this.colintRateType.OptionsColumn.AllowEdit = false;
            this.colintRateType.OptionsColumn.AllowFocus = false;
            this.colintRateType.OptionsColumn.ReadOnly = true;
            this.colintRateType.Width = 85;
            // 
            // sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter
            // 
            this.sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_AT_Action_Edit_Select_Job_Rate
            // 
            this.AcceptButton = this.btnOK;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(633, 531);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_AT_Action_Edit_Select_Job_Rate";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Job Rate";
            this.Load += new System.EventHandler(this.frm_AT_Action_Edit_Select_Job_Rate_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01392ATJobRatesForActionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00206JobRateManagerJobRateParametersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp01392ATJobRatesForActionBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01392_AT_Job_Rates_For_ActionTableAdapter sp01392_AT_Job_Rates_For_ActionTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRateID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colValidFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colValidTo;
        private DevExpress.XtraGrid.Columns.GridColumn colRate;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colRateTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRateDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colMatch;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem beiGridHighlightColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource sp00206JobRateManagerJobRateParametersBindingSource;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobcode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobdescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRateTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRateDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colmonRate;
        private DevExpress.XtraGrid.Columns.GridColumn coldecRateUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colstrContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colstrCriteriaDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colintCriteriaID;
        private DevExpress.XtraGrid.Columns.GridColumn colintContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colintParameterID;
        private DevExpress.XtraGrid.Columns.GridColumn colintRateID;
        private DevExpress.XtraGrid.Columns.GridColumn colintJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colintRateType;
        private WoodPlan5.DataSet_ATTableAdapters.sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

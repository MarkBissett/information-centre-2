namespace WoodPlan5
{
    partial class frm_AT_Data_Transfer_GBM_Mobile_Template_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Data_Transfer_GBM_Mobile_Template_Edit));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.templateDescriptionMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.sp00113exporttoGBMsavedtemplateeditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataTransfer = new WoodPlan5.DataSet_AT_DataTransfer();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.TemplateDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTemplateDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00113_export_to_GBM_saved_template_editTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00113_export_to_GBM_saved_template_editTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.templateDescriptionMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00113exporttoGBMsavedtemplateeditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataTransfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemplateDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTemplateDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(617, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 334);
            this.barDockControlBottom.Size = new System.Drawing.Size(617, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 334);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(617, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 334);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.templateDescriptionMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.TemplateDescriptionTextEdit);
            this.dataLayoutControl1.DataSource = this.sp00113exporttoGBMsavedtemplateeditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.MenuManager = this.barManager1;
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1142, 134, 617, 477);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(617, 334);
            this.dataLayoutControl1.TabIndex = 21;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // templateDescriptionMemoEdit
            // 
            this.templateDescriptionMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00113exporttoGBMsavedtemplateeditBindingSource, "TemplateRemarks", true));
            this.templateDescriptionMemoEdit.Location = new System.Drawing.Point(119, 64);
            this.templateDescriptionMemoEdit.MenuManager = this.barManager1;
            this.templateDescriptionMemoEdit.Name = "templateDescriptionMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.templateDescriptionMemoEdit, true);
            this.templateDescriptionMemoEdit.Size = new System.Drawing.Size(486, 170);
            this.scSpellChecker.SetSpellCheckerOptions(this.templateDescriptionMemoEdit, optionsSpelling1);
            this.templateDescriptionMemoEdit.StyleController = this.dataLayoutControl1;
            this.templateDescriptionMemoEdit.TabIndex = 13;
            // 
            // sp00113exporttoGBMsavedtemplateeditBindingSource
            // 
            this.sp00113exporttoGBMsavedtemplateeditBindingSource.DataMember = "sp00113_export_to_GBM_saved_template_edit";
            this.sp00113exporttoGBMsavedtemplateeditBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // dataSet_AT_DataTransfer
            // 
            this.dataSet_AT_DataTransfer.DataSetName = "DataSet_AT_DataTransfer";
            this.dataSet_AT_DataTransfer.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Hint = "Cancel changes and close screen";
            this.dataNavigator1.Buttons.CancelEdit.ImageIndex = 1;
            this.dataNavigator1.Buttons.EndEdit.Hint = "Save changes and close screen";
            this.dataNavigator1.Buttons.EndEdit.ImageIndex = 0;
            this.dataNavigator1.Buttons.ImageList = this.imageCollection1;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00113exporttoGBMsavedtemplateeditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.ShowToolTips = true;
            this.dataNavigator1.Size = new System.Drawing.Size(145, 24);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 10;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.SaveAndClose_16x16, "SaveAndClose_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "SaveAndClose_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.close_16x16, "close_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "close_16x16");
            // 
            // TemplateDescriptionTextEdit
            // 
            this.TemplateDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00113exporttoGBMsavedtemplateeditBindingSource, "TemplateDescription", true));
            this.TemplateDescriptionTextEdit.Location = new System.Drawing.Point(119, 40);
            this.TemplateDescriptionTextEdit.MenuManager = this.barManager1;
            this.TemplateDescriptionTextEdit.Name = "TemplateDescriptionTextEdit";
            this.TemplateDescriptionTextEdit.Properties.NullValuePrompt = "Enter a name for the Template";
            this.scSpellChecker.SetShowSpellCheckMenu(this.TemplateDescriptionTextEdit, true);
            this.TemplateDescriptionTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TemplateDescriptionTextEdit, optionsSpelling2);
            this.TemplateDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.TemplateDescriptionTextEdit.TabIndex = 12;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.ItemForTemplateDescription,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup1.Size = new System.Drawing.Size(617, 334);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 226);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(597, 88);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTemplateDescription
            // 
            this.ItemForTemplateDescription.AllowHtmlStringInCaption = true;
            this.ItemForTemplateDescription.Control = this.TemplateDescriptionTextEdit;
            this.ItemForTemplateDescription.CustomizationFormText = "Template Description:";
            this.ItemForTemplateDescription.Location = new System.Drawing.Point(0, 28);
            this.ItemForTemplateDescription.Name = "ItemForTemplateDescription";
            this.ItemForTemplateDescription.Size = new System.Drawing.Size(597, 24);
            this.ItemForTemplateDescription.Text = "Template Description:";
            this.ItemForTemplateDescription.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AllowHtmlStringInCaption = true;
            this.layoutControlItem3.Control = this.templateDescriptionMemoEdit;
            this.layoutControlItem3.CustomizationFormText = "Template Description:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(597, 174);
            this.layoutControlItem3.Text = "Template Description:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(149, 28);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(149, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(448, 28);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00113_export_to_GBM_saved_template_editTableAdapter
            // 
            this.sp00113_export_to_GBM_saved_template_editTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AT_Data_Transfer_GBM_Mobile_Template_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(617, 334);
            this.ControlBox = false;
            this.Controls.Add(this.dataLayoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_AT_Data_Transfer_GBM_Mobile_Template_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Export Template";
            this.Load += new System.EventHandler(this.frm_AT_Data_Transfer_GBM_Mobile_Template_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.templateDescriptionMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00113exporttoGBMsavedtemplateeditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataTransfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemplateDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTemplateDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit TemplateDescriptionTextEdit;
        private System.Windows.Forms.BindingSource sp00113exporttoGBMsavedtemplateeditBindingSource;
        private DataSet_AT_DataTransfer dataSet_AT_DataTransfer;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTemplateDescription;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00113_export_to_GBM_saved_template_editTableAdapter sp00113_export_to_GBM_saved_template_editTableAdapter;
        private DevExpress.XtraEditors.MemoEdit templateDescriptionMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;

    }
}

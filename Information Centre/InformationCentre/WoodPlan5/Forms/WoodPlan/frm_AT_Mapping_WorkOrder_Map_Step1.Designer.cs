namespace WoodPlan5
{
    partial class frm_AT_Mapping_WorkOrder_Map_Step1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Mapping_WorkOrder_Map_Step1));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.colcolor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01297ATTreePickerWorkOrderMapAvailableWorkOrdersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuingBody = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedPreviousWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidents = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colWorkOrderUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnRefreshWorkOrderFilterList = new DevExpress.XtraEditors.SimpleButton();
            this.deFromDateFilter = new DevExpress.XtraEditors.DateEdit();
            this.deToDateFilter = new DevExpress.XtraEditors.DateEdit();
            this.ceIncludeCompletedFilter = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01296ATTreePickerWorkOrderMapListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnThumbNail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp01298ATTreePickerWorkOrderMapUnallocatedListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnThumbNail2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01296_AT_Tree_Picker_WorkOrder_Map_ListTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01296_AT_Tree_Picker_WorkOrder_Map_ListTableAdapter();
            this.sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrdersTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrdersTableAdapter();
            this.sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_ListTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_ListTableAdapter();
            this.bbiPreviewMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAllocate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUnallocate = new DevExpress.XtraBars.BarButtonItem();
            this.bsiMapTools = new DevExpress.XtraBars.BarSubItem();
            this.bbiRotateAntiClockwise = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRotateClockwise = new DevExpress.XtraBars.BarButtonItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01297ATTreePickerWorkOrderMapAvailableWorkOrdersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDateFilter.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDateFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDateFilter.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDateFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIncludeCompletedFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01296ATTreePickerWorkOrderMapListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01298ATTreePickerWorkOrderMapUnallocatedListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiMapTools, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPreviewMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAllocate),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUnallocate)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(787, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(787, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(787, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiPreviewMap,
            this.bbiAllocate,
            this.bbiUnallocate,
            this.bsiMapTools,
            this.bbiRotateAntiClockwise,
            this.bbiRotateClockwise});
            this.barManager1.MaxItemId = 31;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colcolor
            // 
            this.colcolor.Caption = "Colour";
            this.colcolor.FieldName = "color";
            this.colcolor.Name = "colcolor";
            this.colcolor.OptionsColumn.AllowEdit = false;
            this.colcolor.OptionsColumn.AllowFocus = false;
            this.colcolor.OptionsColumn.ReadOnly = true;
            this.colcolor.Width = 43;
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "No Work Order(s)";
            this.popupContainerEdit1.Location = new System.Drawing.Point(83, 7);
            this.popupContainerEdit1.MenuManager = this.barManager1;
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Refresh List - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click to refresh the list of saved Maps based upon the selected Work Orders.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "DropDown", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Refresh List", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "RefreshList", superToolTip3, DevExpress.Utils.ToolTipAnchor.Default)});
            this.popupContainerEdit1.Properties.CloseOnOuterMouseClick = false;
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControl1;
            this.popupContainerEdit1.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit1.Size = new System.Drawing.Size(697, 20);
            this.popupContainerEdit1.StyleController = this.layoutControl1;
            this.popupContainerEdit1.TabIndex = 4;
            this.popupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            this.popupContainerEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.popupContainerEdit1_ButtonClick);
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.layoutControl2);
            this.popupContainerControl1.Location = new System.Drawing.Point(36, 0);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(564, 454);
            this.popupContainerControl1.TabIndex = 6;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btnOK);
            this.layoutControl2.Controls.Add(this.gridSplitContainer2);
            this.layoutControl2.Controls.Add(this.btnRefreshWorkOrderFilterList);
            this.layoutControl2.Controls.Add(this.deFromDateFilter);
            this.layoutControl2.Controls.Add(this.deToDateFilter);
            this.layoutControl2.Controls.Add(this.ceIncludeCompletedFilter);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(564, 454);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(7, 425);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 22);
            this.btnOK.StyleController = this.layoutControl2;
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Grid = this.gridControl3;
            this.gridSplitContainer2.Location = new System.Drawing.Point(7, 71);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer2.Size = new System.Drawing.Size(550, 350);
            this.gridSplitContainer2.TabIndex = 10;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp01297ATTreePickerWorkOrderMapAvailableWorkOrdersBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemMemoExEdit3});
            this.gridControl3.Size = new System.Drawing.Size(550, 350);
            this.gridControl3.TabIndex = 8;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp01297ATTreePickerWorkOrderMapAvailableWorkOrdersBindingSource
            // 
            this.sp01297ATTreePickerWorkOrderMapAvailableWorkOrdersBindingSource.DataMember = "sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrders";
            this.sp01297ATTreePickerWorkOrderMapAvailableWorkOrdersBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderID2,
            this.colPaddedWorkOrderID,
            this.colContractor,
            this.colIssuingBody,
            this.colWorkOrderType,
            this.colIssuedBy,
            this.colIssueDate,
            this.colPriority,
            this.colPaddedPreviousWorkOrderID,
            this.colTotalCost,
            this.colWorkUnits,
            this.colCompletedDate,
            this.colcolor,
            this.colEmailDetails,
            this.Calculated1,
            this.Calculated2,
            this.Calculated3,
            this.colIncidents,
            this.colRemarks2,
            this.colWorkOrderUserDefined1,
            this.colWorkOrderUserDefined2,
            this.colWorkOrderUserDefined3});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colcolor;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "DarkGray";
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsPrint.AutoWidth = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colWorkOrderID2
            // 
            this.colWorkOrderID2.Caption = "Work Order ID";
            this.colWorkOrderID2.FieldName = "WorkOrderID";
            this.colWorkOrderID2.Name = "colWorkOrderID2";
            this.colWorkOrderID2.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID2.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID2.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID2.Width = 82;
            // 
            // colPaddedWorkOrderID
            // 
            this.colPaddedWorkOrderID.Caption = "Work Order Ref";
            this.colPaddedWorkOrderID.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID.Name = "colPaddedWorkOrderID";
            this.colPaddedWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID.Visible = true;
            this.colPaddedWorkOrderID.VisibleIndex = 0;
            this.colPaddedWorkOrderID.Width = 111;
            // 
            // colContractor
            // 
            this.colContractor.Caption = "Contractor";
            this.colContractor.FieldName = "Contractor";
            this.colContractor.Name = "colContractor";
            this.colContractor.OptionsColumn.AllowEdit = false;
            this.colContractor.OptionsColumn.AllowFocus = false;
            this.colContractor.OptionsColumn.ReadOnly = true;
            this.colContractor.Visible = true;
            this.colContractor.VisibleIndex = 2;
            this.colContractor.Width = 121;
            // 
            // colIssuingBody
            // 
            this.colIssuingBody.Caption = "Issuing Body";
            this.colIssuingBody.FieldName = "IssuingBody";
            this.colIssuingBody.Name = "colIssuingBody";
            this.colIssuingBody.OptionsColumn.AllowEdit = false;
            this.colIssuingBody.OptionsColumn.AllowFocus = false;
            this.colIssuingBody.OptionsColumn.ReadOnly = true;
            this.colIssuingBody.Visible = true;
            this.colIssuingBody.VisibleIndex = 3;
            this.colIssuingBody.Width = 139;
            // 
            // colWorkOrderType
            // 
            this.colWorkOrderType.Caption = "Type";
            this.colWorkOrderType.FieldName = "WorkOrderType";
            this.colWorkOrderType.Name = "colWorkOrderType";
            this.colWorkOrderType.OptionsColumn.AllowEdit = false;
            this.colWorkOrderType.OptionsColumn.AllowFocus = false;
            this.colWorkOrderType.OptionsColumn.ReadOnly = true;
            this.colWorkOrderType.Visible = true;
            this.colWorkOrderType.VisibleIndex = 1;
            this.colWorkOrderType.Width = 93;
            // 
            // colIssuedBy
            // 
            this.colIssuedBy.Caption = "Issued By";
            this.colIssuedBy.FieldName = "IssuedBy";
            this.colIssuedBy.Name = "colIssuedBy";
            this.colIssuedBy.OptionsColumn.AllowEdit = false;
            this.colIssuedBy.OptionsColumn.AllowFocus = false;
            this.colIssuedBy.OptionsColumn.ReadOnly = true;
            this.colIssuedBy.Visible = true;
            this.colIssuedBy.VisibleIndex = 4;
            this.colIssuedBy.Width = 113;
            // 
            // colIssueDate
            // 
            this.colIssueDate.Caption = "Issue Date";
            this.colIssueDate.FieldName = "IssueDate";
            this.colIssueDate.Name = "colIssueDate";
            this.colIssueDate.OptionsColumn.AllowEdit = false;
            this.colIssueDate.OptionsColumn.AllowFocus = false;
            this.colIssueDate.Visible = true;
            this.colIssueDate.VisibleIndex = 5;
            this.colIssueDate.Width = 74;
            // 
            // colPriority
            // 
            this.colPriority.Caption = "Priority";
            this.colPriority.FieldName = "Priority";
            this.colPriority.Name = "colPriority";
            this.colPriority.OptionsColumn.AllowEdit = false;
            this.colPriority.OptionsColumn.AllowFocus = false;
            this.colPriority.OptionsColumn.ReadOnly = true;
            this.colPriority.Visible = true;
            this.colPriority.VisibleIndex = 6;
            this.colPriority.Width = 62;
            // 
            // colPaddedPreviousWorkOrderID
            // 
            this.colPaddedPreviousWorkOrderID.Caption = "Linked To";
            this.colPaddedPreviousWorkOrderID.FieldName = "PaddedPreviousWorkOrderID";
            this.colPaddedPreviousWorkOrderID.Name = "colPaddedPreviousWorkOrderID";
            this.colPaddedPreviousWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedPreviousWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedPreviousWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedPreviousWorkOrderID.Visible = true;
            this.colPaddedPreviousWorkOrderID.VisibleIndex = 7;
            this.colPaddedPreviousWorkOrderID.Width = 67;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.DisplayFormat.FormatString = "c2";
            this.colTotalCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 8;
            this.colTotalCost.Width = 71;
            // 
            // colWorkUnits
            // 
            this.colWorkUnits.Caption = "Work Units";
            this.colWorkUnits.DisplayFormat.FormatString = "#.00";
            this.colWorkUnits.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colWorkUnits.FieldName = "WorkUnits";
            this.colWorkUnits.Name = "colWorkUnits";
            this.colWorkUnits.OptionsColumn.AllowEdit = false;
            this.colWorkUnits.OptionsColumn.AllowFocus = false;
            this.colWorkUnits.OptionsColumn.ReadOnly = true;
            this.colWorkUnits.Visible = true;
            this.colWorkUnits.VisibleIndex = 9;
            this.colWorkUnits.Width = 74;
            // 
            // colCompletedDate
            // 
            this.colCompletedDate.Caption = "Completed Date";
            this.colCompletedDate.FieldName = "CompletedDate";
            this.colCompletedDate.Name = "colCompletedDate";
            this.colCompletedDate.OptionsColumn.AllowEdit = false;
            this.colCompletedDate.OptionsColumn.AllowFocus = false;
            this.colCompletedDate.Width = 89;
            // 
            // colEmailDetails
            // 
            this.colEmailDetails.Caption = "Email Details";
            this.colEmailDetails.FieldName = "EmailDetails";
            this.colEmailDetails.Name = "colEmailDetails";
            this.colEmailDetails.OptionsColumn.AllowEdit = false;
            this.colEmailDetails.OptionsColumn.AllowFocus = false;
            this.colEmailDetails.Width = 71;
            // 
            // Calculated1
            // 
            this.Calculated1.Caption = "<b>Calculated 1</b>";
            this.Calculated1.FieldName = "Calculated1";
            this.Calculated1.Name = "Calculated1";
            this.Calculated1.OptionsColumn.AllowEdit = false;
            this.Calculated1.OptionsColumn.AllowFocus = false;
            this.Calculated1.OptionsColumn.ReadOnly = true;
            this.Calculated1.ShowUnboundExpressionMenu = true;
            this.Calculated1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1.Visible = true;
            this.Calculated1.VisibleIndex = 15;
            this.Calculated1.Width = 90;
            // 
            // Calculated2
            // 
            this.Calculated2.Caption = "<b>Calculated 2</b>";
            this.Calculated2.FieldName = "Calculated2";
            this.Calculated2.Name = "Calculated2";
            this.Calculated2.OptionsColumn.AllowEdit = false;
            this.Calculated2.OptionsColumn.AllowFocus = false;
            this.Calculated2.OptionsColumn.ReadOnly = true;
            this.Calculated2.ShowUnboundExpressionMenu = true;
            this.Calculated2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2.Visible = true;
            this.Calculated2.VisibleIndex = 16;
            this.Calculated2.Width = 90;
            // 
            // Calculated3
            // 
            this.Calculated3.Caption = "<b>Calculated 3</b>";
            this.Calculated3.FieldName = "Calculated3";
            this.Calculated3.Name = "Calculated3";
            this.Calculated3.OptionsColumn.AllowEdit = false;
            this.Calculated3.OptionsColumn.AllowFocus = false;
            this.Calculated3.OptionsColumn.ReadOnly = true;
            this.Calculated3.ShowUnboundExpressionMenu = true;
            this.Calculated3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3.Visible = true;
            this.Calculated3.VisibleIndex = 17;
            this.Calculated3.Width = 90;
            // 
            // colIncidents
            // 
            this.colIncidents.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colIncidents.FieldName = "Incidents";
            this.colIncidents.Name = "colIncidents";
            this.colIncidents.OptionsColumn.ReadOnly = true;
            this.colIncidents.Visible = true;
            this.colIncidents.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colRemarks2
            // 
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colWorkOrderUserDefined1
            // 
            this.colWorkOrderUserDefined1.Caption = "User Defined 1";
            this.colWorkOrderUserDefined1.FieldName = "WorkOrderUserDefined1";
            this.colWorkOrderUserDefined1.Name = "colWorkOrderUserDefined1";
            this.colWorkOrderUserDefined1.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined1.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined1.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined1.Visible = true;
            this.colWorkOrderUserDefined1.VisibleIndex = 10;
            this.colWorkOrderUserDefined1.Width = 92;
            // 
            // colWorkOrderUserDefined2
            // 
            this.colWorkOrderUserDefined2.Caption = "User Defined 2";
            this.colWorkOrderUserDefined2.FieldName = "WorkOrderUserDefined2";
            this.colWorkOrderUserDefined2.Name = "colWorkOrderUserDefined2";
            this.colWorkOrderUserDefined2.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined2.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined2.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined2.Visible = true;
            this.colWorkOrderUserDefined2.VisibleIndex = 11;
            this.colWorkOrderUserDefined2.Width = 92;
            // 
            // colWorkOrderUserDefined3
            // 
            this.colWorkOrderUserDefined3.Caption = "User Defined 3";
            this.colWorkOrderUserDefined3.FieldName = "WorkOrderUserDefined3";
            this.colWorkOrderUserDefined3.Name = "colWorkOrderUserDefined3";
            this.colWorkOrderUserDefined3.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined3.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined3.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined3.Visible = true;
            this.colWorkOrderUserDefined3.VisibleIndex = 12;
            this.colWorkOrderUserDefined3.Width = 92;
            // 
            // btnRefreshWorkOrderFilterList
            // 
            this.btnRefreshWorkOrderFilterList.Location = new System.Drawing.Point(473, 37);
            this.btnRefreshWorkOrderFilterList.Name = "btnRefreshWorkOrderFilterList";
            this.btnRefreshWorkOrderFilterList.Size = new System.Drawing.Size(76, 22);
            this.btnRefreshWorkOrderFilterList.StyleController = this.layoutControl2;
            this.btnRefreshWorkOrderFilterList.TabIndex = 7;
            this.btnRefreshWorkOrderFilterList.Text = "Refresh";
            this.btnRefreshWorkOrderFilterList.Click += new System.EventHandler(this.btnRefreshWorkOrderFilterList_Click);
            // 
            // deFromDateFilter
            // 
            this.deFromDateFilter.EditValue = null;
            this.deFromDateFilter.Location = new System.Drawing.Point(48, 37);
            this.deFromDateFilter.MenuManager = this.barManager1;
            this.deFromDateFilter.Name = "deFromDateFilter";
            this.deFromDateFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFromDateFilter.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFromDateFilter.Properties.MaxValue = new System.DateTime(9999, 12, 31, 0, 0, 0, 0);
            this.deFromDateFilter.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.deFromDateFilter.Size = new System.Drawing.Size(106, 20);
            this.deFromDateFilter.StyleController = this.layoutControl2;
            this.deFromDateFilter.TabIndex = 4;
            // 
            // deToDateFilter
            // 
            this.deToDateFilter.EditValue = null;
            this.deToDateFilter.Location = new System.Drawing.Point(179, 37);
            this.deToDateFilter.MenuManager = this.barManager1;
            this.deToDateFilter.Name = "deToDateFilter";
            this.deToDateFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deToDateFilter.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deToDateFilter.Properties.MaxValue = new System.DateTime(9999, 12, 31, 0, 0, 0, 0);
            this.deToDateFilter.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.deToDateFilter.Size = new System.Drawing.Size(106, 20);
            this.deToDateFilter.StyleController = this.layoutControl2;
            this.deToDateFilter.TabIndex = 5;
            // 
            // ceIncludeCompletedFilter
            // 
            this.ceIncludeCompletedFilter.EditValue = 0;
            this.ceIncludeCompletedFilter.Location = new System.Drawing.Point(289, 37);
            this.ceIncludeCompletedFilter.MenuManager = this.barManager1;
            this.ceIncludeCompletedFilter.Name = "ceIncludeCompletedFilter";
            this.ceIncludeCompletedFilter.Properties.Caption = "Include Completed Work Orders";
            this.ceIncludeCompletedFilter.Properties.ValueChecked = 1;
            this.ceIncludeCompletedFilter.Properties.ValueUnchecked = 0;
            this.ceIncludeCompletedFilter.Size = new System.Drawing.Size(180, 19);
            this.ceIncludeCompletedFilter.StyleController = this.layoutControl2;
            this.ceIncludeCompletedFilter.TabIndex = 6;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(564, 454);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Filter Criteria";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(554, 64);
            this.layoutControlGroup3.Text = "Filter Criteria";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.deFromDateFilter;
            this.layoutControlItem2.CustomizationFormText = "From:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(143, 26);
            this.layoutControlItem2.Text = "From:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(28, 13);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.deToDateFilter;
            this.layoutControlItem3.CustomizationFormText = "To:";
            this.layoutControlItem3.Location = new System.Drawing.Point(143, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(131, 26);
            this.layoutControlItem3.Text = "To:";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(16, 13);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.ceIncludeCompletedFilter;
            this.layoutControlItem5.CustomizationFormText = "Include Completed Work Orders:";
            this.layoutControlItem5.Location = new System.Drawing.Point(274, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(184, 26);
            this.layoutControlItem5.Text = "Include Completed Work Orders:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnRefreshWorkOrderFilterList;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(458, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gridSplitContainer2;
            this.layoutControlItem7.CustomizationFormText = "WorkOrder Grid:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 64);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(554, 354);
            this.layoutControlItem7.Text = "WorkOrder Grid:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnOK;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 418);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(79, 26);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(79, 418);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(475, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.splitContainerControl1);
            this.layoutControl1.Controls.Add(this.popupContainerEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(787, 537);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(7, 31);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Maps Linked to Selected Work Orders";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer3);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Unallocated Maps  [No Work Order Linked]";
            this.splitContainerControl1.Size = new System.Drawing.Size(773, 499);
            this.splitContainerControl1.SplitterPosition = 283;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(769, 259);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.DataSource = this.sp01296ATTreePickerWorkOrderMapListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemPictureEdit1});
            this.gridControl1.Size = new System.Drawing.Size(769, 259);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.gridControl1_Paint);
            this.gridControl1.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControl1_DragDrop);
            this.gridControl1.DragOver += new System.Windows.Forms.DragEventHandler(this.gridControl1_DragOver);
            this.gridControl1.DragLeave += new System.EventHandler(this.gridControl1_DragLeave);
            this.gridControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridControl1_MouseMove);
            // 
            // sp01296ATTreePickerWorkOrderMapListBindingSource
            // 
            this.sp01296ATTreePickerWorkOrderMapListBindingSource.DataMember = "sp01296_AT_Tree_Picker_WorkOrder_Map_List";
            this.sp01296ATTreePickerWorkOrderMapListBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderMapID,
            this.colWorkOrderID,
            this.colMapDescription,
            this.colRemarks,
            this.colCreatedByID,
            this.colDateCreated,
            this.colMapOrder,
            this.colWorkOrderNumber,
            this.colCreatedByName,
            this.gridColumnThumbNail});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colWorkOrderNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMapOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseMove);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colWorkOrderMapID
            // 
            this.colWorkOrderMapID.Caption = "Map ID";
            this.colWorkOrderMapID.FieldName = "WorkOrderMapID";
            this.colWorkOrderMapID.Name = "colWorkOrderMapID";
            this.colWorkOrderMapID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderMapID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderMapID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderMapID.Width = 46;
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order ID";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.Width = 82;
            // 
            // colMapDescription
            // 
            this.colMapDescription.Caption = "Map Description";
            this.colMapDescription.FieldName = "MapDescription";
            this.colMapDescription.Name = "colMapDescription";
            this.colMapDescription.OptionsColumn.AllowEdit = false;
            this.colMapDescription.OptionsColumn.AllowFocus = false;
            this.colMapDescription.OptionsColumn.ReadOnly = true;
            this.colMapDescription.Visible = true;
            this.colMapDescription.VisibleIndex = 0;
            this.colMapDescription.Width = 392;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            this.colRemarks.Width = 63;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colCreatedByID
            // 
            this.colCreatedByID.Caption = "Created By ID";
            this.colCreatedByID.FieldName = "CreatedByID";
            this.colCreatedByID.Name = "colCreatedByID";
            this.colCreatedByID.OptionsColumn.AllowEdit = false;
            this.colCreatedByID.OptionsColumn.AllowFocus = false;
            this.colCreatedByID.OptionsColumn.ReadOnly = true;
            this.colCreatedByID.Width = 80;
            // 
            // colDateCreated
            // 
            this.colDateCreated.Caption = "Date Created";
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 2;
            this.colDateCreated.Width = 87;
            // 
            // colMapOrder
            // 
            this.colMapOrder.Caption = "Order";
            this.colMapOrder.FieldName = "MapOrder";
            this.colMapOrder.Name = "colMapOrder";
            this.colMapOrder.OptionsColumn.AllowEdit = false;
            this.colMapOrder.OptionsColumn.AllowFocus = false;
            this.colMapOrder.OptionsColumn.ReadOnly = true;
            this.colMapOrder.Width = 63;
            // 
            // colWorkOrderNumber
            // 
            this.colWorkOrderNumber.Caption = "Work Order";
            this.colWorkOrderNumber.FieldName = "WorkOrderNumber";
            this.colWorkOrderNumber.Name = "colWorkOrderNumber";
            this.colWorkOrderNumber.OptionsColumn.AllowEdit = false;
            this.colWorkOrderNumber.OptionsColumn.AllowFocus = false;
            this.colWorkOrderNumber.OptionsColumn.ReadOnly = true;
            this.colWorkOrderNumber.Width = 78;
            // 
            // colCreatedByName
            // 
            this.colCreatedByName.Caption = "Created By";
            this.colCreatedByName.FieldName = "CreatedByName";
            this.colCreatedByName.Name = "colCreatedByName";
            this.colCreatedByName.OptionsColumn.AllowEdit = false;
            this.colCreatedByName.OptionsColumn.AllowFocus = false;
            this.colCreatedByName.OptionsColumn.ReadOnly = true;
            this.colCreatedByName.Visible = true;
            this.colCreatedByName.VisibleIndex = 1;
            this.colCreatedByName.Width = 136;
            // 
            // gridColumnThumbNail
            // 
            this.gridColumnThumbNail.Caption = "Preview";
            this.gridColumnThumbNail.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumnThumbNail.FieldName = "gridColumnThumbNail";
            this.gridColumnThumbNail.Name = "gridColumnThumbNail";
            this.gridColumnThumbNail.OptionsColumn.AllowEdit = false;
            this.gridColumnThumbNail.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnThumbNail.OptionsColumn.AllowSize = false;
            this.gridColumnThumbNail.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnThumbNail.OptionsColumn.FixedWidth = true;
            this.gridColumnThumbNail.OptionsColumn.ReadOnly = true;
            this.gridColumnThumbNail.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnThumbNail.OptionsFilter.AllowFilter = false;
            this.gridColumnThumbNail.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnThumbNail.Visible = true;
            this.gridColumnThumbNail.VisibleIndex = 4;
            this.gridColumnThumbNail.Width = 49;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl2;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer3.Size = new System.Drawing.Size(769, 186);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp01298ATTreePickerWorkOrderMapUnallocatedListBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemPictureEdit2});
            this.gridControl2.Size = new System.Drawing.Size(769, 186);
            this.gridControl2.TabIndex = 6;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gridControl2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridControl2_MouseMove);
            // 
            // sp01298ATTreePickerWorkOrderMapUnallocatedListBindingSource
            // 
            this.sp01298ATTreePickerWorkOrderMapUnallocatedListBindingSource.DataMember = "sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_List";
            this.sp01298ATTreePickerWorkOrderMapUnallocatedListBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumnThumbNail2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.RowAutoHeight = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView2_CustomUnboundColumnData);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseDown);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Map ID";
            this.gridColumn1.FieldName = "WorkOrderMapID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 46;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Work Order ID";
            this.gridColumn2.FieldName = "WorkOrderID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 82;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Map Description";
            this.gridColumn3.FieldName = "MapDescription";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 392;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Remarks";
            this.gridColumn4.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn4.FieldName = "Remarks";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 63;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Created By ID";
            this.gridColumn5.FieldName = "CreatedByID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 80;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Date Created";
            this.gridColumn6.FieldName = "DateCreated";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 87;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Order";
            this.gridColumn7.FieldName = "MapOrder";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 63;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Work Order";
            this.gridColumn8.FieldName = "WorkOrderNumber";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 78;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Created By";
            this.gridColumn9.FieldName = "CreatedByName";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 136;
            // 
            // gridColumnThumbNail2
            // 
            this.gridColumnThumbNail2.Caption = "Preview";
            this.gridColumnThumbNail2.ColumnEdit = this.repositoryItemPictureEdit2;
            this.gridColumnThumbNail2.FieldName = "gridColumnThumbNail";
            this.gridColumnThumbNail2.Name = "gridColumnThumbNail2";
            this.gridColumnThumbNail2.OptionsColumn.AllowEdit = false;
            this.gridColumnThumbNail2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnThumbNail2.OptionsColumn.AllowSize = false;
            this.gridColumnThumbNail2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnThumbNail2.OptionsColumn.FixedWidth = true;
            this.gridColumnThumbNail2.OptionsColumn.ReadOnly = true;
            this.gridColumnThumbNail2.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnThumbNail2.OptionsFilter.AllowFilter = false;
            this.gridColumnThumbNail2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnThumbNail2.Visible = true;
            this.gridColumnThumbNail2.VisibleIndex = 4;
            this.gridColumnThumbNail2.Width = 49;
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            this.repositoryItemPictureEdit2.ShowMenu = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem4});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(787, 537);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.popupContainerEdit1;
            this.layoutControlItem1.CustomizationFormText = "Work Order(s):";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(777, 24);
            this.layoutControlItem1.Text = "Work Order(s):";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.splitContainerControl1;
            this.layoutControlItem4.CustomizationFormText = "Grid Split Container:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(777, 503);
            this.layoutControlItem4.Text = "Grid Split Container:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // sp01296_AT_Tree_Picker_WorkOrder_Map_ListTableAdapter
            // 
            this.sp01296_AT_Tree_Picker_WorkOrder_Map_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrdersTableAdapter
            // 
            this.sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrdersTableAdapter.ClearBeforeFill = true;
            // 
            // sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_ListTableAdapter
            // 
            this.sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_ListTableAdapter.ClearBeforeFill = true;
            // 
            // bbiPreviewMap
            // 
            this.bbiPreviewMap.Caption = "Preview Map...";
            this.bbiPreviewMap.Id = 25;
            this.bbiPreviewMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPreviewMap.ImageOptions.Image")));
            this.bbiPreviewMap.Name = "bbiPreviewMap";
            this.bbiPreviewMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPreviewMap_ItemClick);
            // 
            // bbiAllocate
            // 
            this.bbiAllocate.Caption = "Allocate Unallocated Map...";
            this.bbiAllocate.Enabled = false;
            this.bbiAllocate.Id = 26;
            this.bbiAllocate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAllocate.ImageOptions.Image")));
            this.bbiAllocate.Name = "bbiAllocate";
            this.bbiAllocate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAllocate_ItemClick);
            // 
            // bbiUnallocate
            // 
            this.bbiUnallocate.Caption = "De-allocate Allocated Map...";
            this.bbiUnallocate.Enabled = false;
            this.bbiUnallocate.Id = 27;
            this.bbiUnallocate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUnallocate.ImageOptions.Image")));
            this.bbiUnallocate.Name = "bbiUnallocate";
            this.bbiUnallocate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUnallocate_ItemClick);
            // 
            // bsiMapTools
            // 
            this.bsiMapTools.Caption = "Map Tools";
            this.bsiMapTools.Id = 28;
            this.bsiMapTools.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiMapTools.ImageOptions.Image")));
            this.bsiMapTools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateAntiClockwise),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateClockwise)});
            this.bsiMapTools.Name = "bsiMapTools";
            // 
            // bbiRotateAntiClockwise
            // 
            this.bbiRotateAntiClockwise.Caption = "Rotate Map Anti-clockwise";
            this.bbiRotateAntiClockwise.Id = 29;
            this.bbiRotateAntiClockwise.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRotateAntiClockwise.ImageOptions.Image")));
            this.bbiRotateAntiClockwise.Name = "bbiRotateAntiClockwise";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Rotate Map Anti-clockwise";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to rotate map 90 degress anti-clockwise. \r\nRepeat to keep rotating anti-" +
    "clockwise.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiRotateAntiClockwise.SuperTip = superToolTip1;
            this.bbiRotateAntiClockwise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAntiClockwise_ItemClick);
            // 
            // bbiRotateClockwise
            // 
            this.bbiRotateClockwise.Caption = "Rotate Map Clockwise";
            this.bbiRotateClockwise.Id = 30;
            this.bbiRotateClockwise.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRotateClockwise.ImageOptions.Image")));
            this.bbiRotateClockwise.Name = "bbiRotateClockwise";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Rotate Map Clockwise";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to rotate map 90 degress clockwise. \r\nRepeat to keep rotating clockwise." +
    "";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiRotateClockwise.SuperTip = superToolTip2;
            this.bbiRotateClockwise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateClockwise_ItemClick);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // frm_AT_Mapping_WorkOrder_Map_Step1
            // 
            this.ClientSize = new System.Drawing.Size(787, 537);
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_AT_Mapping_WorkOrder_Map_Step1";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Work Order Mapping";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Mapping_WorkOrder_Map_Step1_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Mapping_WorkOrder_Map_Step1_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01297ATTreePickerWorkOrderMapAvailableWorkOrdersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDateFilter.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDateFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDateFilter.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDateFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIncludeCompletedFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01296ATTreePickerWorkOrderMapListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01298ATTreePickerWorkOrderMapUnallocatedListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.BindingSource sp01296ATTreePickerWorkOrderMapListBindingSource;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colMapDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colMapOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByName;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01296_AT_Tree_Picker_WorkOrder_Map_ListTableAdapter sp01296_AT_Tree_Picker_WorkOrder_Map_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnThumbNail;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.DateEdit deFromDateFilter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnRefreshWorkOrderFilterList;
        private DevExpress.XtraEditors.CheckEdit ceIncludeCompletedFilter;
        private DevExpress.XtraEditors.DateEdit deToDateFilter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private System.Windows.Forms.BindingSource sp01297ATTreePickerWorkOrderMapAvailableWorkOrdersBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuingBody;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderType;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedPreviousWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colcolor;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailDetails;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidents;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined3;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrdersTableAdapter sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrdersTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID2;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private System.Windows.Forms.BindingSource sp01298ATTreePickerWorkOrderMapUnallocatedListBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_ListTableAdapter sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnThumbNail2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraBars.BarButtonItem bbiPreviewMap;
        private DevExpress.XtraBars.BarButtonItem bbiAllocate;
        private DevExpress.XtraBars.BarButtonItem bbiUnallocate;
        private DevExpress.XtraBars.BarSubItem bsiMapTools;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAntiClockwise;
        private DevExpress.XtraBars.BarButtonItem bbiRotateClockwise;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
    }
}

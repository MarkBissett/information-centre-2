namespace WoodPlan5
{
    partial class frm_AT_Budget_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Budget_Edit));
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.sp01355ATBudgetItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.intBudgetIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strBudgetDescTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strBudgetCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dtStartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.dtEndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.monStartingAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.monSpentBudgetedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.monSpentActualTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.monRemainingBudgetedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.monRemainingActualTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForintBudgetID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrBudgetDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemFormonStartingAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFormonSpentBudgeted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFormonRemainingBudgeted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemFormonSpentActual = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFormonRemainingActual = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrBudgetCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01355_AT_Budget_ItemTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01355_AT_Budget_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp01355ATBudgetItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intBudgetIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strBudgetDescTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strBudgetCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monStartingAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monSpentBudgetedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monSpentActualTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monRemainingBudgetedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monRemainingActualTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintBudgetID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrBudgetDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonStartingAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonSpentBudgeted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonRemainingBudgeted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonSpentActual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonRemainingActual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrBudgetCode)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intBudgetIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strBudgetDescTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strBudgetCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dtStartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.dtEndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.monStartingAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.monSpentBudgetedTextEdit);
            this.dataLayoutControl1.Controls.Add(this.monSpentActualTextEdit);
            this.dataLayoutControl1.Controls.Add(this.monRemainingBudgetedTextEdit);
            this.dataLayoutControl1.Controls.Add(this.monRemainingActualTextEdit);
            this.dataLayoutControl1.DataSource = this.sp01355ATBudgetItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintBudgetID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp01355ATBudgetItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(119, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(191, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 17;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // sp01355ATBudgetItemBindingSource
            // 
            this.sp01355ATBudgetItemBindingSource.DataMember = "sp01355_AT_Budget_Item";
            this.sp01355ATBudgetItemBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // intBudgetIDTextEdit
            // 
            this.intBudgetIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "intBudgetID", true));
            this.intBudgetIDTextEdit.Location = new System.Drawing.Point(118, 35);
            this.intBudgetIDTextEdit.MenuManager = this.barManager1;
            this.intBudgetIDTextEdit.Name = "intBudgetIDTextEdit";
            this.intBudgetIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intBudgetIDTextEdit, true);
            this.intBudgetIDTextEdit.Size = new System.Drawing.Size(481, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intBudgetIDTextEdit, optionsSpelling1);
            this.intBudgetIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intBudgetIDTextEdit.TabIndex = 4;
            // 
            // strBudgetDescTextEdit
            // 
            this.strBudgetDescTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "strBudgetDesc", true));
            this.strBudgetDescTextEdit.Location = new System.Drawing.Point(117, 59);
            this.strBudgetDescTextEdit.MenuManager = this.barManager1;
            this.strBudgetDescTextEdit.Name = "strBudgetDescTextEdit";
            this.strBudgetDescTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strBudgetDescTextEdit, true);
            this.strBudgetDescTextEdit.Size = new System.Drawing.Size(499, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strBudgetDescTextEdit, optionsSpelling2);
            this.strBudgetDescTextEdit.StyleController = this.dataLayoutControl1;
            this.strBudgetDescTextEdit.TabIndex = 5;
            this.strBudgetDescTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strBudgetDescTextEdit_Validating);
            // 
            // strBudgetCodeTextEdit
            // 
            this.strBudgetCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "strBudgetCode", true));
            this.strBudgetCodeTextEdit.Location = new System.Drawing.Point(117, 35);
            this.strBudgetCodeTextEdit.MenuManager = this.barManager1;
            this.strBudgetCodeTextEdit.Name = "strBudgetCodeTextEdit";
            this.strBudgetCodeTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strBudgetCodeTextEdit, true);
            this.strBudgetCodeTextEdit.Size = new System.Drawing.Size(499, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strBudgetCodeTextEdit, optionsSpelling3);
            this.strBudgetCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strBudgetCodeTextEdit.TabIndex = 6;
            // 
            // dtStartDateDateEdit
            // 
            this.dtStartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "dtStartDate", true));
            this.dtStartDateDateEdit.EditValue = null;
            this.dtStartDateDateEdit.Location = new System.Drawing.Point(141, 163);
            this.dtStartDateDateEdit.MenuManager = this.barManager1;
            this.dtStartDateDateEdit.Name = "dtStartDateDateEdit";
            this.dtStartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Clear Date", null, null, true)});
            this.dtStartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtStartDateDateEdit.Size = new System.Drawing.Size(132, 20);
            this.dtStartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtStartDateDateEdit.TabIndex = 7;
            this.dtStartDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtStartDateDateEdit_ButtonClick);
            // 
            // dtEndDateDateEdit
            // 
            this.dtEndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "dtEndDate", true));
            this.dtEndDateDateEdit.EditValue = null;
            this.dtEndDateDateEdit.Location = new System.Drawing.Point(141, 187);
            this.dtEndDateDateEdit.MenuManager = this.barManager1;
            this.dtEndDateDateEdit.Name = "dtEndDateDateEdit";
            this.dtEndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Date", null, null, true)});
            this.dtEndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtEndDateDateEdit.Size = new System.Drawing.Size(132, 20);
            this.dtEndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtEndDateDateEdit.TabIndex = 8;
            this.dtEndDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtEndDateDateEdit_ButtonClick);
            // 
            // monStartingAmountSpinEdit
            // 
            this.monStartingAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "monStartingAmount", true));
            this.monStartingAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.monStartingAmountSpinEdit.Location = new System.Drawing.Point(141, 221);
            this.monStartingAmountSpinEdit.MenuManager = this.barManager1;
            this.monStartingAmountSpinEdit.Name = "monStartingAmountSpinEdit";
            this.monStartingAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.monStartingAmountSpinEdit.Properties.Mask.EditMask = "c";
            this.monStartingAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monStartingAmountSpinEdit.Size = new System.Drawing.Size(132, 20);
            this.monStartingAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.monStartingAmountSpinEdit.TabIndex = 9;
            this.monStartingAmountSpinEdit.EditValueChanged += new System.EventHandler(this.monStartingAmountSpinEdit_EditValueChanged);
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 163);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(556, 207);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling4);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 10;
            // 
            // monSpentBudgetedTextEdit
            // 
            this.monSpentBudgetedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "monSpentBudgeted", true));
            this.monSpentBudgetedTextEdit.Location = new System.Drawing.Point(141, 245);
            this.monSpentBudgetedTextEdit.MenuManager = this.barManager1;
            this.monSpentBudgetedTextEdit.Name = "monSpentBudgetedTextEdit";
            this.monSpentBudgetedTextEdit.Properties.Mask.EditMask = "c";
            this.monSpentBudgetedTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.monSpentBudgetedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monSpentBudgetedTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.monSpentBudgetedTextEdit, true);
            this.monSpentBudgetedTextEdit.Size = new System.Drawing.Size(132, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.monSpentBudgetedTextEdit, optionsSpelling5);
            this.monSpentBudgetedTextEdit.StyleController = this.dataLayoutControl1;
            this.monSpentBudgetedTextEdit.TabIndex = 13;
            // 
            // monSpentActualTextEdit
            // 
            this.monSpentActualTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "monSpentActual", true));
            this.monSpentActualTextEdit.Location = new System.Drawing.Point(141, 303);
            this.monSpentActualTextEdit.MenuManager = this.barManager1;
            this.monSpentActualTextEdit.Name = "monSpentActualTextEdit";
            this.monSpentActualTextEdit.Properties.Mask.EditMask = "c";
            this.monSpentActualTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.monSpentActualTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monSpentActualTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.monSpentActualTextEdit, true);
            this.monSpentActualTextEdit.Size = new System.Drawing.Size(132, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.monSpentActualTextEdit, optionsSpelling6);
            this.monSpentActualTextEdit.StyleController = this.dataLayoutControl1;
            this.monSpentActualTextEdit.TabIndex = 14;
            // 
            // monRemainingBudgetedTextEdit
            // 
            this.monRemainingBudgetedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "monRemainingBudgeted", true));
            this.monRemainingBudgetedTextEdit.Location = new System.Drawing.Point(141, 269);
            this.monRemainingBudgetedTextEdit.MenuManager = this.barManager1;
            this.monRemainingBudgetedTextEdit.Name = "monRemainingBudgetedTextEdit";
            this.monRemainingBudgetedTextEdit.Properties.Mask.EditMask = "c";
            this.monRemainingBudgetedTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.monRemainingBudgetedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monRemainingBudgetedTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.monRemainingBudgetedTextEdit, true);
            this.monRemainingBudgetedTextEdit.Size = new System.Drawing.Size(132, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.monRemainingBudgetedTextEdit, optionsSpelling7);
            this.monRemainingBudgetedTextEdit.StyleController = this.dataLayoutControl1;
            this.monRemainingBudgetedTextEdit.TabIndex = 15;
            // 
            // monRemainingActualTextEdit
            // 
            this.monRemainingActualTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01355ATBudgetItemBindingSource, "monRemainingActual", true));
            this.monRemainingActualTextEdit.Location = new System.Drawing.Point(141, 327);
            this.monRemainingActualTextEdit.MenuManager = this.barManager1;
            this.monRemainingActualTextEdit.Name = "monRemainingActualTextEdit";
            this.monRemainingActualTextEdit.Properties.Mask.EditMask = "c";
            this.monRemainingActualTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.monRemainingActualTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monRemainingActualTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.monRemainingActualTextEdit, true);
            this.monRemainingActualTextEdit.Size = new System.Drawing.Size(132, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.monRemainingActualTextEdit, optionsSpelling8);
            this.monRemainingActualTextEdit.StyleController = this.dataLayoutControl1;
            this.monRemainingActualTextEdit.TabIndex = 16;
            // 
            // ItemForintBudgetID
            // 
            this.ItemForintBudgetID.Control = this.intBudgetIDTextEdit;
            this.ItemForintBudgetID.CustomizationFormText = "Budget ID:";
            this.ItemForintBudgetID.Location = new System.Drawing.Point(0, 23);
            this.ItemForintBudgetID.Name = "ItemForintBudgetID";
            this.ItemForintBudgetID.Size = new System.Drawing.Size(591, 24);
            this.ItemForintBudgetID.Text = "Budget ID:";
            this.ItemForintBudgetID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 481);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrBudgetDesc,
            this.emptySpaceItem1,
            this.layoutControlGroup3,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.emptySpaceItem2,
            this.ItemForstrBudgetCode});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 461);
            // 
            // ItemForstrBudgetDesc
            // 
            this.ItemForstrBudgetDesc.AllowHide = false;
            this.ItemForstrBudgetDesc.Control = this.strBudgetDescTextEdit;
            this.ItemForstrBudgetDesc.CustomizationFormText = "Budget Description:";
            this.ItemForstrBudgetDesc.Location = new System.Drawing.Point(0, 47);
            this.ItemForstrBudgetDesc.Name = "ItemForstrBudgetDesc";
            this.ItemForstrBudgetDesc.Size = new System.Drawing.Size(608, 24);
            this.ItemForstrBudgetDesc.Text = "Budget Description:";
            this.ItemForstrBudgetDesc.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 81);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(608, 305);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 259);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemFormonStartingAmount,
            this.ItemFormonSpentBudgeted,
            this.ItemFormonRemainingBudgeted,
            this.ItemFordtStartDate,
            this.ItemFordtEndDate,
            this.emptySpaceItem6,
            this.ItemFormonSpentActual,
            this.ItemFormonRemainingActual,
            this.emptySpaceItem3,
            this.emptySpaceItem7,
            this.emptySpaceItem8});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(560, 211);
            this.layoutControlGroup5.Text = "Details";
            // 
            // ItemFormonStartingAmount
            // 
            this.ItemFormonStartingAmount.AllowHide = false;
            this.ItemFormonStartingAmount.Control = this.monStartingAmountSpinEdit;
            this.ItemFormonStartingAmount.CustomizationFormText = "Starting Amount:";
            this.ItemFormonStartingAmount.Location = new System.Drawing.Point(0, 58);
            this.ItemFormonStartingAmount.MaxSize = new System.Drawing.Size(241, 24);
            this.ItemFormonStartingAmount.MinSize = new System.Drawing.Size(241, 24);
            this.ItemFormonStartingAmount.Name = "ItemFormonStartingAmount";
            this.ItemFormonStartingAmount.Size = new System.Drawing.Size(241, 24);
            this.ItemFormonStartingAmount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFormonStartingAmount.Text = "Starting Amount:";
            this.ItemFormonStartingAmount.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemFormonSpentBudgeted
            // 
            this.ItemFormonSpentBudgeted.Control = this.monSpentBudgetedTextEdit;
            this.ItemFormonSpentBudgeted.CustomizationFormText = "Budgeted Spent:";
            this.ItemFormonSpentBudgeted.Location = new System.Drawing.Point(0, 82);
            this.ItemFormonSpentBudgeted.MaxSize = new System.Drawing.Size(241, 24);
            this.ItemFormonSpentBudgeted.MinSize = new System.Drawing.Size(241, 24);
            this.ItemFormonSpentBudgeted.Name = "ItemFormonSpentBudgeted";
            this.ItemFormonSpentBudgeted.Size = new System.Drawing.Size(241, 24);
            this.ItemFormonSpentBudgeted.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFormonSpentBudgeted.Text = "Budgeted Spent:";
            this.ItemFormonSpentBudgeted.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemFormonRemainingBudgeted
            // 
            this.ItemFormonRemainingBudgeted.Control = this.monRemainingBudgetedTextEdit;
            this.ItemFormonRemainingBudgeted.CustomizationFormText = "Budgeted Remaining:";
            this.ItemFormonRemainingBudgeted.Location = new System.Drawing.Point(0, 106);
            this.ItemFormonRemainingBudgeted.MaxSize = new System.Drawing.Size(241, 24);
            this.ItemFormonRemainingBudgeted.MinSize = new System.Drawing.Size(241, 24);
            this.ItemFormonRemainingBudgeted.Name = "ItemFormonRemainingBudgeted";
            this.ItemFormonRemainingBudgeted.Size = new System.Drawing.Size(241, 24);
            this.ItemFormonRemainingBudgeted.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFormonRemainingBudgeted.Text = "Budgeted Remaining:";
            this.ItemFormonRemainingBudgeted.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemFordtStartDate
            // 
            this.ItemFordtStartDate.AllowHide = false;
            this.ItemFordtStartDate.Control = this.dtStartDateDateEdit;
            this.ItemFordtStartDate.CustomizationFormText = "Start Date:";
            this.ItemFordtStartDate.Location = new System.Drawing.Point(0, 0);
            this.ItemFordtStartDate.MaxSize = new System.Drawing.Size(241, 24);
            this.ItemFordtStartDate.MinSize = new System.Drawing.Size(241, 24);
            this.ItemFordtStartDate.Name = "ItemFordtStartDate";
            this.ItemFordtStartDate.Size = new System.Drawing.Size(241, 24);
            this.ItemFordtStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtStartDate.Text = "Start Date:";
            this.ItemFordtStartDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemFordtEndDate
            // 
            this.ItemFordtEndDate.AllowHide = false;
            this.ItemFordtEndDate.Control = this.dtEndDateDateEdit;
            this.ItemFordtEndDate.CustomizationFormText = "End Date:";
            this.ItemFordtEndDate.Location = new System.Drawing.Point(0, 24);
            this.ItemFordtEndDate.MaxSize = new System.Drawing.Size(241, 24);
            this.ItemFordtEndDate.MinSize = new System.Drawing.Size(241, 24);
            this.ItemFordtEndDate.Name = "ItemFordtEndDate";
            this.ItemFordtEndDate.Size = new System.Drawing.Size(241, 24);
            this.ItemFordtEndDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtEndDate.Text = "End Date:";
            this.ItemFordtEndDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(241, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(241, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(241, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemFormonSpentActual
            // 
            this.ItemFormonSpentActual.Control = this.monSpentActualTextEdit;
            this.ItemFormonSpentActual.CustomizationFormText = "Actual Spent:";
            this.ItemFormonSpentActual.Location = new System.Drawing.Point(0, 140);
            this.ItemFormonSpentActual.MaxSize = new System.Drawing.Size(241, 24);
            this.ItemFormonSpentActual.MinSize = new System.Drawing.Size(241, 24);
            this.ItemFormonSpentActual.Name = "ItemFormonSpentActual";
            this.ItemFormonSpentActual.Size = new System.Drawing.Size(241, 24);
            this.ItemFormonSpentActual.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFormonSpentActual.Text = "Actual Spent:";
            this.ItemFormonSpentActual.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemFormonRemainingActual
            // 
            this.ItemFormonRemainingActual.Control = this.monRemainingActualTextEdit;
            this.ItemFormonRemainingActual.CustomizationFormText = "Actual Remaining:";
            this.ItemFormonRemainingActual.Location = new System.Drawing.Point(0, 164);
            this.ItemFormonRemainingActual.MaxSize = new System.Drawing.Size(241, 24);
            this.ItemFormonRemainingActual.MinSize = new System.Drawing.Size(241, 24);
            this.ItemFormonRemainingActual.Name = "ItemFormonRemainingActual";
            this.ItemFormonRemainingActual.Size = new System.Drawing.Size(241, 24);
            this.ItemFormonRemainingActual.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFormonRemainingActual.Text = "Actual Remaining:";
            this.ItemFormonRemainingActual.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(241, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(319, 211);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 188);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(241, 23);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(241, 23);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(241, 23);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 130);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(241, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(560, 211);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.MaxSize = new System.Drawing.Size(0, 211);
            this.ItemForstrRemarks.MinSize = new System.Drawing.Size(120, 211);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(560, 211);
            this.ItemForstrRemarks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrRemarks.Text = "Remarks";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(107, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(107, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(107, 23);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(302, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(306, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(107, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(195, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 386);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 75);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrBudgetCode
            // 
            this.ItemForstrBudgetCode.AllowHide = false;
            this.ItemForstrBudgetCode.Control = this.strBudgetCodeTextEdit;
            this.ItemForstrBudgetCode.CustomizationFormText = "Budget Code:";
            this.ItemForstrBudgetCode.Location = new System.Drawing.Point(0, 23);
            this.ItemForstrBudgetCode.Name = "ItemForstrBudgetCode";
            this.ItemForstrBudgetCode.Size = new System.Drawing.Size(608, 24);
            this.ItemForstrBudgetCode.Text = "Budget Code:";
            this.ItemForstrBudgetCode.TextSize = new System.Drawing.Size(102, 13);
            // 
            // sp01355_AT_Budget_ItemTableAdapter
            // 
            this.sp01355_AT_Budget_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AT_Budget_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Budget_Edit";
            this.Text = "Edit Budget";
            this.Activated += new System.EventHandler(this.frm_AT_Budget_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Budget_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Budget_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp01355ATBudgetItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intBudgetIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strBudgetDescTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strBudgetCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monStartingAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monSpentBudgetedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monSpentActualTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monRemainingBudgetedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monRemainingActualTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintBudgetID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrBudgetDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonStartingAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonSpentBudgeted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonRemainingBudgeted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonSpentActual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonRemainingActual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrBudgetCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit intBudgetIDTextEdit;
        private System.Windows.Forms.BindingSource sp01355ATBudgetItemBindingSource;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.TextEdit strBudgetDescTextEdit;
        private DevExpress.XtraEditors.TextEdit strBudgetCodeTextEdit;
        private DevExpress.XtraEditors.DateEdit dtStartDateDateEdit;
        private DevExpress.XtraEditors.DateEdit dtEndDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit monStartingAmountSpinEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit monSpentBudgetedTextEdit;
        private DevExpress.XtraEditors.TextEdit monSpentActualTextEdit;
        private DevExpress.XtraEditors.TextEdit monRemainingBudgetedTextEdit;
        private DevExpress.XtraEditors.TextEdit monRemainingActualTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintBudgetID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrBudgetDesc;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrBudgetCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonStartingAmount;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonSpentBudgeted;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonSpentActual;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonRemainingBudgeted;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonRemainingActual;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01355_AT_Budget_ItemTableAdapter sp01355_AT_Budget_ItemTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

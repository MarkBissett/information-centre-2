using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_AT_Data_Transfer_GBM_Mobile_Rollback_Import : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        private bool isRunning = false;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public frm_AT_Data_Transfer_GBM_Mobile_Rollback_Import()
        {
            InitializeComponent();
        }

        private void frm_AT_Data_Transfer_GBM_Mobile_Rollback_Import_Load(object sender, EventArgs e)
        {
            strConnectionString = GlobalSettings.ConnectionString;

            sp00144_import_from_GBM_rollback_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00144_import_from_GBM_rollback_listTableAdapter.Fill(dataSet_AT_DataTransfer.sp00144_import_from_GBM_rollback_list);
        }
 

        private void btn_Ok_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more imports to rollback before proceeding.", "Rollback Import", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;
            
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Rolling Back Import...");
            fProgress.Show();
            Application.DoEvents();

            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter RollbackData = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            RollbackData.ChangeConnectionString(strConnectionString);

            int intImportID = 0;
            DateTime dtImportDateTime;
            DataRow dr = null;
            try
            {

                foreach (int intRowHandle in intRowHandles)
                {
                    dr = view.GetDataRow(intRowHandle);
                    intImportID = Convert.ToInt32(dr["ImportID"] ?? 0);
                    dtImportDateTime = (dr["ImportDateTime"] == DBNull.Value ? Convert.ToDateTime("01/01/1900 00:00:00.000") : Convert.ToDateTime(dr["ImportDateTime"]));

                    RollbackData.sp00141_import_from_GBM_merge_data_Rollback(dtImportDateTime);
                    RollbackData.sp00145_import_from_GBM_delete_import_set_from_log(intImportID);

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            catch (Exception Ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close(); // Close Progress Window //
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while rolling back the import data [" + Ex.Message + "]. Try running the process again.\n\nIf the problem persists, try restarting the application first.", "Rollback Import", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            view.BeginDataUpdate();
            sp00144_import_from_GBM_rollback_listTableAdapter.Fill(dataSet_AT_DataTransfer.sp00144_import_from_GBM_rollback_list);
            view.EndDataUpdate();

            if (fProgress != null)
            {
                fProgress.Close(); // Close Progress Window //
                fProgress = null;
            }
            DevExpress.XtraEditors.XtraMessageBox.Show("Import Rollback Completed successfully", "Rollback Import", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region GridView 1
        
        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No imports available for rollback");
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion

    }
}


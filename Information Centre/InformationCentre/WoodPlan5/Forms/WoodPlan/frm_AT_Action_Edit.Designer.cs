namespace WoodPlan5
{
    partial class frm_AT_Action_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Action_Edit));
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions14 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions15 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject57 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject58 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject59 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject60 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions16 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject61 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject62 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject63 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject64 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions17 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject65 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject66 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject67 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject68 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions18 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject69 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject70 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject71 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject72 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions19 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject73 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject74 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject75 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject76 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions20 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject77 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject78 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject79 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject80 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition7 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions21 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject81 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject82 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject83 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject84 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions22 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject85 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject86 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject87 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject88 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition8 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions23 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject89 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject90 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject91 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject92 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions24 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject93 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject94 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject95 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject96 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions25 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject97 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject98 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject99 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject100 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions26 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject101 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject102 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject103 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject104 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition9 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions27 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject105 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject106 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject107 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject108 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            this.colintJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiCopyDetails = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReload = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPasteAllValues = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPasteSelectedValues = new DevExpress.XtraBars.BarButtonItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bciShowPrevious = new DevExpress.XtraBars.BarCheckItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.gridControl39 = new DevExpress.XtraGrid.GridControl();
            this.sp02066ATActionPicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView39 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn169 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn170 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn171 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn172 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn173 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn174 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn175 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn176 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn177 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn178 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn179 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShortLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ActualJobRateDescTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp01388ATActionEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.intActionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intInspectionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dtDueDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.dtDoneDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.intWorkOrderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intPrevActionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strNearestHouseTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intJobRateBudgetedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intJobRateActualSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intSortOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intCostCentreIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateAddedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.GUIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.monJobRateBudgetedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.monJobRateActualSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.monBudgetedCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.monActualCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.monJobWorkUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.monDiscountRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.InspectionReferenceButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.strJobNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.intBudgetIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.JobRateDescButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.intOwnershipIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01362ATOwnershipListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOwnershipCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intActionGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00214MasterJobListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDefaultWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intActionByGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00190ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalCOntractorDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intSupervisorGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intPriorityIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList1GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intScheduleOfRatesCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.CostCentreCodeButtonEdit = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.dateEditDateTo = new DevExpress.XtraEditors.DateEdit();
            this.dateEditDateFrom = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01391ATCostCentresWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBudgetID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBudgetCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBudgetDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.WorkOrderDescriptionTextEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForintActionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintBudgetID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCostCentreID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateAdded = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGUID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintInspectionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintJobRateActual = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintPrevActionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintSortOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintWorkOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintJobRateBudgeted = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForInspectionReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtDoneDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintActionBy = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintSupervisor = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintAction = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrJobNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintScheduleOfRates = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemFormonJobRateBudgeted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFormonBudgetedCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobRateDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemFormonJobRateActual = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFormonActualCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFormonDiscountRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualJobRateDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintOwnershipID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCentreCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFormonJobWorkUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedPicturesGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrUser1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForintPriorityID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrNearestHouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkOrderDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp01388_AT_Action_EditTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01388_AT_Action_EditTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLastAction = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.vGridControl1 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.sp01390ATActionEditPreviousActionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowDistrictID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLocalityID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeReference = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeMappingID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowInspectionReferenceButtonEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowInspectionDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowInspectionID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow2 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowActionID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintActionGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowstrJobNumberButtonEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowdtDueDateDateEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowdtDoneDateDateEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintActionByGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintSupervisorGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintPriorityIDGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWorkOrderDescriptionTextEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow4 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowintOwnershipIDGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowCostCentreCodeButtonEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowBudgetID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowmonJobWorkUnitsSpinEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintScheduleOfRatesCheckEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow5 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowJobRateDescButtonEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowmonJobRateBudgetedSpinEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowmonBudgetedCostSpinEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow6 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowActualJobRateDescTextEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowmonJobRateActualSpinEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowmonDiscountRateSpinEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowmonActualCostSpinEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow17 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow7 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowstrUser1TextEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowstrUser2TextEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowstrUser3TextEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUserPickList1GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUserPickList2GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUserPickList3GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow18 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow19 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowstrRemarksMemoEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.checkEditSychronise = new DevExpress.XtraEditors.CheckEdit();
            this.rowActionOwnership = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionCostCentre = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionBudget = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionWorkUnits = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionScheduleOfRates = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionBudgetedRateDescription = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionBudgetedRate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionBudgetedCost = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionActualRateDescription = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionActualRate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionActualCost = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionWorkOrder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionRemarks = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionUserDefined1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionUserDefined2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionUserDefined3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionCostDifference = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionCompletionStatus = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowActionTimeliness = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter();
            this.sp00190_Contractor_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter();
            this.sp01375_AT_User_Screen_SettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01375_AT_User_Screen_SettingsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01375_AT_User_Screen_SettingsTableAdapter();
            this.sp01390_AT_Action_Edit_Previous_ActionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01390_AT_Action_Edit_Previous_ActionsTableAdapter();
            this.sp01362_AT_Ownership_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01362_AT_Ownership_List_With_BlankTableAdapter();
            this.sp00214_Master_Job_List_With_BlankTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00214_Master_Job_List_With_BlankTableAdapter();
            this.categoryRow3 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.sp01391_AT_Cost_Centres_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01391_AT_Cost_Centres_With_BlankTableAdapter();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp02066_AT_Action_Pictures_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp02066_AT_Action_Pictures_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending39 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02066ATActionPicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualJobRateDescTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01388ATActionEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intActionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intInspectionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDueDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDueDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDoneDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDoneDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intWorkOrderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPrevActionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strNearestHouseTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intJobRateBudgetedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intJobRateActualSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSortOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCostCentreIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GUIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monJobRateBudgetedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monJobRateActualSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monBudgetedCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monActualCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monJobWorkUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monDiscountRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionReferenceButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strJobNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intBudgetIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobRateDescButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOwnershipIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01362ATOwnershipListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intActionGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00214MasterJobListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intActionByGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSupervisorGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPriorityIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList1GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intScheduleOfRatesCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreCodeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01391ATCostCentresWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintActionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintBudgetID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCostCentreID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGUID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintInspectionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintJobRateActual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPrevActionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSortOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintWorkOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintJobRateBudgeted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtDoneDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintActionBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSupervisor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintAction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrJobNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintScheduleOfRates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonJobRateBudgeted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonBudgetedCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobRateDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonJobRateActual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonActualCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonDiscountRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualJobRateDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOwnershipID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonJobWorkUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedPicturesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPriorityID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrNearestHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelLastAction.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01390ATActionEditPreviousActionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSychronise.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01375_AT_User_Screen_SettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1110, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 672);
            this.barDockControlBottom.Size = new System.Drawing.Size(1110, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 646);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1110, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 646);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.DockManager = this.dockManager1;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colintJobID
            // 
            this.colintJobID.Caption = "Job ID";
            this.colintJobID.FieldName = "intJobID";
            this.colintJobID.Name = "colintJobID";
            this.colintJobID.OptionsColumn.AllowEdit = false;
            this.colintJobID.OptionsColumn.AllowFocus = false;
            this.colintJobID.OptionsColumn.ReadOnly = true;
            this.colintJobID.Width = 52;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Contractor ID";
            this.gridColumn7.FieldName = "ContractorID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 87;
            // 
            // colItemID
            // 
            this.colItemID.Caption = "Item ID";
            this.colItemID.FieldName = "ItemID";
            this.colItemID.Name = "colItemID";
            this.colItemID.OptionsColumn.AllowEdit = false;
            this.colItemID.OptionsColumn.AllowFocus = false;
            this.colItemID.OptionsColumn.ReadOnly = true;
            this.colItemID.Width = 58;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Item ID";
            this.gridColumn35.FieldName = "ItemID";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Width = 58;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Item ID";
            this.gridColumn41.FieldName = "ItemID";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 58;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Item ID";
            this.gridColumn47.FieldName = "ItemID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Width = 58;
            // 
            // colCostCentreID
            // 
            this.colCostCentreID.Caption = "Cost Centre ID";
            this.colCostCentreID.FieldName = "CostCentreID";
            this.colCostCentreID.Name = "colCostCentreID";
            this.colCostCentreID.OptionsColumn.AllowEdit = false;
            this.colCostCentreID.OptionsColumn.AllowFocus = false;
            this.colCostCentreID.OptionsColumn.ReadOnly = true;
            this.colCostCentreID.Width = 93;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2,
            this.bar4});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiPasteAllValues,
            this.bbiPasteSelectedValues,
            this.bbiCopyDetails,
            this.bbiReload,
            this.bciShowPrevious});
            this.barManager2.MaxItemId = 26;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Save Button - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiFormSave.SuperTip = superToolTip9;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Cancel Button - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.bbiFormCancel.SuperTip = superToolTip10;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "Form Mode - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.barStaticItemFormMode.SuperTip = superToolTip11;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(591, 185);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.bbiCopyDetails, "Copy Action"),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReload, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPasteAllValues, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPasteSelectedValues)});
            this.bar2.Offset = 29;
            this.bar2.Text = "Copy Values";
            // 
            // bbiCopyDetails
            // 
            this.bbiCopyDetails.Caption = "Copy Inspection";
            this.bbiCopyDetails.Id = 21;
            this.bbiCopyDetails.Name = "bbiCopyDetails";
            toolTipTitleItem12.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Text = "Copy Action - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to take a snapshot of the current record for later recall.\r\n\r\nThe recall" +
    "ed details can be copied into other records using the Paste Actions button.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.bbiCopyDetails.SuperTip = superToolTip12;
            this.bbiCopyDetails.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopyDetails_ItemClick);
            // 
            // bbiReload
            // 
            this.bbiReload.Caption = "Reload";
            this.bbiReload.Id = 22;
            this.bbiReload.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiReload.ImageOptions.Image")));
            this.bbiReload.Name = "bbiReload";
            toolTipTitleItem13.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Text = "Reload - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to refresh the currently loaded last copied record.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.bbiReload.SuperTip = superToolTip13;
            this.bbiReload.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReload_ItemClick);
            // 
            // bbiPasteAllValues
            // 
            this.bbiPasteAllValues.Caption = "Copy ALL Values";
            this.bbiPasteAllValues.Id = 15;
            this.bbiPasteAllValues.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPasteAllValues.ImageOptions.Image")));
            this.bbiPasteAllValues.Name = "bbiPasteAllValues";
            superToolTip14.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem14.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Appearance.Options.UseImage = true;
            toolTipTitleItem14.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Text = "Paste All Values - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to paste <b>all</b> values from the last copied record to this record.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.bbiPasteAllValues.SuperTip = superToolTip14;
            this.bbiPasteAllValues.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPasteAllValues_ItemClick);
            // 
            // bbiPasteSelectedValues
            // 
            this.bbiPasteSelectedValues.Caption = "Copy Selected Values";
            this.bbiPasteSelectedValues.Id = 16;
            this.bbiPasteSelectedValues.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPasteSelectedValues.ImageOptions.Image")));
            this.bbiPasteSelectedValues.Name = "bbiPasteSelectedValues";
            superToolTip15.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem15.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Appearance.Options.UseImage = true;
            toolTipTitleItem15.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Text = "Paste Selected Values - Information";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Click me to open a screen to specify which values to paste from the last copied r" +
    "ecord to this record.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.bbiPasteSelectedValues.SuperTip = superToolTip15;
            this.bbiPasteSelectedValues.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPasteSelectedValues_ItemClick);
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 5";
            this.bar4.DockCol = 2;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciShowPrevious)});
            this.bar4.Text = "Custom 5";
            // 
            // bciShowPrevious
            // 
            this.bciShowPrevious.Caption = "Show Previous Details";
            this.bciShowPrevious.Id = 23;
            this.bciShowPrevious.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciShowPrevious.ImageOptions.Image")));
            this.bciShowPrevious.Name = "bciShowPrevious";
            toolTipTitleItem16.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem16.Appearance.Options.UseImage = true;
            toolTipTitleItem16.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem16.Text = "Show Last Action - Information\r\n";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Click me to show \\ hide the Last Action Details Panel.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            this.bciShowPrevious.SuperTip = superToolTip16;
            this.bciShowPrevious.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciShowPrevious_CheckedChanged);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1110, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 672);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1110, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 646);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1110, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 646);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Refresh2_16x16, "Refresh2_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "Refresh2_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl39);
            this.dataLayoutControl1.Controls.Add(this.ActualJobRateDescTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intActionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intInspectionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dtDueDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.dtDoneDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.intWorkOrderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intPrevActionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strNearestHouseTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intJobRateBudgetedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intJobRateActualSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intSortOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intCostCentreIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateAddedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.GUIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.monJobRateBudgetedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.monJobRateActualSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.monBudgetedCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.monActualCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.monJobWorkUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.monDiscountRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectionReferenceButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.strJobNumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.intBudgetIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.JobRateDescButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.intOwnershipIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intActionGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intActionByGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intSupervisorGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intPriorityIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList1GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intScheduleOfRatesCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCentreCodeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkOrderDescriptionTextEdit);
            this.dataLayoutControl1.DataSource = this.sp01388ATActionEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintActionID,
            this.ItemForintBudgetID,
            this.ItemForintCostCentreID,
            this.ItemForDateAdded,
            this.ItemForGUID,
            this.ItemForintInspectionID,
            this.ItemForintJobRateActual,
            this.ItemForintPrevActionID,
            this.ItemForintSortOrder,
            this.ItemForintWorkOrderID,
            this.ItemForintJobRateBudgeted});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1496, 268, 366, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1110, 646);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            this.dataLayoutControl1.HideCustomization += new System.EventHandler(this.dataLayoutControl1_HideCustomization);
            this.dataLayoutControl1.LayoutUpdate += new System.EventHandler(this.dataLayoutControl1_LayoutUpdate);
            // 
            // gridControl39
            // 
            this.gridControl39.DataSource = this.sp02066ATActionPicturesListBindingSource;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl39.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl39.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl39_EmbeddedNavigator_ButtonClick);
            this.gridControl39.Location = new System.Drawing.Point(24, 359);
            this.gridControl39.MainView = this.gridView39;
            this.gridControl39.MenuManager = this.barManager1;
            this.gridControl39.Name = "gridControl39";
            this.gridControl39.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextEditDateTime});
            this.gridControl39.Size = new System.Drawing.Size(1045, 258);
            this.gridControl39.TabIndex = 82;
            this.gridControl39.UseEmbeddedNavigator = true;
            this.gridControl39.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView39});
            // 
            // sp02066ATActionPicturesListBindingSource
            // 
            this.sp02066ATActionPicturesListBindingSource.DataMember = "sp02066_AT_Action_Pictures_List";
            this.sp02066ATActionPicturesListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView39
            // 
            this.gridView39.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn169,
            this.gridColumn170,
            this.gridColumn171,
            this.gridColumn172,
            this.gridColumn173,
            this.gridColumn174,
            this.gridColumn175,
            this.gridColumn176,
            this.gridColumn177,
            this.gridColumn178,
            this.gridColumn179,
            this.colShortLinkedRecordDescription});
            this.gridView39.GridControl = this.gridControl39;
            this.gridView39.Name = "gridView39";
            this.gridView39.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView39.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView39.OptionsLayout.StoreAppearance = true;
            this.gridView39.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView39.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView39.OptionsSelection.MultiSelect = true;
            this.gridView39.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView39.OptionsView.ColumnAutoWidth = false;
            this.gridView39.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView39.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView39.OptionsView.ShowGroupPanel = false;
            this.gridView39.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn174, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView39.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView39_CustomDrawCell);
            this.gridView39.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView39_PopupMenuShowing);
            this.gridView39.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView39_SelectionChanged);
            this.gridView39.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView39_CustomDrawEmptyForeground);
            this.gridView39.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView39_CustomFilterDialog);
            this.gridView39.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView39_FilterEditorCreated);
            this.gridView39.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView39_MouseUp);
            this.gridView39.DoubleClick += new System.EventHandler(this.gridView39_DoubleClick);
            this.gridView39.GotFocus += new System.EventHandler(this.gridView39_GotFocus);
            // 
            // gridColumn169
            // 
            this.gridColumn169.Caption = "Survey Picture ID";
            this.gridColumn169.FieldName = "SurveyPictureID";
            this.gridColumn169.Name = "gridColumn169";
            this.gridColumn169.OptionsColumn.AllowEdit = false;
            this.gridColumn169.OptionsColumn.AllowFocus = false;
            this.gridColumn169.OptionsColumn.ReadOnly = true;
            this.gridColumn169.Width = 105;
            // 
            // gridColumn170
            // 
            this.gridColumn170.Caption = "Linked To Record ID";
            this.gridColumn170.FieldName = "LinkedToRecordID";
            this.gridColumn170.Name = "gridColumn170";
            this.gridColumn170.OptionsColumn.AllowEdit = false;
            this.gridColumn170.OptionsColumn.AllowFocus = false;
            this.gridColumn170.OptionsColumn.ReadOnly = true;
            this.gridColumn170.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn170.Width = 117;
            // 
            // gridColumn171
            // 
            this.gridColumn171.Caption = "Linked To Record Type ID";
            this.gridColumn171.FieldName = "LinkedToRecordTypeID";
            this.gridColumn171.Name = "gridColumn171";
            this.gridColumn171.OptionsColumn.AllowEdit = false;
            this.gridColumn171.OptionsColumn.AllowFocus = false;
            this.gridColumn171.OptionsColumn.ReadOnly = true;
            this.gridColumn171.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn171.Width = 144;
            // 
            // gridColumn172
            // 
            this.gridColumn172.Caption = "Picture Type ID";
            this.gridColumn172.FieldName = "PictureTypeID";
            this.gridColumn172.Name = "gridColumn172";
            this.gridColumn172.OptionsColumn.AllowEdit = false;
            this.gridColumn172.OptionsColumn.AllowFocus = false;
            this.gridColumn172.OptionsColumn.ReadOnly = true;
            this.gridColumn172.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn172.Width = 95;
            // 
            // gridColumn173
            // 
            this.gridColumn173.Caption = "Picture Path";
            this.gridColumn173.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.gridColumn173.FieldName = "PicturePath";
            this.gridColumn173.Name = "gridColumn173";
            this.gridColumn173.OptionsColumn.ReadOnly = true;
            this.gridColumn173.Visible = true;
            this.gridColumn173.VisibleIndex = 2;
            this.gridColumn173.Width = 472;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // gridColumn174
            // 
            this.gridColumn174.Caption = "Date Taken";
            this.gridColumn174.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.gridColumn174.FieldName = "DateTimeTaken";
            this.gridColumn174.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn174.Name = "gridColumn174";
            this.gridColumn174.OptionsColumn.AllowEdit = false;
            this.gridColumn174.OptionsColumn.AllowFocus = false;
            this.gridColumn174.OptionsColumn.ReadOnly = true;
            this.gridColumn174.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn174.Visible = true;
            this.gridColumn174.VisibleIndex = 0;
            this.gridColumn174.Width = 99;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // gridColumn175
            // 
            this.gridColumn175.Caption = "Added By Staff ID";
            this.gridColumn175.FieldName = "AddedByStaffID";
            this.gridColumn175.Name = "gridColumn175";
            this.gridColumn175.OptionsColumn.AllowEdit = false;
            this.gridColumn175.OptionsColumn.AllowFocus = false;
            this.gridColumn175.OptionsColumn.ReadOnly = true;
            this.gridColumn175.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn175.Width = 108;
            // 
            // gridColumn176
            // 
            this.gridColumn176.Caption = "GUID";
            this.gridColumn176.FieldName = "GUID";
            this.gridColumn176.Name = "gridColumn176";
            this.gridColumn176.OptionsColumn.AllowEdit = false;
            this.gridColumn176.OptionsColumn.AllowFocus = false;
            this.gridColumn176.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn177
            // 
            this.gridColumn177.Caption = "Remarks";
            this.gridColumn177.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn177.FieldName = "Remarks";
            this.gridColumn177.Name = "gridColumn177";
            this.gridColumn177.OptionsColumn.ReadOnly = true;
            this.gridColumn177.Visible = true;
            this.gridColumn177.VisibleIndex = 3;
            this.gridColumn177.Width = 252;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // gridColumn178
            // 
            this.gridColumn178.Caption = "Linked To Action";
            this.gridColumn178.FieldName = "LinkedRecordDescription";
            this.gridColumn178.Name = "gridColumn178";
            this.gridColumn178.OptionsColumn.AllowEdit = false;
            this.gridColumn178.OptionsColumn.AllowFocus = false;
            this.gridColumn178.OptionsColumn.ReadOnly = true;
            this.gridColumn178.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn178.Width = 303;
            // 
            // gridColumn179
            // 
            this.gridColumn179.Caption = "Added By";
            this.gridColumn179.FieldName = "AddedByStaffName";
            this.gridColumn179.Name = "gridColumn179";
            this.gridColumn179.OptionsColumn.AllowEdit = false;
            this.gridColumn179.OptionsColumn.AllowFocus = false;
            this.gridColumn179.OptionsColumn.ReadOnly = true;
            this.gridColumn179.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn179.Visible = true;
            this.gridColumn179.VisibleIndex = 1;
            this.gridColumn179.Width = 108;
            // 
            // colShortLinkedRecordDescription
            // 
            this.colShortLinkedRecordDescription.Caption = "Linked To Action (Short Desc)";
            this.colShortLinkedRecordDescription.FieldName = "ShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.Name = "colShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colShortLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colShortLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colShortLinkedRecordDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShortLinkedRecordDescription.Width = 231;
            // 
            // ActualJobRateDescTextEdit
            // 
            this.ActualJobRateDescTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sp01388ATActionEditBindingSource, "ActualJobRateDesc", true));
            this.ActualJobRateDescTextEdit.Location = new System.Drawing.Point(679, 513);
            this.ActualJobRateDescTextEdit.MenuManager = this.barManager1;
            this.ActualJobRateDescTextEdit.Name = "ActualJobRateDescTextEdit";
            this.ActualJobRateDescTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ActualJobRateDescTextEdit, true);
            this.ActualJobRateDescTextEdit.Size = new System.Drawing.Size(378, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ActualJobRateDescTextEdit, optionsSpelling1);
            this.ActualJobRateDescTextEdit.StyleController = this.dataLayoutControl1;
            this.ActualJobRateDescTextEdit.TabIndex = 48;
            // 
            // sp01388ATActionEditBindingSource
            // 
            this.sp01388ATActionEditBindingSource.DataMember = "sp01388_AT_Action_Edit";
            this.sp01388ATActionEditBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp01388ATActionEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(127, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(185, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 46;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // intActionIDTextEdit
            // 
            this.intActionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intActionID", true));
            this.intActionIDTextEdit.Location = new System.Drawing.Point(126, 229);
            this.intActionIDTextEdit.MenuManager = this.barManager1;
            this.intActionIDTextEdit.Name = "intActionIDTextEdit";
            this.intActionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intActionIDTextEdit, true);
            this.intActionIDTextEdit.Size = new System.Drawing.Size(586, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intActionIDTextEdit, optionsSpelling2);
            this.intActionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intActionIDTextEdit.TabIndex = 4;
            // 
            // intInspectionIDTextEdit
            // 
            this.intInspectionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intInspectionID", true));
            this.intInspectionIDTextEdit.Location = new System.Drawing.Point(126, 143);
            this.intInspectionIDTextEdit.MenuManager = this.barManager1;
            this.intInspectionIDTextEdit.Name = "intInspectionIDTextEdit";
            this.intInspectionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intInspectionIDTextEdit, true);
            this.intInspectionIDTextEdit.Size = new System.Drawing.Size(586, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intInspectionIDTextEdit, optionsSpelling3);
            this.intInspectionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intInspectionIDTextEdit.TabIndex = 6;
            // 
            // dtDueDateDateEdit
            // 
            this.dtDueDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "dtDueDate", true));
            this.dtDueDateDateEdit.EditValue = null;
            this.dtDueDateDateEdit.Location = new System.Drawing.Point(125, 119);
            this.dtDueDateDateEdit.MenuManager = this.barManager1;
            this.dtDueDateDateEdit.Name = "dtDueDateDateEdit";
            this.dtDueDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dtDueDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtDueDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtDueDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dtDueDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtDueDateDateEdit.Size = new System.Drawing.Size(166, 20);
            this.dtDueDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtDueDateDateEdit.TabIndex = 9;
            this.dtDueDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.dtDueDateDateEdit_Validating);
            // 
            // dtDoneDateDateEdit
            // 
            this.dtDoneDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "dtDoneDate", true));
            this.dtDoneDateDateEdit.EditValue = null;
            this.dtDoneDateDateEdit.Location = new System.Drawing.Point(125, 143);
            this.dtDoneDateDateEdit.MenuManager = this.barManager1;
            this.dtDoneDateDateEdit.Name = "dtDoneDateDateEdit";
            this.dtDoneDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dtDoneDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtDoneDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtDoneDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dtDoneDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtDoneDateDateEdit.Size = new System.Drawing.Size(166, 20);
            this.dtDoneDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtDoneDateDateEdit.TabIndex = 10;
            // 
            // intWorkOrderIDTextEdit
            // 
            this.intWorkOrderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intWorkOrderID", true));
            this.intWorkOrderIDTextEdit.Location = new System.Drawing.Point(126, 203);
            this.intWorkOrderIDTextEdit.MenuManager = this.barManager1;
            this.intWorkOrderIDTextEdit.Name = "intWorkOrderIDTextEdit";
            this.intWorkOrderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intWorkOrderIDTextEdit, true);
            this.intWorkOrderIDTextEdit.Size = new System.Drawing.Size(586, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intWorkOrderIDTextEdit, optionsSpelling4);
            this.intWorkOrderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intWorkOrderIDTextEdit.TabIndex = 20;
            // 
            // intPrevActionIDTextEdit
            // 
            this.intPrevActionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intPrevActionID", true));
            this.intPrevActionIDTextEdit.Location = new System.Drawing.Point(126, 203);
            this.intPrevActionIDTextEdit.MenuManager = this.barManager1;
            this.intPrevActionIDTextEdit.Name = "intPrevActionIDTextEdit";
            this.intPrevActionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intPrevActionIDTextEdit, true);
            this.intPrevActionIDTextEdit.Size = new System.Drawing.Size(586, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intPrevActionIDTextEdit, optionsSpelling5);
            this.intPrevActionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intPrevActionIDTextEdit.TabIndex = 21;
            // 
            // strUser1TextEdit
            // 
            this.strUser1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "strUser1", true));
            this.strUser1TextEdit.Location = new System.Drawing.Point(137, 359);
            this.strUser1TextEdit.MenuManager = this.barManager1;
            this.strUser1TextEdit.Name = "strUser1TextEdit";
            this.strUser1TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser1TextEdit, true);
            this.strUser1TextEdit.Size = new System.Drawing.Size(223, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser1TextEdit, optionsSpelling6);
            this.strUser1TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser1TextEdit.TabIndex = 16;
            // 
            // strUser2TextEdit
            // 
            this.strUser2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "strUser2", true));
            this.strUser2TextEdit.Location = new System.Drawing.Point(137, 383);
            this.strUser2TextEdit.MenuManager = this.barManager1;
            this.strUser2TextEdit.Name = "strUser2TextEdit";
            this.strUser2TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser2TextEdit, true);
            this.strUser2TextEdit.Size = new System.Drawing.Size(223, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser2TextEdit, optionsSpelling7);
            this.strUser2TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser2TextEdit.TabIndex = 17;
            // 
            // strUser3TextEdit
            // 
            this.strUser3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "strUser3", true));
            this.strUser3TextEdit.Location = new System.Drawing.Point(137, 407);
            this.strUser3TextEdit.MenuManager = this.barManager1;
            this.strUser3TextEdit.Name = "strUser3TextEdit";
            this.strUser3TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser3TextEdit, true);
            this.strUser3TextEdit.Size = new System.Drawing.Size(223, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser3TextEdit, optionsSpelling8);
            this.strUser3TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser3TextEdit.TabIndex = 18;
            // 
            // strNearestHouseTextEdit
            // 
            this.strNearestHouseTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "strNearestHouse", true));
            this.strNearestHouseTextEdit.Location = new System.Drawing.Point(125, 289);
            this.strNearestHouseTextEdit.MenuManager = this.barManager1;
            this.strNearestHouseTextEdit.Name = "strNearestHouseTextEdit";
            this.strNearestHouseTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strNearestHouseTextEdit, true);
            this.strNearestHouseTextEdit.Size = new System.Drawing.Size(956, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strNearestHouseTextEdit, optionsSpelling9);
            this.strNearestHouseTextEdit.StyleController = this.dataLayoutControl1;
            this.strNearestHouseTextEdit.TabIndex = 22;
            // 
            // intJobRateBudgetedSpinEdit
            // 
            this.intJobRateBudgetedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intJobRateBudgeted", true));
            this.intJobRateBudgetedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intJobRateBudgetedSpinEdit.Location = new System.Drawing.Point(126, 287);
            this.intJobRateBudgetedSpinEdit.MenuManager = this.barManager1;
            this.intJobRateBudgetedSpinEdit.Name = "intJobRateBudgetedSpinEdit";
            this.intJobRateBudgetedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intJobRateBudgetedSpinEdit.Properties.ReadOnly = true;
            this.intJobRateBudgetedSpinEdit.Size = new System.Drawing.Size(586, 20);
            this.intJobRateBudgetedSpinEdit.StyleController = this.dataLayoutControl1;
            this.intJobRateBudgetedSpinEdit.TabIndex = 25;
            // 
            // intJobRateActualSpinEdit
            // 
            this.intJobRateActualSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intJobRateActual", true));
            this.intJobRateActualSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intJobRateActualSpinEdit.Location = new System.Drawing.Point(126, 203);
            this.intJobRateActualSpinEdit.MenuManager = this.barManager1;
            this.intJobRateActualSpinEdit.Name = "intJobRateActualSpinEdit";
            this.intJobRateActualSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intJobRateActualSpinEdit.Properties.ReadOnly = true;
            this.intJobRateActualSpinEdit.Size = new System.Drawing.Size(586, 20);
            this.intJobRateActualSpinEdit.StyleController = this.dataLayoutControl1;
            this.intJobRateActualSpinEdit.TabIndex = 26;
            // 
            // intSortOrderSpinEdit
            // 
            this.intSortOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intSortOrder", true));
            this.intSortOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSortOrderSpinEdit.Location = new System.Drawing.Point(126, 203);
            this.intSortOrderSpinEdit.MenuManager = this.barManager1;
            this.intSortOrderSpinEdit.Name = "intSortOrderSpinEdit";
            this.intSortOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intSortOrderSpinEdit.Properties.ReadOnly = true;
            this.intSortOrderSpinEdit.Size = new System.Drawing.Size(586, 20);
            this.intSortOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.intSortOrderSpinEdit.TabIndex = 32;
            // 
            // intCostCentreIDTextEdit
            // 
            this.intCostCentreIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intCostCentreID", true));
            this.intCostCentreIDTextEdit.Location = new System.Drawing.Point(126, 229);
            this.intCostCentreIDTextEdit.MenuManager = this.barManager1;
            this.intCostCentreIDTextEdit.Name = "intCostCentreIDTextEdit";
            this.intCostCentreIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intCostCentreIDTextEdit, true);
            this.intCostCentreIDTextEdit.Size = new System.Drawing.Size(586, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intCostCentreIDTextEdit, optionsSpelling10);
            this.intCostCentreIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intCostCentreIDTextEdit.TabIndex = 38;
            // 
            // DateAddedDateEdit
            // 
            this.DateAddedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "DateAdded", true));
            this.DateAddedDateEdit.EditValue = null;
            this.DateAddedDateEdit.Location = new System.Drawing.Point(126, 203);
            this.DateAddedDateEdit.MenuManager = this.barManager1;
            this.DateAddedDateEdit.Name = "DateAddedDateEdit";
            this.DateAddedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateAddedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateAddedDateEdit.Properties.ReadOnly = true;
            this.DateAddedDateEdit.Size = new System.Drawing.Size(586, 20);
            this.DateAddedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateAddedDateEdit.TabIndex = 39;
            // 
            // GUIDTextEdit
            // 
            this.GUIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "GUID", true));
            this.GUIDTextEdit.Location = new System.Drawing.Point(126, 203);
            this.GUIDTextEdit.MenuManager = this.barManager1;
            this.GUIDTextEdit.Name = "GUIDTextEdit";
            this.GUIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GUIDTextEdit, true);
            this.GUIDTextEdit.Size = new System.Drawing.Size(586, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GUIDTextEdit, optionsSpelling11);
            this.GUIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GUIDTextEdit.TabIndex = 40;
            // 
            // monJobRateBudgetedSpinEdit
            // 
            this.monJobRateBudgetedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "monJobRateBudgeted", true));
            this.monJobRateBudgetedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.monJobRateBudgetedSpinEdit.Location = new System.Drawing.Point(149, 537);
            this.monJobRateBudgetedSpinEdit.MenuManager = this.barManager1;
            this.monJobRateBudgetedSpinEdit.Name = "monJobRateBudgetedSpinEdit";
            this.monJobRateBudgetedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.monJobRateBudgetedSpinEdit.Properties.Mask.EditMask = "c";
            this.monJobRateBudgetedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monJobRateBudgetedSpinEdit.Size = new System.Drawing.Size(383, 20);
            this.monJobRateBudgetedSpinEdit.StyleController = this.dataLayoutControl1;
            this.monJobRateBudgetedSpinEdit.TabIndex = 27;
            this.monJobRateBudgetedSpinEdit.EditValueChanged += new System.EventHandler(this.monJobRateBudgetedSpinEdit_EditValueChanged);
            this.monJobRateBudgetedSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.monJobRateBudgetedSpinEdit_Validating);
            // 
            // monJobRateActualSpinEdit
            // 
            this.monJobRateActualSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "monJobRateActual", true));
            this.monJobRateActualSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.monJobRateActualSpinEdit.Location = new System.Drawing.Point(679, 537);
            this.monJobRateActualSpinEdit.MenuManager = this.barManager1;
            this.monJobRateActualSpinEdit.Name = "monJobRateActualSpinEdit";
            this.monJobRateActualSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.monJobRateActualSpinEdit.Properties.Mask.EditMask = "c";
            this.monJobRateActualSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monJobRateActualSpinEdit.Properties.ReadOnly = true;
            this.monJobRateActualSpinEdit.Size = new System.Drawing.Size(378, 20);
            this.monJobRateActualSpinEdit.StyleController = this.dataLayoutControl1;
            this.monJobRateActualSpinEdit.TabIndex = 28;
            // 
            // monBudgetedCostSpinEdit
            // 
            this.monBudgetedCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "monBudgetedCost", true));
            this.monBudgetedCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.monBudgetedCostSpinEdit.Location = new System.Drawing.Point(149, 585);
            this.monBudgetedCostSpinEdit.MenuManager = this.barManager1;
            this.monBudgetedCostSpinEdit.Name = "monBudgetedCostSpinEdit";
            this.monBudgetedCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.monBudgetedCostSpinEdit.Properties.Mask.EditMask = "c";
            this.monBudgetedCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monBudgetedCostSpinEdit.Properties.ReadOnly = true;
            this.monBudgetedCostSpinEdit.Size = new System.Drawing.Size(383, 20);
            this.monBudgetedCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.monBudgetedCostSpinEdit.TabIndex = 29;
            // 
            // monActualCostSpinEdit
            // 
            this.monActualCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "monActualCost", true));
            this.monActualCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.monActualCostSpinEdit.Location = new System.Drawing.Point(679, 585);
            this.monActualCostSpinEdit.MenuManager = this.barManager1;
            this.monActualCostSpinEdit.Name = "monActualCostSpinEdit";
            this.monActualCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.monActualCostSpinEdit.Properties.Mask.EditMask = "c";
            this.monActualCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monActualCostSpinEdit.Properties.ReadOnly = true;
            this.monActualCostSpinEdit.Size = new System.Drawing.Size(378, 20);
            this.monActualCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.monActualCostSpinEdit.TabIndex = 30;
            // 
            // monJobWorkUnitsSpinEdit
            // 
            this.monJobWorkUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "monJobWorkUnits", true));
            this.monJobWorkUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.monJobWorkUnitsSpinEdit.Location = new System.Drawing.Point(137, 422);
            this.monJobWorkUnitsSpinEdit.MenuManager = this.barManager1;
            this.monJobWorkUnitsSpinEdit.Name = "monJobWorkUnitsSpinEdit";
            this.monJobWorkUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.monJobWorkUnitsSpinEdit.Properties.Mask.EditMask = "f2";
            this.monJobWorkUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monJobWorkUnitsSpinEdit.Size = new System.Drawing.Size(932, 20);
            this.monJobWorkUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.monJobWorkUnitsSpinEdit.TabIndex = 31;
            this.monJobWorkUnitsSpinEdit.EditValueChanged += new System.EventHandler(this.monJobWorkUnitsSpinEdit_EditValueChanged);
            this.monJobWorkUnitsSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.monJobWorkUnitsSpinEdit_Validating);
            // 
            // monDiscountRateSpinEdit
            // 
            this.monDiscountRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "monDiscountRate", true));
            this.monDiscountRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.monDiscountRateSpinEdit.Location = new System.Drawing.Point(679, 561);
            this.monDiscountRateSpinEdit.MenuManager = this.barManager1;
            this.monDiscountRateSpinEdit.Name = "monDiscountRateSpinEdit";
            this.monDiscountRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.monDiscountRateSpinEdit.Properties.Mask.EditMask = "P";
            this.monDiscountRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monDiscountRateSpinEdit.Properties.ReadOnly = true;
            this.monDiscountRateSpinEdit.Size = new System.Drawing.Size(378, 20);
            this.monDiscountRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.monDiscountRateSpinEdit.TabIndex = 34;
            // 
            // InspectionReferenceButtonEdit
            // 
            this.InspectionReferenceButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "InspectionReference", true));
            this.InspectionReferenceButtonEdit.Location = new System.Drawing.Point(125, 35);
            this.InspectionReferenceButtonEdit.MenuManager = this.barManager1;
            this.InspectionReferenceButtonEdit.Name = "InspectionReferenceButtonEdit";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Choose Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>open</b> the <b>Choose Inspection screen</b> to <b>select the pare" +
    "nt inspection</b> for the action.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "View Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>open</b> the <b>Parent Inspection</b> record for the current actio" +
    "n.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.InspectionReferenceButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "choose", superToolTip1, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "view", superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.InspectionReferenceButtonEdit.Properties.ReadOnly = true;
            this.InspectionReferenceButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.InspectionReferenceButtonEdit.Size = new System.Drawing.Size(956, 20);
            this.InspectionReferenceButtonEdit.StyleController = this.dataLayoutControl1;
            this.InspectionReferenceButtonEdit.TabIndex = 5;
            this.InspectionReferenceButtonEdit.TabStop = false;
            this.InspectionReferenceButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.InspectionReferenceButtonEdit_ButtonClick);
            this.InspectionReferenceButtonEdit.EditValueChanged += new System.EventHandler(this.InspectionReferenceButtonEdit_EditValueChanged);
            this.InspectionReferenceButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.InspectionReferenceButtonEdit_Validating);
            // 
            // strJobNumberButtonEdit
            // 
            this.strJobNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "strJobNumber", true));
            this.strJobNumberButtonEdit.Location = new System.Drawing.Point(125, 85);
            this.strJobNumberButtonEdit.MenuManager = this.barManager1;
            this.strJobNumberButtonEdit.Name = "strJobNumberButtonEdit";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem3.Text = "Sequence Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Text = "Number Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.strJobNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "Sequence", superToolTip3, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "Number", superToolTip4, DevExpress.Utils.ToolTipAnchor.Default)});
            this.strJobNumberButtonEdit.Properties.MaxLength = 20;
            this.strJobNumberButtonEdit.Size = new System.Drawing.Size(956, 20);
            this.strJobNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.strJobNumberButtonEdit.TabIndex = 14;
            this.strJobNumberButtonEdit.TabStop = false;
            this.strJobNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.strJobNumberButtonEdit_ButtonClick);
            this.strJobNumberButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strJobNumberButtonEdit_Validating);
            // 
            // intBudgetIDTextEdit
            // 
            this.intBudgetIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intBudgetID", true));
            this.intBudgetIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intBudgetIDTextEdit.Location = new System.Drawing.Point(126, 203);
            this.intBudgetIDTextEdit.MenuManager = this.barManager1;
            this.intBudgetIDTextEdit.Name = "intBudgetIDTextEdit";
            this.intBudgetIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.intBudgetIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.intBudgetIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intBudgetIDTextEdit, true);
            this.intBudgetIDTextEdit.Size = new System.Drawing.Size(586, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intBudgetIDTextEdit, optionsSpelling12);
            this.intBudgetIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intBudgetIDTextEdit.TabIndex = 33;
            this.intBudgetIDTextEdit.TabStop = false;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(24, 359);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(1045, 258);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling13);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 19;
            // 
            // JobRateDescButtonEdit
            // 
            this.JobRateDescButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "JobRateDesc", true));
            this.JobRateDescButtonEdit.Location = new System.Drawing.Point(149, 513);
            this.JobRateDescButtonEdit.MenuManager = this.barManager1;
            this.JobRateDescButtonEdit.Name = "JobRateDescButtonEdit";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Choose Job Rate Button - Information";
            toolTipItem5.LeftIndent = 6;
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.JobRateDescButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "choose", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Clear Selected Rate", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.JobRateDescButtonEdit.Properties.ReadOnly = true;
            this.JobRateDescButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.JobRateDescButtonEdit.Size = new System.Drawing.Size(383, 20);
            this.JobRateDescButtonEdit.StyleController = this.dataLayoutControl1;
            this.JobRateDescButtonEdit.TabIndex = 24;
            this.JobRateDescButtonEdit.TabStop = false;
            this.JobRateDescButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.JobRateDescButtonEdit_ButtonClick);
            // 
            // intOwnershipIDGridLookUpEdit
            // 
            this.intOwnershipIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intOwnershipID", true));
            this.intOwnershipIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intOwnershipIDGridLookUpEdit.Location = new System.Drawing.Point(137, 359);
            this.intOwnershipIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intOwnershipIDGridLookUpEdit.Name = "intOwnershipIDGridLookUpEdit";
            editorButtonImageOptions7.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions8.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intOwnershipIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intOwnershipIDGridLookUpEdit.Properties.DataSource = this.sp01362ATOwnershipListWithBlankBindingSource;
            this.intOwnershipIDGridLookUpEdit.Properties.DisplayMember = "OwnershipName";
            this.intOwnershipIDGridLookUpEdit.Properties.NullText = "";
            this.intOwnershipIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.intOwnershipIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intOwnershipIDGridLookUpEdit.Properties.ValueMember = "OwnershipID";
            this.intOwnershipIDGridLookUpEdit.Size = new System.Drawing.Size(932, 22);
            this.intOwnershipIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intOwnershipIDGridLookUpEdit.TabIndex = 36;
            this.intOwnershipIDGridLookUpEdit.TabStop = false;
            this.intOwnershipIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intOwnershipIDGridLookUpEdit_ButtonClick);
            this.intOwnershipIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.intOwnershipIDGridLookUpEdit_EditValueChanged);
            this.intOwnershipIDGridLookUpEdit.Validated += new System.EventHandler(this.intOwnershipIDGridLookUpEdit_Validated);
            // 
            // sp01362ATOwnershipListWithBlankBindingSource
            // 
            this.sp01362ATOwnershipListWithBlankBindingSource.DataMember = "sp01362_AT_Ownership_List_With_Blank";
            this.sp01362ATOwnershipListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOwnershipCode,
            this.colOwnershipID,
            this.colOwnershipName,
            this.gridColumn49});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOwnershipName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colOwnershipCode
            // 
            this.colOwnershipCode.Caption = "Ownership Code";
            this.colOwnershipCode.FieldName = "OwnershipCode";
            this.colOwnershipCode.Name = "colOwnershipCode";
            this.colOwnershipCode.OptionsColumn.AllowEdit = false;
            this.colOwnershipCode.OptionsColumn.AllowFocus = false;
            this.colOwnershipCode.OptionsColumn.ReadOnly = true;
            this.colOwnershipCode.Visible = true;
            this.colOwnershipCode.VisibleIndex = 0;
            this.colOwnershipCode.Width = 124;
            // 
            // colOwnershipID
            // 
            this.colOwnershipID.Caption = "Ownership ID";
            this.colOwnershipID.FieldName = "OwnershipID";
            this.colOwnershipID.Name = "colOwnershipID";
            this.colOwnershipID.OptionsColumn.AllowEdit = false;
            this.colOwnershipID.OptionsColumn.AllowFocus = false;
            this.colOwnershipID.OptionsColumn.ReadOnly = true;
            this.colOwnershipID.Width = 120;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.Caption = "Ownership Name";
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.OptionsColumn.AllowEdit = false;
            this.colOwnershipName.OptionsColumn.AllowFocus = false;
            this.colOwnershipName.OptionsColumn.ReadOnly = true;
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 1;
            this.colOwnershipName.Width = 239;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Remarks";
            this.gridColumn49.FieldName = "Remarks";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 2;
            // 
            // intActionGridLookUpEdit
            // 
            this.intActionGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intAction", true));
            this.intActionGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intActionGridLookUpEdit.Location = new System.Drawing.Point(125, 59);
            this.intActionGridLookUpEdit.MenuManager = this.barManager1;
            this.intActionGridLookUpEdit.Name = "intActionGridLookUpEdit";
            editorButtonImageOptions9.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions10.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intActionGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intActionGridLookUpEdit.Properties.DataSource = this.sp00214MasterJobListWithBlankBindingSource;
            this.intActionGridLookUpEdit.Properties.DisplayMember = "strJobDescription";
            this.intActionGridLookUpEdit.Properties.NullText = "";
            this.intActionGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.intActionGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit3});
            this.intActionGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intActionGridLookUpEdit.Properties.ValueMember = "intJobID";
            this.intActionGridLookUpEdit.Size = new System.Drawing.Size(956, 22);
            this.intActionGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intActionGridLookUpEdit.TabIndex = 11;
            this.intActionGridLookUpEdit.TabStop = false;
            this.intActionGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intActionGridLookUpEdit_ButtonClick);
            this.intActionGridLookUpEdit.Enter += new System.EventHandler(this.intActionGridLookUpEdit_Enter);
            this.intActionGridLookUpEdit.Leave += new System.EventHandler(this.intActionGridLookUpEdit_Leave);
            this.intActionGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.intActionGridLookUpEdit_Validating);
            // 
            // sp00214MasterJobListWithBlankBindingSource
            // 
            this.sp00214MasterJobListWithBlankBindingSource.DataMember = "sp00214_Master_Job_List_With_Blank";
            this.sp00214MasterJobListWithBlankBindingSource.DataSource = this.dataSet_AT;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDefaultWorkUnits,
            this.colintJobID,
            this.colstrJobCode,
            this.colstrJobDescription,
            this.colstrRemarks,
            this.colJobDisabled});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colintJobID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrJobDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDefaultWorkUnits
            // 
            this.colDefaultWorkUnits.Caption = "Default Work Units";
            this.colDefaultWorkUnits.FieldName = "DefaultWorkUnits";
            this.colDefaultWorkUnits.Name = "colDefaultWorkUnits";
            this.colDefaultWorkUnits.OptionsColumn.AllowEdit = false;
            this.colDefaultWorkUnits.OptionsColumn.AllowFocus = false;
            this.colDefaultWorkUnits.OptionsColumn.ReadOnly = true;
            this.colDefaultWorkUnits.Visible = true;
            this.colDefaultWorkUnits.VisibleIndex = 2;
            this.colDefaultWorkUnits.Width = 111;
            // 
            // colstrJobCode
            // 
            this.colstrJobCode.Caption = "Job Code";
            this.colstrJobCode.FieldName = "strJobCode";
            this.colstrJobCode.Name = "colstrJobCode";
            this.colstrJobCode.OptionsColumn.AllowEdit = false;
            this.colstrJobCode.OptionsColumn.AllowFocus = false;
            this.colstrJobCode.OptionsColumn.ReadOnly = true;
            this.colstrJobCode.Visible = true;
            this.colstrJobCode.VisibleIndex = 1;
            this.colstrJobCode.Width = 124;
            // 
            // colstrJobDescription
            // 
            this.colstrJobDescription.Caption = "Job Description";
            this.colstrJobDescription.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colstrJobDescription.FieldName = "strJobDescription";
            this.colstrJobDescription.Name = "colstrJobDescription";
            this.colstrJobDescription.OptionsColumn.AllowEdit = false;
            this.colstrJobDescription.OptionsColumn.AllowFocus = false;
            this.colstrJobDescription.OptionsColumn.ReadOnly = true;
            this.colstrJobDescription.Visible = true;
            this.colstrJobDescription.VisibleIndex = 0;
            this.colstrJobDescription.Width = 300;
            // 
            // colstrRemarks
            // 
            this.colstrRemarks.Caption = "Remarks";
            this.colstrRemarks.FieldName = "strRemarks";
            this.colstrRemarks.Name = "colstrRemarks";
            this.colstrRemarks.Visible = true;
            this.colstrRemarks.VisibleIndex = 4;
            this.colstrRemarks.Width = 103;
            // 
            // colJobDisabled
            // 
            this.colJobDisabled.Caption = "Disabled";
            this.colJobDisabled.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colJobDisabled.FieldName = "JobDisabled";
            this.colJobDisabled.Name = "colJobDisabled";
            this.colJobDisabled.OptionsColumn.AllowEdit = false;
            this.colJobDisabled.OptionsColumn.AllowFocus = false;
            this.colJobDisabled.OptionsColumn.ReadOnly = true;
            this.colJobDisabled.Visible = true;
            this.colJobDisabled.VisibleIndex = 3;
            this.colJobDisabled.Width = 61;
            // 
            // intActionByGridLookUpEdit
            // 
            this.intActionByGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intActionBy", true));
            this.intActionByGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intActionByGridLookUpEdit.Location = new System.Drawing.Point(125, 177);
            this.intActionByGridLookUpEdit.MenuManager = this.barManager1;
            this.intActionByGridLookUpEdit.Name = "intActionByGridLookUpEdit";
            editorButtonImageOptions11.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions12.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intActionByGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intActionByGridLookUpEdit.Properties.DataSource = this.sp00190ContractorListWithBlankBindingSource;
            this.intActionByGridLookUpEdit.Properties.DisplayMember = "ContractorName";
            this.intActionByGridLookUpEdit.Properties.NullText = "";
            this.intActionByGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.intActionByGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.intActionByGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intActionByGridLookUpEdit.Properties.ValueMember = "ContractorID";
            this.intActionByGridLookUpEdit.Size = new System.Drawing.Size(956, 22);
            this.intActionByGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intActionByGridLookUpEdit.TabIndex = 12;
            this.intActionByGridLookUpEdit.TabStop = false;
            this.intActionByGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intActionByGridLookUpEdit_ButtonClick);
            this.intActionByGridLookUpEdit.Enter += new System.EventHandler(this.intActionByGridLookUpEdit_Enter);
            this.intActionByGridLookUpEdit.Leave += new System.EventHandler(this.intActionByGridLookUpEdit_Leave);
            // 
            // sp00190ContractorListWithBlankBindingSource
            // 
            this.sp00190ContractorListWithBlankBindingSource.DataMember = "sp00190_Contractor_List_With_Blank";
            this.sp00190ContractorListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colContractorCode,
            this.colContractorID,
            this.colContractorName,
            this.colDisabled,
            this.colEmailPassword,
            this.colInternalContractor,
            this.colInternalCOntractorDescription,
            this.colMobile,
            this.colPostcode,
            this.colRemarks,
            this.colTelephone1,
            this.colTelephone2,
            this.colTypeDescription,
            this.colTypeID,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colVatReg,
            this.colWebsite});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colContractorID;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInternalCOntractorDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 273;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor ID";
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 118;
            // 
            // colInternalCOntractorDescription
            // 
            this.colInternalCOntractorDescription.Caption = "Status";
            this.colInternalCOntractorDescription.FieldName = "InternalCOntractorDescription";
            this.colInternalCOntractorDescription.Name = "colInternalCOntractorDescription";
            this.colInternalCOntractorDescription.OptionsColumn.AllowEdit = false;
            this.colInternalCOntractorDescription.OptionsColumn.AllowFocus = false;
            this.colInternalCOntractorDescription.OptionsColumn.ReadOnly = true;
            this.colInternalCOntractorDescription.Width = 52;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Width = 51;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Width = 62;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 1;
            this.colTypeDescription.Width = 111;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.Width = 49;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Reg";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 62;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // intSupervisorGridLookUpEdit
            // 
            this.intSupervisorGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intSupervisor", true));
            this.intSupervisorGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSupervisorGridLookUpEdit.Location = new System.Drawing.Point(125, 203);
            this.intSupervisorGridLookUpEdit.MenuManager = this.barManager1;
            this.intSupervisorGridLookUpEdit.Name = "intSupervisorGridLookUpEdit";
            editorButtonImageOptions13.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions14.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intSupervisorGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions13, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions14, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intSupervisorGridLookUpEdit.Properties.DataSource = this.sp00190ContractorListWithBlankBindingSource;
            this.intSupervisorGridLookUpEdit.Properties.DisplayMember = "ContractorName";
            this.intSupervisorGridLookUpEdit.Properties.NullText = "";
            this.intSupervisorGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.intSupervisorGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.intSupervisorGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intSupervisorGridLookUpEdit.Properties.ValueMember = "ContractorID";
            this.intSupervisorGridLookUpEdit.Size = new System.Drawing.Size(956, 22);
            this.intSupervisorGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intSupervisorGridLookUpEdit.TabIndex = 13;
            this.intSupervisorGridLookUpEdit.TabStop = false;
            this.intSupervisorGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intSupervisorGridLookUpEdit_ButtonClick);
            this.intSupervisorGridLookUpEdit.Enter += new System.EventHandler(this.intSupervisorGridLookUpEdit_Enter);
            this.intSupervisorGridLookUpEdit.Leave += new System.EventHandler(this.intSupervisorGridLookUpEdit_Leave);
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.gridColumn7;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Address Line 1";
            this.gridColumn1.FieldName = "AddressLine1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 144;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Address Line 2";
            this.gridColumn2.FieldName = "AddressLine2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 91;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Address Line 3";
            this.gridColumn3.FieldName = "AddressLine3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 91;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Address Line 4";
            this.gridColumn4.FieldName = "AddressLine4";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 91;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Address Line 5";
            this.gridColumn5.FieldName = "AddressLine5";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 91;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Contractor Code";
            this.gridColumn6.FieldName = "ContractorCode";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 101;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Contractor Name";
            this.gridColumn8.FieldName = "ContractorName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 273;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Disabled";
            this.gridColumn9.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn9.FieldName = "Disabled";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            this.gridColumn9.Width = 61;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Email Password";
            this.gridColumn10.FieldName = "EmailPassword";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 94;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Internal Contractor ID";
            this.gridColumn11.FieldName = "InternalContractor";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 118;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Status";
            this.gridColumn12.FieldName = "InternalCOntractorDescription";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 52;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Mobile";
            this.gridColumn13.FieldName = "Mobile";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 51;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Postcode";
            this.gridColumn14.FieldName = "Postcode";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 65;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Remarks";
            this.gridColumn15.FieldName = "Remarks";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 62;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Telephone 1";
            this.gridColumn16.FieldName = "Telephone1";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 80;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Telephone 2";
            this.gridColumn17.FieldName = "Telephone2";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 80;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Type";
            this.gridColumn18.FieldName = "TypeDescription";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            this.gridColumn18.Width = 111;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Type ID";
            this.gridColumn19.FieldName = "TypeID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 49;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "User Defined 1";
            this.gridColumn20.FieldName = "UserDefined1";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 92;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "User Defined 2";
            this.gridColumn21.FieldName = "UserDefined2";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 92;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "User Defined 3";
            this.gridColumn22.FieldName = "UserDefined3";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 92;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "VAT Reg";
            this.gridColumn23.FieldName = "VatReg";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 62;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Website";
            this.gridColumn24.FieldName = "Website";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 60;
            // 
            // intPriorityIDGridLookUpEdit
            // 
            this.intPriorityIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intPriorityID", true));
            this.intPriorityIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intPriorityIDGridLookUpEdit.Location = new System.Drawing.Point(125, 229);
            this.intPriorityIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intPriorityIDGridLookUpEdit.Name = "intPriorityIDGridLookUpEdit";
            editorButtonImageOptions15.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions16.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intPriorityIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions15, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject57, serializableAppearanceObject58, serializableAppearanceObject59, serializableAppearanceObject60, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject61, serializableAppearanceObject62, serializableAppearanceObject63, serializableAppearanceObject64, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intPriorityIDGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intPriorityIDGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intPriorityIDGridLookUpEdit.Properties.NullText = "";
            this.intPriorityIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.intPriorityIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intPriorityIDGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intPriorityIDGridLookUpEdit.Size = new System.Drawing.Size(956, 22);
            this.intPriorityIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intPriorityIDGridLookUpEdit.TabIndex = 23;
            this.intPriorityIDGridLookUpEdit.TabStop = false;
            this.intPriorityIDGridLookUpEdit.Tag = "126";
            // 
            // sp01372ATMultiplePicklistsWithBlanksBindingSource
            // 
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataMember = "sp01372_AT_Multiple_Picklists_With_Blanks";
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHeaderDescription,
            this.colHeaderID,
            this.colItemCode,
            this.colItemDescription,
            this.colItemID,
            this.colOrder1});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.colItemID;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView4.OptionsCustomization.AllowFilter = false;
            this.gridView4.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView4.OptionsFilter.AllowFilterEditor = false;
            this.gridView4.OptionsFilter.AllowMRUFilterList = false;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Picklist Type";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Width = 189;
            // 
            // colHeaderID
            // 
            this.colHeaderID.Caption = "Picklist ID";
            this.colHeaderID.FieldName = "HeaderID";
            this.colHeaderID.Name = "colHeaderID";
            this.colHeaderID.OptionsColumn.AllowEdit = false;
            this.colHeaderID.OptionsColumn.AllowFocus = false;
            this.colHeaderID.OptionsColumn.ReadOnly = true;
            this.colHeaderID.Width = 67;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item ID";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.OptionsColumn.AllowFocus = false;
            this.colItemCode.OptionsColumn.ReadOnly = true;
            this.colItemCode.Width = 58;
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Description";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 264;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.AllowFocus = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            this.colOrder1.Width = 65;
            // 
            // UserPickList1GridLookUpEdit
            // 
            this.UserPickList1GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "UserPickList1", true));
            this.UserPickList1GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList1GridLookUpEdit.Location = new System.Drawing.Point(137, 441);
            this.UserPickList1GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList1GridLookUpEdit.Name = "UserPickList1GridLookUpEdit";
            editorButtonImageOptions17.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions18.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPickList1GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions17, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject65, serializableAppearanceObject66, serializableAppearanceObject67, serializableAppearanceObject68, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions18, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject69, serializableAppearanceObject70, serializableAppearanceObject71, serializableAppearanceObject72, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPickList1GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList1GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList1GridLookUpEdit.Properties.NullText = "";
            this.UserPickList1GridLookUpEdit.Properties.PopupView = this.gridView6;
            this.UserPickList1GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList1GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList1GridLookUpEdit.Size = new System.Drawing.Size(932, 22);
            this.UserPickList1GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList1GridLookUpEdit.TabIndex = 41;
            this.UserPickList1GridLookUpEdit.TabStop = false;
            this.UserPickList1GridLookUpEdit.Tag = "147";
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.gridColumn35;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView6.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView6.OptionsCustomization.AllowFilter = false;
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn36, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Picklist Type";
            this.gridColumn31.FieldName = "HeaderDescription";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Width = 189;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Picklist ID";
            this.gridColumn32.FieldName = "HeaderID";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 67;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Item ID";
            this.gridColumn33.FieldName = "ItemCode";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 58;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Description";
            this.gridColumn34.FieldName = "ItemDescription";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 0;
            this.gridColumn34.Width = 264;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Order";
            this.gridColumn36.FieldName = "Order";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 65;
            // 
            // UserPickList2GridLookUpEdit
            // 
            this.UserPickList2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "UserPickList2", true));
            this.UserPickList2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList2GridLookUpEdit.Location = new System.Drawing.Point(137, 467);
            this.UserPickList2GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList2GridLookUpEdit.Name = "UserPickList2GridLookUpEdit";
            editorButtonImageOptions19.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions20.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPickList2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions19, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject73, serializableAppearanceObject74, serializableAppearanceObject75, serializableAppearanceObject76, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions20, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject77, serializableAppearanceObject78, serializableAppearanceObject79, serializableAppearanceObject80, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPickList2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList2GridLookUpEdit.Properties.NullText = "";
            this.UserPickList2GridLookUpEdit.Properties.PopupView = this.gridView7;
            this.UserPickList2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList2GridLookUpEdit.Size = new System.Drawing.Size(932, 22);
            this.UserPickList2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList2GridLookUpEdit.TabIndex = 42;
            this.UserPickList2GridLookUpEdit.TabStop = false;
            this.UserPickList2GridLookUpEdit.Tag = "148";
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition7.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition7.Appearance.Options.UseForeColor = true;
            styleFormatCondition7.ApplyToRow = true;
            styleFormatCondition7.Column = this.gridColumn41;
            styleFormatCondition7.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition7.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition7});
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView7.OptionsCustomization.AllowFilter = false;
            this.gridView7.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView7.OptionsFilter.AllowFilterEditor = false;
            this.gridView7.OptionsFilter.AllowMRUFilterList = false;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn42, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Picklist Type";
            this.gridColumn37.FieldName = "HeaderDescription";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 189;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Picklist ID";
            this.gridColumn38.FieldName = "HeaderID";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 67;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Item ID";
            this.gridColumn39.FieldName = "ItemCode";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 58;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Description";
            this.gridColumn40.FieldName = "ItemDescription";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 0;
            this.gridColumn40.Width = 264;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Order";
            this.gridColumn42.FieldName = "Order";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Width = 65;
            // 
            // UserPickList3GridLookUpEdit
            // 
            this.UserPickList3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "UserPickList3", true));
            this.UserPickList3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList3GridLookUpEdit.Location = new System.Drawing.Point(137, 493);
            this.UserPickList3GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList3GridLookUpEdit.Name = "UserPickList3GridLookUpEdit";
            editorButtonImageOptions21.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions22.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPickList3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions21, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject81, serializableAppearanceObject82, serializableAppearanceObject83, serializableAppearanceObject84, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions22, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject85, serializableAppearanceObject86, serializableAppearanceObject87, serializableAppearanceObject88, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPickList3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList3GridLookUpEdit.Properties.NullText = "";
            this.UserPickList3GridLookUpEdit.Properties.PopupView = this.gridView8;
            this.UserPickList3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList3GridLookUpEdit.Size = new System.Drawing.Size(932, 22);
            this.UserPickList3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList3GridLookUpEdit.TabIndex = 43;
            this.UserPickList3GridLookUpEdit.TabStop = false;
            this.UserPickList3GridLookUpEdit.Tag = "149";
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition8.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition8.Appearance.Options.UseForeColor = true;
            styleFormatCondition8.ApplyToRow = true;
            styleFormatCondition8.Column = this.gridColumn47;
            styleFormatCondition8.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition8.Value1 = 0;
            this.gridView8.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition8});
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView8.OptionsCustomization.AllowFilter = false;
            this.gridView8.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowFilterEditor = false;
            this.gridView8.OptionsFilter.AllowMRUFilterList = false;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn48, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Picklist Type";
            this.gridColumn43.FieldName = "HeaderDescription";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Width = 189;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Picklist ID";
            this.gridColumn44.FieldName = "HeaderID";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Width = 67;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Item ID";
            this.gridColumn45.FieldName = "ItemCode";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Width = 58;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Description";
            this.gridColumn46.FieldName = "ItemDescription";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 0;
            this.gridColumn46.Width = 264;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Order";
            this.gridColumn48.FieldName = "Order";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Width = 65;
            // 
            // intScheduleOfRatesCheckEdit
            // 
            this.intScheduleOfRatesCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "intScheduleOfRates", true));
            this.intScheduleOfRatesCheckEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intScheduleOfRatesCheckEdit.Location = new System.Drawing.Point(137, 446);
            this.intScheduleOfRatesCheckEdit.MenuManager = this.barManager1;
            this.intScheduleOfRatesCheckEdit.Name = "intScheduleOfRatesCheckEdit";
            this.intScheduleOfRatesCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.intScheduleOfRatesCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.intScheduleOfRatesCheckEdit.Properties.ValueChecked = 1;
            this.intScheduleOfRatesCheckEdit.Properties.ValueUnchecked = 0;
            this.intScheduleOfRatesCheckEdit.Size = new System.Drawing.Size(108, 19);
            this.intScheduleOfRatesCheckEdit.StyleController = this.dataLayoutControl1;
            this.intScheduleOfRatesCheckEdit.TabIndex = 35;
            this.intScheduleOfRatesCheckEdit.TabStop = false;
            this.intScheduleOfRatesCheckEdit.CheckedChanged += new System.EventHandler(this.intScheduleOfRatesCheckEdit_CheckedChanged);
            // 
            // CostCentreCodeButtonEdit
            // 
            this.CostCentreCodeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "CostCentreCode", true));
            this.CostCentreCodeButtonEdit.Location = new System.Drawing.Point(137, 385);
            this.CostCentreCodeButtonEdit.MenuManager = this.barManager1;
            this.CostCentreCodeButtonEdit.Name = "CostCentreCodeButtonEdit";
            editorButtonImageOptions23.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions24.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.CostCentreCodeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions23, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject89, serializableAppearanceObject90, serializableAppearanceObject91, serializableAppearanceObject92, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject93, serializableAppearanceObject94, serializableAppearanceObject95, serializableAppearanceObject96, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CostCentreCodeButtonEdit.Properties.CloseOnLostFocus = false;
            this.CostCentreCodeButtonEdit.Properties.CloseOnOuterMouseClick = false;
            this.CostCentreCodeButtonEdit.Properties.PopupControl = this.popupContainerControl1;
            this.CostCentreCodeButtonEdit.Properties.ShowPopupCloseButton = false;
            this.CostCentreCodeButtonEdit.Size = new System.Drawing.Size(932, 22);
            this.CostCentreCodeButtonEdit.StyleController = this.dataLayoutControl1;
            this.CostCentreCodeButtonEdit.TabIndex = 37;
            this.CostCentreCodeButtonEdit.TabStop = false;
            this.CostCentreCodeButtonEdit.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            this.CostCentreCodeButtonEdit.Popup += new System.EventHandler(this.CostCentreCodeButtonEdit_Popup);
            this.CostCentreCodeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CostCentreCodeButtonEdit_ButtonClick);
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.splitContainerControl1);
            this.popupContainerControl1.Controls.Add(this.btnOK);
            this.popupContainerControl1.Location = new System.Drawing.Point(-3, 178);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(369, 321);
            this.popupContainerControl1.TabIndex = 15;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(3, 3);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.dateEditDateTo);
            this.splitContainerControl1.Panel1.Controls.Add(this.dateEditDateFrom);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.btnRefresh);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(363, 288);
            this.splitContainerControl1.SplitterPosition = 26;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // dateEditDateTo
            // 
            this.dateEditDateTo.EditValue = null;
            this.dateEditDateTo.Location = new System.Drawing.Point(195, 3);
            this.dateEditDateTo.MenuManager = this.barManager1;
            this.dateEditDateTo.Name = "dateEditDateTo";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Clear Date - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.dateEditDateTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions25, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject97, serializableAppearanceObject98, serializableAppearanceObject99, serializableAppearanceObject100, "", null, superToolTip6, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditDateTo.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.NullValuePrompt = "No Date";
            this.dateEditDateTo.Size = new System.Drawing.Size(104, 20);
            this.dateEditDateTo.TabIndex = 12;
            this.dateEditDateTo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditDateTo_ButtonClick);
            // 
            // dateEditDateFrom
            // 
            this.dateEditDateFrom.EditValue = null;
            this.dateEditDateFrom.Location = new System.Drawing.Point(69, 3);
            this.dateEditDateFrom.MenuManager = this.barManager1;
            this.dateEditDateFrom.Name = "dateEditDateFrom";
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Clear Date - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.dateEditDateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions26, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject101, serializableAppearanceObject102, serializableAppearanceObject103, serializableAppearanceObject104, "", null, superToolTip7, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditDateFrom.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.NullValuePrompt = "No Date";
            this.dateEditDateFrom.Size = new System.Drawing.Size(104, 20);
            this.dateEditDateFrom.TabIndex = 11;
            this.dateEditDateFrom.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditDateFrom_ButtonClick);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(61, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Date Range:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(178, 5);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(12, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "To";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(304, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(56, 23);
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(363, 256);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01391ATCostCentresWithBlankBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView5;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(363, 256);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp01391ATCostCentresWithBlankBindingSource
            // 
            this.sp01391ATCostCentresWithBlankBindingSource.DataMember = "sp01391_AT_Cost_Centres_With_Blank";
            this.sp01391ATCostCentresWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCostCentreID,
            this.colBudgetID1,
            this.colOwnershipID1,
            this.colCostCentreCode,
            this.colBudgetCode1,
            this.colBudgetDesc,
            this.colOwnershipCode1,
            this.colOwnershipName1});
            styleFormatCondition9.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition9.Appearance.Options.UseForeColor = true;
            styleFormatCondition9.ApplyToRow = true;
            styleFormatCondition9.Column = this.colCostCentreID;
            styleFormatCondition9.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition9.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition9});
            this.gridView5.GridControl = this.gridControl1;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBudgetDesc, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOwnershipName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCostCentreCode, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView5_CustomDrawEmptyForeground);
            this.gridView5.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView5_FocusedRowChanged);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseDown);
            // 
            // colBudgetID1
            // 
            this.colBudgetID1.Caption = "Budget ID";
            this.colBudgetID1.FieldName = "BudgetID";
            this.colBudgetID1.Name = "colBudgetID1";
            this.colBudgetID1.OptionsColumn.AllowEdit = false;
            this.colBudgetID1.OptionsColumn.AllowFocus = false;
            this.colBudgetID1.OptionsColumn.ReadOnly = true;
            this.colBudgetID1.Width = 69;
            // 
            // colOwnershipID1
            // 
            this.colOwnershipID1.Caption = "Ownership ID";
            this.colOwnershipID1.FieldName = "OwnershipID";
            this.colOwnershipID1.Name = "colOwnershipID1";
            this.colOwnershipID1.OptionsColumn.AllowEdit = false;
            this.colOwnershipID1.OptionsColumn.AllowFocus = false;
            this.colOwnershipID1.OptionsColumn.ReadOnly = true;
            this.colOwnershipID1.Width = 86;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.Caption = "Cost Centre Code";
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.Visible = true;
            this.colCostCentreCode.VisibleIndex = 0;
            this.colCostCentreCode.Width = 129;
            // 
            // colBudgetCode1
            // 
            this.colBudgetCode1.Caption = "Budget Code";
            this.colBudgetCode1.FieldName = "BudgetCode";
            this.colBudgetCode1.Name = "colBudgetCode1";
            this.colBudgetCode1.OptionsColumn.AllowEdit = false;
            this.colBudgetCode1.OptionsColumn.AllowFocus = false;
            this.colBudgetCode1.OptionsColumn.ReadOnly = true;
            this.colBudgetCode1.Visible = true;
            this.colBudgetCode1.VisibleIndex = 1;
            this.colBudgetCode1.Width = 104;
            // 
            // colBudgetDesc
            // 
            this.colBudgetDesc.Caption = "Budget Description";
            this.colBudgetDesc.FieldName = "BudgetDesc";
            this.colBudgetDesc.Name = "colBudgetDesc";
            this.colBudgetDesc.OptionsColumn.AllowEdit = false;
            this.colBudgetDesc.OptionsColumn.AllowFocus = false;
            this.colBudgetDesc.OptionsColumn.ReadOnly = true;
            this.colBudgetDesc.Visible = true;
            this.colBudgetDesc.VisibleIndex = 2;
            this.colBudgetDesc.Width = 194;
            // 
            // colOwnershipCode1
            // 
            this.colOwnershipCode1.Caption = "Ownership Code";
            this.colOwnershipCode1.FieldName = "OwnershipCode";
            this.colOwnershipCode1.Name = "colOwnershipCode1";
            this.colOwnershipCode1.OptionsColumn.AllowEdit = false;
            this.colOwnershipCode1.OptionsColumn.AllowFocus = false;
            this.colOwnershipCode1.OptionsColumn.ReadOnly = true;
            this.colOwnershipCode1.Visible = true;
            this.colOwnershipCode1.VisibleIndex = 2;
            this.colOwnershipCode1.Width = 129;
            // 
            // colOwnershipName1
            // 
            this.colOwnershipName1.Caption = "Ownership Name";
            this.colOwnershipName1.FieldName = "OwnershipName";
            this.colOwnershipName1.Name = "colOwnershipName1";
            this.colOwnershipName1.OptionsColumn.AllowEdit = false;
            this.colOwnershipName1.OptionsColumn.AllowFocus = false;
            this.colOwnershipName1.OptionsColumn.ReadOnly = true;
            this.colOwnershipName1.Visible = true;
            this.colOwnershipName1.VisibleIndex = 3;
            this.colOwnershipName1.Width = 288;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(4, 294);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(61, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // WorkOrderDescriptionTextEdit
            // 
            this.WorkOrderDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01388ATActionEditBindingSource, "WorkOrderDescription", true));
            this.WorkOrderDescriptionTextEdit.Location = new System.Drawing.Point(125, 265);
            this.WorkOrderDescriptionTextEdit.MenuManager = this.barManager1;
            this.WorkOrderDescriptionTextEdit.Name = "WorkOrderDescriptionTextEdit";
            this.WorkOrderDescriptionTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, true, editorButtonImageOptions27, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject105, serializableAppearanceObject106, serializableAppearanceObject107, serializableAppearanceObject108, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.WorkOrderDescriptionTextEdit.Properties.ReadOnly = true;
            this.WorkOrderDescriptionTextEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.WorkOrderDescriptionTextEdit.Size = new System.Drawing.Size(956, 20);
            this.WorkOrderDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "View Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to open the Work Order this action is linked to.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.WorkOrderDescriptionTextEdit.SuperTip = superToolTip8;
            this.WorkOrderDescriptionTextEdit.TabIndex = 47;
            this.WorkOrderDescriptionTextEdit.TabStop = false;
            this.WorkOrderDescriptionTextEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.WorkOrderDescriptionTextEdit_ButtonClick);
            // 
            // ItemForintActionID
            // 
            this.ItemForintActionID.Control = this.intActionIDTextEdit;
            this.ItemForintActionID.CustomizationFormText = "Action ID:";
            this.ItemForintActionID.Location = new System.Drawing.Point(0, 217);
            this.ItemForintActionID.Name = "ItemForintActionID";
            this.ItemForintActionID.Size = new System.Drawing.Size(704, 24);
            this.ItemForintActionID.Text = "Action ID:";
            this.ItemForintActionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintBudgetID
            // 
            this.ItemForintBudgetID.Control = this.intBudgetIDTextEdit;
            this.ItemForintBudgetID.CustomizationFormText = "Budget:";
            this.ItemForintBudgetID.Location = new System.Drawing.Point(0, 191);
            this.ItemForintBudgetID.Name = "ItemForintBudgetID";
            this.ItemForintBudgetID.Size = new System.Drawing.Size(704, 24);
            this.ItemForintBudgetID.Text = "Budget:";
            this.ItemForintBudgetID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintCostCentreID
            // 
            this.ItemForintCostCentreID.Control = this.intCostCentreIDTextEdit;
            this.ItemForintCostCentreID.CustomizationFormText = "Cost Centre ID:";
            this.ItemForintCostCentreID.Location = new System.Drawing.Point(0, 217);
            this.ItemForintCostCentreID.Name = "ItemForintCostCentreID";
            this.ItemForintCostCentreID.Size = new System.Drawing.Size(704, 24);
            this.ItemForintCostCentreID.Text = "Cost Centre ID:";
            this.ItemForintCostCentreID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForDateAdded
            // 
            this.ItemForDateAdded.Control = this.DateAddedDateEdit;
            this.ItemForDateAdded.CustomizationFormText = "Date Added:";
            this.ItemForDateAdded.Location = new System.Drawing.Point(0, 191);
            this.ItemForDateAdded.Name = "ItemForDateAdded";
            this.ItemForDateAdded.Size = new System.Drawing.Size(704, 24);
            this.ItemForDateAdded.Text = "Date Added:";
            this.ItemForDateAdded.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForGUID
            // 
            this.ItemForGUID.Control = this.GUIDTextEdit;
            this.ItemForGUID.CustomizationFormText = "GUID:";
            this.ItemForGUID.Location = new System.Drawing.Point(0, 191);
            this.ItemForGUID.Name = "ItemForGUID";
            this.ItemForGUID.Size = new System.Drawing.Size(704, 24);
            this.ItemForGUID.Text = "GUID:";
            this.ItemForGUID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintInspectionID
            // 
            this.ItemForintInspectionID.Control = this.intInspectionIDTextEdit;
            this.ItemForintInspectionID.CustomizationFormText = "Inspection ID:";
            this.ItemForintInspectionID.Location = new System.Drawing.Point(0, 131);
            this.ItemForintInspectionID.Name = "ItemForintInspectionID";
            this.ItemForintInspectionID.Size = new System.Drawing.Size(704, 24);
            this.ItemForintInspectionID.Text = "Inspection ID:";
            this.ItemForintInspectionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintJobRateActual
            // 
            this.ItemForintJobRateActual.Control = this.intJobRateActualSpinEdit;
            this.ItemForintJobRateActual.CustomizationFormText = "Job Rate Actual ID:";
            this.ItemForintJobRateActual.Location = new System.Drawing.Point(0, 191);
            this.ItemForintJobRateActual.Name = "ItemForintJobRateActual";
            this.ItemForintJobRateActual.Size = new System.Drawing.Size(704, 24);
            this.ItemForintJobRateActual.Text = "Job Rate Actual ID:";
            this.ItemForintJobRateActual.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintPrevActionID
            // 
            this.ItemForintPrevActionID.Control = this.intPrevActionIDTextEdit;
            this.ItemForintPrevActionID.CustomizationFormText = "Previous Action ID:";
            this.ItemForintPrevActionID.Location = new System.Drawing.Point(0, 191);
            this.ItemForintPrevActionID.Name = "ItemForintPrevActionID";
            this.ItemForintPrevActionID.Size = new System.Drawing.Size(704, 24);
            this.ItemForintPrevActionID.Text = "Previous Action ID:";
            this.ItemForintPrevActionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintSortOrder
            // 
            this.ItemForintSortOrder.Control = this.intSortOrderSpinEdit;
            this.ItemForintSortOrder.CustomizationFormText = "Sort Order:";
            this.ItemForintSortOrder.Location = new System.Drawing.Point(0, 191);
            this.ItemForintSortOrder.Name = "ItemForintSortOrder";
            this.ItemForintSortOrder.Size = new System.Drawing.Size(704, 24);
            this.ItemForintSortOrder.Text = "Sort Order:";
            this.ItemForintSortOrder.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintWorkOrderID
            // 
            this.ItemForintWorkOrderID.Control = this.intWorkOrderIDTextEdit;
            this.ItemForintWorkOrderID.CustomizationFormText = "Work Order ID:";
            this.ItemForintWorkOrderID.Location = new System.Drawing.Point(0, 191);
            this.ItemForintWorkOrderID.Name = "ItemForintWorkOrderID";
            this.ItemForintWorkOrderID.Size = new System.Drawing.Size(704, 24);
            this.ItemForintWorkOrderID.Text = "Work Order ID:";
            this.ItemForintWorkOrderID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForintJobRateBudgeted
            // 
            this.ItemForintJobRateBudgeted.Control = this.intJobRateBudgetedSpinEdit;
            this.ItemForintJobRateBudgeted.CustomizationFormText = "Job Rate Budgeted ID:";
            this.ItemForintJobRateBudgeted.Location = new System.Drawing.Point(0, 301);
            this.ItemForintJobRateBudgeted.Name = "ItemForintJobRateBudgeted";
            this.ItemForintJobRateBudgeted.Size = new System.Drawing.Size(704, 24);
            this.ItemForintJobRateBudgeted.Text = "Job Rate Budgeted ID:";
            this.ItemForintJobRateBudgeted.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1093, 651);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForInspectionReference,
            this.ItemFordtDueDate,
            this.ItemFordtDoneDate,
            this.ItemForintActionBy,
            this.ItemForintSupervisor,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.ItemForintAction,
            this.ItemForstrJobNumber,
            this.tabbedControlGroup1,
            this.emptySpaceItem8,
            this.emptySpaceItem9,
            this.emptySpaceItem10,
            this.ItemForintPriorityID,
            this.emptySpaceItem11,
            this.ItemForstrNearestHouse,
            this.ItemForWorkOrderDescription,
            this.emptySpaceItem13,
            this.emptySpaceItem14});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1073, 621);
            // 
            // ItemForInspectionReference
            // 
            this.ItemForInspectionReference.AllowHide = false;
            this.ItemForInspectionReference.Control = this.InspectionReferenceButtonEdit;
            this.ItemForInspectionReference.CustomizationFormText = "Linked to Inspection:";
            this.ItemForInspectionReference.Location = new System.Drawing.Point(0, 23);
            this.ItemForInspectionReference.Name = "ItemForInspectionReference";
            this.ItemForInspectionReference.Size = new System.Drawing.Size(1073, 24);
            this.ItemForInspectionReference.Text = "Linked to Inspection:";
            this.ItemForInspectionReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemFordtDueDate
            // 
            this.ItemFordtDueDate.Control = this.dtDueDateDateEdit;
            this.ItemFordtDueDate.CustomizationFormText = "Due Date:";
            this.ItemFordtDueDate.Location = new System.Drawing.Point(0, 107);
            this.ItemFordtDueDate.MaxSize = new System.Drawing.Size(283, 24);
            this.ItemFordtDueDate.MinSize = new System.Drawing.Size(283, 24);
            this.ItemFordtDueDate.Name = "ItemFordtDueDate";
            this.ItemFordtDueDate.Size = new System.Drawing.Size(283, 24);
            this.ItemFordtDueDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtDueDate.Text = "Due Date:";
            this.ItemFordtDueDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemFordtDoneDate
            // 
            this.ItemFordtDoneDate.Control = this.dtDoneDateDateEdit;
            this.ItemFordtDoneDate.CustomizationFormText = "Done Date:";
            this.ItemFordtDoneDate.Location = new System.Drawing.Point(0, 131);
            this.ItemFordtDoneDate.MaxSize = new System.Drawing.Size(283, 24);
            this.ItemFordtDoneDate.MinSize = new System.Drawing.Size(283, 24);
            this.ItemFordtDoneDate.Name = "ItemFordtDoneDate";
            this.ItemFordtDoneDate.Size = new System.Drawing.Size(283, 24);
            this.ItemFordtDoneDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtDoneDate.Text = "Done Date:";
            this.ItemFordtDoneDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForintActionBy
            // 
            this.ItemForintActionBy.Control = this.intActionByGridLookUpEdit;
            this.ItemForintActionBy.CustomizationFormText = "Action By:";
            this.ItemForintActionBy.Location = new System.Drawing.Point(0, 165);
            this.ItemForintActionBy.Name = "ItemForintActionBy";
            this.ItemForintActionBy.Size = new System.Drawing.Size(1073, 26);
            this.ItemForintActionBy.Text = "Action By:";
            this.ItemForintActionBy.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForintSupervisor
            // 
            this.ItemForintSupervisor.Control = this.intSupervisorGridLookUpEdit;
            this.ItemForintSupervisor.CustomizationFormText = "Supervisor:";
            this.ItemForintSupervisor.Location = new System.Drawing.Point(0, 191);
            this.ItemForintSupervisor.Name = "ItemForintSupervisor";
            this.ItemForintSupervisor.Size = new System.Drawing.Size(1073, 26);
            this.ItemForintSupervisor.Text = "Supervisor:";
            this.ItemForintSupervisor.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(115, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(115, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(115, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(304, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(769, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(115, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(189, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForintAction
            // 
            this.ItemForintAction.AllowHide = false;
            this.ItemForintAction.Control = this.intActionGridLookUpEdit;
            this.ItemForintAction.CustomizationFormText = "Action:";
            this.ItemForintAction.Location = new System.Drawing.Point(0, 47);
            this.ItemForintAction.Name = "ItemForintAction";
            this.ItemForintAction.Size = new System.Drawing.Size(1073, 26);
            this.ItemForintAction.Text = "Action:";
            this.ItemForintAction.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForstrJobNumber
            // 
            this.ItemForstrJobNumber.Control = this.strJobNumberButtonEdit;
            this.ItemForstrJobNumber.CustomizationFormText = "Job Number:";
            this.ItemForstrJobNumber.Location = new System.Drawing.Point(0, 73);
            this.ItemForstrJobNumber.Name = "ItemForstrJobNumber";
            this.ItemForstrJobNumber.Size = new System.Drawing.Size(1073, 24);
            this.ItemForstrJobNumber.Text = "Job Number:";
            this.ItemForstrJobNumber.TextSize = new System.Drawing.Size(110, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 311);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1073, 310);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Costings";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintScheduleOfRates,
            this.layoutControlGroup8,
            this.layoutControlGroup9,
            this.ItemForintOwnershipID,
            this.ItemForCostCentreCode,
            this.ItemFormonJobWorkUnits,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.splitterItem1,
            this.emptySpaceItem15});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1049, 262);
            this.layoutControlGroup7.Text = "Costings";
            // 
            // ItemForintScheduleOfRates
            // 
            this.ItemForintScheduleOfRates.Control = this.intScheduleOfRatesCheckEdit;
            this.ItemForintScheduleOfRates.CustomizationFormText = "On Schedule Of Rates:";
            this.ItemForintScheduleOfRates.Location = new System.Drawing.Point(0, 87);
            this.ItemForintScheduleOfRates.MaxSize = new System.Drawing.Size(225, 23);
            this.ItemForintScheduleOfRates.MinSize = new System.Drawing.Size(225, 23);
            this.ItemForintScheduleOfRates.Name = "ItemForintScheduleOfRates";
            this.ItemForintScheduleOfRates.Size = new System.Drawing.Size(225, 23);
            this.ItemForintScheduleOfRates.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForintScheduleOfRates.Text = "On Schedule Of Rates:";
            this.ItemForintScheduleOfRates.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Budgeted";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemFormonJobRateBudgeted,
            this.ItemFormonBudgetedCost,
            this.ItemForJobRateDesc,
            this.emptySpaceItem5});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 120);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(524, 142);
            this.layoutControlGroup8.Text = "Budgeted";
            // 
            // ItemFormonJobRateBudgeted
            // 
            this.ItemFormonJobRateBudgeted.Control = this.monJobRateBudgetedSpinEdit;
            this.ItemFormonJobRateBudgeted.CustomizationFormText = "Budgeted Job Rate:";
            this.ItemFormonJobRateBudgeted.Location = new System.Drawing.Point(0, 24);
            this.ItemFormonJobRateBudgeted.Name = "ItemFormonJobRateBudgeted";
            this.ItemFormonJobRateBudgeted.Size = new System.Drawing.Size(500, 24);
            this.ItemFormonJobRateBudgeted.Text = "Job Rate:";
            this.ItemFormonJobRateBudgeted.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemFormonBudgetedCost
            // 
            this.ItemFormonBudgetedCost.Control = this.monBudgetedCostSpinEdit;
            this.ItemFormonBudgetedCost.CustomizationFormText = "Budgeted Cost:";
            this.ItemFormonBudgetedCost.Location = new System.Drawing.Point(0, 72);
            this.ItemFormonBudgetedCost.Name = "ItemFormonBudgetedCost";
            this.ItemFormonBudgetedCost.Size = new System.Drawing.Size(500, 24);
            this.ItemFormonBudgetedCost.Text = "Budgeted Cost:";
            this.ItemFormonBudgetedCost.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForJobRateDesc
            // 
            this.ItemForJobRateDesc.Control = this.JobRateDescButtonEdit;
            this.ItemForJobRateDesc.CustomizationFormText = "Job Rate Description:";
            this.ItemForJobRateDesc.Location = new System.Drawing.Point(0, 0);
            this.ItemForJobRateDesc.Name = "ItemForJobRateDesc";
            this.ItemForJobRateDesc.Size = new System.Drawing.Size(500, 24);
            this.ItemForJobRateDesc.Text = "Rate Description:";
            this.ItemForJobRateDesc.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(500, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Actual  [Set from Work Orders]";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemFormonJobRateActual,
            this.ItemFormonActualCost,
            this.ItemFormonDiscountRate,
            this.ItemForActualJobRateDesc});
            this.layoutControlGroup9.Location = new System.Drawing.Point(530, 120);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(519, 142);
            this.layoutControlGroup9.Text = "Actual  [Set from Work Orders]";
            // 
            // ItemFormonJobRateActual
            // 
            this.ItemFormonJobRateActual.Control = this.monJobRateActualSpinEdit;
            this.ItemFormonJobRateActual.CustomizationFormText = "Actual Job Rate:";
            this.ItemFormonJobRateActual.Location = new System.Drawing.Point(0, 24);
            this.ItemFormonJobRateActual.Name = "ItemFormonJobRateActual";
            this.ItemFormonJobRateActual.Size = new System.Drawing.Size(495, 24);
            this.ItemFormonJobRateActual.Text = "Job Rate:";
            this.ItemFormonJobRateActual.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemFormonActualCost
            // 
            this.ItemFormonActualCost.Control = this.monActualCostSpinEdit;
            this.ItemFormonActualCost.CustomizationFormText = "Actual Cost:";
            this.ItemFormonActualCost.Location = new System.Drawing.Point(0, 72);
            this.ItemFormonActualCost.Name = "ItemFormonActualCost";
            this.ItemFormonActualCost.Size = new System.Drawing.Size(495, 24);
            this.ItemFormonActualCost.Text = "Actual Cost:";
            this.ItemFormonActualCost.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemFormonDiscountRate
            // 
            this.ItemFormonDiscountRate.Control = this.monDiscountRateSpinEdit;
            this.ItemFormonDiscountRate.CustomizationFormText = "Discount Rate:";
            this.ItemFormonDiscountRate.Location = new System.Drawing.Point(0, 48);
            this.ItemFormonDiscountRate.Name = "ItemFormonDiscountRate";
            this.ItemFormonDiscountRate.Size = new System.Drawing.Size(495, 24);
            this.ItemFormonDiscountRate.Text = "Discount Rate:";
            this.ItemFormonDiscountRate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForActualJobRateDesc
            // 
            this.ItemForActualJobRateDesc.Control = this.ActualJobRateDescTextEdit;
            this.ItemForActualJobRateDesc.CustomizationFormText = "Rate Description:";
            this.ItemForActualJobRateDesc.Location = new System.Drawing.Point(0, 0);
            this.ItemForActualJobRateDesc.Name = "ItemForActualJobRateDesc";
            this.ItemForActualJobRateDesc.Size = new System.Drawing.Size(495, 24);
            this.ItemForActualJobRateDesc.Text = "Rate Description:";
            this.ItemForActualJobRateDesc.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForintOwnershipID
            // 
            this.ItemForintOwnershipID.Control = this.intOwnershipIDGridLookUpEdit;
            this.ItemForintOwnershipID.CustomizationFormText = "Ownership:";
            this.ItemForintOwnershipID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintOwnershipID.Name = "ItemForintOwnershipID";
            this.ItemForintOwnershipID.Size = new System.Drawing.Size(1049, 26);
            this.ItemForintOwnershipID.Text = "Ownership:";
            this.ItemForintOwnershipID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForCostCentreCode
            // 
            this.ItemForCostCentreCode.Control = this.CostCentreCodeButtonEdit;
            this.ItemForCostCentreCode.CustomizationFormText = "Cost Centre Code:";
            this.ItemForCostCentreCode.Location = new System.Drawing.Point(0, 26);
            this.ItemForCostCentreCode.Name = "ItemForCostCentreCode";
            this.ItemForCostCentreCode.Size = new System.Drawing.Size(1049, 26);
            this.ItemForCostCentreCode.Text = "Cost Centre Code:";
            this.ItemForCostCentreCode.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemFormonJobWorkUnits
            // 
            this.ItemFormonJobWorkUnits.Control = this.monJobWorkUnitsSpinEdit;
            this.ItemFormonJobWorkUnits.CustomizationFormText = "Job Work Units:";
            this.ItemFormonJobWorkUnits.Location = new System.Drawing.Point(0, 63);
            this.ItemFormonJobWorkUnits.MaxSize = new System.Drawing.Size(0, 24);
            this.ItemFormonJobWorkUnits.MinSize = new System.Drawing.Size(168, 24);
            this.ItemFormonJobWorkUnits.Name = "ItemFormonJobWorkUnits";
            this.ItemFormonJobWorkUnits.Size = new System.Drawing.Size(1049, 24);
            this.ItemFormonJobWorkUnits.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFormonJobWorkUnits.Text = "Job Work Units:";
            this.ItemFormonJobWorkUnits.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 52);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1049, 11);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 110);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(1049, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(524, 120);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 142);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(225, 87);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(824, 23);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImageOptions.Image")));
            this.layoutControlGroup4.CustomizationFormText = "Linked Pictures";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedPicturesGrid});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1049, 262);
            this.layoutControlGroup4.Text = "Linked Pictures";
            // 
            // ItemForLinkedPicturesGrid
            // 
            this.ItemForLinkedPicturesGrid.Control = this.gridControl39;
            this.ItemForLinkedPicturesGrid.CustomizationFormText = "Linked Pictures Grid:";
            this.ItemForLinkedPicturesGrid.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedPicturesGrid.Name = "ItemForLinkedPicturesGrid";
            this.ItemForLinkedPicturesGrid.Size = new System.Drawing.Size(1049, 262);
            this.ItemForLinkedPicturesGrid.Text = "Linked Pictures Grid:";
            this.ItemForLinkedPicturesGrid.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForLinkedPicturesGrid.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "User Fields";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrUser1,
            this.ItemForstrUser2,
            this.ItemForstrUser3,
            this.ItemForUserPickList1,
            this.ItemForUserPickList2,
            this.ItemForUserPickList3,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem16,
            this.emptySpaceItem17,
            this.emptySpaceItem18});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1049, 262);
            this.layoutControlGroup5.Text = "User Fields";
            // 
            // ItemForstrUser1
            // 
            this.ItemForstrUser1.Control = this.strUser1TextEdit;
            this.ItemForstrUser1.CustomizationFormText = "Text 1:";
            this.ItemForstrUser1.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrUser1.MaxSize = new System.Drawing.Size(340, 24);
            this.ItemForstrUser1.MinSize = new System.Drawing.Size(340, 24);
            this.ItemForstrUser1.Name = "ItemForstrUser1";
            this.ItemForstrUser1.Size = new System.Drawing.Size(340, 24);
            this.ItemForstrUser1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrUser1.Text = "Text 1:";
            this.ItemForstrUser1.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForstrUser2
            // 
            this.ItemForstrUser2.Control = this.strUser2TextEdit;
            this.ItemForstrUser2.CustomizationFormText = "Text 2:";
            this.ItemForstrUser2.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrUser2.MaxSize = new System.Drawing.Size(340, 24);
            this.ItemForstrUser2.MinSize = new System.Drawing.Size(340, 24);
            this.ItemForstrUser2.Name = "ItemForstrUser2";
            this.ItemForstrUser2.Size = new System.Drawing.Size(340, 24);
            this.ItemForstrUser2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrUser2.Text = "Text 2:";
            this.ItemForstrUser2.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForstrUser3
            // 
            this.ItemForstrUser3.Control = this.strUser3TextEdit;
            this.ItemForstrUser3.CustomizationFormText = "Text 3:";
            this.ItemForstrUser3.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrUser3.MaxSize = new System.Drawing.Size(340, 24);
            this.ItemForstrUser3.MinSize = new System.Drawing.Size(340, 24);
            this.ItemForstrUser3.Name = "ItemForstrUser3";
            this.ItemForstrUser3.Size = new System.Drawing.Size(340, 24);
            this.ItemForstrUser3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrUser3.Text = "Text 3:";
            this.ItemForstrUser3.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForUserPickList1
            // 
            this.ItemForUserPickList1.Control = this.UserPickList1GridLookUpEdit;
            this.ItemForUserPickList1.CustomizationFormText = "Pick List 1:";
            this.ItemForUserPickList1.Location = new System.Drawing.Point(0, 82);
            this.ItemForUserPickList1.Name = "ItemForUserPickList1";
            this.ItemForUserPickList1.Size = new System.Drawing.Size(1049, 26);
            this.ItemForUserPickList1.Text = "Pick List 1:";
            this.ItemForUserPickList1.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForUserPickList2
            // 
            this.ItemForUserPickList2.Control = this.UserPickList2GridLookUpEdit;
            this.ItemForUserPickList2.CustomizationFormText = "Pick List 2:";
            this.ItemForUserPickList2.Location = new System.Drawing.Point(0, 108);
            this.ItemForUserPickList2.Name = "ItemForUserPickList2";
            this.ItemForUserPickList2.Size = new System.Drawing.Size(1049, 26);
            this.ItemForUserPickList2.Text = "Pick List 2:";
            this.ItemForUserPickList2.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForUserPickList3
            // 
            this.ItemForUserPickList3.Control = this.UserPickList3GridLookUpEdit;
            this.ItemForUserPickList3.CustomizationFormText = "Pick List 3:";
            this.ItemForUserPickList3.Location = new System.Drawing.Point(0, 134);
            this.ItemForUserPickList3.Name = "ItemForUserPickList3";
            this.ItemForUserPickList3.Size = new System.Drawing.Size(1049, 26);
            this.ItemForUserPickList3.Text = "Pick List 3:";
            this.ItemForUserPickList3.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 160);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1049, 102);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(1049, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(340, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(709, 24);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(340, 24);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(709, 24);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(340, 48);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(709, 24);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup6.CaptionImageOptions.Image")));
            this.layoutControlGroup6.CustomizationFormText = "Remarks";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1049, 262);
            this.layoutControlGroup6.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(1049, 262);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 97);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1073, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 301);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1073, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 155);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(1073, 10);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForintPriorityID
            // 
            this.ItemForintPriorityID.Control = this.intPriorityIDGridLookUpEdit;
            this.ItemForintPriorityID.CustomizationFormText = "Priority:";
            this.ItemForintPriorityID.Location = new System.Drawing.Point(0, 217);
            this.ItemForintPriorityID.Name = "ItemForintPriorityID";
            this.ItemForintPriorityID.Size = new System.Drawing.Size(1073, 26);
            this.ItemForintPriorityID.Text = "Priority:";
            this.ItemForintPriorityID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 243);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(1073, 10);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrNearestHouse
            // 
            this.ItemForstrNearestHouse.Control = this.strNearestHouseTextEdit;
            this.ItemForstrNearestHouse.CustomizationFormText = "Nearest House Name:";
            this.ItemForstrNearestHouse.Location = new System.Drawing.Point(0, 277);
            this.ItemForstrNearestHouse.Name = "ItemForstrNearestHouse";
            this.ItemForstrNearestHouse.Size = new System.Drawing.Size(1073, 24);
            this.ItemForstrNearestHouse.Text = "Nearest House Name:";
            this.ItemForstrNearestHouse.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForWorkOrderDescription
            // 
            this.ItemForWorkOrderDescription.Control = this.WorkOrderDescriptionTextEdit;
            this.ItemForWorkOrderDescription.CustomizationFormText = "Work Order:";
            this.ItemForWorkOrderDescription.Location = new System.Drawing.Point(0, 253);
            this.ItemForWorkOrderDescription.Name = "ItemForWorkOrderDescription";
            this.ItemForWorkOrderDescription.Size = new System.Drawing.Size(1073, 24);
            this.ItemForWorkOrderDescription.Text = "Work Order:";
            this.ItemForWorkOrderDescription.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(283, 107);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(790, 24);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(283, 131);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(790, 24);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem12});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 621);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1073, 10);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(1073, 10);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp01388_AT_Action_EditTableAdapter
            // 
            this.sp01388_AT_Action_EditTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLastAction});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelLastAction
            // 
            this.dockPanelLastAction.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelLastAction.Appearance.Options.UseBackColor = true;
            this.dockPanelLastAction.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLastAction.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelLastAction.ID = new System.Guid("bb61d5c6-8866-45c1-84a7-4b6fe41d9640");
            this.dockPanelLastAction.Location = new System.Drawing.Point(741, 26);
            this.dockPanelLastAction.Name = "dockPanelLastAction";
            this.dockPanelLastAction.Options.ShowCloseButton = false;
            this.dockPanelLastAction.OriginalSize = new System.Drawing.Size(369, 200);
            this.dockPanelLastAction.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelLastAction.SavedIndex = 0;
            this.dockPanelLastAction.Size = new System.Drawing.Size(369, 646);
            this.dockPanelLastAction.Text = "Last Action Details";
            this.dockPanelLastAction.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelLastAction.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelLastInspection_ClosingPanel);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.popupContainerControl1);
            this.dockPanel1_Container.Controls.Add(this.vGridControl1);
            this.dockPanel1_Container.Controls.Add(this.checkEditSychronise);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(363, 614);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // vGridControl1
            // 
            this.vGridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vGridControl1.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.vGridControl1.DataSource = this.sp01390ATActionEditPreviousActionsBindingSource;
            this.vGridControl1.Location = new System.Drawing.Point(3, 22);
            this.vGridControl1.Name = "vGridControl1";
            this.vGridControl1.RecordWidth = 158;
            this.vGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.vGridControl1.RowHeaderWidth = 159;
            this.vGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow1,
            this.categoryRow2});
            this.vGridControl1.Size = new System.Drawing.Size(357, 589);
            this.vGridControl1.TabIndex = 14;
            this.vGridControl1.CustomDrawRowHeaderCell += new DevExpress.XtraVerticalGrid.Events.CustomDrawRowHeaderCellEventHandler(this.vGridControl1_CustomDrawRowHeaderCell);
            // 
            // sp01390ATActionEditPreviousActionsBindingSource
            // 
            this.sp01390ATActionEditPreviousActionsBindingSource.DataMember = "sp01390_AT_Action_Edit_Previous_Actions";
            this.sp01390ATActionEditPreviousActionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "##0.00 %";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // categoryRow1
            // 
            this.categoryRow1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowDistrictID,
            this.rowLocalityID,
            this.rowTreeID,
            this.rowTreeReference,
            this.rowTreeMappingID,
            this.rowInspectionReferenceButtonEdit,
            this.rowInspectionDate,
            this.rowInspectionID});
            this.categoryRow1.Expanded = false;
            this.categoryRow1.Name = "categoryRow1";
            this.categoryRow1.Properties.Caption = "Inspection Details";
            // 
            // rowDistrictID
            // 
            this.rowDistrictID.Name = "rowDistrictID";
            this.rowDistrictID.Properties.Caption = "District ID";
            this.rowDistrictID.Properties.FieldName = "DistrictID";
            this.rowDistrictID.Properties.ReadOnly = true;
            this.rowDistrictID.Visible = false;
            // 
            // rowLocalityID
            // 
            this.rowLocalityID.Name = "rowLocalityID";
            this.rowLocalityID.Properties.Caption = "Locality ID";
            this.rowLocalityID.Properties.FieldName = "LocalityID";
            this.rowLocalityID.Properties.ReadOnly = true;
            this.rowLocalityID.Visible = false;
            // 
            // rowTreeID
            // 
            this.rowTreeID.Name = "rowTreeID";
            this.rowTreeID.Properties.Caption = "Tree ID";
            this.rowTreeID.Properties.FieldName = "TreeID";
            this.rowTreeID.Properties.ReadOnly = true;
            this.rowTreeID.Visible = false;
            // 
            // rowTreeReference
            // 
            this.rowTreeReference.Name = "rowTreeReference";
            this.rowTreeReference.Properties.Caption = "Tree Reference";
            this.rowTreeReference.Properties.FieldName = "TreeReference";
            this.rowTreeReference.Properties.ReadOnly = true;
            // 
            // rowTreeMappingID
            // 
            this.rowTreeMappingID.Name = "rowTreeMappingID";
            this.rowTreeMappingID.Properties.Caption = "Tree Mapping ID";
            this.rowTreeMappingID.Properties.FieldName = "TreeMappingID";
            this.rowTreeMappingID.Properties.ReadOnly = true;
            // 
            // rowInspectionReferenceButtonEdit
            // 
            this.rowInspectionReferenceButtonEdit.Name = "rowInspectionReferenceButtonEdit";
            this.rowInspectionReferenceButtonEdit.Properties.Caption = "Inspection Reference";
            this.rowInspectionReferenceButtonEdit.Properties.FieldName = "InspectionReference";
            this.rowInspectionReferenceButtonEdit.Properties.ReadOnly = true;
            // 
            // rowInspectionDate
            // 
            this.rowInspectionDate.Height = 19;
            this.rowInspectionDate.Name = "rowInspectionDate";
            this.rowInspectionDate.Properties.Caption = "Inspection Date";
            this.rowInspectionDate.Properties.FieldName = "InspectionDate";
            this.rowInspectionDate.Properties.ReadOnly = true;
            // 
            // rowInspectionID
            // 
            this.rowInspectionID.Name = "rowInspectionID";
            this.rowInspectionID.Properties.Caption = "Inspection ID";
            this.rowInspectionID.Properties.FieldName = "InspectionID";
            this.rowInspectionID.Properties.ReadOnly = true;
            this.rowInspectionID.Visible = false;
            // 
            // categoryRow2
            // 
            this.categoryRow2.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowActionID,
            this.rowintActionGridLookUpEdit,
            this.rowstrJobNumberButtonEdit,
            this.rowdtDueDateDateEdit,
            this.rowdtDoneDateDateEdit,
            this.rowintActionByGridLookUpEdit,
            this.rowintSupervisorGridLookUpEdit,
            this.rowintPriorityIDGridLookUpEdit,
            this.rowWorkOrderDescriptionTextEdit,
            this.categoryRow4,
            this.categoryRow7,
            this.editorRow18,
            this.editorRow19,
            this.rowstrRemarksMemoEdit});
            this.categoryRow2.Name = "categoryRow2";
            this.categoryRow2.Properties.Caption = "Action Details";
            // 
            // rowActionID
            // 
            this.rowActionID.Name = "rowActionID";
            this.rowActionID.Properties.Caption = "Action ID";
            this.rowActionID.Properties.FieldName = "ActionID";
            this.rowActionID.Properties.ReadOnly = true;
            this.rowActionID.Visible = false;
            // 
            // rowintActionGridLookUpEdit
            // 
            this.rowintActionGridLookUpEdit.Name = "rowintActionGridLookUpEdit";
            this.rowintActionGridLookUpEdit.Properties.Caption = "Action";
            this.rowintActionGridLookUpEdit.Properties.FieldName = "Action";
            this.rowintActionGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowstrJobNumberButtonEdit
            // 
            this.rowstrJobNumberButtonEdit.Name = "rowstrJobNumberButtonEdit";
            this.rowstrJobNumberButtonEdit.Properties.Caption = "Job Number";
            this.rowstrJobNumberButtonEdit.Properties.FieldName = "ActionJobNumber";
            this.rowstrJobNumberButtonEdit.Properties.ReadOnly = true;
            // 
            // rowdtDueDateDateEdit
            // 
            this.rowdtDueDateDateEdit.Name = "rowdtDueDateDateEdit";
            this.rowdtDueDateDateEdit.Properties.Caption = "Due Date";
            this.rowdtDueDateDateEdit.Properties.FieldName = "ActionDueDate";
            this.rowdtDueDateDateEdit.Properties.ReadOnly = true;
            // 
            // rowdtDoneDateDateEdit
            // 
            this.rowdtDoneDateDateEdit.Name = "rowdtDoneDateDateEdit";
            this.rowdtDoneDateDateEdit.Properties.Caption = "Done Date";
            this.rowdtDoneDateDateEdit.Properties.FieldName = "ActionDoneDate";
            this.rowdtDoneDateDateEdit.Properties.ReadOnly = true;
            // 
            // rowintActionByGridLookUpEdit
            // 
            this.rowintActionByGridLookUpEdit.Name = "rowintActionByGridLookUpEdit";
            this.rowintActionByGridLookUpEdit.Properties.Caption = "Action By";
            this.rowintActionByGridLookUpEdit.Properties.FieldName = "ActionBy";
            this.rowintActionByGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintSupervisorGridLookUpEdit
            // 
            this.rowintSupervisorGridLookUpEdit.Name = "rowintSupervisorGridLookUpEdit";
            this.rowintSupervisorGridLookUpEdit.Properties.Caption = "Supervisor";
            this.rowintSupervisorGridLookUpEdit.Properties.FieldName = "ActionSupervisor";
            this.rowintSupervisorGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintPriorityIDGridLookUpEdit
            // 
            this.rowintPriorityIDGridLookUpEdit.Name = "rowintPriorityIDGridLookUpEdit";
            this.rowintPriorityIDGridLookUpEdit.Properties.Caption = "Priority";
            this.rowintPriorityIDGridLookUpEdit.Properties.FieldName = "ActionPriority";
            this.rowintPriorityIDGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowWorkOrderDescriptionTextEdit
            // 
            this.rowWorkOrderDescriptionTextEdit.Name = "rowWorkOrderDescriptionTextEdit";
            this.rowWorkOrderDescriptionTextEdit.Properties.Caption = "Work Order";
            this.rowWorkOrderDescriptionTextEdit.Properties.FieldName = "ActionWorkOrder";
            this.rowWorkOrderDescriptionTextEdit.Properties.ReadOnly = true;
            // 
            // categoryRow4
            // 
            this.categoryRow4.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowintOwnershipIDGridLookUpEdit,
            this.rowCostCentreCodeButtonEdit,
            this.rowBudgetID,
            this.rowmonJobWorkUnitsSpinEdit,
            this.rowintScheduleOfRatesCheckEdit,
            this.categoryRow5,
            this.categoryRow6,
            this.editorRow17});
            this.categoryRow4.Name = "categoryRow4";
            this.categoryRow4.Properties.Caption = "Costings";
            // 
            // rowintOwnershipIDGridLookUpEdit
            // 
            this.rowintOwnershipIDGridLookUpEdit.Name = "rowintOwnershipIDGridLookUpEdit";
            this.rowintOwnershipIDGridLookUpEdit.Properties.Caption = "Ownership";
            this.rowintOwnershipIDGridLookUpEdit.Properties.FieldName = "ActionOwnership";
            this.rowintOwnershipIDGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowCostCentreCodeButtonEdit
            // 
            this.rowCostCentreCodeButtonEdit.Name = "rowCostCentreCodeButtonEdit";
            this.rowCostCentreCodeButtonEdit.Properties.Caption = "Cost Centre";
            this.rowCostCentreCodeButtonEdit.Properties.FieldName = "ActionCostCentre";
            this.rowCostCentreCodeButtonEdit.Properties.ReadOnly = true;
            // 
            // rowBudgetID
            // 
            this.rowBudgetID.Name = "rowBudgetID";
            this.rowBudgetID.Properties.Caption = "Budget";
            this.rowBudgetID.Properties.FieldName = "ActionBudget";
            this.rowBudgetID.Properties.ReadOnly = true;
            this.rowBudgetID.Visible = false;
            // 
            // rowmonJobWorkUnitsSpinEdit
            // 
            this.rowmonJobWorkUnitsSpinEdit.Name = "rowmonJobWorkUnitsSpinEdit";
            this.rowmonJobWorkUnitsSpinEdit.Properties.Caption = "Work Units";
            this.rowmonJobWorkUnitsSpinEdit.Properties.FieldName = "ActionWorkUnits";
            this.rowmonJobWorkUnitsSpinEdit.Properties.ReadOnly = true;
            // 
            // rowintScheduleOfRatesCheckEdit
            // 
            this.rowintScheduleOfRatesCheckEdit.Name = "rowintScheduleOfRatesCheckEdit";
            this.rowintScheduleOfRatesCheckEdit.Properties.Caption = "Schedule Of Rates";
            this.rowintScheduleOfRatesCheckEdit.Properties.FieldName = "ActionScheduleOfRates";
            this.rowintScheduleOfRatesCheckEdit.Properties.ReadOnly = true;
            // 
            // categoryRow5
            // 
            this.categoryRow5.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowJobRateDescButtonEdit,
            this.rowmonJobRateBudgetedSpinEdit,
            this.rowmonBudgetedCostSpinEdit});
            this.categoryRow5.Name = "categoryRow5";
            this.categoryRow5.Properties.Caption = "Budgeted Cost";
            // 
            // rowJobRateDescButtonEdit
            // 
            this.rowJobRateDescButtonEdit.Name = "rowJobRateDescButtonEdit";
            this.rowJobRateDescButtonEdit.Properties.Caption = "Rate Description";
            this.rowJobRateDescButtonEdit.Properties.FieldName = "ActionBudgetedRateDescription";
            this.rowJobRateDescButtonEdit.Properties.ReadOnly = true;
            // 
            // rowmonJobRateBudgetedSpinEdit
            // 
            this.rowmonJobRateBudgetedSpinEdit.Name = "rowmonJobRateBudgetedSpinEdit";
            this.rowmonJobRateBudgetedSpinEdit.Properties.Caption = "Budgeted Rate";
            this.rowmonJobRateBudgetedSpinEdit.Properties.FieldName = "ActionBudgetedRate";
            this.rowmonJobRateBudgetedSpinEdit.Properties.ReadOnly = true;
            this.rowmonJobRateBudgetedSpinEdit.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // rowmonBudgetedCostSpinEdit
            // 
            this.rowmonBudgetedCostSpinEdit.Name = "rowmonBudgetedCostSpinEdit";
            this.rowmonBudgetedCostSpinEdit.Properties.Caption = "Budgeted Cost";
            this.rowmonBudgetedCostSpinEdit.Properties.FieldName = "ActionBudgetedCost";
            this.rowmonBudgetedCostSpinEdit.Properties.ReadOnly = true;
            this.rowmonBudgetedCostSpinEdit.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // categoryRow6
            // 
            this.categoryRow6.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowActualJobRateDescTextEdit,
            this.rowmonJobRateActualSpinEdit,
            this.rowmonDiscountRateSpinEdit,
            this.rowmonActualCostSpinEdit});
            this.categoryRow6.Name = "categoryRow6";
            this.categoryRow6.Properties.Caption = "Actual Cost";
            // 
            // rowActualJobRateDescTextEdit
            // 
            this.rowActualJobRateDescTextEdit.Name = "rowActualJobRateDescTextEdit";
            this.rowActualJobRateDescTextEdit.Properties.Caption = "Rate Description";
            this.rowActualJobRateDescTextEdit.Properties.FieldName = "ActionActualRateDescription";
            this.rowActualJobRateDescTextEdit.Properties.ReadOnly = true;
            // 
            // rowmonJobRateActualSpinEdit
            // 
            this.rowmonJobRateActualSpinEdit.Name = "rowmonJobRateActualSpinEdit";
            this.rowmonJobRateActualSpinEdit.Properties.Caption = "Actual Rate";
            this.rowmonJobRateActualSpinEdit.Properties.FieldName = "ActionActualRate";
            this.rowmonJobRateActualSpinEdit.Properties.ReadOnly = true;
            this.rowmonJobRateActualSpinEdit.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // rowmonDiscountRateSpinEdit
            // 
            this.rowmonDiscountRateSpinEdit.Name = "rowmonDiscountRateSpinEdit";
            this.rowmonDiscountRateSpinEdit.Properties.Caption = "Discount Rate";
            this.rowmonDiscountRateSpinEdit.Properties.FieldName = "monDiscountRate";
            this.rowmonDiscountRateSpinEdit.Properties.ReadOnly = true;
            this.rowmonDiscountRateSpinEdit.Properties.RowEdit = this.repositoryItemTextEdit2;
            // 
            // rowmonActualCostSpinEdit
            // 
            this.rowmonActualCostSpinEdit.Name = "rowmonActualCostSpinEdit";
            this.rowmonActualCostSpinEdit.Properties.Caption = "Actual Cost";
            this.rowmonActualCostSpinEdit.Properties.FieldName = "ActionActualCost";
            this.rowmonActualCostSpinEdit.Properties.ReadOnly = true;
            this.rowmonActualCostSpinEdit.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // editorRow17
            // 
            this.editorRow17.Name = "editorRow17";
            this.editorRow17.Properties.Caption = "Cost Difference";
            this.editorRow17.Properties.FieldName = "ActionCostDifference";
            this.editorRow17.Properties.ReadOnly = true;
            // 
            // categoryRow7
            // 
            this.categoryRow7.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowstrUser1TextEdit,
            this.rowstrUser2TextEdit,
            this.rowstrUser3TextEdit,
            this.rowUserPickList1GridLookUpEdit,
            this.rowUserPickList2GridLookUpEdit,
            this.rowUserPickList3GridLookUpEdit});
            this.categoryRow7.Name = "categoryRow7";
            this.categoryRow7.Properties.Caption = "User Defined";
            // 
            // rowstrUser1TextEdit
            // 
            this.rowstrUser1TextEdit.Name = "rowstrUser1TextEdit";
            this.rowstrUser1TextEdit.Properties.Caption = "User Text 1";
            this.rowstrUser1TextEdit.Properties.FieldName = "ActionUserDefined1";
            this.rowstrUser1TextEdit.Properties.ReadOnly = true;
            // 
            // rowstrUser2TextEdit
            // 
            this.rowstrUser2TextEdit.Name = "rowstrUser2TextEdit";
            this.rowstrUser2TextEdit.Properties.Caption = "User Text 2";
            this.rowstrUser2TextEdit.Properties.FieldName = "ActionUserDefined2";
            this.rowstrUser2TextEdit.Properties.ReadOnly = true;
            // 
            // rowstrUser3TextEdit
            // 
            this.rowstrUser3TextEdit.Name = "rowstrUser3TextEdit";
            this.rowstrUser3TextEdit.Properties.Caption = "User Text 3";
            this.rowstrUser3TextEdit.Properties.FieldName = "ActionUserDefined3";
            this.rowstrUser3TextEdit.Properties.ReadOnly = true;
            // 
            // rowUserPickList1GridLookUpEdit
            // 
            this.rowUserPickList1GridLookUpEdit.Name = "rowUserPickList1GridLookUpEdit";
            this.rowUserPickList1GridLookUpEdit.Properties.Caption = "User Picklist 1";
            this.rowUserPickList1GridLookUpEdit.Properties.FieldName = "ActionUserPickList1";
            this.rowUserPickList1GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowUserPickList2GridLookUpEdit
            // 
            this.rowUserPickList2GridLookUpEdit.Name = "rowUserPickList2GridLookUpEdit";
            this.rowUserPickList2GridLookUpEdit.Properties.Caption = "User Picklist 2";
            this.rowUserPickList2GridLookUpEdit.Properties.FieldName = "ActionUserPickList2";
            this.rowUserPickList2GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowUserPickList3GridLookUpEdit
            // 
            this.rowUserPickList3GridLookUpEdit.Name = "rowUserPickList3GridLookUpEdit";
            this.rowUserPickList3GridLookUpEdit.Properties.Caption = "User Picklist 3";
            this.rowUserPickList3GridLookUpEdit.Properties.FieldName = "ActionUserPickList3";
            this.rowUserPickList3GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // editorRow18
            // 
            this.editorRow18.Name = "editorRow18";
            this.editorRow18.Properties.Caption = "Completion Status";
            this.editorRow18.Properties.FieldName = "ActionCompletionStatus";
            this.editorRow18.Properties.ReadOnly = true;
            // 
            // editorRow19
            // 
            this.editorRow19.Name = "editorRow19";
            this.editorRow19.Properties.Caption = "Timeliness";
            this.editorRow19.Properties.FieldName = "ActionTimeliness";
            this.editorRow19.Properties.ReadOnly = true;
            // 
            // rowstrRemarksMemoEdit
            // 
            this.rowstrRemarksMemoEdit.Name = "rowstrRemarksMemoEdit";
            this.rowstrRemarksMemoEdit.Properties.Caption = "Remarks";
            this.rowstrRemarksMemoEdit.Properties.FieldName = "ActionRemarks";
            this.rowstrRemarksMemoEdit.Properties.ReadOnly = true;
            this.rowstrRemarksMemoEdit.Properties.RowEdit = this.repositoryItemMemoExEdit2;
            // 
            // checkEditSychronise
            // 
            this.checkEditSychronise.Location = new System.Drawing.Point(3, 2);
            this.checkEditSychronise.MenuManager = this.barManager1;
            this.checkEditSychronise.Name = "checkEditSychronise";
            this.checkEditSychronise.Properties.Caption = "Synchronise";
            this.checkEditSychronise.Size = new System.Drawing.Size(87, 19);
            this.checkEditSychronise.TabIndex = 13;
            // 
            // rowActionOwnership
            // 
            this.rowActionOwnership.Name = "rowActionOwnership";
            this.rowActionOwnership.Properties.Caption = "Ownership";
            this.rowActionOwnership.Properties.FieldName = "ActionOwnership";
            // 
            // rowActionCostCentre
            // 
            this.rowActionCostCentre.Name = "rowActionCostCentre";
            this.rowActionCostCentre.Properties.Caption = "Cost Centre";
            this.rowActionCostCentre.Properties.FieldName = "ActionCostCentre";
            // 
            // rowActionBudget
            // 
            this.rowActionBudget.Name = "rowActionBudget";
            this.rowActionBudget.Properties.Caption = "Budget";
            this.rowActionBudget.Properties.FieldName = "ActionBudget";
            // 
            // rowActionWorkUnits
            // 
            this.rowActionWorkUnits.Name = "rowActionWorkUnits";
            this.rowActionWorkUnits.Properties.Caption = "Work Units";
            this.rowActionWorkUnits.Properties.FieldName = "ActionWorkUnits";
            // 
            // rowActionScheduleOfRates
            // 
            this.rowActionScheduleOfRates.Name = "rowActionScheduleOfRates";
            this.rowActionScheduleOfRates.Properties.Caption = "On Schedule Of Rates";
            this.rowActionScheduleOfRates.Properties.FieldName = "ActionScheduleOfRates";
            // 
            // rowActionBudgetedRateDescription
            // 
            this.rowActionBudgetedRateDescription.Name = "rowActionBudgetedRateDescription";
            this.rowActionBudgetedRateDescription.Properties.Caption = "Budgeted Rate Description";
            this.rowActionBudgetedRateDescription.Properties.FieldName = "ActionBudgetedRateDescription";
            // 
            // rowActionBudgetedRate
            // 
            this.rowActionBudgetedRate.Enabled = false;
            this.rowActionBudgetedRate.Name = "rowActionBudgetedRate";
            this.rowActionBudgetedRate.Properties.Caption = "Budgeted Rate";
            this.rowActionBudgetedRate.Properties.FieldName = "ActionBudgetedRate";
            // 
            // rowActionBudgetedCost
            // 
            this.rowActionBudgetedCost.Enabled = false;
            this.rowActionBudgetedCost.Name = "rowActionBudgetedCost";
            this.rowActionBudgetedCost.Properties.Caption = "Budgeted Cost";
            this.rowActionBudgetedCost.Properties.FieldName = "ActionBudgetedCost";
            // 
            // rowActionActualRateDescription
            // 
            this.rowActionActualRateDescription.Enabled = false;
            this.rowActionActualRateDescription.Name = "rowActionActualRateDescription";
            this.rowActionActualRateDescription.Properties.Caption = "Actual Rate Description";
            this.rowActionActualRateDescription.Properties.FieldName = "ActionActualRateDescription";
            // 
            // rowActionActualRate
            // 
            this.rowActionActualRate.Enabled = false;
            this.rowActionActualRate.Name = "rowActionActualRate";
            this.rowActionActualRate.Properties.Caption = "Actual Rate";
            this.rowActionActualRate.Properties.FieldName = "ActionActualRate";
            // 
            // rowActionActualCost
            // 
            this.rowActionActualCost.Enabled = false;
            this.rowActionActualCost.Name = "rowActionActualCost";
            this.rowActionActualCost.Properties.Caption = "Actual Cost";
            this.rowActionActualCost.Properties.FieldName = "ActionActualCost";
            // 
            // rowActionWorkOrder
            // 
            this.rowActionWorkOrder.Name = "rowActionWorkOrder";
            this.rowActionWorkOrder.Properties.Caption = "Work Order";
            this.rowActionWorkOrder.Properties.FieldName = "ActionWorkOrder";
            // 
            // rowActionRemarks
            // 
            this.rowActionRemarks.Name = "rowActionRemarks";
            // 
            // rowActionUserDefined1
            // 
            this.rowActionUserDefined1.Enabled = false;
            this.rowActionUserDefined1.Name = "rowActionUserDefined1";
            this.rowActionUserDefined1.Properties.Caption = "User Text 1";
            this.rowActionUserDefined1.Properties.FieldName = "ActionUserDefined1";
            // 
            // rowActionUserDefined2
            // 
            this.rowActionUserDefined2.Enabled = false;
            this.rowActionUserDefined2.Name = "rowActionUserDefined2";
            this.rowActionUserDefined2.Properties.Caption = "User Text 2";
            this.rowActionUserDefined2.Properties.FieldName = "ActionUserDefined2";
            // 
            // rowActionUserDefined3
            // 
            this.rowActionUserDefined3.Enabled = false;
            this.rowActionUserDefined3.Name = "rowActionUserDefined3";
            this.rowActionUserDefined3.Properties.Caption = "User Text 3";
            this.rowActionUserDefined3.Properties.FieldName = "ActionUserDefined3";
            // 
            // rowActionCostDifference
            // 
            this.rowActionCostDifference.Enabled = false;
            this.rowActionCostDifference.Name = "rowActionCostDifference";
            this.rowActionCostDifference.Properties.Caption = "Cost Difference";
            this.rowActionCostDifference.Properties.FieldName = "ActionCostDifference";
            // 
            // rowActionCompletionStatus
            // 
            this.rowActionCompletionStatus.Enabled = false;
            this.rowActionCompletionStatus.Name = "rowActionCompletionStatus";
            this.rowActionCompletionStatus.Properties.Caption = "Completion Status";
            this.rowActionCompletionStatus.Properties.FieldName = "ActionCompletionStatus";
            // 
            // rowActionTimeliness
            // 
            this.rowActionTimeliness.Enabled = false;
            this.rowActionTimeliness.Name = "rowActionTimeliness";
            this.rowActionTimeliness.Properties.Caption = "Timeliness";
            this.rowActionTimeliness.Properties.FieldName = "ActionTimeliness";
            // 
            // sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter
            // 
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.ClearBeforeFill = true;
            // 
            // sp00190_Contractor_List_With_BlankTableAdapter
            // 
            this.sp00190_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01375_AT_User_Screen_SettingsBindingSource
            // 
            this.sp01375_AT_User_Screen_SettingsBindingSource.DataMember = "sp01375_AT_User_Screen_Settings";
            this.sp01375_AT_User_Screen_SettingsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp01375_AT_User_Screen_SettingsTableAdapter
            // 
            this.sp01375_AT_User_Screen_SettingsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01390_AT_Action_Edit_Previous_ActionsTableAdapter
            // 
            this.sp01390_AT_Action_Edit_Previous_ActionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01362_AT_Ownership_List_With_BlankTableAdapter
            // 
            this.sp01362_AT_Ownership_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00214_Master_Job_List_With_BlankTableAdapter
            // 
            this.sp00214_Master_Job_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // categoryRow3
            // 
            this.categoryRow3.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowActionOwnership,
            this.rowActionCostCentre,
            this.rowActionBudget,
            this.rowActionWorkUnits,
            this.rowActionScheduleOfRates});
            this.categoryRow3.Name = "categoryRow3";
            this.categoryRow3.Properties.Caption = "Costing Details";
            // 
            // sp01391_AT_Cost_Centres_With_BlankTableAdapter
            // 
            this.sp01391_AT_Cost_Centres_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp02066_AT_Action_Pictures_ListTableAdapter
            // 
            this.sp02066_AT_Action_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending39
            // 
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending39.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending39.GridControl = this.gridControl39;
            // 
            // frm_AT_Action_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1110, 702);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Action_Edit";
            this.Text = "Edit Action";
            this.Activated += new System.EventHandler(this.frm_AT_Action_Edit_Activated);
            this.Deactivate += new System.EventHandler(this.frm_AT_Action_Edit_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Action_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Action_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02066ATActionPicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualJobRateDescTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01388ATActionEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intActionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intInspectionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDueDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDueDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDoneDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDoneDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intWorkOrderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPrevActionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strNearestHouseTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intJobRateBudgetedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intJobRateActualSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSortOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCostCentreIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GUIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monJobRateBudgetedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monJobRateActualSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monBudgetedCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monActualCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monJobWorkUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monDiscountRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionReferenceButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strJobNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intBudgetIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobRateDescButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOwnershipIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01362ATOwnershipListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intActionGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00214MasterJobListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intActionByGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSupervisorGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPriorityIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList1GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intScheduleOfRatesCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreCodeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01391ATCostCentresWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintActionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintBudgetID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCostCentreID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGUID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintInspectionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintJobRateActual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPrevActionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSortOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintWorkOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintJobRateBudgeted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtDoneDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintActionBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSupervisor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintAction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrJobNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintScheduleOfRates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonJobRateBudgeted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonBudgetedCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobRateDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonJobRateActual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonActualCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonDiscountRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualJobRateDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOwnershipID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonJobWorkUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedPicturesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPriorityID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrNearestHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelLastAction.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01390ATActionEditPreviousActionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSychronise.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01375_AT_User_Screen_SettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiCopyDetails;
        private DevExpress.XtraBars.BarButtonItem bbiReload;
        private DevExpress.XtraBars.BarButtonItem bbiPasteAllValues;
        private DevExpress.XtraBars.BarButtonItem bbiPasteSelectedValues;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarCheckItem bciShowPrevious;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp01388ATActionEditBindingSource;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01388_AT_Action_EditTableAdapter sp01388_AT_Action_EditTableAdapter;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.TextEdit intActionIDTextEdit;
        private DevExpress.XtraEditors.TextEdit intInspectionIDTextEdit;
        private DevExpress.XtraEditors.DateEdit dtDueDateDateEdit;
        private DevExpress.XtraEditors.DateEdit dtDoneDateDateEdit;
        private DevExpress.XtraEditors.TextEdit strUser1TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser2TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser3TextEdit;
        private DevExpress.XtraEditors.TextEdit intWorkOrderIDTextEdit;
        private DevExpress.XtraEditors.TextEdit intPrevActionIDTextEdit;
        private DevExpress.XtraEditors.TextEdit strNearestHouseTextEdit;
        private DevExpress.XtraEditors.SpinEdit intJobRateBudgetedSpinEdit;
        private DevExpress.XtraEditors.SpinEdit intJobRateActualSpinEdit;
        private DevExpress.XtraEditors.SpinEdit monJobRateBudgetedSpinEdit;
        private DevExpress.XtraEditors.SpinEdit monJobRateActualSpinEdit;
        private DevExpress.XtraEditors.SpinEdit monBudgetedCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit monActualCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit monJobWorkUnitsSpinEdit;
        private DevExpress.XtraEditors.SpinEdit intSortOrderSpinEdit;
        private DevExpress.XtraEditors.SpinEdit monDiscountRateSpinEdit;
        private DevExpress.XtraEditors.TextEdit intCostCentreIDTextEdit;
        private DevExpress.XtraEditors.DateEdit DateAddedDateEdit;
        private DevExpress.XtraEditors.TextEdit GUIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintActionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintInspectionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtDueDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtDoneDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintAction;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintActionBy;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSupervisor;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrJobNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintWorkOrderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintPrevActionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrNearestHouse;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintPriorityID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobRateDesc;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintJobRateBudgeted;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintJobRateActual;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonJobRateBudgeted;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonJobRateActual;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonBudgetedCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonActualCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonJobWorkUnits;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSortOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintBudgetID;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonDiscountRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintScheduleOfRates;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintOwnershipID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCentreCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCostCentreID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateAdded;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGUID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList3;
        private DevExpress.XtraEditors.ButtonEdit InspectionReferenceButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit strJobNumberButtonEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser1;
        private DevExpress.XtraEditors.ButtonEdit JobRateDescButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit intBudgetIDTextEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkOrderDescription;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLastAction;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraEditors.CheckEdit checkEditSychronise;
        private DevExpress.XtraEditors.GridLookUpEdit intOwnershipIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit intActionGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GridLookUpEdit intActionByGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.GridLookUpEdit intSupervisorGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.GridLookUpEdit intPriorityIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList1GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private System.Windows.Forms.BindingSource sp01372ATMultiplePicklistsWithBlanksBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00190ContractorListWithBlankBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter sp00190_Contractor_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalCOntractorDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private System.Windows.Forms.BindingSource sp01375_AT_User_Screen_SettingsBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01375_AT_User_Screen_SettingsTableAdapter sp01375_AT_User_Screen_SettingsTableAdapter;
        private System.Windows.Forms.BindingSource sp01390ATActionEditPreviousActionsBindingSource;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionOwnership;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionCostCentre;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionBudget;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionWorkUnits;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionScheduleOfRates;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionBudgetedRateDescription;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionBudgetedRate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionBudgetedCost;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionActualRateDescription;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionActualRate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionActualCost;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionWorkOrder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionRemarks;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionUserDefined1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionUserDefined2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionUserDefined3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionCostDifference;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionCompletionStatus;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionTimeliness;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01390_AT_Action_Edit_Previous_ActionsTableAdapter sp01390_AT_Action_Edit_Previous_ActionsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCode;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private System.Windows.Forms.BindingSource sp01362ATOwnershipListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01362_AT_Ownership_List_With_BlankTableAdapter sp01362_AT_Ownership_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipCode;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipID;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraEditors.CheckEdit intScheduleOfRatesCheckEdit;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00214MasterJobListWithBlankBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00214_Master_Job_List_With_BlankTableAdapter sp00214_Master_Job_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colintJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow3;
        private DevExpress.XtraVerticalGrid.VGridControl vGridControl1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowInspectionID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLocalityID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDistrictID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeReference;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeMappingID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowInspectionReferenceButtonEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowInspectionDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActionID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrJobNumberButtonEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintActionGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintPriorityIDGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowdtDueDateDateEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowdtDoneDateDateEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintActionByGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintSupervisorGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintOwnershipIDGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCostCentreCodeButtonEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowBudgetID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowmonJobWorkUnitsSpinEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintScheduleOfRatesCheckEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowJobRateDescButtonEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowmonJobRateBudgetedSpinEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowmonBudgetedCostSpinEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowActualJobRateDescTextEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowmonJobRateActualSpinEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowmonActualCostSpinEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWorkOrderDescriptionTextEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrRemarksMemoEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrUser1TextEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrUser2TextEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrUser3TextEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow17;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow18;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow19;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow4;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow5;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.TextEdit ActualJobRateDescTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualJobRateDesc;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowmonDiscountRateSpinEdit;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow7;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUserPickList1GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUserPickList2GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUserPickList3GridLookUpEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.PopupContainerEdit CostCentreCodeButtonEdit;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.BindingSource sp01391ATCostCentresWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreID;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetID1;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName1;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01391_AT_Cost_Centres_With_BlankTableAdapter sp01391_AT_Cost_Centres_With_BlankTableAdapter;
        private DevExpress.XtraEditors.DateEdit dateEditDateTo;
        private DevExpress.XtraEditors.DateEdit dateEditDateFrom;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.ButtonEdit WorkOrderDescriptionTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDisabled;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.GridControl gridControl39;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn169;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn170;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn171;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn172;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn173;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn174;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn175;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn176;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn177;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn178;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn179;
        private DevExpress.XtraGrid.Columns.GridColumn colShortLinkedRecordDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedPicturesGrid;
        private System.Windows.Forms.BindingSource sp02066ATActionPicturesListBindingSource;
        private DataSet_ATTableAdapters.sp02066_AT_Action_Pictures_ListTableAdapter sp02066_AT_Action_Pictures_ListTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending39;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
    }
}

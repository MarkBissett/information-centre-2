using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using WoodPlan5.Properties;
using BaseObjects;
using Utilities;


namespace WoodPlan5
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_AT_Data_Transfer_GBM_Mobile_Block_Edit_Sequence_Data : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        private Settings set = Settings.Default;
        /// <summary>
        /// 
        /// </summary>
        public string PassedInTableName = "";
        /// <summary>
        /// 
        /// </summary>
        public string PassedInFieldName = "";
        /// <summary>
        /// 
        /// </summary>
        public int SelectedSequenceID = 0;
        /// <summary>
        /// 
        /// </summary>
        public string SelectedSequence = "";
        /// <summary>
        /// 
        /// </summary>
        public string UserFriendlyFieldName = "";
        /// <summary>
        /// 
        /// </summary>
        public int intRecordCount = 0;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public frm_AT_Data_Transfer_GBM_Mobile_Block_Edit_Sequence_Data()
        {
            InitializeComponent();
        }

        private void frm_AT_Data_Transfer_GBM_Mobile_Block_Edit_Sequence_Data_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;
            sp01247_AT_Sequence_List_FilteredTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01247_AT_Sequence_List_FilteredTableAdapter.Fill(dataSet_AT.sp01247_AT_Sequence_List_Filtered, PassedInTableName, PassedInFieldName);
            BlockEditLabel.Text = "Block Editing " + intRecordCount.ToString() + " " + UserFriendlyFieldName + "(s)";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Sequence to use from the list before proceeding!", "Block Edit - Select Sequence", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LengthOfStringPart")) <= 0)
            {
                SelectedSequence = "";
            }
            else
            {
                SelectedSequence = view.GetRowCellValue(view.FocusedRowHandle, "Sequence").ToString();
            }
            SelectedSequenceID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SequenceID"));
            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails validation test earlier in this process //
            this.Close();
        }


        #region GridControl 1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No sequences available - use the Sequence Manager screen to create first");
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion

    }
}


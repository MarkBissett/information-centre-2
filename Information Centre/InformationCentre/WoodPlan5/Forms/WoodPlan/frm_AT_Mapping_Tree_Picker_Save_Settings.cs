using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Tree_Picker_Save_Settings : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;

        public bool boolSaveLoadedLayers = true;
        public bool boolSaveLabelStructure = true;
        public bool boolSaveThematicStyling = true;
        public bool boolSaveXY = true;
        public bool boolSaveMapScale = true;
        public string strWorkspaceName = "";
        public string strRemarks = "";
        public int intDefaultWorkspace = 0;
        public int intShareType = 0;
        public string strShareWithGroupIDs = "";

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion


        public frm_AT_Mapping_Tree_Picker_Save_Settings()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Tree_Picker_Save_Settings_Load(object sender, EventArgs e)
        {
            this.FormID = 200410;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            ceSaveLayers.Checked = boolSaveLoadedLayers;
            ceSaveLabelStructure.Checked = boolSaveLabelStructure;
            ceSaveThematics.Checked = boolSaveThematicStyling;
            ceSaveThematics.Enabled = boolSaveThematicStyling;
            ceSaveXY.Checked = boolSaveXY;
            ceSaveMapScale.Checked = boolSaveMapScale;

            if (strFormMode == "Save")
            {
                layoutControl1.BeginUpdate();
                layoutControlGroup3.Enabled = false;
                layoutControlGroup3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem11.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                emptySpaceItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                emptySpaceItem5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControl1.EndUpdate();
                this.Height = 250;
            }
            else  // Save As //
            {
                this.Text = "Mapping - Save Current Settings As New Workspace";
                InitValidationRules(dxValidationProvider1);
                dxValidationProvider1.Validate();
                this.sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter.Connection.ConnectionString = strConnectionString;
                this.sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01266_AT_Tree_Picker_Workspace_Share_groups, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);
                gridControl2.ForceInitialize();
                
                // Add record selection checkboxes to grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 30;

            }
        }

        private void InitValidationRules(DXValidationProvider dxValidationProvider)
        {
            // ***** IMPORTANT NOTE: Forms AutoValidate property set to EnableAllowFocusChange ***** //
            // Create Rule // 
            ConditionValidationRule notEmptyValidationRule = new ConditionValidationRule();
            notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank;
            notEmptyValidationRule.ErrorText = "Please enter a value [100 characters maximum]";

            // Link Rule to controls //
            if (strFormMode != "blockadd" && strFormMode != "blockedit")
            {
                dxValidationProvider.SetValidationRule(WorkspaceNameTextEdit, notEmptyValidationRule);
            }
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            boolSaveLoadedLayers = ceSaveLayers.Checked;
            boolSaveLabelStructure = ceSaveLabelStructure.Checked;
            boolSaveThematicStyling = ceSaveThematics.Checked;
            boolSaveXY = ceSaveXY.Checked;
            boolSaveMapScale = ceSaveMapScale.Checked;

            if (!boolSaveLoadedLayers && !boolSaveLabelStructure && !boolSaveThematicStyling && !boolSaveXY && !boolSaveMapScale)  // Nothing selected for save //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more settings to save before proceeding or click Cancel to close the screen withouot saving.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (strFormMode == "Save")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else // Save As //
            {
                // Error Checking Prior to Saving - See if Group Sharing is on [make sure at least one group selected] //
                strShareWithGroupIDs = "";
                intShareType = Convert.ToInt32(radioGroup1.EditValue);
                if (intShareType == 2)
                {
                    GridView view2 = (GridView)gridControl2.MainView;
                    for (int i = 0; i < view2.DataRowCount; i++)
                    {
                        if (Convert.ToInt32(view2.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                        {
                            strShareWithGroupIDs += Convert.ToString(view2.GetRowCellValue(i, "GroupID")) + ",";
                        }
                    }
                    if (strShareWithGroupIDs == "")
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("You have set the Sharing option to 'Only Staff in the Following Groups...', but you have not chosen any groups to share with!\n\nTip: To specify groups, tick the tick box next to their name.\n\nIf you cannot see any groups in the list then you do not have access to any groups. Contact your system administrator for advice.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
                // Ensure the Workspace Name is present //
                strWorkspaceName = WorkspaceNameTextEdit.Text.ToString().Trim();
                if (string.IsNullOrEmpty(strWorkspaceName))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Workspace Name has been entered!\n\nPlease enter a name before proceeding.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // Ensure the Workspace Name is Unique //
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter CheckUnique = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                CheckUnique.ChangeConnectionString(strConnectionString);
                int intMatches = 0;
                try
                {
                    intMatches = Convert.ToInt32(CheckUnique.sp01275_AT_Tree_Picker_Workspace_Check_Name_Unique(-1, strWorkspaceName));
                    if (intMatches > 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The workspace name entered [" + strWorkspaceName + "]  has already been asigned to a different Workspace!\n\nPlease enter a different name before proceeding.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
                catch
                {
                    XtraMessageBox.Show("Unable to save workspace changes - an error occurred while checking the Name was unique!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                intDefaultWorkspace = (DefaultWorkspaceCheckEdit.Checked ? 1 : 0);
                strRemarks = WorkspaceRemarksMemoEdit.Text.ToString();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioGroup rg = (RadioGroup)sender;
            gridControl2.Enabled = (Convert.ToInt32(rg.EditValue) == 2 ? true : false);
        }
    }
}


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Utils;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Generic_Query_Tool : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public VGridControl vg;
        #endregion

        public frm_AT_Mapping_Generic_Query_Tool()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Generic_Query_Tool_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;

            vGridControl1.BeginUpdate();
            vGridControl1.BeginDataUpdate();
            string strCaption = "";
            foreach (CategoryRow cRow in vg.Rows)
            {
                //vGridControl1.Rows.Add(cRow);

                CategoryRow ParentRow = new CategoryRow();
                ParentRow.Properties.Caption = cRow.Properties.Caption;
                //ParentRow.Properties.ReadOnly = true;
                foreach (EditorRow eRow in cRow.ChildRows)
                {
                    EditorRow ChildRow = new EditorRow();
                    switch (eRow.Properties.Caption)
                    {
                        case "id":
                            strCaption = "Mapping ID";
                            break;
                        case "TreeID":
                            strCaption = "Tree ID";
                            break;
                        case "TreeRef":
                            strCaption = "Tree Reference";
                            break;
                        case "DistrictName":
                            strCaption = "District Name";
                            break;
                        case "LocalityName":
                            strCaption = "Locality Name";
                            break;
                        case "Species":
                            strCaption = "Species";
                            break;
                        case "intX":
                            strCaption = "X Coordinate";
                            break;
                        case "intY":
                            strCaption = "Y Coordinate";
                            break;
                        case "intObjectType":
                            strCaption = "WoodPlan Object Type";
                            break;
                        case "intHighlighted":
                            strCaption = "Highlighted";
                            break;
                        case "CrownDiameter":
                            strCaption = "Crown Diameter";
                            break;
                        case "CalculatedSize":
                            strCaption = "Calculated Size";
                            break;
                        case "CalculatedPerimeter":
                            strCaption = "Calculated Perimeter";
                            break;
                        case "RiskFactor":
                            strCaption = "Risk Factor";
                            break;
                        case "ShortTreeRef":
                            strCaption = "Short Tree Reference";
                            break;
                        case "ModuleID":
                            strCaption = "Module ID";
                            break;
                        case "AssetID":
                            strCaption = "Asset ID";
                            break;
                        case "AssetNumber":
                            strCaption = "Asset Number";
                            break;
                        case "PartNumber":
                            strCaption = "Part Number";
                            break;
                        case "ModelNumber":
                            strCaption = "Model Number";
                            break;
                        case "SerialNumber":
                            strCaption = "Serial Number";
                            break;
                        case "ClientName":
                            strCaption = "Client Name";
                            break;
                        case "ClientCode":
                            strCaption = "Client Code";
                            break;
                        case "SiteName":
                            strCaption = "Site Name";
                            break;
                        case "SiteCode":
                            strCaption = "Site Code";
                            break;
                        case "AssetType":
                            strCaption = "Asset Type";
                            break;
                        case "AssetSubType":
                            strCaption = "Asset Sub Type";
                            break;
                        case "Thematics":
                            strCaption = "Thematics";
                            break;
                        case "MI_Style":
                            strCaption = "MapInfo Style";
                            break;
                        case "Obj":
                            strCaption = "MapInfo Object";
                            break;
                        default:
                            strCaption = eRow.Properties.Caption;
                            break;
                    }
                    ChildRow.Properties.Caption = strCaption;
                    //ChildRow.Properties.ReadOnly = true;
                    ChildRow.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
                    ChildRow.Properties.Value = eRow.Properties.Value;
                    ChildRow.Properties.RowEdit = repositoryItemTextEdit1;
                    ParentRow.ChildRows.Add(ChildRow);
                }
                vGridControl1.Rows.Add(ParentRow);
            }
            vGridControl1.EndDataUpdate();
            vGridControl1.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}


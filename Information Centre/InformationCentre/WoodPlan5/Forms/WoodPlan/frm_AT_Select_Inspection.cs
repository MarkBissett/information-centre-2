using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_AT_Select_Inspection : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedValue = "";
        public int intSelectedID = 0;
        public int intTreeOwnershipID = 0;
        public string strNearestHouseName = "";
        GridHitInfo downHitInfo = null;

        public int intFilterActiveClient = 0;
        public int intFilterUtilityArbClient = 0;
        public int intFilterSummerMaintenanceClient = 0;
        public int intWinterMaintenanceClient = 0;
        public int intAmenityArbClient = 0;

        #endregion

        public frm_AT_Select_Inspection()
        {
            InitializeComponent();
        }

        private void frm_AT_Select_Inspection_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 20037;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            
            sp06022_OM_Select_Site_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01456_AT_Trees_Linked_To_Sites_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01341_AT_Inspections_Linked_To_Tree_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                this.sp06021_OM_Select_Client_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
                this.sp06021_OM_Select_Client_FilterTableAdapter.Fill(this.dataSet_GC_Summer_Core.sp06021_OM_Select_Client_Filter, new DateTime(1900, 1, 1), new DateTime(2500, 1, 1), intFilterActiveClient, intFilterUtilityArbClient, intFilterSummerMaintenanceClient, intWinterMaintenanceClient, intAmenityArbClient);
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the clients list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }


        bool internalRowFocusing;


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Sites Available For Selection - Select a Client to see Related Sites";
                    break;
                case "gridView3":
                    message = "No Trees Available For Selection - Select a Site to see Related Trees";
                    break;
                case "gridView4":
                    message = "No Inspection Available For Selection - Select a Tree to see Related Inspections";
                    break;
                 default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                case "gridView2":
                    LoadLinkedData2();
                    break;
                case "gridView3":
                    LoadLinkedData3();
                    break;
                case "gridView4":
                    strSelectedValue = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "InspectionReference"));
                    intSelectedID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "InspectionID"));
                    if (intSelectedID > 0 && string.IsNullOrEmpty(strSelectedValue)) strSelectedValue = "[Unknown Inspection Reference]";
                    intTreeOwnershipID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TreeOwnershipID"));
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ClientID"])) + ',';
            }

            //Populate Linked Map Links //
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_GC_Summer_Core.sp06022_OM_Select_Site_Filter.Clear();
            }
            else
            {
                try
                {
                    sp06022_OM_Select_Site_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06022_OM_Select_Site_Filter, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related localities.\n\nTry selecting a client again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void LoadLinkedData2()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["SiteID"])) + ',';
            }

            //Populate Linked Map Links //
            gridControl3.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_AT_DataEntry.sp01456_AT_Trees_Linked_To_Sites_List.Clear();
            }
            else
            {
                try
                {
                    sp01456_AT_Trees_Linked_To_Sites_ListTableAdapter.Fill(dataSet_AT_DataEntry.sp01456_AT_Trees_Linked_To_Sites_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related trees.\n\nTry selecting a site again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl3.MainView.EndUpdate();
        }

        private void LoadLinkedData3()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
            }

            //Populate Linked Map Links //
            gridControl4.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_AT_DataEntry.sp01341_AT_Inspections_Linked_To_Tree_List.Clear();
            }
            else
            {
                try
                {
                    sp01341_AT_Inspections_Linked_To_Tree_ListTableAdapter.Fill(dataSet_AT_DataEntry.sp01341_AT_Inspections_Linked_To_Tree_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related inspections.\n\nTry selecting a tree again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl4.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (strSelectedValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

 

    
    
    
    
    }
}


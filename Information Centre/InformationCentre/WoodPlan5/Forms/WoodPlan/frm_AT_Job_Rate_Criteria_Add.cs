using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Job_Rate_Criteria_Add : BaseObjects.frmBase
    {
        #region Instance Variables
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        public int i_int_RateCriteriaType = 0;  // 1 = crown diameters, 2 = dbh bands, 3 = 'districts', 4 = 'height bands', 5 = risk categories, 6 = site hazard classes, 7 = sizes, 8 = 'species' //

        #endregion

        public frm_AT_Job_Rate_Criteria_Add()
        {
            InitializeComponent();
        }

        private void frm_AT_Job_Rate_Criteria_Add_Load(object sender, EventArgs e)
        {
            this.FormID = 20113;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            
            sp00216_Job_Rate_Criteria_AvailableTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00216_Job_Rate_Criteria_AvailableTableAdapter.Fill(this.dataSet_AT.sp00216_Job_Rate_Criteria_Available, strRecordIDs, i_int_RateCriteriaType, intRecordCount);

            sp00217_Job_Rate_Contractors_AvailableTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00217_Job_Rate_Contractors_AvailableTableAdapter.Fill(this.dataSet_AT.sp00217_Job_Rate_Contractors_Available, strRecordIDs, intRecordCount);

        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            Application.DoEvents();  // Allow Form time to repaint itself //

            ConfigureFormAccordingToMode();
            
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
            barStaticItemInformation.Appearance.ForeColor = Color.Red;
        }

        private void frm_AT_Job_Rate_Criteria_Add_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_AT_Job_Rate_Criteria_Add_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Criteria Available";
                    break;
                case "gridView2":
                    message = "No Contractors Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int[] intRowHandles1 = null;
            GridView view1 = (GridView)gridControl1.MainView;
            intRowHandles1 = view1.GetSelectedRows();
            int intCount1 = intRowHandles1.Length;
            if (intCount1 <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records from the Criteria list before proceeding with Saving.", "Add new Job Rate Criteria", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int[] intRowHandles2 = null;
            GridView view2 = (GridView)gridControl2.MainView;
            intRowHandles2 = view2.GetSelectedRows();
            int intCount2 = intRowHandles2.Length;
            if (intCount2 <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records from the Contractor list before proceeding with Saving.", "Add new Job Rate Criteria", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Records Selected, so OK to Proceed... //
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            string strNewIDs = "";
            int intCriteriaID = 0;
            int intContractorID = 0;

            DataSet_ATTableAdapters.QueriesTableAdapter AddRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
            AddRecords.ChangeConnectionString(strConnectionString);

            foreach (int intRowHandle1 in intRowHandles1)
            {
                intCriteriaID = Convert.ToInt32(gridView1.GetRowCellValue(intRowHandle1, "intCriteriaID"));
                foreach (int intRowHandle2 in intRowHandles2)
                {
                    intContractorID = Convert.ToInt32(gridView2.GetRowCellValue(intRowHandle2, "intCriteriaID"));
                    try
                    {
                        strNewIDs += AddRecords.sp00218_Job_Rate_Manager_Add_Criteria(strRecordIDs, intCriteriaID, intContractorID).ToString() + ";";
                    }
                    catch (Exception ex)
                    {
                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add job rate criteria - an error occurred [" + ex.Message + "]!\n\nTry adding again - if the problem persists, contact Technical Support.", "Add Job Rate Criteria", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_Core_Master_Job_Manager")
                    {
                        frm_Core_Master_Job_Manager fParentForm = (frm_Core_Master_Job_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(6, GridEnumerations.enmFocusedGrid.MasterJobTypeRateCriteria, strNewIDs);
                    }
                }
            }
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

    }
}


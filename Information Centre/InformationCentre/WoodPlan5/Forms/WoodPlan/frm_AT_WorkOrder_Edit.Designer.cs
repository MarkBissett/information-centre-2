namespace WoodPlan5
{
    partial class frm_AT_WorkOrder_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_WorkOrder_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions14 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions15 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject57 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject58 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject59 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject60 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions16 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject61 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject62 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject63 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject64 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions17 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject65 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject66 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject67 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject68 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions18 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject69 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject70 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject71 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject72 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions19 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject73 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject74 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject75 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject76 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions20 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject77 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject78 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject79 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject80 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions21 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject81 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject82 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject83 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject84 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions22 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject85 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject86 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject87 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject88 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions23 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject89 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject90 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject91 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject92 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions24 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject93 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject94 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject95 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject96 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions25 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject97 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject98 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject99 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject100 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions26 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject101 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject102 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject103 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject104 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions27 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject105 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject106 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject107 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject108 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition7 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions28 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject109 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject110 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject111 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject112 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions29 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject113 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject114 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject115 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject116 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition8 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions30 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject117 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject118 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject119 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject120 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions31 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject121 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject122 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject123 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject124 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition9 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions32 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject125 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject126 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject127 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject128 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions33 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject129 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject130 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject131 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject132 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition10 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions34 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject133 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject134 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject135 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject136 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions35 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject137 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject138 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject139 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject140 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition11 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions36 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject141 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject142 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject143 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject144 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions37 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject145 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject146 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject147 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject148 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition12 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions38 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject149 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject150 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject151 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject152 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions39 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject153 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject154 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject155 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject156 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition13 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions40 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject157 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject158 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject159 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject160 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions41 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject161 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject162 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject163 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject164 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition14 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions42 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject165 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject166 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject167 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject168 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions43 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject169 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject170 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject171 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject172 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition15 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions44 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject173 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject174 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject175 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject176 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions45 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject177 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject178 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject179 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject180 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition16 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions46 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject181 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject182 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject183 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject184 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions47 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject185 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject186 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject187 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject188 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition17 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiCalculateDiscounts = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.WorkOrderPDFFileHyperLinkEdit = new DevExpress.XtraEditors.HyperLinkEdit();
            this.sp01399ATWorkOrderEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.strIncidentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01402ATWorkOrderActionsEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.coldtDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPrevActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrNearestHouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNearestHouse = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colintPriorityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditPriority = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintJobRateBudgeted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonJobRateBudgeted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMonJobRateActual = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colmonJobWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditMonJobWorkUnits = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colmonBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintJobRateActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualJobRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditActualJobRateDescription = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colmonJobRateActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonDiscountRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditDiscountRate = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colmonActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickListDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickListDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickListDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colintSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditActionByPerson = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00190ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalCOntractorDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditSupervisor = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView16 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn114 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn115 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn116 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn117 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn118 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn119 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn120 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn121 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintBudgetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOwnershipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditOwnership = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp01362ATOwnershipListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOwnershipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditCostCentreCode = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colintCostCentreID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intWorkOrderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dtIssueDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.strInvoiceNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.intOperationIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strEmailDetailsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.strUser1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dtCompleteDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.WorkOrderReferenceButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.TargetDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.intPrevWorkOrderIDButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.StatusGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intPriorityIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intSupervisor1GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00226StaffListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisplayName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetworkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intSupervisor2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intSupervisor3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCertifiedByIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intContractorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intIssuedByPersonIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intIssuedByBodyIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCompanyHeaderIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrSlogan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intRecordTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01403ATWorkOrderTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colintTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPicklist1GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPicklist2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPicklist3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForstrEmailDetails = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintOperationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintWorkOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintPrevWorkOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkOrderReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrInvoiceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintPriorityID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintIssuedByPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintIssuedByBodyID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCompanyHeaderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCertifiedByID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTargetDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemFordtIssueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtCompleteDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintSupervisor1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintSupervisor2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintSupervisor3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrUser1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup18 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForUserPicklist1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPicklist2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPicklist3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator5 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWorkOrderPDFFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForintRecordType = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator6 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.sp01391ATCostCentresWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01399_AT_Work_Order_EditTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01399_AT_Work_Order_EditTableAdapter();
            this.sp00190_Contractor_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter();
            this.sp00226_Staff_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter();
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter();
            this.sp01402_AT_Work_Order_Actions_EditTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01402_AT_Work_Order_Actions_EditTableAdapter();
            this.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter();
            this.sp01403_AT_Work_Order_TypesTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01403_AT_Work_Order_TypesTableAdapter();
            this.sp01362_AT_Ownership_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01362_AT_Ownership_List_With_BlankTableAdapter();
            this.sp01391_AT_Cost_Centres_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01391_AT_Cost_Centres_With_BlankTableAdapter();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderPDFFileHyperLinkEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01399ATWorkOrderEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01402ATWorkOrderActionsEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNearestHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMonJobRateActual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMonJobWorkUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditActualJobRateDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditDiscountRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditActionByPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSupervisor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditOwnership)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01362ATOwnershipListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditCostCentreCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intWorkOrderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtIssueDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtIssueDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strInvoiceNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOperationIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmailDetailsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompleteDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompleteDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderReferenceButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPrevWorkOrderIDButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPriorityIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSupervisor1GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSupervisor2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSupervisor3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCertifiedByIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContractorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIssuedByPersonIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIssuedByBodyIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCompanyHeaderIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00232CompanyHeaderDropDownListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRecordTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01403ATWorkOrderTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicklist1GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicklist2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicklist3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmailDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOperationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintWorkOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPrevWorkOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrInvoiceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPriorityID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIssuedByPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIssuedByBodyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCompanyHeaderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCertifiedByID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtCompleteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSupervisor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSupervisor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSupervisor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPicklist1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPicklist2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPicklist3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderPDFFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRecordType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01391ATCostCentresWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1143, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 720);
            this.barDockControlBottom.Size = new System.Drawing.Size(1143, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 694);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1143, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 694);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Item ID";
            this.gridColumn95.FieldName = "ItemID";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            this.gridColumn95.Width = 58;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Contractor ID";
            this.gridColumn104.FieldName = "ContractorID";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            this.gridColumn104.Width = 87;
            // 
            // colItemID
            // 
            this.colItemID.Caption = "Item ID";
            this.colItemID.FieldName = "ItemID";
            this.colItemID.Name = "colItemID";
            this.colItemID.OptionsColumn.AllowEdit = false;
            this.colItemID.OptionsColumn.AllowFocus = false;
            this.colItemID.OptionsColumn.ReadOnly = true;
            this.colItemID.Width = 58;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Item ID";
            this.gridColumn65.FieldName = "ItemID";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Width = 58;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 59;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Staff ID";
            this.gridColumn30.FieldName = "StaffID";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Width = 59;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Staff ID";
            this.gridColumn39.FieldName = "StaffID";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 59;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Staff ID";
            this.gridColumn48.FieldName = "StaffID";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Width = 59;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Contractor ID";
            this.gridColumn7.FieldName = "ContractorID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 87;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Staff ID";
            this.gridColumn57.FieldName = "StaffID";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Width = 59;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Item ID";
            this.gridColumn71.FieldName = "ItemID";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Width = 58;
            // 
            // colintHeaderID
            // 
            this.colintHeaderID.Caption = "Header ID";
            this.colintHeaderID.FieldName = "intHeaderID";
            this.colintHeaderID.Name = "colintHeaderID";
            this.colintHeaderID.OptionsColumn.AllowEdit = false;
            this.colintHeaderID.OptionsColumn.AllowFocus = false;
            this.colintHeaderID.OptionsColumn.ReadOnly = true;
            this.colintHeaderID.Width = 70;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Item ID";
            this.gridColumn77.FieldName = "ItemID";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.Width = 58;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Item ID";
            this.gridColumn83.FieldName = "ItemID";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Width = 58;
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Item ID";
            this.gridColumn89.FieldName = "ItemID";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Width = 58;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiCalculateDiscounts,
            this.barButtonItemPrint});
            this.barManager2.MaxItemId = 28;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCalculateDiscounts),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint, true)});
            this.bar2.Text = "Discounts";
            // 
            // bbiCalculateDiscounts
            // 
            this.bbiCalculateDiscounts.Caption = "Calculate Discounts";
            this.bbiCalculateDiscounts.Id = 26;
            this.bbiCalculateDiscounts.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCalculateDiscounts.ImageOptions.Image")));
            this.bbiCalculateDiscounts.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiCalculateDiscounts.ImageOptions.LargeImage")));
            this.bbiCalculateDiscounts.Name = "bbiCalculateDiscounts";
            this.bbiCalculateDiscounts.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiCalculateDiscounts.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCalculateDiscounts_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Print";
            this.barButtonItemPrint.Id = 27;
            this.barButtonItemPrint.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1143, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 720);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1143, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 694);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1143, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 694);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(6, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(7, "arrow_down_blue_round.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.WorkOrderPDFFileHyperLinkEdit);
            this.dataLayoutControl1.Controls.Add(this.strIncidentsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer1);
            this.dataLayoutControl1.Controls.Add(this.intWorkOrderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dtIssueDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.strInvoiceNumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.intOperationIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strEmailDetailsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.dtCompleteDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkOrderReferenceButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.TargetDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.intPrevWorkOrderIDButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intPriorityIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intSupervisor1GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intSupervisor2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intSupervisor3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCertifiedByIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intContractorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intIssuedByPersonIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intIssuedByBodyIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCompanyHeaderIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intRecordTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPicklist1GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPicklist2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPicklist3GridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp01399ATWorkOrderEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrEmailDetails,
            this.ItemForintOperationID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1499, 546, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1143, 694);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // WorkOrderPDFFileHyperLinkEdit
            // 
            this.WorkOrderPDFFileHyperLinkEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "WorkOrderPDFFile", true));
            this.WorkOrderPDFFileHyperLinkEdit.Location = new System.Drawing.Point(121, 146);
            this.WorkOrderPDFFileHyperLinkEdit.MenuManager = this.barManager1;
            this.WorkOrderPDFFileHyperLinkEdit.Name = "WorkOrderPDFFileHyperLinkEdit";
            this.WorkOrderPDFFileHyperLinkEdit.Properties.ReadOnly = true;
            this.WorkOrderPDFFileHyperLinkEdit.Size = new System.Drawing.Size(998, 20);
            this.WorkOrderPDFFileHyperLinkEdit.StyleController = this.dataLayoutControl1;
            this.WorkOrderPDFFileHyperLinkEdit.TabIndex = 43;
            this.WorkOrderPDFFileHyperLinkEdit.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.WorkOrderPDFFileHyperLinkEdit_OpenLink);
            // 
            // sp01399ATWorkOrderEditBindingSource
            // 
            this.sp01399ATWorkOrderEditBindingSource.DataMember = "sp01399_AT_Work_Order_Edit";
            this.sp01399ATWorkOrderEditBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // strIncidentsMemoEdit
            // 
            this.strIncidentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "strIncidents", true));
            this.strIncidentsMemoEdit.Location = new System.Drawing.Point(48, 250);
            this.strIncidentsMemoEdit.MenuManager = this.barManager1;
            this.strIncidentsMemoEdit.Name = "strIncidentsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strIncidentsMemoEdit, true);
            this.strIncidentsMemoEdit.Size = new System.Drawing.Size(1047, 130);
            this.scSpellChecker.SetSpellCheckerOptions(this.strIncidentsMemoEdit, optionsSpelling1);
            this.strIncidentsMemoEdit.StyleController = this.dataLayoutControl1;
            this.strIncidentsMemoEdit.TabIndex = 41;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp01399ATWorkOrderEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 40;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(24, 456);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1095, 214);
            this.gridSplitContainer1.TabIndex = 42;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01402ATWorkOrderActionsEditBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Actions to Work Order", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Remove Selected Linked Action(s) from Work Order", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Move Item Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Move Item Down", "down")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditMonJobRateActual,
            this.repositoryItemButtonEditActualJobRateDescription,
            this.repositoryItemDateEdit1,
            this.repositoryItemTextEditNearestHouse,
            this.repositoryItemGridLookUpEditOwnership,
            this.repositoryItemGridLookUpEditActionByPerson,
            this.repositoryItemGridLookUpEditSupervisor,
            this.repositoryItemGridLookUpEditPriority,
            this.repositoryItemButtonEditCostCentreCode,
            this.repositoryItemSpinEditDiscountRate,
            this.repositoryItemSpinEditMonJobWorkUnits});
            this.gridControl1.Size = new System.Drawing.Size(1095, 214);
            this.gridControl1.TabIndex = 39;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01402ATWorkOrderActionsEditBindingSource
            // 
            this.sp01402ATWorkOrderActionsEditBindingSource.DataMember = "sp01402_AT_Work_Order_Actions_Edit";
            this.sp01402ATWorkOrderActionsEditBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientName,
            this.colSiteName,
            this.colTreeReference,
            this.colInspectionReference,
            this.colTreeMappingID,
            this.colSpeciesName,
            this.coldtDueDate,
            this.coldtDoneDate,
            this.colintActionID,
            this.colstrJobNumber,
            this.colActionDescription,
            this.colintWorkOrderID,
            this.colintPrevActionID,
            this.colstrNearestHouse,
            this.colintPriorityID,
            this.colintScheduleOfRates,
            this.colActionScheduleOfRates,
            this.colintJobRateBudgeted,
            this.colBudgetedRateDescription,
            this.colmonJobRateBudgeted,
            this.colmonJobWorkUnits,
            this.colmonBudgetedCost,
            this.colintJobRateActual,
            this.colActualJobRateDescription,
            this.colmonJobRateActual,
            this.colmonDiscountRate,
            this.colmonActualCost,
            this.colintInspectionID,
            this.colintAction,
            this.colstrUser1,
            this.colstrUser2,
            this.colstrUser3,
            this.colUserPickListDescription1,
            this.colUserPickListDescription2,
            this.colUserPickListDescription3,
            this.colstrRemarks,
            this.colintSortOrder,
            this.colintActionBy,
            this.colintSupervisor,
            this.colintBudgetID,
            this.colintOwnershipID,
            this.colCostCentreCode,
            this.colintCostCentreID});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsFilter.AllowMRUFilterList = false;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colintSortOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Width = 112;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteName.Width = 107;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 0;
            this.colTreeReference.Width = 102;
            // 
            // colInspectionReference
            // 
            this.colInspectionReference.Caption = "Inspection Reference";
            this.colInspectionReference.FieldName = "InspectionReference";
            this.colInspectionReference.Name = "colInspectionReference";
            this.colInspectionReference.OptionsColumn.AllowEdit = false;
            this.colInspectionReference.OptionsColumn.AllowFocus = false;
            this.colInspectionReference.OptionsColumn.ReadOnly = true;
            this.colInspectionReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionReference.Width = 124;
            // 
            // colTreeMappingID
            // 
            this.colTreeMappingID.Caption = "Mapping ID";
            this.colTreeMappingID.FieldName = "TreeMappingID";
            this.colTreeMappingID.Name = "colTreeMappingID";
            this.colTreeMappingID.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeMappingID.Width = 93;
            // 
            // colSpeciesName
            // 
            this.colSpeciesName.Caption = "Species";
            this.colSpeciesName.FieldName = "SpeciesName";
            this.colSpeciesName.Name = "colSpeciesName";
            this.colSpeciesName.OptionsColumn.AllowEdit = false;
            this.colSpeciesName.OptionsColumn.AllowFocus = false;
            this.colSpeciesName.OptionsColumn.ReadOnly = true;
            this.colSpeciesName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeciesName.Visible = true;
            this.colSpeciesName.VisibleIndex = 2;
            this.colSpeciesName.Width = 91;
            // 
            // coldtDueDate
            // 
            this.coldtDueDate.Caption = "Due Date";
            this.coldtDueDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.coldtDueDate.FieldName = "dtDueDate";
            this.coldtDueDate.Name = "coldtDueDate";
            this.coldtDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.coldtDueDate.Visible = true;
            this.coldtDueDate.VisibleIndex = 3;
            this.coldtDueDate.Width = 66;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.repositoryItemDateEdit1.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemDateEdit1_EditValueChanged);
            // 
            // coldtDoneDate
            // 
            this.coldtDoneDate.Caption = "Done Date";
            this.coldtDoneDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.coldtDoneDate.FieldName = "dtDoneDate";
            this.coldtDoneDate.Name = "coldtDoneDate";
            this.coldtDoneDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.coldtDoneDate.Visible = true;
            this.coldtDoneDate.VisibleIndex = 4;
            this.coldtDoneDate.Width = 72;
            // 
            // colintActionID
            // 
            this.colintActionID.Caption = "Action ID";
            this.colintActionID.FieldName = "intActionID";
            this.colintActionID.Name = "colintActionID";
            this.colintActionID.OptionsColumn.AllowEdit = false;
            this.colintActionID.OptionsColumn.AllowFocus = false;
            this.colintActionID.OptionsColumn.ReadOnly = true;
            this.colintActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintActionID.Width = 65;
            // 
            // colstrJobNumber
            // 
            this.colstrJobNumber.Caption = "Job Number";
            this.colstrJobNumber.FieldName = "strJobNumber";
            this.colstrJobNumber.Name = "colstrJobNumber";
            this.colstrJobNumber.OptionsColumn.AllowEdit = false;
            this.colstrJobNumber.OptionsColumn.AllowFocus = false;
            this.colstrJobNumber.OptionsColumn.ReadOnly = true;
            this.colstrJobNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrJobNumber.Visible = true;
            this.colstrJobNumber.VisibleIndex = 5;
            this.colstrJobNumber.Width = 101;
            // 
            // colActionDescription
            // 
            this.colActionDescription.Caption = "Action";
            this.colActionDescription.FieldName = "ActionDescription";
            this.colActionDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colActionDescription.Name = "colActionDescription";
            this.colActionDescription.OptionsColumn.AllowEdit = false;
            this.colActionDescription.OptionsColumn.AllowFocus = false;
            this.colActionDescription.OptionsColumn.ReadOnly = true;
            this.colActionDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionDescription.Visible = true;
            this.colActionDescription.VisibleIndex = 1;
            this.colActionDescription.Width = 148;
            // 
            // colintWorkOrderID
            // 
            this.colintWorkOrderID.Caption = "Work Order ID";
            this.colintWorkOrderID.FieldName = "intWorkOrderID";
            this.colintWorkOrderID.Name = "colintWorkOrderID";
            this.colintWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colintWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colintWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colintWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintWorkOrderID.Width = 91;
            // 
            // colintPrevActionID
            // 
            this.colintPrevActionID.Caption = "Linked To Action";
            this.colintPrevActionID.FieldName = "intPrevActionID";
            this.colintPrevActionID.Name = "colintPrevActionID";
            this.colintPrevActionID.OptionsColumn.AllowEdit = false;
            this.colintPrevActionID.OptionsColumn.AllowFocus = false;
            this.colintPrevActionID.OptionsColumn.ReadOnly = true;
            this.colintPrevActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintPrevActionID.Width = 99;
            // 
            // colstrNearestHouse
            // 
            this.colstrNearestHouse.Caption = "Nearest House";
            this.colstrNearestHouse.ColumnEdit = this.repositoryItemTextEditNearestHouse;
            this.colstrNearestHouse.FieldName = "strNearestHouse";
            this.colstrNearestHouse.Name = "colstrNearestHouse";
            this.colstrNearestHouse.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrNearestHouse.Visible = true;
            this.colstrNearestHouse.VisibleIndex = 6;
            this.colstrNearestHouse.Width = 129;
            // 
            // repositoryItemTextEditNearestHouse
            // 
            this.repositoryItemTextEditNearestHouse.AutoHeight = false;
            this.repositoryItemTextEditNearestHouse.MaxLength = 50;
            this.repositoryItemTextEditNearestHouse.Name = "repositoryItemTextEditNearestHouse";
            this.repositoryItemTextEditNearestHouse.EditValueChanged += new System.EventHandler(this.repositoryItemTextEditNearestHouse_EditValueChanged);
            // 
            // colintPriorityID
            // 
            this.colintPriorityID.Caption = "Priority";
            this.colintPriorityID.ColumnEdit = this.repositoryItemGridLookUpEditPriority;
            this.colintPriorityID.FieldName = "intPriorityID";
            this.colintPriorityID.Name = "colintPriorityID";
            this.colintPriorityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintPriorityID.Tag = "126";
            this.colintPriorityID.Visible = true;
            this.colintPriorityID.VisibleIndex = 7;
            this.colintPriorityID.Width = 115;
            // 
            // repositoryItemGridLookUpEditPriority
            // 
            this.repositoryItemGridLookUpEditPriority.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.repositoryItemGridLookUpEditPriority.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemGridLookUpEditPriority.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.repositoryItemGridLookUpEditPriority.DisplayMember = "ItemDescription";
            this.repositoryItemGridLookUpEditPriority.Name = "repositoryItemGridLookUpEditPriority";
            this.repositoryItemGridLookUpEditPriority.NullText = "";
            this.repositoryItemGridLookUpEditPriority.PopupView = this.gridView17;
            this.repositoryItemGridLookUpEditPriority.ValueMember = "ItemID";
            this.repositoryItemGridLookUpEditPriority.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemGridLookUpEditPriority_ButtonClick);
            this.repositoryItemGridLookUpEditPriority.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditPriority_EditValueChanged);
            this.repositoryItemGridLookUpEditPriority.Enter += new System.EventHandler(this.repositoryItemGridLookUpEditPriority_Enter);
            this.repositoryItemGridLookUpEditPriority.Leave += new System.EventHandler(this.repositoryItemGridLookUpEditPriority_Leave);
            // 
            // sp01372ATMultiplePicklistsWithBlanksBindingSource
            // 
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataMember = "sp01372_AT_Multiple_Picklists_With_Blanks";
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView17
            // 
            this.gridView17.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn91,
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn94,
            this.gridColumn95,
            this.gridColumn96});
            this.gridView17.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn95;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView17.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView17.OptionsCustomization.AllowFilter = false;
            this.gridView17.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView17.OptionsFilter.AllowFilterEditor = false;
            this.gridView17.OptionsFilter.AllowMRUFilterList = false;
            this.gridView17.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView17.OptionsLayout.StoreAppearance = true;
            this.gridView17.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView17.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView17.OptionsView.ColumnAutoWidth = false;
            this.gridView17.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView17.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            this.gridView17.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView17.OptionsView.ShowIndicator = false;
            this.gridView17.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn96, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Picklist Type";
            this.gridColumn91.FieldName = "HeaderDescription";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            this.gridColumn91.Width = 189;
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Picklist ID";
            this.gridColumn92.FieldName = "HeaderID";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            this.gridColumn92.Width = 67;
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Item ID";
            this.gridColumn93.FieldName = "ItemCode";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Width = 58;
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Description";
            this.gridColumn94.FieldName = "ItemDescription";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            this.gridColumn94.Visible = true;
            this.gridColumn94.VisibleIndex = 0;
            this.gridColumn94.Width = 264;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Order";
            this.gridColumn96.FieldName = "Order";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Width = 65;
            // 
            // colintScheduleOfRates
            // 
            this.colintScheduleOfRates.Caption = "Schedule Of Rates ID";
            this.colintScheduleOfRates.FieldName = "intScheduleOfRates";
            this.colintScheduleOfRates.Name = "colintScheduleOfRates";
            this.colintScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colintScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colintScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colintScheduleOfRates.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintScheduleOfRates.Width = 124;
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Schedule";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionScheduleOfRates.ToolTip = "On Schedule of Rates";
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 13;
            this.colActionScheduleOfRates.Width = 64;
            // 
            // colintJobRateBudgeted
            // 
            this.colintJobRateBudgeted.Caption = "Job Rate Budgeted ID";
            this.colintJobRateBudgeted.FieldName = "intJobRateBudgeted";
            this.colintJobRateBudgeted.Name = "colintJobRateBudgeted";
            this.colintJobRateBudgeted.OptionsColumn.AllowEdit = false;
            this.colintJobRateBudgeted.OptionsColumn.AllowFocus = false;
            this.colintJobRateBudgeted.OptionsColumn.ReadOnly = true;
            this.colintJobRateBudgeted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintJobRateBudgeted.Width = 127;
            // 
            // colBudgetedRateDescription
            // 
            this.colBudgetedRateDescription.Caption = "Budgeted Rate Desc";
            this.colBudgetedRateDescription.FieldName = "BudgetedRateDescription";
            this.colBudgetedRateDescription.Name = "colBudgetedRateDescription";
            this.colBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colBudgetedRateDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBudgetedRateDescription.Width = 154;
            // 
            // colmonJobRateBudgeted
            // 
            this.colmonJobRateBudgeted.Caption = "Budgeted Rate";
            this.colmonJobRateBudgeted.ColumnEdit = this.repositoryItemTextEditMonJobRateActual;
            this.colmonJobRateBudgeted.FieldName = "monJobRateBudgeted";
            this.colmonJobRateBudgeted.Name = "colmonJobRateBudgeted";
            this.colmonJobRateBudgeted.OptionsColumn.AllowEdit = false;
            this.colmonJobRateBudgeted.OptionsColumn.AllowFocus = false;
            this.colmonJobRateBudgeted.OptionsColumn.ReadOnly = true;
            this.colmonJobRateBudgeted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonJobRateBudgeted.Width = 93;
            // 
            // repositoryItemTextEditMonJobRateActual
            // 
            this.repositoryItemTextEditMonJobRateActual.AutoHeight = false;
            this.repositoryItemTextEditMonJobRateActual.Mask.EditMask = "c";
            this.repositoryItemTextEditMonJobRateActual.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMonJobRateActual.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMonJobRateActual.Name = "repositoryItemTextEditMonJobRateActual";
            this.repositoryItemTextEditMonJobRateActual.EditValueChanged += new System.EventHandler(this.repositoryItemTextEditMonJobRateActual_EditValueChanged);
            // 
            // colmonJobWorkUnits
            // 
            this.colmonJobWorkUnits.Caption = "Work Units";
            this.colmonJobWorkUnits.ColumnEdit = this.repositoryItemSpinEditMonJobWorkUnits;
            this.colmonJobWorkUnits.FieldName = "monJobWorkUnits";
            this.colmonJobWorkUnits.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colmonJobWorkUnits.Name = "colmonJobWorkUnits";
            this.colmonJobWorkUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonJobWorkUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "monJobWorkUnits", "SUM={0:#.##}")});
            this.colmonJobWorkUnits.Visible = true;
            this.colmonJobWorkUnits.VisibleIndex = 16;
            this.colmonJobWorkUnits.Width = 73;
            // 
            // repositoryItemSpinEditMonJobWorkUnits
            // 
            this.repositoryItemSpinEditMonJobWorkUnits.AutoHeight = false;
            this.repositoryItemSpinEditMonJobWorkUnits.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditMonJobWorkUnits.Mask.EditMask = "#######0.00";
            this.repositoryItemSpinEditMonJobWorkUnits.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditMonJobWorkUnits.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            131072});
            this.repositoryItemSpinEditMonJobWorkUnits.Name = "repositoryItemSpinEditMonJobWorkUnits";
            this.repositoryItemSpinEditMonJobWorkUnits.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditMonJobWorkUnits_EditValueChanged);
            // 
            // colmonBudgetedCost
            // 
            this.colmonBudgetedCost.Caption = "Budgeted Cost";
            this.colmonBudgetedCost.ColumnEdit = this.repositoryItemTextEditMonJobRateActual;
            this.colmonBudgetedCost.FieldName = "monBudgetedCost";
            this.colmonBudgetedCost.Name = "colmonBudgetedCost";
            this.colmonBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colmonBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colmonBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colmonBudgetedCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonBudgetedCost.Width = 92;
            // 
            // colintJobRateActual
            // 
            this.colintJobRateActual.Caption = "Job Rate Actual ID";
            this.colintJobRateActual.FieldName = "intJobRateActual";
            this.colintJobRateActual.Name = "colintJobRateActual";
            this.colintJobRateActual.OptionsColumn.AllowEdit = false;
            this.colintJobRateActual.OptionsColumn.AllowFocus = false;
            this.colintJobRateActual.OptionsColumn.ReadOnly = true;
            this.colintJobRateActual.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintJobRateActual.Width = 111;
            // 
            // colActualJobRateDescription
            // 
            this.colActualJobRateDescription.Caption = "Rate Description";
            this.colActualJobRateDescription.ColumnEdit = this.repositoryItemButtonEditActualJobRateDescription;
            this.colActualJobRateDescription.FieldName = "ActualJobRateDescription";
            this.colActualJobRateDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colActualJobRateDescription.Name = "colActualJobRateDescription";
            this.colActualJobRateDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualJobRateDescription.Visible = true;
            this.colActualJobRateDescription.VisibleIndex = 14;
            this.colActualJobRateDescription.Width = 172;
            // 
            // repositoryItemButtonEditActualJobRateDescription
            // 
            this.repositoryItemButtonEditActualJobRateDescription.AutoHeight = false;
            this.repositoryItemButtonEditActualJobRateDescription.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Rate Selection Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Job Rate", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditActualJobRateDescription.Name = "repositoryItemButtonEditActualJobRateDescription";
            this.repositoryItemButtonEditActualJobRateDescription.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditActualJobRateDescription.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditActualJobRateDescription_ButtonClick);
            this.repositoryItemButtonEditActualJobRateDescription.EditValueChanged += new System.EventHandler(this.repositoryItemButtonEditActualJobRateDescription_EditValueChanged);
            // 
            // colmonJobRateActual
            // 
            this.colmonJobRateActual.Caption = "Job Rate";
            this.colmonJobRateActual.ColumnEdit = this.repositoryItemTextEditMonJobRateActual;
            this.colmonJobRateActual.FieldName = "monJobRateActual";
            this.colmonJobRateActual.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colmonJobRateActual.Name = "colmonJobRateActual";
            this.colmonJobRateActual.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonJobRateActual.Visible = true;
            this.colmonJobRateActual.VisibleIndex = 15;
            this.colmonJobRateActual.Width = 81;
            // 
            // colmonDiscountRate
            // 
            this.colmonDiscountRate.Caption = "Discount %";
            this.colmonDiscountRate.ColumnEdit = this.repositoryItemSpinEditDiscountRate;
            this.colmonDiscountRate.FieldName = "monDiscountRate";
            this.colmonDiscountRate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colmonDiscountRate.Name = "colmonDiscountRate";
            this.colmonDiscountRate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonDiscountRate.Visible = true;
            this.colmonDiscountRate.VisibleIndex = 17;
            this.colmonDiscountRate.Width = 76;
            // 
            // repositoryItemSpinEditDiscountRate
            // 
            this.repositoryItemSpinEditDiscountRate.AutoHeight = false;
            this.repositoryItemSpinEditDiscountRate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditDiscountRate.Mask.EditMask = "P";
            this.repositoryItemSpinEditDiscountRate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditDiscountRate.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEditDiscountRate.MinValue = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.repositoryItemSpinEditDiscountRate.Name = "repositoryItemSpinEditDiscountRate";
            this.repositoryItemSpinEditDiscountRate.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditDiscountRate_EditValueChanged);
            // 
            // colmonActualCost
            // 
            this.colmonActualCost.Caption = "Job Cost";
            this.colmonActualCost.ColumnEdit = this.repositoryItemTextEditMonJobRateActual;
            this.colmonActualCost.FieldName = "monActualCost";
            this.colmonActualCost.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colmonActualCost.Name = "colmonActualCost";
            this.colmonActualCost.OptionsColumn.AllowEdit = false;
            this.colmonActualCost.OptionsColumn.AllowFocus = false;
            this.colmonActualCost.OptionsColumn.ReadOnly = true;
            this.colmonActualCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonActualCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "monActualCost", "SUM={0:#.##}")});
            this.colmonActualCost.Visible = true;
            this.colmonActualCost.VisibleIndex = 18;
            this.colmonActualCost.Width = 79;
            // 
            // colintInspectionID
            // 
            this.colintInspectionID.Caption = "Inspection ID";
            this.colintInspectionID.FieldName = "intInspectionID";
            this.colintInspectionID.Name = "colintInspectionID";
            this.colintInspectionID.OptionsColumn.AllowEdit = false;
            this.colintInspectionID.OptionsColumn.AllowFocus = false;
            this.colintInspectionID.OptionsColumn.ReadOnly = true;
            this.colintInspectionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintInspectionID.Width = 85;
            // 
            // colintAction
            // 
            this.colintAction.Caption = "Action Type ID";
            this.colintAction.FieldName = "intAction";
            this.colintAction.Name = "colintAction";
            this.colintAction.OptionsColumn.AllowEdit = false;
            this.colintAction.OptionsColumn.AllowFocus = false;
            this.colintAction.OptionsColumn.ReadOnly = true;
            this.colintAction.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintAction.Width = 92;
            // 
            // colstrUser1
            // 
            this.colstrUser1.Caption = "User Text 1";
            this.colstrUser1.FieldName = "strUser1";
            this.colstrUser1.Name = "colstrUser1";
            this.colstrUser1.OptionsColumn.AllowEdit = false;
            this.colstrUser1.OptionsColumn.AllowFocus = false;
            this.colstrUser1.OptionsColumn.ReadOnly = true;
            this.colstrUser1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrUser1.Width = 77;
            // 
            // colstrUser2
            // 
            this.colstrUser2.Caption = "User Text 2";
            this.colstrUser2.FieldName = "strUser2";
            this.colstrUser2.Name = "colstrUser2";
            this.colstrUser2.OptionsColumn.AllowEdit = false;
            this.colstrUser2.OptionsColumn.AllowFocus = false;
            this.colstrUser2.OptionsColumn.ReadOnly = true;
            this.colstrUser2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrUser2.Width = 77;
            // 
            // colstrUser3
            // 
            this.colstrUser3.Caption = "User Text 3";
            this.colstrUser3.FieldName = "strUser3";
            this.colstrUser3.Name = "colstrUser3";
            this.colstrUser3.OptionsColumn.AllowEdit = false;
            this.colstrUser3.OptionsColumn.AllowFocus = false;
            this.colstrUser3.OptionsColumn.ReadOnly = true;
            this.colstrUser3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrUser3.Width = 77;
            // 
            // colUserPickListDescription1
            // 
            this.colUserPickListDescription1.Caption = "User Picklist 1";
            this.colUserPickListDescription1.FieldName = "UserPickListDescription1";
            this.colUserPickListDescription1.Name = "colUserPickListDescription1";
            this.colUserPickListDescription1.OptionsColumn.AllowEdit = false;
            this.colUserPickListDescription1.OptionsColumn.AllowFocus = false;
            this.colUserPickListDescription1.OptionsColumn.ReadOnly = true;
            this.colUserPickListDescription1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickListDescription1.Width = 86;
            // 
            // colUserPickListDescription2
            // 
            this.colUserPickListDescription2.Caption = "User Picklist 2";
            this.colUserPickListDescription2.FieldName = "UserPickListDescription2";
            this.colUserPickListDescription2.Name = "colUserPickListDescription2";
            this.colUserPickListDescription2.OptionsColumn.AllowEdit = false;
            this.colUserPickListDescription2.OptionsColumn.AllowFocus = false;
            this.colUserPickListDescription2.OptionsColumn.ReadOnly = true;
            this.colUserPickListDescription2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickListDescription2.Width = 86;
            // 
            // colUserPickListDescription3
            // 
            this.colUserPickListDescription3.Caption = "User Picklist 3";
            this.colUserPickListDescription3.FieldName = "UserPickListDescription3";
            this.colUserPickListDescription3.Name = "colUserPickListDescription3";
            this.colUserPickListDescription3.OptionsColumn.AllowEdit = false;
            this.colUserPickListDescription3.OptionsColumn.AllowFocus = false;
            this.colUserPickListDescription3.OptionsColumn.ReadOnly = true;
            this.colUserPickListDescription3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickListDescription3.Width = 86;
            // 
            // colstrRemarks
            // 
            this.colstrRemarks.Caption = "Remarks";
            this.colstrRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colstrRemarks.FieldName = "strRemarks";
            this.colstrRemarks.Name = "colstrRemarks";
            this.colstrRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrRemarks.Visible = true;
            this.colstrRemarks.VisibleIndex = 12;
            this.colstrRemarks.Width = 62;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            this.repositoryItemMemoExEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemMemoExEdit1_EditValueChanged);
            // 
            // colintSortOrder
            // 
            this.colintSortOrder.Caption = "Order";
            this.colintSortOrder.FieldName = "intSortOrder";
            this.colintSortOrder.Name = "colintSortOrder";
            this.colintSortOrder.OptionsColumn.AllowEdit = false;
            this.colintSortOrder.OptionsColumn.AllowFocus = false;
            this.colintSortOrder.OptionsColumn.ReadOnly = true;
            this.colintSortOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintSortOrder.Width = 65;
            // 
            // colintActionBy
            // 
            this.colintActionBy.Caption = "Action By";
            this.colintActionBy.ColumnEdit = this.repositoryItemGridLookUpEditActionByPerson;
            this.colintActionBy.FieldName = "intActionBy";
            this.colintActionBy.Name = "colintActionBy";
            this.colintActionBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintActionBy.Visible = true;
            this.colintActionBy.VisibleIndex = 10;
            this.colintActionBy.Width = 126;
            // 
            // repositoryItemGridLookUpEditActionByPerson
            // 
            this.repositoryItemGridLookUpEditActionByPerson.AutoHeight = false;
            editorButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions5.Image")));
            editorButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions6.Image")));
            this.repositoryItemGridLookUpEditActionByPerson.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemGridLookUpEditActionByPerson.DataSource = this.sp00190ContractorListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditActionByPerson.DisplayMember = "ContractorName";
            this.repositoryItemGridLookUpEditActionByPerson.Name = "repositoryItemGridLookUpEditActionByPerson";
            this.repositoryItemGridLookUpEditActionByPerson.NullText = "";
            this.repositoryItemGridLookUpEditActionByPerson.PopupView = this.gridView10;
            this.repositoryItemGridLookUpEditActionByPerson.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.repositoryItemGridLookUpEditActionByPerson.ValueMember = "ContractorID";
            this.repositoryItemGridLookUpEditActionByPerson.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemGridLookUpEditActionByPerson_ButtonClick);
            this.repositoryItemGridLookUpEditActionByPerson.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditActionByPerson_EditValueChanged);
            // 
            // sp00190ContractorListWithBlankBindingSource
            // 
            this.sp00190ContractorListWithBlankBindingSource.DataMember = "sp00190_Contractor_List_With_Blank";
            this.sp00190ContractorListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colContractorCode,
            this.colContractorID,
            this.colContractorName,
            this.colDisabled,
            this.colEmailPassword,
            this.colInternalContractor,
            this.colInternalCOntractorDescription,
            this.colMobile,
            this.colPostcode,
            this.gridColumn97,
            this.colTelephone1,
            this.colTelephone2,
            this.colTypeDescription,
            this.colTypeID,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colVatReg,
            this.colWebsite});
            this.gridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colContractorID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView10.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView10.GroupCount = 1;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView10.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView10.OptionsView.ShowIndicator = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInternalCOntractorDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 273;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor ID";
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 118;
            // 
            // colInternalCOntractorDescription
            // 
            this.colInternalCOntractorDescription.Caption = "Status";
            this.colInternalCOntractorDescription.FieldName = "InternalCOntractorDescription";
            this.colInternalCOntractorDescription.Name = "colInternalCOntractorDescription";
            this.colInternalCOntractorDescription.OptionsColumn.AllowEdit = false;
            this.colInternalCOntractorDescription.OptionsColumn.AllowFocus = false;
            this.colInternalCOntractorDescription.OptionsColumn.ReadOnly = true;
            this.colInternalCOntractorDescription.Width = 52;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Width = 51;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Remarks";
            this.gridColumn97.FieldName = "Remarks";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Width = 62;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 1;
            this.colTypeDescription.Width = 111;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.Width = 49;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Reg";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 62;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // colintSupervisor
            // 
            this.colintSupervisor.Caption = "Supervisor";
            this.colintSupervisor.ColumnEdit = this.repositoryItemGridLookUpEditSupervisor;
            this.colintSupervisor.FieldName = "intSupervisor";
            this.colintSupervisor.Name = "colintSupervisor";
            this.colintSupervisor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintSupervisor.Visible = true;
            this.colintSupervisor.VisibleIndex = 11;
            this.colintSupervisor.Width = 119;
            // 
            // repositoryItemGridLookUpEditSupervisor
            // 
            this.repositoryItemGridLookUpEditSupervisor.AutoHeight = false;
            editorButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions7.Image")));
            editorButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions8.Image")));
            this.repositoryItemGridLookUpEditSupervisor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemGridLookUpEditSupervisor.DataSource = this.sp00190ContractorListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditSupervisor.DisplayMember = "ContractorName";
            this.repositoryItemGridLookUpEditSupervisor.Name = "repositoryItemGridLookUpEditSupervisor";
            this.repositoryItemGridLookUpEditSupervisor.NullText = "";
            this.repositoryItemGridLookUpEditSupervisor.PopupView = this.gridView16;
            this.repositoryItemGridLookUpEditSupervisor.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3});
            this.repositoryItemGridLookUpEditSupervisor.ValueMember = "ContractorID";
            this.repositoryItemGridLookUpEditSupervisor.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemGridLookUpEditSupervisor_ButtonClick);
            this.repositoryItemGridLookUpEditSupervisor.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditSupervisor_EditValueChanged);
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // gridView16
            // 
            this.gridView16.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn98,
            this.gridColumn99,
            this.gridColumn100,
            this.gridColumn101,
            this.gridColumn102,
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106,
            this.gridColumn107,
            this.gridColumn108,
            this.gridColumn109,
            this.gridColumn110,
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113,
            this.gridColumn114,
            this.gridColumn115,
            this.gridColumn116,
            this.gridColumn117,
            this.gridColumn118,
            this.gridColumn119,
            this.gridColumn120,
            this.gridColumn121});
            this.gridView16.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn104;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView16.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView16.GroupCount = 1;
            this.gridView16.Name = "gridView16";
            this.gridView16.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView16.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView16.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView16.OptionsLayout.StoreAppearance = true;
            this.gridView16.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView16.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView16.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView16.OptionsView.ColumnAutoWidth = false;
            this.gridView16.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView16.OptionsView.ShowGroupPanel = false;
            this.gridView16.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView16.OptionsView.ShowIndicator = false;
            this.gridView16.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn109, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn105, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Address Line 1";
            this.gridColumn98.FieldName = "AddressLine1";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Visible = true;
            this.gridColumn98.VisibleIndex = 2;
            this.gridColumn98.Width = 144;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Address Line 2";
            this.gridColumn99.FieldName = "AddressLine2";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            this.gridColumn99.Width = 91;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Address Line 3";
            this.gridColumn100.FieldName = "AddressLine3";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.Width = 91;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Address Line 4";
            this.gridColumn101.FieldName = "AddressLine4";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.AllowEdit = false;
            this.gridColumn101.OptionsColumn.AllowFocus = false;
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            this.gridColumn101.Width = 91;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "Address Line 5";
            this.gridColumn102.FieldName = "AddressLine5";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.AllowEdit = false;
            this.gridColumn102.OptionsColumn.AllowFocus = false;
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.Width = 91;
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "Contractor Code";
            this.gridColumn103.FieldName = "ContractorCode";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            this.gridColumn103.Width = 101;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Contractor Name";
            this.gridColumn105.FieldName = "ContractorName";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Visible = true;
            this.gridColumn105.VisibleIndex = 0;
            this.gridColumn105.Width = 273;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Disabled";
            this.gridColumn106.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn106.FieldName = "Disabled";
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowFocus = false;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.Visible = true;
            this.gridColumn106.VisibleIndex = 3;
            this.gridColumn106.Width = 61;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "Email Password";
            this.gridColumn107.FieldName = "EmailPassword";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.AllowEdit = false;
            this.gridColumn107.OptionsColumn.AllowFocus = false;
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.Width = 94;
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "Internal Contractor ID";
            this.gridColumn108.FieldName = "InternalContractor";
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowFocus = false;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            this.gridColumn108.Width = 118;
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "Status";
            this.gridColumn109.FieldName = "InternalCOntractorDescription";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowFocus = false;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.Width = 52;
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "Mobile";
            this.gridColumn110.FieldName = "Mobile";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowFocus = false;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            this.gridColumn110.Width = 51;
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "Postcode";
            this.gridColumn111.FieldName = "Postcode";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.AllowEdit = false;
            this.gridColumn111.OptionsColumn.AllowFocus = false;
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            this.gridColumn111.Width = 65;
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "Remarks";
            this.gridColumn112.FieldName = "Remarks";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.Width = 62;
            // 
            // gridColumn113
            // 
            this.gridColumn113.Caption = "Telephone 1";
            this.gridColumn113.FieldName = "Telephone1";
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.OptionsColumn.AllowEdit = false;
            this.gridColumn113.OptionsColumn.AllowFocus = false;
            this.gridColumn113.OptionsColumn.ReadOnly = true;
            this.gridColumn113.Width = 80;
            // 
            // gridColumn114
            // 
            this.gridColumn114.Caption = "Telephone 2";
            this.gridColumn114.FieldName = "Telephone2";
            this.gridColumn114.Name = "gridColumn114";
            this.gridColumn114.OptionsColumn.AllowEdit = false;
            this.gridColumn114.OptionsColumn.AllowFocus = false;
            this.gridColumn114.OptionsColumn.ReadOnly = true;
            this.gridColumn114.Width = 80;
            // 
            // gridColumn115
            // 
            this.gridColumn115.Caption = "Type";
            this.gridColumn115.FieldName = "TypeDescription";
            this.gridColumn115.Name = "gridColumn115";
            this.gridColumn115.OptionsColumn.AllowEdit = false;
            this.gridColumn115.OptionsColumn.AllowFocus = false;
            this.gridColumn115.OptionsColumn.ReadOnly = true;
            this.gridColumn115.Visible = true;
            this.gridColumn115.VisibleIndex = 1;
            this.gridColumn115.Width = 111;
            // 
            // gridColumn116
            // 
            this.gridColumn116.Caption = "Type ID";
            this.gridColumn116.FieldName = "TypeID";
            this.gridColumn116.Name = "gridColumn116";
            this.gridColumn116.OptionsColumn.AllowEdit = false;
            this.gridColumn116.OptionsColumn.AllowFocus = false;
            this.gridColumn116.OptionsColumn.ReadOnly = true;
            this.gridColumn116.Width = 49;
            // 
            // gridColumn117
            // 
            this.gridColumn117.Caption = "User Defined 1";
            this.gridColumn117.FieldName = "UserDefined1";
            this.gridColumn117.Name = "gridColumn117";
            this.gridColumn117.OptionsColumn.AllowEdit = false;
            this.gridColumn117.OptionsColumn.AllowFocus = false;
            this.gridColumn117.OptionsColumn.ReadOnly = true;
            this.gridColumn117.Width = 92;
            // 
            // gridColumn118
            // 
            this.gridColumn118.Caption = "User Defined 2";
            this.gridColumn118.FieldName = "UserDefined2";
            this.gridColumn118.Name = "gridColumn118";
            this.gridColumn118.OptionsColumn.AllowEdit = false;
            this.gridColumn118.OptionsColumn.AllowFocus = false;
            this.gridColumn118.OptionsColumn.ReadOnly = true;
            this.gridColumn118.Width = 92;
            // 
            // gridColumn119
            // 
            this.gridColumn119.Caption = "User Defined 3";
            this.gridColumn119.FieldName = "UserDefined3";
            this.gridColumn119.Name = "gridColumn119";
            this.gridColumn119.OptionsColumn.AllowEdit = false;
            this.gridColumn119.OptionsColumn.AllowFocus = false;
            this.gridColumn119.OptionsColumn.ReadOnly = true;
            this.gridColumn119.Width = 92;
            // 
            // gridColumn120
            // 
            this.gridColumn120.Caption = "VAT Reg";
            this.gridColumn120.FieldName = "VatReg";
            this.gridColumn120.Name = "gridColumn120";
            this.gridColumn120.OptionsColumn.AllowEdit = false;
            this.gridColumn120.OptionsColumn.AllowFocus = false;
            this.gridColumn120.OptionsColumn.ReadOnly = true;
            this.gridColumn120.Width = 62;
            // 
            // gridColumn121
            // 
            this.gridColumn121.Caption = "Website";
            this.gridColumn121.FieldName = "Website";
            this.gridColumn121.Name = "gridColumn121";
            this.gridColumn121.OptionsColumn.AllowEdit = false;
            this.gridColumn121.OptionsColumn.AllowFocus = false;
            this.gridColumn121.OptionsColumn.ReadOnly = true;
            this.gridColumn121.Width = 60;
            // 
            // colintBudgetID
            // 
            this.colintBudgetID.Caption = "Budget ID";
            this.colintBudgetID.FieldName = "intBudgetID";
            this.colintBudgetID.Name = "colintBudgetID";
            this.colintBudgetID.OptionsColumn.AllowEdit = false;
            this.colintBudgetID.OptionsColumn.AllowFocus = false;
            this.colintBudgetID.OptionsColumn.ReadOnly = true;
            this.colintBudgetID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintBudgetID.Width = 69;
            // 
            // colintOwnershipID
            // 
            this.colintOwnershipID.Caption = "Ownership";
            this.colintOwnershipID.ColumnEdit = this.repositoryItemGridLookUpEditOwnership;
            this.colintOwnershipID.FieldName = "intOwnershipID";
            this.colintOwnershipID.Name = "colintOwnershipID";
            this.colintOwnershipID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintOwnershipID.Visible = true;
            this.colintOwnershipID.VisibleIndex = 8;
            this.colintOwnershipID.Width = 120;
            // 
            // repositoryItemGridLookUpEditOwnership
            // 
            this.repositoryItemGridLookUpEditOwnership.AutoHeight = false;
            editorButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions9.Image")));
            editorButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions10.Image")));
            this.repositoryItemGridLookUpEditOwnership.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemGridLookUpEditOwnership.DataSource = this.sp01362ATOwnershipListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditOwnership.DisplayMember = "OwnershipName";
            this.repositoryItemGridLookUpEditOwnership.Name = "repositoryItemGridLookUpEditOwnership";
            this.repositoryItemGridLookUpEditOwnership.NullText = "";
            this.repositoryItemGridLookUpEditOwnership.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditOwnership.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.repositoryItemGridLookUpEditOwnership.ValueMember = "OwnershipID";
            this.repositoryItemGridLookUpEditOwnership.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemGridLookUpEditOwnership_ButtonClick);
            this.repositoryItemGridLookUpEditOwnership.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditOwnership_EditValueChanged);
            // 
            // sp01362ATOwnershipListWithBlankBindingSource
            // 
            this.sp01362ATOwnershipListWithBlankBindingSource.DataMember = "sp01362_AT_Ownership_List_With_Blank";
            this.sp01362ATOwnershipListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOwnershipID,
            this.colOwnershipCode,
            this.colOwnershipName,
            this.colRemarks});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOwnershipName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colOwnershipID
            // 
            this.colOwnershipID.Caption = "Ownership ID";
            this.colOwnershipID.FieldName = "OwnershipID";
            this.colOwnershipID.Name = "colOwnershipID";
            this.colOwnershipID.OptionsColumn.AllowEdit = false;
            this.colOwnershipID.OptionsColumn.AllowFocus = false;
            this.colOwnershipID.OptionsColumn.ReadOnly = true;
            this.colOwnershipID.Width = 86;
            // 
            // colOwnershipCode
            // 
            this.colOwnershipCode.Caption = "Ownership Code";
            this.colOwnershipCode.FieldName = "OwnershipCode";
            this.colOwnershipCode.Name = "colOwnershipCode";
            this.colOwnershipCode.OptionsColumn.AllowEdit = false;
            this.colOwnershipCode.OptionsColumn.AllowFocus = false;
            this.colOwnershipCode.OptionsColumn.ReadOnly = true;
            this.colOwnershipCode.Width = 100;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.Caption = "Ownership Name";
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.OptionsColumn.AllowEdit = false;
            this.colOwnershipName.OptionsColumn.AllowFocus = false;
            this.colOwnershipName.OptionsColumn.ReadOnly = true;
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 0;
            this.colOwnershipName.Width = 209;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 1;
            this.colRemarks.Width = 118;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.Caption = "Cost Centre";
            this.colCostCentreCode.ColumnEdit = this.repositoryItemButtonEditCostCentreCode;
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode.Visible = true;
            this.colCostCentreCode.VisibleIndex = 9;
            this.colCostCentreCode.Width = 136;
            // 
            // repositoryItemButtonEditCostCentreCode
            // 
            this.repositoryItemButtonEditCostCentreCode.AutoHeight = false;
            this.repositoryItemButtonEditCostCentreCode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Click me to Open Cost Centre Selection Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Clear Cost Centre", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditCostCentreCode.Name = "repositoryItemButtonEditCostCentreCode";
            this.repositoryItemButtonEditCostCentreCode.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditCostCentreCode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditCostCentreCode_ButtonClick);
            this.repositoryItemButtonEditCostCentreCode.EditValueChanged += new System.EventHandler(this.repositoryItemButtonEditCostCentreCode_EditValueChanged);
            // 
            // colintCostCentreID
            // 
            this.colintCostCentreID.Caption = "Cost Centre ID";
            this.colintCostCentreID.FieldName = "intCostCentreID";
            this.colintCostCentreID.Name = "colintCostCentreID";
            this.colintCostCentreID.OptionsColumn.AllowEdit = false;
            this.colintCostCentreID.OptionsColumn.AllowFocus = false;
            this.colintCostCentreID.OptionsColumn.ReadOnly = true;
            this.colintCostCentreID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintCostCentreID.Width = 93;
            // 
            // intWorkOrderIDTextEdit
            // 
            this.intWorkOrderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intWorkOrderID", true));
            this.intWorkOrderIDTextEdit.Location = new System.Drawing.Point(121, 72);
            this.intWorkOrderIDTextEdit.MenuManager = this.barManager1;
            this.intWorkOrderIDTextEdit.Name = "intWorkOrderIDTextEdit";
            this.intWorkOrderIDTextEdit.Properties.Mask.EditMask = "0000000";
            this.intWorkOrderIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.intWorkOrderIDTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.intWorkOrderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intWorkOrderIDTextEdit, true);
            this.intWorkOrderIDTextEdit.Size = new System.Drawing.Size(427, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intWorkOrderIDTextEdit, optionsSpelling2);
            this.intWorkOrderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intWorkOrderIDTextEdit.TabIndex = 4;
            // 
            // dtIssueDateDateEdit
            // 
            this.dtIssueDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "dtIssueDate", true));
            this.dtIssueDateDateEdit.EditValue = null;
            this.dtIssueDateDateEdit.Location = new System.Drawing.Point(145, 274);
            this.dtIssueDateDateEdit.MenuManager = this.barManager1;
            this.dtIssueDateDateEdit.Name = "dtIssueDateDateEdit";
            this.dtIssueDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions13, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dtIssueDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtIssueDateDateEdit.Size = new System.Drawing.Size(170, 20);
            this.dtIssueDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtIssueDateDateEdit.TabIndex = 9;
            this.dtIssueDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtIssueDateDateEdit_ButtonClick);
            // 
            // strInvoiceNumberButtonEdit
            // 
            this.strInvoiceNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "strInvoiceNumber", true));
            this.strInvoiceNumberButtonEdit.Location = new System.Drawing.Point(649, 96);
            this.strInvoiceNumberButtonEdit.MenuManager = this.barManager1;
            this.strInvoiceNumberButtonEdit.Name = "strInvoiceNumberButtonEdit";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Sequence Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Number Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.strInvoiceNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions14, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "", "Sequence", superToolTip4, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions15, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject57, serializableAppearanceObject58, serializableAppearanceObject59, serializableAppearanceObject60, "", "Number", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.strInvoiceNumberButtonEdit.Properties.MaxLength = 20;
            this.strInvoiceNumberButtonEdit.Size = new System.Drawing.Size(470, 20);
            this.strInvoiceNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.strInvoiceNumberButtonEdit.TabIndex = 19;
            this.strInvoiceNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.strInvoiceNumberButtonEdit_ButtonClick);
            // 
            // intOperationIDSpinEdit
            // 
            this.intOperationIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intOperationID", true));
            this.intOperationIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intOperationIDSpinEdit.Location = new System.Drawing.Point(141, 455);
            this.intOperationIDSpinEdit.MenuManager = this.barManager1;
            this.intOperationIDSpinEdit.Name = "intOperationIDSpinEdit";
            this.intOperationIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intOperationIDSpinEdit.Size = new System.Drawing.Size(973, 20);
            this.intOperationIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.intOperationIDSpinEdit.TabIndex = 20;
            // 
            // strEmailDetailsMemoEdit
            // 
            this.strEmailDetailsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "strEmailDetails", true));
            this.strEmailDetailsMemoEdit.Location = new System.Drawing.Point(141, 525);
            this.strEmailDetailsMemoEdit.MenuManager = this.barManager1;
            this.strEmailDetailsMemoEdit.Name = "strEmailDetailsMemoEdit";
            this.strEmailDetailsMemoEdit.Properties.MaxLength = 5000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strEmailDetailsMemoEdit, true);
            this.strEmailDetailsMemoEdit.Size = new System.Drawing.Size(973, 16);
            this.scSpellChecker.SetSpellCheckerOptions(this.strEmailDetailsMemoEdit, optionsSpelling3);
            this.strEmailDetailsMemoEdit.StyleController = this.dataLayoutControl1;
            this.strEmailDetailsMemoEdit.TabIndex = 27;
            // 
            // strUser1TextEdit
            // 
            this.strUser1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "strUser1", true));
            this.strUser1TextEdit.Location = new System.Drawing.Point(157, 284);
            this.strUser1TextEdit.MenuManager = this.barManager1;
            this.strUser1TextEdit.Name = "strUser1TextEdit";
            this.strUser1TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser1TextEdit, true);
            this.strUser1TextEdit.Size = new System.Drawing.Size(378, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser1TextEdit, optionsSpelling4);
            this.strUser1TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser1TextEdit.TabIndex = 21;
            // 
            // strUser2TextEdit
            // 
            this.strUser2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "strUser2", true));
            this.strUser2TextEdit.Location = new System.Drawing.Point(157, 308);
            this.strUser2TextEdit.MenuManager = this.barManager1;
            this.strUser2TextEdit.Name = "strUser2TextEdit";
            this.strUser2TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser2TextEdit, true);
            this.strUser2TextEdit.Size = new System.Drawing.Size(378, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser2TextEdit, optionsSpelling5);
            this.strUser2TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser2TextEdit.TabIndex = 22;
            // 
            // strUser3TextEdit
            // 
            this.strUser3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "strUser3", true));
            this.strUser3TextEdit.Location = new System.Drawing.Point(157, 332);
            this.strUser3TextEdit.MenuManager = this.barManager1;
            this.strUser3TextEdit.Name = "strUser3TextEdit";
            this.strUser3TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser3TextEdit, true);
            this.strUser3TextEdit.Size = new System.Drawing.Size(378, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser3TextEdit, optionsSpelling6);
            this.strUser3TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser3TextEdit.TabIndex = 23;
            // 
            // dtCompleteDateDateEdit
            // 
            this.dtCompleteDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "dtCompleteDate", true));
            this.dtCompleteDateDateEdit.EditValue = null;
            this.dtCompleteDateDateEdit.Location = new System.Drawing.Point(145, 298);
            this.dtCompleteDateDateEdit.MenuManager = this.barManager1;
            this.dtCompleteDateDateEdit.Name = "dtCompleteDateDateEdit";
            this.dtCompleteDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear Date", -1, true, true, false, editorButtonImageOptions16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject61, serializableAppearanceObject62, serializableAppearanceObject63, serializableAppearanceObject64, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dtCompleteDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtCompleteDateDateEdit.Size = new System.Drawing.Size(170, 20);
            this.dtCompleteDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtCompleteDateDateEdit.TabIndex = 24;
            this.dtCompleteDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtCompleteDateDateEdit_ButtonClick);
            this.dtCompleteDateDateEdit.EditValueChanged += new System.EventHandler(this.dtCompleteDateDateEdit_EditValueChanged);
            this.dtCompleteDateDateEdit.Validated += new System.EventHandler(this.dtCompleteDateDateEdit_Validated);
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(48, 250);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(1047, 130);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling7);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 26;
            // 
            // WorkOrderReferenceButtonEdit
            // 
            this.WorkOrderReferenceButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "WorkOrderReference", true));
            this.WorkOrderReferenceButtonEdit.Location = new System.Drawing.Point(121, 96);
            this.WorkOrderReferenceButtonEdit.MenuManager = this.barManager1;
            this.WorkOrderReferenceButtonEdit.Name = "WorkOrderReferenceButtonEdit";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Sequence Button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Number Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.WorkOrderReferenceButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions17, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject65, serializableAppearanceObject66, serializableAppearanceObject67, serializableAppearanceObject68, "", "Sequence", superToolTip6, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions18, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject69, serializableAppearanceObject70, serializableAppearanceObject71, serializableAppearanceObject72, "", "Number", superToolTip7, DevExpress.Utils.ToolTipAnchor.Default)});
            this.WorkOrderReferenceButtonEdit.Properties.MaxLength = 20;
            this.WorkOrderReferenceButtonEdit.Size = new System.Drawing.Size(427, 20);
            this.WorkOrderReferenceButtonEdit.StyleController = this.dataLayoutControl1;
            this.WorkOrderReferenceButtonEdit.TabIndex = 31;
            this.WorkOrderReferenceButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.WorkOrderReferenceButtonEdit_ButtonClick);
            // 
            // TargetDateDateEdit
            // 
            this.TargetDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "TargetDate", true));
            this.TargetDateDateEdit.EditValue = null;
            this.TargetDateDateEdit.Location = new System.Drawing.Point(145, 250);
            this.TargetDateDateEdit.MenuManager = this.barManager1;
            this.TargetDateDateEdit.Name = "TargetDateDateEdit";
            this.TargetDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear Date", -1, true, true, false, editorButtonImageOptions19, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject73, serializableAppearanceObject74, serializableAppearanceObject75, serializableAppearanceObject76, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.TargetDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TargetDateDateEdit.Size = new System.Drawing.Size(170, 20);
            this.TargetDateDateEdit.StyleController = this.dataLayoutControl1;
            this.TargetDateDateEdit.TabIndex = 36;
            this.TargetDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TargetDateDateEdit_ButtonClick);
            // 
            // intPrevWorkOrderIDButtonEdit
            // 
            this.intPrevWorkOrderIDButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intPrevWorkOrderID", true));
            this.intPrevWorkOrderIDButtonEdit.EditValue = "";
            this.intPrevWorkOrderIDButtonEdit.Location = new System.Drawing.Point(649, 72);
            this.intPrevWorkOrderIDButtonEdit.MenuManager = this.barManager1;
            this.intPrevWorkOrderIDButtonEdit.Name = "intPrevWorkOrderIDButtonEdit";
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Linked To Work Order Choose Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to display a list of previous work orders from which the linked work ord" +
    "er can be selected.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.intPrevWorkOrderIDButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions20, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject77, serializableAppearanceObject78, serializableAppearanceObject79, serializableAppearanceObject80, "", "choose", superToolTip8, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Delete", -1, true, true, false, editorButtonImageOptions21, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject81, serializableAppearanceObject82, serializableAppearanceObject83, serializableAppearanceObject84, "Clear Linked Work Order", "delete", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intPrevWorkOrderIDButtonEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.intPrevWorkOrderIDButtonEdit.Properties.Mask.EditMask = "0000000";
            this.intPrevWorkOrderIDButtonEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.intPrevWorkOrderIDButtonEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.intPrevWorkOrderIDButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.intPrevWorkOrderIDButtonEdit.Size = new System.Drawing.Size(470, 20);
            this.intPrevWorkOrderIDButtonEdit.StyleController = this.dataLayoutControl1;
            this.intPrevWorkOrderIDButtonEdit.TabIndex = 5;
            this.intPrevWorkOrderIDButtonEdit.TabStop = false;
            this.intPrevWorkOrderIDButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intPrevWorkOrderIDButtonEdit_ButtonClick);
            // 
            // StatusGridLookUpEdit
            // 
            this.StatusGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "Status", true));
            this.StatusGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StatusGridLookUpEdit.Location = new System.Drawing.Point(121, 120);
            this.StatusGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusGridLookUpEdit.Name = "StatusGridLookUpEdit";
            editorButtonImageOptions22.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions23.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.StatusGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions22, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject85, serializableAppearanceObject86, serializableAppearanceObject87, serializableAppearanceObject88, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions23, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject89, serializableAppearanceObject90, serializableAppearanceObject91, serializableAppearanceObject92, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.StatusGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.StatusGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.StatusGridLookUpEdit.Properties.NullText = "";
            this.StatusGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.StatusGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.StatusGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.StatusGridLookUpEdit.Size = new System.Drawing.Size(427, 22);
            this.StatusGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusGridLookUpEdit.TabIndex = 35;
            this.StatusGridLookUpEdit.TabStop = false;
            this.StatusGridLookUpEdit.Tag = "153";
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHeaderDescription,
            this.colHeaderID,
            this.colItemCode,
            this.colItemDescription,
            this.colItemID,
            this.colOrder1});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.colItemID;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsCustomization.AllowFilter = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowFilterEditor = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Picklist Type";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Width = 189;
            // 
            // colHeaderID
            // 
            this.colHeaderID.Caption = "Picklist ID";
            this.colHeaderID.FieldName = "HeaderID";
            this.colHeaderID.Name = "colHeaderID";
            this.colHeaderID.OptionsColumn.AllowEdit = false;
            this.colHeaderID.OptionsColumn.AllowFocus = false;
            this.colHeaderID.OptionsColumn.ReadOnly = true;
            this.colHeaderID.Width = 67;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item ID";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.OptionsColumn.AllowFocus = false;
            this.colItemCode.OptionsColumn.ReadOnly = true;
            this.colItemCode.Width = 58;
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Description";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 264;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.AllowFocus = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            this.colOrder1.Width = 65;
            // 
            // intPriorityIDGridLookUpEdit
            // 
            this.intPriorityIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intPriorityID", true));
            this.intPriorityIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intPriorityIDGridLookUpEdit.Location = new System.Drawing.Point(649, 120);
            this.intPriorityIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intPriorityIDGridLookUpEdit.Name = "intPriorityIDGridLookUpEdit";
            editorButtonImageOptions24.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions25.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intPriorityIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject93, serializableAppearanceObject94, serializableAppearanceObject95, serializableAppearanceObject96, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions25, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject97, serializableAppearanceObject98, serializableAppearanceObject99, serializableAppearanceObject100, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intPriorityIDGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intPriorityIDGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intPriorityIDGridLookUpEdit.Properties.NullText = "";
            this.intPriorityIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.intPriorityIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intPriorityIDGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intPriorityIDGridLookUpEdit.Size = new System.Drawing.Size(470, 22);
            this.intPriorityIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intPriorityIDGridLookUpEdit.TabIndex = 15;
            this.intPriorityIDGridLookUpEdit.TabStop = false;
            this.intPriorityIDGridLookUpEdit.Tag = "126";
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.gridColumn65;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn66, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Picklist Type";
            this.gridColumn61.FieldName = "HeaderDescription";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.Width = 189;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Picklist ID";
            this.gridColumn62.FieldName = "HeaderID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.Width = 67;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Item ID";
            this.gridColumn63.FieldName = "ItemCode";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Width = 58;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Description";
            this.gridColumn64.FieldName = "ItemDescription";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 0;
            this.gridColumn64.Width = 264;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Order";
            this.gridColumn66.FieldName = "Order";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Width = 65;
            // 
            // intSupervisor1GridLookUpEdit
            // 
            this.intSupervisor1GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intSupervisor1", true));
            this.intSupervisor1GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSupervisor1GridLookUpEdit.Location = new System.Drawing.Point(145, 250);
            this.intSupervisor1GridLookUpEdit.MenuManager = this.barManager1;
            this.intSupervisor1GridLookUpEdit.Name = "intSupervisor1GridLookUpEdit";
            editorButtonImageOptions26.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions27.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intSupervisor1GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions26, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject101, serializableAppearanceObject102, serializableAppearanceObject103, serializableAppearanceObject104, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions27, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject105, serializableAppearanceObject106, serializableAppearanceObject107, serializableAppearanceObject108, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intSupervisor1GridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.intSupervisor1GridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.intSupervisor1GridLookUpEdit.Properties.NullText = "";
            this.intSupervisor1GridLookUpEdit.Properties.PopupView = this.gridView3;
            this.intSupervisor1GridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.intSupervisor1GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intSupervisor1GridLookUpEdit.Properties.ValueMember = "StaffID";
            this.intSupervisor1GridLookUpEdit.Size = new System.Drawing.Size(950, 22);
            this.intSupervisor1GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intSupervisor1GridLookUpEdit.TabIndex = 16;
            this.intSupervisor1GridLookUpEdit.TabStop = false;
            this.intSupervisor1GridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intSupervisor1GridLookUpEdit_ButtonClick);
            // 
            // sp00226StaffListWithBlankBindingSource
            // 
            this.sp00226StaffListWithBlankBindingSource.DataMember = "sp00226_Staff_List_With_Blank";
            this.sp00226StaffListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActive,
            this.colDisplayName,
            this.colEmail,
            this.colForename,
            this.colNetworkID,
            this.colStaffID,
            this.colStaffName,
            this.colSurname,
            this.colUserType});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition7.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition7.Appearance.Options.UseForeColor = true;
            styleFormatCondition7.ApplyToRow = true;
            styleFormatCondition7.Column = this.colStaffID;
            styleFormatCondition7.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition7.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition7});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDisplayName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 51;
            // 
            // colDisplayName
            // 
            this.colDisplayName.Caption = "Surname: Forename";
            this.colDisplayName.FieldName = "DisplayName";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.OptionsColumn.AllowEdit = false;
            this.colDisplayName.OptionsColumn.AllowFocus = false;
            this.colDisplayName.OptionsColumn.ReadOnly = true;
            this.colDisplayName.Visible = true;
            this.colDisplayName.VisibleIndex = 0;
            this.colDisplayName.Width = 225;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowEdit = false;
            this.colEmail.OptionsColumn.AllowFocus = false;
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Width = 192;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Width = 178;
            // 
            // colNetworkID
            // 
            this.colNetworkID.Caption = "Network ID";
            this.colNetworkID.FieldName = "NetworkID";
            this.colNetworkID.Name = "colNetworkID";
            this.colNetworkID.OptionsColumn.AllowEdit = false;
            this.colNetworkID.OptionsColumn.AllowFocus = false;
            this.colNetworkID.OptionsColumn.ReadOnly = true;
            this.colNetworkID.Width = 180;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Width = 231;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Width = 191;
            // 
            // colUserType
            // 
            this.colUserType.Caption = "User Type";
            this.colUserType.FieldName = "UserType";
            this.colUserType.Name = "colUserType";
            this.colUserType.OptionsColumn.AllowEdit = false;
            this.colUserType.OptionsColumn.AllowFocus = false;
            this.colUserType.OptionsColumn.ReadOnly = true;
            this.colUserType.Width = 223;
            // 
            // intSupervisor2GridLookUpEdit
            // 
            this.intSupervisor2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intSupervisor2", true));
            this.intSupervisor2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSupervisor2GridLookUpEdit.Location = new System.Drawing.Point(145, 276);
            this.intSupervisor2GridLookUpEdit.MenuManager = this.barManager1;
            this.intSupervisor2GridLookUpEdit.Name = "intSupervisor2GridLookUpEdit";
            editorButtonImageOptions28.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions29.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intSupervisor2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions28, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject109, serializableAppearanceObject110, serializableAppearanceObject111, serializableAppearanceObject112, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions29, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject113, serializableAppearanceObject114, serializableAppearanceObject115, serializableAppearanceObject116, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intSupervisor2GridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.intSupervisor2GridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.intSupervisor2GridLookUpEdit.Properties.NullText = "";
            this.intSupervisor2GridLookUpEdit.Properties.PopupView = this.gridView4;
            this.intSupervisor2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intSupervisor2GridLookUpEdit.Properties.ValueMember = "StaffID";
            this.intSupervisor2GridLookUpEdit.Size = new System.Drawing.Size(950, 22);
            this.intSupervisor2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intSupervisor2GridLookUpEdit.TabIndex = 17;
            this.intSupervisor2GridLookUpEdit.TabStop = false;
            this.intSupervisor2GridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intSupervisor2GridLookUpEdit_ButtonClick);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition8.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition8.Appearance.Options.UseForeColor = true;
            styleFormatCondition8.ApplyToRow = true;
            styleFormatCondition8.Column = this.gridColumn30;
            styleFormatCondition8.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition8.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition8});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn26, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Active";
            this.gridColumn25.FieldName = "Active";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 1;
            this.gridColumn25.Width = 51;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Surname: Forename";
            this.gridColumn26.FieldName = "DisplayName";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 0;
            this.gridColumn26.Width = 225;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Email";
            this.gridColumn27.FieldName = "Email";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Width = 192;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Forename";
            this.gridColumn28.FieldName = "Forename";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Width = 178;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Network ID";
            this.gridColumn29.FieldName = "NetworkID";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 180;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Staff Name";
            this.gridColumn31.FieldName = "StaffName";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Width = 231;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Surname";
            this.gridColumn32.FieldName = "Surname";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 191;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "User Type";
            this.gridColumn33.FieldName = "UserType";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 223;
            // 
            // intSupervisor3GridLookUpEdit
            // 
            this.intSupervisor3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intSupervisor3", true));
            this.intSupervisor3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSupervisor3GridLookUpEdit.Location = new System.Drawing.Point(145, 302);
            this.intSupervisor3GridLookUpEdit.MenuManager = this.barManager1;
            this.intSupervisor3GridLookUpEdit.Name = "intSupervisor3GridLookUpEdit";
            editorButtonImageOptions30.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions31.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intSupervisor3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions30, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject117, serializableAppearanceObject118, serializableAppearanceObject119, serializableAppearanceObject120, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions31, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject121, serializableAppearanceObject122, serializableAppearanceObject123, serializableAppearanceObject124, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intSupervisor3GridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.intSupervisor3GridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.intSupervisor3GridLookUpEdit.Properties.NullText = "";
            this.intSupervisor3GridLookUpEdit.Properties.PopupView = this.gridView5;
            this.intSupervisor3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intSupervisor3GridLookUpEdit.Properties.ValueMember = "StaffID";
            this.intSupervisor3GridLookUpEdit.Size = new System.Drawing.Size(950, 22);
            this.intSupervisor3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intSupervisor3GridLookUpEdit.TabIndex = 18;
            this.intSupervisor3GridLookUpEdit.TabStop = false;
            this.intSupervisor3GridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intSupervisor3GridLookUpEdit_ButtonClick);
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition9.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition9.Appearance.Options.UseForeColor = true;
            styleFormatCondition9.ApplyToRow = true;
            styleFormatCondition9.Column = this.gridColumn39;
            styleFormatCondition9.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition9.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition9});
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn35, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Active";
            this.gridColumn34.FieldName = "Active";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 1;
            this.gridColumn34.Width = 51;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Surname: Forename";
            this.gridColumn35.FieldName = "DisplayName";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 0;
            this.gridColumn35.Width = 225;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Email";
            this.gridColumn36.FieldName = "Email";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 192;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Forename";
            this.gridColumn37.FieldName = "Forename";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 178;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Network ID";
            this.gridColumn38.FieldName = "NetworkID";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 180;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Staff Name";
            this.gridColumn40.FieldName = "StaffName";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Width = 231;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Surname";
            this.gridColumn41.FieldName = "Surname";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 191;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "User Type";
            this.gridColumn42.FieldName = "UserType";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Width = 223;
            // 
            // intCertifiedByIDGridLookUpEdit
            // 
            this.intCertifiedByIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intCertifiedByID", true));
            this.intCertifiedByIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCertifiedByIDGridLookUpEdit.Location = new System.Drawing.Point(145, 354);
            this.intCertifiedByIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intCertifiedByIDGridLookUpEdit.Name = "intCertifiedByIDGridLookUpEdit";
            editorButtonImageOptions32.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions33.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCertifiedByIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions32, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject125, serializableAppearanceObject126, serializableAppearanceObject127, serializableAppearanceObject128, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions33, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject129, serializableAppearanceObject130, serializableAppearanceObject131, serializableAppearanceObject132, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCertifiedByIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.intCertifiedByIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.intCertifiedByIDGridLookUpEdit.Properties.NullText = "";
            this.intCertifiedByIDGridLookUpEdit.Properties.PopupView = this.gridView6;
            this.intCertifiedByIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCertifiedByIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.intCertifiedByIDGridLookUpEdit.Size = new System.Drawing.Size(950, 22);
            this.intCertifiedByIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCertifiedByIDGridLookUpEdit.TabIndex = 25;
            this.intCertifiedByIDGridLookUpEdit.TabStop = false;
            this.intCertifiedByIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intCertifiedByIDGridLookUpEdit_ButtonClick);
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition10.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition10.Appearance.Options.UseForeColor = true;
            styleFormatCondition10.ApplyToRow = true;
            styleFormatCondition10.Column = this.gridColumn48;
            styleFormatCondition10.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition10.Value1 = 0;
            this.gridView6.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition10});
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn44, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Active";
            this.gridColumn43.FieldName = "Active";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 0;
            this.gridColumn43.Width = 51;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Surname: Forename";
            this.gridColumn44.FieldName = "DisplayName";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 1;
            this.gridColumn44.Width = 225;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Email";
            this.gridColumn45.FieldName = "Email";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Width = 192;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Forename";
            this.gridColumn46.FieldName = "Forename";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Width = 178;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Network ID";
            this.gridColumn47.FieldName = "NetworkID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Width = 180;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Staff Name";
            this.gridColumn49.FieldName = "StaffName";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Width = 231;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Surname";
            this.gridColumn50.FieldName = "Surname";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Width = 191;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "User Type";
            this.gridColumn51.FieldName = "UserType";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Width = 223;
            // 
            // intContractorIDGridLookUpEdit
            // 
            this.intContractorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intContractorID", true));
            this.intContractorIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intContractorIDGridLookUpEdit.Location = new System.Drawing.Point(145, 250);
            this.intContractorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intContractorIDGridLookUpEdit.Name = "intContractorIDGridLookUpEdit";
            editorButtonImageOptions34.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions35.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intContractorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions34, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject133, serializableAppearanceObject134, serializableAppearanceObject135, serializableAppearanceObject136, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions35, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject137, serializableAppearanceObject138, serializableAppearanceObject139, serializableAppearanceObject140, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intContractorIDGridLookUpEdit.Properties.DataSource = this.sp00190ContractorListWithBlankBindingSource;
            this.intContractorIDGridLookUpEdit.Properties.DisplayMember = "ContractorName";
            this.intContractorIDGridLookUpEdit.Properties.NullText = "";
            this.intContractorIDGridLookUpEdit.Properties.PopupView = this.gridView7;
            this.intContractorIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4});
            this.intContractorIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intContractorIDGridLookUpEdit.Properties.ValueMember = "ContractorID";
            this.intContractorIDGridLookUpEdit.Size = new System.Drawing.Size(950, 22);
            this.intContractorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intContractorIDGridLookUpEdit.TabIndex = 6;
            this.intContractorIDGridLookUpEdit.TabStop = false;
            this.intContractorIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intContractorIDGridLookUpEdit_ButtonClick);
            this.intContractorIDGridLookUpEdit.Enter += new System.EventHandler(this.intContractorIDGridLookUpEdit_Enter);
            this.intContractorIDGridLookUpEdit.Leave += new System.EventHandler(this.intContractorIDGridLookUpEdit_Leave);
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition11.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition11.Appearance.Options.UseForeColor = true;
            styleFormatCondition11.ApplyToRow = true;
            styleFormatCondition11.Column = this.gridColumn7;
            styleFormatCondition11.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition11.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition11});
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Address Line 1";
            this.gridColumn1.FieldName = "AddressLine1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 144;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Address Line 2";
            this.gridColumn2.FieldName = "AddressLine2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 91;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Address Line 3";
            this.gridColumn3.FieldName = "AddressLine3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 91;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Address Line 4";
            this.gridColumn4.FieldName = "AddressLine4";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 91;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Address Line 5";
            this.gridColumn5.FieldName = "AddressLine5";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 91;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Contractor Code";
            this.gridColumn6.FieldName = "ContractorCode";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 101;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Contractor Name";
            this.gridColumn8.FieldName = "ContractorName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 273;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Disabled";
            this.gridColumn9.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn9.FieldName = "Disabled";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            this.gridColumn9.Width = 61;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Email Password";
            this.gridColumn10.FieldName = "EmailPassword";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 94;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Internal Contractor ID";
            this.gridColumn11.FieldName = "InternalContractor";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 118;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Status";
            this.gridColumn12.FieldName = "InternalCOntractorDescription";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 52;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Mobile";
            this.gridColumn13.FieldName = "Mobile";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 51;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Postcode";
            this.gridColumn14.FieldName = "Postcode";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 65;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Remarks";
            this.gridColumn15.FieldName = "Remarks";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 62;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Telephone 1";
            this.gridColumn16.FieldName = "Telephone1";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 80;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Telephone 2";
            this.gridColumn17.FieldName = "Telephone2";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 80;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Type";
            this.gridColumn18.FieldName = "TypeDescription";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            this.gridColumn18.Width = 111;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Type ID";
            this.gridColumn19.FieldName = "TypeID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 49;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "User Defined 1";
            this.gridColumn20.FieldName = "UserDefined1";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 92;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "User Defined 2";
            this.gridColumn21.FieldName = "UserDefined2";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 92;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "User Defined 3";
            this.gridColumn22.FieldName = "UserDefined3";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 92;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "VAT Reg";
            this.gridColumn23.FieldName = "VatReg";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 62;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Website";
            this.gridColumn24.FieldName = "Website";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 60;
            // 
            // intIssuedByPersonIDGridLookUpEdit
            // 
            this.intIssuedByPersonIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intIssuedByPersonID", true));
            this.intIssuedByPersonIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intIssuedByPersonIDGridLookUpEdit.Location = new System.Drawing.Point(145, 276);
            this.intIssuedByPersonIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intIssuedByPersonIDGridLookUpEdit.Name = "intIssuedByPersonIDGridLookUpEdit";
            editorButtonImageOptions36.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions37.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intIssuedByPersonIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions36, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject141, serializableAppearanceObject142, serializableAppearanceObject143, serializableAppearanceObject144, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions37, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject145, serializableAppearanceObject146, serializableAppearanceObject147, serializableAppearanceObject148, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intIssuedByPersonIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.intIssuedByPersonIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.intIssuedByPersonIDGridLookUpEdit.Properties.NullText = "";
            this.intIssuedByPersonIDGridLookUpEdit.Properties.PopupView = this.gridView8;
            this.intIssuedByPersonIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intIssuedByPersonIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.intIssuedByPersonIDGridLookUpEdit.Size = new System.Drawing.Size(950, 22);
            this.intIssuedByPersonIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intIssuedByPersonIDGridLookUpEdit.TabIndex = 7;
            this.intIssuedByPersonIDGridLookUpEdit.TabStop = false;
            this.intIssuedByPersonIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intIssuedByPersonIDGridLookUpEdit_ButtonClick);
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition12.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition12.Appearance.Options.UseForeColor = true;
            styleFormatCondition12.ApplyToRow = true;
            styleFormatCondition12.Column = this.gridColumn57;
            styleFormatCondition12.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition12.Value1 = 0;
            this.gridView8.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition12});
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn53, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Active";
            this.gridColumn52.FieldName = "Active";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 1;
            this.gridColumn52.Width = 51;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Surname: Forename";
            this.gridColumn53.FieldName = "DisplayName";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 0;
            this.gridColumn53.Width = 225;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Email";
            this.gridColumn54.FieldName = "Email";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 192;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Forename";
            this.gridColumn55.FieldName = "Forename";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Width = 178;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Network ID";
            this.gridColumn56.FieldName = "NetworkID";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Width = 180;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Staff Name";
            this.gridColumn58.FieldName = "StaffName";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Width = 231;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Surname";
            this.gridColumn59.FieldName = "Surname";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Width = 191;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "User Type";
            this.gridColumn60.FieldName = "UserType";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Width = 223;
            // 
            // intIssuedByBodyIDGridLookUpEdit
            // 
            this.intIssuedByBodyIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intIssuedByBodyID", true));
            this.intIssuedByBodyIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intIssuedByBodyIDGridLookUpEdit.Location = new System.Drawing.Point(145, 302);
            this.intIssuedByBodyIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intIssuedByBodyIDGridLookUpEdit.Name = "intIssuedByBodyIDGridLookUpEdit";
            editorButtonImageOptions38.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions39.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intIssuedByBodyIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions38, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject149, serializableAppearanceObject150, serializableAppearanceObject151, serializableAppearanceObject152, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions39, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject153, serializableAppearanceObject154, serializableAppearanceObject155, serializableAppearanceObject156, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intIssuedByBodyIDGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intIssuedByBodyIDGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intIssuedByBodyIDGridLookUpEdit.Properties.NullText = "";
            this.intIssuedByBodyIDGridLookUpEdit.Properties.PopupView = this.gridView9;
            this.intIssuedByBodyIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intIssuedByBodyIDGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intIssuedByBodyIDGridLookUpEdit.Size = new System.Drawing.Size(950, 22);
            this.intIssuedByBodyIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intIssuedByBodyIDGridLookUpEdit.TabIndex = 8;
            this.intIssuedByBodyIDGridLookUpEdit.TabStop = false;
            this.intIssuedByBodyIDGridLookUpEdit.Tag = "127";
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72});
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition13.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition13.Appearance.Options.UseForeColor = true;
            styleFormatCondition13.ApplyToRow = true;
            styleFormatCondition13.Column = this.gridColumn71;
            styleFormatCondition13.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition13.Value1 = 0;
            this.gridView9.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition13});
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView9.OptionsCustomization.AllowFilter = false;
            this.gridView9.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowFilterEditor = false;
            this.gridView9.OptionsFilter.AllowMRUFilterList = false;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9.OptionsView.ShowIndicator = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn72, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Picklist Type";
            this.gridColumn67.FieldName = "HeaderDescription";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Width = 189;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Picklist ID";
            this.gridColumn68.FieldName = "HeaderID";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            this.gridColumn68.Width = 67;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Item ID";
            this.gridColumn69.FieldName = "ItemCode";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Width = 58;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Description";
            this.gridColumn70.FieldName = "ItemDescription";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Visible = true;
            this.gridColumn70.VisibleIndex = 0;
            this.gridColumn70.Width = 264;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Order";
            this.gridColumn72.FieldName = "Order";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Width = 65;
            // 
            // intCompanyHeaderIDGridLookUpEdit
            // 
            this.intCompanyHeaderIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intCompanyHeaderID", true));
            this.intCompanyHeaderIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCompanyHeaderIDGridLookUpEdit.Location = new System.Drawing.Point(145, 328);
            this.intCompanyHeaderIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intCompanyHeaderIDGridLookUpEdit.Name = "intCompanyHeaderIDGridLookUpEdit";
            editorButtonImageOptions40.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions41.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCompanyHeaderIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions40, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject157, serializableAppearanceObject158, serializableAppearanceObject159, serializableAppearanceObject160, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions41, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject161, serializableAppearanceObject162, serializableAppearanceObject163, serializableAppearanceObject164, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCompanyHeaderIDGridLookUpEdit.Properties.DataSource = this.sp00232CompanyHeaderDropDownListWithBlankBindingSource;
            this.intCompanyHeaderIDGridLookUpEdit.Properties.DisplayMember = "strDescription";
            this.intCompanyHeaderIDGridLookUpEdit.Properties.NullText = "";
            this.intCompanyHeaderIDGridLookUpEdit.Properties.PopupView = this.gridView11;
            this.intCompanyHeaderIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCompanyHeaderIDGridLookUpEdit.Properties.ValueMember = "intHeaderID";
            this.intCompanyHeaderIDGridLookUpEdit.Size = new System.Drawing.Size(950, 22);
            this.intCompanyHeaderIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCompanyHeaderIDGridLookUpEdit.TabIndex = 28;
            this.intCompanyHeaderIDGridLookUpEdit.TabStop = false;
            this.intCompanyHeaderIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intCompanyHeaderIDGridLookUpEdit_ButtonClick);
            // 
            // sp00232CompanyHeaderDropDownListWithBlankBindingSource
            // 
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource.DataMember = "sp00232_Company_Header_Drop_Down_List_With_Blank";
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colintHeaderID,
            this.colstrAddressLine1,
            this.colstrCompanyName,
            this.colstrDescription,
            this.colstrTelephone1,
            this.colstrSlogan});
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition14.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition14.Appearance.Options.UseForeColor = true;
            styleFormatCondition14.ApplyToRow = true;
            styleFormatCondition14.Column = this.colintHeaderID;
            styleFormatCondition14.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition14.Value1 = 0;
            this.gridView11.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition14});
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView11.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView11.OptionsView.ShowIndicator = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrCompanyName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colstrAddressLine1
            // 
            this.colstrAddressLine1.Caption = "Address Line 1";
            this.colstrAddressLine1.FieldName = "strAddressLine1";
            this.colstrAddressLine1.Name = "colstrAddressLine1";
            this.colstrAddressLine1.OptionsColumn.AllowEdit = false;
            this.colstrAddressLine1.OptionsColumn.AllowFocus = false;
            this.colstrAddressLine1.OptionsColumn.ReadOnly = true;
            this.colstrAddressLine1.Visible = true;
            this.colstrAddressLine1.VisibleIndex = 2;
            this.colstrAddressLine1.Width = 133;
            // 
            // colstrCompanyName
            // 
            this.colstrCompanyName.Caption = "Company Name";
            this.colstrCompanyName.FieldName = "strCompanyName";
            this.colstrCompanyName.Name = "colstrCompanyName";
            this.colstrCompanyName.OptionsColumn.AllowEdit = false;
            this.colstrCompanyName.OptionsColumn.AllowFocus = false;
            this.colstrCompanyName.OptionsColumn.ReadOnly = true;
            this.colstrCompanyName.Visible = true;
            this.colstrCompanyName.VisibleIndex = 1;
            this.colstrCompanyName.Width = 211;
            // 
            // colstrDescription
            // 
            this.colstrDescription.Caption = "Header Description";
            this.colstrDescription.FieldName = "strDescription";
            this.colstrDescription.Name = "colstrDescription";
            this.colstrDescription.OptionsColumn.AllowEdit = false;
            this.colstrDescription.OptionsColumn.AllowFocus = false;
            this.colstrDescription.OptionsColumn.ReadOnly = true;
            this.colstrDescription.Visible = true;
            this.colstrDescription.VisibleIndex = 0;
            this.colstrDescription.Width = 255;
            // 
            // colstrTelephone1
            // 
            this.colstrTelephone1.Caption = "Telephone 1";
            this.colstrTelephone1.FieldName = "strTelephone1";
            this.colstrTelephone1.Name = "colstrTelephone1";
            this.colstrTelephone1.OptionsColumn.AllowEdit = false;
            this.colstrTelephone1.OptionsColumn.AllowFocus = false;
            this.colstrTelephone1.OptionsColumn.ReadOnly = true;
            this.colstrTelephone1.Visible = true;
            this.colstrTelephone1.VisibleIndex = 3;
            this.colstrTelephone1.Width = 108;
            // 
            // colstrSlogan
            // 
            this.colstrSlogan.Caption = "Slogan";
            this.colstrSlogan.FieldName = "strSlogan";
            this.colstrSlogan.Name = "colstrSlogan";
            this.colstrSlogan.OptionsColumn.AllowEdit = false;
            this.colstrSlogan.OptionsColumn.AllowFocus = false;
            this.colstrSlogan.OptionsColumn.ReadOnly = true;
            this.colstrSlogan.Width = 53;
            // 
            // intRecordTypeGridLookUpEdit
            // 
            this.intRecordTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "intRecordType", true));
            this.intRecordTypeGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intRecordTypeGridLookUpEdit.Location = new System.Drawing.Point(603, 12);
            this.intRecordTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.intRecordTypeGridLookUpEdit.Name = "intRecordTypeGridLookUpEdit";
            this.intRecordTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.intRecordTypeGridLookUpEdit.Properties.DataSource = this.sp01403ATWorkOrderTypesBindingSource;
            this.intRecordTypeGridLookUpEdit.Properties.DisplayMember = "strDescription";
            this.intRecordTypeGridLookUpEdit.Properties.NullText = "";
            this.intRecordTypeGridLookUpEdit.Properties.PopupView = this.gridView12;
            this.intRecordTypeGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intRecordTypeGridLookUpEdit.Properties.ValueMember = "intTypeID";
            this.intRecordTypeGridLookUpEdit.Size = new System.Drawing.Size(528, 20);
            this.intRecordTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intRecordTypeGridLookUpEdit.TabIndex = 30;
            this.intRecordTypeGridLookUpEdit.TabStop = false;
            // 
            // sp01403ATWorkOrderTypesBindingSource
            // 
            this.sp01403ATWorkOrderTypesBindingSource.DataMember = "sp01403_AT_Work_Order_Types";
            this.sp01403ATWorkOrderTypesBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colintTypeID,
            this.colstrDescription1,
            this.colintOrder});
            this.gridView12.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView12.OptionsView.ShowIndicator = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colintOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colintTypeID
            // 
            this.colintTypeID.Caption = "Type ID";
            this.colintTypeID.FieldName = "intTypeID";
            this.colintTypeID.Name = "colintTypeID";
            this.colintTypeID.OptionsColumn.AllowEdit = false;
            this.colintTypeID.OptionsColumn.AllowFocus = false;
            this.colintTypeID.OptionsColumn.ReadOnly = true;
            this.colintTypeID.Width = 59;
            // 
            // colstrDescription1
            // 
            this.colstrDescription1.Caption = "Work Order Type";
            this.colstrDescription1.FieldName = "strDescription";
            this.colstrDescription1.Name = "colstrDescription1";
            this.colstrDescription1.OptionsColumn.AllowEdit = false;
            this.colstrDescription1.OptionsColumn.AllowFocus = false;
            this.colstrDescription1.OptionsColumn.ReadOnly = true;
            this.colstrDescription1.Visible = true;
            this.colstrDescription1.VisibleIndex = 0;
            this.colstrDescription1.Width = 203;
            // 
            // colintOrder
            // 
            this.colintOrder.Caption = "Order";
            this.colintOrder.FieldName = "intOrder";
            this.colintOrder.Name = "colintOrder";
            this.colintOrder.OptionsColumn.AllowEdit = false;
            this.colintOrder.OptionsColumn.AllowFocus = false;
            this.colintOrder.OptionsColumn.ReadOnly = true;
            this.colintOrder.Width = 73;
            // 
            // UserPicklist1GridLookUpEdit
            // 
            this.UserPicklist1GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "UserPicklist1", true));
            this.UserPicklist1GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPicklist1GridLookUpEdit.Location = new System.Drawing.Point(662, 284);
            this.UserPicklist1GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPicklist1GridLookUpEdit.Name = "UserPicklist1GridLookUpEdit";
            editorButtonImageOptions42.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions43.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPicklist1GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions42, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject165, serializableAppearanceObject166, serializableAppearanceObject167, serializableAppearanceObject168, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions43, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject169, serializableAppearanceObject170, serializableAppearanceObject171, serializableAppearanceObject172, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPicklist1GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPicklist1GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPicklist1GridLookUpEdit.Properties.NullText = "";
            this.UserPicklist1GridLookUpEdit.Properties.PopupView = this.gridView13;
            this.UserPicklist1GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPicklist1GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPicklist1GridLookUpEdit.Size = new System.Drawing.Size(421, 22);
            this.UserPicklist1GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPicklist1GridLookUpEdit.TabIndex = 32;
            this.UserPicklist1GridLookUpEdit.TabStop = false;
            this.UserPicklist1GridLookUpEdit.Tag = "150";
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.gridColumn77,
            this.gridColumn78});
            this.gridView13.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition15.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition15.Appearance.Options.UseForeColor = true;
            styleFormatCondition15.ApplyToRow = true;
            styleFormatCondition15.Column = this.gridColumn77;
            styleFormatCondition15.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition15.Value1 = 0;
            this.gridView13.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition15});
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView13.OptionsCustomization.AllowFilter = false;
            this.gridView13.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView13.OptionsFilter.AllowFilterEditor = false;
            this.gridView13.OptionsFilter.AllowMRUFilterList = false;
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView13.OptionsView.ShowIndicator = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn78, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Picklist Type";
            this.gridColumn73.FieldName = "HeaderDescription";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Width = 189;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Picklist ID";
            this.gridColumn74.FieldName = "HeaderID";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Width = 67;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Item ID";
            this.gridColumn75.FieldName = "ItemCode";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Width = 58;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Description";
            this.gridColumn76.FieldName = "ItemDescription";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 0;
            this.gridColumn76.Width = 264;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Order";
            this.gridColumn78.FieldName = "Order";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.Width = 65;
            // 
            // UserPicklist2GridLookUpEdit
            // 
            this.UserPicklist2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "UserPicklist2", true));
            this.UserPicklist2GridLookUpEdit.EditValue = "151";
            this.UserPicklist2GridLookUpEdit.Location = new System.Drawing.Point(662, 310);
            this.UserPicklist2GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPicklist2GridLookUpEdit.Name = "UserPicklist2GridLookUpEdit";
            editorButtonImageOptions44.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions45.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPicklist2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions44, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject173, serializableAppearanceObject174, serializableAppearanceObject175, serializableAppearanceObject176, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions45, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject177, serializableAppearanceObject178, serializableAppearanceObject179, serializableAppearanceObject180, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPicklist2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPicklist2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPicklist2GridLookUpEdit.Properties.NullText = "";
            this.UserPicklist2GridLookUpEdit.Properties.PopupView = this.gridView14;
            this.UserPicklist2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPicklist2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPicklist2GridLookUpEdit.Size = new System.Drawing.Size(421, 22);
            this.UserPicklist2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPicklist2GridLookUpEdit.TabIndex = 33;
            this.UserPicklist2GridLookUpEdit.TabStop = false;
            this.UserPicklist2GridLookUpEdit.Tag = "151";
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn83,
            this.gridColumn84});
            this.gridView14.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition16.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition16.Appearance.Options.UseForeColor = true;
            styleFormatCondition16.ApplyToRow = true;
            styleFormatCondition16.Column = this.gridColumn83;
            styleFormatCondition16.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition16.Value1 = 0;
            this.gridView14.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition16});
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView14.OptionsCustomization.AllowFilter = false;
            this.gridView14.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView14.OptionsFilter.AllowFilterEditor = false;
            this.gridView14.OptionsFilter.AllowMRUFilterList = false;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView14.OptionsView.ShowIndicator = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn84, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Picklist Type";
            this.gridColumn79.FieldName = "HeaderDescription";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.Width = 189;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Picklist ID";
            this.gridColumn80.FieldName = "HeaderID";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Width = 67;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Item ID";
            this.gridColumn81.FieldName = "ItemCode";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Width = 58;
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Description";
            this.gridColumn82.FieldName = "ItemDescription";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.Visible = true;
            this.gridColumn82.VisibleIndex = 0;
            this.gridColumn82.Width = 264;
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Order";
            this.gridColumn84.FieldName = "Order";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Width = 65;
            // 
            // UserPicklist3GridLookUpEdit
            // 
            this.UserPicklist3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01399ATWorkOrderEditBindingSource, "UserPicklist3", true));
            this.UserPicklist3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPicklist3GridLookUpEdit.Location = new System.Drawing.Point(662, 336);
            this.UserPicklist3GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPicklist3GridLookUpEdit.Name = "UserPicklist3GridLookUpEdit";
            editorButtonImageOptions46.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions47.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPicklist3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions46, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject181, serializableAppearanceObject182, serializableAppearanceObject183, serializableAppearanceObject184, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions47, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject185, serializableAppearanceObject186, serializableAppearanceObject187, serializableAppearanceObject188, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPicklist3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPicklist3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPicklist3GridLookUpEdit.Properties.NullText = "";
            this.UserPicklist3GridLookUpEdit.Properties.PopupView = this.gridView15;
            this.UserPicklist3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPicklist3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPicklist3GridLookUpEdit.Size = new System.Drawing.Size(421, 22);
            this.UserPicklist3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPicklist3GridLookUpEdit.TabIndex = 34;
            this.UserPicklist3GridLookUpEdit.TabStop = false;
            this.UserPicklist3GridLookUpEdit.Tag = "152";
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87,
            this.gridColumn88,
            this.gridColumn89,
            this.gridColumn90});
            this.gridView15.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition17.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition17.Appearance.Options.UseForeColor = true;
            styleFormatCondition17.ApplyToRow = true;
            styleFormatCondition17.Column = this.gridColumn89;
            styleFormatCondition17.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition17.Value1 = 0;
            this.gridView15.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition17});
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView15.OptionsCustomization.AllowFilter = false;
            this.gridView15.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView15.OptionsFilter.AllowFilterEditor = false;
            this.gridView15.OptionsFilter.AllowMRUFilterList = false;
            this.gridView15.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView15.OptionsLayout.StoreAppearance = true;
            this.gridView15.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView15.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView15.OptionsView.ColumnAutoWidth = false;
            this.gridView15.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView15.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            this.gridView15.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView15.OptionsView.ShowIndicator = false;
            this.gridView15.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn90, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Picklist Type";
            this.gridColumn85.FieldName = "HeaderDescription";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Width = 189;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Picklist ID";
            this.gridColumn86.FieldName = "HeaderID";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.Width = 67;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Item ID";
            this.gridColumn87.FieldName = "ItemCode";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Width = 58;
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Description";
            this.gridColumn88.FieldName = "ItemDescription";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Visible = true;
            this.gridColumn88.VisibleIndex = 0;
            this.gridColumn88.Width = 264;
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Order";
            this.gridColumn90.FieldName = "Order";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Width = 65;
            // 
            // ItemForstrEmailDetails
            // 
            this.ItemForstrEmailDetails.Control = this.strEmailDetailsMemoEdit;
            this.ItemForstrEmailDetails.CustomizationFormText = "Email Details:";
            this.ItemForstrEmailDetails.Location = new System.Drawing.Point(0, 72);
            this.ItemForstrEmailDetails.Name = "ItemForstrEmailDetails";
            this.ItemForstrEmailDetails.Size = new System.Drawing.Size(1106, 20);
            this.ItemForstrEmailDetails.Text = "Email Details:";
            this.ItemForstrEmailDetails.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintOperationID
            // 
            this.ItemForintOperationID.Control = this.intOperationIDSpinEdit;
            this.ItemForintOperationID.CustomizationFormText = "Operation ID:";
            this.ItemForintOperationID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintOperationID.Name = "ItemForintOperationID";
            this.ItemForintOperationID.Size = new System.Drawing.Size(1106, 24);
            this.ItemForintOperationID.Text = "Operation ID:";
            this.ItemForintOperationID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1143, 694);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup13,
            this.layoutControlItem2,
            this.emptySpaceItem3,
            this.ItemForintRecordType,
            this.simpleSeparator6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1123, 408);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Work Order Header";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintWorkOrderID,
            this.ItemForintPrevWorkOrderID,
            this.ItemForWorkOrderReference,
            this.ItemForstrInvoiceNumber,
            this.ItemForStatus,
            this.ItemForintPriorityID,
            this.layoutControlGroup15,
            this.emptySpaceItem2,
            this.ItemForWorkOrderPDFFile});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 26);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(1123, 382);
            this.layoutControlGroup13.Text = "Work Order Header";
            // 
            // ItemForintWorkOrderID
            // 
            this.ItemForintWorkOrderID.Control = this.intWorkOrderIDTextEdit;
            this.ItemForintWorkOrderID.CustomizationFormText = "Work Order ID:";
            this.ItemForintWorkOrderID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintWorkOrderID.Name = "ItemForintWorkOrderID";
            this.ItemForintWorkOrderID.Size = new System.Drawing.Size(528, 24);
            this.ItemForintWorkOrderID.Text = "Work Order ID:";
            this.ItemForintWorkOrderID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForintPrevWorkOrderID
            // 
            this.ItemForintPrevWorkOrderID.Control = this.intPrevWorkOrderIDButtonEdit;
            this.ItemForintPrevWorkOrderID.CustomizationFormText = "Linked To W\\O:";
            this.ItemForintPrevWorkOrderID.Location = new System.Drawing.Point(528, 0);
            this.ItemForintPrevWorkOrderID.Name = "ItemForintPrevWorkOrderID";
            this.ItemForintPrevWorkOrderID.Size = new System.Drawing.Size(571, 24);
            this.ItemForintPrevWorkOrderID.Text = "Linked To W\\O:";
            this.ItemForintPrevWorkOrderID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForWorkOrderReference
            // 
            this.ItemForWorkOrderReference.Control = this.WorkOrderReferenceButtonEdit;
            this.ItemForWorkOrderReference.CustomizationFormText = "Reference Number:";
            this.ItemForWorkOrderReference.Location = new System.Drawing.Point(0, 24);
            this.ItemForWorkOrderReference.Name = "ItemForWorkOrderReference";
            this.ItemForWorkOrderReference.Size = new System.Drawing.Size(528, 24);
            this.ItemForWorkOrderReference.Text = "Reference Number:";
            this.ItemForWorkOrderReference.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForstrInvoiceNumber
            // 
            this.ItemForstrInvoiceNumber.Control = this.strInvoiceNumberButtonEdit;
            this.ItemForstrInvoiceNumber.CustomizationFormText = "Contract Number:";
            this.ItemForstrInvoiceNumber.Location = new System.Drawing.Point(528, 24);
            this.ItemForstrInvoiceNumber.Name = "ItemForstrInvoiceNumber";
            this.ItemForstrInvoiceNumber.Size = new System.Drawing.Size(571, 24);
            this.ItemForstrInvoiceNumber.Text = "Contract Number:";
            this.ItemForstrInvoiceNumber.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForStatus
            // 
            this.ItemForStatus.Control = this.StatusGridLookUpEdit;
            this.ItemForStatus.CustomizationFormText = "Status";
            this.ItemForStatus.Location = new System.Drawing.Point(0, 48);
            this.ItemForStatus.Name = "ItemForStatus";
            this.ItemForStatus.Size = new System.Drawing.Size(528, 26);
            this.ItemForStatus.Text = "Status";
            this.ItemForStatus.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForintPriorityID
            // 
            this.ItemForintPriorityID.Control = this.intPriorityIDGridLookUpEdit;
            this.ItemForintPriorityID.CustomizationFormText = "Priority:";
            this.ItemForintPriorityID.Location = new System.Drawing.Point(528, 48);
            this.ItemForintPriorityID.Name = "ItemForintPriorityID";
            this.ItemForintPriorityID.Size = new System.Drawing.Size(571, 26);
            this.ItemForintPriorityID.Text = "Priority:";
            this.ItemForintPriorityID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.CustomizationFormText = "Details";
            this.layoutControlGroup15.ExpandButtonVisible = true;
            this.layoutControlGroup15.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 108);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(1099, 228);
            this.layoutControlGroup15.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup16;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1075, 182);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup16,
            this.layoutControlGroup14,
            this.layoutControlGroup11,
            this.layoutControlGroup9,
            this.layoutControlGroup4,
            this.layoutControlGroup10});
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.CustomizationFormText = "Details";
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintIssuedByPersonID,
            this.ItemForintIssuedByBodyID,
            this.ItemForintCompanyHeaderID,
            this.ItemForintContractorID,
            this.ItemForintCertifiedByID});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Size = new System.Drawing.Size(1051, 134);
            this.layoutControlGroup16.Text = "Details";
            // 
            // ItemForintIssuedByPersonID
            // 
            this.ItemForintIssuedByPersonID.Control = this.intIssuedByPersonIDGridLookUpEdit;
            this.ItemForintIssuedByPersonID.CustomizationFormText = "Issuing Person:";
            this.ItemForintIssuedByPersonID.Location = new System.Drawing.Point(0, 26);
            this.ItemForintIssuedByPersonID.Name = "ItemForintIssuedByPersonID";
            this.ItemForintIssuedByPersonID.Size = new System.Drawing.Size(1051, 26);
            this.ItemForintIssuedByPersonID.Text = "Issuing Person:";
            this.ItemForintIssuedByPersonID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForintIssuedByBodyID
            // 
            this.ItemForintIssuedByBodyID.Control = this.intIssuedByBodyIDGridLookUpEdit;
            this.ItemForintIssuedByBodyID.CustomizationFormText = "Issuing Body:";
            this.ItemForintIssuedByBodyID.Location = new System.Drawing.Point(0, 52);
            this.ItemForintIssuedByBodyID.Name = "ItemForintIssuedByBodyID";
            this.ItemForintIssuedByBodyID.Size = new System.Drawing.Size(1051, 26);
            this.ItemForintIssuedByBodyID.Text = "Issuing Body:";
            this.ItemForintIssuedByBodyID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForintCompanyHeaderID
            // 
            this.ItemForintCompanyHeaderID.Control = this.intCompanyHeaderIDGridLookUpEdit;
            this.ItemForintCompanyHeaderID.CustomizationFormText = "Company Header:";
            this.ItemForintCompanyHeaderID.Location = new System.Drawing.Point(0, 78);
            this.ItemForintCompanyHeaderID.Name = "ItemForintCompanyHeaderID";
            this.ItemForintCompanyHeaderID.Size = new System.Drawing.Size(1051, 26);
            this.ItemForintCompanyHeaderID.Text = "Company Header:";
            this.ItemForintCompanyHeaderID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForintContractorID
            // 
            this.ItemForintContractorID.Control = this.intContractorIDGridLookUpEdit;
            this.ItemForintContractorID.CustomizationFormText = "Contractor:";
            this.ItemForintContractorID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintContractorID.Name = "ItemForintContractorID";
            this.ItemForintContractorID.Size = new System.Drawing.Size(1051, 26);
            this.ItemForintContractorID.Text = "Contractor:";
            this.ItemForintContractorID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForintCertifiedByID
            // 
            this.ItemForintCertifiedByID.Control = this.intCertifiedByIDGridLookUpEdit;
            this.ItemForintCertifiedByID.CustomizationFormText = "Certified By:";
            this.ItemForintCertifiedByID.Location = new System.Drawing.Point(0, 104);
            this.ItemForintCertifiedByID.Name = "ItemForintCertifiedByID";
            this.ItemForintCertifiedByID.Size = new System.Drawing.Size(1051, 30);
            this.ItemForintCertifiedByID.Text = "Certified By:";
            this.ItemForintCertifiedByID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "Dates";
            this.layoutControlGroup14.ExpandButtonVisible = true;
            this.layoutControlGroup14.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTargetDate,
            this.emptySpaceItem4,
            this.ItemFordtIssueDate,
            this.ItemFordtCompleteDate,
            this.emptySpaceItem5});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(1051, 134);
            this.layoutControlGroup14.Text = "Dates";
            // 
            // ItemForTargetDate
            // 
            this.ItemForTargetDate.Control = this.TargetDateDateEdit;
            this.ItemForTargetDate.CustomizationFormText = "Target Date:";
            this.ItemForTargetDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForTargetDate.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForTargetDate.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForTargetDate.Name = "ItemForTargetDate";
            this.ItemForTargetDate.Size = new System.Drawing.Size(271, 24);
            this.ItemForTargetDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTargetDate.Text = "Target Date:";
            this.ItemForTargetDate.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(271, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(780, 72);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemFordtIssueDate
            // 
            this.ItemFordtIssueDate.Control = this.dtIssueDateDateEdit;
            this.ItemFordtIssueDate.CustomizationFormText = "Issue Date:";
            this.ItemFordtIssueDate.Location = new System.Drawing.Point(0, 24);
            this.ItemFordtIssueDate.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemFordtIssueDate.MinSize = new System.Drawing.Size(271, 24);
            this.ItemFordtIssueDate.Name = "ItemFordtIssueDate";
            this.ItemFordtIssueDate.Size = new System.Drawing.Size(271, 24);
            this.ItemFordtIssueDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtIssueDate.Text = "Issue Date:";
            this.ItemFordtIssueDate.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemFordtCompleteDate
            // 
            this.ItemFordtCompleteDate.Control = this.dtCompleteDateDateEdit;
            this.ItemFordtCompleteDate.CustomizationFormText = "Completed Date:";
            this.ItemFordtCompleteDate.Location = new System.Drawing.Point(0, 48);
            this.ItemFordtCompleteDate.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemFordtCompleteDate.MinSize = new System.Drawing.Size(271, 24);
            this.ItemFordtCompleteDate.Name = "ItemFordtCompleteDate";
            this.ItemFordtCompleteDate.Size = new System.Drawing.Size(271, 24);
            this.ItemFordtCompleteDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtCompleteDate.Text = "Completed Date:";
            this.ItemFordtCompleteDate.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1051, 62);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Supervisors";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintSupervisor1,
            this.ItemForintSupervisor2,
            this.ItemForintSupervisor3});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(1051, 134);
            this.layoutControlGroup11.Text = "Supervisors";
            // 
            // ItemForintSupervisor1
            // 
            this.ItemForintSupervisor1.Control = this.intSupervisor1GridLookUpEdit;
            this.ItemForintSupervisor1.CustomizationFormText = "Supervisor 1:";
            this.ItemForintSupervisor1.Location = new System.Drawing.Point(0, 0);
            this.ItemForintSupervisor1.Name = "ItemForintSupervisor1";
            this.ItemForintSupervisor1.Size = new System.Drawing.Size(1051, 26);
            this.ItemForintSupervisor1.Text = "Supervisor 1:";
            this.ItemForintSupervisor1.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForintSupervisor2
            // 
            this.ItemForintSupervisor2.Control = this.intSupervisor2GridLookUpEdit;
            this.ItemForintSupervisor2.CustomizationFormText = "Supervisor 2:";
            this.ItemForintSupervisor2.Location = new System.Drawing.Point(0, 26);
            this.ItemForintSupervisor2.Name = "ItemForintSupervisor2";
            this.ItemForintSupervisor2.Size = new System.Drawing.Size(1051, 26);
            this.ItemForintSupervisor2.Text = "Supervisor 2:";
            this.ItemForintSupervisor2.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForintSupervisor3
            // 
            this.ItemForintSupervisor3.Control = this.intSupervisor3GridLookUpEdit;
            this.ItemForintSupervisor3.CustomizationFormText = "Supervisor 3:";
            this.ItemForintSupervisor3.Location = new System.Drawing.Point(0, 52);
            this.ItemForintSupervisor3.Name = "ItemForintSupervisor3";
            this.ItemForintSupervisor3.Size = new System.Drawing.Size(1051, 82);
            this.ItemForintSupervisor3.Text = "Supervisor 3:";
            this.ItemForintSupervisor3.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "User Fields";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup17,
            this.layoutControlGroup18,
            this.simpleSeparator5,
            this.emptySpaceItem1});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1051, 134);
            this.layoutControlGroup9.Text = "User Fields";
            // 
            // layoutControlGroup17
            // 
            this.layoutControlGroup17.CustomizationFormText = "Text";
            this.layoutControlGroup17.ExpandButtonVisible = true;
            this.layoutControlGroup17.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrUser1,
            this.ItemForstrUser2,
            this.ItemForstrUser3});
            this.layoutControlGroup17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup17.Name = "layoutControlGroup17";
            this.layoutControlGroup17.Size = new System.Drawing.Size(503, 124);
            this.layoutControlGroup17.Text = "Text";
            // 
            // ItemForstrUser1
            // 
            this.ItemForstrUser1.Control = this.strUser1TextEdit;
            this.ItemForstrUser1.CustomizationFormText = "Text 1:";
            this.ItemForstrUser1.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrUser1.Name = "ItemForstrUser1";
            this.ItemForstrUser1.Size = new System.Drawing.Size(479, 24);
            this.ItemForstrUser1.Text = "Text 1:";
            this.ItemForstrUser1.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForstrUser2
            // 
            this.ItemForstrUser2.Control = this.strUser2TextEdit;
            this.ItemForstrUser2.CustomizationFormText = "Text 2:";
            this.ItemForstrUser2.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrUser2.Name = "ItemForstrUser2";
            this.ItemForstrUser2.Size = new System.Drawing.Size(479, 24);
            this.ItemForstrUser2.Text = "Text 2:";
            this.ItemForstrUser2.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForstrUser3
            // 
            this.ItemForstrUser3.Control = this.strUser3TextEdit;
            this.ItemForstrUser3.CustomizationFormText = "Text 3:";
            this.ItemForstrUser3.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrUser3.Name = "ItemForstrUser3";
            this.ItemForstrUser3.Size = new System.Drawing.Size(479, 30);
            this.ItemForstrUser3.Text = "Text 3:";
            this.ItemForstrUser3.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlGroup18
            // 
            this.layoutControlGroup18.CustomizationFormText = "Pick Lists";
            this.layoutControlGroup18.ExpandButtonVisible = true;
            this.layoutControlGroup18.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForUserPicklist1,
            this.ItemForUserPicklist2,
            this.ItemForUserPicklist3});
            this.layoutControlGroup18.Location = new System.Drawing.Point(505, 0);
            this.layoutControlGroup18.Name = "layoutControlGroup18";
            this.layoutControlGroup18.Size = new System.Drawing.Size(546, 124);
            this.layoutControlGroup18.Text = "Pick Lists";
            // 
            // ItemForUserPicklist1
            // 
            this.ItemForUserPicklist1.Control = this.UserPicklist1GridLookUpEdit;
            this.ItemForUserPicklist1.CustomizationFormText = "Pick List 1:";
            this.ItemForUserPicklist1.Location = new System.Drawing.Point(0, 0);
            this.ItemForUserPicklist1.Name = "ItemForUserPicklist1";
            this.ItemForUserPicklist1.Size = new System.Drawing.Size(522, 26);
            this.ItemForUserPicklist1.Text = "Pick List 1:";
            this.ItemForUserPicklist1.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForUserPicklist2
            // 
            this.ItemForUserPicklist2.Control = this.UserPicklist2GridLookUpEdit;
            this.ItemForUserPicklist2.CustomizationFormText = "Pick List 2:";
            this.ItemForUserPicklist2.Location = new System.Drawing.Point(0, 26);
            this.ItemForUserPicklist2.Name = "ItemForUserPicklist2";
            this.ItemForUserPicklist2.Size = new System.Drawing.Size(522, 26);
            this.ItemForUserPicklist2.Text = "Pick List 2:";
            this.ItemForUserPicklist2.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForUserPicklist3
            // 
            this.ItemForUserPicklist3.Control = this.UserPicklist3GridLookUpEdit;
            this.ItemForUserPicklist3.CustomizationFormText = "Pick List 3:";
            this.ItemForUserPicklist3.Location = new System.Drawing.Point(0, 52);
            this.ItemForUserPicklist3.Name = "ItemForUserPicklist3";
            this.ItemForUserPicklist3.Size = new System.Drawing.Size(522, 26);
            this.ItemForUserPicklist3.Text = "Pick List 3:";
            this.ItemForUserPicklist3.TextSize = new System.Drawing.Size(94, 13);
            // 
            // simpleSeparator5
            // 
            this.simpleSeparator5.AllowHotTrack = false;
            this.simpleSeparator5.CustomizationFormText = "simpleSeparator5";
            this.simpleSeparator5.Location = new System.Drawing.Point(503, 0);
            this.simpleSeparator5.Name = "simpleSeparator5";
            this.simpleSeparator5.Size = new System.Drawing.Size(2, 124);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 124);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1051, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Incidents";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1051, 134);
            this.layoutControlGroup4.Text = "Incidents";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.strIncidentsMemoEdit;
            this.layoutControlItem3.CustomizationFormText = "Incidents";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1051, 134);
            this.layoutControlItem3.Text = "Incidents";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup10.CaptionImageOptions.Image")));
            this.layoutControlGroup10.CustomizationFormText = "Remarks";
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(1051, 134);
            this.layoutControlGroup10.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(1051, 134);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1099, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWorkOrderPDFFile
            // 
            this.ItemForWorkOrderPDFFile.Control = this.WorkOrderPDFFileHyperLinkEdit;
            this.ItemForWorkOrderPDFFile.CustomizationFormText = "Saved PDF File:";
            this.ItemForWorkOrderPDFFile.Location = new System.Drawing.Point(0, 74);
            this.ItemForWorkOrderPDFFile.Name = "ItemForWorkOrderPDFFile";
            this.ItemForWorkOrderPDFFile.Size = new System.Drawing.Size(1099, 24);
            this.ItemForWorkOrderPDFFile.Text = "Saved PDF File:";
            this.ItemForWorkOrderPDFFile.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dataNavigator1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(200, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(294, 24);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForintRecordType
            // 
            this.ItemForintRecordType.Control = this.intRecordTypeGridLookUpEdit;
            this.ItemForintRecordType.CustomizationFormText = "Work Order Type:";
            this.ItemForintRecordType.Location = new System.Drawing.Point(494, 0);
            this.ItemForintRecordType.Name = "ItemForintRecordType";
            this.ItemForintRecordType.Size = new System.Drawing.Size(629, 24);
            this.ItemForintRecordType.Text = "Work Order Type:";
            this.ItemForintRecordType.TextSize = new System.Drawing.Size(94, 13);
            // 
            // simpleSeparator6
            // 
            this.simpleSeparator6.AllowHotTrack = false;
            this.simpleSeparator6.CustomizationFormText = "simpleSeparator6";
            this.simpleSeparator6.Location = new System.Drawing.Point(0, 24);
            this.simpleSeparator6.Name = "simpleSeparator6";
            this.simpleSeparator6.Size = new System.Drawing.Size(1123, 2);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup12,
            this.simpleSeparator1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 408);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1123, 266);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Work";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 2);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(1123, 264);
            this.layoutControlGroup12.Text = "Work";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridSplitContainer1;
            this.layoutControlItem1.CustomizationFormText = "Work Grid:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1099, 218);
            this.layoutControlItem1.Text = "Work Grid:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(1123, 2);
            // 
            // sp01391ATCostCentresWithBlankBindingSource
            // 
            this.sp01391ATCostCentresWithBlankBindingSource.DataMember = "sp01391_AT_Cost_Centres_With_Blank";
            this.sp01391ATCostCentresWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp01399_AT_Work_Order_EditTableAdapter
            // 
            this.sp01399_AT_Work_Order_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp00190_Contractor_List_With_BlankTableAdapter
            // 
            this.sp00190_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00226_Staff_List_With_BlankTableAdapter
            // 
            this.sp00226_Staff_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter
            // 
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.ClearBeforeFill = true;
            // 
            // sp01402_AT_Work_Order_Actions_EditTableAdapter
            // 
            this.sp01402_AT_Work_Order_Actions_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter
            // 
            this.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01403_AT_Work_Order_TypesTableAdapter
            // 
            this.sp01403_AT_Work_Order_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp01362_AT_Ownership_List_With_BlankTableAdapter
            // 
            this.sp01362_AT_Ownership_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01391_AT_Cost_Centres_With_BlankTableAdapter
            // 
            this.sp01391_AT_Cost_Centres_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_AT_WorkOrder_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1143, 750);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_WorkOrder_Edit";
            this.Text = "Edit Work Order";
            this.Activated += new System.EventHandler(this.frm_AT_WorkOrder_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_WorkOrder_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_WorkOrder_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderPDFFileHyperLinkEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01399ATWorkOrderEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01402ATWorkOrderActionsEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNearestHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMonJobRateActual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMonJobWorkUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditActualJobRateDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditDiscountRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditActionByPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSupervisor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditOwnership)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01362ATOwnershipListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditCostCentreCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intWorkOrderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtIssueDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtIssueDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strInvoiceNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOperationIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmailDetailsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompleteDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompleteDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderReferenceButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPrevWorkOrderIDButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPriorityIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSupervisor1GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSupervisor2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSupervisor3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCertifiedByIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContractorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIssuedByPersonIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIssuedByBodyIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCompanyHeaderIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00232CompanyHeaderDropDownListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRecordTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01403ATWorkOrderTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicklist1GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicklist2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicklist3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmailDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOperationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintWorkOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPrevWorkOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrInvoiceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPriorityID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIssuedByPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIssuedByBodyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCompanyHeaderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCertifiedByID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtCompleteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSupervisor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSupervisor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSupervisor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPicklist1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPicklist2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPicklist3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderPDFFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRecordType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01391ATCostCentresWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit intWorkOrderIDTextEdit;
        private System.Windows.Forms.BindingSource sp01399ATWorkOrderEditBindingSource;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private DevExpress.XtraEditors.DateEdit dtIssueDateDateEdit;
        private DevExpress.XtraEditors.ButtonEdit strInvoiceNumberButtonEdit;
        private DevExpress.XtraEditors.SpinEdit intOperationIDSpinEdit;
        private DevExpress.XtraEditors.TextEdit strUser1TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser2TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser3TextEdit;
        private DevExpress.XtraEditors.DateEdit dtCompleteDateDateEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.MemoEdit strEmailDetailsMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit WorkOrderReferenceButtonEdit;
        private DevExpress.XtraEditors.DateEdit TargetDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintWorkOrderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintPrevWorkOrderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintContractorID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintIssuedByPersonID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintIssuedByBodyID;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtIssueDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintPriorityID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSupervisor1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSupervisor2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSupervisor3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrInvoiceNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintOperationID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser3;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtCompleteDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCertifiedByID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrEmailDetails;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCompanyHeaderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintRecordType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkOrderReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPicklist1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPicklist2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPicklist3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatus;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTargetDate;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01399_AT_Work_Order_EditTableAdapter sp01399_AT_Work_Order_EditTableAdapter;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraEditors.ButtonEdit intPrevWorkOrderIDButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup18;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator6;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.GridLookUpEdit StatusGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit intPriorityIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.GridLookUpEdit intSupervisor1GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.GridLookUpEdit intSupervisor2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.GridLookUpEdit intSupervisor3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.GridLookUpEdit intCertifiedByIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.GridLookUpEdit intContractorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.GridLookUpEdit intIssuedByPersonIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.GridLookUpEdit intIssuedByBodyIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.GridLookUpEdit intCompanyHeaderIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraEditors.GridLookUpEdit intRecordTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraEditors.GridLookUpEdit UserPicklist1GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraEditors.GridLookUpEdit UserPicklist2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraEditors.GridLookUpEdit UserPicklist3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00190ContractorListWithBlankBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter sp00190_Contractor_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp00226StaffListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter sp00226_Staff_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colNetworkID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private System.Windows.Forms.BindingSource sp01372ATMultiplePicklistsWithBlanksBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCode;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource sp01402ATWorkOrderActionsEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colintActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colintWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colintPrevActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrNearestHouse;
        private DevExpress.XtraGrid.Columns.GridColumn colintPriorityID;
        private DevExpress.XtraGrid.Columns.GridColumn colintScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colintJobRateBudgeted;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobRateBudgeted;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colmonBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colintJobRateActual;
        private DevExpress.XtraGrid.Columns.GridColumn colActualJobRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobRateActual;
        private DevExpress.XtraGrid.Columns.GridColumn colmonDiscountRate;
        private DevExpress.XtraGrid.Columns.GridColumn colmonActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colintInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colintAction;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickListDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickListDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickListDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colintSortOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colintActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colintSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colintBudgetID;
        private DevExpress.XtraGrid.Columns.GridColumn colintOwnershipID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colintCostCentreID;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01402_AT_Work_Order_Actions_EditTableAdapter sp01402_AT_Work_Order_Actions_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMonJobRateActual;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditActualJobRateDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNearestHouse;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditActionByPerson;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditSupervisor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView16;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditOwnership;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditPriority;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private System.Windows.Forms.BindingSource sp00232CompanyHeaderDropDownListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colintHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colstrDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrSlogan;
        private System.Windows.Forms.BindingSource sp01403ATWorkOrderTypesBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01403_AT_Work_Order_TypesTableAdapter sp01403_AT_Work_Order_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colintTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colintOrder;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiCalculateDiscounts;
        private System.Windows.Forms.BindingSource sp01362ATOwnershipListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01362_AT_Ownership_List_With_BlankTableAdapter sp01362_AT_Ownership_List_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp01391ATCostCentresWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01391_AT_Cost_Centres_With_BlankTableAdapter sp01391_AT_Cost_Centres_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipID;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipCode;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditCostCentreCode;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditDiscountRate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalCOntractorDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn114;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn115;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn116;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn117;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn118;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn119;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn120;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn121;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditMonJobWorkUnits;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraEditors.MemoEdit strIncidentsMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.HyperLinkEdit WorkOrderPDFFileHyperLinkEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkOrderPDFFile;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

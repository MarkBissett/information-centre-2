using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_AT_Action_Edit_Select_Job_Rate : WoodPlan5.frmBase_Modal
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;


        public int intInspectionID = 0;
        public int intJobRate_RateID = 0;
        public int intActionByID = 0;
        public int intOnScheduleOfRates = 0;
        public int intAction = 0;

        public int intSelectedRateID = 0;
        public string strSelectedRateDescription = "";
        public decimal decSelectedRate = (decimal)0.00;

        private int intCriteriaType = 0;

        #endregion

        public frm_AT_Action_Edit_Select_Job_Rate()
        {
            InitializeComponent();
        }

        private void frm_AT_Action_Edit_Select_Job_Rate_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 200131;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp01392_AT_Job_Rates_For_ActionTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter.Connection.ConnectionString = strConnectionString;

            GridView view = (GridView)gridControl1.MainView;
            view.Appearance.FocusedRow.BackColor = Color.FromArgb(60, 0, 0, 240);

            view.BeginUpdate();
            this.sp01392_AT_Job_Rates_For_ActionTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01392_AT_Job_Rates_For_Action);
            view.EndUpdate();
            gridControl1.ForceInitialize();


            if (intJobRate_RateID != 0)  // Rate selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["RateID"], intJobRate_RateID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            view.BeginUpdate();
            view.BeginSort();

            int intFocusBestMatchRateID = 0;

            // Attempt to highlight best match rates... //
            int intRowHandle = 0;
            if (intOnScheduleOfRates == 1)
            {
                // Get Criteria Used //
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                intCriteriaType = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesJobRateCriteriaID"));

                DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter GetMatches = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                string strMatchingJobRates = GetMatches.sp01393_AT_Action_Get_Matching_Job_Rates(intInspectionID, intCriteriaType, intAction, intActionByID).ToString() ?? "";
                if (!string.IsNullOrEmpty(strMatchingJobRates))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] strArray = strMatchingJobRates.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    int intID = 0;
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["RateID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intRowHandle, "Match", 1);
                            view.MakeRowVisible(intRowHandle, false);
                            if (strArray.Length == 1 && intJobRate_RateID == 0) intFocusBestMatchRateID = intID;
                        }
                    }
                }
            }

            view.EndSort();
            view.EndUpdate();

            if (intFocusBestMatchRateID > 0)
            {
                intRowHandle = view.LocateByValue(0, view.Columns["RateID"], intFocusBestMatchRateID);
                if (intRowHandle != GridControl.InvalidRowHandle) gridView1.FocusedRowHandle = intRowHandle;
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //


        }

        public override void PostLoadView(object objParameter)
        {
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Rates Available";
                    break;
                case "gridView2":
                    message = "No Rate Criteria Availabl";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    intSelectedRateID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "RateID"));

                    float rate = (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "Rate").ToString()) ? (float)0.00 : (float)Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "Rate")));
                    strSelectedRateDescription = view.GetRowCellValue(view.FocusedRowHandle, "RateDesc").ToString() + " [" + String.Format("{0:C}", rate) + "]";

                    decSelectedRate = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "Rate"));
                    LoadLinkedRecords();
                    break;
                default:
                    break;
            }
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (intSelectedRateID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a job rate before proceeding.", "Select Job Rate", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void gridView1_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0) return;
            GridView view = sender as GridView;
            if (e.Column.FieldName == "Match" && e.Column.GroupIndex >= 0)
            {
                int rowHandle = e.ListSourceRowIndex;
                if (view.IsGroupRow(rowHandle))
                {
                    rowHandle = view.GetDataRowHandleByGroupRowHandle(rowHandle);
                }

                e.DisplayText = (Convert.ToInt64(gridView1.GetRowCellValue(rowHandle, "Match")) == 1 ? "Match" : "Non Match");
            }
        }

        private void repositoryItemColorEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            Color colour = (Color)ce.EditValue;

            GridView view = (GridView)gridControl1.MainView;
            view.FormatConditions.Clear();

            StyleFormatCondition NewCondition = new StyleFormatCondition(FormatConditionEnum.Equal, view.Columns["Match"], null, 1);
            NewCondition.Appearance.BackColor = colour;
            NewCondition.ApplyToRow = true;
            view.FormatConditions.Add(NewCondition);
            view.Appearance.FocusedRow.BackColor = Color.FromArgb(60, 0, 0, 240);

        }

        private void repositoryItemColorEdit1_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                ColorEdit ce = (ColorEdit)sender;
                ce.EditValue = Color.Empty;
                GridView view = (GridView)gridControl1.MainView;
                view.FormatConditions.Clear();
                view.Appearance.FocusedRow.Reset();
            }
         }

        private void LoadLinkedRecords()
        {
            GridView view = (GridView)gridControl1.MainView;
            int intRowHandle = view.FocusedRowHandle;
            string strSelectedIDs = "";
            if (intRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedIDs = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["RateID"])) + ',';
            }
            //Populate Linked Contacts //
            gridControl2.MainView.BeginUpdate();
            if (intRowHandle < 0)
            {
                this.dataSet_AT.sp00206_Job_Rate_Manager_Job_Rate_Parameters.Clear();
            }
            else
            {
                sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter.Fill(dataSet_AT.sp00206_Job_Rate_Manager_Job_Rate_Parameters, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intCriteriaType, 0);
            }
            gridControl2.MainView.EndUpdate();
        }



    }
}


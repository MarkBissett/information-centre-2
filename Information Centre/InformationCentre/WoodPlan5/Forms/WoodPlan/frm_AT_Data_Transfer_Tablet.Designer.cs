namespace WoodPlan5
{
    partial class frm_AT_Data_Transfer_Tablet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Data_Transfer_Tablet));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEditSyncPictures = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSyncLinkedDocumentsDesktop = new DevExpress.XtraEditors.CheckEdit();
            this.buttonEditChooseImportFolder = new DevExpress.XtraEditors.ButtonEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.comboBoxEditWorkOrderFilter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditInspectionDateFilter = new DevExpress.XtraEditors.DateEdit();
            this.btnServer = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditImportFromTablet = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditExportToTablet = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEditSyncLinkedPicturesTablet = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSyncLinkedDocumentsTablet = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSyncMaps = new DevExpress.XtraEditors.CheckEdit();
            this.btnTablet = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditExportToDesktop = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditImportFromDesktop = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncPictures.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncLinkedDocumentsDesktop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditChooseImportFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWorkOrderFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDateFilter.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDateFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditImportFromTablet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExportToTablet.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncLinkedPicturesTablet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncLinkedDocumentsTablet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncMaps.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExportToDesktop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditImportFromDesktop.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(519, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 318);
            this.barDockControlBottom.Size = new System.Drawing.Size(519, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 318);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(519, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 318);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(519, 318);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.checkEditSyncPictures);
            this.xtraTabPage1.Controls.Add(this.checkEditSyncLinkedDocumentsDesktop);
            this.xtraTabPage1.Controls.Add(this.buttonEditChooseImportFolder);
            this.xtraTabPage1.Controls.Add(this.groupControl1);
            this.xtraTabPage1.Controls.Add(this.btnServer);
            this.xtraTabPage1.Controls.Add(this.checkEditImportFromTablet);
            this.xtraTabPage1.Controls.Add(this.checkEditExportToTablet);
            this.xtraTabPage1.Controls.Add(this.labelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(514, 292);
            this.xtraTabPage1.Text = "Server \\ Desktop PC";
            // 
            // checkEditSyncPictures
            // 
            this.checkEditSyncPictures.Location = new System.Drawing.Point(36, 166);
            this.checkEditSyncPictures.MenuManager = this.barManager1;
            this.checkEditSyncPictures.Name = "checkEditSyncPictures";
            this.checkEditSyncPictures.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditSyncPictures.Properties.Caption = "Export <b>Linked Picture Files</b>";
            this.checkEditSyncPictures.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEditSyncPictures.Size = new System.Drawing.Size(189, 19);
            this.checkEditSyncPictures.TabIndex = 15;
            this.checkEditSyncPictures.Visible = false;
            // 
            // checkEditSyncLinkedDocumentsDesktop
            // 
            this.checkEditSyncLinkedDocumentsDesktop.Location = new System.Drawing.Point(36, 146);
            this.checkEditSyncLinkedDocumentsDesktop.MenuManager = this.barManager1;
            this.checkEditSyncLinkedDocumentsDesktop.Name = "checkEditSyncLinkedDocumentsDesktop";
            this.checkEditSyncLinkedDocumentsDesktop.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditSyncLinkedDocumentsDesktop.Properties.Caption = "Export <b>Linked Document Files</b>";
            this.checkEditSyncLinkedDocumentsDesktop.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEditSyncLinkedDocumentsDesktop.Size = new System.Drawing.Size(189, 19);
            this.checkEditSyncLinkedDocumentsDesktop.TabIndex = 14;
            this.checkEditSyncLinkedDocumentsDesktop.Visible = false;
            // 
            // buttonEditChooseImportFolder
            // 
            this.buttonEditChooseImportFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditChooseImportFolder.Location = new System.Drawing.Point(26, 119);
            this.buttonEditChooseImportFolder.MenuManager = this.barManager1;
            this.buttonEditChooseImportFolder.Name = "buttonEditChooseImportFolder";
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Choose Folder - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to open the Browse For Folder screen for selecting the data set to impor" +
    "t.\r\n";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.buttonEditChooseImportFolder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose Folder", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleLeft, ((System.Drawing.Image)(resources.GetObject("buttonEditChooseImportFolder.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, superToolTip1, true)});
            this.buttonEditChooseImportFolder.Properties.NullValuePrompt = "Click Choose Folder to select data set for import";
            this.buttonEditChooseImportFolder.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditChooseImportFolder.Size = new System.Drawing.Size(468, 22);
            this.buttonEditChooseImportFolder.TabIndex = 12;
            this.buttonEditChooseImportFolder.Visible = false;
            this.buttonEditChooseImportFolder.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditChooseImportFolder_ButtonClick);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.comboBoxEditWorkOrderFilter);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.dateEditInspectionDateFilter);
            this.groupControl1.Location = new System.Drawing.Point(26, 120);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(475, 98);
            this.groupControl1.TabIndex = 10;
            this.groupControl1.Text = "Filter Exported Data:";
            this.groupControl1.Visible = false;
            // 
            // comboBoxEditWorkOrderFilter
            // 
            this.comboBoxEditWorkOrderFilter.EditValue = "No Work Orders";
            this.comboBoxEditWorkOrderFilter.Location = new System.Drawing.Point(258, 67);
            this.comboBoxEditWorkOrderFilter.MenuManager = this.barManager1;
            this.comboBoxEditWorkOrderFilter.Name = "comboBoxEditWorkOrderFilter";
            this.comboBoxEditWorkOrderFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditWorkOrderFilter.Properties.Items.AddRange(new object[] {
            "No Work Orders",
            "Incomplete Work Orders Only",
            "All Work Orders"});
            this.comboBoxEditWorkOrderFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditWorkOrderFilter.Size = new System.Drawing.Size(208, 20);
            this.comboBoxEditWorkOrderFilter.TabIndex = 11;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Location = new System.Drawing.Point(12, 73);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(110, 13);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Export <b>Work Orders</b>:";
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Location = new System.Drawing.Point(12, 49);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(348, 13);
            this.labelControl3.TabIndex = 9;
            this.labelControl3.Text = "Export <b>Inspections</b> and <b>Actions</b> with a Date greater than or equal to" +
    ":";
            // 
            // dateEditInspectionDateFilter
            // 
            this.dateEditInspectionDateFilter.EditValue = null;
            this.dateEditInspectionDateFilter.Location = new System.Drawing.Point(366, 43);
            this.dateEditInspectionDateFilter.MenuManager = this.barManager1;
            this.dateEditInspectionDateFilter.Name = "dateEditInspectionDateFilter";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all In" +
    "spections and Actions will be exported.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditInspectionDateFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, superToolTip2, true)});
            this.dateEditInspectionDateFilter.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditInspectionDateFilter.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.dateEditInspectionDateFilter.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.dateEditInspectionDateFilter.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditInspectionDateFilter.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditInspectionDateFilter.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditInspectionDateFilter.Size = new System.Drawing.Size(100, 20);
            this.dateEditInspectionDateFilter.TabIndex = 8;
            this.dateEditInspectionDateFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditInspectionDateFilter_ButtonClick);
            // 
            // btnServer
            // 
            this.btnServer.Enabled = false;
            this.btnServer.Location = new System.Drawing.Point(10, 260);
            this.btnServer.Name = "btnServer";
            this.btnServer.Size = new System.Drawing.Size(183, 23);
            this.btnServer.TabIndex = 7;
            this.btnServer.Text = "Export \\ Import ?";
            this.btnServer.Click += new System.EventHandler(this.btnServer_Click);
            // 
            // checkEditImportFromTablet
            // 
            this.checkEditImportFromTablet.Location = new System.Drawing.Point(8, 87);
            this.checkEditImportFromTablet.MenuManager = this.barManager1;
            this.checkEditImportFromTablet.Name = "checkEditImportFromTablet";
            this.checkEditImportFromTablet.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditImportFromTablet.Properties.Caption = "<b>Import</b> data from your Tablet PC";
            this.checkEditImportFromTablet.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditImportFromTablet.Properties.RadioGroupIndex = 1;
            this.checkEditImportFromTablet.Size = new System.Drawing.Size(187, 19);
            this.checkEditImportFromTablet.TabIndex = 2;
            this.checkEditImportFromTablet.TabStop = false;
            this.checkEditImportFromTablet.CheckedChanged += new System.EventHandler(this.checkEditImportFromTablet_CheckedChanged);
            // 
            // checkEditExportToTablet
            // 
            this.checkEditExportToTablet.Location = new System.Drawing.Point(8, 63);
            this.checkEditExportToTablet.MenuManager = this.barManager1;
            this.checkEditExportToTablet.Name = "checkEditExportToTablet";
            this.checkEditExportToTablet.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditExportToTablet.Properties.Caption = "<b>Export</b> data to your Tablet PC";
            this.checkEditExportToTablet.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditExportToTablet.Properties.RadioGroupIndex = 1;
            this.checkEditExportToTablet.Size = new System.Drawing.Size(187, 19);
            this.checkEditExportToTablet.TabIndex = 0;
            this.checkEditExportToTablet.TabStop = false;
            this.checkEditExportToTablet.CheckedChanged += new System.EventHandler(this.checkEditExportToTablet_CheckedChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 16);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(425, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Welcome to the <b>Server \\ Desktop PC</b> Data Transfer page. What would you like" +
    " to do?";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.checkEditSyncLinkedPicturesTablet);
            this.xtraTabPage2.Controls.Add(this.checkEditSyncLinkedDocumentsTablet);
            this.xtraTabPage2.Controls.Add(this.checkEditSyncMaps);
            this.xtraTabPage2.Controls.Add(this.btnTablet);
            this.xtraTabPage2.Controls.Add(this.checkEditExportToDesktop);
            this.xtraTabPage2.Controls.Add(this.checkEditImportFromDesktop);
            this.xtraTabPage2.Controls.Add(this.labelControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(514, 292);
            this.xtraTabPage2.Text = "Tablet PC";
            // 
            // checkEditSyncLinkedPicturesTablet
            // 
            this.checkEditSyncLinkedPicturesTablet.Location = new System.Drawing.Point(26, 144);
            this.checkEditSyncLinkedPicturesTablet.MenuManager = this.barManager1;
            this.checkEditSyncLinkedPicturesTablet.Name = "checkEditSyncLinkedPicturesTablet";
            this.checkEditSyncLinkedPicturesTablet.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditSyncLinkedPicturesTablet.Properties.Caption = "Import <b>Linked Picture Files</b> during Import";
            this.checkEditSyncLinkedPicturesTablet.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEditSyncLinkedPicturesTablet.Size = new System.Drawing.Size(279, 19);
            this.checkEditSyncLinkedPicturesTablet.TabIndex = 14;
            this.checkEditSyncLinkedPicturesTablet.Visible = false;
            // 
            // checkEditSyncLinkedDocumentsTablet
            // 
            this.checkEditSyncLinkedDocumentsTablet.Location = new System.Drawing.Point(26, 120);
            this.checkEditSyncLinkedDocumentsTablet.MenuManager = this.barManager1;
            this.checkEditSyncLinkedDocumentsTablet.Name = "checkEditSyncLinkedDocumentsTablet";
            this.checkEditSyncLinkedDocumentsTablet.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditSyncLinkedDocumentsTablet.Properties.Caption = "Import <b>Linked Document Files</b> during Import";
            this.checkEditSyncLinkedDocumentsTablet.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEditSyncLinkedDocumentsTablet.Size = new System.Drawing.Size(279, 19);
            this.checkEditSyncLinkedDocumentsTablet.TabIndex = 13;
            this.checkEditSyncLinkedDocumentsTablet.Visible = false;
            // 
            // checkEditSyncMaps
            // 
            this.checkEditSyncMaps.Location = new System.Drawing.Point(26, 168);
            this.checkEditSyncMaps.MenuManager = this.barManager1;
            this.checkEditSyncMaps.Name = "checkEditSyncMaps";
            this.checkEditSyncMaps.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditSyncMaps.Properties.Caption = "Synchronize <b>Mapping Files</b> during Import";
            this.checkEditSyncMaps.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEditSyncMaps.Size = new System.Drawing.Size(279, 19);
            this.checkEditSyncMaps.TabIndex = 12;
            this.checkEditSyncMaps.Visible = false;
            // 
            // btnTablet
            // 
            this.btnTablet.Enabled = false;
            this.btnTablet.Location = new System.Drawing.Point(10, 260);
            this.btnTablet.Name = "btnTablet";
            this.btnTablet.Size = new System.Drawing.Size(183, 23);
            this.btnTablet.TabIndex = 6;
            this.btnTablet.Text = "Import \\ Export ?";
            this.btnTablet.Click += new System.EventHandler(this.btnTablet_Click);
            // 
            // checkEditExportToDesktop
            // 
            this.checkEditExportToDesktop.Location = new System.Drawing.Point(8, 87);
            this.checkEditExportToDesktop.MenuManager = this.barManager1;
            this.checkEditExportToDesktop.Name = "checkEditExportToDesktop";
            this.checkEditExportToDesktop.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditExportToDesktop.Properties.Caption = "<b>Export</b> data from your Tablet PC to your Server \\ Desktop PC";
            this.checkEditExportToDesktop.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditExportToDesktop.Properties.RadioGroupIndex = 1;
            this.checkEditExportToDesktop.Size = new System.Drawing.Size(348, 19);
            this.checkEditExportToDesktop.TabIndex = 5;
            this.checkEditExportToDesktop.TabStop = false;
            this.checkEditExportToDesktop.CheckedChanged += new System.EventHandler(this.checkEditExportToDesktop_CheckedChanged);
            // 
            // checkEditImportFromDesktop
            // 
            this.checkEditImportFromDesktop.Location = new System.Drawing.Point(8, 63);
            this.checkEditImportFromDesktop.MenuManager = this.barManager1;
            this.checkEditImportFromDesktop.Name = "checkEditImportFromDesktop";
            this.checkEditImportFromDesktop.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditImportFromDesktop.Properties.Caption = "<b>Import</b> data from your Server \\ Desktop PC into your Tablet PC";
            this.checkEditImportFromDesktop.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditImportFromDesktop.Properties.RadioGroupIndex = 1;
            this.checkEditImportFromDesktop.Size = new System.Drawing.Size(348, 19);
            this.checkEditImportFromDesktop.TabIndex = 3;
            this.checkEditImportFromDesktop.TabStop = false;
            this.checkEditImportFromDesktop.CheckedChanged += new System.EventHandler(this.checkEditImportFromDesktop_CheckedChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Location = new System.Drawing.Point(10, 16);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(364, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Welcome to the <b>Tablet PC</b> Data Transfer page. What would you like to do?";
            // 
            // frm_AT_Data_Transfer_Tablet
            // 
            this.ClientSize = new System.Drawing.Size(519, 318);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_AT_Data_Transfer_Tablet";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tablet Data Transfer";
            this.Load += new System.EventHandler(this.frm_AT_Data_Transfer_Tablet_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncPictures.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncLinkedDocumentsDesktop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditChooseImportFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWorkOrderFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDateFilter.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDateFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditImportFromTablet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExportToTablet.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncLinkedPicturesTablet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncLinkedDocumentsTablet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncMaps.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExportToDesktop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditImportFromDesktop.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditExportToTablet;
        private DevExpress.XtraEditors.CheckEdit checkEditImportFromTablet;
        private DevExpress.XtraEditors.CheckEdit checkEditExportToDesktop;
        private DevExpress.XtraEditors.CheckEdit checkEditImportFromDesktop;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnTablet;
        private DevExpress.XtraEditors.SimpleButton btnServer;
        private DevExpress.XtraEditors.DateEdit dateEditInspectionDateFilter;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditWorkOrderFilter;
        private DevExpress.XtraEditors.ButtonEdit buttonEditChooseImportFolder;
        private DevExpress.XtraEditors.CheckEdit checkEditSyncMaps;
        private DevExpress.XtraEditors.CheckEdit checkEditSyncLinkedDocumentsTablet;
        private DevExpress.XtraEditors.CheckEdit checkEditSyncLinkedDocumentsDesktop;
        private DevExpress.XtraEditors.CheckEdit checkEditSyncPictures;
        private DevExpress.XtraEditors.CheckEdit checkEditSyncLinkedPicturesTablet;
    }
}

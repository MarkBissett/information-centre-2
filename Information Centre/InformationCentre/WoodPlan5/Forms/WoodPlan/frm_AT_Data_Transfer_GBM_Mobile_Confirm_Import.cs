using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WoodPlan5
{
    public partial class frm_AT_Data_Transfer_GBM_Mobile_Confirm_Import : BaseObjects.frmBase
    {
        public frm_AT_Data_Transfer_GBM_Mobile_Confirm_Import()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}


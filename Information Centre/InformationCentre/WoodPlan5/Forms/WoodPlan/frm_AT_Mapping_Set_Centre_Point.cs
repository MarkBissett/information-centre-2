using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Set_Centre_Point : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intRecordID = 0;
        public string strRecordType = "";

        #endregion
        
        public frm_AT_Mapping_Set_Centre_Point()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Set_Centre_Point_Load(object sender, EventArgs e)
        {
            this.FormID = 200416;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked at end of event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01318_AT_Tree_Picker_Set_Centre_PointTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01318_AT_Tree_Picker_Set_Centre_PointTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01318_AT_Tree_Picker_Set_Centre_Point, strRecordType);

            switch (strRecordType.ToLower())
            {
                case "site":
                    this.Text = "Set Site Centre Point";
                    labelControl1.Text = "Select <b>Site</b> to set the centre point for then click <b>OK button</b>.\n<color=Red>Red</color> records have no Centre Point Defined.";
                    gridControl1.MainView = gridView1;
                    break;
                case "client":
                    this.Text = "Set Client Centre Point";
                    labelControl1.Text = "Select <b>Client</b> to set the centre point for then click <b>OK button</b>.\n<color=Red>Red</color> records have no Centre Point Defined.";
                    gridControl1.MainView = gridView2;
                    break;
            }
            view.EndUpdate();
            gridControl1.ForceInitialize();

            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.FocusedView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                XtraMessageBox.Show("Select the record to set the centre point for before proceeding.", "Set Centre Point", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intRecordID = Convert.ToInt32(view.GetFocusedRowCellValue("RecordID"));
            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails any validation tests //
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }



        #region Grid Control 1

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        #endregion

    }
}


namespace WoodPlan5
{
    partial class frm_Mail_Merge_Select_Data_For_Merge_Incidents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colintHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEditShowCompleted = new DevExpress.XtraEditors.CheckEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.btnLoadIncidents = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.CompanyHeaderGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrSlogan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01411ATIncidentManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_Incidents = new WoodPlan5.DataSet_AT_Incidents();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBriefDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonRecordedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonRecordedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonResponsibleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonResponsible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTargetDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateClosed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedBySurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickList1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickList2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickList3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedInspectionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp01411_AT_Incident_ManagerTableAdapter = new WoodPlan5.DataSet_AT_IncidentsTableAdapters.sp01411_AT_Incident_ManagerTableAdapter();
            this.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowCompleted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyHeaderGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00232CompanyHeaderDropDownListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01411ATIncidentManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Incidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(607, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 421);
            this.barDockControlBottom.Size = new System.Drawing.Size(607, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 395);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(607, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 395);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colintHeaderID
            // 
            this.colintHeaderID.Caption = "Header ID";
            this.colintHeaderID.FieldName = "intHeaderID";
            this.colintHeaderID.Name = "colintHeaderID";
            this.colintHeaderID.OptionsColumn.AllowEdit = false;
            this.colintHeaderID.OptionsColumn.AllowFocus = false;
            this.colintHeaderID.OptionsColumn.ReadOnly = true;
            this.colintHeaderID.Width = 70;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.labelControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.CompanyHeaderGridLookUpEdit);
            this.splitContainerControl1.Panel2.Controls.Add(this.btnCancel);
            this.splitContainerControl1.Panel2.Controls.Add(this.btnOK);
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Incidents";
            this.splitContainerControl1.Size = new System.Drawing.Size(607, 395);
            this.splitContainerControl1.SplitterPosition = 27;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.checkEditShowCompleted);
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Controls.Add(this.btnLoadIncidents);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(607, 27);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // checkEditShowCompleted
            // 
            this.checkEditShowCompleted.EditValue = 0;
            this.checkEditShowCompleted.Location = new System.Drawing.Point(332, 2);
            this.checkEditShowCompleted.MenuManager = this.barManager1;
            this.checkEditShowCompleted.Name = "checkEditShowCompleted";
            this.checkEditShowCompleted.Properties.Caption = "Show Completed Incidents";
            this.checkEditShowCompleted.Properties.ValueChecked = 1;
            this.checkEditShowCompleted.Properties.ValueUnchecked = 0;
            this.checkEditShowCompleted.Size = new System.Drawing.Size(150, 19);
            this.checkEditShowCompleted.StyleController = this.layoutControl1;
            this.checkEditShowCompleted.TabIndex = 12;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(231, 2);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all In" +
    "cidents will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, true)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(97, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 10;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(59, 2);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all In" +
    "cidents will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, true)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(111, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // btnLoadIncidents
            // 
            this.btnLoadIncidents.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnLoadIncidents.Location = new System.Drawing.Point(486, 2);
            this.btnLoadIncidents.Name = "btnLoadIncidents";
            this.btnLoadIncidents.Size = new System.Drawing.Size(119, 22);
            this.btnLoadIncidents.StyleController = this.layoutControl1;
            this.btnLoadIncidents.TabIndex = 11;
            this.btnLoadIncidents.Text = "Load Incidents";
            this.btnLoadIncidents.Click += new System.EventHandler(this.btnLoadIncidents_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(607, 27);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditFromDate;
            this.layoutControlItem2.CustomizationFormText = "From Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(172, 27);
            this.layoutControlItem2.Text = "From Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dateEditToDate;
            this.layoutControlItem3.CustomizationFormText = "To Date:";
            this.layoutControlItem3.Location = new System.Drawing.Point(172, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(158, 27);
            this.layoutControlItem3.Text = "To Date:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnLoadIncidents;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(484, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(123, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(123, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(123, 27);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkEditShowCompleted;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(330, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(154, 27);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Location = new System.Drawing.Point(2, 314);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(114, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Company Header Used:";
            // 
            // CompanyHeaderGridLookUpEdit
            // 
            this.CompanyHeaderGridLookUpEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CompanyHeaderGridLookUpEdit.Location = new System.Drawing.Point(117, 311);
            this.CompanyHeaderGridLookUpEdit.MenuManager = this.barManager1;
            this.CompanyHeaderGridLookUpEdit.Name = "CompanyHeaderGridLookUpEdit";
            this.CompanyHeaderGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CompanyHeaderGridLookUpEdit.Properties.DataSource = this.sp00232CompanyHeaderDropDownListWithBlankBindingSource;
            this.CompanyHeaderGridLookUpEdit.Properties.DisplayMember = "strDescription";
            this.CompanyHeaderGridLookUpEdit.Properties.NullText = "";
            this.CompanyHeaderGridLookUpEdit.Properties.ValueMember = "intHeaderID";
            this.CompanyHeaderGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.CompanyHeaderGridLookUpEdit.Size = new System.Drawing.Size(299, 20);
            this.CompanyHeaderGridLookUpEdit.TabIndex = 3;
            // 
            // sp00232CompanyHeaderDropDownListWithBlankBindingSource
            // 
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource.DataMember = "sp00232_Company_Header_Drop_Down_List_With_Blank";
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colintHeaderID,
            this.colstrAddressLine1,
            this.colstrCompanyName,
            this.colstrDescription,
            this.colstrTelephone1,
            this.colstrSlogan});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colintHeaderID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrCompanyName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colstrAddressLine1
            // 
            this.colstrAddressLine1.Caption = "Address Line 1";
            this.colstrAddressLine1.FieldName = "strAddressLine1";
            this.colstrAddressLine1.Name = "colstrAddressLine1";
            this.colstrAddressLine1.OptionsColumn.AllowEdit = false;
            this.colstrAddressLine1.OptionsColumn.AllowFocus = false;
            this.colstrAddressLine1.OptionsColumn.ReadOnly = true;
            this.colstrAddressLine1.Visible = true;
            this.colstrAddressLine1.VisibleIndex = 2;
            this.colstrAddressLine1.Width = 133;
            // 
            // colstrCompanyName
            // 
            this.colstrCompanyName.Caption = "Company Name";
            this.colstrCompanyName.FieldName = "strCompanyName";
            this.colstrCompanyName.Name = "colstrCompanyName";
            this.colstrCompanyName.OptionsColumn.AllowEdit = false;
            this.colstrCompanyName.OptionsColumn.AllowFocus = false;
            this.colstrCompanyName.OptionsColumn.ReadOnly = true;
            this.colstrCompanyName.Visible = true;
            this.colstrCompanyName.VisibleIndex = 1;
            this.colstrCompanyName.Width = 211;
            // 
            // colstrDescription
            // 
            this.colstrDescription.Caption = "Header Description";
            this.colstrDescription.FieldName = "strDescription";
            this.colstrDescription.Name = "colstrDescription";
            this.colstrDescription.OptionsColumn.AllowEdit = false;
            this.colstrDescription.OptionsColumn.AllowFocus = false;
            this.colstrDescription.OptionsColumn.ReadOnly = true;
            this.colstrDescription.Visible = true;
            this.colstrDescription.VisibleIndex = 0;
            this.colstrDescription.Width = 255;
            // 
            // colstrTelephone1
            // 
            this.colstrTelephone1.Caption = "Telephone 1";
            this.colstrTelephone1.FieldName = "strTelephone1";
            this.colstrTelephone1.Name = "colstrTelephone1";
            this.colstrTelephone1.OptionsColumn.AllowEdit = false;
            this.colstrTelephone1.OptionsColumn.AllowFocus = false;
            this.colstrTelephone1.OptionsColumn.ReadOnly = true;
            this.colstrTelephone1.Visible = true;
            this.colstrTelephone1.VisibleIndex = 3;
            this.colstrTelephone1.Width = 108;
            // 
            // colstrSlogan
            // 
            this.colstrSlogan.Caption = "Slogan";
            this.colstrSlogan.FieldName = "strSlogan";
            this.colstrSlogan.Name = "colstrSlogan";
            this.colstrSlogan.OptionsColumn.AllowEdit = false;
            this.colstrSlogan.OptionsColumn.AllowFocus = false;
            this.colstrSlogan.OptionsColumn.ReadOnly = true;
            this.colstrSlogan.Width = 53;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(524, 311);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(443, 311);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(603, 305);
            this.gridSplitContainer1.TabIndex = 5;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01411ATIncidentManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl1.Size = new System.Drawing.Size(603, 305);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01411ATIncidentManagerBindingSource
            // 
            this.sp01411ATIncidentManagerBindingSource.DataMember = "sp01411_AT_Incident_Manager";
            this.sp01411ATIncidentManagerBindingSource.DataSource = this.dataSet_AT_Incidents;
            // 
            // dataSet_AT_Incidents
            // 
            this.dataSet_AT_Incidents.DataSetName = "DataSet_AT_Incidents";
            this.dataSet_AT_Incidents.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIncidentID,
            this.colIncidentTypeID,
            this.colIncidentType,
            this.colReferenceID,
            this.colBriefDescription,
            this.colPersonRecordedByID,
            this.colPersonRecordedBy,
            this.colPersonResponsibleID,
            this.colPersonResponsible,
            this.colDateRecorded,
            this.colTargetDate,
            this.colDateClosed,
            this.colIncidentStatus,
            this.colIncidentStatusDescription,
            this.colintX,
            this.colintY,
            this.colReportedBySurname,
            this.colReportedByForename,
            this.colReportedByAddressLine1,
            this.colReportedByAddressLine2,
            this.colReportedByAddressLine3,
            this.colReportedByAddressLine4,
            this.colReportedByAddressLine5,
            this.colReportedByPostcode,
            this.colReportedByTelephone1,
            this.colIncidentAddressLine1,
            this.colIncidentAddressLine2,
            this.colIncidentAddressLine3,
            this.colIncidentAddressLine4,
            this.colIncidentAddressLine5,
            this.colIncidentPostcode,
            this.colUserPickList1,
            this.colUserPickList2,
            this.colUserPickList3,
            this.colClientID,
            this.colClientName,
            this.colRemarks,
            this.colLinkedInspectionCount,
            this.colLinkedActionCount});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 518, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIncidentType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTargetDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            // 
            // colIncidentID
            // 
            this.colIncidentID.Caption = "Incident ID";
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentID.Width = 74;
            // 
            // colIncidentTypeID
            // 
            this.colIncidentTypeID.Caption = "Incident Type ID";
            this.colIncidentTypeID.FieldName = "IncidentTypeID";
            this.colIncidentTypeID.Name = "colIncidentTypeID";
            this.colIncidentTypeID.OptionsColumn.AllowEdit = false;
            this.colIncidentTypeID.OptionsColumn.AllowFocus = false;
            this.colIncidentTypeID.OptionsColumn.ReadOnly = true;
            this.colIncidentTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentTypeID.Width = 101;
            // 
            // colIncidentType
            // 
            this.colIncidentType.Caption = "Incident Type";
            this.colIncidentType.FieldName = "IncidentType";
            this.colIncidentType.Name = "colIncidentType";
            this.colIncidentType.OptionsColumn.AllowEdit = false;
            this.colIncidentType.OptionsColumn.AllowFocus = false;
            this.colIncidentType.OptionsColumn.ReadOnly = true;
            this.colIncidentType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentType.Visible = true;
            this.colIncidentType.VisibleIndex = 0;
            this.colIncidentType.Width = 135;
            // 
            // colReferenceID
            // 
            this.colReferenceID.Caption = "Reference Number";
            this.colReferenceID.FieldName = "ReferenceID";
            this.colReferenceID.Name = "colReferenceID";
            this.colReferenceID.OptionsColumn.AllowEdit = false;
            this.colReferenceID.OptionsColumn.AllowFocus = false;
            this.colReferenceID.OptionsColumn.ReadOnly = true;
            this.colReferenceID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReferenceID.Visible = true;
            this.colReferenceID.VisibleIndex = 1;
            this.colReferenceID.Width = 123;
            // 
            // colBriefDescription
            // 
            this.colBriefDescription.Caption = "Brief Description";
            this.colBriefDescription.FieldName = "BriefDescription";
            this.colBriefDescription.Name = "colBriefDescription";
            this.colBriefDescription.OptionsColumn.AllowEdit = false;
            this.colBriefDescription.OptionsColumn.AllowFocus = false;
            this.colBriefDescription.OptionsColumn.ReadOnly = true;
            this.colBriefDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBriefDescription.Visible = true;
            this.colBriefDescription.VisibleIndex = 2;
            this.colBriefDescription.Width = 242;
            // 
            // colPersonRecordedByID
            // 
            this.colPersonRecordedByID.Caption = "Recorded By ID";
            this.colPersonRecordedByID.FieldName = "PersonRecordedByID";
            this.colPersonRecordedByID.Name = "colPersonRecordedByID";
            this.colPersonRecordedByID.OptionsColumn.AllowEdit = false;
            this.colPersonRecordedByID.OptionsColumn.AllowFocus = false;
            this.colPersonRecordedByID.OptionsColumn.ReadOnly = true;
            this.colPersonRecordedByID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPersonRecordedByID.Width = 96;
            // 
            // colPersonRecordedBy
            // 
            this.colPersonRecordedBy.Caption = "Recorded By";
            this.colPersonRecordedBy.FieldName = "PersonRecordedBy";
            this.colPersonRecordedBy.Name = "colPersonRecordedBy";
            this.colPersonRecordedBy.OptionsColumn.AllowEdit = false;
            this.colPersonRecordedBy.OptionsColumn.AllowFocus = false;
            this.colPersonRecordedBy.OptionsColumn.ReadOnly = true;
            this.colPersonRecordedBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPersonRecordedBy.Width = 124;
            // 
            // colPersonResponsibleID
            // 
            this.colPersonResponsibleID.Caption = "Person Responsible ID";
            this.colPersonResponsibleID.FieldName = "PersonResponsibleID";
            this.colPersonResponsibleID.Name = "colPersonResponsibleID";
            this.colPersonResponsibleID.OptionsColumn.AllowEdit = false;
            this.colPersonResponsibleID.OptionsColumn.AllowFocus = false;
            this.colPersonResponsibleID.OptionsColumn.ReadOnly = true;
            this.colPersonResponsibleID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPersonResponsibleID.Width = 128;
            // 
            // colPersonResponsible
            // 
            this.colPersonResponsible.Caption = "Person Responsible";
            this.colPersonResponsible.FieldName = "PersonResponsible";
            this.colPersonResponsible.Name = "colPersonResponsible";
            this.colPersonResponsible.OptionsColumn.AllowEdit = false;
            this.colPersonResponsible.OptionsColumn.AllowFocus = false;
            this.colPersonResponsible.OptionsColumn.ReadOnly = true;
            this.colPersonResponsible.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPersonResponsible.Visible = true;
            this.colPersonResponsible.VisibleIndex = 7;
            this.colPersonResponsible.Width = 114;
            // 
            // colDateRecorded
            // 
            this.colDateRecorded.Caption = "Date Recorded";
            this.colDateRecorded.FieldName = "DateRecorded";
            this.colDateRecorded.Name = "colDateRecorded";
            this.colDateRecorded.OptionsColumn.AllowEdit = false;
            this.colDateRecorded.OptionsColumn.AllowFocus = false;
            this.colDateRecorded.OptionsColumn.ReadOnly = true;
            this.colDateRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateRecorded.Visible = true;
            this.colDateRecorded.VisibleIndex = 4;
            this.colDateRecorded.Width = 110;
            // 
            // colTargetDate
            // 
            this.colTargetDate.Caption = "Target Date";
            this.colTargetDate.FieldName = "TargetDate";
            this.colTargetDate.Name = "colTargetDate";
            this.colTargetDate.OptionsColumn.AllowEdit = false;
            this.colTargetDate.OptionsColumn.AllowFocus = false;
            this.colTargetDate.OptionsColumn.ReadOnly = true;
            this.colTargetDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTargetDate.Visible = true;
            this.colTargetDate.VisibleIndex = 5;
            this.colTargetDate.Width = 92;
            // 
            // colDateClosed
            // 
            this.colDateClosed.Caption = "Closed Date";
            this.colDateClosed.FieldName = "DateClosed";
            this.colDateClosed.Name = "colDateClosed";
            this.colDateClosed.OptionsColumn.AllowEdit = false;
            this.colDateClosed.OptionsColumn.AllowFocus = false;
            this.colDateClosed.OptionsColumn.ReadOnly = true;
            this.colDateClosed.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateClosed.Visible = true;
            this.colDateClosed.VisibleIndex = 6;
            this.colDateClosed.Width = 87;
            // 
            // colIncidentStatus
            // 
            this.colIncidentStatus.Caption = "Status ID";
            this.colIncidentStatus.FieldName = "IncidentStatus";
            this.colIncidentStatus.Name = "colIncidentStatus";
            this.colIncidentStatus.OptionsColumn.AllowEdit = false;
            this.colIncidentStatus.OptionsColumn.AllowFocus = false;
            this.colIncidentStatus.OptionsColumn.ReadOnly = true;
            this.colIncidentStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatus.Width = 66;
            // 
            // colIncidentStatusDescription
            // 
            this.colIncidentStatusDescription.Caption = "Status";
            this.colIncidentStatusDescription.FieldName = "IncidentStatusDescription";
            this.colIncidentStatusDescription.Name = "colIncidentStatusDescription";
            this.colIncidentStatusDescription.OptionsColumn.AllowEdit = false;
            this.colIncidentStatusDescription.OptionsColumn.AllowFocus = false;
            this.colIncidentStatusDescription.OptionsColumn.ReadOnly = true;
            this.colIncidentStatusDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatusDescription.Visible = true;
            this.colIncidentStatusDescription.VisibleIndex = 3;
            this.colIncidentStatusDescription.Width = 127;
            // 
            // colintX
            // 
            this.colintX.Caption = "X";
            this.colintX.FieldName = "intX";
            this.colintX.Name = "colintX";
            this.colintX.OptionsColumn.AllowEdit = false;
            this.colintX.OptionsColumn.AllowFocus = false;
            this.colintX.OptionsColumn.ReadOnly = true;
            this.colintX.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintX.Width = 27;
            // 
            // colintY
            // 
            this.colintY.Caption = "Y";
            this.colintY.FieldName = "intY";
            this.colintY.Name = "colintY";
            this.colintY.OptionsColumn.AllowEdit = false;
            this.colintY.OptionsColumn.AllowFocus = false;
            this.colintY.OptionsColumn.ReadOnly = true;
            this.colintY.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintY.Width = 27;
            // 
            // colReportedBySurname
            // 
            this.colReportedBySurname.Caption = "Recorded By Surname";
            this.colReportedBySurname.FieldName = "ReportedBySurname";
            this.colReportedBySurname.Name = "colReportedBySurname";
            this.colReportedBySurname.OptionsColumn.AllowEdit = false;
            this.colReportedBySurname.OptionsColumn.AllowFocus = false;
            this.colReportedBySurname.OptionsColumn.ReadOnly = true;
            this.colReportedBySurname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportedBySurname.Visible = true;
            this.colReportedBySurname.VisibleIndex = 8;
            this.colReportedBySurname.Width = 127;
            // 
            // colReportedByForename
            // 
            this.colReportedByForename.Caption = "Recorded By Forename";
            this.colReportedByForename.FieldName = "ReportedByForename";
            this.colReportedByForename.Name = "colReportedByForename";
            this.colReportedByForename.OptionsColumn.AllowEdit = false;
            this.colReportedByForename.OptionsColumn.AllowFocus = false;
            this.colReportedByForename.OptionsColumn.ReadOnly = true;
            this.colReportedByForename.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportedByForename.Visible = true;
            this.colReportedByForename.VisibleIndex = 9;
            this.colReportedByForename.Width = 133;
            // 
            // colReportedByAddressLine1
            // 
            this.colReportedByAddressLine1.Caption = "Reported By Address Line 1";
            this.colReportedByAddressLine1.FieldName = "ReportedByAddressLine1";
            this.colReportedByAddressLine1.Name = "colReportedByAddressLine1";
            this.colReportedByAddressLine1.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine1.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine1.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportedByAddressLine1.Visible = true;
            this.colReportedByAddressLine1.VisibleIndex = 10;
            this.colReportedByAddressLine1.Width = 154;
            // 
            // colReportedByAddressLine2
            // 
            this.colReportedByAddressLine2.Caption = "Reported By Address Line 2";
            this.colReportedByAddressLine2.FieldName = "ReportedByAddressLine2";
            this.colReportedByAddressLine2.Name = "colReportedByAddressLine2";
            this.colReportedByAddressLine2.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine2.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine2.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportedByAddressLine2.Width = 154;
            // 
            // colReportedByAddressLine3
            // 
            this.colReportedByAddressLine3.Caption = "Reported By Address Line 3";
            this.colReportedByAddressLine3.FieldName = "ReportedByAddressLine3";
            this.colReportedByAddressLine3.Name = "colReportedByAddressLine3";
            this.colReportedByAddressLine3.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine3.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine3.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportedByAddressLine3.Width = 154;
            // 
            // colReportedByAddressLine4
            // 
            this.colReportedByAddressLine4.Caption = "Reported By Address Line 4";
            this.colReportedByAddressLine4.FieldName = "ReportedByAddressLine4";
            this.colReportedByAddressLine4.Name = "colReportedByAddressLine4";
            this.colReportedByAddressLine4.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine4.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine4.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportedByAddressLine4.Width = 154;
            // 
            // colReportedByAddressLine5
            // 
            this.colReportedByAddressLine5.Caption = "Reported By Address Line 5";
            this.colReportedByAddressLine5.FieldName = "ReportedByAddressLine5";
            this.colReportedByAddressLine5.Name = "colReportedByAddressLine5";
            this.colReportedByAddressLine5.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine5.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine5.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportedByAddressLine5.Width = 154;
            // 
            // colReportedByPostcode
            // 
            this.colReportedByPostcode.Caption = "Reported By Postcode";
            this.colReportedByPostcode.FieldName = "ReportedByPostcode";
            this.colReportedByPostcode.Name = "colReportedByPostcode";
            this.colReportedByPostcode.OptionsColumn.AllowEdit = false;
            this.colReportedByPostcode.OptionsColumn.AllowFocus = false;
            this.colReportedByPostcode.OptionsColumn.ReadOnly = true;
            this.colReportedByPostcode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportedByPostcode.Visible = true;
            this.colReportedByPostcode.VisibleIndex = 11;
            this.colReportedByPostcode.Width = 128;
            // 
            // colReportedByTelephone1
            // 
            this.colReportedByTelephone1.Caption = "Reported By Telephone 1";
            this.colReportedByTelephone1.FieldName = "ReportedByTelephone1";
            this.colReportedByTelephone1.Name = "colReportedByTelephone1";
            this.colReportedByTelephone1.OptionsColumn.AllowEdit = false;
            this.colReportedByTelephone1.OptionsColumn.AllowFocus = false;
            this.colReportedByTelephone1.OptionsColumn.ReadOnly = true;
            this.colReportedByTelephone1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportedByTelephone1.Visible = true;
            this.colReportedByTelephone1.VisibleIndex = 12;
            this.colReportedByTelephone1.Width = 143;
            // 
            // colIncidentAddressLine1
            // 
            this.colIncidentAddressLine1.Caption = "Incident Address Line 1";
            this.colIncidentAddressLine1.FieldName = "IncidentAddressLine1";
            this.colIncidentAddressLine1.Name = "colIncidentAddressLine1";
            this.colIncidentAddressLine1.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine1.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine1.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentAddressLine1.Visible = true;
            this.colIncidentAddressLine1.VisibleIndex = 13;
            this.colIncidentAddressLine1.Width = 133;
            // 
            // colIncidentAddressLine2
            // 
            this.colIncidentAddressLine2.Caption = "Incident Address Line 2";
            this.colIncidentAddressLine2.FieldName = "IncidentAddressLine2";
            this.colIncidentAddressLine2.Name = "colIncidentAddressLine2";
            this.colIncidentAddressLine2.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine2.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine2.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentAddressLine2.Width = 133;
            // 
            // colIncidentAddressLine3
            // 
            this.colIncidentAddressLine3.Caption = "Incident Address Line 3";
            this.colIncidentAddressLine3.FieldName = "IncidentAddressLine3";
            this.colIncidentAddressLine3.Name = "colIncidentAddressLine3";
            this.colIncidentAddressLine3.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine3.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine3.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentAddressLine3.Width = 133;
            // 
            // colIncidentAddressLine4
            // 
            this.colIncidentAddressLine4.Caption = "Incident Address Line 4";
            this.colIncidentAddressLine4.FieldName = "IncidentAddressLine4";
            this.colIncidentAddressLine4.Name = "colIncidentAddressLine4";
            this.colIncidentAddressLine4.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine4.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine4.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentAddressLine4.Width = 133;
            // 
            // colIncidentAddressLine5
            // 
            this.colIncidentAddressLine5.Caption = "Incident Address Line 5";
            this.colIncidentAddressLine5.FieldName = "IncidentAddressLine5";
            this.colIncidentAddressLine5.Name = "colIncidentAddressLine5";
            this.colIncidentAddressLine5.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine5.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine5.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentAddressLine5.Width = 133;
            // 
            // colIncidentPostcode
            // 
            this.colIncidentPostcode.Caption = "Incident Postcode";
            this.colIncidentPostcode.FieldName = "IncidentPostcode";
            this.colIncidentPostcode.Name = "colIncidentPostcode";
            this.colIncidentPostcode.OptionsColumn.AllowEdit = false;
            this.colIncidentPostcode.OptionsColumn.AllowFocus = false;
            this.colIncidentPostcode.OptionsColumn.ReadOnly = true;
            this.colIncidentPostcode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentPostcode.Visible = true;
            this.colIncidentPostcode.VisibleIndex = 14;
            this.colIncidentPostcode.Width = 107;
            // 
            // colUserPickList1
            // 
            this.colUserPickList1.Caption = "User Picklist 1";
            this.colUserPickList1.FieldName = "UserPickList1";
            this.colUserPickList1.Name = "colUserPickList1";
            this.colUserPickList1.OptionsColumn.AllowEdit = false;
            this.colUserPickList1.OptionsColumn.AllowFocus = false;
            this.colUserPickList1.OptionsColumn.ReadOnly = true;
            this.colUserPickList1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickList1.Visible = true;
            this.colUserPickList1.VisibleIndex = 17;
            this.colUserPickList1.Width = 87;
            // 
            // colUserPickList2
            // 
            this.colUserPickList2.Caption = "User Picklist 2";
            this.colUserPickList2.FieldName = "UserPickList2";
            this.colUserPickList2.Name = "colUserPickList2";
            this.colUserPickList2.OptionsColumn.AllowEdit = false;
            this.colUserPickList2.OptionsColumn.AllowFocus = false;
            this.colUserPickList2.OptionsColumn.ReadOnly = true;
            this.colUserPickList2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickList2.Visible = true;
            this.colUserPickList2.VisibleIndex = 18;
            this.colUserPickList2.Width = 87;
            // 
            // colUserPickList3
            // 
            this.colUserPickList3.Caption = "User Picklist 3";
            this.colUserPickList3.FieldName = "UserPickList3";
            this.colUserPickList3.Name = "colUserPickList3";
            this.colUserPickList3.OptionsColumn.AllowEdit = false;
            this.colUserPickList3.OptionsColumn.AllowFocus = false;
            this.colUserPickList3.OptionsColumn.ReadOnly = true;
            this.colUserPickList3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickList3.Visible = true;
            this.colUserPickList3.VisibleIndex = 19;
            this.colUserPickList3.Width = 87;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientID.Width = 68;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 20;
            this.colClientName.Width = 103;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 21;
            this.colRemarks.Width = 62;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colLinkedInspectionCount
            // 
            this.colLinkedInspectionCount.Caption = "Linked Inspections";
            this.colLinkedInspectionCount.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colLinkedInspectionCount.FieldName = "LinkedInspectionCount";
            this.colLinkedInspectionCount.Name = "colLinkedInspectionCount";
            this.colLinkedInspectionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedInspectionCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedInspectionCount.Visible = true;
            this.colLinkedInspectionCount.VisibleIndex = 15;
            this.colLinkedInspectionCount.Width = 109;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Linked Actions";
            this.colLinkedActionCount.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 16;
            this.colLinkedActionCount.Width = 89;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // sp01411_AT_Incident_ManagerTableAdapter
            // 
            this.sp01411_AT_Incident_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter
            // 
            this.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_Mail_Merge_Select_Data_For_Merge_Incidents
            // 
            this.AcceptButton = this.btnOK;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(607, 421);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_Mail_Merge_Select_Data_For_Merge_Incidents";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mail Merge - Select Data - Incidents";
            this.Load += new System.EventHandler(this.frm_Mail_Merge_Select_Data_For_Merge_Incidents_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowCompleted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyHeaderGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00232CompanyHeaderDropDownListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01411ATIncidentManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Incidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditShowCompleted;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.SimpleButton btnLoadIncidents;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentType;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceID;
        private DevExpress.XtraGrid.Columns.GridColumn colBriefDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonRecordedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonRecordedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonResponsibleID;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonResponsible;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colTargetDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDateClosed;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colintX;
        private DevExpress.XtraGrid.Columns.GridColumn colintY;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedBySurname;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByForename;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickList1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickList2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickList3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedInspectionCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp01411ATIncidentManagerBindingSource;
        private DataSet_AT_Incidents dataSet_AT_Incidents;
        private WoodPlan5.DataSet_AT_IncidentsTableAdapters.sp01411_AT_Incident_ManagerTableAdapter sp01411_AT_Incident_ManagerTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GridLookUpEdit CompanyHeaderGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private System.Windows.Forms.BindingSource sp00232CompanyHeaderDropDownListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colintHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colstrDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrSlogan;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

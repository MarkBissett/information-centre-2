using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Legend : BaseObjects.frmBase
    {
        public frm_AT_Mapping_Legend()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Legend_Load(object sender, EventArgs e)
        {
            this.ResizeRedraw = true;  // Force form to redraw itself when it's size changes //

            Bitmap B = new Bitmap(150, 500);

            using (Graphics G = Graphics.FromImage(B)) 
            {
                Brush brush = new HatchBrush(HatchStyle.OutlinedDiamond, Color.Red, Color.Yellow);
                DrawShape(G, "Rectangle", 50, 300, Color.Blue, brush);
                DrawShape(G, "Circle", 50, 320, Color.Blue, brush);
                DrawShape(G, "Star", 50, 340, Color.Blue, brush);
                DrawShape(G, "Diamond", 50, 360, Color.Blue, brush);
                DrawShape(G, "Triangle1", 50, 380, Color.Blue, brush);
                DrawShape(G, "Triangle2", 50, 400, Color.Blue, brush);
                DrawShape(G, "Polygon", 50, 420, Color.Blue, brush);
                DrawShape(G, "Line", 50, 440, Color.Blue, brush);
                brush.Dispose();
               
                Brush textBrush = new SolidBrush(Color.Black);
                DrawText(G, "Description", 50, 460, textBrush);
                textBrush.Dispose();
            }
            pictureBox1.Image = B;

        }

    //    private void frm_AT_Mapping_Legend_Paint(object sender, PaintEventArgs e)
        private void frm_AT_Mapping_Legend_Paint(object sender, PaintEventArgs e)
        {
            //base.OnPaint(e);
            Graphics G = e.Graphics;

            Pen drawingPen = new Pen(Color.Red, 15);
            G.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            G.DrawEllipse(drawingPen, new Rectangle(new Point(0, 0), this.ClientSize));
            drawingPen.Dispose();

            GraphicsPath star = Star(110, 110, 40, 100, 5, 270);
            G.SmoothingMode = SmoothingMode.AntiAlias;
            //e.Graphics.FillPath(Brushes.Black, star);
            G.FillPath(new HatchBrush(HatchStyle.ForwardDiagonal, Color.Blue, Color.Yellow), star);

            G.DrawPolygon(Pens.Orange, new Point[] { new Point(300,
                                           100), new Point(400, 300), new Point(300,
                                            500), new Point(200, 300) });

            Brush brush = new HatchBrush(HatchStyle.OutlinedDiamond, Color.Red, Color.Yellow);
            DrawShape(G, "Rectangle", 50, 300, Color.Blue, brush);
            DrawShape(G, "Circle", 50, 320, Color.Blue, brush);
            DrawShape(G, "Star", 50, 340, Color.Blue, brush);
            DrawShape(G, "Diamond", 50, 360, Color.Blue, brush);
            DrawShape(G, "Triangle1", 50, 380, Color.Blue, brush);
            DrawShape(G, "Triangle2", 50, 400, Color.Blue, brush);
            DrawShape(G, "Polygon", 50, 420, Color.Blue, brush);
            DrawShape(G, "Line", 50, 440, Color.Blue, brush);



            DrawShape(G, "Rectangle", 100, 200, Color.Blue, brush);
            DrawShape(G, "Circle", 120, 200, Color.Blue, brush);
            DrawShape(G, "Star", 140, 200, Color.Blue, brush);
            DrawShape(G, "Diamond", 160, 200, Color.Blue, brush);
            DrawShape(G, "Triangle1", 180, 200, Color.Blue, brush);
            DrawShape(G, "Triangle2", 200, 200, Color.Blue, brush);
            DrawShape(G, "Polygon", 220, 200, Color.Blue, brush);
            DrawShape(G, "Line", 240, 200, Color.Blue, brush);
            brush.Dispose();

            Brush textBrush = new SolidBrush(Color.Black);
            DrawText(G, "Description", 260, 200, textBrush);
            textBrush.Dispose();
        }

        private void DrawShape(Graphics G, string strShapeName, int intStartX, int intStartY, Color BorderColor, Brush BrushStyle)
        {
            G.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Pen pen = new Pen(BorderColor, 1);
            pen.Alignment = PenAlignment.Inset;
            switch (strShapeName)
	        {
                case "Rectangle":
                    intStartX += 2;
                    intStartY += 2;
                    Rectangle rec = new Rectangle(intStartX, intStartY, 16, 16);
                    G.DrawRectangle(pen, rec);
                    G.FillRectangle(BrushStyle, rec);
                    break;
                case "Diamond":
                    //Point[] myPointArray = { new Point(0, 0), new Point(50, 30), new Point(30, 60) };
                    //new Point[] {new Point(300, 100), new Point(400, 300), new Point(300, 500), new Point(200, 300) })
                    Point[] myPointArray = { new Point(intStartX, intStartY + 10), new Point(intStartX + 10, intStartY + 20), new Point(intStartX + 20, intStartY + 10), new Point(intStartX + 10, intStartY) };
                    G.FillPolygon(BrushStyle, myPointArray);
                    G.DrawPolygon(pen, myPointArray);
                    break;
                case "Star":
                    intStartX += 10;
                    intStartY += 10;
                    GraphicsPath star = Star(intStartX, intStartY, 4, 11, 5, 270);
                    G.FillPath(BrushStyle, star);
                    G.DrawPath(pen, star);
                    break;
                case "Circle":
                    intStartX += 2;
                    intStartY += 2;
                    Rectangle rec2 = new Rectangle(intStartX, intStartY, 16, 16);
                    G.FillEllipse(BrushStyle, rec2);
                    G.DrawEllipse(pen, rec2);
                    break;
                case "Triangle1":
                    intStartY += 5;
                    Point[] myPointArray2 = { new Point(intStartX, intStartY + 10), new Point(intStartX + 10, intStartY), new Point(intStartX + 20, intStartY + 10) };
                    G.FillPolygon(BrushStyle, myPointArray2);
                    G.DrawPolygon(pen, myPointArray2);
                    break;
                case "Triangle2":
                    intStartY += 5;
                    Point[] myPointArray3 = { new Point(intStartX, intStartY), new Point(intStartX + 10, intStartY + 10), new Point(intStartX + 20, intStartY) };
                    G.FillPolygon(BrushStyle, myPointArray3);
                    G.DrawPolygon(pen, myPointArray3);
                    break;
                case "Polygon":
                    Point[] myPointArray4 = {   new Point(intStartX, intStartY + 7), 
                                                new Point(intStartX, intStartY + 19), 
                                                new Point(intStartX + 20, intStartY + 19), 
                                                new Point(intStartX + 20, intStartY + 11), 
                                                new Point(intStartX + 10, intStartY + 1), 
                                                new Point(intStartX, intStartY + 7) };
                    G.FillPolygon(BrushStyle, myPointArray4);
                    G.DrawPolygon(pen, myPointArray4);
                    break;
                case "Line":
                    G.DrawLine(pen, new Point(intStartX, intStartY + 9), new Point(intStartX + 20, intStartY + 9));
                    break;
		        default:
                    break;
	        }
            pen.Dispose();
        }

        private void DrawText(Graphics G, string strText,  int intStartX, int intStartY, Brush BrushStyle)
        {
            G.DrawString(strText, new Font(FontFamily.GenericSerif, 9), BrushStyle, (float)intStartX, (float)intStartY);
        }

        private const double DEGREE = 0.017453292519943295769236907684886;  // Number of radians in one degree //
        public static GraphicsPath Star(int c_x, int c_y, int inner_radius, int radius, int n, int rot)
        {
            rot %= 360;
            Point[] points = new Point[n * 2];
            byte[] types = new byte[n * 2];
            int index = 0;
            for (int a = rot; a < 360 + rot; a += 180 / n)
            {
                if (index >= points.Length) break;
                points[index] = new Point(
                                            c_x + (int)(((index % 2 == 0) ? radius : inner_radius) * Math.Cos(DEGREE * a)),
                                            c_y + (int)(((index % 2 == 0) ? radius : inner_radius) * Math.Sin(DEGREE * a))
                                            );
                types[index] = (byte)PathPointType.Line;
                index++;
            }
            types[0] = (byte)PathPointType.Start; 
            return new GraphicsPath(points, types);
        }

     

    }
}


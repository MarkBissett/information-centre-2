namespace WoodPlan5
{
    partial class frm_AT_Utilities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Utilities));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditSiteFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.btnLoadTrees = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01200TreeListALLBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHighlight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContext = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroundType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProtectionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteHazardClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgeClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaHa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReplantCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGardenSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPropertyProximity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSituation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedInspectionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetentionCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickList1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickList2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickList3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCavat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCrownNorth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownEast = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownSouth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownWest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSULE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesVariety = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnRiskFormula = new DevExpress.XtraEditors.SimpleButton();
            this.btnRiskCalculate = new DevExpress.XtraEditors.SimpleButton();
            this.btnRiskClear = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.sp01200_TreeListALLTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01200_TreeListALLTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.bbiChecked = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUnchecked = new DevExpress.XtraBars.BarButtonItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01200TreeListALLBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChecked, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUnchecked)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1200, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 707);
            this.barDockControlBottom.Size = new System.Drawing.Size(1200, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 707);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1200, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 707);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.bbiChecked,
            this.bbiUnchecked});
            this.barManager1.StatusBar = this.bar1;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1200, 707);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1195, 681);
            this.xtraTabPage1.Text = "Risk Factors \\ CAVAT Calculation";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1195, 681);
            this.splitContainerControl1.SplitterPosition = 36;
            this.splitContainerControl1.TabIndex = 17;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.buttonEditSiteFilter);
            this.layoutControl1.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl1.Controls.Add(this.btnLoadTrees);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1195, 36);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // buttonEditSiteFilter
            // 
            this.buttonEditSiteFilter.Location = new System.Drawing.Point(574, 7);
            this.buttonEditSiteFilter.MenuManager = this.barManager1;
            this.buttonEditSiteFilter.Name = "buttonEditSiteFilter";
            this.buttonEditSiteFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Choose Site Filter Screen", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, true)});
            this.buttonEditSiteFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSiteFilter.Size = new System.Drawing.Size(522, 20);
            this.buttonEditSiteFilter.StyleController = this.layoutControl1;
            this.buttonEditSiteFilter.TabIndex = 13;
            this.buttonEditSiteFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSiteFilter_ButtonClick);
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.Location = new System.Drawing.Point(41, 7);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Client Filter Screen", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Filter", "clear", null, true)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(495, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl1;
            this.buttonEditClientFilter.TabIndex = 5;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // btnLoadTrees
            // 
            this.btnLoadTrees.ImageIndex = 5;
            this.btnLoadTrees.ImageList = this.imageCollection1;
            this.btnLoadTrees.Location = new System.Drawing.Point(1100, 7);
            this.btnLoadTrees.Name = "btnLoadTrees";
            this.btnLoadTrees.Size = new System.Drawing.Size(88, 22);
            this.btnLoadTrees.StyleController = this.layoutControl1;
            this.btnLoadTrees.TabIndex = 7;
            this.btnLoadTrees.Text = "Load Trees";
            this.btnLoadTrees.Click += new System.EventHandler(this.btnLoadTrees_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "refresh_16x16");
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1195, 36);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnLoadTrees;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(1093, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(92, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(92, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(92, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonEditClientFilter;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(533, 26);
            this.layoutControlItem1.Text = "Client:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(31, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonEditSiteFilter;
            this.layoutControlItem3.Location = new System.Drawing.Point(533, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(560, 26);
            this.layoutControlItem3.Text = "Site:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(31, 13);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.groupControl2);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1195, 639);
            this.splitContainerControl2.SplitterPosition = 162;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1027, 639);
            this.gridSplitContainer1.TabIndex = 9;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01200TreeListALLBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1027, 639);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01200TreeListALLBindingSource
            // 
            this.sp01200TreeListALLBindingSource.DataMember = "sp01200_TreeListALL";
            this.sp01200TreeListALLBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.ActiveFilterEnabled = false;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientName,
            this.colSiteName,
            this.colTreeID,
            this.colMapID,
            this.colTreeReference,
            this.colXCoordinate,
            this.colYCoordinate,
            this.colPolygonXY,
            this.colSpecies,
            this.colObjectType,
            this.colHighlight,
            this.colGridReference,
            this.colDistance,
            this.colHouseName,
            this.colAccess,
            this.colVisibility,
            this.colContext,
            this.colManagement,
            this.colLegalStatus,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnit,
            this.colNextInspectionDate,
            this.colGroundType,
            this.colDBH,
            this.colDBHRange,
            this.colHeight,
            this.colHeightRange,
            this.colProtectionType,
            this.colCrownDiameter,
            this.colPlantDate,
            this.colGroupNumber,
            this.colRiskCategory,
            this.colSiteHazardClass,
            this.colPlantSize,
            this.colPlantSource,
            this.colPlantMethod,
            this.colPostcode,
            this.colMapLabel,
            this.colStatus,
            this.colSize,
            this.colTarget1,
            this.colTarget2,
            this.colTarget3,
            this.colLastModifiedDate,
            this.colTreeRemarks,
            this.colUser1,
            this.colUser2,
            this.colUser3,
            this.colAgeClass,
            this.colAreaHa,
            this.colReplantCount,
            this.colSurveyDate,
            this.colTreeType,
            this.colHedgeOwner,
            this.colHedgeLength,
            this.colGardenSize,
            this.colPropertyProximity,
            this.colSiteLevel,
            this.colSituation,
            this.colRiskFactor,
            this.colOwnershipName,
            this.colLinkedInspectionCount,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.colRetentionCategory,
            this.colUserPickList1,
            this.colUserPickList2,
            this.colUserPickList3,
            this.colCavat,
            this.colCrownNorth,
            this.colCrownEast,
            this.colCrownSouth,
            this.colCrownWest,
            this.colStemCount,
            this.colSULE,
            this.colClientID2,
            this.colSiteID2,
            this.colSpeciesVariety,
            this.colObjectLength,
            this.colObjectWidth,
            this.colSelected});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.ColumnFilterPopupMaxRecordsCount = 10000;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Width = 129;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteName.Width = 125;
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            this.colTreeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMapID.Visible = true;
            this.colMapID.VisibleIndex = 15;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 0;
            this.colTreeReference.Width = 191;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            this.colXCoordinate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colXCoordinate.Width = 83;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            this.colYCoordinate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colYCoordinate.Width = 83;
            // 
            // colPolygonXY
            // 
            this.colPolygonXY.Caption = "Polygon XY";
            this.colPolygonXY.FieldName = "PolygonXY";
            this.colPolygonXY.Name = "colPolygonXY";
            this.colPolygonXY.OptionsColumn.ReadOnly = true;
            this.colPolygonXY.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpecies
            // 
            this.colSpecies.Caption = "Species";
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.OptionsColumn.AllowEdit = false;
            this.colSpecies.OptionsColumn.AllowFocus = false;
            this.colSpecies.OptionsColumn.ReadOnly = true;
            this.colSpecies.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpecies.Visible = true;
            this.colSpecies.VisibleIndex = 10;
            this.colSpecies.Width = 107;
            // 
            // colObjectType
            // 
            this.colObjectType.Caption = "Object Type";
            this.colObjectType.FieldName = "ObjectType";
            this.colObjectType.Name = "colObjectType";
            this.colObjectType.OptionsColumn.AllowEdit = false;
            this.colObjectType.OptionsColumn.AllowFocus = false;
            this.colObjectType.OptionsColumn.ReadOnly = true;
            this.colObjectType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colObjectType.Visible = true;
            this.colObjectType.VisibleIndex = 12;
            this.colObjectType.Width = 80;
            // 
            // colHighlight
            // 
            this.colHighlight.Caption = "Highlight";
            this.colHighlight.FieldName = "Highlight";
            this.colHighlight.Name = "colHighlight";
            this.colHighlight.OptionsColumn.AllowEdit = false;
            this.colHighlight.OptionsColumn.AllowFocus = false;
            this.colHighlight.OptionsColumn.ReadOnly = true;
            this.colHighlight.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colGridReference
            // 
            this.colGridReference.Caption = "Grid Reference";
            this.colGridReference.FieldName = "GridReference";
            this.colGridReference.Name = "colGridReference";
            this.colGridReference.OptionsColumn.AllowEdit = false;
            this.colGridReference.OptionsColumn.AllowFocus = false;
            this.colGridReference.OptionsColumn.ReadOnly = true;
            this.colGridReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colDistance
            // 
            this.colDistance.Caption = "Distance";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colHouseName
            // 
            this.colHouseName.Caption = "Nearest House Name";
            this.colHouseName.FieldName = "HouseName";
            this.colHouseName.Name = "colHouseName";
            this.colHouseName.OptionsColumn.AllowEdit = false;
            this.colHouseName.OptionsColumn.AllowFocus = false;
            this.colHouseName.OptionsColumn.ReadOnly = true;
            this.colHouseName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHouseName.Visible = true;
            this.colHouseName.VisibleIndex = 3;
            this.colHouseName.Width = 122;
            // 
            // colAccess
            // 
            this.colAccess.Caption = "Access";
            this.colAccess.FieldName = "Access";
            this.colAccess.Name = "colAccess";
            this.colAccess.OptionsColumn.AllowEdit = false;
            this.colAccess.OptionsColumn.AllowFocus = false;
            this.colAccess.OptionsColumn.ReadOnly = true;
            this.colAccess.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colVisibility
            // 
            this.colVisibility.Caption = "Visibility";
            this.colVisibility.FieldName = "Visibility";
            this.colVisibility.Name = "colVisibility";
            this.colVisibility.OptionsColumn.AllowEdit = false;
            this.colVisibility.OptionsColumn.AllowFocus = false;
            this.colVisibility.OptionsColumn.ReadOnly = true;
            this.colVisibility.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colContext
            // 
            this.colContext.Caption = "Context";
            this.colContext.FieldName = "Context";
            this.colContext.Name = "colContext";
            this.colContext.OptionsColumn.AllowEdit = false;
            this.colContext.OptionsColumn.AllowFocus = false;
            this.colContext.OptionsColumn.ReadOnly = true;
            this.colContext.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colManagement
            // 
            this.colManagement.Caption = "Management";
            this.colManagement.FieldName = "Management";
            this.colManagement.Name = "colManagement";
            this.colManagement.OptionsColumn.AllowEdit = false;
            this.colManagement.OptionsColumn.AllowFocus = false;
            this.colManagement.OptionsColumn.ReadOnly = true;
            this.colManagement.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colManagement.Visible = true;
            this.colManagement.VisibleIndex = 13;
            this.colManagement.Width = 83;
            // 
            // colLegalStatus
            // 
            this.colLegalStatus.Caption = "Legal Status";
            this.colLegalStatus.FieldName = "LegalStatus";
            this.colLegalStatus.Name = "colLegalStatus";
            this.colLegalStatus.OptionsColumn.AllowEdit = false;
            this.colLegalStatus.OptionsColumn.AllowFocus = false;
            this.colLegalStatus.OptionsColumn.ReadOnly = true;
            this.colLegalStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalStatus.Visible = true;
            this.colLegalStatus.VisibleIndex = 14;
            this.colLegalStatus.Width = 80;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection Date";
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 17;
            this.colLastInspectionDate.Width = 120;
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Inspection Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionCycle.Visible = true;
            this.colInspectionCycle.VisibleIndex = 18;
            this.colInspectionCycle.Width = 100;
            // 
            // colInspectionUnit
            // 
            this.colInspectionUnit.Caption = "Inspection Unit";
            this.colInspectionUnit.FieldName = "InspectionUnit";
            this.colInspectionUnit.Name = "colInspectionUnit";
            this.colInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colInspectionUnit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionUnit.Visible = true;
            this.colInspectionUnit.VisibleIndex = 19;
            this.colInspectionUnit.Width = 93;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection Date";
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 20;
            this.colNextInspectionDate.Width = 123;
            // 
            // colGroundType
            // 
            this.colGroundType.Caption = "Ground Type";
            this.colGroundType.FieldName = "GroundType";
            this.colGroundType.Name = "colGroundType";
            this.colGroundType.OptionsColumn.AllowEdit = false;
            this.colGroundType.OptionsColumn.AllowFocus = false;
            this.colGroundType.OptionsColumn.ReadOnly = true;
            this.colGroundType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGroundType.Width = 83;
            // 
            // colDBH
            // 
            this.colDBH.Caption = "DBH";
            this.colDBH.FieldName = "DBH";
            this.colDBH.Name = "colDBH";
            this.colDBH.OptionsColumn.AllowEdit = false;
            this.colDBH.OptionsColumn.AllowFocus = false;
            this.colDBH.OptionsColumn.ReadOnly = true;
            this.colDBH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDBH.Visible = true;
            this.colDBH.VisibleIndex = 22;
            // 
            // colDBHRange
            // 
            this.colDBHRange.Caption = "DBH Range";
            this.colDBHRange.FieldName = "DBHRange";
            this.colDBHRange.Name = "colDBHRange";
            this.colDBHRange.OptionsColumn.AllowEdit = false;
            this.colDBHRange.OptionsColumn.AllowFocus = false;
            this.colDBHRange.OptionsColumn.ReadOnly = true;
            this.colDBHRange.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDBHRange.Visible = true;
            this.colDBHRange.VisibleIndex = 23;
            // 
            // colHeight
            // 
            this.colHeight.Caption = "Height";
            this.colHeight.FieldName = "Height";
            this.colHeight.Name = "colHeight";
            this.colHeight.OptionsColumn.AllowEdit = false;
            this.colHeight.OptionsColumn.AllowFocus = false;
            this.colHeight.OptionsColumn.ReadOnly = true;
            this.colHeight.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeight.Visible = true;
            this.colHeight.VisibleIndex = 24;
            // 
            // colHeightRange
            // 
            this.colHeightRange.Caption = "Height Range";
            this.colHeightRange.FieldName = "HeightRange";
            this.colHeightRange.Name = "colHeightRange";
            this.colHeightRange.OptionsColumn.AllowEdit = false;
            this.colHeightRange.OptionsColumn.AllowFocus = false;
            this.colHeightRange.OptionsColumn.ReadOnly = true;
            this.colHeightRange.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeightRange.Visible = true;
            this.colHeightRange.VisibleIndex = 25;
            this.colHeightRange.Width = 86;
            // 
            // colProtectionType
            // 
            this.colProtectionType.Caption = "Protection Type";
            this.colProtectionType.FieldName = "ProtectionType";
            this.colProtectionType.Name = "colProtectionType";
            this.colProtectionType.OptionsColumn.AllowEdit = false;
            this.colProtectionType.OptionsColumn.AllowFocus = false;
            this.colProtectionType.OptionsColumn.ReadOnly = true;
            this.colProtectionType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProtectionType.Visible = true;
            this.colProtectionType.VisibleIndex = 27;
            this.colProtectionType.Width = 97;
            // 
            // colCrownDiameter
            // 
            this.colCrownDiameter.Caption = "Crown Diameter";
            this.colCrownDiameter.FieldName = "CrownDiameter";
            this.colCrownDiameter.Name = "colCrownDiameter";
            this.colCrownDiameter.OptionsColumn.AllowEdit = false;
            this.colCrownDiameter.OptionsColumn.AllowFocus = false;
            this.colCrownDiameter.OptionsColumn.ReadOnly = true;
            this.colCrownDiameter.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCrownDiameter.Visible = true;
            this.colCrownDiameter.VisibleIndex = 26;
            this.colCrownDiameter.Width = 98;
            // 
            // colPlantDate
            // 
            this.colPlantDate.Caption = "Plant Date";
            this.colPlantDate.FieldName = "PlantDate";
            this.colPlantDate.Name = "colPlantDate";
            this.colPlantDate.OptionsColumn.AllowEdit = false;
            this.colPlantDate.OptionsColumn.AllowFocus = false;
            this.colPlantDate.OptionsColumn.ReadOnly = true;
            this.colPlantDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlantDate.Visible = true;
            this.colPlantDate.VisibleIndex = 29;
            // 
            // colGroupNumber
            // 
            this.colGroupNumber.Caption = "Group Number";
            this.colGroupNumber.FieldName = "GroupNumber";
            this.colGroupNumber.Name = "colGroupNumber";
            this.colGroupNumber.OptionsColumn.AllowEdit = false;
            this.colGroupNumber.OptionsColumn.AllowFocus = false;
            this.colGroupNumber.OptionsColumn.ReadOnly = true;
            this.colGroupNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGroupNumber.Visible = true;
            this.colGroupNumber.VisibleIndex = 30;
            this.colGroupNumber.Width = 90;
            // 
            // colRiskCategory
            // 
            this.colRiskCategory.Caption = "Risk Category";
            this.colRiskCategory.FieldName = "RiskCategory";
            this.colRiskCategory.Name = "colRiskCategory";
            this.colRiskCategory.OptionsColumn.AllowEdit = false;
            this.colRiskCategory.OptionsColumn.AllowFocus = false;
            this.colRiskCategory.OptionsColumn.ReadOnly = true;
            this.colRiskCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRiskCategory.Visible = true;
            this.colRiskCategory.VisibleIndex = 6;
            this.colRiskCategory.Width = 88;
            // 
            // colSiteHazardClass
            // 
            this.colSiteHazardClass.Caption = "Site Hazard Class";
            this.colSiteHazardClass.FieldName = "SiteHazardClass";
            this.colSiteHazardClass.Name = "colSiteHazardClass";
            this.colSiteHazardClass.OptionsColumn.AllowEdit = false;
            this.colSiteHazardClass.OptionsColumn.AllowFocus = false;
            this.colSiteHazardClass.OptionsColumn.ReadOnly = true;
            this.colSiteHazardClass.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteHazardClass.Visible = true;
            this.colSiteHazardClass.VisibleIndex = 9;
            this.colSiteHazardClass.Width = 104;
            // 
            // colPlantSize
            // 
            this.colPlantSize.Caption = "Plant Size";
            this.colPlantSize.FieldName = "PlantSize";
            this.colPlantSize.Name = "colPlantSize";
            this.colPlantSize.OptionsColumn.AllowEdit = false;
            this.colPlantSize.OptionsColumn.AllowFocus = false;
            this.colPlantSize.OptionsColumn.ReadOnly = true;
            this.colPlantSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPlantSource
            // 
            this.colPlantSource.Caption = "Plant Source";
            this.colPlantSource.FieldName = "PlantSource";
            this.colPlantSource.Name = "colPlantSource";
            this.colPlantSource.OptionsColumn.AllowEdit = false;
            this.colPlantSource.OptionsColumn.AllowFocus = false;
            this.colPlantSource.OptionsColumn.ReadOnly = true;
            this.colPlantSource.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlantSource.Width = 81;
            // 
            // colPlantMethod
            // 
            this.colPlantMethod.Caption = "Plant Method";
            this.colPlantMethod.FieldName = "PlantMethod";
            this.colPlantMethod.Name = "colPlantMethod";
            this.colPlantMethod.OptionsColumn.AllowEdit = false;
            this.colPlantMethod.OptionsColumn.AllowFocus = false;
            this.colPlantMethod.OptionsColumn.ReadOnly = true;
            this.colPlantMethod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlantMethod.Width = 84;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 4;
            // 
            // colMapLabel
            // 
            this.colMapLabel.Caption = "Map Label";
            this.colMapLabel.FieldName = "MapLabel";
            this.colMapLabel.Name = "colMapLabel";
            this.colMapLabel.OptionsColumn.AllowEdit = false;
            this.colMapLabel.OptionsColumn.AllowFocus = false;
            this.colMapLabel.OptionsColumn.ReadOnly = true;
            this.colMapLabel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 1;
            // 
            // colSize
            // 
            this.colSize.Caption = "Size";
            this.colSize.FieldName = "Size";
            this.colSize.Name = "colSize";
            this.colSize.OptionsColumn.AllowEdit = false;
            this.colSize.OptionsColumn.AllowFocus = false;
            this.colSize.OptionsColumn.ReadOnly = true;
            this.colSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 21;
            // 
            // colTarget1
            // 
            this.colTarget1.Caption = "Target 1";
            this.colTarget1.FieldName = "Target1";
            this.colTarget1.Name = "colTarget1";
            this.colTarget1.OptionsColumn.AllowEdit = false;
            this.colTarget1.OptionsColumn.AllowFocus = false;
            this.colTarget1.OptionsColumn.ReadOnly = true;
            this.colTarget1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTarget1.Visible = true;
            this.colTarget1.VisibleIndex = 31;
            // 
            // colTarget2
            // 
            this.colTarget2.Caption = "Target 2";
            this.colTarget2.FieldName = "Target2";
            this.colTarget2.Name = "colTarget2";
            this.colTarget2.OptionsColumn.AllowEdit = false;
            this.colTarget2.OptionsColumn.AllowFocus = false;
            this.colTarget2.OptionsColumn.ReadOnly = true;
            this.colTarget2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTarget2.Visible = true;
            this.colTarget2.VisibleIndex = 32;
            // 
            // colTarget3
            // 
            this.colTarget3.Caption = "Target 3";
            this.colTarget3.FieldName = "Target3";
            this.colTarget3.Name = "colTarget3";
            this.colTarget3.OptionsColumn.AllowEdit = false;
            this.colTarget3.OptionsColumn.AllowFocus = false;
            this.colTarget3.OptionsColumn.ReadOnly = true;
            this.colTarget3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTarget3.Visible = true;
            this.colTarget3.VisibleIndex = 33;
            // 
            // colLastModifiedDate
            // 
            this.colLastModifiedDate.Caption = "Last Modified Date";
            this.colLastModifiedDate.FieldName = "LastModifiedDate";
            this.colLastModifiedDate.Name = "colLastModifiedDate";
            this.colLastModifiedDate.OptionsColumn.AllowEdit = false;
            this.colLastModifiedDate.OptionsColumn.AllowFocus = false;
            this.colLastModifiedDate.OptionsColumn.ReadOnly = true;
            this.colLastModifiedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastModifiedDate.Width = 110;
            // 
            // colTreeRemarks
            // 
            this.colTreeRemarks.Caption = "Tree Remarks";
            this.colTreeRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colTreeRemarks.FieldName = "TreeRemarks";
            this.colTreeRemarks.Name = "colTreeRemarks";
            this.colTreeRemarks.OptionsColumn.ReadOnly = true;
            this.colTreeRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeRemarks.Visible = true;
            this.colTreeRemarks.VisibleIndex = 34;
            this.colTreeRemarks.Width = 87;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colUser1
            // 
            this.colUser1.Caption = "User Defined 1";
            this.colUser1.FieldName = "User1";
            this.colUser1.Name = "colUser1";
            this.colUser1.OptionsColumn.AllowEdit = false;
            this.colUser1.OptionsColumn.AllowFocus = false;
            this.colUser1.OptionsColumn.ReadOnly = true;
            this.colUser1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUser1.Visible = true;
            this.colUser1.VisibleIndex = 35;
            this.colUser1.Width = 92;
            // 
            // colUser2
            // 
            this.colUser2.Caption = "User Defined 2";
            this.colUser2.FieldName = "User2";
            this.colUser2.Name = "colUser2";
            this.colUser2.OptionsColumn.AllowEdit = false;
            this.colUser2.OptionsColumn.AllowFocus = false;
            this.colUser2.OptionsColumn.ReadOnly = true;
            this.colUser2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUser2.Visible = true;
            this.colUser2.VisibleIndex = 36;
            this.colUser2.Width = 105;
            // 
            // colUser3
            // 
            this.colUser3.Caption = "User Defined 3";
            this.colUser3.FieldName = "User3";
            this.colUser3.Name = "colUser3";
            this.colUser3.OptionsColumn.AllowEdit = false;
            this.colUser3.OptionsColumn.AllowFocus = false;
            this.colUser3.OptionsColumn.ReadOnly = true;
            this.colUser3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUser3.Visible = true;
            this.colUser3.VisibleIndex = 37;
            this.colUser3.Width = 92;
            // 
            // colAgeClass
            // 
            this.colAgeClass.Caption = "Age Class";
            this.colAgeClass.FieldName = "AgeClass";
            this.colAgeClass.Name = "colAgeClass";
            this.colAgeClass.OptionsColumn.AllowEdit = false;
            this.colAgeClass.OptionsColumn.AllowFocus = false;
            this.colAgeClass.OptionsColumn.ReadOnly = true;
            this.colAgeClass.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAgeClass.Visible = true;
            this.colAgeClass.VisibleIndex = 38;
            // 
            // colAreaHa
            // 
            this.colAreaHa.Caption = "Area (M�)";
            this.colAreaHa.FieldName = "AreaHa";
            this.colAreaHa.Name = "colAreaHa";
            this.colAreaHa.OptionsColumn.AllowEdit = false;
            this.colAreaHa.OptionsColumn.AllowFocus = false;
            this.colAreaHa.OptionsColumn.ReadOnly = true;
            this.colAreaHa.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAreaHa.Visible = true;
            this.colAreaHa.VisibleIndex = 39;
            // 
            // colReplantCount
            // 
            this.colReplantCount.Caption = "Replant Count";
            this.colReplantCount.FieldName = "ReplantCount";
            this.colReplantCount.Name = "colReplantCount";
            this.colReplantCount.OptionsColumn.AllowEdit = false;
            this.colReplantCount.OptionsColumn.AllowFocus = false;
            this.colReplantCount.OptionsColumn.ReadOnly = true;
            this.colReplantCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReplantCount.Width = 90;
            // 
            // colSurveyDate
            // 
            this.colSurveyDate.Caption = "Survey Date";
            this.colSurveyDate.FieldName = "SurveyDate";
            this.colSurveyDate.Name = "colSurveyDate";
            this.colSurveyDate.OptionsColumn.AllowEdit = false;
            this.colSurveyDate.OptionsColumn.AllowFocus = false;
            this.colSurveyDate.OptionsColumn.ReadOnly = true;
            this.colSurveyDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyDate.Width = 81;
            // 
            // colTreeType
            // 
            this.colTreeType.Caption = "Tree Type";
            this.colTreeType.FieldName = "TreeType";
            this.colTreeType.Name = "colTreeType";
            this.colTreeType.OptionsColumn.AllowEdit = false;
            this.colTreeType.OptionsColumn.AllowFocus = false;
            this.colTreeType.OptionsColumn.ReadOnly = true;
            this.colTreeType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeType.Visible = true;
            this.colTreeType.VisibleIndex = 40;
            // 
            // colHedgeOwner
            // 
            this.colHedgeOwner.Caption = "Hedge Owner";
            this.colHedgeOwner.FieldName = "HedgeOwner";
            this.colHedgeOwner.Name = "colHedgeOwner";
            this.colHedgeOwner.OptionsColumn.AllowEdit = false;
            this.colHedgeOwner.OptionsColumn.AllowFocus = false;
            this.colHedgeOwner.OptionsColumn.ReadOnly = true;
            this.colHedgeOwner.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHedgeOwner.Width = 87;
            // 
            // colHedgeLength
            // 
            this.colHedgeLength.Caption = "Hedge Length";
            this.colHedgeLength.FieldName = "HedgeLength";
            this.colHedgeLength.Name = "colHedgeLength";
            this.colHedgeLength.OptionsColumn.AllowEdit = false;
            this.colHedgeLength.OptionsColumn.AllowFocus = false;
            this.colHedgeLength.OptionsColumn.ReadOnly = true;
            this.colHedgeLength.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHedgeLength.Width = 88;
            // 
            // colGardenSize
            // 
            this.colGardenSize.Caption = "Garden Size";
            this.colGardenSize.FieldName = "GardenSize";
            this.colGardenSize.Name = "colGardenSize";
            this.colGardenSize.OptionsColumn.AllowEdit = false;
            this.colGardenSize.OptionsColumn.AllowFocus = false;
            this.colGardenSize.OptionsColumn.ReadOnly = true;
            this.colGardenSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGardenSize.Width = 78;
            // 
            // colPropertyProximity
            // 
            this.colPropertyProximity.Caption = "Property Proximity";
            this.colPropertyProximity.FieldName = "PropertyProximity";
            this.colPropertyProximity.Name = "colPropertyProximity";
            this.colPropertyProximity.OptionsColumn.AllowEdit = false;
            this.colPropertyProximity.OptionsColumn.AllowFocus = false;
            this.colPropertyProximity.OptionsColumn.ReadOnly = true;
            this.colPropertyProximity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPropertyProximity.Width = 110;
            // 
            // colSiteLevel
            // 
            this.colSiteLevel.Caption = "Site Level";
            this.colSiteLevel.FieldName = "SiteLevel";
            this.colSiteLevel.Name = "colSiteLevel";
            this.colSiteLevel.OptionsColumn.AllowEdit = false;
            this.colSiteLevel.OptionsColumn.AllowFocus = false;
            this.colSiteLevel.OptionsColumn.ReadOnly = true;
            this.colSiteLevel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSituation
            // 
            this.colSituation.Caption = "Situation";
            this.colSituation.FieldName = "Situation";
            this.colSituation.Name = "colSituation";
            this.colSituation.OptionsColumn.ReadOnly = true;
            this.colSituation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRiskFactor
            // 
            this.colRiskFactor.Caption = "Risk Factor";
            this.colRiskFactor.FieldName = "RiskFactor";
            this.colRiskFactor.Name = "colRiskFactor";
            this.colRiskFactor.OptionsColumn.AllowEdit = false;
            this.colRiskFactor.OptionsColumn.AllowFocus = false;
            this.colRiskFactor.OptionsColumn.ReadOnly = true;
            this.colRiskFactor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRiskFactor.Visible = true;
            this.colRiskFactor.VisibleIndex = 5;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.Caption = "Ownership";
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.OptionsColumn.AllowEdit = false;
            this.colOwnershipName.OptionsColumn.AllowFocus = false;
            this.colOwnershipName.OptionsColumn.ReadOnly = true;
            this.colOwnershipName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 2;
            // 
            // colLinkedInspectionCount
            // 
            this.colLinkedInspectionCount.Caption = "Linked Inspection Count";
            this.colLinkedInspectionCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedInspectionCount.FieldName = "LinkedInspectionCount";
            this.colLinkedInspectionCount.Name = "colLinkedInspectionCount";
            this.colLinkedInspectionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedInspectionCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedInspectionCount.Visible = true;
            this.colLinkedInspectionCount.VisibleIndex = 16;
            this.colLinkedInspectionCount.Width = 136;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "<b>Calculated 1</b>";
            this.gridColumn3.FieldName = "Calculated1";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn3.ShowUnboundExpressionMenu = true;
            this.gridColumn3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 41;
            this.gridColumn3.Width = 90;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "<b>Calculated 2</b>";
            this.gridColumn4.FieldName = "Calculated2";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.ShowUnboundExpressionMenu = true;
            this.gridColumn4.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 42;
            this.gridColumn4.Width = 90;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "<b>Calculated 3</b>";
            this.gridColumn5.FieldName = "Calculated3";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.ShowUnboundExpressionMenu = true;
            this.gridColumn5.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 43;
            this.gridColumn5.Width = 90;
            // 
            // colRetentionCategory
            // 
            this.colRetentionCategory.Caption = "Retention Category";
            this.colRetentionCategory.FieldName = "RetentionCategory";
            this.colRetentionCategory.Name = "colRetentionCategory";
            this.colRetentionCategory.OptionsColumn.AllowEdit = false;
            this.colRetentionCategory.OptionsColumn.AllowFocus = false;
            this.colRetentionCategory.OptionsColumn.ReadOnly = true;
            this.colRetentionCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRetentionCategory.Visible = true;
            this.colRetentionCategory.VisibleIndex = 28;
            this.colRetentionCategory.Width = 116;
            // 
            // colUserPickList1
            // 
            this.colUserPickList1.Caption = "User Picklist 1";
            this.colUserPickList1.FieldName = "UserPickList1";
            this.colUserPickList1.Name = "colUserPickList1";
            this.colUserPickList1.OptionsColumn.AllowEdit = false;
            this.colUserPickList1.OptionsColumn.AllowFocus = false;
            this.colUserPickList1.OptionsColumn.ReadOnly = true;
            this.colUserPickList1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickList1.Width = 86;
            // 
            // colUserPickList2
            // 
            this.colUserPickList2.Caption = "User Picklist 2";
            this.colUserPickList2.FieldName = "UserPickList2";
            this.colUserPickList2.Name = "colUserPickList2";
            this.colUserPickList2.OptionsColumn.AllowEdit = false;
            this.colUserPickList2.OptionsColumn.AllowFocus = false;
            this.colUserPickList2.OptionsColumn.ReadOnly = true;
            this.colUserPickList2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickList2.Width = 86;
            // 
            // colUserPickList3
            // 
            this.colUserPickList3.Caption = "User Picklist 3";
            this.colUserPickList3.FieldName = "UserPickList3";
            this.colUserPickList3.Name = "colUserPickList3";
            this.colUserPickList3.OptionsColumn.AllowEdit = false;
            this.colUserPickList3.OptionsColumn.AllowFocus = false;
            this.colUserPickList3.OptionsColumn.ReadOnly = true;
            this.colUserPickList3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickList3.Width = 86;
            // 
            // colCavat
            // 
            this.colCavat.Caption = "CAVAT";
            this.colCavat.ColumnEdit = this.repositoryItemTextEdit1;
            this.colCavat.FieldName = "Cavat";
            this.colCavat.Name = "colCavat";
            this.colCavat.OptionsColumn.AllowEdit = false;
            this.colCavat.OptionsColumn.AllowFocus = false;
            this.colCavat.OptionsColumn.ReadOnly = true;
            this.colCavat.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCavat.Visible = true;
            this.colCavat.VisibleIndex = 8;
            this.colCavat.Width = 65;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colCrownNorth
            // 
            this.colCrownNorth.Caption = "Crown North";
            this.colCrownNorth.FieldName = "CrownNorth";
            this.colCrownNorth.Name = "colCrownNorth";
            this.colCrownNorth.OptionsColumn.AllowEdit = false;
            this.colCrownNorth.OptionsColumn.AllowFocus = false;
            this.colCrownNorth.OptionsColumn.ReadOnly = true;
            this.colCrownNorth.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCrownNorth.Width = 82;
            // 
            // colCrownEast
            // 
            this.colCrownEast.Caption = "Crown East";
            this.colCrownEast.FieldName = "CrownEast";
            this.colCrownEast.Name = "colCrownEast";
            this.colCrownEast.OptionsColumn.AllowEdit = false;
            this.colCrownEast.OptionsColumn.AllowFocus = false;
            this.colCrownEast.OptionsColumn.ReadOnly = true;
            this.colCrownEast.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCrownSouth
            // 
            this.colCrownSouth.Caption = "Crown South";
            this.colCrownSouth.FieldName = "CrownSouth";
            this.colCrownSouth.Name = "colCrownSouth";
            this.colCrownSouth.OptionsColumn.AllowEdit = false;
            this.colCrownSouth.OptionsColumn.AllowFocus = false;
            this.colCrownSouth.OptionsColumn.ReadOnly = true;
            this.colCrownSouth.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCrownSouth.Width = 83;
            // 
            // colCrownWest
            // 
            this.colCrownWest.Caption = "Crown West";
            this.colCrownWest.FieldName = "CrownWest";
            this.colCrownWest.Name = "colCrownWest";
            this.colCrownWest.OptionsColumn.AllowEdit = false;
            this.colCrownWest.OptionsColumn.AllowFocus = false;
            this.colCrownWest.OptionsColumn.ReadOnly = true;
            this.colCrownWest.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCrownWest.Width = 80;
            // 
            // colStemCount
            // 
            this.colStemCount.Caption = "Stem Count";
            this.colStemCount.FieldName = "StemCount";
            this.colStemCount.Name = "colStemCount";
            this.colStemCount.OptionsColumn.AllowEdit = false;
            this.colStemCount.OptionsColumn.AllowFocus = false;
            this.colStemCount.OptionsColumn.ReadOnly = true;
            this.colStemCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStemCount.Width = 77;
            // 
            // colSULE
            // 
            this.colSULE.Caption = "SULE";
            this.colSULE.FieldName = "SULE";
            this.colSULE.Name = "colSULE";
            this.colSULE.OptionsColumn.AllowEdit = false;
            this.colSULE.OptionsColumn.AllowFocus = false;
            this.colSULE.OptionsColumn.ReadOnly = true;
            this.colSULE.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSULE.Visible = true;
            this.colSULE.VisibleIndex = 7;
            this.colSULE.Width = 50;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "SiteID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpeciesVariety
            // 
            this.colSpeciesVariety.Caption = "Species Variety";
            this.colSpeciesVariety.FieldName = "SpeciesVariety";
            this.colSpeciesVariety.Name = "colSpeciesVariety";
            this.colSpeciesVariety.OptionsColumn.AllowEdit = false;
            this.colSpeciesVariety.OptionsColumn.AllowFocus = false;
            this.colSpeciesVariety.OptionsColumn.ReadOnly = true;
            this.colSpeciesVariety.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeciesVariety.Visible = true;
            this.colSpeciesVariety.VisibleIndex = 11;
            this.colSpeciesVariety.Width = 125;
            // 
            // colObjectLength
            // 
            this.colObjectLength.Caption = "Object Length";
            this.colObjectLength.FieldName = "ObjectLength";
            this.colObjectLength.Name = "colObjectLength";
            this.colObjectLength.OptionsColumn.AllowEdit = false;
            this.colObjectLength.OptionsColumn.AllowFocus = false;
            this.colObjectLength.OptionsColumn.ReadOnly = true;
            this.colObjectLength.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colObjectWidth
            // 
            this.colObjectWidth.Caption = "Object Width";
            this.colObjectWidth.FieldName = "ObjectWidth";
            this.colObjectWidth.Name = "colObjectWidth";
            this.colObjectWidth.OptionsColumn.AllowEdit = false;
            this.colObjectWidth.OptionsColumn.AllowFocus = false;
            this.colObjectWidth.OptionsColumn.ReadOnly = true;
            this.colObjectWidth.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSelected.Width = 60;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.btnRiskFormula);
            this.groupControl2.Controls.Add(this.btnRiskCalculate);
            this.groupControl2.Controls.Add(this.btnRiskClear);
            this.groupControl2.Location = new System.Drawing.Point(0, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(161, 111);
            this.groupControl2.TabIndex = 16;
            this.groupControl2.Text = "Risk";
            // 
            // btnRiskFormula
            // 
            this.btnRiskFormula.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRiskFormula.Location = new System.Drawing.Point(5, 83);
            this.btnRiskFormula.Name = "btnRiskFormula";
            this.btnRiskFormula.Size = new System.Drawing.Size(151, 23);
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Set Risk Formula Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to set Formula.\r\n\r\n<b>Current Formula:</b> ";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnRiskFormula.SuperTip = superToolTip1;
            this.btnRiskFormula.TabIndex = 14;
            this.btnRiskFormula.Text = "Set Formula";
            this.btnRiskFormula.Click += new System.EventHandler(this.btnRiskFormula_Click);
            // 
            // btnRiskCalculate
            // 
            this.btnRiskCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRiskCalculate.Location = new System.Drawing.Point(5, 25);
            this.btnRiskCalculate.Name = "btnRiskCalculate";
            this.btnRiskCalculate.Size = new System.Drawing.Size(151, 23);
            this.btnRiskCalculate.TabIndex = 10;
            this.btnRiskCalculate.Text = "Calculate for Selected Trees";
            this.btnRiskCalculate.Click += new System.EventHandler(this.btnRiskCalculate_Click);
            // 
            // btnRiskClear
            // 
            this.btnRiskClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRiskClear.Location = new System.Drawing.Point(5, 54);
            this.btnRiskClear.Name = "btnRiskClear";
            this.btnRiskClear.Size = new System.Drawing.Size(151, 23);
            this.btnRiskClear.TabIndex = 12;
            this.btnRiskClear.Text = "Clear for Selected Trees";
            this.btnRiskClear.Click += new System.EventHandler(this.btnRiskClear_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "StatusBar";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Status Bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Information: Adjust Filters and Click Load Trees. Tick one or more trees to set t" +
    "heir values then click required button.";
            this.barStaticItem1.Id = 26;
            this.barStaticItem1.ImageIndex = 3;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "add_16.png");
            this.imageList1.Images.SetKeyName(1, "edit_16.png");
            this.imageList1.Images.SetKeyName(2, "info_16.png");
            this.imageList1.Images.SetKeyName(3, "attention_16.png");
            this.imageList1.Images.SetKeyName(4, "delete_16.png");
            // 
            // sp01200_TreeListALLTableAdapter
            // 
            this.sp01200_TreeListALLTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // bbiChecked
            // 
            this.bbiChecked.Caption = "Set Checked";
            this.bbiChecked.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiChecked.Glyph")));
            this.bbiChecked.Id = 27;
            this.bbiChecked.Name = "bbiChecked";
            this.bbiChecked.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChecked_ItemClick);
            // 
            // bbiUnchecked
            // 
            this.bbiUnchecked.Caption = "Set Un-checked";
            this.bbiUnchecked.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiUnchecked.Glyph")));
            this.bbiUnchecked.Id = 28;
            this.bbiUnchecked.Name = "bbiUnchecked";
            this.bbiUnchecked.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUnchecked_ItemClick);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_AT_Utilities
            // 
            this.ClientSize = new System.Drawing.Size(1200, 737);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Utilities";
            this.Text = "Amenity Trees - Utilities";
            this.Activated += new System.EventHandler(this.frm_AT_Utilities_Activated);
            this.Load += new System.EventHandler(this.frm_AT_Utilities_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01200TreeListALLBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnLoadTrees;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnRiskFormula;
        private DevExpress.XtraEditors.SimpleButton btnRiskCalculate;
        private DevExpress.XtraEditors.SimpleButton btnRiskClear;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.BindingSource sp01200TreeListALLBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType;
        private DevExpress.XtraGrid.Columns.GridColumn colGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colVisibility;
        private DevExpress.XtraGrid.Columns.GridColumn colContext;
        private DevExpress.XtraGrid.Columns.GridColumn colManagement;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroundType;
        private DevExpress.XtraGrid.Columns.GridColumn colDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colProtectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteHazardClass;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colMapLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget1;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget2;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget3;
        private DevExpress.XtraGrid.Columns.GridColumn colLastModifiedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colAgeClass;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaHa;
        private DevExpress.XtraGrid.Columns.GridColumn colReplantCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeType;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeLength;
        private DevExpress.XtraGrid.Columns.GridColumn colGardenSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPropertyProximity;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSituation;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedInspectionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colRetentionCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickList1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickList2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickList3;
        private DevExpress.XtraGrid.Columns.GridColumn colCavat;
        private DevExpress.XtraGrid.Columns.GridColumn colStemCount;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownNorth;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownSouth;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownEast;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownWest;
        private DevExpress.XtraGrid.Columns.GridColumn colSULE;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesVariety;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectLength;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectWidth;
        private WoodPlan5.DataSet_ATTableAdapters.sp01200_TreeListALLTableAdapter sp01200_TreeListALLTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colHighlight;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiChecked;
        private DevExpress.XtraBars.BarButtonItem bbiUnchecked;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSiteFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

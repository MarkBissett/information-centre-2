using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_AT_Utilities_Risk_Formula : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";

        GridHitInfo downHitInfo = null;

        public string strFormula = "";
        int i_int_FocusedGrid = 1;

        #endregion

        public frm_AT_Utilities_Risk_Formula()
        {
            InitializeComponent();
        }

        private void frm_AT_Utilities_Risk_Formula_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 20151;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp01420_AT_Risk_Factor_Formula_Available_ColumnsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01420_AT_Risk_Factor_Formula_Available_ColumnsTableAdapter.Fill(dataSet_AT.sp01420_AT_Risk_Factor_Formula_Available_Columns);

            sp01424_AT_Formula_Available_OperatorsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01424_AT_Formula_Available_OperatorsTableAdapter.Fill(dataSet_AT1.sp01424_AT_Formula_Available_Operators);

            memoEditFormula.Text = strFormula;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        private void TransferFieldToFormula(GridView view)
        {
            string strText = memoEditFormula.Text.Trim() ?? "";

            if (memoEditFormula.SelectionLength > 0)  // Replace selected text //
            {
                memoEditFormula.SelectedText = view.GetFocusedRowCellValue("strField").ToString();
            }
            else if (memoEditFormula.SelectionStart < memoEditFormula.Text.Length) // Insert at current Position //
            {
                memoEditFormula.SelectedText = view.GetFocusedRowCellValue("strField").ToString() + " ";
            }
            else  // Append to end of text //
            {
                if (strText == "")
                {
                    strText += view.GetFocusedRowCellValue("strField").ToString();
                }
                else
                {
                    strText += " " + view.GetFocusedRowCellValue("strField").ToString();
                }
                memoEditFormula.Text = strText;
            }
        }

        private void bbiTransferField_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TransferFieldToFormula((i_int_FocusedGrid == 1 ? gridView1 : gridView2));
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Formula Fields Available";
                    break;
                case "gridView2":
                    message = "No Functions Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            TransferFieldToFormula(view); // Transfer Field Name to current curser position in Formula Box //
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiTransferField.Enabled = (view.FocusedRowHandle == GridControl.InvalidRowHandle ? false : true);
                pmGrid1Menu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            TransferFieldToFormula(view); // Transfer Field Name to current curser position in Formula Box //
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiTransferField.Enabled = (view.FocusedRowHandle == GridControl.InvalidRowHandle ? false : true);
                pmGrid1Menu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void btnTest_Click(object sender, EventArgs e)
        {
            switch (TestFormula())
            {
                case -2:
                    XtraMessageBox.Show("No formula has been entered.\n\nEnter the formula then try again.", "Test Formula", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    break;
                case -1:
                    XtraMessageBox.Show("The formula entered is Invalid - adjust the formula then try again.", "Test Formula", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                case 0:
                    XtraMessageBox.Show("The formula entered is Valid.", "Test Formula", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }

        private int TestFormula()
        {
            string strFormula = memoEditFormula.Text.Trim() ?? "";
            if (strFormula == "") return -2;  // No Formula //

            // Now Execute it in SQL to see if it returns an error //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter Test = new DataSet_ATTableAdapters.QueriesTableAdapter();
                Test.ChangeConnectionString(strConnectionString);
                Test.sp01422_AT_Risk_Factor_Formula_Test(strFormula);
            }
            catch (Exception)
            {
                return -1;  // Formula Invalid //
            }
            return 0;  // Formula Valid //
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            switch (TestFormula())
            {
                case -2:
                    XtraMessageBox.Show("No formula has been entered.\n\nEnter the formula then try saving again.", "Save Formula", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                case -1:
                    XtraMessageBox.Show("The formula entered is Invalid - adjust the formula then try saving again.", "Save Formula", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                case 0:
                    // Valid Formula so save it //
                    try
                    {
                        WoodPlanDataSetTableAdapters.QueriesTableAdapter UpdateSettings = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                        UpdateSettings.ChangeConnectionString(strConnectionString);
                        UpdateSettings.sp00122_system_setting_update(memoEditFormula.Text.Trim(), "AmenityTreesRiskFactorFormula", 1);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show("An error occurred saving the formula [" + ex.Message + "] - try saving again. If the error persist contact Technical Support.", "Save Formula", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    break;
            }
            strFormula = memoEditFormula.Text.Trim();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

 

    }
}


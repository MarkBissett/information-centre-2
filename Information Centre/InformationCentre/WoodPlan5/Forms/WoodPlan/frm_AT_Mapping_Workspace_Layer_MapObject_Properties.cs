using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Workspace_Layer_MapObject_Properties : BaseObjects.frmBase
    {
        #region Instance Variables      
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intDefaultThematicStyleSet = 0;
        public int intUseThematicStyling = 0;
        public string strPointSize = "Fixed [8]"; 
        
        #endregion

        public frm_AT_Mapping_Workspace_Layer_MapObject_Properties()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Workspace_Layer_MapObject_Properties_Load(object sender, EventArgs e)
        {
            this.FormID = 20043;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked at end of event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            this.sp01265_AT_Tree_Picker_Workspace_Saved_Thematic_SetsTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01265_AT_Tree_Picker_Workspace_Saved_Thematic_SetsTableAdapter.Fill(dataSet_AT_TreePicker.sp01265_AT_Tree_Picker_Workspace_Saved_Thematic_Sets, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);

            glueDefaultThematicStyleSet.EditValue = intDefaultThematicStyleSet;
            ceUseThematicStyling.Checked = (intUseThematicStyling == 1 ? true : false);
            if (strPointSize.StartsWith("Fix"))
            {
                checkEdit2.Checked = true;
            }
            else
            {
                checkEdit3.Checked = true;
            }
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }


        private void glueDefaultThematicStyleSet_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to CLEAR the default Thematic Style Set!\n\nAre you sure you wish to proceed?", "Clear Default Thematic Style Set", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    glueDefaultThematicStyleSet.EditValue = null;
                }
            }

        }


        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
       }

        private void btnOK_Click(object sender, EventArgs e)
        {
            intDefaultThematicStyleSet = Convert.ToInt32(glueDefaultThematicStyleSet.EditValue);
            intUseThematicStyling = (ceUseThematicStyling.Checked ? 1 : 0);
            strPointSize = (checkEdit2.Checked ? "Fixed" : "Dynamic");
            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails any validation tests //
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

    }
}


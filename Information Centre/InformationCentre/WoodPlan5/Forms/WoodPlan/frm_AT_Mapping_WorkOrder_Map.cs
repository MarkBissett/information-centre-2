using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;  // Required for ImageMap Call on Drawing Legend //

using WoodPlan5.Properties;
using BaseObjects;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraRichEdit.API.Native;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_WorkOrder_Map : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        
        public Image imageMap;
        public Image imageBackup;  // used for restoring original image if user click clears drawing button //
        UserRect rect;
        int intRectangleWidth = 0;
        int intRectangleHeight = 0;
        int intPixelsPerInch = 0;
        bool boolFormLoading = true;  /// Prevent Resize of Snapshot box on initial setting of Size Grid edit value //
        string strMapsPath = "";
        public int intMapRecordID = 0;
        public int intWorkOrderID = 0;
        private int intAddControlCount = 0;
        public frm_AT_Mapping_Tree_Picker frmParentMappingForm = null;
        public int intCurrentMapScale = 0;  
        public int intDPI = 0;
        private int intRectangleOffsetX = 30;
        private int intRectangleOffsetY = 30;
        private int intRectanglePadWidth = 37;  // Distance between the left of the outer and inner rectangle //
        private int intRectanglePadHeight = 110;  // Distance between the top of the outer and inner rectangle //

        private Point? _Previous = null;
        private Color _color = Color.Black;
        private Pen _Pen = new Pen(System.Drawing.Color.Red);
        private int _oldX = 0;
        private int _oldY = 0;
        private Size _oldSize = new Size();

        private string _ObjectDrawn = "";
        private bool isMoving = false;
        private Point mouseDownPosition = Point.Empty;
        private Point mouseMovePosition = Point.Empty;
        private Dictionary<Point, Point> Circles = new Dictionary<Point, Point>();
        private bool _boolRectangleHidden = false;
        private int _ArrowHeadSize = 3;

        #endregion

        public frm_AT_Mapping_WorkOrder_Map()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_WorkOrder_Map_Load(object sender, EventArgs e)
        {
            this.FormID = 200413;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            _Pen.Width = (float)5;
            beiPenColour.EditValue = Color.Red;

            if (strFormMode != "View")
            {
                if (intWorkOrderID > 0)
                {
                    this.Text += "  [Link to Work Order: " + intWorkOrderID.ToString().PadLeft(7, '0') + "]";
                }
                else
                {
                    this.Text += "  [No Link to Work Order]";
                }
            }
            else
            {
                // Disable Text boxes and Save button //
                textEdit2.Enabled = false;
                labelControl2.Enabled = false;
                memoEdit1.Enabled = false;
                labelControl3.Enabled = false;
                btnSave.Enabled = false;
            }

            this.pictureBox1.Image = imageMap;
            imageBackup = (Image)imageMap.Clone();
            intPixelsPerInch = intDPI;
            if (strFormMode == "Add" || strFormMode == "View")
            {
                textEdit2.EditValue = "";
                memoEdit1.EditValue = "";

                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    strMapsPath = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderMapLocation")) + "\\";
                    if (strMapsPath == null) strMapsPath = "C:\\";
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred when attempting to get the folder used to hold saved map images [" + ex.Message + "]!", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Close();
                }

                try
                {
                    String strOSRef = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderOSCopyright"));
                    resizableMemoEditBoxOSRef.EditValue = strOSRef;

                    string strDefaultWidth = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderOSCopyrightWidth"));
                    if (string.IsNullOrEmpty(strDefaultWidth) || Convert.ToInt32(strDefaultWidth) <= 0)
                    {

                        // Attempt to size text box to text width - minimum size of 20 //
                        const int width = 20;
                        Font font = new Font(resizableMemoEditBoxOSRef.Font.Name, resizableMemoEditBoxOSRef.Font.Size);
                        Size s = TextRenderer.MeasureText(resizableMemoEditBoxOSRef.Text, font);
                        if (s.Width > width)
                        {
                            resizableMemoEditBoxOSRef.Width = s.Width;
                        }
                    }
                    else resizableMemoEditBoxOSRef.Width = Convert.ToInt32(strDefaultWidth);
                }
                catch
                {
                }


                int intDefaultPageSize = 0;
                sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01300_AT_WorkOrder_Map_Page_Sizes);
                gridLookUpEdit1.ForceInitialize();
                gridLookUpEdit1.ShowPopup();  // Force GridLookupEdit to Load Data //
                gridLookUpEdit1.ClosePopup();
                bool boolSizeFound = false;
                if (gridLookUpEdit1View.DataRowCount > 0)
                {
                    intDefaultPageSize = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderDefaultPaperSize"));
                    if (intDefaultPageSize > 0)
                    {
                        GridLookUpEdit glue = (GridLookUpEdit)gridLookUpEdit1;
                        GridView view = glue.Properties.View;
                        int intFoundRow = 0;
                        intFoundRow = view.LocateByValue(0, view.Columns["PageSizeID"], intDefaultPageSize);
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intFoundRow);
                            intRectangleWidth = Convert.ToInt32((float)Convert.ToInt32(view.GetRowCellValue(intFoundRow, "PageWidth")) / 25.4 * intPixelsPerInch);
                            intRectangleHeight = Convert.ToInt32((float)Convert.ToInt32(view.GetRowCellValue(intFoundRow, "PageHeight")) / 25.4 * intPixelsPerInch);
                            boolSizeFound = true;
                        }
                    }
                }
                if (!boolSizeFound)
                {
                    if (intRectangleWidth <= 0) intRectangleWidth = Convert.ToInt32((float)(210 / 25.4) * (float)intPixelsPerInch);
                    if (intRectangleHeight <= 0) intRectangleHeight = Convert.ToInt32((float)(297 / 25.4) * (float)intPixelsPerInch);
                }
                if (intRectangleWidth <= 0) intRectangleWidth = Convert.ToInt32((float)(210 / 25.4) * (float)intPixelsPerInch);
                if (intRectangleHeight <= 0) intRectangleHeight = Convert.ToInt32((float)(297 / 25.4) * (float)intPixelsPerInch);

                this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;

                // Scroll xtraScrollableControl1 along and down so the view matches the map //
                int intDifferenceX = (imageMap.Width - xtraScrollableControl1.Width) / 2;
                int intDifferenceY = (imageMap.Height - xtraScrollableControl1.Height) / 2;
                if (intDifferenceX > 0 || intDifferenceY > 0) this.xtraScrollableControl1.AutoScrollPosition = new System.Drawing.Point((intDifferenceX > 0 ? intDifferenceX : 0), (intDifferenceY > 0 ? intDifferenceY : 0));

                rect = new UserRect(new Rectangle((intDifferenceX > 0 ? intDifferenceX + intRectangleOffsetX : intRectangleOffsetX), (intDifferenceY > 0 ? intDifferenceY + intRectangleOffsetY : intRectangleOffsetY), intRectangleWidth, intRectangleHeight));
                rect.SetPictureBox(this.pictureBox1);

                // Load into map and position any objects set as visible by default from System_settings table //
                // Legend //
                string strLegendPosition = "";
                try
                {
                    strLegendPosition = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderDefaultLegendPosition"));
                }
                catch
                {
                }
                if (strLegendPosition != "" && strLegendPosition != "None")
                {
                    switch (strLegendPosition)
                    {
                        case "Top Left":
                            resizablePictureBox_Legend.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizablePictureBox_Legend.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Top Middle":
                            resizablePictureBox_Legend.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizablePictureBox_Legend.Width / 2));
                            resizablePictureBox_Legend.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Top Right":
                            resizablePictureBox_Legend.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizablePictureBox_Legend.Width;
                            resizablePictureBox_Legend.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Middle Left":
                            resizablePictureBox_Legend.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizablePictureBox_Legend.Top = ((intRectangleHeight - resizablePictureBox_Legend.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Middle Middle":
                            resizablePictureBox_Legend.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizablePictureBox_Legend.Width / 2));
                            resizablePictureBox_Legend.Top = ((intRectangleHeight - resizablePictureBox_Legend.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Middle Right":
                            resizablePictureBox_Legend.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizablePictureBox_Legend.Width;
                            resizablePictureBox_Legend.Top = ((intRectangleHeight - resizablePictureBox_Legend.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Bottom Left":
                            resizablePictureBox_Legend.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizablePictureBox_Legend.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizablePictureBox_Legend.Height;
                            break;
                        case "Bottom Middle":
                            resizablePictureBox_Legend.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizablePictureBox_Legend.Width / 2));
                            resizablePictureBox_Legend.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizablePictureBox_Legend.Height;
                            break;
                        case "Bottom Right":
                            resizablePictureBox_Legend.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizablePictureBox_Legend.Width;
                            resizablePictureBox_Legend.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizablePictureBox_Legend.Height;
                            break;
                        default:
                            break;
                    }
                    bciLegend.Checked = true;  // Show Legend //
                    resizablePictureBox_Legend.BringToFront();
                }

                // North Arrow //
                string strNorthArrowPosition = "";
                try
                {
                    strNorthArrowPosition = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderDefaultNorthArrowPosition"));
                }
                catch
                {
                }
                if (strNorthArrowPosition != "" && strNorthArrowPosition != "None")
                {
                    switch (strNorthArrowPosition)
                    {
                        case "Top Left":
                            resizablePictureBox_NorthArrow.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizablePictureBox_NorthArrow.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Top Middle":
                            resizablePictureBox_NorthArrow.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizablePictureBox_NorthArrow.Width / 2));
                            resizablePictureBox_NorthArrow.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Top Right":
                            resizablePictureBox_NorthArrow.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizablePictureBox_NorthArrow.Width;
                            resizablePictureBox_NorthArrow.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Middle Left":
                            resizablePictureBox_NorthArrow.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizablePictureBox_NorthArrow.Top = ((intRectangleHeight - resizablePictureBox_NorthArrow.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Middle Middle":
                            resizablePictureBox_NorthArrow.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizablePictureBox_NorthArrow.Width / 2));
                            resizablePictureBox_NorthArrow.Top = ((intRectangleHeight - resizablePictureBox_NorthArrow.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Middle Right":
                            resizablePictureBox_NorthArrow.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizablePictureBox_NorthArrow.Width;
                            resizablePictureBox_NorthArrow.Top = ((intRectangleHeight - resizablePictureBox_NorthArrow.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Bottom Left":
                            resizablePictureBox_NorthArrow.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizablePictureBox_NorthArrow.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizablePictureBox_NorthArrow.Height;
                            break;
                        case "Bottom Middle":
                            resizablePictureBox_NorthArrow.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizablePictureBox_NorthArrow.Width / 2));
                            resizablePictureBox_NorthArrow.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizablePictureBox_NorthArrow.Height;
                            break;
                        case "Bottom Right":
                            resizablePictureBox_NorthArrow.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizablePictureBox_NorthArrow.Width;
                            resizablePictureBox_NorthArrow.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizablePictureBox_NorthArrow.Height;
                            break;
                        default:
                            break;
                    }
                    bciNorthArrow.Checked = true;  // Show North Arrow //
                    resizablePictureBox_NorthArrow.BringToFront();
                }

                // Scale Bar //
                string strScaleBarPosition = "";
                try
                {
                    strScaleBarPosition = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderDefaultScaleBarPosition"));
                }
                catch
                {
                }
                if (strScaleBarPosition != "" && strScaleBarPosition != "None")
                {
                    switch (strScaleBarPosition)
                    {
                        case "Top Left":
                            resizablePictureBox_MapScaleBar.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizablePictureBox_MapScaleBar.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Top Middle":
                            resizablePictureBox_MapScaleBar.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizablePictureBox_MapScaleBar.Width / 2));
                            resizablePictureBox_MapScaleBar.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Top Right":
                            resizablePictureBox_MapScaleBar.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizablePictureBox_MapScaleBar.Width;
                            resizablePictureBox_MapScaleBar.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Middle Left":
                            resizablePictureBox_MapScaleBar.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizablePictureBox_MapScaleBar.Top = ((intRectangleHeight - resizablePictureBox_MapScaleBar.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Middle Middle":
                            resizablePictureBox_MapScaleBar.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizablePictureBox_MapScaleBar.Width / 2));
                            resizablePictureBox_MapScaleBar.Top = ((intRectangleHeight - resizablePictureBox_MapScaleBar.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Middle Right":
                            resizablePictureBox_MapScaleBar.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizablePictureBox_MapScaleBar.Width;
                            resizablePictureBox_MapScaleBar.Top = ((intRectangleHeight - resizablePictureBox_MapScaleBar.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Bottom Left":
                            resizablePictureBox_MapScaleBar.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizablePictureBox_MapScaleBar.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizablePictureBox_MapScaleBar.Height;
                            break;
                        case "Bottom Middle":
                            resizablePictureBox_MapScaleBar.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizablePictureBox_MapScaleBar.Width / 2));
                            resizablePictureBox_MapScaleBar.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizablePictureBox_MapScaleBar.Height;
                            break;
                        case "Bottom Right":
                            resizablePictureBox_MapScaleBar.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizablePictureBox_MapScaleBar.Width;
                            resizablePictureBox_MapScaleBar.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizablePictureBox_MapScaleBar.Height;
                            break;
                        default:
                            break;
                    }
                    bciScaleBar.Checked = true;  // Show Scale Bar //
                    resizablePictureBox_MapScaleBar.BringToFront();
                }

                // OS Copyright Ref //
                string strOSRefPosition = "";
                try
                {
                    strOSRefPosition = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderOSCopyrightDefaultPosition"));
                }
                catch
                {
                }
                if (strOSRefPosition != "" && strOSRefPosition != "None")
                {
                    switch (strOSRefPosition)
                    {
                        case "Top Left":
                            resizableMemoEditBoxOSRef.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizableMemoEditBoxOSRef.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Top Middle":
                            resizableMemoEditBoxOSRef.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizableMemoEditBoxOSRef.Width / 2));
                            resizableMemoEditBoxOSRef.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Top Right":
                            resizableMemoEditBoxOSRef.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizableMemoEditBoxOSRef.Width;
                            resizableMemoEditBoxOSRef.Top = intRectangleOffsetY + intRectanglePadHeight;
                            break;
                        case "Middle Left":
                            resizableMemoEditBoxOSRef.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizableMemoEditBoxOSRef.Top = ((intRectangleHeight - resizableMemoEditBoxOSRef.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Middle Middle":
                            resizableMemoEditBoxOSRef.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizableMemoEditBoxOSRef.Width / 2));
                            resizableMemoEditBoxOSRef.Top = ((intRectangleHeight - resizableMemoEditBoxOSRef.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Middle Right":
                            resizableMemoEditBoxOSRef.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizableMemoEditBoxOSRef.Width;
                            resizableMemoEditBoxOSRef.Top = ((intRectangleHeight - resizableMemoEditBoxOSRef.Height) / 2) + intRectangleOffsetY;
                            break;
                        case "Bottom Left":
                            resizableMemoEditBoxOSRef.Left = intRectangleOffsetX + intRectanglePadWidth + 1;
                            resizableMemoEditBoxOSRef.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizableMemoEditBoxOSRef.Height;
                            break;
                        case "Bottom Middle":
                            resizableMemoEditBoxOSRef.Left = ((intRectangleWidth / 2) + intRectangleOffsetX - (resizableMemoEditBoxOSRef.Width / 2));
                            resizableMemoEditBoxOSRef.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizableMemoEditBoxOSRef.Height;
                            break;
                        case "Bottom Right":
                            resizableMemoEditBoxOSRef.Left = intRectangleWidth + intRectangleOffsetX - intRectanglePadWidth - resizableMemoEditBoxOSRef.Width;
                            resizableMemoEditBoxOSRef.Top = intRectangleHeight + intRectangleOffsetY - intRectanglePadHeight - resizableMemoEditBoxOSRef.Height;
                            break;
                        default:
                            break;
                    }
                    bciOSRef.Checked = true;  // Show OS Copyright Ref //
                    resizableMemoEditBoxOSRef.BringToFront();
                }
                // Need the following down here otherwise it crashes - no idea why!!! //
                if (intDefaultPageSize > 0) gridLookUpEdit1.EditValue = intDefaultPageSize;

            }
            else  // Edit //
            {

            }
            TCResize tcResizablePictureBox_Legend = new TCResize(resizablePictureBox_Legend);  // Add End-user Sizing //
            TCResize tcResizablePictureBox_NorthArrow = new TCResize(resizablePictureBox_NorthArrow);  // Add End-user Sizing //
            TCResize tcResizablePictureBox_MapScaleBar = new TCResize(resizablePictureBox_MapScaleBar);  // Add End-user Sizing //    
            TCResize tcResizableMemoEditBoxOSRef = new TCResize(resizableMemoEditBoxOSRef);  // Add End-user Sizing //
            
            boolFormLoading = false;
        }

        public override void RectangleMoved(object sender, int DifferenceX, int DifferenceY)
        {
            // *** Triggered by UserRect class (mPictureBox_MouseMove event). Note: Stub for event exists on frmBase also *** //
            this.LockThisWindow();  // Stop redraw as they will leave a temporary ghost image //
            Rectangle oldRectBounds = rect.rect;
            Rectangle rectControlReal;
            oldRectBounds.X -= DifferenceX;
            oldRectBounds.Y -= DifferenceY;
            int intScrollBarX = xtraScrollableControl1.AutoScrollPosition.X;
            int intScrollBarY = xtraScrollableControl1.AutoScrollPosition.Y;
            foreach (Control c in xtraScrollableControl1.Controls)
            {
                if (c.Name == "pictureBox1") continue;
                rectControlReal = c.Bounds;
                rectControlReal.X -= intScrollBarX;
                rectControlReal.Y -= intScrollBarY;
                if (rectControlReal.IntersectsWith(oldRectBounds))
                {
                    c.Left = c.Left + DifferenceX;
                    c.Top = c.Top + DifferenceY;
                }
            }
            this.UnlockThisWindow(); // Switch back on redraw //
            xtraScrollableControl1.Update();  // Force screen repaint so objects are redrawn in new location //
        }

        private void gridLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (!boolFormLoading)
            {
                intRectangleWidth = Convert.ToInt32((float)Convert.ToInt32(gridLookUpEdit1View.GetRowCellValue(gridLookUpEdit1View.FocusedRowHandle, "PageWidth")) / 25.4 * intPixelsPerInch);
                intRectangleHeight = Convert.ToInt32((float)Convert.ToInt32(gridLookUpEdit1View.GetRowCellValue(gridLookUpEdit1View.FocusedRowHandle, "PageHeight")) / 25.4 * intPixelsPerInch);

                if (intRectangleWidth <= 0) intRectangleWidth = Convert.ToInt32((float)(210 / 25.4) * (float)intPixelsPerInch);
                if (intRectangleHeight <= 0) intRectangleHeight =  Convert.ToInt32((float)(297 / 25.4) * (float)intPixelsPerInch);

                rect.Resize(intRectangleWidth, intRectangleHeight);
            }
        }

        private void ceCustomSizing_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            rect.SetManualResize(ce.Checked);
        }

        private void bbiAddText_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            intAddControlCount++;
            MoveableMemoEditBox resizableMemoEditBox = new MoveableMemoEditBox();
            resizableMemoEditBox.Location = new Point(100, 100);
            resizableMemoEditBox.Size = new System.Drawing.Size(200, 50);
            resizableMemoEditBox.Name = "resizableMemoEditBox" + intAddControlCount.ToString();
            resizableMemoEditBox.TabIndex = intAddControlCount;
            resizableMemoEditBox.MenuManager = this.barManager1;
            resizableMemoEditBox.Properties.BeforeShowMenu += new DevExpress.XtraEditors.Controls.BeforeShowMenuEventHandler(resizableMemoEditBox_Properties_BeforeShowMenu);
            xtraScrollableControl1.Controls.Add(resizableMemoEditBox);
            resizableMemoEditBox.Visible = true;
            resizableMemoEditBox.BringToFront();
            TCResize tcResizableMemoEdit = new TCResize(resizableMemoEditBox);  // Add End-user Sizing //
        }

        private void bbiAddRichText_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            intAddControlCount++;
            MoveableRichEditBox resizableRichTextBox = new MoveableRichEditBox();
            resizableRichTextBox.Location = new Point(100, 100);
            resizableRichTextBox.Size = new System.Drawing.Size(200, 200);
            resizableRichTextBox.Name = "resizableRichTextBox" + intAddControlCount.ToString();
            resizableRichTextBox.TabIndex = intAddControlCount;
            resizableRichTextBox.MenuManager = this.barManager1;
            resizableRichTextBox.SpellChecker = this.scSpellChecker;
            resizableRichTextBox.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            resizableRichTextBox.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            resizableRichTextBox.Options.VerticalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Hidden;
            resizableRichTextBox.Options.HorizontalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Hidden;
            resizableRichTextBox.Text = "";
            resizableRichTextBox.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple; 
            resizableRichTextBox.PopupMenuShowing += new DevExpress.XtraRichEdit.PopupMenuShowingEventHandler(resizableRichTextBox_PopupMenuShowing);
            xtraScrollableControl1.Controls.Add(resizableRichTextBox);
            this.richEditBarController1.RichEditControl = resizableRichTextBox;
            resizableRichTextBox.Enter += new System.EventHandler(this.resizableRichTextBox_Enter);
            resizableRichTextBox.Leave += new System.EventHandler(this.resizableRichTextBox_Leave);
            resizableRichTextBox.Visible = true;
            resizableRichTextBox.BringToFront();
            TCResize tcResizableRichEdit = new TCResize(resizableRichTextBox);  // Add End-user Sizing //
        }

        private void bbiAddImage_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            intAddControlCount++;
            MoveablePictureBox resizablePictureBox = new MoveablePictureBox();

            //ResizablePictureBox resizablePictureBox = new ResizablePictureBox();
            resizablePictureBox.Location = new Point(100, 100);
            resizablePictureBox.Size = new System.Drawing.Size(200, 200);
            resizablePictureBox.Name = "resizablePictureBox" + intAddControlCount.ToString();
            resizablePictureBox.TabIndex = intAddControlCount;
            resizablePictureBox.MenuManager = this.barManager1;
            resizablePictureBox.MouseUp += new MouseEventHandler(resizablePictureBox_MouseUp);
            xtraScrollableControl1.Controls.Add(resizablePictureBox);
            resizablePictureBox.Visible = true;
            resizablePictureBox.BringToFront();
            TCResize tcResizablePicture = new TCResize(resizablePictureBox);  // Add End-user Sizing //
         }

        private void resizableRichTextBox_Enter(object sender, EventArgs e)
        {
            this.richEditBarController1.RichEditControl = (DevExpress.XtraRichEdit.RichEditControl)sender;
        }

        private void resizableRichTextBox_Leave(object sender, EventArgs e)
        {
            //this.richEditBarController1.RichEditControl = null;
        }

        private void resizableMemoEditBox_Properties_BeforeShowMenu(object sender, DevExpress.XtraEditors.Controls.BeforeShowMenuEventArgs e)
        {
            bool boolDeleteCreated = false;
            foreach (DevExpress.Utils.Menu.DXMenuItem item in e.Menu.Items)
            {
                if (item.Caption == "Delete Object")
                {
                    boolDeleteCreated = true;
                    break;
                }
            }
            if (!boolDeleteCreated)
            {
                DevExpress.Utils.Menu.DXMenuItem item = new DevExpress.Utils.Menu.DXMenuItem("bbiDeleteObject", new EventHandler(bbiDeleteObject_ItemClick));
                item.Caption = "Delete Object";
                item.Image = imageList1.Images[0];
                item.BeginGroup = true;
                item.Enabled = true;
                //item.BeginGroup = true;
                e.Menu.Items.Add(item);
            }
        }

        private void resizableRichTextBox_PopupMenuShowing(object sender, DevExpress.XtraRichEdit.PopupMenuShowingEventArgs e)
        {
            bool boolDeleteCreated = false;
            foreach (DevExpress.Utils.Menu.DXMenuItem item in e.Menu.Items)
            {
                if (item.Caption == "Delete Object")
                {
                    boolDeleteCreated = true;
                    break;
                }
            }
            if (!boolDeleteCreated)
            {
                DevExpress.Utils.Menu.DXMenuItem item = new DevExpress.Utils.Menu.DXMenuItem("bbiDeleteObject", new EventHandler(bbiDeleteObject_ItemClick));
                item.Caption = "Delete Object";
                item.Image = imageList1.Images[0];
                item.BeginGroup = true;
                item.Enabled = true;
                e.Menu.Items.Add(item);
            }
        }

        private void resizablePictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            // menu property of edit is a property on the ResizablePictureBox class //
            MoveablePictureBox edit = sender as MoveablePictureBox;
            this.ActiveControl = edit;
            if (edit.menu == null)
            {
                System.Reflection.PropertyInfo info = typeof(MoveablePictureBox).GetProperty("Menu", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                edit.menu = info.GetValue(edit, null) as DevExpress.Utils.Menu.DXPopupMenu;
                DevExpress.Utils.Menu.DXMenuItem item = new DevExpress.Utils.Menu.DXMenuItem("bbiDeleteObject", new EventHandler(bbiDeleteObject_ItemClick));
                item.Caption = "Delete Object";
                item.Image = imageList1.Images[0];
                item.BeginGroup = true;
                item.Enabled = true;
                edit.menu.Items.Add(item);
            }
        }

        private void bbiDeleteObject_ItemClick(object sender, EventArgs e)
        {
            Control ctrl = this.ActiveControl;
            if (ctrl.GetType().Name == "MoveableRichEditBox")
            {
                MoveableRichEditBox rrtb = (MoveableRichEditBox)ctrl;
                rrtb.PopupMenuShowing -= new DevExpress.XtraRichEdit.PopupMenuShowingEventHandler(resizableRichTextBox_PopupMenuShowing);
                this.Controls.Remove(ctrl);
                ctrl.Dispose();
            }
            else if (ctrl.GetType().Name == "MoveablePictureBox")
            {
                MoveablePictureBox rpb = (MoveablePictureBox)ctrl;
                rpb.MouseUp -= new MouseEventHandler(resizablePictureBox_MouseUp);
                this.Controls.Remove(ctrl);
                ctrl.Dispose();
            }
            else
            {
                ctrl = this.ActiveControl.Parent;
                if (ctrl.GetType().Name == "MoveableMemoEditBox")
                {
                    MoveableMemoEditBox rmeb = (MoveableMemoEditBox)ctrl;
                    rmeb.Properties.BeforeShowMenu -= new DevExpress.XtraEditors.Controls.BeforeShowMenuEventHandler(resizableMemoEditBox_Properties_BeforeShowMenu);
                    this.Controls.Remove(ctrl);
                    ctrl.Dispose();
                }
            }
        }

        private void bciLegend_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciLegend.Checked)
            {
                if (frmParentMappingForm.bbiLegend.Checked)
                {
                    resizablePictureBox_Legend.Image = frmParentMappingForm.pictureBoxLegend.Image;
                }
                else
                {
                    frmParentMappingForm.Create_Legend();
                    resizablePictureBox_Legend.Image = frmParentMappingForm.pictureBoxLegend.Image;
                }
            }
            else
            {
                if (!frmParentMappingForm.bbiLegend.Checked)
                {
                    frmParentMappingForm.pictureBoxLegend.Image = null;
                }
                resizablePictureBox_Legend.Image = null;
            }
            resizablePictureBox_Legend.Visible = bciLegend.Checked;
        }

        private void bciOSRef_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            resizableMemoEditBoxOSRef.Visible = bciOSRef.Checked;
        }

        private void bciNorthArrow_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            resizablePictureBox_NorthArrow.Visible = bciNorthArrow.Checked;
            
        }

        private void bciScaleBar_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Image image;
            if (bciScaleBar.Checked)
                image = Create_Scale_Bar();
            else
                image = null;
            resizablePictureBox_MapScaleBar.Image = image;
            resizablePictureBox_MapScaleBar.Visible = bciScaleBar.Checked;
        }

        private void resizablePictureBox_MapScaleBar_Resize(object sender, EventArgs e)
        {
            if (resizablePictureBox_MapScaleBar.Image != null) resizablePictureBox_MapScaleBar.Image = Create_Scale_Bar();
        }

        private Image Create_Scale_Bar()
        {
            Mapping_Functions MapFunctions = new Mapping_Functions();
            Image image = MapFunctions.Create_Scale_Bar(resizablePictureBox_MapScaleBar.Size.Width, resizablePictureBox_MapScaleBar.Size.Height, Convert.ToInt32(intCurrentMapScale));
            return image;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            using (DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter AddWorkOrderMap = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter())
            {
                AddWorkOrderMap.ChangeConnectionString(strConnectionString);
                try
                {
                    intMapRecordID = Convert.ToInt32(AddWorkOrderMap.sp01301_AT_WorkOrder_Map_Add(intWorkOrderID, textEdit2.EditValue.ToString(), memoEdit1.EditValue.ToString(), this.GlobalSettings.UserID));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Work Order Map - an error occurred [" + ex.Message + "]!\n\nTry saving again, if the problem persists, contact Technical Support.", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            
            string strImagePath = strMapsPath + intMapRecordID.ToString() + ".gif";
            strImagePath = strImagePath.ToLower();
            string strImageThumbPath = strMapsPath + intMapRecordID.ToString() + "_thumb.jpg";
            strImageThumbPath = strImageThumbPath.ToLower();
            
            try
            {
                using (Bitmap orignal = new Bitmap(pictureBox1.Image))
                {
                    using (Bitmap newimage = new Bitmap((int)rect.rect.Width, (int)rect.rect.Height))
                    {
		                using (Graphics newgraphics = Graphics.FromImage(newimage))
		                {
                            newgraphics.DrawImage(orignal, 0, 0, new Rectangle(rect.rect.X, rect.rect.Y, rect.rect.Width, rect.rect.Height), GraphicsUnit.Pixel);
                            newgraphics.Flush();

                            // Merge any objects on top of the snapshow rectangle //
                            int intScrollBarX = xtraScrollableControl1.AutoScrollPosition.X;
                            int intScrollBarY = xtraScrollableControl1.AutoScrollPosition.Y;
                            Rectangle rectControlReal;
                            foreach (Control c in xtraScrollableControl1.Controls)
                            {
                                if (c.Name == "pictureBox1" || !c.Visible) continue;
                                
                                rectControlReal = c.Bounds;
                                rectControlReal.X -= intScrollBarX;
                                rectControlReal.Y -= intScrollBarY;
                                if (rectControlReal.IntersectsWith(rect.rect))
                                {
                                    if (c.GetType().Name == "MoveablePictureBox")
                                    {
                                        MoveablePictureBox rpb = (MoveablePictureBox)c;
                                        
                                        // Stop Image from being transparent by painting a white box behind it with a black border //
                                        //newgraphics.DrawRectangle(new Pen(new SolidBrush(Color.Black)), rectControlReal.X - rect.rect.X - 1, rectControlReal.Y - rect.rect.Y - 1, rpb.Image.Width + 2, rpb.Image.Height + 2);
                                        //newgraphics.FillRectangle(new SolidBrush(Color.White), rectControlReal.X - rect.rect.X, rectControlReal.Y - rect.rect.Y, rpb.Image.Width, rpb.Image.Height);

                                        using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(rpb.Width, rpb.Height))
                                        {
                                            c.DrawToBitmap(bmp, c.ClientRectangle);
                                            // Embed Object in map //
                                            newgraphics.DrawImage(bmp, rectControlReal.X - rect.rect.X, rectControlReal.Y - rect.rect.Y);
                                        }
                                    }
                                    else if (c.GetType().Name == "MoveableMemoEditBox")
                                    {
                                        // Clear any highlighted text //
                                        MoveableMemoEditBox rmeb = (MoveableMemoEditBox)c;
                                        rmeb.Select(0, 0);

                                        using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(c.Width, c.Height))
                                        {
                                            c.DrawToBitmap(bmp, c.ClientRectangle);
                                            // Embed Object in map //
                                            newgraphics.DrawImage(bmp, rectControlReal.X - rect.rect.X, rectControlReal.Y - rect.rect.Y);
                                        }
                                    }
                                    else if (c.GetType().Name == "MoveableRichEditBox")
                                    {
                                        // Clear any highlighted text //
                                        MoveableRichEditBox rrtb = (MoveableRichEditBox)c;
                                        DocumentRange myRange = rrtb.Document.CreateRange(1, 1);  // Don't set to (0, 0) otherwise crash occurs //
                                        rrtb.Document.Selection = myRange;

                                        rrtb.SpellChecker.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand;  // Clear any highlightig of miss-spelled words //

                                        using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(c.Width, c.Height))
                                        {
                                            c.DrawToBitmap(bmp, c.ClientRectangle);
                                            // Embed Object in map //
                                            newgraphics.DrawImage(bmp, rectControlReal.X - rect.rect.X, rectControlReal.Y - rect.rect.Y);
                                        }
                                    }
                                }
                            }
		                }
                        // Cut image down so margin areas are removed //
                        Rectangle bounds = new Rectangle(Point.Empty, newimage.Size);
                        //bounds.Inflate((int)-20, (int)-20);
                        bounds.X = bounds.X + 37;
                        bounds.Y = bounds.Y + 110;
                        bounds.Width = bounds.Width - 74;
                        bounds.Height = bounds.Height - 220;

                        Image newimage2 = newimage.Clone(bounds, PixelFormat.DontCare);                       

                        newimage2.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Gif);

                        newimage2 = newimage2.GetThumbnailImage(50, 50, null, new IntPtr());
                        newimage2.Save(strImageThumbPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }
            }
            catch (Exception ex)
            {
                // Remove the Saved Record in the DB //
                using (DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter DeleteWorkOrderMap = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter())
                {
                    DeleteWorkOrderMap.ChangeConnectionString(strConnectionString);
                    try
                    {
                        DeleteWorkOrderMap.sp01302_AT_WorkOrder_Map_Delete(intMapRecordID);
                    }
                    catch
                    {
                    }
                }              
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred when attempting to save the image [" + ex.Message + "]!", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails validation test earlier in this process //
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void bbiPreview_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using (Bitmap orignal = new Bitmap(pictureBox1.Image))
                {
                    using (Bitmap newimage = new Bitmap((int)rect.rect.Width, (int)rect.rect.Height))
                    {
                        using (Graphics newgraphics = Graphics.FromImage(newimage))
                        {
                            newgraphics.DrawImage(orignal, 0, 0, new Rectangle(rect.rect.X, rect.rect.Y, rect.rect.Width, rect.rect.Height), GraphicsUnit.Pixel);
                            newgraphics.Flush();

                            // Merge any objects on top of the snapshow rectangle //
                            int intScrollBarX = xtraScrollableControl1.AutoScrollPosition.X;
                            int intScrollBarY = xtraScrollableControl1.AutoScrollPosition.Y;
                            Rectangle rectControlReal;
                            foreach (Control c in xtraScrollableControl1.Controls)
                            {
                                if (c.Name == "pictureBox1" || !c.Visible) continue;

                                rectControlReal = c.Bounds;
                                rectControlReal.X -= intScrollBarX;
                                rectControlReal.Y -= intScrollBarY;
                                if (rectControlReal.IntersectsWith(rect.rect))
                                {
                                    if (c.GetType().Name == "MoveablePictureBox")
                                    {
                                        MoveablePictureBox rpb = (MoveablePictureBox)c;

                                        // Stop Image from being transparent by painting a white box behind it with a black border //
                                        //newgraphics.DrawRectangle(new Pen(new SolidBrush(Color.Black)), rectControlReal.X - rect.rect.X - 1, rectControlReal.Y - rect.rect.Y - 1, rpb.Image.Width + 2, rpb.Image.Height + 2);
                                        //newgraphics.FillRectangle(new SolidBrush(Color.White), rectControlReal.X - rect.rect.X, rectControlReal.Y - rect.rect.Y, rpb.Image.Width, rpb.Image.Height);

                                        using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(rpb.Width, rpb.Height))
                                        {
                                            c.DrawToBitmap(bmp, c.ClientRectangle);
                                            // Embed Object in map //
                                            newgraphics.DrawImage(bmp, rectControlReal.X - rect.rect.X, rectControlReal.Y - rect.rect.Y);
                                        }
                                    }
                                    else if (c.GetType().Name == "MoveableMemoEditBox")
                                    {
                                        // Clear any highlighted text //
                                        MoveableMemoEditBox rmeb = (MoveableMemoEditBox)c;
                                        rmeb.Select(0, 0);

                                        using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(c.Width, c.Height))
                                        {
                                            c.DrawToBitmap(bmp, c.ClientRectangle);
                                            // Embed Object in map //
                                            newgraphics.DrawImage(bmp, rectControlReal.X - rect.rect.X, rectControlReal.Y - rect.rect.Y);
                                        }
                                    }
                                    else if (c.GetType().Name == "MoveableRichEditBox")
                                    {
                                        // Clear any highlighted text //
                                        MoveableRichEditBox rrtb = (MoveableRichEditBox)c;
                                        DocumentRange myRange = rrtb.Document.CreateRange(1, 1);  // Don't set to (0, 0) otherwise crash occurs //
                                        rrtb.Document.Selection = myRange;

                                        rrtb.SpellChecker.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand;  // Clear any highlightig of miss-spelled words //

                                        using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(c.Width, c.Height))
                                        {
                                            c.DrawToBitmap(bmp, c.ClientRectangle);
                                            // Embed Object in map //
                                            newgraphics.DrawImage(bmp, rectControlReal.X - rect.rect.X, rectControlReal.Y - rect.rect.Y);
                                        }
                                    }
                                }
                            }
                        }
                        // Cut image down so margin areas are removed //
                        Rectangle bounds = new Rectangle(Point.Empty, newimage.Size);
                        //bounds.Inflate((int)-20, (int)-20);
                        bounds.X = bounds.X + 37;
                        bounds.Y = bounds.Y + 110;
                        bounds.Width = bounds.Width - 74;
                        bounds.Height = bounds.Height - 220;
                        
                        Image newimage2 = newimage.Clone(bounds, PixelFormat.DontCare);

                        frmPrintPreview fPrintPreview = new frmPrintPreview();
                        frmBase fbBase;
                        fbBase = this;
                        fPrintPreview.objObject = fbBase;
                        fPrintPreview.passedImage = newimage2;
                        fPrintPreview.GlobalSettings = this.GlobalSettings;
                        fPrintPreview.ShowDialog();
                        fPrintPreview.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred when attempting to print preview the map [" + ex.Message + "]!", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void frm_AT_Mapping_WorkOrder_Map_FormClosing(object sender, FormClosingEventArgs e)
        {
            pictureBox1.Dispose();
            pictureBox1.Image = null;  // Clear image and collect garbage so no references are left to it. //
            imageMap.Dispose();
            imageMap = null;
            GC.GetTotalMemory(true);
        }


        #region DrawingOnMap

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (_ObjectDrawn == "") return;
            isMoving = true;
            mouseDownPosition = e.Location;
            _Previous = e.Location;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_ObjectDrawn == "") return;
            if (_ObjectDrawn != "freehand")
            {
                if (isMoving)
                {
                    mouseMovePosition = e.Location;
                    pictureBox1.Invalidate();
                }
            }
            else  // Freehand //
            {
                if (_Previous != null)
                {
                    if (pictureBox1.Image == null)
                    {
                        Bitmap bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
                        using (Graphics g = Graphics.FromImage(bmp))
                        {
                            g.Clear(Color.White);
                        }
                        pictureBox1.Image = bmp;
                    }
                    using (Graphics g = Graphics.FromImage(pictureBox1.Image))
                    {
                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        g.DrawLine(_Pen, _Previous.Value, e.Location);
                    }
                    pictureBox1.Invalidate();
                    _Previous = e.Location;
                }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (_ObjectDrawn == "") return;
            isMoving = false;
            _Previous = null;

            if (pictureBox1.Image == null)
            {
                Bitmap bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.Clear(Color.White);
                }
                pictureBox1.Image = bmp;
            }
            using (Graphics g = Graphics.FromImage(pictureBox1.Image))
            {
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                switch (_ObjectDrawn)
                {
                    case "elypsis":
                        {
                            int intStartX = mouseDownPosition.X;
                            int intStartY = mouseDownPosition.Y;
                            int intEndX = mouseMovePosition.X;
                            int intEndY = mouseMovePosition.Y;
                            if (intEndX < intStartX)
                            {
                                intEndX = intStartX;
                                intStartX = mouseMovePosition.X;
                            }
                            if (intEndY < intStartY)
                            {
                                intEndY = intStartY;
                                intStartY = mouseMovePosition.Y;
                            }
                            g.DrawEllipse(_Pen, new Rectangle(new Point(intStartX, intStartY), new Size(intEndX - intStartX, intEndY - intStartY)));
                            Circles.Clear();
                            pictureBox1.Invalidate();
                            _Previous = e.Location;
                        }
                        break;
                    case "rectangle":
                        {
                            int intStartX = mouseDownPosition.X;
                            int intStartY = mouseDownPosition.Y;
                            int intEndX = mouseMovePosition.X;
                            int intEndY = mouseMovePosition.Y;
                            if (intEndX < intStartX)
                            {
                                intEndX = intStartX;
                                intStartX = mouseMovePosition.X;
                            }
                            if (intEndY < intStartY)
                            {
                                intEndY = intStartY;
                                intStartY = mouseMovePosition.Y;
                            }
                            g.DrawRectangle(_Pen, new Rectangle(new Point(intStartX, intStartY), new Size(intEndX - intStartX, intEndY - intStartY)));
                            Circles.Clear();
                            pictureBox1.Invalidate();
                            _Previous = e.Location;
                        }
                        break;
                    case "line":
                        {
                            if (mouseDownPosition != mouseMovePosition)
                            {
                                int intStartX = mouseDownPosition.X;
                                int intStartY = mouseDownPosition.Y;
                                int intEndX = mouseMovePosition.X;
                                int intEndY = mouseMovePosition.Y;
                                Point StartPoint = new Point(intStartX, intStartY);
                                Point EndPoint = new Point(intEndX, intEndY);
                                DrawArrow(g, StartPoint, EndPoint, _Pen.Color, (int)_Pen.Width, _ArrowHeadSize, false);
                            }
                            Circles.Clear();
                            pictureBox1.Invalidate();
                            _Previous = e.Location;
                        }
                        break;
                    case "arrow":
                        {
                            if (mouseDownPosition != mouseMovePosition)
                            {
                                int intStartX = mouseDownPosition.X;
                                int intStartY = mouseDownPosition.Y;
                                int intEndX = mouseMovePosition.X;
                                int intEndY = mouseMovePosition.Y;
                                Point StartPoint = new Point(intStartX, intStartY);
                                Point EndPoint = new Point(intEndX, intEndY);
                                DrawArrow(g, StartPoint, EndPoint, _Pen.Color, (int)_Pen.Width, _ArrowHeadSize, true);
                            }
                            Circles.Clear();
                            pictureBox1.Invalidate();
                            _Previous = e.Location;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (_ObjectDrawn == "") return;
            var g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            if (isMoving)
            {
                int intStartX = mouseDownPosition.X;
                int intStartY = mouseDownPosition.Y;
                int intEndX = mouseMovePosition.X;
                int intEndY = mouseMovePosition.Y;
                if (intEndX < intStartX)
                {
                    intEndX = intStartX;
                    intStartX = mouseMovePosition.X;
                }
                if (intEndY < intStartY)
                {
                    intEndY = intStartY;
                    intStartY = mouseMovePosition.Y;
                }
                switch (_ObjectDrawn)
                {
                    case "elypsis":
                        {
                            g.DrawEllipse(_Pen, new Rectangle(new Point(intStartX, intStartY), new Size(intEndX - intStartX, intEndY - intStartY)));
                        }
                        break;
                    case "rectangle":
                        {
                            g.DrawRectangle(_Pen, new Rectangle(new Point(intStartX, intStartY), new Size(intEndX - intStartX, intEndY - intStartY)));
                        }
                        break;
                    case "line":
                        {
                            if (mouseDownPosition != mouseMovePosition) DrawArrow(g, mouseDownPosition, mouseMovePosition, _Pen.Color, (int)_Pen.Width, _ArrowHeadSize, false);
                        }
                        break;
                    case "arrow":
                        {
                            if (mouseDownPosition != mouseMovePosition) DrawArrow(g, mouseDownPosition, mouseMovePosition, _Pen.Color, (int)_Pen.Width, _ArrowHeadSize, true);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void repositoryItemColorPickEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ColorPickEdit cpe = (ColorPickEdit)sender;
            _Pen.Color = (Color)cpe.Color;
        }

        private void repositoryItemSpinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            _Pen.Width = (float)Convert.ToDecimal(se.EditValue);
        }

        private string strTitleCaption = "";

        private void bbiClearDrawing_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear any drawing performed on top of the map - proceed?", "Clear Drawing", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                imageMap = imageBackup;
                pictureBox1.Image = imageMap;
                imageBackup = (Image)imageMap.Clone();
                pictureBox1.Refresh();
            }
        }

        private void DrawArrow(Graphics g, Point ArrowStart, Point ArrowEnd, Color ArrowColor, int LineWidth, int ArrowMultiplier, bool IncludeEndShape)
        {
            // draw the line //
            g.DrawLine(_Pen, ArrowStart, ArrowEnd);

            // determine the coords for the arrow point //
            // tip of the arrow //
            PointF arrowPoint = ArrowEnd;

            // determine arrow length //
            double arrowLength = Math.Sqrt(Math.Pow(Math.Abs(ArrowStart.X - ArrowEnd.X), 2) + Math.Pow(Math.Abs(ArrowStart.Y - ArrowEnd.Y), 2));

            // determine arrow angle //
            double arrowAngle = Math.Atan2(Math.Abs(ArrowStart.Y - ArrowEnd.Y), Math.Abs(ArrowStart.X - ArrowEnd.X));

            // get the x,y of the back of the point - to change from an arrow to a diamond, change the 3 in the next if/else blocks to 6
            double pointX, pointY;
            if (ArrowStart.X > ArrowEnd.X)
            {
                pointX = ArrowStart.X - (Math.Cos(arrowAngle) * (arrowLength - (3 * ArrowMultiplier)));
            }
            else
            {
                pointX = Math.Cos(arrowAngle) * (arrowLength - (3 * ArrowMultiplier)) + ArrowStart.X;
            }

            if (ArrowStart.Y > ArrowEnd.Y)
            {
                pointY = ArrowStart.Y - (Math.Sin(arrowAngle) * (arrowLength - (3 * ArrowMultiplier)));
            }
            else
            {
                pointY = (Math.Sin(arrowAngle) * (arrowLength - (3 * ArrowMultiplier))) + ArrowStart.Y;
            }

            PointF arrowPointBack = new PointF((float)pointX, (float)pointY);

            // get the secondary angle of the left tip //
            double angleB = Math.Atan2((3 * ArrowMultiplier), (arrowLength - (3 * ArrowMultiplier)));

            double angleC = Math.PI * (90 - (arrowAngle * (180 / Math.PI)) - (angleB * (180 / Math.PI))) / 180;

            // get the secondary length //
            double secondaryLength = (3 * ArrowMultiplier) / Math.Sin(angleB);

            if (ArrowStart.X > ArrowEnd.X)
            {
                pointX = ArrowStart.X - (Math.Sin(angleC) * secondaryLength);
            }
            else
            {
                pointX = (Math.Sin(angleC) * secondaryLength) + ArrowStart.X;
            }

            if (ArrowStart.Y > ArrowEnd.Y)
            {
                pointY = ArrowStart.Y - (Math.Cos(angleC) * secondaryLength);
            }
            else
            {
                pointY = (Math.Cos(angleC) * secondaryLength) + ArrowStart.Y;
            }

            // get the left point //
            PointF arrowPointLeft = new PointF((float)pointX, (float)pointY);

            // move to the right point //
            angleC = arrowAngle - angleB;

            if (ArrowStart.X > ArrowEnd.X)
            {
                pointX = ArrowStart.X - (Math.Cos(angleC) * secondaryLength);
            }
            else
            {
                pointX = (Math.Cos(angleC) * secondaryLength) + ArrowStart.X;
            }

            if (ArrowStart.Y > ArrowEnd.Y)
            {
                pointY = ArrowStart.Y - (Math.Sin(angleC) * secondaryLength);
            }
            else
            {
                pointY = (Math.Sin(angleC) * secondaryLength) + ArrowStart.Y;
            }

            PointF arrowPointRight = new PointF((float)pointX, (float)pointY);

            // create the point list //
            PointF[] arrowPoints = new PointF[4];
            arrowPoints[0] = arrowPoint;
            arrowPoints[1] = arrowPointLeft;
            arrowPoints[2] = arrowPointBack;
            arrowPoints[3] = arrowPointRight;

            // draw the outline //
            if (IncludeEndShape) g.DrawPolygon(_Pen, arrowPoints);

            // fill the polygon //
            if (IncludeEndShape) g.FillPolygon(new SolidBrush(ArrowColor), arrowPoints);
        }

        private void bciNoDrawing_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Text = strTitleCaption;
            rect = new UserRect(new Rectangle(_oldX, _oldY, _oldSize.Width, _oldSize.Height));
            rect.SetPictureBox(this.pictureBox1);
            btnSave.Enabled = true;
            xtraScrollableControl1.Update();
            gridLookUpEdit1.Enabled = true;
            _ObjectDrawn = "";
            pictureBox1.Cursor = Cursors.Arrow;
            _boolRectangleHidden = false;
            bsiSelectedDrawingTool.Caption = "Draw [None]";
            beiArrowSize.Enabled = false;
        }

        private void bciDrawFreehand_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciDrawFreehand.Checked)  // In drawing mode //
            {
                if (!_boolRectangleHidden)
                {
                    strTitleCaption = this.Text;
                    this.Text += "      ***** SAVE DISABLED UNTIL DRAWING MODE SWITCHED OFF *****";
                    _oldX = rect.GetX();
                    _oldY = rect.GetY();
                    _oldSize = rect.GetSize();
                    rect.ClearPictureBox(this.pictureBox1);
                    rect = null;
                    pictureBox1.Refresh();
                    btnSave.Enabled = false;
                    gridLookUpEdit1.Enabled = false;
                    _boolRectangleHidden = true;
                }
                _ObjectDrawn = "freehand";
                isMoving = false;
                _Previous = null;
                try
                {
                    pictureBox1.Cursor = BaseObjects.LoadCursor.LoadCustomCursor(Application.StartupPath + "\\Pencil.cur");
                }
                catch (Exception)
                {
                    pictureBox1.Cursor = Cursors.Arrow;
                }
                bsiSelectedDrawingTool.Caption = "Draw [Freehand]";
                beiArrowSize.Enabled = false;
            }
        }

        private void bciDrawRectangle_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciDrawRectangle.Checked)  // In drawing mode //
            {
                if (!_boolRectangleHidden)
                {
                    strTitleCaption = this.Text;
                    this.Text += "      ***** SAVE DISABLED UNTIL DRAWING MODE SWITCHED OFF *****";
                    _oldX = rect.GetX();
                    _oldY = rect.GetY();
                    _oldSize = rect.GetSize();
                    rect.ClearPictureBox(this.pictureBox1);
                    rect = null;
                    pictureBox1.Refresh();
                    btnSave.Enabled = false;
                    gridLookUpEdit1.Enabled = false;
                    _boolRectangleHidden = true;
                }
                _ObjectDrawn = "rectangle";
                isMoving = false;
                _Previous = null;
                pictureBox1.Cursor = Cursors.Cross;
                bsiSelectedDrawingTool.Caption = "Draw [Rectangle]";
                beiArrowSize.Enabled = false;
            }
        }

        private void bciDrawElypsis_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciDrawElypsis.Checked)  // In drawing mode //
            {
                if (!_boolRectangleHidden)
                {
                    strTitleCaption = this.Text;
                    this.Text += "      ***** SAVE DISABLED UNTIL DRAWING MODE SWITCHED OFF *****";
                    _oldX = rect.GetX();
                    _oldY = rect.GetY();
                    _oldSize = rect.GetSize();
                    rect.ClearPictureBox(this.pictureBox1);
                    rect = null;
                    pictureBox1.Refresh();
                    btnSave.Enabled = false;
                    gridLookUpEdit1.Enabled = false;
                    _boolRectangleHidden = true;
                }
                _ObjectDrawn = "elypsis";
                isMoving = false;
                _Previous = null;
                pictureBox1.Cursor = Cursors.Cross;
                bsiSelectedDrawingTool.Caption = "Draw [Elypsis]";
                beiArrowSize.Enabled = false;
            }
        }

        private void bciDrawLine_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciDrawLine.Checked)  // In drawing mode //
            {
                if (!_boolRectangleHidden)
                {
                    strTitleCaption = this.Text;
                    this.Text += "      ***** SAVE DISABLED UNTIL DRAWING MODE SWITCHED OFF *****";
                    _oldX = rect.GetX();
                    _oldY = rect.GetY();
                    _oldSize = rect.GetSize();
                    rect.ClearPictureBox(this.pictureBox1);
                    rect = null;
                    pictureBox1.Refresh();
                    btnSave.Enabled = false;
                    gridLookUpEdit1.Enabled = false;
                    _boolRectangleHidden = true;
                }
                _ObjectDrawn = "line";
                isMoving = false;
                _Previous = null;
                pictureBox1.Cursor = Cursors.Cross;
                bsiSelectedDrawingTool.Caption = "Draw [Line]";
                beiArrowSize.Enabled = false;
            }
        }

        private void bciDrawArrow_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciDrawArrow.Checked)  // In drawing mode //
            {
                if (!_boolRectangleHidden)
                {
                    strTitleCaption = this.Text;
                    this.Text += "      ***** SAVE DISABLED UNTIL DRAWING MODE SWITCHED OFF *****";
                    _oldX = rect.GetX();
                    _oldY = rect.GetY();
                    _oldSize = rect.GetSize();
                    rect.ClearPictureBox(this.pictureBox1);
                    rect = null;
                    pictureBox1.Refresh();
                    btnSave.Enabled = false;
                    gridLookUpEdit1.Enabled = false;
                    _boolRectangleHidden = true;
                }
                _ObjectDrawn = "arrow";
                isMoving = false;
                _Previous = null;
                pictureBox1.Cursor = Cursors.Cross;
                bsiSelectedDrawingTool.Caption = "Draw [Arrow]";
                beiArrowSize.Enabled = true;
            }
        }

        private void repositoryItemSpinEdit2_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            _ArrowHeadSize = Convert.ToInt32(se.EditValue);
        }

 
        #endregion


 
    }
}


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Collections;


using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_AT_Data_Transfer_Tablet : BaseObjects.frmBase
    {

        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        #endregion

        public frm_AT_Data_Transfer_Tablet()
        {
            InitializeComponent();
        }

        private void frm_AT_Data_Transfer_Tablet_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;
            this.FormID = 2006;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            if (this.GlobalSettings.SystemDataTransferMode.ToLower() == "desktop")
            {
                xtraTabPage1.PageEnabled = true;
                xtraTabPage2.PageEnabled = false;
                xtraTabControl1.SelectedTabPage = xtraTabPage1;
                dateEditInspectionDateFilter.DateTime = this.GlobalSettings.LiveStartDate;
            }
            else
            {
                xtraTabPage1.PageEnabled = false;
                xtraTabPage2.PageEnabled = true;
                xtraTabControl1.SelectedTabPage = xtraTabPage2;
            }
        }


        #region Desktop \ Server PC Version

        private void checkEditExportToTablet_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                btnServer.Text = "Export To Tablet PC";
                checkEditSyncLinkedDocumentsDesktop.Text = "Export <b>Linked Document Files</b>";
                checkEditSyncLinkedDocumentsDesktop.Checked = true;
                checkEditSyncPictures.Text = "Export <b>Linked Picture Files</b>";
                checkEditSyncPictures.Checked = true;
            }
            groupControl1.Visible = true;
            buttonEditChooseImportFolder.Visible = false;
            btnServer.Enabled = true;
            checkEditSyncLinkedDocumentsDesktop.Visible = true;
            checkEditSyncPictures.Visible = true;
        }

        private void checkEditImportFromTablet_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                btnServer.Text = "Import To Tablet PC";
                checkEditSyncLinkedDocumentsDesktop.Text = "Import <b>Linked Document Files</b>";
                checkEditSyncLinkedDocumentsDesktop.Checked = true;
                checkEditSyncPictures.Text = "Import <b>Linked Picture Files</b>";
                checkEditSyncPictures.Checked = true;
            }
            groupControl1.Visible = false;
            buttonEditChooseImportFolder.Visible = true;
            btnServer.Enabled = (buttonEditChooseImportFolder.EditValue == null || buttonEditChooseImportFolder.EditValue.ToString() == "" ? false : true);
            checkEditSyncLinkedDocumentsDesktop.Visible = true;
            checkEditSyncPictures.Visible = true;
        }

        private void dateEditInspectionDateFilter_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nIf you proceed with no date when exporting, all Inspections and Actions will be exported.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditInspectionDateFilter.EditValue = null;
                }
            }
        }

        private void buttonEditChooseImportFolder_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                ButtonEdit edit = sender as ButtonEdit;

                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                string strMobileInterfaceImportFolder = "";
                try
                {
                    strMobileInterfaceImportFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "MobileInterfaceImportFolder"));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Tablet to Desktop Transfer File Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Select Data Set to Import", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (string.IsNullOrEmpty(strMobileInterfaceImportFolder)) strMobileInterfaceImportFolder = "C:";

                using (FolderBrowserDialog fbd = new FolderBrowserDialog())
                {
                    fbd.SelectedPath = strMobileInterfaceImportFolder;
                    fbd.Description = "Select a Folder then click OK.";
                    fbd.ShowNewFolderButton = false;
                    if (fbd.ShowDialog() == DialogResult.OK)
                    {
                        frmProgress fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Checking Data Present...");
                        fProgress.Show();
                        Application.DoEvents();

                        ArrayList arrayListTableNames = new ArrayList();
                        arrayListTableNames.Add("Districts");                   // 0 //
                        arrayListTableNames.Add("Localities");                  // 1 //
                        arrayListTableNames.Add("Trees");                       // 2 //
                        arrayListTableNames.Add("Inspections");                 // 3 //
                        arrayListTableNames.Add("Actions");                     // 4 //
                        arrayListTableNames.Add("Picklist Headers");            // 5 //
                        arrayListTableNames.Add("Picklist Items");              // 6 //
                        arrayListTableNames.Add("Species Variety");             // 7 //
                        arrayListTableNames.Add("Species");                     // 8 //
                        arrayListTableNames.Add("Sequences");                   // 9 //
                        arrayListTableNames.Add("Sequence Fields");             // 10 //
                        arrayListTableNames.Add("Budgets");                     // 11 //
                        arrayListTableNames.Add("Company  Headers");            // 12 //
                        arrayListTableNames.Add("Contractors");                 // 13 //
                        arrayListTableNames.Add("District Map Links");          // 14 //
                        arrayListTableNames.Add("Equipment Masters");           // 15 //
                        arrayListTableNames.Add("Filter Headers");              // 16 //
                        arrayListTableNames.Add("Filter Details");              // 17 //
                        arrayListTableNames.Add("Hse Masters");                 // 18 //
                        arrayListTableNames.Add("Incidents");                   // 19 //
                        arrayListTableNames.Add("Job Equipment HSEs");          // 20 //
                        arrayListTableNames.Add("Job Masters");                 // 21 //
                        arrayListTableNames.Add("Job Rates");                   // 22 //
                        arrayListTableNames.Add("Job Rate Discounts");          // 23 //
                        arrayListTableNames.Add("Job Rate Parameters");         // 24 //
                        arrayListTableNames.Add("Site Considerations");         // 25 //
                        arrayListTableNames.Add("Legal Texts");                 // 26 //
                        arrayListTableNames.Add("Linked Documents");             // 27 //
                        arrayListTableNames.Add("Staff");                       // 28 //
                        arrayListTableNames.Add("Work Orders");                 // 29 //
                        arrayListTableNames.Add("Cost Centres");                // 30 //
                        arrayListTableNames.Add("Ownerships");                  // 31 // 
                        //arrayListTableNames.Add("Mapping Thermatic Headers");   // 32 // 
                        //arrayListTableNames.Add("Mapping Thematic Items");      // 33 // 
                        //arrayListTableNames.Add("Mapping Workspaces");          // 34 // 
                        //arrayListTableNames.Add("Mapping workspace Access");    // 35 // 
                        //arrayListTableNames.Add("Mapping Workspace Layer");     // 36 // 
                        //arrayListTableNames.Add("Mapping Workspace Label");     // 37 // 
                        //arrayListTableNames.Add("Mapping Workspace Structure"); // 38 // 

                        int intUpdateProgressThreshhold = arrayListTableNames.Count / 10;
                        int intUpdateProgressTempCount = 0;
                        string strErrorMessage = "";
                        foreach (string strTableName in arrayListTableNames)
                        {
                            fProgress.UpdateCaption("Checking Data Present - " + strTableName + "...");
                            Application.DoEvents();   // Allow Form time to repaint itself //
                            if (!System.IO.File.Exists(fbd.SelectedPath + "\\" + strTableName + ".XML")) strErrorMessage += strTableName + "\n";
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (strErrorMessage != "")
                        {
                            strErrorMessage = "Unable to Import from selected Folder - the following export files were missing...\n\n" + strErrorMessage + "\nTry choosing a different folder or re-export the data from your Tablet PC.";
                            DevExpress.XtraEditors.XtraMessageBox.Show(strErrorMessage, "Select Data Set to Import", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            edit.EditValue = null;
                            btnServer.Enabled = false;
                            return;
                        }
                        else
                        {
                            edit.EditValue = fbd.SelectedPath;
                            btnServer.Enabled = true;
                        }
                    }
                }
            }
        }

        private void btnServer_Click(object sender, EventArgs e)
        {
            if (checkEditExportToTablet.Checked)  // Export Data From Desktop to Tablet //
            {
                Export_Data_From_Desktop_To_XML_File();
            }
            else  // Import Data into Desktop from Tablet //
            {
                Import_Data_From_Tablet_XML_File();
            }
        }

        public void Export_Data_From_Desktop_To_XML_File()
        {            
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strMobileInterfaceExportFolder = "";
            try
            {
                strMobileInterfaceExportFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "MobileInterfaceExportFolder"));
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Desktop to Tablet Transfer File Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to export your server \\ desktop data to the " + strMobileInterfaceExportFolder + " folder.\n\nAre you sure you wish to proceed?", "Export Server \\ Desktop Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No) return;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Exporting Data...");
            fProgress.Show();
            Application.DoEvents();

            ArrayList arrayListTableNames = new ArrayList();
            arrayListTableNames.Add("Districts");                   // 0 //
            arrayListTableNames.Add("Localities");                  // 1 //
            arrayListTableNames.Add("Trees");                       // 2 //
            arrayListTableNames.Add("Inspections");                 // 3 //
            arrayListTableNames.Add("Actions");                     // 4 //
            arrayListTableNames.Add("Picklist Headers");            // 5 //
            arrayListTableNames.Add("Picklist Items");              // 6 //
            arrayListTableNames.Add("Species Variety");             // 7 //
            arrayListTableNames.Add("Species");                     // 8 //
            arrayListTableNames.Add("Sequences");                   // 9 //
            arrayListTableNames.Add("Sequence Fields");             // 10 //
            arrayListTableNames.Add("Budgets");                     // 11 //
            arrayListTableNames.Add("Company  Headers");            // 12 //
            arrayListTableNames.Add("Contractors");                 // 13 //
            arrayListTableNames.Add("District Map Links");          // 14 //
            arrayListTableNames.Add("Equipment Masters");           // 15 //
            arrayListTableNames.Add("Filter Headers");              // 16 //
            arrayListTableNames.Add("Filter Details");              // 17 //
            arrayListTableNames.Add("Hse Masters");                 // 18 //
            arrayListTableNames.Add("Incidents");                   // 19 //
            arrayListTableNames.Add("Job Equipment HSEs");          // 20 //
            arrayListTableNames.Add("Job Masters");                 // 21 //
            arrayListTableNames.Add("Job Rates");                   // 22 //
            arrayListTableNames.Add("Job Rate Discounts");          // 23 //
            arrayListTableNames.Add("Job Rate Parameters");         // 24 //
            arrayListTableNames.Add("Site Considerations");         // 25 //
            arrayListTableNames.Add("Legal Texts");                 // 26 //
            arrayListTableNames.Add("Linked Documents");            // 27 //
            arrayListTableNames.Add("Staff");                       // 28 //
            arrayListTableNames.Add("Work Orders");                 // 29 //
            arrayListTableNames.Add("Cost Centres");                // 30 //
            arrayListTableNames.Add("Ownerships");                  // 31 // 
            arrayListTableNames.Add("Mapping Thermatic Headers");   // 32 // 
            arrayListTableNames.Add("Mapping Thematic Items");      // 33 // 
            arrayListTableNames.Add("Mapping Workspaces");          // 34 // 
            arrayListTableNames.Add("Mapping Workspace Access");    // 35 // 
            arrayListTableNames.Add("Mapping Workspace Layer");     // 36 // 
            arrayListTableNames.Add("Mapping Workspace Label");     // 37 // 
            arrayListTableNames.Add("Mapping Workspace Structure"); // 38 // 
            arrayListTableNames.Add("Linked Pictures");             // 39 // 

            ArrayList arrayListStoredProcedureNames = new ArrayList();
            arrayListStoredProcedureNames.Add("sp02003_AT_Data_Interchange_Server_District_Data");
            arrayListStoredProcedureNames.Add("sp02002_AT_Data_Interchange_Server_Location_Data");
            arrayListStoredProcedureNames.Add("sp02001_AT_Data_Interchange_Server_Tree_Data");
            arrayListStoredProcedureNames.Add("sp02004_AT_Data_Interchange_Server_Inspection_Data");
            arrayListStoredProcedureNames.Add("sp02005_AT_Data_Interchange_Server_Action_Data");
            arrayListStoredProcedureNames.Add("sp02006_AT_Data_Interchange_Server_PickListHeaders_Data");
            arrayListStoredProcedureNames.Add("sp02007_AT_Data_Interchange_Server_PickListItems_Data");
            arrayListStoredProcedureNames.Add("sp02008_AT_Data_Interchange_Server_Species_Variety_Data");
            arrayListStoredProcedureNames.Add("sp02009_AT_Data_Interchange_Server_Species_Data");
            arrayListStoredProcedureNames.Add("sp02010_AT_Data_Interchange_Server_Sequences_Data");
            arrayListStoredProcedureNames.Add("sp02011_AT_Data_Interchange_Server_SequenceFields_Data");
            arrayListStoredProcedureNames.Add("sp02012_AT_Data_Interchange_Server_Budgets_Data");
            arrayListStoredProcedureNames.Add("sp02013_AT_Data_Interchange_Server_CompanyHeaders_Data");
            arrayListStoredProcedureNames.Add("sp02014_AT_Data_Interchange_Server_Contractors_Data");
            arrayListStoredProcedureNames.Add("sp02015_AT_Data_Interchange_Server_DistrictMapLinks_Data");
            arrayListStoredProcedureNames.Add("sp02016_AT_Data_Interchange_Server_EquipmentMasters_Data");
            arrayListStoredProcedureNames.Add("sp02017_AT_Data_Interchange_Server_FilterHeaders_Data");
            arrayListStoredProcedureNames.Add("sp02018_AT_Data_Interchange_Server_FilterDetails_Data");
            arrayListStoredProcedureNames.Add("sp02019_AT_Data_Interchange_Server_HseMasters_Data");
            arrayListStoredProcedureNames.Add("sp02020_AT_Data_Interchange_Server_Incidents_Data");
            arrayListStoredProcedureNames.Add("sp02021_AT_Data_Interchange_Server_JobEquipmentHses_Data");
            arrayListStoredProcedureNames.Add("sp02022_AT_Data_Interchange_Server_JobMasters_Data");
            arrayListStoredProcedureNames.Add("sp02023_AT_Data_Interchange_Server_JobRates_Data");
            arrayListStoredProcedureNames.Add("sp02024_AT_Data_Interchange_Server_JobRateDiscounts_Data");
            arrayListStoredProcedureNames.Add("sp02025_AT_Data_Interchange_Server_JobRateParameters_Data");
            arrayListStoredProcedureNames.Add("sp02027_AT_Data_Interchange_Server_LocalitySiteConsiderations_Data");
            arrayListStoredProcedureNames.Add("sp02028_AT_Data_Interchange_Server_LegalTexts_Data");
            arrayListStoredProcedureNames.Add("sp02029_AT_Data_Interchange_Server_Linked_Document_Data");
            arrayListStoredProcedureNames.Add("sp02030_AT_Data_Interchange_Server_Staff_Data");
            arrayListStoredProcedureNames.Add("sp02031_AT_Data_Interchange_Server_Workorders_Data");
            arrayListStoredProcedureNames.Add("sp02032_AT_Data_Interchange_Server_CostCentres_Data");
            arrayListStoredProcedureNames.Add("sp02033_AT_Data_Interchange_Server_Ownerships_Data");
            arrayListStoredProcedureNames.Add("sp02034_AT_Data_Interchange_Server_MappingTheamticSetHeader_Data");
            arrayListStoredProcedureNames.Add("sp02035_AT_Data_Interchange_Server_MappingThematicSetItem_Data");
            arrayListStoredProcedureNames.Add("sp02036_AT_Data_Interchange_Server_MappingWorkspace_Data");
            arrayListStoredProcedureNames.Add("sp02037_AT_Data_Interchange_Server_MappingWorkspaceAcess_Data");
            arrayListStoredProcedureNames.Add("sp02038_AT_Data_Interchange_Server_MappingWorkspaceLayer_Data");
            arrayListStoredProcedureNames.Add("sp02039_AT_Data_Interchange_Server_MappingWorkspaceLayerLabelFont_Data");
            arrayListStoredProcedureNames.Add("sp02040_AT_Data_Interchange_Server_MappingWorkspaceLayerLabelStructure_Data");
            arrayListStoredProcedureNames.Add("sp02068_AT_Data_Interchange_Server_Linked_Picture_Data");

            int intUpdateProgressThreshhold = arrayListTableNames.Count / 10;
            int intUpdateProgressTempCount = 0;

            SqlConnection SQlConn = new SqlConnection(strConnectionString);

            int intPosition = 0;
            foreach (string strTableName in arrayListTableNames)
	        {
	            fProgress.UpdateCaption("Exporting Data - " + strTableName + "...");
                Application.DoEvents();   // Allow Form time to repaint itself //

                SqlCommand cmd = null;
                cmd = new SqlCommand(arrayListStoredProcedureNames[intPosition].ToString(), SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (intPosition <= 4 || intPosition == 7 || intPosition == 19 || intPosition == 25 || intPosition == 27 || intPosition == 30 || intPosition == 39)
                {
                    string strParameter1 = "Desktop";
                    cmd.Parameters.Add(new SqlParameter("@strCalledBy", strParameter1));
                    if (intPosition == 3 || intPosition == 4)  // Inspections and Actions //
                    {
                        DateTime dtParameter1 = dateEditInspectionDateFilter.DateTime;
                        cmd.Parameters.Add(new SqlParameter("@FromDate", dtParameter1));
                    }
                }
                else if (intPosition == 29)  // Work Orders //
                {
                    int intParameter1 = 0;
                    switch (comboBoxEditWorkOrderFilter.EditValue.ToString().ToLower())
                    {
                        case "no work orders":
                            intParameter1 = 0;
                            break;
                        case "incomplete work orders only":
                            intParameter1 = 1;
                            break;
                        case "all work orders":
                            intParameter1 = 2;
                            break;
                        default:
                            break;
                    }
                    cmd.Parameters.Add(new SqlParameter("@ExportWhat", intParameter1));
                    string strParameter2 = "Desktop";
                    cmd.Parameters.Add(new SqlParameter("@strCalledBy", strParameter2));
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("NewDataSet");
                try
                {
                    da.Fill(ds, "Table");
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Read the " + strTableName + " data from the database [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SQlConn.Close();
                    SQlConn.Dispose();
                    return;
                }

                StreamWriter xmlDoc = new StreamWriter(strMobileInterfaceExportFolder + "\\" + strTableName + ".xml", false);  // Get a FileStream object //
                try
                {
                    ds.WriteXml(xmlDoc);  // Write XML document //
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Write the " + strTableName + ".XML file [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SQlConn.Close();
                    SQlConn.Dispose();
                    return;
                }
                xmlDoc.Close();

                intPosition++;

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            if (checkEditSyncLinkedDocumentsDesktop.Checked)
            {
                string strLinkedDocumentsFolder = "";
                try
                {
                    strLinkedDocumentsFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath"));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Linked Documents Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Export_Linked_Documents(strLinkedDocumentsFolder, strMobileInterfaceExportFolder, fProgress, "Export");
            }
            if (checkEditSyncPictures.Checked)
            {
                string strLinkedPicturesFolder = "";
                try
                {
                    strLinkedPicturesFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder"));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Linked Pictures Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Export_Linked_Pictures(strLinkedPicturesFolder, strMobileInterfaceExportFolder, fProgress, "Export");
            }
            
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            DevExpress.XtraEditors.XtraMessageBox.Show("Data successfully exported to the " + strMobileInterfaceExportFolder + " folder.\n\nRemember to import this data into your Tablet PC before you use it for data capture.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void Import_Data_From_Tablet_XML_File()
        {
            if (buttonEditChooseImportFolder.EditValue == null || buttonEditChooseImportFolder.EditValue.ToString() == "")  // In theory, this should never fire! //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Folder containing the data to be imported before proceeding.", "Import Tablet PC Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to Import your Tablet PC data into your Server \\ Desktop database.\n\nWARNING... WARNING... WARNING...\n\nThis process may update your data.\n\nAre you sure you wish to proceed?", "Import Tablet PC Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Loading Data...");
            fProgress.Show();
            Application.DoEvents();

            ArrayList arrayListTableNames = new ArrayList();
            arrayListTableNames.Add("Sequences");                   // 0 //
            arrayListTableNames.Add("Budgets");                     // 1 //
            arrayListTableNames.Add("Picklist Items");              // 2 //
            arrayListTableNames.Add("Ownerships");                  // 3 // 
            arrayListTableNames.Add("Cost Centres");                // 4 //
            arrayListTableNames.Add("Species");                     // 5 //
            arrayListTableNames.Add("Incidents");                   // 6 //
            arrayListTableNames.Add("Work Orders");                 // 7 //
            arrayListTableNames.Add("Districts");                   // 8 //
            arrayListTableNames.Add("Localities");                  // 9 //
            arrayListTableNames.Add("Site Considerations");         // 10 //
            arrayListTableNames.Add("Species Variety");             // 12 //
            arrayListTableNames.Add("Trees");                       // 11 //
            arrayListTableNames.Add("Inspections");                 // 13 //
            arrayListTableNames.Add("Actions");                     // 14 //
            arrayListTableNames.Add("Linked Documents");            // 15 //
            arrayListTableNames.Add("Linked Pictures");            // 16 //
            //arrayListTableNames.Add("Mapping Thermatic Headers");   // 17// 
            //arrayListTableNames.Add("Mapping Thematic Items");      // 18 // 
            //arrayListTableNames.Add("Mapping Workspaces");          // 19 // 
            //arrayListTableNames.Add("Mapping workspace Access");    // 20 // 
            //arrayListTableNames.Add("Mapping Workspace Layer");     // 21 // 
            //arrayListTableNames.Add("Mapping Workspace Label");     // 22 // 
            //arrayListTableNames.Add("Mapping Workspace Structure"); // 23 // 
            //arrayListTableNames.Add("Staff");                       // 24 //
            //arrayListTableNames.Add("Sequence Fields");             // 25 //
            //arrayListTableNames.Add("Picklist Headers");            // 26 //
            //arrayListTableNames.Add("Company  Headers");            // 27 //
            //arrayListTableNames.Add("Contractors");                 // 28 //
            //arrayListTableNames.Add("District Map Links");          // 29 //
            //arrayListTableNames.Add("Equipment Masters");           // 30 //
            //arrayListTableNames.Add("Hse Masters");                 // 31 //
            //arrayListTableNames.Add("Job Equipment HSEs");          // 32 //
            //arrayListTableNames.Add("Job Masters");                 // 33 //
            //arrayListTableNames.Add("Job Rates");                   // 34 //
            //arrayListTableNames.Add("Job Rate Discounts");          // 35 //
            //arrayListTableNames.Add("Job Rate Parameters");         // 36 //
            //arrayListTableNames.Add("Legal Texts");                 // 37 //

            int intUpdateProgressThreshhold = arrayListTableNames.Count / 10;
            int intUpdateProgressTempCount = 0;
            string strMobileInterfaceImportFolder = buttonEditChooseImportFolder.EditValue.ToString();

            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SQlConn.Open();
            int intPosition = 0;
            DataSet ds = new DataSet();
            foreach (string strTableName in arrayListTableNames)
            {
                fProgress.UpdateCaption("Loading Data - " + strTableName + "...");
                Application.DoEvents();   // Allow Form time to repaint itself //
                try
                {
                    DataSet dsTemp = new DataSet();
                    dsTemp.ReadXml(strMobileInterfaceImportFolder + "\\" + strTableName + ".xml", XmlReadMode.ReadSchema);
                    dsTemp.Tables[0].TableName = strTableName;
                    ds.Tables.Add(dsTemp.Tables[0].Copy());
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Read the " + strTableName + ".XML file [" + ex.Message + "]!\n\nTry running the import process again. If the problem persists contact Technical Support.", "Import Tablet PC Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SQlConn.Close();
                    SQlConn.Dispose();
                    return;
                }
                intPosition++;
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }

            // All data loaded into memory so process it and merge to back-end database //
            fProgress.SetProgressValue(0);
            fProgress.UpdateCaption("Importing Data...");
            Application.DoEvents();   // Allow Form time to repaint itself //

            ArrayList arrayListStoredProcNames = new ArrayList();
            arrayListStoredProcNames.Add("sp02041_AT_Data_Interchange_Server_Merge_Sequence_Data");                     // 0 //
            arrayListStoredProcNames.Add("sp02042_AT_Data_Interchange_Server_Merge_Budget_Data");                       // 1 //        
            arrayListStoredProcNames.Add("sp02043_AT_Data_Interchange_Server_Merge_Picklist_Data");                     // 2 //        
            arrayListStoredProcNames.Add("sp02044_AT_Data_Interchange_Server_Merge_Ownership_Data");                    // 3 //        
            arrayListStoredProcNames.Add("sp02045_AT_Data_Interchange_Server_Merge_CostCentre_Data");                   // 4 //        
            arrayListStoredProcNames.Add("sp02046_AT_Data_Interchange_Server_Merge_Species_Data");                      // 5 //        
            arrayListStoredProcNames.Add("sp02047_AT_Data_Interchange_Server_Merge_Incidents_Data");                    // 6 //        
            arrayListStoredProcNames.Add("sp02048_AT_Data_Interchange_Server_Merge_WorkOrders_Data");                   // 7 //        
            arrayListStoredProcNames.Add("sp02049_AT_Data_Interchange_Server_Merge_Districts_Data");                    // 8 //        
            arrayListStoredProcNames.Add("sp02050_AT_Data_Interchange_Server_Merge_Localities_Data");                   // 9 //        
            arrayListStoredProcNames.Add("sp02051_AT_Data_Interchange_Server_Merge_Locality_Site_Considerations_Data"); // 10 //        
            arrayListStoredProcNames.Add("sp02053_AT_Data_Interchange_Server_Merge_Species_Variety_Data");              // 11 //        
            arrayListStoredProcNames.Add("sp02052_AT_Data_Interchange_Server_Merge_Trees_Data");                        // 12 //        
            arrayListStoredProcNames.Add("sp02054_AT_Data_Interchange_Server_Merge_Inspections_Data");                  // 13 //        
            arrayListStoredProcNames.Add("sp02055_AT_Data_Interchange_Server_Merge_Actions_Data");                      // 14 //        
            arrayListStoredProcNames.Add("sp02056_AT_Data_Interchange_Server_Merge_Linked_Document_Data");              // 15 //        
            arrayListStoredProcNames.Add("sp02069_AT_Data_Interchange_Server_Merge_Linked_Pocture_Data");               // 16 //        

            int intTableLoop = 0;
            int intStoredProcReturnValue = 0;
            string strPath = strMobileInterfaceImportFolder + "\\";
            string strUser = this.GlobalSettings.Username;
            DataRow[] drFiltered;
            foreach (string strTableName in arrayListTableNames)
            {
                fProgress.UpdateCaption("Importing Data - " + strTableName + "...");
                Application.DoEvents();   // Allow Form time to repaint itself //

                SqlCommand cmd = null;
                cmd = new SqlCommand(arrayListStoredProcNames[intTableLoop].ToString(), SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                // Add paramaters to Stored Proc //
                foreach (DataColumn c in ds.Tables[intTableLoop].Columns)
                {
                    cmd.Parameters.Add(new SqlParameter("@" + c.ColumnName.Trim(), BaseObjects.ExtensionFunctions.ToSqlDbType(c.DataType), c.MaxLength));
                }
                cmd.Parameters.Add(new SqlParameter("@strPath", strPath));  // Extra parameter for import log //
                cmd.Parameters.Add(new SqlParameter("@strUser", strUser));  // Extra parameter for import log //
                // Merge Rows into Database... //
                foreach (DataRow r in ds.Tables[intTableLoop].Rows)
                {
                    foreach (DataColumn c in ds.Tables[intTableLoop].Columns)
                    {
                        cmd.Parameters["@" + c.ColumnName.Trim()].Value = r[c.ColumnName.Trim()];  // Populate parameters //
                    }
                    try
                    {
                        intStoredProcReturnValue = Convert.ToInt32(cmd.ExecuteScalar());// Fire Stored Proc to merge data //
                    }
                    catch (Exception ex)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to import " + strTableName + " data!\n\nError: [" + ex.Message + "]!\n\nTry running the import process again. If the problem persists contact Technical Support.", "Import Tablet PC Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        SQlConn.Close();
                        SQlConn.Dispose();
                        return;
                    }

                    if (intStoredProcReturnValue > 0 && intStoredProcReturnValue != Convert.ToInt32(cmd.Parameters[0].Value))  // Record's ID has changed during merge in SP so update any related records //
                    {
                        switch (strTableName)
                        {
                            case "Budgets":
                                drFiltered = ds.Tables["Cost Centres"].Select("intBudgetID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intBudgetID"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                /*
                                foreach (DataRow dr in ds.Tables["Cost Centres"].Rows)
                                {
                                    if (Convert.ToInt32(dr["intBudgetID"]) == Convert.ToInt32(cmd.Parameters[0].Value))
                                    {
                                        dr["intBudgetID"] = intStoredProcReturnValue;
                                        dr.AcceptChanges();
                                    }
                                }
                                */
                                break;
                            case "Picklist Items":
                                #region Picklist Switch
                                switch (Convert.ToInt32(cmd.Parameters[1].Value))  // column 1 = intHeaderID //
                                {
                                    case 101:  // Status List - Tree Table [intStatus] //
                                        drFiltered = ds.Tables["Trees"].Select("intStatus = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intStatus"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 103:  // Risk Categories - Tree Table [intSafetyPriority] , Inspection Table [intSafety] //
                                        drFiltered = ds.Tables["Trees"].Select("intSafetyPriority = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intSafetyPriority"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intSafety = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intSafety"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 104:  // General Conditions - Inspection Table [intGeneralCondition] //
                                        drFiltered = ds.Tables["Inspections"].Select("intGeneralCondition = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intGeneralCondition"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 105:  // Stem Physical Conditions - Inspection Table [intStemPhysical, intStemPhysical2, intStemPhysical3] //
                                        drFiltered = ds.Tables["Inspections"].Select("intStemPhysical = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intStemPhysical"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intStemPhysical2 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intStemPhysical2"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intStemPhysical3 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intStemPhysical3"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 106:  // Stem Diseases - Inspection Table [intStemDisease, intStemDisease2, intStemDisease3] //
                                        drFiltered = ds.Tables["Inspections"].Select("intStemDisease = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intStemDisease"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intStemDisease2 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intStemDisease2"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intStemDisease3 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intStemDisease3"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 107:  // Crown Physical Conditions - Inspection Table [intCrownPhysical, intCrownPhysical2, intCrownPhysical3] //
                                        drFiltered = ds.Tables["Inspections"].Select("intCrownPhysical = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intCrownPhysical"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intCrownPhysical2 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intCrownPhysical2"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intCrownPhysical3 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intCrownPhysical3"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 108:  // Crown Diseases - Inspection Table [intCrownDisease, intCrownDisease2, intCrownDisease3] //
                                        drFiltered = ds.Tables["Inspections"].Select("intCrownDisease = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intCrownDisease"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intCrownDisease2 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intCrownDisease2"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intCrownDisease3 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intCrownDisease3"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 109:  // Crown Foliation - Inspection Table [intCrownFoliation, intCrownFoliation2, intCrownFoliation3] //
                                        drFiltered = ds.Tables["Inspections"].Select("intCrownFoliation = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intCrownFoliation"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intCrownFoliation2 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intCrownFoliation2"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intCrownFoliation3 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intCrownFoliation3"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 110:  // Environment Category - Locality Table [intEnvironmentCategory] //
                                        drFiltered = ds.Tables["Localities"].Select("intEnvironmentCategory = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intEnvironmentCategory"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 111:  // Locality Types - Locality Table [intLocalityType] //
                                        drFiltered = ds.Tables["Localities"].Select("intLocalityType = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intLocalityType"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 112:  // Accessibility - Tree Table [intAccess] //
                                        drFiltered = ds.Tables["Trees"].Select("intAccess = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intAccess"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 113:  // Visibility - Tree Table [intVisibility] //
                                        drFiltered = ds.Tables["Trees"].Select("intVisibility = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intVisibility"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 114:  // Legal Status - Tree Table [intLegalStatus] //
                                        drFiltered = ds.Tables["Trees"].Select("intLegalStatus = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intLegalStatus"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 115:  // Ground Conditions - Tree Table [intGroundType] //
                                        drFiltered = ds.Tables["Trees"].Select("intGroundType = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intGroundType"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 116:  // Protection Types - Tree Table [intProtectionType] //
                                        drFiltered = ds.Tables["Trees"].Select("intProtectionType = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intProtectionType"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 118:  // Site Hazard Classes - Tree Table [intSiteHazardClass] //
                                        drFiltered = ds.Tables["Trees"].Select("intSiteHazardClass = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intSiteHazardClass"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 119:  // Plant Sizes - Tree Table [intPlantSize] //
                                        drFiltered = ds.Tables["Trees"].Select("intPlantSize = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intPlantSize"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 120:  // Plant Methods - Tree Table [intPlantMethod] //
                                        drFiltered = ds.Tables["Trees"].Select("intPlantMethod = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intPlantMethod"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 121:  // Plant Source - Tree Table [intPlantSource] //
                                        drFiltered = ds.Tables["Trees"].Select("intPlantSource = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intPlantSource"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 122:  // Sizes - Tree Table [intSize] //
                                        drFiltered = ds.Tables["Trees"].Select("intSize = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intSize"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 123:  // Age Classes - Tree Table [intAgeClass] //
                                        drFiltered = ds.Tables["Trees"].Select("intAgeClass = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intAgeClass"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 126:  // Job Priorities - WorkOrder Table [intPriorityID], Action Table [intPriorityID] //
                                        drFiltered = ds.Tables["Work Orders"].Select("intPriorityID = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intPriorityID"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 127:  // Issuing Bodies - WorkOrder Table [intIssuedByBodyID] //
                                        drFiltered = ds.Tables["Work Orders"].Select("intIssuedByBodyID = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intIssuedByBodyID"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 128:  // Hospital A & E Wards - District Table [intNearestHospital] //
                                        drFiltered = ds.Tables["Districts"].Select("intNearestHospital = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intNearestHospital"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 130:  // Locality Site Considerations - LocalitySiteConsiderationLink Table [intSiteConsiderationID] //
                                        drFiltered = ds.Tables["Site Considerations"].Select("intSiteConsiderationID = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intSiteConsiderationID"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 131:  // Incident Types - Incident Table [intIncidentTypeID] //
                                        drFiltered = ds.Tables["Incidents"].Select("intIncidentTypeID = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intIncidentTypeID"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 132:  // Nearby Objects - Tree Table [intNearbyObject, intNearbyObject2, intNearbyObject3] //
                                        drFiltered = ds.Tables["Trees"].Select("intNearbyObject = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intNearbyObject"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Trees"].Select("intNearbyObject2 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intNearbyObject2"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Trees"].Select("intNearbyObject3 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intNearbyObject3"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 133:  // Root Heave - Inspection Table [intRootHeave, intRootHeave2, intRootHeave3] //
                                        drFiltered = ds.Tables["Inspections"].Select("intRootHeave = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intRootHeave"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intRootHeave2 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intRootHeave2"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        drFiltered = ds.Tables["Inspections"].Select("intRootHeave3 = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intRootHeave3"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 135:  // Height Bands - Trees Table [intHeightRange] //
                                        drFiltered = ds.Tables["Districts"].Select("intHeightRange = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intHeightRange"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 136:  // DBH Bands - Tree Table [intDBHRange] //
                                        drFiltered = ds.Tables["Districts"].Select("intDBHRange = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intDBHRange"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    case 138:  // Vitalities - Inspection Table [intVitality] //
                                        drFiltered = ds.Tables["Inspections"].Select("intVitality = " + cmd.Parameters[0].Value.ToString());
                                        if (drFiltered.Length != 0)
                                        {
                                            foreach (DataRow fdr in drFiltered)
                                            {
                                                fdr["intVitality"] = intStoredProcReturnValue;
                                                fdr.AcceptChanges();
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                #endregion
                                break;
                            case "Ownerships":
                                drFiltered = ds.Tables["Cost Centres"].Select("intOwnershipID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intOwnershipID"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Districts"].Select("intOwnership = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intOwnership"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Localities"].Select("intOwnership = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intOwnership"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Trees"].Select("intOwnershipID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intOwnershipID"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Actions"].Select("intOwnershipID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intOwnershipID"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                break;
                            case "Cost Centres":
                                drFiltered = ds.Tables["Actions"].Select("intCostCentreID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intCostCentreID"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                break;
                            case "Species":
                                drFiltered = ds.Tables["Trees"].Select("intSpeciesID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intSpeciesID"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Species Variety"].Select("SpeciesID = " + cmd.Parameters[0].Value.ToString()+ " and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["SpeciesID"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                break;
                            case "Species Variety":
                                drFiltered = ds.Tables["Trees"].Select("SpeciesVariety = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["SpeciesVariety"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                break;
                            case "Incidents":
                                drFiltered = ds.Tables["Inspections"].Select("intIncidentID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intIncidentID"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Linked Documents"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 6 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Inspections"].Select("intIncidentID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intIncidentID"] = intStoredProcReturnValue;
                                        //  No UpdateFlag updating as it's only set if the Actions' parent Inspection has changed //
                                        fdr.AcceptChanges();
                                    }
                                }
                               break;
                            case "Work Orders":
                                drFiltered = ds.Tables["Actions"].Select("intWorkOrderID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intWorkOrderID"] = intStoredProcReturnValue;
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Linked Documents"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 7 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Actions"].Select("intWorkOrderID = " + cmd.Parameters[0].Value.ToString());
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intWorkOrderID"] = intStoredProcReturnValue;
                                        //  No UpdateFlag updating as it's only set if the Actions' parent Inspection has changed //
                                        fdr.AcceptChanges();
                                    }
                                }
                                break;
                            case "Districts":
                                drFiltered = ds.Tables["Localities"].Select("intDistrictID = " + cmd.Parameters[0].Value.ToString() + " and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intDistrictID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Linked Documents"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 1 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                break;
                            case "Localities":
                                drFiltered = ds.Tables["Trees"].Select("intLocalityID = " + cmd.Parameters[0].Value.ToString() + " and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intLocalityID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Site Considerations"].Select("intLocalityID = " + cmd.Parameters[0].Value.ToString() + " and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intLocalityID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Linked Documents"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 2 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                break;
                            case "Trees":
                                drFiltered = ds.Tables["Inspections"].Select("intTreeID = " + cmd.Parameters[0].Value.ToString() + " and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intTreeID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Linked Documents"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 3 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Linked Pictures"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 101 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                               break;
                            case "Inspections":
                                drFiltered = ds.Tables["Actions"].Select("intInspectionID = " + cmd.Parameters[0].Value.ToString() + " and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["intInspectionID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Linked Documents"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 4 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Linked Pictures"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 102 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                break;
                            case "Actions":
                                drFiltered = ds.Tables["Linked Documents"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 5 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                drFiltered = ds.Tables["Linked Pictures"].Select("LinkedToRecordID = " + cmd.Parameters[0].Value.ToString() + " and LinkedToRecordTypeID = 103 and UpdateFlag = 0");
                                if (drFiltered.Length != 0)
                                {
                                    foreach (DataRow fdr in drFiltered)
                                    {
                                        fdr["LinkedToRecordID"] = intStoredProcReturnValue;
                                        fdr["UpdateFlag"] = 1;  // UpdateFlag = 1 stops this record being subsequently updated again //
                                        fdr.AcceptChanges();
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
                intTableLoop++;
            }

            if (checkEditSyncLinkedDocumentsDesktop.Checked)
            {
                string strLinkedDocumentsFolder = "";
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strLinkedDocumentsFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath"));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Linked Documents Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Export_Linked_Documents(strMobileInterfaceImportFolder, strLinkedDocumentsFolder, fProgress, "Import");
            }
            if (checkEditSyncPictures.Checked)
            {
                string strLinkedPicturesFolder = "";
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strLinkedPicturesFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder"));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Linked Pictures Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Export_Linked_Pictures(strMobileInterfaceImportFolder, strLinkedPicturesFolder, fProgress, "Import");
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            SQlConn.Close();
            SQlConn.Dispose();
            DevExpress.XtraEditors.XtraMessageBox.Show("Data Imported successfully.", "Import Tablet PC Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion


        #region Tablet PC Version

        private void checkEditImportFromDesktop_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                btnTablet.Text = "Import From Server \\ Desktop PC";
                checkEditSyncLinkedDocumentsTablet.Text = "Import <b>Linked Document Files</b>";
                checkEditSyncLinkedDocumentsTablet.Checked = true;
                checkEditSyncLinkedPicturesTablet.Text = "Import <b>Linked Picture Files</b>";
                checkEditSyncLinkedPicturesTablet.Checked = true;
            }
            btnTablet.Enabled = true;
            checkEditSyncMaps.Visible = true;
            checkEditSyncLinkedDocumentsTablet.Visible = true;
            checkEditSyncLinkedPicturesTablet.Visible = true;
        }

        private void checkEditExportToDesktop_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                btnTablet.Text = "Export To Server \\ Desktop PC";
                checkEditSyncLinkedDocumentsTablet.Text = "Export <b>Linked Document Files</b>";
                checkEditSyncLinkedDocumentsTablet.Checked = true;
                checkEditSyncLinkedPicturesTablet.Text = "Export <b>Linked Picture Files</b>";
                checkEditSyncLinkedPicturesTablet.Checked = true;
            }
            btnTablet.Enabled = true;
            checkEditSyncMaps.Visible = false;
            checkEditSyncLinkedDocumentsTablet.Visible = true;
            checkEditSyncLinkedPicturesTablet.Visible = true;
        }

        private void btnTablet_Click(object sender, EventArgs e)
        {
            if (checkEditImportFromDesktop.Checked)  // Import Data From Desktop //
            {
                Import_Data_From_Desktop_XML_File();
            }
            else  // Export data from Tablet to Desktop //
            {
                Export_Data_From_Tablet_To_XML_File();
            }
        }

        public void Import_Data_From_Desktop_XML_File()
        {
            string strMobileInterfaceExportFolder;
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to Import your server \\ desktop data into your Tablet PC.\n\nWARNING... WARNING... WARNING...\n\nAll existing data on your Tablet PC will be cleared first!\n\nAre you sure you wish to proceed?", "Import Server \\ Desktop Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strMobileInterfaceExportFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "MobileInterfaceExportFolder"));
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Desktop to Tablet Transfer File Location' Path [" + ex.Message + "]!\n\nTry running the import process again. If the problem persists contact Technical Support.", "Import Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Removing Existing Data...");
            fProgress.Show();
            Application.DoEvents();

            // Empty existing data from database //
            try
            {
                DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter TruncateData = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                TruncateData.ChangeConnectionString(strConnectionString);
                TruncateData.sp02000_AT_Data_Interchange_Tablet_Empty_DB((checkEditSyncMaps.Checked ? 1 : 0));
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to removing the existing data [" + ex.Message + "]!\n\nTry running the import process again. If the problem persists contact Technical Support.", "Import Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            ArrayList arrayListTableNames = new ArrayList();
            arrayListTableNames.Add("Staff");                       // 0 //
            arrayListTableNames.Add("Districts");                   // 1 //
            arrayListTableNames.Add("Localities");                  // 2 //
            arrayListTableNames.Add("Trees");                       // 3 //
            arrayListTableNames.Add("Inspections");                 // 4 //
            arrayListTableNames.Add("Actions");                     // 5 //
            arrayListTableNames.Add("Picklist Headers");            // 6 //
            arrayListTableNames.Add("Picklist Items");              // 7 //
            arrayListTableNames.Add("Species Variety");             // 8 //
            arrayListTableNames.Add("Species");                     // 9 //
            arrayListTableNames.Add("Sequences");                   // 10 //
            arrayListTableNames.Add("Sequence Fields");             // 11 //
            arrayListTableNames.Add("Budgets");                     // 12 //
            arrayListTableNames.Add("Company  Headers");            // 13 //
            arrayListTableNames.Add("Contractors");                 // 14 //
            arrayListTableNames.Add("District Map Links");          // 15 //
            arrayListTableNames.Add("Equipment Masters");           // 16 //
            arrayListTableNames.Add("Filter Headers");              // 17 //
            arrayListTableNames.Add("Filter Details");              // 18 //
            arrayListTableNames.Add("Hse Masters");                 // 19 //
            arrayListTableNames.Add("Incidents");                   // 20 //
            arrayListTableNames.Add("Job Equipment HSEs");          // 21 //
            arrayListTableNames.Add("Job Masters");                 // 22 //
            arrayListTableNames.Add("Job Rates");                   // 23 //
            arrayListTableNames.Add("Job Rate Discounts");          // 24 //
            arrayListTableNames.Add("Job Rate Parameters");         // 25 //
            arrayListTableNames.Add("Site Considerations");         // 26 //
            arrayListTableNames.Add("Legal Texts");                 // 27 //
            arrayListTableNames.Add("Linked Documents");            // 28 //
            arrayListTableNames.Add("Work Orders");                 // 29 //
            arrayListTableNames.Add("Cost Centres");                // 30 //
            arrayListTableNames.Add("Ownerships");                  // 31 // 
            arrayListTableNames.Add("Linked Pictures");             // 32 // 
            if (checkEditSyncMaps.Checked)
            {
                arrayListTableNames.Add("Mapping Thermatic Headers");   // 33 // 
                arrayListTableNames.Add("Mapping Thematic Items");      // 34 // 
                arrayListTableNames.Add("Mapping Workspaces");          // 35 // 
                arrayListTableNames.Add("Mapping Workspace Access");    // 36 // 
                arrayListTableNames.Add("Mapping Workspace Layer");     // 37 // 
                arrayListTableNames.Add("Mapping Workspace Label");     // 38 // 
                arrayListTableNames.Add("Mapping Workspace Structure"); // 39 // 
            }

            ArrayList arrayListActualTableNames = new ArrayList();
            arrayListActualTableNames.Add("tblStaff");
            arrayListActualTableNames.Add("tblDistrict");
            arrayListActualTableNames.Add("tblLocality");
            arrayListActualTableNames.Add("tblTree");
            arrayListActualTableNames.Add("tblInspection");
            arrayListActualTableNames.Add("tblAction");
            arrayListActualTableNames.Add("tblPickListHeader");
            arrayListActualTableNames.Add("tblPickListItem");
            arrayListActualTableNames.Add("Species_Variety");
            arrayListActualTableNames.Add("tblSpecies");
            arrayListActualTableNames.Add("tblSequence");
            arrayListActualTableNames.Add("tblSequenceFields");
            arrayListActualTableNames.Add("tblBudget");
            arrayListActualTableNames.Add("tblCompanyHeader");
            arrayListActualTableNames.Add("tblContractor");
            arrayListActualTableNames.Add("tblDistrictMapLink");
            arrayListActualTableNames.Add("tblEquipmentMaster");
            arrayListActualTableNames.Add("tblFilterHeader");
            arrayListActualTableNames.Add("tblFilterDetail");
            arrayListActualTableNames.Add("tblHSEMaster");
            arrayListActualTableNames.Add("tblIncident");
            arrayListActualTableNames.Add("tblJobEquipmentHSE");
            arrayListActualTableNames.Add("tblJobMaster");
            arrayListActualTableNames.Add("tblJobRate");
            arrayListActualTableNames.Add("tblJobRateDiscount");
            arrayListActualTableNames.Add("tblJobRateParameter");
            arrayListActualTableNames.Add("tblLocalitySiteConsiderationLink");
            arrayListActualTableNames.Add("tblLegalText");
            arrayListActualTableNames.Add("Linked_Document");
            arrayListActualTableNames.Add("tblWorkOrder");
            arrayListActualTableNames.Add("tblCostCentre");
            arrayListActualTableNames.Add("tblOwnership");
            arrayListActualTableNames.Add("UT_Survey_Picture");
            if (checkEditSyncMaps.Checked)
            {
                arrayListActualTableNames.Add("Mapping_Thematic_Set_Header");
                arrayListActualTableNames.Add("Mapping_Thematic_Set_Item");
                arrayListActualTableNames.Add("Mapping_Workspace");
                arrayListActualTableNames.Add("Mapping_Workspace_Access");
                arrayListActualTableNames.Add("Mapping_Workspace_Layer");
                arrayListActualTableNames.Add("Mapping_Workspace_Layer_Label_Font");
                arrayListActualTableNames.Add("Mapping_Workspace_Layer_Label_Structure");
            }

            int intUpdateProgressThreshhold = arrayListTableNames.Count / 10;
            int intUpdateProgressTempCount = 0;

            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SQlConn.Open();

            int intPosition = 0;
            foreach (string strTableName in arrayListTableNames)
            {
                fProgress.UpdateCaption("Importing Data - " + strTableName + "...");
                Application.DoEvents();   // Allow Form time to repaint itself //

                DataSet ds = new DataSet();
                try
                {
                    ds.ReadXml(strMobileInterfaceExportFolder + "\\" + strTableName + ".xml");
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Read the " + strTableName + ".XML file [" + ex.Message + "]!\n\nTry running the import process again. If the problem persists contact Technical Support.", "Import Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SQlConn.Close();
                    SQlConn.Dispose();
                    return;
                }

                using (SqlBulkCopy sbc = new SqlBulkCopy(strConnectionString, SqlBulkCopyOptions.KeepIdentity))
                {
                    sbc.DestinationTableName = arrayListActualTableNames[intPosition].ToString();

                    // if your DB col names don�t match your XML element names 100% or columns are missing then relate the source XML elements (1st param) with the destination DB cols //
                    sbc.ColumnMappings.Clear();
                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataColumn c in ds.Tables[0].Columns)
                        {
                            sbc.ColumnMappings.Add(c.ColumnName.Trim(), c.ColumnName.Trim());
                        }

                        try
                        {
                            sbc.WriteToServer(ds.Tables[0]);
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Import the " + strTableName + ".XML file [" + ex.Message + "]!\n\nTry running the import process again. If the problem persists contact Technical Support.", "Import Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            SQlConn.Close();
                            SQlConn.Dispose();
                            return;
                        }
                    }
                }
                intPosition++;

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }

            if (checkEditSyncMaps.Checked)  // Copy Mapping Files from Desktop location to Tablet location //
            {
                fProgress.SetProgressValue(0);
                fProgress.UpdateCaption("Synchronizing Mapping Files...");
                Application.DoEvents();   // Allow Form time to repaint itself //

                // Get Paths //
                string strMappingBackgoundFolderDesktop;
                string strMappingBackgoundFolderTablet;
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    strMappingBackgoundFolderDesktop = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesMapBackgroundFolder").ToString();
                    strMappingBackgoundFolderTablet = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesMapBackgroundFolderTablet").ToString();
                }
                catch (Exception)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the folders specified in the System Settings table for the Tablet and Desktop Map Background Folders!\n\nCheck the paths are correct in the System Settings screen before proceeding or contact Technical Support for further information.", "Import Server \\ Desktop Data - Syncronize Map Background Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SQlConn.Close();
                    SQlConn.Dispose();
                    return;
                }
                // Check Paths are valid //
                if (!System.IO.Directory.Exists(strMappingBackgoundFolderDesktop))
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                        SQlConn.Close();
                        SQlConn.Dispose();
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("The path specified in the System Settings table for the Desktop Map Background Folder is invalid!\n\nCorrect the path using the System Settings screen before proceeding or contact Technical Support for further information.", "Import Server \\ Desktop Data - Synchronize Map Background Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                if (!System.IO.Directory.Exists(strMappingBackgoundFolderTablet))
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                        SQlConn.Close();
                        SQlConn.Dispose();
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("The path specified in the System Settings table for the Tablet Map Background Folder is invalid!\n\nCorrect the path using the System Settings screen before proceeding or contact Technical Support for further information.", "Import Server \\ Desktop Data - Synchronize Map Background Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                // Copy files from Desktop location to Tablet location //
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp02057_AT_Data_Interchange_Tablet_Sync_File_List", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("NewDataSet");
                try
                {
                    da.Fill(ds, "Table");
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Read the Mapping Workspace Layers data from the database [" + ex.Message + "]!\n\nTry running the import process again. If the problem persists contact Technical Support.", "Import Server \\ Desktop Data - Synchronize Map Background Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SQlConn.Close();
                    SQlConn.Dispose();
                    return;
                }
                string strFileNameSource, strFilename, strFilenameMiddle;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    intUpdateProgressThreshhold = ds.Tables[0].Rows.Count / 10;
                    intUpdateProgressTempCount = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        strFilename = dr["LayerPath"].ToString();
                        strFileNameSource = strMappingBackgoundFolderDesktop + strFilename;
                        strFilenameMiddle = Path.GetDirectoryName(strFilename);
                        //strFileNameDestination = strMappingBackgoundFolderTablet + strFilename;

                        fProgress.UpdateCaption("Synchronizing Mapping Files - " + strFilename + "...");
                        Application.DoEvents();   // Allow Form time to repaint itself //

                        // Process all files with the same name (but different extensions) //
                        DirectoryInfo dir = new DirectoryInfo(Path.GetDirectoryName(strFileNameSource));
                        FileInfo[] files = dir.GetFiles(Path.GetFileNameWithoutExtension(strFilename) + ".*", SearchOption.TopDirectoryOnly);

                        foreach (FileInfo fi in dir.GetFiles(Path.GetFileNameWithoutExtension(strFilename) + ".*", SearchOption.TopDirectoryOnly))
                        {
                            try
                            {
                                fi.CopyTo(strMappingBackgoundFolderTablet + strFilenameMiddle + "\\" + fi.Name, true);
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to synchronize the Mapping File " + strFilename + " [" + ex.Message + "]!\n\nTry running the import process again. If the problem persists contact Technical Support.", "Import Server \\ Desktop Data - Synchronize Map Background Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                SQlConn.Close();
                                SQlConn.Dispose();
                                return;
                            }
                        }
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                    }
                }
            }

            if (checkEditSyncLinkedDocumentsTablet.Checked)
            {
                string strLinkedDocumentsFolder = "";
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strLinkedDocumentsFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath"));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Linked Documents Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Export_Linked_Documents(strMobileInterfaceExportFolder, strLinkedDocumentsFolder, fProgress, "Import");
            }

            if (checkEditSyncLinkedPicturesTablet.Checked)
            {
                string strLinkedPicturesFolder = "";
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strLinkedPicturesFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder"));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Linked Pictures Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Export_Linked_Pictures(strMobileInterfaceExportFolder, strLinkedPicturesFolder, fProgress, "Import");
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            SQlConn.Close();
            SQlConn.Dispose();
            DevExpress.XtraEditors.XtraMessageBox.Show("Data Imported successfully.", "Import Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void Export_Data_From_Tablet_To_XML_File()
        {
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strMobileInterfaceExportFolder = "";
            try
            {
                strMobileInterfaceExportFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "MobileInterfaceImportFolder"));
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Tablet to Desktop Transfer File Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Tablet Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to export your tablet data to the " + strMobileInterfaceExportFolder + " folder.\n\nAre you sure you wish to proceed?", "Export Tablet Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No) return;

            // Create directory to hold exported data //
            strMobileInterfaceExportFolder += "\\" + this.GlobalSettings.Username + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm");
            try
            {
                if (!System.IO.Directory.Exists(strMobileInterfaceExportFolder)) System.IO.Directory.CreateDirectory(strMobileInterfaceExportFolder);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create a new export folder: " + strMobileInterfaceExportFolder + " [" + ex.Message + "]!\n\nyou may not have sufficient access rights to create folders - see your Network Administrator for help.", "Export Tablet Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Exporting Data...");
            fProgress.Show();
            Application.DoEvents();

            ArrayList arrayListTableNames = new ArrayList();
            arrayListTableNames.Add("Districts");                   // 0 //
            arrayListTableNames.Add("Localities");                  // 1 //
            arrayListTableNames.Add("Trees");                       // 2 //
            arrayListTableNames.Add("Inspections");                 // 3 //
            arrayListTableNames.Add("Actions");                     // 4 //
            arrayListTableNames.Add("Picklist Headers");            // 5 //
            arrayListTableNames.Add("Picklist Items");              // 6 //
            arrayListTableNames.Add("Species Variety");             // 7 //
            arrayListTableNames.Add("Species");                     // 8 //
            arrayListTableNames.Add("Sequences");                   // 9 //
            arrayListTableNames.Add("Sequence Fields");             // 10 //
            arrayListTableNames.Add("Budgets");                     // 11 //
            arrayListTableNames.Add("Company  Headers");            // 12 //
            arrayListTableNames.Add("Contractors");                 // 13 //
            arrayListTableNames.Add("District Map Links");          // 14 //
            arrayListTableNames.Add("Equipment Masters");           // 15 //
            arrayListTableNames.Add("Filter Headers");              // 16 //
            arrayListTableNames.Add("Filter Details");              // 17 //
            arrayListTableNames.Add("Hse Masters");                 // 18 //
            arrayListTableNames.Add("Incidents");                   // 19 //
            arrayListTableNames.Add("Job Equipment HSEs");          // 20 //
            arrayListTableNames.Add("Job Masters");                 // 21 //
            arrayListTableNames.Add("Job Rates");                   // 22 //
            arrayListTableNames.Add("Job Rate Discounts");          // 23 //
            arrayListTableNames.Add("Job Rate Parameters");         // 24 //
            arrayListTableNames.Add("Site Considerations");         // 25 //
            arrayListTableNames.Add("Legal Texts");                 // 26 //
            arrayListTableNames.Add("Linked Documents");            // 27 //
            arrayListTableNames.Add("Staff");                       // 28 //
            arrayListTableNames.Add("Work Orders");                 // 29 //
            arrayListTableNames.Add("Cost Centres");                // 30 //
            arrayListTableNames.Add("Ownerships");                  // 31 // 
            arrayListTableNames.Add("Linked Pictures");             // 32 // 
            //arrayListTableNames.Add("Mapping Thermatic Headers");   // 33 // 
            //arrayListTableNames.Add("Mapping Thematic Items");      // 34 // 
            //arrayListTableNames.Add("Mapping Workspaces");          // 35 // 
            //arrayListTableNames.Add("Mapping workspace Access");    // 36 // 
            //arrayListTableNames.Add("Mapping Workspace Layer");     // 37 // 
            //arrayListTableNames.Add("Mapping Workspace Label");     // 38 // 
            //arrayListTableNames.Add("Mapping Workspace Structure"); // 39 // 

            ArrayList arrayListStoredProcedureNames = new ArrayList();
            arrayListStoredProcedureNames.Add("sp02003_AT_Data_Interchange_Server_District_Data");
            arrayListStoredProcedureNames.Add("sp02002_AT_Data_Interchange_Server_Location_Data");
            arrayListStoredProcedureNames.Add("sp02001_AT_Data_Interchange_Server_Tree_Data");
            arrayListStoredProcedureNames.Add("sp02004_AT_Data_Interchange_Server_Inspection_Data");
            arrayListStoredProcedureNames.Add("sp02005_AT_Data_Interchange_Server_Action_Data");
            arrayListStoredProcedureNames.Add("sp02006_AT_Data_Interchange_Server_PickListHeaders_Data");
            arrayListStoredProcedureNames.Add("sp02007_AT_Data_Interchange_Server_PickListItems_Data");
            arrayListStoredProcedureNames.Add("sp02008_AT_Data_Interchange_Server_Species_Variety_Data");
            arrayListStoredProcedureNames.Add("sp02009_AT_Data_Interchange_Server_Species_Data");
            arrayListStoredProcedureNames.Add("sp02010_AT_Data_Interchange_Server_Sequences_Data");
            arrayListStoredProcedureNames.Add("sp02011_AT_Data_Interchange_Server_SequenceFields_Data");
            arrayListStoredProcedureNames.Add("sp02012_AT_Data_Interchange_Server_Budgets_Data");
            arrayListStoredProcedureNames.Add("sp02013_AT_Data_Interchange_Server_CompanyHeaders_Data");
            arrayListStoredProcedureNames.Add("sp02014_AT_Data_Interchange_Server_Contractors_Data");
            arrayListStoredProcedureNames.Add("sp02015_AT_Data_Interchange_Server_DistrictMapLinks_Data");
            arrayListStoredProcedureNames.Add("sp02016_AT_Data_Interchange_Server_EquipmentMasters_Data");
            arrayListStoredProcedureNames.Add("sp02017_AT_Data_Interchange_Server_FilterHeaders_Data");
            arrayListStoredProcedureNames.Add("sp02018_AT_Data_Interchange_Server_FilterDetails_Data");
            arrayListStoredProcedureNames.Add("sp02019_AT_Data_Interchange_Server_HseMasters_Data");
            arrayListStoredProcedureNames.Add("sp02020_AT_Data_Interchange_Server_Incidents_Data");
            arrayListStoredProcedureNames.Add("sp02021_AT_Data_Interchange_Server_JobEquipmentHses_Data");
            arrayListStoredProcedureNames.Add("sp02022_AT_Data_Interchange_Server_JobMasters_Data");
            arrayListStoredProcedureNames.Add("sp02023_AT_Data_Interchange_Server_JobRates_Data");
            arrayListStoredProcedureNames.Add("sp02024_AT_Data_Interchange_Server_JobRateDiscounts_Data");
            arrayListStoredProcedureNames.Add("sp02025_AT_Data_Interchange_Server_JobRateParameters_Data");
            arrayListStoredProcedureNames.Add("sp02027_AT_Data_Interchange_Server_LocalitySiteConsiderations_Data");
            arrayListStoredProcedureNames.Add("sp02028_AT_Data_Interchange_Server_LegalTexts_Data");
            arrayListStoredProcedureNames.Add("sp02029_AT_Data_Interchange_Server_Linked_Document_Data");
            arrayListStoredProcedureNames.Add("sp02030_AT_Data_Interchange_Server_Staff_Data");
            arrayListStoredProcedureNames.Add("sp02031_AT_Data_Interchange_Server_Workorders_Data");
            arrayListStoredProcedureNames.Add("sp02032_AT_Data_Interchange_Server_CostCentres_Data");
            arrayListStoredProcedureNames.Add("sp02033_AT_Data_Interchange_Server_Ownerships_Data");
            arrayListStoredProcedureNames.Add("sp02068_AT_Data_Interchange_Server_Linked_Picture_Data");
            //arrayListStoredProcedureNames.Add("sp02034_AT_Data_Interchange_Server_MappingTheamticSetHeader_Data");
            //arrayListStoredProcedureNames.Add("sp02035_AT_Data_Interchange_Server_MappingThematicSetItem_Data");
            //arrayListStoredProcedureNames.Add("sp02036_AT_Data_Interchange_Server_MappingWorkspace_Data");
            //arrayListStoredProcedureNames.Add("sp02037_AT_Data_Interchange_Server_MappingWorkspaceAcess_Data");
            //arrayListStoredProcedureNames.Add("sp02038_AT_Data_Interchange_Server_MappingWorkspaceLayer_Data");
            //arrayListStoredProcedureNames.Add("sp02039_AT_Data_Interchange_Server_MappingWorkspaceLayerLabelFont_Data");
            //arrayListStoredProcedureNames.Add("sp02040_AT_Data_Interchange_Server_MappingWorkspaceLayerLabelStructure_Data");

            int intUpdateProgressThreshhold = arrayListTableNames.Count / 10;
            int intUpdateProgressTempCount = 0;

            SqlConnection SQlConn = new SqlConnection(strConnectionString);

            int intPosition = 0;
            foreach (string strTableName in arrayListTableNames)
            {
                fProgress.UpdateCaption("Exporting Data - " + strTableName + "...");
                Application.DoEvents();   // Allow Form time to repaint itself //

                SqlCommand cmd = null;
                cmd = new SqlCommand(arrayListStoredProcedureNames[intPosition].ToString(), SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (intPosition <= 4 || intPosition == 7 || intPosition == 19 || intPosition == 25 || intPosition == 27 || intPosition == 30 || intPosition == 32)
                {
                    string strParameter1 = "Tablet";
                    cmd.Parameters.Add(new SqlParameter("@strCalledBy", strParameter1));
                    if (intPosition == 3 || intPosition == 4)  // Inspections and Actions //
                    {
                        DateTime dtParameter1 = DateTime.Today;  // Dummy value [not used by Stored Procedure when in Tablet mode] //
                        cmd.Parameters.Add(new SqlParameter("@FromDate", dtParameter1));
                    }
                }
                else if (intPosition == 29)  // Work Orders //
                {
                    int intParameter1 = 2;  // 2 = all work orders //
                    //string strParameter2 = "Desktop";
                    string strParameter2 = "Tablet";
                    cmd.Parameters.Add(new SqlParameter("@ExportWhat", intParameter1));
                    cmd.Parameters.Add(new SqlParameter("@strCalledBy", strParameter2));
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("NewDataSet");
                try
                {
                    da.Fill(ds, "Table");
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Read the " + strTableName + " data from the database [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Tablet Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SQlConn.Close();
                    SQlConn.Dispose();
                    return;
                }

                StreamWriter xmlDoc = new StreamWriter(strMobileInterfaceExportFolder + "\\" + strTableName + ".xml", false);  // Get a FileStream object //
                try
                {
                    ds.WriteXml(xmlDoc, System.Data.XmlWriteMode.WriteSchema);  // Write XML document with Schema //
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Write the " + strTableName + ".XML file [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Tablet Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    SQlConn.Close();
                    SQlConn.Dispose();
                    return;
                }
                xmlDoc.Close();

                intPosition++;

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }

            if (checkEditSyncLinkedDocumentsTablet.Checked)
            {
                string strLinkedDocumentsFolder = "";
                try
                {
                    strLinkedDocumentsFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath"));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Linked Documents Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Export_Linked_Documents(strLinkedDocumentsFolder, strMobileInterfaceExportFolder, fProgress, "Export");
            }

            if (checkEditSyncLinkedPicturesTablet.Checked)
            {
                string strLinkedPicturesFolder = "";
                try
                {
                    strLinkedPicturesFolder = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder"));
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to locate the 'Linked Pictures Location' Path [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Server \\ Desktop Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Export_Linked_Pictures(strLinkedPicturesFolder, strMobileInterfaceExportFolder, fProgress, "Export");
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            DevExpress.XtraEditors.XtraMessageBox.Show("Data successfully exported to the " + strMobileInterfaceExportFolder + " folder.\n\nRemember to import this data into your Desktop \\ Server PC.", "Export Tablet Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion


        private void Export_Linked_Documents(string strOriginalPath, string strNewPath, frmProgress fProgress, string strCaller)
        {
            if (strCaller == "Export")
            {
                strOriginalPath += (!strOriginalPath.EndsWith("\\") ? "\\" : "");
                strNewPath += (!strNewPath.EndsWith("\\") ? "\\" : "") + "Linked_Documents\\";
            }
            else  // Import //
            {
                strOriginalPath += (!strOriginalPath.EndsWith("\\") ? "\\" : "") + "Linked_Documents\\";
                strNewPath += (!strNewPath.EndsWith("\\") ? "\\" : "");
            }

          // Copy files from Desktop location to Tablet location //
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp02058_AT_Data_Interchange_Tablet_Sync_Linked_Docs_List", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet("NewDataSet");
            try
            {
                da.Fill(ds, "Table");
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Read the Linked Documents data from the database [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Data - Export Linked Documents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                SQlConn.Close();
                SQlConn.Dispose();
                return;
            }
            string strFilenameSource, strFilenameNew, strFilename, strDirectoryStructure;
            if (ds.Tables[0].Rows.Count > 0)
            {
                int intUpdateProgressThreshhold = ds.Tables[0].Rows.Count / 10;
                int intUpdateProgressTempCount = 0;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    strFilename = dr["DocumentPath"].ToString();
                    strFilenameSource = strOriginalPath + strFilename;
                    strFilenameNew = strNewPath + strFilename;

                    fProgress.UpdateCaption("Exporting Linked Documents - " + strFilename + "...");
                    Application.DoEvents();   // Allow Form time to repaint itself //

                    FileInfo f1 = new FileInfo(strFilenameSource);
                    if (!f1.Exists) continue;  // Abort copy of this file and move to next //

                    FileInfo f2 = new FileInfo(strFilenameNew);
                    if (f2.Exists)
                    {
                        // Check if file size or last modified date has changed //
                        if (f1.Length == f2.Length && f1.LastWriteTime == f2.LastWriteTime) continue;  // Abort copy of this file and move to next //
                    }
                    try
                    {
                        // Check if a directory structure to hold the new document is required //
                        strDirectoryStructure = f2.DirectoryName;
                        if (!Directory.Exists(strDirectoryStructure))
                        {
                            try
                            {
                                Directory.CreateDirectory(strDirectoryStructure);
                            }
                            catch (Exception)
                            {
                            /*    if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the directory structure for Linked Document " + strFilename + " [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Data - Export Linked Documents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                SQlConn.Close();
                                SQlConn.Dispose();
                                return;*/
                            }
                        }
                        File.Copy(strFilenameSource, strFilenameNew, true);
                    }
                    catch (Exception)
                    {
                    /*    if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to export the Linked Document " + strFilename + " [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Data - Export Linked Documents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        SQlConn.Close();
                        SQlConn.Dispose();
                        return;*/
                    }
                   
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
        }

        private void Export_Linked_Pictures(string strOriginalPath, string strNewPath, frmProgress fProgress, string strCaller)
        {
            if (strCaller == "Export")
            {
                strOriginalPath += (!strOriginalPath.EndsWith("\\") ? "\\" : "");
                strNewPath += (!strNewPath.EndsWith("\\") ? "\\" : "") + "Linked_Pictures\\";
            }
            else  // Import //
            {
                strOriginalPath += (!strOriginalPath.EndsWith("\\") ? "\\" : "") + "Linked_Pictures\\";
                strNewPath += (!strNewPath.EndsWith("\\") ? "\\" : "");
            }

            // Copy files from Desktop location to Tablet location //
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp02067_AT_Data_Interchange_Tablet_Sync_Linked_Pictures_List", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet("NewDataSet");
            try
            {
                da.Fill(ds, "Table");
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to Read the Linked Pictures data from the database [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Data - Export Linked Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                SQlConn.Close();
                SQlConn.Dispose();
                return;
            }
            string strFilenameSource, strFilenameNew, strFilename, strDirectoryStructure;
            if (ds.Tables[0].Rows.Count > 0)
            {
                int intUpdateProgressThreshhold = ds.Tables[0].Rows.Count / 10;
                int intUpdateProgressTempCount = 0;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    strFilename = dr["DocumentPath"].ToString();
                    strFilenameSource = strOriginalPath + strFilename;
                    strFilenameNew = strNewPath + strFilename;

                    fProgress.UpdateCaption("Exporting Linked Pictures - " + strFilename + "...");
                    Application.DoEvents();   // Allow Form time to repaint itself //

                    FileInfo f1 = new FileInfo(strFilenameSource);
                    if (!f1.Exists) continue;  // Abort copy of this file and move to next //

                    FileInfo f2 = new FileInfo(strFilenameNew);
                    if (f2.Exists)
                    {
                        // Check if file size or last modified date has changed //
                        if (f1.Length == f2.Length && f1.LastWriteTime == f2.LastWriteTime) continue;  // Abort copy of this file and move to next //
                    }
                    try
                    {
                        // Check if a directory structure to hold the new document is required //
                        strDirectoryStructure = f2.DirectoryName;
                        if (!Directory.Exists(strDirectoryStructure))
                        {
                            try
                            {
                                Directory.CreateDirectory(strDirectoryStructure);
                            }
                            catch (Exception)
                            {
                                /*    if (fProgress != null)
                                    {
                                        fProgress.Close();
                                        fProgress = null;
                                    }
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the directory structure for Linked Document " + strFilename + " [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Data - Export Linked Documents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    SQlConn.Close();
                                    SQlConn.Dispose();
                                    return;*/
                            }
                        }
                        File.Copy(strFilenameSource, strFilenameNew, true);
                    }
                    catch (Exception)
                    {
                        /*    if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to export the Linked Document " + strFilename + " [" + ex.Message + "]!\n\nTry running the export process again. If the problem persists contact Technical Support.", "Export Data - Export Linked Documents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            SQlConn.Close();
                            SQlConn.Dispose();
                            return;*/
                    }

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
        }


        

    }
}


namespace WoodPlan5
{
    partial class frm_AT_Monitor_Outstanding_Actions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Monitor_Outstanding_Actions));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D1 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel1 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView1 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.SeriesTitle seriesTitle1 = new DevExpress.XtraCharts.SeriesTitle();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel2 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView2 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.SeriesTitle seriesTitle2 = new DevExpress.XtraCharts.SeriesTitle();
            DevExpress.XtraCharts.Series series3 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel3 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView3 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.SeriesTitle seriesTitle3 = new DevExpress.XtraCharts.SeriesTitle();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel4 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView4 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D2 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series4 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel5 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView5 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.SeriesTitle seriesTitle4 = new DevExpress.XtraCharts.SeriesTitle();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel6 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView6 = new DevExpress.XtraCharts.Pie3DSeriesView();
            this.sp01338ATActionsDueBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.gridSplitContainer5 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01336ATContractorMasterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobileTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp01335ATJobMasterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDefaultWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01457ATSiteFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnLoadData = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.sp01338_AT_Actions_DueTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01338_AT_Actions_DueTableAdapter();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.chartControl2 = new DevExpress.XtraCharts.ChartControl();
            this.sp01337_Inspections_DueTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01337_Inspections_DueTableAdapter();
            this.sp01337InspectionsDueBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeContext = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeManagement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGroundType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeProtectionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeCrownDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePlantDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGroupNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSiteHazardClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePlantSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePlantSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePlantMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMapLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeTarget1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeTarget2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeTarget3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeLastModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeAgeClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeAreaHa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplantCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHedgeOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHedgeLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGardenSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePropertyProximity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSiteLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSituation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeRiskFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionInspector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionIncidentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionIncidentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionAngleToVertical = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionGeneralCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionVitality = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDateLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostDifference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrderIssueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrderCompleteDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueWithinDays1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueWithinDaysDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHighlight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContext = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroundType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProtectionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteHazardClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgeClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaHa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReplantCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGardenSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPropertyProximity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSituation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedInspectionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueWithinDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueWithinDaysDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT2 = new WoodPlan5.DataSet_AT();
            this.dataSet_AT1 = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01335_AT_Job_Master_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01335_AT_Job_Master_ListTableAdapter();
            this.sp01336_AT_Contractor_Master_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01336_AT_Contractor_Master_ListTableAdapter();
            this.pmChart = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiChartWizard = new DevExpress.XtraBars.BarButtonItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp01457_AT_Site_Filter_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01457_AT_Site_Filter_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01338ATActionsDueBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).BeginInit();
            this.gridSplitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01336ATContractorMasterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01335ATJobMasterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01457ATSiteFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView4)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01337InspectionsDueBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1010, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 690);
            this.barDockControlBottom.Size = new System.Drawing.Size(1010, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 690);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1010, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 690);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiChartWizard});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // sp01338ATActionsDueBindingSource
            // 
            this.sp01338ATActionsDueBindingSource.DataMember = "sp01338_AT_Actions_Due";
            this.sp01338ATActionsDueBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.layoutControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1010, 690);
            this.splitContainerControl1.SplitterPosition = 335;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(335, 690);
            this.splitContainerControl2.SplitterPosition = 386;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Controls.Add(this.gridSplitContainer5);
            this.layoutControl1.Controls.Add(this.gridSplitContainer4);
            this.layoutControl1.Controls.Add(this.gridSplitContainer3);
            this.layoutControl1.Controls.Add(this.btnLoadData);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(857, 98, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(335, 298);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(233, 2);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, no dat" +
    "e filter will be used when loading records.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 11;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(59, 2);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, no dat" +
    "e filter will be used when loading records.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(113, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 11;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // gridSplitContainer5
            // 
            this.gridSplitContainer5.Grid = this.gridControl3;
            this.gridSplitContainer5.Location = new System.Drawing.Point(5, 50);
            this.gridSplitContainer5.Name = "gridSplitContainer5";
            this.gridSplitContainer5.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer5.Size = new System.Drawing.Size(325, 217);
            this.gridSplitContainer5.TabIndex = 14;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp01336ATContractorMasterListBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit1});
            this.gridControl3.Size = new System.Drawing.Size(325, 217);
            this.gridControl3.TabIndex = 7;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp01336ATContractorMasterListBindingSource
            // 
            this.sp01336ATContractorMasterListBindingSource.DataMember = "sp01336_AT_Contractor_Master_List";
            this.sp01336ATContractorMasterListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colContractorID,
            this.colContractorCode,
            this.colContractorTypeID,
            this.colContractorTypeDescription,
            this.colContractorName,
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colPostcode,
            this.colTelephone1,
            this.colTelephone2,
            this.colMobileTelephone1,
            this.colWebsite,
            this.colVatReg,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colRemarks1,
            this.colDisabled});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorTypeID
            // 
            this.colContractorTypeID.Caption = "Contractor Type ID";
            this.colContractorTypeID.FieldName = "ContractorTypeID";
            this.colContractorTypeID.Name = "colContractorTypeID";
            this.colContractorTypeID.OptionsColumn.AllowEdit = false;
            this.colContractorTypeID.OptionsColumn.AllowFocus = false;
            this.colContractorTypeID.OptionsColumn.ReadOnly = true;
            this.colContractorTypeID.Width = 114;
            // 
            // colContractorTypeDescription
            // 
            this.colContractorTypeDescription.Caption = "Contractor Type";
            this.colContractorTypeDescription.FieldName = "ContractorTypeDescription";
            this.colContractorTypeDescription.Name = "colContractorTypeDescription";
            this.colContractorTypeDescription.OptionsColumn.AllowEdit = false;
            this.colContractorTypeDescription.OptionsColumn.AllowFocus = false;
            this.colContractorTypeDescription.OptionsColumn.ReadOnly = true;
            this.colContractorTypeDescription.Visible = true;
            this.colContractorTypeDescription.VisibleIndex = 1;
            this.colContractorTypeDescription.Width = 130;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 290;
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 183;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colMobileTelephone1
            // 
            this.colMobileTelephone1.Caption = "Mobile 1";
            this.colMobileTelephone1.FieldName = "MobileTelephone1";
            this.colMobileTelephone1.Name = "colMobileTelephone1";
            this.colMobileTelephone1.OptionsColumn.AllowEdit = false;
            this.colMobileTelephone1.OptionsColumn.AllowFocus = false;
            this.colMobileTelephone1.OptionsColumn.ReadOnly = true;
            this.colMobileTelephone1.Width = 60;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Registration No";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 117;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Width = 62;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Grid = this.gridControl2;
            this.gridSplitContainer4.Location = new System.Drawing.Point(5, 50);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer4.Size = new System.Drawing.Size(325, 217);
            this.gridSplitContainer4.TabIndex = 13;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp01335ATJobMasterListBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl2.Size = new System.Drawing.Size(325, 217);
            this.gridControl2.TabIndex = 7;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp01335ATJobMasterListBindingSource
            // 
            this.sp01335ATJobMasterListBindingSource.DataMember = "sp01335_AT_Job_Master_List";
            this.sp01335ATJobMasterListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.colJobCode,
            this.colJobDescription,
            this.colRemarks,
            this.colDefaultWorkUnits});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            this.colJobID.Width = 52;
            // 
            // colJobCode
            // 
            this.colJobCode.Caption = "Job Code";
            this.colJobCode.FieldName = "JobCode";
            this.colJobCode.Name = "colJobCode";
            this.colJobCode.OptionsColumn.AllowEdit = false;
            this.colJobCode.OptionsColumn.AllowFocus = false;
            this.colJobCode.OptionsColumn.ReadOnly = true;
            this.colJobCode.Visible = true;
            this.colJobCode.VisibleIndex = 0;
            this.colJobCode.Width = 66;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 1;
            this.colJobDescription.Width = 291;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Width = 62;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colDefaultWorkUnits
            // 
            this.colDefaultWorkUnits.Caption = "Default Work Units";
            this.colDefaultWorkUnits.FieldName = "DefaultWorkUnits";
            this.colDefaultWorkUnits.Name = "colDefaultWorkUnits";
            this.colDefaultWorkUnits.OptionsColumn.AllowEdit = false;
            this.colDefaultWorkUnits.OptionsColumn.AllowFocus = false;
            this.colDefaultWorkUnits.OptionsColumn.ReadOnly = true;
            this.colDefaultWorkUnits.Visible = true;
            this.colDefaultWorkUnits.VisibleIndex = 2;
            this.colDefaultWorkUnits.Width = 111;
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Grid = this.gridControl1;
            this.gridSplitContainer3.Location = new System.Drawing.Point(5, 50);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer3.Size = new System.Drawing.Size(325, 217);
            this.gridSplitContainer3.TabIndex = 12;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01457ATSiteFilterListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(325, 217);
            this.gridControl1.TabIndex = 6;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01457ATSiteFilterListBindingSource
            // 
            this.sp01457ATSiteFilterListBindingSource.DataMember = "sp01457_AT_Site_Filter_List";
            this.sp01457ATSiteFilterListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteID,
            this.colSiteCode,
            this.colSiteName,
            this.colClientID,
            this.colClientCode,
            this.colClientName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Width = 62;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Width = 86;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 270;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Width = 69;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Width = 83;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 1;
            this.colClientName.Width = 178;
            // 
            // btnLoadData
            // 
            this.btnLoadData.ImageOptions.ImageIndex = 0;
            this.btnLoadData.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadData.Location = new System.Drawing.Point(253, 274);
            this.btnLoadData.Name = "btnLoadData";
            this.btnLoadData.Size = new System.Drawing.Size(80, 22);
            this.btnLoadData.StyleController = this.layoutControl1;
            this.btnLoadData.TabIndex = 8;
            this.btnLoadData.Text = "Load Data";
            this.btnLoadData.Click += new System.EventHandler(this.btnLoadData_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "refresh_16x16");
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.layoutControlItem1,
            this.layoutControlItem6,
            this.emptySpaceItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(335, 298);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 24);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(335, 248);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Client \\ Site";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(329, 221);
            this.layoutControlGroup2.Text = "Client \\ Site";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridSplitContainer3;
            this.layoutControlItem3.CustomizationFormText = "District \\ Locality Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(329, 221);
            this.layoutControlItem3.Text = "District \\ Locality Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Action Type";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(329, 221);
            this.layoutControlGroup3.Text = "Action Type";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridSplitContainer4;
            this.layoutControlItem4.CustomizationFormText = "Action Type Grid:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(329, 221);
            this.layoutControlItem4.Text = "Action Type Grid:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Contractor";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(329, 221);
            this.layoutControlGroup4.Text = "Contractor";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridSplitContainer5;
            this.layoutControlItem5.CustomizationFormText = "Contractor Gird:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(329, 221);
            this.layoutControlItem5.Text = "Contractor Gird:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDate;
            this.layoutControlItem1.CustomizationFormText = "From Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(174, 24);
            this.layoutControlItem1.Text = "From Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnLoadData;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(251, 272);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(84, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(84, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(84, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 272);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(251, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDate;
            this.layoutControlItem2.CustomizationFormText = "To Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(174, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem2.Text = "To Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(54, 13);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl1.Size = new System.Drawing.Size(335, 386);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.chartControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(330, 360);
            this.xtraTabPage2.Text = "Actions";
            // 
            // chartControl1
            // 
            this.chartControl1.DataAdapter = this.sp01338_AT_Actions_DueTableAdapter;
            this.chartControl1.DataSource = this.sp01338ATActionsDueBindingSource;
            simpleDiagram3D1.Dimension = 1;
            simpleDiagram3D1.RotationMatrixSerializable = "1;0;0;0;0;0.5;-0.866025403784439;0;0;0.866025403784439;0.5;0;0;0;0;1";
            simpleDiagram3D1.RuntimeRotation = true;
            simpleDiagram3D1.RuntimeScrolling = true;
            simpleDiagram3D1.RuntimeZooming = true;
            this.chartControl1.Diagram = simpleDiagram3D1;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl1.EmptyChartText.Text = "No Data To Graph - Try Changing the Data Filter.";
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            series1.ArgumentDataMember = "Action";
            pie3DSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pie3DSeriesLabel1.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns;
            pie3DSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            pie3DSeriesLabel1.TextPattern = "{A}: {VP:P0}";
            series1.Label = pie3DSeriesLabel1;
            series1.LegendTextPattern = "{A}: {VP:P0}";
            series1.Name = "Actions Due by Type";
            series1.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            series1.SummaryFunction = "COUNT()";
            seriesTitle1.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            seriesTitle1.Font = new System.Drawing.Font("Tahoma", 10F);
            pie3DSeriesView1.Titles.AddRange(new DevExpress.XtraCharts.SeriesTitle[] {
            seriesTitle1});
            series1.View = pie3DSeriesView1;
            series2.ArgumentDataMember = "ActionBy";
            pie3DSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pie3DSeriesLabel2.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns;
            pie3DSeriesLabel2.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            pie3DSeriesLabel2.TextPattern = "{A}: {VP:P0}";
            series2.Label = pie3DSeriesLabel2;
            series2.LegendTextPattern = "{A}: {VP:P0}";
            series2.Name = "Actions Due by Contractor";
            series2.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            series2.SummaryFunction = "COUNT()";
            seriesTitle2.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            seriesTitle2.Font = new System.Drawing.Font("Tahoma", 10F);
            pie3DSeriesView2.Titles.AddRange(new DevExpress.XtraCharts.SeriesTitle[] {
            seriesTitle2});
            series2.View = pie3DSeriesView2;
            series3.ArgumentDataMember = "DueDescription";
            pie3DSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pie3DSeriesLabel3.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns;
            pie3DSeriesLabel3.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            pie3DSeriesLabel3.TextPattern = "{A}: {VP:P0}";
            series3.Label = pie3DSeriesLabel3;
            series3.LegendTextPattern = "{A}: {VP:P0}";
            series3.Name = "Actions Due by Due Date";
            series3.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            series3.SummaryFunction = "COUNT()";
            seriesTitle3.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            seriesTitle3.Font = new System.Drawing.Font("Tahoma", 10F);
            pie3DSeriesView3.Titles.AddRange(new DevExpress.XtraCharts.SeriesTitle[] {
            seriesTitle3});
            series3.View = pie3DSeriesView3;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2,
        series3};
            pie3DSeriesLabel4.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl1.SeriesTemplate.Label = pie3DSeriesLabel4;
            this.chartControl1.SeriesTemplate.View = pie3DSeriesView4;
            this.chartControl1.Size = new System.Drawing.Size(330, 360);
            this.chartControl1.TabIndex = 0;
            this.chartControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chartControl1_MouseUp);
            // 
            // sp01338_AT_Actions_DueTableAdapter
            // 
            this.sp01338_AT_Actions_DueTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.chartControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(330, 360);
            this.xtraTabPage1.Text = "Inspections";
            // 
            // chartControl2
            // 
            this.chartControl2.DataAdapter = this.sp01337_Inspections_DueTableAdapter;
            this.chartControl2.DataSource = this.sp01337InspectionsDueBindingSource;
            simpleDiagram3D2.Dimension = 1;
            simpleDiagram3D2.RotationMatrixSerializable = "1;0;0;0;0;0.5;-0.866025403784439;0;0;0.866025403784439;0.5;0;0;0;0;1";
            simpleDiagram3D2.RuntimeRotation = true;
            simpleDiagram3D2.RuntimeScrolling = true;
            simpleDiagram3D2.RuntimeZooming = true;
            this.chartControl2.Diagram = simpleDiagram3D2;
            this.chartControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl2.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl2.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl2.EmptyChartText.Text = "No Data To Graph - Try Changing the Data Filter.";
            this.chartControl2.Legend.EquallySpacedItems = false;
            this.chartControl2.Legend.Name = "Default Legend";
            this.chartControl2.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl2.Location = new System.Drawing.Point(0, 0);
            this.chartControl2.Name = "chartControl2";
            series4.ArgumentDataMember = "DueDescription";
            pie3DSeriesLabel5.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pie3DSeriesLabel5.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns;
            pie3DSeriesLabel5.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            pie3DSeriesLabel5.TextPattern = "{A}: {VP:P0}";
            series4.Label = pie3DSeriesLabel5;
            series4.LegendTextPattern = "{A}: {VP:P0}";
            series4.Name = "Inspections Due By Date";
            series4.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            series4.SummaryFunction = "COUNT()";
            seriesTitle4.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            seriesTitle4.Font = new System.Drawing.Font("Tahoma", 10F);
            pie3DSeriesView5.Titles.AddRange(new DevExpress.XtraCharts.SeriesTitle[] {
            seriesTitle4});
            series4.View = pie3DSeriesView5;
            this.chartControl2.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series4};
            pie3DSeriesLabel6.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl2.SeriesTemplate.Label = pie3DSeriesLabel6;
            pie3DSeriesView6.ExplodeMode = DevExpress.XtraCharts.PieExplodeMode.UsePoints;
            this.chartControl2.SeriesTemplate.View = pie3DSeriesView6;
            this.chartControl2.Size = new System.Drawing.Size(330, 360);
            this.chartControl2.TabIndex = 0;
            this.chartControl2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chartControl2_MouseUp);
            // 
            // sp01337_Inspections_DueTableAdapter
            // 
            this.sp01337_Inspections_DueTableAdapter.ClearBeforeFill = true;
            // 
            // sp01337InspectionsDueBindingSource
            // 
            this.sp01337InspectionsDueBindingSource.DataMember = "sp01337_Inspections_Due";
            this.sp01337InspectionsDueBindingSource.DataSource = this.dataSet_AT;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.gridSplitContainer2);
            this.layoutControl2.Controls.Add(this.gridSplitContainer1);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup5;
            this.layoutControl2.Size = new System.Drawing.Size(669, 690);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Grid = this.gridControl5;
            this.gridSplitContainer2.Location = new System.Drawing.Point(5, 376);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl5);
            this.gridSplitContainer2.Size = new System.Drawing.Size(659, 309);
            this.gridSplitContainer2.TabIndex = 7;
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp01338ATActionsDueBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(659, 309);
            this.gridControl5.TabIndex = 5;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.colTreeMappingID,
            this.colTreeGridReference,
            this.colTreeDistance,
            this.colTreeHouseName,
            this.colTreeSpeciesName,
            this.colTreeAccess,
            this.colTreeVisibility,
            this.colTreeContext,
            this.colTreeManagement,
            this.colTreeLegalStatus,
            this.colTreeLastInspectionDate,
            this.colTreeInspectionCycle,
            this.colTreeInspectionUnit,
            this.colTreeNextInspectionDate,
            this.colTreeGroundType,
            this.colTreeDBH,
            this.colTreeDBHRange,
            this.colTreeHeight,
            this.colTreeHeightRange,
            this.colTreeProtectionType,
            this.colTreeCrownDiameter,
            this.colTreePlantDate,
            this.colTreeGroupNumber,
            this.colTreeRiskCategory,
            this.colTreeSiteHazardClass,
            this.colTreePlantSize,
            this.colTreePlantSource,
            this.colTreePlantMethod,
            this.colTreePostcode,
            this.colTreeMapLabel,
            this.colTreeStatus,
            this.colTreeSize,
            this.colTreeTarget1,
            this.colTreeTarget2,
            this.colTreeTarget3,
            this.colTreeLastModifiedDate,
            this.gridColumn13,
            this.colTreeUser1,
            this.colTreeUser2,
            this.colTreeUser3,
            this.colTreeAgeClass,
            this.colTreeX,
            this.colTreeY,
            this.colTreeAreaHa,
            this.colTreeReplantCount,
            this.colTreeSurveyDate,
            this.gridColumn14,
            this.colTreeHedgeOwner,
            this.colTreeHedgeLength,
            this.colTreeGardenSize,
            this.colTreePropertyProximity,
            this.colTreeSiteLevel,
            this.colTreeSituation,
            this.colTreeRiskFactor,
            this.colTreePolygonXY,
            this.colTreeOwnershipName,
            this.colInspectionReference,
            this.colInspectionInspector,
            this.colInspectionDate,
            this.colInspectionIncidentReference,
            this.colInspectionIncidentDate,
            this.colIncidentID,
            this.colInspectionAngleToVertical,
            this.colInspectionLastModified,
            this.colInspectionUserDefined1,
            this.colInspectionUserDefined2,
            this.colInspectionUserDefined3,
            this.colInspectionRemarks,
            this.colInspectionStemPhysical1,
            this.colInspectionStemPhysical2,
            this.colInspectionStemPhysical3,
            this.colInspectionStemDisease1,
            this.colInspectionStemDisease2,
            this.colInspectionStemDisease3,
            this.colInspectionCrownPhysical1,
            this.colInspectionCrownPhysical2,
            this.colInspectionCrownPhysical3,
            this.colInspectionCrownDisease1,
            this.colInspectionCrownDisease2,
            this.colInspectionCrownDisease3,
            this.colInspectionCrownFoliation1,
            this.colInspectionCrownFoliation2,
            this.colInspectionCrownFoliation3,
            this.colInspectionRiskCategory,
            this.colInspectionGeneralCondition,
            this.colInspectionBasePhysical1,
            this.colInspectionBasePhysical2,
            this.colInspectionBasePhysical3,
            this.colInspectionVitality,
            this.colActionID,
            this.colActionJobNumber,
            this.colAction,
            this.colActionPriority,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colActionBy,
            this.colActionSupervisor,
            this.colActionOwnership,
            this.colActionCostCentre,
            this.colActionBudget,
            this.colActionWorkUnits,
            this.colActionScheduleOfRates,
            this.colActionBudgetedRateDescription,
            this.colActionBudgetedRate,
            this.colActionBudgetedCost,
            this.colActionActualRateDescription,
            this.colActionActualRate,
            this.colActionActualCost,
            this.colActionDateLastModified,
            this.colActionWorkOrder,
            this.colActionRemarks,
            this.colActionUserDefined1,
            this.colActionUserDefined2,
            this.colActionUserDefined3,
            this.colActionCostDifference,
            this.colActionCompletionStatus,
            this.colActionTimeliness,
            this.colActionWorkOrderIssueDate,
            this.colActionWorkOrderCompleteDate,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.colDueWithinDays1,
            this.colDueWithinDaysDescription1,
            this.colDueDescription});
            this.gridView5.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 518, 208, 191);
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupedColumns = true;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDueDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            this.colInspectionID.Width = 85;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Tree ID";
            this.gridColumn7.FieldName = "TreeID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 57;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Site ID";
            this.gridColumn8.FieldName = "SiteID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 71;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Client ID";
            this.gridColumn9.FieldName = "ClientID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 68;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Site Name";
            this.gridColumn10.FieldName = "SiteName";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 1;
            this.gridColumn10.Width = 183;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Client Name";
            this.gridColumn11.FieldName = "ClientName";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 176;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Tree Reference";
            this.gridColumn12.FieldName = "TreeReference";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 96;
            // 
            // colTreeMappingID
            // 
            this.colTreeMappingID.Caption = "Mapping ID";
            this.colTreeMappingID.FieldName = "TreeMappingID";
            this.colTreeMappingID.Name = "colTreeMappingID";
            this.colTreeMappingID.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID.OptionsColumn.ReadOnly = true;
            // 
            // colTreeGridReference
            // 
            this.colTreeGridReference.Caption = "Grid Reference";
            this.colTreeGridReference.FieldName = "TreeGridReference";
            this.colTreeGridReference.Name = "colTreeGridReference";
            this.colTreeGridReference.OptionsColumn.AllowEdit = false;
            this.colTreeGridReference.OptionsColumn.AllowFocus = false;
            this.colTreeGridReference.OptionsColumn.ReadOnly = true;
            this.colTreeGridReference.Width = 93;
            // 
            // colTreeDistance
            // 
            this.colTreeDistance.Caption = "Distance";
            this.colTreeDistance.FieldName = "TreeDistance";
            this.colTreeDistance.Name = "colTreeDistance";
            this.colTreeDistance.OptionsColumn.AllowEdit = false;
            this.colTreeDistance.OptionsColumn.AllowFocus = false;
            this.colTreeDistance.OptionsColumn.ReadOnly = true;
            this.colTreeDistance.Width = 74;
            // 
            // colTreeHouseName
            // 
            this.colTreeHouseName.Caption = "Nearest House";
            this.colTreeHouseName.FieldName = "TreeHouseName";
            this.colTreeHouseName.Name = "colTreeHouseName";
            this.colTreeHouseName.OptionsColumn.AllowEdit = false;
            this.colTreeHouseName.OptionsColumn.AllowFocus = false;
            this.colTreeHouseName.OptionsColumn.ReadOnly = true;
            this.colTreeHouseName.Width = 92;
            // 
            // colTreeSpeciesName
            // 
            this.colTreeSpeciesName.Caption = "Species";
            this.colTreeSpeciesName.FieldName = "TreeSpeciesName";
            this.colTreeSpeciesName.Name = "colTreeSpeciesName";
            this.colTreeSpeciesName.OptionsColumn.AllowEdit = false;
            this.colTreeSpeciesName.OptionsColumn.AllowFocus = false;
            this.colTreeSpeciesName.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesName.Width = 171;
            // 
            // colTreeAccess
            // 
            this.colTreeAccess.Caption = "Access";
            this.colTreeAccess.FieldName = "TreeAccess";
            this.colTreeAccess.Name = "colTreeAccess";
            this.colTreeAccess.OptionsColumn.AllowEdit = false;
            this.colTreeAccess.OptionsColumn.AllowFocus = false;
            this.colTreeAccess.OptionsColumn.ReadOnly = true;
            this.colTreeAccess.Width = 54;
            // 
            // colTreeVisibility
            // 
            this.colTreeVisibility.Caption = "Visibility";
            this.colTreeVisibility.FieldName = "TreeVisibility";
            this.colTreeVisibility.Name = "colTreeVisibility";
            this.colTreeVisibility.OptionsColumn.AllowEdit = false;
            this.colTreeVisibility.OptionsColumn.AllowFocus = false;
            this.colTreeVisibility.OptionsColumn.ReadOnly = true;
            this.colTreeVisibility.Width = 58;
            // 
            // colTreeContext
            // 
            this.colTreeContext.Caption = "Context";
            this.colTreeContext.FieldName = "TreeContext";
            this.colTreeContext.Name = "colTreeContext";
            this.colTreeContext.OptionsColumn.AllowEdit = false;
            this.colTreeContext.OptionsColumn.AllowFocus = false;
            this.colTreeContext.OptionsColumn.ReadOnly = true;
            this.colTreeContext.Width = 60;
            // 
            // colTreeManagement
            // 
            this.colTreeManagement.Caption = "Management";
            this.colTreeManagement.FieldName = "TreeManagement";
            this.colTreeManagement.Name = "colTreeManagement";
            this.colTreeManagement.OptionsColumn.AllowEdit = false;
            this.colTreeManagement.OptionsColumn.AllowFocus = false;
            this.colTreeManagement.OptionsColumn.ReadOnly = true;
            this.colTreeManagement.Width = 83;
            // 
            // colTreeLegalStatus
            // 
            this.colTreeLegalStatus.Caption = "Legal Status";
            this.colTreeLegalStatus.FieldName = "TreeLegalStatus";
            this.colTreeLegalStatus.Name = "colTreeLegalStatus";
            this.colTreeLegalStatus.OptionsColumn.AllowEdit = false;
            this.colTreeLegalStatus.OptionsColumn.AllowFocus = false;
            this.colTreeLegalStatus.OptionsColumn.ReadOnly = true;
            this.colTreeLegalStatus.Width = 80;
            // 
            // colTreeLastInspectionDate
            // 
            this.colTreeLastInspectionDate.Caption = "Last Inspection Date";
            this.colTreeLastInspectionDate.FieldName = "TreeLastInspectionDate";
            this.colTreeLastInspectionDate.Name = "colTreeLastInspectionDate";
            this.colTreeLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colTreeLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colTreeLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colTreeLastInspectionDate.Width = 120;
            // 
            // colTreeInspectionCycle
            // 
            this.colTreeInspectionCycle.Caption = "Inspection Cycle";
            this.colTreeInspectionCycle.FieldName = "TreeInspectionCycle";
            this.colTreeInspectionCycle.Name = "colTreeInspectionCycle";
            this.colTreeInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colTreeInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colTreeInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colTreeInspectionCycle.Width = 100;
            // 
            // colTreeInspectionUnit
            // 
            this.colTreeInspectionUnit.Caption = "Inspection Unit";
            this.colTreeInspectionUnit.FieldName = "TreeInspectionUnit";
            this.colTreeInspectionUnit.Name = "colTreeInspectionUnit";
            this.colTreeInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colTreeInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colTreeInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colTreeInspectionUnit.Width = 93;
            // 
            // colTreeNextInspectionDate
            // 
            this.colTreeNextInspectionDate.Caption = "Next Inspection Date";
            this.colTreeNextInspectionDate.FieldName = "TreeNextInspectionDate";
            this.colTreeNextInspectionDate.Name = "colTreeNextInspectionDate";
            this.colTreeNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colTreeNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colTreeNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colTreeNextInspectionDate.Width = 123;
            // 
            // colTreeGroundType
            // 
            this.colTreeGroundType.Caption = "Ground Type";
            this.colTreeGroundType.FieldName = "TreeGroundType";
            this.colTreeGroundType.Name = "colTreeGroundType";
            this.colTreeGroundType.OptionsColumn.AllowEdit = false;
            this.colTreeGroundType.OptionsColumn.AllowFocus = false;
            this.colTreeGroundType.OptionsColumn.ReadOnly = true;
            this.colTreeGroundType.Width = 83;
            // 
            // colTreeDBH
            // 
            this.colTreeDBH.Caption = "DBH";
            this.colTreeDBH.FieldName = "TreeDBH";
            this.colTreeDBH.Name = "colTreeDBH";
            this.colTreeDBH.OptionsColumn.AllowEdit = false;
            this.colTreeDBH.OptionsColumn.AllowFocus = false;
            this.colTreeDBH.OptionsColumn.ReadOnly = true;
            this.colTreeDBH.Width = 41;
            // 
            // colTreeDBHRange
            // 
            this.colTreeDBHRange.Caption = "DBH Band";
            this.colTreeDBHRange.FieldName = "TreeDBHRange";
            this.colTreeDBHRange.Name = "colTreeDBHRange";
            this.colTreeDBHRange.OptionsColumn.AllowEdit = false;
            this.colTreeDBHRange.OptionsColumn.AllowFocus = false;
            this.colTreeDBHRange.OptionsColumn.ReadOnly = true;
            this.colTreeDBHRange.Width = 68;
            // 
            // colTreeHeight
            // 
            this.colTreeHeight.Caption = "Height";
            this.colTreeHeight.FieldName = "TreeHeight";
            this.colTreeHeight.Name = "colTreeHeight";
            this.colTreeHeight.OptionsColumn.AllowEdit = false;
            this.colTreeHeight.OptionsColumn.AllowFocus = false;
            this.colTreeHeight.OptionsColumn.ReadOnly = true;
            this.colTreeHeight.Width = 52;
            // 
            // colTreeHeightRange
            // 
            this.colTreeHeightRange.Caption = "Height Band";
            this.colTreeHeightRange.FieldName = "TreeHeightRange";
            this.colTreeHeightRange.Name = "colTreeHeightRange";
            this.colTreeHeightRange.OptionsColumn.AllowEdit = false;
            this.colTreeHeightRange.OptionsColumn.AllowFocus = false;
            this.colTreeHeightRange.OptionsColumn.ReadOnly = true;
            this.colTreeHeightRange.Width = 79;
            // 
            // colTreeProtectionType
            // 
            this.colTreeProtectionType.Caption = "Protection Type";
            this.colTreeProtectionType.FieldName = "TreeProtectionType";
            this.colTreeProtectionType.Name = "colTreeProtectionType";
            this.colTreeProtectionType.OptionsColumn.AllowEdit = false;
            this.colTreeProtectionType.OptionsColumn.AllowFocus = false;
            this.colTreeProtectionType.OptionsColumn.ReadOnly = true;
            this.colTreeProtectionType.Width = 97;
            // 
            // colTreeCrownDiameter
            // 
            this.colTreeCrownDiameter.Caption = "Crown Diameter";
            this.colTreeCrownDiameter.FieldName = "TreeCrownDiameter";
            this.colTreeCrownDiameter.Name = "colTreeCrownDiameter";
            this.colTreeCrownDiameter.OptionsColumn.AllowEdit = false;
            this.colTreeCrownDiameter.OptionsColumn.AllowFocus = false;
            this.colTreeCrownDiameter.OptionsColumn.ReadOnly = true;
            this.colTreeCrownDiameter.Width = 98;
            // 
            // colTreePlantDate
            // 
            this.colTreePlantDate.Caption = "Plant Date";
            this.colTreePlantDate.FieldName = "TreePlantDate";
            this.colTreePlantDate.Name = "colTreePlantDate";
            this.colTreePlantDate.OptionsColumn.AllowEdit = false;
            this.colTreePlantDate.OptionsColumn.AllowFocus = false;
            this.colTreePlantDate.OptionsColumn.ReadOnly = true;
            this.colTreePlantDate.Width = 71;
            // 
            // colTreeGroupNumber
            // 
            this.colTreeGroupNumber.Caption = "Gorup Number";
            this.colTreeGroupNumber.FieldName = "TreeGroupNumber";
            this.colTreeGroupNumber.Name = "colTreeGroupNumber";
            this.colTreeGroupNumber.OptionsColumn.AllowEdit = false;
            this.colTreeGroupNumber.OptionsColumn.AllowFocus = false;
            this.colTreeGroupNumber.OptionsColumn.ReadOnly = true;
            this.colTreeGroupNumber.Width = 90;
            // 
            // colTreeRiskCategory
            // 
            this.colTreeRiskCategory.Caption = "Risk Category";
            this.colTreeRiskCategory.FieldName = "TreeRiskCategory";
            this.colTreeRiskCategory.Name = "colTreeRiskCategory";
            this.colTreeRiskCategory.OptionsColumn.AllowEdit = false;
            this.colTreeRiskCategory.OptionsColumn.AllowFocus = false;
            this.colTreeRiskCategory.OptionsColumn.ReadOnly = true;
            this.colTreeRiskCategory.Width = 88;
            // 
            // colTreeSiteHazardClass
            // 
            this.colTreeSiteHazardClass.Caption = "Site Hazard Class";
            this.colTreeSiteHazardClass.FieldName = "TreeSiteHazardClass";
            this.colTreeSiteHazardClass.Name = "colTreeSiteHazardClass";
            this.colTreeSiteHazardClass.OptionsColumn.AllowEdit = false;
            this.colTreeSiteHazardClass.OptionsColumn.AllowFocus = false;
            this.colTreeSiteHazardClass.OptionsColumn.ReadOnly = true;
            this.colTreeSiteHazardClass.Width = 104;
            // 
            // colTreePlantSize
            // 
            this.colTreePlantSize.Caption = "Plant Size";
            this.colTreePlantSize.FieldName = "TreePlantSize";
            this.colTreePlantSize.Name = "colTreePlantSize";
            this.colTreePlantSize.OptionsColumn.AllowEdit = false;
            this.colTreePlantSize.OptionsColumn.AllowFocus = false;
            this.colTreePlantSize.OptionsColumn.ReadOnly = true;
            this.colTreePlantSize.Width = 67;
            // 
            // colTreePlantSource
            // 
            this.colTreePlantSource.Caption = "Plant Source";
            this.colTreePlantSource.FieldName = "TreePlantSource";
            this.colTreePlantSource.Name = "colTreePlantSource";
            this.colTreePlantSource.OptionsColumn.AllowEdit = false;
            this.colTreePlantSource.OptionsColumn.AllowFocus = false;
            this.colTreePlantSource.OptionsColumn.ReadOnly = true;
            this.colTreePlantSource.Width = 81;
            // 
            // colTreePlantMethod
            // 
            this.colTreePlantMethod.Caption = "Plant Method";
            this.colTreePlantMethod.FieldName = "TreePlantMethod";
            this.colTreePlantMethod.Name = "colTreePlantMethod";
            this.colTreePlantMethod.OptionsColumn.AllowEdit = false;
            this.colTreePlantMethod.OptionsColumn.AllowFocus = false;
            this.colTreePlantMethod.OptionsColumn.ReadOnly = true;
            this.colTreePlantMethod.Width = 84;
            // 
            // colTreePostcode
            // 
            this.colTreePostcode.Caption = "Postcode";
            this.colTreePostcode.FieldName = "TreePostcode";
            this.colTreePostcode.Name = "colTreePostcode";
            this.colTreePostcode.OptionsColumn.AllowEdit = false;
            this.colTreePostcode.OptionsColumn.AllowFocus = false;
            this.colTreePostcode.OptionsColumn.ReadOnly = true;
            this.colTreePostcode.Width = 65;
            // 
            // colTreeMapLabel
            // 
            this.colTreeMapLabel.Caption = "Map Label";
            this.colTreeMapLabel.FieldName = "TreeMapLabel";
            this.colTreeMapLabel.Name = "colTreeMapLabel";
            this.colTreeMapLabel.OptionsColumn.AllowEdit = false;
            this.colTreeMapLabel.OptionsColumn.AllowFocus = false;
            this.colTreeMapLabel.OptionsColumn.ReadOnly = true;
            this.colTreeMapLabel.Width = 69;
            // 
            // colTreeStatus
            // 
            this.colTreeStatus.Caption = "Tree Status";
            this.colTreeStatus.FieldName = "TreeStatus";
            this.colTreeStatus.Name = "colTreeStatus";
            this.colTreeStatus.OptionsColumn.AllowEdit = false;
            this.colTreeStatus.OptionsColumn.AllowFocus = false;
            this.colTreeStatus.OptionsColumn.ReadOnly = true;
            this.colTreeStatus.Width = 77;
            // 
            // colTreeSize
            // 
            this.colTreeSize.Caption = "Size";
            this.colTreeSize.FieldName = "TreeSize";
            this.colTreeSize.Name = "colTreeSize";
            this.colTreeSize.OptionsColumn.AllowEdit = false;
            this.colTreeSize.OptionsColumn.AllowFocus = false;
            this.colTreeSize.OptionsColumn.ReadOnly = true;
            this.colTreeSize.Width = 40;
            // 
            // colTreeTarget1
            // 
            this.colTreeTarget1.Caption = "Target 1";
            this.colTreeTarget1.FieldName = "TreeTarget1";
            this.colTreeTarget1.Name = "colTreeTarget1";
            this.colTreeTarget1.OptionsColumn.AllowEdit = false;
            this.colTreeTarget1.OptionsColumn.AllowFocus = false;
            this.colTreeTarget1.OptionsColumn.ReadOnly = true;
            this.colTreeTarget1.Width = 62;
            // 
            // colTreeTarget2
            // 
            this.colTreeTarget2.Caption = "Target 2";
            this.colTreeTarget2.FieldName = "TreeTarget2";
            this.colTreeTarget2.Name = "colTreeTarget2";
            this.colTreeTarget2.OptionsColumn.AllowEdit = false;
            this.colTreeTarget2.OptionsColumn.AllowFocus = false;
            this.colTreeTarget2.OptionsColumn.ReadOnly = true;
            this.colTreeTarget2.Width = 62;
            // 
            // colTreeTarget3
            // 
            this.colTreeTarget3.Caption = "Target 3";
            this.colTreeTarget3.FieldName = "TreeTarget3";
            this.colTreeTarget3.Name = "colTreeTarget3";
            this.colTreeTarget3.OptionsColumn.AllowEdit = false;
            this.colTreeTarget3.OptionsColumn.AllowFocus = false;
            this.colTreeTarget3.OptionsColumn.ReadOnly = true;
            this.colTreeTarget3.Width = 62;
            // 
            // colTreeLastModifiedDate
            // 
            this.colTreeLastModifiedDate.Caption = "Tree Last Modified";
            this.colTreeLastModifiedDate.FieldName = "TreeLastModifiedDate";
            this.colTreeLastModifiedDate.Name = "colTreeLastModifiedDate";
            this.colTreeLastModifiedDate.OptionsColumn.AllowEdit = false;
            this.colTreeLastModifiedDate.OptionsColumn.AllowFocus = false;
            this.colTreeLastModifiedDate.OptionsColumn.ReadOnly = true;
            this.colTreeLastModifiedDate.Width = 109;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Tree Remarks";
            this.gridColumn13.FieldName = "TreeRemarks";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 87;
            // 
            // colTreeUser1
            // 
            this.colTreeUser1.Caption = "Tree User Defined 1";
            this.colTreeUser1.FieldName = "TreeUser1";
            this.colTreeUser1.Name = "colTreeUser1";
            this.colTreeUser1.OptionsColumn.AllowEdit = false;
            this.colTreeUser1.OptionsColumn.AllowFocus = false;
            this.colTreeUser1.OptionsColumn.ReadOnly = true;
            this.colTreeUser1.Width = 117;
            // 
            // colTreeUser2
            // 
            this.colTreeUser2.Caption = "Tree User Defined 2";
            this.colTreeUser2.FieldName = "TreeUser2";
            this.colTreeUser2.Name = "colTreeUser2";
            this.colTreeUser2.OptionsColumn.AllowEdit = false;
            this.colTreeUser2.OptionsColumn.AllowFocus = false;
            this.colTreeUser2.OptionsColumn.ReadOnly = true;
            this.colTreeUser2.Width = 117;
            // 
            // colTreeUser3
            // 
            this.colTreeUser3.Caption = "Tree User Defined 3";
            this.colTreeUser3.FieldName = "TreeUser3";
            this.colTreeUser3.Name = "colTreeUser3";
            this.colTreeUser3.OptionsColumn.AllowEdit = false;
            this.colTreeUser3.OptionsColumn.AllowFocus = false;
            this.colTreeUser3.OptionsColumn.ReadOnly = true;
            this.colTreeUser3.Width = 117;
            // 
            // colTreeAgeClass
            // 
            this.colTreeAgeClass.Caption = "Age Class";
            this.colTreeAgeClass.FieldName = "TreeAgeClass";
            this.colTreeAgeClass.Name = "colTreeAgeClass";
            this.colTreeAgeClass.OptionsColumn.AllowEdit = false;
            this.colTreeAgeClass.OptionsColumn.AllowFocus = false;
            this.colTreeAgeClass.OptionsColumn.ReadOnly = true;
            this.colTreeAgeClass.Width = 68;
            // 
            // colTreeX
            // 
            this.colTreeX.Caption = "X Coordinate";
            this.colTreeX.FieldName = "TreeX";
            this.colTreeX.Name = "colTreeX";
            this.colTreeX.OptionsColumn.AllowEdit = false;
            this.colTreeX.OptionsColumn.AllowFocus = false;
            this.colTreeX.OptionsColumn.ReadOnly = true;
            this.colTreeX.Width = 83;
            // 
            // colTreeY
            // 
            this.colTreeY.Caption = "Y Coordinate";
            this.colTreeY.FieldName = "TreeY";
            this.colTreeY.Name = "colTreeY";
            this.colTreeY.OptionsColumn.AllowEdit = false;
            this.colTreeY.OptionsColumn.AllowFocus = false;
            this.colTreeY.OptionsColumn.ReadOnly = true;
            this.colTreeY.Width = 83;
            // 
            // colTreeAreaHa
            // 
            this.colTreeAreaHa.Caption = "Area (M�)";
            this.colTreeAreaHa.FieldName = "TreeAreaHa";
            this.colTreeAreaHa.Name = "colTreeAreaHa";
            this.colTreeAreaHa.OptionsColumn.AllowEdit = false;
            this.colTreeAreaHa.OptionsColumn.AllowFocus = false;
            this.colTreeAreaHa.OptionsColumn.ReadOnly = true;
            this.colTreeAreaHa.Width = 61;
            // 
            // colTreeReplantCount
            // 
            this.colTreeReplantCount.Caption = "Replant Count";
            this.colTreeReplantCount.FieldName = "TreeReplantCount";
            this.colTreeReplantCount.Name = "colTreeReplantCount";
            this.colTreeReplantCount.OptionsColumn.AllowEdit = false;
            this.colTreeReplantCount.OptionsColumn.AllowFocus = false;
            this.colTreeReplantCount.OptionsColumn.ReadOnly = true;
            this.colTreeReplantCount.Width = 90;
            // 
            // colTreeSurveyDate
            // 
            this.colTreeSurveyDate.Caption = "Survey Date";
            this.colTreeSurveyDate.FieldName = "TreeSurveyDate";
            this.colTreeSurveyDate.Name = "colTreeSurveyDate";
            this.colTreeSurveyDate.OptionsColumn.AllowEdit = false;
            this.colTreeSurveyDate.OptionsColumn.AllowFocus = false;
            this.colTreeSurveyDate.OptionsColumn.ReadOnly = true;
            this.colTreeSurveyDate.Width = 81;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Tree Type";
            this.gridColumn14.FieldName = "TreeType";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 70;
            // 
            // colTreeHedgeOwner
            // 
            this.colTreeHedgeOwner.Caption = "Hedge Owner";
            this.colTreeHedgeOwner.FieldName = "TreeHedgeOwner";
            this.colTreeHedgeOwner.Name = "colTreeHedgeOwner";
            this.colTreeHedgeOwner.OptionsColumn.AllowEdit = false;
            this.colTreeHedgeOwner.OptionsColumn.AllowFocus = false;
            this.colTreeHedgeOwner.OptionsColumn.ReadOnly = true;
            this.colTreeHedgeOwner.Width = 87;
            // 
            // colTreeHedgeLength
            // 
            this.colTreeHedgeLength.Caption = "Hedge Length";
            this.colTreeHedgeLength.FieldName = "TreeHedgeLength";
            this.colTreeHedgeLength.Name = "colTreeHedgeLength";
            this.colTreeHedgeLength.OptionsColumn.AllowEdit = false;
            this.colTreeHedgeLength.OptionsColumn.AllowFocus = false;
            this.colTreeHedgeLength.OptionsColumn.ReadOnly = true;
            this.colTreeHedgeLength.Width = 88;
            // 
            // colTreeGardenSize
            // 
            this.colTreeGardenSize.Caption = "Garden Size";
            this.colTreeGardenSize.FieldName = "TreeGardenSize";
            this.colTreeGardenSize.Name = "colTreeGardenSize";
            this.colTreeGardenSize.OptionsColumn.AllowEdit = false;
            this.colTreeGardenSize.OptionsColumn.AllowFocus = false;
            this.colTreeGardenSize.OptionsColumn.ReadOnly = true;
            this.colTreeGardenSize.Width = 78;
            // 
            // colTreePropertyProximity
            // 
            this.colTreePropertyProximity.Caption = "Property Proximity";
            this.colTreePropertyProximity.FieldName = "TreePropertyProximity";
            this.colTreePropertyProximity.Name = "colTreePropertyProximity";
            this.colTreePropertyProximity.OptionsColumn.AllowEdit = false;
            this.colTreePropertyProximity.OptionsColumn.AllowFocus = false;
            this.colTreePropertyProximity.OptionsColumn.ReadOnly = true;
            this.colTreePropertyProximity.Width = 110;
            // 
            // colTreeSiteLevel
            // 
            this.colTreeSiteLevel.Caption = "Site Level";
            this.colTreeSiteLevel.FieldName = "TreeSiteLevel";
            this.colTreeSiteLevel.Name = "colTreeSiteLevel";
            this.colTreeSiteLevel.OptionsColumn.AllowEdit = false;
            this.colTreeSiteLevel.OptionsColumn.AllowFocus = false;
            this.colTreeSiteLevel.OptionsColumn.ReadOnly = true;
            this.colTreeSiteLevel.Width = 67;
            // 
            // colTreeSituation
            // 
            this.colTreeSituation.Caption = "Situation";
            this.colTreeSituation.FieldName = "TreeSituation";
            this.colTreeSituation.Name = "colTreeSituation";
            this.colTreeSituation.OptionsColumn.ReadOnly = true;
            this.colTreeSituation.Width = 63;
            // 
            // colTreeRiskFactor
            // 
            this.colTreeRiskFactor.Caption = "Risk Factor";
            this.colTreeRiskFactor.FieldName = "TreeRiskFactor";
            this.colTreeRiskFactor.Name = "colTreeRiskFactor";
            this.colTreeRiskFactor.OptionsColumn.AllowEdit = false;
            this.colTreeRiskFactor.OptionsColumn.AllowFocus = false;
            this.colTreeRiskFactor.OptionsColumn.ReadOnly = true;
            this.colTreeRiskFactor.Width = 74;
            // 
            // colTreePolygonXY
            // 
            this.colTreePolygonXY.Caption = "Polygon XY";
            this.colTreePolygonXY.FieldName = "TreePolygonXY";
            this.colTreePolygonXY.Name = "colTreePolygonXY";
            this.colTreePolygonXY.OptionsColumn.AllowEdit = false;
            this.colTreePolygonXY.OptionsColumn.AllowFocus = false;
            this.colTreePolygonXY.OptionsColumn.ReadOnly = true;
            this.colTreePolygonXY.Width = 74;
            // 
            // colTreeOwnershipName
            // 
            this.colTreeOwnershipName.Caption = "Tree Ownership";
            this.colTreeOwnershipName.FieldName = "TreeOwnershipName";
            this.colTreeOwnershipName.Name = "colTreeOwnershipName";
            this.colTreeOwnershipName.OptionsColumn.AllowEdit = false;
            this.colTreeOwnershipName.OptionsColumn.AllowFocus = false;
            this.colTreeOwnershipName.OptionsColumn.ReadOnly = true;
            this.colTreeOwnershipName.Width = 97;
            // 
            // colInspectionReference
            // 
            this.colInspectionReference.Caption = "Inspection Reference Number";
            this.colInspectionReference.FieldName = "InspectionReference";
            this.colInspectionReference.Name = "colInspectionReference";
            this.colInspectionReference.OptionsColumn.AllowEdit = false;
            this.colInspectionReference.OptionsColumn.AllowFocus = false;
            this.colInspectionReference.OptionsColumn.ReadOnly = true;
            this.colInspectionReference.Visible = true;
            this.colInspectionReference.VisibleIndex = 3;
            this.colInspectionReference.Width = 164;
            // 
            // colInspectionInspector
            // 
            this.colInspectionInspector.Caption = "Inspector";
            this.colInspectionInspector.FieldName = "InspectionInspector";
            this.colInspectionInspector.Name = "colInspectionInspector";
            this.colInspectionInspector.OptionsColumn.AllowEdit = false;
            this.colInspectionInspector.OptionsColumn.AllowFocus = false;
            this.colInspectionInspector.OptionsColumn.ReadOnly = true;
            this.colInspectionInspector.Width = 67;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Width = 97;
            // 
            // colInspectionIncidentReference
            // 
            this.colInspectionIncidentReference.Caption = "Incident Reference";
            this.colInspectionIncidentReference.FieldName = "InspectionIncidentReference";
            this.colInspectionIncidentReference.Name = "colInspectionIncidentReference";
            this.colInspectionIncidentReference.OptionsColumn.AllowEdit = false;
            this.colInspectionIncidentReference.OptionsColumn.AllowFocus = false;
            this.colInspectionIncidentReference.OptionsColumn.ReadOnly = true;
            this.colInspectionIncidentReference.Width = 113;
            // 
            // colInspectionIncidentDate
            // 
            this.colInspectionIncidentDate.Caption = "Incident Date";
            this.colInspectionIncidentDate.FieldName = "InspectionIncidentDate";
            this.colInspectionIncidentDate.Name = "colInspectionIncidentDate";
            this.colInspectionIncidentDate.OptionsColumn.AllowEdit = false;
            this.colInspectionIncidentDate.OptionsColumn.AllowFocus = false;
            this.colInspectionIncidentDate.OptionsColumn.ReadOnly = true;
            this.colInspectionIncidentDate.Width = 86;
            // 
            // colIncidentID
            // 
            this.colIncidentID.Caption = "Incident ID";
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.Width = 74;
            // 
            // colInspectionAngleToVertical
            // 
            this.colInspectionAngleToVertical.Caption = "Angle To Vertical";
            this.colInspectionAngleToVertical.FieldName = "InspectionAngleToVertical";
            this.colInspectionAngleToVertical.Name = "colInspectionAngleToVertical";
            this.colInspectionAngleToVertical.OptionsColumn.AllowEdit = false;
            this.colInspectionAngleToVertical.OptionsColumn.AllowFocus = false;
            this.colInspectionAngleToVertical.OptionsColumn.ReadOnly = true;
            this.colInspectionAngleToVertical.Width = 101;
            // 
            // colInspectionLastModified
            // 
            this.colInspectionLastModified.Caption = "Inspection Last Modified";
            this.colInspectionLastModified.FieldName = "InspectionLastModified";
            this.colInspectionLastModified.Name = "colInspectionLastModified";
            this.colInspectionLastModified.OptionsColumn.AllowEdit = false;
            this.colInspectionLastModified.OptionsColumn.AllowFocus = false;
            this.colInspectionLastModified.OptionsColumn.ReadOnly = true;
            this.colInspectionLastModified.Width = 137;
            // 
            // colInspectionUserDefined1
            // 
            this.colInspectionUserDefined1.Caption = "Inspection User Defined 1";
            this.colInspectionUserDefined1.FieldName = "InspectionUserDefined1";
            this.colInspectionUserDefined1.Name = "colInspectionUserDefined1";
            this.colInspectionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined1.Width = 145;
            // 
            // colInspectionUserDefined2
            // 
            this.colInspectionUserDefined2.Caption = "Inspection User Defined 2";
            this.colInspectionUserDefined2.FieldName = "InspectionUserDefined2";
            this.colInspectionUserDefined2.Name = "colInspectionUserDefined2";
            this.colInspectionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined2.Width = 145;
            // 
            // colInspectionUserDefined3
            // 
            this.colInspectionUserDefined3.Caption = "Inspection User Defined 3";
            this.colInspectionUserDefined3.FieldName = "InspectionUserDefined3";
            this.colInspectionUserDefined3.Name = "colInspectionUserDefined3";
            this.colInspectionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined3.Width = 145;
            // 
            // colInspectionRemarks
            // 
            this.colInspectionRemarks.Caption = "Inspection Remarks";
            this.colInspectionRemarks.FieldName = "InspectionRemarks";
            this.colInspectionRemarks.Name = "colInspectionRemarks";
            this.colInspectionRemarks.OptionsColumn.ReadOnly = true;
            this.colInspectionRemarks.Width = 115;
            // 
            // colInspectionStemPhysical1
            // 
            this.colInspectionStemPhysical1.Caption = "Stem Physical 1";
            this.colInspectionStemPhysical1.FieldName = "InspectionStemPhysical1";
            this.colInspectionStemPhysical1.Name = "colInspectionStemPhysical1";
            this.colInspectionStemPhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical1.Width = 95;
            // 
            // colInspectionStemPhysical2
            // 
            this.colInspectionStemPhysical2.Caption = "Stem Physical 2";
            this.colInspectionStemPhysical2.FieldName = "InspectionStemPhysical2";
            this.colInspectionStemPhysical2.Name = "colInspectionStemPhysical2";
            this.colInspectionStemPhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical2.Width = 95;
            // 
            // colInspectionStemPhysical3
            // 
            this.colInspectionStemPhysical3.Caption = "Stem Physical 3";
            this.colInspectionStemPhysical3.FieldName = "InspectionStemPhysical3";
            this.colInspectionStemPhysical3.Name = "colInspectionStemPhysical3";
            this.colInspectionStemPhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical3.Width = 95;
            // 
            // colInspectionStemDisease1
            // 
            this.colInspectionStemDisease1.Caption = "Stem Disease 1";
            this.colInspectionStemDisease1.FieldName = "InspectionStemDisease1";
            this.colInspectionStemDisease1.Name = "colInspectionStemDisease1";
            this.colInspectionStemDisease1.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease1.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease1.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease1.Width = 94;
            // 
            // colInspectionStemDisease2
            // 
            this.colInspectionStemDisease2.Caption = "Stem Disease 2";
            this.colInspectionStemDisease2.FieldName = "InspectionStemDisease2";
            this.colInspectionStemDisease2.Name = "colInspectionStemDisease2";
            this.colInspectionStemDisease2.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease2.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease2.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease2.Width = 94;
            // 
            // colInspectionStemDisease3
            // 
            this.colInspectionStemDisease3.Caption = "Stem Disease 3";
            this.colInspectionStemDisease3.FieldName = "InspectionStemDisease3";
            this.colInspectionStemDisease3.Name = "colInspectionStemDisease3";
            this.colInspectionStemDisease3.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease3.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease3.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease3.Width = 94;
            // 
            // colInspectionCrownPhysical1
            // 
            this.colInspectionCrownPhysical1.Caption = "Crown Physical 1";
            this.colInspectionCrownPhysical1.FieldName = "InspectionCrownPhysical1";
            this.colInspectionCrownPhysical1.Name = "colInspectionCrownPhysical1";
            this.colInspectionCrownPhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical1.Width = 102;
            // 
            // colInspectionCrownPhysical2
            // 
            this.colInspectionCrownPhysical2.Caption = "Crown Physical 2";
            this.colInspectionCrownPhysical2.FieldName = "InspectionCrownPhysical2";
            this.colInspectionCrownPhysical2.Name = "colInspectionCrownPhysical2";
            this.colInspectionCrownPhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical2.Width = 102;
            // 
            // colInspectionCrownPhysical3
            // 
            this.colInspectionCrownPhysical3.Caption = "Crown Physical 3";
            this.colInspectionCrownPhysical3.FieldName = "InspectionCrownPhysical3";
            this.colInspectionCrownPhysical3.Name = "colInspectionCrownPhysical3";
            this.colInspectionCrownPhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical3.Width = 102;
            // 
            // colInspectionCrownDisease1
            // 
            this.colInspectionCrownDisease1.Caption = "Crown Physical 1";
            this.colInspectionCrownDisease1.FieldName = "InspectionCrownDisease1";
            this.colInspectionCrownDisease1.Name = "colInspectionCrownDisease1";
            this.colInspectionCrownDisease1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease1.Width = 102;
            // 
            // colInspectionCrownDisease2
            // 
            this.colInspectionCrownDisease2.Caption = "Crown Physical 2";
            this.colInspectionCrownDisease2.FieldName = "InspectionCrownDisease2";
            this.colInspectionCrownDisease2.Name = "colInspectionCrownDisease2";
            this.colInspectionCrownDisease2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease2.Width = 102;
            // 
            // colInspectionCrownDisease3
            // 
            this.colInspectionCrownDisease3.Caption = "Crown Physical 3";
            this.colInspectionCrownDisease3.FieldName = "InspectionCrownDisease3";
            this.colInspectionCrownDisease3.Name = "colInspectionCrownDisease3";
            this.colInspectionCrownDisease3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease3.Width = 102;
            // 
            // colInspectionCrownFoliation1
            // 
            this.colInspectionCrownFoliation1.Caption = "Crown Foliation 1";
            this.colInspectionCrownFoliation1.FieldName = "InspectionCrownFoliation1";
            this.colInspectionCrownFoliation1.Name = "colInspectionCrownFoliation1";
            this.colInspectionCrownFoliation1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation1.Width = 104;
            // 
            // colInspectionCrownFoliation2
            // 
            this.colInspectionCrownFoliation2.Caption = "Crown Foliation 2";
            this.colInspectionCrownFoliation2.FieldName = "InspectionCrownFoliation2";
            this.colInspectionCrownFoliation2.Name = "colInspectionCrownFoliation2";
            this.colInspectionCrownFoliation2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation2.Width = 104;
            // 
            // colInspectionCrownFoliation3
            // 
            this.colInspectionCrownFoliation3.Caption = "Crown Foliation 3";
            this.colInspectionCrownFoliation3.FieldName = "InspectionCrownFoliation3";
            this.colInspectionCrownFoliation3.Name = "colInspectionCrownFoliation3";
            this.colInspectionCrownFoliation3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation3.Width = 104;
            // 
            // colInspectionRiskCategory
            // 
            this.colInspectionRiskCategory.Caption = "Inspection Risk Category";
            this.colInspectionRiskCategory.FieldName = "InspectionRiskCategory";
            this.colInspectionRiskCategory.Name = "colInspectionRiskCategory";
            this.colInspectionRiskCategory.OptionsColumn.AllowEdit = false;
            this.colInspectionRiskCategory.OptionsColumn.AllowFocus = false;
            this.colInspectionRiskCategory.OptionsColumn.ReadOnly = true;
            this.colInspectionRiskCategory.Width = 141;
            // 
            // colInspectionGeneralCondition
            // 
            this.colInspectionGeneralCondition.Caption = "General Condition";
            this.colInspectionGeneralCondition.FieldName = "InspectionGeneralCondition";
            this.colInspectionGeneralCondition.Name = "colInspectionGeneralCondition";
            this.colInspectionGeneralCondition.OptionsColumn.AllowEdit = false;
            this.colInspectionGeneralCondition.OptionsColumn.AllowFocus = false;
            this.colInspectionGeneralCondition.OptionsColumn.ReadOnly = true;
            this.colInspectionGeneralCondition.Width = 106;
            // 
            // colInspectionBasePhysical1
            // 
            this.colInspectionBasePhysical1.Caption = "Base Physical 1";
            this.colInspectionBasePhysical1.FieldName = "InspectionBasePhysical1";
            this.colInspectionBasePhysical1.Name = "colInspectionBasePhysical1";
            this.colInspectionBasePhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical1.Width = 94;
            // 
            // colInspectionBasePhysical2
            // 
            this.colInspectionBasePhysical2.Caption = "Base Physical 2";
            this.colInspectionBasePhysical2.FieldName = "InspectionBasePhysical2";
            this.colInspectionBasePhysical2.Name = "colInspectionBasePhysical2";
            this.colInspectionBasePhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical2.Width = 94;
            // 
            // colInspectionBasePhysical3
            // 
            this.colInspectionBasePhysical3.Caption = "Base Physical 3";
            this.colInspectionBasePhysical3.FieldName = "InspectionBasePhysical3";
            this.colInspectionBasePhysical3.Name = "colInspectionBasePhysical3";
            this.colInspectionBasePhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical3.Width = 94;
            // 
            // colInspectionVitality
            // 
            this.colInspectionVitality.Caption = "Vitality";
            this.colInspectionVitality.FieldName = "InspectionVitality";
            this.colInspectionVitality.Name = "colInspectionVitality";
            this.colInspectionVitality.OptionsColumn.AllowEdit = false;
            this.colInspectionVitality.OptionsColumn.AllowFocus = false;
            this.colInspectionVitality.OptionsColumn.ReadOnly = true;
            this.colInspectionVitality.Width = 53;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            this.colActionID.Width = 65;
            // 
            // colActionJobNumber
            // 
            this.colActionJobNumber.Caption = "Job Number";
            this.colActionJobNumber.FieldName = "ActionJobNumber";
            this.colActionJobNumber.Name = "colActionJobNumber";
            this.colActionJobNumber.OptionsColumn.AllowEdit = false;
            this.colActionJobNumber.OptionsColumn.AllowFocus = false;
            this.colActionJobNumber.OptionsColumn.ReadOnly = true;
            this.colActionJobNumber.Visible = true;
            this.colActionJobNumber.VisibleIndex = 5;
            this.colActionJobNumber.Width = 78;
            // 
            // colAction
            // 
            this.colAction.Caption = "Action";
            this.colAction.FieldName = "Action";
            this.colAction.Name = "colAction";
            this.colAction.OptionsColumn.AllowEdit = false;
            this.colAction.OptionsColumn.AllowFocus = false;
            this.colAction.OptionsColumn.ReadOnly = true;
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 6;
            this.colAction.Width = 104;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Action Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 7;
            this.colActionPriority.Width = 88;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Action Due Date";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 8;
            this.colActionDueDate.Width = 112;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Action Done Date";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 9;
            this.colActionDoneDate.Width = 105;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action Carried Out By";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            this.colActionBy.Visible = true;
            this.colActionBy.VisibleIndex = 10;
            this.colActionBy.Width = 125;
            // 
            // colActionSupervisor
            // 
            this.colActionSupervisor.Caption = "Action Supervisor";
            this.colActionSupervisor.FieldName = "ActionSupervisor";
            this.colActionSupervisor.Name = "colActionSupervisor";
            this.colActionSupervisor.OptionsColumn.AllowEdit = false;
            this.colActionSupervisor.OptionsColumn.AllowFocus = false;
            this.colActionSupervisor.OptionsColumn.ReadOnly = true;
            this.colActionSupervisor.Visible = true;
            this.colActionSupervisor.VisibleIndex = 11;
            this.colActionSupervisor.Width = 105;
            // 
            // colActionOwnership
            // 
            this.colActionOwnership.Caption = "Action Ownership";
            this.colActionOwnership.FieldName = "ActionOwnership";
            this.colActionOwnership.Name = "colActionOwnership";
            this.colActionOwnership.OptionsColumn.AllowEdit = false;
            this.colActionOwnership.OptionsColumn.AllowFocus = false;
            this.colActionOwnership.OptionsColumn.ReadOnly = true;
            this.colActionOwnership.Visible = true;
            this.colActionOwnership.VisibleIndex = 12;
            this.colActionOwnership.Width = 105;
            // 
            // colActionCostCentre
            // 
            this.colActionCostCentre.Caption = "Action Cost Centre";
            this.colActionCostCentre.FieldName = "ActionCostCentre";
            this.colActionCostCentre.Name = "colActionCostCentre";
            this.colActionCostCentre.OptionsColumn.AllowEdit = false;
            this.colActionCostCentre.OptionsColumn.AllowFocus = false;
            this.colActionCostCentre.OptionsColumn.ReadOnly = true;
            this.colActionCostCentre.Visible = true;
            this.colActionCostCentre.VisibleIndex = 13;
            this.colActionCostCentre.Width = 112;
            // 
            // colActionBudget
            // 
            this.colActionBudget.Caption = "Action Budget";
            this.colActionBudget.FieldName = "ActionBudget";
            this.colActionBudget.Name = "colActionBudget";
            this.colActionBudget.OptionsColumn.AllowEdit = false;
            this.colActionBudget.OptionsColumn.AllowFocus = false;
            this.colActionBudget.OptionsColumn.ReadOnly = true;
            this.colActionBudget.Visible = true;
            this.colActionBudget.VisibleIndex = 14;
            this.colActionBudget.Width = 91;
            // 
            // colActionWorkUnits
            // 
            this.colActionWorkUnits.Caption = "Action Work Units";
            this.colActionWorkUnits.FieldName = "ActionWorkUnits";
            this.colActionWorkUnits.Name = "colActionWorkUnits";
            this.colActionWorkUnits.OptionsColumn.AllowEdit = false;
            this.colActionWorkUnits.OptionsColumn.AllowFocus = false;
            this.colActionWorkUnits.OptionsColumn.ReadOnly = true;
            this.colActionWorkUnits.Visible = true;
            this.colActionWorkUnits.VisibleIndex = 15;
            this.colActionWorkUnits.Width = 106;
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Action Schedule of Rates";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 16;
            this.colActionScheduleOfRates.Width = 141;
            // 
            // colActionBudgetedRateDescription
            // 
            this.colActionBudgetedRateDescription.Caption = "Action Budgeted Rate Description";
            this.colActionBudgetedRateDescription.FieldName = "ActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.Name = "colActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRateDescription.Visible = true;
            this.colActionBudgetedRateDescription.VisibleIndex = 17;
            this.colActionBudgetedRateDescription.Width = 182;
            // 
            // colActionBudgetedRate
            // 
            this.colActionBudgetedRate.Caption = "Action Budgeted Rate";
            this.colActionBudgetedRate.FieldName = "ActionBudgetedRate";
            this.colActionBudgetedRate.Name = "colActionBudgetedRate";
            this.colActionBudgetedRate.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRate.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRate.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRate.Visible = true;
            this.colActionBudgetedRate.VisibleIndex = 19;
            this.colActionBudgetedRate.Width = 126;
            // 
            // colActionBudgetedCost
            // 
            this.colActionBudgetedCost.Caption = "Action Budgeted Cost";
            this.colActionBudgetedCost.FieldName = "ActionBudgetedCost";
            this.colActionBudgetedCost.Name = "colActionBudgetedCost";
            this.colActionBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedCost.Visible = true;
            this.colActionBudgetedCost.VisibleIndex = 21;
            this.colActionBudgetedCost.Width = 125;
            // 
            // colActionActualRateDescription
            // 
            this.colActionActualRateDescription.Caption = "Action Actual Rate Description";
            this.colActionActualRateDescription.FieldName = "ActionActualRateDescription";
            this.colActionActualRateDescription.Name = "colActionActualRateDescription";
            this.colActionActualRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionActualRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionActualRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionActualRateDescription.Visible = true;
            this.colActionActualRateDescription.VisibleIndex = 18;
            this.colActionActualRateDescription.Width = 166;
            // 
            // colActionActualRate
            // 
            this.colActionActualRate.Caption = "Ation Actual Rate";
            this.colActionActualRate.FieldName = "ActionActualRate";
            this.colActionActualRate.Name = "colActionActualRate";
            this.colActionActualRate.OptionsColumn.AllowEdit = false;
            this.colActionActualRate.OptionsColumn.AllowFocus = false;
            this.colActionActualRate.OptionsColumn.ReadOnly = true;
            this.colActionActualRate.Visible = true;
            this.colActionActualRate.VisibleIndex = 20;
            this.colActionActualRate.Width = 105;
            // 
            // colActionActualCost
            // 
            this.colActionActualCost.Caption = "Action Actual Cost";
            this.colActionActualCost.FieldName = "ActionActualCost";
            this.colActionActualCost.Name = "colActionActualCost";
            this.colActionActualCost.OptionsColumn.AllowEdit = false;
            this.colActionActualCost.OptionsColumn.AllowFocus = false;
            this.colActionActualCost.OptionsColumn.ReadOnly = true;
            this.colActionActualCost.Visible = true;
            this.colActionActualCost.VisibleIndex = 22;
            this.colActionActualCost.Width = 109;
            // 
            // colActionDateLastModified
            // 
            this.colActionDateLastModified.Caption = "Action Date Last Modified";
            this.colActionDateLastModified.FieldName = "ActionDateLastModified";
            this.colActionDateLastModified.Name = "colActionDateLastModified";
            this.colActionDateLastModified.OptionsColumn.AllowEdit = false;
            this.colActionDateLastModified.OptionsColumn.AllowFocus = false;
            this.colActionDateLastModified.OptionsColumn.ReadOnly = true;
            this.colActionDateLastModified.Width = 143;
            // 
            // colActionWorkOrder
            // 
            this.colActionWorkOrder.Caption = "Action Work Order";
            this.colActionWorkOrder.FieldName = "ActionWorkOrder";
            this.colActionWorkOrder.Name = "colActionWorkOrder";
            this.colActionWorkOrder.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrder.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrder.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrder.Visible = true;
            this.colActionWorkOrder.VisibleIndex = 26;
            this.colActionWorkOrder.Width = 110;
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.Caption = "Action Remarks";
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 29;
            this.colActionRemarks.Width = 95;
            // 
            // colActionUserDefined1
            // 
            this.colActionUserDefined1.Caption = "Action User Defined 1";
            this.colActionUserDefined1.FieldName = "ActionUserDefined1";
            this.colActionUserDefined1.Name = "colActionUserDefined1";
            this.colActionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined1.Visible = true;
            this.colActionUserDefined1.VisibleIndex = 30;
            this.colActionUserDefined1.Width = 125;
            // 
            // colActionUserDefined2
            // 
            this.colActionUserDefined2.Caption = "Action User Defined 2";
            this.colActionUserDefined2.FieldName = "ActionUserDefined2";
            this.colActionUserDefined2.Name = "colActionUserDefined2";
            this.colActionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined2.Visible = true;
            this.colActionUserDefined2.VisibleIndex = 31;
            this.colActionUserDefined2.Width = 125;
            // 
            // colActionUserDefined3
            // 
            this.colActionUserDefined3.Caption = "Action User Defined 3";
            this.colActionUserDefined3.FieldName = "ActionUserDefined3";
            this.colActionUserDefined3.Name = "colActionUserDefined3";
            this.colActionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined3.Visible = true;
            this.colActionUserDefined3.VisibleIndex = 32;
            this.colActionUserDefined3.Width = 125;
            // 
            // colActionCostDifference
            // 
            this.colActionCostDifference.Caption = "Action Cost Difference";
            this.colActionCostDifference.FieldName = "ActionCostDifference";
            this.colActionCostDifference.Name = "colActionCostDifference";
            this.colActionCostDifference.OptionsColumn.AllowEdit = false;
            this.colActionCostDifference.OptionsColumn.AllowFocus = false;
            this.colActionCostDifference.OptionsColumn.ReadOnly = true;
            this.colActionCostDifference.Visible = true;
            this.colActionCostDifference.VisibleIndex = 23;
            this.colActionCostDifference.Width = 129;
            // 
            // colActionCompletionStatus
            // 
            this.colActionCompletionStatus.Caption = "Action Completion Status";
            this.colActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.colActionCompletionStatus.Name = "colActionCompletionStatus";
            this.colActionCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colActionCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colActionCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colActionCompletionStatus.Visible = true;
            this.colActionCompletionStatus.VisibleIndex = 24;
            this.colActionCompletionStatus.Width = 141;
            // 
            // colActionTimeliness
            // 
            this.colActionTimeliness.Caption = "Action Timeliness";
            this.colActionTimeliness.FieldName = "ActionTimeliness";
            this.colActionTimeliness.Name = "colActionTimeliness";
            this.colActionTimeliness.OptionsColumn.AllowEdit = false;
            this.colActionTimeliness.OptionsColumn.AllowFocus = false;
            this.colActionTimeliness.OptionsColumn.ReadOnly = true;
            this.colActionTimeliness.Visible = true;
            this.colActionTimeliness.VisibleIndex = 25;
            this.colActionTimeliness.Width = 102;
            // 
            // colActionWorkOrderIssueDate
            // 
            this.colActionWorkOrderIssueDate.Caption = "Action Work Order Issue Date";
            this.colActionWorkOrderIssueDate.FieldName = "ActionWorkOrderIssueDate";
            this.colActionWorkOrderIssueDate.Name = "colActionWorkOrderIssueDate";
            this.colActionWorkOrderIssueDate.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrderIssueDate.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrderIssueDate.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrderIssueDate.Visible = true;
            this.colActionWorkOrderIssueDate.VisibleIndex = 27;
            this.colActionWorkOrderIssueDate.Width = 165;
            // 
            // colActionWorkOrderCompleteDate
            // 
            this.colActionWorkOrderCompleteDate.Caption = "Action Work Order Complete Date";
            this.colActionWorkOrderCompleteDate.FieldName = "ActionWorkOrderCompleteDate";
            this.colActionWorkOrderCompleteDate.Name = "colActionWorkOrderCompleteDate";
            this.colActionWorkOrderCompleteDate.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrderCompleteDate.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrderCompleteDate.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrderCompleteDate.Visible = true;
            this.colActionWorkOrderCompleteDate.VisibleIndex = 28;
            this.colActionWorkOrderCompleteDate.Width = 184;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "<b>Calculated 1<b>";
            this.gridColumn15.FieldName = "Calculated1";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.ShowUnboundExpressionMenu = true;
            this.gridColumn15.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 33;
            this.gridColumn15.Width = 90;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "<b>Calculated 2<b>";
            this.gridColumn16.FieldName = "Calculated2";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.ShowUnboundExpressionMenu = true;
            this.gridColumn16.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 34;
            this.gridColumn16.Width = 90;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "<b>Calculated 3<b>";
            this.gridColumn17.FieldName = "Calculated3";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.ShowUnboundExpressionMenu = true;
            this.gridColumn17.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 35;
            this.gridColumn17.Width = 90;
            // 
            // colDueWithinDays1
            // 
            this.colDueWithinDays1.Caption = "Due Within [Days]";
            this.colDueWithinDays1.FieldName = "DueWithinDays";
            this.colDueWithinDays1.Name = "colDueWithinDays1";
            this.colDueWithinDays1.OptionsColumn.AllowEdit = false;
            this.colDueWithinDays1.OptionsColumn.AllowFocus = false;
            this.colDueWithinDays1.OptionsColumn.ReadOnly = true;
            this.colDueWithinDays1.Width = 100;
            // 
            // colDueWithinDaysDescription1
            // 
            this.colDueWithinDaysDescription1.Caption = "Due Within Description";
            this.colDueWithinDaysDescription1.FieldName = "DueWithinDaysDescription";
            this.colDueWithinDaysDescription1.Name = "colDueWithinDaysDescription1";
            this.colDueWithinDaysDescription1.OptionsColumn.AllowEdit = false;
            this.colDueWithinDaysDescription1.OptionsColumn.AllowFocus = false;
            this.colDueWithinDaysDescription1.OptionsColumn.ReadOnly = true;
            this.colDueWithinDaysDescription1.Visible = true;
            this.colDueWithinDaysDescription1.VisibleIndex = 4;
            this.colDueWithinDaysDescription1.Width = 156;
            // 
            // colDueDescription
            // 
            this.colDueDescription.Caption = "Due Description";
            this.colDueDescription.FieldName = "DueDescription";
            this.colDueDescription.Name = "colDueDescription";
            this.colDueDescription.OptionsColumn.AllowEdit = false;
            this.colDueDescription.OptionsColumn.AllowFocus = false;
            this.colDueDescription.OptionsColumn.ReadOnly = true;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl4;
            this.gridSplitContainer1.Location = new System.Drawing.Point(5, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer1.Size = new System.Drawing.Size(659, 311);
            this.gridSplitContainer1.TabIndex = 6;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp01337InspectionsDueBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(659, 311);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.ActiveFilterEnabled = false;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.colTreeID,
            this.colMapID,
            this.colTreeReference,
            this.colXCoordinate,
            this.colYCoordinate,
            this.colPolygonXY,
            this.colSpecies,
            this.colObjectType,
            this.colHighlight,
            this.colGridReference,
            this.colDistance,
            this.colHouseName,
            this.colAccess,
            this.colVisibility,
            this.colContext,
            this.colManagement,
            this.colLegalStatus,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnit,
            this.colNextInspectionDate,
            this.colGroundType,
            this.colDBH,
            this.colDBHRange,
            this.colHeight,
            this.colHeightRange,
            this.colProtectionType,
            this.colCrownDiameter,
            this.colPlantDate,
            this.colGroupNumber,
            this.colRiskCategory,
            this.colSiteHazardClass,
            this.colPlantSize,
            this.colPlantSource,
            this.colPlantMethod,
            this.gridColumn3,
            this.colMapLabel,
            this.colStatus,
            this.colSize,
            this.colTarget1,
            this.colTarget2,
            this.colTarget3,
            this.colLastModifiedDate,
            this.colTreeRemarks,
            this.colUser1,
            this.colUser2,
            this.colUser3,
            this.colAgeClass,
            this.colAreaHa,
            this.colReplantCount,
            this.colSurveyDate,
            this.colTreeType,
            this.colHedgeOwner,
            this.colHedgeLength,
            this.colGardenSize,
            this.colPropertyProximity,
            this.colSiteLevel,
            this.colSituation,
            this.colRiskFactor,
            this.colOwnershipName,
            this.colLinkedInspectionCount,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.colDueWithinDays,
            this.colDueWithinDaysDescription,
            this.colDueDescription1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.ColumnFilterPopupMaxRecordsCount = 10000;
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupedColumns = true;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNextInspectionDate, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Client Name";
            this.gridColumn1.FieldName = "ClientName";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 171;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Site Name";
            this.gridColumn2.FieldName = "SiteName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 183;
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 3;
            this.colTreeReference.Width = 191;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            this.colXCoordinate.Width = 83;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            this.colYCoordinate.Width = 83;
            // 
            // colPolygonXY
            // 
            this.colPolygonXY.Caption = "Polygon XY";
            this.colPolygonXY.FieldName = "PolygonXY";
            this.colPolygonXY.Name = "colPolygonXY";
            this.colPolygonXY.OptionsColumn.ReadOnly = true;
            // 
            // colSpecies
            // 
            this.colSpecies.Caption = "Species";
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.OptionsColumn.AllowEdit = false;
            this.colSpecies.OptionsColumn.AllowFocus = false;
            this.colSpecies.OptionsColumn.ReadOnly = true;
            this.colSpecies.Visible = true;
            this.colSpecies.VisibleIndex = 14;
            // 
            // colObjectType
            // 
            this.colObjectType.Caption = "Object Type";
            this.colObjectType.FieldName = "ObjectType";
            this.colObjectType.Name = "colObjectType";
            this.colObjectType.OptionsColumn.AllowEdit = false;
            this.colObjectType.OptionsColumn.AllowFocus = false;
            this.colObjectType.OptionsColumn.ReadOnly = true;
            this.colObjectType.Width = 80;
            // 
            // colHighlight
            // 
            this.colHighlight.Caption = "Highlight";
            this.colHighlight.FieldName = "Highlight";
            this.colHighlight.Name = "colHighlight";
            this.colHighlight.OptionsColumn.AllowEdit = false;
            this.colHighlight.OptionsColumn.AllowFocus = false;
            this.colHighlight.OptionsColumn.ReadOnly = true;
            // 
            // colGridReference
            // 
            this.colGridReference.Caption = "Grid Reference";
            this.colGridReference.FieldName = "GridReference";
            this.colGridReference.Name = "colGridReference";
            this.colGridReference.OptionsColumn.AllowEdit = false;
            this.colGridReference.OptionsColumn.AllowFocus = false;
            this.colGridReference.OptionsColumn.ReadOnly = true;
            // 
            // colDistance
            // 
            this.colDistance.Caption = "Distance";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            // 
            // colHouseName
            // 
            this.colHouseName.Caption = "Nearest House Name";
            this.colHouseName.FieldName = "HouseName";
            this.colHouseName.Name = "colHouseName";
            this.colHouseName.OptionsColumn.AllowEdit = false;
            this.colHouseName.OptionsColumn.AllowFocus = false;
            this.colHouseName.OptionsColumn.ReadOnly = true;
            this.colHouseName.Visible = true;
            this.colHouseName.VisibleIndex = 10;
            this.colHouseName.Width = 122;
            // 
            // colAccess
            // 
            this.colAccess.Caption = "Access";
            this.colAccess.FieldName = "Access";
            this.colAccess.Name = "colAccess";
            this.colAccess.OptionsColumn.AllowEdit = false;
            this.colAccess.OptionsColumn.AllowFocus = false;
            this.colAccess.OptionsColumn.ReadOnly = true;
            // 
            // colVisibility
            // 
            this.colVisibility.Caption = "Visibility";
            this.colVisibility.FieldName = "Visibility";
            this.colVisibility.Name = "colVisibility";
            this.colVisibility.OptionsColumn.AllowEdit = false;
            this.colVisibility.OptionsColumn.AllowFocus = false;
            this.colVisibility.OptionsColumn.ReadOnly = true;
            // 
            // colContext
            // 
            this.colContext.Caption = "Context";
            this.colContext.FieldName = "Context";
            this.colContext.Name = "colContext";
            this.colContext.OptionsColumn.AllowEdit = false;
            this.colContext.OptionsColumn.AllowFocus = false;
            this.colContext.OptionsColumn.ReadOnly = true;
            // 
            // colManagement
            // 
            this.colManagement.Caption = "Management";
            this.colManagement.FieldName = "Management";
            this.colManagement.Name = "colManagement";
            this.colManagement.OptionsColumn.AllowEdit = false;
            this.colManagement.OptionsColumn.AllowFocus = false;
            this.colManagement.OptionsColumn.ReadOnly = true;
            this.colManagement.Width = 83;
            // 
            // colLegalStatus
            // 
            this.colLegalStatus.Caption = "Legal Status";
            this.colLegalStatus.FieldName = "LegalStatus";
            this.colLegalStatus.Name = "colLegalStatus";
            this.colLegalStatus.OptionsColumn.AllowEdit = false;
            this.colLegalStatus.OptionsColumn.AllowFocus = false;
            this.colLegalStatus.OptionsColumn.ReadOnly = true;
            this.colLegalStatus.Width = 80;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection Date";
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 4;
            this.colLastInspectionDate.Width = 120;
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Inspection Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.Visible = true;
            this.colInspectionCycle.VisibleIndex = 5;
            this.colInspectionCycle.Width = 100;
            // 
            // colInspectionUnit
            // 
            this.colInspectionUnit.Caption = "Inspection Unit";
            this.colInspectionUnit.FieldName = "InspectionUnit";
            this.colInspectionUnit.Name = "colInspectionUnit";
            this.colInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colInspectionUnit.Visible = true;
            this.colInspectionUnit.VisibleIndex = 6;
            this.colInspectionUnit.Width = 93;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection Date";
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 2;
            this.colNextInspectionDate.Width = 136;
            // 
            // colGroundType
            // 
            this.colGroundType.Caption = "Ground Type";
            this.colGroundType.FieldName = "GroundType";
            this.colGroundType.Name = "colGroundType";
            this.colGroundType.OptionsColumn.AllowEdit = false;
            this.colGroundType.OptionsColumn.AllowFocus = false;
            this.colGroundType.OptionsColumn.ReadOnly = true;
            this.colGroundType.Width = 83;
            // 
            // colDBH
            // 
            this.colDBH.Caption = "DBH";
            this.colDBH.FieldName = "DBH";
            this.colDBH.Name = "colDBH";
            this.colDBH.OptionsColumn.AllowEdit = false;
            this.colDBH.OptionsColumn.AllowFocus = false;
            this.colDBH.OptionsColumn.ReadOnly = true;
            this.colDBH.Visible = true;
            this.colDBH.VisibleIndex = 17;
            // 
            // colDBHRange
            // 
            this.colDBHRange.Caption = "DBH Range";
            this.colDBHRange.FieldName = "DBHRange";
            this.colDBHRange.Name = "colDBHRange";
            this.colDBHRange.OptionsColumn.AllowEdit = false;
            this.colDBHRange.OptionsColumn.AllowFocus = false;
            this.colDBHRange.OptionsColumn.ReadOnly = true;
            this.colDBHRange.Visible = true;
            this.colDBHRange.VisibleIndex = 18;
            // 
            // colHeight
            // 
            this.colHeight.Caption = "Height";
            this.colHeight.FieldName = "Height";
            this.colHeight.Name = "colHeight";
            this.colHeight.OptionsColumn.AllowEdit = false;
            this.colHeight.OptionsColumn.AllowFocus = false;
            this.colHeight.OptionsColumn.ReadOnly = true;
            this.colHeight.Visible = true;
            this.colHeight.VisibleIndex = 19;
            // 
            // colHeightRange
            // 
            this.colHeightRange.Caption = "Height Range";
            this.colHeightRange.FieldName = "HeightRange";
            this.colHeightRange.Name = "colHeightRange";
            this.colHeightRange.OptionsColumn.AllowEdit = false;
            this.colHeightRange.OptionsColumn.AllowFocus = false;
            this.colHeightRange.OptionsColumn.ReadOnly = true;
            this.colHeightRange.Visible = true;
            this.colHeightRange.VisibleIndex = 20;
            this.colHeightRange.Width = 86;
            // 
            // colProtectionType
            // 
            this.colProtectionType.Caption = "Protection Type";
            this.colProtectionType.FieldName = "ProtectionType";
            this.colProtectionType.Name = "colProtectionType";
            this.colProtectionType.OptionsColumn.AllowEdit = false;
            this.colProtectionType.OptionsColumn.AllowFocus = false;
            this.colProtectionType.OptionsColumn.ReadOnly = true;
            this.colProtectionType.Width = 97;
            // 
            // colCrownDiameter
            // 
            this.colCrownDiameter.Caption = "Crown Diameter";
            this.colCrownDiameter.FieldName = "CrownDiameter";
            this.colCrownDiameter.Name = "colCrownDiameter";
            this.colCrownDiameter.OptionsColumn.AllowEdit = false;
            this.colCrownDiameter.OptionsColumn.AllowFocus = false;
            this.colCrownDiameter.OptionsColumn.ReadOnly = true;
            this.colCrownDiameter.Visible = true;
            this.colCrownDiameter.VisibleIndex = 21;
            this.colCrownDiameter.Width = 98;
            // 
            // colPlantDate
            // 
            this.colPlantDate.Caption = "Plant Date";
            this.colPlantDate.FieldName = "PlantDate";
            this.colPlantDate.Name = "colPlantDate";
            this.colPlantDate.OptionsColumn.AllowEdit = false;
            this.colPlantDate.OptionsColumn.AllowFocus = false;
            this.colPlantDate.OptionsColumn.ReadOnly = true;
            // 
            // colGroupNumber
            // 
            this.colGroupNumber.Caption = "Group Number";
            this.colGroupNumber.FieldName = "GroupNumber";
            this.colGroupNumber.Name = "colGroupNumber";
            this.colGroupNumber.OptionsColumn.AllowEdit = false;
            this.colGroupNumber.OptionsColumn.AllowFocus = false;
            this.colGroupNumber.OptionsColumn.ReadOnly = true;
            this.colGroupNumber.Width = 90;
            // 
            // colRiskCategory
            // 
            this.colRiskCategory.Caption = "Risk Category";
            this.colRiskCategory.FieldName = "RiskCategory";
            this.colRiskCategory.Name = "colRiskCategory";
            this.colRiskCategory.OptionsColumn.AllowEdit = false;
            this.colRiskCategory.OptionsColumn.AllowFocus = false;
            this.colRiskCategory.OptionsColumn.ReadOnly = true;
            this.colRiskCategory.Visible = true;
            this.colRiskCategory.VisibleIndex = 12;
            this.colRiskCategory.Width = 88;
            // 
            // colSiteHazardClass
            // 
            this.colSiteHazardClass.Caption = "Site Hazard Class";
            this.colSiteHazardClass.FieldName = "SiteHazardClass";
            this.colSiteHazardClass.Name = "colSiteHazardClass";
            this.colSiteHazardClass.OptionsColumn.AllowEdit = false;
            this.colSiteHazardClass.OptionsColumn.AllowFocus = false;
            this.colSiteHazardClass.OptionsColumn.ReadOnly = true;
            this.colSiteHazardClass.Visible = true;
            this.colSiteHazardClass.VisibleIndex = 13;
            this.colSiteHazardClass.Width = 104;
            // 
            // colPlantSize
            // 
            this.colPlantSize.Caption = "Plant Size";
            this.colPlantSize.FieldName = "PlantSize";
            this.colPlantSize.Name = "colPlantSize";
            this.colPlantSize.OptionsColumn.AllowEdit = false;
            this.colPlantSize.OptionsColumn.AllowFocus = false;
            this.colPlantSize.OptionsColumn.ReadOnly = true;
            // 
            // colPlantSource
            // 
            this.colPlantSource.Caption = "Plant Source";
            this.colPlantSource.FieldName = "PlantSource";
            this.colPlantSource.Name = "colPlantSource";
            this.colPlantSource.OptionsColumn.AllowEdit = false;
            this.colPlantSource.OptionsColumn.AllowFocus = false;
            this.colPlantSource.OptionsColumn.ReadOnly = true;
            this.colPlantSource.Width = 81;
            // 
            // colPlantMethod
            // 
            this.colPlantMethod.Caption = "Plant Method";
            this.colPlantMethod.FieldName = "PlantMethod";
            this.colPlantMethod.Name = "colPlantMethod";
            this.colPlantMethod.OptionsColumn.AllowEdit = false;
            this.colPlantMethod.OptionsColumn.AllowFocus = false;
            this.colPlantMethod.OptionsColumn.ReadOnly = true;
            this.colPlantMethod.Width = 84;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Postcode";
            this.gridColumn3.FieldName = "Postcode";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // colMapLabel
            // 
            this.colMapLabel.Caption = "Map Label";
            this.colMapLabel.FieldName = "MapLabel";
            this.colMapLabel.Name = "colMapLabel";
            this.colMapLabel.OptionsColumn.AllowEdit = false;
            this.colMapLabel.OptionsColumn.AllowFocus = false;
            this.colMapLabel.OptionsColumn.ReadOnly = true;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 8;
            // 
            // colSize
            // 
            this.colSize.Caption = "Size";
            this.colSize.FieldName = "Size";
            this.colSize.Name = "colSize";
            this.colSize.OptionsColumn.AllowEdit = false;
            this.colSize.OptionsColumn.AllowFocus = false;
            this.colSize.OptionsColumn.ReadOnly = true;
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 16;
            // 
            // colTarget1
            // 
            this.colTarget1.Caption = "Target 1";
            this.colTarget1.FieldName = "Target1";
            this.colTarget1.Name = "colTarget1";
            this.colTarget1.OptionsColumn.AllowEdit = false;
            this.colTarget1.OptionsColumn.AllowFocus = false;
            this.colTarget1.OptionsColumn.ReadOnly = true;
            // 
            // colTarget2
            // 
            this.colTarget2.Caption = "Target 2";
            this.colTarget2.FieldName = "Target2";
            this.colTarget2.Name = "colTarget2";
            this.colTarget2.OptionsColumn.AllowEdit = false;
            this.colTarget2.OptionsColumn.AllowFocus = false;
            this.colTarget2.OptionsColumn.ReadOnly = true;
            // 
            // colTarget3
            // 
            this.colTarget3.Caption = "Target 3";
            this.colTarget3.FieldName = "Target3";
            this.colTarget3.Name = "colTarget3";
            this.colTarget3.OptionsColumn.AllowEdit = false;
            this.colTarget3.OptionsColumn.AllowFocus = false;
            this.colTarget3.OptionsColumn.ReadOnly = true;
            // 
            // colLastModifiedDate
            // 
            this.colLastModifiedDate.Caption = "Last Modified Date";
            this.colLastModifiedDate.FieldName = "LastModifiedDate";
            this.colLastModifiedDate.Name = "colLastModifiedDate";
            this.colLastModifiedDate.OptionsColumn.AllowEdit = false;
            this.colLastModifiedDate.OptionsColumn.AllowFocus = false;
            this.colLastModifiedDate.OptionsColumn.ReadOnly = true;
            this.colLastModifiedDate.Width = 110;
            // 
            // colTreeRemarks
            // 
            this.colTreeRemarks.Caption = "Tree Remarks";
            this.colTreeRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colTreeRemarks.FieldName = "TreeRemarks";
            this.colTreeRemarks.Name = "colTreeRemarks";
            this.colTreeRemarks.OptionsColumn.ReadOnly = true;
            this.colTreeRemarks.Visible = true;
            this.colTreeRemarks.VisibleIndex = 22;
            this.colTreeRemarks.Width = 87;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colUser1
            // 
            this.colUser1.Caption = "User Defined 1";
            this.colUser1.FieldName = "User1";
            this.colUser1.Name = "colUser1";
            this.colUser1.OptionsColumn.AllowEdit = false;
            this.colUser1.OptionsColumn.AllowFocus = false;
            this.colUser1.OptionsColumn.ReadOnly = true;
            this.colUser1.Width = 92;
            // 
            // colUser2
            // 
            this.colUser2.Caption = "User Defined 2";
            this.colUser2.FieldName = "User2";
            this.colUser2.Name = "colUser2";
            this.colUser2.OptionsColumn.AllowEdit = false;
            this.colUser2.OptionsColumn.AllowFocus = false;
            this.colUser2.OptionsColumn.ReadOnly = true;
            this.colUser2.Width = 105;
            // 
            // colUser3
            // 
            this.colUser3.Caption = "User Defined 3";
            this.colUser3.FieldName = "User3";
            this.colUser3.Name = "colUser3";
            this.colUser3.OptionsColumn.AllowEdit = false;
            this.colUser3.OptionsColumn.AllowFocus = false;
            this.colUser3.OptionsColumn.ReadOnly = true;
            this.colUser3.Width = 92;
            // 
            // colAgeClass
            // 
            this.colAgeClass.Caption = "Age Class";
            this.colAgeClass.FieldName = "AgeClass";
            this.colAgeClass.Name = "colAgeClass";
            this.colAgeClass.OptionsColumn.AllowEdit = false;
            this.colAgeClass.OptionsColumn.AllowFocus = false;
            this.colAgeClass.OptionsColumn.ReadOnly = true;
            // 
            // colAreaHa
            // 
            this.colAreaHa.Caption = "Area (M�)";
            this.colAreaHa.FieldName = "AreaHa";
            this.colAreaHa.Name = "colAreaHa";
            this.colAreaHa.OptionsColumn.AllowEdit = false;
            this.colAreaHa.OptionsColumn.AllowFocus = false;
            this.colAreaHa.OptionsColumn.ReadOnly = true;
            // 
            // colReplantCount
            // 
            this.colReplantCount.Caption = "Replant Count";
            this.colReplantCount.FieldName = "ReplantCount";
            this.colReplantCount.Name = "colReplantCount";
            this.colReplantCount.OptionsColumn.AllowEdit = false;
            this.colReplantCount.OptionsColumn.AllowFocus = false;
            this.colReplantCount.OptionsColumn.ReadOnly = true;
            this.colReplantCount.Width = 90;
            // 
            // colSurveyDate
            // 
            this.colSurveyDate.Caption = "Survey Date";
            this.colSurveyDate.FieldName = "SurveyDate";
            this.colSurveyDate.Name = "colSurveyDate";
            this.colSurveyDate.OptionsColumn.AllowEdit = false;
            this.colSurveyDate.OptionsColumn.AllowFocus = false;
            this.colSurveyDate.OptionsColumn.ReadOnly = true;
            this.colSurveyDate.Width = 81;
            // 
            // colTreeType
            // 
            this.colTreeType.Caption = "Tree Type";
            this.colTreeType.FieldName = "TreeType";
            this.colTreeType.Name = "colTreeType";
            this.colTreeType.OptionsColumn.AllowEdit = false;
            this.colTreeType.OptionsColumn.AllowFocus = false;
            this.colTreeType.OptionsColumn.ReadOnly = true;
            // 
            // colHedgeOwner
            // 
            this.colHedgeOwner.Caption = "Hedge Owner";
            this.colHedgeOwner.FieldName = "HedgeOwner";
            this.colHedgeOwner.Name = "colHedgeOwner";
            this.colHedgeOwner.OptionsColumn.AllowEdit = false;
            this.colHedgeOwner.OptionsColumn.AllowFocus = false;
            this.colHedgeOwner.OptionsColumn.ReadOnly = true;
            this.colHedgeOwner.Width = 87;
            // 
            // colHedgeLength
            // 
            this.colHedgeLength.Caption = "Hedge Length";
            this.colHedgeLength.FieldName = "HedgeLength";
            this.colHedgeLength.Name = "colHedgeLength";
            this.colHedgeLength.OptionsColumn.AllowEdit = false;
            this.colHedgeLength.OptionsColumn.AllowFocus = false;
            this.colHedgeLength.OptionsColumn.ReadOnly = true;
            this.colHedgeLength.Width = 88;
            // 
            // colGardenSize
            // 
            this.colGardenSize.Caption = "Garden Size";
            this.colGardenSize.FieldName = "GardenSize";
            this.colGardenSize.Name = "colGardenSize";
            this.colGardenSize.OptionsColumn.AllowEdit = false;
            this.colGardenSize.OptionsColumn.AllowFocus = false;
            this.colGardenSize.OptionsColumn.ReadOnly = true;
            this.colGardenSize.Width = 78;
            // 
            // colPropertyProximity
            // 
            this.colPropertyProximity.Caption = "Property Proximity";
            this.colPropertyProximity.FieldName = "PropertyProximity";
            this.colPropertyProximity.Name = "colPropertyProximity";
            this.colPropertyProximity.OptionsColumn.AllowEdit = false;
            this.colPropertyProximity.OptionsColumn.AllowFocus = false;
            this.colPropertyProximity.OptionsColumn.ReadOnly = true;
            this.colPropertyProximity.Width = 110;
            // 
            // colSiteLevel
            // 
            this.colSiteLevel.Caption = "Site Level";
            this.colSiteLevel.FieldName = "SiteLevel";
            this.colSiteLevel.Name = "colSiteLevel";
            this.colSiteLevel.OptionsColumn.AllowEdit = false;
            this.colSiteLevel.OptionsColumn.AllowFocus = false;
            this.colSiteLevel.OptionsColumn.ReadOnly = true;
            // 
            // colSituation
            // 
            this.colSituation.Caption = "Situation";
            this.colSituation.FieldName = "Situation";
            this.colSituation.Name = "colSituation";
            this.colSituation.OptionsColumn.ReadOnly = true;
            // 
            // colRiskFactor
            // 
            this.colRiskFactor.Caption = "Risk Factor";
            this.colRiskFactor.FieldName = "RiskFactor";
            this.colRiskFactor.Name = "colRiskFactor";
            this.colRiskFactor.OptionsColumn.AllowEdit = false;
            this.colRiskFactor.OptionsColumn.AllowFocus = false;
            this.colRiskFactor.OptionsColumn.ReadOnly = true;
            this.colRiskFactor.Visible = true;
            this.colRiskFactor.VisibleIndex = 11;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.Caption = "Ownership";
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.OptionsColumn.AllowEdit = false;
            this.colOwnershipName.OptionsColumn.AllowFocus = false;
            this.colOwnershipName.OptionsColumn.ReadOnly = true;
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 9;
            // 
            // colLinkedInspectionCount
            // 
            this.colLinkedInspectionCount.Caption = "Linked Inspection Count";
            this.colLinkedInspectionCount.FieldName = "LinkedInspectionCount";
            this.colLinkedInspectionCount.Name = "colLinkedInspectionCount";
            this.colLinkedInspectionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedInspectionCount.Visible = true;
            this.colLinkedInspectionCount.VisibleIndex = 15;
            this.colLinkedInspectionCount.Width = 136;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "<b>Calculated 1</b>";
            this.gridColumn4.FieldName = "Calculated1";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.ShowUnboundExpressionMenu = true;
            this.gridColumn4.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 23;
            this.gridColumn4.Width = 90;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "<b>Calculated 2</b>";
            this.gridColumn5.FieldName = "Calculated2";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.ShowUnboundExpressionMenu = true;
            this.gridColumn5.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 24;
            this.gridColumn5.Width = 90;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "<b>Calculated 3</b>";
            this.gridColumn6.FieldName = "Calculated3";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.ShowUnboundExpressionMenu = true;
            this.gridColumn6.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 25;
            this.gridColumn6.Width = 90;
            // 
            // colDueWithinDays
            // 
            this.colDueWithinDays.Caption = "Inspection Due [Days]";
            this.colDueWithinDays.FieldName = "DueWithinDays";
            this.colDueWithinDays.Name = "colDueWithinDays";
            this.colDueWithinDays.OptionsColumn.AllowEdit = false;
            this.colDueWithinDays.OptionsColumn.AllowFocus = false;
            this.colDueWithinDays.OptionsColumn.ReadOnly = true;
            this.colDueWithinDays.Width = 141;
            // 
            // colDueWithinDaysDescription
            // 
            this.colDueWithinDaysDescription.Caption = "Inspection Due Description";
            this.colDueWithinDaysDescription.FieldName = "DueWithinDaysDescription";
            this.colDueWithinDaysDescription.Name = "colDueWithinDaysDescription";
            this.colDueWithinDaysDescription.OptionsColumn.AllowEdit = false;
            this.colDueWithinDaysDescription.OptionsColumn.AllowFocus = false;
            this.colDueWithinDaysDescription.OptionsColumn.ReadOnly = true;
            this.colDueWithinDaysDescription.Visible = true;
            this.colDueWithinDaysDescription.VisibleIndex = 7;
            this.colDueWithinDaysDescription.Width = 192;
            // 
            // colDueDescription1
            // 
            this.colDueDescription1.Caption = "Due Description";
            this.colDueDescription1.FieldName = "DueDescription";
            this.colDueDescription1.Name = "colDueDescription1";
            this.colDueDescription1.OptionsColumn.AllowEdit = false;
            this.colDueDescription1.OptionsColumn.AllowFocus = false;
            this.colDueDescription1.OptionsColumn.ReadOnly = true;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Root";
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.splitterItem1});
            this.layoutControlGroup5.Name = "Root";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(669, 690);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Outstanding Inspections";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Size = new System.Drawing.Size(669, 343);
            this.layoutControlGroup6.Text = "Outstanding Inspections";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gridSplitContainer1;
            this.layoutControlItem7.CustomizationFormText = "Outstanding Inspections Grid:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(663, 315);
            this.layoutControlItem7.Text = "Outstanding Inspections Grid:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Outstanding Actions";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 349);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(669, 341);
            this.layoutControlGroup7.Text = "Outstanding Actions";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.gridSplitContainer2;
            this.layoutControlItem8.CustomizationFormText = "Outstanding Actions Grid:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(663, 313);
            this.layoutControlItem8.Text = "Outstanding Actions Grid:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 343);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(669, 6);
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT2
            // 
            this.dataSet_AT2.DataSetName = "DataSet_AT";
            this.dataSet_AT2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT1
            // 
            this.dataSet_AT1.DataSetName = "DataSet_AT";
            this.dataSet_AT1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            // 
            // sp01335_AT_Job_Master_ListTableAdapter
            // 
            this.sp01335_AT_Job_Master_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp01336_AT_Contractor_Master_ListTableAdapter
            // 
            this.sp01336_AT_Contractor_Master_ListTableAdapter.ClearBeforeFill = true;
            // 
            // pmChart
            // 
            this.pmChart.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChartWizard)});
            this.pmChart.Manager = this.barManager1;
            this.pmChart.MenuCaption = "Chart Menu";
            this.pmChart.Name = "pmChart";
            // 
            // bbiChartWizard
            // 
            this.bbiChartWizard.Caption = "Chart Wizard...";
            this.bbiChartWizard.Id = 26;
            this.bbiChartWizard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiChartWizard.ImageOptions.Image")));
            this.bbiChartWizard.Name = "bbiChartWizard";
            this.bbiChartWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChartWizard_ItemClick);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // sp01457_AT_Site_Filter_ListTableAdapter
            // 
            this.sp01457_AT_Site_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AT_Monitor_Outstanding_Actions
            // 
            this.ClientSize = new System.Drawing.Size(1010, 690);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Monitor_Outstanding_Actions";
            this.Text = "Monitor Outstanding Work";
            this.Activated += new System.EventHandler(this.frm_AT_Monitor_Outstanding_Actions_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Monitor_Outstanding_Actions_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Monitor_Outstanding_Actions_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01338ATActionsDueBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).EndInit();
            this.gridSplitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01336ATContractorMasterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01335ATJobMasterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01457ATSiteFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01337InspectionsDueBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnLoadData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private System.Windows.Forms.BindingSource sp01335ATJobMasterListBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp01335_AT_Job_Master_ListTableAdapter sp01335_AT_Job_Master_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultWorkUnits;
        private System.Windows.Forms.BindingSource sp01336ATContractorMasterListBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp01336_AT_Contractor_Master_ListTableAdapter sp01336_AT_Contractor_Master_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DataSet_AT dataSet_AT1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType;
        private DevExpress.XtraGrid.Columns.GridColumn colGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colVisibility;
        private DevExpress.XtraGrid.Columns.GridColumn colContext;
        private DevExpress.XtraGrid.Columns.GridColumn colManagement;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroundType;
        private DevExpress.XtraGrid.Columns.GridColumn colDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colProtectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteHazardClass;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colMapLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget1;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget2;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget3;
        private DevExpress.XtraGrid.Columns.GridColumn colLastModifiedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colAgeClass;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaHa;
        private DevExpress.XtraGrid.Columns.GridColumn colReplantCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeType;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeLength;
        private DevExpress.XtraGrid.Columns.GridColumn colGardenSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPropertyProximity;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSituation;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedInspectionCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colHighlight;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colDueWithinDays;
        private System.Windows.Forms.BindingSource sp01337InspectionsDueBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp01337_Inspections_DueTableAdapter sp01337_Inspections_DueTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDueWithinDaysDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DataSet_AT dataSet_AT2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeVisibility;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeContext;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeManagement;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGroundType;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeProtectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeCrownDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePlantDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGroupNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSiteHazardClass;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePlantSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePlantSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePlantMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMapLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeTarget1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeTarget2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeTarget3;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeLastModifiedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeAgeClass;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeX;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeY;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeAreaHa;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplantCount;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHedgeOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHedgeLength;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGardenSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePropertyProximity;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSiteLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSituation;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeRiskFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionInspector;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionIncidentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionIncidentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionAngleToVertical;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionLastModified;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionGeneralCondition;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionVitality;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colActionOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudget;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDateLastModified;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostDifference;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTimeliness;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrderIssueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrderCompleteDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn colDueWithinDays1;
        private DevExpress.XtraGrid.Columns.GridColumn colDueWithinDaysDescription1;
        private System.Windows.Forms.BindingSource sp01338ATActionsDueBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp01338_AT_Actions_DueTableAdapter sp01338_AT_Actions_DueTableAdapter;
        private DevExpress.XtraBars.PopupMenu pmChart;
        private DevExpress.XtraBars.BarButtonItem bbiChartWizard;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.Columns.GridColumn colDueDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDueDescription1;
        private DevExpress.XtraCharts.ChartControl chartControl2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer5;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private System.Windows.Forms.BindingSource sp01457ATSiteFilterListBindingSource;
        private DataSet_ATTableAdapters.sp01457_AT_Site_Filter_ListTableAdapter sp01457_AT_Site_Filter_ListTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

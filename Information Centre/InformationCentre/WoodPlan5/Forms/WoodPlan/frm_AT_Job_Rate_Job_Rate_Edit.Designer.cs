namespace WoodPlan5
{
    partial class frm_AT_Job_Rate_Job_Rate_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Job_Rate_Job_Rate_Edit));
            this.colintJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.sp00210JobRateItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.intRateIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dtValidFromDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.dtValidToDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.monRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.strRateDescTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.decUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00213JobRateTypeListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intJobIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00214MasterJobListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDefaultWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForintRateID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintJobID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtValidFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrRateDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemFordtValidTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFormonRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordecUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00210_Job_Rate_ItemTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00210_Job_Rate_ItemTableAdapter();
            this.sp00213_Job_Rate_Type_List_With_BlankTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00213_Job_Rate_Type_List_With_BlankTableAdapter();
            this.sp00214_Master_Job_List_With_BlankTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00214_Master_Job_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp00210JobRateItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRateIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtValidFromDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtValidFromDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtValidToDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtValidToDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRateDescTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.decUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00213JobRateTypeListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intJobIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00214MasterJobListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintJobID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtValidFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRateDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtValidTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colintJobID
            // 
            this.colintJobID.Caption = "Job ID";
            this.colintJobID.FieldName = "intJobID";
            this.colintJobID.Name = "colintJobID";
            this.colintJobID.OptionsColumn.AllowEdit = false;
            this.colintJobID.OptionsColumn.AllowFocus = false;
            this.colintJobID.OptionsColumn.ReadOnly = true;
            this.colintJobID.Width = 52;
            // 
            // colItemValue
            // 
            this.colItemValue.Caption = "Type ID";
            this.colItemValue.FieldName = "ItemValue";
            this.colItemValue.Name = "colItemValue";
            this.colItemValue.OptionsColumn.AllowEdit = false;
            this.colItemValue.OptionsColumn.AllowFocus = false;
            this.colItemValue.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intRateIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dtValidFromDateEdit);
            this.dataLayoutControl1.Controls.Add(this.dtValidToDateEdit);
            this.dataLayoutControl1.Controls.Add(this.monRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.strRateDescTextEdit);
            this.dataLayoutControl1.Controls.Add(this.decUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intJobIDGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp00210JobRateItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintRateID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1292, 310, 450, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00210JobRateItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(128, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // sp00210JobRateItemBindingSource
            // 
            this.sp00210JobRateItemBindingSource.DataMember = "sp00210_Job_Rate_Item";
            this.sp00210JobRateItemBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // intRateIDTextEdit
            // 
            this.intRateIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00210JobRateItemBindingSource, "intRateID", true));
            this.intRateIDTextEdit.Location = new System.Drawing.Point(85, 12);
            this.intRateIDTextEdit.MenuManager = this.barManager1;
            this.intRateIDTextEdit.Name = "intRateIDTextEdit";
            this.intRateIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intRateIDTextEdit, true);
            this.intRateIDTextEdit.Size = new System.Drawing.Size(531, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intRateIDTextEdit, optionsSpelling1);
            this.intRateIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intRateIDTextEdit.TabIndex = 4;
            // 
            // dtValidFromDateEdit
            // 
            this.dtValidFromDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00210JobRateItemBindingSource, "dtValidFrom", true));
            this.dtValidFromDateEdit.EditValue = null;
            this.dtValidFromDateEdit.Location = new System.Drawing.Point(126, 107);
            this.dtValidFromDateEdit.MenuManager = this.barManager1;
            this.dtValidFromDateEdit.Name = "dtValidFromDateEdit";
            this.dtValidFromDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Clear Date [No Start Date]", null, null, true)});
            this.dtValidFromDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtValidFromDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtValidFromDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dtValidFromDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtValidFromDateEdit.Properties.NullDate = "01/01/1900 00:00:00";
            this.dtValidFromDateEdit.Size = new System.Drawing.Size(154, 20);
            this.dtValidFromDateEdit.StyleController = this.dataLayoutControl1;
            this.dtValidFromDateEdit.TabIndex = 6;
            this.dtValidFromDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtValidFromDateEdit_ButtonClick);
            // 
            // dtValidToDateEdit
            // 
            this.dtValidToDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00210JobRateItemBindingSource, "dtValidTo", true));
            this.dtValidToDateEdit.EditValue = null;
            this.dtValidToDateEdit.Location = new System.Drawing.Point(126, 131);
            this.dtValidToDateEdit.MenuManager = this.barManager1;
            this.dtValidToDateEdit.Name = "dtValidToDateEdit";
            this.dtValidToDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Date [No End Date]", null, null, true)});
            this.dtValidToDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtValidToDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtValidToDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dtValidToDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtValidToDateEdit.Properties.NullDate = "01/01/1900 00:00:00";
            this.dtValidToDateEdit.Size = new System.Drawing.Size(154, 20);
            this.dtValidToDateEdit.StyleController = this.dataLayoutControl1;
            this.dtValidToDateEdit.TabIndex = 7;
            this.dtValidToDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtValidToDateEdit_ButtonClick);
            // 
            // monRateSpinEdit
            // 
            this.monRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00210JobRateItemBindingSource, "monRate", true));
            this.monRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.monRateSpinEdit.Location = new System.Drawing.Point(126, 155);
            this.monRateSpinEdit.MenuManager = this.barManager1;
            this.monRateSpinEdit.Name = "monRateSpinEdit";
            this.monRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.monRateSpinEdit.Properties.Mask.EditMask = "c";
            this.monRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.monRateSpinEdit.Size = new System.Drawing.Size(154, 20);
            this.monRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.monRateSpinEdit.TabIndex = 8;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00210JobRateItemBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 283);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(556, 162);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling2);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 9;
            // 
            // strRateDescTextEdit
            // 
            this.strRateDescTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00210JobRateItemBindingSource, "strRateDesc", true));
            this.strRateDescTextEdit.Location = new System.Drawing.Point(126, 59);
            this.strRateDescTextEdit.MenuManager = this.barManager1;
            this.strRateDescTextEdit.Name = "strRateDescTextEdit";
            this.strRateDescTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRateDescTextEdit, true);
            this.strRateDescTextEdit.Size = new System.Drawing.Size(490, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRateDescTextEdit, optionsSpelling3);
            this.strRateDescTextEdit.StyleController = this.dataLayoutControl1;
            this.strRateDescTextEdit.TabIndex = 10;
            this.strRateDescTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strRateDescTextEdit_Validating);
            // 
            // decUnitsSpinEdit
            // 
            this.decUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00210JobRateItemBindingSource, "decUnits", true));
            this.decUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.decUnitsSpinEdit.Location = new System.Drawing.Point(126, 179);
            this.decUnitsSpinEdit.MenuManager = this.barManager1;
            this.decUnitsSpinEdit.Name = "decUnitsSpinEdit";
            this.decUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.decUnitsSpinEdit.Properties.Mask.EditMask = "######0.00 Units";
            this.decUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.decUnitsSpinEdit.Size = new System.Drawing.Size(154, 20);
            this.decUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.decUnitsSpinEdit.TabIndex = 11;
            // 
            // intTypeGridLookUpEdit
            // 
            this.intTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00210JobRateItemBindingSource, "intType", true));
            this.intTypeGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intTypeGridLookUpEdit.Location = new System.Drawing.Point(126, 83);
            this.intTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.intTypeGridLookUpEdit.Name = "intTypeGridLookUpEdit";
            this.intTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.intTypeGridLookUpEdit.Properties.DataSource = this.sp00213JobRateTypeListWithBlankBindingSource;
            this.intTypeGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intTypeGridLookUpEdit.Properties.NullText = "";
            this.intTypeGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intTypeGridLookUpEdit.Properties.ValueMember = "ItemValue";
            this.intTypeGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.intTypeGridLookUpEdit.Size = new System.Drawing.Size(220, 20);
            this.intTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intTypeGridLookUpEdit.TabIndex = 12;
            this.intTypeGridLookUpEdit.TabStop = false;
            this.intTypeGridLookUpEdit.EditValueChanged += new System.EventHandler(this.intTypeGridLookUpEdit_EditValueChanged);
            this.intTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.intTypeGridLookUpEdit_Validating);
            this.intTypeGridLookUpEdit.Validated += new System.EventHandler(this.intTypeGridLookUpEdit_Validated);
            // 
            // sp00213JobRateTypeListWithBlankBindingSource
            // 
            this.sp00213JobRateTypeListWithBlankBindingSource.DataMember = "sp00213_Job_Rate_Type_List_With_Blank";
            this.sp00213JobRateTypeListWithBlankBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemDescription,
            this.colItemOrder,
            this.colItemValue});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Rate Type";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 235;
            // 
            // colItemOrder
            // 
            this.colItemOrder.Caption = "Order";
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.AllowEdit = false;
            this.colItemOrder.OptionsColumn.AllowFocus = false;
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            // 
            // intJobIDGridLookUpEdit
            // 
            this.intJobIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00210JobRateItemBindingSource, "intJobID", true));
            this.intJobIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intJobIDGridLookUpEdit.Location = new System.Drawing.Point(126, 35);
            this.intJobIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intJobIDGridLookUpEdit.Name = "intJobIDGridLookUpEdit";
            this.intJobIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.intJobIDGridLookUpEdit.Properties.DataSource = this.sp00214MasterJobListWithBlankBindingSource;
            this.intJobIDGridLookUpEdit.Properties.DisplayMember = "strJobDescription";
            this.intJobIDGridLookUpEdit.Properties.NullText = "";
            this.intJobIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.intJobIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intJobIDGridLookUpEdit.Properties.ValueMember = "intJobID";
            this.intJobIDGridLookUpEdit.Properties.View = this.gridView1;
            this.intJobIDGridLookUpEdit.Size = new System.Drawing.Size(490, 20);
            this.intJobIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intJobIDGridLookUpEdit.TabIndex = 5;
            this.intJobIDGridLookUpEdit.TabStop = false;
            this.intJobIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.intJobIDGridLookUpEdit_Validating);
            // 
            // sp00214MasterJobListWithBlankBindingSource
            // 
            this.sp00214MasterJobListWithBlankBindingSource.DataMember = "sp00214_Master_Job_List_With_Blank";
            this.sp00214MasterJobListWithBlankBindingSource.DataSource = this.dataSet_AT;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDefaultWorkUnits,
            this.colintJobID,
            this.colstrJobCode,
            this.colstrJobDescription,
            this.colstrRemarks});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colintJobID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrJobDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDefaultWorkUnits
            // 
            this.colDefaultWorkUnits.Caption = "Default Work Units";
            this.colDefaultWorkUnits.FieldName = "DefaultWorkUnits";
            this.colDefaultWorkUnits.Name = "colDefaultWorkUnits";
            this.colDefaultWorkUnits.OptionsColumn.AllowEdit = false;
            this.colDefaultWorkUnits.OptionsColumn.AllowFocus = false;
            this.colDefaultWorkUnits.OptionsColumn.ReadOnly = true;
            this.colDefaultWorkUnits.Visible = true;
            this.colDefaultWorkUnits.VisibleIndex = 2;
            this.colDefaultWorkUnits.Width = 111;
            // 
            // colstrJobCode
            // 
            this.colstrJobCode.Caption = "Job Code";
            this.colstrJobCode.FieldName = "strJobCode";
            this.colstrJobCode.Name = "colstrJobCode";
            this.colstrJobCode.OptionsColumn.AllowEdit = false;
            this.colstrJobCode.OptionsColumn.AllowFocus = false;
            this.colstrJobCode.OptionsColumn.ReadOnly = true;
            this.colstrJobCode.Visible = true;
            this.colstrJobCode.VisibleIndex = 1;
            this.colstrJobCode.Width = 124;
            // 
            // colstrJobDescription
            // 
            this.colstrJobDescription.Caption = "Job Description";
            this.colstrJobDescription.FieldName = "strJobDescription";
            this.colstrJobDescription.Name = "colstrJobDescription";
            this.colstrJobDescription.OptionsColumn.AllowEdit = false;
            this.colstrJobDescription.OptionsColumn.AllowFocus = false;
            this.colstrJobDescription.OptionsColumn.ReadOnly = true;
            this.colstrJobDescription.Visible = true;
            this.colstrJobDescription.VisibleIndex = 0;
            this.colstrJobDescription.Width = 300;
            // 
            // colstrRemarks
            // 
            this.colstrRemarks.Caption = "Remarks";
            this.colstrRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colstrRemarks.FieldName = "strRemarks";
            this.colstrRemarks.Name = "colstrRemarks";
            this.colstrRemarks.Visible = true;
            this.colstrRemarks.VisibleIndex = 3;
            this.colstrRemarks.Width = 103;
            // 
            // ItemForintRateID
            // 
            this.ItemForintRateID.Control = this.intRateIDTextEdit;
            this.ItemForintRateID.CustomizationFormText = "Rate ID:";
            this.ItemForintRateID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintRateID.Name = "ItemForintRateID";
            this.ItemForintRateID.Size = new System.Drawing.Size(608, 24);
            this.ItemForintRateID.Text = "Rate ID:";
            this.ItemForintRateID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 481);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintJobID,
            this.ItemFordtValidFrom,
            this.ItemForintType,
            this.layoutControlGroup4,
            this.emptySpaceItem1,
            this.ItemForstrRateDesc,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.ItemFordtValidTo,
            this.ItemFormonRate,
            this.ItemFordecUnits,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 461);
            // 
            // ItemForintJobID
            // 
            this.ItemForintJobID.Control = this.intJobIDGridLookUpEdit;
            this.ItemForintJobID.CustomizationFormText = "Linked To Job:";
            this.ItemForintJobID.Location = new System.Drawing.Point(0, 23);
            this.ItemForintJobID.Name = "ItemForintJobID";
            this.ItemForintJobID.Size = new System.Drawing.Size(608, 24);
            this.ItemForintJobID.Text = "Linked To Job:";
            this.ItemForintJobID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemFordtValidFrom
            // 
            this.ItemFordtValidFrom.Control = this.dtValidFromDateEdit;
            this.ItemFordtValidFrom.CustomizationFormText = "Valid From:";
            this.ItemFordtValidFrom.Location = new System.Drawing.Point(0, 95);
            this.ItemFordtValidFrom.MaxSize = new System.Drawing.Size(272, 24);
            this.ItemFordtValidFrom.MinSize = new System.Drawing.Size(272, 24);
            this.ItemFordtValidFrom.Name = "ItemFordtValidFrom";
            this.ItemFordtValidFrom.Size = new System.Drawing.Size(272, 24);
            this.ItemFordtValidFrom.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtValidFrom.Text = "Valid From:";
            this.ItemFordtValidFrom.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForintType
            // 
            this.ItemForintType.Control = this.intTypeGridLookUpEdit;
            this.ItemForintType.CustomizationFormText = "Rate Type:";
            this.ItemForintType.Location = new System.Drawing.Point(0, 71);
            this.ItemForintType.MaxSize = new System.Drawing.Size(338, 24);
            this.ItemForintType.MinSize = new System.Drawing.Size(338, 24);
            this.ItemForintType.Name = "ItemForintType";
            this.ItemForintType.Size = new System.Drawing.Size(338, 24);
            this.ItemForintType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForintType.Text = "Rate Type:";
            this.ItemForintType.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details:";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 201);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(608, 260);
            this.layoutControlGroup4.Text = "Details:";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 214);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImage")));
            this.layoutControlGroup3.CustomizationFormText = "Remarks:";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(560, 166);
            this.layoutControlGroup3.Text = "Remarks:";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(560, 166);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 191);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrRateDesc
            // 
            this.ItemForstrRateDesc.Control = this.strRateDescTextEdit;
            this.ItemForstrRateDesc.CustomizationFormText = "Rate Description:";
            this.ItemForstrRateDesc.Location = new System.Drawing.Point(0, 47);
            this.ItemForstrRateDesc.Name = "ItemForstrRateDesc";
            this.ItemForstrRateDesc.Size = new System.Drawing.Size(608, 24);
            this.ItemForstrRateDesc.Text = "Rate Description:";
            this.ItemForstrRateDesc.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(116, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(116, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(116, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(116, 23);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(316, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(292, 23);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(272, 95);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(336, 96);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemFordtValidTo
            // 
            this.ItemFordtValidTo.Control = this.dtValidToDateEdit;
            this.ItemFordtValidTo.CustomizationFormText = "Valid To:";
            this.ItemFordtValidTo.Location = new System.Drawing.Point(0, 119);
            this.ItemFordtValidTo.MaxSize = new System.Drawing.Size(272, 24);
            this.ItemFordtValidTo.MinSize = new System.Drawing.Size(272, 24);
            this.ItemFordtValidTo.Name = "ItemFordtValidTo";
            this.ItemFordtValidTo.Size = new System.Drawing.Size(272, 24);
            this.ItemFordtValidTo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtValidTo.Text = "Valid To:";
            this.ItemFordtValidTo.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemFormonRate
            // 
            this.ItemFormonRate.Control = this.monRateSpinEdit;
            this.ItemFormonRate.CustomizationFormText = "Rate:";
            this.ItemFormonRate.Location = new System.Drawing.Point(0, 143);
            this.ItemFormonRate.MaxSize = new System.Drawing.Size(272, 24);
            this.ItemFormonRate.MinSize = new System.Drawing.Size(272, 24);
            this.ItemFormonRate.Name = "ItemFormonRate";
            this.ItemFormonRate.Size = new System.Drawing.Size(272, 24);
            this.ItemFormonRate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFormonRate.Text = "Rate:";
            this.ItemFormonRate.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemFordecUnits
            // 
            this.ItemFordecUnits.Control = this.decUnitsSpinEdit;
            this.ItemFordecUnits.CustomizationFormText = "Apply to Greater Than:";
            this.ItemFordecUnits.Location = new System.Drawing.Point(0, 167);
            this.ItemFordecUnits.MaxSize = new System.Drawing.Size(272, 24);
            this.ItemFordecUnits.MinSize = new System.Drawing.Size(272, 24);
            this.ItemFordecUnits.Name = "ItemFordecUnits";
            this.ItemFordecUnits.Size = new System.Drawing.Size(272, 24);
            this.ItemFordecUnits.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordecUnits.Text = "Apply to Greater Than:";
            this.ItemFordecUnits.TextSize = new System.Drawing.Size(111, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(338, 71);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(270, 24);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00210_Job_Rate_ItemTableAdapter
            // 
            this.sp00210_Job_Rate_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00213_Job_Rate_Type_List_With_BlankTableAdapter
            // 
            this.sp00213_Job_Rate_Type_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00214_Master_Job_List_With_BlankTableAdapter
            // 
            this.sp00214_Master_Job_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AT_Job_Rate_Job_Rate_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Job_Rate_Job_Rate_Edit";
            this.Text = "Job Rate Edit";
            this.Activated += new System.EventHandler(this.frm_AT_Job_Rate_Job_Rate_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Job_Rate_Job_Rate_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Job_Rate_Job_Rate_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp00210JobRateItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRateIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtValidFromDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtValidFromDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtValidToDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtValidToDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRateDescTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.decUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00213JobRateTypeListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intJobIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00214MasterJobListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintJobID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtValidFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRateDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtValidTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormonRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit intRateIDTextEdit;
        private System.Windows.Forms.BindingSource sp00210JobRateItemBindingSource;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.DateEdit dtValidFromDateEdit;
        private DevExpress.XtraEditors.DateEdit dtValidToDateEdit;
        private DevExpress.XtraEditors.SpinEdit monRateSpinEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit strRateDescTextEdit;
        private DevExpress.XtraEditors.SpinEdit decUnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintRateID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintJobID;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtValidFrom;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtValidTo;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormonRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRateDesc;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordecUnits;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintType;
        private WoodPlan5.DataSet_ATTableAdapters.sp00210_Job_Rate_ItemTableAdapter sp00210_Job_Rate_ItemTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.GridLookUpEdit intTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit intJobIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private System.Windows.Forms.BindingSource sp00213JobRateTypeListWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colItemValue;
        private WoodPlan5.DataSet_ATTableAdapters.sp00213_Job_Rate_Type_List_With_BlankTableAdapter sp00213_Job_Rate_Type_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.BindingSource sp00214MasterJobListWithBlankBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00214_Master_Job_List_With_BlankTableAdapter sp00214_Master_Job_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colintJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}

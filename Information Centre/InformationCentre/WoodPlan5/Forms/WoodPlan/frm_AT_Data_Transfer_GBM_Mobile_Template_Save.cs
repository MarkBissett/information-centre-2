using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // for StreamReader //
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors.DXErrorProvider;  // For Validation Rules //
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;  // Mouse down Args for GridView Mouse Down //

namespace WoodPlan5
{
     public partial class frm_AT_Data_Transfer_GBM_Mobile_Template_Save : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;
        BaseObjects.GridCheckMarksSelection selection1;
        /// <summary>
        /// 
        /// </summary>
        public int intTemplateID = 0;
        /// <summary>
        /// 
        /// </summary>
        public int intTemplateCreatedByID = 0;
        /// <summary>
        /// 
        /// </summary>
        public string strTemplateDescription = "";
        /// <summary>
        /// 
        /// </summary>
        public string strTemplateRemarks = "";
        /// <summary>
        /// 
        /// </summary>
        public int intTemplateShareType = 0;
        /// <summary>
        /// 
        /// </summary>
        public string strTemplateShareGroups = "";
        
        #endregion
        
         public frm_AT_Data_Transfer_GBM_Mobile_Template_Save()
        {
            InitializeComponent();

            InitValidationRules();
        }

        private void InitValidationRules()
        {
            // Create Rule // 
            ConditionValidationRule notEmptyValidationRule = new ConditionValidationRule();
            notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank;
            notEmptyValidationRule.ErrorText = "Please enter a value [100 characters maximum]";

            // Link Rule to control //
            dxValidationProvider1.SetValidationRule(textEdit1, notEmptyValidationRule);
        }

        private void frm_AT_Data_Transfer_GBM_Mobile_Template_Save_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;
            textEdit1.Text = "";

            dxValidationProvider1.ValidationMode = ValidationMode.Auto;
            dxValidationProvider1.Validate();

            // Add record selection checkboxes to grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
                     
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();

            sp00105_export_to_GBM_save_template_access_group_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00105_export_to_GBM_save_template_access_group_listTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00105_export_to_GBM_save_template_access_group_list, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);
            view.EndUpdate();

            InitValidationRules();

            if (this.GlobalSettings.PersonType == 0)  // Super User //
            {
                checkEdit4.Enabled = true;
            }
            if (dataSet_AT_DataTransfer.sp00105_export_to_GBM_save_template_access_group_list.Rows.Count == 0)
            {
                checkEdit5.Enabled = false;  // No groups available to share with so disable option //
            }
        }

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No groups available to share with");
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            strTemplateDescription = textEdit1.Text;
            if (strTemplateDescription.Trim() == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Template Description has been entered!\n\nPlease enter a description before proceeding.", "Save Template As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            strTemplateShareGroups = "";
         
            // Check the Layout Descriptor is Unique //
            int intMatchingDescriptors = 0;
            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter GetCount = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            GetCount.ChangeConnectionString(strConnectionString);
            intMatchingDescriptors = Convert.ToInt32(GetCount.sp00106_export_to_GBM_save_template_check_descriptor_unique(strTemplateDescription));
            if (intMatchingDescriptors > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Template Description entered [" + strTemplateDescription + "] has already been linked to another template!\n\nPlease enter a different description before proceeding.", "Save Template As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (checkEdit5.Checked)
            {
                GridView view = (GridView)gridControl1.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        strTemplateShareGroups += Convert.ToString(view.GetRowCellValue(i, "GroupID")) + ",";
                    }
                }
                if (strTemplateShareGroups == "")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("You have set the Sharing option to 'Only staff in the following groups...', but you have not chosen any groups to share with!\n\nTip: To specify groups, tick the tick box next to their name.\n\nIf you cannot see any groups in the list then you do not have access to any groups. Contact your system administrator for advice.", "Save Template As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to save the current template?", "Save Template As", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
           
            intTemplateID = 0;
            intTemplateCreatedByID = this.GlobalSettings.UserID;
            strTemplateRemarks = memoEdit1.Text;
            intTemplateShareType = 0;
            if (checkEdit4.Checked)
            {
                intTemplateShareType = 1;
            }
            else if (checkEdit5.Checked)
            {
                intTemplateShareType = 2;
            }

            if (this.ValidateChildren() == true)
            {
                this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails validation test earlier in this process //
                this.Close();
            }
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit3.Checked)
            {
                gridControl1.Enabled = false;
            }
        }

        private void checkEdit4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit4.Checked)
            {
                gridControl1.Enabled = false;
            }
        }

        private void checkEdit5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit5.Checked)
            {
                gridControl1.Enabled = true;
            }
        }
 
    }
}


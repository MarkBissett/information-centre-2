using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_AT_Job_Rate_Adjust_By_Uplift_Amount : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        #endregion

        public frm_AT_Job_Rate_Adjust_By_Uplift_Amount()
        {
            InitializeComponent();
        }

        private void frm_AT_Job_Rate_Adjust_By_Uplift_Amount_Load(object sender, EventArgs e)
        {
            this.FormID = 20114;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            barStaticItemInformation.Caption = "Records Selected For Update: " + intRecordCount.ToString();
        }


        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditAmount.Properties.Mask.EditMask = "P";  // Percentage //
            }
            else
            {
                spinEditAmount.Properties.Mask.EditMask = "c";  // Currency //
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditAmount.Properties.Mask.EditMask = "c";  // Currency //
            }
            else
            {
                spinEditAmount.Properties.Mask.EditMask = "P";  // Percentage //
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(spinEditAmount.EditValue) == (decimal)0.00)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter the Amount before proceeding.", "Adjust Rates by Uplift Amount", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Updating Rates...");
            fProgress.Show();
            Application.DoEvents();
            DataSet_ATTableAdapters.QueriesTableAdapter UpdateRates = new DataSet_ATTableAdapters.QueriesTableAdapter();
            UpdateRates.ChangeConnectionString(strConnectionString);
            try
            {
                UpdateRates.sp00215_Adjust_Rates_By_Uplift_Amount(strRecordIDs, Convert.ToDecimal(spinEditAmount.EditValue), (checkEdit1.Checked ? "percentage" : "fixed amount"));
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while updating the rates [" + ex.Message + "]!\n\nTry again - if the problem persists, contact Technical Support.", "Adjust Rates by Uplift Amount", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            fProgress.SetProgressValue(100);
            if (this.GlobalSettings.ShowConfirmations == 1)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Rates Updated Successfully");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }
            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }




    
    }
}


namespace WoodPlan5
{
    partial class frm_AT_Work_Order_Select_On_Map_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.colCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01410ATWorkOrdersSelectOnMapAddBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuingBody = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedPreviousWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidents = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedMapCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnLoadWorkOrders = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.sp01410_AT_WorkOrders_Select_On_Map_AddTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01410_AT_WorkOrders_Select_On_Map_AddTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01410ATWorkOrdersSelectOnMapAddBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(823, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 432);
            this.barDockControlBottom.Size = new System.Drawing.Size(823, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 432);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(823, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 432);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colCompletionStatus
            // 
            this.colCompletionStatus.Caption = "Completion Status";
            this.colCompletionStatus.FieldName = "CompletionStatus";
            this.colCompletionStatus.Name = "colCompletionStatus";
            this.colCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colCompletionStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompletionStatus.Visible = true;
            this.colCompletionStatus.VisibleIndex = 3;
            this.colCompletionStatus.Width = 108;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01410ATWorkOrdersSelectOnMapAddBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(819, 300);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01410ATWorkOrdersSelectOnMapAddBindingSource
            // 
            this.sp01410ATWorkOrdersSelectOnMapAddBindingSource.DataMember = "sp01410_AT_WorkOrders_Select_On_Map_Add";
            this.sp01410ATWorkOrdersSelectOnMapAddBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderID2,
            this.colPaddedWorkOrderID,
            this.colContractor,
            this.colIssuingBody,
            this.colWorkOrderType,
            this.colIssuedBy,
            this.colIssueDate,
            this.colPriority,
            this.colCompletionStatus,
            this.colPaddedPreviousWorkOrderID,
            this.colTotalCost,
            this.colWorkUnits,
            this.colCompletedDate,
            this.colEmailDetails,
            this.Calculated1,
            this.Calculated2,
            this.Calculated3,
            this.colIncidents,
            this.colRemarks2,
            this.colWorkOrderUserDefined1,
            this.colWorkOrderUserDefined2,
            this.colWorkOrderUserDefined3,
            this.colLinkedMapCount,
            this.colLinkedActionCount});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colCompletionStatus;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "Complete";
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colWorkOrderID2
            // 
            this.colWorkOrderID2.Caption = "Work Order ID";
            this.colWorkOrderID2.FieldName = "WorkOrderID";
            this.colWorkOrderID2.Name = "colWorkOrderID2";
            this.colWorkOrderID2.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID2.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID2.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderID2.Width = 82;
            // 
            // colPaddedWorkOrderID
            // 
            this.colPaddedWorkOrderID.Caption = "Work Order Ref";
            this.colPaddedWorkOrderID.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID.Name = "colPaddedWorkOrderID";
            this.colPaddedWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPaddedWorkOrderID.Visible = true;
            this.colPaddedWorkOrderID.VisibleIndex = 0;
            this.colPaddedWorkOrderID.Width = 111;
            // 
            // colContractor
            // 
            this.colContractor.Caption = "Contractor";
            this.colContractor.FieldName = "Contractor";
            this.colContractor.Name = "colContractor";
            this.colContractor.OptionsColumn.AllowEdit = false;
            this.colContractor.OptionsColumn.AllowFocus = false;
            this.colContractor.OptionsColumn.ReadOnly = true;
            this.colContractor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContractor.Visible = true;
            this.colContractor.VisibleIndex = 4;
            this.colContractor.Width = 121;
            // 
            // colIssuingBody
            // 
            this.colIssuingBody.Caption = "Issuing Body";
            this.colIssuingBody.FieldName = "IssuingBody";
            this.colIssuingBody.Name = "colIssuingBody";
            this.colIssuingBody.OptionsColumn.AllowEdit = false;
            this.colIssuingBody.OptionsColumn.AllowFocus = false;
            this.colIssuingBody.OptionsColumn.ReadOnly = true;
            this.colIssuingBody.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIssuingBody.Visible = true;
            this.colIssuingBody.VisibleIndex = 5;
            this.colIssuingBody.Width = 139;
            // 
            // colWorkOrderType
            // 
            this.colWorkOrderType.Caption = "Type";
            this.colWorkOrderType.FieldName = "WorkOrderType";
            this.colWorkOrderType.Name = "colWorkOrderType";
            this.colWorkOrderType.OptionsColumn.AllowEdit = false;
            this.colWorkOrderType.OptionsColumn.AllowFocus = false;
            this.colWorkOrderType.OptionsColumn.ReadOnly = true;
            this.colWorkOrderType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderType.Visible = true;
            this.colWorkOrderType.VisibleIndex = 2;
            this.colWorkOrderType.Width = 110;
            // 
            // colIssuedBy
            // 
            this.colIssuedBy.Caption = "Issued By";
            this.colIssuedBy.FieldName = "IssuedBy";
            this.colIssuedBy.Name = "colIssuedBy";
            this.colIssuedBy.OptionsColumn.AllowEdit = false;
            this.colIssuedBy.OptionsColumn.AllowFocus = false;
            this.colIssuedBy.OptionsColumn.ReadOnly = true;
            this.colIssuedBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIssuedBy.Visible = true;
            this.colIssuedBy.VisibleIndex = 6;
            this.colIssuedBy.Width = 113;
            // 
            // colIssueDate
            // 
            this.colIssueDate.Caption = "Issue Date";
            this.colIssueDate.FieldName = "IssueDate";
            this.colIssueDate.Name = "colIssueDate";
            this.colIssueDate.OptionsColumn.AllowEdit = false;
            this.colIssueDate.OptionsColumn.AllowFocus = false;
            this.colIssueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIssueDate.Visible = true;
            this.colIssueDate.VisibleIndex = 7;
            this.colIssueDate.Width = 74;
            // 
            // colPriority
            // 
            this.colPriority.Caption = "Priority";
            this.colPriority.FieldName = "Priority";
            this.colPriority.Name = "colPriority";
            this.colPriority.OptionsColumn.AllowEdit = false;
            this.colPriority.OptionsColumn.AllowFocus = false;
            this.colPriority.OptionsColumn.ReadOnly = true;
            this.colPriority.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPriority.Visible = true;
            this.colPriority.VisibleIndex = 8;
            this.colPriority.Width = 100;
            // 
            // colPaddedPreviousWorkOrderID
            // 
            this.colPaddedPreviousWorkOrderID.Caption = "Linked To W\\O";
            this.colPaddedPreviousWorkOrderID.FieldName = "PaddedPreviousWorkOrderID";
            this.colPaddedPreviousWorkOrderID.Name = "colPaddedPreviousWorkOrderID";
            this.colPaddedPreviousWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedPreviousWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedPreviousWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedPreviousWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPaddedPreviousWorkOrderID.Visible = true;
            this.colPaddedPreviousWorkOrderID.VisibleIndex = 10;
            this.colPaddedPreviousWorkOrderID.Width = 91;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.DisplayFormat.FormatString = "c2";
            this.colTotalCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 11;
            this.colTotalCost.Width = 71;
            // 
            // colWorkUnits
            // 
            this.colWorkUnits.Caption = "Work Units";
            this.colWorkUnits.DisplayFormat.FormatString = "#.00";
            this.colWorkUnits.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colWorkUnits.FieldName = "WorkUnits";
            this.colWorkUnits.Name = "colWorkUnits";
            this.colWorkUnits.OptionsColumn.AllowEdit = false;
            this.colWorkUnits.OptionsColumn.AllowFocus = false;
            this.colWorkUnits.OptionsColumn.ReadOnly = true;
            this.colWorkUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkUnits.Visible = true;
            this.colWorkUnits.VisibleIndex = 12;
            this.colWorkUnits.Width = 74;
            // 
            // colCompletedDate
            // 
            this.colCompletedDate.Caption = "Completed Date";
            this.colCompletedDate.FieldName = "CompletedDate";
            this.colCompletedDate.Name = "colCompletedDate";
            this.colCompletedDate.OptionsColumn.AllowEdit = false;
            this.colCompletedDate.OptionsColumn.AllowFocus = false;
            this.colCompletedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompletedDate.Visible = true;
            this.colCompletedDate.VisibleIndex = 13;
            this.colCompletedDate.Width = 98;
            // 
            // colEmailDetails
            // 
            this.colEmailDetails.Caption = "Email Details";
            this.colEmailDetails.FieldName = "EmailDetails";
            this.colEmailDetails.Name = "colEmailDetails";
            this.colEmailDetails.OptionsColumn.AllowEdit = false;
            this.colEmailDetails.OptionsColumn.AllowFocus = false;
            this.colEmailDetails.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmailDetails.Width = 71;
            // 
            // Calculated1
            // 
            this.Calculated1.Caption = "<b>Calculated 1</b>";
            this.Calculated1.FieldName = "Calculated1";
            this.Calculated1.Name = "Calculated1";
            this.Calculated1.OptionsColumn.AllowEdit = false;
            this.Calculated1.OptionsColumn.AllowFocus = false;
            this.Calculated1.OptionsColumn.ReadOnly = true;
            this.Calculated1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.Calculated1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1.Visible = true;
            this.Calculated1.VisibleIndex = 19;
            this.Calculated1.Width = 90;
            // 
            // Calculated2
            // 
            this.Calculated2.Caption = "<b>Calculated 2</b>";
            this.Calculated2.FieldName = "Calculated2";
            this.Calculated2.Name = "Calculated2";
            this.Calculated2.OptionsColumn.AllowEdit = false;
            this.Calculated2.OptionsColumn.AllowFocus = false;
            this.Calculated2.OptionsColumn.ReadOnly = true;
            this.Calculated2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.Calculated2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2.Visible = true;
            this.Calculated2.VisibleIndex = 20;
            this.Calculated2.Width = 90;
            // 
            // Calculated3
            // 
            this.Calculated3.Caption = "<b>Calculated 3</b>";
            this.Calculated3.FieldName = "Calculated3";
            this.Calculated3.Name = "Calculated3";
            this.Calculated3.OptionsColumn.AllowEdit = false;
            this.Calculated3.OptionsColumn.AllowFocus = false;
            this.Calculated3.OptionsColumn.ReadOnly = true;
            this.Calculated3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.Calculated3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3.Visible = true;
            this.Calculated3.VisibleIndex = 21;
            this.Calculated3.Width = 90;
            // 
            // colIncidents
            // 
            this.colIncidents.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colIncidents.FieldName = "Incidents";
            this.colIncidents.Name = "colIncidents";
            this.colIncidents.OptionsColumn.ReadOnly = true;
            this.colIncidents.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidents.Visible = true;
            this.colIncidents.VisibleIndex = 17;
            this.colIncidents.Width = 121;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colRemarks2
            // 
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 18;
            this.colRemarks2.Width = 122;
            // 
            // colWorkOrderUserDefined1
            // 
            this.colWorkOrderUserDefined1.Caption = "User Defined 1";
            this.colWorkOrderUserDefined1.FieldName = "WorkOrderUserDefined1";
            this.colWorkOrderUserDefined1.Name = "colWorkOrderUserDefined1";
            this.colWorkOrderUserDefined1.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined1.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined1.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderUserDefined1.Visible = true;
            this.colWorkOrderUserDefined1.VisibleIndex = 14;
            this.colWorkOrderUserDefined1.Width = 92;
            // 
            // colWorkOrderUserDefined2
            // 
            this.colWorkOrderUserDefined2.Caption = "User Defined 2";
            this.colWorkOrderUserDefined2.FieldName = "WorkOrderUserDefined2";
            this.colWorkOrderUserDefined2.Name = "colWorkOrderUserDefined2";
            this.colWorkOrderUserDefined2.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined2.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined2.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderUserDefined2.Visible = true;
            this.colWorkOrderUserDefined2.VisibleIndex = 15;
            this.colWorkOrderUserDefined2.Width = 92;
            // 
            // colWorkOrderUserDefined3
            // 
            this.colWorkOrderUserDefined3.Caption = "User Defined 3";
            this.colWorkOrderUserDefined3.FieldName = "WorkOrderUserDefined3";
            this.colWorkOrderUserDefined3.Name = "colWorkOrderUserDefined3";
            this.colWorkOrderUserDefined3.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined3.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined3.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderUserDefined3.Visible = true;
            this.colWorkOrderUserDefined3.VisibleIndex = 16;
            this.colWorkOrderUserDefined3.Width = 92;
            // 
            // colLinkedMapCount
            // 
            this.colLinkedMapCount.Caption = "Linked Maps";
            this.colLinkedMapCount.FieldName = "LinkedMapCount";
            this.colLinkedMapCount.Name = "colLinkedMapCount";
            this.colLinkedMapCount.OptionsColumn.AllowEdit = false;
            this.colLinkedMapCount.OptionsColumn.AllowFocus = false;
            this.colLinkedMapCount.OptionsColumn.ReadOnly = true;
            this.colLinkedMapCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedMapCount.Visible = true;
            this.colLinkedMapCount.VisibleIndex = 1;
            this.colLinkedMapCount.Width = 79;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Linked Actions";
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 9;
            this.colLinkedActionCount.Width = 102;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(324, 401);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(424, 401);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 4);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Filter Criteria";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Work Orders";
            this.splitContainerControl1.Size = new System.Drawing.Size(823, 391);
            this.splitContainerControl1.SplitterPosition = 61;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnLoadWorkOrders);
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(819, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnLoadWorkOrders
            // 
            this.btnLoadWorkOrders.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnLoadWorkOrders.Location = new System.Drawing.Point(696, 7);
            this.btnLoadWorkOrders.Name = "btnLoadWorkOrders";
            this.btnLoadWorkOrders.Size = new System.Drawing.Size(116, 22);
            this.btnLoadWorkOrders.StyleController = this.layoutControl1;
            this.btnLoadWorkOrders.TabIndex = 12;
            this.btnLoadWorkOrders.Text = "Load Work Orders";
            this.btnLoadWorkOrders.Click += new System.EventHandler(this.btnLoadWorkOrders_Click);
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(238, 7);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Wo" +
    "rk Orders\r\nwill be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, true)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(128, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 12;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(64, 7);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Wo" +
    "rk Orders will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, true)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(125, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 11;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(819, 37);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDate;
            this.layoutControlItem1.CustomizationFormText = "From Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(186, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(186, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(186, 27);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "From Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDate;
            this.layoutControlItem2.CustomizationFormText = "To Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(186, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(177, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(177, 27);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "To Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(42, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(363, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(326, 27);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnLoadWorkOrders;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(689, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(120, 27);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(819, 300);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // sp01410_AT_WorkOrders_Select_On_Map_AddTableAdapter
            // 
            this.sp01410_AT_WorkOrders_Select_On_Map_AddTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_AT_Work_Order_Select_On_Map_Add
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(823, 432);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_AT_Work_Order_Select_On_Map_Add";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transfer Actions From Map To Work Order - Select Work Order";
            this.Load += new System.EventHandler(this.frm_AT_Work_Order_Select_On_Map_Add_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01410ATWorkOrdersSelectOnMapAddBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.SimpleButton btnLoadWorkOrders;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuingBody;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderType;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedPreviousWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailDetails;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidents;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedMapCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private System.Windows.Forms.BindingSource sp01410ATWorkOrdersSelectOnMapAddBindingSource;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01410_AT_WorkOrders_Select_On_Map_AddTableAdapter sp01410_AT_WorkOrders_Select_On_Map_AddTableAdapter;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

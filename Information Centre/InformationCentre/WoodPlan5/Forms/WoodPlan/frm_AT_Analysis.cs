using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using System.Data.SqlClient;  // Used by Declaring Binding Source through code //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Wizard;
using DevExpress.XtraCharts.Native;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_AT_Analysis : BaseObjects.frmBase
    {

        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        private ExtendedPivotGridMenu epgmMenu;  // Used to extend Pivot Grid Field Right-click Menu //
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;

        WaitDialogForm loadingForm;
        //private int i_int_SelectedLayoutID = 0;

        /// <summary>
        /// 
        /// </summary>
        public string i_str_SavedDirectoryName = "";

        //private string i_str_SelectedFilename = "";
        private int i_int_FocusedGrid = 1;
        private int i_int_AnalysisType = 2;  // Tree Data //
        
        string i_str_selected_client_ids = "";
        string i_str_selected_client_names = "";

        string i_str_selected_client_ids2 = "";
        string i_str_selected_client_names2 = "";

        string i_str_selected_client_ids3 = "";
        string i_str_selected_client_names3 = "";

        private string i_str_selected_trees = "";
        private string i_str_selected_inspections = "";
        private string i_str_selected_actions = "";

        private DataSet_Selection DS_Selection1;
        private DataSet_Selection DS_Selection2;
        private DataSet_Selection DS_Selection3;

        //private SqlDataAdapter sdaDataAdpter = null;
        //private DataSet dsDataSet = null;
 
        #endregion

        public frm_AT_Analysis()
        {
            InitializeComponent();
        }

        private void frm_AT_Analysis_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 92005;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;
            i_int_AnalysisType = 1;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            
            ProcessPermissionsForForm();

            sp01220_AT_Reports_Listings_Tree_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01232_AT_Reports_Listings_Action_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01234_AT_Data_Analysis_TreesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01235_AT_Data_Analysis_InspectionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01236_AT_Data_Analysis_ActionsTableAdapter.Connection.ConnectionString = strConnectionString;
            
            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridView1, strConnectionString);  // Trees List //
            DS_Selection2 = new Utilities.DataSet_Selection((GridView)gridView2, strConnectionString);  // Inspections List //
            DS_Selection3 = new Utilities.DataSet_Selection((GridView)gridView3, strConnectionString);  // Inspections List //
                        
            LinkChartsToPivotGrids("1,2,3");

            LinkChartToRibbonBar(); // Link Chart to Chart Gallery on Ribbon Bar //
        }

        private void Chart_Preserve_User_Interaction(ChartControl chart)
        {
            // Ensure end-user rotation working if 3D chart //
            try
            {
                SimpleDiagram3D diagram = (SimpleDiagram3D)chart.Diagram;
                if (diagram != null)
                {
                    diagram.RuntimeRotation = true;
                    diagram.RuntimeScrolling = true;
                    diagram.RuntimeZooming = true;
                }
            }
            catch
            {
            }
        }

        private void LinkChartsToPivotGrids(string strChartControls)
        {
            if (strChartControls.Contains("1"))
            {
                chartControl1.DataSource = pivotGridControl1;
                chartControl1.SeriesDataMember = "Series";
                chartControl1.SeriesTemplate.ArgumentDataMember = "Arguments";
                chartControl1.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "Values" });
                Chart_Preserve_User_Interaction(chartControl1);
            }
            if (strChartControls.Contains("2"))
            {
                chartControl2.DataSource = pivotGridControl2;
                chartControl2.SeriesDataMember = "Series";
                chartControl2.SeriesTemplate.ArgumentDataMember = "Arguments";
                chartControl2.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "Values" });
                Chart_Preserve_User_Interaction(chartControl2);
            }
            if (strChartControls.Contains("3"))
            {

                chartControl3.DataSource = pivotGridControl3;
                chartControl3.SeriesDataMember = "Series";
                chartControl3.SeriesTemplate.ArgumentDataMember = "Arguments";
                chartControl3.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "Values" });
                Chart_Preserve_User_Interaction(chartControl3);
            }
        }

        public override void PostOpen(object objParameter)
        {
            dockPanel1.Width = 450;

            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            this.Refresh();

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

        }

        public override void PostLoadView(object objParameter)
        {
            LinkChartsToPivotGrids("1,2,3");
        }

        private void frm_AT_Analysis_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
            LinkChartToRibbonBar();  // Ensure Charting items on Ribbon Bar are set correctly //
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //
                        break;
                    case 1:  // Edit Report Layout //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "rgbiChartTypes", "bbiChartPalette", "rgbiChartAppearance", "bbiRotateAxis", "bbiChartWizard", "bbiLayoutFlip", "bbiLayoutRotate", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            if (i_int_FocusedGrid == 99)
            {

                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                    iBool_AllowDelete = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                    iBool_AllowDelete = false;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 2 || i_int_FocusedGrid == 3)
            {
                string strType = "";
                string strFieldName = "";
                GridView view = null;
                switch (i_int_FocusedGrid)
                {
                    case 1:
                        view = (GridView)gridControl1.MainView;
                        strFieldName = "TreeID";
                        strType = "tree";
                        break;
                    case 2:
                        view = (GridView)gridControl2.MainView;
                        strFieldName = "InspectionID";
                        strType = "inspection";
                        break;
                    case 3:
                        view = (GridView)gridControl3.MainView;
                        strFieldName = "ActionID";
                        strType = "action";
                        break;
                    default:
                        return;
                }
                view.PostEditor();
                string strSelectedIDs = "";
                int[] intRowHandles;
                int intCount = 0;
                intRowHandles = view.GetSelectedRows();
                intCount = intRowHandles.Length;
                if (intCount <= 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                foreach (int intRowHandle in intRowHandles)
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, strFieldName)) + ',';
                }
                try
                {
                    Mapping_Functions MapFunctions = new Mapping_Functions();
                    MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, strType);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = "";
            string strSelectedTrees = "";
            string strSelectedTree = "";
            int intSelectedTreeCount = 0;
            string strSelectedInspections = "";
            string strSelectedInspection = "";
            int intSelectedInspectionCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Trees Grid //
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Trees //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                    }
                    CreateDataset("Tree", intCount, strSelectedIDs, 0, "", 0, "");
                    break;
                case 2:  // Available Inspections Grid //
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Inspections //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                    }
                    CreateDataset("Inspection", intSelectedTreeCount, strSelectedTrees, intCount, strSelectedIDs, 0, "");
                    break;
                case 3:  // Available Action Grid //
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Actions //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ActionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ActionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                        // Inspections //
                        if (strSelectedInspections == "")
                        {
                            strSelectedInspections = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                            intSelectedInspectionCount++;
                        }
                        else
                        {
                            strSelectedInspection = Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedInspections.Contains("," + strSelectedInspection))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedInspections += strSelectedInspection;
                                intSelectedInspectionCount++;
                            }
                        }
                    }
                    CreateDataset("Action", intSelectedTreeCount, strSelectedTrees, intSelectedInspectionCount, strSelectedInspections, intCount, strSelectedIDs);
                    break;
                default:
                    return;
            }
        }


        private void CreateDataset(string strType, int intTreeCount, string strSelectedTreeIDs, int intInspectionCount, string strSelectedInspectionIDs, int intActionCount, string strSelectedActionIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            Form frmMain = this.MdiParent;
            frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_tree_count = intTreeCount;
            fChildForm.i_int_selected_inspection_count = intInspectionCount;
            fChildForm.i_int_selected_action_count = intActionCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                string strSelectedIDs = "";
                switch (strDatasetType.ToUpper())
                {
                    case "TREE":
                        strSelectedIDs = strSelectedTreeIDs;
                        break;
                    case "INSPECTION":
                        strSelectedIDs = strSelectedInspectionIDs;
                        break;
                    case "ACTION":
                        strSelectedIDs = strSelectedActionIDs;
                        break;
                    default:
                        strSelectedIDs = "";
                        break;
                }
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strType, strName, this.GlobalSettings.UserID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }

        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Trees Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 2:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 3:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Trees Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 2:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 3:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region TreePage

        private void gridView1_a_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 51;
            SetMenuStatus();
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 2 || i_int_FocusedGrid == 3)
                {
                    if (view.SelectedRowsCount > 0)
                    {
                        bbiDatasetCreate.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetCreate.Enabled = false;
                    }
                    if (view.RowCount > 0)
                    {
                        bbiDatasetSelection.Enabled = true;
                        bsiDataset.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetSelection.Enabled = false;
                        bsiDataset.Enabled = false;
                    }
                    bsiDataset.Enabled = true;
                    bbiDatasetManager.Enabled = true;
                    bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void btnLoadTrees_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Trees...");
            }

            if (i_str_selected_client_ids == null) i_str_selected_client_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01220_AT_Reports_Listings_Tree_FilterTableAdapter.Fill(this.dataSet_AT.sp01220_AT_Reports_Listings_Tree_Filter, i_str_selected_client_ids);
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void pivotGridControl1_MouseUp(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            if (e.Button != MouseButtons.Right) return;
            if (pivotGridControl1.CalcHitInfo(pt).HitTest != PivotGridHitTest.Cell) return;
            // Shows the context menu.
            popupMenu1.ShowPopup(barManager1, pivotGridControl1.PointToScreen(pt));
        }

        private void pivotGridControl1_PopupMenuShowing(object sender, DevExpress.XtraPivotGrid.PopupMenuShowingEventArgs e)
        {
            PivotGridControl pivotGrid = (PivotGridControl)sender;
            epgmMenu = new ExtendedPivotGridMenu(pivotGrid);
            epgmMenu.ShowPivotGridMenu(sender, e);

        }

        private void buttonEditClientFilter_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids = "";
                i_str_selected_client_names = "";
                buttonEditClientFilter.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;
                }
            }
        }

        #endregion


        #region InspectionPage

        private void gridView2_a_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 52;
            SetMenuStatus();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 2 || i_int_FocusedGrid == 3)
                {
                    if (view.SelectedRowsCount > 0)
                    {
                        bbiDatasetCreate.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetCreate.Enabled = false;
                    }
                    if (view.RowCount > 0)
                    {
                        bbiDatasetSelection.Enabled = true;
                        bsiDataset.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetSelection.Enabled = false;
                        bsiDataset.Enabled = false;
                    }
                    bsiDataset.Enabled = true;
                    bbiDatasetManager.Enabled = true;
                    bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void btnLoadInspections_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Inspections...");
            }
            int intLastInspectionOnly = 0;
            if (ceLastInspectionOnly.Checked) intLastInspectionOnly = 1;

            DateTime? dtFromDate = Convert.ToDateTime(deInspectionFromDate.DateTime);
            if (dtFromDate <= Convert.ToDateTime("2001-01-01")) dtFromDate = null;
            DateTime? dtToDate = Convert.ToDateTime(deInspectionToDate.DateTime);
            if (dtToDate <= Convert.ToDateTime("2001-01-01")) dtToDate = null;

            if (i_str_selected_client_ids2 == null) i_str_selected_client_ids2 = "";
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter.Fill(this.dataSet_AT.sp01230_AT_Reports_Listings_Inspection_Filter, i_str_selected_client_ids2, dtFromDate, dtToDate, intLastInspectionOnly);
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void pivotGridControl2_MouseUp(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            if (e.Button != MouseButtons.Right) return;
            if (pivotGridControl2.CalcHitInfo(pt).HitTest != PivotGridHitTest.Cell) return;
            // Shows the context menu.
            popupMenu1.ShowPopup(barManager1, pivotGridControl2.PointToScreen(pt));
        }

        private void pivotGridControl2_PopupMenuShowing(object sender, DevExpress.XtraPivotGrid.PopupMenuShowingEventArgs e)
        {
            PivotGridControl pivotGrid = (PivotGridControl)sender;
            epgmMenu = new ExtendedPivotGridMenu(pivotGrid);
            epgmMenu.ShowPivotGridMenu(sender, e);

        }

        private void buttonEditClientFilter2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids2 = "";
                i_str_selected_client_names2 = "";
                buttonEditClientFilter2.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids2;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids2 = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names2 = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter2.Text = i_str_selected_client_names;
                }
            }
        }

        #endregion


        #region ActionPage

        private void gridView3_a_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 53;
            SetMenuStatus();
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1 || i_int_FocusedGrid == 2 || i_int_FocusedGrid == 3)
                {
                    if (view.SelectedRowsCount > 0)
                    {
                        bbiDatasetCreate.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetCreate.Enabled = false;
                    }
                    if (view.RowCount > 0)
                    {
                        bbiDatasetSelection.Enabled = true;
                        bsiDataset.Enabled = true;
                    }
                    else
                    {
                        bbiDatasetSelection.Enabled = false;
                        bsiDataset.Enabled = false;
                    }
                    bsiDataset.Enabled = true;
                    bbiDatasetManager.Enabled = true;
                    bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void btnLoadActions_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Actions...");
            }

            DateTime? dtFromDate = Convert.ToDateTime(deActionFromDate.DateTime);
            if (dtFromDate <= Convert.ToDateTime("1900-01-01")) dtFromDate = null;
            DateTime? dtToDate = Convert.ToDateTime(deActionToDate.DateTime);
            if (dtToDate <= Convert.ToDateTime("1900-01-01")) dtToDate = null;

            if (i_str_selected_client_ids3 == null) i_str_selected_client_ids3 = "";
            GridView view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            sp01232_AT_Reports_Listings_Action_FilterTableAdapter.Fill(this.dataSet_AT.sp01232_AT_Reports_Listings_Action_Filter, i_str_selected_client_ids3, dtFromDate, dtToDate);
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void pivotGridControl3_MouseUp(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            if (e.Button != MouseButtons.Right) return;
            if (pivotGridControl3.CalcHitInfo(pt).HitTest != PivotGridHitTest.Cell) return;
            // Shows the context menu.
            popupMenu1.ShowPopup(barManager1, pivotGridControl3.PointToScreen(pt));
        }

        private void pivotGridControl3_PopupMenuShowing(object sender, DevExpress.XtraPivotGrid.PopupMenuShowingEventArgs e)
        {
            PivotGridControl pivotGrid = (PivotGridControl)sender;
            epgmMenu = new ExtendedPivotGridMenu(pivotGrid);
            epgmMenu.ShowPivotGridMenu(sender, e);

        }

        private void buttonEditClientFilter3_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids3 = "";
                i_str_selected_client_names3 = "";
                buttonEditClientFilter3.Text = "";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids3;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids3 = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names3 = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter3.Text = i_str_selected_client_names;
                }
            }
        }
        
        #endregion


        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Text)
            {
                    case "Trees":
                        xtraTabControl2.SelectedTabPage = xtraTabPage4;
                        i_int_AnalysisType = 1;
                        /*// Pass structure of dataset to Pivot Grid (it won't contain any data yet - loaded through btnAnalyze_Click event) //
                        SqlConnection conn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp01234_AT_Data_Analysis_Trees", conn);
                        cmd.Parameters.Add(new SqlParameter("@strTreeIDs", i_str_selected_trees));
                        cmd.CommandType = CommandType.StoredProcedure;
                        sdaDataAdpter = new SqlDataAdapter(cmd);
                        dsDataSet = new DataSet("NewDataSet");
                        dsDataSet.Tables.Add("Table");
                        sdaDataAdpter.Fill(dsDataSet, "Table");
                    
                        pivotGridControl1.DataSource = dsDataSet.Tables["Table"];
                        pivotGridControl1.RetrieveFields();

                        pivotGridControl1.Fields.Add("TreeCount", PivotArea.DataArea);
                        //pivotGridControl1.Fields.Add("SpeciesName", PivotArea.DataArea);

                        fieldCompanyName.Area = fieldProductName.Area = PivotArea.RowArea;
                        */
                        break;
                    case "Inspections":
                        xtraTabControl2.SelectedTabPage = xtraTabPage5;
                        i_int_AnalysisType = 2;
                        break;
                    case "Actions":
                        xtraTabControl2.SelectedTabPage = xtraTabPage6;
                        i_int_AnalysisType = 3;
                        break;
                    case "Work Orders":
                        i_int_AnalysisType = 4;
                        break;
                    case "Incidents":
                        i_int_AnalysisType = 5;
                        break;
                    case "Import History":
                        i_int_AnalysisType = 6;
                        break;
                    default:
                        i_int_AnalysisType = 0;
                        break;
            }
        }

        private void xtraTabControl2_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            LinkChartToRibbonBar();
        }

        private void LinkChartToRibbonBar()
        {
            ChartControl chart = null;
            PivotGridControl pivotGrid = null;
            switch (xtraTabControl2.SelectedTabPage.Text)
            {
                case "Tree Analysis":
                    chart = chartControl1;
                    pivotGrid = pivotGridControl1;
                    break;
                case "Inspection Analysis":
                    chart = chartControl2;
                    pivotGrid = pivotGridControl2;
                    break;
                case "Action Analysis":
                    chart = chartControl3;
                    pivotGrid = pivotGridControl3;
                    break;
                case "Work Order Analysis":
                    break;
                case "Incident Analysis":
                    break;
                case "Import History Analysis":
                    break;
                default:
                    break;
            }
            // Link Chart to Chart Gallery on Ribbon Bar //
            try
            {
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.Set_Chart(chart, pivotGrid);
                MainForm.InitChartPaletteGallery(MainForm.gddChartPalette);  // Analysis Drop Down list of Colour schemes for charting //
                MainForm.InitChartAppearanceGallery(MainForm.rgbiChartAppearance, chart.PaletteName);
                MainForm.ViewType = DevExpress.XtraCharts.Native.SeriesViewFactory.GetViewType(chart.Series[0].View);
            }
            catch { }
        }

        private void btnAnalyze_Click(object sender, EventArgs e)
        {
            i_str_selected_trees = "";
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            switch (i_int_AnalysisType)
            {
                case 1:
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    i_str_selected_trees = "";
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to analyse before proceeding!", "Analyse Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        i_str_selected_trees += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TreeID"])) + ',';
                    }
                    sp01234_AT_Data_Analysis_TreesTableAdapter.Fill(this.dataSet_AT_Reports.sp01234_AT_Data_Analysis_Trees, i_str_selected_trees);
                    // Populate Pivot Grid //
                    /*SqlConnection conn = new SqlConnection(strConnectionString);
                     SqlCommand cmd = null;
                     cmd = new SqlCommand("sp01234_AT_Data_Analysis_Trees", conn);
                     cmd.Parameters.Add(new SqlParameter("@strTreeIDs", i_str_selected_trees));
                     cmd.CommandType = CommandType.StoredProcedure;
                     sdaDataAdpter = new SqlDataAdapter(cmd);
                     dsDataSet = new DataSet("NewDataSet");
                     dsDataSet.Tables.Add("Table");
                     sdaDataAdpter.Fill(dsDataSet, "Table");
                     pivotGridControl1.DataSource = dsDataSet.Tables["Table"];
                     //pivotGridControl1.RetrieveFields();*/

                    if (chartControl1.DataSource == null) LinkChartsToPivotGrids("1");
                    break;
                case 2:
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    i_str_selected_inspections = "";
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to analyse before proceeding!", "Analyse Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        i_str_selected_inspections += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["InspectionID"])) + ',';
                    }
                    sp01235_AT_Data_Analysis_InspectionsTableAdapter.Fill(this.dataSet_AT_Reports.sp01235_AT_Data_Analysis_Inspections, i_str_selected_inspections);

                    if (chartControl2.DataSource == null) LinkChartsToPivotGrids("2");
                    break;
                case 3:
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    i_str_selected_actions = "";
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to analyse before proceeding!", "Analyse Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        i_str_selected_actions += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ActionID"])) + ',';
                    }
                    sp01236_AT_Data_Analysis_ActionsTableAdapter.Fill(this.dataSet_AT_Reports.sp01236_AT_Data_Analysis_Actions, i_str_selected_actions);

                    if (chartControl3.DataSource == null) LinkChartsToPivotGrids("3");
                    break;
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Copy to Clipboard popup menu item - fired from pivotGridControl's MouseUp event //
            PivotGridControl PivotGrid = null;
            switch (xtraTabControl2.SelectedTabPage.Text)
            {
                case "Tree Analysis":
                    PivotGrid = pivotGridControl1;
                    break;
                case "Inspection Analysis":
                    PivotGrid = pivotGridControl2;
                    break;
                case "Action Analysis":
                    PivotGrid = pivotGridControl3;
                    break;
                default:
                    return;
            }          
            PivotGrid.Cells.CopySelectionToClipboard();
        }

        private void frm_AT_Analysis_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                // Update Main Forms Ribbon (Chart Styling) - Remove link from current chart //
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.Kill_Chart_Styling();
            }
            catch (Exception)
            {
            }
        }

        public override void OnLayoutRotateEvent(object sender, EventArgs e)
        {
            switch (xtraTabControl2.SelectedTabPage.Text)
            {
                case "Tree Analysis":
                    splitContainerControl1.Horizontal = !splitContainerControl1.Horizontal;
                    break;
                case "Inspection Analysis":
                    splitContainerControl2.Horizontal = !splitContainerControl2.Horizontal;
                    break;
                case "Action Analysis":
                    splitContainerControl3.Horizontal = !splitContainerControl3.Horizontal;
                    break;
                case "Work Order Analysis":
                    break;
                case "Incident Analysis":
                    break;
                case "Import History Analysis":
                    break;
                default:
                    break;
            }
        }

        public override void OnLayoutFlipEvent(object sender, EventArgs e)
        {
            switch (xtraTabControl2.SelectedTabPage.Text)
            {
                case "Tree Analysis":
                    {
                        splitContainerControl1.Visible = false;
                        Control parent1 = (Control)pivotGridControl1.Parent;
                        Control parent2 = (Control)chartControl1.Parent;
                        Control child1 = (Control)pivotGridControl1;
                        Control child2 = (Control)chartControl1;
                        parent1.Controls.Add(child2);
                        parent2.Controls.Add(child1);
                        splitContainerControl1.Visible = true;
                    }
                    break;
                case "Inspection Analysis":
                    {
                        splitContainerControl2.Visible = false;
                        Control parent1 = (Control)pivotGridControl2.Parent;
                        Control parent2 = (Control)chartControl2.Parent;
                        Control child1 = (Control)pivotGridControl2;
                        Control child2 = (Control)chartControl2;
                        parent1.Controls.Add(child2);
                        parent2.Controls.Add(child1);
                        splitContainerControl2.Visible = true;
                    }
                    break;
                case "Action Analysis":
                    {
                        splitContainerControl3.Visible = false;
                        Control parent1 = (Control)pivotGridControl3.Parent;
                        Control parent2 = (Control)chartControl3.Parent;
                        Control child1 = (Control)pivotGridControl3;
                        Control child2 = (Control)chartControl3;
                        parent1.Controls.Add(child2);
                        parent2.Controls.Add(child1);
                        splitContainerControl3.Visible = true;
                    }
                    break;
                case "Work Order Analysis":
                    break;
                case "Incident Analysis":
                    break;
                case "Import History Analysis":
                    break;
                default:
                    break;
            }
        }

        private void chartControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                pmChart.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void chartControl2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                pmChart.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void chartControl3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                pmChart.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void bbiChartWizard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.RunChartWizard();
            }
            catch { }
        }

        private void bbiRotateAxis_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.RotateChartAxis();
            }
            catch { }
        }



    


    }
}

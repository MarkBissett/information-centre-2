using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // Used by File.Delete and FileSystemWatcher //
using System.Data.SqlClient;  // Used by Generate Map process //
using System.Reflection;

using DevExpress.LookAndFeel;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using DevExpress.XtraEditors.Repository;  // Required by Hyperlink Repository Items //
using DevExpress.XtraPrinting;

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_WorkOrder_Print : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        int intSelectedWorkOrderID = 0;
        string strSavePath = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;

        WaitDialogForm loadingForm;
        private int i_int_SelectedLayoutID = 0;
        /// <summary>
        /// 
        /// </summary>
        public string i_str_SavedDirectoryName = "";
        private string i_str_SelectedFilename = "";
        private int i_int_FocusedGrid = 1;

        rpt_AT_Report_Layout_WorkOrder_Blank rptReport;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        #endregion
       
        public frm_AT_WorkOrder_Print()
        {
            InitializeComponent();
        }

        private void frm_AT_WorkOrder_Print_Load(object sender, EventArgs e)
        {
            this.FormID = 92003;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            printPreviewBarItem44.Enabled = false;  // Switch off Open button //

            emptyEditor = new RepositoryItem();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            ProcessPermissionsForForm();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp01205_AT_Report_Workorder_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01205_AT_Report_Workorder_ListTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01205_AT_Report_Workorder_List, this.GlobalSettings.ViewedStartDate, this.GlobalSettings.ViewedEndDate);
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            FromDate.DateTime = this.GlobalSettings.ViewedStartDate.Date;
            ToDate.DateTime = this.GlobalSettings.ViewedEndDate.Date;

            sp01206_AT_Report_Available_LayoutsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, 1, "AmenityTreesWorkOrderLayoutLocation");
            sp01206_AT_Report_Available_LayoutsGridControl.ForceInitialize();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            // Get default layout file directory from System_Settings table //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            i_str_SavedDirectoryName = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderLayoutLocation")) + "\\";

            // Get Default WorkOrder Layout if one is present //
            string strDefaultWorkOrderLayout = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderDefaultLayoutID").ToString();
            int intDefaultWorkOrderLayout = (string.IsNullOrEmpty(strDefaultWorkOrderLayout) ? 0 : Convert.ToInt32(strDefaultWorkOrderLayout));
            if (intDefaultWorkOrderLayout > 0)
            {
                GridView view = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["ReportLayoutID"], intDefaultWorkOrderLayout);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    i_int_SelectedLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
                    i_str_SelectedFilename = Convert.ToString(i_int_SelectedLayoutID) + ".repx";
                    popupContainerEdit1.EditValue = Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
                }
            }

            // Get default value for ShowMap checkbox //
            string strDefaultShowMap = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderMapTicked").ToString();
            int intDefaultShowMap = (string.IsNullOrEmpty(strDefaultShowMap) ? 0 : Convert.ToInt32(strDefaultShowMap));
            checkEdit_ShowMap.Checked = (intDefaultShowMap == 1 ? true : false);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            btnLayoutAddNew.Enabled = iBool_AllowEdit;

            strSavePath = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderMapLocation")) + "\\";
            if (strSavePath == null) strSavePath = "C:\\";

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }

        public override void PostOpen(object objParameter)
        {
            dockPanel1.Width = 386;
            
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
        }

        private void frm_AT_WorkOrder_Print_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_AT_WorkOrder_Print_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (string.IsNullOrEmpty(printControl1.PrintingSystem.Document.Name)) return;
            if (printControl1.PrintingSystem.Document.Name != "Document")
            {
                printControl1.PrintingSystem.ClearContent(); // This should free up any map jpg if open //
                rptReport.Dispose();
                rptReport = null;
                GC.GetTotalMemory(true);
            }
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Edit Report Layout //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] {"iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            if (i_int_FocusedGrid == 2)
            {

                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        public void LoadWorkOrdersList()
        {
            GridView view = (GridView)sp01205_AT_Report_Workorder_ListGridControl.MainView;

            view.BeginUpdate();
            sp01205_AT_Report_Workorder_ListTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01205_AT_Report_Workorder_List, Convert.ToDateTime(FromDate.Text), Convert.ToDateTime(ToDate.Text));
            view.EndUpdate();
        }

        private void btnRefreshWorkOrderList_Click(object sender, EventArgs e)
        {
            LoadWorkOrdersList();
        }

        private void bbiWorkOrderMapping_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.FocusedRowHandle == GridControl.InvalidRowHandle || intSelectedWorkOrderID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to open Work Order Map Generation, no Work Order selected for viewing.\n\nSelect a Work Order from the list then try again.", "Work Order Map Generation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            OpenMapping();
        }

        private void OpenMapping()
        {
            int intWorkSpaceID = 0;
            int intWorkspaceOwner = 0;
            string strWorkspaceName = "";
            Mapping_Functions MapFunctions = new Mapping_Functions();
            MapFunctions.Get_Mapping_Workspace(this.GlobalSettings, ref intWorkSpaceID, ref intWorkspaceOwner, ref strWorkspaceName);  // Pass by ref so values are updated in called function //
            if (intWorkSpaceID == 0) return;
            frm_AT_Mapping_Tree_Picker fm_AT_Mapping_Tree_Picker = new frm_AT_Mapping_Tree_Picker(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

            WaitDialogForm loading = new WaitDialogForm("Preparing Mapping, Please Wait...", "WoodPlan Mapping");
            loading.Show();
            
            // Get Highlight Colour //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intHighlightColour = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderMapTreeHighlightColour"));

            // Get Show Other Trees Status //
            string strShowOtherTrees = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderPrintShowDetailsOfOtherTrees").ToString();

            // Get list of parent districts for the work order to pass to mapping //
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp01458_AT_WorkOrder_Print_Mapping_ClientIDs", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intSelectedWorkOrderID));
            SqlDataAdapter sdaDistricts = new SqlDataAdapter(cmd);
            DataSet dsDistricts = new DataSet("NewDataSet");
            sdaDistricts.Fill(dsDistricts, "Table");
            string strDistrictIDsToExpandOnMap = "";
            foreach (DataRow dr in dsDistricts.Tables[0].Rows)
            {
                strDistrictIDsToExpandOnMap += dr["ClientID"].ToString() + ",";
            }

            // Get list of work order trees to highlight on the map //
            SQlConn = new SqlConnection(strConnectionString);
            cmd = null;
            cmd = new SqlCommand("sp01309_AT_WorkOrder_Print_Mapping_TreeIDs_Highlight", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intSelectedWorkOrderID));
            SqlDataAdapter sdaTrees = new SqlDataAdapter(cmd);
            DataSet dsTrees = new DataSet("NewDataSet");
            sdaTrees.Fill(dsTrees, "Table");
            string strTreeIDsToHighlight = "";
            foreach (DataRow dr in dsTrees.Tables[0].Rows)
            {
                strTreeIDsToHighlight += dr["TreeID"].ToString() + ",";
            }

            // Get list of trees to be shown on the map //
            string strTreeIDsToShow = "";
            if (strShowOtherTrees.ToLower() != "show")
            {
                strTreeIDsToShow = strTreeIDsToHighlight;  // Other trees not shown so just set to the same trees to be highlighted //
            }
            else
            {
                SQlConn = new SqlConnection(strConnectionString);
                cmd = null;
                cmd = new SqlCommand("sp01310_AT_WorkOrder_Print_Mapping_TreeIDs_Shown", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TreeIDs", strTreeIDsToHighlight));
                SqlDataAdapter sdaTreesShow = new SqlDataAdapter(cmd);
                DataSet dsTreesShow = new DataSet("NewDataSet");
                sdaTreesShow.Fill(dsTreesShow, "Table");
                foreach (DataRow dr in dsTreesShow.Tables[0].Rows)
                {
                    strTreeIDsToShow += dr["TreeID"].ToString() + ",";
                }
            }


            // Check if Mapping form (intFormID = 2004) is already open. If yes, activate it and reload contents otherwise open it //
            try
            {
                frmMain2 frmMDI = (frmMain2)this.MdiParent;
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 2004)
                    {
                        frmChild.Activate();
                        frm_AT_Mapping_Tree_Picker frmTreePicker = (frm_AT_Mapping_Tree_Picker)frmChild;
                        frmTreePicker.i_str_selected_client_ids = strDistrictIDsToExpandOnMap;
                        frmTreePicker.strVisibleIDs = strTreeIDsToShow;
                        frmTreePicker.strHighlightedIDs = strTreeIDsToHighlight;
                        frmTreePicker.intInitialHighlightColour = intHighlightColour;
                        frmTreePicker.i_str_selected_workorders = intSelectedWorkOrderID.ToString() + ",";
                        //frmTreePicker.DeselectAllHighlightedMapObjects();
                        frmTreePicker.Pre_Select_Clients();
                        frmTreePicker.Load_Map_Objects_Grid();
                        frmTreePicker.Load_Layer_Manager();
                        frmTreePicker.PostOpen(new object[] { null });  // Trigger this event so the Locus effect fires //
                        loading.Close();
                        return;
                    }
                }
            }
            catch (Exception)
            {
            }
            loading.Close();
            // If we are here then the form was not already open, so open it //
            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();
            fm_AT_Mapping_Tree_Picker.fProgress = fProgress;
            
            fm_AT_Mapping_Tree_Picker.MdiParent = this.MdiParent;
            fm_AT_Mapping_Tree_Picker.GlobalSettings = this.GlobalSettings;

            // Other Data Configuration Parameters for Map Below... //
            fm_AT_Mapping_Tree_Picker.i_str_selected_client_ids = strDistrictIDsToExpandOnMap;
            fm_AT_Mapping_Tree_Picker.strVisibleIDs = strTreeIDsToShow;
            fm_AT_Mapping_Tree_Picker.strHighlightedIDs = strTreeIDsToHighlight;
            fm_AT_Mapping_Tree_Picker.intInitialHighlightColour = intHighlightColour;
            fm_AT_Mapping_Tree_Picker.i_str_selected_workorders = intSelectedWorkOrderID.ToString() + ",";

            fm_AT_Mapping_Tree_Picker.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fm_AT_Mapping_Tree_Picker, new object[] { null });
        }


        private void btn_view_Click(object sender, EventArgs e)
        {
            if (intSelectedWorkOrderID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, no Work Order selected for viewing.\n\nSelect a Work Order to view from the Work Order list then try again.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (popupContainerEdit1.Text == "No Work Order Layout Selected")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, no layout specified.\n\nSelect a layout to use from the Layout list then try again.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            loadingForm = new WaitDialogForm("Loading Report Builder...", "Work Order Printing");
            loadingForm.Show();

            if (printControl1.PrintingSystem != null)
            {
                if (printControl1.PrintingSystem.Document.Name != null)
                {
                    if (printControl1.PrintingSystem.Document.Name != "Document")
                    {
                        printControl1.PrintingSystem.ClearContent(); // This should free up any map jpg if open so it can be deleted first then regenerated should the same work order by reloaded. Similar code in FormClosed event. //
                        rptReport.Dispose();
                        rptReport = null;
                        GC.GetTotalMemory(true);
                        Application.DoEvents();  // Allow Form time to repaint itself //
                    }
                }
            }

            if (checkEdit_ShowMap.Checked)
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);

                // Get Count of linked maps //
                DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter GetCount = new DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter();
                GetCount.ChangeConnectionString(strConnectionString);
                int intLinkedMapCount = Convert.ToInt32(GetCount.sp01434_AT_WorkOrder_Print_Linked_Map_Count(intSelectedWorkOrderID));

                if (intLinkedMapCount <= 0)
                {
                    switch (DevExpress.XtraEditors.XtraMessageBox.Show("Unable to generate Work Order with map(s)!\n\nNo Maps have been created for this Work Order.\n\nWould you like to open the Mapping screen to create one or more maps?\n\nClick Yes to Open a Map.\n\nClick No to Proceed with Work Order Generation WITHOUT Maps.\n\nClick Cancel to Abort theWork Order Generation.", "Generate Work Order", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                    {
                        case DialogResult.Cancel:
                            loadingForm.Close();
                            return;
                        case DialogResult.No:
                            break;
                        case DialogResult.Yes:
                            loadingForm.Close();
                            OpenMapping();
                            return;
                    }
                }
            }

            loadingForm.Caption = "Loading Report...";
            rptReport = new rpt_AT_Report_Layout_WorkOrder_Blank(this.GlobalSettings, intSelectedWorkOrderID, strSavePath, checkEdit_LoadLinkedPictures.Checked, checkEdit_ShowMap.Checked);
            rptReport.Name = "Work Order";  // Set Document Root Node to meaningful text //
            try
            {
                rptReport.LoadLayout(i_str_SavedDirectoryName + i_str_SelectedFilename);
            }
            catch (Exception Ex)
            {
                loadingForm.Close();
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            AdjustEventHandlers(rptReport);  // Tie in custom Sorting //
            
            printControl1.PrintingSystem = rptReport.PrintingSystem;
            BaseObjects.Report_Watermark rpt_watermark = new BaseObjects.Report_Watermark();
            rpt_watermark.Create_Report_Watermark(rptReport, strConnectionString);
            rptReport.CreateDocument();
            rpt_watermark = null;
            loadingForm.Close();
        }

        private void btnSaveAsPDF_Click(object sender, EventArgs e)
        {
            if (intSelectedWorkOrderID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Please select the Work Order to be generated as a PDF file then click the View Work Order button to load the work order report before proceeding.", "Save Work Order as PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (printControl1.PrintingSystem.Document.Name == "Document")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Please click the View Work Order button to load the work order report before proceeding.", "Save Work Order as PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            this.splashScreenManager = splashScreenManager1;
            this.splashScreenManager.ShowWaitForm();

            // Get Save Folder //
            string strPDFFolder = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strPDFFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Linked Document Folder (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Save Work Order as PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strPDFFolder.EndsWith("\\")) strPDFFolder += "\\";

            /*
            // Load Report into memory //
            string strReportFileName = "Permission_Document_Layout.repx";
            rpt_UT_Report_Layout_Permission_Form rptReport = new rpt_UT_Report_Layout_Permission_Form(this.GlobalSettings, _intPermissionDocumentID, strSignaturePath, strPicturePath, strMapPath);
            try
            {
                rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Create Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            */


            // Set security options of report so when it is exported, it can't be edited and is password protected //            
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
            rptReport.ExportOptions.Pdf.Compressed = true;
            rptReport.ExportOptions.Pdf.ImageQuality = PdfJpegImageQuality.Low;
            //rptReport.ExportOptions.Pdf.NeverEmbeddedFonts = "";

            // Save physical PDF file //
            string strPDFName = "WorkOrder_" + intSelectedWorkOrderID.ToString().PadLeft(7, '0') + "__" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".pdf"; ;
            string strFolderAndPDFName = strPDFFolder + strPDFName;  // Put path onto start of filename //
            try
            {
                rptReport.ExportToPdf(strFolderAndPDFName);
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                //Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Work Order PDF File - an error occurred while generating the PDF File [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Create Work Order PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Delete any prior version from file system //
            /*try
            {
                if (!string.IsNullOrEmpty(_strExistingPDFFileName))
                {
                    string OldFileName = strPDFFolder + _strExistingPDFFileName;
                    System.IO.File.Delete(OldFileName);
                }
            }
            catch (Exception Ex)
            {
            }*/

            // Update Permission Record with name of PDF file //
            DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter UpdateWorkOrderRecord = new DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter();
            UpdateWorkOrderRecord.ChangeConnectionString(strConnectionString);
            try
            {
                UpdateWorkOrderRecord.sp01453_AT_Work_Order_Store_PDF_File_Name(intSelectedWorkOrderID, strPDFName);

                // Update Calling screen with PDF Filename //
                if (this.ParentForm != null)
                {
                    foreach (Form frmChild in this.ParentForm.MdiChildren)
                    {
                        if (frmChild.Name == "frm_AT_WorkOrder_Edit")
                        {
                            var fParentForm = (frm_AT_WorkOrder_Edit)frmChild;
                            fParentForm.UpdateFormWithPDFFileName(intSelectedWorkOrderID, strPDFName);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Work Order PDF File - an error occurred while updating the Work Order Record [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Save Work Order PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            this.Close();
        }


        public int CreateCSVFile(DataTable dt, string strFilePath)
        {
            // Create the CSV file to which data will be exported //
            try
            {
                StreamWriter sw = new StreamWriter(strFilePath, false);
                // First write the headers //
                int iColCount = dt.Columns.Count;
                for (int i = 0; i < iColCount; i++)
                {
                    sw.Write(dt.Columns[i]);
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
                // Now write all the rows //
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < iColCount; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]))
                        {
                            sw.Write(dr[i].ToString());
                        }
                        if (i < iColCount - 1)
                        {
                            sw.Write(",");
                        }
                    }
                    sw.Write(sw.NewLine);
                }
                sw.Close();

            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to generate report - a problem occurred while creating the map [unable to write to CSV file], the CSV file may be open from another process - contact Technical Support.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return - 1;
            }
            return 1;
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            DeleteRecord();
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
            string strReportLayoutName = Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
            string strReportFileName = Convert.ToString(intReportLayoutID) + ".repx";
            if (intReportLayoutID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout, no layout selected for deletion!\n\nSelect a layout before proceeding.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to delete the selected layout: " + strReportLayoutName + "?\n\nIf you proceed, the layout will no longer be available for use!", "Delete Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                loadingForm = new WaitDialogForm("Loading Report Builder...", "Work Order Printing");
                loadingForm.Show();

                // Delete the record then the physical file layout //
                DataSet_ATTableAdapters.QueriesTableAdapter DeleteLayout = new DataSet_ATTableAdapters.QueriesTableAdapter();
                DeleteLayout.ChangeConnectionString(strConnectionString);
                DeleteLayout.sp01213_AT_Report_Layouts_Delete_Layout_Record(intReportLayoutID);


                try
                {
                    File.Delete(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout, it may no longer exist or you may not have the necessary permissions to delete it - contact Technical Support.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                // Reload list of available layout and pre-select new one //
                sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, 1, "AmenityTreesWorkOrderLayoutLocation");
                popupContainerEdit1.Text = "No Work Order Layout Selected";
                i_int_SelectedLayoutID = 0;
                loadingForm.Close();
            }

        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            EditRecord();
        }

        private void EditRecord()
        {
            GridView View = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(View.GetFocusedRowCellValue("ReportLayoutID"));
            string strReportLayoutName = Convert.ToString(View.GetFocusedRowCellValue("ReportLayoutName"));
            string strReportFileName = Convert.ToString(intReportLayoutID) + ".repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Work Order Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                rptReport = new rpt_AT_Report_Layout_WorkOrder_Blank(this.GlobalSettings, intSelectedWorkOrderID, strSavePath, checkEdit_LoadLinkedPictures.Checked, checkEdit_ShowMap.Checked);

                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                    //strText = strReportFileName;
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                loadingForm = new WaitDialogForm("Loading Report Builder...", "Work Order Printing");
                loadingForm.Show();

                // Open the report in the Report Builder //
                // Create a design form and get its panel.
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);                

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                loadingForm.Close();
                form.ShowDialog();
                panel.CloseReport();

            }

        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            if (!iBool_AllowAdd) return;
            AddLayout();
        }

        void form_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        private void checkEdit_ShowMap_CheckedChanged(object sender, EventArgs e)
        {
            //glueMapScale.Enabled = checkEdit_ShowMap.Checked;
        }

        public void LoadWorkOrder_ExternalCall(string strParameterString)
        {
            string[] strArray;
            char[] delimiters = new char[] { ';' };
            strArray = strParameterString.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length < 1) return;
            //if (strArray[0] != "WorkOrder") return;

            int intWorkOrderID = 0;
            if (!BaseObjects.QuickConversion.ConvertToInt(strArray[0], ref intWorkOrderID)) return;  // WorkOrderId set by ref if it succeeded //
            if (intWorkOrderID <= 0) return;
            intSelectedWorkOrderID = intWorkOrderID;
            GridView view = gridView1;
            int intRowHandle = view.LocateByValue(0, view.Columns["WorkOrderID"], intWorkOrderID);
            if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) return;
            view.MakeRowVisible(intRowHandle, false);
            view.FocusedRowHandle = intRowHandle;
            
            if (popupContainerEdit1.Text == "No Work Order Layout Selected") return;
            btn_view_Click(null, null);
        }


        #region Grid View Generic Events

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    intSelectedWorkOrderID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderID"));
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            intSelectedWorkOrderID = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "WorkOrderID"));

        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedActionCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strWorkOrderIDs = view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderID").ToString() + ",";
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strRecordIDs = "";
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedActionCount":
                    strRecordIDs = GetSetting.sp01345_AT_Work_Order_Manager_Get_Linked_Action_IDs(strWorkOrderIDs).ToString();
                    break;
                default:
                    break;
            }

            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "workorder");
        }

        #endregion


        #region GridView2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }
        
        #endregion


        #region GridView3

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }
        
        #endregion


        #region PopupContainerLayout
 
        private void btnLayoutOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void btnLayoutAddNew_Click(object sender, EventArgs e)
        {
            if (!iBool_AllowAdd) return;
            AddLayout();
        }

        private void AddLayout()
        {
            Form frmMain = this.MdiParent;
            frm_AT_WorkOrder_Add_Layout fChildForm = new frm_AT_WorkOrder_Add_Layout();
            frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            int intNewLayoutID = 0;
            string strNewLayoutName = "";
            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                return;
            }
            intNewLayoutID = fChildForm.intReturnedValue;
            strNewLayoutName = fChildForm.strReturnedLayoutName;

            // Save the new Layout and get back it's ID for the physical filename //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intNewID = 0;
            intNewID = Convert.ToInt32(GetSetting.sp01212_AT_Report_Add_Layouts_Create_Layout_Record(1, 1, strNewLayoutName, this.GlobalSettings.UserID, 0));
            if (intNewID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create new layout, a problem occurred - contact Technical Support.", "Create Work Order Layout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // Load the report - Create a design form and get its panel.
            XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
            //XRDesignFormEx form = new XRDesignFormEx();

            XRDesignPanel panel = form.DesignPanel;
            panel.SetCommandVisibility(ReportCommand.NewReport, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.OpenFile, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.SaveFileAs, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);

            rptReport = new rpt_AT_Report_Layout_WorkOrder_Blank(this.GlobalSettings, intSelectedWorkOrderID, strSavePath, checkEdit_LoadLinkedPictures.Checked, checkEdit_ShowMap.Checked);
            rptReport.Name = "Work Order";  // Set Document Root Node to meaningful text //
            if (intNewLayoutID > 0)
            {
                i_str_SelectedFilename = Convert.ToInt32(intNewLayoutID) + ".repx";
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + i_str_SelectedFilename);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            // Set filename to correct one so that when save fires from the report designer, the correct file is created/updated //
            i_str_SelectedFilename = Convert.ToInt32(intNewID) + ".repx";

            // Add a new command handler to the Report Designer which saves the report in a custom way.
            panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, i_str_SelectedFilename));

            // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
            panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

            loadingForm = new WaitDialogForm("Loading Report Builder...", "Work Order Printing");
            loadingForm.Show();

            // Load the report into the design form and show the form.
            panel.OpenReport(rptReport);
            form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
            form.WindowState = FormWindowState.Maximized;
            loadingForm.Close();
            form.ShowDialog();
            panel.CloseReport();

            // Reload list of available layout and pre-select new one //
            sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, 1, "AmenityTreesWorkOrderLayoutLocation");
            GridView view = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.MainView;
            Int32 intFoundRow = view.LocateByValue(0, view.Columns["ReportLayoutID"], intNewID);
            if (intFoundRow > -1)
            {
                view.FocusedRowHandle = intFoundRow;
            }
            popupContainerEdit1.Text = PopupContainerEdit1_Get_Layout_Selected();  // Set Selected Layout Text //
        }

        private void popupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Layout_Selected();
        }

        private string PopupContainerEdit1_Get_Layout_Selected()
        {
            int[] intRowHandles;
            int intCount = 0;

            GridView view = (GridView)sp01206_AT_Report_Available_LayoutsGridControl.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                i_int_SelectedLayoutID = 0;
                return  "No Work Order Layout Selected";
            }
            else
            {
                i_int_SelectedLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
                i_str_SelectedFilename = Convert.ToString(i_int_SelectedLayoutID) + ".repx";
                return Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
            }

        }

        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void sp01206_AT_Report_Available_LayoutsGridControl_DoubleClick(object sender, EventArgs e)
        {
            if (!iBool_AllowEdit) return;
            EditRecord();
        }

        #endregion


        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);

            }
        }
       
        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }            
         }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Work Order...", "Work Order Printing");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        /*
                public class SaveCommandHandler : ICommandHandler
                {
                    XRDesignPanel panel;

                    public string strFullPath = "";

                    public string strFileName = "";

                    public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
                    {
                        this.panel = panel;
                        this.strFullPath = strFullPath;
                        this.strFileName = strFileName;
                    }

                    public virtual void HandleCommand(ReportCommand command, object[] args, ref bool handled)
                    {
                        if (!CanHandleCommand(command)) return;
                        Save();  // Save report //
                        handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
                    }

                    public virtual bool CanHandleCommand(ReportCommand command)
                    {
                        // This handler is used for SaveFile, SaveFileAs and Closing commands.
                        return command == ReportCommand.SaveFile ||
                            command == ReportCommand.SaveFileAs ||
                            command == ReportCommand.Closing;
                    }

                    void Save()
                    {
                        // Custom Saving Logic //
                        Boolean blSaved = false;
                        panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                        // Update existing file layout //
                        panel.Report.DataSource = null;
                        panel.Report.DataMember = null;
                        panel.Report.DataAdapter = null;
                        try
                        {
                            panel.Report.SaveLayout(strFullPath + strFileName);
                            blSaved = true;
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            Console.WriteLine(ex.Message);
                            blSaved = false;
                        }
                        if (blSaved)
                        {
                            panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                        }
                    }
                }
        */
        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }

 




    }
}


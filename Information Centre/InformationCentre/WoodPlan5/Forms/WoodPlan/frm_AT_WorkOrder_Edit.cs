using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Skins;
using DevExpress.XtraEditors.Repository;  // Required by emptyEditor //

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_AT_WorkOrder_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        ArrayList ArrayListFilteredPicklists;  // Holds all the picklists bound to sp01372 - used for iterating round to bind and unbind generic Enter, Leave and ButtonClick events //
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        private DataSet_Selection DS_Selection1;
        private string strDefaultPath = "";

        #endregion

        public frm_AT_WorkOrder_Edit()
        {
            InitializeComponent();
        }

        private void frm_AT_WorkOrder_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 20131;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            ArrayListFilteredPicklists = new ArrayList();  // Note: Each GridLookUpEdit within the array should have it's tag value set to it's value in the SQL PicklistHeader Table //
            ArrayListFilteredPicklists.Add(UserPicklist1GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPicklist2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPicklist3GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intPriorityIDGridLookUpEdit);
            ArrayListFilteredPicklists.Add(StatusGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intIssuedByBodyIDGridLookUpEdit);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp00232_Company_Header_Drop_Down_List_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp01403_AT_Work_Order_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01403_AT_Work_Order_TypesTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01403_AT_Work_Order_Types);

            sp00190_Contractor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00190_Contractor_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00190_Contractor_List_With_Blank);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00226_Staff_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00226_Staff_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);

            sp01362_AT_Ownership_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01362_AT_Ownership_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01362_AT_Ownership_List_With_Blank);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            emptyEditor = new RepositoryItem();

            sp01402_AT_Work_Order_Actions_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "intActionID");

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridView1, strConnectionString);  // Actions List //

            // Populate Main Dataset //
            sp01399_AT_Work_Order_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    int intDefaultContractor = 0;
                    int intDefaultCompanyHeader = 0;
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    try
                    {
                        intDefaultContractor = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultContractor"));
                    }
                    catch (Exception) {}
                    try
                    {
                        intDefaultCompanyHeader = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultCompanyHeader"));
                    }
                    catch (Exception) {}
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["dtIssueDate"] = DateTime.Today;
                        drNewRow["intRecordType"] = 0;  // 0: Work Order, 1: Work Tender, 2: Converted Work Tender //
                        drNewRow["intContractorID"] = intDefaultContractor;
                        drNewRow["intCompanyHeaderID"] = intDefaultCompanyHeader;
                        this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.Rows.Add(drNewRow);
                        this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp01399_AT_Work_Order_EditTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!strDefaultPath.EndsWith("\\")) strDefaultPath += "\\";

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
            Attach_Enter_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach Enter Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //
            Attach_ButtonClick_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach ButtonClick Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        DataTable data;  // Used for holding filtered dataset for picklists //
        private void Attach_Enter_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter += new EventHandler(Filter_GridView);
            }
        }

        private void Detach_Enter_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter -= new EventHandler(Filter_GridView);
            }
        }

        private void Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            int intHeaderID = Convert.ToInt32(glue.Tag);
            data = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks;
            DataView newView = new DataView(data);
            newView.RowFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            glue.Properties.DataSource = newView;
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        private void Attach_Leave_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave += new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Detach_Leave_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave -= new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Clear_Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            glue.Properties.DataSource = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks.DefaultView;
            view.EndUpdate();
        }


        private void Attach_ButtonClick_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void Detach_ButtonClick_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick -= new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void ButtonClick_GridView(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    int intHeaderID = Convert.ToInt32(glue.Tag);
                    String strPickListName = "";
                    switch (glue.Name)
                    {
                        case "intSafetyGridLookUpEdit":
                            strPickListName = "Risk Categories";
                            break;
                        case "intGeneralConditionGridLookUpEdit":
                            strPickListName = "General Conditions";
                            break;
                        case "intVitalityGridLookUpEdit":
                            strPickListName = "Vitality";
                            break;
                        case "intRootHeaveGridLookUpEdit":
                        case "intRootHeave2GridLookUpEdit":
                        case "intRootHeave3GridLookUpEdit":
                            strPickListName = "Root Heave";
                            break;
                        case "intStemPhysicalGridLookUpEdit":
                        case "intStemPhysical2GridLookUpEdit":
                        case "intStemPhysical3GridLookUpEdit":
                            strPickListName = "Stem Physical Conditions";
                            break;
                        case "intStemDiseaseGridLookUpEdit":
                        case "intStemDisease2GridLookUpEdit":
                        case "intStemDisease3GridLookUpEdit":
                            strPickListName = "Stem Diseases";
                            break;
                        case "intCrownPhysicalGridLookUpEdit":
                        case "intCrownPhysical2GridLookUpEdit":
                        case "intCrownPhysical3GridLookUpEdit":
                            strPickListName = "Crown Physical Conditions";
                            break;
                        case "intCrownDiseaseGridLookUpEdit":
                        case "intCrownDisease2GridLookUpEdit":
                        case "intCrownDisease3GridLookUpEdit":
                            strPickListName = "Crown Diseases";
                            break;
                        case "intCrownFoliationGridLookUpEdit":
                        case "intCrownFoliation2GridLookUpEdit":
                        case "intCrownFoliation3GridLookUpEdit":
                            strPickListName = "Crown Foliation";
                            break;
                        case "UserPickList1GridLookUpEdit":
                            strPickListName = "Inspection User Defined 1";
                            break;
                        case "UserPickList2GridLookUpEdit":
                            strPickListName = "Inspection User Defined 2";
                            break;
                        case "UserPickList3GridLookUpEdit":
                            strPickListName = "Inspection User Defined 3";
                            break;
                        default:
                            break;
                    }
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, intHeaderID, strPickListName);
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
                    this.ValidateChildren();
                }
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        WorkOrderReferenceButtonEdit.Focus();

                        WorkOrderReferenceButtonEdit.Properties.ReadOnly = false;
                        WorkOrderReferenceButtonEdit.Properties.Buttons[0].Enabled = true;
                        WorkOrderReferenceButtonEdit.Properties.Buttons[1].Enabled = true;

                        strInvoiceNumberButtonEdit.Properties.ReadOnly = false;
                        strInvoiceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        strInvoiceNumberButtonEdit.Properties.Buttons[1].Enabled = true;

                        bbiCalculateDiscounts.Enabled = true;
                        barButtonItemPrint.Enabled = true;
                        gridControl1.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        WorkOrderReferenceButtonEdit.Focus();

                        WorkOrderReferenceButtonEdit.Properties.ReadOnly = false;
                        WorkOrderReferenceButtonEdit.Properties.Buttons[0].Enabled = true;
                        WorkOrderReferenceButtonEdit.Properties.Buttons[1].Enabled = true;

                        strInvoiceNumberButtonEdit.Properties.ReadOnly = false;
                        strInvoiceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        strInvoiceNumberButtonEdit.Properties.Buttons[1].Enabled = true;

                        bbiCalculateDiscounts.Enabled = true;
                        barButtonItemPrint.Enabled = true;
                        gridControl1.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        WorkOrderReferenceButtonEdit.Focus();

                        WorkOrderReferenceButtonEdit.Properties.ReadOnly = true;
                        WorkOrderReferenceButtonEdit.Properties.Buttons[0].Enabled = true;
                        WorkOrderReferenceButtonEdit.Properties.Buttons[1].Enabled = false;

                        strInvoiceNumberButtonEdit.Properties.ReadOnly = false;
                        strInvoiceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        strInvoiceNumberButtonEdit.Properties.Buttons[1].Enabled = false;

                        bbiCalculateDiscounts.Enabled = false;
                        barButtonItemPrint.Enabled = false;
                        gridControl1.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
                bbiCalculateDiscounts.Enabled = false;
                barButtonItemPrint.Enabled = false;
                gridControl1.Enabled = false;
            }
            SetMenuStatus();  // Just in case any default layout has permissions set wrongly //
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            StatusGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            StatusGridLookUpEdit.Properties.Buttons[1].Visible = false;
            StatusGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            StatusGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intPriorityIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intPriorityIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intPriorityIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intPriorityIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intContractorIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intContractorIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intContractorIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intContractorIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intIssuedByPersonIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intIssuedByPersonIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intIssuedByPersonIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intIssuedByPersonIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intIssuedByBodyIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intIssuedByBodyIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intIssuedByBodyIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intIssuedByBodyIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intCertifiedByIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intCertifiedByIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intCertifiedByIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intCertifiedByIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intSupervisor1GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intSupervisor1GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intSupervisor1GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intSupervisor1GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intSupervisor2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intSupervisor2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intSupervisor2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intSupervisor2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intSupervisor3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intSupervisor3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intSupervisor3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intSupervisor3GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPicklist1GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPicklist1GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPicklist1GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPicklist1GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPicklist2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPicklist2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPicklist2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPicklist2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPicklist3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPicklist3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPicklist3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPicklist3GridLookUpEdit.Properties.Buttons[2].Visible = false;

            repositoryItemGridLookUpEditPriority.Buttons[1].Enabled = false;
            repositoryItemGridLookUpEditPriority.Buttons[1].Visible = false;
            repositoryItemGridLookUpEditPriority.Buttons[2].Enabled = false;
            repositoryItemGridLookUpEditPriority.Buttons[2].Visible = false;
            repositoryItemGridLookUpEditOwnership.Buttons[1].Enabled = false;
            repositoryItemGridLookUpEditOwnership.Buttons[1].Visible = false;
            repositoryItemGridLookUpEditOwnership.Buttons[2].Enabled = false;
            repositoryItemGridLookUpEditOwnership.Buttons[2].Visible = false;
            repositoryItemGridLookUpEditActionByPerson.Buttons[1].Enabled = false;
            repositoryItemGridLookUpEditActionByPerson.Buttons[1].Visible = false;
            repositoryItemGridLookUpEditActionByPerson.Buttons[2].Enabled = false;
            repositoryItemGridLookUpEditActionByPerson.Buttons[2].Visible = false;
            repositoryItemGridLookUpEditSupervisor.Buttons[1].Enabled = false;
            repositoryItemGridLookUpEditSupervisor.Buttons[1].Visible = false;
            repositoryItemGridLookUpEditSupervisor.Buttons[2].Enabled = false;
            repositoryItemGridLookUpEditSupervisor.Buttons[2].Visible = false;    

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9053,9026,8001,30,9027,16,9050,9051,9052,2012,2009", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9053:  // Work Order Status //    
                        {
                            StatusGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            StatusGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            StatusGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            StatusGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9026:  // Job Priorities \ Linked Jobs Priorities //    
                        {
                            intPriorityIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intPriorityIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intPriorityIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intPriorityIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            repositoryItemGridLookUpEditPriority.Buttons[1].Enabled = boolUpdate;
                            repositoryItemGridLookUpEditPriority.Buttons[1].Visible = boolUpdate;
                            repositoryItemGridLookUpEditPriority.Buttons[2].Enabled = boolUpdate;
                            repositoryItemGridLookUpEditPriority.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 8001:  // Contractor Manager \ Linked Jobs - Action By \ Linked Jobs - Supervisor //    
                        {
                            intContractorIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intContractorIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intContractorIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intContractorIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            repositoryItemGridLookUpEditActionByPerson.Buttons[1].Enabled = boolUpdate;
                            repositoryItemGridLookUpEditActionByPerson.Buttons[1].Visible = boolUpdate;
                            repositoryItemGridLookUpEditActionByPerson.Buttons[2].Enabled = boolUpdate;
                            repositoryItemGridLookUpEditActionByPerson.Buttons[2].Visible = boolUpdate;

                            repositoryItemGridLookUpEditSupervisor.Buttons[1].Enabled = boolUpdate;
                            repositoryItemGridLookUpEditSupervisor.Buttons[1].Visible = boolUpdate;
                            repositoryItemGridLookUpEditSupervisor.Buttons[2].Enabled = boolUpdate;
                            repositoryItemGridLookUpEditSupervisor.Buttons[2].Visible = boolUpdate;    
                        }
                        break;
                    case 30:  // Staff Manager \ Certified By \Supervisor 1-3 //    
                        {
                            intIssuedByPersonIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intIssuedByPersonIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intIssuedByPersonIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intIssuedByPersonIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intCertifiedByIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intCertifiedByIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intCertifiedByIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intCertifiedByIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intSupervisor1GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intSupervisor1GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intSupervisor1GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intSupervisor1GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intSupervisor2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intSupervisor2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intSupervisor2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intSupervisor2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intSupervisor3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intSupervisor3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intSupervisor3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intSupervisor3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9027:  // Issuing Body //    
                        {
                            intIssuedByBodyIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intIssuedByBodyIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intIssuedByBodyIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intIssuedByBodyIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 16:  // Company Header //    
                        {
                            intCompanyHeaderIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intCompanyHeaderIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intCompanyHeaderIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intCompanyHeaderIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9050:  // User Define 1 //    
                        {
                            UserPicklist1GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPicklist1GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPicklist1GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPicklist1GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9051:  // User Define 2 //    
                        {
                            UserPicklist2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPicklist2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPicklist2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPicklist2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9052:  // User Define 3 //    
                        {
                            UserPicklist3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPicklist3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPicklist3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPicklist3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 2012:  // Linked Jobs - Ownership //    
                        {
                            repositoryItemGridLookUpEditOwnership.Buttons[1].Enabled = boolUpdate;
                            repositoryItemGridLookUpEditOwnership.Buttons[1].Visible = boolUpdate;
                            repositoryItemGridLookUpEditOwnership.Buttons[2].Enabled = boolUpdate;
                            repositoryItemGridLookUpEditOwnership.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 2009:  // Linked Actions //    
                        {
                            iBool_AllowAdd = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["CreateAccess"]);
                            iBool_AllowEdit = boolUpdate;
                            iBool_AllowDelete = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["DeleteAccess"]);
                         }
                        break;
                }
            }
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_AT_WorkOrders.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                this.sp01402ATWorkOrderActionsEditBindingSource.EndEdit();
                dsChanges = this.dataSet_AT_WorkOrders.GetChanges();
                if (dsChanges != null)
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                    bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                    bbiFormSave.Enabled = true;
                    return true;
                }
                else
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                    bbiSave.Enabled = false;
                    bbiFormSave.Enabled = false;
                    return false;
                }
            }
        }


        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

 
            if (!iBool_AllowEdit || strFormMode == "blockedit")
            {
                gridControl1.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intWorkOrderID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            DateTime dtDateCompleted = Convert.ToDateTime("01/01/1900");
            if (currentRow != null)
            {
                intWorkOrderID = (currentRow["intWorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["intWorkOrderID"]));
                dtDateCompleted = (currentRow["dtCompleteDate"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(currentRow["dtCompleteDate"]));
            }

            if (i_int_FocusedGrid == 1)  // Actions //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                /*if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }*/
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view" ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (strFormMode != "view" ? true : false);  // Move Up Btn //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (strFormMode != "view" ? true : false);  // Move Down Btn //

        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }

        public void UpdateFormWithPDFFileName(int intWorkOrderID, string strPDFFileName)
        {
            // Called by frm_AT_WorkOrder_Print screen only, to update the Saved PDF Filename field //
            int intSelectedWorkOrderID = 0;
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                if (currentRow["intWorkOrderID"] == null || string.IsNullOrEmpty(currentRow["intWorkOrderID"].ToString()))
                {
                    intSelectedWorkOrderID = 0;
                }
                else
                {
                    intSelectedWorkOrderID = Convert.ToInt32(currentRow["intWorkOrderID"]);
                }
            }
            if (intSelectedWorkOrderID == intWorkOrderID)
            {
                currentRow["WorkOrderPDFFile"] = strPDFFileName;
            }
        }


        private void frm_AT_WorkOrder_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                LoadLinkedRecords();
            }
            SetMenuStatus();
        }

        private void frm_AT_WorkOrder_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        Detach_Enter_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Enter Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_Leave_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Leave Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_ButtonClick_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach ButtonClick Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            // Commit any outstanding changes within the grid control //
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            } 



            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            // Save Work Order Header //
            this.sp01399ATWorkOrderEditBindingSource.EndEdit();
            try
            {
                this.sp01399_AT_Work_Order_EditTableAdapter.Update(dataSet_AT_WorkOrders);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked Actions //
            this.sp01402ATWorkOrderActionsEditBindingSource.EndEdit();
            try
            {
                this.sp01402_AT_Work_Order_Actions_EditTableAdapter.Update(dataSet_AT_WorkOrders);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked action changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["intWorkOrderID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open forms which reference this data that they will need to refresh their data on activating //
            Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
            Broadcast.WorkOrder_Refresh(this.ParentForm, this.Name, strNewIDs);

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            int intLinkedRecordNew = 0;
            int intLinkedRecordModified = 0;
            int intLinkedRecordDeleted = 0;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }


            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_AT_WorkOrders.sp01402_AT_Work_Order_Actions_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_AT_WorkOrders.sp01402_AT_Work_Order_Actions_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intLinkedRecordNew > 0 || intLinkedRecordModified > 0 || intLinkedRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New Work Order record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated Work Order record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted Work Order record(s)\n";
                if (intLinkedRecordNew > 0) strMessage += Convert.ToString(intLinkedRecordNew) + " New Linked Action record(s)\n";
                if (intLinkedRecordModified > 0) strMessage += Convert.ToString(intLinkedRecordModified) + " Updated Linked Action record(s)\n";
                if (intLinkedRecordDeleted > 0) strMessage += Convert.ToString(intLinkedRecordDeleted) + " Deleted Linked Action record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nYou must save the change(s) before opening the Print Work Order screen.\n\nSave Changes now?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
                {
                    case DialogResult.No:
                        return;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) return;  // Save Failed so abort opening Print Work Order screen //
                        break;
                }
            }
            string strSelectedWorkOrderID = "";
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            if (currentRow != null) strSelectedWorkOrderID = (currentRow["intWorkOrderID"] == null || string.IsNullOrEmpty(currentRow["intWorkOrderID"].ToString()) ? "" : currentRow["intWorkOrderID"].ToString() + ";");

            // Attempt to activate Print Work Orders screen if it is already open otherwise open it //
            frm_AT_WorkOrder_Print frmWorkOrderPrint = null;
            try
            {
                foreach (frmBase frmChild in this.MdiParent.MdiChildren)
                {
                    if (frmChild.FormID == 92003)
                    {
                        frmChild.Activate();
                        frmWorkOrderPrint = (frm_AT_WorkOrder_Print)frmChild;
                        frmWorkOrderPrint.LoadWorkOrdersList();  // Make sure list of Work Orders on screen is up-to-date //
                        frmWorkOrderPrint.LoadWorkOrder_ExternalCall(strSelectedWorkOrderID);
                        return;
                    }
                }
            }
            catch (Exception)
            {
            }
            // If we are here then the form was NOT already open, so open it //
            frmWorkOrderPrint = new frm_AT_WorkOrder_Print();
            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();
            frmWorkOrderPrint.fProgress = fProgress;
            frmWorkOrderPrint.MdiParent = this.MdiParent;
            frmWorkOrderPrint.GlobalSettings = this.GlobalSettings;
            frmWorkOrderPrint.Show();

            frmWorkOrderPrint.LoadWorkOrder_ExternalCall(strSelectedWorkOrderID);
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmWorkOrderPrint, new object[] { null });
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strWorkOrderID = "";
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                strWorkOrderID = (currentRow["intWorkOrderID"] == null ? "" : currentRow["intWorkOrderID"].ToString() + ",");
                if (strWorkOrderID == "" || strWorkOrderID == "0,") strWorkOrderID = "-9999,";  // Don't load anythingas Work Order hasn't had it's first save yet //
            }

            // Inspections //
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            sp01402_AT_Work_Order_Actions_EditTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01402_AT_Work_Order_Actions_Edit, strWorkOrderID);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Actions Linked - Click Add button to link Actions");
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record("");
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 0) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "intSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "intSortOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "intSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "intSortOrder")) + 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow >= view.DataRowCount - 1) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "intSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "intSortOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "intSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "intSortOrder")) - 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "CostCentreCode":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "intOwnershipID")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "CostCentreCode":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("intOwnershipID")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            int intFocusedRow = view.FocusedRowHandle;
            if (intFocusedRow == GridControl.InvalidRowHandle) return;
            DataRow dr = view.GetDataRow(intFocusedRow);
            switch (view.FocusedColumn.Name)
            {
                case "colmonJobRateActual":
                    {
                        decimal decRate = (e.Value == null || string.IsNullOrEmpty(e.Value.ToString()) ? (decimal)0.00 : (decimal)e.Value);
                        decimal decUnits = (string.IsNullOrEmpty(dr["monJobWorkUnits"].ToString()) ? (decimal)0.00 : (decimal)dr["monJobWorkUnits"]);
                        decimal decDiscount = (string.IsNullOrEmpty(dr["monDiscountRate"].ToString()) ? (decimal)0.00 : (decimal)dr["monDiscountRate"]);
                        view.SetFocusedRowCellValue("monActualCost", decimal.Round((decRate * decUnits) - ((decRate * decUnits) * decDiscount) / 100, 2));
                    }
                    break;
                case "colmonJobWorkUnits":
                    {
                        decimal decRate = (string.IsNullOrEmpty(dr["monJobRateActual"].ToString()) ? (decimal)0.00 : (decimal)dr["monJobRateActual"]);
                        decimal decUnits = (e.Value == null || string.IsNullOrEmpty(e.Value.ToString()) ? (decimal)0.00 : (decimal)e.Value);
                        decimal decDiscount = (string.IsNullOrEmpty(dr["monDiscountRate"].ToString()) ? (decimal)0.00 : (decimal)dr["monDiscountRate"]);
                        view.SetFocusedRowCellValue("monActualCost", decimal.Round((decRate * decUnits) - ((decRate * decUnits) * decDiscount) / 100, 2));

                        // Calculate Budgeted Cost as Work Units have changed //
                        decRate = (string.IsNullOrEmpty(dr["monJobRateBudgeted"].ToString()) ? (decimal)0.00 : (decimal)dr["monJobRateBudgeted"]);
                        view.SetFocusedRowCellValue("monBudgetedCost", decimal.Round((decRate * decUnits), 2));
                    }
                    break;
                case "colmonDiscountRate":
                    {
                        decimal decRate = (string.IsNullOrEmpty(dr["monJobRateActual"].ToString()) ? (decimal)0.00 : (decimal)dr["monJobRateActual"]);
                        decimal decUnits = (string.IsNullOrEmpty(dr["monJobWorkUnits"].ToString()) ? (decimal)0.00 : (decimal)dr["monJobWorkUnits"]);
                        decimal decDiscount = (e.Value == null || string.IsNullOrEmpty(e.Value.ToString()) ? (decimal)0.00 : (decimal)e.Value);
                        view.SetFocusedRowCellValue("monActualCost", decimal.Round((decRate * decUnits) - ((decRate * decUnits) * decDiscount) / 100, 2));
                    }
                    break;
            }

        }


        private void repositoryItemButtonEditCostCentreCode_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            if (intFocusedRow == GridControl.InvalidRowHandle) return;
            
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("choose".Equals(e.Button.Tag))
                {
                    frm_AT_Select_Cost_Centre fChildForm = new frm_AT_Select_Cost_Centre();
                    fChildForm.intOwnershipID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("intOwnershipID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("intOwnershipID")));
                    fChildForm.intOriginalCostCentreID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("intCostCentreID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("intCostCentreID")));
                    this.ParentForm.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;

                    if (fChildForm.ShowDialog() == DialogResult.OK)
                    {
                        view.SetFocusedRowCellValue("intBudgetID", fChildForm.intSelectedBudgetID);
                        view.SetFocusedRowCellValue("intCostCentreID", fChildForm.intSelectedCostCentreID);
                        view.SetFocusedRowCellValue("CostCentreCode", fChildForm.strSelectedCostCentreCode);
                        SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
                    }
                }
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered cost centre.\n\nAre you sure you wish to proceed?", "Clear Cost Centre", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    view.SetFocusedRowCellValue("intCostCentreID", 0);
                    view.SetFocusedRowCellValue("CostCentreCode", "");
                    SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
                }
            }

        }

        private void repositoryItemButtonEditActualJobRateDescription_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            if (intFocusedRow == GridControl.InvalidRowHandle) return;
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            if (currentRow == null) return;
     
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {

                view.PostEditor();
                view.CloseEditor();
                if (!view.UpdateCurrentRow())
                {
                    fProgress.Close();
                    fProgress.Dispose();
                    XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Get Rate", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                } 

                int intOnScheduleOfRates = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("intScheduleOfRates").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("intScheduleOfRates")));
                int intInspectionID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("intInspectionID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("intInspectionID")));
                int intActionBy = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("intActionBy").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("intActionBy")));
                int intAction = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("intAction").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("intAction")));
                if (intOnScheduleOfRates == 1 && intAction == 0)
                {
                    XtraMessageBox.Show("No Job Type has been set for this action - the system will be unable to highlight best match rates for you.", "Select Job Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                var fChildForm = new frm_AT_Action_Edit_Select_Job_Rate();
                fChildForm.intJobRate_RateID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("intJobRateActual").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("intJobRateActual")));
                fChildForm.intOnScheduleOfRates = intOnScheduleOfRates;
                fChildForm.intInspectionID = intInspectionID;
                fChildForm.intActionByID = intActionBy;
                fChildForm.intAction = intAction;

                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    view.SetFocusedRowCellValue("ActualJobRateDescription", fChildForm.strSelectedRateDescription);
                    view.SetFocusedRowCellValue("monJobRateActual", fChildForm.decSelectedRate);
                    view.SetFocusedRowCellValue("intJobRateActual", fChildForm.intSelectedRateID);
                    view.UpdateCurrentRow();
                    SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered Job Rate.\n\nAre you sure you wish to proceed?", "Clear Job Rate", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    view.SetFocusedRowCellValue("ActualJobRateDescription", "");
                    view.SetFocusedRowCellValue("monJobRateActual", (decimal)0.00);
                    view.SetFocusedRowCellValue("intJobRateActual", 0);
                    view.UpdateCurrentRow();
                    SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
                }
            }
            Calculate_Total_Cost("Budgeted", intFocusedRow);
            Calculate_Total_Cost("Actual", intFocusedRow);
        }

        private void Calculate_Total_Cost(string strType, int intRowHandle)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (strType == "Budgeted")
            {
                view.SetFocusedRowCellValue("monBudgetedCost", decimal.Round((string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "monJobRateBudgeted").ToString()) ? (decimal)0.00 : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "monJobRateBudgeted"))) * (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "monJobWorkUnits").ToString()) ? (decimal)0.00 : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "monJobWorkUnits"))), 2));
            }
            else // Actual //
            {
                decimal decAmount = decimal.Round((string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "monJobRateActual").ToString()) ? (decimal)0.00 : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "monJobRateActual"))) * (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "monJobWorkUnits").ToString()) ? (decimal)0.00 : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "monJobWorkUnits"))), 2);
                view.SetFocusedRowCellValue("monActualCost", decimal.Round(decAmount - (decAmount * (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "monDiscountRate").ToString()) ? (decimal)0.00 : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "monDiscountRate"))) / 100), 2));
            }
        }


        private void repositoryItemGridLookUpEditPriority_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 126, "Priorities");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
                }
            }
        }

        private void repositoryItemGridLookUpEditPriority_Enter(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = 126 or [HeaderID] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void repositoryItemGridLookUpEditPriority_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void repositoryItemGridLookUpEditOwnership_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 900, "Cost Centres");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01362_AT_Ownership_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01362_AT_Ownership_List_With_Blank);
                }
            }
        }

        private void repositoryItemGridLookUpEditActionByPerson_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 1, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00190_Contractor_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00190_Contractor_List_With_Blank);
                }
            }
        }

        private void repositoryItemGridLookUpEditSupervisor_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 1, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00190_Contractor_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00190_Contractor_List_With_Blank);
                }
            }
        }


        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemTextEditMonJobRateActual_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemTextEdit2_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemButtonEditActualJobRateDescription_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemSpinEditMonJobWorkUnits_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemDateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetChangesPendingLabel();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemTextEditNearestHouse_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemGridLookUpEditOwnership_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemGridLookUpEditActionByPerson_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemGridLookUpEditSupervisor_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemGridLookUpEditPriority_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemButtonEditCostCentreCode_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemSpinEditDiscountRate_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        #endregion

        #endregion


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveFirst();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveFirst();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveFirst();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //
                                        bs.MovePrevious();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MovePrevious();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MovePrevious();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveNext();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveNext();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveNext();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveLast();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveLast();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveLast();
                        }
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else  // Load in Past Inspections... //
                {
                    if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd")
                    {
                        LoadLinkedRecords();
                        GridView view = (GridView)gridControl1.MainView;
                        view.ExpandAllGroups();
                    }
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void WorkOrderReferenceButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            string strTableName = "tblWorkOrder";
            string strFieldName = "WorkOrderReference";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    WorkOrderReferenceButtonEdit.EditValue = fChildForm.SelectedSequence;
                    //i_strLastUsedSequencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = WorkOrderReferenceButtonEdit.EditValue.ToString() ?? "";

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";


                // Check if value is already present within dataset //
                string strCurrentWorkOrderReference = WorkOrderReferenceButtonEdit.EditValue.ToString();
                if (string.IsNullOrEmpty(strCurrentWorkOrderReference)) strCurrentWorkOrderReference = "";
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.Rows)
                    {
                        if (dr["WorkOrderReference"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                WorkOrderReferenceButtonEdit.EditValue = strSequence + strTempNumber;
            }
        }

        private void strInvoiceNumberButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            string strTableName = "tblWorkOrder";
            string strFieldName = "strInvoiceNumber";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    strInvoiceNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    //i_strLastUsedSequencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = strInvoiceNumberButtonEdit.EditValue.ToString() ?? "";

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";


                // Check if value is already present within dataset //
                string strCurrentInvoiceNumber = strInvoiceNumberButtonEdit.EditValue.ToString();
                if (string.IsNullOrEmpty(strCurrentInvoiceNumber)) strCurrentInvoiceNumber = "";
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_AT_WorkOrders.sp01399_AT_Work_Order_Edit.Rows)
                    {
                        if (dr["strInvoiceNumber"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                strInvoiceNumberButtonEdit.EditValue = strSequence + strTempNumber;
            }
        }

        private void intPrevWorkOrderIDButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            if (currentRow == null) return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("choose".Equals(e.Button.Tag))
                {
                    frm_AT_Select_Work_Order fChildForm = new frm_AT_Select_Work_Order();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strExcludedWorkOrderIDs = (string.IsNullOrEmpty(currentRow["intWorkOrderID"].ToString()) ? "" : currentRow["intWorkOrderID"].ToString() + ",");
                    fChildForm.intOriginalSelectedValue = (string.IsNullOrEmpty(currentRow["intPrevWorkOrderID"].ToString()) ? 0 : Convert.ToInt32(currentRow["intPrevWorkOrderID"]));                 
                    
                    if (fChildForm.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                    {
                        currentRow["intPrevWorkOrderID"] = fChildForm.intSelectedID;
                    }
                }
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the Linked Prior Work Order.\n\nAre you sure you wish to proceed?", "Clear Linked Prior Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    currentRow["intPrevWorkOrderID"] = 0;
                }
            }
        }

        private void intContractorIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 1, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00190_Contractor_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00190_Contractor_List_With_Blank);
                }
            }
        }

        private void intIssuedByPersonIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 102, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00226_Staff_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);
                }
            }
        }

        private void intCompanyHeaderIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 8, "Company Headers");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp00232_Company_Header_Drop_Down_List_With_Blank);
                }
            }
        }

        private void intCertifiedByIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 102, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00226_Staff_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);
                }
            }
        }

        private void intSupervisor1GridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 102, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00226_Staff_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);
                }
            }
        }

        private void intSupervisor2GridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 102, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00226_Staff_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);
                }
            }
        }

        private void intSupervisor3GridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 102, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00226_Staff_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);
                }
            }
        }

        private void TargetDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", ItemForTargetDate.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    TargetDateDateEdit.EditValue = null;
                }
            }
        }

        private void dtIssueDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", ItemFordtIssueDate.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dtIssueDateDateEdit.EditValue = null;
                }
            }
        }

        private void dtCompleteDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", ItemFordtCompleteDate.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dtCompleteDateDateEdit.EditValue = null;
                    SetMenuStatus();  // Make Sure Add Action button is enabled if appropriate //
                }
            }
        }

        private void dtCompleteDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }

        private void dtCompleteDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;

            DateEdit de = (DateEdit)sender;
            if (de.DateTime != Convert.ToDateTime("01/01/0001"))
            {
                // Set all outstanding actions to complete //
                GridView view = (GridView)gridControl1.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "dtDoneDate").ToString())) view.SetRowCellValue(i, "dtDoneDate", de.DateTime);
                }
            }
            SetMenuStatus();  // Make Sure Add Action button is enabled if appropriate //
        }

        private void intContractorIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[Disabled] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void intContractorIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void WorkOrderPDFFileHyperLinkEdit_OpenLink(object sender, OpenLinkEventArgs e)
        {
            string strFile = WorkOrderPDFFileHyperLinkEdit.Text.ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Saved Work Order File Linked - unable to proceed.", "View Saved Work Order File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strFilePath = "";
            try
            {
                strFilePath = strDefaultPath;
                if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                strFilePath += strFile;
                //System.Diagnostics.Process.Start(strFilePath);

                frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                fChildForm.strPDFFile = strFilePath;
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.Show();
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Saved Work Order File: " + strFilePath + ".\n\nThe Saved Work Order File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Saved Work Order File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        #endregion


        private void bbiCalculateDiscounts_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            if (currentRow == null) return;
            int ll_WorkOrderContractorID = (currentRow["intContractorID"] == null || string.IsNullOrEmpty(currentRow["intContractorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["intContractorID"]));
            
            string strDiscountedActions = "";
            GridView view = (GridView)gridControl1.MainView;
            ArrayList ArrayListUniqueJobRates = new ArrayList(); // Stores unique set of Job Rate IDs in ArrayList from linked actions //

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "intScheduleOfRates")) == 1)
                {
                    strDiscountedActions += view.GetRowCellValue(i, "intActionID").ToString() + ",";
                    if (!ArrayListUniqueJobRates.Contains(Convert.ToInt32(view.GetRowCellValue(i, "intJobRateActual")))) ArrayListUniqueJobRates.Add(Convert.ToInt32(view.GetRowCellValue(i, "intJobRateActual")));
                }
            }
            if (string.IsNullOrEmpty(strDiscountedActions))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The current Work Order does not have any linked actions which are On Schedule Of Rates.\n\nNo Discounts Calculated.", "Calculate Discounts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (ArrayListUniqueJobRates.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The current Work Order does not have any linked actions which are linked to Job Rates.\n\nNo Discounts Calculated.", "Calculate Discounts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Resolve missing criteria data for each action and store in memory along with dummy rate field used to hold any matched discount rate calculated later //
            SqlDataAdapter sdaResolvedActions = new SqlDataAdapter();
            DataSet dsResolvedActions = new DataSet("NewDataSet");

            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp01407_AT_Work_Order_Discounts_Resolve_Criteria", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@strRecordIDs", strDiscountedActions));
            sdaResolvedActions = new SqlDataAdapter(cmd);
            sdaResolvedActions.Fill(dsResolvedActions, "Table");
            if (dsResolvedActions.Tables[0].Rows.Count == 0) return;  // No Resolved Actions so abort //

            // Get Full list of Rates and store in memory //
            SqlDataAdapter sdaRates = new SqlDataAdapter();
            DataSet dsRates = new DataSet("NewDataSet");
            cmd = null;
            cmd = new SqlCommand("sp01408_AT_Work_Order_Discounts_Get_Rates", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@strRecordIDs", strDiscountedActions));
            sdaRates = new SqlDataAdapter(cmd);
            sdaRates.Fill(dsRates, "Table");
            if (dsRates.Tables[0].Rows.Count == 0) 
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No discount rates are present within the Job Rate Manager for the linked actions.\n\nNo Discounts Calculated.", "Calculate Discounts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Criteria Used //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intCriteriaType = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesJobRateCriteriaID"));

            int intRowHandle = 0;
            Decimal decTotalWorkUnits = (decimal)0.00;
            decimal decRate = (decimal)0.00;
            decimal decUnits = (decimal)0.00;

            // Step round each unique selected Job Rate and calculate discount amount for that rate //
            foreach (int intUniqueJobRateID in ArrayListUniqueJobRates)
            {
                foreach (DataRow dr in dsRates.Tables[0].Rows)
                {
		            decimal decRateDiscount = Convert.ToDecimal(dr["monRate"]);
		            int intRateCriteriaID = Convert.ToInt32(dr["intCriteriaID"]);
		            int intRateContractorID = Convert.ToInt32(dr["intContractorID"]);
		            decimal decRateWorkUnits = Convert.ToDecimal(dr["decRateUnits"]);
            	
		            if (intRateContractorID != 0 && intRateContractorID != ll_WorkOrderContractorID) continue;  // Abort at this point if not blank ContractorID or no match on ContractorID //

                    string strFilter = "monDummySelectedRate = 0.00 and intAction = " + dr["intJobID"].ToString() + " and intJobRateActual = " + intUniqueJobRateID.ToString();
	
	                if (intRateCriteriaID > 0)  // Add to filter string //
                    {
                        switch (intCriteriaType)
	                    {
			                case 1:  // crown diameters //
				                strFilter += " and intCrownDiameter = " + intRateCriteriaID.ToString();
                                break;
                            case 2:  // dbh bands //
				                strFilter += " and intDBHRange = " + intRateCriteriaID.ToString();
                                break;
			                case 3:  // clients //
				                strFilter += " and ClientID = " + intRateCriteriaID.ToString();
                                break;
                            case 4:  // height bands //
				                strFilter += " and intHeightRange = " + intRateCriteriaID.ToString();
                                break;
                            case 5:  // risk categories //
				                strFilter += " and intSafetyPriority = " + intRateCriteriaID.ToString();
                                break;
                            case 6:  // site hazard classes //
				                strFilter += " and intSiteHazardClass = " + intRateCriteriaID.ToString();
                                break;
                            case 7:  //sizes //
				                strFilter += " and intSize = " + intRateCriteriaID.ToString();
                                break;
                            case 8:  // species //
				                strFilter += " and intSpeciesID = " + intRateCriteriaID.ToString();
                                break;
                        }
                    }
	                // Apply filter //
                    DataRow[] drFilteredActions = dsResolvedActions.Tables[0].Select(strFilter);
	                if (drFilteredActions.Length <= 0 ) continue; // Go to next iteration of loop //

                    // Get total work units to see if it is more that the current discount rate threshold - lookup value from actual action grid in case monJobworkUnits in grid is different than value in DB //
                    decTotalWorkUnits = (decimal)0.00;
                    foreach (DataRow drFilteredItem in drFilteredActions)
                    {
                        intRowHandle = view.LocateByValue(0, view.Columns["intActionID"], Convert.ToInt32(drFilteredItem["intActionID"]));
                        if (intRowHandle != GridControl.InvalidRowHandle) decTotalWorkUnits += (decimal)(string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "monJobWorkUnits").ToString()) ? 0 : view.GetRowCellValue(intRowHandle, "monJobWorkUnits"));
                    }
                  	if (decTotalWorkUnits <= (decimal)0.00 || decTotalWorkUnits <= decRateWorkUnits) continue;  // Go to next iteration of loop //

		            // Update holding ds - set each actions dummy discount rate value so they are not used again in this process //
                    foreach (DataRow drFilteredItem in drFilteredActions)
	                {
                		 drFilteredItem["monDummySelectedRate"] = decRateDiscount;
	                }
                }
            }
            // Update Linked Actions grid with correct discount amounts and adjust the total actual cost of each action //
            foreach (DataRow dr in dsResolvedActions.Tables[0].Rows)
            {
                intRowHandle = view.LocateByValue(0, view.Columns["intActionID"], Convert.ToInt32(dr["intActionID"]));
                if (intRowHandle != GridControl.InvalidRowHandle)
                {
                    view.SetRowCellValue(intRowHandle, "monDiscountRate", (decimal)dr["monDummySelectedRate"]);
                    // Calculate Toitals //
                    decRate = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "monJobRateActual").ToString()) ? (decimal)0.00 : (decimal)view.GetRowCellValue(intRowHandle, "monJobRateActual"));
                    decUnits = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "monJobWorkUnits").ToString()) ? (decimal)0.00 : (decimal)view.GetRowCellValue(intRowHandle, "monJobWorkUnits"));
                    view.SetRowCellValue(intRowHandle, "monActualCost", (decRate * decUnits) - ((decRate * decUnits) * (decimal)dr["monDummySelectedRate"]) / 100);
                }
            }


        }

 
        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record("");
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        public void Add_Record(string strTreeIDs)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            if (currentRow == null) return;
            int intWorkOrderID = (currentRow["intWorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["intWorkOrderID"]));
            if (intWorkOrderID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before adding Linked Actions.", "Add Linked Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            GridView view = (GridView)gridControl1.MainView; ;
            view.PostEditor();
            if (!iBool_AllowAdd) return;
            
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            int intMaxOrder = 0;
            string strExcludeActions = "";
            for (int i = 0; i < view.DataRowCount; i++)
            {
                strExcludeActions += view.GetRowCellValue(i, "intActionID").ToString() + ";";
                if (Convert.ToInt32(view.GetRowCellValue(i, "intSortOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "intSortOrder"));
            }

            frm_AT_Work_Order_Add_Actions fChildForm = new frm_AT_Work_Order_Add_Actions();
            fChildForm.strExcludedActionIDs = strExcludeActions;
            this.ParentForm.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInTreeIDsFromMap = strTreeIDs;

            fProgress.SetProgressValue(100);  // Show full progress //
            fProgress.Close();  // close Progress Window //
            fProgress = null;

            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Linking Actions...");
                fProgress.Show();
                Application.DoEvents();

                view.BeginUpdate();
                view.BeginSort();
                DataTable dtNew = fChildForm.dtSelectedActions;
                // Remove columns which don't exist in the Work Orders Linked Actions datatable otherwise the Import statement later in the process won't work (the source and destination must have the same structure) // 
                dtNew.Columns.Remove("ActionPriority");
                dtNew.Columns.Remove("ActionBy");
                dtNew.Columns.Remove("ActionSupervisor");
                dtNew.Columns.Remove("ActionOwnership");

                foreach (DataRow dr in dtNew.Rows)
                {
                    intMaxOrder++;
                    dr["intSortOrder"] = intMaxOrder;
                    dr["intWorkOrderID"] = intWorkOrderID;

                    if (Convert.ToDecimal(dr["monJobRateBudgeted"]) != (decimal)0.00)  // Copy Budgeted Rate //
                    {
                        dr["ActualJobRateDescription"] = dr["BudgetedRateDescription"];
                        dr["monJobRateActual"] = dr["monJobRateBudgeted"];
                        dr["intJobRateActual"] = dr["intJobRateBudgeted"];
                        decimal decUnits = Convert.ToDecimal(dr["monJobWorkUnits"]);
                        decimal decDiscount = Convert.ToDecimal(dr["monDiscountRate"]);
                        decimal decReturnedRate = decimal.Round((string.IsNullOrEmpty(dr["monJobRateActual"].ToString()) ? 0 : Convert.ToDecimal(dr["monJobRateActual"])), 2);
                        dr["monActualCost"] = decimal.Round((decReturnedRate * decUnits) - ((decReturnedRate * decUnits) * decDiscount) / 100, 2);
                    }
                    else if (Convert.ToInt32(dr["intScheduleOfRates"]) == 1 && Convert.ToDecimal(dr["monJobRateActual"]) == (decimal)0.00)  // Need to Calc Best Match Rate if Possible //
                    {
                        int intInspectionID = (string.IsNullOrEmpty(dr["intInspectionID"].ToString()) ? 0 : Convert.ToInt32(dr["intInspectionID"]));
                        int intActionBy = (string.IsNullOrEmpty(dr["intActionBy"].ToString()) ? 0 : Convert.ToInt32(dr["intActionBy"]));
                        int intAction = (string.IsNullOrEmpty(dr["intAction"].ToString()) ? 0 : Convert.ToInt32(dr["intAction"]));
                        if (intAction > 0)
                        {
                            string strReturnedRateDescription = "";
                            decimal decReturnedRate = (decimal)0.00;
                            int intReturnedRateID = 0;
                            Get_Best_Match_Rate BestMatchRate = new Get_Best_Match_Rate();
                            BestMatchRate.GetRate(strConnectionString, ref strReturnedRateDescription, ref decReturnedRate, ref intReturnedRateID, 1, intInspectionID, intActionBy, intAction);

                            if (!string.IsNullOrEmpty(strReturnedRateDescription))
                            {
                                dr["ActualJobRateDescription"] = strReturnedRateDescription;
                                dr["monJobRateActual"] = decReturnedRate;
                                dr["intJobRateActual"] = intReturnedRateID;
                                decimal decUnits = Convert.ToDecimal(dr["monJobWorkUnits"]);
                                decimal decDiscount = Convert.ToDecimal(dr["monDiscountRate"]);
                                dr["monActualCost"] = decimal.Round((decReturnedRate * decUnits) - ((decReturnedRate * decUnits) * decDiscount) / 100, 2);
                            }
                        }
                    }
                    this.dataSet_AT_WorkOrders.sp01402_AT_Work_Order_Actions_Edit.ImportRow(dr);
                }
                view.EndSort();
                view.EndUpdate();
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
            }
        }

        private void Delete_Record()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            if (!iBool_AllowDelete) return;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Actions to remove from the Work Order by clicking on them then try again.", "Remove Linked Actions from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Action" : Convert.ToString(intRowHandles.Length) + " Linked Actions") + " selected for removing from the current Work Order!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Action" : "these Linked Actions") + " will be removed from the work order. They will then be available for linking to a different work order.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Actions from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Removing...");
                fProgress.Show();
                Application.DoEvents();

                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    view.DeleteRow(intRowHandles[i]);
                }

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }

                // Clean Up any holes in the Sort Order Sequence... //
                int intCorrectOrder = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    intCorrectOrder++;
                    if (Convert.ToInt32(view.GetRowCellValue(i, "intSortOrder")) != intCorrectOrder) view.SetRowCellValue(i, "intSortOrder", intCorrectOrder);
                }

                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Work Order.\n\nIMPORTANT NOTE: You must save changes to this work order before the marked linked records are physically removed from the Work Order.", "Remove Linked Actions from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public Boolean Action_Adding_Allowed(ref string strWorkOrderIDs)
        {
            // Called by Tree Picker - Transfer Actions to Work Order process //
            // strWorkOrderIDs passed by reference so original value is updated if there is a WorkOrderID present on the form //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //          
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            if (currentRow == null) return false;
            int intWorkOrderID = (currentRow["intWorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["intWorkOrderID"]));
            if (intWorkOrderID == 0) return false;

            DateTime dt = (currentRow["dtCompleteDate"] == DBNull.Value ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(currentRow["dtCompleteDate"]));
            if (dt == Convert.ToDateTime("01/01/0001"))
            {
                strWorkOrderIDs += intWorkOrderID.ToString() + ",";
                return true;
            }
            else
            {
                return false;
            }
        }

        public int Return_ActionsID_To_Calling_Form()
        {
            // Called by Tree Picker - Transfer Actions to Work Order process //
            // Returns the WorkOrderID if the work order is not completed. If completed it returns 0 //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //          
            DataRowView currentRow = (DataRowView)sp01399ATWorkOrderEditBindingSource.Current;
            if (currentRow == null) return 0;
            int intWorkOrderID = (currentRow["intWorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["intWorkOrderID"]));
            if (intWorkOrderID == 0) return 0;

            DateTime dt = (currentRow["dtCompleteDate"] == DBNull.Value ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(currentRow["dtCompleteDate"]));
            if (dt == Convert.ToDateTime("01/01/0001"))
            {
                return intWorkOrderID;
            }
            else
            {
                return 0;
            }
        }



    }
}


namespace WoodPlan5
{
    partial class frm_AT_Inspection_Edit_Paste_Actions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01380ATActionsLinkedToInspectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostDifference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.gridLookUpEditCarriedOutBy = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00190ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalCOntractorDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.dateEditDueDate = new DevExpress.XtraEditors.DateEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnGetActions = new DevExpress.XtraEditors.SimpleButton();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.buttonEditSequence = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.sp00190_Contractor_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01380ATActionsLinkedToInspectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCarriedOutBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDueDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSequence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(625, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 618);
            this.barDockControlBottom.Size = new System.Drawing.Size(625, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 592);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(625, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 592);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.groupControl4);
            this.layoutControl1.Controls.Add(this.groupControl3);
            this.layoutControl1.Controls.Add(this.groupControl2);
            this.layoutControl1.Controls.Add(this.groupControl1);
            this.layoutControl1.Controls.Add(this.buttonEditSequence);
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(624, 559);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.gridSplitContainer1);
            this.groupControl4.Location = new System.Drawing.Point(7, 233);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(610, 319);
            this.groupControl4.TabIndex = 8;
            this.groupControl4.Text = "Select one or more Actions by Ticking:";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(2, 22);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(606, 295);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01380ATActionsLinkedToInspectionsBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(606, 295);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01380ATActionsLinkedToInspectionsBindingSource
            // 
            this.sp01380ATActionsLinkedToInspectionsBindingSource.DataMember = "sp01380_AT_Actions_Linked_To_Inspections";
            this.sp01380ATActionsLinkedToInspectionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID1,
            this.colTreeID1,
            this.colLocalityID2,
            this.colDistrictID2,
            this.colTreeReference1,
            this.colTreeMappingID1,
            this.colInspectionReference1,
            this.colInspectionDate1,
            this.colActionID,
            this.colActionJobNumber,
            this.colAction,
            this.colActionPriority,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colActionBy,
            this.colActionSupervisor,
            this.colActionOwnership,
            this.colActionCostCentre,
            this.colActionBudget,
            this.colActionWorkUnits,
            this.colActionScheduleOfRates,
            this.colActionBudgetedRateDescription,
            this.colActionBudgetedRate,
            this.colActionBudgetedCost,
            this.colActionActualRateDescription,
            this.colActionActualRate,
            this.colActionActualCost,
            this.colActionWorkOrder,
            this.colActionRemarks,
            this.colActionUserDefined1,
            this.colActionUserDefined2,
            this.colActionUserDefined3,
            this.colActionCostDifference,
            this.colActionCompletionStatus,
            this.colActionTimeliness});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDueDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            // 
            // colInspectionID1
            // 
            this.colInspectionID1.Caption = "Inspection ID";
            this.colInspectionID1.FieldName = "InspectionID";
            this.colInspectionID1.Name = "colInspectionID1";
            this.colInspectionID1.OptionsColumn.AllowEdit = false;
            this.colInspectionID1.OptionsColumn.AllowFocus = false;
            this.colInspectionID1.OptionsColumn.ReadOnly = true;
            this.colInspectionID1.Width = 76;
            // 
            // colTreeID1
            // 
            this.colTreeID1.Caption = "Tree ID";
            this.colTreeID1.FieldName = "TreeID";
            this.colTreeID1.Name = "colTreeID1";
            this.colTreeID1.OptionsColumn.AllowEdit = false;
            this.colTreeID1.OptionsColumn.AllowFocus = false;
            this.colTreeID1.OptionsColumn.ReadOnly = true;
            this.colTreeID1.Width = 48;
            // 
            // colLocalityID2
            // 
            this.colLocalityID2.Caption = "Locality ID";
            this.colLocalityID2.FieldName = "LocalityID";
            this.colLocalityID2.Name = "colLocalityID2";
            this.colLocalityID2.OptionsColumn.AllowEdit = false;
            this.colLocalityID2.OptionsColumn.AllowFocus = false;
            this.colLocalityID2.OptionsColumn.ReadOnly = true;
            this.colLocalityID2.Width = 62;
            // 
            // colDistrictID2
            // 
            this.colDistrictID2.Caption = "District ID";
            this.colDistrictID2.FieldName = "DistrictID";
            this.colDistrictID2.Name = "colDistrictID2";
            this.colDistrictID2.OptionsColumn.AllowEdit = false;
            this.colDistrictID2.OptionsColumn.AllowFocus = false;
            this.colDistrictID2.OptionsColumn.ReadOnly = true;
            this.colDistrictID2.Width = 59;
            // 
            // colTreeReference1
            // 
            this.colTreeReference1.Caption = "Tree Reference";
            this.colTreeReference1.FieldName = "TreeReference";
            this.colTreeReference1.Name = "colTreeReference1";
            this.colTreeReference1.OptionsColumn.AllowEdit = false;
            this.colTreeReference1.OptionsColumn.AllowFocus = false;
            this.colTreeReference1.OptionsColumn.ReadOnly = true;
            this.colTreeReference1.Width = 97;
            // 
            // colTreeMappingID1
            // 
            this.colTreeMappingID1.Caption = "Map ID";
            this.colTreeMappingID1.FieldName = "TreeMappingID";
            this.colTreeMappingID1.Name = "colTreeMappingID1";
            this.colTreeMappingID1.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID1.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID1.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID1.Width = 46;
            // 
            // colInspectionReference1
            // 
            this.colInspectionReference1.Caption = "Inspection Reference";
            this.colInspectionReference1.FieldName = "InspectionReference";
            this.colInspectionReference1.Name = "colInspectionReference1";
            this.colInspectionReference1.OptionsColumn.AllowEdit = false;
            this.colInspectionReference1.OptionsColumn.AllowFocus = false;
            this.colInspectionReference1.OptionsColumn.ReadOnly = true;
            this.colInspectionReference1.Visible = true;
            this.colInspectionReference1.VisibleIndex = 0;
            this.colInspectionReference1.Width = 125;
            // 
            // colInspectionDate1
            // 
            this.colInspectionDate1.Caption = "Inspection Date";
            this.colInspectionDate1.FieldName = "InspectionDate";
            this.colInspectionDate1.Name = "colInspectionDate1";
            this.colInspectionDate1.OptionsColumn.AllowEdit = false;
            this.colInspectionDate1.OptionsColumn.AllowFocus = false;
            this.colInspectionDate1.OptionsColumn.ReadOnly = true;
            this.colInspectionDate1.Width = 98;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            this.colActionID.Width = 56;
            // 
            // colActionJobNumber
            // 
            this.colActionJobNumber.Caption = "Job Number";
            this.colActionJobNumber.FieldName = "ActionJobNumber";
            this.colActionJobNumber.Name = "colActionJobNumber";
            this.colActionJobNumber.OptionsColumn.AllowEdit = false;
            this.colActionJobNumber.OptionsColumn.AllowFocus = false;
            this.colActionJobNumber.OptionsColumn.ReadOnly = true;
            this.colActionJobNumber.Visible = true;
            this.colActionJobNumber.VisibleIndex = 1;
            this.colActionJobNumber.Width = 106;
            // 
            // colAction
            // 
            this.colAction.Caption = "Action";
            this.colAction.FieldName = "Action";
            this.colAction.Name = "colAction";
            this.colAction.OptionsColumn.AllowEdit = false;
            this.colAction.OptionsColumn.AllowFocus = false;
            this.colAction.OptionsColumn.ReadOnly = true;
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 2;
            this.colAction.Width = 114;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 3;
            this.colActionPriority.Width = 119;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Due Date";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 4;
            this.colActionDueDate.Width = 88;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Done Date";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 5;
            this.colActionDoneDate.Width = 79;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action By";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            this.colActionBy.Visible = true;
            this.colActionBy.VisibleIndex = 8;
            this.colActionBy.Width = 126;
            // 
            // colActionSupervisor
            // 
            this.colActionSupervisor.Caption = "Supervisor";
            this.colActionSupervisor.FieldName = "ActionSupervisor";
            this.colActionSupervisor.Name = "colActionSupervisor";
            this.colActionSupervisor.OptionsColumn.AllowEdit = false;
            this.colActionSupervisor.OptionsColumn.AllowFocus = false;
            this.colActionSupervisor.OptionsColumn.ReadOnly = true;
            this.colActionSupervisor.Visible = true;
            this.colActionSupervisor.VisibleIndex = 9;
            this.colActionSupervisor.Width = 125;
            // 
            // colActionOwnership
            // 
            this.colActionOwnership.Caption = "Ownership";
            this.colActionOwnership.FieldName = "ActionOwnership";
            this.colActionOwnership.Name = "colActionOwnership";
            this.colActionOwnership.OptionsColumn.AllowEdit = false;
            this.colActionOwnership.OptionsColumn.AllowFocus = false;
            this.colActionOwnership.OptionsColumn.ReadOnly = true;
            this.colActionOwnership.Visible = true;
            this.colActionOwnership.VisibleIndex = 10;
            this.colActionOwnership.Width = 118;
            // 
            // colActionCostCentre
            // 
            this.colActionCostCentre.Caption = "Cost Centre";
            this.colActionCostCentre.FieldName = "ActionCostCentre";
            this.colActionCostCentre.Name = "colActionCostCentre";
            this.colActionCostCentre.OptionsColumn.AllowEdit = false;
            this.colActionCostCentre.OptionsColumn.AllowFocus = false;
            this.colActionCostCentre.OptionsColumn.ReadOnly = true;
            this.colActionCostCentre.Visible = true;
            this.colActionCostCentre.VisibleIndex = 11;
            this.colActionCostCentre.Width = 124;
            // 
            // colActionBudget
            // 
            this.colActionBudget.Caption = "Budget";
            this.colActionBudget.FieldName = "ActionBudget";
            this.colActionBudget.Name = "colActionBudget";
            this.colActionBudget.OptionsColumn.AllowEdit = false;
            this.colActionBudget.OptionsColumn.AllowFocus = false;
            this.colActionBudget.OptionsColumn.ReadOnly = true;
            this.colActionBudget.Visible = true;
            this.colActionBudget.VisibleIndex = 12;
            this.colActionBudget.Width = 111;
            // 
            // colActionWorkUnits
            // 
            this.colActionWorkUnits.Caption = "Work Units";
            this.colActionWorkUnits.FieldName = "ActionWorkUnits";
            this.colActionWorkUnits.Name = "colActionWorkUnits";
            this.colActionWorkUnits.OptionsColumn.AllowEdit = false;
            this.colActionWorkUnits.OptionsColumn.AllowFocus = false;
            this.colActionWorkUnits.OptionsColumn.ReadOnly = true;
            this.colActionWorkUnits.Visible = true;
            this.colActionWorkUnits.VisibleIndex = 13;
            this.colActionWorkUnits.Width = 74;
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Schedule Of Rates";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 14;
            this.colActionScheduleOfRates.Width = 111;
            // 
            // colActionBudgetedRateDescription
            // 
            this.colActionBudgetedRateDescription.Caption = "Budgeted Rate Desc";
            this.colActionBudgetedRateDescription.FieldName = "ActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.Name = "colActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRateDescription.Visible = true;
            this.colActionBudgetedRateDescription.VisibleIndex = 15;
            this.colActionBudgetedRateDescription.Width = 165;
            // 
            // colActionBudgetedRate
            // 
            this.colActionBudgetedRate.Caption = "Budgeted Rate";
            this.colActionBudgetedRate.FieldName = "ActionBudgetedRate";
            this.colActionBudgetedRate.Name = "colActionBudgetedRate";
            this.colActionBudgetedRate.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRate.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRate.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRate.Visible = true;
            this.colActionBudgetedRate.VisibleIndex = 16;
            this.colActionBudgetedRate.Width = 94;
            // 
            // colActionBudgetedCost
            // 
            this.colActionBudgetedCost.Caption = "Budgeted Cost";
            this.colActionBudgetedCost.FieldName = "ActionBudgetedCost";
            this.colActionBudgetedCost.Name = "colActionBudgetedCost";
            this.colActionBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedCost.Visible = true;
            this.colActionBudgetedCost.VisibleIndex = 17;
            this.colActionBudgetedCost.Width = 93;
            // 
            // colActionActualRateDescription
            // 
            this.colActionActualRateDescription.Caption = "Actual Rate Desc";
            this.colActionActualRateDescription.FieldName = "ActionActualRateDescription";
            this.colActionActualRateDescription.Name = "colActionActualRateDescription";
            this.colActionActualRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionActualRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionActualRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionActualRateDescription.Visible = true;
            this.colActionActualRateDescription.VisibleIndex = 18;
            this.colActionActualRateDescription.Width = 148;
            // 
            // colActionActualRate
            // 
            this.colActionActualRate.Caption = "Actual Rate";
            this.colActionActualRate.FieldName = "ActionActualRate";
            this.colActionActualRate.Name = "colActionActualRate";
            this.colActionActualRate.OptionsColumn.AllowEdit = false;
            this.colActionActualRate.OptionsColumn.AllowFocus = false;
            this.colActionActualRate.OptionsColumn.ReadOnly = true;
            this.colActionActualRate.Visible = true;
            this.colActionActualRate.VisibleIndex = 19;
            this.colActionActualRate.Width = 78;
            // 
            // colActionActualCost
            // 
            this.colActionActualCost.Caption = "Actual Cost";
            this.colActionActualCost.FieldName = "ActionActualCost";
            this.colActionActualCost.Name = "colActionActualCost";
            this.colActionActualCost.OptionsColumn.AllowEdit = false;
            this.colActionActualCost.OptionsColumn.AllowFocus = false;
            this.colActionActualCost.OptionsColumn.ReadOnly = true;
            this.colActionActualCost.Visible = true;
            this.colActionActualCost.VisibleIndex = 20;
            this.colActionActualCost.Width = 77;
            // 
            // colActionWorkOrder
            // 
            this.colActionWorkOrder.FieldName = "ActionWorkOrder";
            this.colActionWorkOrder.Name = "colActionWorkOrder";
            this.colActionWorkOrder.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrder.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrder.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrder.Visible = true;
            this.colActionWorkOrder.VisibleIndex = 22;
            this.colActionWorkOrder.Width = 111;
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.Caption = "Remarks";
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 23;
            this.colActionRemarks.Width = 63;
            // 
            // colActionUserDefined1
            // 
            this.colActionUserDefined1.Caption = "User 1";
            this.colActionUserDefined1.FieldName = "ActionUserDefined1";
            this.colActionUserDefined1.Name = "colActionUserDefined1";
            this.colActionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined1.Visible = true;
            this.colActionUserDefined1.VisibleIndex = 24;
            this.colActionUserDefined1.Width = 53;
            // 
            // colActionUserDefined2
            // 
            this.colActionUserDefined2.Caption = "User 2";
            this.colActionUserDefined2.FieldName = "ActionUserDefined2";
            this.colActionUserDefined2.Name = "colActionUserDefined2";
            this.colActionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined2.Visible = true;
            this.colActionUserDefined2.VisibleIndex = 25;
            this.colActionUserDefined2.Width = 53;
            // 
            // colActionUserDefined3
            // 
            this.colActionUserDefined3.Caption = "User 3";
            this.colActionUserDefined3.FieldName = "ActionUserDefined3";
            this.colActionUserDefined3.Name = "colActionUserDefined3";
            this.colActionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined3.Visible = true;
            this.colActionUserDefined3.VisibleIndex = 26;
            this.colActionUserDefined3.Width = 53;
            // 
            // colActionCostDifference
            // 
            this.colActionCostDifference.Caption = "Cost Difference";
            this.colActionCostDifference.FieldName = "ActionCostDifference";
            this.colActionCostDifference.Name = "colActionCostDifference";
            this.colActionCostDifference.OptionsColumn.AllowEdit = false;
            this.colActionCostDifference.OptionsColumn.AllowFocus = false;
            this.colActionCostDifference.OptionsColumn.ReadOnly = true;
            this.colActionCostDifference.Visible = true;
            this.colActionCostDifference.VisibleIndex = 21;
            this.colActionCostDifference.Width = 97;
            // 
            // colActionCompletionStatus
            // 
            this.colActionCompletionStatus.Caption = "Completion Status";
            this.colActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.colActionCompletionStatus.Name = "colActionCompletionStatus";
            this.colActionCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colActionCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colActionCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colActionCompletionStatus.Visible = true;
            this.colActionCompletionStatus.VisibleIndex = 7;
            this.colActionCompletionStatus.Width = 109;
            // 
            // colActionTimeliness
            // 
            this.colActionTimeliness.Caption = "Timeliness";
            this.colActionTimeliness.FieldName = "ActionTimeliness";
            this.colActionTimeliness.Name = "colActionTimeliness";
            this.colActionTimeliness.OptionsColumn.AllowEdit = false;
            this.colActionTimeliness.OptionsColumn.AllowFocus = false;
            this.colActionTimeliness.OptionsColumn.ReadOnly = true;
            this.colActionTimeliness.Visible = true;
            this.colActionTimeliness.VisibleIndex = 6;
            this.colActionTimeliness.Width = 70;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gridLookUpEditCarriedOutBy);
            this.groupControl3.Controls.Add(this.checkEdit6);
            this.groupControl3.Controls.Add(this.checkEdit5);
            this.groupControl3.Location = new System.Drawing.Point(7, 105);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(610, 50);
            this.groupControl3.TabIndex = 10;
            this.groupControl3.Text = "Carried Out By Used:";
            // 
            // gridLookUpEditCarriedOutBy
            // 
            this.gridLookUpEditCarriedOutBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditCarriedOutBy.Location = new System.Drawing.Point(274, 25);
            this.gridLookUpEditCarriedOutBy.MenuManager = this.barManager1;
            this.gridLookUpEditCarriedOutBy.Name = "gridLookUpEditCarriedOutBy";
            this.gridLookUpEditCarriedOutBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditCarriedOutBy.Properties.DataSource = this.sp00190ContractorListWithBlankBindingSource;
            this.gridLookUpEditCarriedOutBy.Properties.DisplayMember = "ContractorName";
            this.gridLookUpEditCarriedOutBy.Properties.NullText = "";
            this.gridLookUpEditCarriedOutBy.Properties.ValueMember = "ContractorID";
            this.gridLookUpEditCarriedOutBy.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEditCarriedOutBy.Size = new System.Drawing.Size(331, 20);
            this.gridLookUpEditCarriedOutBy.TabIndex = 2;
            this.gridLookUpEditCarriedOutBy.Validating += new System.ComponentModel.CancelEventHandler(this.gridLookUpEditCarriedOutBy_Validating);
            // 
            // sp00190ContractorListWithBlankBindingSource
            // 
            this.sp00190ContractorListWithBlankBindingSource.DataMember = "sp00190_Contractor_List_With_Blank";
            this.sp00190ContractorListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colContractorCode,
            this.colContractorID,
            this.colContractorName,
            this.colDisabled,
            this.colEmailPassword,
            this.colInternalContractor,
            this.colInternalCOntractorDescription,
            this.colMobile,
            this.colPostcode,
            this.colRemarks,
            this.colTelephone1,
            this.colTelephone2,
            this.colTypeDescription,
            this.colTypeID,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colVatReg,
            this.colWebsite});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colContractorID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInternalCOntractorDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 273;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor ID";
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 118;
            // 
            // colInternalCOntractorDescription
            // 
            this.colInternalCOntractorDescription.Caption = "Status";
            this.colInternalCOntractorDescription.FieldName = "InternalCOntractorDescription";
            this.colInternalCOntractorDescription.Name = "colInternalCOntractorDescription";
            this.colInternalCOntractorDescription.OptionsColumn.AllowEdit = false;
            this.colInternalCOntractorDescription.OptionsColumn.AllowFocus = false;
            this.colInternalCOntractorDescription.OptionsColumn.ReadOnly = true;
            this.colInternalCOntractorDescription.Width = 52;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Width = 51;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Width = 62;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 1;
            this.colTypeDescription.Width = 111;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.Width = 49;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Reg";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 62;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // checkEdit6
            // 
            this.checkEdit6.EditValue = true;
            this.checkEdit6.Location = new System.Drawing.Point(148, 28);
            this.checkEdit6.MenuManager = this.barManager1;
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "Specified Person";
            this.checkEdit6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit6.Properties.RadioGroupIndex = 3;
            this.checkEdit6.Size = new System.Drawing.Size(107, 19);
            this.checkEdit6.TabIndex = 1;
            this.checkEdit6.CheckedChanged += new System.EventHandler(this.checkEdit6_CheckedChanged);
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(5, 28);
            this.checkEdit5.MenuManager = this.barManager1;
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "Action\'s Carried Out By";
            this.checkEdit5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit5.Properties.RadioGroupIndex = 3;
            this.checkEdit5.Size = new System.Drawing.Size(136, 19);
            this.checkEdit5.TabIndex = 0;
            this.checkEdit5.TabStop = false;
            this.checkEdit5.CheckedChanged += new System.EventHandler(this.checkEdit5_CheckedChanged);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.checkEdit4);
            this.groupControl2.Controls.Add(this.checkEdit3);
            this.groupControl2.Controls.Add(this.dateEditDueDate);
            this.groupControl2.Location = new System.Drawing.Point(7, 41);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(610, 50);
            this.groupControl2.TabIndex = 9;
            this.groupControl2.Text = "Due Date Used:";
            // 
            // checkEdit4
            // 
            this.checkEdit4.EditValue = true;
            this.checkEdit4.Location = new System.Drawing.Point(148, 28);
            this.checkEdit4.MenuManager = this.barManager1;
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "Specified Date";
            this.checkEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit4.Properties.RadioGroupIndex = 2;
            this.checkEdit4.Size = new System.Drawing.Size(96, 19);
            this.checkEdit4.TabIndex = 1;
            this.checkEdit4.CheckedChanged += new System.EventHandler(this.checkEdit4_CheckedChanged);
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(5, 28);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Action\'s Date";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 2;
            this.checkEdit3.Size = new System.Drawing.Size(107, 19);
            this.checkEdit3.TabIndex = 0;
            this.checkEdit3.TabStop = false;
            this.checkEdit3.CheckedChanged += new System.EventHandler(this.checkEdit3_CheckedChanged);
            // 
            // dateEditDueDate
            // 
            this.dateEditDueDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditDueDate.EditValue = null;
            this.dateEditDueDate.Location = new System.Drawing.Point(274, 25);
            this.dateEditDueDate.MenuManager = this.barManager1;
            this.dateEditDueDate.Name = "dateEditDueDate";
            this.dateEditDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDueDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditDueDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditDueDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDueDate.Size = new System.Drawing.Size(331, 20);
            this.dateEditDueDate.TabIndex = 5;
            this.dateEditDueDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEditDueDate_Validating);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnGetActions);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Location = new System.Drawing.Point(7, 169);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(610, 50);
            this.groupControl1.TabIndex = 8;
            this.groupControl1.Text = "Get Actions to be Pasted from:";
            // 
            // btnGetActions
            // 
            this.btnGetActions.Enabled = false;
            this.btnGetActions.Location = new System.Drawing.Point(274, 24);
            this.btnGetActions.Name = "btnGetActions";
            this.btnGetActions.Size = new System.Drawing.Size(75, 23);
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Get Actions - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to open the Select Inspection(s) screen from which you can select multip" +
    "le inspections for adding to the current inspection.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnGetActions.SuperTip = superToolTip1;
            this.btnGetActions.TabIndex = 8;
            this.btnGetActions.Text = "Get Actions";
            this.btnGetActions.Visible = false;
            this.btnGetActions.Click += new System.EventHandler(this.btnGetActions_Click);
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(5, 28);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Actions in Memory";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(107, 19);
            this.checkEdit1.TabIndex = 6;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(148, 28);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Actions From List";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(107, 19);
            this.checkEdit2.TabIndex = 7;
            this.checkEdit2.TabStop = false;
            this.checkEdit2.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // buttonEditSequence
            // 
            this.buttonEditSequence.Location = new System.Drawing.Point(117, 7);
            this.buttonEditSequence.MenuManager = this.barManager1;
            this.buttonEditSequence.Name = "buttonEditSequence";
            this.buttonEditSequence.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Sequence", null, true)});
            this.buttonEditSequence.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSequence.Size = new System.Drawing.Size(500, 20);
            this.buttonEditSequence.StyleController = this.layoutControl1;
            this.buttonEditSequence.TabIndex = 4;
            this.buttonEditSequence.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSequence_ButtonClick);
            this.buttonEditSequence.Validating += new System.ComponentModel.CancelEventHandler(this.buttonEditSequence_Validating);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(624, 559);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AllowHide = false;
            this.layoutControlItem1.Control = this.buttonEditSequence;
            this.layoutControlItem1.CustomizationFormText = "Sequence:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(614, 24);
            this.layoutControlItem1.Text = "Inspection Reference:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 152);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(614, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AllowHide = false;
            this.layoutControlItem5.Control = this.groupControl1;
            this.layoutControlItem5.CustomizationFormText = "Get Actions From:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 162);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 54);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(104, 54);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(614, 54);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Get Actions From:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AllowHide = false;
            this.layoutControlItem3.Control = this.groupControl2;
            this.layoutControlItem3.CustomizationFormText = "Due Date:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 54);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(104, 54);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(614, 54);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Due Date:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHide = false;
            this.layoutControlItem2.Control = this.groupControl3;
            this.layoutControlItem2.CustomizationFormText = "Action Performed By:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 54);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(104, 54);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(614, 54);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Action Performed By:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 88);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(614, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(614, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 216);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(614, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AllowHide = false;
            this.layoutControlItem4.Control = this.groupControl4;
            this.layoutControlItem4.CustomizationFormText = "Actions Grid:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 226);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(614, 323);
            this.layoutControlItem4.Text = "Actions Grid:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(460, 589);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(541, 589);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp01380_AT_Actions_Linked_To_InspectionsTableAdapter
            // 
            this.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // sp00190_Contractor_List_With_BlankTableAdapter
            // 
            this.sp00190_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AT_Inspection_Edit_Paste_Actions
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(625, 618);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Inspection_Edit_Paste_Actions";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paste Copied Actions";
            this.Load += new System.EventHandler(this.frm_AT_Inspection_Edit_Paste_Actions_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01380ATActionsLinkedToInspectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCarriedOutBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDueDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSequence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.DateEdit dateEditDueDate;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSequence;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp01380ATActionsLinkedToInspectionsBindingSource;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colActionOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudget;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostDifference;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTimeliness;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter sp01380_AT_Actions_Linked_To_InspectionsTableAdapter;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.SimpleButton btnGetActions;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditCarriedOutBy;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00190ContractorListWithBlankBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter sp00190_Contractor_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalCOntractorDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
    }
}

namespace WoodPlan5
{
    partial class frm_Mail_Merge_Select_Merge_Layout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colintHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00233MailMergeManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Utilities = new WoodPlan5.DataSet_Utilities();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReportLayoutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.CompanyHeaderGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrSlogan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CompanyHeaderLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter();
            this.sp00233_Mail_Merge_ManagerTableAdapter = new WoodPlan5.DataSet_UtilitiesTableAdapters.sp00233_Mail_Merge_ManagerTableAdapter();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00233MailMergeManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Utilities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyHeaderGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00232CompanyHeaderDropDownListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(622, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 314);
            this.barDockControlBottom.Size = new System.Drawing.Size(622, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 288);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(622, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 288);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colintHeaderID
            // 
            this.colintHeaderID.Caption = "Header ID";
            this.colintHeaderID.FieldName = "intHeaderID";
            this.colintHeaderID.Name = "colintHeaderID";
            this.colintHeaderID.OptionsColumn.AllowEdit = false;
            this.colintHeaderID.OptionsColumn.AllowFocus = false;
            this.colintHeaderID.OptionsColumn.ReadOnly = true;
            this.colintHeaderID.Width = 70;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00233MailMergeManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(622, 253);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00233MailMergeManagerBindingSource
            // 
            this.sp00233MailMergeManagerBindingSource.DataMember = "sp00233_Mail_Merge_Manager";
            this.sp00233MailMergeManagerBindingSource.DataSource = this.dataSet_Utilities;
            // 
            // dataSet_Utilities
            // 
            this.dataSet_Utilities.DataSetName = "DataSet_Utilities";
            this.dataSet_Utilities.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReportLayoutID,
            this.colReportLayoutName,
            this.colReportTypeName,
            this.colModuleID,
            this.colCreatedBy,
            this.colReportLayoutTypeID});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReportLayoutName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colReportLayoutID
            // 
            this.colReportLayoutID.Caption = "Report Layout ID";
            this.colReportLayoutID.FieldName = "ReportLayoutID";
            this.colReportLayoutID.Name = "colReportLayoutID";
            this.colReportLayoutID.OptionsColumn.AllowEdit = false;
            this.colReportLayoutID.OptionsColumn.AllowFocus = false;
            this.colReportLayoutID.OptionsColumn.ReadOnly = true;
            this.colReportLayoutID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportLayoutName.Visible = true;
            this.colReportLayoutName.VisibleIndex = 0;
            this.colReportLayoutName.Width = 334;
            // 
            // colReportTypeName
            // 
            this.colReportTypeName.Caption = "Layout Type";
            this.colReportTypeName.FieldName = "ReportTypeName";
            this.colReportTypeName.Name = "colReportTypeName";
            this.colReportTypeName.OptionsColumn.AllowEdit = false;
            this.colReportTypeName.OptionsColumn.AllowFocus = false;
            this.colReportTypeName.OptionsColumn.ReadOnly = true;
            this.colReportTypeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportTypeName.Visible = true;
            this.colReportTypeName.VisibleIndex = 1;
            this.colReportTypeName.Width = 134;
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.AllowEdit = false;
            this.colModuleID.OptionsColumn.AllowFocus = false;
            this.colModuleID.OptionsColumn.ReadOnly = true;
            this.colModuleID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.AllowFocus = false;
            this.colCreatedBy.OptionsColumn.ReadOnly = true;
            this.colCreatedBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 2;
            this.colCreatedBy.Width = 118;
            // 
            // colReportLayoutTypeID
            // 
            this.colReportLayoutTypeID.Caption = "Report Type ID";
            this.colReportLayoutTypeID.FieldName = "ReportLayoutTypeID";
            this.colReportLayoutTypeID.Name = "colReportLayoutTypeID";
            this.colReportLayoutTypeID.OptionsColumn.AllowEdit = false;
            this.colReportLayoutTypeID.OptionsColumn.AllowFocus = false;
            this.colReportLayoutTypeID.OptionsColumn.ReadOnly = true;
            this.colReportLayoutTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(462, 287);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(543, 287);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // CompanyHeaderGridLookUpEdit
            // 
            this.CompanyHeaderGridLookUpEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CompanyHeaderGridLookUpEdit.Enabled = false;
            this.CompanyHeaderGridLookUpEdit.Location = new System.Drawing.Point(93, 288);
            this.CompanyHeaderGridLookUpEdit.MenuManager = this.barManager1;
            this.CompanyHeaderGridLookUpEdit.Name = "CompanyHeaderGridLookUpEdit";
            this.CompanyHeaderGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CompanyHeaderGridLookUpEdit.Properties.DataSource = this.sp00232CompanyHeaderDropDownListWithBlankBindingSource;
            this.CompanyHeaderGridLookUpEdit.Properties.DisplayMember = "strDescription";
            this.CompanyHeaderGridLookUpEdit.Properties.NullText = "";
            this.CompanyHeaderGridLookUpEdit.Properties.ValueMember = "intHeaderID";
            this.CompanyHeaderGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.CompanyHeaderGridLookUpEdit.Size = new System.Drawing.Size(363, 20);
            this.CompanyHeaderGridLookUpEdit.TabIndex = 7;
            this.CompanyHeaderGridLookUpEdit.Visible = false;
            // 
            // sp00232CompanyHeaderDropDownListWithBlankBindingSource
            // 
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource.DataMember = "sp00232_Company_Header_Drop_Down_List_With_Blank";
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colintHeaderID,
            this.colstrAddressLine1,
            this.colstrCompanyName,
            this.colstrDescription,
            this.colstrTelephone1,
            this.colstrSlogan});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colintHeaderID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrCompanyName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colstrAddressLine1
            // 
            this.colstrAddressLine1.Caption = "Address Line 1";
            this.colstrAddressLine1.FieldName = "strAddressLine1";
            this.colstrAddressLine1.Name = "colstrAddressLine1";
            this.colstrAddressLine1.OptionsColumn.AllowEdit = false;
            this.colstrAddressLine1.OptionsColumn.AllowFocus = false;
            this.colstrAddressLine1.OptionsColumn.ReadOnly = true;
            this.colstrAddressLine1.Visible = true;
            this.colstrAddressLine1.VisibleIndex = 2;
            this.colstrAddressLine1.Width = 133;
            // 
            // colstrCompanyName
            // 
            this.colstrCompanyName.Caption = "Company Name";
            this.colstrCompanyName.FieldName = "strCompanyName";
            this.colstrCompanyName.Name = "colstrCompanyName";
            this.colstrCompanyName.OptionsColumn.AllowEdit = false;
            this.colstrCompanyName.OptionsColumn.AllowFocus = false;
            this.colstrCompanyName.OptionsColumn.ReadOnly = true;
            this.colstrCompanyName.Visible = true;
            this.colstrCompanyName.VisibleIndex = 1;
            this.colstrCompanyName.Width = 211;
            // 
            // colstrDescription
            // 
            this.colstrDescription.Caption = "Header Description";
            this.colstrDescription.FieldName = "strDescription";
            this.colstrDescription.Name = "colstrDescription";
            this.colstrDescription.OptionsColumn.AllowEdit = false;
            this.colstrDescription.OptionsColumn.AllowFocus = false;
            this.colstrDescription.OptionsColumn.ReadOnly = true;
            this.colstrDescription.Visible = true;
            this.colstrDescription.VisibleIndex = 0;
            this.colstrDescription.Width = 255;
            // 
            // colstrTelephone1
            // 
            this.colstrTelephone1.Caption = "Telephone 1";
            this.colstrTelephone1.FieldName = "strTelephone1";
            this.colstrTelephone1.Name = "colstrTelephone1";
            this.colstrTelephone1.OptionsColumn.AllowEdit = false;
            this.colstrTelephone1.OptionsColumn.AllowFocus = false;
            this.colstrTelephone1.OptionsColumn.ReadOnly = true;
            this.colstrTelephone1.Visible = true;
            this.colstrTelephone1.VisibleIndex = 3;
            this.colstrTelephone1.Width = 108;
            // 
            // colstrSlogan
            // 
            this.colstrSlogan.Caption = "Slogan";
            this.colstrSlogan.FieldName = "strSlogan";
            this.colstrSlogan.Name = "colstrSlogan";
            this.colstrSlogan.OptionsColumn.AllowEdit = false;
            this.colstrSlogan.OptionsColumn.AllowFocus = false;
            this.colstrSlogan.OptionsColumn.ReadOnly = true;
            this.colstrSlogan.Width = 53;
            // 
            // CompanyHeaderLabelControl
            // 
            this.CompanyHeaderLabelControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CompanyHeaderLabelControl.Enabled = false;
            this.CompanyHeaderLabelControl.Location = new System.Drawing.Point(0, 291);
            this.CompanyHeaderLabelControl.Name = "CompanyHeaderLabelControl";
            this.CompanyHeaderLabelControl.Size = new System.Drawing.Size(87, 13);
            this.CompanyHeaderLabelControl.TabIndex = 8;
            this.CompanyHeaderLabelControl.Text = "Company Header:";
            this.CompanyHeaderLabelControl.Visible = false;
            // 
            // sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter
            // 
            this.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00233_Mail_Merge_ManagerTableAdapter
            // 
            this.sp00233_Mail_Merge_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 29);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(622, 253);
            this.gridSplitContainer1.TabIndex = 9;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_Mail_Merge_Select_Merge_Layout
            // 
            this.AcceptButton = this.btnOK;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(622, 314);
            this.Controls.Add(this.CompanyHeaderLabelControl);
            this.Controls.Add(this.CompanyHeaderGridLookUpEdit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gridSplitContainer1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_Mail_Merge_Select_Merge_Layout";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mail Merge - Select Layout";
            this.Load += new System.EventHandler(this.frm_Mail_Merge_Select_Merge_Layout_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.CompanyHeaderGridLookUpEdit, 0);
            this.Controls.SetChildIndex(this.CompanyHeaderLabelControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00233MailMergeManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Utilities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyHeaderGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00232CompanyHeaderDropDownListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_Utilities dataSet_Utilities;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.GridLookUpEdit CompanyHeaderGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl CompanyHeaderLabelControl;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private System.Windows.Forms.BindingSource sp00232CompanyHeaderDropDownListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colintHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colstrDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrSlogan;
        private System.Windows.Forms.BindingSource sp00233MailMergeManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private WoodPlan5.DataSet_UtilitiesTableAdapters.sp00233_Mail_Merge_ManagerTableAdapter sp00233_Mail_Merge_ManagerTableAdapter;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

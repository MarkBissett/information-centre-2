namespace WoodPlan5
{
    partial class frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors));
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnClearMissingPhotoLinks = new DevExpress.XtraEditors.SimpleButton();
            this.memoEditMissingPhotos = new DevExpress.XtraEditors.MemoEdit();
            this.labelControlError1 = new DevExpress.XtraEditors.LabelControl();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.labelControlTips = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControlError3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlError2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditMissingPhotos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(132, 355);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "Cancel";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnClearMissingPhotoLinks
            // 
            this.btnClearMissingPhotoLinks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearMissingPhotoLinks.Location = new System.Drawing.Point(226, 355);
            this.btnClearMissingPhotoLinks.Name = "btnClearMissingPhotoLinks";
            this.btnClearMissingPhotoLinks.Size = new System.Drawing.Size(135, 23);
            this.btnClearMissingPhotoLinks.TabIndex = 5;
            this.btnClearMissingPhotoLinks.Text = "Clear Missing Photo Links";
            this.btnClearMissingPhotoLinks.Click += new System.EventHandler(this.btnClearMissingPhotoLinks_Click);
            // 
            // memoEditMissingPhotos
            // 
            this.memoEditMissingPhotos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditMissingPhotos.EditValue = "All photo links are valid.";
            this.memoEditMissingPhotos.Location = new System.Drawing.Point(3, 69);
            this.memoEditMissingPhotos.MenuManager = this.barManager1;
            this.memoEditMissingPhotos.Name = "memoEditMissingPhotos";
            this.memoEditMissingPhotos.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.memoEditMissingPhotos, true);
            this.memoEditMissingPhotos.Size = new System.Drawing.Size(505, 147);
            this.scSpellChecker.SetSpellCheckerOptions(this.memoEditMissingPhotos, optionsSpelling2);
            this.memoEditMissingPhotos.TabIndex = 8;
            // 
            // labelControlError1
            // 
            this.labelControlError1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlError1.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlError1.Appearance.ImageIndex = 0;
            this.labelControlError1.Appearance.ImageList = this.imageList1;
            this.labelControlError1.Appearance.Options.UseTextOptions = true;
            this.labelControlError1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.labelControlError1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlError1.Location = new System.Drawing.Point(3, 6);
            this.labelControlError1.Name = "labelControlError1";
            this.labelControlError1.Size = new System.Drawing.Size(482, 14);
            this.labelControlError1.TabIndex = 9;
            this.labelControlError1.Text = "        Error Message 1";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "ok_16.png");
            this.imageList1.Images.SetKeyName(1, "cancel_16.png");
            // 
            // labelControlTips
            // 
            this.labelControlTips.AllowHtmlString = true;
            this.labelControlTips.Appearance.Options.UseTextOptions = true;
            this.labelControlTips.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControlTips.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControlTips.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControlTips.Location = new System.Drawing.Point(0, 0);
            this.labelControlTips.Name = "labelControlTips";
            this.labelControlTips.Size = new System.Drawing.Size(507, 70);
            this.labelControlTips.TabIndex = 13;
            this.labelControlTips.Text = resources.GetString("labelControlTips.Text");
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(1, 1);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControlError3);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControlError2);
            this.splitContainerControl1.Panel1.Controls.Add(this.memoEditMissingPhotos);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControlError1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Check Data Results";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.labelControlTips);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Tips";
            this.splitContainerControl1.Size = new System.Drawing.Size(515, 348);
            this.splitContainerControl1.SplitterPosition = 242;
            this.splitContainerControl1.TabIndex = 15;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // labelControlError3
            // 
            this.labelControlError3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlError3.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlError3.Appearance.ImageIndex = 0;
            this.labelControlError3.Appearance.ImageList = this.imageList1;
            this.labelControlError3.Appearance.Options.UseTextOptions = true;
            this.labelControlError3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.labelControlError3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlError3.Location = new System.Drawing.Point(3, 50);
            this.labelControlError3.Name = "labelControlError3";
            this.labelControlError3.Size = new System.Drawing.Size(482, 14);
            this.labelControlError3.TabIndex = 16;
            this.labelControlError3.Text = "        Photo Links:";
            // 
            // labelControlError2
            // 
            this.labelControlError2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlError2.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlError2.Appearance.ImageIndex = 0;
            this.labelControlError2.Appearance.ImageList = this.imageList1;
            this.labelControlError2.Appearance.Options.UseTextOptions = true;
            this.labelControlError2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.labelControlError2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlError2.Location = new System.Drawing.Point(3, 28);
            this.labelControlError2.Name = "labelControlError2";
            this.labelControlError2.Size = new System.Drawing.Size(482, 14);
            this.labelControlError2.TabIndex = 15;
            this.labelControlError2.Text = "        Error Message 2";
            // 
            // frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors
            // 
            this.ClientSize = new System.Drawing.Size(518, 384);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.btnClearMissingPhotoLinks);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import GBM Mobile Survey - Check Data Errors";
            this.Load += new System.EventHandler(this.frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnClearMissingPhotoLinks, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditMissingPhotos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnClearMissingPhotoLinks;
        private DevExpress.XtraEditors.MemoEdit memoEditMissingPhotos;
        private DevExpress.XtraEditors.LabelControl labelControlError1;
        private DevExpress.XtraEditors.LabelControl labelControlTips;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.LabelControl labelControlError3;
        private DevExpress.XtraEditors.LabelControl labelControlError2;
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Block_Edit_Thematic_Styles : BaseObjects.frmBase
    {
        #region Instance Variables

        public int intCount = 0;
        public int? intPointSymbol = null;
        public decimal? decPointSize = null;
        public int? intPointColour = null;
        public int? intPolygonFillColour = null;
        public int? intPolygonLineColour = null;
        public int? intPolygonFillPattern = null;
        public int? intPolygonFillPatternColour = null;
        public int? intPolygonLineWidth = null;
        public int? intPolylineStyle = null;
        public int? intPolylineColour = null;
        public int? intPolylineWidth = null;
        public int? intShowInLegend = null;

        #endregion

        public frm_AT_Mapping_Block_Edit_Thematic_Styles()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Block_Edit_Thematic_Styles_Load(object sender, EventArgs e)
        {
            this.Text += " [" + Convert.ToString(intCount) + (intCount == 1 ? " Style Selected]" : " Styles Selected]");
            editPointSymbol.EditValue = null;
            editPointSize.EditValue = null;
            editPointColour.EditValue = null;
            editPolygonFillColour.EditValue = null;
            editPolygonLineColour.EditValue = null;
            editPolygonFillPattern.EditValue = null;
            editPolygonFillPatternColour.EditValue = null;
            editPolygonLineWidth.EditValue = null;
            editPolylineColour.EditValue = null;
            editPolylineStyle.EditValue = null;
            editPolylineWidth.EditValue = null;
            editShowInLegend.EditValue = null;
        }

        private void editPointSymbol_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPointSymbol.EditValue = null;
            }
        }

        private void editPointSize_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPointSize.EditValue = null;
            }
        }

        private void editPointColour_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPointColour.EditValue = null;
            }
        }

        private void editPolygonFillColour_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPolygonFillColour.EditValue = null;
            }
        }

        private void editPolygonLineColour_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPolygonLineColour.EditValue = null;
            }
        }

        private void editPolygonFillPattern_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPolygonFillPattern.EditValue = null;
            }
        }

        private void editPolygonFillPatternColour_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPolygonFillPatternColour.EditValue = null;
            }
        }

        private void editPolygonLineWidth_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPolygonLineWidth.EditValue = null;
            }
        }

        private void editPolylineStyle_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPolylineStyle.EditValue = null;
            }
        }

        private void editPolylineColour_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPolylineColour.EditValue = null;
            }
        }

        private void editPolylineWidth_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                editPolylineWidth.EditValue = null;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (editPointSymbol.EditValue != null) intPointSymbol = Convert.ToInt32(editPointSymbol.EditValue);
            if (editPointSize.EditValue != null) decPointSize = Convert.ToDecimal(editPointSize.EditValue);
            if (Convert.ToInt32(editPointColour.EditValue) != 0) intPointColour = Convert.ToInt32(editPointColour.EditValue);
            
            if (Convert.ToInt32(editPolygonFillColour.EditValue) != 0) intPolygonFillColour = Convert.ToInt32(editPolygonFillColour.EditValue);
            if (Convert.ToInt32(editPolygonLineColour.EditValue) != 0) intPolygonLineColour = Convert.ToInt32(editPolygonLineColour.EditValue);
            if (editPolygonFillPattern.EditValue != null) intPolygonFillPattern = Convert.ToInt32(editPolygonFillPattern.EditValue);
            if (Convert.ToInt32(editPolygonFillPatternColour.EditValue) != 0) intPolygonFillPatternColour = Convert.ToInt32(editPolygonFillPatternColour.EditValue);
            if (editPolygonLineWidth.EditValue != null) intPolygonLineWidth = Convert.ToInt32(editPolygonLineWidth.EditValue);

            if (editPolylineStyle.EditValue != null) intPolylineStyle = Convert.ToInt32(editPolylineStyle.EditValue);
            if (editPolylineWidth.EditValue != null) intPolylineWidth = Convert.ToInt32(editPolylineWidth.EditValue);
            if (Convert.ToInt32(editPolylineColour.EditValue) != 0) intPolylineColour = Convert.ToInt32(editPolylineColour.EditValue);

            if (editShowInLegend.EditValue != null) intShowInLegend = Convert.ToInt32(editShowInLegend.EditValue);

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

  
    
    }
}


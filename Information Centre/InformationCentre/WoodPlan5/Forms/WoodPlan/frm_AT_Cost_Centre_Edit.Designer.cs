namespace WoodPlan5
{
    partial class frm_AT_Cost_Centre_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Cost_Centre_Edit));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colOwnershipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBudgetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.sp01359ATCostCentreItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.intCostCentreIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strCostCentreCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intOwnershipIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01362ATOwnershipListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOwnershipCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intBudgetIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01361ATBudgetListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBudgetDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBudgetCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBudgetedSpent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualSpent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBudgetedRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForintCostCentreID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintBudgetID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintOwnershipID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrCostCentreCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01359_AT_CostCentre_ItemTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01359_AT_CostCentre_ItemTableAdapter();
            this.sp01361_AT_Budget_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01361_AT_Budget_List_With_BlankTableAdapter();
            this.sp01362_AT_Ownership_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01362_AT_Ownership_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp01359ATCostCentreItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCostCentreIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strCostCentreCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOwnershipIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01362ATOwnershipListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intBudgetIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01361ATBudgetListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCostCentreID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintBudgetID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOwnershipID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrCostCentreCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colOwnershipID
            // 
            this.colOwnershipID.Caption = "Ownership ID";
            this.colOwnershipID.FieldName = "OwnershipID";
            this.colOwnershipID.Name = "colOwnershipID";
            this.colOwnershipID.OptionsColumn.AllowEdit = false;
            this.colOwnershipID.OptionsColumn.AllowFocus = false;
            this.colOwnershipID.OptionsColumn.ReadOnly = true;
            this.colOwnershipID.Width = 86;
            // 
            // colBudgetID
            // 
            this.colBudgetID.Caption = "Budget ID";
            this.colBudgetID.FieldName = "BudgetID";
            this.colBudgetID.Name = "colBudgetID";
            this.colBudgetID.OptionsColumn.AllowEdit = false;
            this.colBudgetID.OptionsColumn.AllowFocus = false;
            this.colBudgetID.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intCostCentreIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strCostCentreCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intOwnershipIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intBudgetIDGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp01359ATCostCentreItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintCostCentreID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp01359ATCostCentreItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(106, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(197, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 10;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // sp01359ATCostCentreItemBindingSource
            // 
            this.sp01359ATCostCentreItemBindingSource.DataMember = "sp01359_AT_CostCentre_Item";
            this.sp01359ATCostCentreItemBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // intCostCentreIDTextEdit
            // 
            this.intCostCentreIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01359ATCostCentreItemBindingSource, "intCostCentreID", true));
            this.intCostCentreIDTextEdit.Location = new System.Drawing.Point(118, 12);
            this.intCostCentreIDTextEdit.MenuManager = this.barManager1;
            this.intCostCentreIDTextEdit.Name = "intCostCentreIDTextEdit";
            this.intCostCentreIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intCostCentreIDTextEdit, true);
            this.intCostCentreIDTextEdit.Size = new System.Drawing.Size(498, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intCostCentreIDTextEdit, optionsSpelling1);
            this.intCostCentreIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intCostCentreIDTextEdit.TabIndex = 4;
            // 
            // strCostCentreCodeTextEdit
            // 
            this.strCostCentreCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01359ATCostCentreItemBindingSource, "strCostCentreCode", true));
            this.strCostCentreCodeTextEdit.Location = new System.Drawing.Point(105, 83);
            this.strCostCentreCodeTextEdit.MenuManager = this.barManager1;
            this.strCostCentreCodeTextEdit.Name = "strCostCentreCodeTextEdit";
            this.strCostCentreCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strCostCentreCodeTextEdit, true);
            this.strCostCentreCodeTextEdit.Size = new System.Drawing.Size(511, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strCostCentreCodeTextEdit, optionsSpelling2);
            this.strCostCentreCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strCostCentreCodeTextEdit.TabIndex = 7;
            this.strCostCentreCodeTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strCostCentreCodeTextEdit_Validating);
            // 
            // intOwnershipIDGridLookUpEdit
            // 
            this.intOwnershipIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01359ATCostCentreItemBindingSource, "intOwnershipID", true));
            this.intOwnershipIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intOwnershipIDGridLookUpEdit.Location = new System.Drawing.Point(105, 59);
            this.intOwnershipIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intOwnershipIDGridLookUpEdit.Name = "intOwnershipIDGridLookUpEdit";
            this.intOwnershipIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.intOwnershipIDGridLookUpEdit.Properties.DataSource = this.sp01362ATOwnershipListWithBlankBindingSource;
            this.intOwnershipIDGridLookUpEdit.Properties.DisplayMember = "OwnershipName";
            this.intOwnershipIDGridLookUpEdit.Properties.NullText = "";
            this.intOwnershipIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.intOwnershipIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intOwnershipIDGridLookUpEdit.Properties.ValueMember = "OwnershipID";
            this.intOwnershipIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.intOwnershipIDGridLookUpEdit.Size = new System.Drawing.Size(511, 20);
            this.intOwnershipIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intOwnershipIDGridLookUpEdit.TabIndex = 6;
            this.intOwnershipIDGridLookUpEdit.TabStop = false;
            this.intOwnershipIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.intOwnershipIDGridLookUpEdit_Validating);
            // 
            // sp01362ATOwnershipListWithBlankBindingSource
            // 
            this.sp01362ATOwnershipListWithBlankBindingSource.DataMember = "sp01362_AT_Ownership_List_With_Blank";
            this.sp01362ATOwnershipListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOwnershipID,
            this.colOwnershipCode,
            this.colOwnershipName,
            this.colRemarks1,
            this.Calculated21,
            this.Calculated22,
            this.Calculated23});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colOwnershipID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOwnershipName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colOwnershipCode
            // 
            this.colOwnershipCode.Caption = "Ownership Code";
            this.colOwnershipCode.FieldName = "OwnershipCode";
            this.colOwnershipCode.Name = "colOwnershipCode";
            this.colOwnershipCode.OptionsColumn.AllowEdit = false;
            this.colOwnershipCode.OptionsColumn.AllowFocus = false;
            this.colOwnershipCode.OptionsColumn.ReadOnly = true;
            this.colOwnershipCode.Visible = true;
            this.colOwnershipCode.VisibleIndex = 0;
            this.colOwnershipCode.Width = 151;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.Caption = "Ownership Name";
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.OptionsColumn.AllowEdit = false;
            this.colOwnershipName.OptionsColumn.AllowFocus = false;
            this.colOwnershipName.OptionsColumn.ReadOnly = true;
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 1;
            this.colOwnershipName.Width = 265;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 2;
            this.colRemarks1.Width = 104;
            // 
            // Calculated21
            // 
            this.Calculated21.Caption = "<b>Calculated 1</b>";
            this.Calculated21.FieldName = "Calculated21";
            this.Calculated21.Name = "Calculated21";
            this.Calculated21.OptionsColumn.AllowEdit = false;
            this.Calculated21.OptionsColumn.AllowFocus = false;
            this.Calculated21.OptionsColumn.ReadOnly = true;
            this.Calculated21.ShowUnboundExpressionMenu = true;
            this.Calculated21.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated21.Visible = true;
            this.Calculated21.VisibleIndex = 3;
            this.Calculated21.Width = 95;
            // 
            // Calculated22
            // 
            this.Calculated22.Caption = "<b>Calculated 2</b>";
            this.Calculated22.FieldName = "Calculated22";
            this.Calculated22.Name = "Calculated22";
            this.Calculated22.OptionsColumn.AllowEdit = false;
            this.Calculated22.OptionsColumn.AllowFocus = false;
            this.Calculated22.OptionsColumn.ReadOnly = true;
            this.Calculated22.ShowUnboundExpressionMenu = true;
            this.Calculated22.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated22.Visible = true;
            this.Calculated22.VisibleIndex = 4;
            this.Calculated22.Width = 95;
            // 
            // Calculated23
            // 
            this.Calculated23.Caption = "<b>Calculated 3</b>";
            this.Calculated23.FieldName = "Calculated31";
            this.Calculated23.Name = "Calculated23";
            this.Calculated23.OptionsColumn.AllowEdit = false;
            this.Calculated23.OptionsColumn.AllowFocus = false;
            this.Calculated23.OptionsColumn.ReadOnly = true;
            this.Calculated23.ShowUnboundExpressionMenu = true;
            this.Calculated23.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated23.Visible = true;
            this.Calculated23.VisibleIndex = 5;
            this.Calculated23.Width = 95;
            // 
            // intBudgetIDGridLookUpEdit
            // 
            this.intBudgetIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01359ATCostCentreItemBindingSource, "intBudgetID", true));
            this.intBudgetIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intBudgetIDGridLookUpEdit.Location = new System.Drawing.Point(105, 35);
            this.intBudgetIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intBudgetIDGridLookUpEdit.Name = "intBudgetIDGridLookUpEdit";
            this.intBudgetIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.intBudgetIDGridLookUpEdit.Properties.DataSource = this.sp01361ATBudgetListWithBlankBindingSource;
            this.intBudgetIDGridLookUpEdit.Properties.DisplayMember = "BudgetDescription";
            this.intBudgetIDGridLookUpEdit.Properties.NullText = "";
            this.intBudgetIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.intBudgetIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intBudgetIDGridLookUpEdit.Properties.ValueMember = "BudgetID";
            this.intBudgetIDGridLookUpEdit.Properties.View = this.gridView1;
            this.intBudgetIDGridLookUpEdit.Size = new System.Drawing.Size(511, 20);
            this.intBudgetIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intBudgetIDGridLookUpEdit.TabIndex = 5;
            this.intBudgetIDGridLookUpEdit.TabStop = false;
            this.intBudgetIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.intBudgetIDGridLookUpEdit_Validating);
            // 
            // sp01361ATBudgetListWithBlankBindingSource
            // 
            this.sp01361ATBudgetListWithBlankBindingSource.DataMember = "sp01361_AT_Budget_List_With_Blank";
            this.sp01361ATBudgetListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBudgetID,
            this.colBudgetDescription,
            this.colBudgetCode,
            this.colStartDate,
            this.colEndDate,
            this.colStartAmount,
            this.colBudgetedSpent,
            this.colActualSpent,
            this.colBudgetedRemaining,
            this.colActualRemaining,
            this.colRemarks,
            this.Calculated1,
            this.Calculated2,
            this.Calculated3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colBudgetID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBudgetDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colBudgetDescription
            // 
            this.colBudgetDescription.Caption = "Budget Description";
            this.colBudgetDescription.FieldName = "BudgetDescription";
            this.colBudgetDescription.Name = "colBudgetDescription";
            this.colBudgetDescription.OptionsColumn.AllowEdit = false;
            this.colBudgetDescription.OptionsColumn.AllowFocus = false;
            this.colBudgetDescription.OptionsColumn.ReadOnly = true;
            this.colBudgetDescription.Visible = true;
            this.colBudgetDescription.VisibleIndex = 1;
            this.colBudgetDescription.Width = 359;
            // 
            // colBudgetCode
            // 
            this.colBudgetCode.Caption = "Budget Code";
            this.colBudgetCode.FieldName = "BudgetCode";
            this.colBudgetCode.Name = "colBudgetCode";
            this.colBudgetCode.OptionsColumn.AllowEdit = false;
            this.colBudgetCode.OptionsColumn.AllowFocus = false;
            this.colBudgetCode.OptionsColumn.ReadOnly = true;
            this.colBudgetCode.Visible = true;
            this.colBudgetCode.VisibleIndex = 0;
            this.colBudgetCode.Width = 132;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 2;
            this.colStartDate.Width = 87;
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 3;
            // 
            // colStartAmount
            // 
            this.colStartAmount.Caption = "Start Amount";
            this.colStartAmount.FieldName = "StartAmount";
            this.colStartAmount.Name = "colStartAmount";
            this.colStartAmount.OptionsColumn.AllowEdit = false;
            this.colStartAmount.OptionsColumn.AllowFocus = false;
            this.colStartAmount.OptionsColumn.ReadOnly = true;
            this.colStartAmount.Visible = true;
            this.colStartAmount.VisibleIndex = 4;
            this.colStartAmount.Width = 85;
            // 
            // colBudgetedSpent
            // 
            this.colBudgetedSpent.Caption = "Budgeted Spent";
            this.colBudgetedSpent.FieldName = "BudgetedSpent";
            this.colBudgetedSpent.Name = "colBudgetedSpent";
            this.colBudgetedSpent.OptionsColumn.AllowEdit = false;
            this.colBudgetedSpent.OptionsColumn.AllowFocus = false;
            this.colBudgetedSpent.OptionsColumn.ReadOnly = true;
            this.colBudgetedSpent.Visible = true;
            this.colBudgetedSpent.VisibleIndex = 5;
            this.colBudgetedSpent.Width = 98;
            // 
            // colActualSpent
            // 
            this.colActualSpent.Caption = "Actual Spent";
            this.colActualSpent.FieldName = "ActualSpent";
            this.colActualSpent.Name = "colActualSpent";
            this.colActualSpent.OptionsColumn.AllowEdit = false;
            this.colActualSpent.OptionsColumn.AllowFocus = false;
            this.colActualSpent.OptionsColumn.ReadOnly = true;
            this.colActualSpent.Visible = true;
            this.colActualSpent.VisibleIndex = 6;
            this.colActualSpent.Width = 82;
            // 
            // colBudgetedRemaining
            // 
            this.colBudgetedRemaining.Caption = "Budgeted Remaining";
            this.colBudgetedRemaining.FieldName = "BudgetedRemaining";
            this.colBudgetedRemaining.Name = "colBudgetedRemaining";
            this.colBudgetedRemaining.OptionsColumn.AllowEdit = false;
            this.colBudgetedRemaining.OptionsColumn.AllowFocus = false;
            this.colBudgetedRemaining.OptionsColumn.ReadOnly = true;
            this.colBudgetedRemaining.Visible = true;
            this.colBudgetedRemaining.VisibleIndex = 7;
            this.colBudgetedRemaining.Width = 119;
            // 
            // colActualRemaining
            // 
            this.colActualRemaining.Caption = "Actual Remaining";
            this.colActualRemaining.FieldName = "ActualRemaining";
            this.colActualRemaining.Name = "colActualRemaining";
            this.colActualRemaining.OptionsColumn.AllowEdit = false;
            this.colActualRemaining.OptionsColumn.AllowFocus = false;
            this.colActualRemaining.OptionsColumn.ReadOnly = true;
            this.colActualRemaining.Visible = true;
            this.colActualRemaining.VisibleIndex = 8;
            this.colActualRemaining.Width = 103;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 9;
            // 
            // Calculated1
            // 
            this.Calculated1.Caption = "<b>Calculated 1</b>";
            this.Calculated1.FieldName = "Calculated1";
            this.Calculated1.Name = "Calculated1";
            this.Calculated1.OptionsColumn.AllowEdit = false;
            this.Calculated1.OptionsColumn.AllowFocus = false;
            this.Calculated1.OptionsColumn.ReadOnly = true;
            this.Calculated1.ShowUnboundExpressionMenu = true;
            this.Calculated1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1.Visible = true;
            this.Calculated1.VisibleIndex = 10;
            this.Calculated1.Width = 95;
            // 
            // Calculated2
            // 
            this.Calculated2.Caption = "<b>Calculated 2</b>";
            this.Calculated2.FieldName = "Calculated2";
            this.Calculated2.Name = "Calculated2";
            this.Calculated2.OptionsColumn.AllowEdit = false;
            this.Calculated2.OptionsColumn.AllowFocus = false;
            this.Calculated2.OptionsColumn.ReadOnly = true;
            this.Calculated2.ShowUnboundExpressionMenu = true;
            this.Calculated2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2.Visible = true;
            this.Calculated2.VisibleIndex = 11;
            this.Calculated2.Width = 95;
            // 
            // Calculated3
            // 
            this.Calculated3.Caption = "<b>Calculated 3</b>";
            this.Calculated3.FieldName = "Calculated3";
            this.Calculated3.Name = "Calculated3";
            this.Calculated3.OptionsColumn.AllowEdit = false;
            this.Calculated3.OptionsColumn.AllowFocus = false;
            this.Calculated3.OptionsColumn.ReadOnly = true;
            this.Calculated3.ShowUnboundExpressionMenu = true;
            this.Calculated3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3.Visible = true;
            this.Calculated3.VisibleIndex = 12;
            this.Calculated3.Width = 95;
            // 
            // ItemForintCostCentreID
            // 
            this.ItemForintCostCentreID.Control = this.intCostCentreIDTextEdit;
            this.ItemForintCostCentreID.CustomizationFormText = "Cost Centre ID:";
            this.ItemForintCostCentreID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintCostCentreID.Name = "ItemForintCostCentreID";
            this.ItemForintCostCentreID.Size = new System.Drawing.Size(608, 24);
            this.ItemForintCostCentreID.Text = "Cost Centre ID:";
            this.ItemForintCostCentreID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 481);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintBudgetID,
            this.ItemForintOwnershipID,
            this.ItemForstrCostCentreCode,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 461);
            // 
            // ItemForintBudgetID
            // 
            this.ItemForintBudgetID.AllowHide = false;
            this.ItemForintBudgetID.AllowHtmlStringInCaption = true;
            this.ItemForintBudgetID.Control = this.intBudgetIDGridLookUpEdit;
            this.ItemForintBudgetID.CustomizationFormText = "Linked Budget:";
            this.ItemForintBudgetID.Location = new System.Drawing.Point(0, 23);
            this.ItemForintBudgetID.Name = "ItemForintBudgetID";
            this.ItemForintBudgetID.Size = new System.Drawing.Size(608, 24);
            this.ItemForintBudgetID.Text = "Linked Budget:";
            this.ItemForintBudgetID.TextSize = new System.Drawing.Size(90, 13);
            // 
            // ItemForintOwnershipID
            // 
            this.ItemForintOwnershipID.AllowHide = false;
            this.ItemForintOwnershipID.AllowHtmlStringInCaption = true;
            this.ItemForintOwnershipID.Control = this.intOwnershipIDGridLookUpEdit;
            this.ItemForintOwnershipID.CustomizationFormText = "Linked Ownership:";
            this.ItemForintOwnershipID.Location = new System.Drawing.Point(0, 47);
            this.ItemForintOwnershipID.Name = "ItemForintOwnershipID";
            this.ItemForintOwnershipID.Size = new System.Drawing.Size(608, 24);
            this.ItemForintOwnershipID.Text = "Linked Ownership:";
            this.ItemForintOwnershipID.TextSize = new System.Drawing.Size(90, 13);
            // 
            // ItemForstrCostCentreCode
            // 
            this.ItemForstrCostCentreCode.AllowHide = false;
            this.ItemForstrCostCentreCode.AllowHtmlStringInCaption = true;
            this.ItemForstrCostCentreCode.Control = this.strCostCentreCodeTextEdit;
            this.ItemForstrCostCentreCode.CustomizationFormText = "Cost Centre Code:";
            this.ItemForstrCostCentreCode.Location = new System.Drawing.Point(0, 71);
            this.ItemForstrCostCentreCode.Name = "ItemForstrCostCentreCode";
            this.ItemForstrCostCentreCode.Size = new System.Drawing.Size(608, 24);
            this.ItemForstrCostCentreCode.Text = "Cost Centre Code:";
            this.ItemForstrCostCentreCode.TextSize = new System.Drawing.Size(90, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 95);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 366);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(94, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(94, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(94, 23);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(295, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(313, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(94, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(201, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // sp01359_AT_CostCentre_ItemTableAdapter
            // 
            this.sp01359_AT_CostCentre_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp01361_AT_Budget_List_With_BlankTableAdapter
            // 
            this.sp01361_AT_Budget_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01362_AT_Ownership_List_With_BlankTableAdapter
            // 
            this.sp01362_AT_Ownership_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AT_Cost_Centre_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Cost_Centre_Edit";
            this.Text = "Edit Cost Centre";
            this.Activated += new System.EventHandler(this.frm_AT_Cost_Centre_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Cost_Centre_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Cost_Centre_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp01359ATCostCentreItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCostCentreIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strCostCentreCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOwnershipIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01362ATOwnershipListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intBudgetIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01361ATBudgetListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCostCentreID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintBudgetID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOwnershipID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrCostCentreCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit intCostCentreIDTextEdit;
        private System.Windows.Forms.BindingSource sp01359ATCostCentreItemBindingSource;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.TextEdit strCostCentreCodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCostCentreID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintBudgetID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintOwnershipID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrCostCentreCode;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01359_AT_CostCentre_ItemTableAdapter sp01359_AT_CostCentre_ItemTableAdapter;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.GridLookUpEdit intOwnershipIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit intBudgetIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp01361ATBudgetListWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetID;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetCode;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetedSpent;
        private DevExpress.XtraGrid.Columns.GridColumn colActualSpent;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetedRemaining;
        private DevExpress.XtraGrid.Columns.GridColumn colActualRemaining;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01361_AT_Budget_List_With_BlankTableAdapter sp01361_AT_Budget_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp01362ATOwnershipListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01362_AT_Ownership_List_With_BlankTableAdapter sp01362_AT_Ownership_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipID;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipCode;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated21;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated22;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated23;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

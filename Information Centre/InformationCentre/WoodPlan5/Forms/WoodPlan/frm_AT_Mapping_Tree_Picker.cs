using System;
using System.Collections;
using System.Collections.Generic;  // Required for List call //
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MapInfo.Engine;
using MapInfo.Geometry;
using MapInfo.Mapping;
using MapInfo.Tools;
using MapInfo.Windows.Controls;
using MapInfo.Windows.Dialogs;
using MapInfo.Styles;
using MapInfo.Data;
using MapInfo.Mapping.Thematics;
using MapInfo.Mapping.Legends;

using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;  // Required for GraphicsPath call on Drawing Legend //
using System.Drawing.Imaging;  // Required for ImageMap Call on Drawing Legend //

using System.Reflection;  // Required by GridViewFiltering //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;  // Used for adding menu items on-the-fly for StoredGridViews //
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;

using WoodPlan5.Properties;
using Utilities;
using BaseObjects;
using GPS;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Tree_Picker : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int intLoadedWorkspaceID = 0;
        public int intLoadedWorkspaceOwner = 0;
        public string strLoadedWorkspaceName = "";


        SuperToolTip superToolTipSiteContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsSiteContractFilter = null;

        SuperToolTip superToolTipClientContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsClientContractFilter = null;

        SuperToolTip superToolTipSiteContractFilter2 = null;
        SuperToolTipSetupArgs superToolTipSetupArgsSiteContractFilter2 = null;

        SuperToolTip superToolTipClientContractFilter2 = null;
        SuperToolTipSetupArgs superToolTipSetupArgsClientContractFilter2 = null;

        public string i_str_selected_client_ids = "";  // Used to filter list of available map objects //
        string i_str_selected_client_names = "";
        string i_str_selected_site_ids = "";
        string i_str_selected_site_names = "";

        public string i_str_selected_client_ids2 = "";  // Used to filter list of available map objects //
        string i_str_selected_client_names2 = "";
        public string i_str_selected_site_ids2 = "";  // Used to filter list of available map objects //
        string i_str_selected_site_names2 = "";

        public string strAssetTypeIDs = "";  // Used to filter list of available map objects //
        public string strVisibleIDs = "";  // Used to control which MapObjects are intially visible on the map after loading //
        public string strVisibleAssetIDs = "";  // Used to control which MapObjects are intially visible on the map after loading //

        public string i_str_selected_workorders = "";  // Passed through to WorkOrder Mapping if this screen is called from WorkOrders //
        public string strHighlightedIDs = "";  // Used to control which MapObjects are initailly highlighted on the map after loading //
        public string strHighlightedAssetIDs = "";  // Used to control which MapObjects are initailly highlighted on the map after loading //
        public int intInitialHighlightColour = -10185235;  // [Default CornflowerBlue] Used to control the Initial Highlighter colour on the map after loading //
        public int intPassedMapX = 0;  // Passed in from Mapping_Functions when no tres have been selected for highlighting so the map can centre on the passed in X //
        public int intPassedMapY = 0;  // Passed in from Mapping_Functions when no tres have been selected for highlighting so the map can centre on the passed in Y //
        private bool PreventDefaultMapScaleUse = false;  // This is set to true temporarily if highlighting and map scaling from passed parameters is used //

        private string strMappingFilesPath = "";
        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selectionAssets;
        BaseObjects.GridCheckMarksSelection selectionAssetTypes;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        string i_str_tooltip_structure;
        string i_str_label_structure;
        bool i_bool_FullExtentFired = false;  // Used to stop popupContainerEdit2 firing QueryResultValue when it's Full Extent button clicked //
        RepositoryItem emptyEditor;  // Used to conditionally hide the Label Seperator Text editor if New Line Checkbox is ticked //
        private int intMapObjectSizing = 8;  // Value 6 to 20 or -1 for dynamic sizing based on Crown Diameter //
        public static GPSHandler GPS;

        private string i_str_Polygon_Area_Measurement_Unit = "M�";
        private MapInfo.Geometry.AreaUnit i_AreaUnit_Polygon_Area_Measurement_Unit = MapInfo.Geometry.AreaUnit.SquareMeter;
        private int i_int_GPS_CrossHair_Colour = -65536;  // Red //
        private int i_int_GPS_Polygon_Line_Plot_Colour = -16776961;  // Blue //
        private int i_int_GPS_Polygon_Plot_Line_Style = 2;  // Solid //
        private int i_str_GPS_Polygon_Plot_Line_Width = 1;
        private Timer GPSTimer = null;
        private bool boolLogGPS = false;

        MouseHook mouseHook = new MouseHook();  // Hooks for Mouse Events //
        KeyboardHook keyboardHook = new KeyboardHook();  // Hooks for Keyboard Events //

        double dMeasuredDistance = (double)0.00;
        double dCurrentMapScale = (double)0.00;  // Used as a Kludge as the MapViewChanged Event can't tell between Map Panned and Map Zoomed //

        SqlDataAdapter sdaDefaultStyles = new SqlDataAdapter();
        DataSet dsDefaultStyles = new DataSet("NewDataSet");
        SqlDataAdapter sdaThematicStyleItems = new SqlDataAdapter();
        DataSet dsThematicStyleItems = new DataSet("NewDataSet");
        SqlDataAdapter sdaThematicStyleSets = new SqlDataAdapter();
        DataSet dsThematicStyleSets = new DataSet("NewDataSet");

        SqlDataAdapter sdaFontSettings = new SqlDataAdapter();
        DataSet dsFontSettings = new DataSet("NewDataSet");
        SqlDataAdapter sdaLabelStructure = new SqlDataAdapter();
        DataSet dsLabelStructure = new DataSet("NewDataSet");

        int intThematicStylingFieldHeaderID = 0;
        int intThematicStylingFieldHeaderType = 0;  // 0 = System Picklist, 1 = Saved Thematic Style Set //
        int intLoadedStyleSetOwnerID = 0;
        int intThematicStylingBasePicklistID = 0;
        int intUseThematicStyling = 0;
        string strThematicStylingFieldName = "";  // Used by the Legend //
        int intMapObjectTransparency = 0;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private BeaconLocusEffect m_customBeaconLocusEffect1 = null;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        AutoScrollHelper autoScrollHelper;

        private bool boolQueryToolInUse = false;  // Used to get round MapInfo bug which calls the custom query tool twice //

        private DataSet_Selection DS_Selection1;
        private DataSet_Selection DS_Selection2;
        private DataSet_Selection DS_Selection3;

        private int intStrPolygonXYMaxLength = 32000; //255;

        private decimal decCurrentMouseX = 0;  // Used when setting client \ site centre point //
        private decimal decCurrentMouseY = 0;  // Used when setting client \ site centre point //
        private bool boolTrackXandY = true;  // Used when setting client \ site centre point //

        private List<Mapping_View> listStoredMapView = new List<Mapping_View>();
        private bool boolStoreViewChanges = true;
        private bool boolStoreViewChangesTempDisable = false;  // used to temporarily stop the map view being logeed when the user selects a stored view otherwise it would be logged again //
        private int intCurrentViewID = 0;

        int intTreeRefStructureType = 0;
        string strTreeRefStructureStripPattern = "";
        int intTreeRefStructureStripNumber = 0;

        private MapInfo.Styles.BitmapPointStyle bitmapPointStyle1;

        private int intGazetteerSearchCircleColour = -65536;  // Red //
        private int intGazetteerSearchCircleTransparency = 75;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr WindowFromPoint(System.Drawing.Point pnt); // Used by mouseHook_MouseUp event to get current control under Mouse Pointer //

        private string strDefaultMappingProjection = "";

        private decimal decDefaultHedgeWidth = (decimal)0.00;

        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //

        bool iBoolDontFireGridGotFocusOnDoubleClick = false;
        private int i_int_FocusedGrid = 1;
        int i_intMapObjectsSelected = 0;  // Used to switch on and off Add buttons in Linked Inspections and Actions grid - set in FeatureSelected Event //
        public int UpdateRefreshStatus = 0; // Controls if grids needs to refresh themselves on activate when a child screen has updated their data //
        string i_str_AddedRecordIDs1 = "";
        string i_str_AddedRecordIDs2 = "";
        string i_str_AddedRecordIDs3 = "";
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        bool iBool_AllowDelete = true;
        bool iBool_AllowAdd = true;
        bool iBool_AllowEdit = true;

        public int i_intCallingModule = 0;
        int i_intMappingShowAmenityTreeObjects = 0;
        int i_intMappingShowEstatePlanObjects = 0;
        int i_intSelectedObjectType = 0;

        #endregion

        public frm_AT_Mapping_Tree_Picker(int WorkSpaceID, int LoadedWorkspaceOwner, string LoadedWorkspaceName, int CallingModule)
        {
            InitializeComponent();

            intLoadedWorkspaceID = WorkSpaceID;
            intLoadedWorkspaceOwner = LoadedWorkspaceOwner;
            strLoadedWorkspaceName = LoadedWorkspaceName;
            i_intCallingModule = CallingModule;

            // Assign the Pan tool to the middle mouse button
            mapControl1.Tools.MiddleButtonTool = "Pan";

            // Listen to map events //
            mapControl1.Map.ViewChangedEvent += new ViewChangedEventHandler(Map_ViewChanged);
            mapControl1.Map.Layers.Added += new CollectionEventHandler(Layers_CountChanged);
            mapControl1.Map.Layers.Removed += new CollectionEventHandler(Layers_CountChanged);

            // Listen to tool events //
            mapControl1.Tools.FeatureAdding += new FeatureAddingEventHandler(FeatureAdding);
            //mapControl1.Tools.FeatureAdded += new FeatureAddedEventHandler(FeatureAdded);
            mapControl1.Tools.FeatureChanging += new FeatureChangingEventHandler(FeatureChanging);
            mapControl1.Tools.FeatureChanged += new MapInfo.Tools.FeatureChangedEventHandler(FeatureChanged);
            //mapControl1.Tools.FeatureSelecting += new FeatureSelectingEventHandler(FeatureSelecting);
            mapControl1.Tools.FeatureSelected += new FeatureSelectedEventHandler(FeatureSelected);
            mapControl1.Tools.NodeChanging += new NodeChangingEventHandler(Tools_NodeChanging);
            mapControl1.Tools.NodeChanged += new NodeChangedEventHandler(Tools_NodeChanged);

            mapControl1.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseWheel);
            mapControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseMove);

            // *** Enable translucency and anti-aliasing (anti-aliasing is optional) *** //
            mapControl1.Map.DrawingAttributes.EnableTranslucency = true;
            //mapControl1.Map.DrawingAttributes.SmoothingMode = MapInfo.Mapping.SmoothingMode.AntiAlias;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // *** Register Custom Map Marker (Plot Point) Tool *** //
            MapInfo.Tools.IMouseToolProperties mp3 = mapControl1.Tools.MouseToolProperties;
            MapInfo.Tools.MapTool ptMapTool2 = new MapInfo.Tools.CustomPointMapTool(false, this.mapControl1.Tools.FeatureViewer, this.mapControl1.Handle.ToInt32(), this.mapControl1.Tools, mp3, this.mapControl1.Tools.MapToolProperties);
            ptMapTool2.UseDefaultCursor = false;
            ptMapTool2.UseDefaultControlCursor = false;
            ptMapTool2.UseDefaultShiftCursor = false;
            this.mapControl1.Tools.Add("MapMarkerTool", ptMapTool2);

            //*** Register Custom Measuring Tool ***//
            MapInfo.Tools.IMouseToolProperties mp = mapControl1.Tools.MouseToolProperties;
            mp.Cursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            mp.ShiftCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");  // Holding Down Shift puts in right angles or controlled percentages of an angle //
            mp.ControlCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            WoodPlan5.PSGDistanceTool psgDistTool = new WoodPlan5.PSGDistanceTool(true, true, true, mapControl1.Viewer, mapControl1.Handle.ToInt32(), mapControl1.Tools, mp, mapControl1.Tools.MapToolProperties);
            // Following stops subsequent Query Tool from setting this tool to it's cursor //
            psgDistTool.UseDefaultCursor = false;
            psgDistTool.UseDefaultControlCursor = false;
            psgDistTool.UseDefaultShiftCursor = false;
            psgDistTool.Cursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            psgDistTool.ControlCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            psgDistTool.ShiftCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            // End of... Following stops subsequent Query Tool from setting this tool to it's cursor //
            mapControl1.Tools.Add("Distance", psgDistTool);  // Add the tool to the tools collection "Distance" is the same as the ToolId for the mapToolBarButtonDistance //
            psgDistTool.PSGDistanceToolPointAdded += new WoodPlan5.PSGDistanceToolPointAddedEventHandler(psgDistTool_PSGDistanceToolPointAdded);  // Register events for measuring tool //
            psgDistTool.PSGDistanceToolMove += new WoodPlan5.PSGDistanceToolPointAddedEventHandler(psgDistTool_PSGDistanceToolMove);  // Register events for measuring tool //

            // *** Register Custom Query Tool *** //
            MapInfo.Tools.IMouseToolProperties mp2 = mapControl1.Tools.MouseToolProperties;
            mp2.Cursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Map_Query.cur");
            mp2.ShiftCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Map_Query.cur");  // Holding Down Shift puts in right angles or controlled percentages of an angle //
            mp2.ControlCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Map_Query.cur");
            MapInfo.Tools.MapTool ptMapTool = new MapInfo.Tools.CustomPointMapTool(false, this.mapControl1.Tools.FeatureViewer, this.mapControl1.Handle.ToInt32(), this.mapControl1.Tools, mp2, this.mapControl1.Tools.MapToolProperties);
            ptMapTool.UseDefaultCursor = false;
            this.mapControl1.Tools.Add("InfoTool", ptMapTool);

            this.mapControl1.Tools.Used += new MapInfo.Tools.ToolUsedEventHandler(Tools_Used);

            GPS = new GPSHandler(this); //Initialize GPS handler
            GPS.TimeOut = 5; //Set timeout to 5 seconds
            GPS.NewGPSFix += new GPSHandler.NewGPSFixHandler(this.GPSEventHandler); //Hook up GPS data events to a handler

            GPSTimer = new Timer();
            GPSTimer.Interval = 5000;
            GPSTimer.Tick += new EventHandler(GPSTimer_Tick);

            // Define MapMarker Point Icon (bitmap) //
            bitmapPointStyle1 = new MapInfo.Styles.BitmapPointStyle("PushPin_Red_16.bmp");
            MapInfo.Styles.StyleRepository sr = MapInfo.Engine.Session.Current.StyleRepository;
            String path = Application.StartupPath + "\\";
            MapInfo.Styles.BitmapPointStyleRepository bpsr = sr.BitmapPointStyleRepository;
            bpsr.Reload(path);

            //loading.Close();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }

        MapInfo.Geometry.DPoint ptMouseDown;
        private void Tools_Used(object sender, MapInfo.Tools.ToolUsedEventArgs e)
        {
            switch (e.ToolName.ToString())
            {
                case "Measurer":
                    switch (e.ToolStatus)
                    {
                        case MapInfo.Tools.ToolStatus.Start:
                            ptMouseDown = e.MapCoordinate;
                            break;
                        case MapInfo.Tools.ToolStatus.End:
                            MapInfo.Geometry.DRect Rect = new MapInfo.Geometry.DRect(ptMouseDown, e.MapCoordinate);
                            break;
                    }
                    break;
                case "InfoTool":
                    CustomQueryInfo(e);
                    break;
                case "MapMarkerTool":
                    MapMarkerAddEvent(e);
                    break;
            }
        }

        private void frm_AT_Mapping_Tree_Picker_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            this.FormID = 2004;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            dockManager1.ActivePanel = dockPanel4;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "2004", GlobalSettings.ViewedPeriodID);
            if (this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count > 0)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.blRead = true;
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[0]["CreateAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[0]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[0]["DeleteAccess"]);
                iBool_AllowAdd = sfpPermissions.blCreate;
                iBool_AllowEdit = sfpPermissions.blUpdate;
                iBool_AllowDelete = sfpPermissions.blDelete;
                ArrayList alPermissions = new ArrayList();
                alPermissions.Add(sfpPermissions);
                this.FormPermissions = alPermissions;

                bbiAddPoint.Enabled = iBool_AllowAdd;
                bbiAddPolygon.Enabled = iBool_AllowAdd;
                bbiAddPolyLine.Enabled = iBool_AllowAdd;
                btnGPS_Start.Enabled = iBool_AllowAdd;
            }

            // Get default Polygon Area Measurement Unit from System_Settings table //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            i_str_Polygon_Area_Measurement_Unit = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Polygon_Area"));
            switch (i_str_Polygon_Area_Measurement_Unit)
            {
                case "M�":
                    i_AreaUnit_Polygon_Area_Measurement_Unit = MapInfo.Geometry.AreaUnit.SquareMeter;
                    break;
                case "Hectares":
                    i_AreaUnit_Polygon_Area_Measurement_Unit = MapInfo.Geometry.AreaUnit.Hectare;
                    i_str_Polygon_Area_Measurement_Unit = "Ha";
                    break;
                case "Acres":
                    i_AreaUnit_Polygon_Area_Measurement_Unit = MapInfo.Geometry.AreaUnit.Acre;
                    break;
                default:
                    break;
            }

            strDefaultMappingProjection = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultMappingProjection").ToString();
            if (string.IsNullOrEmpty(strDefaultMappingProjection)) strDefaultMappingProjection = "CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000";
                        
            string strDefaultHedgeWidth = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Default_Hedge_Width").ToString();
            decDefaultHedgeWidth = (string.IsNullOrEmpty(strDefaultHedgeWidth) ? (decimal)1.00 : Convert.ToDecimal(strDefaultHedgeWidth));

            //colorEditHighlight.EditValue = intInitialHighlightColour;

            // Get default GPS CrossHair Colour from System_Settings table //
            i_int_GPS_CrossHair_Colour = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_GPS_CrossHair_Colour"));

            // Get default GPS Polygon Plot Line Colour from System_Settings table //
            i_int_GPS_Polygon_Line_Plot_Colour = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour"));

            // Get default GPS Polygon Plot Line Style from System_Settings table //
            i_int_GPS_Polygon_Plot_Line_Style = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style"));

            // Get default GPS Polygon Plot Line Width from System_Settings table //
            i_str_GPS_Polygon_Plot_Line_Width = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width"));

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Get Tree Reference Structure Default Settings from System_Settings table //
            string strTreeRefFormat = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Tree_Ref_Format_Used"));
            if (!string.IsNullOrEmpty(strTreeRefFormat))
            {
                if (strTreeRefFormat.StartsWith("Default"))
                {
                    checkEditTreeRef1.Checked = true;
                }
                else if (strTreeRefFormat.StartsWith("Remove First"))
                {
                    checkEditTreeRef2.Checked = true;
                    strTreeRefFormat = strTreeRefFormat.Substring(14, strTreeRefFormat.Length - 27);
                    spinEditTreeRefRemoveNumberOfChars.EditValue = (CheckingFunctions.IsNumeric(strTreeRefFormat) ? Convert.ToInt32(strTreeRefFormat) : 1);
                }
                else if (strTreeRefFormat.StartsWith("Remove All"))
                {
                    checkEditTreeRef3.Checked = true;
                    textEditTreeRefRemovePreceedingChars.EditValue = strTreeRefFormat.Substring(43);
                }
            }
            SetTreeRefStructureControlStatus();
            popupContainerEditTreeRefStructure.EditValue = popupContainerEditTreeRefStructure_Get_Selected();
            // End of Tree Reference Structure Defualt Settings from System_Settings table  //

            // Create a Tree Filter SuperToolTips //
            superToolTipSiteContractFilter = new SuperToolTip();
            superToolTipSetupArgsSiteContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsSiteContractFilter.Title.Text = "Site Filter - Information";
            superToolTipSetupArgsSiteContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsSiteContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsSiteContractFilter.Footer.Text = "";
            superToolTipSiteContractFilter.Setup(superToolTipSetupArgsSiteContractFilter);
            superToolTipSiteContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditSiteFilter.SuperTip = superToolTipSiteContractFilter;

            // Create a Tree Filter SuperToolTips //
            superToolTipClientContractFilter = new SuperToolTip();
            superToolTipSetupArgsClientContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsClientContractFilter.Title.Text = "Client Filter - Information";
            superToolTipSetupArgsClientContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsClientContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsClientContractFilter.Footer.Text = "";
            superToolTipClientContractFilter.Setup(superToolTipSetupArgsClientContractFilter);
            superToolTipClientContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditClientFilter.SuperTip = superToolTipClientContractFilter;


            // Create a Asset Filter SuperToolTips //
            superToolTipSiteContractFilter2 = new SuperToolTip();
            superToolTipSetupArgsSiteContractFilter2 = new SuperToolTipSetupArgs();
            superToolTipSetupArgsSiteContractFilter2.Title.Text = "Site Filter - Information";
            superToolTipSetupArgsSiteContractFilter2.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsSiteContractFilter2.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsSiteContractFilter2.ShowFooterSeparator = false;
            superToolTipSetupArgsSiteContractFilter2.Footer.Text = "";
            superToolTipSiteContractFilter2.Setup(superToolTipSetupArgsSiteContractFilter2);
            superToolTipSiteContractFilter2.AllowHtmlText = DefaultBoolean.True;
            buttonEditSiteFilter2.SuperTip = superToolTipSiteContractFilter2;

            // Create a Asset Filter SuperToolTips //
            superToolTipClientContractFilter2 = new SuperToolTip();
            superToolTipSetupArgsClientContractFilter2 = new SuperToolTipSetupArgs();
            superToolTipSetupArgsClientContractFilter2.Title.Text = "Client Filter - Information";
            superToolTipSetupArgsClientContractFilter2.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsClientContractFilter2.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsClientContractFilter2.ShowFooterSeparator = false;
            superToolTipSetupArgsClientContractFilter2.Footer.Text = "";
            superToolTipClientContractFilter2.Setup(superToolTipSetupArgsClientContractFilter2);
            superToolTipClientContractFilter2.AllowHtmlText = DefaultBoolean.True;
            buttonEditClientFilter2.SuperTip = superToolTipClientContractFilter2;

            // Check what map objects we should be loading... //
            i_intMappingShowAmenityTreeObjects = (GetSetting.sp00043_RetrieveSingleSystemSetting(0, "MappingShowAmenityTreeObjects").ToString() == "On" ? 1 : 0);
            i_intMappingShowEstatePlanObjects = (GetSetting.sp00043_RetrieveSingleSystemSetting(0, "MappingShowEstatePlanObjects").ToString() == "On" ? 1 : 0);
            if (i_intMappingShowAmenityTreeObjects == 1)  // Enable Amenity Tree Map Objects //
            {
                layoutControlGroup3.Enabled = true;
                gridControl2.Enabled = true;
                popupContainerEditTreeRefStructure.Enabled = true;
                buttonEditClientFilter.Enabled = true;
                buttonEditSiteFilter.Enabled = true;

                // Add record visibility checkboxes to map object grid //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 70; // Make wider to allow for caption in header //
                selection1.CheckMarkColumn.Caption = "     Visible";  // Spaces at start so room for checkbox in front //
                selection1.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                selection1.CheckMarkColumn.OptionsColumn.ShowCaption = true;
                selection1.CheckMarkColumn.ToolTip = "Controls which Map Objects are Loaded into the map.";
                selection1.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
                selection1.editColumnHeader.Caption = "";
                selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                selection1.CheckMarkColumn.VisibleIndex = 0;

                emptyEditor = new RepositoryItem();  // Used to conditionally hide the Label Seperator Text editor if New Line Checkbox is ticked //
                gridControl2.RepositoryItems.Add(emptyEditor);

                Pre_Select_Clients();  // Check if any clients need to be pre-selected, if yes then select them... //
                gridControl2.ForceInitialize();
                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            }
            else
            {
                layoutControlGroup3.Enabled = false;
                gridControl2.Enabled = false;
                popupContainerEditTreeRefStructure.Enabled = false;
                buttonEditClientFilter.Enabled = false;
                buttonEditSiteFilter.Enabled = false;
            }
            if (i_intMappingShowEstatePlanObjects == 1)  // Enable Asset Management Map Objects //
            {
                layoutControlGroup5.Enabled = true;
                gridControlAssetObjects.Enabled = true;
                buttonEditClientFilter2.Enabled = true;
                buttonEditSiteFilter2.Enabled = true;
                sp03069_Tree_Picker_Asset_Objects_ListTableAdapter.Connection.ConnectionString = strConnectionString;

                sp03021_EP_Asset_Type_Filter_DropDownTableAdapter.Connection.ConnectionString = strConnectionString;
                sp03021_EP_Asset_Type_Filter_DropDownTableAdapter.Fill(this.dataSet_EP.sp03021_EP_Asset_Type_Filter_DropDown, "");
                // Add Selection Checkboxes to Grid //
                selectionAssetTypes = new BaseObjects.GridCheckMarksSelection((GridView)gridControlAssetTypes.MainView);
                selectionAssetTypes.CheckMarkColumn.VisibleIndex = 0;
                selectionAssetTypes.CheckMarkColumn.Width = 30;
                gridControlAssetTypes.ForceInitialize();

                // Add record visibility checkboxes to map asset object grid //
                selectionAssets = new BaseObjects.GridCheckMarksSelection((GridView)gridControlAssetObjects.MainView);
                selectionAssets.CheckMarkColumn.VisibleIndex = 0;
                selectionAssets.CheckMarkColumn.Width = 70; // Make wider to allow for caption in header //
                selectionAssets.CheckMarkColumn.Caption = "     Visible";  // Spaces at start so room for checkbox in front //
                selectionAssets.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
                selectionAssets.CheckMarkColumn.OptionsColumn.ShowCaption = true;
                selectionAssets.CheckMarkColumn.ToolTip = "Controls which Map Objects are Loaded into the map.";
                selectionAssets.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
                selectionAssets.editColumnHeader.Caption = "";
                selectionAssets.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                selectionAssets.CheckMarkColumn.VisibleIndex = 0;

                Pre_Select_Sites();  // Check if any sites need to be pre-selected, if yes then select them... //
                gridControlAssetTypes.ForceInitialize();
                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            }
            else
            {
                layoutControlGroup5.Enabled = false;
                gridControlAssetObjects.Enabled = false;
                buttonEditClientFilter2.Enabled = false;
                buttonEditSiteFilter2.Enabled = false;
            }

            // Decide what type of map objects to load by default //
            if (i_intCallingModule == 1 && i_intMappingShowAmenityTreeObjects == 1)  // Called by Amenity Trees //
            {
                Load_Map_Objects_Grid();
                tabbedControlGroup1.SelectedTabPageIndex = 0;
            }
            else if (i_intMappingShowEstatePlanObjects == 1)  // Called by Estate Plan //
            {
                if (!string.IsNullOrEmpty(i_str_selected_site_ids2)) Load_Map_Objects_Grid_Assets();  // Speed up loading by only loading map objects if required //
                tabbedControlGroup1.SelectedTabPageIndex = 1;
            }

            // Populate list of available plottable object types on the toolbar - Check which object types to work with from System_Settings table //
            beiPlotObjectType.EditValue = 0;
            sp03068_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03068_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter.Fill(dataSet_AT_TreePicker.sp03068_Tree_Picker_Plotting_Object_Types_With_Blank, i_intMappingShowAmenityTreeObjects, i_intMappingShowEstatePlanObjects);

            this.sp01254_AT_Tree_Picker_scale_listTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01254_AT_Tree_Picker_scale_listTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01254_AT_Tree_Picker_scale_list);
            gridControl4.ForceInitialize();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // *** Gazetteer *** //
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01311_AT_Tree_Picker_Gazetteer_Search_Types);
            if (this.dataSet_AT_TreePicker.sp01311_AT_Tree_Picker_Gazetteer_Search_Types.Rows.Count > 1)
            {
                lookUpEditGazetteerSearchType.EditValue = this.dataSet_AT_TreePicker.sp01311_AT_Tree_Picker_Gazetteer_Search_Types.Rows[1]["Description"].ToString();
            }
            buttonEditGazetteerFindValue.EditValue = "";
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl3.MainView = gridView3;
            
            // Get default value for Gazetteer Search Radius on Menu //
            string strGazetteerSearchRadius = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Gazetteer_Search_Radius").ToString();
            beiGazetteerLoadObjectsWithinRange.EditValue = (string.IsNullOrEmpty(strGazetteerSearchRadius) ? (decimal)50.00 : Convert.ToDecimal(strGazetteerSearchRadius));
            // Get back colour for Gazetteer Search Area //
            string strGazetteerSearchCircleColour = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour").ToString();
            intGazetteerSearchCircleColour = (string.IsNullOrEmpty(strGazetteerSearchCircleColour) ? (int)-65536 : Convert.ToInt32(strGazetteerSearchCircleColour));
            // Get transparency level for Gazetteer Search Area //
            string strGazetterSearchCircleTransparency = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency").ToString();
            intGazetteerSearchCircleTransparency = (string.IsNullOrEmpty(strGazetterSearchCircleTransparency) ? (int)-65536 : Convert.ToInt32(strGazetterSearchCircleTransparency));
            // *** End of Gazetteer *** //

            // Get Mapping Background Files Path Location //
            //strMappingFilesPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesMapBackgroundFolder").ToString().ToLower();
            strMappingFilesPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, (this.GlobalSettings.SystemDataTransferMode != "Tablet" ? "AmenityTreesMapBackgroundFolder" : "AmenityTreesMapBackgroundFolderTablet")).ToString().ToLower();          
            if (strMappingFilesPath == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Load Map - No Mapping Background Folder Location has been specified in the System Settings table.", "Load Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                this.UnlockThisWindow();  // ***** Locked in Load event ***** //
                Close();
            }
            this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter.Connection.ConnectionString = strConnectionString;  // Holds the Mapping layers to load //
            gridControl5.ForceInitialize();
            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Connection.ConnectionString = strConnectionString;  // Holds the Mapping Workspaces properties //

            #region Load Default Styles and Thematics
            int intPickListID = -999;  // Set to -999 so nothing is initially retrieved //
            int intPickListType = 0;
            int intDefaultSymbol = 0;
            int DefaultSymbolColour = 0;
            int DefaultSymbolSize = 0;
            int DefaultPolygonBoundaryColour = 0;
            int DefaultPolygonBoundaryWidth = 0;
            int DefaultPolygonFillColour = 0;
            int DefaultPolygonFillPattern = 0;
            int DefaultPolygonFillPatternColour = 0;
            int DefaultLineSymbol = 0;
            int DefaultLineColour = 0;
            int DefaultLineWidth = 0;

            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp01257_AT_Tree_Picker_Thematic_Mapping_Default_Dummy", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbol", intDefaultSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolColour", DefaultSymbolColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolSize", DefaultSymbolSize));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryColour", DefaultPolygonBoundaryColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryWidth", DefaultPolygonBoundaryWidth));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillColour", DefaultPolygonFillColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPattern", DefaultPolygonFillPattern));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPatternColour", DefaultPolygonFillPatternColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineSymbol", DefaultLineSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineColour", DefaultLineColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineWidth", DefaultLineWidth));
            sdaDefaultStyles = new SqlDataAdapter(cmd);
            sdaDefaultStyles.Fill(dsDefaultStyles, "Table");

            int intCurrentUser = this.GlobalSettings.UserID;
            int intCurrentUserType = this.GlobalSettings.PersonType;
            cmd = null;
            cmd = new SqlCommand("sp01256_AT_Tree_Picker_Thematic_Mapping_Available_Sets", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@intUserID", intCurrentUser));
            cmd.Parameters.Add(new SqlParameter("@intUserType", intCurrentUserType));
     
            sdaThematicStyleSets = new SqlDataAdapter(cmd);
            sdaThematicStyleSets.Fill(dsThematicStyleSets, "Table");

            cmd = null;
            cmd = new SqlCommand("sp01255_AT_Tree_Picker_Thematic_Mapping_Default_Items", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PickListID", intPickListID));
            cmd.Parameters.Add(new SqlParameter("@PicklistType", intPickListType));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbol", intDefaultSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolColour", DefaultSymbolColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolSize", DefaultSymbolSize));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryColour", DefaultPolygonBoundaryColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryWidth", DefaultPolygonBoundaryWidth));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillColour", DefaultPolygonFillColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPattern", DefaultPolygonFillPattern));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPatternColour", DefaultPolygonFillPatternColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineSymbol", DefaultLineSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineColour", DefaultLineColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineWidth", DefaultLineWidth));
            sdaThematicStyleItems = new SqlDataAdapter(cmd);
            sdaThematicStyleItems.Fill(dsThematicStyleItems, "Table");
            dsThematicStyleItems.Clear();  // Remove any values as the user will need to choose thematic criteria from the child Thematic Styling screen first //

            SQlConn.Close();
            SQlConn.Dispose();
            #endregion

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Prepare LocusEffects and add custom effect //
            locusEffectsProvider1 = new LocusEffectsProvider();
            locusEffectsProvider1.Initialize();
            locusEffectsProvider1.FramesPerSecond = 30;
            m_customBeaconLocusEffect1 = new BeaconLocusEffect();
            m_customBeaconLocusEffect1.Name = "CustomBeacon1";
            m_customBeaconLocusEffect1.InitialSize = new Size(150, 150);
            m_customBeaconLocusEffect1.AnimationTime = 1500;
            m_customBeaconLocusEffect1.LeadInTime = 0;
            m_customBeaconLocusEffect1.LeadOutTime = 0;
            m_customBeaconLocusEffect1.AnimationStartColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationEndColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationOuterColor = Color.Salmon;
            m_customBeaconLocusEffect1.RingWidth = 6; // 6;
            m_customBeaconLocusEffect1.OuterRingWidth = 3;// 3;
            m_customBeaconLocusEffect1.BodyFadeOut = true;
            m_customBeaconLocusEffect1.Style = BeaconEffectStyles.Shrink;//.HeartBeat;
            locusEffectsProvider1.AddLocusEffect(m_customBeaconLocusEffect1);

            m_customArrowLocusEffect1 = new ArrowLocusEffect();
            m_customArrowLocusEffect1.Name = "CustomeArrow1";
            m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
            m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
            m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
            m_customArrowLocusEffect1.MovementCycles = 20;
            m_customArrowLocusEffect1.MovementAmplitude = 200;
            m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
            m_customArrowLocusEffect1.LeadInTime = 0; //msec
            m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
            m_customArrowLocusEffect1.AnimationTime = 2000; //msec
            locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

            sp01367_AT_Inspections_For_TreesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView7, "InspectionID");
            
            sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView8, "ActionID");
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            Load_Layer_Manager();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            LoadGPSComPortList();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // *** Hoook into Mouse and Keyboard Events *** //
            mouseHook.MouseMove += new MouseEventHandler(mouseHook_MouseMove);
            mouseHook.MouseDown += new MouseEventHandler(mouseHook_MouseDown);
            mouseHook.MouseUp += new MouseEventHandler(mouseHook_MouseUp);
            mouseHook.MouseWheel += new MouseEventHandler(mouseHook_MouseWheel);
            keyboardHook.KeyDown += new KeyEventHandler(keyboardHook_KeyDown);
            keyboardHook.KeyUp += new KeyEventHandler(keyboardHook_KeyUp);
            keyboardHook.KeyPress += new KeyPressEventHandler(keyboardHook_KeyPress);
            // Mouse and Keyboard Hooks started in mapControl1_Enter event //

            mapControl1.Visible = true;
            autoScrollHelper = new AutoScrollHelper(gridView5);  // Add Autoscroll functionality when dragging to adjust record order //
            gridControl5.AllowDrop = true;

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridView2, strConnectionString);  // Trees List //
            DS_Selection2 = new Utilities.DataSet_Selection((GridView)gridView7, strConnectionString);  // Inspections List //
            DS_Selection3 = new Utilities.DataSet_Selection((GridView)gridView8, strConnectionString);  // Actions List //
        }

        public override void PostOpen(object objParameter)
        {
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }

            // If screen fired from WorkOrder Printing - Highlight the Map Linking button on the toolbar //
            if (i_str_selected_workorders != "")
            {
                // Get BarButtons Location //
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, 0, 0);
                foreach (BarItemLink link in bar4.ItemLinks)
                {
                    if (link.Item.Name == "bbiWorkOrderMap")  // Find the Correct button then call function to gets it position using Reflection (as this info is not publicly available //
                    {
                        rect = GetLinksScreenRect(link);
                        break;
                    }
                }
                if (rect.X > 0 && rect.Y > 0)
                {
                    // Draw Locus Effect on object so user can see it //
                    System.Drawing.Point screenPoint = new System.Drawing.Point(rect.X + rect.Width - 10, rect.Y + rect.Height - 10);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                }
            }
            else
            {
                if (i_intMappingShowAmenityTreeObjects == 1) beiPlotObjectType.EditValue = -1;  // Trees //
            }
            Clear_Stored_Map_Views();  // Make sure no Stored Map Views are left //
        }

        private System.Drawing.Rectangle GetLinksScreenRect(BarItemLink link)
        {
            System.Reflection.PropertyInfo info = typeof(BarItemLink).GetProperty("BarControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            Control c = (Control)info.GetValue(link, null);
            return c.RectangleToScreen(link.Bounds);
        }

        private void frm_AT_Mapping_Tree_Picker_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                Process_Selected_Map_Objects();  // Refresh Linked Inspections and Actions grids //
            }
            SetMenuStatus();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in arraylistFloatingPanels)
            {
                dp.Show();
            }
        }

        private void frm_AT_Mapping_Tree_Picker_Deactivate(object sender, EventArgs e)
        {
            arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }
            //MouseSimulator.MouseDown(MouseButton.Left); // This line is commented out as it can cause ghosting when switching between page tabs and any opened child form needs a second click to focus it... //
            KeyboardSimulator.KeyPress(Keys.Escape);

        }

        private void frm_AT_Mapping_Tree_Picker_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void frm_AT_Mapping_Tree_Picker_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (btnGPS_Start.Text == "Stop GPS")
            {
                btnGPS_Start.PerformClick();  // Stop GPS by simulating Click on GPS Button //
            }

            mapControl1.Map.ViewChangedEvent -= new ViewChangedEventHandler(Map_ViewChanged); // Unhook event so it doesn't fire on shut down //
            mapControl1.Map.Clear();
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
            if (t != null)
            {
                if (t.IsOpen) t.Close();
            }

            t = conn.Catalog.GetTable("TempMapObjects");
            if (t != null)
            {
                if (t.IsOpen) t.Close();
            }
            conn.Close();

            if (GPSTimer != null)
            {
                GPSTimer.Stop();
                GPSTimer = null;
            }

            // Remove Mouse and Keyboard Hooks //
            mouseHook.Stop();
            keyboardHook.Stop();

            GC.GetTotalMemory(true);
        }

        private void dockManager1_ActivePanelChanged(object sender, DevExpress.XtraBars.Docking.ActivePanelChangedEventArgs e)
        {
        }

        private void dockManager1_ClosedPanel(object sender, DevExpress.XtraBars.Docking.DockPanelEventArgs e)
        {
        }


        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }

 
        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            if (view.GridControl.MainView.Name != "gridView2") return;  // Only fire for appropriate view //
            if (row == GridControl.InvalidRowHandle) return;

            if (Control.ModifierKeys != Keys.Shift) return;

            if (row == -99999999)  // User clicked on Viewed
            {
                view.BeginUpdate();
                view.BeginDataUpdate();
                // Process just child rows of the current group //
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    view.SetRowCellValue((i), "Highlight", Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")));
                }
                view.EndDataUpdate();
                view.EndUpdate();
            }
            else if (row < 0)  // Viewed Column checkbox on a Group or the Grid Column Header clicked so copy Visible setting to Highlighted //
            {
                view.BeginUpdate();
                view.BeginDataUpdate();
                // Process just child rows of the current group //
                ArrayList childRows = new ArrayList();
                GetChildDataRows(view, row, childRows);
                for (int i = 0; i < childRows.Count; i++)
                {
                    view.SetRowCellValue(Convert.ToInt32(childRows[i]), "Highlight", Convert.ToInt32(view.GetRowCellValue(Convert.ToInt32(childRows[i]), "CheckMarkSelection")));
                }
                view.EndDataUpdate();
                view.EndUpdate();
            }
            /*else if (row != 0)  // Indivdual row checkbox clicked //
            {
                view.BeginUpdate();
                if (Control.ModifierKeys == Keys.Shift)
                {
                    view.SetRowCellValue(row, "Highlight", Convert.ToInt32(view.GetRowCellValue(row, "CheckMarkSelection")));
                }
                else
                {
                    if (Convert.ToInt32(view.GetRowCellValue(row, "CheckMarkSelection")) == 0) view.SetRowCellValue(row, "Highlight", 0);
                }
                view.EndUpdate();
            }*/
        }

        void GetChildDataRows(GridView view, int rowHandle, ArrayList r)
        {
            int childCount = view.GetChildRowCount(rowHandle);
            for (int i = 0; i < childCount; i++)
            {
                int childRowHandle = view.GetChildRowHandle(rowHandle, i);
                if (childRowHandle < 0)
                {
                    GetChildDataRows(view, childRowHandle, r);
                }
                else
                {
                    r.Add(childRowHandle);
                }
            }
        } 

        
        public void Pre_Select_Clients()
        {
            colorEditHighlight.EditValue = intInitialHighlightColour;
            if (i_str_selected_client_ids != "")
            {
                try
                {
                   var GetValue = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                   GetValue.ChangeConnectionString(strConnectionString);
                   i_str_selected_client_names = GetValue.sp01018_Core_Client_Names_From_ClientIDs(i_str_selected_client_ids).ToString();
                }
                catch (Exception) { }
            }
        }

        public void Pre_Select_Sites()
        {
            colorEditHighlight.EditValue = intInitialHighlightColour;
            if (i_str_selected_site_ids2 != "")
            {
                try
                {
                    var GetValue = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                    GetValue.ChangeConnectionString(strConnectionString);
                    i_str_selected_site_names2 = GetValue.sp01019_Core_Site_Names_From_SiteIDs(i_str_selected_site_ids2).ToString();
                }
                catch (Exception) { }
            }
        }

        public void Load_Map_Objects_Grid()
        {
            // Populate list of searchable Map Objects //
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            this.sp01252_AT_Tree_Picker_load_object_managerTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01252_AT_Tree_Picker_load_object_managerTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01252_AT_Tree_Picker_load_object_manager, i_str_selected_client_ids, i_str_selected_site_ids);
            view.EndUpdate();
        }

        public void Load_Map_Objects_Grid_Assets()
        {
            // Populate list of searchable Map Objects //
            GridView view = (GridView)gridControlAssetObjects.MainView;
            view.BeginUpdate();
            this.sp03069_Tree_Picker_Asset_Objects_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp03069_Tree_Picker_Asset_Objects_ListTableAdapter.Fill(this.dataSet_AT_TreePicker.sp03069_Tree_Picker_Asset_Objects_List, i_str_selected_site_ids2, i_str_selected_client_ids2, strAssetTypeIDs);
            view.EndUpdate();
        }



        private void CreateFeatureLayerModifier(MapInfo.Mapping.IMapLayer l, DataRow dr)
        {
            int intTempPointSize = (intMapObjectSizing <= 0 ? 8 : intMapObjectSizing);

            if (l.Type == LayerType.Normal)  // Vector Layer //
            {
                // Create a feature override style modifier if PreserveDefaultStyling switched off //
                if (Convert.ToInt32(dr["PreserveDefaultStyling"]) == 0)  // Allow styling according to criteria //
                {
                    CompositeStyle style = new CompositeStyle();
                    //((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * Convert.ToInt32(dr["Transparency"])), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));
                    style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(dr["LineWidth"]), MapInfo.Styles.LineWidthUnit.Pixel), 2, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"]))));

                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.White);
                    ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));
                    style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));


                    FeatureOverrideStyleModifier modifier = new FeatureOverrideStyleModifier();
                    modifier.Style = style;
                    modifier.Name = "FeatureStyleModifier1";
                    FeatureLayer layer = mapControl1.Map.Layers[mapControl1.Map.Layers.IndexOf(l)] as FeatureLayer;
                    layer.Modifiers.Append(modifier);  // *** Allow switching on and off *** //
                }
            }
            else if (l.Type == LayerType.Raster)  // Raster Layer //
            {
                // Create a feature override style modifier //
                CompositeStyle style = new CompositeStyle();
                style.RasterStyle.Grayscale = (Convert.ToInt32(dr["GreyScale"]) == 1 ? true : false);
                //style.RasterStyle.Transparent = true;    // *** This line commented out otherwise monocrome image layers don't display! *** //
                style.RasterStyle.Alpha = (int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100)));  // Controls Transparency //

                FeatureOverrideStyleModifier modifier = new FeatureOverrideStyleModifier();
                modifier.Style = style;
                modifier.Name = "FeatureStyleModifier1";
                FeatureLayer layer = mapControl1.Map.Layers[mapControl1.Map.Layers.IndexOf(l)] as FeatureLayer;
                layer.Modifiers.Append(modifier);  // *** Allow switching on and off *** //

            }
        }

        private int GetLayerManagerMapObjectsLayerRow()
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view == null) return -1;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (view.GetRowCellValue(i, "LayerPath").ToString() == "MapObjects") return i;
            }
            return -1;  // Not found //
        }

        private string Get_ThematicField_ColumnName(int ThematicStylingBasePicklistID)
        {
            // Returns the field name (column) from which the value needs to be extracted from the MapObject Grid so it can be used for a match against the thematic styles //
            switch (ThematicStylingBasePicklistID)
            {
                case -1:  // Sites //
                    return "SiteID";
                case 2:  // Species //
                    return "intSpeciesID";
                case 6:  // Ownerships //
                    return "intOwnershipID";
                case 101:  // Status //
                    return "intStatus";
                case 103:  // Risk Categories //
                    return "intSafetyPriority";
                case 112:  // Accessibility //
                    return "intAccess";
                case 113:  // Visibility //
                    return "intVisibility";
                case 114:  // Legal Status //
                    return "intLegalStatus";
                case 115:  // Ground Conditions //
                    return "intGroundType";
                case 116:  // Protection Types //
                    return "intProtectionType";
                case 118:  // Site Hazard Classes //
                    return "intSiteHazardClass";
                case 122:  // Sizes //
                    return "intSize";
                case 123:  // Age Classes //
                    return "intAgeClass";
                case 135:  // Height Bands //
                    return "intHeightRange";
                case 136:  // DBH Bands //
                    return "intDBHRange";
                case 90000:  // DBH //
                    return "DBH";
                case 90001:  // Height //
                    return "Height";
                case 90002:  // Risk Factor //
                    return "RiskFactor";
                case 90003:  // Area M2 [Value is actual M2 but stored in field labeled Ha for Historical Reasons] //
                    return "AreaHa";
                case 90004:  // Number of days since last inspection [stored at Tree level (LastInspectionDate)] //
                    return "LastInspectionElapsedDays";
                default:
                    return "";
            }
        }

        private void Clear_MapInfo_Themes()
        {
            Map map = mapControl1.Map;
            FeatureLayer lyr = map.Layers["MapObjects"] as MapInfo.Mapping.FeatureLayer;
            if (lyr == null) return;

            lyr.Modifiers.Clear();  // Clear any prior themes on the Map Objects layer //
            map.Adornments.Clear();  // Clear any prior map adornments //
            if (intMapObjectTransparency <= 0) return;
        }

        private void Create_MapInfo_Theme()
        {
            Map map = mapControl1.Map;
            FeatureLayer lyr = map.Layers["MapObjects"] as MapInfo.Mapping.FeatureLayer;
            if (lyr == null) return;
            int i = 0;

            // Get number of bins in theme to create //
            if (intUseThematicStyling != 1 || intThematicStylingBasePicklistID <= 0)  // No Thematics so create a single catch all so transparency effect can be applied //
            {
                MapInfo.Mapping.Thematics.IndividualValueTheme thm = new MapInfo.Mapping.Thematics.IndividualValueTheme(lyr, "Thematics", "Thematics", 2);
                thm.ApplyStylePart = StylePart.Color;
                CompositeStyle defaultStyle = Get_Default_Style();
                thm.Bins[0].Style = defaultStyle;
                thm.Bins[0].Value = Convert.ToDouble(9999998);
                // Create Highlighting Bin //
                thm.Bins[1].Style = Get_Highlight_Style();
                thm.Bins[1].Value = Convert.ToDouble(999995);
                thm.AllOthersBin.Style = defaultStyle;
                lyr.Modifiers.Append(thm);

                thm.AllOthersBin.Style = Get_Default_Style();
                lyr.Modifiers.Clear();  // Clear any prior themes on the Map Objects layer //
                lyr.Modifiers.Append(thm);

            }
            else if (intThematicStylingBasePicklistID >= 90000)  // Range Theme //
            {
                DataSet dataSetFiltered = new DataSet();
                dataSetFiltered.Merge(dsThematicStyleItems.Tables[0].Select("ShowInLegend = 1"));

                int intMaxItems = dataSetFiltered.Tables[0].Rows.Count;
                if (intMaxItems < 0) return;

                // Create Theme //
                MapInfo.Mapping.Thematics.RangedTheme thm = new MapInfo.Mapping.Thematics.RangedTheme(lyr, "Thematics", "Thematics", intMaxItems + 1, MapInfo.Mapping.Thematics.DistributionMethod.CustomRanges);
                thm.ApplyStylePart = StylePart.Color;
                thm.SpreadBy = SpreadByPart.None;

                // Create Legend //
                MapInfo.Mapping.Legends.LegendFactory lgdFactory = this.mapControl1.Map.Legends;
                MapInfo.Mapping.Legends.Legend lgd = lgdFactory.CreateLegend(new Size(5, 5));
                MapInfo.Mapping.Legends.CustomLegendFrameRow[] rows = new MapInfo.Mapping.Legends.CustomLegendFrameRow[intMaxItems + 1];
                //MapInfo.Mapping.Legends.AllOthersLegendFrameRow[] rowAllOthers = new AllOthersLegendFrameRow[1];
                lgd.Border = true;
                lgd.BorderPen = new System.Drawing.Pen(System.Drawing.Color.Black, 1f);

                // Populate Theme //
                int intDefaultSymbol = 0;
                int intDefaultSymbolColour = 0;
                int intDefaultSymbolSize = 0;
                int intDefaultPolygonBoundaryColour = 0;
                int intDefaultPolygonBoundaryWidth = 0;
                int intDefaultPolygonFillColour = 0;
                int intDefaultPolygonFillPattern = 0;
                int intDefaultPolygonFillPatternColour = 0;
                int intDefaultLineStyle = 0;
                int intDefaultLineColour = 0;
                int intDefaultLineWidth = 0;

                CompositeStyle style = null;
                foreach (DataRow dr in dataSetFiltered.Tables[0].Rows)
                {
                    intDefaultSymbol = Convert.ToInt32(dr["Symbol"]);
                    intDefaultSymbolColour = Convert.ToInt32(dr["SymbolColour"]);
                    intDefaultSymbolSize = Convert.ToInt32(dr["Size"]);
                    intDefaultPolygonBoundaryColour = Convert.ToInt32(dr["PolygonLineColour"]);
                    intDefaultPolygonBoundaryWidth = Convert.ToInt32(dr["PolygonLineWidth"]);
                    intDefaultPolygonFillColour = Convert.ToInt32(dr["PolygonFillColour"]);
                    intDefaultPolygonFillPattern = Convert.ToInt32(dr["PolygonFillPattern"]);
                    intDefaultPolygonFillPatternColour = Convert.ToInt32(dr["PolygonFillPatternColour"]);
                    intDefaultLineStyle = Convert.ToInt32(dr["PolylineStyle"]);
                    intDefaultLineColour = Convert.ToInt32(dr["PolylineColour"]);
                    intDefaultLineWidth = Convert.ToInt32(dr["PolylineWidth"]);
                    style = new CompositeStyle();
                    if (intDefaultPolygonFillPattern == 2) // Solid //
                    {
                        ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                        ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                    }
                    else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                    {
                        ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                        ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                    }
                    ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
                    ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
                    ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
                    ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);
                    style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
                    style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbolColour))), intDefaultSymbolSize); // Symbol 34 = circle //

                    if (intThematicStylingBasePicklistID >= 90000)  // 90000+ = Thematic Set using Continues Variables [BandStart and BandEnd] //
                    {
                        thm.Bins[i].Min = Convert.ToDouble(dr["StartBand"]);
                        thm.Bins[i].Max = Convert.ToDouble(dr["EndBand"]); ;
                    }
                    else
                    {
                        thm.Bins[i].Min = Convert.ToDouble(dr["PicklistItemID"]);
                        thm.Bins[i].Max = Convert.ToDouble(dr["PicklistItemID"]);
                    }
                    thm.Bins[i].Style = style;
                    i++;  // Increment counter //
                }
                // Create Highlighting Bin //
                thm.Bins[i].Style = Get_Highlight_Style();
                thm.Bins[i].Min = Convert.ToDouble(999990);
                thm.Bins[i].Max = Convert.ToDouble(999999);
                thm.Recompute();  // Recompute Bins and Styles since theybhave changed //

                thm.AllOthersBin.Style = Get_Default_Style();
                lyr.Modifiers.Clear();  // Clear any prior themes on the Map Objects layer //
                lyr.Modifiers.Append(thm);
                /*
                // Populate Legend //
                i = 0;
                for (i = 0; i < thm.Bins.Count - 1; i++)
                {
                    DataRow dr = dataSetFiltered.Tables[0].Rows[i];
                    rows[i] = new CustomLegendFrameRow(new CompositeStyle(thm.Bins[i].Style.AreaStyle, thm.Bins[i].Style.LineStyle, null, thm.Bins[i].Style.SymbolStyle), dr["ItemDescription"].ToString());
                }
                rows[intMaxItems] = new CustomLegendFrameRow(new CompositeStyle(thm.Bins[intMaxItems].Style.AreaStyle, thm.Bins[intMaxItems].Style.LineStyle, null, thm.Bins[intMaxItems].Style.SymbolStyle), "Highlighted");

                MapInfo.Mapping.Legends.LegendFrame thmFrame = MapInfo.Mapping.Legends.LegendFrameFactory.CreateCustomLegendFrame("Legend", "Legend", rows);
                lgd.Frames.Append(thmFrame);
                lgd.BackgroundBrush = new System.Drawing.SolidBrush(Color.White);
                thmFrame.TitleStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.SubTitleStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.RowTextStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.Title = "Legend: " + strThematicStylingFieldName;
                //thmFrame.TitleStyle.FontWeight = FontWeight.Bold;
                map.Adornments.Append(lgd);
                LegendExport lgdExporter = new LegendExport(mapControl1.Map, lgd);
                lgdExporter.Format = ExportFormat.Png;
                pictureBoxLegend.Image = lgdExporter.Export();
                */
            }
            else   // Individual Theme //
            {
                DataSet dataSetFiltered = new DataSet();
                dataSetFiltered.Merge(dsThematicStyleItems.Tables[0].Select("ShowInLegend = 1"));

                int intMaxItems = dataSetFiltered.Tables[0].Rows.Count;
                if (intMaxItems < 0) return;
                //else if (intMaxItems > 30) intMaxItems = 30;  // Max 50, so 49 themes plus 1 for Highlight theme. //

                // Create Theme //
                MapInfo.Mapping.Thematics.IndividualValueTheme thm = new MapInfo.Mapping.Thematics.IndividualValueTheme(lyr, "Thematics", "Thematics", intMaxItems + 1);
                thm.ApplyStylePart = StylePart.Color;

                // Create Legend //
                MapInfo.Mapping.Legends.LegendFactory lgdFactory = this.mapControl1.Map.Legends;
                MapInfo.Mapping.Legends.Legend lgd = lgdFactory.CreateLegend(new Size(5, 5));
                MapInfo.Mapping.Legends.CustomLegendFrameRow[] rows = new MapInfo.Mapping.Legends.CustomLegendFrameRow[intMaxItems + 1];
                //MapInfo.Mapping.Legends.AllOthersLegendFrameRow[] rowAllOthers = new AllOthersLegendFrameRow[1];
                lgd.Border = true;
                lgd.BorderPen = new System.Drawing.Pen(System.Drawing.Color.Black, 1f);

                // Populate Theme //
                int intDefaultSymbol = 0;
                int intDefaultSymbolColour = 0;
                int intDefaultSymbolSize = 0;
                int intDefaultPolygonBoundaryColour = 0;
                int intDefaultPolygonBoundaryWidth = 0;
                int intDefaultPolygonFillColour = 0;
                int intDefaultPolygonFillPattern = 0;
                int intDefaultPolygonFillPatternColour = 0;
                int intDefaultLineStyle = 0;
                int intDefaultLineColour = 0;
                int intDefaultLineWidth = 0;

                CompositeStyle style = null;
                foreach (DataRow dr in dataSetFiltered.Tables[0].Rows)
                {
                    intDefaultSymbol = Convert.ToInt32(dr["Symbol"]);
                    intDefaultSymbolColour = Convert.ToInt32(dr["SymbolColour"]);
                    intDefaultSymbolSize = Convert.ToInt32(dr["Size"]);
                    intDefaultPolygonBoundaryColour = Convert.ToInt32(dr["PolygonLineColour"]);
                    intDefaultPolygonBoundaryWidth = Convert.ToInt32(dr["PolygonLineWidth"]);
                    intDefaultPolygonFillColour = Convert.ToInt32(dr["PolygonFillColour"]);
                    intDefaultPolygonFillPattern = Convert.ToInt32(dr["PolygonFillPattern"]);
                    intDefaultPolygonFillPatternColour = Convert.ToInt32(dr["PolygonFillPatternColour"]);
                    intDefaultLineStyle = Convert.ToInt32(dr["PolylineStyle"]);
                    intDefaultLineColour = Convert.ToInt32(dr["PolylineColour"]);
                    intDefaultLineWidth = Convert.ToInt32(dr["PolylineWidth"]);
                    style = new CompositeStyle();
                    if (intDefaultPolygonFillPattern == 2) // Solid //
                    {
                        ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                        ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                    }
                    else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                    {
                        ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                        ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                    }
                    ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
                    ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
                    ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
                    ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);
                    style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
                    style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbolColour))), intDefaultSymbolSize); // Symbol 34 = circle //

                    thm.Bins[i].Value = Convert.ToDouble(dr["PicklistItemID"]);
                    thm.Bins[i].Style = style;
                    i++;  // Increment counter //
                }
                // Create Highlighting Bin //
                thm.Bins[i].Style = Get_Highlight_Style();
                thm.Bins[i].Value = Convert.ToDouble(999995);

                thm.AllOthersBin.Style = Get_Default_Style();
                lyr.Modifiers.Clear();  // Clear any prior themes on the Map Objects layer //
                lyr.Modifiers.Append(thm);
                /*
                // Populate Legend //
                i = 0;
                for (i = 0; i < thm.Bins.Count - 1; i++)
                {
                    DataRow dr = dataSetFiltered.Tables[0].Rows[i];
                    rows[i] = new CustomLegendFrameRow(new CompositeStyle(thm.Bins[i].Style.AreaStyle, thm.Bins[i].Style.LineStyle, null, thm.Bins[i].Style.SymbolStyle), dr["ItemDescription"].ToString());
                }
                rows[intMaxItems] = new CustomLegendFrameRow(new CompositeStyle(thm.Bins[intMaxItems].Style.AreaStyle, thm.Bins[intMaxItems].Style.LineStyle, null, thm.Bins[intMaxItems].Style.SymbolStyle), "Highlighted");
                
                MapInfo.Mapping.Legends.LegendFrame thmFrame = MapInfo.Mapping.Legends.LegendFrameFactory.CreateCustomLegendFrame("Legend", "Legend", rows);
                lgd.Frames.Append(thmFrame);
                lgd.BackgroundBrush = new System.Drawing.SolidBrush(Color.White);
                thmFrame.TitleStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.SubTitleStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.RowTextStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.Title = "Legend: " + strThematicStylingFieldName;
                //thmFrame.TitleStyle.FontWeight = FontWeight.Bold;
                map.Adornments.Append(lgd);
                LegendExport lgdExporter = new LegendExport(mapControl1.Map, lgd);
                lgdExporter.Format = ExportFormat.Png;
                pictureBoxLegend.Image = lgdExporter.Export();
                */
            }
        }

        private MapInfo.Styles.CompositeStyle Get_Default_Style()
        {
            DataRow drStyle = dsDefaultStyles.Tables[0].Rows[0];
            int intDefaultSymbol = Convert.ToInt32(drStyle["Symbol"]);
            int intDefaultSymbolColour = Convert.ToInt32(drStyle["SymbolColour"]);
            int intDefaultSymbolSize = Convert.ToInt32(drStyle["Size"]);
            int intDefaultPolygonBoundaryColour = Convert.ToInt32(drStyle["PolygonLineColour"]);
            int intDefaultPolygonBoundaryWidth = Convert.ToInt32(drStyle["PolygonLineWidth"]);
            int intDefaultPolygonFillColour = Convert.ToInt32(drStyle["PolygonFillColour"]);
            int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
            int intDefaultPolygonFillPatternColour = Convert.ToInt32(drStyle["PolygonFillPatternColour"]);
            int intDefaultLineStyle = Convert.ToInt32(drStyle["PolylineStyle"]);
            int intDefaultLineColour = Convert.ToInt32(drStyle["PolylineColour"]);
            int intDefaultLineWidth = Convert.ToInt32(drStyle["PolylineWidth"]);

            CompositeStyle style = new CompositeStyle();
            if (intDefaultPolygonFillPattern == 2) // Solid //
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                //((SimpleInterior)style.AreaStyle.Interior).Transparent = true;
            }
            else if (intDefaultPolygonFillPattern == 1)  // Tranparent //  The 50 on the lines below = transparent level [hardcoded for now]
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(75) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(75) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                //((SimpleInterior)style.AreaStyle.Interior).Transparent = true;
            }

            else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                //((SimpleInterior)style.AreaStyle.Interior).Transparent = true;
            }
            ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
            ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
            ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
            ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);

            style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
            style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbolColour))), intDefaultSymbolSize); // Symbol 34 = circle //
            
            return style;
        }

        private MapInfo.Styles.CompositeStyle Get_Thematic_Style(int intPicklistValue, decimal decFieldValue)
        {
            if (intThematicStylingBasePicklistID == 0) return Get_Default_Style();
            CompositeStyle style = null;
            DataRow[] drFiltered;
            if (intThematicStylingBasePicklistID >= 90000)  // 90000+ = Thematic Set using Continues Variables [BandStart and BandEnd] //
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("StartBand <= " + decFieldValue.ToString() + " and EndBand >= " + decFieldValue.ToString());
            }
            else
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("PicklistItemID = " + intPicklistValue.ToString());
            }
            if (drFiltered.Length != 0)
            {
                style = new CompositeStyle();
                // Set Style here //
                DataRow drStyle = drFiltered[0];
                int intDefaultSymbol = Convert.ToInt32(drStyle["Symbol"]);
                int intDefaultSymbolColour = Convert.ToInt32(drStyle["SymbolColour"]);
                int intDefaultSymbolSize = Convert.ToInt32(drStyle["Size"]);
                int intDefaultPolygonBoundaryColour = Convert.ToInt32(drStyle["PolygonLineColour"]);
                int intDefaultPolygonBoundaryWidth = Convert.ToInt32(drStyle["PolygonLineWidth"]);
                int intDefaultPolygonFillColour = Convert.ToInt32(drStyle["PolygonFillColour"]);
                int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
                int intDefaultPolygonFillPatternColour = Convert.ToInt32(drStyle["PolygonFillPatternColour"]);
                int intDefaultLineStyle = Convert.ToInt32(drStyle["PolylineStyle"]);
                int intDefaultLineColour = Convert.ToInt32(drStyle["PolylineColour"]);
                int intDefaultLineWidth = Convert.ToInt32(drStyle["PolylineWidth"]);

                if (intDefaultPolygonFillPattern == 2) // Solid //
                {
                    ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                }
                else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                {
                    ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                }
                ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
                ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
                ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
                ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);

                style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
                style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbolColour))), intDefaultSymbolSize); // Symbol 34 = circle //
            }
            return (style == null ? Get_Default_Style() : style);
        }

        private MapInfo.Styles.CompositeStyle Get_Highlight_Style()
        {
            // Get a Highlight Style - base it on the default style then adjust the colours to match that on the highlight colour drop down //
            System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
            CompositeStyle style = Get_Default_Style();
            if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)
            {
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            }
            else
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            }
            style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            ((SimpleLineStyle)style.LineStyle).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            return style;
        }

        public void Update_Map_Object_Grid_Visible_And_Highlighted()  // Public so it can be called by the Mapping_Functions Class //
        {
            char[] delimiters = new char[] { ',' };
            string[] strArray = strVisibleIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length > 0)
            {
                GridView view = view = (GridView)gridControl2.MainView;
                GridColumn column = view.Columns["TreeID"];
                       
                int intFoundRow = 0;
                view.BeginUpdate();
                int intLastFoundRow = GridControl.InvalidRowHandle;
                for (int i = 0; i < strArray.Length; i++)
                {
                    intFoundRow = view.LocateByValue(0, column, Convert.ToInt32(strArray[i]));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                        intLastFoundRow = intFoundRow;
                    }
                }
                if (selection1.SelectedCount > 0 || selectionAssets.SelectedCount > 0)
                {
                    UpdateMapObjectsDisplayed(false);  // false for no progress screen as we already have a progress screen open //
                }
                view.EndUpdate();
                // Move map to last found row only if no passed IDs for highlighting otherwise moving / highlighting / scaling done in next process //
                if (strHighlightedIDs == "")
                {
                    string strMapID = view.GetRowCellValue(intLastFoundRow, "MapID").ToString();
                    System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
                    double MinX = (double)9999999.00;  // Used for Zooming to Scale //
                    double MinY = (double)9999999.00;
                    double MaxX = (double)-9999999.00;
                    double MaxY = (double)-9999999.00;
                    FindAndHighlight(strMapID, true, false, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                }
            }
            strArray = strVisibleAssetIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length > 0)
            {
                GridView view = view = (GridView)gridControlAssetObjects.MainView;
                GridColumn column = view.Columns["AssetID"];

                int intFoundRow = 0;
                view.BeginUpdate();
                int intLastFoundRow = GridControl.InvalidRowHandle;
                for (int i = 0; i < strArray.Length; i++)
                {
                    intFoundRow = view.LocateByValue(0, column, Convert.ToInt32(strArray[i]));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                        intLastFoundRow = intFoundRow;
                    }
                }
                if (selection1.SelectedCount > 0 || selectionAssets.SelectedCount > 0)
                {
                    UpdateMapObjectsDisplayed(false);  // false for no progress screen as we already have a progress screen open //
                }
                view.EndUpdate();
                // Move map to last found row only if no passed IDs for highlighting otherwise moving / highlighting / scaling done in next process //
                if (strHighlightedAssetIDs == "")
                {
                    string strMapID = view.GetRowCellValue(intLastFoundRow, "MapID").ToString();
                    System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
                    double MinX = (double)9999999.00;  // Used for Zooming to Scale //
                    double MinY = (double)9999999.00;
                    double MaxX = (double)-9999999.00;
                    double MaxY = (double)-9999999.00;
                    FindAndHighlight(strMapID, true, false, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                }
            }

            int intHighlightedCount = 0;
            if (strHighlightedIDs != "")
            {
                strArray = strHighlightedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length > 0)
                {
                    GridView view = (GridView)gridControl2.MainView;
                    GridColumn column = view.Columns["TreeID"];
                    int intFoundRow = 0;
                    view.BeginUpdate();
                    int intLastFoundRow = GridControl.InvalidRowHandle;
                    for (int i = 0; i < strArray.Length; i++)
                    {
                        intFoundRow = view.LocateByValue(0, column, Convert.ToInt32(strArray[i]));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "Highlight", 1);
                            view.MakeRowVisible(intFoundRow, false);
                            intLastFoundRow = intFoundRow;
                            intHighlightedCount++;
                        }
                    }
                   view.EndUpdate();
                }
            }

            if (strHighlightedAssetIDs != "")
            {
                strArray = strHighlightedAssetIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length > 0)
                {
                    GridView view = (GridView)gridControlAssetObjects.MainView;
                    GridColumn column = view.Columns["AssetID"];
                    int intFoundRow = 0;
                    view.BeginUpdate();
                    int intLastFoundRow = GridControl.InvalidRowHandle;
                    for (int i = 0; i < strArray.Length; i++)
                    {
                        intFoundRow = view.LocateByValue(0, column, Convert.ToInt32(strArray[i]));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "Highlight", 1);
                            view.MakeRowVisible(intFoundRow, false);
                            intLastFoundRow = intFoundRow;
                            intHighlightedCount++;
                        }
                    }
                    view.EndUpdate();
                }
            }
            if (selection1.SelectedCount > 0 || selectionAssets.SelectedCount > 0)
            {
                UpdateMapObjectsDisplayed(false);  // false for no progress screen as we already have a progress screen open //
            }
            if (intHighlightedCount > 0)
            {
                //if (i_str_selected_workorders != "")  // Work Order Passed In //
                //{
                ceCentreMap.Checked = false;
                csScaleMapForHighlighted.Checked = true;
                //}
                Highlight_Clicked(false, intHighlightedCount);
                PreventDefaultMapScaleUse = true;  // Stop default Scale and centre point from being set in Load_Map_Layers() event //
            }


        }

        private void LoadMapObjects()
        {
            CompositeStyle style = Get_Default_Style();

            //*** Create Temporary Tab file to hold points, polygons and polylines etc ***** //
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
            if (t != null)
            {
                if (t.IsOpen) t.Close();
            }
            TableInfoMemTable tiTemp = new TableInfoMemTable("MapObjects");
            tiTemp.Temporary = true;
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedStringColumn("id", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("TreeID"));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("TreeRef", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ClientName", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("SiteName", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("Species", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("intX"));
            tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("intY"));
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedIntColumn("intObjectType"));  // 0 = Point, 1 = Polygon, 2 = Polyline //
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedIntColumn("intHighlighted"));  // 0 = No, 1 = Yes //
            tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("CrownDiameter"));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("CalculatedSize", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("CalculatedPerimeter", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateDecimalColumn("RiskFactor", 18, 2));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ShortTreeRef", 50));

            // New Columns //
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedIntColumn("ModuleID"));  // 1 = Amenity Trees, 2 = Assets //
            tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("AssetID"));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("AssetNumber", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("PartNumber", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ModelNumber", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("SerialNumber", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ClientName", 100));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ClientCode", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("SiteName", 100));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("SiteCode", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("AssetType", 100));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("AssetSubType", 100));
            tiTemp.Columns.Add(ColumnFactory.CreateDoubleColumn("Thematics"));


            tiTemp.Columns.Add(ColumnFactory.CreateStyleColumn());

            CoordSysFactory factory = Session.Current.CoordSysFactory;
            CoordSys BritishGrid = factory.CreateFromMapBasicString(strDefaultMappingProjection);
            //CoordSys BritishGrid = factory.CreateFromMapBasicString("CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000");
            tiTemp.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(BritishGrid));

            // Note: No need to add a column of type Key. Every table automatically contains a column named "MI_Key". //
            Table table = Session.Current.Catalog.CreateTable(tiTemp);  // This layer not yet loaded into map - done later in process //

            // Check if any map objects need to be pre-loaded, if yes then load them... //
            Update_Map_Object_Grid_Visible_And_Highlighted();

            /*
                   // ***** Load Polygons and Polylines ***** //
                   SqlConnection SQlConn = new SqlConnection(strConnectionString);
                   SqlCommand cmd = null;
                   cmd = new SqlCommand("sp01250_AT_Tree_Picker_load_polygons", SQlConn);
                   cmd.CommandType = CommandType.StoredProcedure;
                   cmd.Parameters.Add(new SqlParameter("@ClientIDs", strClientIDs));
                   SqlDataAdapter sdaDataAdpter = new SqlDataAdapter();
                   DataSet dsDataSet = new DataSet("NewDataSet");
                   sdaDataAdpter = new SqlDataAdapter(cmd);
                   sdaDataAdpter.Fill(dsDataSet, "Table");
                   //SQlConn.Close();
                   //SQlConn.Dispose();
                   //if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

                   int intTreeID = 0;
                   string strID = "";
                   string strTreeRef = "";
                   string strPolygonXY = "";
                   string strClientName = "";
                   string strSiteName = "";
                   string strSpecies = "";
                   int intCrownDiameter = 0;
                   string[] strArray;
                   string strCalculatedSize = "";

                   char[] delimiters = new char[] { ';' };
                   int iPoint = 0;

                   t = conn.Catalog.GetTable("MapObjects");
                   t.BeginAccess(MapInfo.Data.TableAccessMode.Write);

                   foreach (DataRow dr in dsDataSet.Tables[0].Rows)
                   {
                       strID = Convert.ToString(dr["MapID"]);
                       intTreeID = Convert.ToInt32(dr["TreeID"]);
                       strTreeRef = Convert.ToString(dr["TreeReference"]);
                       strPolygonXY = Convert.ToString(dr["PolygonXY"]);
                       strClientName = Convert.ToString(dr["ClientName"]);
                       strSiteName = Convert.ToString(dr["SiteName"]);
                       strSpecies = Convert.ToString(dr["Species"]);
                       intTreeID = Convert.ToInt32(dr["TreeID"]);
                       intCrownDiameter = Convert.ToInt32(dr["CrownDiameter"]);
                       // Check there is an even number of points //
                       strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                       if (strArray.Length % 2 != 0)  // Modulus (%) //
                       {
                           DevExpress.XtraEditors.XtraMessageBox.Show("Invalid Polygon\\Polyline in database - MapID: " + strID + ".\r\nInvalid Number of Coordinates - Object ignored!", "Load Map Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                       }
                       else
                       {
                           MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                           iPoint = 0;
                           for (int j = 0; j < strArray.Length - 1; j++)
                           {
                               dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                               j++;  // Jump ahead so that we are always processing odd numbered array items //
                               iPoint++;
                           }
                           MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                           if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                           {
                               MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                               f.Geometry = g;
                               f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                               f["intObjectType"] = 1;  // 1 = Polygon //
                           }
                           else
                           {
                               MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                               f.Geometry = g;
                               f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                               f["intObjectType"] = 2;  // 2 = PolyLine //
                           }
                           f["id"] = strID;
                           f["TreeID"] = intTreeID;
                           f["TreeRef"] = strTreeRef;
                           f["ClientName"] = strClientName;
                           f["SiteName"] = strSiteName;
                           f["Species"] = strSpecies;
                           f["CrownDiameter"] = intCrownDiameter;
                           f.Style = style;
                           f["intHighlighted"] = 0;
                           MapInfo.Data.Key k = t.InsertFeature(f);
                       }
                   }
                   conn.Close();
                   conn.Dispose();


                   // ***** Load Points ***** //
                   cmd = new SqlCommand("sp01251_AT_Tree_Picker_load_points", SQlConn);
                   cmd.CommandType = CommandType.StoredProcedure;
                   cmd.Parameters.Add(new SqlParameter("@ClientIDs", strClientIDs));

                   dsDataSet = new DataSet("NewDataSet");
                   sdaDataAdpter = new SqlDataAdapter(cmd);
                   sdaDataAdpter.Fill(dsDataSet, "Table");
                   SQlConn.Close();
                   SQlConn.Dispose();
                   //if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

                   double dX = 0;
                   double dY = 0;

                   MIConnection MapConnection = new MIConnection();
                   MapConnection.Open();
                   MICommand MapCmd = MapConnection.CreateCommand();
                   MapCmd.Parameters.Add("geometry", MIDbType.FeatureGeometry);
                   MapCmd.Parameters.Add("style", MIDbType.Style);
                   MapCmd.Parameters.Add("MapID", MIDbType.String);
                   MapCmd.Parameters.Add("TreeID", MIDbType.Int);
                   MapCmd.Parameters.Add("TreeRef", MIDbType.String);
                   MapCmd.Parameters.Add("ClientName", MIDbType.String);
                   MapCmd.Parameters.Add("SiteName", MIDbType.String);
                   MapCmd.Parameters.Add("Species", MIDbType.String);
                   MapCmd.Parameters.Add("ObjectType", MIDbType.Int);
                   MapCmd.Parameters.Add("Highlighted", MIDbType.Int);
                   MapCmd.Parameters.Add("CrownDiameter", MIDbType.Int);
                   MapCmd.Parameters.Add("CalculatedSize", MIDbType.String);
                   MapCmd.CommandText = "Insert Into MapObjects (MI_Geometry, MI_Style, id, TreeID, TreeRef, ClientName, SiteName, Species, intObjectType, intHighlighted, CrownDiameter, CalculatedSize) values (geometry, style, MapID, TreeID, TreeRef, ClientName, SiteName, Species, ObjectType, Highlighted, CrownDiameter, CalculatedSize)";
                   MapCmd.Prepare();
                   int nchanged = 0;
                   foreach (DataRow dr in dsDataSet.Tables[0].Rows)
                   {
                       strID = Convert.ToString(dr["MapID"]);
                       intTreeID = Convert.ToInt32(dr["TreeID"]);
                       strTreeRef = Convert.ToString(dr["TreeReference"]);
                       dX = Convert.ToDouble(dr["XCoordinate"]);
                       dY = Convert.ToDouble(dr["YCoordinate"]);
                       strClientName = Convert.ToString(dr["ClientName"]);
                       SiteName = Convert.ToString(dr["SiteName"]);
                       strSpecies = Convert.ToString(dr["Species"]);
                       intCrownDiameter = Convert.ToInt32(dr["CrownDiameter"]);
                       strCalculatedSize = "";

                       MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), dX, dY);
                       //MapInfo.Styles.SimpleVectorPointStyle vs = new MapInfo.Styles.SimpleVectorPointStyle(35, System.Drawing.Color.Green, 14); // Symbol 35 = circle //
                       //MapInfo.Styles.CompositeStyle cs = new MapInfo.Styles.CompositeStyle(vs);
            
                       //Update the table with the location and style of the new feature
                       MapCmd.Parameters[0].Value = g;
                       MapCmd.Parameters[1].Value = style;
                       MapCmd.Parameters[2].Value = strID;
                       MapCmd.Parameters[3].Value = intTreeID;
                       MapCmd.Parameters[4].Value = strTreeRef;
                       MapCmd.Parameters[5].Value = strClientName;
                       MapCmd.Parameters[6].Value = strSiteName;
                       MapCmd.Parameters[7].Value = strSpecies;
                       MapCmd.Parameters[8].Value = 0;  // 0 = Point, 1 = Polygon, 2 = Polyline //
                       MapCmd.Parameters[9].Value = 0;  // 0 = Not Highlighted, 1= Highlighted //
                       MapCmd.Parameters[10].Value = intCrownDiameter;
                       MapCmd.Parameters[11].Value = strCalculatedSize;
                       nchanged = MapCmd.ExecuteNonQuery();
                   }

                   t.EndAccess();
                   MapConnection.Close();
                   MapConnection.Dispose();
                   MapCmd.Dispose();

           */
            conn.Close();
            conn.Dispose();

            mapControl1.Map.Load(new MapTableLoader(table)); // Load Object Layer now it has been populated //
        }

        private void CreateScaleBar()
        {
            // NO LONGER USED - MANUALLY GENERATED USING GDI+ //
            /*
            if (ceScaleBar.Checked)
            {
                // Create a scalebar 
                ScaleBarAdornment sba = new ScaleBarAdornment(mapControl1.Map);

                // Position the scalebar at the lower right corner of map
                int x = mapControl1.Map.Size.Width - sba.Size.Width;
                int y = mapControl1.Map.Size.Height - sba.Size.Height;
                sba.Location = new System.Drawing.Point(x, y);
                
                // Add the control to the map
                ScaleBarAdornmentControl sbac = new ScaleBarAdornmentControl(sba, mapControl1.Map);
                
                mapControl1.AddAdornment(sba, sbac);
            }
            else
            {
                // Remove Scale Bar if present //

            }
            */
        }

        private void OpenShapeFile()
        {

            // Open Shape Files //
            TableInfoShapefile ti = new TableInfoShapefile("Clarence_OS_polyline [EsriShape]");
            ti.TablePath = @"C:\Map\TreePicker\ESRI\Clarence_OS_polyline.shp";
            MapInfo.Geometry.CoordSysFactory CSysFactory = Session.Current.CoordSysFactory;

            
            MapInfo.Geometry.CoordSys coordSys = CSysFactory.CreateCoordSys("mapinfo:" + strDefaultMappingProjection + " Bounds (0,0) (5000000,5000000)");
            //MapInfo.Geometry.CoordSys coordSys = CSysFactory.CreateCoordSys("mapinfo:CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000 Bounds (0,0) (5000000,5000000)");
            /*MapInfo.Geometry.CoordSysFactory miCF = new MapInfo.Geometry.CoordSysFactory();
            MapInfo.Data.SpatialSchemaXY miSpS = new MapInfo.Data.SpatialSchemaXY();
            miSpS.XColumn = "intX";  // *** HOW DO I Get polygons to link? *** //
            miSpS.YColumn = "intY";
            string mbCoordSys = strDefaultMappingProjection + " Bounds (0,0) (5000000,5000000)";
            miSpS.CoordSys = miCF.CreateFromMapBasicString(mbCoordSys);
            ti.SpatialSchema = miSpS;
            */


            ti.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(coordSys));
            //ti.Columns.Add(ColumnFactory.CreateStringColumn("State", 2));
            //ti.Columns.Add(ColumnFactory.CreateStringColumn("State_name", 20));

            //ti.Columns.Add(ColumnFactory.CreateStyleColumn()); 
            ti.DefaultStyle = new MapInfo.Styles.AreaStyle(

            new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(2.0, MapInfo.Styles.LineWidthUnit.Pixel), (int)MapInfo.Styles.PatternStyle.Solid, Color.Red),

            new MapInfo.Styles.SimpleInterior((int)MapInfo.Styles.PatternStyle.Cross, Color.Red, Color.White));

            Table table = Session.Current.Catalog.OpenTable(ti);

            MapTableLoader tl = new MapTableLoader(table);
            mapControl1.Map.Load(tl);



        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;
            bbiBlockAddActionsToInspections.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;

            switch (i_int_FocusedGrid)
            {
                case 6:  // Linked Inspections Grid //
                    view = (GridView)gridControl6.MainView;
                    if (view == null) return;
                    intRowHandles = view.GetSelectedRows();
                    if (intRowHandles.Length >= 1)
                    {
                        bbiDelete.Enabled = iBool_AllowDelete;
                        if (iBool_AllowDelete) alItems.Add("iDelete");
 
                        bsiEdit.Enabled = iBool_AllowEdit;
                        bbiSingleEdit.Enabled = iBool_AllowEdit;
                        if (iBool_AllowDelete) alItems.AddRange(new string[] { "iEdit", "sbiEdit" });

                        bbiBlockAddActionsToInspections.Enabled = iBool_AllowAdd;
                        if (intRowHandles.Length >= 2 && iBool_AllowEdit)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (i_intMapObjectsSelected >= 1 && iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    /*else if (i_intMapObjectsSelected > 1)
                    {
                        alItems.AddRange(new string[] { "iBlockAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiBlockAdd.Enabled = true;
                    }*/
                    break;
                case 7:  // Linked Actions Grid //
                    view = (GridView)gridControl7.MainView;
                    if (view == null) return;
                    intRowHandles = view.GetSelectedRows();
                    if (intRowHandles.Length >= 1)
                    {
                        bbiDelete.Enabled = iBool_AllowDelete;
                        if (iBool_AllowDelete) alItems.Add("iDelete");

                        bsiEdit.Enabled = iBool_AllowEdit;
                        bbiSingleEdit.Enabled = iBool_AllowEdit;
                        if (iBool_AllowDelete) alItems.AddRange(new string[] { "iEdit", "sbiEdit" });

                        if (intRowHandles.Length >= 2 && iBool_AllowEdit)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (i_intMapObjectsSelected >= 1 && iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    /*else if (i_intMapObjectsSelected > 1)
                    {
                        alItems.AddRange(new string[] { "iBlockAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiBlockAdd.Enabled = true;
                    }*/
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView6 navigator custom buttons //
            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (i_intMapObjectsSelected >= 1 && iBool_AllowAdd ? true : false);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 && iBool_AllowEdit ? true : false);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 && iBool_AllowDelete ? true : false);

            // Set enabled status of GridView7 navigator custom buttons //
            view = (GridView)gridControl7.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (i_intMapObjectsSelected >= 1 && iBool_AllowAdd ? true : false);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 && iBool_AllowEdit ? true : false);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 && iBool_AllowDelete ? true : false);
        }


        #region Grid View Generic Events

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView4":
                    seUserDefinedScale.Value = 0;  // Clear User Defined Value since grid selection used //
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region Mouse and Keyboard Hook Events

        void keyboardHook_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (Form.ActiveForm != null)  // This line prevents code from firing the event from other running applications. [Excluding the next IF will allow any form regardless of which is active to fire the code below]. //
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            if (mapControl1.Tools.CurrentTool == null) return;
            if (mapControl1.Tools.CurrentTool.ToString() == "WoodPlan5.PSGDistanceTool")
            {
                if (e.KeyChar.ToString() == "d")  // Show Total measured distance //
                {
                    XtraMessageBox.Show("Total Distance Measured = " + dMeasuredDistance.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(MapInfo.Geometry.DistanceUnit.Meter) + ".", "Measure", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (e.KeyChar.ToString() == "p")  // Plot Point //
                {
                    System.Drawing.Point ptCursor = new System.Drawing.Point(Cursor.Position.X, Cursor.Position.Y);
                    System.Drawing.Point ptMap = mapControl1.PointToClient(ptCursor);
                    MapInfo.Geometry.DPoint newPoint = new DPoint(0, 0);
                    MapInfo.Geometry.DisplayTransform converter = this.mapControl1.Map.DisplayTransform;
                    converter.FromDisplay(new PointF(ptMap.X, ptMap.Y), out newPoint);
                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), newPoint.x, newPoint.y);
                    int intSuccess = AddObjectToMap(g);  // Add object to map //
                }
                else if (e.KeyChar.ToString() == "m")  // Plot Map Marker //
                {
                    System.Drawing.Point ptCursor = new System.Drawing.Point(Cursor.Position.X, Cursor.Position.Y);
                    System.Drawing.Point ptMap = mapControl1.PointToClient(ptCursor);
                    MapInfo.Geometry.DPoint newPoint = new DPoint(0, 0);
                    MapInfo.Geometry.DisplayTransform converter = this.mapControl1.Map.DisplayTransform;
                    converter.FromDisplay(new PointF(ptMap.X, ptMap.Y), out newPoint);
                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(Convert.ToInt32(newPoint.x)), Convert.ToDouble(Convert.ToInt32(newPoint.y)));
                    Add_Map_Marker(newPoint.x, newPoint.y);
                }
                else if (e.KeyChar.ToString() == "s")  // Snap //
                {
                    ToggleSnap();  // toggle snap mode if user presses the 'S' key //
                }
            }
        }

        void keyboardHook_KeyUp(object sender, KeyEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.KeyCode.ToString();
        }

        void keyboardHook_KeyDown(object sender, KeyEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.KeyCode.ToString();
        }

        void mouseHook_MouseWheel(object sender, MouseEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.Delta.ToString();
        }

        void mouseHook_MouseUp(object sender, MouseEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.Delta.ToString();

            if (e.Button == MouseButtons.Right)
            {
                IntPtr hWnd = WindowFromPoint(Control.MousePosition);
                if (hWnd != IntPtr.Zero)
                {
                    Control ctl = Control.FromHandle(hWnd);
                    if (ctl != null)
                    {
                        if (ctl.Name == "mapControl1")
                        {
                            if (mapControl1.Tools.LeftButtonTool != null)
                            {
                                string strCurrentTool = mapControl1.Tools.LeftButtonTool;
                                if (strCurrentTool == "Distance" || strCurrentTool == "MapMarkerTool" || strCurrentTool == "AddPoint" || strCurrentTool == "AddPolygon" || strCurrentTool == "AddPolyline")
                                {
                                    // The Current tool won't disengage to allow the menu to process the mouse over [highlighting current menu item], so following takes care of it //
                                    // Switch tool to the Pan tool while menu is shown then switch back again afterwards //
                                    mapControl1.Tools.LeftButtonTool = "Pan";
                                    this.ActiveControl = panelContainer1;
                                    ShowMapControl1Menu(e);
                                    mapControl1.Tools.LeftButtonTool = strCurrentTool;
                                    return;

                                }
                                else
                                {
                                    ShowMapControl1Menu(e);
                                }
                            }
                            else
                            {
                                ShowMapControl1Menu(e);
                            }                           
                        }
                    }
                }
            }
        }

        void mouseHook_MouseDown(object sender, MouseEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.Delta.ToString();

        }

        void mouseHook_MouseMove(object sender, MouseEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            int intX = e.X;
            int intY = e.Y;

        }

        private void ShowMapControl1Menu(MouseEventArgs e)
        {
            bbiGazetteerShowMatch.Enabled = true;

            beiGazetteerLoadObjectsWithinRange.Enabled = true;
            beiGazetteerLoadObjectsWithinRange.Visibility = BarItemVisibility.Always;  // Show Range Spinner menu item //

            bbiGazetteerLoadMapObjectsInRange.Enabled = true;
            bbiGazetteerLoadMapObjectsInRange.Caption = "Load  Map Objects in Range";

            bbiGazetteerClearMapSearch.Visibility = BarItemVisibility.Always;
            

            if (e != null) popupMenu_MapControl1.ShowPopup(new System.Drawing.Point(e.X, e.Y));  // May be null if triggered from barSubItemMapEdit_Popup //
        }

        private void popupMenu_MapControl1_Popup(object sender, EventArgs e)
        {
            // Used to prevent tracking of current mouse position on map when right click menu of Map displayed [used on Set Site \ Client Centre Point] //
            boolTrackXandY = false;

            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            int intMapObjectCount = 0;
            int intTempMapObjectCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        intMapObjectCount++;
                    }
                }
                else if (irfc.BaseTable.Alias == "TempMapObjects")  // Ignore any selectable layers except TempMapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        intTempMapObjectCount++;
                    }
                }
            }
            bsiEditMapObject.Enabled = (intMapObjectCount > 0 && iBool_AllowEdit ? true : false);
            bbiEditSelectedMapObjects.Enabled = (intMapObjectCount > 0 ? true : false);
            bbiBlockEditselectedMapObjects.Enabled = (intMapObjectCount > 1 && iBool_AllowEdit ? true : false);
            bsiAddToMapObjects.Enabled = (intMapObjectCount > 0 && iBool_AllowAdd ? true : false);
            bbiAddInspection.Enabled = (intMapObjectCount > 0 && iBool_AllowAdd ? true : false);
            bbiAddAction.Enabled = (intMapObjectCount > 0 && iBool_AllowAdd ? true : false);
            bbiDeleteMapObjects.Enabled = (intMapObjectCount > 0 && iBool_AllowDelete ? true : false);
            bbiCreateDatasetFromMapObjects.Enabled = (intMapObjectCount > 0 ? true : false);
            bbiTransferToWorkOrder.Enabled = (intMapObjectCount > 0 && iBool_AllowEdit ? true : false);
            bbiDeleteSelectedTempMapObjects.Enabled = (intTempMapObjectCount > 0 ? true : false);
            bbiCreateIncidentFromMap.Enabled = iBool_AllowAdd;

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("Type = 'MapMarker'");
            MapInfo.Data.IResultSetFeatureCollection irfc2 = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            conn.Close();
            conn.Dispose();
            bbiCreatePolygonFromMapMarkers.Enabled = (irfc2.Count >= 3 && iBool_AllowAdd ? true : false);
            bbiCreatePolylineFromMapMarkers.Enabled = (irfc2.Count >= 2 && iBool_AllowAdd ? true : false);
            bbiDeleteAllMapMarkers.Enabled = (irfc2.Count <= 0 ? false : true);
        }

        private void popupMenu_MapControl1_CloseUp(object sender, EventArgs e)
        {
            // Used to prevent tracking of current mouse position on map when right click menu of Map displayed [used on Set Site \ Client Centre Point] //
            boolTrackXandY = true;
        }

        #endregion


        #region Distance Measurer

        private void psgDistTool_PSGDistanceToolPointAdded(object sender, WoodPlan5.PSGDistanceToolEventArgs e)
        {
            MapInfo.Geometry.DistanceUnit unit = MapInfo.Geometry.DistanceUnit.Meter;
            Double dblDist = System.Convert.ToDouble(String.Format("{0:N2}", e.GetCurrentLineLength(unit, MapInfo.Geometry.DistanceType.Spherical)));
            bsiDistanceMeasured.Caption = "Measured Distance: " + dblDist.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(unit);
            dMeasuredDistance = dblDist;
        }

        private void psgDistTool_PSGDistanceToolMove(object sender, WoodPlan5.PSGDistanceToolEventArgs e)
        {
            MapInfo.Geometry.DistanceUnit unit = MapInfo.Geometry.DistanceUnit.Meter;
            Double dblLastDist = System.Convert.ToDouble(String.Format("{0:N2}", e.GetLastSegmentLength(unit, MapInfo.Geometry.DistanceType.Spherical)));
            Double dblDist = System.Convert.ToDouble(String.Format("{0:N2}", e.GetCurrentLineLength(unit, MapInfo.Geometry.DistanceType.Spherical)));
            bsiDistanceMeasured.Caption = "Measured Distance: " + dblDist.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(unit) + " - Last Segment: " + dblLastDist.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(unit);
            dMeasuredDistance = dblDist;

            if (e.IsFinished) XtraMessageBox.Show("Total Distance Measured = " + dblDist.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(unit) + ".", "Measure", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion


        #region Layer Manager

        public void Load_Layer_Manager()
        {
            beWorkspace.EditValue = strLoadedWorkspaceName;
            beWorkspace.Properties.Buttons[1].Enabled = (intLoadedWorkspaceOwner == GlobalSettings.UserID || GlobalSettings.PersonType == 0 ? true : false);
            this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01288_AT_Tree_Picker_Workspace_layers_list, intLoadedWorkspaceID, strMappingFilesPath);

            // Get any default thematic style set specified for the layer //
            int intMapObjectLayerRowID = GetLayerManagerMapObjectsLayerRow();
            if (intMapObjectLayerRowID >= 0)
            {
                GridView view = (GridView)gridControl5.MainView;
                intThematicStylingFieldHeaderID = Convert.ToInt32(view.GetRowCellValue(intMapObjectLayerRowID, "ThematicStyleSet"));
                intUseThematicStyling = Convert.ToInt32(view.GetRowCellValue(intMapObjectLayerRowID, "UseThematicStyling"));

                if (intThematicStylingFieldHeaderID > 0)
                {
                    intThematicStylingFieldHeaderType = 1;  // 0 = System Picklist, 1 = Saved Thematic Style Set //

                    // Populate Thematic Styling - Get Defaults to pass through to SP first then trigger event to load data //
                    int intDefaultSymbol = 0;
                    int intDefaultSymbolColour = 0;
                    int intDefaultSymbolSize = 0;
                    int intDefaultPolygonBoundaryColour = 0;
                    int intDefaultPolygonBoundaryWidth = 0;
                    int intDefaultPolygonFillColour = 0;
                    int intDefaultPolygonFillPattern = 0;
                    int intDefaultPolygonFillPatternColour = 0;
                    int intDefaultLineStyle = 0;
                    int intDefaultLineColour = 0;
                    int intDefaultLineWidth = 0;
                    DataRow dr = dsDefaultStyles.Tables[0].Rows[0];
                    intDefaultSymbol = Convert.ToInt32(dr["Symbol"]);
                    intDefaultSymbolColour = Convert.ToInt32(dr["SymbolColour"]);
                    intDefaultSymbolSize = Convert.ToInt32(dr["Size"]);
                    intDefaultPolygonBoundaryColour = Convert.ToInt32(dr["PolygonLineColour"]);
                    intDefaultPolygonBoundaryWidth = Convert.ToInt32(dr["PolygonLineWidth"]);
                    intDefaultPolygonFillColour = Convert.ToInt32(dr["PolygonFillColour"]);
                    intDefaultPolygonFillPattern = Convert.ToInt32(dr["PolygonFillPattern"]);
                    intDefaultPolygonFillPatternColour = Convert.ToInt32(dr["PolygonFillPatternColour"]);
                    intDefaultLineStyle = Convert.ToInt32(dr["PolylineStyle"]);
                    intDefaultLineColour = Convert.ToInt32(dr["PolylineColour"]);
                    intDefaultLineWidth = Convert.ToInt32(dr["PolylineWidth"]);
                    ChildForm_RefreshThematicItemDataSet(intThematicStylingFieldHeaderID, intThematicStylingFieldHeaderType, intDefaultSymbol, intDefaultSymbolColour, intDefaultSymbolSize, intDefaultPolygonBoundaryColour, intDefaultPolygonBoundaryWidth, intDefaultPolygonFillColour, intDefaultPolygonFillPattern, intDefaultPolygonFillPatternColour, intDefaultLineStyle, intDefaultLineColour, intDefaultLineWidth);

                    // Get Owner of SavedStyleSet if it is a SavedStyleSet //
                    if (intThematicStylingFieldHeaderID <= 0 || intThematicStylingFieldHeaderType == 0)  // StyleSet is a dynamic one [not Saved] //
                    {
                        intLoadedStyleSetOwnerID = 0;
                    }
                    else
                    {
                        DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter GetOwner = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                        GetOwner.ChangeConnectionString(strConnectionString);
                        try
                        {
                            string strIDandOwner = "";
                            strIDandOwner = GetOwner.sp01291_AT_Tree_Picker_Style_Get_owner(intThematicStylingFieldHeaderID).ToString();
                            intLoadedStyleSetOwnerID = Convert.ToInt32(strIDandOwner.Substring(0, strIDandOwner.IndexOf('|')));
                            strThematicStylingFieldName = strIDandOwner.Substring(strIDandOwner.IndexOf('|') + 1);

                            // Get Transparency Level //
                            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter GetTransparency = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                            GetTransparency.ChangeConnectionString(strConnectionString);
                            intMapObjectTransparency = Convert.ToInt32(GetTransparency.sp03077_AT_Tree_Picker_Get_Transparency(intThematicStylingFieldHeaderID));
                        }
                        catch (Exception)
                        {
                            intLoadedStyleSetOwnerID = 0;
                            strThematicStylingFieldName = "";
                        }
                    }
                }
                else  // Clear Thematics //
                {
                    dsThematicStyleItems.Clear();
                }
            }

            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit, intLoadedWorkspaceID);

            Load_Map_Layers();

            // Update Legend //
            if (dockPanel5.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden)
            {
                if (intUseThematicStyling != 0)
                {
                    Create_Legend();
                }
                else  // No Thematics on so hide legend and uncheck Lengend button on toolbar //
                {
                    bbiLegend.Checked = false;
                }
            }
        }

        private void Load_Map_Layers()
        {
            mapControl1.Map.Clear();

            GridView view = (GridView)gridControl5.MainView;
            string strFileName = "";
            for (int i = view.DataRowCount - 1; i >= 0; i--)  // Load Map in reverse Order so Labels and Points are on top //
            {
                DataRow dr = view.GetDataRow(i);
                //if (Convert.ToInt32(dr["LayerType"]) != 2 && Convert.ToString(dr["LayerName"]) != "Map Objects")  // Background Layer //
                if (Convert.ToString(dr["LayerName"]) != "Map Object Labels" && Convert.ToString(dr["LayerName"]) != "Map Objects" && Convert.ToString(dr["LayerName"]) != "Temporary Map Objects")  // Background Layer //
                {
                    // Load Background Files //
                    strFileName = dr["LayerPath"].ToString();
                    if (strFileName.Substring(strFileName.Length - 3, 3).ToUpper() == "TAB")
                    {
                        MapTableLoader tl = new MapTableLoader(@strFileName);
                        try
                        {
                            tl.AutoPosition = false; // Set table loader options //
                            tl.StartPosition = 0;
                            mapControl1.Map.Load(tl);

                            MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[0];
                            l.Name = strFileName;
                            l.Enabled = (Convert.ToInt32(view.GetRowCellValue(i, "LayerVisible")) == 0 ? false : true);  // Controls visibility //
                            MapInfo.Mapping.LayerHelper.SetSelectable(l, (Convert.ToInt32(view.GetRowCellValue(i, "LayerHitable")) == 0 ? false : true));
                            MapInfo.Mapping.LayerHelper.SetEditable(l, false);
                            MapInfo.Mapping.LayerHelper.SetInsertable(l, false);

                            CreateFeatureLayerModifier(l, dr);  // Create Style Modifier for Layer to enable end-user customisation //
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map background [" + strFileName + "].\n\nError: " + ex.Message, "Load Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    else if (strFileName.Substring(strFileName.Length - 3, 3).ToUpper() == "SHP")
                    {
                        /*TableInfoShapefile ti = new TableInfoShapefile("EsriShape");
                        ti.TablePath = @strFileName;
                        MapInfo.Geometry.CoordSysFactory CSysFactory = Session.Current.CoordSysFactory;
                        MapInfo.Geometry.CoordSys coordSys = CSysFactory.CreateCoordSys("mapinfo:" + strDefaultMappingProjection + " Bounds (0,0) (5000000,5000000)");

                        ti.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(coordSys));
                        ti.Columns.Add(ColumnFactory.CreateDecimalColumn("AREA", 12, 3));
                        ti.Columns.Add(ColumnFactory.CreateDecimalColumn("PERIMETER", 12, 3));
                        ti.Columns.Add(ColumnFactory.CreateStringColumn("ID", 25));
                        ti.Columns.Add(ColumnFactory.CreateStringColumn("CAPTION", 35));
                        ti.Columns.Add(ColumnFactory.CreateIntColumn("STYLE"));

                        ti.DefaultStyle = new MapInfo.Styles.AreaStyle(

                        new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(2.0, MapInfo.Styles.LineWidthUnit.Pixel), (int)MapInfo.Styles.PatternStyle.Solid, Color.Red),

                        new MapInfo.Styles.SimpleInterior((int)MapInfo.Styles.PatternStyle.Cross, Color.Red, Color.White));

                        Table tableSHP = Session.Current.Catalog.OpenTable(ti);
                        MapTableLoader tl = new MapTableLoader(tableSHP);

                        try
                        {
                            tl.AutoPosition = true; // Set table loader options //
                            //tl.StartPosition = i;
                            //tl.EnableLayers = EnableLayers.Enable;
                            mapControl1.Map.Load(tl);

                            MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[mapControl1.Map.Layers.Count - 1];
                            l.Name = strFileName;
                       }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map background [" + strFileName + "].\n\nError: " + ex.Message, "Load Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }*/
                    }
                }
                else if (Convert.ToInt32(dr["LayerType"]) == 1 && Convert.ToString(dr["LayerName"]) == "Map Objects")
                {
                    intMapObjectSizing = (view.GetRowCellValue(i, "PointSize").ToString() == "Dynamic" ? -1 : 8);
                    LoadMapObjects();  // Load Map Objects Layer from Database //

                    foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                    {
                        if (l.Alias == "MapObjects")
                        {
                            MapInfo.Mapping.LayerHelper.SetSelectable(l, (Convert.ToInt32(view.GetRowCellValue(i, "LayerHitable")) == 0 ? false : true));
                            MapInfo.Mapping.LayerHelper.SetEditable(l, true);
                            MapInfo.Mapping.LayerHelper.SetInsertable(l, true);
                            //CreateFeatureLayerModifier(l, dr);  // Create Style Modifier for Layer to enable end-user customisation //
                            break;
                        }
                    }
                    Load_Map_Label_Settings();
                    Update_Map_Object_Labels();
                }
                else if (Convert.ToInt32(dr["LayerType"]) == 1 && Convert.ToString(dr["LayerName"]) == "Temporary Map Objects")
                {
                    Create_Temp_Features_Layer();
                }
            }

            if (PreventDefaultMapScaleUse)  // Only set to true LoadMapObjects() event when screen has highlighted items passed in on load and map auto-scales to show them //
            {
                PreventDefaultMapScaleUse = false;
            }
            else
            {
                mapControl1.Map.Scale = (this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultScale"].ToString() == null ? 2500 : Convert.ToInt32(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultScale"]));  // Set Scale //
                // Attempt to Centre the Map //
                if (intPassedMapX == 0 || intPassedMapY == 0)
                {
                    if (String.IsNullOrEmpty(strVisibleIDs) && String.IsNullOrEmpty(strVisibleAssetIDs) && !String.IsNullOrEmpty(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreX"].ToString()) && !String.IsNullOrEmpty(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreX"].ToString()))
                    {
                        MapInfo.Geometry.DPoint dpt1 = new MapInfo.Geometry.DPoint(Convert.ToDouble(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreX"]), Convert.ToDouble(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreY"]));
                        mapControl1.Map.Center = dpt1;
                    }
                }
                else
                {
                    MapInfo.Geometry.DPoint dpt1 = new MapInfo.Geometry.DPoint(Convert.ToDouble(intPassedMapX), Convert.ToDouble(intPassedMapY));
                    mapControl1.Map.Center = dpt1;
                    intPassedMapX = 0;
                    intPassedMapY = 0;
                }
            }
            Clear_Stored_Map_Views();  // Make sure no Stored Map Views are left //
        }

        private bool Load_TAB_File_Layer(int intLayerPosition, string strFileName, DataRow dr)
        {
            MapTableLoader tl = new MapTableLoader(@strFileName);
            try
            {
                tl.AutoPosition = false; // Set table loader options //
                tl.StartPosition = intLayerPosition;
                mapControl1.Map.Load(tl);

                MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[intLayerPosition];
                l.Name = strFileName;

                MapInfo.Mapping.LayerHelper.SetSelectable(l, false);
                MapInfo.Mapping.LayerHelper.SetEditable(l, false);
                MapInfo.Mapping.LayerHelper.SetInsertable(l, false);

                CreateFeatureLayerModifier(l, dr);  // Create Style Modifier for Layer to enable end-user customisation //
                l.Enabled = true;  // Make Layer visible //
                return true;
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map background [" + strFileName + "].\n\nError: " + ex.Message, "Load Map Layers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        private bool Load_SHP_File_Layer(int intLayerPosition, string strFileName, DataRow dr)
        {
            return false;
        }

        private void Load_Map_Label_Settings()
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view == null) return;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (view.GetRowCellValue(i, "LayerPath").ToString() == "MapObjectLabels")
                {
                    int intMapObjectLabelLayerID = Convert.ToInt32(view.GetRowCellValue(i, "LayerID"));

                    // *** Label Styling *** //
                    SqlConnection SQlConn = new SqlConnection(strConnectionString);
                    SqlCommand cmd = new SqlCommand("sp01271_AT_Tree_Picker_Workspace_label_font_settings", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@LayerID", intMapObjectLabelLayerID));
                    dsFontSettings.Clear();  // Remove old values first //
                    sdaFontSettings = new SqlDataAdapter(cmd);
                    sdaFontSettings.Fill(dsFontSettings, "Table");

                    SqlCommand UpdateCmd2 = new SqlCommand("sp01278_AT_Tree_Picker_Workspace_Label_Layer_Font_Save", SQlConn);
                    UpdateCmd2.CommandType = CommandType.StoredProcedure;
                    UpdateCmd2.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FontID", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@LayerID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LayerID", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontName", SqlDbType.VarChar, 100, ParameterDirection.Input, 0, 0, "FontName", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontSize", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FontSize", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontColour", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FontColour", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontAngle", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FontAngle", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@Position", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Position", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@Offset", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "Offset", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@VisibleFromScale", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "VisibleFromScale", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@VisibleToScale", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "VisibleToScale", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@ShowOverlapping", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "ShowOverlapping", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@ShowDuplicates", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "ShowDuplicates", DataRowVersion.Current, false, null, "", "", ""));
                    sdaFontSettings.UpdateCommand = UpdateCmd2;
                    // *** End of Label Styling *** //


                    // *** Label and Tooltip Structure *** //
                    SQlConn = new SqlConnection(strConnectionString);
                    cmd = new SqlCommand("sp01268_AT_Tree_Picker_Workspace_layer_labelling", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@LayerID", intMapObjectLabelLayerID));
                    dsLabelStructure.Clear();  // Remove old values first //
                    sdaLabelStructure = new SqlDataAdapter(cmd);
                    sdaLabelStructure.Fill(dsLabelStructure, "Table");

                    SqlCommand DeleteCmd = new SqlCommand("dbo.sp01281_AT_Tree_Picker_Workspace_Layer_Labelling_Delete", SQlConn);
                    DeleteCmd.CommandType = CommandType.StoredProcedure;
                    DeleteCmd.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
                    DeleteCmd.Parameters.Add(new SqlParameter("@LabelStructureID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LabelStructureID", DataRowVersion.Current, false, null, "", "", ""));
                    sdaLabelStructure.DeleteCommand = DeleteCmd;

                    SqlCommand InsertCmd = new SqlCommand("dbo.sp01280_AT_Tree_Picker_Workspace_Layer_Labelling_insert", SQlConn);
                    InsertCmd.CommandType = CommandType.StoredProcedure;
                    InsertCmd.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@LayerID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LayerID", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@StructureType", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "StructureType", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@Prefix", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Prefix", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@ColumnName", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "ColumnName", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@Suffix", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Suffix", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@NewLine", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "NewLine", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@Seperator", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Seperator", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@FieldOrder", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FieldOrder", DataRowVersion.Current, false, null, "", "", ""));
                    sdaLabelStructure.InsertCommand = InsertCmd;

                    SqlCommand UpdateCmd = new SqlCommand("sp01279_AT_Tree_Picker_Workspace_Layer_Labelling_Save", SQlConn);
                    UpdateCmd.CommandType = CommandType.StoredProcedure;
                    UpdateCmd.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@LabelStructureID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LabelStructureID", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@LayerID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LayerID", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@StructureType", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "StructureType", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@Prefix", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Prefix", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@ColumnName", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "ColumnName", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@Suffix", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Suffix", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@NewLine", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "NewLine", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@Seperator", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Seperator", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@FieldOrder", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FieldOrder", DataRowVersion.Current, false, null, "", "", ""));
                    sdaLabelStructure.UpdateCommand = UpdateCmd;
                    // *** End of Label and Tooltip Structure *** //

                    SQlConn.Close();
                    SQlConn.Dispose();
                    break;
                }
            }
        }

        private void beWorkspace_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            frm_AT_Mapping_Tree_Picker_Save_Settings frmSave = null;
            switch (e.Button.Tag.ToString())
            {
                case "Select":
                    frm_AT_Mapping_Workspace_Select fm_AT_Mapping_Workspace_Select = new frm_AT_Mapping_Workspace_Select();
                    fm_AT_Mapping_Workspace_Select.GlobalSettings = GlobalSettings;
                    fm_AT_Mapping_Workspace_Select.intCurrentlyLoadedWorkspaceID = intLoadedWorkspaceID;
                    if (fm_AT_Mapping_Workspace_Select.ShowDialog() == DialogResult.OK)
                    {
                        intLoadedWorkspaceID = fm_AT_Mapping_Workspace_Select.intWorkspaceID;
                        intLoadedWorkspaceOwner = fm_AT_Mapping_Workspace_Select.intWorkspaceOwner;
                        strLoadedWorkspaceName = fm_AT_Mapping_Workspace_Select.strWorkspaceName;
                        Load_Layer_Manager();
                    }
                    break;
                case "Save":
                    if (intLoadedWorkspaceOwner != this.GlobalSettings.UserID)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save current workspace. Only the owner of a workspace can save changes to it.\n\nTip: If you need to save the changes, click the Save As button to create a new workspace with the current settings.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    frmSave = new frm_AT_Mapping_Tree_Picker_Save_Settings();
                    frmSave.GlobalSettings = GlobalSettings;
                    frmSave.strFormMode = "Save";
                    frmSave.boolSaveLoadedLayers = true;
                    frmSave.boolSaveLabelStructure = true;
                    frmSave.boolSaveThematicStyling = (intLoadedStyleSetOwnerID == this.GlobalSettings.UserID ? true : false);
                    frmSave.boolSaveXY = false;
                    frmSave.boolSaveMapScale = false;

                    if (frmSave.ShowDialog() == DialogResult.OK)
                    {
                        this.fProgress = new frmProgress(20);  // Show Progress Window //
                        this.AddOwnedForm(fProgress);
                        fProgress.UpdateCaption("Saving Mapping Workspace, Please Wait...");
                        fProgress.Show();
                        Application.DoEvents();

                        if (Save_Workspace_Front_End_Changes_To_Database(frmSave.boolSaveLoadedLayers, frmSave.boolSaveLabelStructure, frmSave.boolSaveXY, frmSave.boolSaveMapScale, frmSave.boolSaveThematicStyling) == -1) return;

                        fProgress.UpdateProgress(20);
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                        if (this.GlobalSettings.ShowConfirmations == 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Workspace Changes Saved.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case "SaveAs":
                    frmSave = new frm_AT_Mapping_Tree_Picker_Save_Settings();
                    frmSave.GlobalSettings = GlobalSettings;
                    frmSave.strFormMode = "SaveAs";
                    frmSave.boolSaveLoadedLayers = true;
                    frmSave.boolSaveLabelStructure = true;
                    frmSave.boolSaveThematicStyling = (intLoadedStyleSetOwnerID == this.GlobalSettings.UserID ? true : false);
                    frmSave.boolSaveXY = false;
                    frmSave.boolSaveMapScale = false;

                    if (frmSave.ShowDialog() == DialogResult.OK)
                    {
                        this.fProgress = new frmProgress(10);  // Show Progress Window //
                        this.AddOwnedForm(fProgress);
                        fProgress.UpdateCaption("Saving Mapping Workspace, Please Wait...");
                        fProgress.Show();
                        Application.DoEvents();

                        int intNewWorkspaceID = 0;
                        // First generate Workspace based on current workspace //
                        DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter CreateWorkspace = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                        CreateWorkspace.ChangeConnectionString(strConnectionString);
                        try
                        {
                            intNewWorkspaceID = Convert.ToInt32(CreateWorkspace.sp01292_AT_Tree_Picker_Workspace_SaveAs(this.GlobalSettings.UserID, frmSave.strWorkspaceName, frmSave.strRemarks, frmSave.intDefaultWorkspace, frmSave.intShareType, frmSave.strShareWithGroupIDs, intLoadedWorkspaceID));
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close(); // Close Progress Window //
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the new Mapping Workspace [" + ex.Message + "]!\n\nTry doing a Save As again - if the problem persists, contact Technical Support.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress.UpdateProgress(10);

                        // Second, iterate through all the mapping workpace related datasets in memory and switch IDs from old value to new value //
                        // Workspace Layers //
                        int intOldMapObjectLabel_LayerID = 0;
                        int intNewMapObjectLabel_LayerID = 0;
                        //int intFoundRow = GridControl.InvalidRowHandle;
                        SqlDataAdapter sda = new SqlDataAdapter();
                        DataSet ds = new DataSet("NewDataSet");
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = new SqlCommand("sp01293_AT_Tree_Picker_Workspace_SaveAs_GetNewLayerIDs", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@WorkspaceID", intNewWorkspaceID));
                        ds.Clear();  // Remove old values first //
                        sda = new SqlDataAdapter(cmd);
                        try
                        {
                            sda.Fill(ds, "Table");
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close(); // Close Progress Window //
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while updating the On-screen Layer Manager with the saved layers [" + ex.Message + "]!\n\nTry doing a Save As again - if the problem persists, contact Technical Support.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        bool boolToggleDelete = false;
                        foreach (DataRow drTarget in this.dataSet_AT_TreePicker.sp01288_AT_Tree_Picker_Workspace_layers_list.Rows)
                        {
                            DataRow[] drFoundRows;
                            if (drTarget.RowState == DataRowState.Deleted)  // Set flag then reject changes to ghet deleted row back so we can change it's values then accept values to commit new IDs to DB then re-delete row at very end. //
                            {
                                boolToggleDelete = true;
                                drTarget.RejectChanges();
                            }
                            else if (drTarget.RowState == DataRowState.Added)  // No LayerID //
                            {
                                drTarget["WorkspaceID"] = intNewWorkspaceID;
                                continue;  // Abort rest of loop //
                            }
                            drFoundRows = ds.Tables[0].Select("CreatedFromLayerID = " + drTarget["LayerID", DataRowVersion.Original].ToString());
                            if (drFoundRows.Length == 1)
                            {
                                DataRow drFoundRow = drFoundRows[0];
                                drTarget["LayerID"] = Convert.ToInt32(drFoundRow["LayerID"]);
                                if (drTarget["LayerName", DataRowVersion.Original].ToString() == "Map Object Labels")
                                {
                                    intOldMapObjectLabel_LayerID = Convert.ToInt32(drTarget["LayerID"]);  // Store this for later [need for Label structure and font] //
                                    intNewMapObjectLabel_LayerID = Convert.ToInt32(drFoundRow["LayerID"]);
                                }
                            }
                            drTarget["WorkspaceID"] = intNewWorkspaceID;

                            if (boolToggleDelete)
                            {
                                boolToggleDelete = false;
                                drTarget.AcceptChanges();
                                drTarget.Delete();
                            }
                        }
                        fProgress.UpdateProgress(10);

                        if (intOldMapObjectLabel_LayerID > 0 && intNewMapObjectLabel_LayerID > 0)
                        {
                            // Font Settings //
                            cmd = new SqlCommand("sp01294_AT_Tree_Picker_Workspace_SaveAs_GetNewFontSettingsID", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@LayerID", intNewMapObjectLabel_LayerID));
                            ds.Clear();  // Remove old values first //
                            sda = new SqlDataAdapter(cmd);
                            try
                            {
                                sda.Fill(ds, "Table");
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close(); // Close Progress Window //
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while updating the On-screen Layer Manager with the saved font settings [" + ex.Message + "]!\n\nTry doing a Save As again - if the problem persists, contact Technical Support.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                            foreach (DataRow drTarget in dsFontSettings.Tables[0].Rows)
                            {
                                DataRow[] drFoundRows;
                                if (drTarget.RowState == DataRowState.Deleted)  // Set flag then reject changes to ghet deleted row back so we can change it's values then accept values to commit new IDs to DB then re-delete row at very end. //
                                {
                                    boolToggleDelete = true;
                                    drTarget.RejectChanges();
                                }
                                drFoundRows = ds.Tables[0].Select("CreatedFromFontID = " + drTarget["FontID"].ToString());
                                if (drFoundRows.Length == 1)
                                {
                                    DataRow drFoundRow = drFoundRows[0];
                                    drTarget["FontID"] = Convert.ToInt32(drFoundRow["FontID"]);
                                    drTarget["LayerID"] = Convert.ToInt32(drFoundRow["LayerID"]);
                                }
                                if (boolToggleDelete)
                                {
                                    boolToggleDelete = false;
                                    drTarget.AcceptChanges();
                                    drTarget.Delete();
                                }
                            }

                            // Layer Labelling Structure //
                            cmd = new SqlCommand("sp01295_AT_Tree_Picker_Workspace_SaveAS_GetNewLayerLabellingIDs", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@LayerID", intNewMapObjectLabel_LayerID));
                            ds.Clear();  // Remove old values first //
                            sda = new SqlDataAdapter(cmd);
                            try
                            {
                                sda.Fill(ds, "Table");
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close(); // Close Progress Window //
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while updating the On-screen Layer Manager with the saved Label\tooltip structure settings [" + ex.Message + "]!\n\nTry doing a Save As again - if the problem persists, contact Technical Support.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }


                            // Update the remaining rows //
                            foreach (DataRow drTarget in dsLabelStructure.Tables[0].Rows)
                            {
                                DataRow[] drFoundRows;
                                if (drTarget.RowState == DataRowState.Deleted)  // Set flag then reject changes to ghet deleted row back so we can change it's values then accept values to commit new IDs to DB then re-delete row at very end. //
                                {
                                    boolToggleDelete = true;
                                    drTarget.RejectChanges();
                                }
                                else if (drTarget.RowState == DataRowState.Added)  // No LabelStructureID //
                                {
                                    drTarget["LayerID"] = intNewMapObjectLabel_LayerID;
                                    continue;  // Abort rest of loop //
                                }
                                drFoundRows = ds.Tables[0].Select("CreatedFromLabelStructureID = " + drTarget["LabelStructureID"].ToString());
                                if (drFoundRows.Length == 1)
                                {
                                    DataRow drFoundRow = drFoundRows[0];
                                    drTarget["LabelStructureID"] = Convert.ToInt32(drFoundRow["LabelStructureID"]);
                                    drTarget["LayerID"] = Convert.ToInt32(drFoundRow["LayerID"]);
                                }
                                if (boolToggleDelete)
                                {
                                    boolToggleDelete = false;
                                    drTarget.AcceptChanges();
                                    drTarget.Delete();
                                }
                            }
                        }
                        fProgress.UpdateProgress(10);

                        // Third, Switch the loaded workspace to the new one so when scale and X and Y are saved, the new workspace is updated. //
                        this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit, intNewWorkspaceID);

                        // Fourth, fire the normal Save code to write the pending Mapping Workspace front-end changes to the database //
                        if (Save_Workspace_Front_End_Changes_To_Database(frmSave.boolSaveLoadedLayers, frmSave.boolSaveLabelStructure, frmSave.boolSaveXY, frmSave.boolSaveMapScale, frmSave.boolSaveThematicStyling) == -1) return;

                        // Lastly, switch the currently loaded workspace to this new workspace //
                        intLoadedWorkspaceID = intNewWorkspaceID;
                        intLoadedWorkspaceOwner = this.GlobalSettings.UserID;
                        strLoadedWorkspaceName = frmSave.strWorkspaceName;
                        beWorkspace.EditValue = strLoadedWorkspaceName;

                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                        if (this.GlobalSettings.ShowConfirmations == 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Workspace Changes Saved as New Workspace.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private int Save_Workspace_Front_End_Changes_To_Database(bool boolSaveLoadedLayers, bool boolSaveLabelStructure, bool boolSaveXY, bool boolSaveMapScale, bool boolSaveThematicStyling)
        {
            if (boolSaveLoadedLayers)  // Save Layers //
            {
                this.sp01288ATTreePickerWorkspacelayerslistBindingSource.EndEdit();
                try
                {
                    this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter.Update(dataSet_AT_TreePicker);  // Update, Delete queries defined in Table Adapter //
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    XtraMessageBox.Show("An error occurred while saving the Layer Settings [" + ex.Message + "]!\n\nTry saving again again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
            }
            fProgress.UpdateProgress(20);

            if (boolSaveLabelStructure)  // Save Label\Tooltip Structure and Font Settings //
            {
                try  // Label\Tooltip Structure //
                {
                    sdaLabelStructure.DeleteCommand.Connection.ConnectionString = strConnectionString;  // Need to reapply connection string as it seems to get lost after creating in Load_Map_Label_Settings Event //
                    sdaLabelStructure.InsertCommand.Connection.ConnectionString = strConnectionString;
                    sdaLabelStructure.UpdateCommand.Connection.ConnectionString = strConnectionString;

                    this.sdaLabelStructure.Update(dsLabelStructure);  // Insert, Update and Delete queries defined in code in Load_Label_Settings Method //
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while saving the Object Labelling Settings [" + ex.Message + "]!\n\nTry saving again again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
                try  // Font Settings //
                {
                    sdaFontSettings.UpdateCommand.Connection.ConnectionString = strConnectionString;  // Need to reapply connection string as it seems to get lost after creating in Load_Map_Label_Settings Event //
                    this.sdaFontSettings.Update(dsFontSettings);  // Update query defined in code in Load_Label_Settings Method //
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    XtraMessageBox.Show("An error occurred while saving the Object Labelling Font Settings [" + ex.Message + "]!\n\nTry saving again again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
            }
            fProgress.UpdateProgress(20);

            if (boolSaveXY || boolSaveMapScale) // Save XY and/or Scale //
            {
                if (boolSaveXY)
                {
                    this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreX"] = Convert.ToDouble(mapControl1.Map.Center.x);
                    this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreY"] = Convert.ToDouble(mapControl1.Map.Center.y);
                }
                if (boolSaveMapScale)
                {
                    this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultScale"] = Convert.ToInt32(mapControl1.Map.Scale);
                }
                try
                {
                    this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Update(dataSet_AT_TreePicker);
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    XtraMessageBox.Show("An error occurred while saving the Workspace Settings [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
            }
            fProgress.UpdateProgress(20);

            if (boolSaveThematicStyling) // Save Thematics //
            {
                // Clear the Styling Items for the Style Set header first [they will be recreated in SaveStyleSetItemInfo method call] //
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter ClearStyleItems = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                ClearStyleItems.ChangeConnectionString(strConnectionString);
                try
                {
                    ClearStyleItems.sp01261_AT_Tree_Picker_Style_Set_Delete_Style_Items(intThematicStylingFieldHeaderID);
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while saving the Thematic Styling Settings (Clearing Original Values) [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
                if (SaveStyleSetItemInfo(intThematicStylingFieldHeaderID) == -1)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    return -1;
                }
            }
            return 1;
        }

        private int SaveStyleSetItemInfo(int HeaderID)
        {
            // Save Style Set Items...// 
            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter SaveItem = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
            SaveItem.ChangeConnectionString(strConnectionString);

            int intThematicItemID = 0;
            int intThematicSetID = 0;
            string strItemDescription = "";
            int intPicklistItemID = 0;
            int intPointSymbol = 0;
            int intPointSize = 0;
            int intPointColour = 0;
            int intPolygonFillColour = 0;
            int intPolygonFillPattern = 0;
            int intPolygonFillPatternColour = 0;
            int intPolygonLineWidth = 0;
            int intPolygonLineColour = 0;
            int intPolylineStyle = 0;
            int intPolylineColour = 0;
            int intPolylineWidth = 0;
            int intShowInLegend = 0;
            int intItemOrder = 0;
            decimal decBandStart = (decimal)0.00;
            decimal decBandEnd = (decimal)0.00;

            foreach (DataRow dr in dsThematicStyleItems.Tables[0].Rows)
            {
                intThematicItemID = Convert.ToInt32(dr["ItemID"]);
                intThematicSetID = HeaderID;
                strItemDescription = Convert.ToString(dr["ItemDescription"]);
                intPicklistItemID = Convert.ToInt32(dr["PicklistItemID"]);
                intPointSymbol = Convert.ToInt32(dr["Symbol"]);
                intPointSize = Convert.ToInt32(dr["Size"]);
                intPointColour = Convert.ToInt32(dr["SymbolColour"]);
                intPolygonFillColour = Convert.ToInt32(dr["PolygonFillColour"]);
                intPolygonFillPattern = Convert.ToInt32(dr["PolygonFillPattern"]);
                intPolygonFillPatternColour = Convert.ToInt32(dr["PolygonFillPatternColour"]);
                intPolygonLineWidth = Convert.ToInt32(dr["PolygonLineWidth"]);
                intPolygonLineColour = Convert.ToInt32(dr["PolygonLineColour"]);
                intPolylineStyle = Convert.ToInt32(dr["PolylineStyle"]);
                intPolylineColour = Convert.ToInt32(dr["PolylineColour"]);
                intPolylineWidth = Convert.ToInt32(dr["PolylineWidth"]);
                intShowInLegend = Convert.ToInt32(dr["ShowInLegend"]);
                intItemOrder = Convert.ToInt32(dr["Order"]);
                decBandStart = Convert.ToDecimal(dr["StartBand"]);
                decBandEnd = Convert.ToDecimal(dr["EndBand"]);
                try
                {
                    SaveItem.sp01260_AT_Tree_Picker_Style_Set_Save_Item("insert", 0, intThematicSetID, strItemDescription, intPicklistItemID, intPointSymbol, intPointSize, intPointColour, intPolygonFillColour, intPolygonFillPattern, intPolygonFillPatternColour, intPolygonLineWidth, intPolygonLineColour, intPolylineStyle, intPolylineColour, intPolylineWidth, intShowInLegend, intItemOrder, decBandStart, decBandEnd, intMapObjectTransparency);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while saving the Thematic Styling Settings [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
            }
            return 1;
        }

        private void bbiLayerManagerProperties_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the layer to view the properties for before proceeding.", "View Layer Properties", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strLayerName = view.GetFocusedRowCellValue("LayerName").ToString();
            switch (strLayerName)
            {
                case "Map Objects":
                    // Create clones of DataSets used so if changes are made then cancel is clicked, we can roll back the changes //
                    DataSet dsDefaultStyles_backup = (DataSet)dsDefaultStyles.Copy();
                    DataSet dsThematicStyleItems_backup = (DataSet)dsThematicStyleItems.Copy();
                    DataSet dsThematicStyleSets_backup = (DataSet)dsThematicStyleSets.Copy();
                    int intUseThematicStyling_backup = intUseThematicStyling;
                    int intLoadedStyleSetOwnerID_backup = intLoadedStyleSetOwnerID;

                    frm_AT_Mapping_Styles frm_styles = new frm_AT_Mapping_Styles();
                    //frm_styles.MdiParent = this.MdiParent;
                    frm_styles.FormID = 2020;
                    frm_styles.GlobalSettings = this.GlobalSettings;
                    frm_styles.dsDefaultStyle = this.dsDefaultStyles;
                    frm_styles.dsThematicItems = this.dsThematicStyleItems;
                    frm_styles.dsThematicSets = this.dsThematicStyleSets;
                    frm_styles.intThematicStylingFieldHeaderID = this.intThematicStylingFieldHeaderID;
                    frm_styles.intThematicStylingFieldHeaderType = this.intThematicStylingFieldHeaderType;
                    frm_styles.intLoadedStyleSetOwnerID = this.intLoadedStyleSetOwnerID;
                    frm_styles.intUseThematicStyling = Convert.ToInt32(view.GetFocusedRowCellValue("UseThematicStyling"));
                    frm_styles.strPointSize = Convert.ToString(view.GetFocusedRowCellValue("PointSize"));
                    frm_styles.strThematicStylingFieldName = this.strThematicStylingFieldName;
                    frm_styles.intTransparency = this.intMapObjectTransparency;
                    frm_styles.frmCallingForm = this;
                    if (frm_styles.ShowDialog() == DialogResult.OK)
                    {
                        if (frm_styles.intLoadedStyleSetID != 0) view.SetFocusedRowCellValue("ThematicStyleSet", frm_styles.intLoadedStyleSetID);  // Ignore if 0 as these are not saved sets [just temporary] //
                        view.SetFocusedRowCellValue("UseThematicStyling", frm_styles.intUseThematicStyling);
                        view.SetFocusedRowCellValue("PointSize", frm_styles.strPointSize);
                        intUseThematicStyling = frm_styles.intUseThematicStyling;
                        intLoadedStyleSetOwnerID = frm_styles.intLoadedStyleSetOwnerID;
                        strThematicStylingFieldName = frm_styles.strThematicStylingFieldName;  // Shown on Legend //
                        intMapObjectTransparency = frm_styles.intTransparency;

                        if (view.GetFocusedRowCellValue("PointSize").ToString().StartsWith("Fix"))
                        {
                            string strSize = BaseObjects.ExtensionFunctions.ExtractNumbersFromString(view.GetFocusedRowCellValue("PointSize").ToString());
                            if (string.IsNullOrEmpty(strSize))
                            {
                                intMapObjectSizing = 8;
                            }
                            else
                            {
                                intMapObjectSizing = Convert.ToInt32(strSize);
                            }
                        }
                        else
                        {
                            intMapObjectSizing = -1; // Dynamic sizing based on Crown Diameter //
                        }
                        UpdateMapObjectsDisplayed(true);

                        // Update Legend //
                        if (dockPanel5.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden)
                        {
                            if (intUseThematicStyling != 0)
                            {
                                Create_Legend();
                            }
                            else  // No Thematics on so hide legend and uncheck Lengend button on toolbar //
                            {
                                bbiLegend.Checked = false;
                            }
                        }
                    }
                    else // Use Cloned copies to roll back the changes //
                    {
                        dsDefaultStyles = (DataSet)dsDefaultStyles_backup.Copy();
                        dsThematicStyleItems = (DataSet)dsThematicStyleItems_backup.Copy();
                        dsThematicStyleSets = (DataSet)dsThematicStyleSets_backup.Copy();
                        intUseThematicStyling = intUseThematicStyling_backup;
                        intLoadedStyleSetOwnerID = intLoadedStyleSetOwnerID_backup;
                    }
                    break;
                case "Map Object Labels":
                    // Create clones of DataSets used so if changes are made then cancel is clicked, we can roll back the changes //
                    DataSet dsFontSettings_backup = (DataSet)dsFontSettings.Copy();
                    DataSet dsLabelStructure_backup = (DataSet)dsLabelStructure.Copy();

                    frm_AT_Mapping_Layer_Label_Properties fChildForm2 = new frm_AT_Mapping_Layer_Label_Properties();
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intParentLayerID = Convert.ToInt32(view.GetFocusedRowCellValue("LayerID"));
                    fChildForm2.intTransparency = Convert.ToInt32(view.GetFocusedRowCellValue("Transparency"));
                    fChildForm2.intLineColour = Convert.ToInt32(view.GetFocusedRowCellValue("LineColour"));
                    fChildForm2.intLineWidth = Convert.ToInt32(view.GetFocusedRowCellValue("LineWidth"));
                    fChildForm2.dsFontStyle = this.dsFontSettings;
                    fChildForm2.dsLabelStructure = this.dsLabelStructure;
                    fChildForm2.strCaller = "TreePicker";
                    if (fChildForm2.ShowDialog() == DialogResult.OK)
                    {
                        // OK clicked so update current row //
                        view.SetFocusedRowCellValue("Transparency", fChildForm2.intTransparency);
                        view.SetFocusedRowCellValue("LineColour", fChildForm2.intLineColour);
                        view.SetFocusedRowCellValue("LineWidth", fChildForm2.intLineWidth);

                        // Update Map //                       
                        Update_Map_Object_Labels();
                    }
                    else  // Use Cloned copies to roll back the changes //
                    {
                        dsFontSettings = (DataSet)dsFontSettings_backup.Copy();
                        dsLabelStructure = (DataSet)dsLabelStructure_backup.Copy();
                    }
                    break;
                case "Temporary Map Object Labels":
                    return;

                default:  // Map backgrounds (type = 0: Raster or 1: Vector //
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LayerType")) == 0)  // Raster //
                    {
                        frm_AT_Mapping_Properties_Layer_Raster fChildForm3 = new frm_AT_Mapping_Properties_Layer_Raster();
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intTransparency = Convert.ToInt32(view.GetFocusedRowCellValue("Transparency"));
                        fChildForm3.intGreyScale = Convert.ToInt32(view.GetFocusedRowCellValue("GreyScale"));

                        if (fChildForm3.ShowDialog() == DialogResult.OK)
                        {
                            // OK clicked so update current row //
                            view.SetFocusedRowCellValue("Transparency", fChildForm3.intTransparency);
                            view.SetFocusedRowCellValue("GreyScale", fChildForm3.intGreyScale);

                            // Update Map //
                            DataRow dr = view.GetDataRow(view.FocusedRowHandle);
                            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                            {
                                if (l.Name == dr["LayerPath"].ToString())
                                {
                                    Update_Layer_Setup(l, dr);
                                    break;
                                }
                            }
                        }
                    }
                    else  // Vector //
                    {
                        frm_AT_Mapping_Properties_Layer_Vector fChildForm3 = new frm_AT_Mapping_Properties_Layer_Vector();
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intPreserveDefaultStyling = Convert.ToInt32(view.GetFocusedRowCellValue("PreserveDefaultStyling"));
                        fChildForm3.intTransparency = Convert.ToInt32(view.GetFocusedRowCellValue("Transparency"));
                        fChildForm3.intLineColour = Convert.ToInt32(view.GetFocusedRowCellValue("LineColour"));
                        fChildForm3.intLineWidth = Convert.ToInt32(view.GetFocusedRowCellValue("LineWidth"));

                        if (fChildForm3.ShowDialog() == DialogResult.OK)
                        {
                            // OK clicked so update current row //
                            view.SetFocusedRowCellValue("PreserveDefaultStyling", fChildForm3.intPreserveDefaultStyling);
                            view.SetFocusedRowCellValue("Transparency", fChildForm3.intTransparency);
                            view.SetFocusedRowCellValue("LineColour", fChildForm3.intLineColour);
                            view.SetFocusedRowCellValue("LineWidth", fChildForm3.intLineWidth);

                            // Update Map //
                            DataRow dr = view.GetDataRow(view.FocusedRowHandle);
                            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                            {
                                if (l.Name == dr["LayerPath"].ToString())
                                {
                                    Update_Layer_Setup(l, dr);
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }
        }

        private void bbiAddLayer_ItemClick(object sender, ItemClickEventArgs e)
        {
            AddRecord();
        }

        private void bbiRemoveLayer_ItemClick(object sender, ItemClickEventArgs e)
        {
            DeleteRecord();
        }

        private void AddRecord()
        {
            GridView view = (GridView)gridControl5.MainView;
            // Get Default Mapping Path //
            string strDefaultPath = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                //strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesMapBackgroundFolder").ToString().ToLower();
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, (this.GlobalSettings.SystemDataTransferMode != "Tablet" ? "AmenityTreesMapBackgroundFolder" : "AmenityTreesMapBackgroundFolderTablet")).ToString().ToLower();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Select Mapping File(s) - No Mapping Background Folder Location has been specified in the System Settings table.", "Select Files(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Following will determin where to insert the new layers in the Map [if MapObjects / MapObjectLabels are at the end, then new ayers go in just before them otherwise they go on the end] //
            //bool boolAddPriorToMapObjects = false;
            //int intFoundRow1 = GetLayerPosition("MapObjects");
            //int intFoundRow2 = GetLayerPosition("MapObjectLabels");

            //if (intFoundRow1 == 0 || intFoundRow2 == 0) boolAddPriorToMapObjects = true;  // Remember, that Layer order is actually reverse of GridView order //

            // Process all selected files //
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.DefaultExt = "tab";
                //dlg.Filter = "TAB Files (*.tab)|*.tab|Shape Files (*.shp)|*.shp";
                dlg.Filter = "TAB Files (*.tab)|*.tab";
                if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                dlg.Multiselect = true;
                dlg.ShowDialog();
                if (dlg.FileNames.Length > 0)
                {
                    string strTempFileName = "";
                    int intFileType = 0;  // 0 = Raster, 1 = Vector //
                    int intMaxRowsAtStart = view.DataRowCount;
                    bool boolProceedWithLoadLayerManager = false;
                    foreach (string filename in dlg.FileNames)
                    {
                        if (strDefaultPath != "")
                            if (!filename.ToLower().StartsWith(strDefaultPath))
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Mapping - Background Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                //view.EndDataUpdate();
                                //view.EndSort();
                                //view.EndUpdate();
                                return;
                            }
                        strTempFileName = filename.Substring(strDefaultPath.Length);

                        // Determin if the file is Raster or Vector //
                        try
                        {
                            string strFileText = System.IO.File.ReadAllText(filename);
                            intFileType = (!strFileText.Contains("Type \"RASTER\"") ? 1 : 0);
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to select mapping file: " + filename + ".\n\nThe following error occurred while attempting to determin it's type - [" + Ex.Message + "].", "Select Files(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            continue;
                        }

                        // Data row defined as it must be passed through to Add Map Layer Process [Used by the LayerModifier call] However, it is not added to the grid yet! //
                        DataRow drAdd = this.dataSet_AT_TreePicker.sp01288_AT_Tree_Picker_Workspace_layers_list.NewRow();
                        drAdd["WorkSpaceID"] = intLoadedWorkspaceID;
                        drAdd["LayerType"] = intFileType;
                        drAdd["LayerTypeDescription"] = (intFileType == 0 ? "Raster" : "Vector");
                        drAdd["LayerName"] = System.IO.Path.GetFileName(strTempFileName);
                        drAdd["LayerPath"] = strDefaultPath + strTempFileName;
                        drAdd["LayerOrder"] = 3; // Insert after Map Objects,  Map Labels and Temporary Map Objects //
                        drAdd["LayerVisible"] = 1;
                        drAdd["LayerHitable"] = 0;
                        drAdd["PreserveDefaultStyling"] = 0;
                        drAdd["Transparency"] = 0;
                        drAdd["LineColour"] = -16777216;
                        drAdd["LineWidth"] = 1;
                        drAdd["GreyScale"] = 0;
                        drAdd["ThematicStyleSet"] = 0;
                        drAdd["PointSize"] = 8;
                        drAdd["DummyLayerPathForSave"] = strTempFileName;  // This is the value stored in the DB should the workspace be saved! //

                        if (strTempFileName.Substring(strTempFileName.Length - 3, 3).ToUpper() == "TAB")
                        {
                            boolProceedWithLoadLayerManager = Load_TAB_File_Layer(3, strDefaultPath + strTempFileName, drAdd);  // Load TAB File into Map //

                        }
                        else  // Shape File //
                        {
                            boolProceedWithLoadLayerManager = Load_SHP_File_Layer(3, strDefaultPath + strTempFileName, drAdd);  // Load SHP File into Map //
                        }

                        if (boolProceedWithLoadLayerManager)  // Load of TAB\SHP File was successful so add layer into grid now //
                        {
                            view.BeginSort();
                            view.BeginDataUpdate();
                            for (int i = 3; i < view.DataRowCount; i++)  // Add 1 to existing LayerOrders //
                            {
                                view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) + 1);
                            }
                            view.EndDataUpdate();
                            view.EndSort();
                            this.dataSet_AT_TreePicker.sp01288_AT_Tree_Picker_Workspace_layers_list.Rows.Add(drAdd);
                        }
                    }
                    mapControl1.Map.Invalidate();  // Force Map to Repaint itself //
                }
            }
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                XtraMessageBox.Show("Select the layer to be removed by clicking on it first then try again.", "Remove Map Layer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (view.GetFocusedRowCellValue("LayerName").ToString() == "Map Object Labels" || view.GetFocusedRowCellValue("LayerName").ToString() == "Map Objects" || view.GetFocusedRowCellValue("LayerName").ToString() == "Temporary Map Objects")
            {
                XtraMessageBox.Show("You cannot remove the Map Object, Map Object Label and Temporary Map Object layers. Select a different layer to be removed by clicking on it first then try again.", "Remove Map Layer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (view.DataRowCount <= 4)
            {
                XtraMessageBox.Show("Unable to remove current layer... A Mapping Workspace must have at least 4 Map Layers [Map Objects, Map Object Labels, Temporary Map Objects and at least one background layer].\n\nTip: If you need to remove the current layer, add another map background layer first then try removing this layer again.", "Remove Map Layer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have 1 Map Layer selected for removal!\n\nProceed?", "Remove Map Layer", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                // Remove layer from map then remove it from the grid //
                try
                {
                    mapControl1.Map.Layers.RemoveAt(GetLayerPosition(view.GetFocusedRowCellValue("LayerPath").ToString()));
                    view.DeleteRow(view.FocusedRowHandle);  // Delete record from grid //
                    view.BeginSort();
                    view.BeginDataUpdate();
                    for (int i = 0; i < view.DataRowCount; i++)  // Adjust LayerOrders //
                    {
                        view.SetRowCellValue(i, "LayerOrder", i);
                    }
                    view.EndDataUpdate();
                    view.EndSort();

                    mapControl1.Map.Invalidate();  // Force Map to Repaint itself //
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("Unable to remove selected Map Layer - the following error occurred [" + Ex.Message + "]. Try removint the layer again. If the problem persisits contact Technical Support.", "Remove Map Layer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private int GetLayerPosition(string strLayerName)
        {
            int i = 0;
            foreach (IMapLayer Layer in this.mapControl1.Map.Layers)
            {
                if (Layer.Name.ToUpper() == strLayerName.ToUpper()) break;
                i = i + 1;
            }
            return i;
        }

        private void Update_Layer_Setup(MapInfo.Mapping.IMapLayer l, DataRow dr)
        {
            // Remove any existing feature override style modifier if it exists //
            FeatureLayer layer = mapControl1.Map.Layers[mapControl1.Map.Layers.IndexOf(l)] as FeatureLayer;
            try
            {
                layer.Modifiers.RemoveAt(0);
            }
            catch (Exception)
            {
            }

            if (l.Type == LayerType.Normal)  // Vector Layer //
            {
                // Create a new feature override style modifier if PreserveDefaultStyling switched off //
                if (Convert.ToInt32(dr["PreserveDefaultStyling"]) == 0)  // Allow styling according to criteria //
                {
                    CompositeStyle style = new CompositeStyle();
                    style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(dr["LineWidth"]), MapInfo.Styles.LineWidthUnit.Pixel), 2, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"]))));
                    
                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.White);                 
                    ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));
                    style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));

                    FeatureOverrideStyleModifier modifier = new FeatureOverrideStyleModifier();
                    modifier.Style = style;
                    modifier.Name = "FeatureStyleModifier1";
                    layer.Modifiers.Append(modifier);  // *** Allow switching on and off *** //

                }
            }
            else if (l.Type == LayerType.Raster)  // Raster Layer //
            {
                // Create a feature override style modifier //
                CompositeStyle style = new CompositeStyle();
                style.RasterStyle.Grayscale = (Convert.ToInt32(dr["GreyScale"]) == 1 ? true : false);
                //style.RasterStyle.Transparent = true;    // *** This line commented out otherwise monocrome image layers don't display! *** //
                style.RasterStyle.Alpha = (int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100)));  // Controls Transparency //

                FeatureOverrideStyleModifier modifier = new FeatureOverrideStyleModifier();
                modifier.Style = style;
                modifier.Name = "FeatureStyleModifier1";
                layer.Modifiers.Append(modifier);  // *** Allow switching on and off *** //
            }

        }

        private void Update_Map_Object_Labels()
        {
            // Get Object Label Layer - Layer ID //
            // Load Label Properties from Mapping_Workspace_Layer_Label_Font passing Object Label Layer ID as a parameter //
            // Set Label styling info according to data from Mapping_Workspace_Layer_Label_Font record //
            // Load Label and Tooltip structure from Mapping_Workspace_Layer_Label_Structure table //
            // Parse Label structure to form string value //
            // Parse ToolTip structure to form string value //
            // Update map with Label and Tootip structure //

            GridView view = (GridView)gridControl5.MainView;
            if (view == null) return;

            Build_Label_And_Tooltip_Structure();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (view.GetRowCellValue(i, "LayerPath").ToString() == "MapObjectLabels")
                {
                    int intTransparency = Convert.ToInt32(view.GetRowCellValue(i, "Transparency"));
                    int intLineColour = Convert.ToInt32(view.GetRowCellValue(i, "LineColour"));
                    int intLineWidth = Convert.ToInt32(view.GetRowCellValue(i, "LineWidth"));

                    if (dsFontSettings.Tables[0].Rows.Count <= 0) return;
                    DataRow dr = dsFontSettings.Tables[0].Rows[0];

                    // Close any Existing Label Layer First - Enumerate through label layers only then reconstruct Label Layer //
                    MapLayerEnumerator mle = mapControl1.Map.Layers.GetMapLayerEnumerator(MapLayerFilterFactory.FilterByLayerType(LayerType.Label));
                    while (mle.MoveNext())
                    {
                        LabelLayer current = mle.Current as LabelLayer;
                        if (current != null) mapControl1.Map.Layers.Remove(current);
                    }

                    MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                    conn.Open();
                    MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");

                    // Create a SimpleLineStyle to be used as the callout line's style //
                    MapInfo.Styles.SimpleLineStyle lineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), 2, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intLineColour))));
                    // Construct a TextStyle with a SimpleLineStyle for the callout line //
                    Color FontColour = Color.FromArgb(Convert.ToInt32(dr["FontColour"]));
                    MapInfo.Styles.TextStyle textStyle = new MapInfo.Styles.TextStyle(new MapInfo.Styles.Font(dr["FontName"].ToString(), Convert.ToInt32(dr["FontSize"]), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intTransparency) / 100))), FontColour), FontColour, MapInfo.Styles.FontFaceStyle.Normal, MapInfo.Styles.FontWeight.Normal, MapInfo.Styles.TextEffect.None, MapInfo.Styles.TextDecoration.None, MapInfo.Styles.TextCase.Default, false, false), lineStyle);

                    MapInfo.Mapping.LabelLayer lblLayer = new MapInfo.Mapping.LabelLayer("Labels");
                    MapInfo.Mapping.LabelSource lblSource = new MapInfo.Mapping.LabelSource(t);
                    lblSource.DefaultLabelProperties.Visibility.AllowOverlap = (Convert.ToInt32(dr["ShowOverlapping"]) == 1 ? true : false);
                    lblSource.DefaultLabelProperties.Visibility.AllowDuplicates = (Convert.ToInt32(dr["ShowDuplicates"]) == 1 ? true : false);
                    lblSource.DefaultLabelProperties.Visibility.AllowOutOfView = true;
                    lblSource.DefaultLabelProperties.Style = textStyle;
                    lblSource.DefaultLabelProperties.Caption = i_str_label_structure;  // *** NEED TO ADD TOOLTIP *** //
                    lblSource.DefaultLabelProperties.Visibility.Enabled = true;
                    lblSource.DefaultLabelProperties.Visibility.VisibleRangeEnabled = true;
                    lblSource.DefaultLabelProperties.Visibility.VisibleRange = new VisibleRange(Convert.ToInt32(dr["VisibleFromScale"]), Convert.ToInt32(dr["VisibleToScale"]), MapInfo.Geometry.DistanceUnit.Meter);
                    //lblSource.Maximum = 50;
                    lblSource.DefaultLabelProperties.Layout.UseRelativeOrientation = false;
                    //lblSource.DefaultLabelProperties.Layout.RelativeOrientation = MapInfo.Text.RelativeOrientation.FollowPath;
                    lblSource.DefaultLabelProperties.Layout.Angle = Convert.ToInt32(dr["FontAngle"]);
                    //lblSource.DefaultLabelProperties.Priority.Major = "Pop_1994";
                    lblSource.DefaultLabelProperties.Layout.Offset = Convert.ToInt32(dr["Offset"]);
                    switch (dr["Position"].ToString())
                    {
                        case "BottomCentre":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.BottomCenter;
                            break;
                        case "BottomLeft":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.BottomLeft;
                            break;
                        case "BottomRight":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.BottomRight;
                            break;
                        case "CentreCenter":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.CenterCenter;
                            break;
                        case "CentreLeft":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.CenterLeft;
                            break;
                        case "CentreRight":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.CenterRight;
                            break;
                        case "TopCentre":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.TopCenter;
                            break;
                        case "TopLeft":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.TopLeft;
                            break;
                        case "TopRight":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.TopRight;
                            break;
                        default:
                            break;
                    }
                    lblLayer.Sources.Append(lblSource);
                    int intLayerPosition = mapControl1.Map.Layers.Add(lblLayer);
                    if (intLayerPosition >= 0)
                    {
                        MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[intLayerPosition];
                        l.Name = "MapObjectLabels";
                    }
                    conn.Close();
                    conn.Dispose();

                    // Set Default Tooltip... //
                    MapInfo.Tools.MapTool.SetInfoTipExpression(mapControl1.Tools.MapToolProperties, mapControl1.Map.Layers["MapObjects"] as FeatureLayer, i_str_tooltip_structure);

                    break;
                }
            }
            return;
        }

        private void Build_Label_And_Tooltip_Structure()
        {
            i_str_label_structure = "";
            i_str_tooltip_structure = "";
            // Can't directly sort datatable, so use it's default view //
            dsLabelStructure.Tables[0].DefaultView.Sort = "[StructureType] asc, [FieldOrder] asc";  // Re-sort rows in case any new rows have been added to end //
            dsLabelStructure.Tables[0].DefaultView.RowFilter = "[StructureType] = 0";  // Filter on just Labels //
            int intMaxCount = dsLabelStructure.Tables[0].DefaultView.Count;
            int intCount = 0;
            foreach (DataRowView dr in dsLabelStructure.Tables[0].DefaultView)
            {
                intCount++;
                if (dr["Prefix"].ToString() != "")
                {
                    i_str_label_structure += "'" + dr["Prefix"].ToString() + "'+";
                }
                i_str_label_structure += dr["ColumnName"].ToString();
                if (dr["Suffix"].ToString() != "")
                {
                    i_str_label_structure += "+'" + dr["Suffix"].ToString() + "'";
                }
                if (intCount < intMaxCount)
                {
                    if (Convert.ToInt32(dr["NewLine"]) == 1)
                    {
                        i_str_label_structure += "+char(13)+";
                    }
                    else
                    {
                        i_str_label_structure += "+'" + dr["Seperator"].ToString() + "'+";
                    }
                }
            }

            dsLabelStructure.Tables[0].DefaultView.RowFilter = "[StructureType] = 1";  // Filter on just Tooltips //
            intMaxCount = dsLabelStructure.Tables[0].DefaultView.Count;
            intCount = 0;
            foreach (DataRowView dr in dsLabelStructure.Tables[0].DefaultView)
            {
                intCount++;
                if (dr["Prefix"].ToString() != "")
                {
                    i_str_tooltip_structure += "'" + dr["Prefix"].ToString() + "'+";
                }
                i_str_tooltip_structure += dr["ColumnName"].ToString();
                if (dr["Suffix"].ToString() != "")
                {
                    i_str_tooltip_structure += "+'" + dr["Suffix"].ToString() + "'";
                }
                if (intCount < intMaxCount)
                {
                    if (Convert.ToInt32(dr["NewLine"]) == 1)
                    {
                        i_str_tooltip_structure += "+char(13)+";
                    }
                    else
                    {
                        i_str_tooltip_structure += "+'" + dr["Seperator"].ToString() + "'+";
                    }
                }
            }
            dsLabelStructure.Tables[0].DefaultView.RowFilter = "";  // Remove Filter //
        }


        #region Drag Drop Functionality

        // AutoScrollHelper autoScrollHelper declared under instance vars //
        // autoScrollHelper linked to grid in Form Load Event //
        // gridControl5.AllowDrop = true in form Load Event //

        int dropTargetRowHandle = -1;
        int DropTargetRowHandle
        {
            get
            {
                return dropTargetRowHandle;
            }
            set
            {
                dropTargetRowHandle = value;
                gridControl5.Invalidate();
            }
        }
        const string OrderFieldName = "LayerOrder";

        private void gridControl5_DragOver(object sender, DragEventArgs e)
        {
            GridControl grid = (GridControl)sender;
            System.Drawing.Point pt = new System.Drawing.Point(e.X, e.Y);
            pt = grid.PointToClient(pt);
            GridView view = grid.GetViewAt(pt) as GridView;
            if (view == null) return;

            if (view.FocusedRowHandle == 0 || view.FocusedRowHandle == 1 || view.FocusedRowHandle == 2)  // Don't allow Map Objects or Map Objects Labels or Temporary Map Objects [may be -1 if at end of grid] //
            {
                DropTargetRowHandle = -1;
                return;
            }

            GridHitInfo hitInfo = view.CalcHitInfo(pt);
            if (hitInfo.HitTest == GridHitTest.EmptyRow)
            {
                DropTargetRowHandle = view.DataRowCount;
            }
            else
            {
                DropTargetRowHandle = hitInfo.RowHandle;
            }

            e.Effect = DragDropEffects.None;

            GridHitInfo downHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            if (downHitInfo != null)
            {
                //if (hitInfo.RowHandle != downHitInfo.RowHandle)
                if (hitInfo.RowHandle != downHitInfo.RowHandle && (hitInfo.RowHandle < 0 || hitInfo.RowHandle > 2))  // Everything after && Stops row 1, 2 and 3 being moved //
                    e.Effect = DragDropEffects.Move;
            }

            autoScrollHelper.ScrollIfNeeded(hitInfo);
        }

        private void gridControl5_DragDrop(object sender, DragEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.MainView as GridView;
            GridHitInfo srcHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            GridHitInfo hitInfo = view.CalcHitInfo(grid.PointToClient(new System.Drawing.Point(e.X, e.Y)));
            int sourceRow = srcHitInfo.RowHandle;
            int targetRow = (hitInfo.RowHandle == GridControl.InvalidRowHandle ? -1 : hitInfo.RowHandle);

            if (DropTargetRowHandle < 0) return;  // No Valid Place for Dropping //

            DropTargetRowHandle = -1;
            MoveRow(sourceRow, targetRow, view, true);
        }

        private void gridControl5_DragLeave(object sender, EventArgs e)
        {
            DropTargetRowHandle = -1;
        }

        private void gridControl5_Paint(object sender, PaintEventArgs e)
        {
            if (DropTargetRowHandle < 0) return;
            GridControl grid = (GridControl)sender;
            GridView view = (GridView)grid.MainView;

            bool isBottomLine = DropTargetRowHandle == view.DataRowCount;

            GridViewInfo viewInfo = view.GetViewInfo() as GridViewInfo;
            GridRowInfo rowInfo = viewInfo.GetGridRowInfo(isBottomLine ? DropTargetRowHandle - 1 : DropTargetRowHandle);

            if (rowInfo == null) return;

            System.Drawing.Point p1, p2;
            if (isBottomLine)
            {
                p1 = new System.Drawing.Point(rowInfo.Bounds.Left, rowInfo.Bounds.Bottom - 1);
                p2 = new System.Drawing.Point(rowInfo.Bounds.Right, rowInfo.Bounds.Bottom - 1);
            }
            else
            {
                p1 = new System.Drawing.Point(rowInfo.Bounds.Left, rowInfo.Bounds.Top - 1);
                p2 = new System.Drawing.Point(rowInfo.Bounds.Right, rowInfo.Bounds.Top - 1);
            }
            e.Graphics.DrawLine(Pens.Blue, p1, p2);
        }

        private void gridView5_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == MouseButtons.Left && downHitInfo != null)
            {
                Size dragSize = SystemInformation.DragSize;
                System.Drawing.Rectangle dragRect = new System.Drawing.Rectangle(new System.Drawing.Point(downHitInfo.HitPoint.X - dragSize.Width / 2, downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new System.Drawing.Point(e.X, e.Y)))
                {
                    view.GridControl.DoDragDrop(downHitInfo, DragDropEffects.All);
                    downHitInfo = null;
                }
            }
        }

        private void MoveRow(int sourceRow, int targetRow, GridView view, bool MoveMapLayer)
        {
            view.BeginDataUpdate();
            view.BeginSort();
            if (sourceRow > targetRow && targetRow != -1)
            {
                for (int i = targetRow; i < sourceRow; i++)
                {
                    view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) + 1);
                }
                DataRow dragRow = view.GetDataRow(sourceRow);
                dragRow[OrderFieldName] = targetRow;
                if (MoveMapLayer) Adjust_Layer_Order(sourceRow, targetRow);  // Update Map's Layer Position //
            }
            else
            {
                if (targetRow > sourceRow && targetRow != -1) // Not trying to position on end of grid //
                {
                    for (int i = targetRow - 1; i > sourceRow; i--)
                    {
                        view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) - 1);
                    }
                    DataRow dragRow = view.GetDataRow(sourceRow);
                    dragRow[OrderFieldName] = targetRow - 1;
                    if (MoveMapLayer) Adjust_Layer_Order(sourceRow, targetRow - 1);  // Update Map's Layer Position //
                }
                else // Positioning on end of grid //
                {
                    targetRow = view.DataRowCount - 1;
                    for (int i = targetRow; i > sourceRow; i--)
                    {
                        view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) - 1);
                    }
                    DataRow dragRow = view.GetDataRow(sourceRow);
                    dragRow[OrderFieldName] = targetRow;
                    if (MoveMapLayer) Adjust_Layer_Order(sourceRow, targetRow);  // Update Map's Layer Position //
                }
            }
            view.EndSort();
            view.EndDataUpdate();
        }

        #endregion


        private void Adjust_Layer_Order(int intOriginalPosition, int intNewPosition)
        {
            // Note: Layer order should be reversed as MapInfo draws the layers in reverse order (Last layer is on top) //
            //intOriginalPosition = mapControl1.Map.Layers.Count - 1 - intOriginalPosition;
            //intNewPosition = mapControl1.Map.Layers.Count - 1 - intNewPosition;

            mapControl1.Map.Layers.Move(intOriginalPosition, intNewPosition);
        }


        #region GridView5
        
        private void gridView5_MouseDown(object sender, MouseEventArgs e)
        {
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /*if (i_intLoadedTemplateID == 0)
                {
                    bbiSaveTemplateAs.Enabled = false;
                }
                else
                {
                    bbiSaveTemplateAs.Enabled = true;
                }

                if (i_intLoadedTemplateCreatedByID != GlobalSettings.UserID && i_intLoadedTemplateID > 0 && GlobalSettings.PersonType != 0)
                {
                    bbiSaveTemplate.Enabled = false;
                }
                else
                {
                    bbiSaveTemplate.Enabled = true;
                }
                */
                int[] intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length > 0)
                {
                    bbiLayerManagerProperties.Enabled = (view.GetFocusedRowCellValue("LayerName").ToString() == "Temporary Map Objects" ? false : true);
                }
                else
                {
                    bbiLayerManagerProperties.Enabled = false;
                }
                popupMenu_LayerManager.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView5_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No map layers available");
        }

        private void gridView5_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LayerHitable":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LayerType")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();

        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    e.Handled = true;
                    AddRecord();
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.CancelEdit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Edit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.EndEdit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.NextPage:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.PrevPage:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    e.Handled = true;
                    DeleteRecord();
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:

                    GridView view = gridView5;
                    if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 3) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "LayerOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "LayerOrder")) + 1);
                        Adjust_Layer_Order(intFocusedRow, intFocusedRow - 1);  // Update Map's Layer Position //
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 2 || intFocusedRow >= view.DataRowCount - 1) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "LayerOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "LayerOrder")) - 1);
                        Adjust_Layer_Order(intFocusedRow, intFocusedRow + 1);  // Update Map's Layer Position //
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    break;
                default:
                    break;
            }

        }
        
        private void gridView5_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridView view = gridView5;
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (e.FocusedRowHandle == GridControl.InvalidRowHandle || e.FocusedRowHandle <= 3 ? false : true);
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (e.FocusedRowHandle == GridControl.InvalidRowHandle || e.FocusedRowHandle <= 2 || e.FocusedRowHandle >= view.DataRowCount - 1 ? false : true);
        }

        private void repositoryItemCE_LayerVisible_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            GridView view = (GridView)gridControl5.MainView;
            Set_LayerVisibility(view.FocusedRowHandle, ce.Checked);
        }

        private void repositoryItemCE_LayerHitable_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            GridView view = (GridView)gridControl5.MainView;
            string strFileName = view.GetFocusedRowCellValue("LayerPath").ToString();
            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
            {
                if (l.Name == strFileName)
                {
                    if (l.Name == "MapObjects")
                    {
                        MapInfo.Mapping.LayerHelper.SetSelectable(l, ce.Checked);
                        MapInfo.Mapping.LayerHelper.SetEditable(l, ce.Checked);
                        MapInfo.Mapping.LayerHelper.SetInsertable(l, ce.Checked);
                    }
                    else if (l.Name == "MapObjectLabels")
                    {
                        mapControl1.Tools.SelectMapToolProperties.LabelsAreEditable = ce.Checked;
                    }
                    else
                    {
                        MapInfo.Mapping.LayerHelper.SetSelectable(l, ce.Checked);
                    }
                }
            }
        }

        #endregion


        private void Set_LayerVisibility(int intGridRow, bool boolVisibility)
        {
            GridView view = (GridView)gridControl5.MainView;
            string strFileName = view.GetRowCellValue(intGridRow, "LayerPath").ToString();
            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
            {
                if (l.Name == strFileName) l.Enabled = boolVisibility;
            }
        }

        public void ChildForm_RefreshThematicItemDataSet(int intHeaderID, int intHeaderType, int intDefaultSymbol, int intDefaultSymbolColour, int intDefaultSymbolSize, int intDefaultPolygonBoundaryColour, int intDefaultPolygonBoundaryWidth, int intDefaultPolygonFillColour, int intDefaultPolygonFillPattern, int intDefaultPolygonFillPatternColour, int intDefaultLineStyle, int intDefaultLineColour, int intDefaultLineWidth)
        {
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp01255_AT_Tree_Picker_Thematic_Mapping_Default_Items", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PickListID", intHeaderID));
            cmd.Parameters.Add(new SqlParameter("@PickListType", intHeaderType));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbol", intDefaultSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolColour", intDefaultSymbolColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolSize", intDefaultSymbolSize));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryColour", intDefaultPolygonBoundaryColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryWidth", intDefaultPolygonBoundaryWidth));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillColour", intDefaultPolygonFillColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPattern", intDefaultPolygonFillPattern));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPatternColour", intDefaultPolygonFillPatternColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineSymbol", intDefaultLineStyle));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineColour", intDefaultLineColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineWidth", intDefaultLineWidth));
            dsThematicStyleItems.Clear();  // Remove old values first //
            sdaThematicStyleItems = new SqlDataAdapter(cmd);
            sdaThematicStyleItems.Fill(dsThematicStyleItems, "Table");
            intThematicStylingFieldHeaderID = intHeaderID;
            intThematicStylingFieldHeaderType = intHeaderType;
            SQlConn.Close();
            SQlConn.Dispose();

            if (intHeaderType == 0)  // Selected Thematic set is an in-memory set as opposed to a saved set //
            {
                intThematicStylingBasePicklistID = intHeaderID;
            }
            else  // Set is saved so we need to get the controlling field ID from it.
            {
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter GetSetting = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    intThematicStylingBasePicklistID = Convert.ToInt32(GetSetting.sp01289_AT_Tree_Picker_Style_Get_Bast_PicklistID(intHeaderID));
                }
                catch (Exception)
                {
                    intThematicStylingBasePicklistID = 0;
                }
            }
        }

        public void ChildForm_RefreshListOfAvailableStyleSets(int intNewStyleSetID, int intNewStyleSetType)
        {
            int intCurrentUser = this.GlobalSettings.UserID;
            int intCurrentUserType = this.GlobalSettings.PersonType;
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp01256_AT_Tree_Picker_Thematic_Mapping_Available_Sets", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@intUserID", intCurrentUser));
            cmd.Parameters.Add(new SqlParameter("@intUserType", intCurrentUserType));
            sdaThematicStyleSets = new SqlDataAdapter(cmd);
            dsThematicStyleSets.Clear();
            sdaThematicStyleSets.Fill(dsThematicStyleSets, "Table");
            intThematicStylingFieldHeaderID = intNewStyleSetID;
            intThematicStylingFieldHeaderType = intNewStyleSetType;

            SQlConn.Close();
            SQlConn.Dispose();
        }

        #endregion


        #region grid2 [Tree Map Objects]

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.Tag.ToString())
            {
                case "refresh":
                    WaitDialogForm loading = new WaitDialogForm("Loading Available Map Objects...", "WoodPlan Mapping");
                    loading.Show();

                    if (i_str_selected_client_ids == null) i_str_selected_client_ids = "";
                    Load_Map_Objects_Grid();

                    loading.Close();
                    break;
            }
        }

        private void gridView2_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No objects available");
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }
        
        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                //{
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                if (intCount > 0)
                {
                    bbiHighlightYes.Enabled = true;
                    bbiHighlightNo.Enabled = true;
                    bbiVisibleYes.Enabled = true;
                    bbiVisibleNo.Enabled = true;
                }
                else
                {
                    bbiHighlightYes.Enabled = false;
                    bbiHighlightNo.Enabled = false;
                    bbiVisibleYes.Enabled = false;
                    bbiVisibleNo.Enabled = false;
                }
                bbiViewOnMap.Enabled = (intCount == 1 ? true : false);
                bbiCopyHighlightFromVisible.Enabled = true;

                bool boolRowsSelected = false;
                for (int i = 0; i < view.RowCount; i++)  // Use RowCount and not DataRowCount as it's visible rows we are concerned with //
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                    {
                        boolRowsSelected = true;
                        break;
                    }
                }
                if (boolRowsSelected)
                {
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bbiDatasetCreate.Enabled = false;
                }
                if (view.RowCount > 0)  // Use RowCount and not DataRowCount as it's visible rows we are concerned with //
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;

                popupMenu1.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemCheckEditHighlight_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            GridView view = (GridView)gridControl2.MainView;
            if (ce.Checked) view.SetRowCellValue(view.FocusedRowHandle, "CheckMarkSelection", 1);
            if (ce.Checked) view.SetRowCellValue(view.FocusedRowHandle, "Highlight", 1);
        }

        private void SetButtonStatus()
        {
            /*           if (selection1.SelectedCount == 0)
                       {
                           foreach(EditorButton button in colorEditHighlight.Properties.Buttons) 
                           {
                               if (button.Tag == "Highlight") button.Enabled = false;
                           }
                       }
                       else
                       {
                           foreach(EditorButton button in colorEditHighlight.Properties.Buttons) 
                           {
                               if (button.Tag == "Highlight") button.Enabled = true;
                           }
                       }*/
        }

        private void colorEditHighlight_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "Highlight")
            {
                int intSelectedCount = 0;

                GridView view = (GridView)gridControl2.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1) intSelectedCount++;
                }
                view = (GridView)gridControlAssetObjects.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1) intSelectedCount++;
                }

                if (intSelectedCount == 0)
                {
                    XtraMessageBox.Show("Select one or more map objects to highlight before proceeding.", "Highlight Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Highlight_Clicked(true, intSelectedCount);
            }
            else if (e.Button.Tag.ToString() == "Clear")
            {
                ClearHighlight();
            }
        }

        private void colorEditHighlight_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            if (intMapObjectTransparency > 0)
            {
                Clear_MapInfo_Themes();  // Clear Any MapInfo Thematics already defined - these will be created later if required //
                Create_MapInfo_Theme();  // Transparency on, so we need to create a Map Info Theme to handle thematics //-
            }
        }

        private void Highlight_Clicked(bool boolShowProgress, int intSelectedCount)
        {
            GridView view = (GridView)gridControl2.MainView;
            frmProgress fProgress = new frmProgress(10);
            if (boolShowProgress)
            {
                fProgress.UpdateCaption("Highlighting Map Objects...");
                fProgress.Show();
                Application.DoEvents();
            }
            int intUpdateProgressThreshhold = intSelectedCount / 10;
            int intUpdateProgressTempCount = 0;

            string strMapID = "";
            int intProcessedCount = 0;
            int intMatchingCount = 0;
            System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;

            double MinX = (double)9999999.00;  // Used for Zooming to Scale //
            double MinY = (double)9999999.00;
            double MaxX = (double)-9999999.00;
            double MaxY = (double)-9999999.00;


            mapControl1.SuspendLayout();  // Switch of Map Redraw until done //
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    intProcessedCount++;
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                    if (intProcessedCount == intSelectedCount && ceCentreMap.Checked)
                    {
                        intMatchingCount += FindAndHighlight(strMapID, true, false, true, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }
                    else
                    {
                        intMatchingCount += FindAndHighlight(strMapID, false, false, true, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }

                    if (boolShowProgress)
                    {
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                    }
                    if (intProcessedCount >= intSelectedCount) break;
                }
            }
            // Now Process Assets Grid //
            intProcessedCount = 0;
            view = (GridView)gridControlAssetObjects.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    intProcessedCount++;
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                    if (intProcessedCount == intSelectedCount && ceCentreMap.Checked)
                    {
                        intMatchingCount += FindAndHighlight(strMapID, true, false, true, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }
                    else
                    {
                        intMatchingCount += FindAndHighlight(strMapID, false, false, true, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }

                    if (boolShowProgress)
                    {
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                    }
                    if (intProcessedCount >= intSelectedCount) break;
                }
            }
            if (csScaleMapForHighlighted.Checked)
            {
                // *** Scale map to show all highlighted fatures *** //
                // First Get a margin to add //
                double dMargin = (double)0.0;
                double dMaxVariance = (double)0.00;
                if ((MaxX - MinX) > (MaxY - MinY))
                {
                    dMaxVariance = MaxX - MinX;
                }
                else
                {
                    dMaxVariance = MaxY - MinY;
                }
                dMargin = (0.025 * dMaxVariance) - 0.47;

                MapInfo.Geometry.DRect rect = new MapInfo.Geometry.DRect(MinX - dMargin, MinY - dMargin, MaxX + dMargin, MaxY + dMargin);  // Create rectangle on coordinated with applied margins //
                MapInfo.Geometry.Envelope envelope = new MapInfo.Geometry.Envelope(mapControl1.Map.GetDisplayCoordSys(), rect);  // Create envelopeusing current map projection and rectangle //
                this.mapControl1.Map.Bounds = envelope.Bounds;  // Scale and centre map on rectangle //
                if (this.mapControl1.Map.Scale < 250)  // If Scale has zoomed in too far, zoom out to a sensible scale //
                {
                    this.mapControl1.Map.Scale = 250;
                }
            }

            mapControl1.ResumeLayout();  // Switch on Map Redraw //

            if (boolShowProgress)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
            }
        }

        private int FindAndHighlight(string MapID, bool CentreOnMap, bool ShowLocusEfect, bool HighlightObject, System.Drawing.Color HighlightColour, ref double MinX, ref double MinY, ref double MaxX, ref double MaxY)
        {
            CompositeStyle style = new CompositeStyle();

            //*** Get Map Object to update ***//
            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("id = '" + MapID + "'");
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            connection.Close();
            connection.Dispose();

            foreach (Feature ftr in irfc)  // Should only be one feature in collection //
            {
                if (HighlightObject)  // Change objects background colour to passed in Highlight colour //
                {
                    if (intMapObjectTransparency <= 0)
                    {
                        if (ftr.Style.ToString().Contains("MapInfo.Styles.AreaStyle"))
                        {
                            style.AreaStyle = (AreaStyle)ftr.Style;
                            if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                            }
                            else
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                            }
                        }
                        else if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleVectorPointStyle"))
                        {
                            style.SymbolStyle = (SimpleVectorPointStyle)ftr.Style;
                            style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                        }
                        else if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleLineStyle"))
                        {
                            style.LineStyle = (SimpleLineStyle)ftr.Style;
                            ((SimpleLineStyle)style.LineStyle).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                        }
                        ftr.Style = style;
                    }
                    ftr["intHighlighted"] = 1;
                    ftr["Thematics"] = Convert.ToDouble(999995);
                    ftr.Update();
                }
                ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //
                if (ftr.Geometry.Bounds.x1 < MinX) MinX = ftr.Geometry.Bounds.x1;
                if (ftr.Geometry.Bounds.x2 > MaxX) MaxX = ftr.Geometry.Bounds.x2;
                if (ftr.Geometry.Bounds.y1 < MinY) MinY = ftr.Geometry.Bounds.y1;
                if (ftr.Geometry.Bounds.y2 > MaxY) MaxY = ftr.Geometry.Bounds.y2;

                if (CentreOnMap)
                {
                    Double dblCurrentScale = (this.mapControl1.Map.Scale > 5000 ? 5000 : this.mapControl1.Map.Scale);
                    this.mapControl1.Map.SetView(ftr);
                    this.mapControl1.Map.Scale = dblCurrentScale;
                }

                if (ShowLocusEfect)
                {
                    // Draw Locus Effect on object so user can see it //
                    System.Drawing.Point screenPoint;
                    MapInfo.Geometry.DisplayTransform converter = this.mapControl1.Map.DisplayTransform;
                    converter.ToDisplay(ftr.Geometry.Centroid, out screenPoint);

                    System.Drawing.Point MapControlPoint = new System.Drawing.Point();
                    MapControlPoint = this.mapControl1.PointToScreen(MapControlPoint);

                    screenPoint.X += MapControlPoint.X;
                    screenPoint.Y += MapControlPoint.Y;

                    //locusEffectsProvider1.ShowLocusEffect(this, ptCentreMapObject, LocusEffectsProvider.DefaultLocusEffectArrow);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customBeaconLocusEffect1.Name);
                }
            }
            return 1;
        }

        private int ClearHighlight()
        {
            int intSelectedCount = 0;

            GridView view = (GridView)gridControl2.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1) intSelectedCount++;
            }
            view = (GridView)gridControlAssetObjects.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1) intSelectedCount++;
            }
            if (intSelectedCount == 0) return 0;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Clearing Highlighted Map Objects...");
            fProgress.Show();
            Application.DoEvents();
            int intUpdateProgressThreshhold = intSelectedCount / 10;


            // Prepare Thematic styling first //
            int intPicklistValue = 0;
            decimal decPicklistValue = (decimal)0.00;
            string strThematicsGridViewColumn = "";
            System.Drawing.Color colorObjectColour = Color.Red;

            if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)
            {
                strThematicsGridViewColumn = Get_ThematicField_ColumnName(intThematicStylingBasePicklistID);
            }
            // End of Prepare Thematics //

            string strMapID = "";
            int intUpdateProgressTempCount = 0;
            view = (GridView)gridControl2.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                    intPicklistValue = Convert.ToInt32(view.GetRowCellValue(i, strThematicsGridViewColumn));
                    decPicklistValue = Convert.ToDecimal(view.GetRowCellValue(i, strThematicsGridViewColumn));

                    Reset_Map_Object_Back_Colour(strMapID, (intUseThematicStyling == 1 ? true : false), intPicklistValue, decPicklistValue);

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            // Now Process Assets Grid //
            view = (GridView)gridControlAssetObjects.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                    //intPicklistValue = Convert.ToInt32(view.GetRowCellValue(i, strThematicsGridViewColumn));
                    //decPicklistValue = Convert.ToDecimal(view.GetRowCellValue(i, strThematicsGridViewColumn));
                    //Reset_Map_Object_Back_Colour(strMapID, (intUseThematicStyling == 1 ? true : false), intPicklistValue, decPicklistValue);
                    Reset_Map_Object_Back_Colour(strMapID, false, intPicklistValue, decPicklistValue);

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }
            return 1;
        }

        private void JustHighlight(ref CompositeStyle style, System.Drawing.Color HighlightColour)
        {
            if (intMapObjectTransparency <= 0)
            {
                if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)
                {
                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                }
                else
                {
                    ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                }
                style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                ((SimpleLineStyle)style.LineStyle).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            }
        }

        private void Reset_Map_Object_Back_Colour(string MapID, bool UseThematics, int intThematicPickListValue, decimal decThematicPickListValue)
        {
            CompositeStyle style = new CompositeStyle();

            //*** Get Map Object to update ***//
            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("id = '" + MapID + "'");
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            connection.Close();
            connection.Dispose();
            foreach (Feature ftr in irfc)  // Should only be one feature in collection //
            {
                if (intMapObjectTransparency <= 0)
                {
                    if (ftr.Style.ToString().Contains("MapInfo.Styles.AreaStyle"))
                    {
                        style.AreaStyle = (AreaStyle)ftr.Style;
                        if (UseThematics)
                        {
                            if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)  // Solid Fill //
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Get_Thematic_BackColour(intThematicPickListValue, decThematicPickListValue, "PolygonForeColor");
                            }
                            else // Pattern Fill //
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Get_Thematic_BackColour(intThematicPickListValue, decThematicPickListValue, "Polygon");
                            }
                        }
                        else
                        {
                            if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)  // Solid Fill //
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Get_Default_BackColour("PolygonForeColor");
                            }
                            else  // Pattern Fill //
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Get_Default_BackColour("Polygon");
                            }
                        }
                    }
                    else if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleVectorPointStyle"))
                    {
                        style.SymbolStyle = (SimpleVectorPointStyle)ftr.Style;
                        if (UseThematics)
                        {
                            style.SymbolStyle.Color = Get_Thematic_BackColour(intThematicPickListValue, decThematicPickListValue, "Point");
                        }
                        else
                        {
                            style.SymbolStyle.Color = Get_Default_BackColour("Point");
                        }
                    }
                    else if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleLineStyle"))
                    {
                        style.LineStyle = (SimpleLineStyle)ftr.Style;
                        if (UseThematics)
                        {
                            ((SimpleLineStyle)style.LineStyle).Color = Get_Thematic_BackColour(intThematicPickListValue, decThematicPickListValue, "Polyline");
                        }
                        else
                        {
                            ((SimpleLineStyle)style.LineStyle).Color = Get_Default_BackColour("Polyline");
                        }
                    }
                    ftr.Style = style;
                }
                ftr["intHighlighted"] = 0;
                ftr["Thematics"] = Convert.ToDouble(decThematicPickListValue);
                ftr.Update();
            }
        }

        private void Update_Map_Object_Sizes(bool ShowProgress)
        {
            // Loops through all the visible point map objects and calculates their sizes //
            // Only called when the scale of the map is changed [Map_ViewChanged Event] and Dynamic Sizing according to Crown Diameter is switched on //

            CompositeStyle style = new CompositeStyle();

            //*** Get Map Objects to update ***//
            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("intObjectType = 0");  // Only Points [intObjectType = 0] //
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            connection.Close();
            connection.Dispose();

            foreach (Feature ftr in irfc)
            {
                if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleVectorPointStyle"))
                {
                    style.SymbolStyle = (SimpleVectorPointStyle)ftr.Style;
                    style.SymbolStyle.PointSize = Calculate_Point_Size(ftr);

                    ftr.Style = style;
                    ftr.Update();
                }
            }
        }

        private int Calculate_Point_Size(Feature ftr)
        {
            int intCalculatedSize = 0;
            intCalculatedSize = Convert.ToInt32(ftr["CrownDiameter"]);
            if (intCalculatedSize <= 0)
            {
                intCalculatedSize = 2;
                // Do something to highlight the object as missing a value //
            }
            else if (intCalculatedSize > 240)
            {
                intCalculatedSize = 240;
                // Do something to highlight the object as a bigger value //
            }
            intCalculatedSize = Convert.ToInt32(Math.Round(Convert.ToDouble(intCalculatedSize) / (0.000209 * mapControl1.Map.Scale), 0));
            if (intCalculatedSize < 2)
            {
                intCalculatedSize = 2;
            }
            else if (intCalculatedSize > 240)
            {
                intCalculatedSize = 240;
            }
            return intCalculatedSize;
        }

        private System.Drawing.Color Get_Default_BackColour(string strObjectType)
        {
            DataRow drStyle = dsDefaultStyles.Tables[0].Rows[0];
            switch (strObjectType)
            {
                case "Polygon":
                    int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
                    if (intDefaultPolygonFillPattern == 2) // Solid //
                    {
                        return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillPatternColour"])));
                    }
                    else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                    {
                        return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillColour"])));
                    }
                case "PolygonForeColor":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillColour"])));
                case "Polyline":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolylineColour"])));
                case "Point":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["SymbolColour"])));
                default:
                    return Color.Red;
            }
        }

        private System.Drawing.Color Get_Thematic_BackColour(int intPicklistValue, decimal decFieldValue, string strObjectType)
        {
            if (intThematicStylingBasePicklistID == 0) return Get_Default_BackColour(strObjectType);
            DataRow[] drFiltered;
            if (intThematicStylingBasePicklistID >= 90000)  // 90000+ = Thematic Set using Continues Variables [BandStart and BandEnd] //
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("StartBand <= " + decFieldValue.ToString() + " and EndBand >= " + decFieldValue.ToString());
            }
            else
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("PicklistItemID = " + intPicklistValue.ToString());
            }

            DataRow drStyle;
            if (drFiltered.Length != 0)
            {
                drStyle = drFiltered[0];
            }
            else
            {
                drStyle = dsDefaultStyles.Tables[0].Rows[0];
            }
            switch (strObjectType)
            {
                case "Polygon":
                    int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
                    if (intDefaultPolygonFillPattern == 2) // Solid //
                    {
                        return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillPatternColour"])));
                    }
                    else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                    {
                        return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillColour"])));
                    }
                case "PolygonForeColor":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillColour"])));
                case "Polyline":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolylineColour"])));
                case "Point":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["SymbolColour"])));
                default:
                    return Color.Red;
            }
        }

        private void btnRefreshMapObjects_Click(object sender, EventArgs e)
        {
            UpdateMapObjectsDisplayed(true);
        }

        private void btnClearMapObjects_Click(object sender, EventArgs e)
        {
            mapControl1.SuspendLayout();  // Switch of Map Redraw until done //

            // Clear Layer of all Map Objects //
            try
            {
                MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                conn.Open();
                MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                MICommand MapCmd = conn.CreateCommand();
                MapCmd.CommandText = "Delete From MapObjects";
                MapCmd.Prepare();
                int nchanged = MapCmd.ExecuteNonQuery();
                MapCmd.Dispose();
                t.Pack(MapInfo.Data.PackType.All);  // Pack the table //
                conn.Close();
                conn.Dispose();
            }
            catch (Exception)
            {
            }
            mapControl1.ResumeLayout();
        }

        public void UpdateMapObjectsDisplayed(bool boolShowProgress)
        {
            // Prepare Thematic styling first //
            Clear_MapInfo_Themes();  // Clear Any MapInfo Thematics already defined - these will be created later if required //
            int intPicklistValue = 0;
            string strThematicsGridViewColumn = "";
            CompositeStyle style = new CompositeStyle();
            if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)
            {
                strThematicsGridViewColumn = Get_ThematicField_ColumnName(intThematicStylingBasePicklistID);
            }
            else // No Thematics so get default style //
            {
                //style = Get_Default_Style();
                strThematicsGridViewColumn = "intSpeciesID";
            }
            style = Get_Default_Style();

            // End of Prepare Thematics //

            GridView viewMapObject = (GridView)gridControl2.MainView;

            frmProgress fProgress = null;
            if (boolShowProgress)
            {
                fProgress = new frmProgress(10);
                fProgress.UpdateCaption("Loading Map Objects...");
                fProgress.Show();
                Application.DoEvents();
            }
            int intUpdateProgressThreshhold = (selection1.SelectedCount + selectionAssets.SelectedCount) / 10;
            int intUpdateProgressTempCount = 0;
            int intProcessedCount = 0;

            string strID = "";
            string strPolygonXY = "";
            int intCrownDiameter = 0;
            string[] strArray;
            char[] delimiters = new char[] { ';' };
            int iPoint = 0;
            int nchanged = 0;

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");

            mapControl1.SuspendLayout();  // Switch off Map Redraw until done //

            // Clear Layer of all Map Objects first //
            MICommand MapCmd = conn.CreateCommand();
            MapCmd.CommandText = "Delete From MapObjects";
            MapCmd.Prepare();
            nchanged = MapCmd.ExecuteNonQuery();
            MapCmd.Dispose();
            t.Pack(MapInfo.Data.PackType.All);  // Pack the table //
            // End of Clear Layer //

            t.BeginAccess(MapInfo.Data.TableAccessMode.Write);

            string strCurrentValue = "";
            // Load All Amenity Tree Map Objects ticked for inclusion //
            if (selection1.SelectedCount > 0)
            {
                for (int i = 0; i < viewMapObject.DataRowCount; i++)
                {
                    if (Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        intProcessedCount++;

                        if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)  // Get Thematic Style
                        {
                            intPicklistValue = Convert.ToInt32(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn));
                            if (intMapObjectTransparency <= 0) style = Get_Thematic_Style(intPicklistValue, Convert.ToDecimal(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn)));  // Pass value as both int and decimal and let function sort it out //
                        }

                        if (viewMapObject.GetRowCellValue(i, "ObjectType").ToString() == "Point")  // Load Point //
                        {
                            intCrownDiameter = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CrownDiameter"));
                            MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                            f.Geometry = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(viewMapObject.GetRowCellValue(i, "XCoordinate")), Convert.ToDouble(viewMapObject.GetRowCellValue(i, "YCoordinate")));
                            f["CalculatedSize"] = "";
                            f["CalculatedPerimeter"] = "";
                            f["intObjectType"] = 0;  //0 = Point //
                            f["id"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                            f["TreeID"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "TreeID"));
                            f["TreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                            f["ClientName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ClientName"));
                            f["SiteName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SiteName"));
                            f["Species"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "Species"));
                            f["CrownDiameter"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CrownDiameter"));
                            f["intHighlighted"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "Highlight"));
                            f["RiskFactor"] = Convert.ToDecimal(viewMapObject.GetRowCellValue(i, "RiskFactor"));
                            f["ModuleID"] = 1;  // 1 = Amenity Trees, 2 = Assets //
                            f["Thematics"] = (Convert.ToInt32(f["intHighlighted"]) == 0 ? Convert.ToDouble(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn)) : Convert.ToDouble(999995));
                            switch (intTreeRefStructureType)
                            {
                                case 0:
                                    f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                                    break;
                                case 1:
                                    strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                                    f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                    break;
                                case 2:
                                    strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));  // Note that the second commented out method also works //
                                    f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                    //string[] str1 = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Split(new string[] { strTreeRefStructureStripPattern }, 2, StringSplitOptions.None);
                                    //f["ShortTreeRef"] = (str1.Length == 1 ? str1[0] : str1[1]);
                                    break;
                                default:
                                    break;
                            }
                            if (intMapObjectSizing == -1)  // Dynamic Sizing according to Crown Diameter //
                            {
                                style.SymbolStyle.PointSize = Calculate_Point_Size(f);
                            }
                            f.Style = style;
                            MapInfo.Data.Key k = t.InsertFeature(f);
                        }
                        else  // Polygon / Polyline //
                        {
                            strID = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                            strPolygonXY = Convert.ToString(viewMapObject.GetRowCellValue(i, "PolygonXY"));

                            // Check there is an even number of points //
                            strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray.Length % 2 != 0)  // Modulus (%) //
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Invalid Polygon\\Polyline in database - MapID: " + strID + ".\r\nInvalid Number of Coordinates - Object ignored!", "Load Map Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            else
                            {
                                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                                iPoint = 0;
                                for (int j = 0; j < strArray.Length - 1; j++)
                                {
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                                    iPoint++;
                                }
                                MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                                    f["intObjectType"] = 1;  // 1 = Polygon //
                                    viewMapObject.SetRowCellValue(i, "ObjectType", "Polygon");
                                }
                                else
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["intObjectType"] = 2;  // 2 = PolyLine //
                                    viewMapObject.SetRowCellValue(i, "ObjectType", "Polyline");
                                }
                                f.Style = style;
                                f["id"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                                f["TreeID"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "TreeID"));
                                f["TreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                                f["ClientName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ClientName"));
                                f["SiteName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SiteName"));
                                f["Species"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "Species"));
                                f["CrownDiameter"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CrownDiameter"));
                                f["intHighlighted"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "Highlight"));
                                f["RiskFactor"] = Convert.ToDecimal(viewMapObject.GetRowCellValue(i, "RiskFactor"));
                                f["ModuleID"] = 1;  // 1 = Amenity Trees, 2 = Assets //
                                f["Thematics"] = (Convert.ToInt32(f["intHighlighted"]) == 0 ? Convert.ToDouble(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn)) : Convert.ToDouble(999995));

                                //if (f["TreeID"].ToString() == "31819")
                                //{
                                //    string str = "";
                                //}

                                switch (intTreeRefStructureType)
                                {
                                    case 0:
                                        f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                                        break;
                                    case 1:
                                        strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                                        f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                        break;
                                    case 2:
                                        strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));  // Note that the second commented out method also works //
                                        f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                        //string[] str1 = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Split(new string[] { strTreeRefStructureStripPattern }, 2, StringSplitOptions.None);
                                        //f["ShortTreeRef"] = (str1.Length == 1 ? str1[0] : str1[1]);
                                        break;
                                    default:
                                        break;
                                }
                                MapInfo.Data.Key k = t.InsertFeature(f);
                            }
                        }
                        if (boolShowProgress)
                        {
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }
                        if (intProcessedCount >= selection1.SelectedCount) break;
                    }
                }
            }

            // ----- Now Load Any Assets Selected for Loading ----- //
            viewMapObject = (GridView)gridControlAssetObjects.MainView;
            if (selectionAssets.SelectedCount > 0)
            {
                style = Get_Default_Style();
                for (int i = 0; i < viewMapObject.DataRowCount; i++)
                {
                    if (Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        intProcessedCount++;

                        //if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)  // Get Thematic Style
                        //{
                        //    intPicklistValue = Convert.ToInt32(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn));
                        //    style = Get_Thematic_Style(intPicklistValue, Convert.ToDecimal(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn)));  // Pass value as both int and decimal and let function sort it out //
                        //}
                        // ***** STYLE SET ON LINE BEFORE FOR LOOP ***** //
                        if (viewMapObject.GetRowCellValue(i, "ObjectType").ToString() == "Point")  // Load Point //
                        {
                            //intCrownDiameter = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CrownDiameter"));
                            MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                            f.Geometry = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(viewMapObject.GetRowCellValue(i, "XCoordinate")), Convert.ToDouble(viewMapObject.GetRowCellValue(i, "YCoordinate")));
                            f["CalculatedSize"] = "";
                            f["CalculatedPerimeter"] = "";
                            f["intObjectType"] = 0;  //0 = Point //
                            f["id"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                            f["AssetID"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "AssetID"));
                            f["AssetNumber"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetNumber"));
                            f["PartNumber"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "PartNumber"));
                            f["ModelNumber"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ModelNumber"));
                            f["SerialNumber"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SerialNumber"));
                            f["ClientName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ClientName"));
                            f["ClientCode"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ClientCode"));
                            f["SiteName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SiteName"));
                            f["SiteCode"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SiteCode"));
                            f["AssetType"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetTypeDescription"));
                            f["AssetSubType"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetSubTypeDescription"));
                            f["intHighlighted"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "Highlight"));
                            f["ModuleID"] = 2;  // 1 = Amenity Trees, 2 = Assets //
                            f["CrownDiameter"] = 0;
                            f["Thematics"] = (Convert.ToInt32(f["intHighlighted"]) == 0 ? Convert.ToDouble(0.00) : Convert.ToDouble(999995));

                            // Following populated so object labelling will work //
                            f["TreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetNumber"));
                            f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetNumber"));
                            f["ClientName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ClientName"));
                            f["SiteName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SiteName"));
                            f["Species"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetTypeDescription"));

                            /*switch (intTreeRefStructureType)
                             {
                                 case 0:
                                     f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                                     break;
                                 case 1:
                                     strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                                     f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                     break;
                                 case 2:
                                     strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));  // Note that the second commented out method also works //
                                     f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                     //string[] str1 = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Split(new string[] { strTreeRefStructureStripPattern }, 2, StringSplitOptions.None);
                                     //f["ShortTreeRef"] = (str1.Length == 1 ? str1[0] : str1[1]);
                                     break;
                                 default:
                                     break;
                             }
                             if (intMapObjectSizing == -1)  // Dynamic Sizing according to Crown Diameter //
                             {
                                 style.SymbolStyle.PointSize = Calculate_Point_Size(f);
                             }*/
                            if (intMapObjectSizing == -1)  // Dynamic Sizing according to Crown Diameter //
                            {
                                style.SymbolStyle.PointSize = Calculate_Point_Size(f);
                            }
                            f.Style = style;
                            MapInfo.Data.Key k = t.InsertFeature(f);
                        }
                        else  // Polygon / Polyline //
                        {
                            strID = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                            strPolygonXY = Convert.ToString(viewMapObject.GetRowCellValue(i, "PolygonXY"));

                            // Check there is an even number of points //
                            strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray.Length % 2 != 0)  // Modulus (%) //
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Invalid Polygon\\Polyline in database - MapID: " + strID + ".\r\nInvalid Number of Coordinates - Object ignored!", "Load Map Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            else
                            {
                                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                                iPoint = 0;
                                for (int j = 0; j < strArray.Length - 1; j++)
                                {
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                                    iPoint++;
                                }
                                MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                                    f["intObjectType"] = 1;  // 1 = Polygon //
                                    viewMapObject.SetRowCellValue(i, "ObjectType", "Polygon");
                                }
                                else
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["intObjectType"] = 2;  // 2 = PolyLine //
                                    viewMapObject.SetRowCellValue(i, "ObjectType", "Polyline");
                                }
                                f.Style = style;
                                f["id"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                                f["AssetID"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "AssetID"));
                                f["AssetNumber"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetNumber"));
                                f["PartNumber"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "PartNumber"));
                                f["ModelNumber"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ModelNumber"));
                                f["SerialNumber"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SerialNumber"));
                                f["ClientName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ClientName"));
                                f["ClientCode"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ClientCode"));
                                f["SiteName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SiteName"));
                                f["SiteCode"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SiteCode"));
                                f["AssetType"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetTypeDescription"));
                                f["AssetSubType"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetSubTypeDescription"));
                                f["intHighlighted"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "Highlight"));
                                f["ModuleID"] = 2;  // 1 = Amenity Trees, 2 = Assets //
                                f["CrownDiameter"] = 0;
                                f["Thematics"] = (Convert.ToInt32(f["intHighlighted"]) == 0 ? Convert.ToDouble(0.00) : Convert.ToDouble(999995));

                                // Following populated so object labelling will work //
                                f["TreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetNumber"));
                                f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetNumber"));
                                f["ClientName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "ClientName"));
                                f["SiteName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SiteName"));
                                f["Species"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "AssetTypeDescription"));

                                /*switch (intTreeRefStructureType)
                                 {
                                     case 0:
                                         f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                                         break;
                                     case 1:
                                         strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));
                                         f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                         break;
                                     case 2:
                                         strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference"));  // Note that the second commented out method also works //
                                         f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                         //string[] str1 = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Split(new string[] { strTreeRefStructureStripPattern }, 2, StringSplitOptions.None);
                                         //f["ShortTreeRef"] = (str1.Length == 1 ? str1[0] : str1[1]);
                                         break;
                                     default:
                                         break;
                                 }*/
                                MapInfo.Data.Key k = t.InsertFeature(f);
                            }
                        }
                        if (boolShowProgress)
                        {
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }
                        if (intProcessedCount >= selection1.SelectedCount + selectionAssets.SelectedCount) break;
                    }
                }
            }
            // ----- End of Load Any Assets Selected for Loading ----- //

            t.EndAccess();
            conn.Close();
            conn.Dispose();

            if (intMapObjectTransparency > 0) Create_MapInfo_Theme();  // Transparency on, so we need to create a Map Info Theme to handle thematics //

            mapControl1.Invalidate();
            mapControl1.Refresh();
            mapControl1.ResumeLayout(true);  // Switch back on Map Redrawing //

            if (boolShowProgress)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
            }
        }

        private void ceCentreMap_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) csScaleMapForHighlighted.Checked = false;
        }

        private void csScaleMapForHighlighted_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) ceCentreMap.Checked = false;
        }

        private void btnSelectHighlighted_Click(object sender, EventArgs e)
        {
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            selection.Clear();

            string strMapID = "";
            int intSelectedCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    intSelectedCount++;
                    strMapID += "id='" + Convert.ToString(view.GetRowCellValue(i, "MapID")) + "' or ";
                }
            }
            // Now Process Assets Grid //
            view = (GridView)gridControlAssetObjects.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    intSelectedCount++;
                    strMapID += "id='" + Convert.ToString(view.GetRowCellValue(i, "MapID")) + "' or ";
                }
            }
           
            if (intSelectedCount == 0)
            {
                XtraMessageBox.Show("Select one or more map objects to highlight before proceeding.", "Select Highlighted Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strMapID = strMapID.Remove(strMapID.Length - 4);  // Remove last " or " statement //

            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strMapID);
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            selection.Add(irfc);  // Add the whole feature set to the default selection //
            connection.Close();
            connection.Dispose();
        }

        private void Highlight_Map_Objects_From_Grid_Selection()
        {
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            selection.Clear();

            string strMapID = "";
            GridView view = (GridView)gridControl2.MainView;
            // Now Process Assets Grid //
            GridView view2 = (GridView)gridControlAssetObjects.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int[] intRowHandles2 = view2.GetSelectedRows();
            if (intRowHandles.Length <= 0 && intRowHandles2.Length <= 0) return;

            mapControl1.SuspendLayout();  // Switch of Map Redraw until done //
            foreach (int intRowHandle in intRowHandles)
            {
                strMapID += "id='" + Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID")) + "' or ";
            }
            foreach (int intRowHandle in intRowHandles2)
            {
                strMapID += "id='" + Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID")) + "' or ";
            }

            strMapID = strMapID.Remove(strMapID.Length - 4);  // Remove last " or " statement //

            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strMapID);
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            selection.Add(irfc);  // Add the whole feature set to the default selection //
            connection.Close();
            connection.Dispose();
            
            mapControl1.Invalidate();
            mapControl1.Refresh();
            mapControl1.ResumeLayout(true);  // Switch back on Map Redrawing //
        }

        private void buttonEditClientFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids = "";
                i_str_selected_client_names = "";
                buttonEditClientFilter.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    if (!string.IsNullOrWhiteSpace(fChildForm.strSelectedChildIDs))
                    {
                        i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                        i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                        buttonEditSiteFilter.Text = i_str_selected_site_names;

                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    else
                    {
                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
                    }
                }
            }
        }

        private void buttonEditSiteFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_site_ids = "";
                i_str_selected_site_names = "";
                buttonEditSiteFilter.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 1;
                fChildForm.intMustSelectChildren = 1;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.strPassedInChildIDs = i_str_selected_site_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                    i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                    buttonEditSiteFilter.Text = i_str_selected_site_names;

                    // Update Filter control's tooltip //
                    strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                    superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                }
            }
        }

        #endregion


        #region Grid [Asset Map Objects]

        private void gridControlAssetObjects_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.Tag.ToString())
            {
                case "refresh":
                    WaitDialogForm loading = new WaitDialogForm("Loading Available Map Objects...", "WoodPlan Mapping");
                    loading.Show();

                    if (i_str_selected_site_ids2 == null) i_str_selected_site_ids2 = "";
                    if (strAssetTypeIDs == null) strAssetTypeIDs = "";
                    Load_Map_Objects_Grid_Assets();

                    loading.Close();
                    break;
            }
        }
        
        private void gridViewAssetObjects_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No objects available");
        }

        private void gridViewAssetObjects_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 12;
            SetMenuStatus();
        }

        private void gridViewAssetObjects_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                //{
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                if (intCount > 0)
                {
                    bbiHighlightYes.Enabled = true;
                    bbiHighlightNo.Enabled = true;
                    bbiVisibleYes.Enabled = true;
                    bbiVisibleNo.Enabled = true;
                }
                else
                {
                    bbiHighlightYes.Enabled = false;
                    bbiHighlightNo.Enabled = false;
                    bbiVisibleYes.Enabled = false;
                    bbiVisibleNo.Enabled = false;
                }
                bbiViewOnMap.Enabled = (intCount == 1 ? true : false);
                bbiCopyHighlightFromVisible.Enabled = true;

                bool boolRowsSelected = false;
                for (int i = 0; i < view.RowCount; i++)  // Use RowCount and not DataRowCount as it's visible rows we are concerned with //
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                    {
                        boolRowsSelected = true;
                        break;
                    }
                }
                if (boolRowsSelected)
                {
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bbiDatasetCreate.Enabled = false;
                }
                //if (view.RowCount > 0)  // Use RowCount and not DataRowCount as it's visible rows we are concerned with //
                //{
                //    bbiDatasetSelection.Enabled = true;
                //    bsiDataset.Enabled = true;
                //}
                //else
                //{
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                //}
                //bsiDataset.Enabled = true;
                //bbiDatasetManager.Enabled = true;

                popupMenu1.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemCheckEditHighlight2_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            GridView view = (GridView)gridControl2.MainView;
            if (ce.Checked) view.SetRowCellValue(view.FocusedRowHandle, "CheckMarkSelection", 1);
            if (ce.Checked) view.SetRowCellValue(view.FocusedRowHandle, "Highlight", 1);
        }

        private void buttonEditClientFilter2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids2 = "";
                i_str_selected_client_names2 = "";
                buttonEditClientFilter.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsClientContractFilter2.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids2;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids2 = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names2 = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter2.Text = i_str_selected_client_names2;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names2.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter2.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    if (!string.IsNullOrWhiteSpace(fChildForm.strSelectedChildIDs))
                    {
                        i_str_selected_site_ids2 = fChildForm.strSelectedChildIDs;
                        i_str_selected_site_names2 = fChildForm.strSelectedChildDescriptions;
                        buttonEditSiteFilter2.Text = i_str_selected_site_names2;

                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names2.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter2.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    else
                    {
                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names2.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter2.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
                    }
                }
            }
        }

        private void buttonEditSiteFilter2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_site_ids2 = "";
                i_str_selected_site_names2 = "";
                buttonEditSiteFilter2.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsSiteContractFilter2.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 1;
                fChildForm.intMustSelectChildren = 1;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids2;
                fChildForm.strPassedInChildIDs = i_str_selected_site_ids2;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids2 = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names2 = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter2.Text = i_str_selected_client_names2;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names2.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter2.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    i_str_selected_site_ids2 = fChildForm.strSelectedChildIDs;
                    i_str_selected_site_names2 = fChildForm.strSelectedChildDescriptions;
                    buttonEditSiteFilter2.Text = i_str_selected_site_names2;

                    // Update Filter control's tooltip //
                    strTooltipText = i_str_selected_site_names2.Replace(", ", "\n");
                    superToolTipSetupArgsSiteContractFilter2.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                }
            }
        }

        #endregion


        private void bbiViewOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControlAssetObjects.MainView;
                    break;
                default:
                    return;
            }
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Map Object to be viewed by clicking on the row in the grid then try again.", "View Map Object", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetFocusedRowCellValue("CheckMarkSelection")) != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Ensure the Map Object to be viewed is loaded into the map first then try again.\n\nTip: To load the Map Object into the map, tick it's Visibile column in the grid then click the Refresh Map Objects button.", "View Map Object", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strMapID = view.GetFocusedRowCellValue("MapID").ToString();
            if (string.IsNullOrEmpty(strMapID))
            {
                XtraMessageBox.Show("The Map Object to be viewed does not have a Map ID - unable to perform search.\n\nYou should edit this record and assign it a Map ID before trying again.", "View Map Object", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;

            double MinX = (double)9999999.00;  // Used for Zooming to Scale //
            double MinY = (double)9999999.00;
            double MaxX = (double)-9999999.00;
            double MaxY = (double)-9999999.00;

            FindAndHighlight(strMapID, true, true, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
        }

        private void bbiHighlightYes_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControlAssetObjects.MainView;
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "Highlight", 1);
                }
                view.EndUpdate();
            }
        }

        private void bbiHighlightNo_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControlAssetObjects.MainView;
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "Highlight", 0);
                }
                view.EndUpdate();
            }
        }

        private void bbiCopyHighlightFromVisible_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControlAssetObjects.MainView;
                    break;
                default:
                    return;
            }
            view.BeginUpdate();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                view.SetRowCellValue(i, "Highlight", Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")));
            }
            view.EndUpdate();
        }

        private void bbiVisibleYes_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControlAssetObjects.MainView;
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                }
                view.EndUpdate();
            }
        }

        private void bbiVisibleNo_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControlAssetObjects.MainView;
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 0);
                }
                view.EndUpdate();
            }
        }


        #region PopupContainer Asset Type Filter

        private void btnOkAssetTypes_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditAssetTypes_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditAssetTypes_Get_Selected();
        }

        private string PopupContainerEditAssetTypes_Get_Selected()
        {
            strAssetTypeIDs = "";    // Reset any prior values first //
            string i_str_selected_asset_type_descriptions = "";
            GridView view = (GridView)gridControlAssetTypes.MainView;
            if (view.DataRowCount <= 0)
            {
                strAssetTypeIDs = "";
                return "No Asset Type Filter";

            }
            else if (selectionAssetTypes.SelectedCount <= 0)
            {
                strAssetTypeIDs = "";
                return "No Asset Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strAssetTypeIDs += Convert.ToString(view.GetRowCellValue(i, "AssetTypeID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_asset_type_descriptions = Convert.ToString(view.GetRowCellValue(i, "AssetTypeDescription"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_asset_type_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "AssetTypeDescription"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_asset_type_descriptions;
        }

        private void PopupContainerEditAssetTypes_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString())
            {
                case "RefreshList":
                    WaitDialogForm loading = new WaitDialogForm("Loading Available Map Objects...", "WoodPlan Mapping");
                    loading.Show();

                    if (i_str_selected_site_ids2 == null) i_str_selected_site_ids2 = "";
                    if (strAssetTypeIDs == null) strAssetTypeIDs = "";
                    Load_Map_Objects_Grid_Assets();

                    loading.Close();
                    break;
            }

        }

        private void gridViewAssetTypes_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 10;
            SetMenuStatus();
        }

        #endregion

        
        #region PopupContainer2 Scale

        private void popupContainerEdit2_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            if (i_bool_FullExtentFired) // Don't fire since Full Extents button clicked //
            {
                i_bool_FullExtentFired = false;
            }
            else
            {
                e.Value = PopupContainerEdit2_Get_Selected();
            }
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            string strCurrentScale = "";
            if (Convert.ToInt32(seUserDefinedScale.Value) > 0)
            {
                strCurrentScale = seUserDefinedScale.Value.ToString();
            }
            else
            {
                GridView view = (GridView)gridControl4.MainView;
                if (view.DataRowCount <= 0)
                {
                    XtraMessageBox.Show("Select or enter a scale before proceeding.", "Select Scale", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return strCurrentScale;
                }
                else
                {
                    strCurrentScale = view.GetFocusedRowCellValue("Scale").ToString();
                }
            }
            // Zoom map to scale //
            if (Convert.ToDouble(strCurrentScale) > 0.00) mapControl1.Map.SetView(mapControl1.Map.Center, mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(strCurrentScale));
            return "Scale 1:" + strCurrentScale;
        }

        private void btnSetScale_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            if ((button.Parent as PopupContainerControl).OwnerEdit != null) (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void btnViewFullExtent_Click(object sender, EventArgs e)
        {
            //FeatureLayer lyr = mapControl1.Map.Layers["MapObjects"] as FeatureLayer;
            //if (lyr != null) mapControl1.Map.SetView(lyr);

            MapInfo.Mapping.IMapLayerFilter iml = MapInfo.Mapping.MapLayerFilterFactory.FilterByType(typeof(MapInfo.Mapping.FeatureLayer));
            MapInfo.Mapping.MapLayerEnumerator mle = this.mapControl1.Map.Layers.GetMapLayerEnumerator(iml);
            try
            {
                this.mapControl1.Map.SetView(mle);
            }
            catch (Exception)
            {
            }

            i_bool_FullExtentFired = true;
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                {
                    seUserDefinedScale.Value = 0;  // Clear User Defined Value since grid selection used //
                }
            }
        }

        #endregion


        #region GPS Panel

        string strGpsPort = "";
        int intGpsBaudRate = 0;
        GPS.NMEA.NMEA2OSG OSGconv = new GPS.NMEA.NMEA2OSG();  // OSGridConverter //
        MapInfo.Data.Key GpsPointKey = null;
        MapInfo.Data.Key GpsPolygonPlotterKey = null;

        private void LoadGPSComPortList()
        {
            cmbBaudRate.SelectedIndex = 4;  // List Hardcoded, item 4 (0 based) = 4800 //
            cmbPortName.Properties.Items.Clear();
            cmbPortName.Properties.Items.Add("Auto Detect");  // Add dummy first item //
            foreach (string s in System.IO.Ports.SerialPort.GetPortNames())
            {
                cmbPortName.Properties.Items.Add(s);
            }
            if (cmbPortName.Properties.Items.Count > 0)
            {
                cmbPortName.SelectedIndex = 0;
            }
            else
            {
                XtraMessageBox.Show("There are no COM Ports detected on this computer - GPS Functionality Diabled.\nPlease install a COM Port and reopen this screen if you need to use GPS.", "WoodPlan Mapping - GPS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                btnGPS_Start.Enabled = false;
            }

        }

        private int GetGpsPortSettings(string strPort, int intBaudRate)
        {
            // *** Note this may fail if another device is also connected to a another comp port in addition to the GPS device being connected to a com port! *** //
            WaitDialogForm loading = new WaitDialogForm("Searching for GPS Device...", "WoodPlan Mapping");
            loading.Show();
            string[] strPorts = System.IO.Ports.SerialPort.GetPortNames();
            if (strPort == "Auto Detect")
            {
                foreach (string port in strPorts)
                {
                    try
                    {
                        System.IO.Ports.SerialPort com = new System.IO.Ports.SerialPort(port, intBaudRate);
                        com.Open();
                        System.Threading.Thread.Sleep(500);  // Wait half a second //
                        int nBytes = com.BytesToRead;
                        if (nBytes == 0)  // Try again //
                        {
                            System.Threading.Thread.Sleep(500);  // Wait half a second //
                            nBytes = com.BytesToRead;
                            if (nBytes == 0)  // Try again //
                            {
                                System.Threading.Thread.Sleep(500);  // Wait half a second //
                                nBytes = com.BytesToRead;
                            }
                        }
                        byte[] BufBytes;
                        BufBytes = new byte[nBytes];
                        com.Read(BufBytes, 0, nBytes);
                        string strData = System.Text.Encoding.GetEncoding("ASCII").GetString(BufBytes, 0, nBytes);
                        com.Close();
                        if (strData != "")
                        {
                            strGpsPort = port;
                            intGpsBaudRate = intBaudRate;
                            loading.Close();
                            return 1;
                        }
                    }
                    catch (System.IO.IOException)
                    {
                        //Some devices (like iPAQ H4100 among others) throws an IOException for no reason
                        //Let's just ignore it and run along
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
            else
            {
                try
                {
                    System.IO.Ports.SerialPort com = new System.IO.Ports.SerialPort(strPort, intBaudRate);
                    com.Open();
                    System.Threading.Thread.Sleep(500);  // Wait half a second //
                    int nBytes = com.BytesToRead;
                    if (nBytes == 0)  // Try again //
                    {
                        System.Threading.Thread.Sleep(500);  // Wait half a second //
                        nBytes = com.BytesToRead;
                        if (nBytes == 0)  // Try again //
                        {
                            System.Threading.Thread.Sleep(500);  // Wait half a second //
                            nBytes = com.BytesToRead;
                        }
                    }
                    byte[] BufBytes;
                    BufBytes = new byte[nBytes];
                    com.Read(BufBytes, 0, nBytes);
                    string strData = System.Text.Encoding.GetEncoding("ASCII").GetString(BufBytes, 0, nBytes);
                    com.Close();
                    if (strData != "")
                    {
                        strGpsPort = strPort;
                        //intGpsBaudRate = i;
                        intGpsBaudRate = intBaudRate;
                        loading.Close();
                        return 1;
                    }
                    else
                    {
                        loading.Close();
                        return 0;
                    }
                }
                catch (System.IO.IOException)
                {
                    //Some devices (like iPAQ H4100 among others) throws an IOException for no reason
                    //Let's just ignore it and run along
                }
                catch (System.Exception)
                {
                }
            }
            loading.Close();
            return 0;
        }

        public void btnGPS_Start_Click(object sender, EventArgs e)  // Public so this can be triggered from outwith the form if required //
        {
            ResetGpsPlottingControls();
            if (btnGPS_Start.Text == "Start GPS")
            {
                statusBar1.Text = "";  // Reset the Status text for GPS //
                if (!GPS.IsPortOpen)
                {
                    string strTempPort = cmbPortName.EditValue.ToString();
                    int intTempBaud = Convert.ToInt32(cmbBaudRate.EditValue);
                    int intReturn = 1;
                    if (strGpsPort == "" || intGpsBaudRate <= 0 || (strGpsPort != "" && strGpsPort != strTempPort) || (intGpsBaudRate > 0 && intGpsBaudRate != intTempBaud))
                    {
                        intReturn = GetGpsPortSettings(strTempPort, intTempBaud);
                    }
                    if (strGpsPort == "" || intGpsBaudRate == 0 || intReturn == 0)
                    {
                        XtraMessageBox.Show("Unable to start GPS... The Port or Baud Rate specified may be incorrect.\nTry again by clicking the Start GPS button. If the problem persists, try adjusting the port and/or baud rate.", "Start GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        try
                        {
                            GPS.Start(strGpsPort, intGpsBaudRate); // Open serial port //
                            btnGPS_Start.Text = "Stop GPS";
                            groupControl2.Enabled = true;

                            gridControl5.Enabled = false;
                            beWorkspace.Properties.Buttons[0].Enabled = false;  // Disable Select Workspace button //
                            beWorkspace.Properties.Buttons[1].Enabled = false;  // Disable Save Workspace button //
                            beWorkspace.Properties.Buttons[2].Enabled = false;  // Disable Save As Workspace button //
                            gridControl2.Enabled = false;
                            btnRefreshMapObjects.Enabled = false;
                            gridControl3.Enabled = false;

                            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                            {
                                if (l.Name == "MapObjects")
                                {
                                    l.VolatilityHint = LayerVolatilityHint.Animate;
                                }
                            }

                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("An error occured when trying to open port: " + ex.Message, "Start GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
            }
            else
            {
                GPS.Stop(); // Close serial port //
                btnGPS_Start.Text = "Start GPS";
                groupControl2.Enabled = false;
                foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                {
                    if (l.Name == "MapObjects")
                    {
                        l.VolatilityHint = LayerVolatilityHint.Normal;
                    }
                }

                // Clear GPS CrossHair Object if it exists //
                if (GpsPointKey != null)
                {
                    MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                    conn.Open();
                    MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        try
                        {
                            MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                            t.DeleteFeature(GpsPointKey);  // delete feature //
                            t.Pack(MapInfo.Data.PackType.All);  // pack the table //
                            GpsPointKey = null;
                        }
                        catch (Exception)
                        {
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
                // Clear GPS Polygon Plotter Object if it exists //
                if (GpsPolygonPlotterKey != null)
                {
                    MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                    conn.Open();
                    MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        try
                        {
                            MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                            if (ftr != null)
                            {
                                t.DeleteFeature(GpsPolygonPlotterKey);  // delete feature //
                                t.Pack(MapInfo.Data.PackType.All);  // pack the table //
                            }
                            GpsPolygonPlotterKey = null;
                        }
                        catch (Exception)
                        {
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
                gridControl5.Enabled = true;
                beWorkspace.Properties.Buttons[0].Enabled = true;  // Enable Select Workspace button //
                beWorkspace.Properties.Buttons[1].Enabled = true;  // Enable Save Workspace button //
                beWorkspace.Properties.Buttons[2].Enabled = true;  // Enable Save As Workspace button //
                gridControl2.Enabled = true;
                btnRefreshMapObjects.Enabled = true;
                gridControl3.Enabled = true;
            }
        }

        private void ceGPS_PlotPoint_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            if (ce.Checked) UpdateGPS_Main_Controls("PlotPoint");
        }

        private void ceGPS_PlotPolygon_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            if (ce.Checked) UpdateGPS_Main_Controls("PlotPolygon");
        }

        private void ceGPS_PlotLine_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            if (ce.Checked) UpdateGPS_Main_Controls("PlotLine");
        }

        private void ceGps_PlotWithTimer_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            seGps_PlotTimerInterval.Enabled = ce.Checked;
        }

        private void btnGps_PauseTimer_Click(object sender, EventArgs e)
        {
            if (btnGps_PauseTimer.Text == "Pause Timer")
            {
                // Stop the Timer //
                GPSTimer.Stop();
                btnGps_PauseTimer.Text = "Restart Timer";
                labelPlottingUsingTimer.Text = "Plotting Timer <color=255,0,0>Paused</color>";
            }
            else
            {
                // Restart the timer //
                GPSTimer.Start();
                btnGps_PauseTimer.Text = "Pause Timer";
                labelPlottingUsingTimer.Text = "Plotting using Timer";
            }
        }

        private void seGps_PlotTimerInterval_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.SpinEdit se = (DevExpress.XtraEditors.SpinEdit)sender;
            GPSTimer.Interval = Convert.ToInt32(se.EditValue) * 1000;
        }

        private void GPSTimer_Tick(object sender, EventArgs e)
        {
            if (Update_GPS_Plotter_Polygon("InsertVertice") != 1) DevExpress.XtraEditors.XtraMessageBox.Show("Plot Map Object from GPS on Timer Failed... Ensure the GPS is running [the GPS Crosshairs should be visible on the map] then try again.", "Plot Map Object from GPS using Timer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void UpdateGPS_Main_Controls(string strStatus)
        {
            if (strStatus == "PlotPoint")
            {
                btnGPS_PlotStart.Text = "Start";
                btnGPS_PlotStart.Enabled = false;
                btnGPS_Plot.Text = "Plot";
                btnGPS_Plot.Enabled = true;
                btnGPS_PlotStop.Enabled = false;
                btnGPS_PlotStop.Text = "Stop";
                ceGps_PlotWithTimer.Enabled = false;
                seGps_PlotTimerInterval.Enabled = false;
            }
            else if (strStatus == "PlotPolygon")
            {
                btnGPS_PlotStart.Text = "Start Polygon";
                btnGPS_PlotStart.Enabled = true;
                btnGPS_Plot.Text = "Insert Vertice";
                btnGPS_Plot.Enabled = false;
                btnGPS_PlotStop.Text = "End Polygon";
                btnGPS_PlotStop.Enabled = false;
                ceGps_PlotWithTimer.Enabled = true;
                seGps_PlotTimerInterval.Enabled = true;
            }
            else if (strStatus == "PlotLine")
            {
                btnGPS_PlotStart.Text = "Start Line";
                btnGPS_PlotStart.Enabled = true;
                btnGPS_Plot.Text = "Insert Vertice";
                btnGPS_Plot.Enabled = false;
                btnGPS_PlotStop.Text = "End Line";
                btnGPS_PlotStop.Enabled = false;
                ceGps_PlotWithTimer.Enabled = true;
                seGps_PlotTimerInterval.Enabled = true;
            }
            btnGps_PauseTimer.Text = "Pause Timer";
            btnGps_PauseTimer.Enabled = false;
            GPSTimer.Stop();
            labelPlottingUsingTimer.Visible = false;
            labelPlottingUsingTimer.Text = "Plotting using Timer";
        }

        private void PlotGpsPositionOnMap(Coordinate coordinate, double ellipHeight)
        {
            double doubX = 0.00;
            double doubY = 0.00;
            if (OSGconv.Transform(coordinate.Latitude, coordinate.Longitude, ellipHeight))
            {
                //display decimal values of lat and lon
                doubX = Convert.ToDouble(OSGconv.east);
                doubY = Convert.ToDouble(OSGconv.north);
                lbRMCGridRef.Text = OSGconv.ngr;  // Show Grid Reference //
            }
            else
            {
                return;  // Unable to get OS Grid From Passed Lat/Long so Abort Map Update //
            }

            SimpleVectorPointStyle _vectorSymbol = new SimpleVectorPointStyle();
            _vectorSymbol.Code = (short)49;  // 49 = Cross //
            _vectorSymbol.Color = Color.FromArgb(i_int_GPS_CrossHair_Colour);
            _vectorSymbol.PointSize = (short)30;
            CompositeStyle cs = new CompositeStyle();
            cs.SymbolStyle = _vectorSymbol;

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");

            MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), doubX, doubY);
            bool boolUpdated = false;
            if (GpsPointKey != null)
            {
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                if (si != null)  // Map Object Found //
                {
                    MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                    ftr.Geometry = g;
                    ftr.Update();
                    boolUpdated = true;
                }
            }
            if (!boolUpdated)  // GPS Object not already created or unable to find it so re-create //
            {
                MapInfo.Data.Feature ftr = new MapInfo.Data.Feature(t.TableInfo.Columns);
                ftr["ID"] = "*GPS Position*";
                ftr.Geometry = g;
                ftr.Style = cs;
                GpsPointKey = t.InsertFeature(ftr);


                // *** Prevent Labelling of the GPS Point using a SelectionLabelModifier *** //
                // Remove any previous SelectionLabelModifier //
                try
                {
                    ArrayList lblmods;
                    // LOOP OVER EACH LABEL LAYER //
                    foreach (MapInfo.Mapping.LabelLayer lbllyr in mapControl1.Map.Layers.GetMapLayerEnumerator(new MapInfo.Mapping.FilterByLayerType(MapInfo.Mapping.LayerType.Label)))
                    {
                        // LOOP OVER EACH LABELS SOURCE //
                        foreach (MapInfo.Mapping.LabelSource lblsrc in lbllyr.Sources)
                        {
                            lblmods = new ArrayList();
                            //------------------------------------------------------------------------
                            // CHECK THE MODIFIERS TO SEE IF ANY ARE A SelectionLabelModifier.
                            // IF SO ADD IT TO AN ARRAYLIST.
                            // CANNOT DELETE IT RIGHT A WAY -- ERRORS OUT
                            //------------------------------------------------------------------------
                            foreach (MapInfo.Mapping.LabelModifier lsm in lblsrc.Modifiers)
                            {
                                if (lsm is MapInfo.Mapping.SelectionLabelModifier)
                                {
                                    lblmods.Add(lsm);
                                }
                            }
                            //-----------------------------------------------------------
                            // DELETE ANY SelectionLabelModifier FOUND FOR THIS SOURCE
                            //-----------------------------------------------------------
                            foreach (MapInfo.Mapping.LabelModifier lsm in lblmods)
                            {
                                lblsrc.Modifiers.Remove(lsm);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Error Clearing Custom Labels.\n" + ex.Message, "Error", MessageBoxButtons.OK);
                }

                // Add a SelectionLabelModifier for the GPS Point Feature to customise it's Label //
                foreach (MapInfo.Mapping.LabelLayer lbllyr in mapControl1.Map.Layers.GetMapLayerEnumerator(new MapInfo.Mapping.FilterByLayerType(MapInfo.Mapping.LayerType.Label)))
                {
                    // LOOP OVER EACH LABELS SOURCE //
                    foreach (MapInfo.Mapping.LabelSource lblsrc in lbllyr.Sources)
                    {
                        MapInfo.Mapping.LabelSource lblSource = new MapInfo.Mapping.LabelSource(t, "LabelSource");
                        MapInfo.Mapping.LabelProperties lp = new MapInfo.Mapping.LabelProperties();

                        LabelProperties properties = new LabelProperties();
                        properties.Attributes = LabelAttribute.VisibilityEnabled | LabelAttribute.Caption | LabelAttribute.CalloutLineUse;
                        properties.Visibility.Enabled = false;
                        properties.Caption = "''";
                        properties.CalloutLine.Use = false;

                        // Add the properties to a new selection label modifier and add the modifier to the label source //
                        SelectionLabelModifier modifier = new SelectionLabelModifier();
                        modifier.Properties.Add(GpsPointKey, properties);
                        lblsrc.Modifiers.Append(modifier);

                    }
                }
                // *** End of Prevent Labelling of the GPS Point using a SelectionLabelModifier *** //

            }

            if (GpsPolygonPlotterKey != null) Update_GPS_Plotter_Polygon("UpdateLastVerticePosition");  // Update the GPS Ploygon Plotter //

            int intBuffer = (50 * Convert.ToInt32(mapControl1.Map.Scale)) / 2500;
            if (g.Bounds.x1 - intBuffer <= mapControl1.Map.Bounds.x1 || g.Bounds.x2 + intBuffer >= mapControl1.Map.Bounds.x2 || g.Bounds.y1 - intBuffer <= mapControl1.Map.Bounds.y1 || g.Bounds.y2 + intBuffer >= mapControl1.Map.Bounds.y2)
            {
                this.mapControl1.Map.SetView(g);
            }
            conn.Close();
            conn.Dispose();
        }

        private void btnGPS_PlotStart_Click(object sender, EventArgs e)
        {
            if (btnGPS_PlotStart.Text == "Start Polygon" || btnGPS_PlotStart.Text == "Start Line")
            {
                int intSuccess = 0;
                if (GpsPointKey != null)
                {
                    intSuccess = Update_GPS_Plotter_Polygon("Start");
                    btnGPS_PlotStart.Text = "Cancel";
                    btnGPS_Plot.Enabled = true;  // Insert Vertice Button //
                    btnGPS_PlotStop.Enabled = true;
                    ceGps_PlotWithTimer.Enabled = false;
                    seGps_PlotTimerInterval.Enabled = false;
                    if (ceGps_PlotWithTimer.Checked)
                    {
                        btnGps_PauseTimer.Enabled = true;
                        GPSTimer.Start();
                        labelPlottingUsingTimer.Visible = true;
                    }
                }
                if (intSuccess != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Plot Map Object from GPS Failed... Ensure the GPS is running [the GPS Crosshairs should be visible on the map] then try again.", "Plot Map Object from GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                // abort GPS generated Polygon or Line //
                ResetGpsPlottingControls();
            }
        }

        private void btnGPS_PlotStop_Click(object sender, EventArgs e)
        {
            int intSuccess = 0;
            if (btnGPS_PlotStop.Text == "End Line")
            {
                intSuccess = Update_GPS_Plotter_Polygon("CreateLineFeature");
            }
            else if (btnGPS_PlotStop.Text == "End Polygon")
            {
                intSuccess = Update_GPS_Plotter_Polygon("CreatePolygonFeature");
            }
            if (intSuccess != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Plot Map Object from GPS Failed... Ensure the GPS is running [the GPS Crosshairs should be visible on the map] then try again.", "Plot Map Object from GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        private ArrayList _GpsPolygonPlotterPoints;  // Internal list of coordinates to keep track of the points submitted by the user //
        private int Update_GPS_Plotter_Polygon(string strAction)
        {
            int intSuccess = 0;
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
            switch (strAction)
            {
                case "Start":
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        _GpsPolygonPlotterPoints = new ArrayList();
                        _GpsPolygonPlotterPoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y));
                        _GpsPolygonPlotterPoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y));
                        //_linePoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x + 500, ftr.Geometry.Centroid.y + 500));
                        //_linePoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x + 750, ftr.Geometry.Centroid.y + 750));

                        MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array // 

                        //Create the feature and add it to the layer
                        MapInfo.Data.Feature ftrLine = new MapInfo.Data.Feature(t.TableInfo.Columns);
                        ftrLine["ID"] = "*GPS Plotter*";
                        ftrLine.Geometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);

                        CompositeStyle style = new CompositeStyle();
                        style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(i_str_GPS_Polygon_Plot_Line_Width), MapInfo.Styles.LineWidthUnit.Pixel), i_int_GPS_Polygon_Plot_Line_Style, Color.FromArgb(i_int_GPS_Polygon_Line_Plot_Colour));
                        ftrLine.Style = style;
                        GpsPolygonPlotterKey = t.InsertFeature(ftrLine);
                        intSuccess = 1;
                    }
                    break;
                case "InsertVertice":
                    si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                        if (si != null)  // Map Object Found //
                        {
                            MapInfo.Data.Feature ftrLine = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Plotter line - don't need to bother sorting out geometry coordsys as it will be recontructed from _GpsPolygonPlotterPoints ArrayList //

                            _GpsPolygonPlotterPoints[_GpsPolygonPlotterPoints.Count - 1] = new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y); // Adjust end of last vertice to match GPS CrossHair position //
                            _GpsPolygonPlotterPoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y));  // Add new vertice //

                            MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array //

                            ftrLine.Geometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);
                            ftrLine.Update();
                            intSuccess = 1;
                        }
                    }
                    break;
                case "UpdateLastVerticePosition":
                    si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                        if (si != null)  // Map Object Found //
                        {
                            MapInfo.Data.Feature ftrLine = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Plotter line - don't need to bother sorting out geometry coordsys as it will be recontructed from _GpsPolygonPlotterPoints ArrayList //

                            _GpsPolygonPlotterPoints[_GpsPolygonPlotterPoints.Count - 1] = new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y);  // Adjust end of last vertice to match GPS CrossHair position //

                            MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array //

                            ftrLine.Geometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);
                            ftrLine.Update();
                            intSuccess = 1;
                        }
                    }
                    break;
                case "CreateLineFeature":
                    si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                        if (si != null)  // Map Object Found //
                        {
                            MapInfo.Data.Feature ftrLine = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Plotter line - don't need to bother sorting out geometry coordsys as it will be recontructed from _GpsPolygonPlotterPoints ArrayList //

                            _GpsPolygonPlotterPoints[_GpsPolygonPlotterPoints.Count - 1] = new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y); // Adjust end of last vertice to match GPS CrossHair position //

                            MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array //
                            ftrLine.Geometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);
                            ftrLine.Update();
                            AddObjectToMap(ftrLine.Geometry);  // Create new Line Map Object //
                            ResetGpsPlottingControls();  // Clear the TempMapPlotterPolygon and reset the controlling buttons //
                            intSuccess = 1;
                        }
                    }
                    break;
                case "CreatePolygonFeature":
                    si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                        if (si != null)  // Map Object Found //
                        {
                            MapInfo.Data.Feature ftrLine = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Plotter line - don't need to bother sorting out geometry coordsys as it will be recontructed from _GpsPolygonPlotterPoints ArrayList //

                            _GpsPolygonPlotterPoints[_GpsPolygonPlotterPoints.Count - 1] = new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y); // Adjust end of last vertice to match GPS CrossHair position //
                            _GpsPolygonPlotterPoints.Add(_GpsPolygonPlotterPoints[0]);  // Add new FINAL vertice with same coordinates as the first one to close polygon //

                            MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array //

                            ftrLine.Geometry = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);
                            ftrLine.Update();
                            AddObjectToMap(ftrLine.Geometry);  // Create new Polygon Map Object //
                            ResetGpsPlottingControls();  // Clear the TempMapPlotterPolygon and reset the controlling buttons //
                            intSuccess = 1;
                        }
                    }
                    break;

                default:
                    break;
            }
            conn.Close();
            conn.Dispose();
            return intSuccess;
        }

        private void ResetGpsPlottingControls()
        {
            // *** Send Message to Abort *** //
            if (ceGPS_PlotPoint.Checked)
            {
                UpdateGPS_Main_Controls("PlotPoint");
            }
            else if (ceGPS_PlotPolygon.Checked)
            {
                UpdateGPS_Main_Controls("PlotPolygon");
            }
            else
            {
                UpdateGPS_Main_Controls("PlotLine");
            }

            // Clear GPS Polygon Plotter Object if it exists //
            if (GpsPolygonPlotterKey != null)
            {
                MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                conn.Open();
                MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                if (si != null)  // Map Object Found //
                {
                    try
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                        if (ftr != null)
                        {
                            t.DeleteFeature(GpsPolygonPlotterKey);  // delete feature //
                            t.Pack(MapInfo.Data.PackType.All);  // pack the table //
                        }
                        GpsPolygonPlotterKey = null;
                    }
                    catch (Exception)
                    {
                    }
                }
                conn.Close();
                conn.Dispose();
            }

        }

        private void btnGPS_Plot_Click(object sender, EventArgs e)
        {
            int intSuccess = 0;
            if (ceGPS_PlotPoint.Checked)  // Plot Point according to GPS Signal //
            {
                if (GpsPointKey != null)
                {
                    MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                    conn.Open();
                    MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                        MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //
                        intSuccess = AddObjectToMap(newGeometry);  // Add object to map //
                    }
                    conn.Close();
                    conn.Dispose();
                }
            }
            else
            {
                intSuccess = Update_GPS_Plotter_Polygon("InsertVertice");
            }
            if (intSuccess != 1) DevExpress.XtraEditors.XtraMessageBox.Show("Plot Map Object from GPS Failed... Ensure the GPS is running [the GPS Crosshairs should be visible on the map] then try again.", "Plot Map Object from GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void tbRawLog_Properties_BeforeShowMenu(object sender, BeforeShowMenuEventArgs e)
        {
            bool boolSaveCreated = false;
            bool boolClearCreated = false;

            foreach (DevExpress.Utils.Menu.DXMenuItem item in e.Menu.Items)
            {
                if (item.Caption == "Save GPS Log...")
                {
                    boolSaveCreated = true;
                    item.Enabled = (string.IsNullOrEmpty(tbRawLog.Text) ? false : true);
                }
                else if (item.Caption == "Clear GPS Log")
                {
                    boolClearCreated = true;
                    item.Enabled = (string.IsNullOrEmpty(tbRawLog.Text) ? false : true);

                }
            }
            if (!boolSaveCreated)
            {
                DevExpress.Utils.Menu.DXMenuItem item = new DevExpress.Utils.Menu.DXMenuItem("bbiGPSSaveLog", new EventHandler(bbiGPSSaveLog_ItemClick));
                item.Caption = "Save GPS Log...";
                item.Image = imageListGPSLogMenu.Images[0];
                item.BeginGroup = true;
                item.Enabled = (string.IsNullOrEmpty(tbRawLog.Text) ? false : true);
                item.BeginGroup = true;
                e.Menu.Items.Add(item);
            }
            if (!boolClearCreated)
            {
                DevExpress.Utils.Menu.DXMenuItem item = new DevExpress.Utils.Menu.DXMenuItem("bbiGPSClearLog", new EventHandler(bbiGPSClearLog_ItemClick));
                item.Caption = "Clear GPS Log";
                item.Image = imageListGPSLogMenu.Images[1];
                item.Enabled = (string.IsNullOrEmpty(tbRawLog.Text) ? false : true);
                e.Menu.Items.Add(item);
            }
        }

        private void bbiGPSSaveLog_ItemClick(object sender, EventArgs e)
        {

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Text Files(*.TXT)|*.TXT";
            string strFileName = "";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    strFileName = new System.IO.FileInfo(dlg.FileName).FullName;
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
                    sw.WriteLine(tbRawLog.Text);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Save of GPS Log Failed... [" + ex.Message + "]!\n\nTry again - if the problem persists contact Technical Support.", "Save GPS Log", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
        }

        private void bbiGPSClearLog_ItemClick(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to clear the GPS Log?", "Clear GPS Log", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                tbRawLog.Text = "";
            }
        }

        private void ceLogRawGPSData_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            boolLogGPS = ce.Checked;
        }

        private void NMEAtabs_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (NMEAtabs.SelectedTabPage.Text == "GPGSV") DrawGSV();
        }

        /// <summary>
        /// Responds to sentence events from GPS receiver
        /// </summary>
        private void GPSEventHandler(object sender, GPSHandler.GPSEventArgs e)
        {
            if (boolLogGPS)
            {
                tbRawLog.Text += e.Sentence + "\r\n";
                if (tbRawLog.Text.Length > 20 * 1024 * 1024) //20Kb maximum - prevents crash
                {
                    tbRawLog.Text = tbRawLog.Text.Substring(10 * 1024 * 1024);
                }
                tbRawLog.ScrollToCaret(); //Scroll to bottom
            }
            switch (e.TypeOfEvent)
            {
                case GPSEventType.GPRMC:  //Recommended minimum specific GPS/Transit data
                    if (GPS.HasGPSFix) //Is a GPS fix available?
                    {
                        //lbRMCPosition.Text = GPS.GPRMC.Position.ToString("#.000000");
                        lbRMCPosition.Text = GPS.GPRMC.Position.ToString("DMS");
                        //double[] utmpos = TransformToUTM(GPS.GPRMC.Position);
                        //lbRMCPositionUTM.Text = utmpos[0].ToString("#.0N ") + utmpos[0].ToString("#.0E") + " (Zone: " + utmpos[2] + ")";
                        lbRMCCourse.Text = GPS.GPRMC.Course.ToString();
                        lbRMCSpeed.Text = GPS.GPRMC.Speed.ToString() + " mph";
                        lbRMCTimeOfFix.Text = GPS.GPRMC.TimeOfFix.ToString("F");
                        lbRMCMagneticVariation.Text = GPS.GPRMC.MagneticVariation.ToString();
                    }
                    else
                    {
                        statusBar1.Text = "No Fix";
                        lbRMCCourse.Text = "N/A";
                        lbRMCSpeed.Text = "N/A";
                        lbRMCTimeOfFix.Text = GPS.GPRMC.TimeOfFix.ToString();
                    }
                    break;
                case GPSEventType.GPGGA: //Global Positioning System Fix Data
                    if (GPS.GPGGA.Position != null)
                        lbGGAPosition.Text = GPS.GPGGA.Position.ToString("DM");
                    else
                        lbGGAPosition.Text = "";
                    lbGGATimeOfFix.Text = GPS.GPGGA.TimeOfFix.Hour.ToString() + ":" + GPS.GPGGA.TimeOfFix.Minute.ToString() + ":" + GPS.GPGGA.TimeOfFix.Second.ToString();
                    lbGGAFixQuality.Text = GPS.GPGGA.FixQuality.ToString();
                    lbGGANoOfSats.Text = GPS.GPGGA.NoOfSats.ToString();
                    lbGGAAltitude.Text = GPS.GPGGA.Altitude.ToString() + " " + GPS.GPGGA.AltitudeUnits;
                    lbGGAHDOP.Text = GPS.GPGGA.Dilution.ToString();
                    lbGGAGeoidHeight.Text = GPS.GPGGA.HeightOfGeoid.ToString();
                    lbGGADGPSupdate.Text = GPS.GPGGA.DGPSUpdate.ToString();
                    lbGGADGPSID.Text = GPS.GPGGA.DGPSStationID;
                    if (GPS.GPRMC.Position != null)
                    {
                        if (GPS.GPRMC.Position.Latitude == 0.00 && GPS.GPRMC.Position.Longitude == 0.00)
                        {
                            // Do Nothing //
                        }
                        else
                        {
                            PlotGpsPositionOnMap(GPS.GPRMC.Position, GPS.GPGGA.Altitude);
                        }
                    }
                    break;
                case GPSEventType.GPGLL: //Geographic position, Latitude and Longitude
                    lbGLLPosition.Text = GPS.GPGLL.Position.ToString();
                    lbGLLTimeOfSolution.Text = (GPS.GPGLL.TimeOfSolution.HasValue ? GPS.GPGLL.TimeOfSolution.Value.Hours.ToString() + ":" + GPS.GPGLL.TimeOfSolution.Value.Minutes.ToString() + ":" + GPS.GPGLL.TimeOfSolution.Value.Seconds.ToString() : "");
                    lbGLLDataValid.Text = GPS.GPGLL.DataValid.ToString();
                    break;
                case GPSEventType.GPGSA: //GPS DOP and active satellites
                    if (GPS.GPGSA.Mode == 'A')
                        lbGSAMode.Text = "Auto";
                    else if (GPS.GPGSA.Mode == 'M')
                        lbGSAMode.Text = "Manual";
                    else lbGSAMode.Text = "";
                    lbGSAFixMode.Text = GPS.GPGSA.FixMode.ToString();
                    lbGSAPRNs.Text = "";
                    if (GPS.GPGSA.PRNInSolution.Count > 0)
                        foreach (string prn in GPS.GPGSA.PRNInSolution)
                            lbGSAPRNs.Text += prn + " ";
                    else
                        lbGSAPRNs.Text += "none";
                    lbGSAPDOP.Text = GPS.GPGSA.PDOP.ToString() + " (" + DOPtoWord(GPS.GPGSA.PDOP) + ")";
                    lbGSAHDOP.Text = GPS.GPGSA.HDOP.ToString() + " (" + DOPtoWord(GPS.GPGSA.HDOP) + ")";
                    lbGSAVDOP.Text = GPS.GPGSA.VDOP.ToString() + " (" + DOPtoWord(GPS.GPGSA.VDOP) + ")";
                    break;
                case GPSEventType.GPGSV: //Satellites in view
                    if (NMEAtabs.SelectedTabPage.Text == "GPGSV") //Only update this tab when it is active
                        DrawGSV();
                    break;
                case GPSEventType.PGRME: //Garmin proprietary sentences.
                    //    lbRMEHorError.Text = GPS.PGRME.EstHorisontalError.ToString();
                    //    lbRMEVerError.Text = GPS.PGRME.EstVerticalError.ToString();
                    //    lbRMESphericalError.Text = GPS.PGRME.EstSphericalError.ToString();
                    break;
                case GPSEventType.TimeOut: //Serialport timeout.
                    statusBar1.Text = "Serial Port Timeout";
                    /*notification1.Caption = "GPS Serialport timeout";
                    notification1.InitialDuration = 5;
                    notification1.Text = "Check your settings and connection";
                    notification1.Critical = false;
                    notification1.Visible = true;
                     */
                    break;
            }
        }
        /*   private double[] TransformToUTM(GPS.Coordinate p)
           {
               //For fun, let's use the SharpMap transformation library and display the position in UTM
               int zone = (int)Math.Floor((p.Longitude + 183) / 6.0);
               SharpMap.CoordinateSystems.ProjectedCoordinateSystem proj = SharpMap.CoordinateSystems.ProjectedCoordinateSystem.WGS84_UTM(zone, (p.Latitude >= 0));
               SharpMap.CoordinateSystems.Transformations.ICoordinateTransformation trans =
                   new SharpMap.CoordinateSystems.Transformations.CoordinateTransformationFactory().CreateFromCoordinateSystems(proj.GeographicCoordinateSystem, proj);
               double[] result = trans.MathTransform.Transform(new double[] { p.Longitude, p.Latitude });
               return new double[] { result[0], result[1], zone };
           } */
        private string DOPtoWord(double dop)
        {
            if (dop < 1.5) return "Ideal";
            else if (dop < 3) return "Excellent";
            else if (dop < 6) return "Good";
            else if (dop < 8) return "Moderate";
            else if (dop < 20) return "Fair";
            else return "Poor";
        }
        private void DrawGSV()
        {
            System.Drawing.Color[] Colors = { Color.Blue , Color.Red , Color.Green, Color.Yellow, Color.Cyan, Color.Orange,
											  Color.Gold , Color.Violet, Color.YellowGreen, Color.Brown, Color.GreenYellow,
											  Color.Blue , Color.Red , Color.Green, Color.Yellow, Color.Aqua, Color.Orange};
            //Generate signal level readout
            int SatCount = GPS.GPGSV.SatsInView;
            Bitmap imgSignals = new Bitmap(picGSVSignals.Width, picGSVSignals.Height);
            Graphics g = Graphics.FromImage(imgSignals);
            g.Clear(Color.White);
            Pen penBlack = new Pen(Color.Black, 1);
            Pen penBlackDashed = new Pen(Color.Black, 1);
            penBlackDashed.DashPattern = new float[] { 2f, 2f };
            Pen penGray = new Pen(Color.LightGray, 1);
            int iMargin = 4; //Distance to edge of image
            int iPadding = 4; //Distance between signal bars
            g.DrawRectangle(penBlack, 0, 0, imgSignals.Width - 1, imgSignals.Height - 1);

            StringFormat sFormat = new StringFormat();
            int barWidth = 1;
            if (SatCount > 0)
                barWidth = (imgSignals.Width - 2 * iMargin - iPadding * (SatCount - 1)) / SatCount;

            //Draw horisontal lines
            for (int i = imgSignals.Height - 15; i > iMargin; i -= (imgSignals.Height - 15 - iMargin) / 5)
                g.DrawLine(penGray, 1, i, imgSignals.Width - 2, i);
            sFormat.Alignment = StringAlignment.Center;
            //Draw satellites
            //GPS.GPGSV.Satellites.Sort(); //new Comparison<GPS.NMEA.GPGSV.Satellite>(sat1, sat2) { int.Parse(sat1)
            for (int i = 0; i < GPS.GPGSV.Satellites.Count; i++)
            {
                GPS.NMEA.GPGSV.Satellite sat = GPS.GPGSV.Satellites[i];
                int startx = i * (barWidth + iPadding) + iMargin;
                int starty = imgSignals.Height - 15;
                int height = (imgSignals.Height - 15 - iMargin) / 50 * sat.SNR;
                if (GPS.GPGSA.PRNInSolution.Contains(sat.PRN))
                {
                    g.FillRectangle(new System.Drawing.SolidBrush(Colors[i]), startx, starty - height + 1, barWidth, height);
                    g.DrawRectangle(penBlack, startx, starty - height, barWidth, height);
                }
                else
                {
                    g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(50, Colors[i])), startx, starty - height + 1, barWidth, height);
                    g.DrawRectangle(penBlackDashed, startx, starty - height, barWidth, height);
                }

                sFormat.LineAlignment = StringAlignment.Near;
                g.DrawString(sat.PRN, new System.Drawing.Font("Verdana", 9, FontStyle.Regular), new System.Drawing.SolidBrush(Color.Black), startx + barWidth / 2, imgSignals.Height - 15, sFormat);
                sFormat.LineAlignment = StringAlignment.Far;
                g.DrawString(sat.SNR.ToString(), new System.Drawing.Font("Verdana", 9, FontStyle.Regular), new System.Drawing.SolidBrush(Color.Black), startx + barWidth / 2, starty - height, sFormat);
            }
            picGSVSignals.Image = imgSignals;

            //Generate sky view
            Bitmap imgSkyview = new Bitmap(picGSVSkyview.Width, picGSVSkyview.Height);
            g = Graphics.FromImage(imgSkyview);
            g.Clear(Color.Transparent);
            g.FillEllipse(Brushes.White, 0, 0, imgSkyview.Width - 1, imgSkyview.Height - 1);
            g.DrawEllipse(penGray, 0, 0, imgSkyview.Width - 1, imgSkyview.Height - 1);
            g.DrawEllipse(penGray, imgSkyview.Width / 4, imgSkyview.Height / 4, imgSkyview.Width / 2, imgSkyview.Height / 2);
            g.DrawLine(penGray, imgSkyview.Width / 2, 0, imgSkyview.Width / 2, imgSkyview.Height);
            g.DrawLine(penGray, 0, imgSkyview.Height / 2, imgSkyview.Width, imgSkyview.Height / 2);
            sFormat.LineAlignment = StringAlignment.Near;
            sFormat.Alignment = StringAlignment.Near;
            float radius = 6f;
            for (int i = 0; i < GPS.GPGSV.Satellites.Count; i++)
            {
                GPS.NMEA.GPGSV.Satellite sat = GPS.GPGSV.Satellites[i];
                double ang = 90.0 - sat.Azimuth;
                ang = ang / 180.0 * Math.PI;
                int x = imgSkyview.Width / 2 + (int)Math.Round((Math.Cos(ang) * ((90.0 - sat.Elevation) / 90.0) * (imgSkyview.Width / 2.0 - iMargin)));
                int y = imgSkyview.Height / 2 - (int)Math.Round((Math.Sin(ang) * ((90.0 - sat.Elevation) / 90.0) * (imgSkyview.Height / 2.0 - iMargin)));
                g.FillEllipse(new System.Drawing.SolidBrush(Colors[i]), x - radius * 0.5f, y - radius * 0.5f, radius, radius);

                if (GPS.GPGSA.PRNInSolution.Contains(sat.PRN))
                {
                    g.DrawEllipse(penBlack, x - radius * 0.5f, y - radius * 0.5f, radius, radius);
                    g.DrawString(sat.PRN, new System.Drawing.Font("Verdana", 9, FontStyle.Bold), new System.Drawing.SolidBrush(Color.Black), x, y, sFormat);
                }
                else
                    g.DrawString(sat.PRN, new System.Drawing.Font("Verdana", 8, FontStyle.Italic), new System.Drawing.SolidBrush(Color.Gray), x, y, sFormat);
            }
            picGSVSkyview.Image = imgSkyview;
        }

        #endregion


        #region Legend

        private void bbiLegend_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (bbiLegend.Checked)
            {
                if (intUseThematicStyling == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Create Legend - Thematic Styling is not switched on.\n\nSwitch on Thematic Styling from the Map Object Properties on the Layer Manager first then try again.", "Create Legend", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    Create_Legend();
                    if (dockPanel5.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Hidden) dockPanel5.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
                }
            }
            else
            {
                if (dockPanel5.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden) dockPanel5.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
                pictureBoxLegend.Image = null;  // Save some memory by getting rid of the Legend bitmap //
            }
        }

        private void dockPanel5_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bbiLegend.Checked) bbiLegend.Checked = false;
        }

        public void Create_Legend()
        {
            // Get Number of Items for Legend so height of image can be calculated //
            bool boolTooManyRows = false;
            int intRowsToShow = 0;
            foreach (DataRow dr in dsThematicStyleItems.Tables[0].Rows)
            {
                if (Convert.ToInt32(dr["ShowInLegend"]) == 1) intRowsToShow++;
            }
            if (intRowsToShow > 1250)  // Stop potential crash due to generated bitmap being too big (Can probably get away with about 1400! //
            {
                intRowsToShow = 1250;
                boolTooManyRows = true;
            }

            // Create Bitmap //
            //Bitmap B = new Bitmap(310, 45 + (intRowsToShow > 0 ? intRowsToShow * 23 : 0));
            Bitmap B = new Bitmap(310, 45 + (intRowsToShow > 0 ? intRowsToShow * 23 : 0), PixelFormat.Format32bppArgb);

            Brush brush;
            Brush textBrush = new SolidBrush(Color.Black);
            int intProcessedCount = 0;

            // Draw Legend onto Bitmap //
            int intStartY = 40;
            int intColumn1X = 5;
            int intColumn2X = 35;
            int intColumn3X = 65;
            int intColumn4X = 105;
            using (Graphics G = Graphics.FromImage(B))
            {
                DrawText(G, "Legend", intColumn1X, 0, textBrush, 10, FontStyle.Bold);
                DrawText(G, strThematicStylingFieldName, intColumn3X, 0, textBrush, 10, FontStyle.Bold);

                DrawText(G, "Point", intColumn1X, 20, textBrush, 8, FontStyle.Regular);
                DrawText(G, "Poly", intColumn2X, 20, textBrush, 8, FontStyle.Regular);
                DrawText(G, "Line", intColumn3X, 20, textBrush, 8, FontStyle.Regular);
                DrawText(G, "Description", intColumn4X, 20, textBrush, 8, FontStyle.Regular);
                G.DrawLine(new Pen(Color.Black, 1), new System.Drawing.Point(intColumn1X, 35), new System.Drawing.Point(intColumn1X + 300, 35));

                foreach (DataRow dr in dsThematicStyleItems.Tables[0].Rows)
                {
                    if (Convert.ToInt32(dr["ShowInLegend"]) != 1) continue;  // Skip Item //
                    intProcessedCount++;
                    if (intProcessedCount > 1250 || intProcessedCount > intRowsToShow) break;   // Abort as we have reached our maximum number of rows //
                    // Draw Point //
                    switch (Convert.ToInt32(dr["Symbol"]))
                    {
                        case 34: // Solid Circle //
                            //brush = new SolidBrush(Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])));
                            brush = new SolidBrush(Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["SymbolColour"]))));
                            DrawShape(G, "Circle", intColumn1X, intStartY, Color.Black, brush, 1);
                            brush.Dispose();
                            break;
                        case 40: // Hollow Circle //
                            brush = new SolidBrush(Color.White);
                            DrawShape(G, "Circle", intColumn1X, intStartY, Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])), brush, 1);
                            brush.Dispose();
                            break;
                        case 32: // Solid Square //
                            //brush = new SolidBrush(Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])));
                            brush = new SolidBrush(Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["SymbolColour"]))));
                            DrawShape(G, "Rectangle", intColumn1X, intStartY, Color.Black, brush, 1);
                            brush.Dispose();
                            break;
                        case 38: // Hollow Square //
                            brush = new SolidBrush(Color.White);
                            DrawShape(G, "Rectangle", intColumn1X, intStartY, Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])), brush, 1);
                            brush.Dispose();
                            break;
                        case 33: // Solid Diamond //
                            //brush = new SolidBrush(Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])));
                            brush = new SolidBrush(Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["SymbolColour"]))));
                            DrawShape(G, "Diamond", intColumn1X, intStartY, Color.Black, brush, 1);
                            brush.Dispose();
                            break;
                        case 39: // Hollow Diamond //
                            brush = new SolidBrush(Color.White);
                            DrawShape(G, "Diamond", intColumn1X, intStartY, Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])), brush, 1);
                            brush.Dispose();
                            break;
                        case 36: // Solid Triangle1 //
                            //brush = new SolidBrush(Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])));
                            brush = new SolidBrush(Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["SymbolColour"]))));
                            DrawShape(G, "Triangle1", intColumn1X, intStartY, Color.Black, brush, 1);
                            brush.Dispose();
                            break;
                        case 42: // Hollow Triangle1 //
                            brush = new SolidBrush(Color.White);
                            DrawShape(G, "Triangle1", intColumn1X, intStartY, Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])), brush, 1);
                            brush.Dispose();
                            break;
                        case 37: // Solid Triangle2 //
                            //brush = new SolidBrush(Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])));
                            brush = new SolidBrush(Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["SymbolColour"]))));
                            DrawShape(G, "Triangle2", intColumn1X, intStartY, Color.Black, brush, 1);
                            brush.Dispose();
                            break;
                        case 43: // Hollow Triangle2 //
                            brush = new SolidBrush(Color.White);
                            DrawShape(G, "Triangle2", intColumn1X, intStartY, Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])), brush, 1);
                            brush.Dispose();
                            break;
                        case 35: // Solid Star //
                            //brush = new SolidBrush(Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])));
                            brush = new SolidBrush(Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["SymbolColour"]))));
                            DrawShape(G, "Star", intColumn1X, intStartY, Color.Black, brush, 1);
                            brush.Dispose();
                            break;
                        case 41: // Hollow Star //
                            brush = new SolidBrush(Color.White);
                            DrawShape(G, "Star", intColumn1X, intStartY, Color.FromArgb(Convert.ToInt32(dr["SymbolColour"])), brush, 1);
                            brush.Dispose();
                            break;
                        default:
                            break;
                    }
                    // Draw Polygon //
                    switch (Convert.ToInt32(dr["PolygonFillPattern"]))
                    {
                        case 2: // Solid Fill //
                            //brush = new SolidBrush(Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"])));
                            brush = new SolidBrush(Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"]))));
                            DrawShape(G, "Polygon", intColumn2X, intStartY, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonLineColour"]))), brush, Convert.ToInt32(dr["PolygonLineWidth"]));
                            brush.Dispose();
                            break;
                        case 3: // Horizontal Line Fill //
                            //brush = new HatchBrush(HatchStyle.Horizontal, Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"])), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"])));
                            brush = new HatchBrush(HatchStyle.Horizontal, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"]))), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"]))));
                            DrawShape(G, "Polygon", intColumn2X, intStartY, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonLineColour"]))), brush, Convert.ToInt32(dr["PolygonLineWidth"]));
                            brush.Dispose();
                            break;
                        case 4: // Vertical Line Fill //
                            //brush = new HatchBrush(HatchStyle.Vertical, Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"])), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"])));
                            brush = new HatchBrush(HatchStyle.Vertical, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"]))), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"]))));
                            DrawShape(G, "Polygon", intColumn2X, intStartY, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonLineColour"]))), brush, Convert.ToInt32(dr["PolygonLineWidth"]));
                            brush.Dispose();
                            break;
                        case 6: // Diagonal Fill //
                            //brush = new HatchBrush(HatchStyle.ForwardDiagonal, Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"])), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"])));
                            brush = new HatchBrush(HatchStyle.ForwardDiagonal, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"]))), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"]))));
                            DrawShape(G, "Polygon", intColumn2X, intStartY, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonLineColour"]))), brush, Convert.ToInt32(dr["PolygonLineWidth"]));
                            brush.Dispose();
                            break;
                        case 8: // CrossHatch Fill //
                            //brush = new HatchBrush(HatchStyle.OutlinedDiamond, Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"])), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"])));
                            brush = new HatchBrush(HatchStyle.OutlinedDiamond, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"]))), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"]))));
                            DrawShape(G, "Polygon", intColumn2X, intStartY, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonLineColour"]))), brush, Convert.ToInt32(dr["PolygonLineWidth"]));
                            brush.Dispose();
                            break;
                        case 14: // Dots 1 Fill //
                            //brush = new HatchBrush(HatchStyle.Percent70, Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"])), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"])));
                            brush = new HatchBrush(HatchStyle.Percent70, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"]))), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"]))));
                            DrawShape(G, "Polygon", intColumn2X, intStartY, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonLineColour"]))), brush, Convert.ToInt32(dr["PolygonLineWidth"]));
                            brush.Dispose();
                            break;
                        case 17: // Dots 2 Fill //
                            //brush = new HatchBrush(HatchStyle.Percent20, Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"])), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"])));
                            brush = new HatchBrush(HatchStyle.Percent20, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillPatternColour"]))), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonFillColour"]))));
                            DrawShape(G, "Polygon", intColumn2X, intStartY, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolygonLineColour"]))), brush, Convert.ToInt32(dr["PolygonLineWidth"]));
                            brush.Dispose();
                            break;
                        default:
                            break;
                    }
                    // Draw Polyline //
                    Image img = null;
                    ImageAttributes att = new ImageAttributes();
                    ColorMap map = new ColorMap();
                    switch (Convert.ToInt32(dr["PolylineStyle"]))
                    {
                        case 2: // Solid Line //
                            img = imageListLineStylesLegend.Images[0];
                            map.OldColor = Color.Red;
                            //map.NewColor = Color.FromArgb(Convert.ToInt32(dr["PolylineColour"]));
                            map.NewColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolylineColour"])));
                            att.SetRemapTable(new ColorMap[] { map });
                            G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 7, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, att);
                            //G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 10, img.Width - 45, 1), 8, 8, img.Width - 45, img.Height - 14, GraphicsUnit.Pixel, att);
                            att.Dispose();
                            break;
                        case 6: // Dashed Line //
                            img = imageListLineStylesLegend.Images[1];
                            map.OldColor = Color.Red;
                            //map.NewColor = Color.FromArgb(Convert.ToInt32(dr["PolylineColour"]));
                            map.NewColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolylineColour"])));
                            att.SetRemapTable(new ColorMap[] { map });
                            G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 7, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, att);
                            //G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 10, img.Width - 45, 1), 18, 8, img.Width - 45, img.Height - 14, GraphicsUnit.Pixel, att);
                            att.Dispose();
                            break;
                        case 10: // Dotted Line //
                            img = imageListLineStylesLegend.Images[2];
                            map.OldColor = Color.Red;
                            //map.NewColor = Color.FromArgb(Convert.ToInt32(dr["PolylineColour"]));
                            map.NewColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolylineColour"])));
                            att.SetRemapTable(new ColorMap[] { map });
                            G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 7, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, att);
                            //G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 10, img.Width - 45, 1), 18, 8, img.Width - 45, img.Height - 14, GraphicsUnit.Pixel, att);
                            att.Dispose();
                            break;
                        case 31: // Dot Dashed Line //
                            img = imageListLineStylesLegend.Images[3];
                            map.OldColor = Color.Red;
                            //map.NewColor = Color.FromArgb(Convert.ToInt32(dr["PolylineColour"]));
                            map.NewColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolylineColour"])));
                            att.SetRemapTable(new ColorMap[] { map });
                            G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 7, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, att);
                            //G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 10, img.Width - 45, 1), 18, 8, img.Width - 45, img.Height - 14, GraphicsUnit.Pixel, att);
                            att.Dispose();
                            break;
                        case 54: // Arrowed Line //
                            img = imageListLineStylesLegend.Images[4];
                            map.OldColor = Color.Red;
                            //map.NewColor = Color.FromArgb(Convert.ToInt32(dr["PolylineColour"]));
                            map.NewColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolylineColour"])));
                            att.SetRemapTable(new ColorMap[] { map });
                            G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 7, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, att);
                            //G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 8, img.Width - 45, 5), 18, 6, img.Width - 45, img.Height - 10, GraphicsUnit.Pixel, att);
                            att.Dispose();
                            break;
                        case 63: // Double Line //
                            img = imageListLineStylesLegend.Images[5];
                            map.OldColor = Color.Red;
                            //map.NewColor = Color.FromArgb(Convert.ToInt32(dr["PolylineColour"]));
                            map.NewColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolylineColour"])));
                            att.SetRemapTable(new ColorMap[] { map });
                            G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 7, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, att);
                            //G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 9, img.Width - 45, 3), 18, 6, img.Width - 45, img.Height - 12, GraphicsUnit.Pixel, att);
                            att.Dispose();
                            break;
                        case 64: // Triple Line //
                            img = imageListLineStylesLegend.Images[6];
                            map.OldColor = Color.Red;
                            //map.NewColor = Color.FromArgb(Convert.ToInt32(dr["PolylineColour"]));
                            map.NewColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolylineColour"])));
                            att.SetRemapTable(new ColorMap[] { map });
                            G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 7, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, att);
                            //G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 8, img.Width - 45, 5), 18, 5, img.Width - 45, img.Height - 10, GraphicsUnit.Pixel, att);
                            att.Dispose();
                            break;
                        case 73: // Thick Dashed Line //
                            img = imageListLineStylesLegend.Images[7];
                            map.OldColor = Color.Red;
                            //map.NewColor = Color.FromArgb(Convert.ToInt32(dr["PolylineColour"]));
                            map.NewColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolylineColour"])));
                            att.SetRemapTable(new ColorMap[] { map });
                            G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 7, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, att);
                            //G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 9, img.Width - 45, 3), 18, 6, img.Width - 45, img.Height - 12, GraphicsUnit.Pixel, att);
                            att.Dispose();
                            break;
                        case 93: // Thick Squared Line //
                            img = imageListLineStylesLegend.Images[8];
                            map.OldColor = Color.Red;
                            //map.NewColor = Color.FromArgb(Convert.ToInt32(dr["PolylineColour"]));
                            map.NewColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(dr["PolylineColour"])));
                            att.SetRemapTable(new ColorMap[] { map });
                            G.DrawImage(img, new System.Drawing.Rectangle(intColumn3X, intStartY + 7, img.Width, img.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, att);
                            att.Dispose();
                            break;
                    }
                    // Draw Description //
                    DrawText(G, dr["ItemDescription"].ToString(), intColumn4X, intStartY + 3, textBrush, 8, FontStyle.Regular);

                    intStartY += 23;
                }
            }
            textBrush.Dispose();
            pictureBoxLegend.Image = B;
            if (boolTooManyRows)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Warning: The Map Legend supports up to 1250 items. You have selected more than 1250 items. Only the first 1250 are displayed in the legend.", "Create Legend", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DrawShape(Graphics G, string strShapeName, int intStartX, int intStartY, Color BorderColor, Brush BrushStyle, int intWidth)
        {
            G.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Pen pen = new Pen(BorderColor, intWidth);
            pen.Alignment = PenAlignment.Inset;

            switch (strShapeName)
            {
                case "Rectangle":
                    intStartX += 2;
                    intStartY += 2;
                    System.Drawing.Rectangle rec = new System.Drawing.Rectangle(intStartX, intStartY, 16, 16);
                    G.DrawRectangle(pen, rec);
                    G.FillRectangle(BrushStyle, rec);
                    break;
                case "Diamond":
                    System.Drawing.Point[] myPointArray = { new System.Drawing.Point(intStartX, intStartY + 10), new System.Drawing.Point(intStartX + 10, intStartY + 20), new System.Drawing.Point(intStartX + 20, intStartY + 10), new System.Drawing.Point(intStartX + 10, intStartY) };
                    G.FillPolygon(BrushStyle, myPointArray);
                    G.DrawPolygon(pen, myPointArray);
                    break;
                case "Star":
                    intStartX += 10;
                    intStartY += 10;
                    GraphicsPath star = Star(intStartX, intStartY, 4, 11, 5, 270);
                    G.FillPath(BrushStyle, star);
                    G.DrawPath(pen, star);
                    break;
                case "Circle":
                    intStartX += 2;
                    intStartY += 2;
                    System.Drawing.Rectangle rec2 = new System.Drawing.Rectangle(intStartX, intStartY, 16, 16);
                    G.FillEllipse(BrushStyle, rec2);
                    G.DrawEllipse(pen, rec2);
                    break;
                case "Triangle1":
                    intStartY += 5;
                    System.Drawing.Point[] myPointArray2 = { new System.Drawing.Point(intStartX, intStartY + 10), new System.Drawing.Point(intStartX + 10, intStartY), new System.Drawing.Point(intStartX + 20, intStartY + 10) };
                    G.FillPolygon(BrushStyle, myPointArray2);
                    G.DrawPolygon(pen, myPointArray2);
                    break;
                case "Triangle2":
                    intStartY += 5;
                    System.Drawing.Point[] myPointArray3 = { new System.Drawing.Point(intStartX, intStartY), new System.Drawing.Point(intStartX + 10, intStartY + 10), new System.Drawing.Point(intStartX + 20, intStartY) };
                    G.FillPolygon(BrushStyle, myPointArray3);
                    G.DrawPolygon(pen, myPointArray3);
                    break;
                case "Polygon":
                    System.Drawing.Point[] myPointArray4 = {new System.Drawing.Point(intStartX, intStartY + 7), 
                                                            new System.Drawing.Point(intStartX, intStartY + 19), 
                                                            new System.Drawing.Point(intStartX + 20, intStartY + 19), 
                                                            new System.Drawing.Point(intStartX + 20, intStartY + 11), 
                                                            new System.Drawing.Point(intStartX + 10, intStartY + 1), 
                                                            new System.Drawing.Point(intStartX, intStartY + 7) };
                    G.FillPolygon(BrushStyle, myPointArray4);
                    G.DrawPolygon(pen, myPointArray4);
                    break;
                case "Line":
                    G.DrawLine(pen, new System.Drawing.Point(intStartX, intStartY + 9), new System.Drawing.Point(intStartX + 20, intStartY + 9));
                    break;
                default:
                    break;
            }
            pen.Dispose();
        }

        private void DrawText(Graphics G, string strText, int intStartX, int intStartY, Brush BrushStyle, int intSize, FontStyle fontStyle)
        {
            G.DrawString(strText, new System.Drawing.Font("Tahoma", intSize, fontStyle), BrushStyle, (float)intStartX, (float)intStartY);
        }

        private const double DEGREE = 0.017453292519943295769236907684886;  // Number of radians in one degree //
        public static GraphicsPath Star(int c_x, int c_y, int inner_radius, int radius, int n, int rot)
        {
            rot %= 360;
            System.Drawing.Point[] points = new System.Drawing.Point[n * 2];
            byte[] types = new byte[n * 2];
            int index = 0;
            for (int a = rot; a < 360 + rot; a += 180 / n)
            {
                if (index >= points.Length) break;
                points[index] = new System.Drawing.Point(c_x + (int)(((index % 2 == 0) ? radius : inner_radius) * Math.Cos(DEGREE * a)), c_y + (int)(((index % 2 == 0) ? radius : inner_radius) * Math.Sin(DEGREE * a)));
                types[index] = (byte)PathPointType.Line;
                index++;
            }
            types[0] = (byte)PathPointType.Start;
            return new GraphicsPath(points, types);
        }

        #endregion


        #region Scale Bar

        private void bciScaleBar_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            Image image = null;
            if (bciScaleBar.Checked)
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                image = MapFunctions.Create_Scale_Bar(pictureBoxScaleBar.Size.Width, pictureBoxScaleBar.Size.Height, Convert.ToInt32(dCurrentMapScale));
                dockPanel6.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
                pictureBoxScaleBar.Image = image;
            }
            else
            {
                dockPanel6.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
                pictureBoxScaleBar.Image = image;
            }
        }

        private void dockPanel6_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciScaleBar.Checked) bciScaleBar.Checked = false;
        }

        private void dockPanel6_Resize(object sender, EventArgs e)
        {
            refresh_scale_bar();
        }

        private void pictureBoxScaleBar_Resize(object sender, EventArgs e)
        {
        }

        private void refresh_scale_bar()
        {
            if (pictureBoxScaleBar.Image != null)
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                Image image = MapFunctions.Create_Scale_Bar(pictureBoxScaleBar.Size.Width, pictureBoxScaleBar.Size.Height, Convert.ToInt32(dCurrentMapScale));
                pictureBoxScaleBar.Image = image;
            }

        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        #endregion


        #region LinkedInspections

        private void bciLinkedInspections_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (bciLinkedInspections.Checked)
            {
                dockPanelInspections.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelInspections.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void dockPanelInspections_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciLinkedInspections.Checked) bciLinkedInspections.Checked = false;
            this.dataSet_AT_DataEntry.sp01367_AT_Inspections_For_Trees.Clear();  // Empty Inspection grid to conserve memory //
        }


        #region InspectionsGrid

        private void gridView7_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Inspections Available - Select one or more Map Objects to view related Inspections");
        }

        private void gridView7_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;               
                
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView7_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedOutstandingActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedOutstandingActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView7_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedActionCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedOutstandingActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedOutstandingActionCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        /*    private void repositoryItemHyperLinkEdit6_OpenLink(object sender, OpenLinkEventArgs e)
            {
                GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
                string strInspectionIDs = view.GetRowCellValue(view.FocusedRowHandle, "InspectionID").ToString() + ",";
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                string strRecordIDs = "";
                switch (view.FocusedColumn.FieldName)
                {
                    case "LinkedActionCount":
                        strRecordIDs = GetSetting.sp01333_AT_Inspection_Manager_Get_Linked_Action_IDs(strInspectionIDs, 0).ToString();
                        break;
                    case "LinkedOutstandingActionCount":
                        strRecordIDs = GetSetting.sp01333_AT_Inspection_Manager_Get_Linked_Action_IDs(strInspectionIDs, 1).ToString();
                        break;
                    default:
                        break;
                }
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "inspection");
            }*/

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl6.Focus();  // Just in case Map has focused something else //
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region ActionsGrid

        private void gridView8_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Actions Available - Select one or more Map Objects to view related Actions");
        }

        private void gridView8_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 7;
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;
                
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl7_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl7.Focus();  // Just in case Map has focused something else //
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion

        #endregion


        #region DataSets

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = "";
            string strSelectedTrees = "";
            string strSelectedTree = "";
            int intSelectedTreeCount = 0;
            string strSelectedInspections = "";
            string strSelectedInspection = "";
            int intSelectedInspectionCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Available Trees Grid //
                    intCount = 0;
                    view = (GridView)gridControl2.MainView;
                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                        {
                            if (strSelectedIDs == "")
                            {
                                strSelectedIDs = Convert.ToString(view.GetRowCellValue(i, view.Columns["TreeID"])) + ',';
                                intCount++;
                            }
                            else
                            {
                                strCurrentID = Convert.ToString(view.GetRowCellValue(i, view.Columns["TreeID"])) + ',';
                                if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                                {
                                    strSelectedIDs += strCurrentID;
                                    intCount++;
                                }
                            }
                        }
                    }

                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    CreateDataset("Tree", intCount, strSelectedIDs, 0, "", 0, "");
                    break;

                case 6:  // Available Inspections Grid //
                    view = (GridView)gridControl6.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Inspections //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                    }
                    CreateDataset("Inspection", intSelectedTreeCount, strSelectedTrees, intCount, strSelectedIDs, 0, "");
                    break;
                case 7:  // Available Action Grid //
                    view = (GridView)gridControl7.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Actions //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                        // Inspections //
                        if (strSelectedInspections == "")
                        {
                            strSelectedInspections = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                            intSelectedInspectionCount++;
                        }
                        else
                        {
                            strSelectedInspection = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedInspections.Contains("," + strSelectedInspection))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedInspections += strSelectedInspection;
                                intSelectedInspectionCount++;
                            }
                        }
                    }
                    CreateDataset("Action", intSelectedTreeCount, strSelectedTrees, intSelectedInspectionCount, strSelectedInspections, intCount, strSelectedIDs);
                    break;
                default:
                    return;
            }
        }

        private void CreateDataset(string strType, int intTreeCount, string strSelectedTreeIDs, int intInspectionCount, string strSelectedInspectionIDs, int intActionCount, string strSelectedActionIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            //Form frmMain = this.MdiParent;
            //frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_tree_count = intTreeCount;
            fChildForm.i_int_selected_inspection_count = intInspectionCount;
            fChildForm.i_int_selected_action_count = intActionCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                string strSelectedIDs = "";
                switch (strDatasetType.ToUpper())
                {
                    case "TREE":
                        strSelectedIDs = strSelectedTreeIDs;
                        break;
                    case "INSPECTION":
                        strSelectedIDs = strSelectedInspectionIDs;
                        break;
                    case "ACTION":
                        strSelectedIDs = strSelectedActionIDs;
                        break;
                    default:
                        strSelectedIDs = "";
                        break;
                }
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }

        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Available Trees Grid //

                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "TreeID", "Highlight", true);
                            break;
                        default:
                            break;
                    }
                    return;
                case 6:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 7:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Available Trees Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID", "Highlight", true);
                            break;
                        default:
                            break;
                    }
                    return;
                case 6:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 7:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
        }


        #endregion


        #region Gazetteer

        private void bciGazetteer_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (bciGazetteer.Checked)
            {
                dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void dockPanelGazetteer_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciGazetteer.Checked) bciGazetteer.Checked = false;
        }

        private void lookUpEditGazetteerSearchType_EditValueChanged(object sender, EventArgs e)
        {
            this.dataSet_AT_TreePicker.sp01312_AT_Tree_Picker_Gazetteer_Search.Rows.Clear();
            LookUpEdit lue = (LookUpEdit)sender;
            if (lue.EditValue.ToString().ToLower() == "sites")
            {
                gridControl3.MainView = gridView6;
            }
            else
            {
                gridControl3.MainView = gridView3;
            }
        }

        private void buttonEditGazetteerFindValue_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GazetteerFind();
        }

        private void GazetteerFind()
        {
            WaitDialogForm loading = new WaitDialogForm("Searching for Matches...", "WoodPlan Mapping");
            loading.Show();

            string strType = lookUpEditGazetteerSearchType.EditValue.ToString();
            int intPattern = Convert.ToInt32(radioGroupGazetteerMatchPattern.EditValue);
            string strValue = (intPattern == 0 ? "" : "%") + buttonEditGazetteerFindValue.EditValue.ToString() + "%";
            GridView view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01312_AT_Tree_Picker_Gazetteer_Search, strType, strValue);
            view.EndUpdate();

            loading.Close();
        }

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle) ShowSite(Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "XCoordinate")), Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "YCoordinate")));
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                //{
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiGazetteerShowMatch.Enabled = (intCount == 1 ? true : false);

                beiGazetteerLoadObjectsWithinRange.Enabled = (intCount == 1 ? true : false);
                beiGazetteerLoadObjectsWithinRange.Visibility = BarItemVisibility.Always;  // Show Range Spinner menu item //

                bbiGazetteerLoadMapObjectsInRange.Enabled = (intCount == 1 ? true : false);
                bbiGazetteerLoadMapObjectsInRange.Caption = "Load  Map Objects in Range";

                bbiGazetteerClearMapSearch.Visibility = BarItemVisibility.Always;

                popupMenu_Gazetteer.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle) ShowSite(Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "XCoordinate")), Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "YCoordinate")));
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                //{
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiGazetteerShowMatch.Enabled = (intCount == 1 ? true : false);

                beiGazetteerLoadObjectsWithinRange.Enabled = false;
                beiGazetteerLoadObjectsWithinRange.Visibility = BarItemVisibility.Never;  // Hide Range Spinner menu item //

                bbiGazetteerLoadMapObjectsInRange.Enabled = (intCount == 1 ? true : false);
                bbiGazetteerLoadMapObjectsInRange.Caption = "Load All Map Objects in Site";

                bbiGazetteerClearMapSearch.Visibility = BarItemVisibility.Never;

                popupMenu_Gazetteer.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void ShowSite(int intX, int intY)
        {
            MapInfo.Geometry.DPoint dpt1 = new MapInfo.Geometry.DPoint((double)intX, (double)intY);  // create a DPoint to center the map on //

            Double dblCurrentScale = (this.mapControl1.Map.Scale > 5000 ? 5000 : this.mapControl1.Map.Scale);
            this.mapControl1.Map.SetView(dpt1, mapControl1.Map.GetDisplayCoordSys(), dblCurrentScale);

            // Draw Locus Effect on object so user can see it //
            System.Drawing.Point screenPoint;
            MapInfo.Geometry.DisplayTransform converter = this.mapControl1.Map.DisplayTransform;
            converter.ToDisplay(dpt1, out screenPoint);

            System.Drawing.Point MapControlPoint = new System.Drawing.Point();
            MapControlPoint = this.mapControl1.PointToScreen(MapControlPoint);

            screenPoint.X += MapControlPoint.X;
            screenPoint.Y += MapControlPoint.Y;

            locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customBeaconLocusEffect1.Name);
        }

        private void buttonEditGazetteerFindValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) GazetteerFind();
        }

        private void bbiGazetteerShowMatch_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one Gazetteer match to locate on the map before proceeding.", "Gazetteer - Show Match", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            ShowSite(Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "XCoordinate")), Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "YCoordinate")));
        }

        private void bbiGazetteerLoadMapObjectsInRange_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            string strType = lookUpEditGazetteerSearchType.EditValue.ToString();
            if (strType == "Sites")
            {
                int intSiteID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "RecordID"));

                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Gazetteer_Map_Object_Search(this.GlobalSettings, this.strConnectionString, this.ParentForm, intSiteID.ToString() + ",", "site", "", "");
            }
            else // Address //
            {
                decimal decX = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "XCoordinate"));
                decimal decY = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "YCoordinate"));
                decimal decRange = Convert.ToDecimal(beiGazetteerLoadObjectsWithinRange.EditValue);
                //decimal decRangeAdjusted = (decRangeSpinner * (decimal)100) / (decimal)113.5;  // Gets round round size of circle drawn (always 13.5% too big) //
                decimal decRangeAdjusted = decRange;
                string strDescription = "";
                strDescription = (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine1").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine1").ToString() + " ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine2").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine2").ToString() + ", ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine3").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine3").ToString() + ", ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine4").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine4").ToString() + ", ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine5").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine5").ToString() + ", ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "Postcode").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "Postcode").ToString());
                if (string.IsNullOrEmpty(strDescription)) strDescription = "No Address Specified";
                SearchForNearbyMapObjects(decX, decY, decRange, strDescription);

            }
        }

        private void bbiGazetteerClearMapSearch_ItemClick(object sender, ItemClickEventArgs e)
        {
            Clear_Search_Results();
        }

        private void bbiCreateIncident_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            int intX = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "XCoordinate"));
            int intY = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "YCoordinate"));
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "RecordID"));
            string strAddressLine1 = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine1"));
            string strAddressLine2 = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine2"));
            string strAddressLine3 = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine3"));
            string strAddressLine4 = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine4"));
            string strAddressLine5 = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine5"));
            string strPostcode = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Postcode"));

            CreateIncidentAtLocation(intX, intY, intRecordID, strAddressLine1, strAddressLine2, strAddressLine3, strAddressLine4, strAddressLine5, strPostcode);
        }

        #endregion


        #region TempFeatures

        private void Create_Temp_Features_Layer()
        {
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t != null)
            {
                if (t.IsOpen) t.Close();
            }

            TableInfoMemTable tiTemp = new TableInfoMemTable("TempMapObjects");
            tiTemp.Temporary = true;
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedStringColumn("id", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("Description", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("Type", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateDecimalColumn("decX", 18, 2));
            tiTemp.Columns.Add(ColumnFactory.CreateDecimalColumn("decY", 18, 2));

            tiTemp.Columns.Add(ColumnFactory.CreateStyleColumn());

            CoordSysFactory factory = Session.Current.CoordSysFactory;
            CoordSys BritishGrid = factory.CreateFromMapBasicString(strDefaultMappingProjection);
            //CoordSys BritishGrid = factory.CreateFromMapBasicString("CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000");
            tiTemp.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(BritishGrid));

            Table table = Session.Current.Catalog.CreateTable(tiTemp);  // Note: No need to add a column of type Key. Every table automatically contains a column named "MI_Key". //

            MapTableLoader tl = new MapTableLoader(table);
            try
            {
                tl.AutoPosition = false; // Set table loader options //
                tl.StartPosition = 0;
                mapControl1.Map.Load(tl);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load the Temporary Map Object Layer - an error occurred [ " + ex.Message + "].\nYou should Close the Mapping screen once in has finished loading then try opening the screen again.", "Load Map Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                conn.Close();
                conn.Dispose();
                return;
            }

            MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[0];
            MapInfo.Mapping.LayerHelper.SetSelectable(l, false);
            MapInfo.Mapping.LayerHelper.SetEditable(l, true);  // Allow the map markers to be manually moved and deleted //
            MapInfo.Mapping.LayerHelper.SetInsertable(l, false);

            
            FeatureLayer fl = (FeatureLayer)mapControl1.Map.Layers["TempMapObjects"];           
            MapInfo.Tools.MapTool.SetInfoTipExpression(mapControl1.Tools.MapToolProperties, fl, "Description");

            conn.Close();
            conn.Dispose();
        }

        private void Clear_Search_Results()
        {
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            // Remove any Prior Search First //
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("Type = 'Search'");
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            try
            {
                foreach (Feature f in irfc) // Should  probably be two features: Address Cross Hair and Search Circle //
                {
                    t.DeleteFeature(f);  // delete feature //
                }
                t.Pack(MapInfo.Data.PackType.All);  // pack the table //
            }
            catch (Exception)
            {
            }

            // Clear any existing Search FeatureStyleModifier //
            FeatureLayer fl = this.mapControl1.Map.Layers["TempMapObjects"] as FeatureLayer;
            Clear_FeatureStyleModifier(fl, "SearchFeatures");

            conn.Close();
            conn.Dispose();
        }

        public void Create_Gazetteer_Search_Temp_Feature(double doubX, double doubY, decimal decRadius, string strDescription, decimal decRangeAdjusted)
        {
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            Clear_Search_Results();  // Remove any previous search //

            // *** Create Seach Address CrossHair and Search Circle Objects and add to Map Layer *** //
            // Define Styling //
            SimpleVectorPointStyle _vectorSymbol = new SimpleVectorPointStyle();
            _vectorSymbol.Code = (short)49;  // 49 = Cross //
            _vectorSymbol.Color = Color.FromArgb(0, 0, 255);
            _vectorSymbol.PointSize = (short)30;
            CompositeStyle style = new CompositeStyle();
            style.SymbolStyle = _vectorSymbol;
            ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intGazetteerSearchCircleTransparency) / 100))), Color.FromArgb(intGazetteerSearchCircleColour));

            MapInfo.Data.Feature ftr = null;
            if (decRangeAdjusted > (decimal)0.00)  // Search Radius specified so draw it //
            {
                // Add Search Circle //
                MapInfo.Geometry.DPoint center = new MapInfo.Geometry.DPoint(Convert.ToInt32(doubX), Convert.ToInt32(doubY));
                double rad = Convert.ToDouble(decRangeAdjusted);
                MapInfo.Geometry.Ellipse circle = new MapInfo.Geometry.Ellipse(this.mapControl1.Map.GetDisplayCoordSys(), center, rad, rad, MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical);
                ftr = new MapInfo.Data.Feature(t.TableInfo.Columns);
                ftr["Description"] = "Search Radius: " + decRadius.ToString() + (decRadius != (decimal)1 ? " metres" : " metre");
                ftr["Type"] = "Search";
                ftr.Geometry = circle;
                ftr.Style = style;
                t.InsertFeature(ftr);
            }
            // Add House at centre of Search Circle //
            MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToInt32(doubX), Convert.ToInt32(doubY));
            ftr = new MapInfo.Data.Feature(t.TableInfo.Columns);
            ftr["Description"] = strDescription;
            ftr["Type"] = "Search";
            ftr.Geometry = g;
            ftr.Style = style;
            t.InsertFeature(ftr);

           // Clear any existing Search FeatureStyleModifier then re-create so search polygon area has a transparency applied to it //
            FeatureLayer fl = this.mapControl1.Map.Layers["TempMapObjects"] as FeatureLayer;
            Clear_FeatureStyleModifier(fl, "SearchFeatures");
           
            MapInfo.Data.MICommand command = conn.CreateCommand();
            command.CommandText = "Select * from TempMapObjects where Type = 'Search'";
            command.Prepare();
            MapInfo.Data.IResultSetFeatureCollection irfc = command.ExecuteFeatureCollection();

            SelectedAreaModifier sam = new SelectedAreaModifier("SearchFeatures", "SearchFeatures", irfc, intGazetteerSearchCircleColour, intGazetteerSearchCircleTransparency);  // This is declared in a new class at the end of all the code //
            fl.Modifiers.Append(sam);

            conn.Close();
            conn.Dispose();
        }

        private void Clear_FeatureStyleModifier(FeatureLayer fl, string StyleModifierName)
        {
            foreach (FeatureStyleModifier fsm in fl.Modifiers)
            {
                if (fsm.Name == StyleModifierName)
                {
                    fl.Modifiers.Remove(fsm);
                    break;
                }
            }
        }

        private void bbiDeleteSelectedTempMapObjects_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected temporary items and iterate through them and delete them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");

            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "TempMapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        t.DeleteFeature(f);  // delete feature //
                        intCount++;
                    }
                }
            }
            t.Pack(MapInfo.Data.PackType.All);  // pack the table //
            conn.Close();
            conn.Dispose();
            mapControl1.Invalidate();
            if (intCount == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more temporary map objects to delete before proceeding.", "Delete Selected Temporary Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void bbiDeleteAllMapMarkers_ItemClick(object sender, ItemClickEventArgs e)
        {         
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            try
            {
                MICommand MapCmd = conn.CreateCommand();
                MapCmd.CommandText = "Delete From TempMapObjects Where Type = 'MapMarker'";
                MapCmd.Prepare();
                MapCmd.ExecuteNonQuery();
                MapCmd.Dispose();
                t.Pack(MapInfo.Data.PackType.All);  // pack the table //
            }
            catch (Exception)
            {
            }
            conn.Close();
            conn.Dispose();
            mapControl1.Invalidate();
        }

        private void bbiCreatePolygonFromMapMarkers_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowAdd) return;
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("Type = 'MapMarker'");
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            if (irfc.Count < 3)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create Polygon from Map Markers - at least 3 Map Markers are required to create a polygon.", "Create Polygon from Map Markers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                conn.Close();
                conn.Dispose();
                return;
            }
            try
            {
                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[irfc.Count + 1];  // Extra 1 to close polygon //
                int iPoint = 0;
                foreach (Feature f in irfc)
                {
                    //dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(f.Geometry.Centroid.x), Convert.ToDouble(f.Geometry.Centroid.y));
                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(f["decX"]), Convert.ToDouble(f["decY"]));
                    iPoint++;
                }
                Feature fEnd = irfc[0];
                //dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(fEnd.Geometry.Centroid.x), Convert.ToDouble(fEnd.Geometry.Centroid.y));  // close Polygon //
                dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(fEnd["decX"]), Convert.ToDouble(fEnd["decY"]));  // close Polygon //

                MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)g.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //
                int intSuccess = AddObjectToMap(newGeometry);  // Add object to map and WoodPlan //
            }
            catch (Exception)
            {
            }
            conn.Close();
            conn.Dispose();
        }

        private void bbiCreatePolylineFromMapMarkers_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowAdd) return;
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("Type = 'MapMarker'");
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            if (irfc.Count < 2)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create Polyline from Map Markers - at least 2 Map Markers are required to create a polyline.", "Create Polyline from Map Markers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                conn.Close();
                conn.Dispose();
                return;
            }
            try
            {
                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[irfc.Count];
                int iPoint = 0;
                foreach (Feature f in irfc)
                {
                    //dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(f.Geometry.Centroid.x), Convert.ToDouble(f.Geometry.Centroid.y));
                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(f["decX"]), Convert.ToDouble(f["decY"]));
                    iPoint++;
                }
                MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)g.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //
                int intSuccess = AddObjectToMap(newGeometry);  // Add object to map and WoodPlan //
            }
            catch (Exception)
            {
            }
            conn.Close();
            conn.Dispose();
        }

        private void MapMarkerAddEvent(MapInfo.Tools.ToolUsedEventArgs e)
        {
            Add_Map_Marker(e.MapCoordinate.x, e.MapCoordinate.y);
        }

        private void Add_Map_Marker(double doubX, double doubY)
        {
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            // Add Map Marker //
            int intX = Convert.ToInt32(doubX);  // Strip off decimal places //
            int intY = Convert.ToInt32(doubY);  // Strip off decimal places //
            MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(intX), Convert.ToDouble(intY));
            MapInfo.Data.Feature ftr = new MapInfo.Data.Feature(t.TableInfo.Columns);
            ftr["Description"] = "Map Marker - X,Y: " + doubX.ToString("#0") + "," + doubY.ToString("#0");
            ftr["Type"] = "MapMarker";
            ftr["decX"] = Convert.ToDecimal(intX);
            ftr["decY"] = Convert.ToDecimal(intY);
            ftr.Geometry = g;
            ftr.Style = bitmapPointStyle1;
            t.InsertFeature(ftr);
            conn.Close();
            conn.Dispose();
        }

        #endregion


        #region StoredMapViews

        private void StoreView()
        {
            Mapping_View mapView = new Mapping_View();
            mapView.DViewScale = mapControl1.Map.Scale;
            mapView.DMapCentreX = mapControl1.Map.Center.x;
            mapView.DMapCentreY = mapControl1.Map.Center.y;
            DateTime dt = DateTime.Now;
            mapView.DtDateTime = dt;
            mapView.StrViewDescription = "Scale 1:" + mapControl1.Map.Scale.ToString("#0") + "  |  X,Y: " + mapControl1.Map.Center.x.ToString("#0") + "," + mapControl1.Map.Center.y.ToString("#0") + "  |  Time: " + dt.ToString("HH:MM:ss");

            if (intCurrentViewID > 0)  // Strip out any views after the currently loaded view before the new view is added (this is how IE works when navigating web pages. //
            {
                if (intCurrentViewID == 1)
                {

                    listStoredMapView.RemoveAt(0);  // Need this otherwise crashes in loop below when intCurrentViewID = 1 //
                }
                else
                {
                    for (int i = intCurrentViewID - 1; i >= 0; i--)
                    {
                        listStoredMapView.RemoveAt(i);
                    }
                }
            }
            listStoredMapView.Insert(0, mapView);
            intCurrentViewID = 0;

            bbiLastMapView.Enabled = (intCurrentViewID == listStoredMapView.Count - 1 ? false : true);
            bbiNextMapView.Enabled = false;
        }

        private void bciStoreViewChanges_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            boolStoreViewChanges = bciStoreViewChanges.Checked;
        }

        private void bbiStoreCurrentMapView_ItemClick(object sender, ItemClickEventArgs e)
        {
            StoreView();
        }

        private void bbiClearAllMapViews_ItemClick(object sender, ItemClickEventArgs e)
        {
            Clear_Stored_Map_Views();
        }

        private void bbiStoredViewItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            BarCheckItem bbiItem = (BarCheckItem)e.Item;
            int intPosition = Convert.ToInt32(bbiItem.Tag);
            if (intPosition > listStoredMapView.Count)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load map view - the view is no longer valid.", "Load Map view", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            boolStoreViewChangesTempDisable = true;  // Prevent the view chnage being stored as a new view change as we are moving to an already stored view. //

            intCurrentViewID = intPosition;
            Load_Stored_Map_View();
        }

        private void bbiLastMapView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (intCurrentViewID >= listStoredMapView.Count - 1) return;
            intCurrentViewID++;
            Load_Stored_Map_View();
        }

        private void bbiNextMapView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (intCurrentViewID <= 0) return;
            intCurrentViewID--;
            Load_Stored_Map_View();
        }

        private void popupStoredMapViews_Popup(object sender, EventArgs e)
        {
            bsiGoToMapView.ClearLinks();
            BarCheckItem bciStoredViewItem;
            for (int i = 0; i < listStoredMapView.Count; i++)
            {
                Mapping_View mapView = (Mapping_View)listStoredMapView[i];
                bciStoredViewItem = new BarCheckItem();
                bciStoredViewItem.Tag = Convert.ToInt32(i);
                bciStoredViewItem.Caption = mapView.StrViewDescription.ToString();
                bciStoredViewItem.Name = "iStoredViewItem";
                bsiGoToMapView.AddItem(bciStoredViewItem);
                bciStoredViewItem.ItemClick += new ItemClickEventHandler(bbiStoredViewItem_ItemClick);
                if (i == intCurrentViewID) bciStoredViewItem.Checked = true;
            }

        }

        private void Load_Stored_Map_View()
        {
            boolStoreViewChangesTempDisable = true;  // Prevent the view chnage being stored as a new view change as we are moving to an already stored view. //
            Mapping_View mapView = (Mapping_View)listStoredMapView[intCurrentViewID];

            bbiLastMapView.Enabled = (intCurrentViewID == listStoredMapView.Count - 1 ? false : true);
            bbiNextMapView.Enabled = (intCurrentViewID <= 0 ? false : true);

            MapInfo.Geometry.DPoint newPoint = new DPoint(mapView.DMapCentreX, mapView.DMapCentreY);
            mapControl1.Map.SetView(newPoint, mapControl1.Map.GetDisplayCoordSys(), mapView.DViewScale);

        }

        private void Clear_Stored_Map_Views()
        {
            listStoredMapView.Clear();
            intCurrentViewID = 0;
            bbiLastMapView.Enabled = false;
            bbiNextMapView.Enabled = false;
            StoreView();  // Store current Map View //
        }
        
        #endregion


        #region PopupContainer TreeRefStructure

        private void checkEditTreeRef1_CheckedChanged(object sender, EventArgs e)
        {
            SetTreeRefStructureControlStatus();
        }

        private void checkEditTreeRef2_CheckedChanged(object sender, EventArgs e)
        {
            SetTreeRefStructureControlStatus();
        }

        private void checkEditTreeRef3_CheckedChanged(object sender, EventArgs e)
        {
            SetTreeRefStructureControlStatus();
        }

        private void SetTreeRefStructureControlStatus()
        {
            spinEditTreeRefRemoveNumberOfChars.Enabled = checkEditTreeRef2.Checked;
            textEditTreeRefRemovePreceedingChars.Enabled = checkEditTreeRef3.Checked;
        }

        private void btnSetTreeRefStruc_Click(object sender, EventArgs e)
        {
            if (checkEditTreeRef3.Checked)
            {
                if (string.IsNullOrEmpty(textEditTreeRefRemovePreceedingChars.EditValue.ToString().Trim()))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Enter one or more chracters in the '" + checkEditTreeRef3.Text + "' box before proceeding.", "Set Tree Reference Structure", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else if (checkEditTreeRef2.Checked)
            {
                if (Convert.ToInt32(spinEditTreeRefRemoveNumberOfChars.EditValue) == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Enter the number of characters to remove from the start of the Tree References before proceeding.", "Set Tree Reference Structure", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }



            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();

        }

        private void popupContainerEditTreeRefStructure_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = popupContainerEditTreeRefStructure_Get_Selected();
        }

        private string popupContainerEditTreeRefStructure_Get_Selected()
        {
            string strReturnedValue = "";
            if (checkEditTreeRef1.Checked)
            {
                intTreeRefStructureType = 0;
                strTreeRefStructureStripPattern = "";
                intTreeRefStructureStripNumber = 0;
                strReturnedValue = checkEditTreeRef1.Text;
            }
            else if (checkEditTreeRef2.Checked)
            {
                intTreeRefStructureType = 1;
                strTreeRefStructureStripPattern = "";
                intTreeRefStructureStripNumber = Convert.ToInt32(spinEditTreeRefRemoveNumberOfChars.EditValue);
                strReturnedValue = checkEditTreeRef2.Text + " " + spinEditTreeRefRemoveNumberOfChars.EditValue.ToString() + " character(s)";
            }
            else  // checkEditTreeRef3.Checked //
            {
                intTreeRefStructureType = 2;
                strTreeRefStructureStripPattern = textEditTreeRefRemovePreceedingChars.EditValue.ToString();
                intTreeRefStructureStripNumber = 0;
                strReturnedValue = checkEditTreeRef3.Text + " " + textEditTreeRefRemovePreceedingChars.EditValue.ToString();
            }
            return strReturnedValue;
        }


        #endregion


        private void CustomQueryInfo(MapInfo.Tools.ToolUsedEventArgs e)
        {
            if (boolQueryToolInUse)
            {
                boolQueryToolInUse = false;
                return;
            }
            else
            {
                boolQueryToolInUse = true;
                WaitDialogForm loading = new WaitDialogForm("Checking for Object Properties...", "WoodPlan Mapping");
                loading.Show();

                MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();
                connection.Open();

                // This is the point at which the user clicked on the map //
                MapInfo.Geometry.Point pt = new MapInfo.Geometry.Point(this.mapControl1.Map.GetDisplayCoordSys(), e.MapCoordinate.x, e.MapCoordinate.y);

                bool boolObjectFound = false;
                VGridControl vg = new VGridControl();

                for (int i = 0; i < this.mapControl1.Map.Layers.Count; i++)
                {
                    if (this.mapControl1.Map.Layers[i].Type != LayerType.Normal) continue;  // Only process vector [Normal type] Layers //
                    FeatureLayer fl = this.mapControl1.Map.Layers[i] as FeatureLayer;
                    if (!MapInfo.Mapping.LayerHelper.GetSelectable(fl)) continue;  // Only process selectable layers //

                    if ((fl.VisibleRangeEnabled == true & fl.VisibleRange.Within(this.mapControl1.Map.Zoom)) || (fl.VisibleRangeEnabled == false))  // if the layer is visible within the current map //             
                    {
                        MapInfo.Data.MICommand command = connection.CreateCommand();
                        command.CommandText = "Select * from " + fl.Table.Alias + " where @pt intersects Obj";

                        MapInfo.Data.GeometryColumn geoCol = null;
                        for (int j = 0; j < fl.Table.TableInfo.Columns.Count; j++)
                        {
                            Column col = fl.Table.TableInfo.Columns[j];
                            if (col.DataType == MIDbType.FeatureGeometry)
                            {
                                geoCol = (GeometryColumn)col;
                                break;
                            }
                        }
                        if (geoCol == null) continue;   // Unable to get the geometry column so abort processing this layer //

                        if (geoCol.PredominantGeometryType.ToString() == "MultiCurve" || geoCol.PredominantGeometryType.ToString() == "Curve")
                        {
                            //Checks to see if the table is a table of MultiCurves or Curves. We will use a buffer to search for these features
                            command.Parameters.Add("@pt", pt.Buffer(this.mapControl1.Map.Zoom.Value / 500, MapInfo.Geometry.DistanceUnit.Mile, 99));
                        }
                        else if (geoCol.PredominantGeometryType.ToString() == "Point" || geoCol.PredominantGeometryType.ToString() == "MultiPoint")
                        {
                            //Checks to see if the table is a tabl of Points or MultiPoints. We will use a buffer for these features as well.
                            command.Parameters.Add("@pt", pt.Buffer(this.mapControl1.Map.Zoom.Value / 200, MapInfo.Geometry.DistanceUnit.Mile, 99));
                        }
                        else
                        {
                            command.Parameters.Add("@pt", pt);
                        }

                        MapInfo.Data.IResultSetFeatureCollection irfc = command.ExecuteFeatureCollection();
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            boolObjectFound = true;
                            CategoryRow cRow = new CategoryRow("Layer: " + fl.Alias);  // Create VerticalGrid Category Row //
                            foreach (Column col in f.Columns)
                            {
                                EditorRow eRow = new EditorRow();
                                eRow.Properties.Caption = col.ToString();
                                eRow.Properties.Value = f[col.ToString()].ToString();
                                eRow.Properties.ReadOnly = true;
                                eRow.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
                                cRow.ChildRows.Add(eRow);
                            }
                            vg.Rows.Add(cRow);
                        }
                        command.Dispose();
                        irfc.Close();
                    }
                }
                connection.Close();
                loading.Close();
                if (boolObjectFound)
                {
                    frm_AT_Mapping_Generic_Query_Tool frm_query = new frm_AT_Mapping_Generic_Query_Tool();
                    frm_query.FormID = 20049;
                    frm_query.GlobalSettings = this.GlobalSettings;
                    frm_query.FormPermissions = this.FormPermissions;
                    //frm_query.fProgress = fProgress;
                    frm_query.vg = vg;
                    frm_query.ShowDialog();
                }
            }
        }

        public void RefreshMapObjects(string strMapIDs, int IDType)
        {
            // Check if an outstanding call from a child Tree \ Inspection \ Action Edit screen has notified the form for update on this form activating. //
            // If yes, clear the flag if no new records need to be highlighted as the rest of the event will take care of refreshing the map. //
            if (UpdateRefreshStatus > 0 && i_str_AddedRecordIDs1 == "" && i_str_AddedRecordIDs2 == "" && i_str_AddedRecordIDs3 == "") UpdateRefreshStatus = 0;

            // Get Amenity Trees Updated Objects //
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp01313_AT_Tree_Picker_load_updated_objects", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@MapObjectIDs", strMapIDs));
            cmd.Parameters.Add(new SqlParameter("@IDType", IDType));
            SqlDataAdapter sdaUpdatedMapObjects = new SqlDataAdapter();
            DataSet dsUpdatedMapObjects = new DataSet("NewDataSet");
            dsUpdatedMapObjects.Clear();  // Remove any old values first //
            sdaUpdatedMapObjects = new SqlDataAdapter(cmd);
            sdaUpdatedMapObjects.Fill(dsUpdatedMapObjects, "Table");

            // Get Assets Updated Objects //
            cmd = null;
            cmd = new SqlCommand("sp03070_EP_Tree_Picker_load_updated_asset_objects", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@MapObjectIDs", strMapIDs));
            cmd.Parameters.Add(new SqlParameter("@IDType", IDType));
            SqlDataAdapter sdaUpdatedMapObjects2 = new SqlDataAdapter();
            DataSet dsUpdatedMapObjects2 = new DataSet("NewDataSet");
            dsUpdatedMapObjects2.Clear();  // Remove any old values first //
            sdaUpdatedMapObjects2 = new SqlDataAdapter(cmd);
            sdaUpdatedMapObjects2.Fill(dsUpdatedMapObjects2, "Table");

            if (dsUpdatedMapObjects.Tables[0].Rows.Count <= 0 && dsUpdatedMapObjects2.Tables[0].Rows.Count <= 0 && strMapIDs.Length <= 0) return;  // Abort if no rows and Passed IDs is blank - Note: no rows if a map delete has occurred (but the passed IDs should have the map IDs of the deleted objects) otherwise abort process. //

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
            mapControl1.SuspendLayout();  // Switch of Map Redraw until done //

            string[] strArray;
            string strID = "";
            char[] delimiters; // Value set to correct value later //
            int intDeletedObjectCount = 0;
            int intRowHandle = 0;
            GridView view = (GridView)gridControl2.MainView;
            GridView view2 = (GridView)gridControlAssetObjects.MainView;

            // Clear Selection //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            selection.Clear();
            string strSelectionText = "";

            if (dsUpdatedMapObjects.Tables[0].Rows.Count > 0 || dsUpdatedMapObjects2.Tables[0].Rows.Count > 0)
            {
                // Update Changed Amenity Tree Objects //
                if (dsUpdatedMapObjects.Tables[0].Rows.Count > 0)  // Map Objects need updating on the map so delete them first then re-add them with appropriate styling //
                {
                    view.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
                    view.ClearSelection();  // Clear any selected rows (rows with blue highlighter) - Updated rows will be selected below //

                    // Prepare Thematic Styling //
                    Clear_MapInfo_Themes();  // Clear Any MapInfo Thematics already defined - these will be created later if required //
                    int intPicklistValue = 0;
                    string strThematicsGridViewColumn = "";
                    CompositeStyle style = new CompositeStyle();
                    if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)
                    {
                        strThematicsGridViewColumn = Get_ThematicField_ColumnName(intThematicStylingBasePicklistID);
                    }
                    else // No Thematics so get default style //
                    {
                        //style = Get_Default_Style();
                        strThematicsGridViewColumn = "intSpeciesID";
                    }
                    style = Get_Default_Style();
                    // End of Prepare Thematics //

                    delimiters = new char[] { ';' };
                    int intTreeID = 0;
                    string strPolygonXY = "";
                    int intCrownDiameter = 0;
                    int iPoint = 0;
                    bool boolHighlighted = false;
                    Color colorHighlight = (System.Drawing.Color)colorEditHighlight.EditValue;
                    string strCurrentValue = "";
                    foreach (DataRow dr in dsUpdatedMapObjects.Tables[0].Rows)
                    {
                        intTreeID = Convert.ToInt32(dr["TreeID"]);  // Get ID of record for current row //
                        // Search Map Objects grid to check if ID is present. If yes, remove it (it will be added later in this process) //
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["TreeID"];
                        intRowHandle = view.LocateByValue(0, col, intTreeID);
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            boolHighlighted = (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight")) == 1 ? true : false);
                            strSelectionText += "id='" + Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID")) + "' or ";
                            view.DeleteRow(intRowHandle);

                            // Clear object from map //
                            MICommand MapCmd = conn.CreateCommand();
                            MapCmd.CommandText = "Delete From MapObjects Where TreeID = " + intTreeID.ToString();
                            MapCmd.Prepare();
                            intDeletedObjectCount += MapCmd.ExecuteNonQuery();
                            MapCmd.Dispose();
                            // End of Clear object from map //
                        }
                        dataSet_AT_TreePicker.sp01252_AT_Tree_Picker_load_object_manager.Rows.Add(dr.ItemArray);  // Add the new row into the DataSet so it is shown in the grid //               

                        // Get the new row in the grid and set it's displayed checkbox to 1 //
                        intRowHandle = view.LocateByValue(0, col, intTreeID);
                        if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) return;
                        view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);

                        view.SelectRow(intRowHandle);

                        if (boolHighlighted) view.SetRowCellValue(intRowHandle, "Highlight", 1);  // Set highlighted ticked if required //

                        view.MakeRowVisible(intRowHandle, false);

                        // Update Map //
                        if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)  // Get Thematic Style
                        {
                            intPicklistValue = Convert.ToInt32(view.GetRowCellValue(intRowHandle, strThematicsGridViewColumn));
                            if (intMapObjectTransparency <= 0) style = Get_Thematic_Style(intPicklistValue, Convert.ToDecimal(view.GetRowCellValue(intRowHandle, strThematicsGridViewColumn)));  // Pass value as both int and decimal and let function sort it out //
                        }

                        if (view.GetRowCellValue(intRowHandle, "ObjectType").ToString() == "Point")  // Load Point //
                        {
                            intCrownDiameter = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CrownDiameter"));
                            MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                            f.Geometry = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(view.GetRowCellValue(intRowHandle, "XCoordinate")), Convert.ToDouble(view.GetRowCellValue(intRowHandle, "YCoordinate")));
                            f["CalculatedSize"] = "";
                            f["CalculatedPerimeter"] = "";
                            f["intObjectType"] = 0;  //0 = Point //
                            f["id"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID"));
                            f["TreeID"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "TreeID"));
                            f["TreeRef"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference"));
                            f["ClientName"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientName"));
                            f["SiteName"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteName"));
                            f["Species"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "Species"));
                            f["CrownDiameter"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CrownDiameter"));
                            f["intHighlighted"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight"));
                            f["RiskFactor"] = Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "RiskFactor"));
                            f["ModuleID"] = 1;  // 1 = Amenity Trees, 2 = Assets //
                            f["Thematics"] = (Convert.ToInt32(f["intHighlighted"]) == 0 ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, strThematicsGridViewColumn)) : Convert.ToDouble(999995));
                            switch (intTreeRefStructureType)
                            {
                                case 0:
                                    f["ShortTreeRef"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference"));
                                    break;
                                case 1:
                                    strCurrentValue = Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference"));
                                    f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                    break;
                                case 2:
                                    strCurrentValue = Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference"));  // Note that the second commented out method also works //
                                    f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                    //string[] str1 = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Split(new string[] { strTreeRefStructureStripPattern }, 2, StringSplitOptions.None);
                                    //f["ShortTreeRef"] = (str1.Length == 1 ? str1[0] : str1[1]);
                                    break;
                                default:
                                    break;
                            }
                            if (intMapObjectSizing == -1)  // Dynamic Sizing according to Crown Diameter //
                            {
                                style.SymbolStyle.PointSize = Calculate_Point_Size(f);
                            }
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight")) == 1) JustHighlight(ref style, colorHighlight);  // Pass feature by ref so it is updated with highlight colour //
                            f.Style = style;
                            MapInfo.Data.Key k = t.InsertFeature(f);
                        }
                        else  // Polygon / Polyline //
                        {
                            strID = Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID"));
                            strPolygonXY = Convert.ToString(view.GetRowCellValue(intRowHandle, "PolygonXY"));

                            // Check there is an even number of points //
                            strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray.Length % 2 != 0)  // Modulus (%) //
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Invalid Polygon\\Polyline in database - MapID: " + strID + ".\r\nInvalid Number of Coordinates - Object ignored!", "Load Map Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            else
                            {
                                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                                iPoint = 0;
                                for (int j = 0; j < strArray.Length - 1; j++)
                                {
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                                    iPoint++;
                                }
                                MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                                    f["intObjectType"] = 1;  // 1 = Polygon //
                                    view.SetRowCellValue(intRowHandle, "ObjectType", "Polygon");
                                }
                                else
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["intObjectType"] = 2;  // 2 = PolyLine //
                                    view.SetRowCellValue(intRowHandle, "ObjectType", "Polyline");
                                }
                                f["id"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID"));
                                f["TreeID"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "TreeID"));
                                f["TreeRef"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference"));
                                f["ClientName"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientName"));
                                f["SiteName"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteName"));
                                f["Species"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "Species"));
                                f["CrownDiameter"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CrownDiameter"));
                                f["intHighlighted"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight"));
                                f["RiskFactor"] = Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "RiskFactor"));
                                f["ModuleID"] = 1;  // 1 = Amenity Trees, 2 = Assets //
                                f["Thematics"] = (Convert.ToInt32(f["intHighlighted"]) == 0 ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, strThematicsGridViewColumn)) : Convert.ToDouble(999995));
                                switch (intTreeRefStructureType)
                                {
                                    case 0:
                                        f["ShortTreeRef"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference"));
                                        break;
                                    case 1:
                                        strCurrentValue = Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference"));
                                        f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                        break;
                                    case 2:
                                        strCurrentValue = Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference"));  // Note that the second commented out method also works //
                                        f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                        //string[] str1 = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Split(new string[] { strTreeRefStructureStripPattern }, 2, StringSplitOptions.None);
                                        //f["ShortTreeRef"] = (str1.Length == 1 ? str1[0] : str1[1]);
                                        break;
                                    default:
                                        break;
                                }
                                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight")) == 1) JustHighlight(ref style, colorHighlight);  // Pass feature by ref so it is updated with highlight colour //
                                f.Style = style;
                                MapInfo.Data.Key k = t.InsertFeature(f);
                            }
                        }
                    }
                    view.EndSelection();
                    view.Invalidate();
                }
                // Update Changed Asset Objects //
                if (dsUpdatedMapObjects2.Tables[0].Rows.Count > 0)  // Map Objects need updating on the map so delete them first then re-add them with appropriate styling //
                {
                    view2.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
                    view2.ClearSelection();  // Clear any selected rows (rows with blue highlighter) - Updated rows will be selected below //
                    
                    /*
                    // Prepare Thematic Styling //
                    int intPicklistValue = 0;
                    string strThematicsGridViewColumn = "";
                    CompositeStyle style = new CompositeStyle();
                    if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)
                    {
                        strThematicsGridViewColumn = Get_ThematicField_ColumnName(intThematicStylingBasePicklistID);
                    }
                    else // No Thematics so get default style //
                    {
                        style = Get_Default_Style();
                    }
                    // End of Prepare Thematics //
                    */
                    CompositeStyle style = new CompositeStyle();
                    style = Get_Default_Style();
 
                    delimiters = new char[] { ';' };
                    int intAssetID = 0;
                    string strPolygonXY = "";
                    int intCrownDiameter = 0;
                    int iPoint = 0;
                    bool boolHighlighted = false;
                    Color colorHighlight = (System.Drawing.Color)colorEditHighlight.EditValue;
                    foreach (DataRow dr in dsUpdatedMapObjects2.Tables[0].Rows)
                    {
                        intAssetID = Convert.ToInt32(dr["AssetID"]);  // Get ID of record for current row //
                        // Search Map Objects grid to check if ID is present. If yes, remove it (it will be added later in this process) //
                        DevExpress.XtraGrid.Columns.GridColumn col = view2.Columns["AssetID"];
                        intRowHandle = view2.LocateByValue(0, col, intAssetID);
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            boolHighlighted = (Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight")) == 1 ? true : false);
                            strSelectionText += "id='" + Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID")) + "' or ";
                            view2.DeleteRow(intRowHandle);

                            // Clear object from map //
                            MICommand MapCmd = conn.CreateCommand();
                            MapCmd.CommandText = "Delete From MapObjects Where AssetID = " + intAssetID.ToString();
                            MapCmd.Prepare();
                            intDeletedObjectCount += MapCmd.ExecuteNonQuery();
                            MapCmd.Dispose();
                            // End of Clear object from map //
                        }
                        dataSet_AT_TreePicker.sp03069_Tree_Picker_Asset_Objects_List.Rows.Add(dr.ItemArray);  // Add the new row into the DataSet so it is shown in the grid //               

                        // Get the new row in the grid and set it's displayed checkbox to 1 //
                        intRowHandle = view2.LocateByValue(0, col, intAssetID);
                        if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) return;
                        view2.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);

                        view2.SelectRow(intRowHandle);

                        if (boolHighlighted) view2.SetRowCellValue(intRowHandle, "Highlight", 1);  // Set highlighted ticked if required //

                        view2.MakeRowVisible(intRowHandle, false);

                        // Update Map //
                        /*if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)  // Get Thematic Style
                        {
                            intPicklistValue = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, strThematicsGridViewColumn));
                            style = Get_Thematic_Style(intPicklistValue, Convert.ToDecimal(view2.GetRowCellValue(intRowHandle, strThematicsGridViewColumn)));  // Pass value as both int and decimal and let function sort it out //
                        }*/

                        if (view2.GetRowCellValue(intRowHandle, "ObjectType").ToString() == "Point")  // Load Point //
                        {
                            intCrownDiameter = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "CrownDiameter"));
                            MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                            f.Geometry = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(view2.GetRowCellValue(intRowHandle, "XCoordinate")), Convert.ToDouble(view2.GetRowCellValue(intRowHandle, "YCoordinate")));
                            f["CalculatedSize"] = "";
                            f["CalculatedPerimeter"] = "";
                            f["intObjectType"] = 0;  //0 = Point //
                            f["id"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID"));
                            f["AssetID"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "AssetID"));
                            f["AssetNumber"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetNumber"));
                            f["PartNumber"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "PartNumber"));
                            f["ModelNumber"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "ModelNumber"));
                            f["SerialNumber"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SerialNumber"));
                            f["ClientName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "ClientName"));
                            f["ClientCode"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "ClientCode"));
                            f["SiteName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SiteName"));
                            f["SiteCode"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SiteCode"));
                            f["AssetType"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetTypeDescription"));
                            f["AssetSubType"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetSubTypeDescription"));
                            f["intHighlighted"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight"));
                            f["ModuleID"] = 2;  // 1 = Amenity Trees, 2 = Assets //
                            f["CrownDiameter"] = 0;
                            f["Thematics"] = (Convert.ToInt32(f["intHighlighted"]) == 0 ? Convert.ToDouble(0.00) : Convert.ToDouble(999995));

                            // Following populated so object labelling will work //
                            f["TreeRef"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetNumber"));
                            f["ShortTreeRef"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetNumber"));
                            f["ClientName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "ClientName"));
                            f["SiteName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SiteName"));
                            f["Species"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetTypeDescription"));
                            /*
                            switch (intTreeRefStructureType)
                            {
                                case 0:
                                    f["ShortTreeRef"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReference"));
                                    break;
                                case 1:
                                    strCurrentValue = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReference"));
                                    f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                    break;
                                case 2:
                                    strCurrentValue = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReference"));  // Note that the second commented out method also works //
                                    f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                    //string[] str1 = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Split(new string[] { strTreeRefStructureStripPattern }, 2, StringSplitOptions.None);
                                    //f["ShortTreeRef"] = (str1.Length == 1 ? str1[0] : str1[1]);
                                    break;
                                default:
                                    break;
                            }
                            if (intMapObjectSizing == -1)  // Dynamic Sizing according to Crown Diameter //
                            {
                                style.SymbolStyle.PointSize = Calculate_Point_Size(f);
                            }
                            */
                            if (Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight")) == 1) JustHighlight(ref style, colorHighlight);  // Pass feature by ref so it is updated with highlight colour //
                            f.Style = style;
                            MapInfo.Data.Key k = t.InsertFeature(f);
                        }
                        else  // Polygon / Polyline //
                        {
                            strID = Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID"));
                            strPolygonXY = Convert.ToString(view2.GetRowCellValue(intRowHandle, "PolygonXY"));

                            // Check there is an even number of points //
                            strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray.Length % 2 != 0)  // Modulus (%) //
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Invalid Polygon\\Polyline in database - MapID: " + strID + ".\r\nInvalid Number of Coordinates - Object ignored!", "Load Map Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            else
                            {
                                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                                iPoint = 0;
                                for (int j = 0; j < strArray.Length - 1; j++)
                                {
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                                    iPoint++;
                                }
                                MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                                    f["intObjectType"] = 1;  // 1 = Polygon //
                                    view2.SetRowCellValue(intRowHandle, "ObjectType", "Polygon");
                                }
                                else
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["intObjectType"] = 2;  // 2 = PolyLine //
                                    view2.SetRowCellValue(intRowHandle, "ObjectType", "Polyline");
                                }
                                f["id"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID"));
                                f["AssetID"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "AssetID"));
                                f["AssetNumber"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetNumber"));
                                f["PartNumber"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "PartNumber"));
                                f["ModelNumber"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "ModelNumber"));
                                f["SerialNumber"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SerialNumber"));
                                f["ClientName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "ClientName"));
                                f["ClientCode"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "ClientCode"));
                                f["SiteName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SiteName"));
                                f["SiteCode"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SiteCode"));
                                f["AssetType"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetTypeDescription"));
                                f["AssetSubType"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetSubTypeDescription"));
                                f["intHighlighted"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight"));
                                f["ModuleID"] = 2;  // 1 = Amenity Trees, 2 = Assets //
                                f["CrownDiameter"] = 0;
                                f["Thematics"] = (Convert.ToInt32(f["intHighlighted"]) == 0 ? Convert.ToDouble(0.00) : Convert.ToDouble(999995));

                                // Following populated so object labelling will work //
                                f["TreeRef"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetNumber"));
                                f["ShortTreeRef"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetNumber"));
                                f["ClientName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "ClientName"));
                                f["SiteName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SiteName"));
                                f["Species"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "AssetTypeDescription"));
                                /*
                                switch (intTreeRefStructureType)
                                {
                                    case 0:
                                        f["ShortTreeRef"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReference"));
                                        break;
                                    case 1:
                                        strCurrentValue = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReference"));
                                        f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                        break;
                                    case 2:
                                        strCurrentValue = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReference"));  // Note that the second commented out method also works //
                                        f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                        //string[] str1 = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Split(new string[] { strTreeRefStructureStripPattern }, 2, StringSplitOptions.None);
                                        //f["ShortTreeRef"] = (str1.Length == 1 ? str1[0] : str1[1]);
                                        break;
                                    default:
                                        break;
                                }*/
                                if (Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight")) == 1) JustHighlight(ref style, colorHighlight);  // Pass feature by ref so it is updated with highlight colour //
                                f.Style = style;
                                MapInfo.Data.Key k = t.InsertFeature(f);
                            }
                        }
                    }
                    view2.EndSelection();
                    view2.Invalidate();
                }

                // Re-apply Selected Objects to map //
                if (!string.IsNullOrEmpty(strSelectionText))
                {
                    strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                    MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
                    selection.Add(irfc);  // Add the whole feature set to the default selection //
                }
                if (intMapObjectTransparency > 0) Create_MapInfo_Theme();  // Transparency on, so we need to create a Map Infor Them to handle thematics //
            }
            else  // No Map Objects need updating on the map (map objects have been deleted so remove them from the map // //
            {
                delimiters = new char[] { ',' };
                strArray = strMapIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i <= strArray.Length - 1; i++)
                {
                    strID = strArray[i].ToString();  // Get Map ID //
                    // Search Map Objects grid to check if ID is present. If yes, remove it //
                    DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["MapID"];
                    intRowHandle = view.LocateByValue(0, col, strID);
                    if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)  // Check Amenity Trees Object Grid First //
                    {
                        view.DeleteRow(intRowHandle);

                        // Clear object from map //
                        MICommand MapCmd = conn.CreateCommand();
                        MapCmd.CommandText = "Delete From MapObjects Where id = '" + strID + "'";
                        MapCmd.Prepare();
                        intDeletedObjectCount += MapCmd.ExecuteNonQuery();
                        MapCmd.Dispose();
                        // End of Clear object from map //
                    }
                    else  // Object was not found so check Assets Object Grid //
                    {
                        col = view2.Columns["MapID"];
                        intRowHandle = view2.LocateByValue(0, col, strID);
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            view2.DeleteRow(intRowHandle);

                            // Clear object from map //
                            MICommand MapCmd = conn.CreateCommand();
                            MapCmd.CommandText = "Delete From MapObjects Where id = '" + strID + "'";
                            MapCmd.Prepare();
                            intDeletedObjectCount += MapCmd.ExecuteNonQuery();
                            MapCmd.Dispose();
                            // End of Clear object from map //
                        }
                    }
                }
            }
            if (intDeletedObjectCount > 0) t.Pack(MapInfo.Data.PackType.All);  // Pack the map objects table //
            conn.Close();
            conn.Dispose();

            mapControl1.Invalidate();
            mapControl1.Refresh();
            mapControl1.ResumeLayout(true);  // Switch back on Map Redrawing //
        }

        public void UpdateMapAfterChangesFromWoodplanVersion4(int intParameter)
        {
            // Get the WoodPlan V.4 passed parameter string from the Windows registry //
            string strRegistryValue = "";
            Microsoft.Win32.RegistryKey registry = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\\WoodPlan");
            if (registry == null) return;
            strRegistryValue = Convert.ToString(registry.GetValue("V4toV5ParameterString"));
            registry.Close();
            if (string.IsNullOrEmpty(strRegistryValue)) return;

            switch (intParameter)
            {
                case 1:  // New point record added //
                case 2:  // New polygon record added //
                case 3:  // Point / polygon record deleted //
                    //MessageBox.Show("Message received: Add - " + strRegistryValue);
                    RefreshMapObjects(strRegistryValue, 0);
                    break;
                default:
                    break;
            }
        }

        private bool NotifyWoodPlanVersion4(string strAction, string strPassedParameterString)
        {
            // ***** NO LONGER USED ***** //                    
            // Call WoodPlan V.4 for the data entry screens //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strExePath = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesVersion4WoodPlan_Incident_Link_Exe"));
            if (string.IsNullOrEmpty(strExePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to run the WoodPlan V.4 Link - The path to the WoodPlan V.4 Link Exe File specified in the System Settings table is missing.\n\nEnter the Folder Location of the WoodPlan V.4 Link File in the System Options screen before proceeding.", "WoodPlan V.4 Link", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            string strWoodPlanParameters = "";
            if (strAction.ToLower() != "workorder")  // Normal TreePicker Type Action - Adding a Tree, Hedge or Tree Group //
            {
                strExePath += "\\woodplan_incident_link.exe";
                strWoodPlanParameters = "'" + strAction + strPassedParameterString + "'";
            }
            else  // Add Action to Work Order //
            {
                strExePath += "\\woodplan_workorder_link.exe";
                strWoodPlanParameters = "'" + strPassedParameterString + "'";
            }
            if (!System.IO.File.Exists(strExePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to run the WoodPlan V.4 Link - The path to the WoodPlan V.4 Link Exe File specified in the System Settings table is incorrect.\n\nCorrect the Folder Location of the WoodPlan V.4 Link File in the System Options screen before proceeding.", "WoodPlan V.4 Link", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            try
            {
                System.Diagnostics.Process Proc = new System.Diagnostics.Process();
                //Proc.StartInfo.CreateNoWindow = true;  // Hide DOS Window [This line and next]//
                Proc.StartInfo.UseShellExecute = false;
                //Proc.StartInfo.RedirectStandardError = true;
                Proc.StartInfo.FileName = strExePath;
                Proc.StartInfo.Arguments = strWoodPlanParameters;

                Proc.Start();
                //Proc.WaitForExit(300000);

                //string error = Proc.StandardError.ReadToEnd();
                //Proc.WaitForExit(10000);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to run the WoodPlan V.4 Link - An error occurred [" + ex.Message + "].", "WoodPlan V.4 Link", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }

            return true;
        }

        private int AddObjectToMap_OLD(MapInfo.Geometry.FeatureGeometry PassedFeature)
        {
            // ***** NO LONGER USED ***** //
            MapInfo.Geometry.FeatureGeometry g = PassedFeature;

            CompositeStyle style = new CompositeStyle();
            style = Get_Default_Style();  // *** Get Styling *** //

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();  // create connection and open table //
            conn.Open();
            MapInfo.Data.Table tab1 = conn.Catalog.GetTable("MapObjects");
            MapInfo.Data.Feature ftr = new MapInfo.Data.Feature(tab1.TableInfo.Columns);  // make a blank feature based on the table's structure //
            ftr.Geometry = PassedFeature;  // assign the memory address of the passed geometry to the new features Geometry property //

            int intPreviousX = -99999;
            int intPreviousY = -99999;
            int intCurrentX = 0;
            int intCurrentY = 0;
            string strPassedParameterString = "";
            string strAction = "";

            MapInfo.Geometry.DPoint[] dPoints = null;  // Used for adjusted area/length calculation //
            int iPoint = 0;  // Used for adjusted area / length calculation //
            MapInfo.Geometry.FeatureGeometry tempFeatureGeometry = null; // Used for adjusted area / length calculation //

            switch (PassedFeature.Type)
            {
                case GeometryType.MultiPolygon:
                    MapInfo.Geometry.MultiPolygon mpoly = ftr.Geometry as MapInfo.Geometry.MultiPolygon;
                    foreach (MapInfo.Geometry.Polygon poly in mpoly)
                    {
                        foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                        {
                            dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length]; // Used for adjusted area calculation //
                            foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                            {
                                intCurrentX = Convert.ToInt32(pt.x);
                                intCurrentY = Convert.ToInt32(pt.y);
                                if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                                {
                                    strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                    intPreviousX = intCurrentX;
                                    intPreviousY = intCurrentY;
                                }
                                dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(intCurrentX), Convert.ToDouble(intCurrentY));  // Used for adjusted area calculation //
                                iPoint++;  // Used for adjusted area calculation //
                            }
                        }
                        // Get Polygon Area - Need to create a new feature geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                        tempFeatureGeometry = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                        strAction = "new gp v5;" + string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)tempFeatureGeometry).Area(AreaUnit.SquareMeter)) + ";" + string.Format("{0:N2}", tempFeatureGeometry.Centroid.x) + ";" + string.Format("{0:N2}", tempFeatureGeometry.Centroid.y) + ";polygon;";

                        ftr["intObjectType"] = 1;  // 0 = Point, 1 = Polygon, 2 = Line //
                        ftr["intHighlighted"] = 0;
                        ftr["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)ftr.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                        ftr["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)ftr.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                    }
                    break;
                case GeometryType.MultiCurve:  // PolyLine //
                    MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)ftr.Geometry;
                    MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                    MapInfo.Geometry.DPoint[] dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //

                    dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length]; // Used for adjusted area / length calculation //
                    //step through the array and print the coordinates to the debug window
                    for (int i = 0; i < dptArr.Length; i++)
                    {
                        intCurrentX = Convert.ToInt32(dptArr[i].x);
                        intCurrentY = Convert.ToInt32(dptArr[i].y);
                        if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                        {
                            strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                            intPreviousX = intCurrentX;
                            intPreviousY = intCurrentY;
                        }
                        dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(intCurrentX), Convert.ToDouble(intCurrentY));  // Used for adjusted length calculation //
                        iPoint++;  // Used for adjusted length calculation //                           
                    }
                    // Get Polyline Length - Need to create a new fetaure geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                    tempFeatureGeometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                    //strAction = "new group;";  // No Area Passed with this one //
                    strAction = "new gp v5;" + string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + ";" + string.Format("{0:N2}", tempFeatureGeometry.Centroid.x) + ";" + string.Format("{0:N2}", tempFeatureGeometry.Centroid.y) + ";polyline;";

                    ftr["intObjectType"] = 2;  // 0 = Point, 1 = Polygon, 2 = Line //
                    ftr["intHighlighted"] = 0;
                    ftr["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                    ftr["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                    break;
                case GeometryType.MultiPoint:
                    break;
                case GeometryType.Point:
                    strPassedParameterString += Convert.ToInt32(ftr.Geometry.Centroid.x).ToString() + ";" + Convert.ToInt32(ftr.Geometry.Centroid.y).ToString() + ";";
                    strAction = "new;";
                    ftr["intObjectType"] = 0;  // 0 = Point, 1 = Polygon, 2 = Line //
                    ftr["intHighlighted"] = 0;
                    ftr["CalculatedSize"] = "";
                    ftr["CalculatedPerimeter"] = "";
                    break;
                default:
                    return 0;
            }

            if (strPassedParameterString.Length > intStrPolygonXYMaxLength)  // Trap anything longer that 255 characters otherwise GBM Mobile Export can't handle it //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add object - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.\n\nTry plotting the object again but ensure there are less vertices.", "Add Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }

            bool boolSucceed = NotifyWoodPlanVersion4(strAction, strPassedParameterString);  // Call WoodPlan V.4 for Edit screens //
            if (boolSucceed)
            {
                // Assign data to the feature... *** Once Data Entry screens are created, pick up the following data from them, add object into GridControl2 and get correct themtic styling if switched on. *** //             
                ftr["TREEREF"] = "Mark Test";
                ftr["SPECIES"] = "Scots Pine";
                ftr["ID"] = "1234567890";
                ftr["CrownDiameter"] = 0;  // *** NEED TO GET THIS ONCE USER CHANGES IT in data entry screen *** //                      
                ftr["Thematics"] = Convert.ToDouble(0.00);
                ftr.Style = style;  // ***  NEED TO FIRE DYNAMIC SIZING CODE IF SWITCHED ON THEN UPDATE STYLE'S SIZE *** //

                // ***** FOLLOWING LINE NO LONGER REQUIRED AT THE ONJECT IS ADDED AFTER WOODPLAN V4 HAS FINISHED WITH IT ***** //
                //MapInfo.Data.Key ftrKey = tab1.InsertFeature(ftr);
            }
            /*
             // Folloing Code Prevents and MapLabel from being generated //
            foreach (MapInfo.Mapping.LabelLayer lbllyr in mapControl1.Map.Layers.GetMapLayerEnumerator(new MapInfo.Mapping.FilterByLayerType(MapInfo.Mapping.LayerType.Label)))
            {
                // LOOP OVER EACH LABELS SOURCE //
                foreach (MapInfo.Mapping.LabelSource lblsrc in lbllyr.Sources)
                {
                    MapInfo.Mapping.LabelSource lblSource = new MapInfo.Mapping.LabelSource(tab1, "LabelSource");
                    MapInfo.Mapping.LabelProperties lp = new MapInfo.Mapping.LabelProperties();

                    LabelProperties properties = new LabelProperties();
                    properties.Attributes = LabelAttribute.VisibilityEnabled | LabelAttribute.Caption | LabelAttribute.CalloutLineUse;
                    properties.Visibility.Enabled = false;
                    properties.Caption = "''";
                    properties.CalloutLine.Use = false;

                    // Add the properties to a new selection label modifier and add the modifier to the label source //
                    SelectionLabelModifier modifier = new SelectionLabelModifier();
                    modifier.Properties.Add(ftrKey, properties);
                    //lbllyr.Sources[0].Modifiers..Append(modifier);
                    lblsrc.Modifiers.Append(modifier);
                }
            }
            */
            conn.Close();
            conn.Dispose();
            return 1;
        }

        private int AddObjectToMap(MapInfo.Geometry.FeatureGeometry PassedFeature)
        {
            MapInfo.Geometry.FeatureGeometry g = PassedFeature;

            CompositeStyle style = new CompositeStyle();
            style = Get_Default_Style();  // *** Get Styling *** //

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();  // create connection and open table //
            conn.Open();
            MapInfo.Data.Table tab1 = conn.Catalog.GetTable("MapObjects");
            MapInfo.Data.Feature ftr = new MapInfo.Data.Feature(tab1.TableInfo.Columns);  // make a blank feature based on the table's structure //
            ftr.Geometry = PassedFeature;  // assign the memory address of the passed geometry to the new features Geometry property //

            int intPreviousX = -99999;
            int intPreviousY = -99999;
            int intCurrentX = 0;
            int intCurrentY = 0;

            int intObjectType = -1;
            int intX = 0;
            int intY = 0;
            string strPolygonXY = "";
            decimal decArea = (decimal)0.00;
            decimal decLength = (decimal)0.00;
            decimal decWidth = (decimal)0.00;

            float flPreviousX = (float)-99999;
            float flPreviousY = (float)-99999;
            float flLatLongX = (float)0.00;
            float flLatLongY = (float)0.00;
            string strLatLongPolygonXY = "";

            MapInfo.Geometry.DPoint[] dPoints = null;  // Used for adjusted area/length calculation //
            int iPoint = 0;  // Used for adjusted area / length calculation //
            MapInfo.Geometry.FeatureGeometry tempFeatureGeometry = null; // Used for adjusted area / length calculation //

            switch (PassedFeature.Type)
            {
                case GeometryType.MultiPolygon:
                    {
                        MapInfo.Geometry.MultiPolygon mpoly = ftr.Geometry as MapInfo.Geometry.MultiPolygon;
                        foreach (MapInfo.Geometry.Polygon poly in mpoly)
                        {
                            foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                            {
                                dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length]; // Used for adjusted area calculation //
                                foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                {
                                    intCurrentX = Convert.ToInt32(pt.x);
                                    intCurrentY = Convert.ToInt32(pt.y);
                                    if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                                    {
                                        strPolygonXY += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                        intPreviousX = intCurrentX;
                                        intPreviousY = intCurrentY;
                                    }
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(intCurrentX), Convert.ToDouble(intCurrentY));  // Used for adjusted area calculation //
                                    iPoint++;  // Used for adjusted area calculation //
                                }
                            }
                            // Get Polygon Area - Need to create a new feature geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                            tempFeatureGeometry = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                            intObjectType = 2;  // Tree Group //
                            decArea = Convert.ToDecimal(((MapInfo.Geometry.MultiPolygon)tempFeatureGeometry).Area(AreaUnit.SquareMeter));

                            ftr["intObjectType"] = 1;  // 0 = Point, 1 = Polygon, 2 = Line //
                            ftr["intHighlighted"] = 0;
                            ftr["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)ftr.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                            ftr["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)ftr.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                        }

                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.FeatureGeometry OriginalLatLongFeature = MapFunctions.Convert_Polygon_Between_Projections(mpoly);
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        foreach (MapInfo.Geometry.Polygon poly in (MapInfo.Geometry.MultiPolygon)OriginalLatLongFeature)
                        {
                            foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                            {
                                dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length]; // Used for adjusted area calculation //
                                foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                {
                                    flLatLongX = (float)pt.x;
                                    flLatLongY = (float)pt.y;
                                    if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                                    {
                                        strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                        flPreviousX = flLatLongX;
                                        flPreviousY = flLatLongY;
                                    }
                                }
                            }
                        }
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                    }
                    break;
                case GeometryType.MultiCurve:  // PolyLine //
                    {
                        MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)ftr.Geometry;
                        MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                        MapInfo.Geometry.DPoint[] dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //

                        dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length]; // Used for adjusted area / length calculation //
                        //step through the array and print the coordinates to the debug window
                        for (int i = 0; i < dptArr.Length; i++)
                        {
                            intCurrentX = Convert.ToInt32(dptArr[i].x);
                            intCurrentY = Convert.ToInt32(dptArr[i].y);
                            if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                            {
                                strPolygonXY += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                intPreviousX = intCurrentX;
                                intPreviousY = intCurrentY;
                            }
                            dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(intCurrentX), Convert.ToDouble(intCurrentY));  // Used for adjusted length calculation //
                            iPoint++;  // Used for adjusted length calculation //                           
                        }
                        // Get Polyline Length - Need to create a new fetaure geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                        tempFeatureGeometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                        intObjectType = 1;  // Hedge //
                        decLength = Convert.ToDecimal(((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical));
                        decWidth = decDefaultHedgeWidth;
                        decArea = decLength * decWidth;

                        ftr["intObjectType"] = 2;  // 0 = Point, 1 = Polygon, 2 = Line //
                        ftr["intHighlighted"] = 0;
                        ftr["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                        ftr["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";


                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.FeatureGeometry OriginalLatLongFeature = MapFunctions.Convert_Polyline_Between_Projections(multiCurve);
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        multiCurve = (MapInfo.Geometry.MultiCurve)OriginalLatLongFeature;
                        curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                        dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //

                        dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length]; // Used for adjusted area / length calculation //
                        //step through the array and print the coordinates to the debug window
                        for (int i = 0; i < dptArr.Length; i++)
                        {
                            flLatLongX = (float)dptArr[i].x;
                            flLatLongY = (float)dptArr[i].y;
                            if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                            {
                                strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                flPreviousX = flLatLongX;
                                flPreviousY = flLatLongY;
                            }
                        }
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                    }
                    break;
                case GeometryType.MultiPoint:
                    break;
                case GeometryType.Point:
                    {
                        intObjectType = 0;  // Tree //
                        intX = Convert.ToInt32(ftr.Geometry.Centroid.x);
                        intY = Convert.ToInt32(ftr.Geometry.Centroid.y);
                        ftr["intObjectType"] = 0;  // 0 = Point, 1 = Polygon, 2 = Line //
                        ftr["intHighlighted"] = 0;
                        ftr["CalculatedSize"] = "";
                        ftr["CalculatedPerimeter"] = "";

                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.Point OriginalLatLongFeature = MapFunctions.Convert_Point_Between_Projections(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y);
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                        strLatLongPolygonXY = "";
                    }
                    break;
                default:
                    return 0;
            }
            if (intObjectType == -1) return 0;  // Unknown object type so abort //

            // Get Object Type to be plotted from drop down list of object types on toolbar //
            int intPlotType = Convert.ToInt32(beiPlotObjectType.EditValue);
            if (intPlotType == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the object type to be plotted from the Plot list on the toolbar before proceeding.", "Add Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }
            ftr["ModuleID"] = (intPlotType == -1 ? 1 : 2);

            if (strPolygonXY.Length > intStrPolygonXYMaxLength)  // Trap anything longer that 255 characters otherwise GBM Mobile Export can't handle it //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add object - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.\n\nTry plotting the object again but ensure there are less vertices.", "Add Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }

            Guid guid = Guid.NewGuid();
            string strMapID = guid.ToString();

            if (intPlotType == -1)  // Amenity Trees - Tree //
            {
                // Open Edit Tree screen and pass mapping details of potential new map object to it. //
                int[] intRowHandles;
                frmProgress fProgress = null;

                frm_AT_Tree_Edit fChildForm = new frm_AT_Tree_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = "";
                fChildForm.strFormMode = "add";
                fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                fChildForm.intRecordCount = 0;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();

                fChildForm.ibool_CalledByTreePicker = true;
                fChildForm.intObjectType = intObjectType;
                fChildForm.intX = intX;
                fChildForm.intY = intY;
                fChildForm.strPolygonXY = strPolygonXY;
                fChildForm.decArea = decArea;
                fChildForm.decLength = decLength;
                fChildForm.decWidth = decWidth;
                fChildForm.strID = strMapID;
                fChildForm.flLatLongX = flLatLongX;
                fChildForm.flLatLongY = flLatLongY;
                fChildForm.strLatLongPolygonXY = strLatLongPolygonXY;

                GridView view = (GridView)gridControl2.MainView;
                view.PostEditor();
                intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length == 1)
                {
                    fChildForm.intLinkedToRecordID1 = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SiteID"));
                    fChildForm.intLinkedToRecordID2 = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientID"));
                    fChildForm.strLinkedToRecordDescription1 = view.GetRowCellValue(intRowHandles[0], "SiteName").ToString();
                    fChildForm.strLinkedToRecordDescription2 = view.GetRowCellValue(intRowHandles[0], "ClientName").ToString();
                }

                fChildForm.Show();
                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            else  // Asset Management - Asset //
            {
                int[] intRowHandles;
                frmProgress fProgress = null;

                fProgress = new frmProgress(10);
                this.AddOwnedForm(fProgress);
                fProgress.Show();  // ***** Closed in PostOpen event ***** //
                Application.DoEvents();

                frm_EP_Asset_Edit fChildForm = new frm_EP_Asset_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = "";
                fChildForm.strFormMode = "add";
                fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                fChildForm.intRecordCount = 0;
                fChildForm.FormPermissions = this.FormPermissions;
                fChildForm.fProgress = fProgress;

                fChildForm.ibool_CalledByTreePicker = true;
                //fChildForm.intObjectType = intObjectType;
                fChildForm.intX = intX;
                fChildForm.intY = intY;
                fChildForm.strPolygonXY = strPolygonXY;
                fChildForm.decArea = decArea;
                fChildForm.decLength = decLength;
                fChildForm.decWidth = decWidth;
                fChildForm.strID = strMapID;
                fChildForm.intAssetType = intPlotType;

                GridView view = (GridView)gridControlAssetObjects.MainView;
                view.PostEditor();
                intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length == 1)
                {
                    fChildForm.intLinkedToRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SiteID"));
                }

                fChildForm.Show();
                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });

            }

            //if (intNewTreeID > 0)   // Record added to tblTree successfully, so open it for editing
            //{
                // Assign data to the feature... *** Once Data Entry screens are created, pick up the following data from them, add object into GridControl2 and get correct themtic styling if switched on. *** //             
                ftr["TREEREF"] = "Mark Test";
                ftr["SPECIES"] = "Scots Pine";
                ftr["ID"] = "1234567890";
                ftr["CrownDiameter"] = 0;  // *** NEED TO GET THIS ONCE USER CHANGES IT in data entry screen *** //                      
                ftr["Thematics"] = Convert.ToDouble(0.00);
                ftr.Style = style;  // ***  NEED TO FIRE DYNAMIC SIZING CODE IF SWITCHED ON THEN UPDATE STYLE'S SIZE *** //

                // ***** FOLLOWING LINE NO LONGER REQUIRED AS THE OBJECT IS ADDED AFTER WOODPLAN V4 HAS FINISHED WITH IT ***** //
                //MapInfo.Data.Key ftrKey = tab1.InsertFeature(ftr);
            //}

            conn.Close();
            conn.Dispose();
            return 1;
        }

        private void FeatureAdding(object sender, FeatureAddingEventArgs e)
        {
            if (e.Feature == null) return;
            MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)e.Feature.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //

            // The Current tool won't disengage to allow the user to select a different one [due to e.Cancel = true above], so following takes care of it //
            if (e.Feature.Type == GeometryType.MultiPolygon || e.Feature.Type == GeometryType.MultiCurve)
            {
                MouseSimulator.MouseDown(MouseButton.Left);
                KeyboardSimulator.KeyPress(Keys.Escape);
            }
            e.Cancel = true;  // Cancel the MapXtreme Default Event now //
            System.Windows.Forms.SendKeys.SendWait("{ESC}");  // Simulate Escape Key Press to force Map Extreme to let go

            int intSuccess = AddObjectToMap(newGeometry);  // Add object to map //
        }

        private void FeatureAdded(object sender, FeatureAddedEventArgs e)
        {
            /*            if (e.Feature != null)
                        {
                            memoEdit1.Text = memoEdit1.Text + "\r\nFeatureAdded: " + e.ToString() + "\r\n";
                            memoEdit1.Refresh();
                            DevExpress.XtraEditors.XtraMessageBox.Show("Tool: " + e.ToolName + "\r\nX: " + e.MapCoordinate.x.ToString() + "  Y: " + e.MapCoordinate.y.ToString(), "Feature Added Fired");
                            //UpdateObjCount();

                            // *** Get Styling *** //
                            GridView view = (GridView)gridControl1.MainView;
                            int intFoundRow = GetLayerManagerMapObjectsLayerRow();
                            if (intFoundRow == -1)
                            {
                                XtraMessageBox.Show("Unable to Set Feature Styles - Couldn't locate the Map Objects Layer in the Layer Manager.", "Set Feature Styles", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                            int intPassedLineColour = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "LineColour"));
                            int intPassedLineWidth = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "LineWidth"));
                            int intPassedTransparency = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "Percent"));
                            int intPassedGreyScale = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "GreyScale"));
                            int intPassedSymbolID = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "SymbolID"));
                            int intTempSymbolSize = Convert.ToInt32(spinEditMapObjectSize.EditValue);
                            CompositeStyle style = new CompositeStyle();
                            ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intPassedTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intPassedLineColour)));
                            style.AreaStyle.Interior.Attributes = StyleAttributes.InteriorAttributes.ForeColor;
                            style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intPassedLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), 2, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intPassedTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intPassedLineColour))));
                            style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intPassedSymbolID), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intPassedTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intPassedLineColour))), intTempSymbolSize);
                            // *** End of Get Styling *** //              


                            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();  // create connection and open table //
                            conn.Open();
                            MapInfo.Data.Table tab1 = conn.Catalog.GetTable("MapObjects");
                            MapInfo.Data.SearchInfo s2 = MapInfo.Data.SearchInfoFactory.SearchAll();  // create searchinfo object to capture all records //
                            MapInfo.Data.IResultSetFeatureCollection irfc = conn.Catalog.Search(tab1, s2);  // create an irfc to grab all the features in the table //
                            MapInfo.Data.Key key = new MapInfo.Data.Key(irfc.Count.ToString());  // grab the key for the last row (which is the row inserted by the FeatureAdded event) //
                            MapInfo.Data.Feature f1 = new MapInfo.Data.Feature(tab1.TableInfo.Columns);  // make a blank feature based on the table's structure //
                            f1.Geometry = e.Feature;  // assign the memory address of the inserted geometry to the new features Geometry property //

                            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + key.ToString() + "'");
                            MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(tab1, si);

                            switch (e.Feature.Type)
                            {
                                case GeometryType.MultiPolygon:
                                    MapInfo.Geometry.MultiPolygon mpoly = f1.Geometry as MapInfo.Geometry.MultiPolygon;
                                    foreach (MapInfo.Geometry.Polygon poly in mpoly)
                                    {
                                        foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                                        {
                                            foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                            {
                                                this.memoEdit1.Text += "...X: " + pt.x.ToString();
                                                this.memoEdit1.Text += "  Y: " + pt.y.ToString() + "\r\n";
                                            }
                                        }
                                        ftr["intObjectType"] = 1;  // 0 = Point, 1 = Polygon, 2 = Line //
                                        ftr["intHighlighted"] = 0;
                                    }
                                    break;
                                case GeometryType.MultiCurve:  // PolyLine //
                                    ftr["intObjectType"] = 2;  // 0 = Point, 1 = Polygon, 2 = Line //
                                    ftr["intHighlighted"] = 0;
                                    break;
                                case GeometryType.MultiPoint:
                                    break;
                                case GeometryType.Point:
                                    ftr["intObjectType"] = 0;  // 0 = Point, 1 = Polygon, 2 = Line //
                                    ftr["intHighlighted"] = 0;

                                    // Check if dynamic Sizing - if yes process each point feature and all polygons other wise process all objects in one hit //
                                    if (intMapObjectSizing == -1)  // Dynamic Sizing according to Crown Diameter //
                                    {
                                        int intCalculatedSize = (String.IsNullOrEmpty(ftr["CrownDiameter"].ToString()) ? 0 : Convert.ToInt32(ftr["CrownDiameter"]));
                                        if (intCalculatedSize <= 0)
                                        {
                                            intCalculatedSize = intTempSymbolSize;
                                            // Do something to highlight the object as missing a value //
                                        }
                                        else if (intCalculatedSize > 240)
                                        {
                                            intCalculatedSize = 240;
                                            // Do something to highlight the object as a bigger value //
                                        }
                                        intCalculatedSize = Convert.ToInt32(Math.Round(Convert.ToDouble(intCalculatedSize) / (0.000209 * mapControl1.Map.Scale), 0));
                                        if (intCalculatedSize < 6)
                                        {
                                            intCalculatedSize = 6;
                                        }
                                        else if (intCalculatedSize > 240)
                                        {
                                            intCalculatedSize = 240;
                                        }
                                        style.SymbolStyle.PointSize = intCalculatedSize;
                                    }
                                    break;
                                default:
                                    return;
                            }
                            // assign data to the feature... //
                            ftr["TREEREF"] = "Mark Test";
                            ftr["SPECIES"] = "Scots Pine";
                            ftr["ID"] = "1234567890";
                            ftr["CrownDiameter"] = 0;  //*** NEED TO GET THIS ONCE USER CHANGES IT *** //                      
                            ftr.Style = style; // cs;
                            ftr.Update(); 

                            //tab1.BeginAccess(MapInfo.Data.TableAccessMode.Write);
                            //tab1.DeleteFeature(key);  // delete the last row in the table //
                            //tab1.InsertFeature(f1);  // insert the new feature //
                            //tab1.EndAccess();
                            //tab1.Pack(MapInfo.Data.PackType.All);  // pack the table //
                            conn.Close();
                            conn.Dispose();                
                          }*/
        }

        private void FeatureSelecting(object sender, FeatureSelectingEventArgs e)
        {
            //memoEdit1.Text = memoEdit1.Text + "\r\nFeatureSelecting:  " + (e.Cancel ? "Cancel" : e.ToString());
            //memoEdit1.Refresh();
        }

        private void FeatureSelected(object sender, FeatureSelectedEventArgs e)
        {
            /*memoEdit1.Text = memoEdit1.Text + "\r\nFeatureSelected:  " + e.ToString() + "\r\n";
            memoEdit1.Refresh();
            //DevExpress.XtraEditors.XtraMessageBox.Show("Tool: " + e.ToolName + "\nX: " + e.MapCoordinate.x.ToString() + "\nY: " + e.MapCoordinate.y.ToString() + "\nClientCoordinate: " + e.ClientCoordinate.ToString(), "Selection Fired");
            */
            Process_Selected_Map_Objects();
            GridView view = (GridView)gridControl6.MainView;
            view.ExpandAllGroups();
            view = (GridView)gridControl7.MainView;
            view.ExpandAllGroups();
        }

        private void Process_Selected_Map_Objects()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!checkEditSyncSelection.Checked && dockPanelInspections.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Visible) return;

            int intRowHandle = 0;
            GridView view = (GridView)gridControl2.MainView;
            GridView view2 = (GridView)gridControlAssetObjects.MainView;
            view.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
            view2.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
            DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["TreeID"];
            DevExpress.XtraGrid.Columns.GridColumn col2 = view2.Columns["AssetID"];
            view.ClearSelection(); // Clear any selected rows (rows with blue highlighter) - Updated rows will be selected below //
            view2.ClearSelection(); // Clear any selected rows (rows with blue highlighter) - Updated rows will be selected below //

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            i_intMapObjectsSelected = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers excpet MapObjects //
                {
                    string strSelectedIDs = "";
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        //for (int i = 0; i < f.Columns.Count; i++)
                        //{
                        //    System.Diagnostics.Debug.WriteLine(f[i].ToString());
                        //}
                        //memoEdit1.Text = memoEdit1.Text + "TreeID Selected:  " + f[0].ToString() + "\r\n";
                        i_intMapObjectsSelected++;

                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Amenity Trees Object //
                        {
                            strSelectedIDs += f["TreeID"].ToString() + ",";
                            if (checkEditSyncSelection.Checked)
                            {
                                intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["TreeID"]));
                                if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) return;
                                view.SelectRow(intRowHandle);
                                view.MakeRowVisible(intRowHandle, false);
                            }
                        }
                        else  // Asset Object //
                        {
                            if (checkEditSyncSelection.Checked)
                            {
                                intRowHandle = view2.LocateByValue(0, col2, Convert.ToInt32(f["AssetID"]));
                                if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) return;
                                view2.SelectRow(intRowHandle);
                                view2.MakeRowVisible(intRowHandle, false);
                            }
                        }
                    }

                    if (dockPanelInspections.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible && !string.IsNullOrEmpty(strSelectedIDs))
                    {
                        WaitDialogForm loading = new WaitDialogForm("Loading Linked Inspections \\ Actions...", "WoodPlan Mapping");
                        loading.Show();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store any expanded groups and selected rows //
                        gridControl6.BeginUpdate();
                        sp01367_AT_Inspections_For_TreesTableAdapter.Fill(dataSet_AT_DataEntry.sp01367_AT_Inspections_For_Trees, strSelectedIDs, null, null, 0);
                        this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                        gridControl6.EndUpdate();
                        
                        char[] delimiters = new char[] { ';' };
                        string[] strArray = null;
                        int intID = 0;
                        // Highlight any recently added new rows //
                        if (i_str_AddedRecordIDs2 != "")
                        {
                            strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            intID = 0;
                            view = (GridView)gridControl6.MainView;
                            view.ClearSelection(); // Clear any current selection so just the new record is selected //
                            foreach (string strElement in strArray)
                            {
                                intID = Convert.ToInt32(strElement);
                                intRowHandle = view.LocateByValue(0, view.Columns["InspectionID"], intID);
                                if (intRowHandle != GridControl.InvalidRowHandle)
                                {
                                    view.SelectRow(intRowHandle);
                                    view.MakeRowVisible(intRowHandle, false);
                                }
                            }
                            i_str_AddedRecordIDs2 = "";
                        }

                        this.RefreshGridViewState2.SaveViewInfo();  // Store any expanded groups and selected rows //
                        gridControl7.BeginUpdate();
                        sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Fill(dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections, strSelectedIDs, 1, null, null, 0);
                        this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
                        gridControl7.EndUpdate();

                        // Highlight any recently added new rows //
                        if (i_str_AddedRecordIDs3 != "")
                        {
                            strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            intID = 0;
                            view = (GridView)gridControl7.MainView;
                            view.ClearSelection(); // Clear any current selection so just the new record is selected //
                            foreach (string strElement in strArray)
                            {
                                intID = Convert.ToInt32(strElement);
                                intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                                if (intRowHandle != GridControl.InvalidRowHandle)
                                {
                                    view.SelectRow(intRowHandle);
                                    view.MakeRowVisible(intRowHandle, false);
                                }
                            }
                            i_str_AddedRecordIDs3 = "";
                        }

                        loading.Hide();
                        loading.Dispose();
                    }
                }
            }
            if (i_intMapObjectsSelected <= 0)  // Clear linked Inspections and Actions //
            {
                gridControl6.BeginUpdate();
                dataSet_AT_DataEntry.sp01367_AT_Inspections_For_Trees.Rows.Clear();
                gridControl6.EndUpdate();

                gridControl7.BeginUpdate();
                dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections.Rows.Clear();
                gridControl7.EndUpdate();
            }
            view.EndSelection();
            view2.EndSelection();
            view.Invalidate();
            view2.Invalidate();
        }

        private void FeatureChanging(object sender, FeatureChangingEventArgs e)
        {
            if (e.FeatureChangeMode == FeatureChangeMode.Delete)
            {
                // Get all selected items and iterate through them //
                MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
                System.Collections.IEnumerator enum1 = selection.GetEnumerator();
                int intDeletedCount = 0;

                MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                conn.Open();
                MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
                try
                {
                    while (enum1.MoveNext())
                    {
                        MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                        if (irfc.BaseTable.Alias == "TempMapObjects")  // Ignore any selectable layers except TempMapObjects //
                        {
                            foreach (MapInfo.Data.Feature f in irfc)
                            {
                                intDeletedCount++;
                                t.DeleteFeature(f);  // delete feature //
                            }
                        }
                    }
                    if (intDeletedCount > 0)
                    {
                        t.Pack(MapInfo.Data.PackType.All);  // pack the table //
                        conn.Close();
                        conn.Dispose();
                        e.Cancel = true;  // Now cancel out so that if any Map Objects [not temp ones] are selected, they are not deleted //
                    }
                }
                catch
                {
                    conn.Close();
                    conn.Dispose();
                }

                if (intDeletedCount <= 0)  // Nothing deleted, so only regular map objects selected which need to be deleted via the menu //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("To Delete map objects, select Delete from the Map Menu.", "Delete Selected Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    e.Cancel = true;
                }
            }
        }

        private void FeatureChanged(object sender, MapInfo.Tools.FeatureChangedEventArgs e)
        {
            if (e.ToolStatus == ToolStatus.End)
            {
                //memoEdit1.Text = memoEdit1.Text + "\r\nFeatureChanged:  " + e.ToString() + "  FeatureChangeMode = " + e.FeatureChangeMode.ToString();
                //memoEdit1.Refresh();
                //UpdateObjCount();

                // Get all selected items and iterate through them then update the back-end //
                MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
                System.Collections.IEnumerator enum1 = selection.GetEnumerator();
                while (enum1.MoveNext())
                {
                    MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                    if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                    {
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            UpdateMapObjectPosition(f);
                        }
                    }
                    else if (irfc.BaseTable.Alias == "TempMapObjects")
                    {
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            if (f["Type"].ToString() == "MapMarker")
                            {
                                MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)f.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //
                                int intCurrentX = 0;
                                int intCurrentY = 0;
                                intCurrentX = Convert.ToInt32(newGeometry.Centroid.x);
                                intCurrentY = Convert.ToInt32(newGeometry.Centroid.y);
                                f["decX"] = Convert.ToDecimal(intCurrentX);
                                f["decY"] = Convert.ToDecimal(intCurrentY);
                                f["Description"] = "Map Marker - X,Y: " + newGeometry.Centroid.x.ToString("#0") + "," + newGeometry.Centroid.y.ToString("#0");
                                f.Update(true);
                            }
                        }
                    }
                }
            }
        }

        private void Tools_NodeChanging(object sender, MapInfo.Tools.NodeChangingEventArgs e)
        {
            //memoEdit1.Text = memoEdit1.Text + "\r\nNodeChanging:  " + e.NodeChangeMode.ToString();
            //memoEdit1.Refresh();
            /*
            if (e.NodeChangeMode == NodeChangeMode.Add)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to add a new node to the current map object?", "Add Polgon \\ Polyline Node", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) e.Cancel = true;
            }
            else if (e.NodeChangeMode == NodeChangeMode.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to remove the currently selected new node from the current map object?", "Remove Polgon \\ Polyline Node", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) e.Cancel = true;
            }
            else if (e.NodeChangeMode == NodeChangeMode.Move)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to move the currently selected node in the current map object?", "Move Polgon \\ Polyline Node", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) e.Cancel = true;
            }
            */

            if (e.NodeChangeMode == NodeChangeMode.Delete)
            {
                // If Deleting check enough points will be left in the object and cancel the delete if the object will be deleted as a result of too few points left in it //
                MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
                System.Collections.IEnumerator enum1 = selection.GetEnumerator();
                while (enum1.MoveNext())
                {
                    MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                    if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                    {
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            switch (f.Geometry.Type)
                            {
                                case GeometryType.MultiPolygon:
                                    MapInfo.Geometry.MultiPolygon mpoly = f.Geometry as MapInfo.Geometry.MultiPolygon;
                                    foreach (MapInfo.Geometry.Polygon poly in mpoly)
                                    {
                                        foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                                        {
                                            if (curve.SamplePoints().Length <= 4)
                                            {
                                                e.Cancel = true;
                                                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete node - too few nodes would be left to complete the object.", "Delete Polgon Node", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            }
                                        }
                                    }
                                    break;
                                case GeometryType.MultiCurve:  // PolyLine //
                                    MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)f.Geometry;
                                    MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                                    if (curves.SamplePoints().Length <= 2)
                                    {
                                        e.Cancel = true;
                                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete node - too few nodes would be left to complete the object.", "Delete Polyline Node", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            else if (e.NodeChangeMode == NodeChangeMode.Add)
            {
                // If Adding check total length of points will not exceed 255 character limit of GBM Mobile //
                int intPreviousX = -99999;
                int intPreviousY = -99999;
                int intCurrentX = 0;
                int intCurrentY = 0;
                string strPassedParameterString = "";
                MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
                System.Collections.IEnumerator enum1 = selection.GetEnumerator();
                while (enum1.MoveNext())
                {
                    MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                    if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                    {
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            switch (f.Geometry.Type)
                            {
                                case GeometryType.MultiPolygon:
                                    MapInfo.Geometry.MultiPolygon mpoly = f.Geometry as MapInfo.Geometry.MultiPolygon;
                                    foreach (MapInfo.Geometry.Polygon poly in mpoly)
                                    {
                                        foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                                        {
                                            foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                            {
                                                intCurrentX = Convert.ToInt32(pt.x);
                                                intCurrentY = Convert.ToInt32(pt.y);
                                                if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                                                {
                                                    strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                                    intPreviousX = intCurrentX;
                                                    intPreviousY = intCurrentY;
                                                }
                                            }
                                            if (strPassedParameterString.Length > intStrPolygonXYMaxLength)
                                            {
                                                e.Cancel = true;
                                                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add node - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.", "Add Polgon Node", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            }
                                        }
                                    }
                                    break;
                                case GeometryType.MultiCurve:  // PolyLine //
                                    MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)f.Geometry;
                                    MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                                    MapInfo.Geometry.DPoint[] dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //
                                    // Step through the array and get the coordinates //
                                    for (int i = 0; i < dptArr.Length; i++)
                                    {
                                        intCurrentX = Convert.ToInt32(dptArr[i].x);
                                        intCurrentY = Convert.ToInt32(dptArr[i].y);
                                        if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                                        {
                                            strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                            intPreviousX = intCurrentX;
                                            intPreviousY = intCurrentY;
                                        }
                                    }
                                    if (strPassedParameterString.Length > intStrPolygonXYMaxLength)
                                    {
                                        e.Cancel = true;
                                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add node - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.", "Add Polyline Node", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }

        }

        private void Tools_NodeChanged(object sender, MapInfo.Tools.NodeChangedEventArgs e)
        {
            //memoEdit1.Text = memoEdit1.Text + "\r\nNodeChanged:  " + e.NodeChangeMode.ToString();
            //memoEdit1.Refresh();

            // Get all selected items and iterate through them then update the back-end//
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        UpdateMapObjectPosition(f);
                    }
                }
            }
        }

        private void UpdateMapObjectPosition(MapInfo.Data.Feature f)
        {
            if (f == null) return;
            MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)f.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //
            int intPreviousX = -99999;
            int intPreviousY = -99999;
            int intCurrentX = 0;
            int intCurrentY = 0;
            string strPassedParameterString = "";
            string strObjectType = "";

            float flPreviousX = (float)-99999;
            float flPreviousY = (float)-99999;
            float flLatLongX = (float)0.00;
            float flLatLongY = (float)0.00;
            string strLatLongPolygonXY = "";

            MapInfo.Geometry.FeatureGeometry tempFeatureGeometry = null;  // Used for adjusted area / length calculation //
            int iPoint = 0;  // Used for adjusted area / length calculation //
            MapInfo.Geometry.DPoint[] dPoints = null;  // Used for adjusted area / length calculation ///

            switch (f.Geometry.Type)
            {
                case GeometryType.MultiPolygon:
                    {
                        MapInfo.Geometry.MultiPolygon mpoly = newGeometry as MapInfo.Geometry.MultiPolygon;
                        foreach (MapInfo.Geometry.Polygon poly in mpoly)
                        {
                            foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                            {
                                dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length];  // Used for adjusted area / length calculation //
                                foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                {
                                    intCurrentX = Convert.ToInt32(pt.x);
                                    intCurrentY = Convert.ToInt32(pt.y);
                                    if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                                    {
                                        strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                        intPreviousX = intCurrentX;
                                        intPreviousY = intCurrentY;
                                    }
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(intCurrentX), Convert.ToDouble(intCurrentY));  // Used for adjusted area / length calculation //
                                    iPoint++;  // Used for adjusted area / length calculation //
                                }
                            }
                            // Get Polygon Area - Need to create a new fetaure geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                            tempFeatureGeometry = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                            f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)tempFeatureGeometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                            f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                            strObjectType = "polygon";
                        }
                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.FeatureGeometry OriginalLatLongFeature = MapFunctions.Convert_Polygon_Between_Projections(mpoly);
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        foreach (MapInfo.Geometry.Polygon poly in (MapInfo.Geometry.MultiPolygon)OriginalLatLongFeature)
                        {
                            foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                            {
                                dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length]; // Used for adjusted area calculation //
                                foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                {
                                    flLatLongX = (float)pt.x;
                                    flLatLongY = (float)pt.y;
                                    if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                                    {
                                        strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                        flPreviousX = flLatLongX;
                                        flPreviousY = flLatLongY;
                                    }
                                }
                            }
                        }
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                    }
                    break;
                case GeometryType.MultiCurve:  // PolyLine //
                    {
                        MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)newGeometry;
                        MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                        MapInfo.Geometry.DPoint[] dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //
                        dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length];  // Used for adjusted area / length calculation //
                        // Step through the array and get the coordinates //
                        for (int i = 0; i < dptArr.Length; i++)
                        {
                            intCurrentX = Convert.ToInt32(dptArr[i].x);
                            intCurrentY = Convert.ToInt32(dptArr[i].y);
                            if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                            {
                                strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                intPreviousX = intCurrentX;
                                intPreviousY = intCurrentY;
                            }
                            dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(intCurrentX), Convert.ToDouble(intCurrentY));  // Used for adjusted area / length calculation //
                            iPoint++;  // Used for adjusted area / length calculation //
                        }
                        // Get Polyline Length - Need to create a new fetaure geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                        tempFeatureGeometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                        f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                        f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                        strObjectType = "polyline";

                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.FeatureGeometry OriginalLatLongFeature = MapFunctions.Convert_Polyline_Between_Projections(multiCurve);
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        multiCurve = (MapInfo.Geometry.MultiCurve)OriginalLatLongFeature;
                        curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                        dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //

                        dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length]; // Used for adjusted area / length calculation //
                        //step through the array and print the coordinates to the debug window
                        for (int i = 0; i < dptArr.Length; i++)
                        {
                            flLatLongX = (float)dptArr[i].x;
                            flLatLongY = (float)dptArr[i].y;
                            if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                            {
                                strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                flPreviousX = flLatLongX;
                                flPreviousY = flLatLongY;
                            }
                        }
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                    }
                    break;
                case GeometryType.MultiPoint:
                    break;
                case GeometryType.Point:
                    {
                        intCurrentX = Convert.ToInt32(newGeometry.Centroid.x);
                        intCurrentY = Convert.ToInt32(newGeometry.Centroid.y);
                        f["CalculatedSize"] = "";
                        f["CalculatedPerimeter"] = "";
                        strObjectType = "point";

                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.Point OriginalLatLongFeature = MapFunctions.Convert_Point_Between_Projections(f.Geometry.Centroid.x, f.Geometry.Centroid.y);
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                        strLatLongPolygonXY = "";
                    }
                    break;
                default:
                    return;
            }
            f.Update(true);  // Update CalculatedSize //

            if (strPassedParameterString.Length > intStrPolygonXYMaxLength)  // Trap anything longer that 255 characters otherwise GBM Mobile Export can't handle it //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to edit the object - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.\n\nTry plotting the object again but ensure there are less vertices.", "Edit Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Update Database with changes to the object //
            int intRowHandle = 0;
            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter UpdateBackEnd = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
            UpdateBackEnd.ChangeConnectionString(strConnectionString);
            if (strObjectType == "point")
            {
                try
                {
                    if (Convert.ToInt32(f["ModuleID"]) == 1)  // Amenity Trees //
                    {
                        GridView view = (GridView)gridControl2.MainView;
                        UpdateBackEnd.sp01314_AT_Tree_Picker_Update_X_and_Y(Convert.ToInt32(f["TreeID"]), intCurrentX, intCurrentY, 1, flLatLongX, flLatLongY);
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["TreeID"];
                        intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["TreeID"]));
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "XCoordinate")) != intCurrentX || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "YCoordinate")) != intCurrentY)
                            {
                                view.SetRowCellValue(intRowHandle, "XCoordinate", intCurrentX);
                                view.SetRowCellValue(intRowHandle, "YCoordinate", intCurrentY);
                            }
                        }
                    }
                    else  // Asset Management //
                    {
                        GridView view = (GridView)gridControlAssetObjects.MainView;
                        UpdateBackEnd.sp01314_AT_Tree_Picker_Update_X_and_Y(Convert.ToInt32(f["AssetID"]), intCurrentX, intCurrentY, 2, flLatLongX, flLatLongY);
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["AsserID"];
                        intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["AssetID"]));
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "XCoordinate")) != intCurrentX || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "YCoordinate")) != intCurrentY)
                            {
                                view.SetRowCellValue(intRowHandle, "XCoordinate", intCurrentX);
                                view.SetRowCellValue(intRowHandle, "YCoordinate", intCurrentY);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to update the database [" + ex.Message + "].", "Edit Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else  // Polygon / Polyline //
            {
                try
                {
                    decimal decAreaM2 = (decimal)0.00;
                    string strObType = "";
                    if (strObjectType == "polygon")
                    {
                        decAreaM2 = Convert.ToDecimal(string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)tempFeatureGeometry).Area(AreaUnit.SquareMeter)));
                        strObType = "polygon";
                    }
                    else
                    {
                        decAreaM2 = Convert.ToDecimal(string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)));
                        strObType = "polyline";
                    }
                    if (Convert.ToInt32(f["ModuleID"]) == 1)  // Amenity Trees //
                    {
                        GridView view = (GridView)gridControl2.MainView;
                        UpdateBackEnd.sp01315_AT_Tree_Picker_Update_strPolygonXY(Convert.ToInt32(f["TreeID"]), strPassedParameterString, decAreaM2, Convert.ToInt32(tempFeatureGeometry.Centroid.x), Convert.ToInt32(tempFeatureGeometry.Centroid.y), strObType, 1, flLatLongX, flLatLongY, strLatLongPolygonXY);
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["TreeID"];
                        intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["TreeID"]));
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            if (view.GetRowCellValue(intRowHandle, "PolygonXY").ToString() != strPassedParameterString)
                            {
                                view.SetRowCellValue(intRowHandle, "PolygonXY", strPassedParameterString);
                            }
                        }
                    }
                    else  // Asset Management //
                    {
                        GridView view = (GridView)gridControlAssetObjects.MainView;
                        UpdateBackEnd.sp01315_AT_Tree_Picker_Update_strPolygonXY(Convert.ToInt32(f["AssetID"]), strPassedParameterString, decAreaM2, Convert.ToInt32(tempFeatureGeometry.Centroid.x), Convert.ToInt32(tempFeatureGeometry.Centroid.y), strObType, 2, flLatLongX, flLatLongY, strLatLongPolygonXY);
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["AssetID"];
                        intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["AssetID"]));
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            if (view.GetRowCellValue(intRowHandle, "PolygonXY").ToString() != strPassedParameterString)
                            {
                                view.SetRowCellValue(intRowHandle, "PolygonXY", strPassedParameterString);
                            }
                        }
                    }
               }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to update the database [" + ex.Message + "].", "Edit Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void Map_ViewChanged(object o, ViewChangedEventArgs e)
        {
            if (Math.Round(mapControl1.Map.Scale, 0, MidpointRounding.ToEven) != Math.Round(dCurrentMapScale, 0, MidpointRounding.ToEven))  // Scale changed so check if we need to recalculate the size of all point objects on the map //
            {
                dCurrentMapScale = mapControl1.Map.Scale;
                beiPopupContainerScale.EditValue = "Scale 1:" + mapControl1.Map.Scale.ToString("#0");

                refresh_scale_bar();

                // *** If dynamic point sizing is switched on, redraw points so sizes are calculates correctly *** //
                if (intMapObjectSizing == -1) Update_Map_Object_Sizes(true);
            }
            if (boolStoreViewChanges)
            {
                if (!boolStoreViewChangesTempDisable)
                {
                    StoreView();
                }
                else  // Don't store view change as the user has gone to an exiting sored view and we don't want it stored again - so reset flag //
                {
                    boolStoreViewChangesTempDisable = false;
                }
            }
        }


        private void Layers_CountChanged(object o, CollectionEventArgs e)
        {
            bsiLayerCount.Caption = "Layer Count: " + mapControl1.Map.Layers.Count.ToString();
        }

        private void mapControl1_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //memoEdit1.Text = memoEdit1.Text + "\r\nMouseWheel:  Delta=" + e.Delta + ", X=" + e.X + ", Y=" + e.Y;
            //memoEdit1.Refresh();
        }

        private void mapControl1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 's')
                switch (e.KeyChar)
                {
                    case 's':
                        ToggleSnap();  // toggle snap mode if user presses the 'S' key //
                        break;
                }
        }

        private void mapControl1_Enter(object sender, EventArgs e)
        {
            // Start Mouse and Keyboard Hooks //
            mouseHook.Stop();
            keyboardHook.Stop();
            mouseHook.Start();  // Also Stopped in the FormClosed event //
            keyboardHook.Start();  // Also Stopped in the FormClosed event //
        }

        private void bsiSnap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ToggleSnap();  // toggle snap mode //
        }

        private void ToggleSnap()
        {
            mapControl1.Tools.MapToolProperties.SnapEnabled = !mapControl1.Tools.MapToolProperties.SnapEnabled;
            bsiSnap.Caption = "Snap: " + (mapControl1.Tools.MapToolProperties.SnapEnabled ? "On" : "Off");
        }

        private void mapControl1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            System.Drawing.PointF DisplayPoint = new System.Drawing.PointF(e.X, e.Y);
            MapInfo.Geometry.DPoint MapPoint = new MapInfo.Geometry.DPoint();
            MapInfo.Geometry.DisplayTransform converter = this.mapControl1.Map.DisplayTransform;
            converter.FromDisplay(DisplayPoint, out MapPoint);
            bsiScreenCoords.Caption = "Location: " + String.Format("{0:0.##}", MapPoint.x) + ", " + String.Format("{0:0.##}", MapPoint.y);

            if (boolTrackXandY)  // Used to prevent tracking when right click menu of Map displayed [see popupMenu_MapControl1_Popup and popupMenu_MapControl1_Closeup events] //
            {
                decCurrentMouseX = (decimal)MapPoint.x;
                decCurrentMouseY = (decimal)MapPoint.y;
            }
        }


        private void barSubItemMapEdit_Popup(object sender, EventArgs e)
        {
            MouseEventArgs mea = null;
            ShowMapControl1Menu(mea);  // Set Menu Item status in called event - pass null as we don't want the menu to show from there as barSubItemMapEdit will open it's own version of the menu automatically here. // 
        }

        private void bbiEditSelectedMapObjects_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowEdit) return;

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            string strPassedParameterString = "";
            string strPassedParameterString2 = "";
            int intTreeCount = 0;
            int intAssetCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Amenity Trees Object //
                        {
                            strPassedParameterString += f["TreeID"].ToString() + ",";
                            intTreeCount++;
                        }
                        else // Asset Management Object //
                        {
                            strPassedParameterString2 += f["AssetID"].ToString() + ",";
                            intAssetCount++;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(strPassedParameterString) && string.IsNullOrEmpty(strPassedParameterString2))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more map objects to edit before proceeding.", "Edit Selected Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            //bool boolSucceed = NotifyWoodPlanVersion4("selected;", strPassedParameterString);  // Call WoodPlan V.4 for Edit screens //
            if (!string.IsNullOrEmpty(strPassedParameterString) && !string.IsNullOrEmpty(strPassedParameterString2))
            {
                frm_AT_Mapping_Tree_Picker_Edit_Select_Type fChooseType = new frm_AT_Mapping_Tree_Picker_Edit_Select_Type();
                fChooseType.ShowDialog();
                switch (fChooseType.strReturnedValue)
                {
                    case "Cancel":
                        return;
                    case "AmenityTrees":
                        strPassedParameterString2 = "";  // Clear selected Asset Objectss so the edit screen is not opened //
                        break;
                    case "Assets":
                        strPassedParameterString = "";  // Clear selected Amenity Tree Objects so the edit screen is not opened //
                        break;
                    case "Both":  // Do nothing so both data entry screens are opened //
                        break;
                    default:
                        break;
                }
            }

            if (!string.IsNullOrEmpty(strPassedParameterString))  // Edit Amenity Trees //
            {
                
                frm_AT_Tree_Edit fChildForm = new frm_AT_Tree_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = strPassedParameterString;
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                fChildForm.intRecordCount = intTreeCount;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            if (!string.IsNullOrEmpty(strPassedParameterString2))  // Edit Assets //
            {
                frmProgress fProgress = null;
                //if (!iBool_AllowEdit) return;
                fProgress = new frmProgress(10);
                this.AddOwnedForm(fProgress);
                fProgress.Show();  // ***** Closed in PostOpen event ***** //
                Application.DoEvents();
                
                frm_EP_Asset_Edit fChildForm = new frm_EP_Asset_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = strPassedParameterString2;
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                fChildForm.intRecordCount = intAssetCount;
                fChildForm.FormPermissions = this.FormPermissions;
                fChildForm.fProgress = fProgress;
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }

        }

        private void bbiBlockEditselectedMapObjects_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowEdit) return;

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            string strPassedParameterString = "";
            string strPassedParameterString2 = "";
            int intTreeCount = 0;
            int intAssetCount = 0;
            string strMapID = "";
            string strMapID2 = "";
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Amenity Trees Object //
                        {
                            strPassedParameterString += f["TreeID"].ToString() + ",";
                            strMapID += f["id"].ToString() + ",";  // Map IDs //
                            intTreeCount++;
                        }
                        else // Asset Management Object //
                        {
                            strPassedParameterString2 += f["AssetID"].ToString() + ",";
                            strMapID2 += f["id"].ToString() + ",";  // Map IDs //
                            intAssetCount++;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(strPassedParameterString) && string.IsNullOrEmpty(strPassedParameterString2))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more map objects to block edit before proceeding.", "Block Edit Selected Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!string.IsNullOrEmpty(strPassedParameterString) && !string.IsNullOrEmpty(strPassedParameterString2))
            {
                frm_AT_Mapping_Tree_Picker_Edit_Select_Type fChooseType = new frm_AT_Mapping_Tree_Picker_Edit_Select_Type();
                fChooseType.ShowDialog();
                switch (fChooseType.strReturnedValue)
                {
                    case "Cancel":
                        return;
                    case "AmenityTrees":
                        strPassedParameterString2 = "";  // Clear selected Asset Objectss so the edit screen is not opened //
                        break;
                    case "Assets":
                        strPassedParameterString = "";  // Clear selected Amenity Tree Objects so the edit screen is not opened //
                        break;
                    case "Both":  // Do nothing so both data entry screens are opened //
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(strPassedParameterString))  // Block Edit Amenity Trees //
            {
                frm_AT_Tree_Edit fChildForm = new frm_AT_Tree_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = strPassedParameterString;
                fChildForm.strMapIDs = strMapID;
                fChildForm.strFormMode = "blockedit";
                fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                fChildForm.intRecordCount = selection.TotalCount;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            if (!string.IsNullOrEmpty(strPassedParameterString2))  // Block Edit Assets //
            {
                frmProgress fProgress = null;
                //if (!iBool_AllowEdit) return;
                fProgress = new frmProgress(10);
                this.AddOwnedForm(fProgress);
                fProgress.Show();  // ***** Closed in PostOpen event ***** //
                Application.DoEvents();

                frm_EP_Asset_Edit fChildForm = new frm_EP_Asset_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = strPassedParameterString2;
                fChildForm.strMapIDs = strMapID2;
                fChildForm.strFormMode = "blockedit";
                fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                fChildForm.intRecordCount = selection.TotalCount;
                fChildForm.FormPermissions = this.FormPermissions;
                fChildForm.fProgress = fProgress;
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
        }

        private void bbiAddInspection_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowAdd) return;
            Block_Add_Record("inspection");
        }

        private void bbiAddAction_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowAdd) return;
            // Block add actions (and dummy inspections) to selected map objects //
            Block_Add_Record("map_actions");
        }

        private void bbiBlockAddActionsToInspections_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowAdd) return;
            // Block add actions to selected inspections //
            Block_Add_Record("inspection_actions");
        }


        private void bbiDeleteMapObjects_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowDelete) return;

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            string strRecordIDs1 = "";
            string strRecordIDs2 = "";
            string strMapIDs = "";
            int intCount1 = 0;
            int intCount2 = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Amenity Tree Objects //
                        {
                            strRecordIDs1 += f["TreeID"].ToString() + ",";
                            intCount1++;
                        }
                        else  // Asset Objects //
                        {
                            strRecordIDs2 += f["AssetID"].ToString() + ",";
                            intCount2++;
                        }
                        strMapIDs += f["id"].ToString() + ",";  // MapIDs //
                    }
                }
            }
            if (string.IsNullOrEmpty(strRecordIDs1) && string.IsNullOrEmpty(strRecordIDs2))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more map objects to delete before proceeding.", "Delete Selected Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            //bool boolSucceed = NotifyWoodPlanVersion4("delete;", strPassedParameterString);  // Call WoodPlan V.4 for Edit screens //
            //if (!iBool_AllowDelete) return;
            string strMessage = "";
            if (intCount1 > 0)
            {
                strMessage = "You have " + (intCount1 == 1 ? "1 Tree" : Convert.ToString(intCount1.ToString()) + " Trees") + " selected for delete!";
            }
            if (intCount2 > 0)
            {
                if (!string.IsNullOrEmpty(strMessage)) strMessage += "\n";
                strMessage += "You have " + (intCount2 == 1 ? "1 Asset" : Convert.ToString(intCount2.ToString()) + " Assets") + " selected for delete!";
            }
            strMessage += "\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount1+intCount2 == 1 ? "this object" : "these objects") + " will be deleted from the database and any related records will also be deleted!";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                if (!string.IsNullOrEmpty(strRecordIDs1))
                {
                    DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    RemoveRecords.sp01373_AT_Tree_Manager_Delete("tree", strRecordIDs1);  // Remove the records from the DB in one go //
                    if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                }
                if (!string.IsNullOrEmpty(strRecordIDs2))
                {
                    DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    RemoveRecords.sp03029_EP_Asset_Delete(strRecordIDs2);  // Remove the records from the DB in one go //
                    if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                }

                RefreshMapObjects(strMapIDs, 0);  // Pass the delete object's mapIDs to the event so they can be removed from the map //

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                int intTotal = intCount1 + intCount2;
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intTotal.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void bbiCreateDatasetFromMapObjects_ItemClick(object sender, ItemClickEventArgs e)
        {
            int intCount = 0;
            string strCurrentID = "";
            string strSelectedIDs = "";
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                foreach (MapInfo.Data.Feature f in irfc)
                {
                    if (strSelectedIDs == "")
                    {
                        strSelectedIDs += f["TreeID"].ToString() + ',';
                        intCount++;
                    }
                    else
                    {
                        strCurrentID = f["TreeID"].ToString() + ',';
                        if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                        {
                            strSelectedIDs += strCurrentID;
                            intCount++;
                        }
                    }
                }
            }
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more map objects to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            CreateDataset("Tree", intCount, strSelectedIDs, 0, "", 0, "");
        }

        private void bbiCreateIncidentFromMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            // X and Y set in mouseHook_MouseUp //
            CreateIncidentAtLocation(Convert.ToInt32(decCurrentMouseX), Convert.ToInt32(decCurrentMouseY), 0, "", "", "", "", "", "");
        }

        private void CreateIncidentAtLocation(int intX, int intY, int intRecordID, string strAddressLine1, string strAddressLine2, string strAddressLine3, string strAddressLine4, string strAddressLine5, string strPostcode)
        {
            // Add Incident - can be called by either bbiCreateIncidentFromMap_ItemClick or bbiCreateIncident_ItemClick //
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;

            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            frm_AT_Incident_Edit fChildForm = new frm_AT_Incident_Edit();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = "frm_AT_Incident_Manager";
            fChildForm.intRecordCount = 0;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.fProgress = fProgress;

            fChildForm.intXCoordinate = intX;
            fChildForm.intYCoordinate = intY;
            fChildForm.strIncidentAddressLine1 = strAddressLine1;
            fChildForm.strIncidentAddressLine2 = strAddressLine2;
            fChildForm.strIncidentAddressLine3 = strAddressLine3;
            fChildForm.strIncidentAddressLine4 = strAddressLine4;
            fChildForm.strIncidentAddressLine5 = strAddressLine5;
            fChildForm.strIncidentPostcode = strPostcode;

            fChildForm.Show();

            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiSetSiteCentre_ItemClick(object sender, ItemClickEventArgs e)
        {
            // X and Y set in mouseHook_MouseUp //
            SetRecordCentre("site", decCurrentMouseX, decCurrentMouseY);
        }

        private void bbiSetClientCentre_ItemClick(object sender, ItemClickEventArgs e)
        {
            // X and Y set in mouseHook_MouseUp //
            SetRecordCentre("client", decCurrentMouseX, decCurrentMouseY);
        }

        private void SetRecordCentre(string strRecordType, decimal decX, decimal decY)
        {
            frm_AT_Mapping_Set_Centre_Point frm_child = new frm_AT_Mapping_Set_Centre_Point();
            frm_child.GlobalSettings = GlobalSettings;
            frm_child.strRecordType = strRecordType;
            this.ParentForm.AddOwnedForm(frm_child);
            if (frm_child.ShowDialog() == DialogResult.OK)
            {
                int intRecordID = frm_child.intRecordID;
                System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.InvariantCulture;  // Needed for converting to Title Case on MessageBox //
                System.Globalization.TextInfo textInfo = cultureInfo.TextInfo;  // Needed for converting to Title Case on MessageBox //
                try
                {
                    // Generate Lat Long values //
                    float flLatLongX = (float)0.00;
                    float flLatLongY = (float)0.00;
                    string strLatLongPolygonXY = "";
                    Mapping_Functions MapFunctions = new Mapping_Functions();
                    MapInfo.Geometry.FeatureGeometry ftr = MapFunctions.CreateLatLongFeatureFromNonLatLongCoords((float)decX, (float)decY, "");
                    if (ftr != null) MapFunctions.GetLatLongCoordsFromLatLongFeature(ftr, ref flLatLongX, ref flLatLongY, ref strLatLongPolygonXY);

                    // Update Database //
                    DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter UpdateRecord = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                    UpdateRecord.ChangeConnectionString(strConnectionString);
                    UpdateRecord.sp01319_AT_Tree_Picker_Set_Centre_Point(intRecordID, strRecordType, decX, decY, flLatLongX, flLatLongY);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to set the " + strRecordType + " centre point [" + ex.Message + "]!\n\nTry doing it again. If the problem persist contact Technical Support.", "Set " + textInfo.ToTitleCase(strRecordType) + " Centre Point", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (GlobalSettings.ShowConfirmations == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(textInfo.ToTitleCase(strRecordType) + " Centre Point Updated Successfully.", "Set " + textInfo.ToTitleCase(strRecordType) + " Centre Point", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        private void bbiTransferToWorkOrder_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            string strPassedTreeIDs = "";
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        strPassedTreeIDs += f[1].ToString() + ",";  // Tree IDs //
                    }
                }
            }
            if (string.IsNullOrEmpty(strPassedTreeIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more map objects containing actions before proceeding.", "Transfer Actions To Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            //bool boolSucceed = NotifyWoodPlanVersion4("workorder", strPassedParameterString);  // Call WoodPlan V.4 for Edit screens //

            // Get number of actions from selected map objects available for adding to work order //
            DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter GetSetting = new DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter();       
            GetSetting.ChangeConnectionString(strConnectionString);
            int intCount = Convert.ToInt32(GetSetting.sp01409_AT_Tree_Picker_WorkOrder_Add_Get_Action_Count(strPassedTreeIDs));
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The currently selected map objects have no actions available for linking to Work Orders.\n\nSelect one or more map objects containing actions before proceeding.", "Transfer Actions To Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check if only one Edit work Order screen is open - if yes, make sure the currently loaded Work Order has been saved (intWorkOrderID > 0) //
            Form frm_main;
            frm_main = this.MdiParent;
            frm_AT_WorkOrder_Edit fForm = new frm_AT_WorkOrder_Edit();
            int intFormCount = 0;
            string strWorkOrderIDs = "";
            int intWorkOrderID = 0;
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                if (frmChild.Name == "frm_AT_WorkOrder_Edit")
                {
                    fForm = (frm_AT_WorkOrder_Edit)frmChild;
                    if (fForm.Action_Adding_Allowed(ref strWorkOrderIDs)) 
                    {
                        intFormCount++;
                        intWorkOrderID = fForm.Return_ActionsID_To_Calling_Form();
                    }
                }
            }
            switch (intFormCount)
            {
                case 1:
                    {
                        // Step round the open forms to find the correct one then activate it //
                        foreach (Form frmChild in frm_main.MdiChildren)
                        {
                            if (frmChild.Name == "frm_AT_WorkOrder_Edit")
                            {
                                fForm = (frm_AT_WorkOrder_Edit)frmChild;
                                if (fForm.Return_ActionsID_To_Calling_Form() == intWorkOrderID)
                                {
                                    break;  // We have a reference to the correct form so exit loop. //
                                }
                            }
                        }
                        fForm.Activate();
                        fForm.Add_Record(strPassedTreeIDs);
                        break;
                    }
                case 0:
                    {
                        // Open a list of incomplete Work Orders to allow the user to pick the Work Order to add to //

                        frm_AT_Work_Order_Select_On_Map_Add fSelect = new frm_AT_Work_Order_Select_On_Map_Add();
                        this.ParentForm.AddOwnedForm(fSelect);
                        fSelect.GlobalSettings = this.GlobalSettings;
                        if (fSelect.ShowDialog() == DialogResult.OK)
                        {
                            int intSelectedWorkOrderID = fSelect.intSelectedID;
                            if (intSelectedWorkOrderID <= 0) return;

                            System.Reflection.MethodInfo method = null;
                            fProgress = new frmProgress(10);
                            this.AddOwnedForm(fProgress);
                            fProgress.Show();  // ***** Closed in PostOpen event ***** //
                            Application.DoEvents();

                            frm_AT_WorkOrder_Edit fChildForm = new frm_AT_WorkOrder_Edit();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = intSelectedWorkOrderID.ToString() + ",";
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = "frm_AT_WorkOrder_Manager";
                            fChildForm.intRecordCount = 1;
                            fChildForm.FormPermissions = this.FormPermissions;
                            fChildForm.fProgress = fProgress;
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                            fChildForm.Add_Record(strPassedTreeIDs);

                        }
                        break;
                    }
                default:  // More than 1 so open screen asking the user to select which work order to add to //
                    {
                        // Open a list of incomplete Work Orders that are currently open to allow the user to pick the Work Order to add to //

                        frm_AT_Work_Order_Select_On_Map_Add fSelect = new frm_AT_Work_Order_Select_On_Map_Add();
                        this.ParentForm.AddOwnedForm(fSelect);
                        fSelect.GlobalSettings = this.GlobalSettings;
                        fSelect.strPassedInWorkOrderIDs = strWorkOrderIDs;
                        if (fSelect.ShowDialog() == DialogResult.OK)
                        {
                            int intSelectedWorkOrderID = fSelect.intSelectedID;
                            if (intSelectedWorkOrderID <= 0) return;

                            foreach (Form frmChild in frm_main.MdiChildren)
                            {
                                if (frmChild.Name == "frm_AT_WorkOrder_Edit")
                                {
                                    fForm = (frm_AT_WorkOrder_Edit)frmChild;
                                    if (fForm.Return_ActionsID_To_Calling_Form() == intSelectedWorkOrderID)
                                    {
                                        break;  // We have a reference to the correct form so exit loop. //
                                    }
                                }
                            }
                            fForm.Activate();
                            fForm.Add_Record(strPassedTreeIDs);
                            break;
                        }
                        break;
                    }
            }

        }

        private void bbiNearbyObjectsFind_ItemClick(object sender, ItemClickEventArgs e)
        {
            // X and Y set in mouseHook_MouseUp //
            SearchForNearbyMapObjects(decCurrentMouseX, decCurrentMouseY, Convert.ToDecimal(beiGazetteerLoadObjectsWithinRange.EditValue), "No Address Specified");
        }

        private void SearchForNearbyMapObjects(decimal decX, decimal decY, decimal decRange, string strDescription)
        {
            WaitDialogForm loading = new WaitDialogForm("Checking For Nearby Map Objects...", "WoodPlan Mapping");
            loading.Show();

            //decimal decRangeAdjusted = (decRangeSpinner * (decimal)100) / (decimal)113.5;  // Gets round round size of circle drawn (always 13.5% too big) //
            decimal decRangeAdjusted = decRange;

            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp01320_AT_Gazetteer_Get_Map_Objects_In_Range", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@decX", decX));
            cmd.Parameters.Add(new SqlParameter("@decY", decY));
            cmd.Parameters.Add(new SqlParameter("@decRange", decRange));
            SqlDataAdapter sdaTreesToShow = new SqlDataAdapter(cmd);
            DataSet dsTreesToShow = new DataSet("NewDataSet");
            try
            {
                sdaTreesToShow.Fill(dsTreesToShow, "Table");
            }
            catch (Exception ex)
            {
                loading.Close();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Amenity Tree map objects within " + decRange.ToString() + (decRange == (decimal)1.00 ? " metre" : " metres") + " of the current address.\n\nError: " + ex.Message, "Load  Map Objects in Range", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;

            }

            SQlConn = new SqlConnection(strConnectionString);
            cmd = null;
            cmd = new SqlCommand("sp03075_AT_Gazetteer_Get_Asset_Map_Objects_In_Range", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@decX", decX));
            cmd.Parameters.Add(new SqlParameter("@decY", decY));
            cmd.Parameters.Add(new SqlParameter("@decRange", decRange));
            SqlDataAdapter sdaAssetsToShow = new SqlDataAdapter(cmd);
            DataSet dsAssetsToShow = new DataSet("NewDataSet");
            try
            {
                sdaAssetsToShow.Fill(dsAssetsToShow, "Table");
            }
            catch (Exception ex)
            {
                loading.Close();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Asset map objects within " + decRange.ToString() + (decRange == (decimal)1.00 ? " metre" : " metres") + " of the current address.\n\nError: " + ex.Message, "Load  Map Objects in Range", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;

            }

            if (dsTreesToShow.Tables[0].Rows.Count == 0 && dsAssetsToShow.Tables[0].Rows.Count == 0)
            {
                loading.Close();
                DevExpress.XtraEditors.XtraMessageBox.Show("There are no Map Objects to load within " + decRange.ToString() + (decRange == (decimal)1.00 ? " metre" : " metres") + " of the current location.", "Load  Map Objects in Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string strTreeIDs = "";
            foreach (DataRow dr in dsTreesToShow.Tables[0].Rows)
            {
                strTreeIDs += dr["TreeID"].ToString() + ",";
            }
            string strAssetIDs = "";
            foreach (DataRow dr in dsAssetsToShow.Tables[0].Rows)
            {
                strAssetIDs += dr["AssetID"].ToString() + ",";
            }
            // ***** GOT THIS FAR WITH UPDATE WITH ASSETS ***** //
            Mapping_Functions MapFunctions = new Mapping_Functions();
            MapFunctions.Gazetteer_Map_Object_Search(this.GlobalSettings, this.strConnectionString, this.ParentForm, strTreeIDs, "tree", strAssetIDs, "asset");

            Create_Gazetteer_Search_Temp_Feature(Convert.ToDouble(decX), Convert.ToDouble(decY), decRange, strDescription, decRangeAdjusted);
            loading.Close();

        }

        private void bbiOpen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "MapInfo TAB Files(*.tab)|*.tab|MapInfo Geoset Files(*.gst)|*.gst|MWS Files(*.mws)|*.mws";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    mapControl1.Map.Load(new MapGeosetLoader(dlg.FileName));
                }
                catch (Exception)
                {

                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to load the specified file", "Load File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            //mapControl1.Tools.LeftButtonTool = "Select";

        }

        private void bbiNone_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Arrow";
        }

        private void bbiPan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Pan";
        }

        private void bbiZoomIn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "ZoomIn";
        }

        private void bbiZoomOut_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "ZoomOut";
        }

        private void bbiCentre_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Center";
        }

        private void bbiSelect_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Select";
        }

        private void bbiSelectRectangle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "SelectRect";
        }

        private void bbiSelectRadius_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "SelectRadius";
        }

        private void bbiSelectPolygon_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "SelectPolygon";
        }

        private void bbiSelectRegion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "SelectRegion";
        }

        private void beiPlotObjectType_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem bei = (BarEditItem)sender;
            i_intSelectedObjectType = Convert.ToInt32(bei.EditValue);
            if (i_intSelectedObjectType == 0 || !iBool_AllowEdit) // No object type selected or no Add access so disable Plotting tools on toolbar //
            {
                bbiAddPoint.Enabled = false;
                bbiAddPolygon.Enabled = false;
                bbiAddPolyLine.Enabled = false;
                if (mapControl1.Tools.LeftButtonTool == "AddPoint" || mapControl1.Tools.LeftButtonTool == "AddPolyline" || mapControl1.Tools.LeftButtonTool == "AddPolygon") mapControl1.Tools.LeftButtonTool = "Arrow";
            }
            else // object type selected and Add access so enable Plotting tools on toolbar //
            {
                bbiAddPoint.Enabled = true;
                bbiAddPolygon.Enabled = true;
                bbiAddPolyLine.Enabled = true;
            }
        }

        private void bbiAddPolygon_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "AddPolygon";
        }

        private void bbiAddPolyLine_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "AddPolyline";
        }

        private void bbiAddPoint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "AddPoint";
        }

        private void bbiQueryTool_ItemClick(object sender, ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "InfoTool";
        }

        private void bciEditingNone_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciEditingNone.Checked) mapControl1.Tools.SelectMapToolProperties.EditMode = MapInfo.Tools.EditMode.None;
        }

        private void bciEditingMove_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciEditingMove.Checked) mapControl1.Tools.SelectMapToolProperties.EditMode = MapInfo.Tools.EditMode.Objects;
        }

        private void bciEditingAdd_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciEditingAdd.Checked) mapControl1.Tools.SelectMapToolProperties.EditMode = MapInfo.Tools.EditMode.AddNode;
        }

        private void bciEditingEdit_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciEditingEdit.Checked) mapControl1.Tools.SelectMapToolProperties.EditMode = MapInfo.Tools.EditMode.Nodes;
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Writing Map Objects table to a .Tab file //
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "MapInfo Tab File|*.Tab|ESRI Shape File|*.shp";
            saveFileDialog1.Title = "Save Map Object Layer as Native File";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:  // Tab File //                     
                        frmProgress fProgress = new frmProgress(10);

                        fProgress.UpdateCaption("Exporting Map Objects...");
                        fProgress.Show();
                        Application.DoEvents();

                        int intExportedFeatures = 0;
                        MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                        conn.Open();
                        MapInfo.Data.Table table = conn.Catalog.GetTable("MapObjects");
                        MapInfo.Data.TableInfoNative native = (MapInfo.Data.TableInfoNative)MapInfo.Data.TableInfoFactory.CreateFromFeatureCollection(MapInfo.Data.TableType.Native, table);

                        // Add extra columns to table //
                        native.Columns.Add(ColumnFactory.CreateStringColumn("GridReference", 50));
                        native.Columns.Add(ColumnFactory.CreateIntColumn("Distance"));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("HouseName", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Access", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Visibility", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Context", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Management", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("LegalStatus", 50));
                        native.Columns.Add(ColumnFactory.CreateDateTimeColumn("LastInspectionDate"));
                        native.Columns.Add(ColumnFactory.CreateIntColumn("InspectionCycle"));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("InspectionUnit", 50));
                        native.Columns.Add(ColumnFactory.CreateDateTimeColumn("NextInspectionDate"));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("GroundType", 50));
                        native.Columns.Add(ColumnFactory.CreateIntColumn("DBH"));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("DBHRange", 50));
                        native.Columns.Add(ColumnFactory.CreateDecimalColumn("Height", 18, 2));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("HeightRange", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("ProtectionType", 50));
                        native.Columns.Add(ColumnFactory.CreateDateTimeColumn("PlantDate"));
                        native.Columns.Add(ColumnFactory.CreateIntColumn("GroupNumber"));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("RiskCategory", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("SiteHazardClass", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("PlantSize", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("PlantSource", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("PlantMethod", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("PostCode", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("MapLabel", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Status", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Size", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Target1", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Target2", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Target3", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("User1", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("User2", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("User3", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("AgeClass", 50));
                        native.Columns.Add(ColumnFactory.CreateDecimalColumn("AreaM2", 18, 2));
                        native.Columns.Add(ColumnFactory.CreateIntColumn("ReplantCount"));
                        native.Columns.Add(ColumnFactory.CreateDateTimeColumn("SurveyDate"));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("TreeType", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("HedgeOwner", 50));
                        native.Columns.Add(ColumnFactory.CreateIntColumn("HedgeLength"));
                        native.Columns.Add(ColumnFactory.CreateDecimalColumn("GardenSize", 18, 2));
                        native.Columns.Add(ColumnFactory.CreateDecimalColumn("PropertyProximity", 18, 2));
                        native.Columns.Add(ColumnFactory.CreateDecimalColumn("SiteLevel", 18, 2));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("Situation", 50));
                        native.Columns.Add(ColumnFactory.CreateStringColumn("OwnershipName", 50));

                        native.TablePath = @saveFileDialog1.FileName;  // Set the name of the table and path where it will be saved //
                        MapInfo.Data.Table nativetable = null;
                        try
                        {
                            native.WriteTabFile();
                            nativetable = MapInfo.Engine.Session.Current.Catalog.CreateTable(native);
                            nativetable.Close();  // New table needs to be closed then re-opened before the data can be inserted into it //
                            nativetable = MapInfo.Engine.Session.Current.Catalog.OpenTable(@saveFileDialog1.FileName);
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            fProgress.Close(); // Close Progress Window //
                            fProgress = null;
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the files [" + ex.Message + "] - Process Aborted!\n\nTip: If the filename chosen for the saved file already exists and you have chosen to overwrite the file, make sure the existing version is not already open.", "Save Map Object Layer as TAB File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        /*
                        MapInfo.Data.MICommand com = conn.CreateCommand();
                        com = conn.CreateCommand();
                        MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                        com.CommandText = "Insert into " + nativetable.Alias + " (id,TreeID,TreeRef,ClientName,SiteName,Species,intX,intY,intObjectType,intHighlighted,CrownDiameter,CalculatedSize) Select id,intTreeID,strTreeRef,strClientName,strSiteName,strSpecies,intX,intY,intObjectType,intHighlighted,intCrownDiameter,strCalculatedSize from " + t.Alias;
                        com.Prepare();
                        com.ExecuteNonQuery();
                        com.Dispose();
                        */

                        MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                        MapInfo.Data.MICommand command = conn.CreateCommand();
                        command.CommandText = "Select * from " + t.Alias;
                        MapInfo.Data.IResultSetFeatureCollection irfc = command.ExecuteFeatureCollection();

                        int intUpdateProgressThreshhold = irfc.Count / 10;
                        int intUpdateProgressTempCount = 0;

                        string GridReference = "";
                        int Distance = 0;
                        string HouseName = "";
                        string Access = "";
                        string Visibility = "";
                        string Context = "";
                        string Management = "";
                        string LegalStatus = "";
                        DateTime LastInspectionDate;
                        int InspectionCycle = 0;
                        string InspectionUnit = "";
                        DateTime NextInspectionDate;
                        string GroundType = "";
                        int DBH = 0;
                        string DBHRange = "";
                        decimal Height;
                        string HeightRange = "";
                        string ProtectionType = "";
                        DateTime PlantDate;
                        int GroupNumber = 0;
                        string RiskCategory = "";
                        string SiteHazardClass = "";
                        string PlantSize = "";
                        string PlantSource = "";
                        string PlantMethod = "";
                        string Postcode = "";
                        string MapLabel = "";
                        string Status = "";
                        string Size = "";
                        string Target1 = "";
                        string Target2 = "";
                        string Target3 = "";
                        string User1 = "";
                        string User2 = "";
                        string User3 = "";
                        string AgeClass = "";
                        decimal AreaM2;
                        int ReplantCount = 0;
                        DateTime SurveyDate;
                        string TreeType = "";
                        string HedgeOwner = "";
                        int HedgeLength = 0;
                        decimal GardenSize;
                        decimal PropertyProximity;
                        decimal SiteLevel;
                        string Situation = "";
                        string OwnershipName = "";

                        string strMapID = "";
                        int intRowHandle = 0;
                        GridView view = (GridView)gridControl2.MainView;
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["MapID"];
                        MapInfo.Data.MICommand com = conn.CreateCommand();
                        com = conn.CreateCommand();

                        //System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.InvariantCulture;  // Needed for converting Dates //

                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            intExportedFeatures++;
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                            nativetable.InsertFeature(f);  // Add feature to table //

                            strMapID = f["id"].ToString();
                            if (string.IsNullOrEmpty(strMapID)) continue;  // Go to next iteration of loop //

                            // Get additional data from MapObjects Grid and update new feature //
                            intRowHandle = view.LocateByValue(0, col, strMapID);
                            if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) continue;  // Go to next iteration of loop //

                            GridReference = view.GetRowCellValue(intRowHandle, "GridReference").ToString().Replace("'", "\"");
                            Distance = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "Distance").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Distance")));
                            HouseName = view.GetRowCellValue(intRowHandle, "HouseName").ToString().Replace("'", "\"");
                            Access = view.GetRowCellValue(intRowHandle, "Access").ToString().Replace("'", "\"");
                            Visibility = view.GetRowCellValue(intRowHandle, "Visibility").ToString().Replace("'", "\"");
                            Context = view.GetRowCellValue(intRowHandle, "Context").ToString().Replace("'", "\"");
                            Management = view.GetRowCellValue(intRowHandle, "Management").ToString().Replace("'", "\"");
                            LegalStatus = view.GetRowCellValue(intRowHandle, "LegalStatus").ToString().Replace("'", "\"");
                            LastInspectionDate = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "LastInspectionDate").ToString()) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "LastInspectionDate")));
                            InspectionCycle = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "InspectionCycle").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "InspectionCycle")));
                            InspectionUnit = view.GetRowCellValue(intRowHandle, "InspectionUnit").ToString().Replace("'", "\"");
                            NextInspectionDate = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "NextInspectionDate").ToString()) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "NextInspectionDate")));
                            GroundType = view.GetRowCellValue(intRowHandle, "GroundType").ToString().Replace("'", "\"");
                            DBH = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "DBH").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "DBH")));
                            DBHRange = view.GetRowCellValue(intRowHandle, "DBHRange").ToString().Replace("'", "\"");
                            Height = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "Height").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Height")));
                            HeightRange = view.GetRowCellValue(intRowHandle, "HeightRange").ToString().Replace("'", "\"");
                            ProtectionType = view.GetRowCellValue(intRowHandle, "ProtectionType").ToString().Replace("'", "\"");
                            PlantDate = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "PlantDate").ToString()) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "PlantDate")));
                            GroupNumber = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "GroupNumber").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "GroupNumber")));
                            RiskCategory = view.GetRowCellValue(intRowHandle, "RiskCategory").ToString().Replace("'", "\"");
                            SiteHazardClass = view.GetRowCellValue(intRowHandle, "SiteHazardClass").ToString().Replace("'", "\"");
                            PlantSize = view.GetRowCellValue(intRowHandle, "PlantSize").ToString().Replace("'", "\"");
                            PlantSource = view.GetRowCellValue(intRowHandle, "PlantSource").ToString().Replace("'", "\"");
                            PlantMethod = view.GetRowCellValue(intRowHandle, "PlantMethod").ToString().Replace("'", "\"");
                            Postcode = view.GetRowCellValue(intRowHandle, "Postcode").ToString().Replace("'", "\"");
                            MapLabel = view.GetRowCellValue(intRowHandle, "MapLabel").ToString().Replace("'", "\"");
                            Status = view.GetRowCellValue(intRowHandle, "Status").ToString().Replace("'", "\"");
                            Size = view.GetRowCellValue(intRowHandle, "Size").ToString().Replace("'", "\"");
                            Target1 = view.GetRowCellValue(intRowHandle, "Target1").ToString().Replace("'", "\"");
                            Target2 = view.GetRowCellValue(intRowHandle, "Target2").ToString().Replace("'", "\"");
                            Target3 = view.GetRowCellValue(intRowHandle, "Target3").ToString().Replace("'", "\"");
                            User1 = view.GetRowCellValue(intRowHandle, "User1").ToString().Replace("'", "\"");
                            User2 = view.GetRowCellValue(intRowHandle, "User2").ToString().Replace("'", "\"");
                            User3 = view.GetRowCellValue(intRowHandle, "User3").ToString().Replace("'", "\"");
                            AgeClass = view.GetRowCellValue(intRowHandle, "AgeClass").ToString().Replace("'", "\"");
                            AreaM2 = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "AreaHa").ToString()) ? (decimal)0.00 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "AreaHa")));
                            ReplantCount = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "ReplantCount").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ReplantCount")));
                            SurveyDate = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "SurveyDate").ToString()) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "SurveyDate")));
                            TreeType = view.GetRowCellValue(intRowHandle, "TreeType").ToString().Replace("'", "\"");
                            HedgeOwner = view.GetRowCellValue(intRowHandle, "HedgeOwner").ToString().Replace("'", "\"");
                            HedgeLength = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "HedgeLength").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "HedgeLength")));
                            GardenSize = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "GardenSize").ToString()) ? (decimal)0.00 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "GardenSize")));
                            PropertyProximity = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "PropertyProximity").ToString()) ? (decimal)0.00 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "PropertyProximity")));
                            SiteLevel = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "SiteLevel").ToString()) ? (decimal)0.00 : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteLevel")));
                            Situation = view.GetRowCellValue(intRowHandle, "Situation").ToString().Replace("'", "\"");
                            OwnershipName = view.GetRowCellValue(intRowHandle, "OwnershipName").ToString().Replace("'", "\"");

                            com.CommandText = "Update " + nativetable.Alias + " set GridReference='" + GridReference + "',"
                                + "Distance=" + Distance.ToString() + ","
                                + "Access='" + Access + "',"
                                + "Visibility='" + Visibility + "',"
                                + "Context='" + Context + "',"
                                + "LegalStatus='" + LegalStatus + "',"
                                + "LastInspectionDate='" + LastInspectionDate.ToString("dd/MM/yyyy HH:mm:ss") + "',"  // Notice formating has month before day! //
                                + "InspectionCycle=" + InspectionCycle.ToString() + ","
                                + "InspectionUnit='" + InspectionUnit + "',"
                                + "NextInspectionDate='" + NextInspectionDate.ToString("dd/MM/yyyy HH:mm:ss") + "',"  // Notice formating has month before day! //
                                + "DBH=" + DBH.ToString() + ","
                                + "DBHRange='" + DBHRange + "',"
                                + "Height=" + Height.ToString() + ","
                                + "HeightRange='" + HeightRange + "',"
                                + "ProtectionType='" + ProtectionType + "',"
                                + "PlantDate='" + PlantDate.ToString("dd/MM/yyyy HH:mm:ss") + "',"  // Notice formating has month before day! //
                                + "GroupNumber=" + GroupNumber.ToString() + ","
                                + "RiskCategory='" + RiskCategory + "',"
                                + "SiteHazardClass='" + SiteHazardClass +"',"
                                + "PlantSize='" + PlantSize + "',"
                                + "PlantSource='" + PlantSource + "',"
                                + "PlantMethod='" + PlantMethod + "',"
                                + "Postcode='" + Postcode + "',"
                                + "MapLabel='" + MapLabel + "',"
                                + "Status='" + Status + "',"
                                + "Size='" + Size + "',"
                                + "Target1='" + Target1 + "',"
                                + "Target2='" + Target2 + "',"
                                + "Target3='" + Target3 + "',"
                                + "User1='" + User1 + "',"
                                + "User2='" + User2 + "',"
                                + "User3='" + User3 + "',"
                                + "AgeClass='" + AgeClass + "',"
                                + "AreaM2=" + AreaM2.ToString() + ","
                                + "ReplantCount=" + ReplantCount.ToString() + ","
                                + "SurveyDate='" + SurveyDate.ToString("dd/MM/yyyy HH:mm:ss") + "',"  // Notice formating has month before day! //
                                + "TreeType='" + TreeType + "',"
                                + "HedgeOwner='" + HedgeOwner + "',"
                                + "HedgeLength=" + HedgeLength.ToString() + ","
                                + "GardenSize=" + GardenSize.ToString() + ","
                                + "PropertyProximity=" + PropertyProximity.ToString() + ","
                                + "SiteLevel=" + SiteLevel.ToString() + ","
                                + "Situation='" + Situation + "',"
                                + "OwnershipName='" + OwnershipName + "'"
                                    + " where id='" + strMapID + "'";
                            com.Prepare();
                            com.ExecuteNonQuery();
                        }
                        command.Dispose();
                        com.Dispose();


                        conn.Close();
                        conn.Dispose();
                        fProgress.SetProgressValue(100);
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                        if (intExportedFeatures == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No Map Objects exported to new TAB File!\n\nTip: Only map objects loaded into the map are exported - currently, the map has no loaded map objects.", "Save Map Object Layer as TAB File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        else if (GlobalSettings.ShowConfirmations == 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show((intExportedFeatures == 1 ? "1 Map Object" : intExportedFeatures.ToString() + " Map Objects") + " exported to new TAB File.", "Save Map Object Layer as TAB File", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    default:  // Shape File //
                        DevExpress.XtraEditors.XtraMessageBox.Show("Saving as Shape Files not yet supported - Save Aborted.", "Save Map Object Layer as SHP File", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                        break;
                }
            }
        }

        private void bbiMeasureLine_ItemClick(object sender, ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Distance";  // Custom Tool //
        }

        private void bciMapMarkerAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "MapMarkerTool";  // Custom Tool //
        }

        private void bbiWorkOrderMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            frm_AT_Mapping_WorkOrder_Map_Step1 frm_child = new frm_AT_Mapping_WorkOrder_Map_Step1();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child.i_str_selected_workorders = this.i_str_selected_workorders;
            frm_child.frmParentMappingForm = this;
            frm_child.intCurrentMapScale = Convert.ToInt32(dCurrentMapScale);
            this.ParentForm.AddOwnedForm(frm_child);

            // Get current DPI from screen //
            Graphics dspGraphics = CreateGraphics();
            int intDPI = Convert.ToInt32(dspGraphics.DpiX);
            dspGraphics.Dispose();
            frm_child.intDPI = intDPI;

            frm_child.ShowDialog();
        }

        public void Generate_Work_Order_Map(ref Image image)  // CALLED BY frm_AT_Mapping_WorkOrder_Map_Step1 FORM //
        {
            // Export a a bigger map than what is currently shown on the screen //
            MapInfo.Geometry.DPoint dpt1 = new MapInfo.Geometry.DPoint(mapControl1.Map.Center.x, mapControl1.Map.Center.y);  // create a Dpoint to center the map on //
            MapInfo.Mapping.Map map = (MapInfo.Mapping.Map)mapControl1.Map.Clone();  // clone the map window //
            using (MapInfo.Mapping.MapExport export = new MapInfo.Mapping.MapExport(map))
            {
                // set the size for the exported image //  
                // Get current DPI from screen //
                Graphics dspGraphics = CreateGraphics();
                int intDPI = Convert.ToInt32(dspGraphics.DpiX);
                dspGraphics.Dispose();
                export.ExportSize = new MapInfo.Mapping.ExportSize(3000, 3000, (short)intDPI);
                //300);
                export.Map.SetView(dpt1, mapControl1.Map.GetDisplayCoordSys(), mapControl1.Map.Scale);
                // set the scale for the exported map //
                export.Format = MapInfo.Mapping.ExportFormat.Jpeg;
                // set the image format //
                image = export.Export();
            }  // perform the export to an Image object //
            map = null;
            return;
        }

        private void frm_AT_Mapping_Tree_Picker_MouseClick(object sender, MouseEventArgs e)
        {
            string strButton = e.Button.ToString();
        }

        private void bbiPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            PrintMap();
        }

        private void PrintMap()
        {
            WaitDialogForm loading = new WaitDialogForm("Generating Map Preview...", "WoodPlan Mapping");
            loading.Show();

            Image image = null;
            this.Generate_Work_Order_Map(ref image);
            using (frm_AT_Mapping_WorkOrder_Map frm_child = new frm_AT_Mapping_WorkOrder_Map())
            {
                frm_child.GlobalSettings = this.GlobalSettings;
                frm_child.strFormMode = "View";
                frm_child.imageMap = image;
                frm_child.frmParentMappingForm = this;
                frm_child.intCurrentMapScale = Convert.ToInt32(dCurrentMapScale);
                // Get current DPI from screen //
                Graphics dspGraphics = CreateGraphics();
                int intDPI = Convert.ToInt32(dspGraphics.DpiX);
                dspGraphics.Dispose();
                frm_child.intDPI = intDPI;
                loading.Close();
                frm_child.ShowDialog();
            }
            image = null;
            GC.GetTotalMemory(true);
        }



        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add_Record("inspection");
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            if (!iBool_AllowAdd) return;

            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 6:     // Inspections //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        if (i_intMapObjectsSelected == 1)  // Only one Map Object selected so get it's ID and reference for Edit form //
                        {
                            // Get all selected items and iterate through them //
                            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
                            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

                            while (enum1.MoveNext())
                            {
                                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers excpet MapObjects //
                                {
                                    string strSelectedIDs = "";
                                    foreach (MapInfo.Data.Feature f in irfc)
                                    {
                                        strSelectedIDs += f["TreeID"].ToString() + ",";
                                        strSelectedIDs += f["TreeRef"].ToString() + ",";
                                        fChildForm.intLinkedToRecordID = Convert.ToInt32(f["TreeID"]);
                                        fChildForm.strLinkedToRecordDesc = f["TreeRef"].ToString();
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            int[] intRowHandles;
                            intRowHandles = view.GetSelectedRows();
                            if (intRowHandles.Length == 1)
                            {
                                fChildForm.intLinkedToRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "TreeID"));
                                fChildForm.strLinkedToRecordDesc = view.GetRowCellValue(intRowHandles[0], "TreeReference").ToString();
                            }
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 7:     // Actions //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl7.MainView;
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        GridView ParentView = (GridView)gridControl6.MainView;
                        int[] intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "InspectionID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "InspectionReference"));
                            fChildForm.intParentOwnershipID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "TreeOwnershipID"));
                            fChildForm.strParentNearestHouse = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "TreeHouseName"));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add_Record(string strType)
        {
            if (!iBool_AllowAdd) return;

            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            int intCount = 0;
            string strRecordIDs = "";
            string strFriendlyTreeReferences = "";

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers excpet MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        intCount++;
                        strRecordIDs += f["TreeID"].ToString() + ",";
                        strFriendlyTreeReferences += f["TreeRef"].ToString() + "\r\n";
                    }
                }
            }

            switch (strType)
            {
                case "inspection":     // Inspections //
                    {
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case "map_actions":
                case "inspection_actions":
                    {
                        fProgress = null;
                        method = null;
                        string strInspectionIDs = "";
                        switch (strType)
                        {
                            case "map_actions":     // Trees - Block Add Actions //
                                {
                                    if (intCount <= 0)
                                    {
                                        DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one map object to block add actions to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }
                                    fProgress = new frmProgress(10);
                                    this.AddOwnedForm(fProgress);
                                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                                    Application.DoEvents();

                                    frm_AT_Inspection_Block_Add_Get_Core_Details fChildForm = new frm_AT_Inspection_Block_Add_Get_Core_Details();
                                    fChildForm.GlobalSettings = this.GlobalSettings;
                                    fChildForm.strSelectedTreeIDs = strFriendlyTreeReferences;

                                    if (fProgress != null)
                                    {
                                        fProgress.UpdateProgress(10); // Update Progress Bar //
                                        fProgress.Close();
                                        fProgress = null;
                                    }
                                    Application.DoEvents();  // Allow Form time to repaint itself //

                                    if (fChildForm.ShowDialog() != DialogResult.OK) return;
                                    // OK, so Block Add 1 inspection for each tree then pick up the new Inspections IDs and pass them to the Block Add Action screen //
                                    string strSelectedInspectionReference = fChildForm.strSelectedInspectionReference;
                                    int intSelectedInspector = fChildForm.intSelectedInspector;
                                    DateTime dtSelectedInspectionDate = fChildForm.dtSelectedInspectionDate;
                                    int intSelectedGeneralCondition = fChildForm.intSelectedGeneralCondition;
                                    string strSelectedRemarks = fChildForm.strSelectedRemarks;

                                    try
                                    {

                                        DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter AddInspections = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                                        strInspectionIDs = AddInspections.sp01394_AT_Inspection_Block_Add_From_Trees(strRecordIDs, strSelectedInspectionReference, intSelectedInspector, dtSelectedInspectionDate, intSelectedGeneralCondition, strSelectedRemarks).ToString() ?? "";
                                    }
                                    catch (Exception ex)
                                    {
                                        XtraMessageBox.Show("An error occurred [" + ex.Message + "] while block adding the Dummy Inspection(s)!\n\nYou can try again. If the problem persists contact Technical Support.\n\nBlock Add Action Record(s) Aborted!", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }
                                    if (string.IsNullOrEmpty(strInspectionIDs))
                                    {
                                        DevExpress.XtraEditors.XtraMessageBox.Show("No Dummy Inspections were generated - unable to proceed with Block Add of Actions.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }
                                    strInspectionIDs = strInspectionIDs.Replace(';', ',');  // Swap semi-colons for commas //
                                }
                                break;
                            case "inspection_actions":     //  - Block Add Actions //
                                {

                                    GridView view = (GridView)gridControl6.MainView;
                                    view.PostEditor();
                                    int[] intRowHandles = view.GetSelectedRows();
                                    intCount = intRowHandles.Length;
                                    if (intCount <= 0)
                                    {
                                        DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one inspection record to block add actions to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }
                                    foreach (int intRowHandle in intRowHandles)
                                    {
                                        strInspectionIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                                    }
                                }
                                break;
                            default:
                                return;
                        }

                        // Dummy inspections were generated so open Block Add Actions screen //

                        var fChildForm2 = new frm_AT_Action_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strInspectionIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = "frm_AT_Mapping_Tree_Picker";
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm2.splashScreenManager = splashScreenManager1;
                        fChildForm2.splashScreenManager.ShowWaitForm();
                        fChildForm2.strBlockAddedDummyInspectionIDs = (strType == "map_actions" ? strInspectionIDs : "");  // Only pass IDs if Inspections were created so Inspection level is refreshed by Edit Action screen //
                        fChildForm2.strParentTreeIDs = strRecordIDs;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            if (!iBool_AllowEdit) return;

            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 6:     // Inspections //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 7:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl7.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            if (!iBool_AllowEdit) return;

            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 6:     // Inspections //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 7:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl7.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_AT_Mapping_Tree_Picker";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            if (!iBool_AllowDelete) return;

            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 6:  // Inspection //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl6.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Inspections to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Inspection" : Convert.ToString(intRowHandles.Length) + " Inspections") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this inspection" : "these inspections") + " will no longer be available for selection and any related Actions will also be deleted!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "InspectionID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        RemoveRecords.sp01383_AT_Inspection_Manager_Delete("inspection", strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        Process_Selected_Map_Objects();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }

                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Inspection_Refresh(this.ParentForm, this.Name, "", "");

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 7:  // Action //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl7.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Actions to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Action" : Convert.ToString(intRowHandles.Length) + " Actions") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this action" : "these actions") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ActionID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        RemoveRecords.sp01387_AT_Action_Manager_Delete("action", strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        Process_Selected_Map_Objects();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }

                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Action_Refresh(this.ParentForm, this.Name, "", "");
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strInspectionIDs = view.GetRowCellValue(view.FocusedRowHandle, "InspectionID").ToString() + ",";
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strRecordIDs = "";
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedActionCount":
                    strRecordIDs = GetSetting.sp01333_AT_Inspection_Manager_Get_Linked_Action_IDs(strInspectionIDs, 0).ToString();
                    break;
                case "LinkedOutstandingActionCount":
                    strRecordIDs = GetSetting.sp01333_AT_Inspection_Manager_Get_Linked_Action_IDs(strInspectionIDs, 1).ToString();
                    break;
                default:
                    break;
            }
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "inspection");

        }

        private void dockPanel2_Click(object sender, EventArgs e)
        {

        }







 
        /*
        //Global Variables 
        MapExport _PrintExport = null;
        Image _PrintImage = null;
        System.Drawing.Rectangle _MapPrintArea;
        int _CurrentPrintImageX = 0;
        int _CurrentPrintImageY = 0;
        string _CurrentMapPrintTitle = "Test Print";

        private void Print()
        {
            try
            {
                //Initialise the Print Variables 
                this._MapPrintArea = new System.Drawing.Rectangle();

                //Initialise the Print Document 
                PrintDocument printDoc = new System.Drawing.Printing.PrintDocument();
                printDoc.PrinterSettings.PrintRange = PrintRange.AllPages;
                printDoc.PrintPage += new PrintPageEventHandler(PrintDocument_PrintPage);
                printDoc.BeginPrint += new PrintEventHandler(PrintDocument_BeginPrint);

                //Set the Print Area for the Map to the paper size taking into account the margins. 
                this._MapPrintArea.Height = printDoc.DefaultPageSettings.Bounds.Height - (printDoc.DefaultPageSettings.Margins.Top + printDoc.DefaultPageSettings.Margins.Bottom);
                this._MapPrintArea.Width = printDoc.DefaultPageSettings.Bounds.Width - (printDoc.DefaultPageSettings.Margins.Left + printDoc.DefaultPageSettings.Margins.Right);
                this._MapPrintArea.X = printDoc.DefaultPageSettings.Margins.Left;
                this._MapPrintArea.Y = printDoc.DefaultPageSettings.Margins.Top;

                //Set the Preview Dialog 
                PrintPreviewDialog previewDialog = new PrintPreviewDialog();
                previewDialog.Document = printDoc;
                previewDialog.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Print Failed");
            }
        }

        private void PrintDocument_BeginPrint(object sender, PrintEventArgs e)
        {
            try
            {
                //Reset the Print Variables 
                this._PrintExport = null;
                this._PrintImage = null;
                this._CurrentPrintImageX = 0;
                this._CurrentPrintImageY = 0;

                //Scale 1:25000 as define 0.25km per 1 cm. 
                //double scale = 0.25;
                double scale = 5;

                //Generate a new Map Export File. 
                this._PrintExport = new MapExport(this.mapControl1.Map.Clone() as Map);

                //Determine the distance that the current Map View occupies in Kms so we can work out the scale. 
                DPoint[] xPoints = new DPoint[2];
                xPoints[0] = new DPoint(this._PrintExport.Map.Bounds.x1, this._PrintExport.Map.Bounds.y1);
                xPoints[1] = new DPoint(this._PrintExport.Map.Bounds.x2, this._PrintExport.Map.Bounds.y1);
                Curve curve = new Curve(this._PrintExport.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, xPoints);
                //double widthKms = curve.Length(DistanceUnit.Kilometer, DistanceType.Spherical);
                double widthKms = curve.Length(DistanceUnit.Meter, DistanceType.Spherical);

                DPoint[] yPoints = new DPoint[2];
                yPoints[0] = new DPoint(this._PrintExport.Map.Bounds.x1, this._PrintExport.Map.Bounds.y1);
                yPoints[1] = new DPoint(this._PrintExport.Map.Bounds.x1, this._PrintExport.Map.Bounds.y2);
                curve = new Curve(this._PrintExport.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, yPoints);
                //double heightKms = curve.Length(DistanceUnit.Kilometer, DistanceType.Spherical);
                double heightKms = curve.Length(DistanceUnit.Meter, DistanceType.Spherical);

                //Use the export size feature which allows for the generated to be define in paper units. 
                //This will allow use to calculate the printed scale 
                this._PrintExport.ExportSize = new ExportSize((widthKms / scale), (heightKms / scale), MapInfo.Geometry.PaperUnit.Centimeter);

                //Export the Print Image. 
                this._PrintImage = this._PrintExport.Export();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to export and print the map at the selected Scale.  The generated file maybe too big.", "Print Failed");
            }
        }

        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                // Add header text to each page. 
                Graphics g = e.Graphics;
                //Image logo = (Image)Properties.Resources.Logo;
                string datum = this.mapControl1.Map.GetDisplayCoordSys().Datum.ToString();
                //g.DrawImage(logo, 10, 10);
                g.DrawString(this._CurrentMapPrintTitle, new System.Drawing.Font("Arial", 12, FontStyle.Regular), Brushes.Black, 50, 20);
                g.DrawString(datum, new System.Drawing.Font("Arial", 10, FontStyle.Regular), Brushes.Black, ((e.PageBounds.Width / 2) - ((datum.Length * 10) / 2)), 20);
                g.DrawString("Printed: " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm tt"), new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, (e.PageBounds.Right - 350), 20);
                g.DrawString("By: " + this.GlobalSettings.Username, new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, (e.PageBounds.Right - 350), 35);

                //Do the custom printing for when the user has requested scaling... 
                //This code was taken from another another MapInfo forum article.  It prints the area of the export image that can fit into 
                //print area.  It then moves to the next unprinted section of the export image. 
                e.Graphics.DrawImage(this._PrintImage, this._MapPrintArea, this._CurrentPrintImageX, this._CurrentPrintImageY, this._MapPrintArea.Width, this._MapPrintArea.Height, System.Drawing.GraphicsUnit.Pixel);
                e.HasMorePages = true;
                if (this._CurrentPrintImageY < this._PrintImage.Height - this._MapPrintArea.Height) this._CurrentPrintImageY += this._MapPrintArea.Height;
                else if (this._CurrentPrintImageX < this._PrintImage.Width - this._MapPrintArea.Width)
                {
                    this._CurrentPrintImageX += this._MapPrintArea.Width;
                    this._CurrentPrintImageY = 0;
                }
                else e.HasMorePages = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Print Failed");
            }
        }

  */
    }



    internal class SelectedAreaModifier : MapInfo.Mapping.FeatureStyleModifier
    {
        private System.Collections.Hashtable features;
        private int intBackColour;
        private int intTransparencyLevel;

        public SelectedAreaModifier(string name, string alias, MapInfo.Data.IResultSetFeatureCollection irfc, int intColour, int intTransparancy)
            : base(name, alias)
        {
            features = new System.Collections.Hashtable();
            string[] exp = new string[] { "MI_Key" };
            this.Expressions = exp;

            foreach (MapInfo.Data.Feature f in irfc)
            {
                features.Add(f.Key.Value, f.Key.Value);
            }
            intBackColour = intColour;
            intTransparencyLevel = intTransparancy;
        }

        protected override bool Modify(MapInfo.Styles.FeatureStyleStack styles, object[] values)
        {
            MapInfo.Styles.CompositeStyle cs = styles.Current;

            SimpleVectorPointStyle _vectorSymbol = new SimpleVectorPointStyle();
            _vectorSymbol.Code = (short)49;  // 49 = Cross //
            _vectorSymbol.Color = Color.FromArgb(0, 0, 255);
            _vectorSymbol.PointSize = (short)30;

            if (features.Contains((values[0] as MapInfo.Data.Key).Value))
            {
                // Set the style for region features because we are applying the modifier to a region layer.
                (cs.AreaStyle.Interior as MapInfo.Styles.SimpleInterior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intTransparencyLevel) / 100))), Color.FromArgb(intBackColour));
                cs.SymbolStyle = _vectorSymbol;
                return true;
            }
            return false;
        }
    }


    // Overview Map Class //
    public class MapOverviewAdornment : MapInfo.Mapping.IAdornment
    {
        private string _Alias;
        private string _Name;
        private System.Drawing.Size _Size;
        private System.Drawing.Point _Loc;
        private MapInfo.Mapping.Map _Map;
        private string _FilePath;

        public event ViewChangedEventHandler ViewChangedEvent;

        public void MapAdornment(string alias, string name, System.Drawing.Size size, System.Drawing.Point loc, MapInfo.Mapping.Map map, string filepath)
        {
            _Alias = alias;
            _Name = name;
            _Size = size;
            _Loc = loc;
            _Map = map;
            _FilePath = filepath;

            MapInfo.Mapping.MapExport ExportObject = new MapInfo.Mapping.MapExport(_Map.Clone() as MapInfo.Mapping.Map);
            ExportObject.ExportSize = new MapInfo.Mapping.ExportSize(150, 150);
            ExportObject.Format = MapInfo.Mapping.ExportFormat.Bmp;
            ExportObject.Export(filepath);
            ExportObject.Dispose();
        }

        string IAdornment.Alias
        {
            get
            {
                return _Alias;
            }
            set
            {
                _Alias = value;
            }
        }

        string IAdornment.Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        System.Drawing.Size IAdornment.Size
        {
            get
            {
                return _Size;
            }
            set
            {
                _Size = value;
            }
        }

        System.Drawing.Point IAdornment.Location
        {
            get
            {
                return _Loc;
            }
            set
            {
                _Loc = value;
            }
        }

        MapInfo.Mapping.Map IAdornment.Map
        {
            get
            {
                return _Map;
            }
        }

        public void Draw(System.Drawing.Graphics graphics, System.Drawing.Rectangle updateArea, System.Drawing.Point drawPnt)
        {
            Bitmap img = new Bitmap(_FilePath);
            graphics.DrawImage(img, drawPnt);
        }
    }



}
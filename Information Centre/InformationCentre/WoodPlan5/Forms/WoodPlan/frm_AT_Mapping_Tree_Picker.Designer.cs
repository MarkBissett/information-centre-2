namespace WoodPlan5
{
    partial class frm_AT_Mapping_Tree_Picker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                try
                {
                    components.Dispose();

                }
                catch (System.Exception)
                {

                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling29 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip23 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem23 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem23 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip24 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem24 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem24 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip25 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem25 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem25 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip26 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem26 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem26 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip28 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem28 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem28 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip27 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem27 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem27 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip29 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem29 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem29 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip30 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem30 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem30 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip31 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem31 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem31 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip32 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem32 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem32 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject65 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject66 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject67 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject68 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip33 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem33 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem33 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip34 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem34 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem34 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip35 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem35 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem35 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip36 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem36 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem36 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject57 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject58 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject59 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject60 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip37 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem37 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem37 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject61 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject62 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject63 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject64 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip38 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem38 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem38 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip39 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem39 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem39 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling28 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip40 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem40 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem40 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip41 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem41 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem41 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip43 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem43 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem43 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip44 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem44 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem44 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip42 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem42 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem42 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Mapping_Tree_Picker));
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01312ATTreePickerGazetteerSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_TreePicker = new WoodPlan5.DataSet_AT_TreePicker();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.mapToolBar1 = new MapInfo.Windows.Controls.MapToolBar();
            this.mapToolBarButtonOpenTable = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonLayerControl = new MapInfo.Windows.Controls.MapToolBarButton();
            this.toolBarButtonSperator1 = new System.Windows.Forms.ToolBarButton();
            this.mapToolBarButtonArrow = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonZoomIn = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonZoomOut = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonCenter = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonPan = new MapInfo.Windows.Controls.MapToolBarButton();
            this.toolBarButtonSeperator2 = new System.Windows.Forms.ToolBarButton();
            this.mapToolBarButtonSelect = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonSelectRectangle = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonSelectRadius = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonSelectPolygon = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonSelectRegion = new MapInfo.Windows.Controls.MapToolBarButton();
            this.toolBarButtonSeperator3 = new System.Windows.Forms.ToolBarButton();
            this.mapToolBarButtonAddPoint = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButton1 = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonAddPolyLine = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButton2 = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapControl1 = new MapInfo.Windows.Controls.MapControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiPlotObjectType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp03068TreePickerPlottingObjectTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colModuleDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bbiAddPoint = new DevExpress.XtraBars.BarCheckItem();
            this.bbiAddPolygon = new DevExpress.XtraBars.BarCheckItem();
            this.bbiAddPolyLine = new DevExpress.XtraBars.BarCheckItem();
            this.bbiNone = new DevExpress.XtraBars.BarCheckItem();
            this.bbiZoomIn = new DevExpress.XtraBars.BarCheckItem();
            this.bbiZoomOut = new DevExpress.XtraBars.BarCheckItem();
            this.bbiPan = new DevExpress.XtraBars.BarCheckItem();
            this.bbiCentre = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelect = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelectRectangle = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelectRadius = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelectPolygon = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelectRegion = new DevExpress.XtraBars.BarCheckItem();
            this.bbiMapSelection = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu_MapControl1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bsiEditMapObject = new DevExpress.XtraBars.BarSubItem();
            this.bbiEditSelectedMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockEditselectedMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.bsiAddToMapObjects = new DevExpress.XtraBars.BarSubItem();
            this.bbiAddInspection = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddAction = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.bsiMapNearbyObjects = new DevExpress.XtraBars.BarSubItem();
            this.beiGazetteerLoadObjectsWithinRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEditGazetteerRange = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bbiNearbyObjectsFind = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGazetteerClearMapSearch = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateIncidentFromMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateDatasetFromMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTransferToWorkOrder = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.bbiCreatePolygonFromMapMarkers = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreatePolylineFromMapMarkers = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteSelectedTempMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteAllMapMarkers = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSetCentrePoint = new DevExpress.XtraBars.BarSubItem();
            this.bbiSetSiteCentre = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetClientCentre = new DevExpress.XtraBars.BarButtonItem();
            this.bbiQueryTool = new DevExpress.XtraBars.BarCheckItem();
            this.bbiMeasureLine = new DevExpress.XtraBars.BarCheckItem();
            this.bciMapMarkerAdd = new DevExpress.XtraBars.BarCheckItem();
            this.bbiCreateIncident = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemMapEdit = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.bciEditingNone = new DevExpress.XtraBars.BarCheckItem();
            this.bciEditingMove = new DevExpress.XtraBars.BarCheckItem();
            this.bciEditingAdd = new DevExpress.XtraBars.BarCheckItem();
            this.bciEditingEdit = new DevExpress.XtraBars.BarCheckItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.beiPopupContainerScale = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnSetScale = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.seUserDefinedScale = new DevExpress.XtraEditors.SpinEdit();
            this.gridSplitContainer7 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp01254ATTreePickerscalelistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colScaleDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnViewFullExtent = new DevExpress.XtraEditors.SimpleButton();
            this.bsiScreenCoords = new DevExpress.XtraBars.BarStaticItem();
            this.bsiDistanceMeasured = new DevExpress.XtraBars.BarStaticItem();
            this.bsiSnap = new DevExpress.XtraBars.BarStaticItem();
            this.bsiLayerCount = new DevExpress.XtraBars.BarStaticItem();
            this.bsiZoom = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanel5 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel5_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.pictureBoxLegend = new System.Windows.Forms.PictureBox();
            this.dockPanel6 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel6_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.pictureBoxScaleBar = new System.Windows.Forms.PictureBox();
            this.dockPanelGazetteer = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel7_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.buttonEditGazetteerFindValue = new DevExpress.XtraEditors.ButtonEdit();
            this.lookUpEditGazetteerSearchType = new DevExpress.XtraEditors.LookUpEdit();
            this.sp01311ATTreePickerGazetteerSearchTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupGazetteerMatchPattern = new DevExpress.XtraEditors.RadioGroup();
            this.dockPanelInspections = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer2 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp01367ATInspectionsForTreesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.imageList5 = new System.Windows.Forms.ImageList(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionInspector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionIncidentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionIncidentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionAngleToVertical = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colInspectionStemPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionGeneralCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionVitality = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLinkedOutstandingActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoFurtherActionRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp01380ATActionsLinkedToInspectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActionBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colActionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostDifference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditSiteFilter2 = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditClientFilter2 = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditSiteFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.popupContainerEditAssetTypes = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlAssetTypes = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridSplitContainer6 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlAssetTypes = new DevExpress.XtraGrid.GridControl();
            this.sp03021EPAssetTypeFilterDropDownBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP = new WoodPlan5.DataSet_EP();
            this.gridViewAssetTypes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOkAssetTypes = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlAssetObjects = new DevExpress.XtraGrid.GridControl();
            this.sp03069TreePickerAssetObjectsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewAssetObjects = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAssetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModelNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLifeSpanValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLifeSpanValueDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentConditionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConditionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHighlight1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditHighlight2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnClearMapObjects = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEditTreeRefStructure = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlTreeRefStructure = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnSetTreeRefStruc = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEditTreeRefRemovePreceedingChars = new DevExpress.XtraEditors.TextEdit();
            this.spinEditTreeRefRemoveNumberOfChars = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditTreeRef1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTreeRef3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTreeRef2 = new DevExpress.XtraEditors.CheckEdit();
            this.btnSelectHighlighted = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefreshMapObjects = new DevExpress.XtraEditors.SimpleButton();
            this.csScaleMapForHighlighted = new DevExpress.XtraEditors.CheckEdit();
            this.ceCentreMap = new DevExpress.XtraEditors.CheckEdit();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp01252ATTreePickerloadobjectmanagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContext = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroundType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProtectionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteHazardClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgeClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaHa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReplantCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGardenSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPropertyProximity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSituation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcolor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedInspectionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHighlight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditHighlight = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colintAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAgeClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintDistrictID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintGroundType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintLocalityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintNearbyObject1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintNearbyObject2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintNearbyObject3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOwnershipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintProtectionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSafetyPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSiteHazardClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionElapsedDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditSyncSelection = new DevExpress.XtraEditors.CheckEdit();
            this.colorEditHighlight = new DevExpress.XtraEditors.ColorEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockPanel4 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel4_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl6 = new DevExpress.XtraLayout.LayoutControl();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp01288ATTreePickerWorkspacelayerslistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLayerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkspaceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCE_LayerVisible = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLayerHitable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCE_LayerHitable = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colPreserveDefaultStyling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransparency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUseThematicStyling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThematicStyleSet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPointSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyLayerPathForSave = new DevExpress.XtraGrid.Columns.GridColumn();
            this.beWorkspace = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockPanel3 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.statusBar1 = new DevExpress.XtraEditors.TextEdit();
            this.NMEAtabs = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.labelPlottingUsingTimer = new DevExpress.XtraEditors.LabelControl();
            this.btnGps_PauseTimer = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl_Gps_Page1 = new DevExpress.XtraLayout.LayoutControl();
            this.ceLogRawGPSData = new DevExpress.XtraEditors.CheckEdit();
            this.cmbPortName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbBaudRate = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ceGps_PlotWithTimer = new DevExpress.XtraEditors.CheckEdit();
            this.seGps_PlotTimerInterval = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnGPS_PlotStart = new DevExpress.XtraEditors.SimpleButton();
            this.btnGPS_PlotStop = new DevExpress.XtraEditors.SimpleButton();
            this.ceGPS_PlotLine = new DevExpress.XtraEditors.CheckEdit();
            this.btnGPS_Plot = new DevExpress.XtraEditors.SimpleButton();
            this.ceGPS_PlotPolygon = new DevExpress.XtraEditors.CheckEdit();
            this.ceGPS_PlotPoint = new DevExpress.XtraEditors.CheckEdit();
            this.btnGPS_Start = new DevExpress.XtraEditors.SimpleButton();
            this.tabGPRMC = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl_GPS_Page2 = new DevExpress.XtraLayout.LayoutControl();
            this.lbRMCGridRef = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCPositionUTM = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCMagneticVariation = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCTimeOfFix = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCSpeed = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCCourse = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new DevExpress.XtraEditors.LabelControl();
            this.lbRMCPosition = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabGPGGA = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl_GPS_Page3 = new DevExpress.XtraLayout.LayoutControl();
            this.lbGGADGPSID = new DevExpress.XtraEditors.TextEdit();
            this.lbGGADGPSupdate = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAGeoidHeight = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAHDOP = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAAltitude = new DevExpress.XtraEditors.TextEdit();
            this.lbGGANoOfSats = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAFixQuality = new DevExpress.XtraEditors.TextEdit();
            this.lbGGATimeOfFix = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAPosition = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabGPGLL = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl_GPS_Page4 = new DevExpress.XtraLayout.LayoutControl();
            this.lbGLLDataValid = new DevExpress.XtraEditors.TextEdit();
            this.lbGLLTimeOfSolution = new DevExpress.XtraEditors.TextEdit();
            this.lbGLLPosition = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabGPGSA = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl_GPS_Page5 = new DevExpress.XtraLayout.LayoutControl();
            this.lbGSAVDOP = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAHDOP = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAPDOP = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAPRNs = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAFixMode = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAMode = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabGPGSV = new DevExpress.XtraTab.XtraTabPage();
            this.picGSVSignals = new System.Windows.Forms.PictureBox();
            this.picGSVSkyview = new System.Windows.Forms.PictureBox();
            this.label33 = new DevExpress.XtraEditors.LabelControl();
            this.tabRaw = new DevExpress.XtraTab.XtraTabPage();
            this.tbRawLog = new DevExpress.XtraEditors.MemoEdit();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList4 = new System.Windows.Forms.ImageList(this.components);
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.imageListLineStyles = new System.Windows.Forms.ImageList(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.beiEditMode = new DevExpress.XtraBars.BarEditItem();
            this.bsiEditMode = new DevExpress.XtraBars.BarStaticItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bbiWorkOrderMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPrint = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveImage = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp01252_AT_Tree_Picker_load_object_managerTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01252_AT_Tree_Picker_load_object_managerTableAdapter();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiViewOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiHighlightYes = new DevExpress.XtraBars.BarButtonItem();
            this.bbiHighlightNo = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopyHighlightFromVisible = new DevExpress.XtraBars.BarButtonItem();
            this.bbiVisibleYes = new DevExpress.XtraBars.BarButtonItem();
            this.bbiVisibleNo = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp01254_AT_Tree_Picker_scale_listTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01254_AT_Tree_Picker_scale_listTableAdapter();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridLookUpEdit3View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupMenu_ThematicGrid = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiBlockEditThematicStyles = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveThematicSet = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveThematicSetAs = new DevExpress.XtraBars.BarButtonItem();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.popupMenu_LayerManager = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiAddLayer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRemoveLayer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLayerManagerProperties = new DevExpress.XtraBars.BarButtonItem();
            this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter();
            this.sp01274ATTreePickerWorkspaceEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter();
            this.imageListLineStylesLegend = new System.Windows.Forms.ImageList(this.components);
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.bciGazetteer = new DevExpress.XtraBars.BarCheckItem();
            this.bbiLegend = new DevExpress.XtraBars.BarCheckItem();
            this.bciScaleBar = new DevExpress.XtraBars.BarCheckItem();
            this.bciLinkedInspections = new DevExpress.XtraBars.BarCheckItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter();
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter();
            this.bbiGPSSaveLog = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGPSClearLog = new DevExpress.XtraBars.BarButtonItem();
            this.imageListGPSLogMenu = new System.Windows.Forms.ImageList(this.components);
            this.popupMenu_Gazetteer = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiGazetteerShowMatch = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGazetteerLoadMapObjectsInRange = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupStoredMapViews = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bciStoreViewChanges = new DevExpress.XtraBars.BarCheckItem();
            this.bsiGoToMapView = new DevExpress.XtraBars.BarSubItem();
            this.bbiStoreCurrentMapView = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClearAllMapViews = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bar6 = new DevExpress.XtraBars.Bar();
            this.bbiStoredMapViews = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLastMapView = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNextMapView = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bsiEditObjects = new DevExpress.XtraBars.BarSubItem();
            this.sp01367_AT_Inspections_For_TreesTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01367_AT_Inspections_For_TreesTableAdapter();
            this.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter();
            this.bbiBlockAddActionsToInspections = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItem2 = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sp03068_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp03068_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter();
            this.sp03069_Tree_Picker_Asset_Objects_ListTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp03069_Tree_Picker_Asset_Objects_ListTableAdapter();
            this.sp03021_EP_Asset_Type_Filter_DropDownTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03021_EP_Asset_Type_Filter_DropDownTableAdapter();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.bar7 = new DevExpress.XtraBars.Bar();
            this.bar8 = new DevExpress.XtraBars.Bar();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlendingx = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending7 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01312ATTreePickerGazetteerSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03068TreePickerPlottingObjectTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_MapControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditGazetteerRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seUserDefinedScale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).BeginInit();
            this.gridSplitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01254ATTreePickerscalelistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            this.dockPanel5.SuspendLayout();
            this.dockPanel5_Container.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLegend)).BeginInit();
            this.dockPanel6.SuspendLayout();
            this.dockPanel6_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxScaleBar)).BeginInit();
            this.dockPanelGazetteer.SuspendLayout();
            this.dockPanel7_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditGazetteerFindValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditGazetteerSearchType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01311ATTreePickerGazetteerSearchTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGazetteerMatchPattern.Properties)).BeginInit();
            this.dockPanelInspections.SuspendLayout();
            this.controlContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01367ATInspectionsForTreesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01380ATActionsLinkedToInspectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            this.panelContainer1.SuspendLayout();
            this.dockPanel2.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditAssetTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlAssetTypes)).BeginInit();
            this.popupContainerControlAssetTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).BeginInit();
            this.gridSplitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAssetTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03021EPAssetTypeFilterDropDownBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAssetTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAssetObjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03069TreePickerAssetObjectsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAssetObjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditHighlight2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditTreeRefStructure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTreeRefStructure)).BeginInit();
            this.popupContainerControlTreeRefStructure.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTreeRefRemovePreceedingChars.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTreeRefRemoveNumberOfChars.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.csScaleMapForHighlighted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCentreMap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01252ATTreePickerloadobjectmanagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditHighlight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncSelection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditHighlight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            this.dockPanel4.SuspendLayout();
            this.dockPanel4_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).BeginInit();
            this.layoutControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01288ATTreePickerWorkspacelayerslistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCE_LayerVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCE_LayerHitable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beWorkspace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            this.dockPanel3.SuspendLayout();
            this.dockPanel3_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusBar1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NMEAtabs)).BeginInit();
            this.NMEAtabs.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Gps_Page1)).BeginInit();
            this.layoutControl_Gps_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceLogRawGPSData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPortName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBaudRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGps_PlotWithTimer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seGps_PlotTimerInterval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotLine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotPolygon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotPoint.Properties)).BeginInit();
            this.tabGPRMC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page2)).BeginInit();
            this.layoutControl_GPS_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCGridRef.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCPositionUTM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCMagneticVariation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCTimeOfFix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCSpeed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCCourse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            this.tabGPGGA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page3)).BeginInit();
            this.layoutControl_GPS_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGADGPSID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGADGPSupdate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAGeoidHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAHDOP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAAltitude.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGANoOfSats.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAFixQuality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGATimeOfFix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            this.tabGPGLL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page4)).BeginInit();
            this.layoutControl_GPS_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLDataValid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLTimeOfSolution.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            this.tabGPGSA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page5)).BeginInit();
            this.layoutControl_GPS_Page5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAVDOP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAHDOP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAPDOP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAPRNs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAFixMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            this.tabGPGSV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picGSVSignals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGSVSkyview)).BeginInit();
            this.tabRaw.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbRawLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit3View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_ThematicGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_LayerManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01274ATTreePickerWorkspaceEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_Gazetteer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupStoredMapViews)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockAddActionsToInspections, true)});
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.Caption = "Add...";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1380, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 728);
            this.barDockControlBottom.Size = new System.Drawing.Size(1380, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 686);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1380, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 686);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.Caption = "Highlight Where Records in Dataset";
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.Caption = "Create Dataset from Highlighted Records";
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.Caption = "Highlight Where Records NOT in Dataset";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2,
            this.bar3,
            this.bar4,
            this.bar5,
            this.bar6,
            this.bar7,
            this.bar8});
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiNone,
            this.bbiZoomIn,
            this.bbiZoomOut,
            this.bbiPan,
            this.bbiCentre,
            this.bbiSelect,
            this.bbiSelectRectangle,
            this.bbiSelectRadius,
            this.bbiSelectPolygon,
            this.bbiSelectRegion,
            this.bbiAddPoint,
            this.bbiAddPolygon,
            this.bbiAddPolyLine,
            this.bsiZoom,
            this.barButtonItem1,
            this.bsiScreenCoords,
            this.bsiLayerCount,
            this.bsiSnap,
            this.barCheckItem1,
            this.beiEditMode,
            this.barSubItem1,
            this.bciEditingNone,
            this.bciEditingMove,
            this.bciEditingAdd,
            this.bciEditingEdit,
            this.bsiEditMode,
            this.bbiSaveImage,
            this.bbiViewOnMap,
            this.beiPopupContainerScale,
            this.bbiMeasureLine,
            this.bsiDistanceMeasured,
            this.bbiHighlightYes,
            this.bbiHighlightNo,
            this.bbiCopyHighlightFromVisible,
            this.bbiVisibleYes,
            this.bbiVisibleNo,
            this.bbiPrint,
            this.bbiBlockEditThematicStyles,
            this.bbiSaveThematicSet,
            this.bbiSaveThematicSetAs,
            this.bbiLayerManagerProperties,
            this.bbiWorkOrderMap,
            this.bbiAddLayer,
            this.bbiRemoveLayer,
            this.bbiQueryTool,
            this.bbiLegend,
            this.bciScaleBar,
            this.bciGazetteer,
            this.bbiEditMapObjects,
            this.bbiDeleteMapObjects,
            this.bbiCreateDatasetFromMapObjects,
            this.bbiTransferToWorkOrder,
            this.bbiGPSSaveLog,
            this.bbiGPSClearLog,
            this.bbiGazetteerShowMatch,
            this.barSubItemMapEdit,
            this.beiGazetteerLoadObjectsWithinRange,
            this.barEditItem1,
            this.bbiGazetteerLoadMapObjectsInRange,
            this.bciStoreViewChanges,
            this.bbiClearAllMapViews,
            this.bbiStoreCurrentMapView,
            this.barButtonItem4,
            this.bsiGoToMapView,
            this.bbiLastMapView,
            this.bbiMapSelection,
            this.bbiNextMapView,
            this.bbiStoredMapViews,
            this.bbiGazetteerClearMapSearch,
            this.bbiDeleteSelectedTempMapObjects,
            this.bciMapMarkerAdd,
            this.bbiCreatePolygonFromMapMarkers,
            this.bbiCreatePolylineFromMapMarkers,
            this.bbiDeleteAllMapMarkers,
            this.barSubItem2,
            this.bbiEdit,
            this.bsiEditObjects,
            this.bbiAddInspection,
            this.bbiAddAction,
            this.bsiAddToMapObjects,
            this.bciLinkedInspections,
            this.bbiEditSelectedMapObjects,
            this.bbiBlockEditselectedMapObjects,
            this.bsiEditMapObject,
            this.bbiBlockAddActionsToInspections,
            this.bbiCreateIncident,
            this.bbiCreateIncidentFromMap,
            this.bsiMapNearbyObjects,
            this.bbiNearbyObjectsFind,
            this.barCheckItem2,
            this.barButtonItem3,
            this.beiPlotObjectType,
            this.bsiSetCentrePoint,
            this.bbiSetClientCentre,
            this.bbiSetSiteCentre});
            this.barManager1.MaxItemId = 144;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemSpinEditGazetteerRange,
            this.repositoryItemPopupContainerEdit3,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemGridLookUpEdit2});
            this.barManager1.StatusBar = this.bar2;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13});
            this.gridView6.GridControl = this.gridControl3;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn7, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Site ID";
            this.gridColumn5.FieldName = "RecordID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Site Name";
            this.gridColumn6.FieldName = "AddressLine1";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 211;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Client Name";
            this.gridColumn7.FieldName = "AddressLine2";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            this.gridColumn7.Width = 151;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Unused 1";
            this.gridColumn8.FieldName = "AddressLine3";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 145;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Unused 2";
            this.gridColumn9.FieldName = "AddressLine4";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 145;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Unused 3";
            this.gridColumn10.FieldName = "AddressLine5";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 145;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Unused 4";
            this.gridColumn11.FieldName = "Postcode";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 145;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = " X Coordinate";
            this.gridColumn12.FieldName = "XCoordinate";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Y Coordinate";
            this.gridColumn13.FieldName = "YCoordinate";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp01312ATTreePickerGazetteerSearchBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            gridLevelNode1.LevelTemplate = this.gridView6;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(325, 526);
            this.gridControl3.TabIndex = 6;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3,
            this.gridView6});
            // 
            // sp01312ATTreePickerGazetteerSearchBindingSource
            // 
            this.sp01312ATTreePickerGazetteerSearchBindingSource.DataMember = "sp01312_AT_Tree_Picker_Gazetteer_Search";
            this.sp01312ATTreePickerGazetteerSearchBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // dataSet_AT_TreePicker
            // 
            this.dataSet_AT_TreePicker.DataSetName = "DataSet_AT_TreePicker";
            this.dataSet_AT_TreePicker.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRecordID,
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colPostcode1,
            this.colXCoordinate1,
            this.colYCoordinate1});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAddressLine1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAddressLine2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colRecordID
            // 
            this.colRecordID.Caption = "Record ID";
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 0;
            this.colAddressLine1.Width = 145;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Visible = true;
            this.colAddressLine2.VisibleIndex = 1;
            this.colAddressLine2.Width = 145;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Visible = true;
            this.colAddressLine3.VisibleIndex = 2;
            this.colAddressLine3.Width = 145;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Visible = true;
            this.colAddressLine4.VisibleIndex = 3;
            this.colAddressLine4.Width = 145;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Visible = true;
            this.colAddressLine5.VisibleIndex = 4;
            this.colAddressLine5.Width = 145;
            // 
            // colPostcode1
            // 
            this.colPostcode1.Caption = "Postcode";
            this.colPostcode1.FieldName = "Postcode";
            this.colPostcode1.Name = "colPostcode1";
            this.colPostcode1.OptionsColumn.AllowEdit = false;
            this.colPostcode1.OptionsColumn.AllowFocus = false;
            this.colPostcode1.OptionsColumn.ReadOnly = true;
            this.colPostcode1.Visible = true;
            this.colPostcode1.VisibleIndex = 5;
            this.colPostcode1.Width = 145;
            // 
            // colXCoordinate1
            // 
            this.colXCoordinate1.Caption = " X Coordinate";
            this.colXCoordinate1.FieldName = "XCoordinate";
            this.colXCoordinate1.Name = "colXCoordinate1";
            this.colXCoordinate1.OptionsColumn.AllowEdit = false;
            this.colXCoordinate1.OptionsColumn.AllowFocus = false;
            this.colXCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate1
            // 
            this.colYCoordinate1.Caption = "Y Coordinate";
            this.colYCoordinate1.FieldName = "YCoordinate";
            this.colYCoordinate1.Name = "colYCoordinate1";
            this.colYCoordinate1.OptionsColumn.AllowEdit = false;
            this.colYCoordinate1.OptionsColumn.AllowFocus = false;
            this.colYCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colObjectTypeID
            // 
            this.colObjectTypeID.Caption = "Object Type ID";
            this.colObjectTypeID.FieldName = "ObjectTypeID";
            this.colObjectTypeID.Name = "colObjectTypeID";
            this.colObjectTypeID.OptionsColumn.AllowEdit = false;
            this.colObjectTypeID.OptionsColumn.AllowFocus = false;
            this.colObjectTypeID.OptionsColumn.FixedWidth = true;
            this.colObjectTypeID.Width = 100;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Asset Type ID";
            this.gridColumn47.FieldName = "AssetTypeID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Width = 88;
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Mode", "Edit Mode")});
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            // 
            // mapToolBar1
            // 
            this.mapToolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.mapToolBarButtonOpenTable,
            this.mapToolBarButtonLayerControl,
            this.toolBarButtonSperator1,
            this.mapToolBarButtonArrow,
            this.mapToolBarButtonZoomIn,
            this.mapToolBarButtonZoomOut,
            this.mapToolBarButtonCenter,
            this.mapToolBarButtonPan,
            this.toolBarButtonSeperator2,
            this.mapToolBarButtonSelect,
            this.mapToolBarButtonSelectRectangle,
            this.mapToolBarButtonSelectRadius,
            this.mapToolBarButtonSelectPolygon,
            this.mapToolBarButtonSelectRegion,
            this.toolBarButtonSeperator3,
            this.mapToolBarButtonAddPoint,
            this.mapToolBarButton1,
            this.mapToolBarButtonAddPolyLine,
            this.mapToolBarButton2});
            this.mapToolBar1.ButtonSize = new System.Drawing.Size(25, 22);
            this.mapToolBar1.DropDownArrows = true;
            this.mapToolBar1.Enabled = false;
            this.mapToolBar1.Location = new System.Drawing.Point(340, 42);
            this.mapToolBar1.MapControl = this.mapControl1;
            this.mapToolBar1.Name = "mapToolBar1";
            this.mapToolBar1.ShowToolTips = true;
            this.mapToolBar1.Size = new System.Drawing.Size(1040, 28);
            this.mapToolBar1.TabIndex = 18;
            this.mapToolBar1.Visible = false;
            // 
            // mapToolBarButtonOpenTable
            // 
            this.mapToolBarButtonOpenTable.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.OpenTable;
            this.mapToolBarButtonOpenTable.Name = "mapToolBarButtonOpenTable";
            this.mapToolBarButtonOpenTable.ToolTipText = "Open Table";
            // 
            // mapToolBarButtonLayerControl
            // 
            this.mapToolBarButtonLayerControl.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.LayerControl;
            this.mapToolBarButtonLayerControl.Name = "mapToolBarButtonLayerControl";
            this.mapToolBarButtonLayerControl.ToolTipText = "Layer Control";
            // 
            // toolBarButtonSperator1
            // 
            this.toolBarButtonSperator1.Name = "toolBarButtonSperator1";
            this.toolBarButtonSperator1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // mapToolBarButtonArrow
            // 
            this.mapToolBarButtonArrow.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.Arrow;
            this.mapToolBarButtonArrow.Name = "mapToolBarButtonArrow";
            this.mapToolBarButtonArrow.ToolTipText = "Arrow";
            // 
            // mapToolBarButtonZoomIn
            // 
            this.mapToolBarButtonZoomIn.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.ZoomIn;
            this.mapToolBarButtonZoomIn.Name = "mapToolBarButtonZoomIn";
            this.mapToolBarButtonZoomIn.ToolTipText = "Zoom-in";
            // 
            // mapToolBarButtonZoomOut
            // 
            this.mapToolBarButtonZoomOut.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.ZoomOut;
            this.mapToolBarButtonZoomOut.Name = "mapToolBarButtonZoomOut";
            this.mapToolBarButtonZoomOut.ToolTipText = "Zoom-out";
            // 
            // mapToolBarButtonCenter
            // 
            this.mapToolBarButtonCenter.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.Center;
            this.mapToolBarButtonCenter.Name = "mapToolBarButtonCenter";
            this.mapToolBarButtonCenter.ToolTipText = "Center";
            // 
            // mapToolBarButtonPan
            // 
            this.mapToolBarButtonPan.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.Pan;
            this.mapToolBarButtonPan.Name = "mapToolBarButtonPan";
            this.mapToolBarButtonPan.ToolTipText = "Pan";
            // 
            // toolBarButtonSeperator2
            // 
            this.toolBarButtonSeperator2.Name = "toolBarButtonSeperator2";
            this.toolBarButtonSeperator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // mapToolBarButtonSelect
            // 
            this.mapToolBarButtonSelect.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.Select;
            this.mapToolBarButtonSelect.Name = "mapToolBarButtonSelect";
            this.mapToolBarButtonSelect.ToolTipText = "Select";
            // 
            // mapToolBarButtonSelectRectangle
            // 
            this.mapToolBarButtonSelectRectangle.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.SelectRectangle;
            this.mapToolBarButtonSelectRectangle.Name = "mapToolBarButtonSelectRectangle";
            this.mapToolBarButtonSelectRectangle.ToolTipText = "Marquee Select";
            // 
            // mapToolBarButtonSelectRadius
            // 
            this.mapToolBarButtonSelectRadius.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.SelectRadius;
            this.mapToolBarButtonSelectRadius.Name = "mapToolBarButtonSelectRadius";
            this.mapToolBarButtonSelectRadius.ToolTipText = "Radius Select";
            // 
            // mapToolBarButtonSelectPolygon
            // 
            this.mapToolBarButtonSelectPolygon.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.SelectPolygon;
            this.mapToolBarButtonSelectPolygon.Name = "mapToolBarButtonSelectPolygon";
            this.mapToolBarButtonSelectPolygon.ToolTipText = "Polygon Select";
            // 
            // mapToolBarButtonSelectRegion
            // 
            this.mapToolBarButtonSelectRegion.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.SelectRegion;
            this.mapToolBarButtonSelectRegion.Name = "mapToolBarButtonSelectRegion";
            this.mapToolBarButtonSelectRegion.ToolTipText = "Region Select";
            // 
            // toolBarButtonSeperator3
            // 
            this.toolBarButtonSeperator3.Name = "toolBarButtonSeperator3";
            this.toolBarButtonSeperator3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // mapToolBarButtonAddPoint
            // 
            this.mapToolBarButtonAddPoint.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.AddPoint;
            this.mapToolBarButtonAddPoint.Name = "mapToolBarButtonAddPoint";
            this.mapToolBarButtonAddPoint.ToolTipText = "Add Point";
            // 
            // mapToolBarButton1
            // 
            this.mapToolBarButton1.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.AddPolygon;
            this.mapToolBarButton1.Name = "mapToolBarButton1";
            this.mapToolBarButton1.ToolTipText = "Add Polygon";
            // 
            // mapToolBarButtonAddPolyLine
            // 
            this.mapToolBarButtonAddPolyLine.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.AddPolyline;
            this.mapToolBarButtonAddPolyLine.Name = "mapToolBarButtonAddPolyLine";
            this.mapToolBarButtonAddPolyLine.ToolTipText = "Add Polyline";
            // 
            // mapToolBarButton2
            // 
            this.mapToolBarButton2.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.AddText;
            this.mapToolBarButton2.Name = "mapToolBarButton2";
            this.mapToolBarButton2.ToolTipText = "Add Text";
            // 
            // mapControl1
            // 
            this.mapControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl1.IgnoreLostFocusEvent = false;
            this.mapControl1.Location = new System.Drawing.Point(2, 2);
            this.mapControl1.Name = "mapControl1";
            this.mapControl1.Size = new System.Drawing.Size(1036, 654);
            this.mapControl1.TabIndex = 0;
            this.mapControl1.Text = "mapControl2";
            this.mapControl1.Visible = false;
            this.mapControl1.Enter += new System.EventHandler(this.mapControl1_Enter);
            this.mapControl1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mapControl1_KeyPress);
            this.mapControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseMove);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(0, 0);
            this.memoEdit1.MenuManager = this.barManager1;
            this.memoEdit1.Name = "memoEdit1";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memoEdit1, true);
            this.memoEdit1.Size = new System.Drawing.Size(1059, 93);
            this.scSpellChecker.SetSpellCheckerOptions(this.memoEdit1, optionsSpelling29);
            this.memoEdit1.TabIndex = 21;
            // 
            // bar1
            // 
            this.bar1.BarName = "Plotting";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiPlotObjectType, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPoint),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPolygon),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPolyLine)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Plotting";
            // 
            // beiPlotObjectType
            // 
            this.beiPlotObjectType.Caption = "Plot:";
            this.beiPlotObjectType.Edit = this.repositoryItemGridLookUpEdit2;
            this.beiPlotObjectType.EditValue = -1;
            this.beiPlotObjectType.EditWidth = 158;
            this.beiPlotObjectType.Id = 140;
            this.beiPlotObjectType.Name = "beiPlotObjectType";
            this.beiPlotObjectType.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.beiPlotObjectType.EditValueChanged += new System.EventHandler(this.beiPlotObjectType_EditValueChanged);
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DataSource = this.sp03068TreePickerPlottingObjectTypesWithBlankBindingSource;
            this.repositoryItemGridLookUpEdit2.DisplayMember = "ObjectTypeDescription";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.NullText = "";
            this.repositoryItemGridLookUpEdit2.NullValuePrompt = "Select Object Type";
            this.repositoryItemGridLookUpEdit2.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemGridLookUpEdit2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.repositoryItemGridLookUpEdit2.ValueMember = "ObjectTypeID";
            this.repositoryItemGridLookUpEdit2.View = this.repositoryItemGridLookUpEdit2View;
            // 
            // sp03068TreePickerPlottingObjectTypesWithBlankBindingSource
            // 
            this.sp03068TreePickerPlottingObjectTypesWithBlankBindingSource.DataMember = "sp03068_Tree_Picker_Plotting_Object_Types_With_Blank";
            this.sp03068TreePickerPlottingObjectTypesWithBlankBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colModuleDescription,
            this.colModuleID,
            this.colObjectTypeDescription,
            this.colObjectTypeID,
            this.colObjectTypeOrder,
            this.colRemarks});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colObjectTypeID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.repositoryItemGridLookUpEdit2View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.repositoryItemGridLookUpEdit2View.GroupCount = 1;
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsBehavior.AutoExpandAllGroups = true;
            this.repositoryItemGridLookUpEdit2View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colModuleDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colObjectTypeOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colModuleDescription
            // 
            this.colModuleDescription.Caption = "Module";
            this.colModuleDescription.FieldName = "ModuleDescription";
            this.colModuleDescription.Name = "colModuleDescription";
            this.colModuleDescription.OptionsColumn.AllowEdit = false;
            this.colModuleDescription.OptionsColumn.AllowFocus = false;
            this.colModuleDescription.OptionsColumn.FixedWidth = true;
            this.colModuleDescription.Visible = true;
            this.colModuleDescription.VisibleIndex = 0;
            this.colModuleDescription.Width = 115;
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.AllowEdit = false;
            this.colModuleID.OptionsColumn.AllowFocus = false;
            this.colModuleID.OptionsColumn.FixedWidth = true;
            // 
            // colObjectTypeDescription
            // 
            this.colObjectTypeDescription.Caption = "Object Type";
            this.colObjectTypeDescription.FieldName = "ObjectTypeDescription";
            this.colObjectTypeDescription.Name = "colObjectTypeDescription";
            this.colObjectTypeDescription.OptionsColumn.AllowEdit = false;
            this.colObjectTypeDescription.OptionsColumn.AllowFocus = false;
            this.colObjectTypeDescription.OptionsColumn.FixedWidth = true;
            this.colObjectTypeDescription.Visible = true;
            this.colObjectTypeDescription.VisibleIndex = 0;
            this.colObjectTypeDescription.Width = 232;
            // 
            // colObjectTypeOrder
            // 
            this.colObjectTypeOrder.Caption = "Object Order";
            this.colObjectTypeOrder.FieldName = "ObjectTypeOrder";
            this.colObjectTypeOrder.Name = "colObjectTypeOrder";
            this.colObjectTypeOrder.OptionsColumn.AllowEdit = false;
            this.colObjectTypeOrder.OptionsColumn.AllowFocus = false;
            this.colObjectTypeOrder.OptionsColumn.FixedWidth = true;
            this.colObjectTypeOrder.Width = 165;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.FixedWidth = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 1;
            this.colRemarks.Width = 92;
            // 
            // bbiAddPoint
            // 
            this.bbiAddPoint.Caption = "Add Point";
            this.bbiAddPoint.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddPoint.Glyph")));
            this.bbiAddPoint.GroupIndex = 1;
            this.bbiAddPoint.Id = 35;
            this.bbiAddPoint.Name = "bbiAddPoint";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Text = "Add Point Tool - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "This tool allows you to plot a new point.\r\n\r\n<b><color=green>Tip:</color></b> To " +
    "add points, the Map Objects layer must be <b>Hitable</b> and <b>Editable</b>.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiAddPoint.SuperTip = superToolTip1;
            this.bbiAddPoint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPoint_ItemClick);
            // 
            // bbiAddPolygon
            // 
            this.bbiAddPolygon.Caption = "Add Polygon";
            this.bbiAddPolygon.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddPolygon.Glyph")));
            this.bbiAddPolygon.GroupIndex = 1;
            this.bbiAddPolygon.Id = 36;
            this.bbiAddPolygon.Name = "bbiAddPolygon";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Text = "Add Polygon Tool - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "This tool allows you to plot a new polygon.\r\n\r\n<b><color=green>Tip:</color></b> T" +
    "to add polygons, the Map Objects layer must be <b>Hitable</b> and <b>Editable</b" +
    ">.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiAddPolygon.SuperTip = superToolTip2;
            this.bbiAddPolygon.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPolygon_ItemClick);
            // 
            // bbiAddPolyLine
            // 
            this.bbiAddPolyLine.Caption = "Add PolyLine";
            this.bbiAddPolyLine.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddPolyLine.Glyph")));
            this.bbiAddPolyLine.GroupIndex = 1;
            this.bbiAddPolyLine.Id = 37;
            this.bbiAddPolyLine.Name = "bbiAddPolyLine";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Text = "Add Polyline Tool - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "This tool allows you to plot a new polyline.\r\n\r\n<b><color=green>Tip:</color></b> " +
    "To add polylines, the Map Objects layer must be <b>Hitable</b> and <b>Editable</" +
    "b>.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiAddPolyLine.SuperTip = superToolTip3;
            this.bbiAddPolyLine.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPolyLine_ItemClick);
            // 
            // bbiNone
            // 
            this.bbiNone.BindableChecked = true;
            this.bbiNone.Caption = "Pointer";
            this.bbiNone.Checked = true;
            this.bbiNone.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiNone.Glyph")));
            this.bbiNone.GroupIndex = 1;
            this.bbiNone.Id = 25;
            this.bbiNone.Name = "bbiNone";
            superToolTip23.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem23.Text = "No Tool - Information";
            toolTipItem23.LeftIndent = 6;
            toolTipItem23.Text = "This tool does nothing - it allows you to click on the map without doing anything" +
    ".";
            superToolTip23.Items.Add(toolTipTitleItem23);
            superToolTip23.Items.Add(toolTipItem23);
            this.bbiNone.SuperTip = superToolTip23;
            this.bbiNone.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNone_ItemClick);
            // 
            // bbiZoomIn
            // 
            this.bbiZoomIn.Caption = "Zoom In";
            this.bbiZoomIn.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiZoomIn.Glyph")));
            this.bbiZoomIn.GroupIndex = 1;
            this.bbiZoomIn.Id = 26;
            this.bbiZoomIn.Name = "bbiZoomIn";
            superToolTip19.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem19.Text = "Zoom In Tool - Information";
            toolTipItem19.LeftIndent = 6;
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            this.bbiZoomIn.SuperTip = superToolTip19;
            this.bbiZoomIn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiZoomIn_ItemClick);
            // 
            // bbiZoomOut
            // 
            this.bbiZoomOut.Caption = "Zoom Out";
            this.bbiZoomOut.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiZoomOut.Glyph")));
            this.bbiZoomOut.GroupIndex = 1;
            this.bbiZoomOut.Id = 27;
            this.bbiZoomOut.Name = "bbiZoomOut";
            superToolTip20.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem20.Text = "Zoom Out tool - Infomation";
            toolTipItem20.LeftIndent = 6;
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem20);
            this.bbiZoomOut.SuperTip = superToolTip20;
            this.bbiZoomOut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiZoomOut_ItemClick);
            // 
            // bbiPan
            // 
            this.bbiPan.Caption = "Pan";
            this.bbiPan.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_HandTool;
            this.bbiPan.GroupIndex = 1;
            this.bbiPan.Id = 28;
            this.bbiPan.Name = "bbiPan";
            superToolTip21.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem21.Text = "Pan Map Tool - Information";
            toolTipItem21.LeftIndent = 6;
            superToolTip21.Items.Add(toolTipTitleItem21);
            superToolTip21.Items.Add(toolTipItem21);
            this.bbiPan.SuperTip = superToolTip21;
            this.bbiPan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPan_ItemClick);
            // 
            // bbiCentre
            // 
            this.bbiCentre.Caption = "Centre";
            this.bbiCentre.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCentre.Glyph")));
            this.bbiCentre.GroupIndex = 1;
            this.bbiCentre.Id = 29;
            this.bbiCentre.Name = "bbiCentre";
            superToolTip22.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem22.Text = "Centre Map Tool - Information";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "This tool centres the map.\r\n\r\nSelect the tool the click on the map where the map " +
    "is to be centred.";
            superToolTip22.Items.Add(toolTipTitleItem22);
            superToolTip22.Items.Add(toolTipItem22);
            this.bbiCentre.SuperTip = superToolTip22;
            this.bbiCentre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCentre_ItemClick);
            // 
            // bbiSelect
            // 
            this.bbiSelect.Caption = "Select";
            this.bbiSelect.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Pointer;
            this.bbiSelect.GroupIndex = 1;
            this.bbiSelect.Id = 30;
            this.bbiSelect.Name = "bbiSelect";
            superToolTip24.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem24.Text = "Select Tool - Information";
            toolTipItem24.LeftIndent = 6;
            toolTipItem24.Text = "This tool is used to select objects on the map [points, polygons and polylines].\r" +
    "\n\r\n<color=green><b>Tip:</b></color> To select objects, the Map Objects layer mus" +
    "t be <b>Hitable</b>.";
            superToolTip24.Items.Add(toolTipTitleItem24);
            superToolTip24.Items.Add(toolTipItem24);
            this.bbiSelect.SuperTip = superToolTip24;
            this.bbiSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelect_ItemClick);
            // 
            // bbiSelectRectangle
            // 
            this.bbiSelectRectangle.Caption = "Select Rectangle";
            this.bbiSelectRectangle.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectRectangle.Glyph")));
            this.bbiSelectRectangle.GroupIndex = 1;
            this.bbiSelectRectangle.Id = 31;
            this.bbiSelectRectangle.Name = "bbiSelectRectangle";
            superToolTip25.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem25.Text = "Select Rectangle Tool - Information";
            toolTipItem25.LeftIndent = 6;
            toolTipItem25.Text = "This tool selects all objects within a user-defined rectangle.\r\n\r\n<color=green><b" +
    ">Tip:</b></color> To select objects, the Map Objects layer must be <b>Hitable</b" +
    ">.";
            superToolTip25.Items.Add(toolTipTitleItem25);
            superToolTip25.Items.Add(toolTipItem25);
            this.bbiSelectRectangle.SuperTip = superToolTip25;
            this.bbiSelectRectangle.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectRectangle_ItemClick);
            // 
            // bbiSelectRadius
            // 
            this.bbiSelectRadius.Caption = "Select Radius";
            this.bbiSelectRadius.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectRadius.Glyph")));
            this.bbiSelectRadius.GroupIndex = 1;
            this.bbiSelectRadius.Id = 32;
            this.bbiSelectRadius.Name = "bbiSelectRadius";
            superToolTip26.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem26.Text = "Select Elipses Tool - Information";
            toolTipItem26.LeftIndent = 6;
            toolTipItem26.Text = "This tool selects all objects within a user-defined elypsis.\r\n\r\n<color=green><b>T" +
    "ip:</b></color> To select objects, the Map Objects layer must be <b>Hitable</b>." +
    "";
            superToolTip26.Items.Add(toolTipTitleItem26);
            superToolTip26.Items.Add(toolTipItem26);
            this.bbiSelectRadius.SuperTip = superToolTip26;
            this.bbiSelectRadius.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectRadius_ItemClick);
            // 
            // bbiSelectPolygon
            // 
            this.bbiSelectPolygon.Caption = "Select Polygon";
            this.bbiSelectPolygon.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectPolygon.Glyph")));
            this.bbiSelectPolygon.GroupIndex = 1;
            this.bbiSelectPolygon.Id = 33;
            this.bbiSelectPolygon.Name = "bbiSelectPolygon";
            superToolTip28.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem28.Text = "Select Polygon Tool - Information";
            toolTipItem28.LeftIndent = 6;
            toolTipItem28.Text = "This tool selects all objects within a user-defined polygon.\r\n\r\n<color=green><b>T" +
    "ip:</b></color> To select objects, the Map Objects layer must be <b>Hitable</b>." +
    "";
            superToolTip28.Items.Add(toolTipTitleItem28);
            superToolTip28.Items.Add(toolTipItem28);
            this.bbiSelectPolygon.SuperTip = superToolTip28;
            this.bbiSelectPolygon.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectPolygon_ItemClick);
            // 
            // bbiSelectRegion
            // 
            this.bbiSelectRegion.Caption = "Select Region";
            this.bbiSelectRegion.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectRegion.Glyph")));
            this.bbiSelectRegion.GroupIndex = 1;
            this.bbiSelectRegion.Id = 34;
            this.bbiSelectRegion.Name = "bbiSelectRegion";
            superToolTip27.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem27.Text = "Select Region Tool - Information";
            toolTipItem27.LeftIndent = 6;
            toolTipItem27.Text = "This tool selects all objects within a user-defined region.\r\n\r\n<color=green><b>Ti" +
    "p:</b></color> To select objects, the Map Objects layer must be <b>Hitable</b>.";
            superToolTip27.Items.Add(toolTipTitleItem27);
            superToolTip27.Items.Add(toolTipItem27);
            this.bbiSelectRegion.SuperTip = superToolTip27;
            this.bbiSelectRegion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectRegion_ItemClick);
            // 
            // bbiMapSelection
            // 
            this.bbiMapSelection.ActAsDropDown = true;
            this.bbiMapSelection.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiMapSelection.Caption = "Map Selection";
            this.bbiMapSelection.DropDownControl = this.popupMenu_MapControl1;
            this.bbiMapSelection.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiMapSelection.Glyph")));
            this.bbiMapSelection.Id = 112;
            this.bbiMapSelection.Name = "bbiMapSelection";
            superToolTip29.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem29.Text = "Show Map Menu - Information";
            toolTipItem29.LeftIndent = 6;
            toolTipItem29.Text = "Click me to display the Map Menu.\r\n\r\nThe Map Menu is used for editing and deletin" +
    "g map objects and for manipulating Map Markers.";
            superToolTip29.Items.Add(toolTipTitleItem29);
            superToolTip29.Items.Add(toolTipItem29);
            this.bbiMapSelection.SuperTip = superToolTip29;
            // 
            // popupMenu_MapControl1
            // 
            this.popupMenu_MapControl1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiEditMapObject),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiAddToMapObjects),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteMapObjects),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiMapNearbyObjects, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateIncidentFromMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateDatasetFromMapObjects, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTransferToWorkOrder, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSetCentrePoint, true)});
            this.popupMenu_MapControl1.Manager = this.barManager1;
            this.popupMenu_MapControl1.MenuCaption = "Map Menu";
            this.popupMenu_MapControl1.Name = "popupMenu_MapControl1";
            this.popupMenu_MapControl1.ShowCaption = true;
            this.popupMenu_MapControl1.CloseUp += new System.EventHandler(this.popupMenu_MapControl1_CloseUp);
            this.popupMenu_MapControl1.Popup += new System.EventHandler(this.popupMenu_MapControl1_Popup);
            // 
            // bsiEditMapObject
            // 
            this.bsiEditMapObject.Caption = "Edit...";
            this.bsiEditMapObject.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiEditMapObject.Glyph")));
            this.bsiEditMapObject.Id = 131;
            this.bsiEditMapObject.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditSelectedMapObjects),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEditselectedMapObjects)});
            this.bsiEditMapObject.Name = "bsiEditMapObject";
            // 
            // bbiEditSelectedMapObjects
            // 
            this.bbiEditSelectedMapObjects.Caption = "Edit Selected Map Objects";
            this.bbiEditSelectedMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEditSelectedMapObjects.Glyph")));
            this.bbiEditSelectedMapObjects.Id = 129;
            this.bbiEditSelectedMapObjects.Name = "bbiEditSelectedMapObjects";
            this.bbiEditSelectedMapObjects.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditSelectedMapObjects_ItemClick);
            // 
            // bbiBlockEditselectedMapObjects
            // 
            this.bbiBlockEditselectedMapObjects.Caption = "Block Edit Selected Map Objects";
            this.bbiBlockEditselectedMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditselectedMapObjects.Glyph")));
            this.bbiBlockEditselectedMapObjects.Id = 130;
            this.bbiBlockEditselectedMapObjects.Name = "bbiBlockEditselectedMapObjects";
            this.bbiBlockEditselectedMapObjects.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockEditselectedMapObjects_ItemClick);
            // 
            // bsiAddToMapObjects
            // 
            this.bsiAddToMapObjects.Caption = "Add...";
            this.bsiAddToMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiAddToMapObjects.Glyph")));
            this.bsiAddToMapObjects.Id = 127;
            this.bsiAddToMapObjects.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddInspection),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddAction)});
            this.bsiAddToMapObjects.Name = "bsiAddToMapObjects";
            // 
            // bbiAddInspection
            // 
            this.bbiAddInspection.Caption = "Add Inspection to Selected Map Objects";
            this.bbiAddInspection.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddInspection.Glyph")));
            this.bbiAddInspection.Id = 125;
            this.bbiAddInspection.Name = "bbiAddInspection";
            this.bbiAddInspection.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddInspection_ItemClick);
            // 
            // bbiAddAction
            // 
            this.bbiAddAction.Caption = "Add Action to Selected Map Objects";
            this.bbiAddAction.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddAction.Glyph")));
            this.bbiAddAction.Id = 126;
            this.bbiAddAction.Name = "bbiAddAction";
            this.bbiAddAction.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddAction_ItemClick);
            // 
            // bbiDeleteMapObjects
            // 
            this.bbiDeleteMapObjects.Caption = "Delete Selected Map Objects";
            this.bbiDeleteMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDeleteMapObjects.Glyph")));
            this.bbiDeleteMapObjects.Id = 94;
            this.bbiDeleteMapObjects.Name = "bbiDeleteMapObjects";
            this.bbiDeleteMapObjects.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteMapObjects_ItemClick);
            // 
            // bsiMapNearbyObjects
            // 
            this.bsiMapNearbyObjects.Caption = "Find Nearby Map Objects";
            this.bsiMapNearbyObjects.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.bsiMapNearbyObjects.Id = 135;
            this.bsiMapNearbyObjects.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiGazetteerLoadObjectsWithinRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiNearbyObjectsFind),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerClearMapSearch, true)});
            this.bsiMapNearbyObjects.Name = "bsiMapNearbyObjects";
            // 
            // beiGazetteerLoadObjectsWithinRange
            // 
            this.beiGazetteerLoadObjectsWithinRange.Caption = "Map Objects Range:";
            this.beiGazetteerLoadObjectsWithinRange.Edit = this.repositoryItemSpinEditGazetteerRange;
            this.beiGazetteerLoadObjectsWithinRange.EditValue = "10.00";
            this.beiGazetteerLoadObjectsWithinRange.EditWidth = 130;
            this.beiGazetteerLoadObjectsWithinRange.Id = 103;
            this.beiGazetteerLoadObjectsWithinRange.Name = "beiGazetteerLoadObjectsWithinRange";
            // 
            // repositoryItemSpinEditGazetteerRange
            // 
            this.repositoryItemSpinEditGazetteerRange.AutoHeight = false;
            this.repositoryItemSpinEditGazetteerRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditGazetteerRange.Mask.EditMask = "#######0.00 Metres";
            this.repositoryItemSpinEditGazetteerRange.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditGazetteerRange.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEditGazetteerRange.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.repositoryItemSpinEditGazetteerRange.Name = "repositoryItemSpinEditGazetteerRange";
            // 
            // bbiNearbyObjectsFind
            // 
            this.bbiNearbyObjectsFind.Caption = "Find Nearby Map Objects";
            this.bbiNearbyObjectsFind.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.bbiNearbyObjectsFind.Id = 136;
            this.bbiNearbyObjectsFind.Name = "bbiNearbyObjectsFind";
            this.bbiNearbyObjectsFind.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNearbyObjectsFind_ItemClick);
            // 
            // bbiGazetteerClearMapSearch
            // 
            this.bbiGazetteerClearMapSearch.Caption = "Clear Map Search Results";
            this.bbiGazetteerClearMapSearch.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiGazetteerClearMapSearch.Glyph")));
            this.bbiGazetteerClearMapSearch.Id = 116;
            this.bbiGazetteerClearMapSearch.Name = "bbiGazetteerClearMapSearch";
            this.bbiGazetteerClearMapSearch.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGazetteerClearMapSearch_ItemClick);
            // 
            // bbiCreateIncidentFromMap
            // 
            this.bbiCreateIncidentFromMap.Caption = "Create Incident at this Location";
            this.bbiCreateIncidentFromMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCreateIncidentFromMap.Glyph")));
            this.bbiCreateIncidentFromMap.Id = 134;
            this.bbiCreateIncidentFromMap.Name = "bbiCreateIncidentFromMap";
            this.bbiCreateIncidentFromMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreateIncidentFromMap_ItemClick);
            // 
            // bbiCreateDatasetFromMapObjects
            // 
            this.bbiCreateDatasetFromMapObjects.Caption = "Create Dataset from Selected Map Objects";
            this.bbiCreateDatasetFromMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCreateDatasetFromMapObjects.Glyph")));
            this.bbiCreateDatasetFromMapObjects.Id = 95;
            this.bbiCreateDatasetFromMapObjects.Name = "bbiCreateDatasetFromMapObjects";
            this.bbiCreateDatasetFromMapObjects.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreateDatasetFromMapObjects_ItemClick);
            // 
            // bbiTransferToWorkOrder
            // 
            this.bbiTransferToWorkOrder.Caption = "Transfer Outstanding Jobs on Selected Map Objects to Work Order";
            this.bbiTransferToWorkOrder.Id = 97;
            this.bbiTransferToWorkOrder.Name = "bbiTransferToWorkOrder";
            this.bbiTransferToWorkOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTransferToWorkOrder_ItemClick);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Map Markers";
            this.barSubItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem2.Glyph")));
            this.barSubItem2.Id = 122;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreatePolygonFromMapMarkers),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreatePolylineFromMapMarkers),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteSelectedTempMapObjects, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteAllMapMarkers)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // bbiCreatePolygonFromMapMarkers
            // 
            this.bbiCreatePolygonFromMapMarkers.Caption = "Create Polygon from Map Markers";
            this.bbiCreatePolygonFromMapMarkers.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCreatePolygonFromMapMarkers.Glyph")));
            this.bbiCreatePolygonFromMapMarkers.Id = 119;
            this.bbiCreatePolygonFromMapMarkers.Name = "bbiCreatePolygonFromMapMarkers";
            this.bbiCreatePolygonFromMapMarkers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreatePolygonFromMapMarkers_ItemClick);
            // 
            // bbiCreatePolylineFromMapMarkers
            // 
            this.bbiCreatePolylineFromMapMarkers.Caption = "Create Polyline from Map Markers";
            this.bbiCreatePolylineFromMapMarkers.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCreatePolylineFromMapMarkers.Glyph")));
            this.bbiCreatePolylineFromMapMarkers.Id = 120;
            this.bbiCreatePolylineFromMapMarkers.Name = "bbiCreatePolylineFromMapMarkers";
            this.bbiCreatePolylineFromMapMarkers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreatePolylineFromMapMarkers_ItemClick);
            // 
            // bbiDeleteSelectedTempMapObjects
            // 
            this.bbiDeleteSelectedTempMapObjects.Caption = "Delete Selected Map Markers";
            this.bbiDeleteSelectedTempMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDeleteSelectedTempMapObjects.Glyph")));
            this.bbiDeleteSelectedTempMapObjects.Id = 117;
            this.bbiDeleteSelectedTempMapObjects.Name = "bbiDeleteSelectedTempMapObjects";
            this.bbiDeleteSelectedTempMapObjects.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteSelectedTempMapObjects_ItemClick);
            // 
            // bbiDeleteAllMapMarkers
            // 
            this.bbiDeleteAllMapMarkers.Caption = "Delete ALL Map Markers";
            this.bbiDeleteAllMapMarkers.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDeleteAllMapMarkers.Glyph")));
            this.bbiDeleteAllMapMarkers.Id = 121;
            this.bbiDeleteAllMapMarkers.Name = "bbiDeleteAllMapMarkers";
            this.bbiDeleteAllMapMarkers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteAllMapMarkers_ItemClick);
            // 
            // bsiSetCentrePoint
            // 
            this.bsiSetCentrePoint.Caption = "Set Centre Point...";
            this.bsiSetCentrePoint.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiSetCentrePoint.Glyph")));
            this.bsiSetCentrePoint.Id = 141;
            this.bsiSetCentrePoint.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetSiteCentre, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetClientCentre)});
            this.bsiSetCentrePoint.Name = "bsiSetCentrePoint";
            // 
            // bbiSetSiteCentre
            // 
            this.bbiSetSiteCentre.Caption = "Set Site Centre Point";
            this.bbiSetSiteCentre.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSetSiteCentre.Glyph")));
            this.bbiSetSiteCentre.Id = 143;
            this.bbiSetSiteCentre.Name = "bbiSetSiteCentre";
            this.bbiSetSiteCentre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetSiteCentre_ItemClick);
            // 
            // bbiSetClientCentre
            // 
            this.bbiSetClientCentre.Caption = "Set Client Centre Point";
            this.bbiSetClientCentre.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSetClientCentre.Glyph")));
            this.bbiSetClientCentre.Id = 142;
            this.bbiSetClientCentre.Name = "bbiSetClientCentre";
            this.bbiSetClientCentre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetClientCentre_ItemClick);
            // 
            // bbiQueryTool
            // 
            this.bbiQueryTool.Caption = "Query";
            this.bbiQueryTool.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiQueryTool.Glyph")));
            this.bbiQueryTool.GroupIndex = 1;
            this.bbiQueryTool.Id = 85;
            this.bbiQueryTool.Name = "bbiQueryTool";
            superToolTip30.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem30.Text = "Query Object Tool - Information";
            toolTipItem30.LeftIndent = 6;
            superToolTip30.Items.Add(toolTipTitleItem30);
            superToolTip30.Items.Add(toolTipItem30);
            this.bbiQueryTool.SuperTip = superToolTip30;
            this.bbiQueryTool.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiQueryTool_ItemClick);
            // 
            // bbiMeasureLine
            // 
            this.bbiMeasureLine.Caption = "Measure";
            this.bbiMeasureLine.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiMeasureLine.Glyph")));
            this.bbiMeasureLine.GroupIndex = 1;
            this.bbiMeasureLine.Id = 64;
            this.bbiMeasureLine.Name = "bbiMeasureLine";
            superToolTip31.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem31.Text = "Measure Tool - Information";
            toolTipItem31.LeftIndent = 6;
            superToolTip31.Items.Add(toolTipTitleItem31);
            superToolTip31.Items.Add(toolTipItem31);
            this.bbiMeasureLine.SuperTip = superToolTip31;
            this.bbiMeasureLine.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMeasureLine_ItemClick);
            // 
            // bciMapMarkerAdd
            // 
            this.bciMapMarkerAdd.Caption = "Map Marker";
            this.bciMapMarkerAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bciMapMarkerAdd.Glyph")));
            this.bciMapMarkerAdd.GroupIndex = 1;
            this.bciMapMarkerAdd.Id = 118;
            this.bciMapMarkerAdd.Name = "bciMapMarkerAdd";
            superToolTip32.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem32.Text = "Map Marker Tool - Information";
            toolTipItem32.LeftIndent = 6;
            superToolTip32.Items.Add(toolTipTitleItem32);
            superToolTip32.Items.Add(toolTipItem32);
            this.bciMapMarkerAdd.SuperTip = superToolTip32;
            this.bciMapMarkerAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bciMapMarkerAdd_ItemClick);
            // 
            // bbiCreateIncident
            // 
            this.bbiCreateIncident.Caption = "Create Incident at this Location";
            this.bbiCreateIncident.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCreateIncident.Glyph")));
            this.bbiCreateIncident.Id = 133;
            this.bbiCreateIncident.Name = "bbiCreateIncident";
            this.bbiCreateIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreateIncident_ItemClick);
            // 
            // bbiEditMapObjects
            // 
            this.bbiEditMapObjects.Caption = "Edit Selected Map Objects";
            this.bbiEditMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEditMapObjects.Glyph")));
            this.bbiEditMapObjects.Id = 93;
            this.bbiEditMapObjects.Name = "bbiEditMapObjects";
            // 
            // barSubItemMapEdit
            // 
            this.barSubItemMapEdit.Caption = "Selected Objects";
            this.barSubItemMapEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItemMapEdit.Glyph")));
            this.barSubItemMapEdit.Id = 102;
            this.barSubItemMapEdit.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditMapObjects),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteMapObjects),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateDatasetFromMapObjects, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTransferToWorkOrder, true)});
            this.barSubItemMapEdit.MenuCaption = "Map Menu";
            this.barSubItemMapEdit.Name = "barSubItemMapEdit";
            this.barSubItemMapEdit.ShowMenuCaption = true;
            this.barSubItemMapEdit.Popup += new System.EventHandler(this.barSubItemMapEdit_Popup);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Edit Mode";
            this.barSubItem1.Id = 50;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // bciEditingNone
            // 
            this.bciEditingNone.Caption = "No Editing";
            this.bciEditingNone.Glyph = ((System.Drawing.Image)(resources.GetObject("bciEditingNone.Glyph")));
            this.bciEditingNone.GroupIndex = 2;
            this.bciEditingNone.Id = 51;
            this.bciEditingNone.Name = "bciEditingNone";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Text = "No Object Vertex Editing - Information\r\n";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to switch <b>off</b> Polygon and Polyline vertex node editing.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bciEditingNone.SuperTip = superToolTip5;
            this.bciEditingNone.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditingNone_CheckedChanged);
            // 
            // bciEditingMove
            // 
            this.bciEditingMove.BindableChecked = true;
            this.bciEditingMove.Caption = "Move \\ Resize";
            this.bciEditingMove.Checked = true;
            this.bciEditingMove.Glyph = ((System.Drawing.Image)(resources.GetObject("bciEditingMove.Glyph")));
            this.bciEditingMove.GroupIndex = 2;
            this.bciEditingMove.Id = 52;
            this.bciEditingMove.Name = "bciEditingMove";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Text = "Move \\ Resize Object - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to enable Polygon and Polyline <b>movement</b> and <b>resizing</b>.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bciEditingMove.SuperTip = superToolTip6;
            this.bciEditingMove.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditingMove_CheckedChanged);
            // 
            // bciEditingAdd
            // 
            this.bciEditingAdd.Caption = "Node Adding";
            this.bciEditingAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bciEditingAdd.Glyph")));
            this.bciEditingAdd.GroupIndex = 2;
            this.bciEditingAdd.Id = 53;
            this.bciEditingAdd.Name = "bciEditingAdd";
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Text = "Add Object Vertices - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to switch on Polygon and Polyline vertex <b>node adding.</b>\r\n\r\nTo use, " +
    "click on the map polygon\\polyline to select it then click in the position to add" +
    " the new vertex.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bciEditingAdd.SuperTip = superToolTip7;
            this.bciEditingAdd.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditingAdd_CheckedChanged);
            // 
            // bciEditingEdit
            // 
            this.bciEditingEdit.Caption = "Node Editing";
            this.bciEditingEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bciEditingEdit.Glyph")));
            this.bciEditingEdit.GroupIndex = 2;
            this.bciEditingEdit.Id = 54;
            this.bciEditingEdit.Name = "bciEditingEdit";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Text = "Edit Object Vertices - Information";
            toolTipItem8.LeftIndent = 6;
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bciEditingEdit.SuperTip = superToolTip8;
            this.bciEditingEdit.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditingEdit_CheckedChanged);
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(((DevExpress.XtraBars.BarLinkUserDefines)((DevExpress.XtraBars.BarLinkUserDefines.Caption | DevExpress.XtraBars.BarLinkUserDefines.Width))), this.beiPopupContainerScale, "Scale:", false, true, true, 106),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiScreenCoords),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiDistanceMeasured),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSnap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiLayerCount)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Custom 3";
            // 
            // beiPopupContainerScale
            // 
            this.beiPopupContainerScale.Caption = "Scale:";
            this.beiPopupContainerScale.Edit = this.repositoryItemPopupContainerEdit1;
            this.beiPopupContainerScale.EditValue = "Scale:";
            this.beiPopupContainerScale.EditWidth = 100;
            this.beiPopupContainerScale.Id = 62;
            this.beiPopupContainerScale.Name = "beiPopupContainerScale";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.CloseOnOuterMouseClick = false;
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControl2;
            this.repositoryItemPopupContainerEdit1.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit2_QueryResultValue);
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Controls.Add(this.btnSetScale);
            this.popupContainerControl2.Controls.Add(this.layoutControl4);
            this.popupContainerControl2.Controls.Add(this.btnViewFullExtent);
            this.popupContainerControl2.Location = new System.Drawing.Point(6, 7);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(194, 207);
            this.popupContainerControl2.TabIndex = 23;
            // 
            // btnSetScale
            // 
            this.btnSetScale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSetScale.Location = new System.Drawing.Point(3, 181);
            this.btnSetScale.Name = "btnSetScale";
            this.btnSetScale.Size = new System.Drawing.Size(75, 23);
            this.btnSetScale.TabIndex = 4;
            this.btnSetScale.Text = "OK";
            this.btnSetScale.Click += new System.EventHandler(this.btnSetScale_Click);
            // 
            // layoutControl4
            // 
            this.layoutControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl4.Controls.Add(this.seUserDefinedScale);
            this.layoutControl4.Controls.Add(this.gridSplitContainer7);
            this.layoutControl4.Location = new System.Drawing.Point(3, 3);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup7;
            this.layoutControl4.Size = new System.Drawing.Size(188, 176);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // seUserDefinedScale
            // 
            this.seUserDefinedScale.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seUserDefinedScale.Location = new System.Drawing.Point(100, 153);
            this.seUserDefinedScale.MenuManager = this.barManager1;
            this.seUserDefinedScale.Name = "seUserDefinedScale";
            this.seUserDefinedScale.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seUserDefinedScale.Properties.IsFloatValue = false;
            this.seUserDefinedScale.Properties.Mask.EditMask = "#####0";
            this.seUserDefinedScale.Properties.MaxValue = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.seUserDefinedScale.Size = new System.Drawing.Size(85, 20);
            this.seUserDefinedScale.StyleController = this.layoutControl4;
            this.seUserDefinedScale.TabIndex = 2;
            // 
            // gridSplitContainer7
            // 
            this.gridSplitContainer7.Grid = this.gridControl4;
            this.gridSplitContainer7.Location = new System.Drawing.Point(3, 3);
            this.gridSplitContainer7.Name = "gridSplitContainer7";
            this.gridSplitContainer7.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer7.Size = new System.Drawing.Size(182, 146);
            this.gridSplitContainer7.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp01254ATTreePickerscalelistBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(182, 146);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp01254ATTreePickerscalelistBindingSource
            // 
            this.sp01254ATTreePickerscalelistBindingSource.DataMember = "sp01254_AT_Tree_Picker_scale_list";
            this.sp01254ATTreePickerscalelistBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colScaleDescription,
            this.colScale});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colScale, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseDown);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colScaleDescription
            // 
            this.colScaleDescription.Caption = "Scale";
            this.colScaleDescription.FieldName = "ScaleDescription";
            this.colScaleDescription.Name = "colScaleDescription";
            this.colScaleDescription.OptionsColumn.AllowEdit = false;
            this.colScaleDescription.OptionsColumn.AllowFocus = false;
            this.colScaleDescription.OptionsColumn.ReadOnly = true;
            this.colScaleDescription.Visible = true;
            this.colScaleDescription.VisibleIndex = 0;
            this.colScaleDescription.Width = 162;
            // 
            // colScale
            // 
            this.colScale.Caption = "Scale Number";
            this.colScale.FieldName = "Scale";
            this.colScale.Name = "colScale";
            this.colScale.OptionsColumn.AllowEdit = false;
            this.colScale.OptionsColumn.AllowFocus = false;
            this.colScale.OptionsColumn.ReadOnly = true;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.layoutControlItem20});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(188, 176);
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.gridSplitContainer7;
            this.layoutControlItem19.CustomizationFormText = "Scale Grid:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(186, 150);
            this.layoutControlItem19.Text = "Scale Grid:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AllowHtmlStringInCaption = true;
            this.layoutControlItem20.Control = this.seUserDefinedScale;
            this.layoutControlItem20.CustomizationFormText = "User Defined Scale:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem20.Text = "User Defined Scale:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(94, 13);
            // 
            // btnViewFullExtent
            // 
            this.btnViewFullExtent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnViewFullExtent.Location = new System.Drawing.Point(98, 181);
            this.btnViewFullExtent.Name = "btnViewFullExtent";
            this.btnViewFullExtent.Size = new System.Drawing.Size(93, 22);
            this.btnViewFullExtent.TabIndex = 12;
            this.btnViewFullExtent.Text = "Full Map Extent";
            this.btnViewFullExtent.Click += new System.EventHandler(this.btnViewFullExtent_Click);
            // 
            // bsiScreenCoords
            // 
            this.bsiScreenCoords.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiScreenCoords.Caption = "Location:";
            this.bsiScreenCoords.Id = 42;
            this.bsiScreenCoords.Name = "bsiScreenCoords";
            this.bsiScreenCoords.Size = new System.Drawing.Size(200, 0);
            this.bsiScreenCoords.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiScreenCoords.Width = 200;
            // 
            // bsiDistanceMeasured
            // 
            this.bsiDistanceMeasured.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiDistanceMeasured.Caption = "Distance: 0.00";
            this.bsiDistanceMeasured.Id = 66;
            this.bsiDistanceMeasured.Name = "bsiDistanceMeasured";
            this.bsiDistanceMeasured.Size = new System.Drawing.Size(350, 0);
            this.bsiDistanceMeasured.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiDistanceMeasured.Width = 350;
            // 
            // bsiSnap
            // 
            this.bsiSnap.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiSnap.Caption = "Snap: Off";
            this.bsiSnap.Id = 46;
            this.bsiSnap.Name = "bsiSnap";
            this.bsiSnap.Size = new System.Drawing.Size(50, 0);
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Text = "Snap - Information";
            toolTipItem4.LeftIndent = 6;
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bsiSnap.SuperTip = superToolTip4;
            this.bsiSnap.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiSnap.Width = 50;
            this.bsiSnap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiSnap_ItemClick);
            // 
            // bsiLayerCount
            // 
            this.bsiLayerCount.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiLayerCount.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiLayerCount.Caption = "Layer Count: 0";
            this.bsiLayerCount.Id = 43;
            this.bsiLayerCount.Name = "bsiLayerCount";
            this.bsiLayerCount.Size = new System.Drawing.Size(100, 0);
            this.bsiLayerCount.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiLayerCount.Width = 100;
            // 
            // bsiZoom
            // 
            this.bsiZoom.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiZoom.Caption = "Zoom:";
            this.bsiZoom.Id = 38;
            this.bsiZoom.Name = "bsiZoom";
            this.bsiZoom.Size = new System.Drawing.Size(100, 0);
            this.bsiZoom.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiZoom.Width = 100;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Location:";
            this.barButtonItem1.Id = 39;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // dockManager1
            // 
            this.dockManager1.DockingOptions.HideImmediatelyOnAutoHide = true;
            this.dockManager1.DockModeVS2005FadeFramesCount = 1;
            this.dockManager1.DockModeVS2005FadeSpeed = 1;
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1,
            this.dockPanel5,
            this.dockPanel6,
            this.dockPanelGazetteer,
            this.dockPanelInspections});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.panelContainer1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            this.dockManager1.ActivePanelChanged += new DevExpress.XtraBars.Docking.ActivePanelChangedEventHandler(this.dockManager1_ActivePanelChanged);
            this.dockManager1.ClosedPanel += new DevExpress.XtraBars.Docking.DockPanelEventHandler(this.dockManager1_ClosedPanel);
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.controlContainer1);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanel1.ID = new System.Guid("0a8caa4f-7106-4f54-ae4e-fd61a3486fb4");
            this.dockPanel1.Location = new System.Drawing.Point(0, 670);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.OriginalSize = new System.Drawing.Size(200, 125);
            this.dockPanel1.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanel1.SavedIndex = 1;
            this.dockPanel1.Size = new System.Drawing.Size(1065, 125);
            this.dockPanel1.Text = "Events";
            this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.memoEdit1);
            this.controlContainer1.Location = new System.Drawing.Point(3, 29);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(1059, 93);
            this.controlContainer1.TabIndex = 0;
            // 
            // dockPanel5
            // 
            this.dockPanel5.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.dockPanel5.Appearance.Options.UseBackColor = true;
            this.dockPanel5.Controls.Add(this.dockPanel5_Container);
            this.dockPanel5.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel5.FloatVertical = true;
            this.dockPanel5.ID = new System.Guid("e34b70c8-e235-4065-aa91-83ac9221f164");
            this.dockPanel5.Location = new System.Drawing.Point(1185, 42);
            this.dockPanel5.Name = "dockPanel5";
            this.dockPanel5.OriginalSize = new System.Drawing.Size(195, 771);
            this.dockPanel5.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel5.SavedIndex = 0;
            this.dockPanel5.Size = new System.Drawing.Size(195, 686);
            this.dockPanel5.Text = "Map Legend";
            this.dockPanel5.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanel5.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanel5_ClosingPanel);
            // 
            // dockPanel5_Container
            // 
            this.dockPanel5_Container.Controls.Add(this.xtraScrollableControl1);
            this.dockPanel5_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel5_Container.Name = "dockPanel5_Container";
            this.dockPanel5_Container.Size = new System.Drawing.Size(189, 654);
            this.dockPanel5_Container.TabIndex = 0;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.xtraScrollableControl1.Appearance.Options.UseBackColor = true;
            this.xtraScrollableControl1.Controls.Add(this.pictureBoxLegend);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(189, 654);
            this.xtraScrollableControl1.TabIndex = 1;
            // 
            // pictureBoxLegend
            // 
            this.pictureBoxLegend.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxLegend.Name = "pictureBoxLegend";
            this.pictureBoxLegend.Size = new System.Drawing.Size(150, 300);
            this.pictureBoxLegend.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLegend.TabIndex = 0;
            this.pictureBoxLegend.TabStop = false;
            // 
            // dockPanel6
            // 
            this.dockPanel6.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.dockPanel6.Appearance.Options.UseBackColor = true;
            this.dockPanel6.Controls.Add(this.dockPanel6_Container);
            this.dockPanel6.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanel6.FloatLocation = new System.Drawing.Point(1118, 192);
            this.dockPanel6.FloatSize = new System.Drawing.Size(225, 70);
            this.dockPanel6.ID = new System.Guid("275ce391-de52-42d6-8177-dc0edbbfcecf");
            this.dockPanel6.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanel6.Name = "dockPanel6";
            this.dockPanel6.OriginalSize = new System.Drawing.Size(200, 69);
            this.dockPanel6.SavedIndex = 1;
            this.dockPanel6.Size = new System.Drawing.Size(225, 70);
            this.dockPanel6.Text = "Scale Bar";
            this.dockPanel6.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanel6.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanel6_ClosingPanel);
            this.dockPanel6.Resize += new System.EventHandler(this.dockPanel6_Resize);
            // 
            // dockPanel6_Container
            // 
            this.dockPanel6_Container.Controls.Add(this.pictureBoxScaleBar);
            this.dockPanel6_Container.Location = new System.Drawing.Point(2, 28);
            this.dockPanel6_Container.Name = "dockPanel6_Container";
            this.dockPanel6_Container.Size = new System.Drawing.Size(221, 40);
            this.dockPanel6_Container.TabIndex = 0;
            // 
            // pictureBoxScaleBar
            // 
            this.pictureBoxScaleBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxScaleBar.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxScaleBar.Name = "pictureBoxScaleBar";
            this.pictureBoxScaleBar.Size = new System.Drawing.Size(221, 40);
            this.pictureBoxScaleBar.TabIndex = 0;
            this.pictureBoxScaleBar.TabStop = false;
            this.pictureBoxScaleBar.Resize += new System.EventHandler(this.pictureBoxScaleBar_Resize);
            // 
            // dockPanelGazetteer
            // 
            this.dockPanelGazetteer.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.dockPanelGazetteer.Appearance.Options.UseBackColor = true;
            this.dockPanelGazetteer.Controls.Add(this.dockPanel7_Container);
            this.dockPanelGazetteer.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelGazetteer.FloatSize = new System.Drawing.Size(316, 554);
            this.dockPanelGazetteer.ID = new System.Guid("90273104-7905-4f79-ab1c-1b7deb4093f1");
            this.dockPanelGazetteer.Location = new System.Drawing.Point(1045, 42);
            this.dockPanelGazetteer.Name = "dockPanelGazetteer";
            this.dockPanelGazetteer.OriginalSize = new System.Drawing.Size(335, 200);
            this.dockPanelGazetteer.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelGazetteer.SavedIndex = 0;
            this.dockPanelGazetteer.Size = new System.Drawing.Size(335, 686);
            this.dockPanelGazetteer.Text = "Gazetteer";
            this.dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelGazetteer.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelGazetteer_ClosingPanel);
            // 
            // dockPanel7_Container
            // 
            this.dockPanel7_Container.Controls.Add(this.splitContainerControl1);
            this.dockPanel7_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel7_Container.Name = "dockPanel7_Container";
            this.dockPanel7_Container.Size = new System.Drawing.Size(329, 654);
            this.dockPanel7_Container.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.buttonEditGazetteerFindValue);
            this.splitContainerControl1.Panel1.Controls.Add(this.lookUpEditGazetteerSearchType);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl3);
            this.splitContainerControl1.Panel1.Controls.Add(this.radioGroupGazetteerMatchPattern);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Find Criteria   [Enter criteria then click Find button]";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl3);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Find Results   [Double click on result row to view on map]";
            this.splitContainerControl1.Size = new System.Drawing.Size(329, 654);
            this.splitContainerControl1.SplitterPosition = 98;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(3, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Search Type:";
            // 
            // buttonEditGazetteerFindValue
            // 
            this.buttonEditGazetteerFindValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditGazetteerFindValue.Location = new System.Drawing.Point(74, 50);
            this.buttonEditGazetteerFindValue.MenuManager = this.barManager1;
            this.buttonEditGazetteerFindValue.Name = "buttonEditGazetteerFindValue";
            this.buttonEditGazetteerFindValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Find", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject65, serializableAppearanceObject66, serializableAppearanceObject67, serializableAppearanceObject68, "", null, null, true)});
            this.buttonEditGazetteerFindValue.Properties.MaxLength = 100;
            this.buttonEditGazetteerFindValue.Size = new System.Drawing.Size(249, 20);
            this.buttonEditGazetteerFindValue.TabIndex = 5;
            this.buttonEditGazetteerFindValue.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditGazetteerFindValue_ButtonClick);
            this.buttonEditGazetteerFindValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonEditGazetteerFindValue_KeyPress);
            // 
            // lookUpEditGazetteerSearchType
            // 
            this.lookUpEditGazetteerSearchType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEditGazetteerSearchType.Location = new System.Drawing.Point(74, 3);
            this.lookUpEditGazetteerSearchType.MenuManager = this.barManager1;
            this.lookUpEditGazetteerSearchType.Name = "lookUpEditGazetteerSearchType";
            this.lookUpEditGazetteerSearchType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditGazetteerSearchType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Search Type", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("RecordOrder", "Record Order", 75, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEditGazetteerSearchType.Properties.DataSource = this.sp01311ATTreePickerGazetteerSearchTypesBindingSource;
            this.lookUpEditGazetteerSearchType.Properties.DisplayMember = "Description";
            this.lookUpEditGazetteerSearchType.Properties.NullText = "";
            this.lookUpEditGazetteerSearchType.Properties.ValueMember = "Description";
            this.lookUpEditGazetteerSearchType.Size = new System.Drawing.Size(249, 20);
            this.lookUpEditGazetteerSearchType.TabIndex = 1;
            this.lookUpEditGazetteerSearchType.EditValueChanged += new System.EventHandler(this.lookUpEditGazetteerSearchType_EditValueChanged);
            // 
            // sp01311ATTreePickerGazetteerSearchTypesBindingSource
            // 
            this.sp01311ATTreePickerGazetteerSearchTypesBindingSource.DataMember = "sp01311_AT_Tree_Picker_Gazetteer_Search_Types";
            this.sp01311ATTreePickerGazetteerSearchTypesBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(3, 53);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(66, 13);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Search Value:";
            // 
            // radioGroupGazetteerMatchPattern
            // 
            this.radioGroupGazetteerMatchPattern.EditValue = 0;
            this.radioGroupGazetteerMatchPattern.Location = new System.Drawing.Point(74, 26);
            this.radioGroupGazetteerMatchPattern.MenuManager = this.barManager1;
            this.radioGroupGazetteerMatchPattern.Name = "radioGroupGazetteerMatchPattern";
            this.radioGroupGazetteerMatchPattern.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupGazetteerMatchPattern.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupGazetteerMatchPattern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Starts With"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Contains")});
            this.radioGroupGazetteerMatchPattern.Size = new System.Drawing.Size(163, 21);
            this.radioGroupGazetteerMatchPattern.TabIndex = 4;
            // 
            // dockPanelInspections
            // 
            this.dockPanelInspections.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.dockPanelInspections.Appearance.Options.UseBackColor = true;
            this.dockPanelInspections.Controls.Add(this.controlContainer2);
            this.dockPanelInspections.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanelInspections.FloatSize = new System.Drawing.Size(681, 200);
            this.dockPanelInspections.ID = new System.Guid("d1644cdf-2a31-4a9f-93ae-00eeafa87f74");
            this.dockPanelInspections.Location = new System.Drawing.Point(340, 455);
            this.dockPanelInspections.Name = "dockPanelInspections";
            this.dockPanelInspections.OriginalSize = new System.Drawing.Size(200, 273);
            this.dockPanelInspections.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanelInspections.SavedIndex = 1;
            this.dockPanelInspections.Size = new System.Drawing.Size(1040, 273);
            this.dockPanelInspections.Text = "Inspections and Actions";
            this.dockPanelInspections.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelInspections.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelInspections_ClosingPanel);
            // 
            // controlContainer2
            // 
            this.controlContainer2.Controls.Add(this.splitContainerControl2);
            this.controlContainer2.Location = new System.Drawing.Point(3, 29);
            this.controlContainer2.Name = "controlContainer2";
            this.controlContainer2.Size = new System.Drawing.Size(1034, 241);
            this.controlContainer2.TabIndex = 0;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl6);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl7);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1034, 241);
            this.splitContainerControl2.SplitterPosition = 113;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp01367ATInspectionsForTreesBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageList5;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView7;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemMemoExEdit3});
            this.gridControl6.Size = new System.Drawing.Size(1034, 122);
            this.gridControl6.TabIndex = 0;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp01367ATInspectionsForTreesBindingSource
            // 
            this.sp01367ATInspectionsForTreesBindingSource.DataMember = "sp01367_AT_Inspections_For_Trees";
            this.sp01367ATInspectionsForTreesBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageList5
            // 
            this.imageList5.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList5.ImageStream")));
            this.imageList5.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList5.Images.SetKeyName(0, "add_16.png");
            this.imageList5.Images.SetKeyName(1, "edit_16.png");
            this.imageList5.Images.SetKeyName(2, "delete_16.png");
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID,
            this.gridColumn16,
            this.colLocalityID1,
            this.gridColumn14,
            this.colLocalityName1,
            this.gridColumn15,
            this.colTreeReference2,
            this.colTreeMappingID,
            this.colTreeSpeciesName,
            this.colTreeX,
            this.colTreeY,
            this.colTreePolygonXY,
            this.colInspectionReference,
            this.colInspectionInspector,
            this.colInspectionDate,
            this.colInspectionIncidentReference,
            this.colInspectionIncidentDate,
            this.colIncidentID,
            this.colInspectionAngleToVertical,
            this.colInspectionLastModified,
            this.colInspectionUserDefined1,
            this.colInspectionUserDefined2,
            this.colInspectionUserDefined3,
            this.colInspectionRemarks,
            this.colInspectionStemPhysical1,
            this.colInspectionStemPhysical2,
            this.colInspectionStemPhysical3,
            this.colInspectionStemDisease1,
            this.colInspectionStemDisease2,
            this.colInspectionStemDisease3,
            this.colInspectionCrownPhysical1,
            this.colInspectionCrownPhysical2,
            this.colInspectionCrownPhysical3,
            this.colInspectionCrownDisease1,
            this.colInspectionCrownDisease2,
            this.colInspectionCrownDisease3,
            this.colInspectionCrownFoliation1,
            this.colInspectionCrownFoliation2,
            this.colInspectionCrownFoliation3,
            this.colInspectionRiskCategory,
            this.colInspectionGeneralCondition,
            this.colInspectionBasePhysical1,
            this.colInspectionBasePhysical2,
            this.colInspectionBasePhysical3,
            this.colInspectionVitality,
            this.colLinkedActionCount,
            this.colLinkedOutstandingActionCount,
            this.colNoFurtherActionRequired,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38});
            this.gridView7.GridControl = this.gridControl6;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView7_CustomRowCellEdit);
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView7_CustomDrawEmptyForeground);
            this.gridView7.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView7_ShowingEditor);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.DoubleClick += new System.EventHandler(this.gridView7_DoubleClick);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Tree ID";
            this.gridColumn16.FieldName = "TreeID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 47;
            // 
            // colLocalityID1
            // 
            this.colLocalityID1.Caption = "Locality ID";
            this.colLocalityID1.FieldName = "LocalityID";
            this.colLocalityID1.Name = "colLocalityID1";
            this.colLocalityID1.OptionsColumn.AllowEdit = false;
            this.colLocalityID1.OptionsColumn.AllowFocus = false;
            this.colLocalityID1.OptionsColumn.ReadOnly = true;
            this.colLocalityID1.Width = 61;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "District ID";
            this.gridColumn14.FieldName = "DistrictID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 58;
            // 
            // colLocalityName1
            // 
            this.colLocalityName1.Caption = "Locality Name";
            this.colLocalityName1.FieldName = "LocalityName";
            this.colLocalityName1.Name = "colLocalityName1";
            this.colLocalityName1.OptionsColumn.AllowEdit = false;
            this.colLocalityName1.OptionsColumn.AllowFocus = false;
            this.colLocalityName1.OptionsColumn.ReadOnly = true;
            this.colLocalityName1.Width = 100;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "District Name";
            this.gridColumn15.FieldName = "DistrictName";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 97;
            // 
            // colTreeReference2
            // 
            this.colTreeReference2.Caption = "Tree Reference";
            this.colTreeReference2.FieldName = "TreeReference";
            this.colTreeReference2.Name = "colTreeReference2";
            this.colTreeReference2.OptionsColumn.AllowEdit = false;
            this.colTreeReference2.OptionsColumn.AllowFocus = false;
            this.colTreeReference2.OptionsColumn.ReadOnly = true;
            this.colTreeReference2.Visible = true;
            this.colTreeReference2.VisibleIndex = 0;
            this.colTreeReference2.Width = 109;
            // 
            // colTreeMappingID
            // 
            this.colTreeMappingID.Caption = "Map ID";
            this.colTreeMappingID.FieldName = "TreeMappingID";
            this.colTreeMappingID.Name = "colTreeMappingID";
            this.colTreeMappingID.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID.Width = 45;
            // 
            // colTreeSpeciesName
            // 
            this.colTreeSpeciesName.Caption = "Species";
            this.colTreeSpeciesName.FieldName = "TreeSpeciesName";
            this.colTreeSpeciesName.Name = "colTreeSpeciesName";
            this.colTreeSpeciesName.OptionsColumn.AllowEdit = false;
            this.colTreeSpeciesName.OptionsColumn.AllowFocus = false;
            this.colTreeSpeciesName.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesName.Width = 57;
            // 
            // colTreeX
            // 
            this.colTreeX.Caption = "Tree X Coordinate";
            this.colTreeX.FieldName = "TreeX";
            this.colTreeX.Name = "colTreeX";
            this.colTreeX.OptionsColumn.AllowEdit = false;
            this.colTreeX.OptionsColumn.AllowFocus = false;
            this.colTreeX.OptionsColumn.ReadOnly = true;
            this.colTreeX.Width = 98;
            // 
            // colTreeY
            // 
            this.colTreeY.Caption = "Tree Y Coordinate";
            this.colTreeY.FieldName = "TreeY";
            this.colTreeY.Name = "colTreeY";
            this.colTreeY.OptionsColumn.AllowEdit = false;
            this.colTreeY.OptionsColumn.AllowFocus = false;
            this.colTreeY.OptionsColumn.ReadOnly = true;
            this.colTreeY.Width = 98;
            // 
            // colTreePolygonXY
            // 
            this.colTreePolygonXY.Caption = "Tree Polygon XY";
            this.colTreePolygonXY.FieldName = "TreePolygonXY";
            this.colTreePolygonXY.Name = "colTreePolygonXY";
            this.colTreePolygonXY.OptionsColumn.ReadOnly = true;
            this.colTreePolygonXY.Width = 89;
            // 
            // colInspectionReference
            // 
            this.colInspectionReference.Caption = "Inspection Reference No";
            this.colInspectionReference.FieldName = "InspectionReference";
            this.colInspectionReference.Name = "colInspectionReference";
            this.colInspectionReference.OptionsColumn.AllowEdit = false;
            this.colInspectionReference.OptionsColumn.AllowFocus = false;
            this.colInspectionReference.OptionsColumn.ReadOnly = true;
            this.colInspectionReference.Visible = true;
            this.colInspectionReference.VisibleIndex = 1;
            this.colInspectionReference.Width = 140;
            // 
            // colInspectionInspector
            // 
            this.colInspectionInspector.Caption = "Inspector";
            this.colInspectionInspector.FieldName = "InspectionInspector";
            this.colInspectionInspector.Name = "colInspectionInspector";
            this.colInspectionInspector.OptionsColumn.AllowEdit = false;
            this.colInspectionInspector.OptionsColumn.AllowFocus = false;
            this.colInspectionInspector.OptionsColumn.ReadOnly = true;
            this.colInspectionInspector.Visible = true;
            this.colInspectionInspector.VisibleIndex = 2;
            this.colInspectionInspector.Width = 67;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Visible = true;
            this.colInspectionDate.VisibleIndex = 0;
            this.colInspectionDate.Width = 110;
            // 
            // colInspectionIncidentReference
            // 
            this.colInspectionIncidentReference.Caption = "Incident Reference No";
            this.colInspectionIncidentReference.FieldName = "InspectionIncidentReference";
            this.colInspectionIncidentReference.Name = "colInspectionIncidentReference";
            this.colInspectionIncidentReference.OptionsColumn.AllowEdit = false;
            this.colInspectionIncidentReference.OptionsColumn.AllowFocus = false;
            this.colInspectionIncidentReference.OptionsColumn.ReadOnly = true;
            this.colInspectionIncidentReference.Visible = true;
            this.colInspectionIncidentReference.VisibleIndex = 7;
            this.colInspectionIncidentReference.Width = 129;
            // 
            // colInspectionIncidentDate
            // 
            this.colInspectionIncidentDate.Caption = "Incident Date";
            this.colInspectionIncidentDate.FieldName = "InspectionIncidentDate";
            this.colInspectionIncidentDate.Name = "colInspectionIncidentDate";
            this.colInspectionIncidentDate.OptionsColumn.AllowEdit = false;
            this.colInspectionIncidentDate.OptionsColumn.AllowFocus = false;
            this.colInspectionIncidentDate.OptionsColumn.ReadOnly = true;
            this.colInspectionIncidentDate.Visible = true;
            this.colInspectionIncidentDate.VisibleIndex = 8;
            this.colInspectionIncidentDate.Width = 86;
            // 
            // colIncidentID
            // 
            this.colIncidentID.Caption = "Incident ID";
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.Width = 64;
            // 
            // colInspectionAngleToVertical
            // 
            this.colInspectionAngleToVertical.Caption = "Angle to Vertical";
            this.colInspectionAngleToVertical.FieldName = "InspectionAngleToVertical";
            this.colInspectionAngleToVertical.Name = "colInspectionAngleToVertical";
            this.colInspectionAngleToVertical.OptionsColumn.AllowEdit = false;
            this.colInspectionAngleToVertical.OptionsColumn.AllowFocus = false;
            this.colInspectionAngleToVertical.OptionsColumn.ReadOnly = true;
            this.colInspectionAngleToVertical.Visible = true;
            this.colInspectionAngleToVertical.VisibleIndex = 9;
            this.colInspectionAngleToVertical.Width = 99;
            // 
            // colInspectionLastModified
            // 
            this.colInspectionLastModified.Caption = "Inspection Last Modified";
            this.colInspectionLastModified.FieldName = "InspectionLastModified";
            this.colInspectionLastModified.Name = "colInspectionLastModified";
            this.colInspectionLastModified.OptionsColumn.AllowEdit = false;
            this.colInspectionLastModified.OptionsColumn.AllowFocus = false;
            this.colInspectionLastModified.OptionsColumn.ReadOnly = true;
            this.colInspectionLastModified.OptionsColumn.ShowInCustomizationForm = false;
            this.colInspectionLastModified.Width = 127;
            // 
            // colInspectionUserDefined1
            // 
            this.colInspectionUserDefined1.Caption = "User Defined 1";
            this.colInspectionUserDefined1.FieldName = "InspectionUserDefined1";
            this.colInspectionUserDefined1.Name = "colInspectionUserDefined1";
            this.colInspectionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined1.Visible = true;
            this.colInspectionUserDefined1.VisibleIndex = 20;
            this.colInspectionUserDefined1.Width = 145;
            // 
            // colInspectionUserDefined2
            // 
            this.colInspectionUserDefined2.Caption = "User Defined 2";
            this.colInspectionUserDefined2.FieldName = "InspectionUserDefined2";
            this.colInspectionUserDefined2.Name = "colInspectionUserDefined2";
            this.colInspectionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined2.Visible = true;
            this.colInspectionUserDefined2.VisibleIndex = 32;
            this.colInspectionUserDefined2.Width = 145;
            // 
            // colInspectionUserDefined3
            // 
            this.colInspectionUserDefined3.Caption = "User Defined 3";
            this.colInspectionUserDefined3.FieldName = "InspectionUserDefined3";
            this.colInspectionUserDefined3.Name = "colInspectionUserDefined3";
            this.colInspectionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined3.Visible = true;
            this.colInspectionUserDefined3.VisibleIndex = 33;
            this.colInspectionUserDefined3.Width = 145;
            // 
            // colInspectionRemarks
            // 
            this.colInspectionRemarks.Caption = "Remarks";
            this.colInspectionRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colInspectionRemarks.FieldName = "InspectionRemarks";
            this.colInspectionRemarks.Name = "colInspectionRemarks";
            this.colInspectionRemarks.OptionsColumn.AllowEdit = false;
            this.colInspectionRemarks.OptionsColumn.AllowFocus = false;
            this.colInspectionRemarks.OptionsColumn.ReadOnly = true;
            this.colInspectionRemarks.Visible = true;
            this.colInspectionRemarks.VisibleIndex = 10;
            this.colInspectionRemarks.Width = 115;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colInspectionStemPhysical1
            // 
            this.colInspectionStemPhysical1.Caption = "Stem Physical 1";
            this.colInspectionStemPhysical1.FieldName = "InspectionStemPhysical1";
            this.colInspectionStemPhysical1.Name = "colInspectionStemPhysical1";
            this.colInspectionStemPhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical1.Visible = true;
            this.colInspectionStemPhysical1.VisibleIndex = 11;
            this.colInspectionStemPhysical1.Width = 95;
            // 
            // colInspectionStemPhysical2
            // 
            this.colInspectionStemPhysical2.Caption = "Stem Physical 2";
            this.colInspectionStemPhysical2.FieldName = "InspectionStemPhysical2";
            this.colInspectionStemPhysical2.Name = "colInspectionStemPhysical2";
            this.colInspectionStemPhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical2.Visible = true;
            this.colInspectionStemPhysical2.VisibleIndex = 12;
            this.colInspectionStemPhysical2.Width = 95;
            // 
            // colInspectionStemPhysical3
            // 
            this.colInspectionStemPhysical3.Caption = "Stem Physical 3";
            this.colInspectionStemPhysical3.FieldName = "InspectionStemPhysical3";
            this.colInspectionStemPhysical3.Name = "colInspectionStemPhysical3";
            this.colInspectionStemPhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical3.Visible = true;
            this.colInspectionStemPhysical3.VisibleIndex = 13;
            this.colInspectionStemPhysical3.Width = 95;
            // 
            // colInspectionStemDisease1
            // 
            this.colInspectionStemDisease1.Caption = "Stem Disease 1";
            this.colInspectionStemDisease1.FieldName = "InspectionStemDisease1";
            this.colInspectionStemDisease1.Name = "colInspectionStemDisease1";
            this.colInspectionStemDisease1.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease1.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease1.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease1.Visible = true;
            this.colInspectionStemDisease1.VisibleIndex = 14;
            this.colInspectionStemDisease1.Width = 94;
            // 
            // colInspectionStemDisease2
            // 
            this.colInspectionStemDisease2.Caption = "Stem Disease 2";
            this.colInspectionStemDisease2.FieldName = "InspectionStemDisease2";
            this.colInspectionStemDisease2.Name = "colInspectionStemDisease2";
            this.colInspectionStemDisease2.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease2.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease2.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease2.Visible = true;
            this.colInspectionStemDisease2.VisibleIndex = 15;
            this.colInspectionStemDisease2.Width = 94;
            // 
            // colInspectionStemDisease3
            // 
            this.colInspectionStemDisease3.Caption = "Stem Disease 3";
            this.colInspectionStemDisease3.FieldName = "InspectionStemDisease3";
            this.colInspectionStemDisease3.Name = "colInspectionStemDisease3";
            this.colInspectionStemDisease3.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease3.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease3.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease3.Visible = true;
            this.colInspectionStemDisease3.VisibleIndex = 16;
            this.colInspectionStemDisease3.Width = 94;
            // 
            // colInspectionCrownPhysical1
            // 
            this.colInspectionCrownPhysical1.Caption = "Crown Physical 1";
            this.colInspectionCrownPhysical1.FieldName = "InspectionCrownPhysical1";
            this.colInspectionCrownPhysical1.Name = "colInspectionCrownPhysical1";
            this.colInspectionCrownPhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical1.Visible = true;
            this.colInspectionCrownPhysical1.VisibleIndex = 17;
            this.colInspectionCrownPhysical1.Width = 102;
            // 
            // colInspectionCrownPhysical2
            // 
            this.colInspectionCrownPhysical2.Caption = "Crown Physical 2";
            this.colInspectionCrownPhysical2.FieldName = "InspectionCrownPhysical2";
            this.colInspectionCrownPhysical2.Name = "colInspectionCrownPhysical2";
            this.colInspectionCrownPhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical2.Visible = true;
            this.colInspectionCrownPhysical2.VisibleIndex = 18;
            this.colInspectionCrownPhysical2.Width = 102;
            // 
            // colInspectionCrownPhysical3
            // 
            this.colInspectionCrownPhysical3.Caption = "Crown Physical 3";
            this.colInspectionCrownPhysical3.FieldName = "InspectionCrownPhysical3";
            this.colInspectionCrownPhysical3.Name = "colInspectionCrownPhysical3";
            this.colInspectionCrownPhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical3.Visible = true;
            this.colInspectionCrownPhysical3.VisibleIndex = 19;
            this.colInspectionCrownPhysical3.Width = 102;
            // 
            // colInspectionCrownDisease1
            // 
            this.colInspectionCrownDisease1.Caption = "Crown Disease 1";
            this.colInspectionCrownDisease1.FieldName = "InspectionCrownDisease1";
            this.colInspectionCrownDisease1.Name = "colInspectionCrownDisease1";
            this.colInspectionCrownDisease1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease1.Visible = true;
            this.colInspectionCrownDisease1.VisibleIndex = 21;
            this.colInspectionCrownDisease1.Width = 151;
            // 
            // colInspectionCrownDisease2
            // 
            this.colInspectionCrownDisease2.Caption = "Crown Disease 2";
            this.colInspectionCrownDisease2.FieldName = "InspectionCrownDisease2";
            this.colInspectionCrownDisease2.Name = "colInspectionCrownDisease2";
            this.colInspectionCrownDisease2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease2.Visible = true;
            this.colInspectionCrownDisease2.VisibleIndex = 22;
            this.colInspectionCrownDisease2.Width = 151;
            // 
            // colInspectionCrownDisease3
            // 
            this.colInspectionCrownDisease3.Caption = "Crown Disease 3";
            this.colInspectionCrownDisease3.FieldName = "InspectionCrownDisease3";
            this.colInspectionCrownDisease3.Name = "colInspectionCrownDisease3";
            this.colInspectionCrownDisease3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease3.Visible = true;
            this.colInspectionCrownDisease3.VisibleIndex = 23;
            this.colInspectionCrownDisease3.Width = 151;
            // 
            // colInspectionCrownFoliation1
            // 
            this.colInspectionCrownFoliation1.Caption = "Crown Foliation 1";
            this.colInspectionCrownFoliation1.FieldName = "InspectionCrownFoliation1";
            this.colInspectionCrownFoliation1.Name = "colInspectionCrownFoliation1";
            this.colInspectionCrownFoliation1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation1.Visible = true;
            this.colInspectionCrownFoliation1.VisibleIndex = 24;
            this.colInspectionCrownFoliation1.Width = 154;
            // 
            // colInspectionCrownFoliation2
            // 
            this.colInspectionCrownFoliation2.Caption = "Crown Foliation 2";
            this.colInspectionCrownFoliation2.FieldName = "InspectionCrownFoliation2";
            this.colInspectionCrownFoliation2.Name = "colInspectionCrownFoliation2";
            this.colInspectionCrownFoliation2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation2.Visible = true;
            this.colInspectionCrownFoliation2.VisibleIndex = 25;
            this.colInspectionCrownFoliation2.Width = 154;
            // 
            // colInspectionCrownFoliation3
            // 
            this.colInspectionCrownFoliation3.Caption = "Crown Foliation 3";
            this.colInspectionCrownFoliation3.FieldName = "InspectionCrownFoliation3";
            this.colInspectionCrownFoliation3.Name = "colInspectionCrownFoliation3";
            this.colInspectionCrownFoliation3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation3.Visible = true;
            this.colInspectionCrownFoliation3.VisibleIndex = 26;
            this.colInspectionCrownFoliation3.Width = 154;
            // 
            // colInspectionRiskCategory
            // 
            this.colInspectionRiskCategory.Caption = "Risk Category";
            this.colInspectionRiskCategory.FieldName = "InspectionRiskCategory";
            this.colInspectionRiskCategory.Name = "colInspectionRiskCategory";
            this.colInspectionRiskCategory.OptionsColumn.AllowEdit = false;
            this.colInspectionRiskCategory.OptionsColumn.AllowFocus = false;
            this.colInspectionRiskCategory.OptionsColumn.ReadOnly = true;
            this.colInspectionRiskCategory.Visible = true;
            this.colInspectionRiskCategory.VisibleIndex = 3;
            this.colInspectionRiskCategory.Width = 141;
            // 
            // colInspectionGeneralCondition
            // 
            this.colInspectionGeneralCondition.Caption = "General Condition";
            this.colInspectionGeneralCondition.FieldName = "InspectionGeneralCondition";
            this.colInspectionGeneralCondition.Name = "colInspectionGeneralCondition";
            this.colInspectionGeneralCondition.OptionsColumn.AllowEdit = false;
            this.colInspectionGeneralCondition.OptionsColumn.AllowFocus = false;
            this.colInspectionGeneralCondition.OptionsColumn.ReadOnly = true;
            this.colInspectionGeneralCondition.Visible = true;
            this.colInspectionGeneralCondition.VisibleIndex = 30;
            this.colInspectionGeneralCondition.Width = 159;
            // 
            // colInspectionBasePhysical1
            // 
            this.colInspectionBasePhysical1.Caption = "Base Physical 1";
            this.colInspectionBasePhysical1.FieldName = "InspectionBasePhysical1";
            this.colInspectionBasePhysical1.Name = "colInspectionBasePhysical1";
            this.colInspectionBasePhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical1.Visible = true;
            this.colInspectionBasePhysical1.VisibleIndex = 27;
            this.colInspectionBasePhysical1.Width = 144;
            // 
            // colInspectionBasePhysical2
            // 
            this.colInspectionBasePhysical2.Caption = "Base Physical  2";
            this.colInspectionBasePhysical2.FieldName = "InspectionBasePhysical2";
            this.colInspectionBasePhysical2.Name = "colInspectionBasePhysical2";
            this.colInspectionBasePhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical2.Visible = true;
            this.colInspectionBasePhysical2.VisibleIndex = 28;
            this.colInspectionBasePhysical2.Width = 144;
            // 
            // colInspectionBasePhysical3
            // 
            this.colInspectionBasePhysical3.Caption = "Base Physical 3";
            this.colInspectionBasePhysical3.FieldName = "InspectionBasePhysical3";
            this.colInspectionBasePhysical3.Name = "colInspectionBasePhysical3";
            this.colInspectionBasePhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical3.Visible = true;
            this.colInspectionBasePhysical3.VisibleIndex = 29;
            this.colInspectionBasePhysical3.Width = 144;
            // 
            // colInspectionVitality
            // 
            this.colInspectionVitality.Caption = "Vitality";
            this.colInspectionVitality.FieldName = "InspectionVitality";
            this.colInspectionVitality.Name = "colInspectionVitality";
            this.colInspectionVitality.OptionsColumn.AllowEdit = false;
            this.colInspectionVitality.OptionsColumn.AllowFocus = false;
            this.colInspectionVitality.OptionsColumn.ReadOnly = true;
            this.colInspectionVitality.Visible = true;
            this.colInspectionVitality.VisibleIndex = 31;
            this.colInspectionVitality.Width = 106;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Linked Action Count";
            this.colLinkedActionCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 4;
            this.colLinkedActionCount.Width = 116;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colLinkedOutstandingActionCount
            // 
            this.colLinkedOutstandingActionCount.Caption = "Linked Outstanding Action Count";
            this.colLinkedOutstandingActionCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedOutstandingActionCount.FieldName = "LinkedOutstandingActionCount";
            this.colLinkedOutstandingActionCount.Name = "colLinkedOutstandingActionCount";
            this.colLinkedOutstandingActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedOutstandingActionCount.Visible = true;
            this.colLinkedOutstandingActionCount.VisibleIndex = 5;
            this.colLinkedOutstandingActionCount.Width = 178;
            // 
            // colNoFurtherActionRequired
            // 
            this.colNoFurtherActionRequired.Caption = "No Further Action Required";
            this.colNoFurtherActionRequired.FieldName = "NoFurtherActionRequired";
            this.colNoFurtherActionRequired.Name = "colNoFurtherActionRequired";
            this.colNoFurtherActionRequired.OptionsColumn.AllowEdit = false;
            this.colNoFurtherActionRequired.OptionsColumn.AllowFocus = false;
            this.colNoFurtherActionRequired.OptionsColumn.ReadOnly = true;
            this.colNoFurtherActionRequired.Visible = true;
            this.colNoFurtherActionRequired.VisibleIndex = 6;
            this.colNoFurtherActionRequired.Width = 152;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "<b>Calculated 1</b>";
            this.gridColumn36.FieldName = "Calculated1";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 34;
            this.gridColumn36.Width = 90;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "<b>Calculated 2</b>";
            this.gridColumn37.FieldName = "Calculated2";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 35;
            this.gridColumn37.Width = 90;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "<b>Calculated 3</b>";
            this.gridColumn38.FieldName = "Calculated3";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 36;
            this.gridColumn38.Width = 90;
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp01380ATActionsLinkedToInspectionsBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageList5;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView8;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4});
            this.gridControl7.Size = new System.Drawing.Size(1034, 113);
            this.gridControl7.TabIndex = 0;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8,
            this.gridView9});
            // 
            // sp01380ATActionsLinkedToInspectionsBindingSource
            // 
            this.sp01380ATActionsLinkedToInspectionsBindingSource.DataMember = "sp01380_AT_Actions_Linked_To_Inspections";
            this.sp01380ATActionsLinkedToInspectionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID1,
            this.colTreeID1,
            this.colLocalityID2,
            this.colDistrictID2,
            this.colTreeReference1,
            this.colTreeMappingID1,
            this.colInspectionReference1,
            this.colInspectionDate1,
            this.colActionID,
            this.colActionJobNumber,
            this.colAction,
            this.colActionPriority,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colActionBy,
            this.colActionSupervisor,
            this.colActionOwnership,
            this.colActionCostCentre,
            this.colActionBudget,
            this.colActionWorkUnits,
            this.colActionScheduleOfRates,
            this.colActionBudgetedRateDescription,
            this.colActionBudgetedRate,
            this.colActionBudgetedCost,
            this.colActionActualRateDescription,
            this.colActionActualRate,
            this.colActionActualCost,
            this.colActionWorkOrder,
            this.colActionRemarks,
            this.colActionUserDefined1,
            this.colActionUserDefined2,
            this.colActionUserDefined3,
            this.colActionCostDifference,
            this.colActionCompletionStatus,
            this.colActionTimeliness,
            this.colDiscountRate});
            this.gridView8.GridControl = this.gridControl7;
            this.gridView8.GroupCount = 2;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDueDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView8_CustomDrawEmptyForeground);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.DoubleClick += new System.EventHandler(this.gridView8_DoubleClick);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colInspectionID1
            // 
            this.colInspectionID1.Caption = "Inspection ID";
            this.colInspectionID1.FieldName = "InspectionID";
            this.colInspectionID1.Name = "colInspectionID1";
            this.colInspectionID1.OptionsColumn.AllowEdit = false;
            this.colInspectionID1.OptionsColumn.AllowFocus = false;
            this.colInspectionID1.OptionsColumn.ReadOnly = true;
            this.colInspectionID1.Width = 76;
            // 
            // colTreeID1
            // 
            this.colTreeID1.Caption = "Tree ID";
            this.colTreeID1.FieldName = "TreeID";
            this.colTreeID1.Name = "colTreeID1";
            this.colTreeID1.OptionsColumn.AllowEdit = false;
            this.colTreeID1.OptionsColumn.AllowFocus = false;
            this.colTreeID1.OptionsColumn.ReadOnly = true;
            this.colTreeID1.Width = 48;
            // 
            // colLocalityID2
            // 
            this.colLocalityID2.Caption = "Locality ID";
            this.colLocalityID2.FieldName = "LocalityID";
            this.colLocalityID2.Name = "colLocalityID2";
            this.colLocalityID2.OptionsColumn.AllowEdit = false;
            this.colLocalityID2.OptionsColumn.AllowFocus = false;
            this.colLocalityID2.OptionsColumn.ReadOnly = true;
            this.colLocalityID2.Width = 62;
            // 
            // colDistrictID2
            // 
            this.colDistrictID2.Caption = "District ID";
            this.colDistrictID2.FieldName = "DistrictID";
            this.colDistrictID2.Name = "colDistrictID2";
            this.colDistrictID2.OptionsColumn.AllowEdit = false;
            this.colDistrictID2.OptionsColumn.AllowFocus = false;
            this.colDistrictID2.OptionsColumn.ReadOnly = true;
            this.colDistrictID2.Width = 59;
            // 
            // colTreeReference1
            // 
            this.colTreeReference1.Caption = "Tree Reference";
            this.colTreeReference1.FieldName = "TreeReference";
            this.colTreeReference1.Name = "colTreeReference1";
            this.colTreeReference1.OptionsColumn.AllowEdit = false;
            this.colTreeReference1.OptionsColumn.AllowFocus = false;
            this.colTreeReference1.OptionsColumn.ReadOnly = true;
            this.colTreeReference1.Width = 97;
            // 
            // colTreeMappingID1
            // 
            this.colTreeMappingID1.Caption = "Map ID";
            this.colTreeMappingID1.FieldName = "TreeMappingID";
            this.colTreeMappingID1.Name = "colTreeMappingID1";
            this.colTreeMappingID1.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID1.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID1.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID1.Width = 46;
            // 
            // colInspectionReference1
            // 
            this.colInspectionReference1.Caption = "Inspection Reference";
            this.colInspectionReference1.FieldName = "InspectionReference";
            this.colInspectionReference1.Name = "colInspectionReference1";
            this.colInspectionReference1.OptionsColumn.AllowEdit = false;
            this.colInspectionReference1.OptionsColumn.AllowFocus = false;
            this.colInspectionReference1.OptionsColumn.ReadOnly = true;
            this.colInspectionReference1.Visible = true;
            this.colInspectionReference1.VisibleIndex = 0;
            this.colInspectionReference1.Width = 125;
            // 
            // colInspectionDate1
            // 
            this.colInspectionDate1.Caption = "Inspection Date";
            this.colInspectionDate1.FieldName = "InspectionDate";
            this.colInspectionDate1.Name = "colInspectionDate1";
            this.colInspectionDate1.OptionsColumn.AllowEdit = false;
            this.colInspectionDate1.OptionsColumn.AllowFocus = false;
            this.colInspectionDate1.OptionsColumn.ReadOnly = true;
            this.colInspectionDate1.Width = 98;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            this.colActionID.Width = 56;
            // 
            // colActionJobNumber
            // 
            this.colActionJobNumber.Caption = "Job Number";
            this.colActionJobNumber.FieldName = "ActionJobNumber";
            this.colActionJobNumber.Name = "colActionJobNumber";
            this.colActionJobNumber.OptionsColumn.AllowEdit = false;
            this.colActionJobNumber.OptionsColumn.AllowFocus = false;
            this.colActionJobNumber.OptionsColumn.ReadOnly = true;
            this.colActionJobNumber.Visible = true;
            this.colActionJobNumber.VisibleIndex = 1;
            this.colActionJobNumber.Width = 106;
            // 
            // colAction
            // 
            this.colAction.Caption = "Action";
            this.colAction.FieldName = "Action";
            this.colAction.Name = "colAction";
            this.colAction.OptionsColumn.AllowEdit = false;
            this.colAction.OptionsColumn.AllowFocus = false;
            this.colAction.OptionsColumn.ReadOnly = true;
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 2;
            this.colAction.Width = 114;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 3;
            this.colActionPriority.Width = 119;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Due Date";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 4;
            this.colActionDueDate.Width = 88;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Done Date";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 5;
            this.colActionDoneDate.Width = 79;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action By";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            this.colActionBy.Visible = true;
            this.colActionBy.VisibleIndex = 8;
            this.colActionBy.Width = 126;
            // 
            // colActionSupervisor
            // 
            this.colActionSupervisor.Caption = "Supervisor";
            this.colActionSupervisor.FieldName = "ActionSupervisor";
            this.colActionSupervisor.Name = "colActionSupervisor";
            this.colActionSupervisor.OptionsColumn.AllowEdit = false;
            this.colActionSupervisor.OptionsColumn.AllowFocus = false;
            this.colActionSupervisor.OptionsColumn.ReadOnly = true;
            this.colActionSupervisor.Visible = true;
            this.colActionSupervisor.VisibleIndex = 9;
            this.colActionSupervisor.Width = 125;
            // 
            // colActionOwnership
            // 
            this.colActionOwnership.Caption = "Ownership";
            this.colActionOwnership.FieldName = "ActionOwnership";
            this.colActionOwnership.Name = "colActionOwnership";
            this.colActionOwnership.OptionsColumn.AllowEdit = false;
            this.colActionOwnership.OptionsColumn.AllowFocus = false;
            this.colActionOwnership.OptionsColumn.ReadOnly = true;
            this.colActionOwnership.Visible = true;
            this.colActionOwnership.VisibleIndex = 10;
            this.colActionOwnership.Width = 118;
            // 
            // colActionCostCentre
            // 
            this.colActionCostCentre.Caption = "Cost Centre";
            this.colActionCostCentre.FieldName = "ActionCostCentre";
            this.colActionCostCentre.Name = "colActionCostCentre";
            this.colActionCostCentre.OptionsColumn.AllowEdit = false;
            this.colActionCostCentre.OptionsColumn.AllowFocus = false;
            this.colActionCostCentre.OptionsColumn.ReadOnly = true;
            this.colActionCostCentre.Visible = true;
            this.colActionCostCentre.VisibleIndex = 11;
            this.colActionCostCentre.Width = 124;
            // 
            // colActionBudget
            // 
            this.colActionBudget.Caption = "Budget";
            this.colActionBudget.FieldName = "ActionBudget";
            this.colActionBudget.Name = "colActionBudget";
            this.colActionBudget.OptionsColumn.AllowEdit = false;
            this.colActionBudget.OptionsColumn.AllowFocus = false;
            this.colActionBudget.OptionsColumn.ReadOnly = true;
            this.colActionBudget.Visible = true;
            this.colActionBudget.VisibleIndex = 12;
            this.colActionBudget.Width = 111;
            // 
            // colActionWorkUnits
            // 
            this.colActionWorkUnits.Caption = "Work Units";
            this.colActionWorkUnits.ColumnEdit = this.repositoryItemTextEdit4;
            this.colActionWorkUnits.FieldName = "ActionWorkUnits";
            this.colActionWorkUnits.Name = "colActionWorkUnits";
            this.colActionWorkUnits.OptionsColumn.AllowEdit = false;
            this.colActionWorkUnits.OptionsColumn.AllowFocus = false;
            this.colActionWorkUnits.OptionsColumn.ReadOnly = true;
            this.colActionWorkUnits.Visible = true;
            this.colActionWorkUnits.VisibleIndex = 13;
            this.colActionWorkUnits.Width = 74;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "n2";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Schedule Of Rates";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 14;
            this.colActionScheduleOfRates.Width = 111;
            // 
            // colActionBudgetedRateDescription
            // 
            this.colActionBudgetedRateDescription.Caption = "Budgeted Rate Desc";
            this.colActionBudgetedRateDescription.FieldName = "ActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.Name = "colActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRateDescription.Visible = true;
            this.colActionBudgetedRateDescription.VisibleIndex = 15;
            this.colActionBudgetedRateDescription.Width = 165;
            // 
            // colActionBudgetedRate
            // 
            this.colActionBudgetedRate.Caption = "Budgeted Rate";
            this.colActionBudgetedRate.ColumnEdit = this.repositoryItemTextEdit3;
            this.colActionBudgetedRate.FieldName = "ActionBudgetedRate";
            this.colActionBudgetedRate.Name = "colActionBudgetedRate";
            this.colActionBudgetedRate.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRate.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRate.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRate.Visible = true;
            this.colActionBudgetedRate.VisibleIndex = 16;
            this.colActionBudgetedRate.Width = 94;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "c";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colActionBudgetedCost
            // 
            this.colActionBudgetedCost.Caption = "Budgeted Cost";
            this.colActionBudgetedCost.ColumnEdit = this.repositoryItemTextEdit3;
            this.colActionBudgetedCost.FieldName = "ActionBudgetedCost";
            this.colActionBudgetedCost.Name = "colActionBudgetedCost";
            this.colActionBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedCost.Visible = true;
            this.colActionBudgetedCost.VisibleIndex = 17;
            this.colActionBudgetedCost.Width = 93;
            // 
            // colActionActualRateDescription
            // 
            this.colActionActualRateDescription.Caption = "Actual Rate Desc";
            this.colActionActualRateDescription.FieldName = "ActionActualRateDescription";
            this.colActionActualRateDescription.Name = "colActionActualRateDescription";
            this.colActionActualRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionActualRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionActualRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionActualRateDescription.Visible = true;
            this.colActionActualRateDescription.VisibleIndex = 18;
            this.colActionActualRateDescription.Width = 148;
            // 
            // colActionActualRate
            // 
            this.colActionActualRate.Caption = "Actual Rate";
            this.colActionActualRate.ColumnEdit = this.repositoryItemTextEdit3;
            this.colActionActualRate.FieldName = "ActionActualRate";
            this.colActionActualRate.Name = "colActionActualRate";
            this.colActionActualRate.OptionsColumn.AllowEdit = false;
            this.colActionActualRate.OptionsColumn.AllowFocus = false;
            this.colActionActualRate.OptionsColumn.ReadOnly = true;
            this.colActionActualRate.Visible = true;
            this.colActionActualRate.VisibleIndex = 19;
            this.colActionActualRate.Width = 78;
            // 
            // colActionActualCost
            // 
            this.colActionActualCost.Caption = "Actual Cost";
            this.colActionActualCost.ColumnEdit = this.repositoryItemTextEdit3;
            this.colActionActualCost.FieldName = "ActionActualCost";
            this.colActionActualCost.Name = "colActionActualCost";
            this.colActionActualCost.OptionsColumn.AllowEdit = false;
            this.colActionActualCost.OptionsColumn.AllowFocus = false;
            this.colActionActualCost.OptionsColumn.ReadOnly = true;
            this.colActionActualCost.Visible = true;
            this.colActionActualCost.VisibleIndex = 21;
            this.colActionActualCost.Width = 77;
            // 
            // colActionWorkOrder
            // 
            this.colActionWorkOrder.Caption = "Work Order";
            this.colActionWorkOrder.FieldName = "ActionWorkOrder";
            this.colActionWorkOrder.Name = "colActionWorkOrder";
            this.colActionWorkOrder.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrder.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrder.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrder.Visible = true;
            this.colActionWorkOrder.VisibleIndex = 23;
            this.colActionWorkOrder.Width = 111;
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.Caption = "Remarks";
            this.colActionRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 24;
            this.colActionRemarks.Width = 63;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colActionUserDefined1
            // 
            this.colActionUserDefined1.Caption = "User 1";
            this.colActionUserDefined1.FieldName = "ActionUserDefined1";
            this.colActionUserDefined1.Name = "colActionUserDefined1";
            this.colActionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined1.Visible = true;
            this.colActionUserDefined1.VisibleIndex = 25;
            this.colActionUserDefined1.Width = 53;
            // 
            // colActionUserDefined2
            // 
            this.colActionUserDefined2.Caption = "User 2";
            this.colActionUserDefined2.FieldName = "ActionUserDefined2";
            this.colActionUserDefined2.Name = "colActionUserDefined2";
            this.colActionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined2.Visible = true;
            this.colActionUserDefined2.VisibleIndex = 26;
            this.colActionUserDefined2.Width = 53;
            // 
            // colActionUserDefined3
            // 
            this.colActionUserDefined3.Caption = "User 3";
            this.colActionUserDefined3.FieldName = "ActionUserDefined3";
            this.colActionUserDefined3.Name = "colActionUserDefined3";
            this.colActionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined3.Visible = true;
            this.colActionUserDefined3.VisibleIndex = 27;
            this.colActionUserDefined3.Width = 53;
            // 
            // colActionCostDifference
            // 
            this.colActionCostDifference.Caption = "Cost Difference";
            this.colActionCostDifference.ColumnEdit = this.repositoryItemTextEdit3;
            this.colActionCostDifference.FieldName = "ActionCostDifference";
            this.colActionCostDifference.Name = "colActionCostDifference";
            this.colActionCostDifference.OptionsColumn.AllowEdit = false;
            this.colActionCostDifference.OptionsColumn.AllowFocus = false;
            this.colActionCostDifference.OptionsColumn.ReadOnly = true;
            this.colActionCostDifference.Visible = true;
            this.colActionCostDifference.VisibleIndex = 22;
            this.colActionCostDifference.Width = 97;
            // 
            // colActionCompletionStatus
            // 
            this.colActionCompletionStatus.Caption = "Completion Status";
            this.colActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.colActionCompletionStatus.Name = "colActionCompletionStatus";
            this.colActionCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colActionCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colActionCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colActionCompletionStatus.Visible = true;
            this.colActionCompletionStatus.VisibleIndex = 7;
            this.colActionCompletionStatus.Width = 109;
            // 
            // colActionTimeliness
            // 
            this.colActionTimeliness.Caption = "Timeliness";
            this.colActionTimeliness.FieldName = "ActionTimeliness";
            this.colActionTimeliness.Name = "colActionTimeliness";
            this.colActionTimeliness.OptionsColumn.AllowEdit = false;
            this.colActionTimeliness.OptionsColumn.AllowFocus = false;
            this.colActionTimeliness.OptionsColumn.ReadOnly = true;
            this.colActionTimeliness.Visible = true;
            this.colActionTimeliness.VisibleIndex = 6;
            this.colActionTimeliness.Width = 70;
            // 
            // colDiscountRate
            // 
            this.colDiscountRate.Caption = "Discount Rate";
            this.colDiscountRate.FieldName = "DiscountRate";
            this.colDiscountRate.Name = "colDiscountRate";
            this.colDiscountRate.OptionsColumn.AllowEdit = false;
            this.colDiscountRate.OptionsColumn.AllowFocus = false;
            this.colDiscountRate.OptionsColumn.ReadOnly = true;
            this.colDiscountRate.Visible = true;
            this.colDiscountRate.VisibleIndex = 20;
            this.colDiscountRate.Width = 89;
            // 
            // gridView9
            // 
            this.gridView9.GridControl = this.gridControl7;
            this.gridView9.Name = "gridView9";
            // 
            // panelContainer1
            // 
            this.panelContainer1.ActiveChild = this.dockPanel2;
            this.panelContainer1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panelContainer1.Appearance.Options.UseBackColor = true;
            this.panelContainer1.Controls.Add(this.dockPanel4);
            this.panelContainer1.Controls.Add(this.dockPanel2);
            this.panelContainer1.Controls.Add(this.dockPanel3);
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.panelContainer1.ID = new System.Guid("7fafffa1-da04-4520-9230-b81e0dad9de0");
            this.panelContainer1.Location = new System.Drawing.Point(0, 42);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.Options.ShowCloseButton = false;
            this.panelContainer1.OriginalSize = new System.Drawing.Size(340, 200);
            this.panelContainer1.Size = new System.Drawing.Size(340, 686);
            this.panelContainer1.Tabbed = true;
            this.panelContainer1.Text = "Map Control";
            // 
            // dockPanel2
            // 
            this.dockPanel2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel2.Appearance.Options.UseBackColor = true;
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel2.ID = new System.Guid("113275df-84df-4518-a683-8d7c0d90732f");
            this.dockPanel2.Location = new System.Drawing.Point(3, 29);
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.Options.ShowCloseButton = false;
            this.dockPanel2.OriginalSize = new System.Drawing.Size(334, 698);
            this.dockPanel2.Size = new System.Drawing.Size(334, 633);
            this.dockPanel2.Text = "Map Objects";
            this.dockPanel2.Click += new System.EventHandler(this.dockPanel2_Click);
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.layoutControl1);
            this.dockPanel2_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(334, 633);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.buttonEditSiteFilter2);
            this.layoutControl1.Controls.Add(this.buttonEditClientFilter2);
            this.layoutControl1.Controls.Add(this.buttonEditSiteFilter);
            this.layoutControl1.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl1.Controls.Add(this.popupContainerEditAssetTypes);
            this.layoutControl1.Controls.Add(this.gridSplitContainer1);
            this.layoutControl1.Controls.Add(this.btnClearMapObjects);
            this.layoutControl1.Controls.Add(this.popupContainerEditTreeRefStructure);
            this.layoutControl1.Controls.Add(this.btnSelectHighlighted);
            this.layoutControl1.Controls.Add(this.btnRefreshMapObjects);
            this.layoutControl1.Controls.Add(this.csScaleMapForHighlighted);
            this.layoutControl1.Controls.Add(this.ceCentreMap);
            this.layoutControl1.Controls.Add(this.gridSplitContainer2);
            this.layoutControl1.Controls.Add(this.checkEditSyncSelection);
            this.layoutControl1.Controls.Add(this.colorEditHighlight);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(334, 633);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // buttonEditSiteFilter2
            // 
            this.buttonEditSiteFilter2.Location = new System.Drawing.Point(89, 78);
            this.buttonEditSiteFilter2.MenuManager = this.barManager1;
            this.buttonEditSiteFilter2.Name = "buttonEditSiteFilter2";
            this.buttonEditSiteFilter2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Choose Site Filter Screen", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, true)});
            this.buttonEditSiteFilter2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSiteFilter2.Size = new System.Drawing.Size(235, 20);
            this.buttonEditSiteFilter2.StyleController = this.layoutControl1;
            this.buttonEditSiteFilter2.TabIndex = 29;
            this.buttonEditSiteFilter2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSiteFilter2_ButtonClick);
            // 
            // buttonEditClientFilter2
            // 
            this.buttonEditClientFilter2.Location = new System.Drawing.Point(89, 54);
            this.buttonEditClientFilter2.MenuManager = this.barManager1;
            this.buttonEditClientFilter2.Name = "buttonEditClientFilter2";
            this.buttonEditClientFilter2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Client Filter Screen", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Filter", "clear", null, true)});
            this.buttonEditClientFilter2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter2.Size = new System.Drawing.Size(235, 20);
            this.buttonEditClientFilter2.StyleController = this.layoutControl1;
            this.buttonEditClientFilter2.TabIndex = 29;
            this.buttonEditClientFilter2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter2_ButtonClick);
            // 
            // buttonEditSiteFilter
            // 
            this.buttonEditSiteFilter.Location = new System.Drawing.Point(89, 102);
            this.buttonEditSiteFilter.MenuManager = this.barManager1;
            this.buttonEditSiteFilter.Name = "buttonEditSiteFilter";
            this.buttonEditSiteFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click to open Choose Site Filter Screen", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Clear Filter", "clear", null, true)});
            this.buttonEditSiteFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSiteFilter.Size = new System.Drawing.Size(235, 20);
            this.buttonEditSiteFilter.StyleController = this.layoutControl1;
            this.buttonEditSiteFilter.TabIndex = 28;
            this.buttonEditSiteFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSiteFilter_ButtonClick);
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.Location = new System.Drawing.Point(89, 78);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Click to open Choose Client Filter Screen", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Clear Filter", "clear", null, true)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(235, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl1;
            this.buttonEditClientFilter.TabIndex = 28;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // popupContainerEditAssetTypes
            // 
            this.popupContainerEditAssetTypes.EditValue = "No Asset Type Filter";
            this.popupContainerEditAssetTypes.Location = new System.Drawing.Point(89, 102);
            this.popupContainerEditAssetTypes.MenuManager = this.barManager1;
            this.popupContainerEditAssetTypes.Name = "popupContainerEditAssetTypes";
            toolTipTitleItem33.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem33.Appearance.Options.UseImage = true;
            toolTipTitleItem33.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem33.Text = "Refresh List - Information";
            toolTipItem33.LeftIndent = 6;
            toolTipItem33.Text = "Click to refresh the list of available Map Objects based upon the selected Sites " +
    "and Asset Types.\r\n";
            superToolTip33.Items.Add(toolTipTitleItem33);
            superToolTip33.Items.Add(toolTipItem33);
            this.popupContainerEditAssetTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "", "DropDown", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Refresh List", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "", "RefreshList", superToolTip33, true)});
            this.popupContainerEditAssetTypes.Properties.PopupControl = this.popupContainerControlAssetTypes;
            this.popupContainerEditAssetTypes.Size = new System.Drawing.Size(235, 20);
            this.popupContainerEditAssetTypes.StyleController = this.layoutControl1;
            superToolTip34.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem34.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem34.Appearance.Options.UseImage = true;
            toolTipTitleItem34.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem34.Text = "Asset Type(s) Filter - Information";
            toolTipItem34.LeftIndent = 6;
            toolTipItem34.Text = "Select one or more asset types to filter the list of available map objects by.\r\n\r" +
    "\n<b><color=green>Tip:</color></b> Select no asset types to return all available " +
    "map objects regardless of asset type.";
            superToolTip34.Items.Add(toolTipTitleItem34);
            superToolTip34.Items.Add(toolTipItem34);
            this.popupContainerEditAssetTypes.SuperTip = superToolTip34;
            this.popupContainerEditAssetTypes.TabIndex = 15;
            this.popupContainerEditAssetTypes.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditAssetTypes_QueryResultValue);
            this.popupContainerEditAssetTypes.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PopupContainerEditAssetTypes_ButtonClick);
            // 
            // popupContainerControlAssetTypes
            // 
            this.popupContainerControlAssetTypes.Controls.Add(this.gridSplitContainer6);
            this.popupContainerControlAssetTypes.Controls.Add(this.btnOkAssetTypes);
            this.popupContainerControlAssetTypes.Location = new System.Drawing.Point(206, 5);
            this.popupContainerControlAssetTypes.Name = "popupContainerControlAssetTypes";
            this.popupContainerControlAssetTypes.Size = new System.Drawing.Size(312, 253);
            this.popupContainerControlAssetTypes.TabIndex = 27;
            // 
            // gridSplitContainer6
            // 
            this.gridSplitContainer6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer6.Grid = this.gridControlAssetTypes;
            this.gridSplitContainer6.Location = new System.Drawing.Point(2, 3);
            this.gridSplitContainer6.Name = "gridSplitContainer6";
            this.gridSplitContainer6.Panel1.Controls.Add(this.gridControlAssetTypes);
            this.gridSplitContainer6.Size = new System.Drawing.Size(307, 222);
            this.gridSplitContainer6.TabIndex = 3;
            // 
            // gridControlAssetTypes
            // 
            this.gridControlAssetTypes.DataSource = this.sp03021EPAssetTypeFilterDropDownBindingSource;
            this.gridControlAssetTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAssetTypes.Location = new System.Drawing.Point(0, 0);
            this.gridControlAssetTypes.MainView = this.gridViewAssetTypes;
            this.gridControlAssetTypes.MenuManager = this.barManager1;
            this.gridControlAssetTypes.Name = "gridControlAssetTypes";
            this.gridControlAssetTypes.Size = new System.Drawing.Size(307, 222);
            this.gridControlAssetTypes.TabIndex = 3;
            this.gridControlAssetTypes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAssetTypes});
            // 
            // sp03021EPAssetTypeFilterDropDownBindingSource
            // 
            this.sp03021EPAssetTypeFilterDropDownBindingSource.DataMember = "sp03021_EP_Asset_Type_Filter_DropDown";
            this.sp03021EPAssetTypeFilterDropDownBindingSource.DataSource = this.dataSet_EP;
            // 
            // dataSet_EP
            // 
            this.dataSet_EP.DataSetName = "DataSet_EP";
            this.dataSet_EP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewAssetTypes
            // 
            this.gridViewAssetTypes.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48});
            this.gridViewAssetTypes.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn47;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = "0";
            this.gridViewAssetTypes.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridViewAssetTypes.GridControl = this.gridControlAssetTypes;
            this.gridViewAssetTypes.Name = "gridViewAssetTypes";
            this.gridViewAssetTypes.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewAssetTypes.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewAssetTypes.OptionsLayout.StoreAppearance = true;
            this.gridViewAssetTypes.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewAssetTypes.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewAssetTypes.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewAssetTypes.OptionsView.ColumnAutoWidth = false;
            this.gridViewAssetTypes.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewAssetTypes.OptionsView.ShowGroupPanel = false;
            this.gridViewAssetTypes.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewAssetTypes.OptionsView.ShowIndicator = false;
            this.gridViewAssetTypes.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn48, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Asset Type Description";
            this.gridColumn46.FieldName = "AssetTypeDescription";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 0;
            this.gridColumn46.Width = 291;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Order";
            this.gridColumn48.FieldName = "AssetTypeOrder";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 1;
            this.gridColumn48.Width = 64;
            // 
            // btnOkAssetTypes
            // 
            this.btnOkAssetTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOkAssetTypes.Location = new System.Drawing.Point(3, 227);
            this.btnOkAssetTypes.Name = "btnOkAssetTypes";
            this.btnOkAssetTypes.Size = new System.Drawing.Size(75, 23);
            this.btnOkAssetTypes.TabIndex = 2;
            this.btnOkAssetTypes.Text = "OK";
            this.btnOkAssetTypes.Click += new System.EventHandler(this.btnOkAssetTypes_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControlAssetObjects;
            this.gridSplitContainer1.Location = new System.Drawing.Point(10, 126);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControlAssetObjects);
            this.gridSplitContainer1.Size = new System.Drawing.Size(314, 363);
            this.gridSplitContainer1.TabIndex = 16;
            // 
            // gridControlAssetObjects
            // 
            this.gridControlAssetObjects.DataSource = this.sp03069TreePickerAssetObjectsListBindingSource;
            this.gridControlAssetObjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlAssetObjects.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlAssetObjects.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Refresh Data", "refresh")});
            this.gridControlAssetObjects.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAssetObjects_EmbeddedNavigator_ButtonClick);
            this.gridControlAssetObjects.Location = new System.Drawing.Point(0, 0);
            this.gridControlAssetObjects.MainView = this.gridViewAssetObjects;
            this.gridControlAssetObjects.MenuManager = this.barManager1;
            this.gridControlAssetObjects.Name = "gridControlAssetObjects";
            this.gridControlAssetObjects.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEditHighlight2});
            this.gridControlAssetObjects.Size = new System.Drawing.Size(314, 363);
            this.gridControlAssetObjects.TabIndex = 13;
            this.gridControlAssetObjects.UseEmbeddedNavigator = true;
            this.gridControlAssetObjects.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAssetObjects});
            // 
            // sp03069TreePickerAssetObjectsListBindingSource
            // 
            this.sp03069TreePickerAssetObjectsListBindingSource.DataMember = "sp03069_Tree_Picker_Asset_Objects_List";
            this.sp03069TreePickerAssetObjectsListBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "refresh_16x16");
            // 
            // gridViewAssetObjects
            // 
            this.gridViewAssetObjects.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAssetID,
            this.colSiteID,
            this.colSiteName,
            this.colSiteCode,
            this.colClientName,
            this.colClientCode,
            this.colAssetTypeID,
            this.colAssetTypeDescription,
            this.colAssetTypeOrder,
            this.colAssetSubTypeID,
            this.colAssetSubTypeDescription,
            this.colAssetSubTypeOrder,
            this.colXCoordinate2,
            this.colYCoordinate2,
            this.colPolygonXY1,
            this.colArea,
            this.colLength,
            this.colWidth,
            this.colStatusID,
            this.colStatusDescription,
            this.colPartNumber,
            this.colSerialNumber,
            this.colModelNumber,
            this.colAssetNumber,
            this.colLifeSpanValue,
            this.colLifeSpanValueDescriptor,
            this.colCurrentConditionID,
            this.colConditionDescription,
            this.colLastVisitDate,
            this.colNextVisitDate,
            this.colRemarks1,
            this.colMapID1,
            this.colObjectType1,
            this.colHighlight1});
            this.gridViewAssetObjects.GridControl = this.gridControlAssetObjects;
            this.gridViewAssetObjects.GroupCount = 3;
            this.gridViewAssetObjects.Name = "gridViewAssetObjects";
            this.gridViewAssetObjects.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewAssetObjects.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewAssetObjects.OptionsLayout.StoreAppearance = true;
            this.gridViewAssetObjects.OptionsMenu.ShowAddNewSummaryItem = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewAssetObjects.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewAssetObjects.OptionsSelection.MultiSelect = true;
            this.gridViewAssetObjects.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewAssetObjects.OptionsView.ColumnAutoWidth = false;
            this.gridViewAssetObjects.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewAssetObjects.OptionsView.ShowGroupPanel = false;
            this.gridViewAssetObjects.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewAssetObjects.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewAssetObjects.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewAssetObjects.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridViewAssetObjects_CustomDrawEmptyForeground);
            this.gridViewAssetObjects.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewAssetObjects.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewAssetObjects.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewAssetObjects_MouseUp);
            this.gridViewAssetObjects.GotFocus += new System.EventHandler(this.gridViewAssetObjects_GotFocus);
            // 
            // colAssetID
            // 
            this.colAssetID.Caption = "Asset ID";
            this.colAssetID.FieldName = "AssetID";
            this.colAssetID.Name = "colAssetID";
            this.colAssetID.OptionsColumn.AllowEdit = false;
            this.colAssetID.OptionsColumn.AllowFocus = false;
            this.colAssetID.OptionsColumn.ReadOnly = true;
            this.colAssetID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteName.Width = 161;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Width = 140;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colAssetTypeID
            // 
            this.colAssetTypeID.Caption = "Asset Type ID ";
            this.colAssetTypeID.FieldName = "AssetTypeID";
            this.colAssetTypeID.Name = "colAssetTypeID";
            this.colAssetTypeID.OptionsColumn.AllowEdit = false;
            this.colAssetTypeID.OptionsColumn.AllowFocus = false;
            this.colAssetTypeID.OptionsColumn.ReadOnly = true;
            this.colAssetTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAssetTypeID.Width = 108;
            // 
            // colAssetTypeDescription
            // 
            this.colAssetTypeDescription.Caption = "Asset Type";
            this.colAssetTypeDescription.FieldName = "AssetTypeDescription";
            this.colAssetTypeDescription.Name = "colAssetTypeDescription";
            this.colAssetTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetTypeDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAssetTypeDescription.Width = 113;
            // 
            // colAssetTypeOrder
            // 
            this.colAssetTypeOrder.Caption = "Asset Type Order";
            this.colAssetTypeOrder.FieldName = "AssetTypeOrder";
            this.colAssetTypeOrder.Name = "colAssetTypeOrder";
            this.colAssetTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetTypeOrder.OptionsColumn.ReadOnly = true;
            this.colAssetTypeOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAssetTypeOrder.Width = 133;
            // 
            // colAssetSubTypeID
            // 
            this.colAssetSubTypeID.Caption = "Asset Sub-Type ID";
            this.colAssetSubTypeID.FieldName = "AssetSubTypeID";
            this.colAssetSubTypeID.Name = "colAssetSubTypeID";
            this.colAssetSubTypeID.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeID.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeID.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAssetSubTypeID.Width = 108;
            // 
            // colAssetSubTypeDescription
            // 
            this.colAssetSubTypeDescription.Caption = "Asset Sub Type";
            this.colAssetSubTypeDescription.FieldName = "AssetSubTypeDescription";
            this.colAssetSubTypeDescription.Name = "colAssetSubTypeDescription";
            this.colAssetSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAssetSubTypeDescription.Visible = true;
            this.colAssetSubTypeDescription.VisibleIndex = 1;
            this.colAssetSubTypeDescription.Width = 124;
            // 
            // colAssetSubTypeOrder
            // 
            this.colAssetSubTypeOrder.Caption = "Asset Sub-Type Order";
            this.colAssetSubTypeOrder.FieldName = "AssetSubTypeOrder";
            this.colAssetSubTypeOrder.Name = "colAssetSubTypeOrder";
            this.colAssetSubTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeOrder.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAssetSubTypeOrder.Width = 136;
            // 
            // colXCoordinate2
            // 
            this.colXCoordinate2.Caption = "X Coordinate";
            this.colXCoordinate2.FieldName = "XCoordinate";
            this.colXCoordinate2.Name = "colXCoordinate2";
            this.colXCoordinate2.OptionsColumn.AllowEdit = false;
            this.colXCoordinate2.OptionsColumn.AllowFocus = false;
            this.colXCoordinate2.OptionsColumn.ReadOnly = true;
            this.colXCoordinate2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colYCoordinate2
            // 
            this.colYCoordinate2.Caption = "Y Coordinate";
            this.colYCoordinate2.FieldName = "YCoordinate";
            this.colYCoordinate2.Name = "colYCoordinate2";
            this.colYCoordinate2.OptionsColumn.AllowEdit = false;
            this.colYCoordinate2.OptionsColumn.AllowFocus = false;
            this.colYCoordinate2.OptionsColumn.ReadOnly = true;
            this.colYCoordinate2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPolygonXY1
            // 
            this.colPolygonXY1.Caption = "Poly Coordinates";
            this.colPolygonXY1.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colPolygonXY1.FieldName = "PolygonXY";
            this.colPolygonXY1.Name = "colPolygonXY1";
            this.colPolygonXY1.OptionsColumn.ReadOnly = true;
            this.colPolygonXY1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPolygonXY1.Width = 124;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area (M�)";
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            this.colArea.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colArea.Visible = true;
            this.colArea.VisibleIndex = 14;
            // 
            // colLength
            // 
            this.colLength.Caption = "Length";
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.OptionsColumn.AllowEdit = false;
            this.colLength.OptionsColumn.AllowFocus = false;
            this.colLength.OptionsColumn.ReadOnly = true;
            this.colLength.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLength.Visible = true;
            this.colLength.VisibleIndex = 15;
            // 
            // colWidth
            // 
            this.colWidth.Caption = "Width";
            this.colWidth.FieldName = "Width";
            this.colWidth.Name = "colWidth";
            this.colWidth.OptionsColumn.AllowEdit = false;
            this.colWidth.OptionsColumn.AllowFocus = false;
            this.colWidth.OptionsColumn.ReadOnly = true;
            this.colWidth.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWidth.Visible = true;
            this.colWidth.VisibleIndex = 16;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 2;
            // 
            // colPartNumber
            // 
            this.colPartNumber.Caption = "Part Number";
            this.colPartNumber.FieldName = "PartNumber";
            this.colPartNumber.Name = "colPartNumber";
            this.colPartNumber.OptionsColumn.AllowEdit = false;
            this.colPartNumber.OptionsColumn.AllowFocus = false;
            this.colPartNumber.OptionsColumn.ReadOnly = true;
            this.colPartNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPartNumber.Visible = true;
            this.colPartNumber.VisibleIndex = 4;
            this.colPartNumber.Width = 96;
            // 
            // colSerialNumber
            // 
            this.colSerialNumber.Caption = "Serial Number";
            this.colSerialNumber.FieldName = "SerialNumber";
            this.colSerialNumber.Name = "colSerialNumber";
            this.colSerialNumber.OptionsColumn.AllowEdit = false;
            this.colSerialNumber.OptionsColumn.AllowFocus = false;
            this.colSerialNumber.OptionsColumn.ReadOnly = true;
            this.colSerialNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSerialNumber.Visible = true;
            this.colSerialNumber.VisibleIndex = 5;
            this.colSerialNumber.Width = 91;
            // 
            // colModelNumber
            // 
            this.colModelNumber.Caption = "Model Number";
            this.colModelNumber.FieldName = "ModelNumber";
            this.colModelNumber.Name = "colModelNumber";
            this.colModelNumber.OptionsColumn.AllowEdit = false;
            this.colModelNumber.OptionsColumn.AllowFocus = false;
            this.colModelNumber.OptionsColumn.ReadOnly = true;
            this.colModelNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colModelNumber.Visible = true;
            this.colModelNumber.VisibleIndex = 6;
            this.colModelNumber.Width = 94;
            // 
            // colAssetNumber
            // 
            this.colAssetNumber.Caption = "Asset Number";
            this.colAssetNumber.FieldName = "AssetNumber";
            this.colAssetNumber.Name = "colAssetNumber";
            this.colAssetNumber.OptionsColumn.AllowEdit = false;
            this.colAssetNumber.OptionsColumn.AllowFocus = false;
            this.colAssetNumber.OptionsColumn.ReadOnly = true;
            this.colAssetNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAssetNumber.Visible = true;
            this.colAssetNumber.VisibleIndex = 3;
            this.colAssetNumber.Width = 112;
            // 
            // colLifeSpanValue
            // 
            this.colLifeSpanValue.Caption = "Life Span";
            this.colLifeSpanValue.FieldName = "LifeSpanValue";
            this.colLifeSpanValue.Name = "colLifeSpanValue";
            this.colLifeSpanValue.OptionsColumn.AllowEdit = false;
            this.colLifeSpanValue.OptionsColumn.AllowFocus = false;
            this.colLifeSpanValue.OptionsColumn.ReadOnly = true;
            this.colLifeSpanValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLifeSpanValue.Visible = true;
            this.colLifeSpanValue.VisibleIndex = 11;
            // 
            // colLifeSpanValueDescriptor
            // 
            this.colLifeSpanValueDescriptor.Caption = "Life Span Desc";
            this.colLifeSpanValueDescriptor.FieldName = "LifeSpanValueDescriptor";
            this.colLifeSpanValueDescriptor.Name = "colLifeSpanValueDescriptor";
            this.colLifeSpanValueDescriptor.OptionsColumn.AllowEdit = false;
            this.colLifeSpanValueDescriptor.OptionsColumn.AllowFocus = false;
            this.colLifeSpanValueDescriptor.OptionsColumn.ReadOnly = true;
            this.colLifeSpanValueDescriptor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLifeSpanValueDescriptor.Visible = true;
            this.colLifeSpanValueDescriptor.VisibleIndex = 10;
            this.colLifeSpanValueDescriptor.Width = 94;
            // 
            // colCurrentConditionID
            // 
            this.colCurrentConditionID.Caption = "Condition ID";
            this.colCurrentConditionID.FieldName = "CurrentConditionID";
            this.colCurrentConditionID.Name = "colCurrentConditionID";
            this.colCurrentConditionID.OptionsColumn.AllowEdit = false;
            this.colCurrentConditionID.OptionsColumn.AllowFocus = false;
            this.colCurrentConditionID.OptionsColumn.ReadOnly = true;
            this.colCurrentConditionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentConditionID.Width = 86;
            // 
            // colConditionDescription
            // 
            this.colConditionDescription.Caption = "Condition";
            this.colConditionDescription.FieldName = "ConditionDescription";
            this.colConditionDescription.Name = "colConditionDescription";
            this.colConditionDescription.OptionsColumn.AllowEdit = false;
            this.colConditionDescription.OptionsColumn.AllowFocus = false;
            this.colConditionDescription.OptionsColumn.ReadOnly = true;
            this.colConditionDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colConditionDescription.Visible = true;
            this.colConditionDescription.VisibleIndex = 7;
            this.colConditionDescription.Width = 110;
            // 
            // colLastVisitDate
            // 
            this.colLastVisitDate.Caption = "Last Visit";
            this.colLastVisitDate.FieldName = "LastVisitDate";
            this.colLastVisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastVisitDate.Name = "colLastVisitDate";
            this.colLastVisitDate.OptionsColumn.AllowEdit = false;
            this.colLastVisitDate.OptionsColumn.AllowFocus = false;
            this.colLastVisitDate.OptionsColumn.ReadOnly = true;
            this.colLastVisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastVisitDate.Visible = true;
            this.colLastVisitDate.VisibleIndex = 9;
            // 
            // colNextVisitDate
            // 
            this.colNextVisitDate.Caption = "Next Visit";
            this.colNextVisitDate.FieldName = "NextVisitDate";
            this.colNextVisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextVisitDate.Name = "colNextVisitDate";
            this.colNextVisitDate.OptionsColumn.AllowEdit = false;
            this.colNextVisitDate.OptionsColumn.AllowFocus = false;
            this.colNextVisitDate.OptionsColumn.ReadOnly = true;
            this.colNextVisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextVisitDate.Visible = true;
            this.colNextVisitDate.VisibleIndex = 8;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 12;
            // 
            // colMapID1
            // 
            this.colMapID1.Caption = "Map ID";
            this.colMapID1.FieldName = "MapID";
            this.colMapID1.Name = "colMapID1";
            this.colMapID1.OptionsColumn.AllowEdit = false;
            this.colMapID1.OptionsColumn.AllowFocus = false;
            this.colMapID1.OptionsColumn.ReadOnly = true;
            this.colMapID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMapID1.Visible = true;
            this.colMapID1.VisibleIndex = 17;
            // 
            // colObjectType1
            // 
            this.colObjectType1.Caption = "Object Type";
            this.colObjectType1.FieldName = "ObjectType";
            this.colObjectType1.Name = "colObjectType1";
            this.colObjectType1.OptionsColumn.AllowEdit = false;
            this.colObjectType1.OptionsColumn.AllowFocus = false;
            this.colObjectType1.OptionsColumn.ReadOnly = true;
            this.colObjectType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colObjectType1.Visible = true;
            this.colObjectType1.VisibleIndex = 13;
            this.colObjectType1.Width = 87;
            // 
            // colHighlight1
            // 
            this.colHighlight1.Caption = "Highlight";
            this.colHighlight1.ColumnEdit = this.repositoryItemCheckEditHighlight2;
            this.colHighlight1.FieldName = "Highlight";
            this.colHighlight1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colHighlight1.Name = "colHighlight1";
            this.colHighlight1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHighlight1.Visible = true;
            this.colHighlight1.VisibleIndex = 0;
            // 
            // repositoryItemCheckEditHighlight2
            // 
            this.repositoryItemCheckEditHighlight2.AutoHeight = false;
            this.repositoryItemCheckEditHighlight2.Caption = "Check";
            this.repositoryItemCheckEditHighlight2.Name = "repositoryItemCheckEditHighlight2";
            this.repositoryItemCheckEditHighlight2.ValueChecked = 1;
            this.repositoryItemCheckEditHighlight2.ValueUnchecked = 0;
            this.repositoryItemCheckEditHighlight2.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEditHighlight2_CheckedChanged);
            // 
            // btnClearMapObjects
            // 
            this.btnClearMapObjects.Location = new System.Drawing.Point(135, 501);
            this.btnClearMapObjects.Name = "btnClearMapObjects";
            this.btnClearMapObjects.Size = new System.Drawing.Size(99, 22);
            this.btnClearMapObjects.StyleController = this.layoutControl1;
            this.btnClearMapObjects.TabIndex = 11;
            this.btnClearMapObjects.Text = "Clear Map Objects";
            this.btnClearMapObjects.Click += new System.EventHandler(this.btnClearMapObjects_Click);
            // 
            // popupContainerEditTreeRefStructure
            // 
            this.popupContainerEditTreeRefStructure.EditValue = "Default  [unchanged]";
            this.popupContainerEditTreeRefStructure.Location = new System.Drawing.Point(89, 54);
            this.popupContainerEditTreeRefStructure.MenuManager = this.barManager1;
            this.popupContainerEditTreeRefStructure.Name = "popupContainerEditTreeRefStructure";
            this.popupContainerEditTreeRefStructure.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditTreeRefStructure.Properties.PopupControl = this.popupContainerControlTreeRefStructure;
            this.popupContainerEditTreeRefStructure.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditTreeRefStructure.Size = new System.Drawing.Size(235, 20);
            this.popupContainerEditTreeRefStructure.StyleController = this.layoutControl1;
            this.popupContainerEditTreeRefStructure.TabIndex = 10;
            this.popupContainerEditTreeRefStructure.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditTreeRefStructure_QueryResultValue);
            // 
            // popupContainerControlTreeRefStructure
            // 
            this.popupContainerControlTreeRefStructure.Controls.Add(this.btnSetTreeRefStruc);
            this.popupContainerControlTreeRefStructure.Controls.Add(this.groupControl1);
            this.popupContainerControlTreeRefStructure.Location = new System.Drawing.Point(524, 13);
            this.popupContainerControlTreeRefStructure.Name = "popupContainerControlTreeRefStructure";
            this.popupContainerControlTreeRefStructure.Size = new System.Drawing.Size(332, 126);
            this.popupContainerControlTreeRefStructure.TabIndex = 25;
            // 
            // btnSetTreeRefStruc
            // 
            this.btnSetTreeRefStruc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSetTreeRefStruc.Location = new System.Drawing.Point(3, 100);
            this.btnSetTreeRefStruc.Name = "btnSetTreeRefStruc";
            this.btnSetTreeRefStruc.Size = new System.Drawing.Size(75, 23);
            this.btnSetTreeRefStruc.TabIndex = 5;
            this.btnSetTreeRefStruc.Text = "OK";
            this.btnSetTreeRefStruc.Click += new System.EventHandler(this.btnSetTreeRefStruc_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.textEditTreeRefRemovePreceedingChars);
            this.groupControl1.Controls.Add(this.spinEditTreeRefRemoveNumberOfChars);
            this.groupControl1.Controls.Add(this.checkEditTreeRef1);
            this.groupControl1.Controls.Add(this.checkEditTreeRef3);
            this.groupControl1.Controls.Add(this.checkEditTreeRef2);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(326, 95);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Tree Reference Structure when Displayed on Labels";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(157, 49);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(61, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Character(s)";
            // 
            // textEditTreeRefRemovePreceedingChars
            // 
            this.textEditTreeRefRemovePreceedingChars.EditValue = "|";
            this.textEditTreeRefRemovePreceedingChars.Location = new System.Drawing.Point(241, 68);
            this.textEditTreeRefRemovePreceedingChars.MenuManager = this.barManager1;
            this.textEditTreeRefRemovePreceedingChars.Name = "textEditTreeRefRemovePreceedingChars";
            this.textEditTreeRefRemovePreceedingChars.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTreeRefRemovePreceedingChars, true);
            this.textEditTreeRefRemovePreceedingChars.Size = new System.Drawing.Size(78, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTreeRefRemovePreceedingChars, optionsSpelling1);
            this.textEditTreeRefRemovePreceedingChars.TabIndex = 5;
            // 
            // spinEditTreeRefRemoveNumberOfChars
            // 
            this.spinEditTreeRefRemoveNumberOfChars.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditTreeRefRemoveNumberOfChars.Location = new System.Drawing.Point(97, 45);
            this.spinEditTreeRefRemoveNumberOfChars.MenuManager = this.barManager1;
            this.spinEditTreeRefRemoveNumberOfChars.Name = "spinEditTreeRefRemoveNumberOfChars";
            this.spinEditTreeRefRemoveNumberOfChars.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditTreeRefRemoveNumberOfChars.Properties.IsFloatValue = false;
            this.spinEditTreeRefRemoveNumberOfChars.Properties.Mask.EditMask = "N00";
            this.spinEditTreeRefRemoveNumberOfChars.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.spinEditTreeRefRemoveNumberOfChars.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditTreeRefRemoveNumberOfChars.Size = new System.Drawing.Size(53, 20);
            this.spinEditTreeRefRemoveNumberOfChars.TabIndex = 4;
            // 
            // checkEditTreeRef1
            // 
            this.checkEditTreeRef1.EditValue = true;
            this.checkEditTreeRef1.Location = new System.Drawing.Point(6, 25);
            this.checkEditTreeRef1.MenuManager = this.barManager1;
            this.checkEditTreeRef1.Name = "checkEditTreeRef1";
            this.checkEditTreeRef1.Properties.Caption = "Default  [unchanged]";
            this.checkEditTreeRef1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTreeRef1.Properties.RadioGroupIndex = 1;
            this.checkEditTreeRef1.Size = new System.Drawing.Size(121, 19);
            this.checkEditTreeRef1.TabIndex = 1;
            this.checkEditTreeRef1.CheckedChanged += new System.EventHandler(this.checkEditTreeRef1_CheckedChanged);
            // 
            // checkEditTreeRef3
            // 
            this.checkEditTreeRef3.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.False;
            this.checkEditTreeRef3.Location = new System.Drawing.Point(6, 69);
            this.checkEditTreeRef3.MenuManager = this.barManager1;
            this.checkEditTreeRef3.Name = "checkEditTreeRef3";
            this.checkEditTreeRef3.Properties.Caption = "Remove All Characters Up To and Including:";
            this.checkEditTreeRef3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTreeRef3.Properties.RadioGroupIndex = 1;
            this.checkEditTreeRef3.Size = new System.Drawing.Size(233, 19);
            this.checkEditTreeRef3.TabIndex = 3;
            this.checkEditTreeRef3.TabStop = false;
            this.checkEditTreeRef3.CheckedChanged += new System.EventHandler(this.checkEditTreeRef3_CheckedChanged);
            // 
            // checkEditTreeRef2
            // 
            this.checkEditTreeRef2.Location = new System.Drawing.Point(6, 47);
            this.checkEditTreeRef2.MenuManager = this.barManager1;
            this.checkEditTreeRef2.Name = "checkEditTreeRef2";
            this.checkEditTreeRef2.Properties.Caption = "Remove First:";
            this.checkEditTreeRef2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTreeRef2.Properties.RadioGroupIndex = 1;
            this.checkEditTreeRef2.Size = new System.Drawing.Size(89, 19);
            this.checkEditTreeRef2.TabIndex = 2;
            this.checkEditTreeRef2.TabStop = false;
            this.checkEditTreeRef2.CheckedChanged += new System.EventHandler(this.checkEditTreeRef2_CheckedChanged);
            // 
            // btnSelectHighlighted
            // 
            this.btnSelectHighlighted.Location = new System.Drawing.Point(228, 581);
            this.btnSelectHighlighted.Name = "btnSelectHighlighted";
            this.btnSelectHighlighted.Size = new System.Drawing.Size(96, 22);
            this.btnSelectHighlighted.StyleController = this.layoutControl1;
            toolTipTitleItem35.Text = "Select Highlighted - Information";
            toolTipItem35.LeftIndent = 6;
            toolTipItem35.Text = "Click me to select all highlighted map objects (for Editing \\ Deleting \\ Linking " +
    "to Work Orders).";
            superToolTip35.Items.Add(toolTipTitleItem35);
            superToolTip35.Items.Add(toolTipItem35);
            this.btnSelectHighlighted.SuperTip = superToolTip35;
            this.btnSelectHighlighted.TabIndex = 9;
            this.btnSelectHighlighted.Text = "Select Highlighted";
            this.btnSelectHighlighted.Click += new System.EventHandler(this.btnSelectHighlighted_Click);
            // 
            // btnRefreshMapObjects
            // 
            this.btnRefreshMapObjects.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefreshMapObjects.Location = new System.Drawing.Point(2, 501);
            this.btnRefreshMapObjects.Name = "btnRefreshMapObjects";
            this.btnRefreshMapObjects.Size = new System.Drawing.Size(129, 22);
            this.btnRefreshMapObjects.StyleController = this.layoutControl1;
            this.btnRefreshMapObjects.TabIndex = 7;
            this.btnRefreshMapObjects.Text = "Refresh Map Objects";
            this.btnRefreshMapObjects.Click += new System.EventHandler(this.btnRefreshMapObjects_Click);
            // 
            // csScaleMapForHighlighted
            // 
            this.csScaleMapForHighlighted.Location = new System.Drawing.Point(10, 604);
            this.csScaleMapForHighlighted.MenuManager = this.barManager1;
            this.csScaleMapForHighlighted.Name = "csScaleMapForHighlighted";
            this.csScaleMapForHighlighted.Properties.Caption = "Scale To Show All Highlighted";
            this.csScaleMapForHighlighted.Size = new System.Drawing.Size(214, 19);
            this.csScaleMapForHighlighted.StyleController = this.layoutControl1;
            this.csScaleMapForHighlighted.TabIndex = 5;
            this.csScaleMapForHighlighted.CheckedChanged += new System.EventHandler(this.csScaleMapForHighlighted_CheckedChanged);
            // 
            // ceCentreMap
            // 
            this.ceCentreMap.EditValue = true;
            this.ceCentreMap.Location = new System.Drawing.Point(10, 581);
            this.ceCentreMap.MenuManager = this.barManager1;
            this.ceCentreMap.Name = "ceCentreMap";
            this.ceCentreMap.Properties.Caption = "Centre on Last Highlighted";
            this.ceCentreMap.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.ceCentreMap.Size = new System.Drawing.Size(214, 19);
            this.ceCentreMap.StyleController = this.layoutControl1;
            this.ceCentreMap.TabIndex = 4;
            this.ceCentreMap.CheckedChanged += new System.EventHandler(this.ceCentreMap_CheckedChanged);
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(10, 126);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(314, 363);
            this.gridSplitContainer2.TabIndex = 17;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp01252ATTreePickerloadobjectmanagerBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Refresh data.", "refresh")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditHighlight,
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(314, 363);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp01252ATTreePickerloadobjectmanagerBindingSource
            // 
            this.sp01252ATTreePickerloadobjectmanagerBindingSource.DataMember = "sp01252_AT_Tree_Picker_load_object_manager";
            this.sp01252ATTreePickerloadobjectmanagerBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeID,
            this.colSiteID2,
            this.colClientID1,
            this.colSiteName2,
            this.colClientName2,
            this.colTreeReference,
            this.colMappingID,
            this.colGridReference,
            this.colDistance,
            this.colHouseName,
            this.colAccess,
            this.colVisibility,
            this.colContext,
            this.colManagement,
            this.colLegalStatus,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnit,
            this.colNextInspectionDate,
            this.colGroundType,
            this.colDBH,
            this.colDBHRange,
            this.colHeight,
            this.colHeightRange,
            this.colProtectionType,
            this.colCrownDiameter,
            this.colPlantDate,
            this.colGroupNumber,
            this.colRiskCategory,
            this.colSiteHazardClass,
            this.colPlantSize,
            this.colPlantSource,
            this.colPlantMethod,
            this.colPostcode,
            this.colMapLabel,
            this.colStatus,
            this.colSize,
            this.colTarget1,
            this.colTarget2,
            this.colTarget3,
            this.colLastModifiedDate,
            this.gridColumn1,
            this.colUser1,
            this.colUser2,
            this.colUser3,
            this.colAgeClass,
            this.colAreaHa,
            this.colReplantCount,
            this.colSurveyDate,
            this.colTreeType,
            this.colHedgeOwner,
            this.colHedgeLength,
            this.colGardenSize,
            this.colPropertyProximity,
            this.colSiteLevel,
            this.colSituation,
            this.colRiskFactor,
            this.colPolygonXY,
            this.colcolor,
            this.colOwnershipName,
            this.colLinkedInspectionCount,
            this.Calculated1,
            this.Calculated2,
            this.Calculated3,
            this.colXCoordinate,
            this.colYCoordinate,
            this.colSpecies,
            this.colObjectType,
            this.colMapID,
            this.colHighlight,
            this.colintAccess,
            this.colintAgeClass,
            this.colintDBHRange,
            this.colintDistrictID,
            this.colintGroundType,
            this.colintHeightRange,
            this.colintLegalStatus,
            this.colintLocalityID,
            this.colintNearbyObject1,
            this.colintNearbyObject2,
            this.colintNearbyObject3,
            this.colintOwnershipID,
            this.colintProtectionType,
            this.colintSafetyPriority,
            this.colintSiteHazardClass,
            this.colintSize,
            this.colintSpeciesID,
            this.colintStatus,
            this.colintVisibility,
            this.colLastInspectionElapsedDays});
            this.gridView2.CustomizationFormBounds = new System.Drawing.Rectangle(1158, 513, 208, 191);
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView2_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            this.colTreeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            this.colClientID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 2;
            this.colTreeReference.Width = 112;
            // 
            // colMappingID
            // 
            this.colMappingID.Caption = "Map ID";
            this.colMappingID.FieldName = "MappingID";
            this.colMappingID.Name = "colMappingID";
            this.colMappingID.OptionsColumn.AllowEdit = false;
            this.colMappingID.OptionsColumn.AllowFocus = false;
            this.colMappingID.OptionsColumn.ReadOnly = true;
            this.colMappingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colGridReference
            // 
            this.colGridReference.Caption = "Grid Reference";
            this.colGridReference.FieldName = "GridReference";
            this.colGridReference.Name = "colGridReference";
            this.colGridReference.OptionsColumn.AllowEdit = false;
            this.colGridReference.OptionsColumn.AllowFocus = false;
            this.colGridReference.OptionsColumn.ReadOnly = true;
            this.colGridReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGridReference.Visible = true;
            this.colGridReference.VisibleIndex = 10;
            this.colGridReference.Width = 113;
            // 
            // colDistance
            // 
            this.colDistance.Caption = "Distance";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colHouseName
            // 
            this.colHouseName.Caption = "House Name";
            this.colHouseName.FieldName = "HouseName";
            this.colHouseName.Name = "colHouseName";
            this.colHouseName.OptionsColumn.AllowEdit = false;
            this.colHouseName.OptionsColumn.AllowFocus = false;
            this.colHouseName.OptionsColumn.ReadOnly = true;
            this.colHouseName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHouseName.Visible = true;
            this.colHouseName.VisibleIndex = 8;
            this.colHouseName.Width = 99;
            // 
            // colAccess
            // 
            this.colAccess.Caption = "Access";
            this.colAccess.FieldName = "Access";
            this.colAccess.Name = "colAccess";
            this.colAccess.OptionsColumn.AllowEdit = false;
            this.colAccess.OptionsColumn.AllowFocus = false;
            this.colAccess.OptionsColumn.ReadOnly = true;
            this.colAccess.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colVisibility
            // 
            this.colVisibility.Caption = "Visibility";
            this.colVisibility.FieldName = "Visibility";
            this.colVisibility.Name = "colVisibility";
            this.colVisibility.OptionsColumn.AllowEdit = false;
            this.colVisibility.OptionsColumn.AllowFocus = false;
            this.colVisibility.OptionsColumn.ReadOnly = true;
            this.colVisibility.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colContext
            // 
            this.colContext.Caption = "Context";
            this.colContext.FieldName = "Context";
            this.colContext.Name = "colContext";
            this.colContext.OptionsColumn.AllowEdit = false;
            this.colContext.OptionsColumn.AllowFocus = false;
            this.colContext.OptionsColumn.ReadOnly = true;
            this.colContext.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colManagement
            // 
            this.colManagement.Caption = "Management";
            this.colManagement.FieldName = "Management";
            this.colManagement.Name = "colManagement";
            this.colManagement.OptionsColumn.AllowEdit = false;
            this.colManagement.OptionsColumn.AllowFocus = false;
            this.colManagement.OptionsColumn.ReadOnly = true;
            this.colManagement.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colManagement.Width = 88;
            // 
            // colLegalStatus
            // 
            this.colLegalStatus.Caption = "Legal Status";
            this.colLegalStatus.FieldName = "LegalStatus";
            this.colLegalStatus.Name = "colLegalStatus";
            this.colLegalStatus.OptionsColumn.AllowEdit = false;
            this.colLegalStatus.OptionsColumn.AllowFocus = false;
            this.colLegalStatus.OptionsColumn.ReadOnly = true;
            this.colLegalStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalStatus.Visible = true;
            this.colLegalStatus.VisibleIndex = 28;
            this.colLegalStatus.Width = 92;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection";
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 15;
            this.colLastInspectionDate.Width = 98;
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Inspection Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionCycle.Width = 99;
            // 
            // colInspectionUnit
            // 
            this.colInspectionUnit.Caption = "Inspection Unit";
            this.colInspectionUnit.FieldName = "InspectionUnit";
            this.colInspectionUnit.Name = "colInspectionUnit";
            this.colInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colInspectionUnit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionUnit.Width = 96;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection";
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 16;
            this.colNextInspectionDate.Width = 99;
            // 
            // colGroundType
            // 
            this.colGroundType.Caption = "Ground Type";
            this.colGroundType.FieldName = "GroundType";
            this.colGroundType.Name = "colGroundType";
            this.colGroundType.OptionsColumn.AllowEdit = false;
            this.colGroundType.OptionsColumn.AllowFocus = false;
            this.colGroundType.OptionsColumn.ReadOnly = true;
            this.colGroundType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGroundType.Visible = true;
            this.colGroundType.VisibleIndex = 26;
            this.colGroundType.Width = 91;
            // 
            // colDBH
            // 
            this.colDBH.Caption = "DBH";
            this.colDBH.FieldName = "DBH";
            this.colDBH.Name = "colDBH";
            this.colDBH.OptionsColumn.AllowEdit = false;
            this.colDBH.OptionsColumn.AllowFocus = false;
            this.colDBH.OptionsColumn.ReadOnly = true;
            this.colDBH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDBH.Visible = true;
            this.colDBH.VisibleIndex = 19;
            // 
            // colDBHRange
            // 
            this.colDBHRange.Caption = "DBH Range";
            this.colDBHRange.FieldName = "DBHRange";
            this.colDBHRange.Name = "colDBHRange";
            this.colDBHRange.OptionsColumn.AllowEdit = false;
            this.colDBHRange.OptionsColumn.AllowFocus = false;
            this.colDBHRange.OptionsColumn.ReadOnly = true;
            this.colDBHRange.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDBHRange.Visible = true;
            this.colDBHRange.VisibleIndex = 20;
            // 
            // colHeight
            // 
            this.colHeight.Caption = "Height";
            this.colHeight.FieldName = "Height";
            this.colHeight.Name = "colHeight";
            this.colHeight.OptionsColumn.AllowEdit = false;
            this.colHeight.OptionsColumn.AllowFocus = false;
            this.colHeight.OptionsColumn.ReadOnly = true;
            this.colHeight.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeight.Visible = true;
            this.colHeight.VisibleIndex = 21;
            // 
            // colHeightRange
            // 
            this.colHeightRange.Caption = "Height Range";
            this.colHeightRange.FieldName = "HeightRange";
            this.colHeightRange.Name = "colHeightRange";
            this.colHeightRange.OptionsColumn.AllowEdit = false;
            this.colHeightRange.OptionsColumn.AllowFocus = false;
            this.colHeightRange.OptionsColumn.ReadOnly = true;
            this.colHeightRange.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeightRange.Visible = true;
            this.colHeightRange.VisibleIndex = 22;
            this.colHeightRange.Width = 91;
            // 
            // colProtectionType
            // 
            this.colProtectionType.Caption = "Protection Type";
            this.colProtectionType.FieldName = "ProtectionType";
            this.colProtectionType.Name = "colProtectionType";
            this.colProtectionType.OptionsColumn.AllowEdit = false;
            this.colProtectionType.OptionsColumn.AllowFocus = false;
            this.colProtectionType.OptionsColumn.ReadOnly = true;
            this.colProtectionType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProtectionType.Width = 105;
            // 
            // colCrownDiameter
            // 
            this.colCrownDiameter.Caption = "Crown Diameter";
            this.colCrownDiameter.FieldName = "CrownDiameter";
            this.colCrownDiameter.Name = "colCrownDiameter";
            this.colCrownDiameter.OptionsColumn.AllowEdit = false;
            this.colCrownDiameter.OptionsColumn.AllowFocus = false;
            this.colCrownDiameter.OptionsColumn.ReadOnly = true;
            this.colCrownDiameter.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCrownDiameter.Visible = true;
            this.colCrownDiameter.VisibleIndex = 24;
            this.colCrownDiameter.Width = 111;
            // 
            // colPlantDate
            // 
            this.colPlantDate.Caption = "Plant Date";
            this.colPlantDate.FieldName = "PlantDate";
            this.colPlantDate.Name = "colPlantDate";
            this.colPlantDate.OptionsColumn.AllowEdit = false;
            this.colPlantDate.OptionsColumn.AllowFocus = false;
            this.colPlantDate.OptionsColumn.ReadOnly = true;
            this.colPlantDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colGroupNumber
            // 
            this.colGroupNumber.Caption = "Group Number";
            this.colGroupNumber.FieldName = "GroupNumber";
            this.colGroupNumber.Name = "colGroupNumber";
            this.colGroupNumber.OptionsColumn.AllowEdit = false;
            this.colGroupNumber.OptionsColumn.AllowFocus = false;
            this.colGroupNumber.OptionsColumn.ReadOnly = true;
            this.colGroupNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGroupNumber.Visible = true;
            this.colGroupNumber.VisibleIndex = 18;
            this.colGroupNumber.Width = 96;
            // 
            // colRiskCategory
            // 
            this.colRiskCategory.Caption = "Risk Category";
            this.colRiskCategory.FieldName = "RiskCategory";
            this.colRiskCategory.Name = "colRiskCategory";
            this.colRiskCategory.OptionsColumn.AllowEdit = false;
            this.colRiskCategory.OptionsColumn.AllowFocus = false;
            this.colRiskCategory.OptionsColumn.ReadOnly = true;
            this.colRiskCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRiskCategory.Visible = true;
            this.colRiskCategory.VisibleIndex = 7;
            this.colRiskCategory.Width = 95;
            // 
            // colSiteHazardClass
            // 
            this.colSiteHazardClass.Caption = "Site Hazard Class";
            this.colSiteHazardClass.FieldName = "SiteHazardClass";
            this.colSiteHazardClass.Name = "colSiteHazardClass";
            this.colSiteHazardClass.OptionsColumn.AllowEdit = false;
            this.colSiteHazardClass.OptionsColumn.AllowFocus = false;
            this.colSiteHazardClass.OptionsColumn.ReadOnly = true;
            this.colSiteHazardClass.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteHazardClass.Width = 115;
            // 
            // colPlantSize
            // 
            this.colPlantSize.Caption = "Plant Size";
            this.colPlantSize.FieldName = "PlantSize";
            this.colPlantSize.Name = "colPlantSize";
            this.colPlantSize.OptionsColumn.AllowEdit = false;
            this.colPlantSize.OptionsColumn.AllowFocus = false;
            this.colPlantSize.OptionsColumn.ReadOnly = true;
            this.colPlantSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPlantSource
            // 
            this.colPlantSource.Caption = "Plant Source";
            this.colPlantSource.FieldName = "PlantSource";
            this.colPlantSource.Name = "colPlantSource";
            this.colPlantSource.OptionsColumn.AllowEdit = false;
            this.colPlantSource.OptionsColumn.AllowFocus = false;
            this.colPlantSource.OptionsColumn.ReadOnly = true;
            this.colPlantSource.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlantSource.Width = 107;
            // 
            // colPlantMethod
            // 
            this.colPlantMethod.Caption = "Plant Method";
            this.colPlantMethod.FieldName = "PlantMethod";
            this.colPlantMethod.Name = "colPlantMethod";
            this.colPlantMethod.OptionsColumn.AllowEdit = false;
            this.colPlantMethod.OptionsColumn.AllowFocus = false;
            this.colPlantMethod.OptionsColumn.ReadOnly = true;
            this.colPlantMethod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlantMethod.Width = 110;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 9;
            // 
            // colMapLabel
            // 
            this.colMapLabel.Caption = "Map Label";
            this.colMapLabel.FieldName = "MapLabel";
            this.colMapLabel.Name = "colMapLabel";
            this.colMapLabel.OptionsColumn.AllowEdit = false;
            this.colMapLabel.OptionsColumn.AllowFocus = false;
            this.colMapLabel.OptionsColumn.ReadOnly = true;
            this.colMapLabel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 1;
            // 
            // colSize
            // 
            this.colSize.Caption = "Size";
            this.colSize.FieldName = "Size";
            this.colSize.Name = "colSize";
            this.colSize.OptionsColumn.AllowEdit = false;
            this.colSize.OptionsColumn.AllowFocus = false;
            this.colSize.OptionsColumn.ReadOnly = true;
            this.colSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 23;
            // 
            // colTarget1
            // 
            this.colTarget1.Caption = "Target 1";
            this.colTarget1.FieldName = "Target1";
            this.colTarget1.Name = "colTarget1";
            this.colTarget1.OptionsColumn.AllowEdit = false;
            this.colTarget1.OptionsColumn.AllowFocus = false;
            this.colTarget1.OptionsColumn.ReadOnly = true;
            this.colTarget1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTarget1.Visible = true;
            this.colTarget1.VisibleIndex = 12;
            // 
            // colTarget2
            // 
            this.colTarget2.Caption = "Target 2";
            this.colTarget2.FieldName = "Target2";
            this.colTarget2.Name = "colTarget2";
            this.colTarget2.OptionsColumn.AllowEdit = false;
            this.colTarget2.OptionsColumn.AllowFocus = false;
            this.colTarget2.OptionsColumn.ReadOnly = true;
            this.colTarget2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTarget2.Visible = true;
            this.colTarget2.VisibleIndex = 13;
            // 
            // colTarget3
            // 
            this.colTarget3.Caption = "Target 3";
            this.colTarget3.FieldName = "Target3";
            this.colTarget3.Name = "colTarget3";
            this.colTarget3.OptionsColumn.AllowEdit = false;
            this.colTarget3.OptionsColumn.AllowFocus = false;
            this.colTarget3.OptionsColumn.ReadOnly = true;
            this.colTarget3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTarget3.Visible = true;
            this.colTarget3.VisibleIndex = 14;
            // 
            // colLastModifiedDate
            // 
            this.colLastModifiedDate.Caption = "Last Modified";
            this.colLastModifiedDate.FieldName = "LastModifiedDate";
            this.colLastModifiedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastModifiedDate.Name = "colLastModifiedDate";
            this.colLastModifiedDate.OptionsColumn.AllowEdit = false;
            this.colLastModifiedDate.OptionsColumn.AllowFocus = false;
            this.colLastModifiedDate.OptionsColumn.ReadOnly = true;
            this.colLastModifiedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastModifiedDate.Width = 112;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Remarks";
            this.gridColumn1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn1.FieldName = "Remarks";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 31;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colUser1
            // 
            this.colUser1.Caption = "User 1";
            this.colUser1.FieldName = "User1";
            this.colUser1.Name = "colUser1";
            this.colUser1.OptionsColumn.AllowEdit = false;
            this.colUser1.OptionsColumn.AllowFocus = false;
            this.colUser1.OptionsColumn.ReadOnly = true;
            this.colUser1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colUser2
            // 
            this.colUser2.Caption = "User 2";
            this.colUser2.FieldName = "User2";
            this.colUser2.Name = "colUser2";
            this.colUser2.OptionsColumn.AllowEdit = false;
            this.colUser2.OptionsColumn.AllowFocus = false;
            this.colUser2.OptionsColumn.ReadOnly = true;
            this.colUser2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colUser3
            // 
            this.colUser3.Caption = "User 3";
            this.colUser3.FieldName = "User3";
            this.colUser3.Name = "colUser3";
            this.colUser3.OptionsColumn.AllowEdit = false;
            this.colUser3.OptionsColumn.AllowFocus = false;
            this.colUser3.OptionsColumn.ReadOnly = true;
            this.colUser3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colAgeClass
            // 
            this.colAgeClass.Caption = "Age Class";
            this.colAgeClass.FieldName = "AgeClass";
            this.colAgeClass.Name = "colAgeClass";
            this.colAgeClass.OptionsColumn.AllowEdit = false;
            this.colAgeClass.OptionsColumn.AllowFocus = false;
            this.colAgeClass.OptionsColumn.ReadOnly = true;
            this.colAgeClass.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAgeClass.Visible = true;
            this.colAgeClass.VisibleIndex = 25;
            // 
            // colAreaHa
            // 
            this.colAreaHa.Caption = "Area [M�]";
            this.colAreaHa.FieldName = "AreaHa";
            this.colAreaHa.Name = "colAreaHa";
            this.colAreaHa.OptionsColumn.AllowEdit = false;
            this.colAreaHa.OptionsColumn.AllowFocus = false;
            this.colAreaHa.OptionsColumn.ReadOnly = true;
            this.colAreaHa.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colReplantCount
            // 
            this.colReplantCount.Caption = "Replant Count";
            this.colReplantCount.FieldName = "ReplantCount";
            this.colReplantCount.Name = "colReplantCount";
            this.colReplantCount.OptionsColumn.AllowEdit = false;
            this.colReplantCount.OptionsColumn.AllowFocus = false;
            this.colReplantCount.OptionsColumn.ReadOnly = true;
            this.colReplantCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReplantCount.Width = 106;
            // 
            // colSurveyDate
            // 
            this.colSurveyDate.Caption = "Survey Date";
            this.colSurveyDate.FieldName = "SurveyDate";
            this.colSurveyDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyDate.Name = "colSurveyDate";
            this.colSurveyDate.OptionsColumn.AllowEdit = false;
            this.colSurveyDate.OptionsColumn.AllowFocus = false;
            this.colSurveyDate.OptionsColumn.ReadOnly = true;
            this.colSurveyDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyDate.Width = 115;
            // 
            // colTreeType
            // 
            this.colTreeType.Caption = "Tree Type";
            this.colTreeType.FieldName = "TreeType";
            this.colTreeType.Name = "colTreeType";
            this.colTreeType.OptionsColumn.AllowEdit = false;
            this.colTreeType.OptionsColumn.AllowFocus = false;
            this.colTreeType.OptionsColumn.ReadOnly = true;
            this.colTreeType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeType.Visible = true;
            this.colTreeType.VisibleIndex = 27;
            // 
            // colHedgeOwner
            // 
            this.colHedgeOwner.Caption = "Hedge Owner";
            this.colHedgeOwner.FieldName = "HedgeOwner";
            this.colHedgeOwner.Name = "colHedgeOwner";
            this.colHedgeOwner.OptionsColumn.AllowEdit = false;
            this.colHedgeOwner.OptionsColumn.AllowFocus = false;
            this.colHedgeOwner.OptionsColumn.ReadOnly = true;
            this.colHedgeOwner.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHedgeOwner.Width = 115;
            // 
            // colHedgeLength
            // 
            this.colHedgeLength.Caption = "Hedge Length";
            this.colHedgeLength.FieldName = "HedgeLength";
            this.colHedgeLength.Name = "colHedgeLength";
            this.colHedgeLength.OptionsColumn.AllowEdit = false;
            this.colHedgeLength.OptionsColumn.AllowFocus = false;
            this.colHedgeLength.OptionsColumn.ReadOnly = true;
            this.colHedgeLength.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHedgeLength.Width = 126;
            // 
            // colGardenSize
            // 
            this.colGardenSize.Caption = "Garden Size";
            this.colGardenSize.FieldName = "GardenSize";
            this.colGardenSize.Name = "colGardenSize";
            this.colGardenSize.OptionsColumn.AllowEdit = false;
            this.colGardenSize.OptionsColumn.AllowFocus = false;
            this.colGardenSize.OptionsColumn.ReadOnly = true;
            this.colGardenSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGardenSize.Width = 125;
            // 
            // colPropertyProximity
            // 
            this.colPropertyProximity.Caption = "Property Proximity";
            this.colPropertyProximity.FieldName = "PropertyProximity";
            this.colPropertyProximity.Name = "colPropertyProximity";
            this.colPropertyProximity.OptionsColumn.AllowEdit = false;
            this.colPropertyProximity.OptionsColumn.AllowFocus = false;
            this.colPropertyProximity.OptionsColumn.ReadOnly = true;
            this.colPropertyProximity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPropertyProximity.Width = 117;
            // 
            // colSiteLevel
            // 
            this.colSiteLevel.Caption = "Site Level";
            this.colSiteLevel.FieldName = "SiteLevel";
            this.colSiteLevel.Name = "colSiteLevel";
            this.colSiteLevel.OptionsColumn.AllowEdit = false;
            this.colSiteLevel.OptionsColumn.AllowFocus = false;
            this.colSiteLevel.OptionsColumn.ReadOnly = true;
            this.colSiteLevel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSituation
            // 
            this.colSituation.Caption = "Situation";
            this.colSituation.FieldName = "Situation";
            this.colSituation.Name = "colSituation";
            this.colSituation.OptionsColumn.AllowEdit = false;
            this.colSituation.OptionsColumn.AllowFocus = false;
            this.colSituation.OptionsColumn.ReadOnly = true;
            this.colSituation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSituation.Visible = true;
            this.colSituation.VisibleIndex = 11;
            this.colSituation.Width = 105;
            // 
            // colRiskFactor
            // 
            this.colRiskFactor.Caption = "Risk Factor";
            this.colRiskFactor.FieldName = "RiskFactor";
            this.colRiskFactor.Name = "colRiskFactor";
            this.colRiskFactor.OptionsColumn.AllowEdit = false;
            this.colRiskFactor.OptionsColumn.AllowFocus = false;
            this.colRiskFactor.OptionsColumn.ReadOnly = true;
            this.colRiskFactor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRiskFactor.Visible = true;
            this.colRiskFactor.VisibleIndex = 6;
            // 
            // colPolygonXY
            // 
            this.colPolygonXY.Caption = "Polygon X/Y";
            this.colPolygonXY.FieldName = "PolygonXY";
            this.colPolygonXY.Name = "colPolygonXY";
            this.colPolygonXY.OptionsColumn.AllowEdit = false;
            this.colPolygonXY.OptionsColumn.AllowFocus = false;
            this.colPolygonXY.OptionsColumn.ReadOnly = true;
            this.colPolygonXY.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colcolor
            // 
            this.colcolor.Caption = "Color";
            this.colcolor.FieldName = "color";
            this.colcolor.Name = "colcolor";
            this.colcolor.OptionsColumn.AllowEdit = false;
            this.colcolor.OptionsColumn.AllowFocus = false;
            this.colcolor.OptionsColumn.ReadOnly = true;
            this.colcolor.OptionsColumn.ShowInCustomizationForm = false;
            this.colcolor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.OptionsColumn.AllowEdit = false;
            this.colOwnershipName.OptionsColumn.AllowFocus = false;
            this.colOwnershipName.OptionsColumn.ReadOnly = true;
            this.colOwnershipName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 29;
            this.colOwnershipName.Width = 103;
            // 
            // colLinkedInspectionCount
            // 
            this.colLinkedInspectionCount.Caption = "Inspection Count";
            this.colLinkedInspectionCount.FieldName = "LinkedInspectionCount";
            this.colLinkedInspectionCount.Name = "colLinkedInspectionCount";
            this.colLinkedInspectionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedInspectionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedInspectionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedInspectionCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedInspectionCount.Visible = true;
            this.colLinkedInspectionCount.VisibleIndex = 30;
            this.colLinkedInspectionCount.Width = 104;
            // 
            // Calculated1
            // 
            this.Calculated1.Caption = "<b>Calculated 1</b>";
            this.Calculated1.FieldName = "Calculated1";
            this.Calculated1.Name = "Calculated1";
            this.Calculated1.OptionsColumn.AllowEdit = false;
            this.Calculated1.OptionsColumn.AllowFocus = false;
            this.Calculated1.OptionsColumn.ReadOnly = true;
            this.Calculated1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.Calculated1.ShowUnboundExpressionMenu = true;
            this.Calculated1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1.Visible = true;
            this.Calculated1.VisibleIndex = 32;
            this.Calculated1.Width = 90;
            // 
            // Calculated2
            // 
            this.Calculated2.Caption = "<b>Calculated 2<b>";
            this.Calculated2.FieldName = "Calculated2";
            this.Calculated2.Name = "Calculated2";
            this.Calculated2.OptionsColumn.AllowEdit = false;
            this.Calculated2.OptionsColumn.AllowFocus = false;
            this.Calculated2.OptionsColumn.ReadOnly = true;
            this.Calculated2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.Calculated2.ShowUnboundExpressionMenu = true;
            this.Calculated2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2.Visible = true;
            this.Calculated2.VisibleIndex = 33;
            this.Calculated2.Width = 90;
            // 
            // Calculated3
            // 
            this.Calculated3.Caption = "<b>Calculated 3</b>";
            this.Calculated3.FieldName = "Calculated3";
            this.Calculated3.Name = "Calculated3";
            this.Calculated3.OptionsColumn.AllowEdit = false;
            this.Calculated3.OptionsColumn.AllowFocus = false;
            this.Calculated3.OptionsColumn.ReadOnly = true;
            this.Calculated3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.Calculated3.ShowUnboundExpressionMenu = true;
            this.Calculated3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3.Visible = true;
            this.Calculated3.VisibleIndex = 34;
            this.Calculated3.Width = 90;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coord";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            this.colXCoordinate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coord";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            this.colYCoordinate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpecies
            // 
            this.colSpecies.Caption = "Species Name";
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.OptionsColumn.AllowEdit = false;
            this.colSpecies.OptionsColumn.AllowFocus = false;
            this.colSpecies.OptionsColumn.ReadOnly = true;
            this.colSpecies.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpecies.Visible = true;
            this.colSpecies.VisibleIndex = 3;
            this.colSpecies.Width = 87;
            // 
            // colObjectType
            // 
            this.colObjectType.Caption = "Object Type";
            this.colObjectType.FieldName = "ObjectType";
            this.colObjectType.Name = "colObjectType";
            this.colObjectType.OptionsColumn.AllowEdit = false;
            this.colObjectType.OptionsColumn.AllowFocus = false;
            this.colObjectType.OptionsColumn.ReadOnly = true;
            this.colObjectType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colObjectType.Visible = true;
            this.colObjectType.VisibleIndex = 4;
            this.colObjectType.Width = 80;
            // 
            // colMapID
            // 
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMapID.Visible = true;
            this.colMapID.VisibleIndex = 5;
            // 
            // colHighlight
            // 
            this.colHighlight.Caption = "Highlight";
            this.colHighlight.ColumnEdit = this.repositoryItemCheckEditHighlight;
            this.colHighlight.FieldName = "Highlight";
            this.colHighlight.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colHighlight.Name = "colHighlight";
            this.colHighlight.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHighlight.Visible = true;
            this.colHighlight.VisibleIndex = 0;
            // 
            // repositoryItemCheckEditHighlight
            // 
            this.repositoryItemCheckEditHighlight.AutoHeight = false;
            this.repositoryItemCheckEditHighlight.Caption = "Check";
            this.repositoryItemCheckEditHighlight.Name = "repositoryItemCheckEditHighlight";
            this.repositoryItemCheckEditHighlight.ValueChecked = 1;
            this.repositoryItemCheckEditHighlight.ValueUnchecked = 0;
            this.repositoryItemCheckEditHighlight.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEditHighlight_CheckedChanged);
            // 
            // colintAccess
            // 
            this.colintAccess.FieldName = "intAccess";
            this.colintAccess.Name = "colintAccess";
            this.colintAccess.OptionsColumn.AllowEdit = false;
            this.colintAccess.OptionsColumn.AllowFocus = false;
            this.colintAccess.OptionsColumn.ReadOnly = true;
            this.colintAccess.OptionsColumn.ShowInCustomizationForm = false;
            this.colintAccess.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintAgeClass
            // 
            this.colintAgeClass.FieldName = "intAgeClass";
            this.colintAgeClass.Name = "colintAgeClass";
            this.colintAgeClass.OptionsColumn.AllowEdit = false;
            this.colintAgeClass.OptionsColumn.AllowFocus = false;
            this.colintAgeClass.OptionsColumn.ReadOnly = true;
            this.colintAgeClass.OptionsColumn.ShowInCustomizationForm = false;
            this.colintAgeClass.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintDBHRange
            // 
            this.colintDBHRange.FieldName = "intDBHRange";
            this.colintDBHRange.Name = "colintDBHRange";
            this.colintDBHRange.OptionsColumn.AllowEdit = false;
            this.colintDBHRange.OptionsColumn.AllowFocus = false;
            this.colintDBHRange.OptionsColumn.ReadOnly = true;
            this.colintDBHRange.OptionsColumn.ShowInCustomizationForm = false;
            this.colintDBHRange.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintDistrictID
            // 
            this.colintDistrictID.FieldName = "intDistrictID";
            this.colintDistrictID.Name = "colintDistrictID";
            this.colintDistrictID.OptionsColumn.AllowEdit = false;
            this.colintDistrictID.OptionsColumn.AllowFocus = false;
            this.colintDistrictID.OptionsColumn.ReadOnly = true;
            this.colintDistrictID.OptionsColumn.ShowInCustomizationForm = false;
            this.colintDistrictID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintGroundType
            // 
            this.colintGroundType.FieldName = "intGroundType";
            this.colintGroundType.Name = "colintGroundType";
            this.colintGroundType.OptionsColumn.AllowEdit = false;
            this.colintGroundType.OptionsColumn.AllowFocus = false;
            this.colintGroundType.OptionsColumn.ReadOnly = true;
            this.colintGroundType.OptionsColumn.ShowInCustomizationForm = false;
            this.colintGroundType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintHeightRange
            // 
            this.colintHeightRange.FieldName = "intHeightRange";
            this.colintHeightRange.Name = "colintHeightRange";
            this.colintHeightRange.OptionsColumn.AllowEdit = false;
            this.colintHeightRange.OptionsColumn.AllowFocus = false;
            this.colintHeightRange.OptionsColumn.ReadOnly = true;
            this.colintHeightRange.OptionsColumn.ShowInCustomizationForm = false;
            this.colintHeightRange.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintLegalStatus
            // 
            this.colintLegalStatus.FieldName = "intLegalStatus";
            this.colintLegalStatus.Name = "colintLegalStatus";
            this.colintLegalStatus.OptionsColumn.AllowEdit = false;
            this.colintLegalStatus.OptionsColumn.AllowFocus = false;
            this.colintLegalStatus.OptionsColumn.ReadOnly = true;
            this.colintLegalStatus.OptionsColumn.ShowInCustomizationForm = false;
            this.colintLegalStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintLocalityID
            // 
            this.colintLocalityID.FieldName = "intLocalityID";
            this.colintLocalityID.Name = "colintLocalityID";
            this.colintLocalityID.OptionsColumn.AllowEdit = false;
            this.colintLocalityID.OptionsColumn.AllowFocus = false;
            this.colintLocalityID.OptionsColumn.ReadOnly = true;
            this.colintLocalityID.OptionsColumn.ShowInCustomizationForm = false;
            this.colintLocalityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintNearbyObject1
            // 
            this.colintNearbyObject1.FieldName = "intNearbyObject1";
            this.colintNearbyObject1.Name = "colintNearbyObject1";
            this.colintNearbyObject1.OptionsColumn.AllowEdit = false;
            this.colintNearbyObject1.OptionsColumn.AllowFocus = false;
            this.colintNearbyObject1.OptionsColumn.ReadOnly = true;
            this.colintNearbyObject1.OptionsColumn.ShowInCustomizationForm = false;
            this.colintNearbyObject1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintNearbyObject2
            // 
            this.colintNearbyObject2.FieldName = "intNearbyObject2";
            this.colintNearbyObject2.Name = "colintNearbyObject2";
            this.colintNearbyObject2.OptionsColumn.AllowEdit = false;
            this.colintNearbyObject2.OptionsColumn.AllowFocus = false;
            this.colintNearbyObject2.OptionsColumn.ReadOnly = true;
            this.colintNearbyObject2.OptionsColumn.ShowInCustomizationForm = false;
            this.colintNearbyObject2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintNearbyObject3
            // 
            this.colintNearbyObject3.FieldName = "intNearbyObject3";
            this.colintNearbyObject3.Name = "colintNearbyObject3";
            this.colintNearbyObject3.OptionsColumn.AllowEdit = false;
            this.colintNearbyObject3.OptionsColumn.AllowFocus = false;
            this.colintNearbyObject3.OptionsColumn.ReadOnly = true;
            this.colintNearbyObject3.OptionsColumn.ShowInCustomizationForm = false;
            this.colintNearbyObject3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintOwnershipID
            // 
            this.colintOwnershipID.FieldName = "intOwnershipID";
            this.colintOwnershipID.Name = "colintOwnershipID";
            this.colintOwnershipID.OptionsColumn.AllowEdit = false;
            this.colintOwnershipID.OptionsColumn.AllowFocus = false;
            this.colintOwnershipID.OptionsColumn.ReadOnly = true;
            this.colintOwnershipID.OptionsColumn.ShowInCustomizationForm = false;
            this.colintOwnershipID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintProtectionType
            // 
            this.colintProtectionType.FieldName = "intProtectionType";
            this.colintProtectionType.Name = "colintProtectionType";
            this.colintProtectionType.OptionsColumn.AllowEdit = false;
            this.colintProtectionType.OptionsColumn.AllowFocus = false;
            this.colintProtectionType.OptionsColumn.ReadOnly = true;
            this.colintProtectionType.OptionsColumn.ShowInCustomizationForm = false;
            this.colintProtectionType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintSafetyPriority
            // 
            this.colintSafetyPriority.FieldName = "intSafetyPriority";
            this.colintSafetyPriority.Name = "colintSafetyPriority";
            this.colintSafetyPriority.OptionsColumn.AllowEdit = false;
            this.colintSafetyPriority.OptionsColumn.AllowFocus = false;
            this.colintSafetyPriority.OptionsColumn.ReadOnly = true;
            this.colintSafetyPriority.OptionsColumn.ShowInCustomizationForm = false;
            this.colintSafetyPriority.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintSiteHazardClass
            // 
            this.colintSiteHazardClass.FieldName = "intSiteHazardClass";
            this.colintSiteHazardClass.Name = "colintSiteHazardClass";
            this.colintSiteHazardClass.OptionsColumn.AllowEdit = false;
            this.colintSiteHazardClass.OptionsColumn.AllowFocus = false;
            this.colintSiteHazardClass.OptionsColumn.ReadOnly = true;
            this.colintSiteHazardClass.OptionsColumn.ShowInCustomizationForm = false;
            this.colintSiteHazardClass.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintSize
            // 
            this.colintSize.FieldName = "intSize";
            this.colintSize.Name = "colintSize";
            this.colintSize.OptionsColumn.AllowEdit = false;
            this.colintSize.OptionsColumn.AllowFocus = false;
            this.colintSize.OptionsColumn.ReadOnly = true;
            this.colintSize.OptionsColumn.ShowInCustomizationForm = false;
            this.colintSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintSpeciesID
            // 
            this.colintSpeciesID.FieldName = "intSpeciesID";
            this.colintSpeciesID.Name = "colintSpeciesID";
            this.colintSpeciesID.OptionsColumn.AllowEdit = false;
            this.colintSpeciesID.OptionsColumn.AllowFocus = false;
            this.colintSpeciesID.OptionsColumn.ReadOnly = true;
            this.colintSpeciesID.OptionsColumn.ShowInCustomizationForm = false;
            this.colintSpeciesID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintStatus
            // 
            this.colintStatus.FieldName = "intStatus";
            this.colintStatus.Name = "colintStatus";
            this.colintStatus.OptionsColumn.AllowEdit = false;
            this.colintStatus.OptionsColumn.AllowFocus = false;
            this.colintStatus.OptionsColumn.ReadOnly = true;
            this.colintStatus.OptionsColumn.ShowInCustomizationForm = false;
            this.colintStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintVisibility
            // 
            this.colintVisibility.FieldName = "intVisibility";
            this.colintVisibility.Name = "colintVisibility";
            this.colintVisibility.OptionsColumn.AllowEdit = false;
            this.colintVisibility.OptionsColumn.AllowFocus = false;
            this.colintVisibility.OptionsColumn.AllowShowHide = false;
            this.colintVisibility.OptionsColumn.ReadOnly = true;
            this.colintVisibility.OptionsColumn.ShowInCustomizationForm = false;
            this.colintVisibility.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colLastInspectionElapsedDays
            // 
            this.colLastInspectionElapsedDays.Caption = "Last Inspection Elapsed Days";
            this.colLastInspectionElapsedDays.FieldName = "LastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.Name = "colLastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.OptionsColumn.AllowEdit = false;
            this.colLastInspectionElapsedDays.OptionsColumn.AllowFocus = false;
            this.colLastInspectionElapsedDays.OptionsColumn.ReadOnly = true;
            this.colLastInspectionElapsedDays.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastInspectionElapsedDays.Visible = true;
            this.colLastInspectionElapsedDays.VisibleIndex = 17;
            this.colLastInspectionElapsedDays.Width = 162;
            // 
            // checkEditSyncSelection
            // 
            this.checkEditSyncSelection.Location = new System.Drawing.Point(2, 2);
            this.checkEditSyncSelection.MenuManager = this.barManager1;
            this.checkEditSyncSelection.Name = "checkEditSyncSelection";
            this.checkEditSyncSelection.Properties.Caption = "Synchronise Map and List Selection";
            this.checkEditSyncSelection.Size = new System.Drawing.Size(330, 19);
            this.checkEditSyncSelection.StyleController = this.layoutControl1;
            this.checkEditSyncSelection.TabIndex = 12;
            // 
            // colorEditHighlight
            // 
            this.colorEditHighlight.EditValue = System.Drawing.Color.CornflowerBlue;
            this.colorEditHighlight.Location = new System.Drawing.Point(89, 557);
            this.colorEditHighlight.MenuManager = this.barManager1;
            this.colorEditHighlight.Name = "colorEditHighlight";
            this.colorEditHighlight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "", "DropDown", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Highlight", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "", "Highlight", null, false),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "", "Clear", null, false)});
            this.colorEditHighlight.Size = new System.Drawing.Size(235, 20);
            this.colorEditHighlight.StyleController = this.layoutControl1;
            this.colorEditHighlight.TabIndex = 3;
            this.colorEditHighlight.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.colorEditHighlight_ButtonClick);
            this.colorEditHighlight.EditValueChanged += new System.EventHandler(this.colorEditHighlight_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem55,
            this.emptySpaceItem3,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(334, 633);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem51,
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 525);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(334, 108);
            this.layoutControlGroup2.Text = "Highlighting";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.colorEditHighlight;
            this.layoutControlItem1.CustomizationFormText = "Colour:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem1.Text = "Colour:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ceCentreMap;
            this.layoutControlItem2.CustomizationFormText = "Centre Map:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(218, 23);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.csScaleMapForHighlighted;
            this.layoutControlItem51.CustomizationFormText = "layoutControlItem51";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 47);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(218, 23);
            this.layoutControlItem51.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem51.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnSelectHighlighted;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(218, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(100, 46);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.btnRefreshMapObjects;
            this.layoutControlItem55.CustomizationFormText = "layoutControlItem55";
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 499);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(133, 26);
            this.layoutControlItem55.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem55.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(236, 499);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(98, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnClearMapObjects;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(133, 499);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(103, 26);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.checkEditSyncSelection;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(334, 23);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(334, 476);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup5});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Asset Management Objects";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem7,
            this.layoutControlItem17});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(318, 439);
            this.layoutControlGroup5.Text = "Asset Management Objects";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridSplitContainer1;
            this.layoutControlItem12.CustomizationFormText = "Asset Management Objects Grid:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(318, 367);
            this.layoutControlItem12.Text = "Asset Management Objects Grid:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.popupContainerEditAssetTypes;
            this.layoutControlItem14.CustomizationFormText = "Asset Types(s):";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem14.Text = "Asset Types(s):";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonEditClientFilter2;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem7.Text = "Client(s):";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.buttonEditSiteFilter2;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem17.Text = "Site(s):";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Amenity Trees Objects";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem54,
            this.layoutControlItem8,
            this.layoutControlItem15,
            this.layoutControlItem16});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(318, 439);
            this.layoutControlGroup3.Text = "Amenity Tree Objects";
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.Control = this.gridSplitContainer2;
            this.layoutControlItem54.CustomizationFormText = "Map Object Grid:";
            this.layoutControlItem54.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(318, 367);
            this.layoutControlItem54.Text = "Map Object Grid:";
            this.layoutControlItem54.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem54.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.popupContainerEditTreeRefStructure;
            this.layoutControlItem8.CustomizationFormText = "Tree Ref:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem8.Text = "Tree Ref:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.buttonEditClientFilter;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem15.Text = "Client(s):";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.buttonEditSiteFilter;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem16.Text = "Site(s):";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(76, 13);
            // 
            // dockPanel4
            // 
            this.dockPanel4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel4.Appearance.Options.UseBackColor = true;
            this.dockPanel4.Controls.Add(this.dockPanel4_Container);
            this.dockPanel4.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel4.ID = new System.Guid("c69e68c1-c555-445c-a79b-a9cdadc530a6");
            this.dockPanel4.Location = new System.Drawing.Point(3, 29);
            this.dockPanel4.Name = "dockPanel4";
            this.dockPanel4.Options.ShowCloseButton = false;
            this.dockPanel4.OriginalSize = new System.Drawing.Size(334, 698);
            this.dockPanel4.Size = new System.Drawing.Size(334, 633);
            this.dockPanel4.Text = "Layer Manager";
            // 
            // dockPanel4_Container
            // 
            this.dockPanel4_Container.Controls.Add(this.layoutControl6);
            this.dockPanel4_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel4_Container.Name = "dockPanel4_Container";
            this.dockPanel4_Container.Size = new System.Drawing.Size(334, 633);
            this.dockPanel4_Container.TabIndex = 0;
            // 
            // layoutControl6
            // 
            this.layoutControl6.Controls.Add(this.gridSplitContainer3);
            this.layoutControl6.Controls.Add(this.beWorkspace);
            this.layoutControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl6.Location = new System.Drawing.Point(0, 0);
            this.layoutControl6.Name = "layoutControl6";
            this.layoutControl6.Root = this.layoutControlGroup14;
            this.layoutControl6.Size = new System.Drawing.Size(334, 633);
            this.layoutControl6.TabIndex = 0;
            this.layoutControl6.Text = "layoutControl6";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Grid = this.gridControl5;
            this.gridSplitContainer3.Location = new System.Drawing.Point(2, 28);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl5);
            this.gridSplitContainer3.Size = new System.Drawing.Size(330, 603);
            this.gridSplitContainer3.TabIndex = 5;
            // 
            // gridControl5
            // 
            this.gridControl5.AllowDrop = true;
            this.gridControl5.DataSource = this.sp01288ATTreePickerWorkspacelayerslistBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Move Layer Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 16, true, true, "Move Layer Down", "down")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCE_LayerHitable,
            this.repositoryItemCE_LayerVisible});
            this.gridControl5.Size = new System.Drawing.Size(330, 603);
            this.gridControl5.TabIndex = 5;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            this.gridControl5.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControl5_DragDrop);
            this.gridControl5.DragOver += new System.Windows.Forms.DragEventHandler(this.gridControl5_DragOver);
            this.gridControl5.DragLeave += new System.EventHandler(this.gridControl5_DragLeave);
            this.gridControl5.Paint += new System.Windows.Forms.PaintEventHandler(this.gridControl5_Paint);
            // 
            // sp01288ATTreePickerWorkspacelayerslistBindingSource
            // 
            this.sp01288ATTreePickerWorkspacelayerslistBindingSource.DataMember = "sp01288_AT_Tree_Picker_Workspace_layers_list";
            this.sp01288ATTreePickerWorkspacelayerslistBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLayerID,
            this.colWorkspaceID,
            this.colLayerType,
            this.colLayerTypeDescription,
            this.colLayerPath,
            this.colLayerOrder,
            this.colLayerVisible,
            this.colLayerHitable,
            this.colPreserveDefaultStyling,
            this.colTransparency,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.colUseThematicStyling,
            this.colThematicStyleSet,
            this.colPointSize,
            this.colLayerName1,
            this.colDummyLayerPathForSave});
            this.gridView5.CustomizationFormBounds = new System.Drawing.Rectangle(1158, 518, 208, 191);
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsCustomization.AllowFilter = false;
            this.gridView5.OptionsCustomization.AllowGroup = false;
            this.gridView5.OptionsCustomization.AllowSort = false;
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLayerOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.ViewCaption = "Loaded Layers";
            this.gridView5.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView5_CustomRowCellEdit);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView5_CustomDrawEmptyForeground);
            this.gridView5.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView5_FocusedRowChanged);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseDown);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseMove);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colLayerID
            // 
            this.colLayerID.Caption = "Layer ID";
            this.colLayerID.FieldName = "LayerID";
            this.colLayerID.Name = "colLayerID";
            this.colLayerID.OptionsColumn.AllowEdit = false;
            this.colLayerID.OptionsColumn.AllowFocus = false;
            this.colLayerID.OptionsColumn.ReadOnly = true;
            this.colLayerID.Width = 58;
            // 
            // colWorkspaceID
            // 
            this.colWorkspaceID.Caption = "Workspace ID";
            this.colWorkspaceID.FieldName = "WorkspaceID";
            this.colWorkspaceID.Name = "colWorkspaceID";
            this.colWorkspaceID.OptionsColumn.AllowEdit = false;
            this.colWorkspaceID.OptionsColumn.AllowFocus = false;
            this.colWorkspaceID.OptionsColumn.ReadOnly = true;
            this.colWorkspaceID.Width = 78;
            // 
            // colLayerType
            // 
            this.colLayerType.Caption = "Layer Type ID";
            this.colLayerType.FieldName = "LayerType";
            this.colLayerType.Name = "colLayerType";
            this.colLayerType.OptionsColumn.AllowEdit = false;
            this.colLayerType.OptionsColumn.AllowFocus = false;
            this.colLayerType.OptionsColumn.ReadOnly = true;
            this.colLayerType.Width = 79;
            // 
            // colLayerTypeDescription
            // 
            this.colLayerTypeDescription.Caption = "Layer Type";
            this.colLayerTypeDescription.FieldName = "LayerTypeDescription";
            this.colLayerTypeDescription.Name = "colLayerTypeDescription";
            this.colLayerTypeDescription.OptionsColumn.AllowEdit = false;
            this.colLayerTypeDescription.OptionsColumn.AllowFocus = false;
            this.colLayerTypeDescription.OptionsColumn.ReadOnly = true;
            this.colLayerTypeDescription.Visible = true;
            this.colLayerTypeDescription.VisibleIndex = 1;
            // 
            // colLayerPath
            // 
            this.colLayerPath.Caption = "Layer Path";
            this.colLayerPath.FieldName = "LayerPath";
            this.colLayerPath.Name = "colLayerPath";
            this.colLayerPath.OptionsColumn.AllowEdit = false;
            this.colLayerPath.OptionsColumn.AllowFocus = false;
            this.colLayerPath.OptionsColumn.ReadOnly = true;
            this.colLayerPath.Width = 73;
            // 
            // colLayerOrder
            // 
            this.colLayerOrder.Caption = "Order";
            this.colLayerOrder.FieldName = "LayerOrder";
            this.colLayerOrder.Name = "colLayerOrder";
            this.colLayerOrder.OptionsColumn.AllowEdit = false;
            this.colLayerOrder.OptionsColumn.AllowFocus = false;
            this.colLayerOrder.OptionsColumn.ReadOnly = true;
            this.colLayerOrder.Width = 62;
            // 
            // colLayerVisible
            // 
            this.colLayerVisible.Caption = "Visible";
            this.colLayerVisible.ColumnEdit = this.repositoryItemCE_LayerVisible;
            this.colLayerVisible.FieldName = "LayerVisible";
            this.colLayerVisible.Name = "colLayerVisible";
            this.colLayerVisible.Visible = true;
            this.colLayerVisible.VisibleIndex = 2;
            this.colLayerVisible.Width = 50;
            // 
            // repositoryItemCE_LayerVisible
            // 
            this.repositoryItemCE_LayerVisible.AutoHeight = false;
            this.repositoryItemCE_LayerVisible.Caption = "Check";
            this.repositoryItemCE_LayerVisible.Name = "repositoryItemCE_LayerVisible";
            this.repositoryItemCE_LayerVisible.ValueChecked = 1;
            this.repositoryItemCE_LayerVisible.ValueUnchecked = 0;
            this.repositoryItemCE_LayerVisible.CheckedChanged += new System.EventHandler(this.repositoryItemCE_LayerVisible_CheckedChanged);
            // 
            // colLayerHitable
            // 
            this.colLayerHitable.Caption = "Hitable";
            this.colLayerHitable.ColumnEdit = this.repositoryItemCE_LayerHitable;
            this.colLayerHitable.FieldName = "LayerHitable";
            this.colLayerHitable.Name = "colLayerHitable";
            this.colLayerHitable.Visible = true;
            this.colLayerHitable.VisibleIndex = 3;
            this.colLayerHitable.Width = 54;
            // 
            // repositoryItemCE_LayerHitable
            // 
            this.repositoryItemCE_LayerHitable.AutoHeight = false;
            this.repositoryItemCE_LayerHitable.Caption = "Check";
            this.repositoryItemCE_LayerHitable.Name = "repositoryItemCE_LayerHitable";
            this.repositoryItemCE_LayerHitable.ValueChecked = 1;
            this.repositoryItemCE_LayerHitable.ValueUnchecked = 0;
            this.repositoryItemCE_LayerHitable.CheckedChanged += new System.EventHandler(this.repositoryItemCE_LayerHitable_CheckedChanged);
            // 
            // colPreserveDefaultStyling
            // 
            this.colPreserveDefaultStyling.Caption = "Preserve Default Styling";
            this.colPreserveDefaultStyling.FieldName = "PreserveDefaultStyling";
            this.colPreserveDefaultStyling.Name = "colPreserveDefaultStyling";
            this.colPreserveDefaultStyling.OptionsColumn.AllowEdit = false;
            this.colPreserveDefaultStyling.OptionsColumn.AllowFocus = false;
            this.colPreserveDefaultStyling.OptionsColumn.ReadOnly = true;
            this.colPreserveDefaultStyling.Width = 137;
            // 
            // colTransparency
            // 
            this.colTransparency.Caption = "Transparency";
            this.colTransparency.FieldName = "Transparency";
            this.colTransparency.Name = "colTransparency";
            this.colTransparency.OptionsColumn.AllowEdit = false;
            this.colTransparency.OptionsColumn.AllowFocus = false;
            this.colTransparency.OptionsColumn.ReadOnly = true;
            this.colTransparency.Width = 87;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Line Colour";
            this.gridColumn2.FieldName = "LineColour";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 74;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Line Width";
            this.gridColumn3.FieldName = "LineWidth";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 71;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Grey Scale";
            this.gridColumn4.FieldName = "GreyScale";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 72;
            // 
            // colUseThematicStyling
            // 
            this.colUseThematicStyling.Caption = "Use Thematic Styling";
            this.colUseThematicStyling.FieldName = "UseThematicStyling";
            this.colUseThematicStyling.Name = "colUseThematicStyling";
            this.colUseThematicStyling.OptionsColumn.AllowEdit = false;
            this.colUseThematicStyling.OptionsColumn.AllowFocus = false;
            this.colUseThematicStyling.OptionsColumn.ReadOnly = true;
            this.colUseThematicStyling.Width = 120;
            // 
            // colThematicStyleSet
            // 
            this.colThematicStyleSet.Caption = "Thematic Style Set";
            this.colThematicStyleSet.FieldName = "ThematicStyleSet";
            this.colThematicStyleSet.Name = "colThematicStyleSet";
            this.colThematicStyleSet.OptionsColumn.AllowEdit = false;
            this.colThematicStyleSet.OptionsColumn.AllowFocus = false;
            this.colThematicStyleSet.OptionsColumn.ReadOnly = true;
            this.colThematicStyleSet.Width = 110;
            // 
            // colPointSize
            // 
            this.colPointSize.Caption = "Point Size";
            this.colPointSize.FieldName = "PointSize";
            this.colPointSize.Name = "colPointSize";
            this.colPointSize.OptionsColumn.AllowEdit = false;
            this.colPointSize.OptionsColumn.AllowFocus = false;
            this.colPointSize.OptionsColumn.ReadOnly = true;
            this.colPointSize.Width = 67;
            // 
            // colLayerName1
            // 
            this.colLayerName1.Caption = "Layer Name";
            this.colLayerName1.FieldName = "LayerName";
            this.colLayerName1.Name = "colLayerName1";
            this.colLayerName1.OptionsColumn.AllowEdit = false;
            this.colLayerName1.OptionsColumn.AllowFocus = false;
            this.colLayerName1.OptionsColumn.ReadOnly = true;
            this.colLayerName1.Visible = true;
            this.colLayerName1.VisibleIndex = 0;
            this.colLayerName1.Width = 176;
            // 
            // colDummyLayerPathForSave
            // 
            this.colDummyLayerPathForSave.Caption = "Save Layer Path";
            this.colDummyLayerPathForSave.FieldName = "DummyLayerPathForSave";
            this.colDummyLayerPathForSave.Name = "colDummyLayerPathForSave";
            // 
            // beWorkspace
            // 
            this.beWorkspace.Location = new System.Drawing.Point(62, 2);
            this.beWorkspace.MenuManager = this.barManager1;
            this.beWorkspace.Name = "beWorkspace";
            superToolTip36.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem36.Text = "Select - Information";
            toolTipItem36.LeftIndent = 6;
            toolTipItem36.Text = "Click me to open the <b>Mapping Workspace Selection</b> screen.\r\n\r\n<color=blue>Ti" +
    "p:</color> This screen can be used to select, create, edit and delete workspaces" +
    ".";
            superToolTip36.Items.Add(toolTipTitleItem36);
            superToolTip36.Items.Add(toolTipItem36);
            toolTipTitleItem37.Text = "Save Workspace Changes - Information";
            toolTipItem37.LeftIndent = 6;
            toolTipItem37.Text = "Click me to save changes to the current workspace.";
            superToolTip37.Items.Add(toolTipTitleItem37);
            superToolTip37.Items.Add(toolTipItem37);
            superToolTip38.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem38.Text = "Save Workspace As - Information";
            toolTipItem38.LeftIndent = 6;
            toolTipItem38.Text = "Click me to save a copy of the current workspace as a new workspace.\r\n\r\n<b><color" +
    "=green>Tip:</color></b> You can then make changes to the copy without effecting " +
    "the original workspace.";
            superToolTip38.Items.Add(toolTipTitleItem38);
            superToolTip38.Items.Add(toolTipItem38);
            this.beWorkspace.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Select", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "", "Select", superToolTip36, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Save", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("beWorkspace.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject57, serializableAppearanceObject58, serializableAppearanceObject59, serializableAppearanceObject60, "", "Save", superToolTip37, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Save AS", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("beWorkspace.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject61, serializableAppearanceObject62, serializableAppearanceObject63, serializableAppearanceObject64, "", "SaveAs", superToolTip38, true)});
            this.beWorkspace.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.beWorkspace.Size = new System.Drawing.Size(270, 22);
            this.beWorkspace.StyleController = this.layoutControl6;
            this.beWorkspace.TabIndex = 4;
            this.beWorkspace.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.beWorkspace_ButtonClick);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "layoutControlGroup14";
            this.layoutControlGroup14.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup14.GroupBordersVisible = false;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem52,
            this.layoutControlItem53});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup14.Name = "Root";
            this.layoutControlGroup14.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup14.Size = new System.Drawing.Size(334, 633);
            this.layoutControlGroup14.TextVisible = false;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.beWorkspace;
            this.layoutControlItem52.CustomizationFormText = "Workspace:";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(334, 26);
            this.layoutControlItem52.Text = "Workspace:";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.gridSplitContainer3;
            this.layoutControlItem53.CustomizationFormText = "Layer Grid:";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(334, 607);
            this.layoutControlItem53.Text = "Layer Grid:";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem53.TextVisible = false;
            // 
            // dockPanel3
            // 
            this.dockPanel3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel3.Appearance.Options.UseBackColor = true;
            this.dockPanel3.Controls.Add(this.dockPanel3_Container);
            this.dockPanel3.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel3.FloatSize = new System.Drawing.Size(279, 385);
            this.dockPanel3.FloatVertical = true;
            this.dockPanel3.ID = new System.Guid("59c82752-c9f0-4a41-ad5f-04fc17d69dce");
            this.dockPanel3.Location = new System.Drawing.Point(3, 29);
            this.dockPanel3.Name = "dockPanel3";
            this.dockPanel3.Options.ShowCloseButton = false;
            this.dockPanel3.OriginalSize = new System.Drawing.Size(334, 698);
            this.dockPanel3.Size = new System.Drawing.Size(334, 633);
            this.dockPanel3.Text = "GPS";
            // 
            // dockPanel3_Container
            // 
            this.dockPanel3_Container.Controls.Add(this.statusBar1);
            this.dockPanel3_Container.Controls.Add(this.NMEAtabs);
            this.dockPanel3_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            this.dockPanel3_Container.Size = new System.Drawing.Size(334, 633);
            this.dockPanel3_Container.TabIndex = 0;
            // 
            // statusBar1
            // 
            this.statusBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusBar1.Location = new System.Drawing.Point(0, 610);
            this.statusBar1.MenuManager = this.barManager1;
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.statusBar1.Properties.Appearance.Options.UseFont = true;
            this.statusBar1.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.statusBar1, true);
            this.statusBar1.Size = new System.Drawing.Size(334, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.statusBar1, optionsSpelling2);
            this.statusBar1.TabIndex = 1;
            // 
            // NMEAtabs
            // 
            this.NMEAtabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NMEAtabs.Location = new System.Drawing.Point(0, 0);
            this.NMEAtabs.Name = "NMEAtabs";
            this.NMEAtabs.SelectedTabPage = this.xtraTabPage1;
            this.NMEAtabs.Size = new System.Drawing.Size(335, 607);
            this.NMEAtabs.TabIndex = 0;
            this.NMEAtabs.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.tabGPRMC,
            this.tabGPGGA,
            this.tabGPGLL,
            this.tabGPGSA,
            this.tabGPGSV,
            this.tabRaw});
            this.NMEAtabs.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.NMEAtabs_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.labelPlottingUsingTimer);
            this.xtraTabPage1.Controls.Add(this.btnGps_PauseTimer);
            this.xtraTabPage1.Controls.Add(this.layoutControl_Gps_Page1);
            this.xtraTabPage1.Controls.Add(this.groupControl2);
            this.xtraTabPage1.Controls.Add(this.btnGPS_Start);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(330, 581);
            this.xtraTabPage1.Text = "Main";
            // 
            // labelPlottingUsingTimer
            // 
            this.labelPlottingUsingTimer.AllowHtmlString = true;
            this.labelPlottingUsingTimer.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelPlottingUsingTimer.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelPlottingUsingTimer.Appearance.Image")));
            this.labelPlottingUsingTimer.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelPlottingUsingTimer.Location = new System.Drawing.Point(87, 216);
            this.labelPlottingUsingTimer.Name = "labelPlottingUsingTimer";
            this.labelPlottingUsingTimer.Size = new System.Drawing.Size(150, 36);
            this.labelPlottingUsingTimer.TabIndex = 6;
            this.labelPlottingUsingTimer.Text = "Plotting using Timer";
            this.labelPlottingUsingTimer.Visible = false;
            // 
            // btnGps_PauseTimer
            // 
            this.btnGps_PauseTimer.Enabled = false;
            this.btnGps_PauseTimer.Location = new System.Drawing.Point(9, 135);
            this.btnGps_PauseTimer.Name = "btnGps_PauseTimer";
            this.btnGps_PauseTimer.Size = new System.Drawing.Size(75, 23);
            this.btnGps_PauseTimer.TabIndex = 5;
            this.btnGps_PauseTimer.Text = "Pause Timer";
            this.btnGps_PauseTimer.Click += new System.EventHandler(this.btnGps_PauseTimer_Click);
            // 
            // layoutControl_Gps_Page1
            // 
            this.layoutControl_Gps_Page1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl_Gps_Page1.Controls.Add(this.ceLogRawGPSData);
            this.layoutControl_Gps_Page1.Controls.Add(this.cmbPortName);
            this.layoutControl_Gps_Page1.Controls.Add(this.cmbBaudRate);
            this.layoutControl_Gps_Page1.Controls.Add(this.ceGps_PlotWithTimer);
            this.layoutControl_Gps_Page1.Controls.Add(this.seGps_PlotTimerInterval);
            this.layoutControl_Gps_Page1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_Gps_Page1.MinimumSize = new System.Drawing.Size(0, 90);
            this.layoutControl_Gps_Page1.Name = "layoutControl_Gps_Page1";
            this.layoutControl_Gps_Page1.Root = this.layoutControlGroup12;
            this.layoutControl_Gps_Page1.Size = new System.Drawing.Size(328, 102);
            this.layoutControl_Gps_Page1.TabIndex = 4;
            this.layoutControl_Gps_Page1.Text = "layoutControl6";
            // 
            // ceLogRawGPSData
            // 
            this.ceLogRawGPSData.Location = new System.Drawing.Point(7, 53);
            this.ceLogRawGPSData.MenuManager = this.barManager1;
            this.ceLogRawGPSData.Name = "ceLogRawGPSData";
            this.ceLogRawGPSData.Properties.Caption = "Log Raw GPS Data";
            this.ceLogRawGPSData.Size = new System.Drawing.Size(314, 19);
            this.ceLogRawGPSData.StyleController = this.layoutControl_Gps_Page1;
            this.ceLogRawGPSData.TabIndex = 6;
            this.ceLogRawGPSData.CheckedChanged += new System.EventHandler(this.ceLogRawGPSData_CheckedChanged);
            // 
            // cmbPortName
            // 
            this.cmbPortName.Location = new System.Drawing.Point(36, 29);
            this.cmbPortName.MenuManager = this.barManager1;
            this.cmbPortName.Name = "cmbPortName";
            this.cmbPortName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPortName.Size = new System.Drawing.Size(128, 20);
            this.cmbPortName.StyleController = this.layoutControl_Gps_Page1;
            this.cmbPortName.TabIndex = 2;
            // 
            // cmbBaudRate
            // 
            this.cmbBaudRate.Location = new System.Drawing.Point(227, 29);
            this.cmbBaudRate.MenuManager = this.barManager1;
            this.cmbBaudRate.Name = "cmbBaudRate";
            this.cmbBaudRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBaudRate.Properties.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "28800",
            "36000",
            "38400",
            "115000"});
            this.cmbBaudRate.Size = new System.Drawing.Size(94, 20);
            this.cmbBaudRate.StyleController = this.layoutControl_Gps_Page1;
            this.cmbBaudRate.TabIndex = 3;
            // 
            // ceGps_PlotWithTimer
            // 
            this.ceGps_PlotWithTimer.Enabled = false;
            this.ceGps_PlotWithTimer.Location = new System.Drawing.Point(7, 76);
            this.ceGps_PlotWithTimer.MenuManager = this.barManager1;
            this.ceGps_PlotWithTimer.Name = "ceGps_PlotWithTimer";
            this.ceGps_PlotWithTimer.Properties.Caption = "Plot Using Timer";
            this.ceGps_PlotWithTimer.Size = new System.Drawing.Size(135, 19);
            this.ceGps_PlotWithTimer.StyleController = this.layoutControl_Gps_Page1;
            this.ceGps_PlotWithTimer.TabIndex = 4;
            this.ceGps_PlotWithTimer.CheckedChanged += new System.EventHandler(this.ceGps_PlotWithTimer_CheckedChanged);
            // 
            // seGps_PlotTimerInterval
            // 
            this.seGps_PlotTimerInterval.EditValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.seGps_PlotTimerInterval.Enabled = false;
            this.seGps_PlotTimerInterval.Location = new System.Drawing.Point(220, 76);
            this.seGps_PlotTimerInterval.MenuManager = this.barManager1;
            this.seGps_PlotTimerInterval.Name = "seGps_PlotTimerInterval";
            this.seGps_PlotTimerInterval.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seGps_PlotTimerInterval.Properties.IsFloatValue = false;
            this.seGps_PlotTimerInterval.Properties.Mask.EditMask = "N00";
            this.seGps_PlotTimerInterval.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.seGps_PlotTimerInterval.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.seGps_PlotTimerInterval.Size = new System.Drawing.Size(101, 20);
            this.seGps_PlotTimerInterval.StyleController = this.layoutControl_Gps_Page1;
            superToolTip39.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem39.Text = "GPS Timer Interval - Information";
            toolTipItem39.LeftIndent = 6;
            superToolTip39.Items.Add(toolTipTitleItem39);
            superToolTip39.Items.Add(toolTipItem39);
            this.seGps_PlotTimerInterval.SuperTip = superToolTip39;
            this.seGps_PlotTimerInterval.TabIndex = 5;
            this.seGps_PlotTimerInterval.EditValueChanged += new System.EventHandler(this.seGps_PlotTimerInterval_EditValueChanged);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Root";
            this.layoutControlGroup12.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup12.GroupBordersVisible = false;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup13});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "Root";
            this.layoutControlGroup12.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup12.Size = new System.Drawing.Size(328, 103);
            this.layoutControlGroup12.TextVisible = false;
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "GPG Settings";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem6});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup13.Size = new System.Drawing.Size(328, 103);
            this.layoutControlGroup13.Text = "GPG Settings";
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AllowHtmlStringInCaption = true;
            this.layoutControlItem47.Control = this.cmbPortName;
            this.layoutControlItem47.CustomizationFormText = "Port:";
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem47.Text = "Port:";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(24, 13);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AllowHtmlStringInCaption = true;
            this.layoutControlItem48.Control = this.cmbBaudRate;
            this.layoutControlItem48.CustomizationFormText = "Baud Rate:";
            this.layoutControlItem48.Location = new System.Drawing.Point(161, 0);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(157, 24);
            this.layoutControlItem48.Text = "Baud Rate:";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.ceGps_PlotWithTimer;
            this.layoutControlItem49.CustomizationFormText = "layoutControlItem49";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 47);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(139, 24);
            this.layoutControlItem49.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem49.TextVisible = false;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.seGps_PlotTimerInterval;
            this.layoutControlItem50.CustomizationFormText = "Timer Interval:";
            this.layoutControlItem50.Location = new System.Drawing.Point(139, 47);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(179, 24);
            this.layoutControlItem50.Text = "Timer Interval:";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.ceLogRawGPSData;
            this.layoutControlItem6.CustomizationFormText = "Log Raw GPS Data";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(318, 23);
            this.layoutControlItem6.Text = "Log Raw GPS Data";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.btnGPS_PlotStart);
            this.groupControl2.Controls.Add(this.btnGPS_PlotStop);
            this.groupControl2.Controls.Add(this.ceGPS_PlotLine);
            this.groupControl2.Controls.Add(this.btnGPS_Plot);
            this.groupControl2.Controls.Add(this.ceGPS_PlotPolygon);
            this.groupControl2.Controls.Add(this.ceGPS_PlotPoint);
            this.groupControl2.Enabled = false;
            this.groupControl2.Location = new System.Drawing.Point(91, 110);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(147, 103);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Plot";
            // 
            // btnGPS_PlotStart
            // 
            this.btnGPS_PlotStart.Location = new System.Drawing.Point(67, 25);
            this.btnGPS_PlotStart.Name = "btnGPS_PlotStart";
            this.btnGPS_PlotStart.Size = new System.Drawing.Size(75, 23);
            this.btnGPS_PlotStart.TabIndex = 4;
            this.btnGPS_PlotStart.Text = "Start";
            this.btnGPS_PlotStart.Click += new System.EventHandler(this.btnGPS_PlotStart_Click);
            // 
            // btnGPS_PlotStop
            // 
            this.btnGPS_PlotStop.Enabled = false;
            this.btnGPS_PlotStop.Location = new System.Drawing.Point(67, 75);
            this.btnGPS_PlotStop.Name = "btnGPS_PlotStop";
            this.btnGPS_PlotStop.Size = new System.Drawing.Size(75, 23);
            this.btnGPS_PlotStop.TabIndex = 3;
            this.btnGPS_PlotStop.Text = "End";
            this.btnGPS_PlotStop.Click += new System.EventHandler(this.btnGPS_PlotStop_Click);
            // 
            // ceGPS_PlotLine
            // 
            this.ceGPS_PlotLine.Location = new System.Drawing.Point(5, 77);
            this.ceGPS_PlotLine.MenuManager = this.barManager1;
            this.ceGPS_PlotLine.Name = "ceGPS_PlotLine";
            this.ceGPS_PlotLine.Properties.Caption = "Line";
            this.ceGPS_PlotLine.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ceGPS_PlotLine.Properties.RadioGroupIndex = 1;
            this.ceGPS_PlotLine.Size = new System.Drawing.Size(60, 19);
            this.ceGPS_PlotLine.TabIndex = 2;
            this.ceGPS_PlotLine.TabStop = false;
            this.ceGPS_PlotLine.CheckedChanged += new System.EventHandler(this.ceGPS_PlotLine_CheckedChanged);
            // 
            // btnGPS_Plot
            // 
            this.btnGPS_Plot.Location = new System.Drawing.Point(67, 50);
            this.btnGPS_Plot.Name = "btnGPS_Plot";
            this.btnGPS_Plot.Size = new System.Drawing.Size(75, 23);
            this.btnGPS_Plot.TabIndex = 2;
            this.btnGPS_Plot.Text = "Plot";
            this.btnGPS_Plot.Click += new System.EventHandler(this.btnGPS_Plot_Click);
            // 
            // ceGPS_PlotPolygon
            // 
            this.ceGPS_PlotPolygon.Location = new System.Drawing.Point(5, 52);
            this.ceGPS_PlotPolygon.MenuManager = this.barManager1;
            this.ceGPS_PlotPolygon.Name = "ceGPS_PlotPolygon";
            this.ceGPS_PlotPolygon.Properties.Caption = "Polygon";
            this.ceGPS_PlotPolygon.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ceGPS_PlotPolygon.Properties.RadioGroupIndex = 1;
            this.ceGPS_PlotPolygon.Size = new System.Drawing.Size(60, 19);
            this.ceGPS_PlotPolygon.TabIndex = 1;
            this.ceGPS_PlotPolygon.TabStop = false;
            this.ceGPS_PlotPolygon.CheckedChanged += new System.EventHandler(this.ceGPS_PlotPolygon_CheckedChanged);
            // 
            // ceGPS_PlotPoint
            // 
            this.ceGPS_PlotPoint.EditValue = true;
            this.ceGPS_PlotPoint.Location = new System.Drawing.Point(5, 27);
            this.ceGPS_PlotPoint.MenuManager = this.barManager1;
            this.ceGPS_PlotPoint.Name = "ceGPS_PlotPoint";
            this.ceGPS_PlotPoint.Properties.Caption = "Point";
            this.ceGPS_PlotPoint.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ceGPS_PlotPoint.Properties.RadioGroupIndex = 1;
            this.ceGPS_PlotPoint.Size = new System.Drawing.Size(60, 19);
            this.ceGPS_PlotPoint.TabIndex = 0;
            this.ceGPS_PlotPoint.CheckedChanged += new System.EventHandler(this.ceGPS_PlotPoint_CheckedChanged);
            // 
            // btnGPS_Start
            // 
            this.btnGPS_Start.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnGPS_Start.Appearance.Options.UseFont = true;
            this.btnGPS_Start.Location = new System.Drawing.Point(9, 110);
            this.btnGPS_Start.Name = "btnGPS_Start";
            this.btnGPS_Start.Size = new System.Drawing.Size(75, 23);
            this.btnGPS_Start.TabIndex = 0;
            this.btnGPS_Start.Text = "Start GPS";
            this.btnGPS_Start.Click += new System.EventHandler(this.btnGPS_Start_Click);
            // 
            // tabGPRMC
            // 
            this.tabGPRMC.Controls.Add(this.layoutControl_GPS_Page2);
            this.tabGPRMC.Name = "tabGPRMC";
            this.tabGPRMC.Size = new System.Drawing.Size(330, 581);
            this.tabGPRMC.Text = "GPRMC";
            // 
            // layoutControl_GPS_Page2
            // 
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCGridRef);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCPositionUTM);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCMagneticVariation);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCTimeOfFix);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCSpeed);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCCourse);
            this.layoutControl_GPS_Page2.Controls.Add(this.label5);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCPosition);
            this.layoutControl_GPS_Page2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_GPS_Page2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_GPS_Page2.Name = "layoutControl_GPS_Page2";
            this.layoutControl_GPS_Page2.Root = this.layoutControlGroup4;
            this.layoutControl_GPS_Page2.Size = new System.Drawing.Size(330, 581);
            this.layoutControl_GPS_Page2.TabIndex = 1;
            this.layoutControl_GPS_Page2.Text = "layoutControl5";
            // 
            // lbRMCGridRef
            // 
            this.lbRMCGridRef.Location = new System.Drawing.Point(97, 163);
            this.lbRMCGridRef.MenuManager = this.barManager1;
            this.lbRMCGridRef.Name = "lbRMCGridRef";
            this.lbRMCGridRef.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCGridRef, true);
            this.lbRMCGridRef.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCGridRef, optionsSpelling3);
            this.lbRMCGridRef.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCGridRef.TabIndex = 10;
            // 
            // lbRMCPositionUTM
            // 
            this.lbRMCPositionUTM.Location = new System.Drawing.Point(97, 139);
            this.lbRMCPositionUTM.MenuManager = this.barManager1;
            this.lbRMCPositionUTM.Name = "lbRMCPositionUTM";
            this.lbRMCPositionUTM.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCPositionUTM, true);
            this.lbRMCPositionUTM.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCPositionUTM, optionsSpelling4);
            this.lbRMCPositionUTM.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCPositionUTM.TabIndex = 9;
            // 
            // lbRMCMagneticVariation
            // 
            this.lbRMCMagneticVariation.Location = new System.Drawing.Point(97, 115);
            this.lbRMCMagneticVariation.MenuManager = this.barManager1;
            this.lbRMCMagneticVariation.Name = "lbRMCMagneticVariation";
            this.lbRMCMagneticVariation.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCMagneticVariation, true);
            this.lbRMCMagneticVariation.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCMagneticVariation, optionsSpelling5);
            this.lbRMCMagneticVariation.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCMagneticVariation.TabIndex = 8;
            // 
            // lbRMCTimeOfFix
            // 
            this.lbRMCTimeOfFix.Location = new System.Drawing.Point(97, 91);
            this.lbRMCTimeOfFix.MenuManager = this.barManager1;
            this.lbRMCTimeOfFix.Name = "lbRMCTimeOfFix";
            this.lbRMCTimeOfFix.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCTimeOfFix, true);
            this.lbRMCTimeOfFix.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCTimeOfFix, optionsSpelling6);
            this.lbRMCTimeOfFix.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCTimeOfFix.TabIndex = 7;
            // 
            // lbRMCSpeed
            // 
            this.lbRMCSpeed.Location = new System.Drawing.Point(97, 67);
            this.lbRMCSpeed.MenuManager = this.barManager1;
            this.lbRMCSpeed.Name = "lbRMCSpeed";
            this.lbRMCSpeed.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCSpeed, true);
            this.lbRMCSpeed.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCSpeed, optionsSpelling7);
            this.lbRMCSpeed.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCSpeed.TabIndex = 6;
            // 
            // lbRMCCourse
            // 
            this.lbRMCCourse.Location = new System.Drawing.Point(97, 43);
            this.lbRMCCourse.MenuManager = this.barManager1;
            this.lbRMCCourse.Name = "lbRMCCourse";
            this.lbRMCCourse.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCCourse, true);
            this.lbRMCCourse.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCCourse, optionsSpelling8);
            this.lbRMCCourse.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCCourse.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(2, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.StyleController = this.layoutControl_GPS_Page2;
            this.label5.TabIndex = 0;
            this.label5.Text = "GPS/Transit data";
            // 
            // lbRMCPosition
            // 
            this.lbRMCPosition.Location = new System.Drawing.Point(97, 19);
            this.lbRMCPosition.MenuManager = this.barManager1;
            this.lbRMCPosition.Name = "lbRMCPosition";
            this.lbRMCPosition.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCPosition, true);
            this.lbRMCPosition.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCPosition, optionsSpelling9);
            this.lbRMCPosition.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCPosition.TabIndex = 4;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem18,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem9,
            this.layoutControlItem46});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(330, 581);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lbRMCPosition;
            this.layoutControlItem4.CustomizationFormText = "Position:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem4.Text = "Position:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lbRMCCourse;
            this.layoutControlItem5.CustomizationFormText = "Course:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem5.Text = "Course:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.lbRMCSpeed;
            this.layoutControlItem18.CustomizationFormText = "Speed:";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem18.Text = "Speed:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.lbRMCTimeOfFix;
            this.layoutControlItem21.CustomizationFormText = "Time of Fix:";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem21.Text = "Time of Fix:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.lbRMCMagneticVariation;
            this.layoutControlItem22.CustomizationFormText = "Magnetic Variation:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem22.Text = "Magnetic Variation:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.lbRMCPositionUTM;
            this.layoutControlItem23.CustomizationFormText = "UTM Position:";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem23.Text = "UTM Position:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.label5;
            this.layoutControlItem9.CustomizationFormText = "Title:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(330, 17);
            this.layoutControlItem9.Text = "Title:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.lbRMCGridRef;
            this.layoutControlItem46.CustomizationFormText = "Grid Ref:";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 161);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(330, 420);
            this.layoutControlItem46.Text = "Grid Ref:";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(92, 13);
            // 
            // tabGPGGA
            // 
            this.tabGPGGA.Controls.Add(this.layoutControl_GPS_Page3);
            this.tabGPGGA.Name = "tabGPGGA";
            this.tabGPGGA.Size = new System.Drawing.Size(330, 581);
            this.tabGPGGA.Text = "GPGGA";
            // 
            // layoutControl_GPS_Page3
            // 
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGADGPSID);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGADGPSupdate);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAGeoidHeight);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAHDOP);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAAltitude);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGANoOfSats);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAFixQuality);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGATimeOfFix);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAPosition);
            this.layoutControl_GPS_Page3.Controls.Add(this.label12);
            this.layoutControl_GPS_Page3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_GPS_Page3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_GPS_Page3.Name = "layoutControl_GPS_Page3";
            this.layoutControl_GPS_Page3.Root = this.layoutControlGroup8;
            this.layoutControl_GPS_Page3.Size = new System.Drawing.Size(330, 581);
            this.layoutControl_GPS_Page3.TabIndex = 2;
            this.layoutControl_GPS_Page3.Text = "layoutControl5";
            // 
            // lbGGADGPSID
            // 
            this.lbGGADGPSID.Location = new System.Drawing.Point(93, 211);
            this.lbGGADGPSID.MenuManager = this.barManager1;
            this.lbGGADGPSID.Name = "lbGGADGPSID";
            this.lbGGADGPSID.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGADGPSID, true);
            this.lbGGADGPSID.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGADGPSID, optionsSpelling10);
            this.lbGGADGPSID.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGADGPSID.TabIndex = 12;
            // 
            // lbGGADGPSupdate
            // 
            this.lbGGADGPSupdate.Location = new System.Drawing.Point(93, 187);
            this.lbGGADGPSupdate.MenuManager = this.barManager1;
            this.lbGGADGPSupdate.Name = "lbGGADGPSupdate";
            this.lbGGADGPSupdate.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGADGPSupdate, true);
            this.lbGGADGPSupdate.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGADGPSupdate, optionsSpelling11);
            this.lbGGADGPSupdate.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGADGPSupdate.TabIndex = 11;
            // 
            // lbGGAGeoidHeight
            // 
            this.lbGGAGeoidHeight.Location = new System.Drawing.Point(93, 163);
            this.lbGGAGeoidHeight.MenuManager = this.barManager1;
            this.lbGGAGeoidHeight.Name = "lbGGAGeoidHeight";
            this.lbGGAGeoidHeight.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAGeoidHeight, true);
            this.lbGGAGeoidHeight.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAGeoidHeight, optionsSpelling12);
            this.lbGGAGeoidHeight.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAGeoidHeight.TabIndex = 10;
            // 
            // lbGGAHDOP
            // 
            this.lbGGAHDOP.Location = new System.Drawing.Point(93, 139);
            this.lbGGAHDOP.MenuManager = this.barManager1;
            this.lbGGAHDOP.Name = "lbGGAHDOP";
            this.lbGGAHDOP.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAHDOP, true);
            this.lbGGAHDOP.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAHDOP, optionsSpelling13);
            this.lbGGAHDOP.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAHDOP.TabIndex = 9;
            // 
            // lbGGAAltitude
            // 
            this.lbGGAAltitude.Location = new System.Drawing.Point(93, 115);
            this.lbGGAAltitude.MenuManager = this.barManager1;
            this.lbGGAAltitude.Name = "lbGGAAltitude";
            this.lbGGAAltitude.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAAltitude, true);
            this.lbGGAAltitude.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAAltitude, optionsSpelling14);
            this.lbGGAAltitude.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAAltitude.TabIndex = 8;
            // 
            // lbGGANoOfSats
            // 
            this.lbGGANoOfSats.Location = new System.Drawing.Point(93, 91);
            this.lbGGANoOfSats.MenuManager = this.barManager1;
            this.lbGGANoOfSats.Name = "lbGGANoOfSats";
            this.lbGGANoOfSats.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGANoOfSats, true);
            this.lbGGANoOfSats.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGANoOfSats, optionsSpelling15);
            this.lbGGANoOfSats.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGANoOfSats.TabIndex = 7;
            // 
            // lbGGAFixQuality
            // 
            this.lbGGAFixQuality.Location = new System.Drawing.Point(93, 67);
            this.lbGGAFixQuality.MenuManager = this.barManager1;
            this.lbGGAFixQuality.Name = "lbGGAFixQuality";
            this.lbGGAFixQuality.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAFixQuality, true);
            this.lbGGAFixQuality.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAFixQuality, optionsSpelling16);
            this.lbGGAFixQuality.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAFixQuality.TabIndex = 6;
            // 
            // lbGGATimeOfFix
            // 
            this.lbGGATimeOfFix.Location = new System.Drawing.Point(93, 43);
            this.lbGGATimeOfFix.MenuManager = this.barManager1;
            this.lbGGATimeOfFix.Name = "lbGGATimeOfFix";
            this.lbGGATimeOfFix.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGATimeOfFix, true);
            this.lbGGATimeOfFix.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGATimeOfFix, optionsSpelling17);
            this.lbGGATimeOfFix.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGATimeOfFix.TabIndex = 5;
            // 
            // lbGGAPosition
            // 
            this.lbGGAPosition.Location = new System.Drawing.Point(93, 19);
            this.lbGGAPosition.MenuManager = this.barManager1;
            this.lbGGAPosition.Name = "lbGGAPosition";
            this.lbGGAPosition.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAPosition, true);
            this.lbGGAPosition.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAPosition, optionsSpelling18);
            this.lbGGAPosition.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAPosition.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(2, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(195, 13);
            this.label12.StyleController = this.layoutControl_GPS_Page3;
            this.label12.TabIndex = 1;
            this.label12.Text = "Global Positioning System Fix Data";
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "layoutControlGroup8";
            this.layoutControlGroup8.GroupBordersVisible = false;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem36});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(330, 581);
            this.layoutControlGroup8.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.label12;
            this.layoutControlItem24.CustomizationFormText = "Title:";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(330, 17);
            this.layoutControlItem24.Text = "Title:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.lbGGAPosition;
            this.layoutControlItem28.CustomizationFormText = "Position:";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem28.Text = "Position:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.lbGGATimeOfFix;
            this.layoutControlItem29.CustomizationFormText = "Time of Fix:";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem29.Text = "Time of Fix:";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.lbGGAFixQuality;
            this.layoutControlItem30.CustomizationFormText = "Fix Quality:";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem30.Text = "Fix Quality:";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.lbGGANoOfSats;
            this.layoutControlItem31.CustomizationFormText = "Tracked Satellites:";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem31.Text = "Tracked Satellites:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.lbGGAAltitude;
            this.layoutControlItem32.CustomizationFormText = "Altitude:";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem32.Text = "Altitude:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.lbGGAHDOP;
            this.layoutControlItem33.CustomizationFormText = "HDOP:";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem33.Text = "HDOP:";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.lbGGAGeoidHeight;
            this.layoutControlItem34.CustomizationFormText = "Height of Geoid:";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 161);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem34.Text = "Height of Geoid:";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.lbGGADGPSupdate;
            this.layoutControlItem35.CustomizationFormText = "DGPS Update:";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 185);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem35.Text = "DGPS Update:";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.lbGGADGPSID;
            this.layoutControlItem36.CustomizationFormText = "DGPS Station ID:";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 209);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(330, 372);
            this.layoutControlItem36.Text = "DGPS Station ID:";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(88, 13);
            // 
            // tabGPGLL
            // 
            this.tabGPGLL.Controls.Add(this.layoutControl_GPS_Page4);
            this.tabGPGLL.Name = "tabGPGLL";
            this.tabGPGLL.Size = new System.Drawing.Size(330, 581);
            this.tabGPGLL.Text = "GPGLL";
            // 
            // layoutControl_GPS_Page4
            // 
            this.layoutControl_GPS_Page4.Controls.Add(this.lbGLLDataValid);
            this.layoutControl_GPS_Page4.Controls.Add(this.lbGLLTimeOfSolution);
            this.layoutControl_GPS_Page4.Controls.Add(this.lbGLLPosition);
            this.layoutControl_GPS_Page4.Controls.Add(this.label30);
            this.layoutControl_GPS_Page4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_GPS_Page4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_GPS_Page4.Name = "layoutControl_GPS_Page4";
            this.layoutControl_GPS_Page4.Root = this.layoutControlGroup10;
            this.layoutControl_GPS_Page4.Size = new System.Drawing.Size(330, 581);
            this.layoutControl_GPS_Page4.TabIndex = 3;
            this.layoutControl_GPS_Page4.Text = "layoutControl6";
            // 
            // lbGLLDataValid
            // 
            this.lbGLLDataValid.Location = new System.Drawing.Point(85, 67);
            this.lbGLLDataValid.MenuManager = this.barManager1;
            this.lbGLLDataValid.Name = "lbGLLDataValid";
            this.lbGLLDataValid.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGLLDataValid, true);
            this.lbGLLDataValid.Size = new System.Drawing.Size(243, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGLLDataValid, optionsSpelling19);
            this.lbGLLDataValid.StyleController = this.layoutControl_GPS_Page4;
            this.lbGLLDataValid.TabIndex = 6;
            // 
            // lbGLLTimeOfSolution
            // 
            this.lbGLLTimeOfSolution.Location = new System.Drawing.Point(85, 43);
            this.lbGLLTimeOfSolution.MenuManager = this.barManager1;
            this.lbGLLTimeOfSolution.Name = "lbGLLTimeOfSolution";
            this.lbGLLTimeOfSolution.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGLLTimeOfSolution, true);
            this.lbGLLTimeOfSolution.Size = new System.Drawing.Size(243, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGLLTimeOfSolution, optionsSpelling20);
            this.lbGLLTimeOfSolution.StyleController = this.layoutControl_GPS_Page4;
            this.lbGLLTimeOfSolution.TabIndex = 5;
            // 
            // lbGLLPosition
            // 
            this.lbGLLPosition.Location = new System.Drawing.Point(85, 19);
            this.lbGLLPosition.MenuManager = this.barManager1;
            this.lbGLLPosition.Name = "lbGLLPosition";
            this.lbGLLPosition.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGLLPosition, true);
            this.lbGLLPosition.Size = new System.Drawing.Size(243, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGLLPosition, optionsSpelling21);
            this.lbGLLPosition.StyleController = this.layoutControl_GPS_Page4;
            this.lbGLLPosition.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(2, 2);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(112, 13);
            this.label30.StyleController = this.layoutControl_GPS_Page4;
            this.label30.TabIndex = 2;
            this.label30.Text = "Geographic Position";
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10";
            this.layoutControlGroup10.GroupBordersVisible = false;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(330, 581);
            this.layoutControlGroup10.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.label30;
            this.layoutControlItem26.CustomizationFormText = "Title:";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(330, 17);
            this.layoutControlItem26.Text = "Title:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.lbGLLPosition;
            this.layoutControlItem37.CustomizationFormText = "Position:";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem37.Text = "Position:";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.lbGLLTimeOfSolution;
            this.layoutControlItem38.CustomizationFormText = "Time of Solution:";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem38.Text = "Time of Solution:";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.lbGLLDataValid;
            this.layoutControlItem39.CustomizationFormText = "Data Valid:";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(330, 516);
            this.layoutControlItem39.Text = "Data Valid:";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(80, 13);
            // 
            // tabGPGSA
            // 
            this.tabGPGSA.Controls.Add(this.layoutControl_GPS_Page5);
            this.tabGPGSA.Name = "tabGPGSA";
            this.tabGPGSA.Size = new System.Drawing.Size(330, 581);
            this.tabGPGSA.Text = "GPGSA";
            // 
            // layoutControl_GPS_Page5
            // 
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAVDOP);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAHDOP);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAPDOP);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAPRNs);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAFixMode);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAMode);
            this.layoutControl_GPS_Page5.Controls.Add(this.label32);
            this.layoutControl_GPS_Page5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_GPS_Page5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_GPS_Page5.Name = "layoutControl_GPS_Page5";
            this.layoutControl_GPS_Page5.Root = this.layoutControlGroup11;
            this.layoutControl_GPS_Page5.Size = new System.Drawing.Size(330, 581);
            this.layoutControl_GPS_Page5.TabIndex = 4;
            this.layoutControl_GPS_Page5.Text = "layoutControl7";
            // 
            // lbGSAVDOP
            // 
            this.lbGSAVDOP.Location = new System.Drawing.Point(86, 139);
            this.lbGSAVDOP.MenuManager = this.barManager1;
            this.lbGSAVDOP.Name = "lbGSAVDOP";
            this.lbGSAVDOP.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAVDOP, true);
            this.lbGSAVDOP.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAVDOP, optionsSpelling22);
            this.lbGSAVDOP.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAVDOP.TabIndex = 9;
            // 
            // lbGSAHDOP
            // 
            this.lbGSAHDOP.Location = new System.Drawing.Point(86, 115);
            this.lbGSAHDOP.MenuManager = this.barManager1;
            this.lbGSAHDOP.Name = "lbGSAHDOP";
            this.lbGSAHDOP.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAHDOP, true);
            this.lbGSAHDOP.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAHDOP, optionsSpelling23);
            this.lbGSAHDOP.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAHDOP.TabIndex = 8;
            // 
            // lbGSAPDOP
            // 
            this.lbGSAPDOP.Location = new System.Drawing.Point(86, 91);
            this.lbGSAPDOP.MenuManager = this.barManager1;
            this.lbGSAPDOP.Name = "lbGSAPDOP";
            this.lbGSAPDOP.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAPDOP, true);
            this.lbGSAPDOP.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAPDOP, optionsSpelling24);
            this.lbGSAPDOP.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAPDOP.TabIndex = 7;
            // 
            // lbGSAPRNs
            // 
            this.lbGSAPRNs.Location = new System.Drawing.Point(86, 67);
            this.lbGSAPRNs.MenuManager = this.barManager1;
            this.lbGSAPRNs.Name = "lbGSAPRNs";
            this.lbGSAPRNs.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAPRNs, true);
            this.lbGSAPRNs.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAPRNs, optionsSpelling25);
            this.lbGSAPRNs.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAPRNs.TabIndex = 6;
            // 
            // lbGSAFixMode
            // 
            this.lbGSAFixMode.Location = new System.Drawing.Point(86, 43);
            this.lbGSAFixMode.MenuManager = this.barManager1;
            this.lbGSAFixMode.Name = "lbGSAFixMode";
            this.lbGSAFixMode.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAFixMode, true);
            this.lbGSAFixMode.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAFixMode, optionsSpelling26);
            this.lbGSAFixMode.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAFixMode.TabIndex = 5;
            // 
            // lbGSAMode
            // 
            this.lbGSAMode.Location = new System.Drawing.Point(86, 19);
            this.lbGSAMode.MenuManager = this.barManager1;
            this.lbGSAMode.Name = "lbGSAMode";
            this.lbGSAMode.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAMode, true);
            this.lbGSAMode.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAMode, optionsSpelling27);
            this.lbGSAMode.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAMode.TabIndex = 4;
            // 
            // label32
            // 
            this.label32.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(2, 2);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(161, 13);
            this.label32.StyleController = this.layoutControl_GPS_Page5;
            this.label32.TabIndex = 3;
            this.label32.Text = "GPS DOP and Active Satellite";
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "layoutControlGroup11";
            this.layoutControlGroup11.GroupBordersVisible = false;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem27,
            this.layoutControlItem40,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(330, 581);
            this.layoutControlGroup11.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.label32;
            this.layoutControlItem27.CustomizationFormText = "Title:";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(330, 17);
            this.layoutControlItem27.Text = "Title:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.lbGSAMode;
            this.layoutControlItem40.CustomizationFormText = "Mode:";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem40.Text = "Mode:";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.lbGSAFixMode;
            this.layoutControlItem41.CustomizationFormText = "Fix Mode:";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem41.Text = "Fix Mode:";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.lbGSAPRNs;
            this.layoutControlItem42.CustomizationFormText = "PRNs in Solution:";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem42.Text = "PRNs in Solution:";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.lbGSAPDOP;
            this.layoutControlItem43.CustomizationFormText = "PDOP:";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem43.Text = "PDOP:";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.lbGSAHDOP;
            this.layoutControlItem44.CustomizationFormText = "HDOP:";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem44.Text = "HDOP:";
            this.layoutControlItem44.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.lbGSAVDOP;
            this.layoutControlItem45.CustomizationFormText = "VDOP:";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(330, 444);
            this.layoutControlItem45.Text = "VDOP:";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(81, 13);
            // 
            // tabGPGSV
            // 
            this.tabGPGSV.Controls.Add(this.picGSVSignals);
            this.tabGPGSV.Controls.Add(this.picGSVSkyview);
            this.tabGPGSV.Controls.Add(this.label33);
            this.tabGPGSV.Name = "tabGPGSV";
            this.tabGPGSV.Size = new System.Drawing.Size(330, 581);
            this.tabGPGSV.Text = "GPGSV";
            // 
            // picGSVSignals
            // 
            this.picGSVSignals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picGSVSignals.Location = new System.Drawing.Point(2, 194);
            this.picGSVSignals.Name = "picGSVSignals";
            this.picGSVSignals.Size = new System.Drawing.Size(326, 385);
            this.picGSVSignals.TabIndex = 6;
            this.picGSVSignals.TabStop = false;
            // 
            // picGSVSkyview
            // 
            this.picGSVSkyview.Location = new System.Drawing.Point(2, 18);
            this.picGSVSkyview.Name = "picGSVSkyview";
            this.picGSVSkyview.Size = new System.Drawing.Size(170, 170);
            this.picGSVSkyview.TabIndex = 5;
            this.picGSVSkyview.TabStop = false;
            // 
            // label33
            // 
            this.label33.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(3, 3);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(95, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Satellites in View";
            // 
            // tabRaw
            // 
            this.tabRaw.Controls.Add(this.tbRawLog);
            this.tabRaw.Name = "tabRaw";
            this.tabRaw.Size = new System.Drawing.Size(330, 581);
            this.tabRaw.Text = "Raw";
            // 
            // tbRawLog
            // 
            this.tbRawLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRawLog.Location = new System.Drawing.Point(0, 0);
            this.tbRawLog.MenuManager = this.barManager1;
            this.tbRawLog.Name = "tbRawLog";
            this.tbRawLog.Properties.BeforeShowMenu += new DevExpress.XtraEditors.Controls.BeforeShowMenuEventHandler(this.tbRawLog_Properties_BeforeShowMenu);
            this.scSpellChecker.SetShowSpellCheckMenu(this.tbRawLog, true);
            this.tbRawLog.Size = new System.Drawing.Size(330, 581);
            this.scSpellChecker.SetSpellCheckerOptions(this.tbRawLog, optionsSpelling28);
            this.tbRawLog.TabIndex = 7;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "symbol_circle_16.png");
            this.imageList1.Images.SetKeyName(1, "symbol_square_16.png");
            this.imageList1.Images.SetKeyName(2, "symbol_diamond_16.png");
            this.imageList1.Images.SetKeyName(3, "symbol_triangle1_16.png");
            this.imageList1.Images.SetKeyName(4, "symbol_triangle2_16.png");
            this.imageList1.Images.SetKeyName(5, "symbol_star_16.png");
            this.imageList1.Images.SetKeyName(6, "symbol_hollow_circle_16.png");
            this.imageList1.Images.SetKeyName(7, "symbol_hollow_square_16.png");
            this.imageList1.Images.SetKeyName(8, "symbol_hollow_diamond_16.png");
            this.imageList1.Images.SetKeyName(9, "symbol_hollow_triangle1_16.png");
            this.imageList1.Images.SetKeyName(10, "symbol_hollow_triangle2_16.png");
            this.imageList1.Images.SetKeyName(11, "symbol_hollow_star_16.png");
            // 
            // imageList4
            // 
            this.imageList4.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList4.ImageStream")));
            this.imageList4.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList4.Images.SetKeyName(0, "pattern_02.png");
            this.imageList4.Images.SetKeyName(1, "pattern_03.png");
            this.imageList4.Images.SetKeyName(2, "pattern_04.png");
            this.imageList4.Images.SetKeyName(3, "pattern_06.png");
            this.imageList4.Images.SetKeyName(4, "pattern_08.png");
            this.imageList4.Images.SetKeyName(5, "pattern_14.png");
            this.imageList4.Images.SetKeyName(6, "pattern_17.png");
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "Border_01.png");
            this.imageList3.Images.SetKeyName(1, "Border_02.png");
            this.imageList3.Images.SetKeyName(2, "Border_03.png");
            this.imageList3.Images.SetKeyName(3, "Border_04.png");
            this.imageList3.Images.SetKeyName(4, "Border_05.png");
            this.imageList3.Images.SetKeyName(5, "Border_06.png");
            this.imageList3.Images.SetKeyName(6, "Border_07.png");
            // 
            // imageListLineStyles
            // 
            this.imageListLineStyles.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLineStyles.ImageStream")));
            this.imageListLineStyles.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLineStyles.Images.SetKeyName(0, "line_02.png");
            this.imageListLineStyles.Images.SetKeyName(1, "line_06.png");
            this.imageListLineStyles.Images.SetKeyName(2, "line_10.png");
            this.imageListLineStyles.Images.SetKeyName(3, "line_41.png");
            this.imageListLineStyles.Images.SetKeyName(4, "line_54.png");
            this.imageListLineStyles.Images.SetKeyName(5, "line_63.png");
            this.imageListLineStyles.Images.SetKeyName(6, "line_64.png");
            this.imageListLineStyles.Images.SetKeyName(7, "line_73.png");
            this.imageListLineStyles.Images.SetKeyName(8, "line_93.png");
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.popupContainerControlAssetTypes);
            this.panelControl1.Controls.Add(this.popupContainerControlTreeRefStructure);
            this.panelControl1.Controls.Add(this.popupContainerControl2);
            this.panelControl1.Controls.Add(this.mapControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(340, 70);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1040, 658);
            this.panelControl1.TabIndex = 26;
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 48;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // beiEditMode
            // 
            this.beiEditMode.Caption = "Edit Mode";
            this.beiEditMode.Edit = this.repositoryItemLookUpEdit2;
            this.beiEditMode.Id = 49;
            this.beiEditMode.Name = "beiEditMode";
            // 
            // bsiEditMode
            // 
            this.bsiEditMode.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiEditMode.Caption = "Edit Mode: Move and Resize";
            this.bsiEditMode.Id = 55;
            this.bsiEditMode.Name = "bsiEditMode";
            this.bsiEditMode.Size = new System.Drawing.Size(150, 0);
            this.bsiEditMode.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiEditMode.Width = 150;
            // 
            // bar3
            // 
            this.bar3.BarName = "Edit Mode";
            this.bar3.DockCol = 3;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditingNone),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditingMove),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditingAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditingEdit)});
            this.bar3.OptionsBar.DisableClose = true;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.Text = "Map Edit Mode";
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 5";
            this.bar4.DockCol = 6;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiWorkOrderMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveImage)});
            this.bar4.OptionsBar.DisableClose = true;
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.Text = "Map Output";
            // 
            // bbiWorkOrderMap
            // 
            this.bbiWorkOrderMap.Caption = "WO Map";
            this.bbiWorkOrderMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiWorkOrderMap.Glyph")));
            this.bbiWorkOrderMap.Id = 82;
            this.bbiWorkOrderMap.Name = "bbiWorkOrderMap";
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Text = "Work Order Maps - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to open the Work Order Maps screen.\r\n\r\nThe Work Order Maps screen is use" +
    "d to create and link saved maps to work orders.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiWorkOrderMap.SuperTip = superToolTip9;
            this.bbiWorkOrderMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiWorkOrderMap_ItemClick);
            // 
            // bbiPrint
            // 
            this.bbiPrint.Caption = "Print Map";
            this.bbiPrint.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirectLarge;
            this.bbiPrint.Id = 73;
            this.bbiPrint.Name = "bbiPrint";
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Text = "Print Map - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to open the Print Map screen.\r\n";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.bbiPrint.SuperTip = superToolTip10;
            this.bbiPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrint_ItemClick);
            // 
            // bbiSaveImage
            // 
            this.bbiSaveImage.Caption = "Save Map as Image";
            this.bbiSaveImage.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportFileLarge;
            this.bbiSaveImage.Id = 56;
            this.bbiSaveImage.Name = "bbiSaveImage";
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.Text = "Export Loaded Map Objects to TAB File - Information";
            toolTipItem11.LeftIndent = 6;
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.bbiSaveImage.SuperTip = superToolTip11;
            this.bbiSaveImage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // sp01252_AT_Tree_Picker_load_object_managerTableAdapter
            // 
            this.sp01252_AT_Tree_Picker_load_object_managerTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiHighlightYes, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiHighlightNo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopyHighlightFromVisible),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVisibleYes, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVisibleNo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiDataset, true)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.MenuCaption = "Map Objects Menu";
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.ShowCaption = true;
            // 
            // bbiViewOnMap
            // 
            this.bbiViewOnMap.Caption = "View Object on Map";
            this.bbiViewOnMap.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ScaleLarge;
            this.bbiViewOnMap.Id = 59;
            this.bbiViewOnMap.Name = "bbiViewOnMap";
            superToolTip40.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem40.Text = "View on Map - Information";
            toolTipItem40.LeftIndent = 6;
            toolTipItem40.Text = "Click me to view the current object on the map.\r\n\r\nIf the object is not within th" +
    "e viewed area of the map, the map will automatically pan to show the object.";
            superToolTip40.Items.Add(toolTipTitleItem40);
            superToolTip40.Items.Add(toolTipItem40);
            this.bbiViewOnMap.SuperTip = superToolTip40;
            this.bbiViewOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewOnMap_ItemClick);
            // 
            // bbiHighlightYes
            // 
            this.bbiHighlightYes.Caption = "Highlight Selected Objects";
            this.bbiHighlightYes.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiHighlightYes.Glyph")));
            this.bbiHighlightYes.Id = 68;
            this.bbiHighlightYes.Name = "bbiHighlightYes";
            this.bbiHighlightYes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiHighlightYes_ItemClick);
            // 
            // bbiHighlightNo
            // 
            this.bbiHighlightNo.Caption = "Un-Highlight Selected Objects";
            this.bbiHighlightNo.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiHighlightNo.Glyph")));
            this.bbiHighlightNo.Id = 69;
            this.bbiHighlightNo.Name = "bbiHighlightNo";
            this.bbiHighlightNo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiHighlightNo_ItemClick);
            // 
            // bbiCopyHighlightFromVisible
            // 
            this.bbiCopyHighlightFromVisible.Caption = "Copy Highlight from Visible Settings";
            this.bbiCopyHighlightFromVisible.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCopyHighlightFromVisible.Glyph")));
            this.bbiCopyHighlightFromVisible.Id = 70;
            this.bbiCopyHighlightFromVisible.Name = "bbiCopyHighlightFromVisible";
            superToolTip41.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem41.Text = "Copy Highlight from Visible Settings - Information";
            toolTipItem41.LeftIndent = 6;
            toolTipItem41.Text = "Click me to <b>Copy</b> the Map Object <b>Visibility</b> settings to the Map Obje" +
    "ct <b>Highlight</b> settings.";
            superToolTip41.Items.Add(toolTipTitleItem41);
            superToolTip41.Items.Add(toolTipItem41);
            this.bbiCopyHighlightFromVisible.SuperTip = superToolTip41;
            this.bbiCopyHighlightFromVisible.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopyHighlightFromVisible_ItemClick);
            // 
            // bbiVisibleYes
            // 
            this.bbiVisibleYes.Caption = "Set Selected Objects Visible";
            this.bbiVisibleYes.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiVisibleYes.Glyph")));
            this.bbiVisibleYes.Id = 71;
            this.bbiVisibleYes.Name = "bbiVisibleYes";
            this.bbiVisibleYes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiVisibleYes_ItemClick);
            // 
            // bbiVisibleNo
            // 
            this.bbiVisibleNo.Caption = "Set Selected Objects Hidden";
            this.bbiVisibleNo.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiVisibleNo.Glyph")));
            this.bbiVisibleNo.Id = 72;
            this.bbiVisibleNo.Name = "bbiVisibleNo";
            this.bbiVisibleNo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiVisibleNo_ItemClick);
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // sp01254_AT_Tree_Picker_scale_listTableAdapter
            // 
            this.sp01254_AT_Tree_Picker_scale_listTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.labelControl1);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup9;
            this.layoutControl5.Size = new System.Drawing.Size(272, 212);
            this.layoutControl5.TabIndex = 2;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(2, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(195, 13);
            this.labelControl1.StyleController = this.layoutControl5;
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Global Positioning System Fix Data";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup8";
            this.layoutControlGroup9.GroupBordersVisible = false;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup8";
            this.layoutControlGroup9.Size = new System.Drawing.Size(272, 212);
            this.layoutControlGroup9.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.labelControl1;
            this.layoutControlItem25.CustomizationFormText = "Title:";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem25.Name = "layoutControlItem24";
            this.layoutControlItem25.Size = new System.Drawing.Size(272, 212);
            this.layoutControlItem25.Text = "Title:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // gridLookUpEdit3View
            // 
            this.gridLookUpEdit3View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35});
            this.gridLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit3View.GroupCount = 1;
            this.gridLookUpEdit3View.Name = "gridLookUpEdit3View";
            this.gridLookUpEdit3View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit3View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit3View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit3View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit3View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit3View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit3View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit3View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn17
            // 
            this.gridColumn17.FieldName = "ColumnID";
            this.gridColumn17.Name = "gridColumn17";
            // 
            // gridColumn18
            // 
            this.gridColumn18.FieldName = "InternalColumnName";
            this.gridColumn18.Name = "gridColumn18";
            // 
            // gridColumn19
            // 
            this.gridColumn19.FieldName = "ExternalColumnName";
            this.gridColumn19.Name = "gridColumn19";
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Style Field  [Screen Label]";
            this.gridColumn20.FieldName = "ColumnLabel";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            this.gridColumn20.Width = 277;
            // 
            // gridColumn21
            // 
            this.gridColumn21.FieldName = "ColumnType";
            this.gridColumn21.Name = "gridColumn21";
            // 
            // gridColumn22
            // 
            this.gridColumn22.FieldName = "ColumnLength";
            this.gridColumn22.Name = "gridColumn22";
            // 
            // gridColumn23
            // 
            this.gridColumn23.FieldName = "GroupName";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            this.gridColumn23.Width = 127;
            // 
            // gridColumn24
            // 
            this.gridColumn24.FieldName = "TableName";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn25
            // 
            this.gridColumn25.FieldName = "ColumnOrder";
            this.gridColumn25.Name = "gridColumn25";
            // 
            // gridColumn26
            // 
            this.gridColumn26.FieldName = "PickList";
            this.gridColumn26.Name = "gridColumn26";
            // 
            // gridColumn27
            // 
            this.gridColumn27.FieldName = "PicklistSummery";
            this.gridColumn27.Name = "gridColumn27";
            // 
            // gridColumn28
            // 
            this.gridColumn28.FieldName = "Remarks";
            this.gridColumn28.Name = "gridColumn28";
            // 
            // gridColumn29
            // 
            this.gridColumn29.FieldName = "Required";
            this.gridColumn29.Name = "gridColumn29";
            // 
            // gridColumn30
            // 
            this.gridColumn30.FieldName = "GroupOrder";
            this.gridColumn30.Name = "gridColumn30";
            // 
            // gridColumn31
            // 
            this.gridColumn31.FieldName = "Editable";
            this.gridColumn31.Name = "gridColumn31";
            // 
            // gridColumn32
            // 
            this.gridColumn32.FieldName = "Searchable";
            this.gridColumn32.Name = "gridColumn32";
            // 
            // gridColumn33
            // 
            this.gridColumn33.FieldName = "Manditory";
            this.gridColumn33.Name = "gridColumn33";
            // 
            // gridColumn34
            // 
            this.gridColumn34.FieldName = "UseLastRecordAvailable";
            this.gridColumn34.Name = "gridColumn34";
            // 
            // gridColumn35
            // 
            this.gridColumn35.FieldName = "UseLastRecord";
            this.gridColumn35.Name = "gridColumn35";
            // 
            // popupMenu_ThematicGrid
            // 
            this.popupMenu_ThematicGrid.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEditThematicStyles),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveThematicSet, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveThematicSetAs)});
            this.popupMenu_ThematicGrid.Manager = this.barManager1;
            this.popupMenu_ThematicGrid.Name = "popupMenu_ThematicGrid";
            // 
            // bbiBlockEditThematicStyles
            // 
            this.bbiBlockEditThematicStyles.Caption = "Block Edit Thematic Styles";
            this.bbiBlockEditThematicStyles.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditThematicStyles.Glyph")));
            this.bbiBlockEditThematicStyles.Id = 78;
            this.bbiBlockEditThematicStyles.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditThematicStyles.LargeGlyph")));
            this.bbiBlockEditThematicStyles.Name = "bbiBlockEditThematicStyles";
            // 
            // bbiSaveThematicSet
            // 
            this.bbiSaveThematicSet.Caption = "Save Thematic Set";
            this.bbiSaveThematicSet.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSet.Glyph")));
            this.bbiSaveThematicSet.Id = 79;
            this.bbiSaveThematicSet.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSet.LargeGlyph")));
            this.bbiSaveThematicSet.Name = "bbiSaveThematicSet";
            // 
            // bbiSaveThematicSetAs
            // 
            this.bbiSaveThematicSetAs.Caption = "Save Thematic Set As";
            this.bbiSaveThematicSetAs.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSetAs.Glyph")));
            this.bbiSaveThematicSetAs.Id = 80;
            this.bbiSaveThematicSetAs.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSetAs.LargeGlyph")));
            this.bbiSaveThematicSetAs.Name = "bbiSaveThematicSetAs";
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList2.Images.SetKeyName(0, "");
            this.imageList2.Images.SetKeyName(1, "");
            this.imageList2.Images.SetKeyName(2, "");
            this.imageList2.Images.SetKeyName(3, "footer_16.png");
            this.imageList2.Images.SetKeyName(4, "expand_and_select_16.png");
            this.imageList2.Images.SetKeyName(5, "colour_expression_16.png");
            this.imageList2.Images.SetKeyName(6, "");
            this.imageList2.Images.SetKeyName(7, "");
            this.imageList2.Images.SetKeyName(8, "");
            this.imageList2.Images.SetKeyName(9, "");
            this.imageList2.Images.SetKeyName(10, "");
            this.imageList2.Images.SetKeyName(11, "");
            this.imageList2.Images.SetKeyName(12, "");
            this.imageList2.Images.SetKeyName(13, "");
            // 
            // popupMenu_LayerManager
            // 
            this.popupMenu_LayerManager.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddLayer),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRemoveLayer),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLayerManagerProperties, true)});
            this.popupMenu_LayerManager.Manager = this.barManager1;
            this.popupMenu_LayerManager.MenuCaption = "Layer Manager Menu";
            this.popupMenu_LayerManager.Name = "popupMenu_LayerManager";
            this.popupMenu_LayerManager.ShowCaption = true;
            // 
            // bbiAddLayer
            // 
            this.bbiAddLayer.Caption = "Add Layer";
            this.bbiAddLayer.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddLayer.Glyph")));
            this.bbiAddLayer.Id = 83;
            this.bbiAddLayer.Name = "bbiAddLayer";
            toolTipTitleItem43.Text = "Add Layer - Information";
            toolTipItem43.LeftIndent = 6;
            toolTipItem43.Text = "Add a layer temporarily to the map.\r\n\r\n<b>Tip:</b> To add the layer permanently, " +
    "edit the Map Workspace and add it there.";
            superToolTip43.Items.Add(toolTipTitleItem43);
            superToolTip43.Items.Add(toolTipItem43);
            this.bbiAddLayer.SuperTip = superToolTip43;
            this.bbiAddLayer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddLayer_ItemClick);
            // 
            // bbiRemoveLayer
            // 
            this.bbiRemoveLayer.Caption = "Remove Layer";
            this.bbiRemoveLayer.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRemoveLayer.Glyph")));
            this.bbiRemoveLayer.Id = 84;
            this.bbiRemoveLayer.Name = "bbiRemoveLayer";
            toolTipTitleItem44.Text = "Remove Layer - Information";
            toolTipItem44.LeftIndent = 6;
            toolTipItem44.Text = "Remove a layer temporarily from the map.\r\n\r\n<b>Tip:</b> To remove the layer perma" +
    "nently, edit the Map Workspace and remove it from there.";
            superToolTip44.Items.Add(toolTipTitleItem44);
            superToolTip44.Items.Add(toolTipItem44);
            this.bbiRemoveLayer.SuperTip = superToolTip44;
            this.bbiRemoveLayer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRemoveLayer_ItemClick);
            // 
            // bbiLayerManagerProperties
            // 
            this.bbiLayerManagerProperties.Caption = "Properties";
            this.bbiLayerManagerProperties.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLayerManagerProperties.Glyph")));
            this.bbiLayerManagerProperties.Id = 81;
            this.bbiLayerManagerProperties.Name = "bbiLayerManagerProperties";
            toolTipTitleItem42.Text = "Layer Properties - Information";
            toolTipItem42.LeftIndent = 6;
            superToolTip42.Items.Add(toolTipTitleItem42);
            superToolTip42.Items.Add(toolTipItem42);
            this.bbiLayerManagerProperties.SuperTip = superToolTip42;
            this.bbiLayerManagerProperties.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayerManagerProperties_ItemClick);
            // 
            // sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter
            // 
            this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp01274ATTreePickerWorkspaceEditBindingSource
            // 
            this.sp01274ATTreePickerWorkspaceEditBindingSource.DataMember = "sp01274_AT_Tree_Picker_Workspace_Edit";
            this.sp01274ATTreePickerWorkspaceEditBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // sp01274_AT_Tree_Picker_Workspace_EditTableAdapter
            // 
            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageListLineStylesLegend
            // 
            this.imageListLineStylesLegend.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLineStylesLegend.ImageStream")));
            this.imageListLineStylesLegend.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLineStylesLegend.Images.SetKeyName(0, "line_02.png");
            this.imageListLineStylesLegend.Images.SetKeyName(1, "line_06.png");
            this.imageListLineStylesLegend.Images.SetKeyName(2, "line_10.png");
            this.imageListLineStylesLegend.Images.SetKeyName(3, "line_41.png");
            this.imageListLineStylesLegend.Images.SetKeyName(4, "line_54.png");
            this.imageListLineStylesLegend.Images.SetKeyName(5, "line_63.png");
            this.imageListLineStylesLegend.Images.SetKeyName(6, "line_64.png");
            this.imageListLineStylesLegend.Images.SetKeyName(7, "line_73.png");
            this.imageListLineStylesLegend.Images.SetKeyName(8, "line_93.png");
            // 
            // bar5
            // 
            this.bar5.BarName = "Custom 6";
            this.bar5.DockCol = 5;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciGazetteer),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLegend),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciScaleBar),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciLinkedInspections)});
            this.bar5.OptionsBar.DisableClose = true;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.Text = "Map Adornments";
            // 
            // bciGazetteer
            // 
            this.bciGazetteer.Caption = "Gazetteer";
            this.bciGazetteer.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.bciGazetteer.Id = 92;
            this.bciGazetteer.Name = "bciGazetteer";
            superToolTip12.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem12.Text = "Gazetteer - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to Show \\ Hide the Gazetteer. \r\n\r\nThe Gazetteer is used to search for Lo" +
    "calities and Street Addresses on the Map.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.bciGazetteer.SuperTip = superToolTip12;
            this.bciGazetteer.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciGazetteer_CheckedChanged);
            // 
            // bbiLegend
            // 
            this.bbiLegend.Caption = "Legend";
            this.bbiLegend.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLegend.Glyph")));
            this.bbiLegend.Id = 88;
            this.bbiLegend.Name = "bbiLegend";
            superToolTip13.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem13.Text = "Legend - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to Show \\ Hide the Map Legend.\r\n";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.bbiLegend.SuperTip = superToolTip13;
            this.bbiLegend.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLegend_CheckedChanged);
            // 
            // bciScaleBar
            // 
            this.bciScaleBar.Caption = "Scale Bar";
            this.bciScaleBar.Glyph = ((System.Drawing.Image)(resources.GetObject("bciScaleBar.Glyph")));
            this.bciScaleBar.Id = 89;
            this.bciScaleBar.Name = "bciScaleBar";
            superToolTip14.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem14.Text = "Map Scale Bar - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to Show \\ Hide the Map Scale Bar.\r\n";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.bciScaleBar.SuperTip = superToolTip14;
            this.bciScaleBar.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciScaleBar_CheckedChanged);
            // 
            // bciLinkedInspections
            // 
            this.bciLinkedInspections.Caption = "barCheckItem2";
            this.bciLinkedInspections.Glyph = ((System.Drawing.Image)(resources.GetObject("bciLinkedInspections.Glyph")));
            this.bciLinkedInspections.Id = 128;
            this.bciLinkedInspections.Name = "bciLinkedInspections";
            toolTipTitleItem15.Text = "Inspection and Actions - Information";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Click me to display the Inspections and Actions Panel.\r\n\r\nThe Panel shows Inspect" +
    "ion and Action data linked to the currently selected Map Objects.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.bciLinkedInspections.SuperTip = superToolTip15;
            this.bciLinkedInspections.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciLinkedInspections_CheckedChanged);
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter
            // 
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter
            // 
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // bbiGPSSaveLog
            // 
            this.bbiGPSSaveLog.Caption = "Save Log...";
            this.bbiGPSSaveLog.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiGPSSaveLog.Glyph")));
            this.bbiGPSSaveLog.Id = 99;
            this.bbiGPSSaveLog.Name = "bbiGPSSaveLog";
            this.bbiGPSSaveLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGPSSaveLog_ItemClick);
            // 
            // bbiGPSClearLog
            // 
            this.bbiGPSClearLog.Caption = "Clear Log";
            this.bbiGPSClearLog.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiGPSClearLog.Glyph")));
            this.bbiGPSClearLog.Id = 100;
            this.bbiGPSClearLog.Name = "bbiGPSClearLog";
            this.bbiGPSClearLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGPSClearLog_ItemClick);
            // 
            // imageListGPSLogMenu
            // 
            this.imageListGPSLogMenu.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListGPSLogMenu.ImageStream")));
            this.imageListGPSLogMenu.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListGPSLogMenu.Images.SetKeyName(0, "save_as_32.png");
            this.imageListGPSLogMenu.Images.SetKeyName(1, "delete_32.png");
            // 
            // popupMenu_Gazetteer
            // 
            this.popupMenu_Gazetteer.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerShowMatch),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateIncident),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiGazetteerLoadObjectsWithinRange, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerLoadMapObjectsInRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerClearMapSearch, true)});
            this.popupMenu_Gazetteer.Manager = this.barManager1;
            this.popupMenu_Gazetteer.MenuCaption = "Gazetteer Menu";
            this.popupMenu_Gazetteer.Name = "popupMenu_Gazetteer";
            this.popupMenu_Gazetteer.ShowCaption = true;
            // 
            // bbiGazetteerShowMatch
            // 
            this.bbiGazetteerShowMatch.Caption = "Show Selected Match on Map";
            this.bbiGazetteerShowMatch.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ScaleLarge;
            this.bbiGazetteerShowMatch.Id = 101;
            this.bbiGazetteerShowMatch.Name = "bbiGazetteerShowMatch";
            this.bbiGazetteerShowMatch.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGazetteerShowMatch_ItemClick);
            // 
            // bbiGazetteerLoadMapObjectsInRange
            // 
            this.bbiGazetteerLoadMapObjectsInRange.Caption = "Load Map Objects in Range";
            this.bbiGazetteerLoadMapObjectsInRange.Id = 105;
            this.bbiGazetteerLoadMapObjectsInRange.Name = "bbiGazetteerLoadMapObjectsInRange";
            this.bbiGazetteerLoadMapObjectsInRange.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGazetteerLoadMapObjectsInRange_ItemClick);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Load Map Objects Within";
            this.barEditItem1.Edit = this.repositoryItemPopupContainerEdit3;
            this.barEditItem1.Id = 104;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemPopupContainerEdit3
            // 
            this.repositoryItemPopupContainerEdit3.AutoHeight = false;
            this.repositoryItemPopupContainerEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit3.Name = "repositoryItemPopupContainerEdit3";
            // 
            // popupStoredMapViews
            // 
            this.popupStoredMapViews.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciStoreViewChanges),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiGoToMapView),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiStoreCurrentMapView),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearAllMapViews, true)});
            this.popupStoredMapViews.Manager = this.barManager1;
            this.popupStoredMapViews.MenuCaption = "Map Views Menu";
            this.popupStoredMapViews.Name = "popupStoredMapViews";
            this.popupStoredMapViews.ShowCaption = true;
            this.popupStoredMapViews.Popup += new System.EventHandler(this.popupStoredMapViews_Popup);
            // 
            // bciStoreViewChanges
            // 
            this.bciStoreViewChanges.BindableChecked = true;
            this.bciStoreViewChanges.Caption = "Store Map View Changes";
            this.bciStoreViewChanges.Checked = true;
            this.bciStoreViewChanges.Id = 106;
            this.bciStoreViewChanges.Name = "bciStoreViewChanges";
            this.bciStoreViewChanges.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciStoreViewChanges_CheckedChanged);
            // 
            // bsiGoToMapView
            // 
            this.bsiGoToMapView.Caption = "Go To Map View";
            this.bsiGoToMapView.Id = 110;
            this.bsiGoToMapView.Name = "bsiGoToMapView";
            // 
            // bbiStoreCurrentMapView
            // 
            this.bbiStoreCurrentMapView.Caption = "Store Current Map View";
            this.bbiStoreCurrentMapView.Id = 108;
            this.bbiStoreCurrentMapView.Name = "bbiStoreCurrentMapView";
            this.bbiStoreCurrentMapView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStoreCurrentMapView_ItemClick);
            // 
            // bbiClearAllMapViews
            // 
            this.bbiClearAllMapViews.Caption = "Clear All Stored Map Views";
            this.bbiClearAllMapViews.Id = 107;
            this.bbiClearAllMapViews.Name = "bbiClearAllMapViews";
            this.bbiClearAllMapViews.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearAllMapViews_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 109;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // bar6
            // 
            this.bar6.BarName = "Map Views";
            this.bar6.DockCol = 0;
            this.bar6.DockRow = 0;
            this.bar6.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar6.FloatLocation = new System.Drawing.Point(580, 173);
            this.bar6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiStoredMapViews),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLastMapView, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiNextMapView),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiZoomIn, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiZoomOut),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPan, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCentre, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiNone, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard)});
            this.bar6.OptionsBar.DisableClose = true;
            this.bar6.OptionsBar.DrawDragBorder = false;
            this.bar6.Text = "Map Views";
            // 
            // bbiStoredMapViews
            // 
            this.bbiStoredMapViews.ActAsDropDown = true;
            this.bbiStoredMapViews.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiStoredMapViews.Caption = "Stored Views";
            this.bbiStoredMapViews.DropDownControl = this.popupStoredMapViews;
            this.bbiStoredMapViews.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiStoredMapViews.Glyph")));
            this.bbiStoredMapViews.Id = 115;
            this.bbiStoredMapViews.Name = "bbiStoredMapViews";
            superToolTip16.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem16.Text = "Stored Views - Information";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Click me to display the Stored Views Menu.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            this.bbiStoredMapViews.SuperTip = superToolTip16;
            // 
            // bbiLastMapView
            // 
            this.bbiLastMapView.Caption = "Previous View";
            this.bbiLastMapView.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLastMapView.Glyph")));
            this.bbiLastMapView.Id = 111;
            this.bbiLastMapView.Name = "bbiLastMapView";
            superToolTip17.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem17.Text = "Previous Map View - Information";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Click me to navigate to the previous stored Map view.";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            this.bbiLastMapView.SuperTip = superToolTip17;
            this.bbiLastMapView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLastMapView_ItemClick);
            // 
            // bbiNextMapView
            // 
            this.bbiNextMapView.Caption = "Next View";
            this.bbiNextMapView.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiNextMapView.Glyph")));
            this.bbiNextMapView.Id = 113;
            this.bbiNextMapView.Name = "bbiNextMapView";
            superToolTip18.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem18.Text = "Next Map View - Information";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Click me to navigate to the next stored Map view.";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            this.bbiNextMapView.SuperTip = superToolTip18;
            this.bbiNextMapView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNextMapView_ItemClick);
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Edit...";
            this.bbiEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEdit.Glyph")));
            this.bbiEdit.Id = 123;
            this.bbiEdit.Name = "bbiEdit";
            // 
            // bsiEditObjects
            // 
            this.bsiEditObjects.Caption = "Edit...";
            this.bsiEditObjects.Id = 124;
            this.bsiEditObjects.Name = "bsiEditObjects";
            // 
            // sp01367_AT_Inspections_For_TreesTableAdapter
            // 
            this.sp01367_AT_Inspections_For_TreesTableAdapter.ClearBeforeFill = true;
            // 
            // sp01380_AT_Actions_Linked_To_InspectionsTableAdapter
            // 
            this.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.ClearBeforeFill = true;
            // 
            // bbiBlockAddActionsToInspections
            // 
            this.bbiBlockAddActionsToInspections.Caption = "Add Action to Selected Record(s)";
            this.bbiBlockAddActionsToInspections.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiBlockAddActionsToInspections.Glyph")));
            this.bbiBlockAddActionsToInspections.Id = 132;
            this.bbiBlockAddActionsToInspections.Name = "bbiBlockAddActionsToInspections";
            this.bbiBlockAddActionsToInspections.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockAddActionsToInspections_ItemClick);
            // 
            // barCheckItem2
            // 
            this.barCheckItem2.Caption = "barCheckItem2";
            this.barCheckItem2.Id = 137;
            this.barCheckItem2.Name = "barCheckItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Clear Search Results";
            this.barButtonItem3.Id = 138;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // sp03068_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter
            // 
            this.sp03068_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp03069_Tree_Picker_Asset_Objects_ListTableAdapter
            // 
            this.sp03069_Tree_Picker_Asset_Objects_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp03021_EP_Asset_Type_Filter_DropDownTableAdapter
            // 
            this.sp03021_EP_Asset_Type_Filter_DropDownTableAdapter.ClearBeforeFill = true;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // bar7
            // 
            this.bar7.BarName = "ObbjectSelection";
            this.bar7.DockCol = 2;
            this.bar7.DockRow = 0;
            this.bar7.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar7.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSelect, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectRectangle),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectRadius),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectRegion),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectPolygon)});
            this.bar7.OptionsBar.DrawDragBorder = false;
            this.bar7.Text = "Object Selection";
            // 
            // bar8
            // 
            this.bar8.BarName = "MapTools";
            this.bar8.DockCol = 4;
            this.bar8.DockRow = 0;
            this.bar8.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar8.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiMapSelection),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiQueryTool, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiMeasureLine, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciMapMarkerAdd, true)});
            this.bar8.OptionsBar.DrawDragBorder = false;
            this.bar8.Text = "Map Tools";
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlendingx
            // 
            this.xtraGridBlendingx.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingx.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingx.GridControl = this.gridControlAssetObjects;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // xtraGridBlending7
            // 
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending7.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending7.GridControl = this.gridControl7;
            // 
            // frm_AT_Mapping_Tree_Picker
            // 
            this.ClientSize = new System.Drawing.Size(1380, 756);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.mapToolBar1);
            this.Controls.Add(this.panelContainer1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Mapping_Tree_Picker";
            this.Text = "Mapping";
            this.Activated += new System.EventHandler(this.frm_AT_Mapping_Tree_Picker_Activated);
            this.Deactivate += new System.EventHandler(this.frm_AT_Mapping_Tree_Picker_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Mapping_Tree_Picker_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_AT_Mapping_Tree_Picker_FormClosed);
            this.Load += new System.EventHandler(this.frm_AT_Mapping_Tree_Picker_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frm_AT_Mapping_Tree_Picker_MouseClick);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.panelContainer1, 0);
            this.Controls.SetChildIndex(this.mapToolBar1, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01312ATTreePickerGazetteerSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03068TreePickerPlottingObjectTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_MapControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditGazetteerRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.seUserDefinedScale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).EndInit();
            this.gridSplitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01254ATTreePickerscalelistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            this.dockPanel5.ResumeLayout(false);
            this.dockPanel5_Container.ResumeLayout(false);
            this.xtraScrollableControl1.ResumeLayout(false);
            this.xtraScrollableControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLegend)).EndInit();
            this.dockPanel6.ResumeLayout(false);
            this.dockPanel6_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxScaleBar)).EndInit();
            this.dockPanelGazetteer.ResumeLayout(false);
            this.dockPanel7_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditGazetteerFindValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditGazetteerSearchType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01311ATTreePickerGazetteerSearchTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGazetteerMatchPattern.Properties)).EndInit();
            this.dockPanelInspections.ResumeLayout(false);
            this.controlContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01367ATInspectionsForTreesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01380ATActionsLinkedToInspectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            this.panelContainer1.ResumeLayout(false);
            this.dockPanel2.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditAssetTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlAssetTypes)).EndInit();
            this.popupContainerControlAssetTypes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).EndInit();
            this.gridSplitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAssetTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03021EPAssetTypeFilterDropDownBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAssetTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAssetObjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03069TreePickerAssetObjectsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAssetObjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditHighlight2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditTreeRefStructure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTreeRefStructure)).EndInit();
            this.popupContainerControlTreeRefStructure.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTreeRefRemovePreceedingChars.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTreeRefRemoveNumberOfChars.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.csScaleMapForHighlighted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCentreMap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01252ATTreePickerloadobjectmanagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditHighlight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSyncSelection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditHighlight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            this.dockPanel4.ResumeLayout(false);
            this.dockPanel4_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).EndInit();
            this.layoutControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01288ATTreePickerWorkspacelayerslistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCE_LayerVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCE_LayerHitable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beWorkspace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            this.dockPanel3.ResumeLayout(false);
            this.dockPanel3_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statusBar1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NMEAtabs)).EndInit();
            this.NMEAtabs.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Gps_Page1)).EndInit();
            this.layoutControl_Gps_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ceLogRawGPSData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPortName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBaudRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGps_PlotWithTimer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seGps_PlotTimerInterval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotLine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotPolygon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotPoint.Properties)).EndInit();
            this.tabGPRMC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page2)).EndInit();
            this.layoutControl_GPS_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCGridRef.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCPositionUTM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCMagneticVariation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCTimeOfFix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCCourse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            this.tabGPGGA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page3)).EndInit();
            this.layoutControl_GPS_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbGGADGPSID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGADGPSupdate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAGeoidHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAHDOP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAAltitude.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGANoOfSats.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAFixQuality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGATimeOfFix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            this.tabGPGLL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page4)).EndInit();
            this.layoutControl_GPS_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLDataValid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLTimeOfSolution.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            this.tabGPGSA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page5)).EndInit();
            this.layoutControl_GPS_Page5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAVDOP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAHDOP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAPDOP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAPRNs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAFixMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            this.tabGPGSV.ResumeLayout(false);
            this.tabGPGSV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picGSVSignals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGSVSkyview)).EndInit();
            this.tabRaw.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbRawLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit3View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_ThematicGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_LayerManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01274ATTreePickerWorkspaceEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_Gazetteer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupStoredMapViews)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MapInfo.Windows.Controls.MapToolBar mapToolBar1;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonOpenTable;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonLayerControl;
        private System.Windows.Forms.ToolBarButton toolBarButtonSperator1;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonArrow;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonZoomIn;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonZoomOut;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelectRectangle;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonCenter;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonPan;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelect;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelectRadius;
        private System.Windows.Forms.ToolBarButton toolBarButtonSeperator2;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelectPolygon;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelectRegion;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonAddPoint;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonAddPolygon;
        private System.Windows.Forms.ToolBarButton toolBarButtonSeperator3;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonAddPolygon2;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonAddPolyLine;
        private DevExpress.XtraBars.Bar bar1;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButton1;
        private DevExpress.XtraBars.BarCheckItem bbiZoomIn;
        private DevExpress.XtraBars.BarCheckItem bbiZoomOut;
        private DevExpress.XtraBars.BarCheckItem bbiPan;
        private DevExpress.XtraBars.BarCheckItem bbiCentre;
        private DevExpress.XtraBars.BarCheckItem bbiSelect;
        private DevExpress.XtraBars.BarCheckItem bbiSelectRectangle;
        private DevExpress.XtraBars.BarCheckItem bbiSelectRadius;
        private DevExpress.XtraBars.BarCheckItem bbiSelectPolygon;
        private DevExpress.XtraBars.BarCheckItem bbiSelectRegion;
        private DevExpress.XtraBars.BarCheckItem bbiAddPoint;
        private DevExpress.XtraBars.BarCheckItem bbiAddPolygon;
        private DevExpress.XtraBars.BarCheckItem bbiAddPolyLine;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarStaticItem bsiZoom;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarStaticItem bsiScreenCoords;
        private DevExpress.XtraBars.BarStaticItem bsiLayerCount;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private MapInfo.Windows.Controls.MapControl mapControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraBars.BarStaticItem bsiSnap;
        private DevExpress.XtraBars.BarCheckItem bbiNone;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarCheckItem bciEditingNone;
        private DevExpress.XtraBars.BarCheckItem bciEditingMove;
        private DevExpress.XtraBars.BarEditItem beiEditMode;
        private DevExpress.XtraBars.BarCheckItem bciEditingAdd;
        private DevExpress.XtraBars.BarCheckItem bciEditingEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraBars.BarStaticItem bsiEditMode;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem bbiSaveImage;
        private DataSet_AT_TreePicker dataSet_AT_TreePicker;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource sp01252ATTreePickerloadobjectmanagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01252_AT_Tree_Picker_load_object_managerTableAdapter sp01252_AT_Tree_Picker_load_object_managerTableAdapter;
        private DevExpress.XtraEditors.ColorEdit colorEditHighlight;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.CheckEdit ceCentreMap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem bbiViewOnMap;
        private DevExpress.XtraEditors.SimpleButton btnViewFullExtent;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraBars.BarEditItem beiPopupContainerScale;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.SimpleButton btnSetScale;
        private DevExpress.XtraEditors.SpinEdit seUserDefinedScale;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private System.Windows.Forms.BindingSource sp01254ATTreePickerscalelistBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01254_AT_Tree_Picker_scale_listTableAdapter sp01254_AT_Tree_Picker_scale_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colScaleDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colScale;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarCheckItem bbiMeasureLine;
        private DevExpress.XtraBars.BarStaticItem bsiDistanceMeasured;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel3;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraTab.XtraTabControl NMEAtabs;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage tabGPRMC;
        private DevExpress.XtraTab.XtraTabPage tabGPGGA;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        public DevExpress.XtraEditors.SimpleButton btnGPS_Start;
        private DevExpress.XtraEditors.CheckEdit ceGPS_PlotPoint;
        private DevExpress.XtraEditors.CheckEdit ceGPS_PlotLine;
        private DevExpress.XtraEditors.CheckEdit ceGPS_PlotPolygon;
        private DevExpress.XtraEditors.SimpleButton btnGPS_Plot;
        private DevExpress.XtraEditors.SimpleButton btnGPS_PlotStop;
        private DevExpress.XtraTab.XtraTabPage tabGPGLL;
        private DevExpress.XtraTab.XtraTabPage tabGPGSA;
        private DevExpress.XtraTab.XtraTabPage tabGPGSV;
        private DevExpress.XtraTab.XtraTabPage tabRaw;
        private DevExpress.XtraEditors.LabelControl label5;
        private DevExpress.XtraEditors.LabelControl label12;
        private DevExpress.XtraEditors.LabelControl label30;
        private DevExpress.XtraEditors.LabelControl label32;
        private DevExpress.XtraEditors.LabelControl label33;
        private DevExpress.XtraEditors.MemoEdit tbRawLog;
        private DevExpress.XtraLayout.LayoutControl layoutControl_GPS_Page2;
        private DevExpress.XtraEditors.TextEdit lbRMCCourse;
        private DevExpress.XtraEditors.TextEdit lbRMCPosition;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit lbRMCPositionUTM;
        private DevExpress.XtraEditors.TextEdit lbRMCMagneticVariation;
        private DevExpress.XtraEditors.TextEdit lbRMCTimeOfFix;
        private DevExpress.XtraEditors.TextEdit lbRMCSpeed;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControl layoutControl_GPS_Page3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControl layoutControl_GPS_Page4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControl layoutControl_GPS_Page5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.TextEdit lbGGAPosition;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraEditors.TextEdit lbGGAHDOP;
        private DevExpress.XtraEditors.TextEdit lbGGAAltitude;
        private DevExpress.XtraEditors.TextEdit lbGGANoOfSats;
        private DevExpress.XtraEditors.TextEdit lbGGAFixQuality;
        private DevExpress.XtraEditors.TextEdit lbGGATimeOfFix;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraEditors.TextEdit lbGGAGeoidHeight;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraEditors.TextEdit lbGGADGPSID;
        private DevExpress.XtraEditors.TextEdit lbGGADGPSupdate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.TextEdit lbGLLPosition;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraEditors.TextEdit lbGLLTimeOfSolution;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraEditors.TextEdit lbGLLDataValid;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraEditors.TextEdit lbGSAVDOP;
        private DevExpress.XtraEditors.TextEdit lbGSAHDOP;
        private DevExpress.XtraEditors.TextEdit lbGSAPDOP;
        private DevExpress.XtraEditors.TextEdit lbGSAPRNs;
        private DevExpress.XtraEditors.TextEdit lbGSAFixMode;
        private DevExpress.XtraEditors.TextEdit lbGSAMode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private System.Windows.Forms.PictureBox picGSVSignals;
        private System.Windows.Forms.PictureBox picGSVSkyview;
        private DevExpress.XtraEditors.TextEdit statusBar1;
        private DevExpress.XtraEditors.TextEdit lbRMCGridRef;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPortName;
        private DevExpress.XtraEditors.ComboBoxEdit cmbBaudRate;
        private DevExpress.XtraLayout.LayoutControl layoutControl_Gps_Page1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraEditors.SimpleButton btnGPS_PlotStart;
        private DevExpress.XtraEditors.CheckEdit ceGps_PlotWithTimer;
        private DevExpress.XtraEditors.SpinEdit seGps_PlotTimerInterval;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraEditors.SimpleButton btnGps_PauseTimer;
        private DevExpress.XtraEditors.LabelControl labelPlottingUsingTimer;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditHighlight;
        private DevExpress.XtraEditors.CheckEdit csScaleMapForHighlighted;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraBars.BarButtonItem bbiHighlightYes;
        private DevExpress.XtraBars.BarButtonItem bbiHighlightNo;
        private DevExpress.XtraBars.BarButtonItem bbiCopyHighlightFromVisible;
        private DevExpress.XtraBars.BarButtonItem bbiVisibleYes;
        private DevExpress.XtraBars.BarButtonItem bbiVisibleNo;
        private DevExpress.XtraEditors.SimpleButton btnRefreshMapObjects;
        private DevExpress.XtraBars.BarButtonItem bbiPrint;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit3View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private System.Windows.Forms.ImageList imageList4;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ImageList imageListLineStyles;
        private DevExpress.XtraBars.BarButtonItem bbiBlockEditThematicStyles;
        private DevExpress.XtraBars.BarButtonItem bbiSaveThematicSet;
        private DevExpress.XtraBars.BarButtonItem bbiSaveThematicSetAs;
        private DevExpress.XtraBars.PopupMenu popupMenu_ThematicGrid;
        private System.Windows.Forms.ImageList imageList2;
        private DevExpress.XtraBars.BarButtonItem bbiLayerManagerProperties;
        private DevExpress.XtraBars.PopupMenu popupMenu_LayerManager;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel4;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel4_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl6;
        private DevExpress.XtraEditors.ButtonEdit beWorkspace;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkspaceID;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerType;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerPath;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerVisible;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCE_LayerVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerHitable;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCE_LayerHitable;
        private DevExpress.XtraGrid.Columns.GridColumn colPreserveDefaultStyling;
        private DevExpress.XtraGrid.Columns.GridColumn colTransparency;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colUseThematicStyling;
        private DevExpress.XtraGrid.Columns.GridColumn colThematicStyleSet;
        private DevExpress.XtraGrid.Columns.GridColumn colPointSize;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private System.Windows.Forms.BindingSource sp01288ATTreePickerWorkspacelayerslistBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerName1;
        private System.Windows.Forms.BindingSource sp01274ATTreePickerWorkspaceEditBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter sp01274_AT_Tree_Picker_Workspace_EditTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colVisibility;
        private DevExpress.XtraGrid.Columns.GridColumn colContext;
        private DevExpress.XtraGrid.Columns.GridColumn colManagement;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroundType;
        private DevExpress.XtraGrid.Columns.GridColumn colDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colProtectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteHazardClass;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colMapLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget1;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget2;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget3;
        private DevExpress.XtraGrid.Columns.GridColumn colLastModifiedDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colAgeClass;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaHa;
        private DevExpress.XtraGrid.Columns.GridColumn colReplantCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeType;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeLength;
        private DevExpress.XtraGrid.Columns.GridColumn colGardenSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPropertyProximity;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSituation;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colcolor;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedInspectionCount;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colHighlight;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colintAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colintAgeClass;
        private DevExpress.XtraGrid.Columns.GridColumn colintDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colintDistrictID;
        private DevExpress.XtraGrid.Columns.GridColumn colintGroundType;
        private DevExpress.XtraGrid.Columns.GridColumn colintHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colintLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colintLocalityID;
        private DevExpress.XtraGrid.Columns.GridColumn colintNearbyObject1;
        private DevExpress.XtraGrid.Columns.GridColumn colintNearbyObject2;
        private DevExpress.XtraGrid.Columns.GridColumn colintNearbyObject3;
        private DevExpress.XtraGrid.Columns.GridColumn colintOwnershipID;
        private DevExpress.XtraGrid.Columns.GridColumn colintProtectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colintSafetyPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colintSiteHazardClass;
        private DevExpress.XtraGrid.Columns.GridColumn colintSize;
        private DevExpress.XtraGrid.Columns.GridColumn colintSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colintStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colintVisibility;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraBars.BarButtonItem bbiWorkOrderMap;
        private DevExpress.XtraBars.BarButtonItem bbiAddLayer;
        private DevExpress.XtraBars.BarButtonItem bbiRemoveLayer;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButton2;
        private DevExpress.XtraBars.BarCheckItem bbiQueryTool;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyLayerPathForSave;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel5;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel5_Container;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        public System.Windows.Forms.PictureBox pictureBoxLegend;  // Must be set as Public for child form to see //
        private System.Windows.Forms.ImageList imageListLineStylesLegend;
        private DevExpress.XtraBars.Bar bar5;
        public DevExpress.XtraBars.BarCheckItem bbiLegend;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel6;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel6_Container;
        private System.Windows.Forms.PictureBox pictureBoxScaleBar;
        private DevExpress.XtraBars.BarCheckItem bciScaleBar;
        private DevExpress.XtraEditors.RadioGroup radioGroupGazetteerMatchPattern;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditGazetteerSearchType;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditGazetteerFindValue;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private System.Windows.Forms.BindingSource sp01311ATTreePickerGazetteerSearchTypesBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.BarCheckItem bciGazetteer;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelGazetteer;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel7_Container;
        private System.Windows.Forms.BindingSource sp01312ATTreePickerGazetteerSearchBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate1;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate1;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraBars.PopupMenu popupMenu_MapControl1;
        private DevExpress.XtraBars.BarButtonItem bbiEditMapObjects;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteMapObjects;
        private DevExpress.XtraBars.BarButtonItem bbiCreateDatasetFromMapObjects;
        private DevExpress.XtraEditors.SimpleButton btnSelectHighlighted;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraBars.BarButtonItem bbiTransferToWorkOrder;
        private DevExpress.XtraBars.BarButtonItem bbiGPSSaveLog;
        private DevExpress.XtraBars.BarButtonItem bbiGPSClearLog;
        private System.Windows.Forms.ImageList imageListGPSLogMenu;
        private DevExpress.XtraEditors.CheckEdit ceLogRawGPSData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraBars.BarButtonItem bbiGazetteerShowMatch;
        private DevExpress.XtraBars.PopupMenu popupMenu_Gazetteer;
        private DevExpress.XtraBars.BarSubItem barSubItemMapEdit;
        private DevExpress.XtraBars.BarEditItem beiGazetteerLoadObjectsWithinRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditGazetteerRange;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit3;
        private DevExpress.XtraBars.BarButtonItem bbiGazetteerLoadMapObjectsInRange;
        private DevExpress.XtraBars.BarCheckItem bciStoreViewChanges;
        private DevExpress.XtraBars.BarButtonItem bbiClearAllMapViews;
        private DevExpress.XtraBars.BarButtonItem bbiStoreCurrentMapView;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarSubItem bsiGoToMapView;
        private DevExpress.XtraBars.PopupMenu popupStoredMapViews;
        private DevExpress.XtraBars.Bar bar6;
        private DevExpress.XtraBars.BarButtonItem bbiLastMapView;
        private DevExpress.XtraBars.BarButtonItem bbiMapSelection;
        private DevExpress.XtraBars.BarButtonItem bbiNextMapView;
        private DevExpress.XtraBars.BarButtonItem bbiStoredMapViews;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlTreeRefStructure;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditTreeRef1;
        private DevExpress.XtraEditors.CheckEdit checkEditTreeRef3;
        private DevExpress.XtraEditors.CheckEdit checkEditTreeRef2;
        private DevExpress.XtraEditors.TextEdit textEditTreeRefRemovePreceedingChars;
        private DevExpress.XtraEditors.SpinEdit spinEditTreeRefRemoveNumberOfChars;
        private DevExpress.XtraEditors.SimpleButton btnSetTreeRefStruc;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditTreeRefStructure;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraBars.BarButtonItem bbiGazetteerClearMapSearch;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteSelectedTempMapObjects;
        private DevExpress.XtraBars.BarCheckItem bciMapMarkerAdd;
        private DevExpress.XtraBars.BarButtonItem bbiCreatePolygonFromMapMarkers;
        private DevExpress.XtraBars.BarButtonItem bbiCreatePolylineFromMapMarkers;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteAllMapMarkers;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraEditors.SimpleButton btnClearMapObjects;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.CheckEdit checkEditSyncSelection;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarSubItem bsiEditObjects;
        private DevExpress.XtraBars.BarButtonItem bbiAddInspection;
        private DevExpress.XtraBars.BarButtonItem bbiAddAction;
        private DevExpress.XtraBars.BarSubItem bsiAddToMapObjects;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelInspections;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer2;
        private DevExpress.XtraBars.BarCheckItem bciLinkedInspections;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private System.Windows.Forms.BindingSource sp01367ATInspectionsForTreesBindingSource;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityID1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityName1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeX;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeY;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionInspector;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionIncidentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionIncidentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionAngleToVertical;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionLastModified;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionGeneralCondition;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionVitality;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedOutstandingActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colNoFurtherActionRequired;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01367_AT_Inspections_For_TreesTableAdapter sp01367_AT_Inspections_For_TreesTableAdapter;
        private System.Windows.Forms.BindingSource sp01380ATActionsLinkedToInspectionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colActionOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudget;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostDifference;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTimeliness;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter sp01380_AT_Actions_Linked_To_InspectionsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private System.Windows.Forms.ImageList imageList5;
        private DevExpress.XtraBars.BarButtonItem bbiEditSelectedMapObjects;
        private DevExpress.XtraBars.BarButtonItem bbiBlockEditselectedMapObjects;
        private DevExpress.XtraBars.BarSubItem bsiEditMapObject;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountRate;
        private DevExpress.XtraBars.BarButtonItem bbiBlockAddActionsToInspections;
        private DevExpress.XtraBars.BarButtonItem bbiCreateIncident;
        private DevExpress.XtraBars.BarButtonItem bbiCreateIncidentFromMap;
        private DevExpress.XtraBars.BarSubItem bsiMapNearbyObjects;
        private DevExpress.XtraBars.BarButtonItem bbiNearbyObjectsFind;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarCheckItem barCheckItem2;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionElapsedDays;
        private DevExpress.XtraBars.BarEditItem beiPlotObjectType;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private System.Windows.Forms.BindingSource sp03068TreePickerPlottingObjectTypesWithBlankBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp03068_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter sp03068_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlAssetObjects;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAssetObjects;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private System.Windows.Forms.BindingSource sp03069TreePickerAssetObjectsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate2;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate2;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY1;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colModelNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLifeSpanValue;
        private DevExpress.XtraGrid.Columns.GridColumn colLifeSpanValueDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentConditionID;
        private DevExpress.XtraGrid.Columns.GridColumn colConditionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLastVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNextVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID1;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType1;
        private DevExpress.XtraGrid.Columns.GridColumn colHighlight1;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp03069_Tree_Picker_Asset_Objects_ListTableAdapter sp03069_Tree_Picker_Asset_Objects_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditHighlight2;
        private DataSet_EP dataSet_EP;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditAssetTypes;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlAssetTypes;
        private DevExpress.XtraGrid.GridControl gridControlAssetTypes;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAssetTypes;
        private DevExpress.XtraEditors.SimpleButton btnOkAssetTypes;
        private System.Windows.Forms.BindingSource sp03021EPAssetTypeFilterDropDownBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private WoodPlan5.DataSet_EPTableAdapters.sp03021_EP_Asset_Type_Filter_DropDownTableAdapter sp03021_EP_Asset_Type_Filter_DropDownTableAdapter;
        private DevExpress.XtraBars.BarSubItem bsiSetCentrePoint;
        private DevExpress.XtraBars.BarButtonItem bbiSetClientCentre;
        private DevExpress.XtraBars.BarButtonItem bbiSetSiteCentre;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer7;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer6;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.XtraBars.Bar bar7;
        private DevExpress.XtraBars.Bar bar8;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSiteFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingx;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending7;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSiteFilter2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;  // Must be set as Public for child form to see //
    }
}

namespace WoodPlan5
{
    partial class frm_AT_Work_Order_Add_Actions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Work_Order_Add_Actions));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditDateFrom = new DevExpress.XtraEditors.DateEdit();
            this.dateEditDateTo = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01405ATWorkOrderActionsAvailableForAddingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPrevActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrNearestHouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPriorityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintJobRateBudgeted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonJobRateBudgeted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colmonJobWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colmonBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintJobRateActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualJobRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonJobRateActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonDiscountRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colmonActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickListDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickListDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPickListDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colintSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintBudgetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOwnershipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCostCentreID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sp01405_AT_Work_Order_Actions_Available_For_AddingTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01405_AT_Work_Order_Actions_Available_For_AddingTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01405ATWorkOrderActionsAvailableForAddingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(954, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 716);
            this.barDockControlBottom.Size = new System.Drawing.Size(954, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 690);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(954, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 690);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 28);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.btnRefresh);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(954, 655);
            this.splitContainerControl1.SplitterPosition = 58;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.dateEditDateFrom);
            this.groupControl2.Controls.Add(this.dateEditDateTo);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Location = new System.Drawing.Point(3, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(308, 52);
            this.groupControl2.TabIndex = 24;
            this.groupControl2.Text = "Date Range";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "From:";
            // 
            // dateEditDateFrom
            // 
            this.dateEditDateFrom.EditValue = null;
            this.dateEditDateFrom.Location = new System.Drawing.Point(38, 26);
            this.dateEditDateFrom.MenuManager = this.barManager1;
            this.dateEditDateFrom.Name = "dateEditDateFrom";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditDateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, true)});
            this.dateEditDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditDateFrom.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.NullValuePrompt = "No Date";
            this.dateEditDateFrom.Properties.NullValuePromptShowForEmptyValue = true;
            this.dateEditDateFrom.Size = new System.Drawing.Size(120, 20);
            this.dateEditDateFrom.TabIndex = 21;
            this.dateEditDateFrom.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditDateFrom_ButtonClick);
            // 
            // dateEditDateTo
            // 
            this.dateEditDateTo.EditValue = null;
            this.dateEditDateTo.Location = new System.Drawing.Point(183, 27);
            this.dateEditDateTo.MenuManager = this.barManager1;
            this.dateEditDateTo.Name = "dateEditDateTo";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditDateTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, true)});
            this.dateEditDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditDateTo.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.NullValuePrompt = "No Date";
            this.dateEditDateTo.Properties.NullValuePromptShowForEmptyValue = true;
            this.dateEditDateTo.Size = new System.Drawing.Size(120, 20);
            this.dateEditDateTo.TabIndex = 22;
            this.dateEditDateTo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditDateTo_ButtonClick);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(163, 30);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "To:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.checkEdit3);
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(317, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(551, 51);
            this.groupControl1.TabIndex = 23;
            this.groupControl1.Text = "Action Type";
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(336, 26);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Actions Already Linked to a Work Order";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(211, 19);
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Actions Already Linked To a Work Order - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Use this option to load a list of actions which have already been linked to a pri" +
    "or work order. If actions are then selected from this list, they will be removed" +
    " from the original work order first.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.checkEdit3.SuperTip = superToolTip3;
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(178, 26);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Complete Unlinked Actions";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(152, 19);
            this.checkEdit2.TabIndex = 1;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Outstanding Unlinked Actions";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(166, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnRefresh.Location = new System.Drawing.Point(874, 32);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(73, 23);
            this.btnRefresh.TabIndex = 20;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(954, 591);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01405ATWorkOrderActionsAvailableForAddingBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3});
            this.gridControl1.Size = new System.Drawing.Size(954, 591);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01405ATWorkOrderActionsAvailableForAddingBindingSource
            // 
            this.sp01405ATWorkOrderActionsAvailableForAddingBindingSource.DataMember = "sp01405_AT_Work_Order_Actions_Available_For_Adding";
            this.sp01405ATWorkOrderActionsAvailableForAddingBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientName,
            this.colSiteName,
            this.colTreeReference,
            this.colInspectionReference,
            this.colTreeMappingID,
            this.colSpeciesName,
            this.coldtDueDate,
            this.coldtDoneDate,
            this.colintActionID,
            this.colstrJobNumber,
            this.colActionDescription,
            this.colintWorkOrderID,
            this.colintPrevActionID,
            this.colstrNearestHouse,
            this.colintPriorityID,
            this.colintScheduleOfRates,
            this.colActionScheduleOfRates,
            this.colintJobRateBudgeted,
            this.colBudgetedRateDescription,
            this.colmonJobRateBudgeted,
            this.colmonJobWorkUnits,
            this.colmonBudgetedCost,
            this.colintJobRateActual,
            this.colActualJobRateDescription,
            this.colmonJobRateActual,
            this.colmonDiscountRate,
            this.colmonActualCost,
            this.colintInspectionID,
            this.colintAction,
            this.colstrUser1,
            this.colstrUser2,
            this.colstrUser3,
            this.colUserPickListDescription1,
            this.colUserPickListDescription2,
            this.colUserPickListDescription3,
            this.colstrRemarks,
            this.colintSortOrder,
            this.colintActionBy,
            this.colintSupervisor,
            this.colintBudgetID,
            this.colintOwnershipID,
            this.colCostCentreCode,
            this.colintCostCentreID,
            this.colActionOwnership,
            this.colActionBy,
            this.colActionPriority,
            this.colActionSupervisor});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Width = 112;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteName.Width = 107;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 0;
            this.colTreeReference.Width = 109;
            // 
            // colInspectionReference
            // 
            this.colInspectionReference.Caption = "Inspection Reference";
            this.colInspectionReference.FieldName = "InspectionReference";
            this.colInspectionReference.Name = "colInspectionReference";
            this.colInspectionReference.OptionsColumn.AllowEdit = false;
            this.colInspectionReference.OptionsColumn.AllowFocus = false;
            this.colInspectionReference.OptionsColumn.ReadOnly = true;
            this.colInspectionReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionReference.Width = 124;
            // 
            // colTreeMappingID
            // 
            this.colTreeMappingID.Caption = "Mapping ID";
            this.colTreeMappingID.FieldName = "TreeMappingID";
            this.colTreeMappingID.Name = "colTreeMappingID";
            this.colTreeMappingID.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeMappingID.Width = 93;
            // 
            // colSpeciesName
            // 
            this.colSpeciesName.Caption = "Species";
            this.colSpeciesName.FieldName = "SpeciesName";
            this.colSpeciesName.Name = "colSpeciesName";
            this.colSpeciesName.OptionsColumn.AllowEdit = false;
            this.colSpeciesName.OptionsColumn.AllowFocus = false;
            this.colSpeciesName.OptionsColumn.ReadOnly = true;
            this.colSpeciesName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeciesName.Visible = true;
            this.colSpeciesName.VisibleIndex = 2;
            this.colSpeciesName.Width = 91;
            // 
            // coldtDueDate
            // 
            this.coldtDueDate.Caption = "Due Date";
            this.coldtDueDate.FieldName = "dtDueDate";
            this.coldtDueDate.Name = "coldtDueDate";
            this.coldtDueDate.OptionsColumn.AllowEdit = false;
            this.coldtDueDate.OptionsColumn.AllowFocus = false;
            this.coldtDueDate.OptionsColumn.ReadOnly = true;
            this.coldtDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.coldtDueDate.Visible = true;
            this.coldtDueDate.VisibleIndex = 3;
            this.coldtDueDate.Width = 66;
            // 
            // coldtDoneDate
            // 
            this.coldtDoneDate.Caption = "Done Date";
            this.coldtDoneDate.FieldName = "dtDoneDate";
            this.coldtDoneDate.Name = "coldtDoneDate";
            this.coldtDoneDate.OptionsColumn.AllowEdit = false;
            this.coldtDoneDate.OptionsColumn.AllowFocus = false;
            this.coldtDoneDate.OptionsColumn.ReadOnly = true;
            this.coldtDoneDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.coldtDoneDate.Visible = true;
            this.coldtDoneDate.VisibleIndex = 4;
            this.coldtDoneDate.Width = 72;
            // 
            // colintActionID
            // 
            this.colintActionID.Caption = "Action ID";
            this.colintActionID.FieldName = "intActionID";
            this.colintActionID.Name = "colintActionID";
            this.colintActionID.OptionsColumn.AllowEdit = false;
            this.colintActionID.OptionsColumn.AllowFocus = false;
            this.colintActionID.OptionsColumn.ReadOnly = true;
            this.colintActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintActionID.Width = 65;
            // 
            // colstrJobNumber
            // 
            this.colstrJobNumber.Caption = "Job Number";
            this.colstrJobNumber.FieldName = "strJobNumber";
            this.colstrJobNumber.Name = "colstrJobNumber";
            this.colstrJobNumber.OptionsColumn.AllowEdit = false;
            this.colstrJobNumber.OptionsColumn.AllowFocus = false;
            this.colstrJobNumber.OptionsColumn.ReadOnly = true;
            this.colstrJobNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrJobNumber.Visible = true;
            this.colstrJobNumber.VisibleIndex = 5;
            this.colstrJobNumber.Width = 101;
            // 
            // colActionDescription
            // 
            this.colActionDescription.Caption = "Action";
            this.colActionDescription.FieldName = "ActionDescription";
            this.colActionDescription.Name = "colActionDescription";
            this.colActionDescription.OptionsColumn.AllowEdit = false;
            this.colActionDescription.OptionsColumn.AllowFocus = false;
            this.colActionDescription.OptionsColumn.ReadOnly = true;
            this.colActionDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionDescription.Visible = true;
            this.colActionDescription.VisibleIndex = 1;
            this.colActionDescription.Width = 148;
            // 
            // colintWorkOrderID
            // 
            this.colintWorkOrderID.Caption = "Work Order ID";
            this.colintWorkOrderID.FieldName = "intWorkOrderID";
            this.colintWorkOrderID.Name = "colintWorkOrderID";
            this.colintWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colintWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colintWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colintWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintWorkOrderID.Visible = true;
            this.colintWorkOrderID.VisibleIndex = 18;
            this.colintWorkOrderID.Width = 91;
            // 
            // colintPrevActionID
            // 
            this.colintPrevActionID.Caption = "Linked To Action";
            this.colintPrevActionID.FieldName = "intPrevActionID";
            this.colintPrevActionID.Name = "colintPrevActionID";
            this.colintPrevActionID.OptionsColumn.AllowEdit = false;
            this.colintPrevActionID.OptionsColumn.AllowFocus = false;
            this.colintPrevActionID.OptionsColumn.ReadOnly = true;
            this.colintPrevActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintPrevActionID.Width = 99;
            // 
            // colstrNearestHouse
            // 
            this.colstrNearestHouse.Caption = "Nearest House";
            this.colstrNearestHouse.FieldName = "strNearestHouse";
            this.colstrNearestHouse.Name = "colstrNearestHouse";
            this.colstrNearestHouse.OptionsColumn.AllowEdit = false;
            this.colstrNearestHouse.OptionsColumn.AllowFocus = false;
            this.colstrNearestHouse.OptionsColumn.ReadOnly = true;
            this.colstrNearestHouse.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrNearestHouse.Visible = true;
            this.colstrNearestHouse.VisibleIndex = 7;
            this.colstrNearestHouse.Width = 129;
            // 
            // colintPriorityID
            // 
            this.colintPriorityID.Caption = "Priority ID";
            this.colintPriorityID.FieldName = "intPriorityID";
            this.colintPriorityID.Name = "colintPriorityID";
            this.colintPriorityID.OptionsColumn.AllowEdit = false;
            this.colintPriorityID.OptionsColumn.AllowFocus = false;
            this.colintPriorityID.OptionsColumn.ReadOnly = true;
            this.colintPriorityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintPriorityID.Width = 115;
            // 
            // colintScheduleOfRates
            // 
            this.colintScheduleOfRates.Caption = "Schedule Of Rates ID";
            this.colintScheduleOfRates.FieldName = "intScheduleOfRates";
            this.colintScheduleOfRates.Name = "colintScheduleOfRates";
            this.colintScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colintScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colintScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colintScheduleOfRates.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintScheduleOfRates.Width = 124;
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Schedule";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionScheduleOfRates.ToolTip = "On Schedule of Rates";
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 13;
            this.colActionScheduleOfRates.Width = 64;
            // 
            // colintJobRateBudgeted
            // 
            this.colintJobRateBudgeted.Caption = "Job Rate Budgeted ID";
            this.colintJobRateBudgeted.FieldName = "intJobRateBudgeted";
            this.colintJobRateBudgeted.Name = "colintJobRateBudgeted";
            this.colintJobRateBudgeted.OptionsColumn.AllowEdit = false;
            this.colintJobRateBudgeted.OptionsColumn.AllowFocus = false;
            this.colintJobRateBudgeted.OptionsColumn.ReadOnly = true;
            this.colintJobRateBudgeted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintJobRateBudgeted.Width = 127;
            // 
            // colBudgetedRateDescription
            // 
            this.colBudgetedRateDescription.Caption = "Budgeted Rate Desc";
            this.colBudgetedRateDescription.FieldName = "BudgetedRateDescription";
            this.colBudgetedRateDescription.Name = "colBudgetedRateDescription";
            this.colBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colBudgetedRateDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBudgetedRateDescription.Visible = true;
            this.colBudgetedRateDescription.VisibleIndex = 14;
            this.colBudgetedRateDescription.Width = 154;
            // 
            // colmonJobRateBudgeted
            // 
            this.colmonJobRateBudgeted.Caption = "Budgeted Rate";
            this.colmonJobRateBudgeted.ColumnEdit = this.repositoryItemTextEdit1;
            this.colmonJobRateBudgeted.FieldName = "monJobRateBudgeted";
            this.colmonJobRateBudgeted.Name = "colmonJobRateBudgeted";
            this.colmonJobRateBudgeted.OptionsColumn.AllowEdit = false;
            this.colmonJobRateBudgeted.OptionsColumn.AllowFocus = false;
            this.colmonJobRateBudgeted.OptionsColumn.ReadOnly = true;
            this.colmonJobRateBudgeted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonJobRateBudgeted.Visible = true;
            this.colmonJobRateBudgeted.VisibleIndex = 15;
            this.colmonJobRateBudgeted.Width = 93;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colmonJobWorkUnits
            // 
            this.colmonJobWorkUnits.Caption = "Work Units";
            this.colmonJobWorkUnits.ColumnEdit = this.repositoryItemTextEdit3;
            this.colmonJobWorkUnits.FieldName = "monJobWorkUnits";
            this.colmonJobWorkUnits.Name = "colmonJobWorkUnits";
            this.colmonJobWorkUnits.OptionsColumn.AllowEdit = false;
            this.colmonJobWorkUnits.OptionsColumn.AllowFocus = false;
            this.colmonJobWorkUnits.OptionsColumn.ReadOnly = true;
            this.colmonJobWorkUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonJobWorkUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "monJobWorkUnits", "SUM={0:#.##}")});
            this.colmonJobWorkUnits.Visible = true;
            this.colmonJobWorkUnits.VisibleIndex = 16;
            this.colmonJobWorkUnits.Width = 73;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "######0.00";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colmonBudgetedCost
            // 
            this.colmonBudgetedCost.Caption = "Budgeted Cost";
            this.colmonBudgetedCost.ColumnEdit = this.repositoryItemTextEdit1;
            this.colmonBudgetedCost.FieldName = "monBudgetedCost";
            this.colmonBudgetedCost.Name = "colmonBudgetedCost";
            this.colmonBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colmonBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colmonBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colmonBudgetedCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonBudgetedCost.Visible = true;
            this.colmonBudgetedCost.VisibleIndex = 17;
            this.colmonBudgetedCost.Width = 92;
            // 
            // colintJobRateActual
            // 
            this.colintJobRateActual.Caption = "Job Rate Actual ID";
            this.colintJobRateActual.FieldName = "intJobRateActual";
            this.colintJobRateActual.Name = "colintJobRateActual";
            this.colintJobRateActual.OptionsColumn.AllowEdit = false;
            this.colintJobRateActual.OptionsColumn.AllowFocus = false;
            this.colintJobRateActual.OptionsColumn.ReadOnly = true;
            this.colintJobRateActual.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintJobRateActual.Width = 111;
            // 
            // colActualJobRateDescription
            // 
            this.colActualJobRateDescription.Caption = "Rate Description";
            this.colActualJobRateDescription.FieldName = "ActualJobRateDescription";
            this.colActualJobRateDescription.Name = "colActualJobRateDescription";
            this.colActualJobRateDescription.OptionsColumn.AllowEdit = false;
            this.colActualJobRateDescription.OptionsColumn.AllowFocus = false;
            this.colActualJobRateDescription.OptionsColumn.ReadOnly = true;
            this.colActualJobRateDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualJobRateDescription.Width = 172;
            // 
            // colmonJobRateActual
            // 
            this.colmonJobRateActual.Caption = "Job Rate";
            this.colmonJobRateActual.ColumnEdit = this.repositoryItemTextEdit1;
            this.colmonJobRateActual.FieldName = "monJobRateActual";
            this.colmonJobRateActual.Name = "colmonJobRateActual";
            this.colmonJobRateActual.OptionsColumn.AllowEdit = false;
            this.colmonJobRateActual.OptionsColumn.AllowFocus = false;
            this.colmonJobRateActual.OptionsColumn.ReadOnly = true;
            this.colmonJobRateActual.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonJobRateActual.Width = 81;
            // 
            // colmonDiscountRate
            // 
            this.colmonDiscountRate.Caption = "Discount %";
            this.colmonDiscountRate.ColumnEdit = this.repositoryItemTextEdit2;
            this.colmonDiscountRate.FieldName = "monDiscountRate";
            this.colmonDiscountRate.Name = "colmonDiscountRate";
            this.colmonDiscountRate.OptionsColumn.AllowEdit = false;
            this.colmonDiscountRate.OptionsColumn.AllowFocus = false;
            this.colmonDiscountRate.OptionsColumn.ReadOnly = true;
            this.colmonDiscountRate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonDiscountRate.Width = 76;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "P";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colmonActualCost
            // 
            this.colmonActualCost.Caption = "Job Cost";
            this.colmonActualCost.ColumnEdit = this.repositoryItemTextEdit1;
            this.colmonActualCost.FieldName = "monActualCost";
            this.colmonActualCost.Name = "colmonActualCost";
            this.colmonActualCost.OptionsColumn.AllowEdit = false;
            this.colmonActualCost.OptionsColumn.AllowFocus = false;
            this.colmonActualCost.OptionsColumn.ReadOnly = true;
            this.colmonActualCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colmonActualCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "monActualCost", "SUM={0:#.##}")});
            this.colmonActualCost.Width = 79;
            // 
            // colintInspectionID
            // 
            this.colintInspectionID.Caption = "Inspection ID";
            this.colintInspectionID.FieldName = "intInspectionID";
            this.colintInspectionID.Name = "colintInspectionID";
            this.colintInspectionID.OptionsColumn.AllowEdit = false;
            this.colintInspectionID.OptionsColumn.AllowFocus = false;
            this.colintInspectionID.OptionsColumn.ReadOnly = true;
            this.colintInspectionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintInspectionID.Width = 85;
            // 
            // colintAction
            // 
            this.colintAction.Caption = "Action Type ID";
            this.colintAction.FieldName = "intAction";
            this.colintAction.Name = "colintAction";
            this.colintAction.OptionsColumn.AllowEdit = false;
            this.colintAction.OptionsColumn.AllowFocus = false;
            this.colintAction.OptionsColumn.ReadOnly = true;
            this.colintAction.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintAction.Width = 92;
            // 
            // colstrUser1
            // 
            this.colstrUser1.Caption = "User Text 1";
            this.colstrUser1.FieldName = "strUser1";
            this.colstrUser1.Name = "colstrUser1";
            this.colstrUser1.OptionsColumn.AllowEdit = false;
            this.colstrUser1.OptionsColumn.AllowFocus = false;
            this.colstrUser1.OptionsColumn.ReadOnly = true;
            this.colstrUser1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrUser1.Width = 77;
            // 
            // colstrUser2
            // 
            this.colstrUser2.Caption = "User Text 2";
            this.colstrUser2.FieldName = "strUser2";
            this.colstrUser2.Name = "colstrUser2";
            this.colstrUser2.OptionsColumn.AllowEdit = false;
            this.colstrUser2.OptionsColumn.AllowFocus = false;
            this.colstrUser2.OptionsColumn.ReadOnly = true;
            this.colstrUser2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrUser2.Width = 77;
            // 
            // colstrUser3
            // 
            this.colstrUser3.Caption = "User Text 3";
            this.colstrUser3.FieldName = "strUser3";
            this.colstrUser3.Name = "colstrUser3";
            this.colstrUser3.OptionsColumn.AllowEdit = false;
            this.colstrUser3.OptionsColumn.AllowFocus = false;
            this.colstrUser3.OptionsColumn.ReadOnly = true;
            this.colstrUser3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrUser3.Width = 77;
            // 
            // colUserPickListDescription1
            // 
            this.colUserPickListDescription1.Caption = "User Picklist 1";
            this.colUserPickListDescription1.FieldName = "UserPickListDescription1";
            this.colUserPickListDescription1.Name = "colUserPickListDescription1";
            this.colUserPickListDescription1.OptionsColumn.AllowEdit = false;
            this.colUserPickListDescription1.OptionsColumn.AllowFocus = false;
            this.colUserPickListDescription1.OptionsColumn.ReadOnly = true;
            this.colUserPickListDescription1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickListDescription1.Width = 86;
            // 
            // colUserPickListDescription2
            // 
            this.colUserPickListDescription2.Caption = "User Picklist 2";
            this.colUserPickListDescription2.FieldName = "UserPickListDescription2";
            this.colUserPickListDescription2.Name = "colUserPickListDescription2";
            this.colUserPickListDescription2.OptionsColumn.AllowEdit = false;
            this.colUserPickListDescription2.OptionsColumn.AllowFocus = false;
            this.colUserPickListDescription2.OptionsColumn.ReadOnly = true;
            this.colUserPickListDescription2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickListDescription2.Width = 86;
            // 
            // colUserPickListDescription3
            // 
            this.colUserPickListDescription3.Caption = "User Picklist 3";
            this.colUserPickListDescription3.FieldName = "UserPickListDescription3";
            this.colUserPickListDescription3.Name = "colUserPickListDescription3";
            this.colUserPickListDescription3.OptionsColumn.AllowEdit = false;
            this.colUserPickListDescription3.OptionsColumn.AllowFocus = false;
            this.colUserPickListDescription3.OptionsColumn.ReadOnly = true;
            this.colUserPickListDescription3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUserPickListDescription3.Width = 86;
            // 
            // colstrRemarks
            // 
            this.colstrRemarks.Caption = "Remarks";
            this.colstrRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colstrRemarks.FieldName = "strRemarks";
            this.colstrRemarks.Name = "colstrRemarks";
            this.colstrRemarks.OptionsColumn.ReadOnly = true;
            this.colstrRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colstrRemarks.Visible = true;
            this.colstrRemarks.VisibleIndex = 12;
            this.colstrRemarks.Width = 62;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colintSortOrder
            // 
            this.colintSortOrder.Caption = "Order";
            this.colintSortOrder.FieldName = "intSortOrder";
            this.colintSortOrder.Name = "colintSortOrder";
            this.colintSortOrder.OptionsColumn.AllowEdit = false;
            this.colintSortOrder.OptionsColumn.AllowFocus = false;
            this.colintSortOrder.OptionsColumn.ReadOnly = true;
            this.colintSortOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintSortOrder.Width = 65;
            // 
            // colintActionBy
            // 
            this.colintActionBy.Caption = "Action By ID";
            this.colintActionBy.FieldName = "intActionBy";
            this.colintActionBy.Name = "colintActionBy";
            this.colintActionBy.OptionsColumn.AllowEdit = false;
            this.colintActionBy.OptionsColumn.AllowFocus = false;
            this.colintActionBy.OptionsColumn.ReadOnly = true;
            this.colintActionBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintActionBy.Width = 126;
            // 
            // colintSupervisor
            // 
            this.colintSupervisor.Caption = "Supervisor ID";
            this.colintSupervisor.FieldName = "intSupervisor";
            this.colintSupervisor.Name = "colintSupervisor";
            this.colintSupervisor.OptionsColumn.AllowEdit = false;
            this.colintSupervisor.OptionsColumn.AllowFocus = false;
            this.colintSupervisor.OptionsColumn.ReadOnly = true;
            this.colintSupervisor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintSupervisor.Width = 119;
            // 
            // colintBudgetID
            // 
            this.colintBudgetID.Caption = "Budget ID";
            this.colintBudgetID.FieldName = "intBudgetID";
            this.colintBudgetID.Name = "colintBudgetID";
            this.colintBudgetID.OptionsColumn.AllowEdit = false;
            this.colintBudgetID.OptionsColumn.AllowFocus = false;
            this.colintBudgetID.OptionsColumn.ReadOnly = true;
            this.colintBudgetID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintBudgetID.Width = 69;
            // 
            // colintOwnershipID
            // 
            this.colintOwnershipID.Caption = "Ownership ID";
            this.colintOwnershipID.FieldName = "intOwnershipID";
            this.colintOwnershipID.Name = "colintOwnershipID";
            this.colintOwnershipID.OptionsColumn.AllowEdit = false;
            this.colintOwnershipID.OptionsColumn.AllowFocus = false;
            this.colintOwnershipID.OptionsColumn.ReadOnly = true;
            this.colintOwnershipID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintOwnershipID.Width = 120;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.Caption = "Cost Centre";
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode.Visible = true;
            this.colCostCentreCode.VisibleIndex = 9;
            this.colCostCentreCode.Width = 136;
            // 
            // colintCostCentreID
            // 
            this.colintCostCentreID.Caption = "Cost Centre ID";
            this.colintCostCentreID.FieldName = "intCostCentreID";
            this.colintCostCentreID.Name = "colintCostCentreID";
            this.colintCostCentreID.OptionsColumn.AllowEdit = false;
            this.colintCostCentreID.OptionsColumn.AllowFocus = false;
            this.colintCostCentreID.OptionsColumn.ReadOnly = true;
            this.colintCostCentreID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colintCostCentreID.Width = 93;
            // 
            // colActionOwnership
            // 
            this.colActionOwnership.Caption = "Ownership";
            this.colActionOwnership.FieldName = "ActionOwnership";
            this.colActionOwnership.Name = "colActionOwnership";
            this.colActionOwnership.OptionsColumn.AllowEdit = false;
            this.colActionOwnership.OptionsColumn.AllowFocus = false;
            this.colActionOwnership.OptionsColumn.ReadOnly = true;
            this.colActionOwnership.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionOwnership.Visible = true;
            this.colActionOwnership.VisibleIndex = 8;
            this.colActionOwnership.Width = 124;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action By";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            this.colActionBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionBy.Visible = true;
            this.colActionBy.VisibleIndex = 10;
            this.colActionBy.Width = 122;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 6;
            this.colActionPriority.Width = 103;
            // 
            // colActionSupervisor
            // 
            this.colActionSupervisor.Caption = "Supervisor";
            this.colActionSupervisor.FieldName = "ActionSupervisor";
            this.colActionSupervisor.Name = "colActionSupervisor";
            this.colActionSupervisor.OptionsColumn.AllowEdit = false;
            this.colActionSupervisor.OptionsColumn.AllowFocus = false;
            this.colActionSupervisor.OptionsColumn.ReadOnly = true;
            this.colActionSupervisor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionSupervisor.Visible = true;
            this.colActionSupervisor.VisibleIndex = 11;
            this.colActionSupervisor.Width = 118;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(873, 688);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(792, 688);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // sp01405_AT_Work_Order_Actions_Available_For_AddingTableAdapter
            // 
            this.sp01405_AT_Work_Order_Actions_Available_For_AddingTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information: Tick one or more actions to add then click OK";
            this.barStaticItemInformation.Id = 30;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(6, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(7, "arrow_down_blue_round.png");
            // 
            // frm_AT_Work_Order_Add_Actions
            // 
            this.AcceptButton = this.btnOK;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(954, 746);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_AT_Work_Order_Add_Actions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Work Order - Link Actions";
            this.Load += new System.EventHandler(this.frm_AT_Work_Order_Add_Actions_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01405ATWorkOrderActionsAvailableForAddingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.DateEdit dateEditDateTo;
        private DevExpress.XtraEditors.DateEdit dateEditDateFrom;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp01405ATWorkOrderActionsAvailableForAddingBindingSource;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colintActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colintWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colintPrevActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrNearestHouse;
        private DevExpress.XtraGrid.Columns.GridColumn colintPriorityID;
        private DevExpress.XtraGrid.Columns.GridColumn colintScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colintJobRateBudgeted;
        private DevExpress.XtraGrid.Columns.GridColumn colBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobRateBudgeted;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colmonBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colintJobRateActual;
        private DevExpress.XtraGrid.Columns.GridColumn colActualJobRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobRateActual;
        private DevExpress.XtraGrid.Columns.GridColumn colmonDiscountRate;
        private DevExpress.XtraGrid.Columns.GridColumn colmonActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colintInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colintAction;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickListDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickListDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPickListDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colintSortOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colintActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colintSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colintBudgetID;
        private DevExpress.XtraGrid.Columns.GridColumn colintOwnershipID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colintCostCentreID;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01405_AT_Work_Order_Actions_Available_For_AddingTableAdapter sp01405_AT_Work_Order_Actions_Available_For_AddingTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraGrid.Columns.GridColumn colActionOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSupervisor;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

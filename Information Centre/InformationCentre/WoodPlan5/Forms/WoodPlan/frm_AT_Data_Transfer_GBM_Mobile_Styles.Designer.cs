namespace WoodPlan5
{
    partial class frm_AT_Data_Transfer_GBM_Mobile_Styles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Data_Transfer_GBM_Mobile_Styles));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.sp00098exporttoGBMfieldlistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataTransfer = new WoodPlan5.DataSet_AT_DataTransfer();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp00131exporttoGBMstylefieldsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.sp00098_export_to_GBM_field_listTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00098_export_to_GBM_field_listTableAdapter();
            this.sp00131_export_to_GBM_style_fieldsTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00131_export_to_GBM_style_fieldsTableAdapter();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.editPolygonLineWidth = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.editPolygonFillPatternColour = new DevExpress.XtraEditors.ColorEdit();
            this.editPolygonFillPattern = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.imageList4 = new System.Windows.Forms.ImageList(this.components);
            this.editPolygonLineColour = new DevExpress.XtraEditors.ColorEdit();
            this.editPolygonFillColour = new DevExpress.XtraEditors.ColorEdit();
            this.editPointColour = new DevExpress.XtraEditors.ColorEdit();
            this.editPointSymbol = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.editPointSize = new DevExpress.XtraEditors.SpinEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00098exporttoGBMfieldlistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataTransfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00131exporttoGBMstylefieldsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonLineWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonFillPatternColour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonFillPattern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonLineColour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonFillColour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPointColour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPointSymbol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPointSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(488, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 412);
            this.barDockControlBottom.Size = new System.Drawing.Size(488, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 412);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(488, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 412);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // sp00098exporttoGBMfieldlistBindingSource
            // 
            this.sp00098exporttoGBMfieldlistBindingSource.DataMember = "sp00098_export_to_GBM_field_list";
            this.sp00098exporttoGBMfieldlistBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // dataSet_AT_DataTransfer
            // 
            this.dataSet_AT_DataTransfer.DataSetName = "DataSet_AT_DataTransfer";
            this.dataSet_AT_DataTransfer.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(152, 382);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(255, 382);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp00131exporttoGBMstylefieldsBindingSource
            // 
            this.sp00131exporttoGBMstylefieldsBindingSource.DataMember = "sp00131_export_to_GBM_style_fields";
            this.sp00131exporttoGBMstylefieldsBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "symbol_circle_16.png");
            this.imageList1.Images.SetKeyName(1, "symbol_square_16.png");
            this.imageList1.Images.SetKeyName(2, "symbol_diamond_16.png");
            this.imageList1.Images.SetKeyName(3, "symbol_triangle1_16.png");
            this.imageList1.Images.SetKeyName(4, "symbol_triangle2_16.png");
            this.imageList1.Images.SetKeyName(5, "symbol_star_16.png");
            // 
            // sp00098_export_to_GBM_field_listTableAdapter
            // 
            this.sp00098_export_to_GBM_field_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00131_export_to_GBM_style_fieldsTableAdapter
            // 
            this.sp00131_export_to_GBM_style_fieldsTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.editPolygonLineWidth);
            this.layoutControl1.Controls.Add(this.editPolygonFillPatternColour);
            this.layoutControl1.Controls.Add(this.editPolygonFillPattern);
            this.layoutControl1.Controls.Add(this.editPolygonLineColour);
            this.layoutControl1.Controls.Add(this.editPolygonFillColour);
            this.layoutControl1.Controls.Add(this.editPointColour);
            this.layoutControl1.Controls.Add(this.editPointSymbol);
            this.layoutControl1.Controls.Add(this.editPointSize);
            this.layoutControl1.Controls.Add(this.pictureEdit1);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Location = new System.Drawing.Point(2, 3);
            this.layoutControl1.MenuManager = this.barManager1;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(484, 374);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // editPolygonLineWidth
            // 
            this.editPolygonLineWidth.Location = new System.Drawing.Point(157, 297);
            this.editPolygonLineWidth.MenuManager = this.barManager1;
            this.editPolygonLineWidth.Name = "editPolygonLineWidth";
            this.editPolygonLineWidth.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.editPolygonLineWidth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Clear Block Edit Value", null, null, true)});
            this.editPolygonLineWidth.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("1 Pixel", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("2 Pixels", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("3 Pixels", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("4 Pixels", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("5 Pixels", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("6 Pixels", 6, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("7 Pixels", 7, 6)});
            this.editPolygonLineWidth.Properties.NullText = "0";
            this.editPolygonLineWidth.Properties.SmallImages = this.imageList3;
            this.editPolygonLineWidth.Size = new System.Drawing.Size(286, 20);
            this.editPolygonLineWidth.StyleController = this.layoutControl1;
            this.editPolygonLineWidth.TabIndex = 6;
            this.editPolygonLineWidth.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.editPolygonLineWidth_ButtonClick);
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "Border_01.png");
            this.imageList3.Images.SetKeyName(1, "Border_02.png");
            this.imageList3.Images.SetKeyName(2, "Border_03.png");
            this.imageList3.Images.SetKeyName(3, "Border_04.png");
            this.imageList3.Images.SetKeyName(4, "Border_05.png");
            this.imageList3.Images.SetKeyName(5, "Border_06.png");
            this.imageList3.Images.SetKeyName(6, "Border_07.png");
            // 
            // editPolygonFillPatternColour
            // 
            this.editPolygonFillPatternColour.EditValue = System.Drawing.Color.Empty;
            this.editPolygonFillPatternColour.Location = new System.Drawing.Point(157, 273);
            this.editPolygonFillPatternColour.MenuManager = this.barManager1;
            this.editPolygonFillPatternColour.Name = "editPolygonFillPatternColour";
            this.editPolygonFillPatternColour.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.editPolygonFillPatternColour.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Clear Block Edit Value", null, null, true)});
            this.editPolygonFillPatternColour.Properties.NullText = "No Block Editing";
            this.editPolygonFillPatternColour.Properties.StoreColorAsInteger = true;
            this.editPolygonFillPatternColour.Size = new System.Drawing.Size(286, 20);
            this.editPolygonFillPatternColour.StyleController = this.layoutControl1;
            this.editPolygonFillPatternColour.TabIndex = 5;
            this.editPolygonFillPatternColour.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.editPolygonFillPatternColour_ButtonClick);
            // 
            // editPolygonFillPattern
            // 
            this.editPolygonFillPattern.Location = new System.Drawing.Point(157, 249);
            this.editPolygonFillPattern.MenuManager = this.barManager1;
            this.editPolygonFillPattern.Name = "editPolygonFillPattern";
            this.editPolygonFillPattern.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.editPolygonFillPattern.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Clear Block Edit Value", null, null, true)});
            this.editPolygonFillPattern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Solid", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Horizontal Lines", 3, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Vertical Lines", 4, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diagonal Lines", 6, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Crosshatch", 8, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dots 1", 14, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dots 2", 17, 6)});
            this.editPolygonFillPattern.Properties.NullText = "0";
            this.editPolygonFillPattern.Properties.SmallImages = this.imageList4;
            this.editPolygonFillPattern.Size = new System.Drawing.Size(286, 20);
            this.editPolygonFillPattern.StyleController = this.layoutControl1;
            this.editPolygonFillPattern.TabIndex = 4;
            this.editPolygonFillPattern.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.editPolygonFillPattern_ButtonClick);
            // 
            // imageList4
            // 
            this.imageList4.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList4.ImageStream")));
            this.imageList4.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList4.Images.SetKeyName(0, "pattern_02.png");
            this.imageList4.Images.SetKeyName(1, "pattern_03.png");
            this.imageList4.Images.SetKeyName(2, "pattern_04.png");
            this.imageList4.Images.SetKeyName(3, "pattern_06.png");
            this.imageList4.Images.SetKeyName(4, "pattern_08.png");
            this.imageList4.Images.SetKeyName(5, "pattern_14.png");
            this.imageList4.Images.SetKeyName(6, "pattern_17.png");
            // 
            // editPolygonLineColour
            // 
            this.editPolygonLineColour.EditValue = System.Drawing.Color.Empty;
            this.editPolygonLineColour.Location = new System.Drawing.Point(157, 321);
            this.editPolygonLineColour.MenuManager = this.barManager1;
            this.editPolygonLineColour.Name = "editPolygonLineColour";
            this.editPolygonLineColour.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.editPolygonLineColour.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "Clear Block Edit Value", null, null, true)});
            this.editPolygonLineColour.Properties.NullText = "No Block Editing";
            this.editPolygonLineColour.Properties.StoreColorAsInteger = true;
            this.editPolygonLineColour.Size = new System.Drawing.Size(286, 20);
            this.editPolygonLineColour.StyleController = this.layoutControl1;
            this.editPolygonLineColour.TabIndex = 7;
            this.editPolygonLineColour.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.editPolygonLineColour_ButtonClick);
            // 
            // editPolygonFillColour
            // 
            this.editPolygonFillColour.EditValue = System.Drawing.Color.Empty;
            this.editPolygonFillColour.Location = new System.Drawing.Point(157, 225);
            this.editPolygonFillColour.MenuManager = this.barManager1;
            this.editPolygonFillColour.Name = "editPolygonFillColour";
            this.editPolygonFillColour.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.editPolygonFillColour.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "Clear Block Edit Value", null, null, true)});
            this.editPolygonFillColour.Properties.NullText = "No Block Editing";
            this.editPolygonFillColour.Properties.StoreColorAsInteger = true;
            this.editPolygonFillColour.Size = new System.Drawing.Size(286, 20);
            this.editPolygonFillColour.StyleController = this.layoutControl1;
            this.editPolygonFillColour.TabIndex = 3;
            this.editPolygonFillColour.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.editPolygonFillColour_ButtonClick);
            // 
            // editPointColour
            // 
            this.editPointColour.EditValue = System.Drawing.Color.Empty;
            this.editPointColour.Location = new System.Drawing.Point(157, 140);
            this.editPointColour.MenuManager = this.barManager1;
            this.editPointColour.Name = "editPointColour";
            this.editPointColour.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.editPointColour.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "Clear Block Edit Value", null, null, true)});
            this.editPointColour.Properties.NullText = "No Block Editing";
            this.editPointColour.Properties.StoreColorAsInteger = true;
            this.editPointColour.Size = new System.Drawing.Size(286, 20);
            this.editPointColour.StyleController = this.layoutControl1;
            this.editPointColour.TabIndex = 2;
            this.editPointColour.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.editPointColour_ButtonClick);
            // 
            // editPointSymbol
            // 
            this.editPointSymbol.Location = new System.Drawing.Point(157, 92);
            this.editPointSymbol.MenuManager = this.barManager1;
            this.editPointSymbol.Name = "editPointSymbol";
            this.editPointSymbol.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.editPointSymbol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "Clear Block Edit Value", null, null, true)});
            this.editPointSymbol.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Circle", 35, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Square", 33, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diamond", 34, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 1", 37, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 2", 38, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Star", 36, 5)});
            this.editPointSymbol.Properties.NullText = "0";
            this.editPointSymbol.Properties.SmallImages = this.imageList1;
            this.editPointSymbol.Size = new System.Drawing.Size(286, 20);
            this.editPointSymbol.StyleController = this.layoutControl1;
            this.editPointSymbol.TabIndex = 0;
            this.editPointSymbol.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.editPointSymbol_ButtonClick);
            // 
            // editPointSize
            // 
            this.editPointSize.EditValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.editPointSize.Location = new System.Drawing.Point(157, 116);
            this.editPointSize.MenuManager = this.barManager1;
            this.editPointSize.Name = "editPointSize";
            this.editPointSize.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.editPointSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject8, "Clear Block Edit Value", null, null, true)});
            this.editPointSize.Properties.DisplayFormat.FormatString = "f2";
            this.editPointSize.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.editPointSize.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.editPointSize.Properties.Mask.EditMask = "f2";
            this.editPointSize.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.editPointSize.Properties.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.editPointSize.Properties.NullText = "0";
            this.editPointSize.Size = new System.Drawing.Size(286, 20);
            this.editPointSize.StyleController = this.layoutControl1;
            this.editPointSize.TabIndex = 1;
            this.editPointSize.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.editPointSize_ButtonClick);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(12, 12);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(42, 42);
            this.pictureEdit1.StyleController = this.layoutControl1;
            this.pictureEdit1.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Location = new System.Drawing.Point(58, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(397, 42);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Enter values in fields to be block edited. Any field left unchanged will not be m" +
    "odified.";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(467, 375);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(46, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(393, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(401, 46);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 345);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(447, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.pictureEdit1;
            this.layoutControlItem2.CustomizationFormText = "Icon";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(46, 46);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(46, 46);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(46, 46);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Icon";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Point Group";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 46);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(447, 118);
            this.layoutControlGroup2.Text = "Point";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AllowHtmlStringInCaption = true;
            this.layoutControlItem4.Control = this.editPointSymbol;
            this.layoutControlItem4.CustomizationFormText = "Point Symbol:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(423, 24);
            this.layoutControlItem4.Text = "Point Symbol:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AllowHtmlStringInCaption = true;
            this.layoutControlItem3.Control = this.editPointSize;
            this.layoutControlItem3.CustomizationFormText = "Point Size:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(423, 24);
            this.layoutControlItem3.Text = "Point Size:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AllowHtmlStringInCaption = true;
            this.layoutControlItem5.Control = this.editPointColour;
            this.layoutControlItem5.CustomizationFormText = "Point Colour:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(423, 24);
            this.layoutControlItem5.Text = "Point Colour:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Polygon Group";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 179);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(447, 166);
            this.layoutControlGroup3.Text = "Polygon";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AllowHtmlStringInCaption = true;
            this.layoutControlItem6.Control = this.editPolygonFillColour;
            this.layoutControlItem6.CustomizationFormText = "Polygon Fill Colour:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(423, 24);
            this.layoutControlItem6.Text = "Polygon Fill Colour:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AllowHtmlStringInCaption = true;
            this.layoutControlItem7.Control = this.editPolygonLineColour;
            this.layoutControlItem7.CustomizationFormText = "Polygon Line Colour:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(423, 24);
            this.layoutControlItem7.Text = "Polygon Line Colour:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AllowHtmlStringInCaption = true;
            this.layoutControlItem8.Control = this.editPolygonFillPattern;
            this.layoutControlItem8.CustomizationFormText = "Polygon Fill Pattern:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(423, 24);
            this.layoutControlItem8.Text = "Polygon Fill Pattern:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AllowHtmlStringInCaption = true;
            this.layoutControlItem9.Control = this.editPolygonFillPatternColour;
            this.layoutControlItem9.CustomizationFormText = "Polygon Fill Pattern Colour:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(423, 24);
            this.layoutControlItem9.Text = "Polygon Fill Pattern Colour:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AllowHtmlStringInCaption = true;
            this.layoutControlItem10.Control = this.editPolygonLineWidth;
            this.layoutControlItem10.CustomizationFormText = "Polygon Line Width:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(423, 24);
            this.layoutControlItem10.Text = "Polygon Line Width:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(130, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 164);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 15);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 15);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(447, 15);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_AT_Data_Transfer_GBM_Mobile_Styles
            // 
            this.ClientSize = new System.Drawing.Size(488, 412);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_AT_Data_Transfer_GBM_Mobile_Styles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Export to GBM Mobile - Block Edit Styles";
            this.Load += new System.EventHandler(this.frm_AT_Data_Transfer_GBM_Mobile_Styles_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00098exporttoGBMfieldlistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataTransfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00131exporttoGBMstylefieldsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonLineWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonFillPatternColour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonFillPattern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonLineColour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPolygonFillColour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPointColour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPointSymbol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPointSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DataSet_AT_DataTransfer dataSet_AT_DataTransfer;
        private System.Windows.Forms.BindingSource sp00098exporttoGBMfieldlistBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00098_export_to_GBM_field_listTableAdapter sp00098_export_to_GBM_field_listTableAdapter;
        private System.Windows.Forms.BindingSource sp00131exporttoGBMstylefieldsBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00131_export_to_GBM_style_fieldsTableAdapter sp00131_export_to_GBM_style_fieldsTableAdapter;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SpinEdit editPointSize;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.ImageComboBoxEdit editPointSymbol;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.ColorEdit editPointColour;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.ColorEdit editPolygonLineColour;
        private DevExpress.XtraEditors.ColorEdit editPolygonFillColour;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.ImageComboBoxEdit editPolygonFillPattern;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.ImageComboBoxEdit editPolygonLineWidth;
        private DevExpress.XtraEditors.ColorEdit editPolygonFillPatternColour;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ImageList imageList4;
    }
}

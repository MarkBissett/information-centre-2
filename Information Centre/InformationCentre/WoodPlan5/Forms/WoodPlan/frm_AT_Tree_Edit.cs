using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_AT_Tree_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID1 = 0;
        public int intLinkedToRecordID2 = 0;
        public string strLinkedToRecordDescription1 = "";
        public string strLinkedToRecordDescription2 = "";
        public string strPassedInSiteCode = "";

        public bool ibool_CalledByTreePicker = false;
        public int intObjectType = 0;
        public int intX = 0;
        public int intY = 0;
        public string strPolygonXY = "";
        public decimal decArea = (decimal)0.00;
        public decimal decLength = (decimal)0.00;
        public decimal decWidth = (decimal)0.00;
        public string strID = "";
        public string strMapIDs = "";  // Only required when block editing from the map - used to tell the tree picker which objects to reload //

        public float flLatLongX = 0;  // Following 3 required to store LatLong version of plotted object from map //
        public float flLatLongY = 0;
        public string strLatLongPolygonXY = "";

        ArrayList ArrayListFilteredPicklists;  // Holds all the picklists bound to sp01372 - used for iterating round to bind and unbind generic Enter, Leave and ButtonClick events //
        ArrayList ArrayListFilteredCavatLists;  // Holds all the picklists bound to sp01436_AT_CAVAT_List_Values - used for iterating round to bind and unbind generic Enter, Leave, Validated and Edit events //
        
        private bool ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;
       
        BaseObjects.GridCheckMarksSelection selection1;

        private string i_strLastUsedSequencePrefix = "";

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState39;  // Used by Grid View State Facilities //
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs39 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultPicturesPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        private DataSet_Selection DS_Selection2;
        private DataSet_Selection DS_Selection3;

        private string strCavatOn = "On";
        private string strCavatMethod = "Full";
        private decimal decCavatDefaultUnitValueFactor = (decimal)0.00;

        #endregion

        public frm_AT_Tree_Edit()
        {
            InitializeComponent();
        }

        private void frm_AT_Tree_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 20011;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            
            emptyEditor = new RepositoryItem();

            // Set form Permissions //
            stcFormPermissions sfpPermissions = (stcFormPermissions)this.FormPermissions[0];
            if (this.FormPermissions.Count > 0)
            {
                iBool_AllowAdd = sfpPermissions.blCreate;
                iBool_AllowEdit = sfpPermissions.blUpdate;
                iBool_AllowDelete = sfpPermissions.blDelete;
            }

            ArrayListFilteredPicklists = new ArrayList();  // Note: Each GridLookUpEdit within the array should have it's tag value set to it's value in the SQL PicklistHeader Table //
            ArrayListFilteredPicklists.Add(intSizeGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intDBHRangeGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intHeightRangeGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intLegalStatusGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intAgeClassGridLookUpEdit);
            ArrayListFilteredPicklists.Add(RetentionCategoryGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intSafetyPriorityGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intSiteHazardClassGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intNearbyObjectGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intNearbyObject2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intNearbyObject3GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intAccessGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intVisibilityGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intPlantSizeGridLookUpEdit);           
            ArrayListFilteredPicklists.Add(intPlantSourceGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intPlantMethodGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intGroundTypeGridLookUpEdit);
            ArrayListFilteredPicklists.Add(intProtectionTypeGridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList1GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList3GridLookUpEdit);

            sp01370_AT_Object_Types_No_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01370_AT_Object_Types_No_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01370_AT_Object_Types_No_Blank);

            sp01371_AT_Object_Statuses_No_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01371_AT_Object_Statuses_No_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01371_AT_Object_Statuses_No_Blank);

            sp01362_AT_Ownership_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01362_AT_Ownership_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01362_AT_Ownership_List_With_Blank);

            sp00179_Species_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00179_Species_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00179_Species_List_With_Blank);

            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);

            sp01374_AT_Species_Varieties_With_BlanksTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01374_AT_Species_Varieties_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01374_AT_Species_Varieties_With_Blanks);

            // Last Saved Record Details //
            sp01375_AT_User_Screen_SettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01375_AT_User_Screen_SettingsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings, this.GlobalSettings.UserID, FormID);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp01367_AT_Inspections_For_TreesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView25, "InspectionID");

            sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView29, "ActionID");

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView30, "LinkedDocumentID");

            sp02064_AT_Tree_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState39 = new RefreshGridState(gridView39, "SurveyPictureID");

            DS_Selection2 = new Utilities.DataSet_Selection((GridView)gridView25, strConnectionString);  // Inspections List //
            DS_Selection3 = new Utilities.DataSet_Selection((GridView)gridView29, strConnectionString);  // Actions List //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPicturesPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Pictures path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Pictures Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            
            SqlDataAdapter sdaSettings = new SqlDataAdapter();
            DataSet dsSettings = new DataSet("NewDataSet");
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp01435_AT_CAVAT_Get_Settings", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            sdaSettings = new SqlDataAdapter(cmd);
            sdaSettings.Fill(dsSettings, "Table");
            if (dsSettings.Tables[0].Rows.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default CAVAT Settings (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get CAVAT Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                DataRow dr = dsSettings.Tables[0].Rows[0];
                strCavatOn = dr["AmenityTreesCavatCalculation"].ToString();
                strCavatMethod = dr["AmenityTreesCavatCalculationMethod"].ToString();
                decCavatDefaultUnitValueFactor = Convert.ToDecimal(dr["AmenityTreesCavatDefaultUnitValueFactor"]);
            }
            if (strCavatOn == "On")
            {
                sp01436_AT_CAVAT_List_CTI_FactorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01436_AT_CAVAT_List_CTI_FactorsTableAdapter.Fill(dataSet_AT_DataEntry.sp01436_AT_CAVAT_List_CTI_Factors);

                sp01437_AT_CAVAT_List_CTI_AccessibilityTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01437_AT_CAVAT_List_CTI_AccessibilityTableAdapter.Fill(dataSet_AT_DataEntry.sp01437_AT_CAVAT_List_CTI_Accessibility);

                sp01438_AT_CAVAT_List_CTI_FunctionalValueFactorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01438_AT_CAVAT_List_CTI_FunctionalValueFactorsTableAdapter.Fill(dataSet_AT_DataEntry.sp01438_AT_CAVAT_List_CTI_FunctionalValueFactors);

                sp01439_AT_CAVAT_List_CTI_AmenityFactorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01439_AT_CAVAT_List_CTI_AmenityFactorsTableAdapter.Fill(dataSet_AT_DataEntry.sp01439_AT_CAVAT_List_CTI_AmenityFactors);

                sp01440_AT_CAVAT_List_CTI_AppropriatenessTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01440_AT_CAVAT_List_CTI_AppropriatenessTableAdapter.Fill(dataSet_AT_DataEntry.sp01440_AT_CAVAT_List_CTI_Appropriateness);

                sp01441_AT_CAVAT_List_SLEFactors2TableAdapter.Connection.ConnectionString = strConnectionString;
                sp01441_AT_CAVAT_List_SLEFactors2TableAdapter.Fill(dataSet_AT_DataEntry.sp01441_AT_CAVAT_List_SLEFactors2);

                sp01442_AT_CAVAT_List_CTIRatingTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01442_AT_CAVAT_List_CTIRatingTableAdapter.Fill(dataSet_AT_DataEntry.sp01442_AT_CAVAT_List_CTIRating);

                sp01443_AT_CAVAT_List_FunctionalAdjustmentTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01443_AT_CAVAT_List_FunctionalAdjustmentTableAdapter.Fill(dataSet_AT_DataEntry.sp01443_AT_CAVAT_List_FunctionalAdjustment);

                sp01444_AT_CAVAT_List_SLEFactor2TableAdapter.Connection.ConnectionString = strConnectionString;
                sp01444_AT_CAVAT_List_SLEFactor2TableAdapter.Fill(dataSet_AT_DataEntry.sp01444_AT_CAVAT_List_SLEFactor2);

                ArrayListFilteredCavatLists = new ArrayList();  // Note: Each GridLookUpEdit within the array should have it's tag value set to it's value in the SP //
                ArrayListFilteredCavatLists.Add(CavatCTIFactorGridLookUpEdit);
                ArrayListFilteredCavatLists.Add(CavatAccessibilityGridLookUpEdit);
                ArrayListFilteredCavatLists.Add(CavatFunctionalValueFactorGridLookUpEdit);
                ArrayListFilteredCavatLists.Add(CavatAmenityFactorsGridLookUpEdit);
                ArrayListFilteredCavatLists.Add(CavatAppropriatenessGridLookUpEdit);
                ArrayListFilteredCavatLists.Add(CavatSLEFactorGridLookUpEdit);

                ArrayListFilteredCavatLists.Add(CavatCTIFactorGridLookUpEdit2);
                ArrayListFilteredCavatLists.Add(CavatFunctionalValueFactorGridLookUpEdit2);
                ArrayListFilteredCavatLists.Add(CavatSLEFactorGridLookUpEdit2);
            }

            // Populate Main Dataset //
            sp01204_AT_Edit_Tree_DetailsTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SiteID"] = intLinkedToRecordID1;
                        drNewRow["ClientID"] = intLinkedToRecordID2;
                        drNewRow["SiteName"] = strLinkedToRecordDescription1;
                        drNewRow["ClientName"] = strLinkedToRecordDescription2;
                        drNewRow["SiteCode"] = strPassedInSiteCode;
                        drNewRow["intType"] = intObjectType;  // 0 = Tree, 1 = Hedge, 2 = Tree Group,  Defaults to 0 unless passed in from TreePicker screen //
                        drNewRow["intStatus"] = 1;  // 0 = Planned, 1 = Actual, 2 = Historical //
                        drNewRow["decRiskFactor"] = (decimal)0.00;
                        drNewRow["StemCount"] = 1;
                        if (ibool_CalledByTreePicker)
                        {
                            drNewRow["intX"] = intX;
                            drNewRow["intY"] = intY;
                            drNewRow["strPolygonXY"] = strPolygonXY;
                            drNewRow["decAreaHa"] = decArea;
                            drNewRow["ObjectLength"] = decLength;
                            drNewRow["ObjectWidth"] = decWidth;
                            drNewRow["id"] = strID;
                            drNewRow["CavatAdjustedPercent"] = (decimal)100.00;

                            drNewRow["LocationX"] = flLatLongX;
                            drNewRow["LocationY"] = flLatLongY;
                            drNewRow["LocationPolyXY"] = strLatLongPolygonXY;
                        }
                        if (strCavatOn == "On") drNewRow["CavatUnitValueFactor"] = decCavatDefaultUnitValueFactor;
                        this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.Rows.Add(drNewRow);

                        // Get next sequence //
                        DataRow[] drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = 'strTreeRefButtonEdit'");
                        if (drFiltered.Length != 0)  // Matching row found //
                        {
                            DataRow dr = drFiltered[0];
                            strTreeRefButtonEdit.EditValue = dr["ItemValue"].ToString();
                            // Calculate Next Sequence Number by triggering button click event of strTreeRefButtonEdit //
                            if (!string.IsNullOrEmpty(strTreeRefButtonEdit.EditValue.ToString())) strTreeRefButtonEdit_ButtonClick(strTreeRefButtonEdit, new DevExpress.XtraEditors.Controls.ButtonPressedEventArgs(strTreeRefButtonEdit.Properties.Buttons[1]));
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.Rows.Add(drNewRow);
                        this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp01204_AT_Edit_Tree_DetailsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                        string str = "";
                    }
                    Load_Linked_Pictures();
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
            Attach_Enter_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach Enter Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //
            Attach_ButtonClick_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach ButtonClick Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //

            if (strCavatOn == "On")
            {
                Attach_EditValueChanged_To_CavatGridLookupEdits(ArrayListFilteredCavatLists);  // Attach EditValueChanged Event to All CAVAT GridLookUpEdits... Detached on Form Closing Event //
                Attach_Validated_To_CavatGridLookupEdits(ArrayListFilteredCavatLists);  // Attach Validated Event to All CAVAT GridLookUpEdits... Detached on Form Closing Event //
            }

            // Add record selection checkboxes to grid and popup grid control //
            gridControl2.ForceInitialize();
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            bbiCopyAllValues.Enabled = (this.strFormMode == "add" || ibool_CalledByTreePicker ? true : false);
            bbiCopySelectedValues.Enabled = (this.strFormMode == "add" || ibool_CalledByTreePicker ? true : false);
            bbiReload.Enabled = (this.strFormMode == "add" || ibool_CalledByTreePicker ? true : false);
            bbiRememberDetails.Enabled = (this.strFormMode == "add" || this.strFormMode == "edit" || ibool_CalledByTreePicker ? true : false);
            bbiShowMapButton.Enabled = (this.strFormMode != "blockedit" || ibool_CalledByTreePicker ? true : false);
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2.Name == "intLocalityIDGridLookUpEdit" || item2.Name == "intOwnershipIDGridLookUpEdit" || item2.Name == "DistrictNameTextEdit") continue; // These 3 filed ingnore and Generic_EditChanged code also copied into their Edit_Changed event - previous weird bug on block editing, had to select locality twice before it would be accepted! //
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        DataTable data;  // Used for holding filtered dataset for picklists //
        private void Attach_Enter_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter += new EventHandler(Filter_GridView);
            }
        }

        private void Detach_Enter_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter -= new EventHandler(Filter_GridView);
            }
        }

        private void Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            int intHeaderID = Convert.ToInt32(glue.Tag);
            data = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks;
            DataView newView = new DataView(data);
            newView.RowFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            glue.Properties.DataSource = newView;
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        private void Attach_Leave_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave += new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Detach_Leave_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave -= new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Clear_Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            glue.Properties.DataSource = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks.DefaultView;
            view.EndUpdate();
        }
        
        
        private void Attach_ButtonClick_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void Detach_ButtonClick_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick -= new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void ButtonClick_GridView(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    int intHeaderID = Convert.ToInt32(glue.Tag);
                    String strPickListName = "";
                    switch (glue.Name)
                    {
                        case "intSizeGridLookUpEdit":
                            strPickListName = "Sizes";
                            break;
                        case "intDBHRangeGridLookUpEdit":
                            strPickListName = "DBH Bands";
                            break;
                        case "intHeightRangeGridLookUpEdit":
                            strPickListName = "Height Bands";
                            break;
                        case "intLegalStatusGridLookUpEdit":
                            strPickListName = "Legal Statuses";
                            break;
                        case "intAgeClassGridLookUpEdit":
                            strPickListName = "Age Classes";
                            break;
                        case "RetentionCategoryGridLookUpEdit":
                            strPickListName = "Retention Categories";
                            break;
                        case "intSafetyPriorityGridLookUpEdit":
                            strPickListName = "Risk Categories";
                            break;
                        case "intSiteHazardClassGridLookUpEdit":
                            strPickListName = "Site Hazard Classes";
                            break;
                        case "intNearbyObjectGridLookUpEdit":
                            strPickListName = "Nearby Objects";
                            break;
                        case "intNearbyObject2GridLookUpEdit":
                            strPickListName = "Nearby Objects";
                            break;
                        case "intNearbyObject3GridLookUpEdit":
                            strPickListName = "Nearby Objects";
                            break;
                        case "intAccessGridLookUpEdit":
                            strPickListName = "Accessibility";
                            break;
                        case "intVisibilityGridLookUpEdit":
                            strPickListName = "Visibility";
                            break;
                        case "intPlantSizeGridLookUpEdit":
                            strPickListName = "Plant Sizes";
                            break;
                        case "intPlantSourceGridLookUpEdit":
                            strPickListName = "Plant Sources";
                            break;
                        case "intPlantMethodGridLookUpEdit":
                            strPickListName = "Plant Methods";
                            break;
                        case "intGroundTypeGridLookUpEdit":
                            strPickListName = "Ground Conditions";
                            break;
                        case "intProtectionTypeGridLookUpEdit":
                            strPickListName = "Protection Types";
                            break;
                        case "UserPickList1GridLookUpEdit":
                            strPickListName = "Tree User Defined 1";
                            break;
                        case "UserPickList2GridLookUpEdit":
                            strPickListName = "Tree User Defined 2";
                            break;
                        case "UserPickList3GridLookUpEdit":
                            strPickListName = "Tree User Defined 3";
                            break;
                        default:
                            break;
                    }
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, intHeaderID, strPickListName);
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
                    this.ValidateChildren();
                }
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Tree", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
            
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();  // These 2 lines at start instead of end of event so CAVAT function doesn't fail //
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        strTreeRefButtonEdit.Focus();

                        strTreeRefButtonEdit.Properties.ReadOnly = false;
                        strTreeRefButtonEdit.Properties.Buttons[0].Enabled = true;
                        strTreeRefButtonEdit.Properties.Buttons[1].Enabled = true;

                        idTextEdit.Properties.ReadOnly = false;
                        strMapLabelTextEdit.Properties.ReadOnly = false;
                        intXSpinEdit.Properties.ReadOnly = false;
                        intYSpinEdit.Properties.ReadOnly = false;
                        strPolygonXYTextEdit.Properties.ReadOnly = false;

                        LocationXSpinEdit.Properties.ReadOnly = false;
                        LocationYSpinEdit.Properties.ReadOnly = false;
                        LocationPolyXYMemoEdit.Properties.ReadOnly = false;

                        gridControl1.Enabled = true;
                        gridControl3.Enabled = true;
                        gridControl4.Enabled = true;
                        gridControl39.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        intTypeGridLookUpEdit.Focus();

                        strTreeRefButtonEdit.Properties.ReadOnly = true;
                        strTreeRefButtonEdit.Properties.Buttons[0].Enabled = false;
                        strTreeRefButtonEdit.Properties.Buttons[1].Enabled = false;

                        idTextEdit.Properties.ReadOnly = true;
                        strMapLabelTextEdit.Properties.ReadOnly = true;
                        intXSpinEdit.Properties.ReadOnly = true;
                        intYSpinEdit.Properties.ReadOnly = true;
                        strPolygonXYTextEdit.Properties.ReadOnly = true;

                        LocationXSpinEdit.Properties.ReadOnly = true;
                        LocationYSpinEdit.Properties.ReadOnly = true;
                        LocationPolyXYMemoEdit.Properties.ReadOnly = true;
                        
                        gridControl1.Enabled = false;
                        gridControl3.Enabled = false;
                        gridControl4.Enabled = false;
                        gridControl39.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        intTypeGridLookUpEdit.Focus();

                        strTreeRefButtonEdit.Properties.ReadOnly = false;
                        strTreeRefButtonEdit.Properties.Buttons[0].Enabled = true;
                        strTreeRefButtonEdit.Properties.Buttons[1].Enabled = true;
                        
                        idTextEdit.Properties.ReadOnly = false;
                        strMapLabelTextEdit.Properties.ReadOnly = false;
                        intXSpinEdit.Properties.ReadOnly = false;
                        intYSpinEdit.Properties.ReadOnly = false;
                        strPolygonXYTextEdit.Properties.ReadOnly = false;

                        LocationXSpinEdit.Properties.ReadOnly = false;
                        LocationYSpinEdit.Properties.ReadOnly = false;
                        LocationPolyXYMemoEdit.Properties.ReadOnly = false;

                        gridControl1.Enabled = true;
                        gridControl3.Enabled = true;
                        gridControl4.Enabled = true;
                        gridControl39.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        intTypeGridLookUpEdit.Focus();

                        strTreeRefButtonEdit.Properties.ReadOnly = true;
                        strTreeRefButtonEdit.Properties.Buttons[0].Enabled = true;
                        strTreeRefButtonEdit.Properties.Buttons[1].Enabled = false;
                        
                        idTextEdit.Properties.ReadOnly = true;
                        strMapLabelTextEdit.Properties.ReadOnly = true;
                        intXSpinEdit.Properties.ReadOnly = true;
                        intYSpinEdit.Properties.ReadOnly = true;
                        strPolygonXYTextEdit.Properties.ReadOnly = true;

                        LocationXSpinEdit.Properties.ReadOnly = true;
                        LocationYSpinEdit.Properties.ReadOnly = true;
                        LocationPolyXYMemoEdit.Properties.ReadOnly = true;

                        gridControl1.Enabled = false;
                        gridControl3.Enabled = false;
                        gridControl4.Enabled = false;
                        gridControl39.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetEditorButtons();
            SetChangesPendingLabel();

            if (strFormMode == "blockedit" || intTypeGridLookUpEdit.EditValue == DBNull.Value || Convert.ToInt32(intTypeGridLookUpEdit.EditValue) != 1)  // 0 = Tree, 1 = Hedge, 2 = Tree Group //
            {

                ObjectLengthSpinEdit.Properties.ReadOnly = true;
                ObjectWidthSpinEdit.Properties.ReadOnly = true;
            }
            else
            {
                ObjectLengthSpinEdit.Properties.ReadOnly = false;
                ObjectWidthSpinEdit.Properties.ReadOnly = false;
            }

            // Configure CAVAT //
            if (strCavatOn == "On")
            {
                if (strCavatMethod == "Full")  // display Full Fields and Hide Quick Fields //
                {
                    CavatSpinEdit.Properties.ReadOnly = true;

                    CavatStemDiameterSpinEdit.Enabled = true;
                    CavatUnitValueFactorSpinEdit.Enabled = true;
                    CavatBasicValueTextEdit.Enabled = true;
                    CavatCTIFactorGridLookUpEdit.Enabled = true;
                    CavatAccessibilityGridLookUpEdit.Enabled = true;
                    CavatCTIValueTextEdit.Enabled = true;
                    CavatFunctionalValueFactorGridLookUpEdit.Enabled = true;
                    CavatFunctionalValueTextEdit.Enabled = true;
                    CavatAmenityFactorsGridLookUpEdit.Enabled = true;
                    CavatAppropriatenessGridLookUpEdit.Enabled = true;
                    CavatAdjustedPercentSpinEdit.Enabled = true;
                    CavatAdjustedValueTextEdit.Enabled = true;
                    CavatSLEFactorGridLookUpEdit.Enabled = true;

                    CavatStemDiameterSpinEdit2.Enabled = false;
                    CavatUnitValueFactorSpinEdit2.Enabled = false;
                    CavatCTIFactorGridLookUpEdit2.Enabled = false;
                    CavatCTIValueTextEdit2.Enabled = false;
                    CavatFunctionalValueFactorGridLookUpEdit2.Enabled = false;
                    CavatFunctionalValueTextEdit2.Enabled = false;
                    CavatSLEFactorGridLookUpEdit2.Enabled = false;
                }
                else // "Quick" - display Quick Fields and Hide Full Fields //
                {
                    CavatSpinEdit.Properties.ReadOnly = true;

                    CavatStemDiameterSpinEdit.Enabled = false;
                    CavatUnitValueFactorSpinEdit.Enabled = false;
                    CavatBasicValueTextEdit.Enabled = false;
                    CavatCTIFactorGridLookUpEdit.Enabled = false;
                    CavatAccessibilityGridLookUpEdit.Enabled = false;
                    CavatCTIValueTextEdit.Enabled = false;
                    CavatFunctionalValueFactorGridLookUpEdit.Enabled = false;
                    CavatFunctionalValueTextEdit.Enabled = false;
                    CavatAmenityFactorsGridLookUpEdit.Enabled = false;
                    CavatAppropriatenessGridLookUpEdit.Enabled = false;
                    CavatAdjustedPercentSpinEdit.Enabled = false;
                    CavatAdjustedValueTextEdit.Enabled = false;
                    CavatSLEFactorGridLookUpEdit.Enabled = false;

                    CavatStemDiameterSpinEdit2.Enabled = true;
                    CavatUnitValueFactorSpinEdit2.Enabled = true;
                    CavatCTIFactorGridLookUpEdit2.Enabled = true;
                    CavatCTIValueTextEdit2.Enabled = true;
                    CavatFunctionalValueFactorGridLookUpEdit2.Enabled = true;
                    CavatFunctionalValueTextEdit2.Enabled = true;
                    CavatSLEFactorGridLookUpEdit2.Enabled = true;
                }
            }
            else  // Off - Hide Full and Quick Fields but make CAVAT Field editable //
            {
                CavatSpinEdit.Properties.ReadOnly = false;

                CavatStemDiameterSpinEdit.Enabled = false;
                CavatUnitValueFactorSpinEdit.Enabled = false;
                CavatBasicValueTextEdit.Enabled = false;
                CavatCTIFactorGridLookUpEdit.Enabled = false;
                CavatAccessibilityGridLookUpEdit.Enabled = false;
                CavatCTIValueTextEdit.Enabled = false;
                CavatFunctionalValueFactorGridLookUpEdit.Enabled = false;
                CavatFunctionalValueTextEdit.Enabled = false;
                CavatAmenityFactorsGridLookUpEdit.Enabled = false;
                CavatAppropriatenessGridLookUpEdit.Enabled = false;
                CavatAdjustedPercentSpinEdit.Enabled = false;
                CavatAdjustedValueTextEdit.Enabled = false;
                CavatSLEFactorGridLookUpEdit.Enabled = false;

                CavatStemDiameterSpinEdit2.Enabled = false;
                CavatUnitValueFactorSpinEdit2.Enabled = false;
                CavatCTIFactorGridLookUpEdit2.Enabled = false;
                CavatCTIValueTextEdit2.Enabled = false;
                CavatFunctionalValueFactorGridLookUpEdit2.Enabled = false;
                CavatFunctionalValueTextEdit2.Enabled = false;
                CavatSLEFactorGridLookUpEdit2.Enabled = false;
            }
            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            intOwnershipIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intOwnershipIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intOwnershipIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intOwnershipIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intSpeciesIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intSpeciesIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intSpeciesIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intSpeciesIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            SpeciesVarietyGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            SpeciesVarietyGridLookUpEdit.Properties.Buttons[1].Visible = false;
            SpeciesVarietyGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            SpeciesVarietyGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intSizeGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intSizeGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intSizeGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intSizeGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intDBHRangeGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intDBHRangeGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intDBHRangeGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intDBHRangeGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intHeightRangeGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intHeightRangeGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intHeightRangeGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intHeightRangeGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intLegalStatusGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intLegalStatusGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intLegalStatusGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intLegalStatusGridLookUpEdit.Properties.Buttons[2].Visible = false;
            RetentionCategoryGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            RetentionCategoryGridLookUpEdit.Properties.Buttons[1].Visible = false;
            RetentionCategoryGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            RetentionCategoryGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intAgeClassGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intAgeClassGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intAgeClassGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intAgeClassGridLookUpEdit.Properties.Buttons[2].Visible = false;
            strContextPopupContainerEdit.Properties.Buttons[1].Enabled = false;
            strContextPopupContainerEdit.Properties.Buttons[1].Visible = false;
            strContextPopupContainerEdit.Properties.Buttons[2].Enabled = false;
            strContextPopupContainerEdit.Properties.Buttons[2].Visible = false;
            strManagementPopupContainerEdit.Properties.Buttons[1].Enabled = false;
            strManagementPopupContainerEdit.Properties.Buttons[1].Visible = false;
            strManagementPopupContainerEdit.Properties.Buttons[2].Enabled = false;
            strManagementPopupContainerEdit.Properties.Buttons[2].Visible = false;
            intSafetyPriorityGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intSafetyPriorityGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intSafetyPriorityGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intSafetyPriorityGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intSiteHazardClassGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intSiteHazardClassGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intSiteHazardClassGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intSiteHazardClassGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intNearbyObjectGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intNearbyObjectGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intNearbyObjectGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intNearbyObjectGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intNearbyObject2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intNearbyObject2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intNearbyObject2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intNearbyObject2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intNearbyObject3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intNearbyObject3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            intNearbyObject3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intNearbyObject3GridLookUpEdit.Properties.Buttons[2].Visible = false;
            intAccessGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intAccessGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intAccessGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intAccessGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intVisibilityGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intVisibilityGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intVisibilityGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intVisibilityGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intPlantSizeGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intPlantSizeGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intPlantSizeGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intPlantSizeGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intPlantSourceGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intPlantSourceGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intPlantSourceGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intPlantSourceGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intPlantMethodGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intPlantMethodGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intPlantMethodGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intPlantMethodGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intGroundTypeGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intGroundTypeGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intGroundTypeGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intGroundTypeGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intProtectionTypeGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intProtectionTypeGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intProtectionTypeGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intProtectionTypeGridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "2007,2012,8002,9022,9036,9035,9014,9039,9023,9024,9025,9003,9018,9032,9012,9013,9019,9021,9020,9015,9016,9041,9042,9043", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 2012:  // Ownerships/Budgets //    
                        {
                            intOwnershipIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intOwnershipIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intOwnershipIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intOwnershipIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 8002:  // Species Manager //    
                        {
                            intSpeciesIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intSpeciesIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intSpeciesIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intSpeciesIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            SpeciesVarietyGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            SpeciesVarietyGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            SpeciesVarietyGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            SpeciesVarietyGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9022:  // Size Picklist //    
                        {
                            intSizeGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intSizeGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intSizeGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intSizeGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9036:  // DBH Band Picklist //    
                        {
                            intDBHRangeGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intDBHRangeGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intDBHRangeGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intDBHRangeGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9035:  // Height Band Picklist //    
                        {
                            intHeightRangeGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intHeightRangeGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intHeightRangeGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intHeightRangeGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9014:  // Legal Status Picklist //    
                        {
                            intLegalStatusGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intLegalStatusGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intLegalStatusGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intLegalStatusGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9039:  // Retention Category Picklist //    
                        {
                            RetentionCategoryGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            RetentionCategoryGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            RetentionCategoryGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            RetentionCategoryGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9023:  // Age Class Picklist //    
                        {
                            intAgeClassGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intAgeClassGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intAgeClassGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intAgeClassGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9024:  // Landscape Context Picklist //    
                        {
                            strContextPopupContainerEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            strContextPopupContainerEdit.Properties.Buttons[1].Visible = boolUpdate;
                            strContextPopupContainerEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            strContextPopupContainerEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9025:  // Management Picklist //    
                        {
                            strManagementPopupContainerEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            strManagementPopupContainerEdit.Properties.Buttons[1].Visible = boolUpdate;
                            strManagementPopupContainerEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            strManagementPopupContainerEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9003:  // Risk Category Picklist //    
                        {
                            intSafetyPriorityGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intSafetyPriorityGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intSafetyPriorityGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intSafetyPriorityGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9018:  // Site Hazard Class Picklist //    
                        {
                            intSiteHazardClassGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intSiteHazardClassGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intSiteHazardClassGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intSiteHazardClassGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9032:  // Site Hazard Class Picklist //    
                        {
                            intNearbyObjectGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intNearbyObjectGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intNearbyObjectGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intNearbyObjectGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intNearbyObject2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intNearbyObject2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intNearbyObject2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intNearbyObject2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intNearbyObject3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intNearbyObject3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intNearbyObject3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intNearbyObject3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9012:  // Accessibility Picklist //    
                        {
                            intAccessGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intAccessGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intAccessGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intAccessGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9013:  // Visibility Picklist //    
                        {
                            intVisibilityGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intVisibilityGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intVisibilityGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intVisibilityGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9019:  // Plant Size Picklist //    
                        {
                            intPlantSizeGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intPlantSizeGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intPlantSizeGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intPlantSizeGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9021:  // Plant Source Picklist //    
                        {
                            intPlantSourceGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intPlantSourceGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intPlantSourceGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intPlantSourceGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9020:  // Plant Method Picklist //    
                        {
                            intPlantMethodGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intPlantMethodGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intPlantMethodGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intPlantMethodGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9015:  // Ground Type Picklist //    
                        {
                            intGroundTypeGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intGroundTypeGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intGroundTypeGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intGroundTypeGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9016:  // Protection Type Picklist //    
                        {
                            intProtectionTypeGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intProtectionTypeGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intProtectionTypeGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intProtectionTypeGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9041:  //  Tree User Defined 1 Picklist //    
                        {
                            UserPickList1GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9042:  // Tree User Defined 2 Picklist //    
                        {
                            UserPickList2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9043:  // Tree User Defined 3 Picklist //    
                        {
                            UserPickList3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                      
                }
            }
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_AT_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;
            bbiBlockAddAction.Enabled = false;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intTreeID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
            if (currentRow != null) intTreeID = (currentRow["intTreeID"] == null ? 0 : Convert.ToInt32(currentRow["intTreeID"]));
           
            if (i_int_FocusedGrid == 1)  // Inspections //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intTreeID > 0 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                    if (intRowHandles.Length >= 1)
                    {
                        bbiBlockAddAction.Enabled = true;
                    }
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
           
            else if (i_int_FocusedGrid == 2)  // Actions //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intTreeID > 0 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Linked Documents //
            {
                view = (GridView)gridControl4.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intTreeID > 0 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 39)  // Linked Pictures //
            {
                view = (GridView)gridControl39.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intTreeID > 0 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd && intTreeID > 0 && strFormMode != "view")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit && strFormMode != "view")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete && strFormMode != "view")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            
            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd && intTreeID > 0 && strFormMode != "view")
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit && strFormMode != "view")
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete && strFormMode != "view")
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd && intTreeID > 0 && strFormMode != "view")
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit && strFormMode != "view")
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete && strFormMode != "view")
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView39 navigator custom buttons //
            view = (GridView)gridControl39.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intTreeID > 0 && strFormMode != "view" ? true : false);
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && strFormMode != "view" && intRowHandles.Length == 1 ? true : false);
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }


        private void frm_AT_Tree_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                LoadLinkedRecords();
            }
            SetMenuStatus();
         }

        private void frm_AT_Tree_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();

                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        Detach_Enter_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Enter Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_Leave_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Leave Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_ButtonClick_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach ButtonClick Event from All Filtered GridLookUpEdits... Attached on Form Load Event //

                        if (strCavatOn == "On")
                        {
                            Detach_EditValueChanged_From_CavatGridLookupEdits(ArrayListFilteredCavatLists);  // Detach EditValueChanged Event to All CAVAT GridLookUpEdits... Attached on Form Load Event //
                            Detach_Validated_From_CavatGridLookupEdits(ArrayListFilteredCavatLists);  // Detach Validated Event to All CAVAT GridLookUpEdits... Attached on Form Load Event //
                        }

                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }
        

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp01204ATEditTreeDetailsBindingSource.EndEdit();
            try
            {
                this.sp01204_AT_Edit_Tree_DetailsTableAdapter.Update(dataSet_AT_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["intTreeID"]) + ";";
                
                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Pick up all Map IDs and concatenate together so Tree Picker can be updated if it is running //
            string strUpdateIDs = "";
            if (this.strFormMode.ToLower() == "blockedit")
            {
                strUpdateIDs = strMapIDs;
            }
            else
            {
                foreach (DataRow dr in this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.Rows)
                {
                    if (!string.IsNullOrEmpty(dr["id"].ToString()))
                    {
                        strUpdateIDs += dr["id"].ToString() + ",";
                    }
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_AT_Tree_Manager")
                    {
                        var fParentForm = (frm_AT_Tree_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "");
                    }
                    if (frmChild.Name == "frm_AT_Mapping_Tree_Picker" && !string.IsNullOrEmpty(strUpdateIDs))
                    {
                        var fParentForm = (frm_AT_Mapping_Tree_Picker)frmChild;
                        fParentForm.RefreshMapObjects(strUpdateIDs, 0);
                    }
                    if (frmChild.Name == "frm_AT_Inspection_Manager")
                    {
                        var fParentForm = (frm_AT_Inspection_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

        private void Store_Last_Saved_Record_Details(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit)
                {
                    string strName = ((DevExpress.XtraEditors.BaseEdit)item2).Name;
                    string strText = "";
                    if (strName == "strTreeRefButtonEdit")
                    {
                        if (i_strLastUsedSequencePrefix != "")
                        {
                            strText = i_strLastUsedSequencePrefix;
                        }
                        else
                        {
                            continue;  // Ignore Sequence if empty //
                        }
                    }
                    else if (strName == "DateAddedDateEdit" || strName == "intTreeIDTextEdit" || strName == "intTypeGridLookUpEdit" || strName == "idTextEdit" || strName == "strMapLabelTextEdit" || strName == "intXSpinEdit" || strName == "intYSpinEdit" || strName == "strPolygonXYTextEdit" || strName == "ObjectLengthSpinEdit" || strName == "ObjectWidthSpinEdit" || strName == "decAreaHaSpinEdit")
                    {
                        continue;  // Ignore Record and Mapping Fields //
                    }
                    else
                    {
                        strText = (((DevExpress.XtraEditors.BaseEdit)item2).EditValue == null ? null : ((DevExpress.XtraEditors.BaseEdit)item2).EditValue.ToString());
                    }
                    DataRow[] drFiltered;
                    drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = '" + strName + "'");
                    if (drFiltered.Length != 0)  // Matching row found so Update it //
                    {
                        DataRow drExistingRow = drFiltered[0];
                        drExistingRow["ItemValue"] = strText;
                    }
                    else  // No Matching row so Add it to DataSet //
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.NewRow();
                        drNewRow["ScreenID"] = FormID;
                        drNewRow["UserID"] = this.GlobalSettings.UserID;
                        drNewRow["ItemName"] = strName;
                        drNewRow["ItemValue"] = strText;
                        this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Rows.Add(drNewRow);
                    }
                }
                if (item2 is ContainerControl) Store_Last_Saved_Record_Details(item.Controls);
            }
            try
            {
                this.sp01375_AT_User_Screen_SettingsTableAdapter.Update(dataSet_AT_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("An error occurred while copying the tree details [" + ex.Message + "]!\n\nTry copying again - if the problem persists, contact Technical Support.", "Copy Tree Details", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.Rows.Count; i++)
            {
                switch (this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiShowMapButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
            if (currentRow == null) return;
            if (String.IsNullOrEmpty(currentRow["intTreeID"].ToString()) || currentRow["intTreeID"].ToString() == "0")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Save the current record before attempting to display it on the map.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = currentRow["intTreeID"].ToString() + ',';
            try
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "tree");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void bbiCopyAllValues_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Control ctrlOnForm = null;
            string strName = "";
            string strText = "";
            bool boolSequenceUpdated = false;
            dataLayoutControl1.BeginUpdate();
            foreach (DataRow dr in dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Rows)
            {
                strName = dr["ItemName"].ToString();
                strText = dr["ItemValue"].ToString();
                Control[] controls = this.Controls.Find(strName, true);
                if (controls.Length == 1)  // Only proceed if just one control was found matching the name // 
                {
                    ctrlOnForm = controls[0] as Control;
                    if (ctrlOnForm is DevExpress.XtraEditors.BaseEdit)
                    {
                        if (strName == "strTreeRefButtonEdit")
                        {
                            boolSequenceUpdated = true;
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = "";  // Clear any sequence first //
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                        }
                        if (string.IsNullOrEmpty(strText))
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = DBNull.Value;
                        }
                        else
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = strText;
                        }
                    }
                }              
            }
            // Calculate Next Sequence Number by triggering button click event of strTreeRefButtonEdit //
            if (!string.IsNullOrEmpty(strTreeRefButtonEdit.EditValue.ToString()) && boolSequenceUpdated) strTreeRefButtonEdit_ButtonClick(strTreeRefButtonEdit, new DevExpress.XtraEditors.Controls.ButtonPressedEventArgs(strTreeRefButtonEdit.Properties.Buttons[1]));

            dataLayoutControl1.EndUpdate();
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
        }

        private void bbiCopySelectedValues_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Loop round all objects in DataLayout Control and store the child objects and the Layout Captions in HashTable //
            Hashtable htObjects = new Hashtable();
            foreach (Control item in this.Controls)
            {
                if (item is DevExpress.XtraLayout.LayoutControl)
                {
                    DevExpress.XtraLayout.LayoutControl item2 = (DevExpress.XtraLayout.LayoutControl)item;
                    foreach (object obj in item2.Items)
                    {
                        if (obj is DevExpress.XtraLayout.LayoutControlItem)
                        {
                            DevExpress.XtraLayout.LayoutControlItem lci = (DevExpress.XtraLayout.LayoutControlItem)obj;
                            if (lci.Control != null) htObjects.Add(lci.Control.Name, lci.Text);
                        }
                    }
                }
            }

            frm_Core_Recall_Selected_Values fChildForm = new frm_Core_Recall_Selected_Values();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intPassedInScreenID = this.FormID;
            fChildForm.htPassedInObjects = htObjects;
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                htObjects = fChildForm.htPassedInObjects;
            }

            // Process the updated HashTable //
            Control ctrlOnForm = null;
            string strName = "";
            string strText = "";
            bool boolSequenceUpdated = false;
            dataLayoutControl1.BeginUpdate();
            IDictionaryEnumerator enumerator = htObjects.GetEnumerator();
            while (enumerator.MoveNext())
            {
                strName = enumerator.Key.ToString();
                strText = enumerator.Value.ToString();
                Control[] controls = this.Controls.Find(strName, true);
                if (controls.Length == 1)  // Only proceed if just one control was found matching the name // 
                {
                    ctrlOnForm = controls[0] as Control;
                    if (ctrlOnForm is DevExpress.XtraEditors.BaseEdit)
                    {
                        if (strName == "strTreeRefButtonEdit")
                        {
                            boolSequenceUpdated = true;
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = "";  // Clear any sequence first //
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                        }
                        if (string.IsNullOrEmpty(strText))
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = DBNull.Value;
                        }
                        else
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = strText;
                        }
                    }
                }
            }
            // Calculate Next Sequence Number by triggering button click event of strTreeRefButtonEdit //
            if (!string.IsNullOrEmpty(strTreeRefButtonEdit.EditValue.ToString()) && boolSequenceUpdated) strTreeRefButtonEdit_ButtonClick(strTreeRefButtonEdit, new DevExpress.XtraEditors.Controls.ButtonPressedEventArgs(strTreeRefButtonEdit.Properties.Buttons[1]));

            dataLayoutControl1.EndUpdate();
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //


        }

        private void bbiReload_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Reload Last Saved Record Details into memory//
            sp01375_AT_User_Screen_SettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01375_AT_User_Screen_SettingsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings, this.GlobalSettings.UserID, FormID);
        }

        private void bbiRememberDetails_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save records attributes to User_Settings table for later recall //
            DevExpress.Utils.WaitDialogForm loading = new DevExpress.Utils.WaitDialogForm("Storing Details to Memory...", "Trees");
            loading.Show();
            Store_Last_Saved_Record_Details(this.Controls);
            XtraMessageBox.Show("Details Copied.", "Copy Tree Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
            loading.Hide();
            loading.Dispose();
        }


        private void CheckInspectionsForLastInspectionDate()
        {
            // Check if Inspection with the most recent date is the same as that on the Tree's Last Inspection Date. If not, update the Tree's Date //
            DateTime dtLastDate = DateTime.MinValue;
            if (gridControl1.MainView.DataRowCount > 0)
            {
                foreach (DataRow dr in this.dataSet_AT_DataEntry.sp01367_AT_Inspections_For_Trees.Rows)
                {
                    if (!string.IsNullOrEmpty(dr["InspectionDate"].ToString()))
                    {
                        if (Convert.ToDateTime(dr["InspectionDate"]) > dtLastDate) dtLastDate = Convert.ToDateTime(dr["InspectionDate"]);
                    }
                }
                if (String.IsNullOrEmpty(dtLastInspectionDateDateEdit.DateTime.ToString()) || (dtLastInspectionDateDateEdit.DateTime != dtLastDate) && dtLastDate != DateTime.MinValue)
                {
                    dtLastInspectionDateDateEdit.EditValue = dtLastDate;
                    Calc_Next_Date(false);
                }
            }
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            
            string strTreeID = "";
            DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
            if (currentRow != null)
            {
                strTreeID = (currentRow["intTreeID"] == null ? "" : currentRow["intTreeID"].ToString() + ",");
            }

            // Inspections //
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();         
            sp01367_AT_Inspections_For_TreesTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01367_AT_Inspections_For_Trees, strTreeID, null, null, 0);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();
            
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["InspectionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
            CheckInspectionsForLastInspectionDate();// Check if Inspection with the most recent date is the same as that on the Tree's Last Inspection Date. If not, update the Tree's Date //

            
            // Actions //
            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
            gridControl3.BeginUpdate();
            sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01380_AT_Actions_Linked_To_Inspections,strTreeID, 1, null, null, 0);
            this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl3.EndUpdate();
            
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }
            
            // Linked Documents //
            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
            gridControl4.BeginUpdate();
            sp00220_Linked_Documents_ListTableAdapter.Fill(this.dataSet_AT.sp00220_Linked_Documents_List, strTreeID, 3, strDefaultPath);
            this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl4.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl4.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }
        }

        private void Load_Linked_Pictures()
        {
            string strTreeID = "";
            DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
            if (currentRow != null)
            {
                strTreeID = (currentRow["intTreeID"] == null ? "" : currentRow["intTreeID"].ToString() + ",");
            }
            gridControl39.BeginUpdate();
            this.RefreshGridViewState39.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp02064_AT_Tree_Pictures_ListTableAdapter.Fill(dataSet_AT.sp02064_AT_Tree_Pictures_List, strTreeID, strDefaultPicturesPath);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState39.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl39.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs39 != "")
            {
                strArray = i_str_AddedRecordIDs39.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl39.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyPictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs39 = "";
            }
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Set_Object_Length_And_Width_ReadOnly_Status(intTypeGridLookUpEdit);
                }
            }
            if (this.strFormMode != "blockedit")
            {
                LoadLinkedRecords();
                GridView view = (GridView)gridControl1.MainView;
                view.ExpandAllGroups();
                view = (GridView)gridControl3.MainView;
                view.ExpandAllGroups();
                view = (GridView)gridControl4.MainView;
                view.ExpandAllGroups();
                Load_Linked_Pictures();
                view = (GridView)gridControl39.MainView;
                view.ExpandAllGroups();
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void SiteNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                var currentRow = (DataSet_AT_DataEntry.sp01204_AT_Edit_Tree_DetailsRow)currentRowView.Row;
                if (currentRow == null) return;
                int intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                int intSiteID = (string.IsNullOrEmpty(currentRow.SiteID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteID));
                
                var fChildForm = new frm_Core_Select_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalClientID = intClientID;
                fChildForm.intOriginalSiteID = intSiteID;
                fChildForm.intFilterUtilityArbClient = 1;  // Show only clients with UtilityARB ticked //
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    if (currentRow != null)
                    {
                        currentRow["SiteID"] = fChildForm.intSelectedID;
                        currentRow["SiteName"] = fChildForm.strSelectedValue;
                        currentRow["ClientID"] = fChildForm.intSelectedClientID;
                        currentRow["ClientName"] = fChildForm.strSelectedClientName;
                        currentRow["SiteCode"] = fChildForm.strSelectedSiteCode;
                        sp01204ATEditTreeDetailsBindingSource.EndEdit();
                    }
                }
            }
        }
        private void SiteNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(SiteNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SiteNameButtonEdit, "");
            }
        }

        private void strTreeRefButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            string strTableName = "tblTree";
            string strFieldName = "strTreeRef";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {

                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                int? intNumericLength = 7;
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    intNumericLength = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_Sequence_From_Locality_Length").ToString());
                    if (string.IsNullOrEmpty(intNumericLength.ToString())) intNumericLength = 7;
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while retrieving the length of sequence numbers generated from Locality Codes.\nCheck Your System Settings table to ensure a value has been set.", "Get Sequence", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                string strSeperator = "";
                try
                {
                    strSeperator = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_Sequence_From_Locality_Seperator").ToString();
                    if (string.IsNullOrEmpty(strSeperator)) strSeperator = "";
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while retrieving the sequence seperator.\nCheck Your System Settings table to ensure a value has been set.", "Get Sequence", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                int intAmountToTrim = 0;
                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    if (fChildForm.SelectedSequenceID == -1) // Site Code seed //
                    {
                        string strSiteCode = "";
                        if (strFormMode != "blockedit")
                        {
                            var currentRowView = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                            var currentRow = (DataSet_AT_DataEntry.sp01204_AT_Edit_Tree_DetailsRow)currentRowView.Row;
                            if (currentRow == null) return;
                            strSiteCode = (string.IsNullOrEmpty(currentRow.SiteCode.ToString()) ? "" : currentRow.SiteCode);
                            if (String.IsNullOrEmpty(strSiteCode) || strSiteCode == "N\\A")
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to get a site code for the current record\nTry selecting a Site first.", "Get Sequence Seed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;  // Abort - No Locality Code found] //
                            }
                            intAmountToTrim = (strSiteCode.Length + (int)intNumericLength) + strSeperator.Length - 20;
                            if (intAmountToTrim > 0) strSiteCode = strSiteCode.Substring(0, strSiteCode.Length - intAmountToTrim);
                            strSiteCode += strSeperator;
                            strTreeRefButtonEdit.EditValue = "?" + intNumericLength.ToString() + strSiteCode;  // Just First x Digits preceeding by a question mark and x numeric length so Get Sequence SP can treat it differently //
                        }
                        else  // Block Editing so insert control value which can by pickedup by the Update Stored Proc, and it can resolve the sequences //
                        {
                            strTreeRefButtonEdit.EditValue =  "SiteCode" + intNumericLength.ToString() + strSeperator;
                        }
                    }
                    else
                    {
                        strTreeRefButtonEdit.EditValue = fChildForm.SelectedSequence;
                    }
                }

            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = strTreeRefButtonEdit.EditValue.ToString() ?? "";

                if (this.strFormMode == "add" || ibool_CalledByTreePicker) i_strLastUsedSequencePrefix = strSequence;  // Store Sequence Prefix for saving against Last Saved Record //

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";


                // Check if value is already present within dataset //
                string strCurrentTreeID = intTreeIDTextEdit.EditValue.ToString();
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_AT_DataEntry.sp01204_AT_Edit_Tree_Details.Rows)
                    {
                        if (dr["strTreeRef"].ToString() == (strSequence + strTempNumber))// && dr["intTreeID"].ToString() != strCurrentTreeID)
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                strTreeRefButtonEdit.EditValue = strSequence + strTempNumber;
            }

        }

        private void strTreeRefButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(strTreeRefButtonEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(strTreeRefButtonEdit, "");
            }
        }

        private void strPolygonXYTextEdit_Validating(object sender, CancelEventArgs e)
        {
            MemoEdit me = (MemoEdit)sender;
            string[] strArray;
            char[] delimiters = new char[] { ';' };
            strArray = me.EditValue.ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length > 0)
            {
                if (strArray.Length % 2 != 0)  // Modulus (%) //
                {
                    dxErrorProvider1.SetError(strPolygonXYTextEdit, "Invalid number of coordinates - must be an even number.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
                else if (strArray.Length < 6)  // was 8 but changed to 6 as polyline only needs 3 sets of coords (polygon needs at least 4 sets) //
                {
                    dxErrorProvider1.SetError(strPolygonXYTextEdit, "Invalid number of coordinates - must be an least 6.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
                else
                {
                    try
                    {
                        for (int j = 0; j < strArray.Length - 1; j++)
                        {
                            if (Convert.ToDouble(strArray[j]) < 0 || Convert.ToDouble(strArray[j]) > 0)
                            {
                                // Do Nothing - An error will be tripped in the value is not numeric anc trapped by Catch Statement //
                            }
                            if (Convert.ToDouble(strArray[j + 1]) < 0 || Convert.ToDouble(strArray[j + 1]) > 0)
                            {
                                // Do Nothing - An error will be tripped in the value is not numeric anc trapped by Catch Statement //
                            }
                            j++;  // Jump ahead so that we are always processing odd numbered array items //
                        }
                        dxErrorProvider1.SetError(strPolygonXYTextEdit, "");
                    }
                    catch (Exception)
                    {
                        dxErrorProvider1.SetError(strPolygonXYTextEdit, "Problem with entered points [must be numeric and seperated by ';'].");
                        e.Cancel = true;  // Show stop icon as field is invalid //
                        return;
                    }
                }
            }
            else
            {
                dxErrorProvider1.SetError(strPolygonXYTextEdit, "");
            }
        }

        private void intOwnershipIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 900, "Ownerships");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01362_AT_Ownership_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01362_AT_Ownership_List_With_Blank);
                }
            }
        }
        private void intOwnershipIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void intOwnershipIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never) SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
        }
        

        private void intTypeGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void intTypeGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            if (strFormMode == "blockedit") return;

            GridLookUpEdit glue = (GridLookUpEdit)sender;

            if (glue.EditValue == DBNull.Value || string.IsNullOrEmpty(glue.EditValue.ToString())) return;  // Test numeric value for null first //
            if(Convert.ToInt32(glue.EditValue) != 1)  // 0 = Tree, 1 = Hedge, 2 = Tree Group //
            {
                ObjectLengthSpinEdit.EditValue = (decimal)0.00;
                ObjectWidthSpinEdit.EditValue = (decimal)0.00;
                ObjectLengthSpinEdit.Properties.ReadOnly = true;
                ObjectWidthSpinEdit.Properties.ReadOnly = true;
            }
            else
            {
                ObjectLengthSpinEdit.Properties.ReadOnly = false;
                ObjectWidthSpinEdit.Properties.ReadOnly = false;
            }
            Set_Object_Length_And_Width_ReadOnly_Status(glue);
        }

        private void Set_Object_Length_And_Width_ReadOnly_Status(GridLookUpEdit glue)
        {
            int intValue = 0;
            if (glue.EditValue == null || string.IsNullOrEmpty(glue.EditValue.ToString()))   // Test numeric value for null first //
            {
                intValue = 0;
            }
            else
            {
                intValue = Convert.ToInt32(glue.EditValue);
            }
            if (intValue != 1)  // 0 = Tree, 1 = Hedge, 2 = Tree Group //
            {

                ObjectLengthSpinEdit.Properties.ReadOnly = true;
                ObjectWidthSpinEdit.Properties.ReadOnly = true;
            }
            else
            {
                ObjectLengthSpinEdit.Properties.ReadOnly = false;
                ObjectWidthSpinEdit.Properties.ReadOnly = false;
            }
        }

 
        private void intSpeciesIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 2, "Species");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00179_Species_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00179_Species_List_With_Blank);
                }
            }
        }
        private void intSpeciesIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //         
        }
        private void intSpeciesIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            // Check if Species Variety is valid for current species - if not, clear it //
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (glue.EditValue == DBNull.Value || glue.EditValue.ToString() == "0")
            {
                SpeciesVarietyGridLookUpEdit.EditValue = 0;  // Clear Species Variety //
                return;
            }
            int intCurrentSpeciesID = Convert.ToInt32(glue.EditValue);
            glue = (GridLookUpEdit)SpeciesVarietyGridLookUpEdit;
            GridView view = glue.Properties.View;
            int intFoundRow = 0;
            intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : view.LocateByValue(0, view.Columns["ItemID"], Convert.ToInt32(glue.EditValue)));
            if (intFoundRow != GridControl.InvalidRowHandle)
            {
                if (intCurrentSpeciesID != Convert.ToInt32(view.GetRowCellValue(intFoundRow, "SpeciesID"))) glue.EditValue = 0;
            }
        }


        private void SpeciesVarietyGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 2, "Species");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00179_Species_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00179_Species_List_With_Blank);
                }
            }
        }
        private void SpeciesVarietyGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            string strSpeciesID = intSpeciesIDGridLookUpEdit.EditValue.ToString();
            if (string.IsNullOrEmpty(strSpeciesID) || strSpeciesID == "0")
            {
                return;
            }
            else
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                GridView view = glue.Properties.View;
                view.BeginUpdate();
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[SpeciesID] = " + strSpeciesID + " or [SpeciesID] = 0";
                view.MakeRowVisible(-1, true);
                view.EndUpdate();
            }
        }
        private void SpeciesVarietyGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }
        private void SpeciesVarietyGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void SpeciesVarietyGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            string strSpeciesID = intSpeciesIDGridLookUpEdit.EditValue.ToString();
            if (string.IsNullOrEmpty(strSpeciesID) || strSpeciesID == "0")  // No species set, so pick it up from Species Variety //
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                if (glue.EditValue == DBNull.Value || string.IsNullOrEmpty(glue.EditValue.ToString())) return;

                GridView view = glue.Properties.View;
                int intFoundRow = 0;
                intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : view.LocateByValue(0, view.Columns["ItemID"], Convert.ToInt32(glue.EditValue)));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    int intSpeciesID = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "SpeciesID"));
                    intSpeciesIDGridLookUpEdit.EditValue = intSpeciesID;
                }
            }
        }

 
        private void Calc_Next_Date(bool boolShowConfirmation)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Next Inspection Date Calculation //
            ibool_ignoreValidation = true;

            if (intStatusGridLookUpEdit.EditValue == null || string.IsNullOrEmpty(intStatusGridLookUpEdit.EditValue.ToString())) return;  // Test numeric value for null first //
            int intStatus = Convert.ToInt32(intStatusGridLookUpEdit.EditValue);
            if (intStatus == 3)  // Historical so clear next inspection date //
            {
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
                this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                dtNextInspectionDateDateEdit.EditValue = null;
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
                this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                return;
            }

            if (intInspectionCycleSpinEdit.EditValue == null || string.IsNullOrEmpty(intInspectionCycleSpinEdit.EditValue.ToString())) return;  // Test numeric value for null first //
            int intCycle = Convert.ToInt32(intInspectionCycleSpinEdit.EditValue);

            string strCycleDescriptor = strInspectionUnitComboBoxEdit.EditValue.ToString();
            if (String.IsNullOrEmpty(strCycleDescriptor)) return;

            if (String.IsNullOrEmpty(dtLastInspectionDateDateEdit.DateTime.ToString()) || dtLastInspectionDateDateEdit.DateTime < Convert.ToDateTime("1900-01-01")) return;
            DateTime dtLastDate = Convert.ToDateTime(dtLastInspectionDateDateEdit.EditValue);

            DateTime dtCalculatedDate = DateTime.MinValue;

            switch (strCycleDescriptor)
            {
                case "Days":
                    dtCalculatedDate = dtLastDate.AddDays(intCycle);
                    break;
                case "Weeks":
                    dtCalculatedDate = dtLastDate.AddDays(intCycle * 7);
                    break;
                case "Months":
                    dtCalculatedDate = dtLastDate.AddMonths(intCycle);
                    break;
                case "Years":
                    dtCalculatedDate = dtLastDate.AddYears(intCycle);
                    break;
                default:
                    break;
            }
            if (!String.IsNullOrEmpty(dtNextInspectionDateDateEdit.EditValue.ToString()))
            {
                DateTime dtNextDate = Convert.ToDateTime(dtNextInspectionDateDateEdit.EditValue);
                if (dtNextDate != dtCalculatedDate && boolShowConfirmation)
                {
                    string strMessage = "The calculated " + ItemFordtNextInspectionDate.Text + " is different to the value entered in the " + ItemFordtNextInspectionDate.Text + " field!\n\nCurrent Value:\t\t" + dtNextDate.ToString() + "\nCalculated Value:\t" + dtCalculatedDate.ToString() + "\n\nDo you want to set the current value to the calculated value?";
                    if (XtraMessageBox.Show(strMessage, "Calculate Next Inspection Date", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.No) return;
                }
            }
            // Notice the the dtNextInspectionDateDateEdit update is wrapped with an End Edit before and after it - as sometimes it doesn't always do it but by doing this it solves the problem. //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            dtNextInspectionDateDateEdit.EditValue = dtCalculatedDate;
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

        }

        private void dtLastInspectionDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        }
        private void dtLastInspectionDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            Calc_Next_Date(true);
            ibool_ignoreValidation = true;
        }

        private void intInspectionCycleSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        }
        private void intInspectionCycleSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            Calc_Next_Date(true);
            ibool_ignoreValidation = true;
        }

        private void strInspectionUnitComboBoxEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        }
        private void strInspectionUnitComboBoxEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            Calc_Next_Date(true);
            ibool_ignoreValidation = true;
        }


        private void dtLastInspectionDateDateEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", ItemFordtLastInspectionDate.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dtLastInspectionDateDateEdit.EditValue = null;
                }
            }
        }

        private void dtNextInspectionDateDateEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", ItemFordtNextInspectionDate.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dtNextInspectionDateDateEdit.EditValue = null;
                }
            }
        }

        private void dtSurveyDateDateEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", ItemFordtSurveyDate.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dtSurveyDateDateEdit.EditValue = null;
                }
            }
        }

        private void dtPlantDateDateEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", ItemFordtPlantDate.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dtPlantDateDateEdit.EditValue = null;
                }
            }
        }

        private void intDBHSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void intDBHSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decCurrentValue = Convert.ToDecimal(se.EditValue);

            /* Attempt to find corresponding DBH Band *
             * Create a clone of the Binding source and filter it to find a match.
             * Don't do it on the original as the Height Band list shares it and it causes funny effects on the field */
            BindingSource newBindingSource = new BindingSource(sp01372ATMultiplePicklistsWithBlanksBindingSource.DataSource, sp01372ATMultiplePicklistsWithBlanksBindingSource.DataMember);
            newBindingSource.Filter = "";  // Clear any Prior Filter //
            newBindingSource.Filter = "HeaderID = '136' and BandStart <= '" + decCurrentValue.ToString() + "' and BandEnd >= '" + decCurrentValue.ToString() + "'";
            if (newBindingSource.Count == 1)
            {
                DataRowView currentRow = (DataRowView)newBindingSource[0];
                intDBHRangeGridLookUpEdit.EditValue = Convert.ToInt32(currentRow["ItemID"]);
                this.sp01204ATEditTreeDetailsBindingSource.EndEdit();  // Force changes to underlying dataset so this value is passed to both CAVAT Stem Diamater boxes //
            }
            Calculate_CAVAT();
        }

        private void decHeightSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void decHeightSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decCurrentValue = Convert.ToDecimal(se.EditValue);

            /* Attempt to find corresponding Height Band *
            * Create a clone of the Binding source and filter it to find a match.
            * Don't do it on the original as the DBH Band list shares it and it causes funny effects on the field */
            BindingSource newBindingSource = new BindingSource(sp01372ATMultiplePicklistsWithBlanksBindingSource.DataSource, sp01372ATMultiplePicklistsWithBlanksBindingSource.DataMember);
            newBindingSource.Filter = "";  // Clear any Prior Filter //
            newBindingSource.Filter = "HeaderID = '135' and BandStart <= '" + decCurrentValue.ToString() + "' and BandEnd >= '" + decCurrentValue.ToString() + "'";
            if (newBindingSource.Count == 1)
            {
                DataRowView currentRow = (DataRowView)newBindingSource[0];
                intHeightRangeGridLookUpEdit.EditValue = Convert.ToInt32(currentRow["ItemID"]);
                this.sp01204ATEditTreeDetailsBindingSource.EndEdit();  // Force changes to underlying dataset so this value is passed to both CAVAT Stem Diamater boxes //
            }
        }

        private void ObjectLengthSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void ObjectLengthSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            Calc_Total_Area();
        }

        private void ObjectWidthSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void ObjectWidthSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            Calc_Total_Area();
        }

        private void Calc_Total_Area()
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            if (intTypeGridLookUpEdit.EditValue == null || string.IsNullOrEmpty(intTypeGridLookUpEdit.EditValue.ToString())) return;
            if (Convert.ToInt32(intTypeGridLookUpEdit.EditValue) != 1) return; // 0 = Tree, 1 = Hedge, 2 = Tree Group //

            if (ObjectLengthSpinEdit.EditValue == null || string.IsNullOrEmpty(ObjectLengthSpinEdit.EditValue.ToString())) return;  // Test numeric value for null first //
            if (ObjectWidthSpinEdit.EditValue == null || string.IsNullOrEmpty(ObjectWidthSpinEdit.EditValue.ToString())) return;  // Test numeric value for null first //
            decimal decLength = Convert.ToDecimal(ObjectLengthSpinEdit.EditValue);
            decimal decWidth = Convert.ToDecimal(ObjectWidthSpinEdit.EditValue);

            decAreaHaSpinEdit.EditValue = decLength * decWidth;


        }

        private void decRiskFactorSpinEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
                this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

                DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                if (currentRow == null) return;
                if (String.IsNullOrEmpty(currentRow["intTreeID"].ToString()) || currentRow["intTreeID"].ToString() == "0")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Save the current record before attempting to Calculate Risk.", "Calculate Risk", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                int intTreeID = Convert.ToInt32(currentRow["intTreeID"]);
                string strFormula = "";
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    strFormula = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesRiskFactorFormula").ToString();
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Risk Calculation formula (from the System Settings Table).\n\nUnable to Calculate Risk. If the problem persists, contact Technical Support.", "Calculate Risk", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Your are about to update the Calculated Risk value for the current record.\n\nAre you sure you wish to proceed?", "Calculate Risk", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No) return;

                DataSet_ATTableAdapters.QueriesTableAdapter CalculateValue = new DataSet_ATTableAdapters.QueriesTableAdapter();
                CalculateValue.ChangeConnectionString(strConnectionString);
                try
                {
                    decimal decCalculatedValue = Convert.ToDecimal(CalculateValue.sp01425_AT_Risk_Factors_Calculate_Single(intTreeID, strFormula));
                    currentRow["decRiskFactor"] = decCalculatedValue;
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Calculating Risk. Try again. If the problem persists, contact Technical Support.", "Calculate Risk", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void intStatusGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        }
        private void intStatusGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            Calc_Next_Date(true);
            ibool_ignoreValidation = true;
        }

        #endregion


        #region PopupContainer1

        private void btnPopoupContainerOK1_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView27_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            string str_selected_values = "";    // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                return "";

            }
            else if (selection1.SelectedCount <= 0)
            {
                return "";
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        str_selected_values += Convert.ToString(view.GetRowCellValue(i, "ItemCode")) + ";";
                    }
                }
            }
            return str_selected_values;
        }


        private void strContextPopupContainerEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 124, "Landscape Contexts");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
                }
            }
        }

        private void strContextPopupContainerEdit_Enter(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = 124";  // No Blank Row //
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void strContextPopupContainerEdit_Leave(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void strContextPopupContainerEdit_QueryPopUp(object sender, CancelEventArgs e)
        {
            strContextPopupContainerEdit.Properties.PopupControl = popupContainerControl1;  // Needed as more than 1 Popup Container Edit cannot share the same PopupContainerControl //
        }

        private void strContextPopupContainerEdit_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strReturnedValue = PopupContainerEdit1_Get_Selected();
            if (strReturnedValue.Length > 50) strReturnedValue = strReturnedValue.Substring(0, 50);
            if (ClearAnyExistingValuesCheckEdit.Checked)
            {
                e.Value = strReturnedValue;
            }
            else
            {        
                strReturnedValue = e.Value + (!e.Value.ToString().EndsWith(";") ? ";" : "") + strReturnedValue;
                if (strReturnedValue.Length > 50) strReturnedValue = strReturnedValue.Substring(0,50);
                e.Value = strReturnedValue;
            }
        }


        private void strManagementPopupContainerEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 125, "Management");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
                }
            }
        }

        private void strManagementPopupContainerEdit_Enter(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = 125";  // No Blank Row //
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void strManagementPopupContainerEdit_Leave(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void strManagementPopupContainerEdit_QueryPopUp(object sender, CancelEventArgs e)
        {
            strManagementPopupContainerEdit.Properties.PopupControl = popupContainerControl1;  // Needed as more than 1 Popup Container Edit cannot share the same PopupContainerControl //
        }

        private void strManagementPopupContainerEdit_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strReturnedValue = PopupContainerEdit1_Get_Selected();
            if (strReturnedValue.Length > 50) strReturnedValue = strReturnedValue.Substring(0, 50);
            if (ClearAnyExistingValuesCheckEdit.Checked)
            {
                e.Value = strReturnedValue;
            }
            else
            {
                strReturnedValue = e.Value + (!e.Value.ToString().EndsWith(";") ? ";" : "") + strReturnedValue;
                if (strReturnedValue.Length > 50) strReturnedValue = strReturnedValue.Substring(0, 50);
                e.Value = strReturnedValue;
            }
        }


        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView25":
                    if (this.strFormMode.ToLower() == "add")
                    {
                        DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            if (String.IsNullOrEmpty(currentRow["intTreeID"].ToString()) || currentRow["intTreeID"].ToString() == "0")
                            {
                                message = "No Inspections - Save New Tree Record Before Adding Inspections";
                            }
                        }
                    }
                    else message = "No Inspections";
                    break;
                case "gridView29":
                    if (this.strFormMode.ToLower() == "add")
                    {
                        DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            if (String.IsNullOrEmpty(currentRow["intTreeID"].ToString()) || currentRow["intTreeID"].ToString() == "0")
                            {
                                message = "No Actions - Save New Tree Record Before Adding Actions";
                            }
                        }
                    }
                    else message = "No Actions";
                    break;
                case "gridView30":
                    if (this.strFormMode.ToLower() == "add")
                    {
                        DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            if (String.IsNullOrEmpty(currentRow["intTreeID"].ToString()) || currentRow["intTreeID"].ToString() == "0")
                            {
                                message = "No Linked Documents - Save New Tree Record Before Adding Linked Documents";
                            }
                        }
                    }
                    else message = "No Linked Documents";
                    break;
                case "gridView39":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Pictures NOT Shown When Block Adding and Block Editing" : "No Linked Pictures Available - Click the Add button to Create Linked Pictures");
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region Grid - Inspections

        private void gridView25_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit && strFormMode != "view")
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView25_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView25_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView25_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedOutstandingActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedOutstandingActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView25_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedActionCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedOutstandingActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedOutstandingActionCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strInspectionIDs = view.GetRowCellValue(view.FocusedRowHandle, "InspectionID").ToString() + ",";
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strRecordIDs = "";
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedActionCount":
                    strRecordIDs = GetSetting.sp01333_AT_Inspection_Manager_Get_Linked_Action_IDs(strInspectionIDs, 0).ToString();
                    break;
                case "LinkedOutstandingActionCount":
                    strRecordIDs = GetSetting.sp01333_AT_Inspection_Manager_Get_Linked_Action_IDs(strInspectionIDs, 1).ToString();
                    break;
                default:
                    break;
            }
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "inspection");
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            string strSelectedIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ',';
            }
            try
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "tree");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Grid - Actions

        private void gridView29_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit && strFormMode != "view")
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView29_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView29_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Grid - Linked Documents

        private void gridView30_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit && strFormMode != "view")
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView30_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView30_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }


        #endregion


        #region Grid - Linked Pictures

        private void gridView39_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            //if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView39_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView39_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 39;
            SetMenuStatus();
        }

        private void gridView39_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 39;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl39_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl39.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit3_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = "";
            string strSelectedTrees = "";
            string strSelectedTree = "";
            int intSelectedTreeCount = 0;
            string strSelectedInspections = "";
            string strSelectedInspection = "";
            int intSelectedInspectionCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Inspections Grid //
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Inspections //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                    }
                    CreateDataset("Inspection", intSelectedTreeCount, strSelectedTrees, intCount, strSelectedIDs, 0, "");
                    break;
                case 2:  // Available Action Grid //
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        // Actions //
                        if (strSelectedIDs == "")
                        {
                            strSelectedIDs = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
                        }
                        else
                        {
                            strCurrentID = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
                            if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedIDs += strCurrentID;
                            }
                        }
                        // Trees //
                        if (strSelectedTrees == "")
                        {
                            strSelectedTrees = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            intSelectedTreeCount++;
                        }
                        else
                        {
                            strSelectedTree = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                            if (!strSelectedTrees.Contains("," + strSelectedTree))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedTrees += strSelectedTree;
                                intSelectedTreeCount++;
                            }
                        }
                        // Inspections //
                        if (strSelectedInspections == "")
                        {
                            strSelectedInspections = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                            intSelectedInspectionCount++;
                        }
                        else
                        {
                            strSelectedInspection = Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["InspectionID"])) + ',';
                            if (!strSelectedInspections.Contains("," + strSelectedInspection))  // Put leading comma in temporarily for checking purposes //
                            {
                                strSelectedInspections += strSelectedInspection;
                                intSelectedInspectionCount++;
                            }
                        }
                    }
                    CreateDataset("Action", intSelectedTreeCount, strSelectedTrees, intSelectedInspectionCount, strSelectedInspections, intCount, strSelectedIDs);
                    break;
                default:
                    return;
            }
        }

        private void CreateDataset(string strType, int intTreeCount, string strSelectedTreeIDs, int intInspectionCount, string strSelectedInspectionIDs, int intActionCount, string strSelectedActionIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            //Form frmMain = this.MdiParent;
            //frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_tree_count = intTreeCount;
            fChildForm.i_int_selected_inspection_count = intInspectionCount;
            fChildForm.i_int_selected_action_count = intActionCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                string strSelectedIDs = "";
                switch (strDatasetType.ToUpper())
                {
                    case "TREE":
                        strSelectedIDs = strSelectedTreeIDs;
                        break;
                    case "INSPECTION":
                        strSelectedIDs = strSelectedInspectionIDs;
                        break;
                    case "ACTION":
                        strSelectedIDs = strSelectedActionIDs;
                        break;
                    default:
                        strSelectedIDs = "";
                        break;
                }
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 2:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDataset(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            string strSelectedDatasetType;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Available Inspections Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection2.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        default:
                            break;
                    }
                    return;
                case 2:  // Available Actions Grid //
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Tree,Inspection,Action,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    strSelectedDatasetType = fChildForm.i_str_selected_dataset_type;
                    switch (strSelectedDatasetType)
                    {
                        case "Tree":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "TreeID");
                            break;
                        case "Inspection":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "InspectionID");
                            break;
                        case "Action":
                            DS_Selection3.SelectRecordsFromDatasetInverted(intSelectedDataset, "ActionID");
                            break;
                        default:
                            break;
                    }
                    return;
                default:
                    return;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
        }

        #endregion


        #region CAVAT

        private void Attach_EditValueChanged_To_CavatGridLookupEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).EditValueChanged += new EventHandler(EditValueChanged_CavatGridLookupEdits);
            }
        }

        private void Detach_EditValueChanged_From_CavatGridLookupEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).EditValueChanged -= new EventHandler(EditValueChanged_CavatGridLookupEdits);
            }
        }

        private void EditValueChanged_CavatGridLookupEdits(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }


        private void Attach_Validated_To_CavatGridLookupEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Validated += new EventHandler(Validated_CavatGridLookupEdits);
            }
        }

        private void Detach_Validated_From_CavatGridLookupEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Validated -= new EventHandler(Validated_CavatGridLookupEdits);
            }
        }

        private void Validated_CavatGridLookupEdits(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_CAVAT();
        }


        private void CavatUnitValueFactorSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }

        private void CavatUnitValueFactorSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_CAVAT();
        }


        private void CavatUnitValueFactorSpinEdit2_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }

        private void CavatUnitValueFactorSpinEdit2_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_CAVAT();
        }


        private void Calculate_CAVAT()
        {
            if (strCavatOn != "On") return;
            if (strCavatMethod == "Full")  // Display Full Fields and Hide Quick Fields //
            {
                int intStemDiameter = (CavatStemDiameterSpinEdit.EditValue == null || string.IsNullOrEmpty(CavatStemDiameterSpinEdit.EditValue.ToString()) ? 0 : Convert.ToInt32(CavatStemDiameterSpinEdit.EditValue));
                decimal decUnitValueFactor = (CavatUnitValueFactorSpinEdit.EditValue == null || string.IsNullOrEmpty(CavatUnitValueFactorSpinEdit.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatUnitValueFactorSpinEdit.EditValue));
                decimal decBasicValue = (decimal)(((intStemDiameter / 2) * (intStemDiameter / 2)) * Math.PI) * decUnitValueFactor;
                CavatBasicValueTextEdit.EditValue = decimal.Round(decBasicValue, 0);

                decimal decCTIFactor = (CavatCTIFactorGridLookUpEdit.EditValue == null || string.IsNullOrEmpty(CavatCTIFactorGridLookUpEdit.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatCTIFactorGridLookUpEdit.EditValue));
                decimal decAccessibility = (CavatAccessibilityGridLookUpEdit.EditValue == null || string.IsNullOrEmpty(CavatAccessibilityGridLookUpEdit.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatAccessibilityGridLookUpEdit.EditValue));
                decimal decCTIValue = (decimal)(Convert.ToDecimal(CavatBasicValueTextEdit.EditValue) * decCTIFactor) * decAccessibility;
                CavatCTIValueTextEdit.EditValue = decimal.Round(decCTIValue, 0);

                decimal decFunctionalValueFactor = (CavatFunctionalValueFactorGridLookUpEdit.EditValue == null || string.IsNullOrEmpty(CavatFunctionalValueFactorGridLookUpEdit.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatFunctionalValueFactorGridLookUpEdit.EditValue));
                decimal decFunctionalValue = (decimal)decFunctionalValueFactor * decCTIValue;
                CavatFunctionalValueTextEdit.EditValue = decimal.Round(decFunctionalValue, 0);

                decimal decAmenityFactors = (CavatAmenityFactorsGridLookUpEdit.EditValue == null || string.IsNullOrEmpty(CavatAmenityFactorsGridLookUpEdit.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatAmenityFactorsGridLookUpEdit.EditValue));
                decimal decAppropriateness = (CavatAppropriatenessGridLookUpEdit.EditValue == null || string.IsNullOrEmpty(CavatAppropriatenessGridLookUpEdit.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatAppropriatenessGridLookUpEdit.EditValue));
                decimal decAdjustedValue = (decimal)(decFunctionalValue / (decimal)100) * (decAmenityFactors + decAppropriateness + (decimal)100);
                CavatAdjustedValueTextEdit.EditValue = decimal.Round(decAdjustedValue, 0);

                decimal decSLEFactor = (CavatSLEFactorGridLookUpEdit.EditValue == null || string.IsNullOrEmpty(CavatSLEFactorGridLookUpEdit.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatSLEFactorGridLookUpEdit.EditValue));
                decimal decCavat = decAdjustedValue * decSLEFactor;
                CavatSpinEdit.EditValue = decimal.Round(decCavat, 0);
            }
            else  // Quick //
            {
                decimal decStemDiameter = (decimal)(CavatStemDiameterSpinEdit2.EditValue == null || string.IsNullOrEmpty(CavatStemDiameterSpinEdit2.EditValue.ToString()) ? 0 : Convert.ToInt32(CavatStemDiameterSpinEdit2.EditValue));
                decimal decUnitValueFactor = (CavatUnitValueFactorSpinEdit2.EditValue == null || string.IsNullOrEmpty(CavatUnitValueFactorSpinEdit2.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatUnitValueFactorSpinEdit2.EditValue));
                decimal decCTIFactor = (CavatCTIFactorGridLookUpEdit2.EditValue == null || string.IsNullOrEmpty(CavatCTIFactorGridLookUpEdit2.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatCTIFactorGridLookUpEdit2.EditValue));
                decimal decFirstCashValue = (decimal)0.00;
                if (decStemDiameter <= (decimal)5.9)
                {
                    decFirstCashValue = (decimal)231.25;
                }
                else if (decStemDiameter < (decimal)8.9)
                {
                    decFirstCashValue = (decimal)576.53;
                }
                else if (decStemDiameter < (decimal)11.9)
                {
                    decFirstCashValue = (decimal)1130.00;
                }
                else if (decStemDiameter < (decimal)14.9)
                {
                    decFirstCashValue = (decimal)1867.96;
                }
                else if (decStemDiameter < (decimal)19.9)
                {
                    decFirstCashValue = (decimal)3138.89;
                }
                else if (decStemDiameter < (decimal)24.9)
                {
                    decFirstCashValue = (decimal)5188.78;
                }
                else if (decStemDiameter < (decimal)29.9)
                {
                    decFirstCashValue = (decimal)7751.14;
                }
                else if (decStemDiameter < (decimal)39.9)
                {
                    decFirstCashValue = (decimal)12555.57;
                }
                else if (decStemDiameter < (decimal)49.9)
                {
                    decFirstCashValue = (decimal)20755.13;
                }
                else if (decStemDiameter < (decimal)59.9)
                {
                    decFirstCashValue = (decimal)31004.57;
                }
                else if (decStemDiameter < (decimal)69.9)
                {
                    decFirstCashValue = (decimal)43303.91;
                }
                else if (decStemDiameter < (decimal)84.9)
                {
                    decFirstCashValue = (decimal)57653.13;
                }
                else if (decStemDiameter < (decimal)99.9)
                {
                    decFirstCashValue = (decimal)83020.51;
                }
                else if (decStemDiameter < (decimal)114.9)
                {
                    decFirstCashValue = (decimal)113000.14;
                }
                else if (decStemDiameter < (decimal)129.9)
                {
                    decFirstCashValue = (decimal)147592.02;
                }
                else if (decStemDiameter >= (decimal)130.0)
                {
                    decFirstCashValue = (decimal)186796.15;
                }
                decimal decCTIValue = (decimal)decFirstCashValue * decCTIFactor;
                CavatCTIValueTextEdit2.EditValue = decimal.Round(decCTIValue, 0);

                decimal decFunctionalAdjustment = (CavatFunctionalValueFactorGridLookUpEdit2.EditValue == null || string.IsNullOrEmpty(CavatFunctionalValueFactorGridLookUpEdit2.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatFunctionalValueFactorGridLookUpEdit2.EditValue));
                decimal decFunctionalValue = decCTIValue * decFunctionalAdjustment;
                CavatFunctionalValueTextEdit2.EditValue = decimal.Round(decFunctionalValue, 0);

                decimal decSLEFactor = (CavatSLEFactorGridLookUpEdit2.EditValue == null || string.IsNullOrEmpty(CavatSLEFactorGridLookUpEdit2.EditValue.ToString()) ? (decimal)0.00 : Convert.ToDecimal(CavatSLEFactorGridLookUpEdit2.EditValue));
                decimal decCavat = decFunctionalValue * decSLEFactor;
                CavatSpinEdit.EditValue = decimal.Round(decCavat, 0);
            }
        }
        
        #endregion


        private void bbiBlockAddAction_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        int intTreeID = 0;
                        string strTreeReference = "";
                        DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            intTreeID = (currentRow["intTreeID"] == null ? 0 : Convert.ToInt32(currentRow["intTreeID"]));
                            strTreeReference = (currentRow["strTreeRef"] == null ? "" : currentRow["strTreeRef"].ToString());
                        }
                        fChildForm.intLinkedToRecordID = intTreeID;
                        fChildForm.strLinkedToRecordDesc = strTreeReference;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Actions //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        GridView ParentView = (GridView)gridControl1.MainView;
                        int[] intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "InspectionID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "InspectionReference"));
                            fChildForm.intParentOwnershipID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "TreeOwnershipID"));
                            fChildForm.strParentNearestHouse = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "TreeHouseName"));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 3;  // Trees //

                        int intTreeID = 0;
                        string strTreeReference = "";
                        DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            intTreeID = (currentRow["intTreeID"] == null ? 0 : Convert.ToInt32(currentRow["intTreeID"]));
                            strTreeReference = (currentRow["strTreeRef"] == null ? "" : currentRow["strTreeRef"].ToString());
                        }
                        fChildForm.intLinkedToRecordID = intTreeID;
                        fChildForm.strLinkedToRecordDesc = strTreeReference;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 39:  // Linked Pictures //
                    {
                        int intTreeID = 0;
                        string strTreeRef = "";
                        DataRowView currentRow = (DataRowView)sp01204ATEditTreeDetailsBindingSource.Current;
                        if (currentRow != null)
                        {
                            intTreeID = (currentRow["intTreeID"] == null ? 0 : Convert.ToInt32(currentRow["intTreeID"]));
                            strTreeRef = (currentRow["strTreeRef"] == null ? "" : currentRow["strTreeRef"].ToString()); 
                        }
                        if (intTreeID == 0) return;
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "add";
                        fChildForm.intAddToRecordID = intTreeID;
                        fChildForm.strSaveNameValue = strTreeRef;
                        fChildForm.intAddToRecordTypeID = 101;  // 101 = AT Tree //
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.strDefaultPath = strDefaultPicturesPath;
                        fChildForm._PassedInFileNamePrefix = "Tree";
                        fChildForm.ShowDialog();
                        int intNewPictureID = fChildForm._AddedPolePictureID;
                        if (intNewPictureID != 0)  // At least 1 picture added so refresh picture grid //
                        {
                            i_str_AddedRecordIDs39 = intNewPictureID.ToString() + ";";
                            this.RefreshGridViewState39.SaveViewInfo();
                            Load_Linked_Pictures();
                            SetMenuStatus();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 3;  // Trees //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 39:     // Picture //
                    {
                        view = (GridView)gridControl39.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Picture Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intPictureID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyPictureID"));
                        int intAddToRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordID"));
                        int intLinkedToRecordTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordTypeID"));
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "edit";
                        fChildForm._PassedInEditImageID = intPictureID;
                        fChildForm.intAddToRecordID = intAddToRecordID;
                        fChildForm.intAddToRecordTypeID = intLinkedToRecordTypeID;
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.strDefaultPath = strDefaultPicturesPath;
                        fChildForm._PassedInFileNamePrefix = "Tree";
                        fChildForm.ShowDialog();
                        this.RefreshGridViewState39.SaveViewInfo();
                        Load_Linked_Pictures();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Inspection //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Inspections to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Inspection" : Convert.ToString(intRowHandles.Length) + " Inspections") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this inspection" : "these inspections") + " will no longer be available for selection and any related Actions will also be deleted!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "InspectionID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        RemoveRecords.sp01383_AT_Inspection_Manager_Delete("inspection", strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Inspection_Refresh(this.ParentForm, this.Name, "", "");

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:  // Action //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Actions to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Action" : Convert.ToString(intRowHandles.Length) + " Actions") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this action" : "these actions") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ActionID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        RemoveRecords.sp01387_AT_Action_Manager_Delete("action", strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Action_Refresh(this.ParentForm, this.Name, "", "");

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:  // Linked Documents //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl4.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();
                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.LinkedDocument_Refresh(this.ParentForm, this.Name, "");

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 39:  // Linked Tree Pictures //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl39.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Tree Picture" : Convert.ToString(intRowHandles.Length) + " Tree Pictures") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Tree Picture" : "these Tree Pictures") + " will no longer be available for selection and any associated images will be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            string strImageName = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strImageName = view.GetRowCellValue(intRowHandle, "PicturePath").ToString();
                                try
                                {
                                    base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                                    GC.GetTotalMemory(true);
                                    System.IO.File.Delete(strImageName);
                                }
                                catch (Exception ex)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete a linked picture [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete Tree Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    continue;
                                }
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyPictureID")) + ",";
                            }
                            DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState39.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp01387_AT_Action_Manager_Delete("picture", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Linked_Pictures();
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        //this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:     // Actions //
                    {
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = "frm_AT_Tree_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

 


    }
}


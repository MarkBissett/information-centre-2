using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_AT_Inspection_Block_Add_Get_Core_Details : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";

        public string strSelectedTreeIDs = "";
        public string strSelectedInspectionReference = "";
        public int intSelectedInspector = 0;
        public DateTime dtSelectedInspectionDate = DateTime.Today;
        public int intSelectedGeneralCondition = 0;
        public string strSelectedRemarks = "";
        ArrayList ArrayListFilteredPicklists;  // Holds all the picklists bound to sp01372 - used for iterating round to bind and unbind generic Enter, Leave and ButtonClick events //

        #endregion
        
        public frm_AT_Inspection_Block_Add_Get_Core_Details()
        {
            InitializeComponent();
        }

        private void frm_AT_Inspection_Block_Add_Get_Core_Details_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 200121;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp00190_Contractor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00190_Contractor_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00190_Contractor_List_With_Blank);

            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);

            ArrayListFilteredPicklists = new ArrayList();  // Note: Each GridLookUpEdit within the array should have it's tag value set to it's value in the SQL PicklistHeader Table //
            ArrayListFilteredPicklists.Add(intGeneralConditionGridLookUpEdit);
            Attach_Enter_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach Enter Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //

            strInspectRefButtonEdit.EditValue = "";
            intInspectorGridLookUpEdit.EditValue = 0;
            dtInspectDateDateEdit.DateTime = DateTime.Today;
            intGeneralConditionGridLookUpEdit.EditValue = 0;
            SelectedTreesMemoEdit.EditValue = strSelectedTreeIDs;
            strRemarksMemoEdit.EditValue = "";

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            dxErrorProvider1.ClearErrors();
            this.ValidateChildren();
        }

        public override void PostLoadView(object objParameter)
        {
        }


        private void Attach_Enter_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter += new EventHandler(Filter_GridView);
            }
        }

        private void Detach_Enter_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter -= new EventHandler(Filter_GridView);
            }
        }

        private void Filter_GridView(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        private void Attach_Leave_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave += new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Detach_Leave_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave -= new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Clear_Filter_GridView(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }


        #region Editors

        private void strInspectRefButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string strTableName = "tblInspection";
            string strFieldName = "strInspectRef";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                this.AddOwnedForm(fChildForm);
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK) strInspectRefButtonEdit.EditValue = fChildForm.SelectedSequence;
            }
        }

        private void strInspectRefButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            /*ButtonEdit be = (ButtonEdit)sender;
            if (be.EditValue == null)
            {
                dxErrorProvider1.SetError(strInspectRefButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(strInspectRefButtonEdit, "");
            }*/
        }

        private void dtInspectDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(dtInspectDateDateEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(dtInspectDateDateEdit, "");
            }
        }


        #endregion


        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, LayoutControl Layoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                //layoutControl1.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = Layoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        Layoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        Layoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, layoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the screen!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Block Add Dummy Inspections", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            strSelectedInspectionReference = strInspectRefButtonEdit.EditValue.ToString();
            intSelectedInspector = (string.IsNullOrEmpty(intInspectorGridLookUpEdit.EditValue.ToString()) ? 0 : Convert.ToInt32(intInspectorGridLookUpEdit.EditValue));
            dtSelectedInspectionDate = dtInspectDateDateEdit.DateTime;
            intSelectedGeneralCondition = (string.IsNullOrEmpty(intGeneralConditionGridLookUpEdit.EditValue.ToString()) ? 0 : Convert.ToInt32(intGeneralConditionGridLookUpEdit.EditValue));
            strSelectedRemarks = strRemarksMemoEdit.EditValue.ToString();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void frm_AT_Inspection_Block_Add_Get_Core_Details_FormClosing(object sender, FormClosingEventArgs e)
        {
            Detach_Enter_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Enter Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
            Detach_Leave_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Leave Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
        }


    }
}


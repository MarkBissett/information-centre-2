namespace WoodPlan5
{
    partial class frm_AT_Select_Incident
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Select_Incident));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.colIncidentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01343ATIncidentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBriefDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonRecordedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonRecordedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonResponsibleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonResponsibleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colIncidentAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedBySurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrReportedByPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrReportedByTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateClosed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnLoadIncidents = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditShowOpen = new DevExpress.XtraEditors.CheckEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.dataSetATDataEntryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01343_AT_Incidents_ListTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01343_AT_Incidents_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01343ATIncidentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowOpen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetATDataEntryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(823, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 432);
            this.barDockControlBottom.Size = new System.Drawing.Size(823, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 432);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(823, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 432);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colIncidentStatus
            // 
            this.colIncidentStatus.Caption = "Incident Status";
            this.colIncidentStatus.FieldName = "IncidentStatus";
            this.colIncidentStatus.Name = "colIncidentStatus";
            this.colIncidentStatus.OptionsColumn.AllowEdit = false;
            this.colIncidentStatus.OptionsColumn.AllowFocus = false;
            this.colIncidentStatus.OptionsColumn.ReadOnly = true;
            this.colIncidentStatus.Visible = true;
            this.colIncidentStatus.VisibleIndex = 4;
            this.colIncidentStatus.Width = 94;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01343ATIncidentsListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(819, 300);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01343ATIncidentsListBindingSource
            // 
            this.sp01343ATIncidentsListBindingSource.DataMember = "sp01343_AT_Incidents_List";
            this.sp01343ATIncidentsListBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIncidentID,
            this.colIncidentType,
            this.colIncidentTypeDescription,
            this.colReferenceID,
            this.colBriefDescription,
            this.colPersonRecordedByID,
            this.colPersonRecordedByName,
            this.colPersonResponsibleID,
            this.colPersonResponsibleName,
            this.colDateRecorded,
            this.colRemarks,
            this.colIncidentAddressLine1,
            this.colIncidentAddressLine2,
            this.colIncidentAddressLine3,
            this.colIncidentAddressLine4,
            this.colIncidentAddressLine5,
            this.colIncidentPostcode,
            this.colintX,
            this.colintY,
            this.colReportedBySurname,
            this.colReportedByForename,
            this.colReportedByAddressLine1,
            this.colReportedByAddressLine2,
            this.colReportedByAddressLine3,
            this.colReportedByAddressLine4,
            this.colReportedByAddressLine5,
            this.colstrReportedByPostcode,
            this.colstrReportedByTelephone1,
            this.colIncidentStatus,
            this.colDateClosed,
            this.colClientID,
            this.colClientName});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colIncidentStatus;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "Closed";
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRecorded, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colIncidentID
            // 
            this.colIncidentID.Caption = "Incident ID";
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.Width = 74;
            // 
            // colIncidentType
            // 
            this.colIncidentType.Caption = "Incident Type ID";
            this.colIncidentType.FieldName = "IncidentType";
            this.colIncidentType.Name = "colIncidentType";
            this.colIncidentType.OptionsColumn.AllowEdit = false;
            this.colIncidentType.OptionsColumn.AllowFocus = false;
            this.colIncidentType.OptionsColumn.ReadOnly = true;
            this.colIncidentType.Width = 101;
            // 
            // colIncidentTypeDescription
            // 
            this.colIncidentTypeDescription.Caption = "Incident Type";
            this.colIncidentTypeDescription.FieldName = "IncidentTypeDescription";
            this.colIncidentTypeDescription.Name = "colIncidentTypeDescription";
            this.colIncidentTypeDescription.OptionsColumn.AllowEdit = false;
            this.colIncidentTypeDescription.OptionsColumn.AllowFocus = false;
            this.colIncidentTypeDescription.OptionsColumn.ReadOnly = true;
            this.colIncidentTypeDescription.Visible = true;
            this.colIncidentTypeDescription.VisibleIndex = 2;
            this.colIncidentTypeDescription.Width = 116;
            // 
            // colReferenceID
            // 
            this.colReferenceID.Caption = "Reference No";
            this.colReferenceID.FieldName = "ReferenceID";
            this.colReferenceID.Name = "colReferenceID";
            this.colReferenceID.OptionsColumn.AllowEdit = false;
            this.colReferenceID.OptionsColumn.AllowFocus = false;
            this.colReferenceID.OptionsColumn.ReadOnly = true;
            this.colReferenceID.Visible = true;
            this.colReferenceID.VisibleIndex = 1;
            this.colReferenceID.Width = 146;
            // 
            // colBriefDescription
            // 
            this.colBriefDescription.Caption = "Brief Description";
            this.colBriefDescription.FieldName = "BriefDescription";
            this.colBriefDescription.Name = "colBriefDescription";
            this.colBriefDescription.OptionsColumn.AllowEdit = false;
            this.colBriefDescription.OptionsColumn.AllowFocus = false;
            this.colBriefDescription.OptionsColumn.ReadOnly = true;
            this.colBriefDescription.Visible = true;
            this.colBriefDescription.VisibleIndex = 3;
            this.colBriefDescription.Width = 229;
            // 
            // colPersonRecordedByID
            // 
            this.colPersonRecordedByID.Caption = "Recorded By Person ID";
            this.colPersonRecordedByID.FieldName = "PersonRecordedByID";
            this.colPersonRecordedByID.Name = "colPersonRecordedByID";
            this.colPersonRecordedByID.OptionsColumn.AllowEdit = false;
            this.colPersonRecordedByID.OptionsColumn.AllowFocus = false;
            this.colPersonRecordedByID.OptionsColumn.ReadOnly = true;
            this.colPersonRecordedByID.Width = 132;
            // 
            // colPersonRecordedByName
            // 
            this.colPersonRecordedByName.Caption = "Recorded By";
            this.colPersonRecordedByName.FieldName = "PersonRecordedByName";
            this.colPersonRecordedByName.Name = "colPersonRecordedByName";
            this.colPersonRecordedByName.OptionsColumn.AllowEdit = false;
            this.colPersonRecordedByName.OptionsColumn.AllowFocus = false;
            this.colPersonRecordedByName.OptionsColumn.ReadOnly = true;
            this.colPersonRecordedByName.Visible = true;
            this.colPersonRecordedByName.VisibleIndex = 7;
            this.colPersonRecordedByName.Width = 157;
            // 
            // colPersonResponsibleID
            // 
            this.colPersonResponsibleID.Caption = "Person Responsible ID";
            this.colPersonResponsibleID.FieldName = "PersonResponsibleID";
            this.colPersonResponsibleID.Name = "colPersonResponsibleID";
            this.colPersonResponsibleID.OptionsColumn.AllowEdit = false;
            this.colPersonResponsibleID.OptionsColumn.AllowFocus = false;
            this.colPersonResponsibleID.OptionsColumn.ReadOnly = true;
            this.colPersonResponsibleID.Width = 128;
            // 
            // colPersonResponsibleName
            // 
            this.colPersonResponsibleName.Caption = "Person Responsible";
            this.colPersonResponsibleName.FieldName = "PersonResponsibleName";
            this.colPersonResponsibleName.Name = "colPersonResponsibleName";
            this.colPersonResponsibleName.OptionsColumn.AllowEdit = false;
            this.colPersonResponsibleName.OptionsColumn.AllowFocus = false;
            this.colPersonResponsibleName.OptionsColumn.ReadOnly = true;
            this.colPersonResponsibleName.Visible = true;
            this.colPersonResponsibleName.VisibleIndex = 8;
            this.colPersonResponsibleName.Width = 157;
            // 
            // colDateRecorded
            // 
            this.colDateRecorded.Caption = "Date Recorded";
            this.colDateRecorded.FieldName = "DateRecorded";
            this.colDateRecorded.Name = "colDateRecorded";
            this.colDateRecorded.OptionsColumn.AllowEdit = false;
            this.colDateRecorded.OptionsColumn.AllowFocus = false;
            this.colDateRecorded.OptionsColumn.ReadOnly = true;
            this.colDateRecorded.Visible = true;
            this.colDateRecorded.VisibleIndex = 0;
            this.colDateRecorded.Width = 115;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 9;
            this.colRemarks.Width = 112;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colIncidentAddressLine1
            // 
            this.colIncidentAddressLine1.Caption = "Incident Address Line 1";
            this.colIncidentAddressLine1.FieldName = "IncidentAddressLine1";
            this.colIncidentAddressLine1.Name = "colIncidentAddressLine1";
            this.colIncidentAddressLine1.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine1.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine1.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine1.Visible = true;
            this.colIncidentAddressLine1.VisibleIndex = 10;
            this.colIncidentAddressLine1.Width = 133;
            // 
            // colIncidentAddressLine2
            // 
            this.colIncidentAddressLine2.Caption = "Incident Address Line 2";
            this.colIncidentAddressLine2.FieldName = "IncidentAddressLine2";
            this.colIncidentAddressLine2.Name = "colIncidentAddressLine2";
            this.colIncidentAddressLine2.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine2.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine2.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine2.Width = 133;
            // 
            // colIncidentAddressLine3
            // 
            this.colIncidentAddressLine3.Caption = "Incident Address Line 3";
            this.colIncidentAddressLine3.FieldName = "IncidentAddressLine3";
            this.colIncidentAddressLine3.Name = "colIncidentAddressLine3";
            this.colIncidentAddressLine3.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine3.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine3.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine3.Width = 133;
            // 
            // colIncidentAddressLine4
            // 
            this.colIncidentAddressLine4.Caption = "Incident Address Line 4";
            this.colIncidentAddressLine4.FieldName = "IncidentAddressLine4";
            this.colIncidentAddressLine4.Name = "colIncidentAddressLine4";
            this.colIncidentAddressLine4.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine4.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine4.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine4.Width = 133;
            // 
            // colIncidentAddressLine5
            // 
            this.colIncidentAddressLine5.Caption = "Incident Address Line 5";
            this.colIncidentAddressLine5.FieldName = "IncidentAddressLine5";
            this.colIncidentAddressLine5.Name = "colIncidentAddressLine5";
            this.colIncidentAddressLine5.OptionsColumn.AllowEdit = false;
            this.colIncidentAddressLine5.OptionsColumn.AllowFocus = false;
            this.colIncidentAddressLine5.OptionsColumn.ReadOnly = true;
            this.colIncidentAddressLine5.Width = 133;
            // 
            // colIncidentPostcode
            // 
            this.colIncidentPostcode.Caption = "Incident Postcode";
            this.colIncidentPostcode.FieldName = "IncidentPostcode";
            this.colIncidentPostcode.Name = "colIncidentPostcode";
            this.colIncidentPostcode.OptionsColumn.AllowEdit = false;
            this.colIncidentPostcode.OptionsColumn.AllowFocus = false;
            this.colIncidentPostcode.OptionsColumn.ReadOnly = true;
            this.colIncidentPostcode.Visible = true;
            this.colIncidentPostcode.VisibleIndex = 11;
            this.colIncidentPostcode.Width = 107;
            // 
            // colintX
            // 
            this.colintX.Caption = "X";
            this.colintX.FieldName = "intX";
            this.colintX.Name = "colintX";
            this.colintX.OptionsColumn.AllowEdit = false;
            this.colintX.OptionsColumn.AllowFocus = false;
            this.colintX.OptionsColumn.ReadOnly = true;
            this.colintX.Width = 27;
            // 
            // colintY
            // 
            this.colintY.Caption = "Y";
            this.colintY.FieldName = "intY";
            this.colintY.Name = "colintY";
            this.colintY.OptionsColumn.AllowEdit = false;
            this.colintY.OptionsColumn.AllowFocus = false;
            this.colintY.OptionsColumn.ReadOnly = true;
            this.colintY.Width = 27;
            // 
            // colReportedBySurname
            // 
            this.colReportedBySurname.Caption = "Reported By Surname";
            this.colReportedBySurname.FieldName = "ReportedBySurname";
            this.colReportedBySurname.Name = "colReportedBySurname";
            this.colReportedBySurname.OptionsColumn.AllowEdit = false;
            this.colReportedBySurname.OptionsColumn.AllowFocus = false;
            this.colReportedBySurname.OptionsColumn.ReadOnly = true;
            this.colReportedBySurname.Visible = true;
            this.colReportedBySurname.VisibleIndex = 12;
            this.colReportedBySurname.Width = 126;
            // 
            // colReportedByForename
            // 
            this.colReportedByForename.Caption = "Reported By Forename";
            this.colReportedByForename.FieldName = "ReportedByForename";
            this.colReportedByForename.Name = "colReportedByForename";
            this.colReportedByForename.OptionsColumn.AllowEdit = false;
            this.colReportedByForename.OptionsColumn.AllowFocus = false;
            this.colReportedByForename.OptionsColumn.ReadOnly = true;
            this.colReportedByForename.Visible = true;
            this.colReportedByForename.VisibleIndex = 13;
            this.colReportedByForename.Width = 132;
            // 
            // colReportedByAddressLine1
            // 
            this.colReportedByAddressLine1.Caption = "Reported By Address Line 1";
            this.colReportedByAddressLine1.FieldName = "ReportedByAddressLine1";
            this.colReportedByAddressLine1.Name = "colReportedByAddressLine1";
            this.colReportedByAddressLine1.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine1.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine1.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine1.Visible = true;
            this.colReportedByAddressLine1.VisibleIndex = 14;
            this.colReportedByAddressLine1.Width = 154;
            // 
            // colReportedByAddressLine2
            // 
            this.colReportedByAddressLine2.Caption = "Reported By Address Line 2";
            this.colReportedByAddressLine2.FieldName = "ReportedByAddressLine2";
            this.colReportedByAddressLine2.Name = "colReportedByAddressLine2";
            this.colReportedByAddressLine2.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine2.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine2.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine2.Width = 154;
            // 
            // colReportedByAddressLine3
            // 
            this.colReportedByAddressLine3.Caption = "Reported By Address Line 3";
            this.colReportedByAddressLine3.FieldName = "ReportedByAddressLine3";
            this.colReportedByAddressLine3.Name = "colReportedByAddressLine3";
            this.colReportedByAddressLine3.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine3.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine3.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine3.Width = 154;
            // 
            // colReportedByAddressLine4
            // 
            this.colReportedByAddressLine4.Caption = "Reported By Address Line 4";
            this.colReportedByAddressLine4.FieldName = "ReportedByAddressLine4";
            this.colReportedByAddressLine4.Name = "colReportedByAddressLine4";
            this.colReportedByAddressLine4.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine4.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine4.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine4.Width = 154;
            // 
            // colReportedByAddressLine5
            // 
            this.colReportedByAddressLine5.Caption = "Reported By Address Line 5";
            this.colReportedByAddressLine5.FieldName = "ReportedByAddressLine5";
            this.colReportedByAddressLine5.Name = "colReportedByAddressLine5";
            this.colReportedByAddressLine5.OptionsColumn.AllowEdit = false;
            this.colReportedByAddressLine5.OptionsColumn.AllowFocus = false;
            this.colReportedByAddressLine5.OptionsColumn.ReadOnly = true;
            this.colReportedByAddressLine5.Width = 154;
            // 
            // colstrReportedByPostcode
            // 
            this.colstrReportedByPostcode.Caption = "Reported By Postcode";
            this.colstrReportedByPostcode.FieldName = "strReportedByPostcode";
            this.colstrReportedByPostcode.Name = "colstrReportedByPostcode";
            this.colstrReportedByPostcode.OptionsColumn.AllowEdit = false;
            this.colstrReportedByPostcode.OptionsColumn.AllowFocus = false;
            this.colstrReportedByPostcode.OptionsColumn.ReadOnly = true;
            this.colstrReportedByPostcode.Visible = true;
            this.colstrReportedByPostcode.VisibleIndex = 15;
            this.colstrReportedByPostcode.Width = 128;
            // 
            // colstrReportedByTelephone1
            // 
            this.colstrReportedByTelephone1.Caption = "Reported By Telephone No";
            this.colstrReportedByTelephone1.FieldName = "strReportedByTelephone1";
            this.colstrReportedByTelephone1.Name = "colstrReportedByTelephone1";
            this.colstrReportedByTelephone1.OptionsColumn.AllowEdit = false;
            this.colstrReportedByTelephone1.OptionsColumn.AllowFocus = false;
            this.colstrReportedByTelephone1.OptionsColumn.ReadOnly = true;
            this.colstrReportedByTelephone1.Visible = true;
            this.colstrReportedByTelephone1.VisibleIndex = 16;
            this.colstrReportedByTelephone1.Width = 150;
            // 
            // colDateClosed
            // 
            this.colDateClosed.Caption = "Date Closed";
            this.colDateClosed.FieldName = "DateClosed";
            this.colDateClosed.Name = "colDateClosed";
            this.colDateClosed.OptionsColumn.AllowEdit = false;
            this.colDateClosed.OptionsColumn.AllowFocus = false;
            this.colDateClosed.OptionsColumn.ReadOnly = true;
            this.colDateClosed.Visible = true;
            this.colDateClosed.VisibleIndex = 5;
            this.colDateClosed.Width = 79;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Width = 68;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 6;
            this.colClientName.Width = 194;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(324, 401);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(424, 401);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 4);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Filter Criteria";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Incidents";
            this.splitContainerControl1.Size = new System.Drawing.Size(823, 391);
            this.splitContainerControl1.SplitterPosition = 61;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnLoadIncidents);
            this.layoutControl1.Controls.Add(this.checkEditShowOpen);
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(819, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnLoadIncidents
            // 
            this.btnLoadIncidents.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnLoadIncidents.Location = new System.Drawing.Point(696, 7);
            this.btnLoadIncidents.Name = "btnLoadIncidents";
            this.btnLoadIncidents.Size = new System.Drawing.Size(116, 22);
            this.btnLoadIncidents.StyleController = this.layoutControl1;
            this.btnLoadIncidents.TabIndex = 12;
            this.btnLoadIncidents.Text = "Load Incidents";
            this.btnLoadIncidents.Click += new System.EventHandler(this.btnLoadIncidents_Click);
            // 
            // checkEditShowOpen
            // 
            this.checkEditShowOpen.EditValue = true;
            this.checkEditShowOpen.Location = new System.Drawing.Point(370, 7);
            this.checkEditShowOpen.MenuManager = this.barManager1;
            this.checkEditShowOpen.Name = "checkEditShowOpen";
            this.checkEditShowOpen.Properties.Caption = "Show Open Incidents Only";
            this.checkEditShowOpen.Size = new System.Drawing.Size(223, 19);
            this.checkEditShowOpen.StyleController = this.layoutControl1;
            this.checkEditShowOpen.TabIndex = 9;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(238, 7);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all In" +
    "cidents \r\nwill be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(128, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 12;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(64, 7);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all In" +
    "cidents will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(125, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 11;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(819, 37);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDate;
            this.layoutControlItem1.CustomizationFormText = "From Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(186, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(186, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(186, 27);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "From Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDate;
            this.layoutControlItem2.CustomizationFormText = "To Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(186, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(177, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(177, 27);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "To Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(42, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(590, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(99, 27);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkEditShowOpen;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(363, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(227, 27);
            this.layoutControlItem3.Text = "Show Open Incidents Only:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnLoadIncidents;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(689, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(120, 27);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(819, 300);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // dataSetATDataEntryBindingSource
            // 
            this.dataSetATDataEntryBindingSource.DataSource = this.dataSet_AT_DataEntry;
            this.dataSetATDataEntryBindingSource.Position = 0;
            // 
            // sp01343_AT_Incidents_ListTableAdapter
            // 
            this.sp01343_AT_Incidents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_AT_Select_Incident
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(823, 432);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_AT_Select_Incident";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Incident";
            this.Load += new System.EventHandler(this.frm_AT_Select_Incident_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01343ATIncidentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowOpen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataSetATDataEntryBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CheckEdit checkEditShowOpen;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.BindingSource sp01343ATIncidentsListBindingSource;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentType;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceID;
        private DevExpress.XtraGrid.Columns.GridColumn colBriefDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonRecordedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonRecordedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonResponsibleID;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonResponsibleName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colintX;
        private DevExpress.XtraGrid.Columns.GridColumn colintY;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedBySurname;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByForename;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colstrReportedByPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrReportedByTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colDateClosed;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private System.Windows.Forms.BindingSource dataSetATDataEntryBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01343_AT_Incidents_ListTableAdapter sp01343_AT_Incidents_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.SimpleButton btnLoadIncidents;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

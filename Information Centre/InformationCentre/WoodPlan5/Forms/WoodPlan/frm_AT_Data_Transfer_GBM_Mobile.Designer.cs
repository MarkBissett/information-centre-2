namespace WoodPlan5
{
    /// <summary>
    /// 
    /// </summary>
    partial class frm_AT_Data_Transfer_GBM_Mobile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Data_Transfer_GBM_Mobile));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition7 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition8 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            this.colGroupName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colcolor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDistrictName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00112exporttoGBMsavedtemplateslistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataTransfer = new WoodPlan5.DataSet_AT_DataTransfer();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTemplateType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTemplateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTemplateRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShareType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp00099exporttoGBMdefaultgrouplistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGroupOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl_WorkOrders = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btnWorkOrderCheckingRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditWorkOrderCheckingIncludeCompleted = new DevExpress.XtraEditors.CheckEdit();
            this.dateEditWorkOrderCheckingToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditWorkOrderCheckingFromDate = new DevExpress.XtraEditors.DateEdit();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp01321ATexporttoGBMavailableworkordersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuingBody = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedPreviousWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidents = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colWorkOrderUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedActions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompleteOverTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncompleteActions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalActions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnWorkOrderCheckingOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit3 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl3 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp00135exporttoGBMmapbackgroundfilesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMapLinkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFileType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDistrictName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnMapFilesOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridLookUpEdit2 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00102exporttoGBMmapscalelistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSetATDataTransferBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colScaleDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEdit1 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00101exporttoGBMmapcentrelocalitylistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXcoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYcoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridSplitContainer5 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp00100exporttoGBMlocalityfilterlistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLocalityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityOwnershipCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictOwnershipCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnLocalityFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEditExportType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.IncludeLastInspectionDetailsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.btnExport = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp00103exporttoGBMpicklistitemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSetATDataTransferBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnFilterFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp00098exporttoGBMfieldlistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colColumnID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExternalColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColumnLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColumnType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColumnLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColumnOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPickList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPicklistSummery = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGroupOrder3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colSearchable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colManditory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUseLastRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colUseLastRecordAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridSplitContainer6 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp00131exporttoGBMstylefieldsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSymbol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colSymbolColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.colPolygonFillColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.colPolygonLineColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.colPolygonFillPattern = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageList4 = new System.Windows.Forms.ImageList(this.components);
            this.colPolygonFillPatternColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.colPolygonLineWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.glueStyleField = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit3View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl7 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditImportWorkOrderChecking = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer7 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp00134exporttoGBMexportdataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintDistrictID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditDistrict = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00138importfromGBMdistrictslistwithblankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOwnershipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintLocalityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditLocality = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00139importfromGBMlocailitieslistwithblankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDistrictCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictOwnershipCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictOwnershipName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityOwnershipCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityOwnershipName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrTreeRef = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditSequenceField = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colintX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCoordinates = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colintY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEditPolygonXY = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colintStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditStatus = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00137importfromGBMltreestatuslistwithblankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAgeClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditSpecies = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colScientificName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrContext = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrManagement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintGroundType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldecHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintProtectionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownDepth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtPlantDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintGroupNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSafetyPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSiteHazardClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPlantSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPlantSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPlantMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMapLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintNearbyObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintNearbyObject2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintNearbyObject3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks_Tree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser1_Tree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser2_Tree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser3_Tree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldecAreaHa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrHedgeOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldecGardenSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldecPropertyProximity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldecSiteLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintReplantCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintTreeOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrInspectRef = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintInspector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtInspectDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colintIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintStemPhysical = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintStemPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintStemPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintStemDisease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintStemDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintStemDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAngleToVertical = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownPhysical = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownDisease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownFoliation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownFoliation2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCrownFoliation3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSafety = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintGeneralCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintRootHeave = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintRootHeave2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintRootHeave3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser1_Inspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser2_Inspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser3_Inspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks_Inspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintVitality = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDueDate_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDoneDate_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAction_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintActionBy_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSupervisor_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobNumber_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser1_Action_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser2_Action_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser3_Action_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks_Action_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintWorkOrderID_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPriorityID_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintScheduleOfRates_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOwnership_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDueDate_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDoneDate_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAction_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintActionBy_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSupervisor_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobNumber_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser1_Action_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser2_Action_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser3_Action_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks_Action_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintWorkOrderID_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPriorityID_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintScheduleOfRates_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOwnership_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDueDate_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDoneDate_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAction_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintActionBy_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSupervisor_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobNumber_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser1_Action_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser2_Action_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser3_Action_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks_Action_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintWorkOrderID_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPriorityID_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintScheduleOfRates_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOwnership_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDueDate_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDoneDate_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAction_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintActionBy_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSupervisor_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobNumber_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser1_Action_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser2_Action_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser3_Action_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks_Action_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintWorkOrderID_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPriorityID_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintScheduleOfRates_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOwnership_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDueDate_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldtDoneDate_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintAction_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintActionBy_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintSupervisor_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobNumber_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser1_Action_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser2_Action_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrUser3_Action_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks_Action_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintWorkOrderID_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintPriorityID_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintScheduleOfRates_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOwnership_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePhotograph = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionPhotograph = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCavat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownNorth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownSouth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownEast = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownWest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetentionCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSULE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1_Tree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2_Tree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3_Tree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesVariety = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1_Inspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2_Inspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3_Inspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1_Action_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2_Action_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3_Action_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1_Action_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2_Action_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3_Action_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1_Action_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2_Action_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3_Action_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1_Action_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2_Action_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3_Action_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1_Action_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2_Action_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3_Action_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonJobWorkUnits_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditWorkUnits = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colmonJobWorkUnits_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonJobWorkUnits_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonJobWorkUnits_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonJobWorkUnits_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.btnRollBackImport = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditClearPriorSelection = new DevExpress.XtraEditors.CheckEdit();
            this.btnBlockEdit = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditBlockEditAct5Ref = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditBlockEditAct4Ref = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditBlockEditAct3Ref = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditBlockEditAct2Ref = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditBlockEditAct1Ref = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditBlockEditInspRef = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditBlockEditTreeRef = new DevExpress.XtraEditors.CheckEdit();
            this.btnShowRecordsAll = new DevExpress.XtraEditors.SimpleButton();
            this.btnShowRecordsErrorsOnly = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectRecords = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditSelectTreeRef = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSelectAct5Ref = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSelectInspRef = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSelectAct4Ref = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSelectAct1Ref = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSelectAct3Ref = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSelectAct2Ref = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnImport = new DevExpress.XtraEditors.SimpleButton();
            this.ImportFileButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.btnCheckData = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01323_AT_import_from_GBM_workorder_checkingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00098_export_to_GBM_field_listTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00098_export_to_GBM_field_listTableAdapter();
            this.sp00099_export_to_GBM_default_group_listTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00099_export_to_GBM_default_group_listTableAdapter();
            this.bbiCheckedx = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUncheckedx = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetGroup = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barLinkContainerItem2 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barListItem1 = new DevExpress.XtraBars.BarListItem();
            this.sp00100_export_to_GBM_locality_filter_listTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00100_export_to_GBM_locality_filter_listTableAdapter();
            this.sp00101_export_to_GBM_map_centre_locality_listTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00101_export_to_GBM_map_centre_locality_listTableAdapter();
            this.bliSetGroup = new DevExpress.XtraBars.BarListItem();
            this.beiSetGroup = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGroupName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupOrder2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00102_export_to_GBM_map_scale_listTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00102_export_to_GBM_map_scale_listTableAdapter();
            this.sp00103_export_to_GBM_picklist_itemsTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00103_export_to_GBM_picklist_itemsTableAdapter();
            this.pmGrid3Menu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiChecked = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUnchecked = new DevExpress.XtraBars.BarButtonItem();
            this.bsiGroup = new DevExpress.XtraBars.BarSubItem();
            this.bbiSaveTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveTemplateAs = new DevExpress.XtraBars.BarButtonItem();
            this.pmGrid1Menu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiLoadTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.sp00112_export_to_GBM_saved_templates_listTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00112_export_to_GBM_saved_templates_listTableAdapter();
            this.sp00131_export_to_GBM_style_fieldsTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00131_export_to_GBM_style_fieldsTableAdapter();
            this.pmGrid6Menu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiBlockEditStyles = new DevExpress.XtraBars.BarButtonItem();
            this.sp00134_export_to_GBM_export_dataTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00134_export_to_GBM_export_dataTableAdapter();
            this.sp00135_export_to_GBM_map_background_filesTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00135_export_to_GBM_map_background_filesTableAdapter();
            this.sp00136_import_from_GBM_locality_species_list_with_blankTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00136_import_from_GBM_locality_species_list_with_blankTableAdapter();
            this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.sp00137_import_from_GBM_ltree_status_list_with_blankTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00137_import_from_GBM_ltree_status_list_with_blankTableAdapter();
            this.sp00138_import_from_GBM_districts_list_with_blankTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00138_import_from_GBM_districts_list_with_blankTableAdapter();
            this.sp00139_import_from_GBM_locailities_list_with_blankTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00139_import_from_GBM_locailities_list_with_blankTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.sp01321_AT_export_to_GBM_available_workordersTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp01321_AT_export_to_GBM_available_workordersTableAdapter();
            this.sp01323_AT_import_from_GBM_workorder_checkingTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp01323_AT_import_from_GBM_workorder_checkingTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00112exporttoGBMsavedtemplateslistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataTransfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00099exporttoGBMdefaultgrouplistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl_WorkOrders)).BeginInit();
            this.popupContainerControl_WorkOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditWorkOrderCheckingIncludeCompleted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWorkOrderCheckingToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWorkOrderCheckingToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWorkOrderCheckingFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWorkOrderCheckingFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01321ATexporttoGBMavailableworkordersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl3)).BeginInit();
            this.popupContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00135exporttoGBMmapbackgroundfilesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00102exporttoGBMmapscalelistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetATDataTransferBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00101exporttoGBMmapcentrelocalitylistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).BeginInit();
            this.gridSplitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00100exporttoGBMlocalityfilterlistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditExportType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeLastInspectionDetailsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00103exporttoGBMpicklistitemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetATDataTransferBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00098exporttoGBMfieldlistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).BeginInit();
            this.gridSplitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00131exporttoGBMstylefieldsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.glueStyleField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit3View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).BeginInit();
            this.splitContainerControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditImportWorkOrderChecking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).BeginInit();
            this.gridSplitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00134exporttoGBMexportdataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDistrict)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00138importfromGBMdistrictslistwithblankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditLocality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00139importfromGBMlocailitieslistwithblankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditSequenceField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCoordinates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditPolygonXY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00137importfromGBMltreestatuslistwithblankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSpecies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditWorkUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClearPriorSelection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct5Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct4Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct3Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct2Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct1Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditInspRef.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditTreeRef.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectTreeRef.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct5Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectInspRef.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct4Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct1Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct3Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct2Ref.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportFileButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01323_AT_import_from_GBM_workorder_checkingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmGrid3Menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmGrid1Menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmGrid6Menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1100, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 756);
            this.barDockControlBottom.Size = new System.Drawing.Size(1100, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 756);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1100, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 756);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiChecked,
            this.bbiUnchecked,
            this.bsiGroup,
            this.bbiSaveTemplate,
            this.bbiSaveTemplateAs,
            this.bbiLoadTemplate,
            this.bbiEditTemplate,
            this.bbiDeleteTemplate,
            this.bbiBlockEditStyles});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colGroupName1
            // 
            this.colGroupName1.Caption = "Group";
            this.colGroupName1.ColumnEdit = this.repositoryItemTextEdit1;
            this.colGroupName1.FieldName = "GroupName";
            this.colGroupName1.Name = "colGroupName1";
            this.colGroupName1.Visible = true;
            this.colGroupName1.VisibleIndex = 0;
            this.colGroupName1.Width = 221;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 50;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colcolor
            // 
            this.colcolor.Caption = "Colour";
            this.colcolor.FieldName = "color";
            this.colcolor.Name = "colcolor";
            this.colcolor.OptionsColumn.AllowEdit = false;
            this.colcolor.OptionsColumn.AllowFocus = false;
            this.colcolor.OptionsColumn.ReadOnly = true;
            this.colcolor.Width = 43;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Locality ID";
            this.gridColumn1.FieldName = "LocalityID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 62;
            // 
            // colRequired
            // 
            this.colRequired.Caption = "Required";
            this.colRequired.FieldName = "Required";
            this.colRequired.Name = "colRequired";
            this.colRequired.OptionsColumn.AllowEdit = false;
            this.colRequired.OptionsColumn.AllowFocus = false;
            this.colRequired.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colRequired.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colRequired.OptionsColumn.ReadOnly = true;
            this.colRequired.OptionsColumn.TabStop = false;
            // 
            // colDistrictID2
            // 
            this.colDistrictID2.FieldName = "DistrictID";
            this.colDistrictID2.Name = "colDistrictID2";
            this.colDistrictID2.Width = 69;
            // 
            // colLocalityID1
            // 
            this.colLocalityID1.FieldName = "LocalityID";
            this.colLocalityID1.Name = "colLocalityID1";
            this.colLocalityID1.Width = 72;
            // 
            // colStatusID
            // 
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.Width = 67;
            // 
            // colSpeciesID
            // 
            this.colSpeciesID.FieldName = "SpeciesID";
            this.colSpeciesID.Name = "colSpeciesID";
            this.colSpeciesID.Width = 72;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            // 
            // colDistrictName2
            // 
            this.colDistrictName2.FieldName = "DistrictName";
            this.colDistrictName2.Name = "colDistrictName2";
            this.colDistrictName2.Visible = true;
            this.colDistrictName2.VisibleIndex = 0;
            this.colDistrictName2.Width = 190;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 0;
            this.colStatusDescription.Width = 302;
            // 
            // colSpeciesName
            // 
            this.colSpeciesName.FieldName = "SpeciesName";
            this.colSpeciesName.Name = "colSpeciesName";
            this.colSpeciesName.Visible = true;
            this.colSpeciesName.VisibleIndex = 0;
            this.colSpeciesName.Width = 155;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1100, 756);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1095, 730);
            this.xtraTabPage1.Text = "Export to GBM Mobile";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Saved Templates  [Right-click on row for menu]";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1095, 730);
            this.splitContainerControl1.SplitterPosition = 134;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1091, 110);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00112exporttoGBMsavedtemplateslistBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1091, 110);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00112exporttoGBMsavedtemplateslistBindingSource
            // 
            this.sp00112exporttoGBMsavedtemplateslistBindingSource.DataMember = "sp00112_export_to_GBM_saved_templates_list";
            this.sp00112exporttoGBMsavedtemplateslistBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // dataSet_AT_DataTransfer
            // 
            this.dataSet_AT_DataTransfer.DataSetName = "DataSet_AT_DataTransfer";
            this.dataSet_AT_DataTransfer.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTemplateType,
            this.colTemplateDescription,
            this.colTemplateRemarks,
            this.colTemplateID,
            this.colCreatedByID,
            this.colCreatedByName,
            this.colShareType,
            this.colColour});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTemplateType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTemplateDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colTemplateType
            // 
            this.colTemplateType.Caption = "Template Type";
            this.colTemplateType.FieldName = "TemplateType";
            this.colTemplateType.Name = "colTemplateType";
            this.colTemplateType.OptionsColumn.AllowFocus = false;
            this.colTemplateType.OptionsColumn.ReadOnly = true;
            this.colTemplateType.Width = 107;
            // 
            // colTemplateDescription
            // 
            this.colTemplateDescription.Caption = "Description";
            this.colTemplateDescription.FieldName = "TemplateDescription";
            this.colTemplateDescription.Name = "colTemplateDescription";
            this.colTemplateDescription.OptionsColumn.AllowFocus = false;
            this.colTemplateDescription.OptionsColumn.ReadOnly = true;
            this.colTemplateDescription.Visible = true;
            this.colTemplateDescription.VisibleIndex = 0;
            this.colTemplateDescription.Width = 491;
            // 
            // colTemplateRemarks
            // 
            this.colTemplateRemarks.Caption = "Remarks";
            this.colTemplateRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colTemplateRemarks.FieldName = "TemplateRemarks";
            this.colTemplateRemarks.Name = "colTemplateRemarks";
            this.colTemplateRemarks.OptionsColumn.ReadOnly = true;
            this.colTemplateRemarks.Visible = true;
            this.colTemplateRemarks.VisibleIndex = 3;
            this.colTemplateRemarks.Width = 65;
            // 
            // colTemplateID
            // 
            this.colTemplateID.Caption = "Template ID";
            this.colTemplateID.FieldName = "TemplateID";
            this.colTemplateID.Name = "colTemplateID";
            this.colTemplateID.OptionsColumn.AllowFocus = false;
            this.colTemplateID.OptionsColumn.ReadOnly = true;
            this.colTemplateID.Width = 77;
            // 
            // colCreatedByID
            // 
            this.colCreatedByID.Caption = "Created By ID";
            this.colCreatedByID.FieldName = "CreatedByID";
            this.colCreatedByID.Name = "colCreatedByID";
            this.colCreatedByID.OptionsColumn.AllowFocus = false;
            this.colCreatedByID.OptionsColumn.ReadOnly = true;
            this.colCreatedByID.Width = 84;
            // 
            // colCreatedByName
            // 
            this.colCreatedByName.Caption = "Created By";
            this.colCreatedByName.FieldName = "CreatedByName";
            this.colCreatedByName.Name = "colCreatedByName";
            this.colCreatedByName.OptionsColumn.AllowFocus = false;
            this.colCreatedByName.OptionsColumn.ReadOnly = true;
            this.colCreatedByName.Visible = true;
            this.colCreatedByName.VisibleIndex = 1;
            this.colCreatedByName.Width = 135;
            // 
            // colShareType
            // 
            this.colShareType.Caption = "Sharing";
            this.colShareType.FieldName = "ShareType";
            this.colShareType.Name = "colShareType";
            this.colShareType.OptionsColumn.AllowFocus = false;
            this.colShareType.OptionsColumn.ReadOnly = true;
            this.colShareType.Visible = true;
            this.colShareType.VisibleIndex = 2;
            this.colShareType.Width = 106;
            // 
            // colColour
            // 
            this.colColour.Caption = "Colour";
            this.colColour.FieldName = "Colour";
            this.colColour.Name = "colColour";
            this.colColour.OptionsColumn.AllowFocus = false;
            this.colColour.OptionsColumn.ReadOnly = true;
            this.colColour.OptionsColumn.ShowInCustomizationForm = false;
            this.colColour.Width = 53;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel2.Controls.Add(this.IncludeLastInspectionDetailsCheckEdit);
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControl3);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnExport);
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControl2);
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl4);
            this.splitContainerControl2.Size = new System.Drawing.Size(1091, 586);
            this.splitContainerControl2.SplitterPosition = 156;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl3.Panel1.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "Groups";
            this.splitContainerControl3.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl3.Panel2.Controls.Add(this.layoutControl1);
            this.splitContainerControl3.Panel2.ShowCaption = true;
            this.splitContainerControl3.Panel2.Text = "District \\ Localilty Filtering, Map Defaults and Work Order Checking";
            this.splitContainerControl3.Size = new System.Drawing.Size(1091, 156);
            this.splitContainerControl3.SplitterPosition = 341;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(337, 132);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp00099exporttoGBMdefaultgrouplistBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemSpinEdit1});
            this.gridControl2.Size = new System.Drawing.Size(337, 132);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp00099exporttoGBMdefaultgrouplistBindingSource
            // 
            this.sp00099exporttoGBMdefaultgrouplistBindingSource.DataMember = "sp00099_export_to_GBM_default_group_list";
            this.sp00099exporttoGBMdefaultgrouplistBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGroupName1,
            this.colGroupOrder});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.Column = this.colGroupName1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "None";
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView2_ShowingEditor);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            this.gridView2.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView2_ValidatingEditor);
            // 
            // colGroupOrder
            // 
            this.colGroupOrder.Caption = "Order";
            this.colGroupOrder.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colGroupOrder.FieldName = "GroupOrder";
            this.colGroupOrder.Name = "colGroupOrder";
            this.colGroupOrder.Visible = true;
            this.colGroupOrder.VisibleIndex = 1;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.popupContainerEdit2);
            this.layoutControl1.Controls.Add(this.popupContainerEdit3);
            this.layoutControl1.Controls.Add(this.gridLookUpEdit2);
            this.layoutControl1.Controls.Add(this.gridLookUpEdit1);
            this.layoutControl1.Controls.Add(this.popupContainerEdit1);
            this.layoutControl1.Controls.Add(this.comboBoxEditExportType);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.MenuManager = this.barManager1;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(740, 132);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Work Orders Selected [No Work Order Checking]";
            this.popupContainerEdit2.Location = new System.Drawing.Point(118, 104);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.CloseOnOuterMouseClick = false;
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControl_WorkOrders;
            this.popupContainerEdit2.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit2.Size = new System.Drawing.Size(614, 20);
            this.popupContainerEdit2.StyleController = this.layoutControl1;
            this.popupContainerEdit2.TabIndex = 9;
            this.popupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit2_QueryResultValue);
            // 
            // popupContainerControl_WorkOrders
            // 
            this.popupContainerControl_WorkOrders.Controls.Add(this.groupControl1);
            this.popupContainerControl_WorkOrders.Location = new System.Drawing.Point(3, 31);
            this.popupContainerControl_WorkOrders.Name = "popupContainerControl_WorkOrders";
            this.popupContainerControl_WorkOrders.Size = new System.Drawing.Size(518, 344);
            this.popupContainerControl_WorkOrders.TabIndex = 3;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.splitContainerControl6);
            this.groupControl1.Controls.Add(this.btnWorkOrderCheckingOK);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(518, 344);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Work Order Checking  [Tick one or more Work Orders for checking or none for no ch" +
    "ecking]";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl6.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl6.Horizontal = false;
            this.splitContainerControl6.Location = new System.Drawing.Point(5, 25);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.btnWorkOrderCheckingRefresh);
            this.splitContainerControl6.Panel1.Controls.Add(this.checkEditWorkOrderCheckingIncludeCompleted);
            this.splitContainerControl6.Panel1.Controls.Add(this.dateEditWorkOrderCheckingToDate);
            this.splitContainerControl6.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl6.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl6.Panel1.Controls.Add(this.dateEditWorkOrderCheckingFromDate);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.gridSplitContainer4);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(508, 287);
            this.splitContainerControl6.SplitterPosition = 25;
            this.splitContainerControl6.TabIndex = 0;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // btnWorkOrderCheckingRefresh
            // 
            this.btnWorkOrderCheckingRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWorkOrderCheckingRefresh.Location = new System.Drawing.Point(443, 0);
            this.btnWorkOrderCheckingRefresh.Name = "btnWorkOrderCheckingRefresh";
            this.btnWorkOrderCheckingRefresh.Size = new System.Drawing.Size(64, 23);
            this.btnWorkOrderCheckingRefresh.TabIndex = 5;
            this.btnWorkOrderCheckingRefresh.Text = "Refresh";
            this.btnWorkOrderCheckingRefresh.Click += new System.EventHandler(this.btnWorkOrderCheckingRefresh_Click);
            // 
            // checkEditWorkOrderCheckingIncludeCompleted
            // 
            this.checkEditWorkOrderCheckingIncludeCompleted.EditValue = 0;
            this.checkEditWorkOrderCheckingIncludeCompleted.Location = new System.Drawing.Point(257, 2);
            this.checkEditWorkOrderCheckingIncludeCompleted.MenuManager = this.barManager1;
            this.checkEditWorkOrderCheckingIncludeCompleted.Name = "checkEditWorkOrderCheckingIncludeCompleted";
            this.checkEditWorkOrderCheckingIncludeCompleted.Properties.Caption = "Include Completed Work Orders";
            this.checkEditWorkOrderCheckingIncludeCompleted.Properties.ValueChecked = 1;
            this.checkEditWorkOrderCheckingIncludeCompleted.Properties.ValueUnchecked = 0;
            this.checkEditWorkOrderCheckingIncludeCompleted.Size = new System.Drawing.Size(178, 19);
            this.checkEditWorkOrderCheckingIncludeCompleted.TabIndex = 4;
            // 
            // dateEditWorkOrderCheckingToDate
            // 
            this.dateEditWorkOrderCheckingToDate.EditValue = new System.DateTime(((long)(0)));
            this.dateEditWorkOrderCheckingToDate.Location = new System.Drawing.Point(157, 0);
            this.dateEditWorkOrderCheckingToDate.MenuManager = this.barManager1;
            this.dateEditWorkOrderCheckingToDate.Name = "dateEditWorkOrderCheckingToDate";
            this.dateEditWorkOrderCheckingToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWorkOrderCheckingToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditWorkOrderCheckingToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditWorkOrderCheckingToDate.Properties.MinValue = new System.DateTime(1950, 1, 1, 0, 0, 0, 0);
            this.dateEditWorkOrderCheckingToDate.Size = new System.Drawing.Size(80, 20);
            this.dateEditWorkOrderCheckingToDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(135, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditWorkOrderCheckingFromDate
            // 
            this.dateEditWorkOrderCheckingFromDate.EditValue = new System.DateTime(((long)(0)));
            this.dateEditWorkOrderCheckingFromDate.Location = new System.Drawing.Point(38, 1);
            this.dateEditWorkOrderCheckingFromDate.MenuManager = this.barManager1;
            this.dateEditWorkOrderCheckingFromDate.Name = "dateEditWorkOrderCheckingFromDate";
            this.dateEditWorkOrderCheckingFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWorkOrderCheckingFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditWorkOrderCheckingFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditWorkOrderCheckingFromDate.Properties.MinValue = new System.DateTime(1950, 1, 1, 0, 0, 0, 0);
            this.dateEditWorkOrderCheckingFromDate.Size = new System.Drawing.Size(80, 20);
            this.dateEditWorkOrderCheckingFromDate.TabIndex = 0;
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControl9;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControl9);
            this.gridSplitContainer4.Size = new System.Drawing.Size(508, 256);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.sp01321ATexporttoGBMavailableworkordersBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl9.Location = new System.Drawing.Point(0, 0);
            this.gridControl9.MainView = this.gridView12;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemMemoExEdit5});
            this.gridControl9.Size = new System.Drawing.Size(508, 256);
            this.gridControl9.TabIndex = 0;
            this.gridControl9.UseEmbeddedNavigator = true;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12});
            // 
            // sp01321ATexporttoGBMavailableworkordersBindingSource
            // 
            this.sp01321ATexporttoGBMavailableworkordersBindingSource.DataMember = "sp01321_AT_export_to_GBM_available_workorders";
            this.sp01321ATexporttoGBMavailableworkordersBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderID2,
            this.colPaddedWorkOrderID,
            this.colContractor,
            this.colIssuingBody,
            this.colWorkOrderType,
            this.colIssuedBy,
            this.colIssueDate,
            this.colPriority,
            this.colPaddedPreviousWorkOrderID,
            this.colTotalCost,
            this.colWorkUnits,
            this.colCompletedDate,
            this.colcolor,
            this.colEmailDetails,
            this.Calculated1,
            this.Calculated2,
            this.Calculated3,
            this.colIncidents,
            this.colRemarks2,
            this.colWorkOrderUserDefined1,
            this.colWorkOrderUserDefined2,
            this.colWorkOrderUserDefined3,
            this.colCompletedActions,
            this.colCompleteOverTotal,
            this.colIncompleteActions,
            this.colTotalActions});
            this.gridView12.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 518, 208, 191);
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colcolor;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = "DarkGray";
            this.gridView12.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView12.GridControl = this.gridControl9;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsPrint.AutoWidth = false;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colWorkOrderID2
            // 
            this.colWorkOrderID2.Caption = "Work Order ID";
            this.colWorkOrderID2.FieldName = "WorkOrderID";
            this.colWorkOrderID2.Name = "colWorkOrderID2";
            this.colWorkOrderID2.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID2.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID2.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID2.Width = 82;
            // 
            // colPaddedWorkOrderID
            // 
            this.colPaddedWorkOrderID.Caption = "Work Order Ref";
            this.colPaddedWorkOrderID.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID.Name = "colPaddedWorkOrderID";
            this.colPaddedWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID.Visible = true;
            this.colPaddedWorkOrderID.VisibleIndex = 0;
            this.colPaddedWorkOrderID.Width = 111;
            // 
            // colContractor
            // 
            this.colContractor.Caption = "Contractor";
            this.colContractor.FieldName = "Contractor";
            this.colContractor.Name = "colContractor";
            this.colContractor.OptionsColumn.AllowEdit = false;
            this.colContractor.OptionsColumn.AllowFocus = false;
            this.colContractor.OptionsColumn.ReadOnly = true;
            this.colContractor.Visible = true;
            this.colContractor.VisibleIndex = 3;
            this.colContractor.Width = 121;
            // 
            // colIssuingBody
            // 
            this.colIssuingBody.Caption = "Issuing Body";
            this.colIssuingBody.FieldName = "IssuingBody";
            this.colIssuingBody.Name = "colIssuingBody";
            this.colIssuingBody.OptionsColumn.AllowEdit = false;
            this.colIssuingBody.OptionsColumn.AllowFocus = false;
            this.colIssuingBody.OptionsColumn.ReadOnly = true;
            this.colIssuingBody.Visible = true;
            this.colIssuingBody.VisibleIndex = 4;
            this.colIssuingBody.Width = 139;
            // 
            // colWorkOrderType
            // 
            this.colWorkOrderType.Caption = "Type";
            this.colWorkOrderType.FieldName = "WorkOrderType";
            this.colWorkOrderType.Name = "colWorkOrderType";
            this.colWorkOrderType.OptionsColumn.AllowEdit = false;
            this.colWorkOrderType.OptionsColumn.AllowFocus = false;
            this.colWorkOrderType.OptionsColumn.ReadOnly = true;
            this.colWorkOrderType.Visible = true;
            this.colWorkOrderType.VisibleIndex = 1;
            this.colWorkOrderType.Width = 93;
            // 
            // colIssuedBy
            // 
            this.colIssuedBy.Caption = "Issued By";
            this.colIssuedBy.FieldName = "IssuedBy";
            this.colIssuedBy.Name = "colIssuedBy";
            this.colIssuedBy.OptionsColumn.AllowEdit = false;
            this.colIssuedBy.OptionsColumn.AllowFocus = false;
            this.colIssuedBy.OptionsColumn.ReadOnly = true;
            this.colIssuedBy.Visible = true;
            this.colIssuedBy.VisibleIndex = 5;
            this.colIssuedBy.Width = 113;
            // 
            // colIssueDate
            // 
            this.colIssueDate.Caption = "Issue Date";
            this.colIssueDate.FieldName = "IssueDate";
            this.colIssueDate.Name = "colIssueDate";
            this.colIssueDate.OptionsColumn.AllowEdit = false;
            this.colIssueDate.OptionsColumn.AllowFocus = false;
            this.colIssueDate.Visible = true;
            this.colIssueDate.VisibleIndex = 6;
            this.colIssueDate.Width = 74;
            // 
            // colPriority
            // 
            this.colPriority.Caption = "Priority";
            this.colPriority.FieldName = "Priority";
            this.colPriority.Name = "colPriority";
            this.colPriority.OptionsColumn.AllowEdit = false;
            this.colPriority.OptionsColumn.AllowFocus = false;
            this.colPriority.OptionsColumn.ReadOnly = true;
            this.colPriority.Visible = true;
            this.colPriority.VisibleIndex = 7;
            this.colPriority.Width = 62;
            // 
            // colPaddedPreviousWorkOrderID
            // 
            this.colPaddedPreviousWorkOrderID.Caption = "Linked To";
            this.colPaddedPreviousWorkOrderID.FieldName = "PaddedPreviousWorkOrderID";
            this.colPaddedPreviousWorkOrderID.Name = "colPaddedPreviousWorkOrderID";
            this.colPaddedPreviousWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedPreviousWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedPreviousWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedPreviousWorkOrderID.Visible = true;
            this.colPaddedPreviousWorkOrderID.VisibleIndex = 8;
            this.colPaddedPreviousWorkOrderID.Width = 67;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.DisplayFormat.FormatString = "c2";
            this.colTotalCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 9;
            this.colTotalCost.Width = 71;
            // 
            // colWorkUnits
            // 
            this.colWorkUnits.Caption = "Work Units";
            this.colWorkUnits.DisplayFormat.FormatString = "#.00";
            this.colWorkUnits.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colWorkUnits.FieldName = "WorkUnits";
            this.colWorkUnits.Name = "colWorkUnits";
            this.colWorkUnits.OptionsColumn.AllowEdit = false;
            this.colWorkUnits.OptionsColumn.AllowFocus = false;
            this.colWorkUnits.OptionsColumn.ReadOnly = true;
            this.colWorkUnits.Visible = true;
            this.colWorkUnits.VisibleIndex = 10;
            this.colWorkUnits.Width = 74;
            // 
            // colCompletedDate
            // 
            this.colCompletedDate.Caption = "Completed Date";
            this.colCompletedDate.FieldName = "CompletedDate";
            this.colCompletedDate.Name = "colCompletedDate";
            this.colCompletedDate.OptionsColumn.AllowEdit = false;
            this.colCompletedDate.OptionsColumn.AllowFocus = false;
            this.colCompletedDate.Width = 89;
            // 
            // colEmailDetails
            // 
            this.colEmailDetails.Caption = "Email Details";
            this.colEmailDetails.FieldName = "EmailDetails";
            this.colEmailDetails.Name = "colEmailDetails";
            this.colEmailDetails.OptionsColumn.AllowEdit = false;
            this.colEmailDetails.OptionsColumn.AllowFocus = false;
            this.colEmailDetails.Width = 71;
            // 
            // Calculated1
            // 
            this.Calculated1.Caption = "<b>Calculated 1</b>";
            this.Calculated1.FieldName = "Calculated1";
            this.Calculated1.Name = "Calculated1";
            this.Calculated1.OptionsColumn.AllowEdit = false;
            this.Calculated1.OptionsColumn.AllowFocus = false;
            this.Calculated1.OptionsColumn.ReadOnly = true;
            this.Calculated1.ShowUnboundExpressionMenu = true;
            this.Calculated1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1.Visible = true;
            this.Calculated1.VisibleIndex = 16;
            this.Calculated1.Width = 90;
            // 
            // Calculated2
            // 
            this.Calculated2.Caption = "<b>Calculated 2</b>";
            this.Calculated2.FieldName = "Calculated2";
            this.Calculated2.Name = "Calculated2";
            this.Calculated2.OptionsColumn.AllowEdit = false;
            this.Calculated2.OptionsColumn.AllowFocus = false;
            this.Calculated2.OptionsColumn.ReadOnly = true;
            this.Calculated2.ShowUnboundExpressionMenu = true;
            this.Calculated2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2.Visible = true;
            this.Calculated2.VisibleIndex = 17;
            this.Calculated2.Width = 90;
            // 
            // Calculated3
            // 
            this.Calculated3.Caption = "<b>Calculated 3</b>";
            this.Calculated3.FieldName = "Calculated3";
            this.Calculated3.Name = "Calculated3";
            this.Calculated3.OptionsColumn.AllowEdit = false;
            this.Calculated3.OptionsColumn.AllowFocus = false;
            this.Calculated3.OptionsColumn.ReadOnly = true;
            this.Calculated3.ShowUnboundExpressionMenu = true;
            this.Calculated3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3.Visible = true;
            this.Calculated3.VisibleIndex = 18;
            this.Calculated3.Width = 90;
            // 
            // colIncidents
            // 
            this.colIncidents.Caption = "Incidents";
            this.colIncidents.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colIncidents.FieldName = "Incidents";
            this.colIncidents.Name = "colIncidents";
            this.colIncidents.OptionsColumn.ReadOnly = true;
            this.colIncidents.Visible = true;
            this.colIncidents.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 15;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colWorkOrderUserDefined1
            // 
            this.colWorkOrderUserDefined1.Caption = "User Defined 1";
            this.colWorkOrderUserDefined1.FieldName = "WorkOrderUserDefined1";
            this.colWorkOrderUserDefined1.Name = "colWorkOrderUserDefined1";
            this.colWorkOrderUserDefined1.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined1.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined1.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined1.Visible = true;
            this.colWorkOrderUserDefined1.VisibleIndex = 11;
            this.colWorkOrderUserDefined1.Width = 92;
            // 
            // colWorkOrderUserDefined2
            // 
            this.colWorkOrderUserDefined2.Caption = "User Defined 2";
            this.colWorkOrderUserDefined2.FieldName = "WorkOrderUserDefined2";
            this.colWorkOrderUserDefined2.Name = "colWorkOrderUserDefined2";
            this.colWorkOrderUserDefined2.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined2.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined2.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined2.Visible = true;
            this.colWorkOrderUserDefined2.VisibleIndex = 12;
            this.colWorkOrderUserDefined2.Width = 92;
            // 
            // colWorkOrderUserDefined3
            // 
            this.colWorkOrderUserDefined3.Caption = "User Defined 3";
            this.colWorkOrderUserDefined3.FieldName = "WorkOrderUserDefined3";
            this.colWorkOrderUserDefined3.Name = "colWorkOrderUserDefined3";
            this.colWorkOrderUserDefined3.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined3.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined3.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined3.Visible = true;
            this.colWorkOrderUserDefined3.VisibleIndex = 13;
            this.colWorkOrderUserDefined3.Width = 92;
            // 
            // colCompletedActions
            // 
            this.colCompletedActions.Caption = "Completed Total";
            this.colCompletedActions.FieldName = "CompletedActions";
            this.colCompletedActions.Name = "colCompletedActions";
            this.colCompletedActions.OptionsColumn.AllowEdit = false;
            this.colCompletedActions.OptionsColumn.AllowFocus = false;
            this.colCompletedActions.OptionsColumn.ReadOnly = true;
            // 
            // colCompleteOverTotal
            // 
            this.colCompleteOverTotal.Caption = "Completed / Total";
            this.colCompleteOverTotal.FieldName = "CompleteOverTotal";
            this.colCompleteOverTotal.Name = "colCompleteOverTotal";
            this.colCompleteOverTotal.OptionsColumn.AllowEdit = false;
            this.colCompleteOverTotal.OptionsColumn.AllowFocus = false;
            this.colCompleteOverTotal.OptionsColumn.ReadOnly = true;
            this.colCompleteOverTotal.Visible = true;
            this.colCompleteOverTotal.VisibleIndex = 2;
            this.colCompleteOverTotal.Width = 106;
            // 
            // colIncompleteActions
            // 
            this.colIncompleteActions.Caption = "Incompleted Total";
            this.colIncompleteActions.FieldName = "IncompleteActions";
            this.colIncompleteActions.Name = "colIncompleteActions";
            this.colIncompleteActions.OptionsColumn.AllowEdit = false;
            this.colIncompleteActions.OptionsColumn.AllowFocus = false;
            this.colIncompleteActions.OptionsColumn.ReadOnly = true;
            // 
            // colTotalActions
            // 
            this.colTotalActions.Caption = "Total Actions";
            this.colTotalActions.FieldName = "TotalActions";
            this.colTotalActions.Name = "colTotalActions";
            this.colTotalActions.OptionsColumn.AllowEdit = false;
            this.colTotalActions.OptionsColumn.AllowFocus = false;
            this.colTotalActions.OptionsColumn.ReadOnly = true;
            this.colTotalActions.Width = 117;
            // 
            // btnWorkOrderCheckingOK
            // 
            this.btnWorkOrderCheckingOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWorkOrderCheckingOK.Location = new System.Drawing.Point(4, 316);
            this.btnWorkOrderCheckingOK.Name = "btnWorkOrderCheckingOK";
            this.btnWorkOrderCheckingOK.Size = new System.Drawing.Size(74, 24);
            this.btnWorkOrderCheckingOK.TabIndex = 1;
            this.btnWorkOrderCheckingOK.Text = "OK";
            this.btnWorkOrderCheckingOK.Click += new System.EventHandler(this.btnWorkOrderCheckingOK_Click);
            // 
            // popupContainerEdit3
            // 
            this.popupContainerEdit3.EditValue = "No Background Files Selected";
            this.popupContainerEdit3.Location = new System.Drawing.Point(118, 56);
            this.popupContainerEdit3.Name = "popupContainerEdit3";
            this.popupContainerEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit3.Properties.PopupControl = this.popupContainerControl3;
            this.popupContainerEdit3.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit3.Size = new System.Drawing.Size(614, 20);
            this.popupContainerEdit3.StyleController = this.layoutControl1;
            this.popupContainerEdit3.TabIndex = 8;
            this.popupContainerEdit3.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit3_QueryResultValue);
            // 
            // popupContainerControl3
            // 
            this.popupContainerControl3.Controls.Add(this.gridControl8);
            this.popupContainerControl3.Controls.Add(this.btnMapFilesOK);
            this.popupContainerControl3.Location = new System.Drawing.Point(223, 466);
            this.popupContainerControl3.Name = "popupContainerControl3";
            this.popupContainerControl3.Size = new System.Drawing.Size(422, 391);
            this.popupContainerControl3.TabIndex = 10;
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp00135exporttoGBMmapbackgroundfilesBindingSource;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.Location = new System.Drawing.Point(3, 3);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl8.Size = new System.Drawing.Size(416, 360);
            this.gridControl8.TabIndex = 1;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp00135exporttoGBMmapbackgroundfilesBindingSource
            // 
            this.sp00135exporttoGBMmapbackgroundfilesBindingSource.DataMember = "sp00135_export_to_GBM_map_background_files";
            this.sp00135exporttoGBMmapbackgroundfilesBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMapLinkID,
            this.colDistrictID1,
            this.colFileName,
            this.colFileType,
            this.colRemarks1,
            this.colDistrictName1,
            this.colDistrictCode1});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 1;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDistrictName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFileType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFileName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colMapLinkID
            // 
            this.colMapLinkID.FieldName = "MapLinkID";
            this.colMapLinkID.Name = "colMapLinkID";
            this.colMapLinkID.OptionsColumn.AllowFocus = false;
            this.colMapLinkID.OptionsColumn.ReadOnly = true;
            this.colMapLinkID.Width = 77;
            // 
            // colDistrictID1
            // 
            this.colDistrictID1.FieldName = "DistrictID";
            this.colDistrictID1.Name = "colDistrictID1";
            this.colDistrictID1.OptionsColumn.AllowFocus = false;
            this.colDistrictID1.OptionsColumn.ReadOnly = true;
            this.colDistrictID1.Width = 69;
            // 
            // colFileName
            // 
            this.colFileName.FieldName = "FileName";
            this.colFileName.Name = "colFileName";
            this.colFileName.OptionsColumn.AllowFocus = false;
            this.colFileName.OptionsColumn.ReadOnly = true;
            this.colFileName.Visible = true;
            this.colFileName.VisibleIndex = 1;
            this.colFileName.Width = 279;
            // 
            // colFileType
            // 
            this.colFileType.FieldName = "FileType";
            this.colFileType.Name = "colFileType";
            this.colFileType.OptionsColumn.AllowFocus = false;
            this.colFileType.OptionsColumn.ReadOnly = true;
            this.colFileType.Visible = true;
            this.colFileType.VisibleIndex = 0;
            this.colFileType.Width = 78;
            // 
            // colRemarks1
            // 
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 2;
            this.colRemarks1.Width = 121;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colDistrictName1
            // 
            this.colDistrictName1.FieldName = "DistrictName";
            this.colDistrictName1.Name = "colDistrictName1";
            this.colDistrictName1.OptionsColumn.AllowFocus = false;
            this.colDistrictName1.OptionsColumn.ReadOnly = true;
            this.colDistrictName1.Width = 273;
            // 
            // colDistrictCode1
            // 
            this.colDistrictCode1.FieldName = "DistrictCode";
            this.colDistrictCode1.Name = "colDistrictCode1";
            this.colDistrictCode1.OptionsColumn.AllowFocus = false;
            this.colDistrictCode1.OptionsColumn.ReadOnly = true;
            this.colDistrictCode1.Width = 156;
            // 
            // btnMapFilesOK
            // 
            this.btnMapFilesOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMapFilesOK.Location = new System.Drawing.Point(3, 365);
            this.btnMapFilesOK.Name = "btnMapFilesOK";
            this.btnMapFilesOK.Size = new System.Drawing.Size(75, 23);
            this.btnMapFilesOK.TabIndex = 0;
            this.btnMapFilesOK.Text = "OK";
            this.btnMapFilesOK.Click += new System.EventHandler(this.btnMapFilesOK_Click);
            // 
            // gridLookUpEdit2
            // 
            this.gridLookUpEdit2.Location = new System.Drawing.Point(541, 32);
            this.gridLookUpEdit2.Name = "gridLookUpEdit2";
            this.gridLookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEdit2.Properties.DataSource = this.sp00102exporttoGBMmapscalelistBindingSource;
            this.gridLookUpEdit2.Properties.DisplayMember = "ScaleDescription";
            this.gridLookUpEdit2.Properties.NullText = "No Scale Selected";
            this.gridLookUpEdit2.Properties.PopupView = this.gridLookUpEdit2View;
            this.gridLookUpEdit2.Properties.ValueMember = "Scale";
            this.gridLookUpEdit2.Size = new System.Drawing.Size(191, 20);
            this.gridLookUpEdit2.StyleController = this.layoutControl1;
            this.gridLookUpEdit2.TabIndex = 7;
            // 
            // sp00102exporttoGBMmapscalelistBindingSource
            // 
            this.sp00102exporttoGBMmapscalelistBindingSource.DataMember = "sp00102_export_to_GBM_map_scale_list";
            this.sp00102exporttoGBMmapscalelistBindingSource.DataSource = this.dataSetATDataTransferBindingSource;
            // 
            // dataSetATDataTransferBindingSource
            // 
            this.dataSetATDataTransferBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            this.dataSetATDataTransferBindingSource.Position = 0;
            // 
            // gridLookUpEdit2View
            // 
            this.gridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colScaleDescription,
            this.colScale});
            this.gridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit2View.Name = "gridLookUpEdit2View";
            this.gridLookUpEdit2View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colScale, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colScaleDescription
            // 
            this.colScaleDescription.Caption = "Scale";
            this.colScaleDescription.FieldName = "ScaleDescription";
            this.colScaleDescription.Name = "colScaleDescription";
            this.colScaleDescription.OptionsColumn.AllowEdit = false;
            this.colScaleDescription.OptionsColumn.ReadOnly = true;
            this.colScaleDescription.Visible = true;
            this.colScaleDescription.VisibleIndex = 0;
            // 
            // colScale
            // 
            this.colScale.Caption = "Scale Value";
            this.colScale.FieldName = "Scale";
            this.colScale.Name = "colScale";
            this.colScale.OptionsColumn.AllowEdit = false;
            this.colScale.OptionsColumn.ReadOnly = true;
            // 
            // gridLookUpEdit1
            // 
            this.gridLookUpEdit1.Location = new System.Drawing.Point(118, 32);
            this.gridLookUpEdit1.Name = "gridLookUpEdit1";
            this.gridLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEdit1.Properties.DataSource = this.sp00101exporttoGBMmapcentrelocalitylistBindingSource;
            this.gridLookUpEdit1.Properties.DisplayMember = "FullName";
            this.gridLookUpEdit1.Properties.NullText = "No Map Centre Selected";
            this.gridLookUpEdit1.Properties.PopupView = this.gridLookUpEdit1View;
            this.gridLookUpEdit1.Properties.ValueMember = "LocalityID";
            this.gridLookUpEdit1.Size = new System.Drawing.Size(309, 20);
            this.gridLookUpEdit1.StyleController = this.layoutControl1;
            this.gridLookUpEdit1.TabIndex = 6;
            // 
            // sp00101exporttoGBMmapcentrelocalitylistBindingSource
            // 
            this.sp00101exporttoGBMmapcentrelocalitylistBindingSource.DataMember = "sp00101_export_to_GBM_map_centre_locality_list";
            this.sp00101exporttoGBMmapcentrelocalitylistBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.colXcoordinate,
            this.colYcoordinate,
            this.colFullName});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn1;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Locality Code";
            this.gridColumn2.FieldName = "LocalityCode";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 86;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Locality Name";
            this.gridColumn3.FieldName = "LocalityName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 186;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "District ID";
            this.gridColumn4.FieldName = "DistrictID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 69;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "District Code";
            this.gridColumn5.FieldName = "DistrictCode";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 83;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "District Name";
            this.gridColumn6.FieldName = "DistrictName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 178;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Locality Ownership Name";
            this.gridColumn7.FieldName = "LocalityOwnershipName";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            this.gridColumn7.Width = 166;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Locality Ownership Code";
            this.gridColumn8.FieldName = "LocalityOwnershipCode";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 140;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "District Ownership Name";
            this.gridColumn9.FieldName = "DistrictOwnershipName";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            this.gridColumn9.Width = 192;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "District Ownership Code";
            this.gridColumn10.FieldName = "DistrictOwnershipCode";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 137;
            // 
            // colXcoordinate
            // 
            this.colXcoordinate.Caption = "X Coordinate";
            this.colXcoordinate.FieldName = "Xcoordinate";
            this.colXcoordinate.Name = "colXcoordinate";
            this.colXcoordinate.OptionsColumn.AllowEdit = false;
            this.colXcoordinate.OptionsColumn.AllowFocus = false;
            this.colXcoordinate.OptionsColumn.ReadOnly = true;
            // 
            // colYcoordinate
            // 
            this.colYcoordinate.Caption = "Y Coordinate";
            this.colYcoordinate.FieldName = "Ycoordinate";
            this.colYcoordinate.Name = "colYcoordinate";
            this.colYcoordinate.OptionsColumn.AllowEdit = false;
            this.colYcoordinate.OptionsColumn.AllowFocus = false;
            this.colYcoordinate.OptionsColumn.ReadOnly = true;
            // 
            // colFullName
            // 
            this.colFullName.Caption = "Full Name";
            this.colFullName.FieldName = "FullName";
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.AllowEdit = false;
            this.colFullName.OptionsColumn.AllowFocus = false;
            this.colFullName.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "All Localities [No Filter]";
            this.popupContainerEdit1.Location = new System.Drawing.Point(118, 8);
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControl1;
            this.popupContainerEdit1.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit1.Size = new System.Drawing.Size(614, 20);
            this.popupContainerEdit1.StyleController = this.layoutControl1;
            this.popupContainerEdit1.TabIndex = 5;
            this.popupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.gridSplitContainer5);
            this.popupContainerControl1.Controls.Add(this.btnLocalityFilterOK);
            this.popupContainerControl1.Location = new System.Drawing.Point(234, 0);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(422, 391);
            this.popupContainerControl1.TabIndex = 2;
            // 
            // gridSplitContainer5
            // 
            this.gridSplitContainer5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer5.Grid = this.gridControl4;
            this.gridSplitContainer5.Location = new System.Drawing.Point(3, 3);
            this.gridSplitContainer5.Name = "gridSplitContainer5";
            this.gridSplitContainer5.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer5.Size = new System.Drawing.Size(416, 360);
            this.gridSplitContainer5.TabIndex = 3;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp00100exporttoGBMlocalityfilterlistBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(416, 360);
            this.gridControl4.TabIndex = 2;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp00100exporttoGBMlocalityfilterlistBindingSource
            // 
            this.sp00100exporttoGBMlocalityfilterlistBindingSource.DataMember = "sp00100_export_to_GBM_locality_filter_list";
            this.sp00100exporttoGBMlocalityfilterlistBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLocalityID,
            this.colLocalityCode,
            this.colLocalityName,
            this.colDistrictID,
            this.colDistrictCode,
            this.colDistrictName,
            this.colLocalityOwnershipName,
            this.colLocalityOwnershipCode,
            this.colDistrictOwnershipName,
            this.colDistrictOwnershipCode});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDistrictName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLocalityName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colLocalityID
            // 
            this.colLocalityID.Caption = "Locality ID";
            this.colLocalityID.FieldName = "LocalityID";
            this.colLocalityID.Name = "colLocalityID";
            this.colLocalityID.OptionsColumn.AllowEdit = false;
            this.colLocalityID.OptionsColumn.AllowFocus = false;
            this.colLocalityID.OptionsColumn.ReadOnly = true;
            this.colLocalityID.Width = 62;
            // 
            // colLocalityCode
            // 
            this.colLocalityCode.Caption = "Locality Code";
            this.colLocalityCode.FieldName = "LocalityCode";
            this.colLocalityCode.Name = "colLocalityCode";
            this.colLocalityCode.OptionsColumn.AllowEdit = false;
            this.colLocalityCode.OptionsColumn.AllowFocus = false;
            this.colLocalityCode.OptionsColumn.ReadOnly = true;
            this.colLocalityCode.Width = 86;
            // 
            // colLocalityName
            // 
            this.colLocalityName.Caption = "Locality Name";
            this.colLocalityName.FieldName = "LocalityName";
            this.colLocalityName.Name = "colLocalityName";
            this.colLocalityName.OptionsColumn.AllowEdit = false;
            this.colLocalityName.OptionsColumn.AllowFocus = false;
            this.colLocalityName.OptionsColumn.ReadOnly = true;
            this.colLocalityName.Visible = true;
            this.colLocalityName.VisibleIndex = 0;
            this.colLocalityName.Width = 186;
            // 
            // colDistrictID
            // 
            this.colDistrictID.Caption = "District ID";
            this.colDistrictID.FieldName = "DistrictID";
            this.colDistrictID.Name = "colDistrictID";
            this.colDistrictID.OptionsColumn.AllowEdit = false;
            this.colDistrictID.OptionsColumn.AllowFocus = false;
            this.colDistrictID.OptionsColumn.ReadOnly = true;
            this.colDistrictID.Width = 69;
            // 
            // colDistrictCode
            // 
            this.colDistrictCode.Caption = "District Code";
            this.colDistrictCode.FieldName = "DistrictCode";
            this.colDistrictCode.Name = "colDistrictCode";
            this.colDistrictCode.OptionsColumn.AllowEdit = false;
            this.colDistrictCode.OptionsColumn.AllowFocus = false;
            this.colDistrictCode.OptionsColumn.ReadOnly = true;
            this.colDistrictCode.Width = 83;
            // 
            // colDistrictName
            // 
            this.colDistrictName.Caption = "District Name";
            this.colDistrictName.FieldName = "DistrictName";
            this.colDistrictName.Name = "colDistrictName";
            this.colDistrictName.OptionsColumn.AllowEdit = false;
            this.colDistrictName.OptionsColumn.AllowFocus = false;
            this.colDistrictName.OptionsColumn.ReadOnly = true;
            this.colDistrictName.Visible = true;
            this.colDistrictName.VisibleIndex = 1;
            this.colDistrictName.Width = 178;
            // 
            // colLocalityOwnershipName
            // 
            this.colLocalityOwnershipName.Caption = "Locality Ownership Name";
            this.colLocalityOwnershipName.FieldName = "LocalityOwnershipName";
            this.colLocalityOwnershipName.Name = "colLocalityOwnershipName";
            this.colLocalityOwnershipName.OptionsColumn.AllowEdit = false;
            this.colLocalityOwnershipName.OptionsColumn.AllowFocus = false;
            this.colLocalityOwnershipName.OptionsColumn.ReadOnly = true;
            this.colLocalityOwnershipName.Visible = true;
            this.colLocalityOwnershipName.VisibleIndex = 1;
            this.colLocalityOwnershipName.Width = 166;
            // 
            // colLocalityOwnershipCode
            // 
            this.colLocalityOwnershipCode.Caption = "Locality Ownership Code";
            this.colLocalityOwnershipCode.FieldName = "LocalityOwnershipCode";
            this.colLocalityOwnershipCode.Name = "colLocalityOwnershipCode";
            this.colLocalityOwnershipCode.OptionsColumn.AllowEdit = false;
            this.colLocalityOwnershipCode.OptionsColumn.AllowFocus = false;
            this.colLocalityOwnershipCode.OptionsColumn.ReadOnly = true;
            this.colLocalityOwnershipCode.Width = 140;
            // 
            // colDistrictOwnershipName
            // 
            this.colDistrictOwnershipName.Caption = "District Ownership Name";
            this.colDistrictOwnershipName.FieldName = "DistrictOwnershipName";
            this.colDistrictOwnershipName.Name = "colDistrictOwnershipName";
            this.colDistrictOwnershipName.OptionsColumn.AllowEdit = false;
            this.colDistrictOwnershipName.OptionsColumn.AllowFocus = false;
            this.colDistrictOwnershipName.OptionsColumn.ReadOnly = true;
            this.colDistrictOwnershipName.Visible = true;
            this.colDistrictOwnershipName.VisibleIndex = 2;
            this.colDistrictOwnershipName.Width = 192;
            // 
            // colDistrictOwnershipCode
            // 
            this.colDistrictOwnershipCode.Caption = "District Ownership Code";
            this.colDistrictOwnershipCode.FieldName = "DistrictOwnershipCode";
            this.colDistrictOwnershipCode.Name = "colDistrictOwnershipCode";
            this.colDistrictOwnershipCode.OptionsColumn.AllowEdit = false;
            this.colDistrictOwnershipCode.OptionsColumn.AllowFocus = false;
            this.colDistrictOwnershipCode.OptionsColumn.ReadOnly = true;
            this.colDistrictOwnershipCode.Width = 137;
            // 
            // btnLocalityFilterOK
            // 
            this.btnLocalityFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLocalityFilterOK.Location = new System.Drawing.Point(3, 365);
            this.btnLocalityFilterOK.Name = "btnLocalityFilterOK";
            this.btnLocalityFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnLocalityFilterOK.TabIndex = 2;
            this.btnLocalityFilterOK.Text = "OK";
            this.btnLocalityFilterOK.Click += new System.EventHandler(this.btnLocalityFilterOK_Click);
            // 
            // comboBoxEditExportType
            // 
            this.comboBoxEditExportType.EditValue = "Survey";
            this.comboBoxEditExportType.Location = new System.Drawing.Point(118, 80);
            this.comboBoxEditExportType.MenuManager = this.barManager1;
            this.comboBoxEditExportType.Name = "comboBoxEditExportType";
            this.comboBoxEditExportType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditExportType.Properties.Items.AddRange(new object[] {
            "Survey",
            "Work Order Checking",
            "Survey and Work Order Checking"});
            this.comboBoxEditExportType.Properties.PopupSizeable = true;
            this.comboBoxEditExportType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditExportType.Size = new System.Drawing.Size(614, 20);
            this.comboBoxEditExportType.StyleController = this.layoutControl1;
            this.comboBoxEditExportType.TabIndex = 10;
            this.comboBoxEditExportType.EditValueChanged += new System.EventHandler(this.comboBoxEditExportType_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.layoutControlItem30,
            this.layoutControlItem4,
            this.layoutControlItem31});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(740, 132);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHtmlStringInCaption = true;
            this.layoutControlItem2.Control = this.popupContainerEdit1;
            this.layoutControlItem2.CustomizationFormText = "Locality Filter:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(728, 24);
            this.layoutControlItem2.Text = "Locality Filter:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AllowHtmlStringInCaption = true;
            this.layoutControlItem3.Control = this.gridLookUpEdit1;
            this.layoutControlItem3.CustomizationFormText = "Centre Map On:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(423, 24);
            this.layoutControlItem3.Text = "Centre Map On:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AllowHtmlStringInCaption = true;
            this.layoutControlItem7.Control = this.popupContainerEdit3;
            this.layoutControlItem7.CustomizationFormText = "Map Background Files:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(728, 24);
            this.layoutControlItem7.Text = "Map Background Files:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.popupContainerEdit2;
            this.layoutControlItem30.CustomizationFormText = "Work Orders:";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(728, 24);
            this.layoutControlItem30.Text = "Work Orders:";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AllowHtmlStringInCaption = true;
            this.layoutControlItem4.Control = this.gridLookUpEdit2;
            this.layoutControlItem4.CustomizationFormText = "Map Scale:";
            this.layoutControlItem4.Location = new System.Drawing.Point(423, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(305, 24);
            this.layoutControlItem4.Text = "Map Scale:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.Control = this.comboBoxEditExportType;
            this.layoutControlItem31.CustomizationFormText = "Export Type:";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(728, 24);
            this.layoutControlItem31.Text = "Export Type:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(107, 13);
            // 
            // IncludeLastInspectionDetailsCheckEdit
            // 
            this.IncludeLastInspectionDetailsCheckEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IncludeLastInspectionDetailsCheckEdit.Location = new System.Drawing.Point(129, 394);
            this.IncludeLastInspectionDetailsCheckEdit.MenuManager = this.barManager1;
            this.IncludeLastInspectionDetailsCheckEdit.Name = "IncludeLastInspectionDetailsCheckEdit";
            this.IncludeLastInspectionDetailsCheckEdit.Properties.Caption = "Include Last Inspection Details";
            this.IncludeLastInspectionDetailsCheckEdit.Size = new System.Drawing.Size(241, 19);
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Include Last Inspection Details - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.IncludeLastInspectionDetailsCheckEdit.SuperTip = superToolTip1;
            this.IncludeLastInspectionDetailsCheckEdit.TabIndex = 11;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExport.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnExport.Appearance.Options.UseFont = true;
            this.btnExport.Location = new System.Drawing.Point(4, 393);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(119, 23);
            this.btnExport.TabIndex = 1;
            this.btnExport.Text = "Export Data";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Controls.Add(this.gridControl5);
            this.popupContainerControl2.Controls.Add(this.btnFilterFilterOK);
            this.popupContainerControl2.Location = new System.Drawing.Point(648, 466);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(443, 391);
            this.popupContainerControl2.TabIndex = 3;
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp00103exporttoGBMpicklistitemsBindingSource;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(437, 360);
            this.gridControl5.TabIndex = 2;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp00103exporttoGBMpicklistitemsBindingSource
            // 
            this.sp00103exporttoGBMpicklistitemsBindingSource.DataMember = "sp00103_export_to_GBM_picklist_items";
            this.sp00103exporttoGBMpicklistitemsBindingSource.DataSource = this.dataSetATDataTransferBindingSource1;
            // 
            // dataSetATDataTransferBindingSource1
            // 
            this.dataSetATDataTransferBindingSource1.DataSource = this.dataSet_AT_DataTransfer;
            this.dataSetATDataTransferBindingSource1.Position = 0;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemDescription,
            this.colItemCode,
            this.colItemID,
            this.colHeaderID,
            this.colHeaderDescription,
            this.colOrder});
            this.gridView5.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 517, 208, 191);
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsCustomization.AllowFilter = false;
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHeaderDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Item Description";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 260;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item Code";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.OptionsColumn.AllowFocus = false;
            this.colItemCode.OptionsColumn.ReadOnly = true;
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 1;
            this.colItemCode.Width = 225;
            // 
            // colItemID
            // 
            this.colItemID.Caption = "Item ID";
            this.colItemID.FieldName = "ItemID";
            this.colItemID.Name = "colItemID";
            this.colItemID.OptionsColumn.AllowEdit = false;
            this.colItemID.OptionsColumn.AllowFocus = false;
            this.colItemID.OptionsColumn.ReadOnly = true;
            this.colItemID.Width = 48;
            // 
            // colHeaderID
            // 
            this.colHeaderID.Caption = "Header ID";
            this.colHeaderID.FieldName = "HeaderID";
            this.colHeaderID.Name = "colHeaderID";
            this.colHeaderID.OptionsColumn.AllowEdit = false;
            this.colHeaderID.OptionsColumn.AllowFocus = false;
            this.colHeaderID.OptionsColumn.ReadOnly = true;
            this.colHeaderID.Width = 61;
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Pick List Type";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Width = 258;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.Width = 83;
            // 
            // btnFilterFilterOK
            // 
            this.btnFilterFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFilterFilterOK.Location = new System.Drawing.Point(3, 365);
            this.btnFilterFilterOK.Name = "btnFilterFilterOK";
            this.btnFilterFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnFilterFilterOK.TabIndex = 2;
            this.btnFilterFilterOK.Text = "OK";
            this.btnFilterFilterOK.Click += new System.EventHandler(this.btnFilterFilterOK_Click);
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl4.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl4.Location = new System.Drawing.Point(1, 2);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl4.Panel1.Controls.Add(this.popupContainerControl_WorkOrders);
            this.splitContainerControl4.Panel1.Controls.Add(this.popupContainerControl1);
            this.splitContainerControl4.Panel1.Controls.Add(this.gridSplitContainer3);
            this.splitContainerControl4.Panel1.ShowCaption = true;
            this.splitContainerControl4.Panel1.Text = "Export Layout";
            this.splitContainerControl4.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl4.Panel2.Controls.Add(this.gridSplitContainer6);
            this.splitContainerControl4.Panel2.Controls.Add(this.layoutControl2);
            this.splitContainerControl4.Panel2.ShowCaption = true;
            this.splitContainerControl4.Panel2.Text = "Styles";
            this.splitContainerControl4.Size = new System.Drawing.Size(1084, 385);
            this.splitContainerControl4.SplitterPosition = 672;
            this.splitContainerControl4.TabIndex = 5;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(668, 361);
            this.gridSplitContainer3.TabIndex = 4;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp00098exporttoGBMfieldlistBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckEdit4});
            this.gridControl3.Size = new System.Drawing.Size(668, 361);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            this.gridControl3.Paint += new System.Windows.Forms.PaintEventHandler(this.gridControl3_Paint);
            this.gridControl3.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControl3_DragDrop);
            this.gridControl3.DragOver += new System.Windows.Forms.DragEventHandler(this.gridControl3_DragOver);
            this.gridControl3.DragLeave += new System.EventHandler(this.gridControl3_DragLeave);
            // 
            // sp00098exporttoGBMfieldlistBindingSource
            // 
            this.sp00098exporttoGBMfieldlistBindingSource.DataMember = "sp00098_export_to_GBM_field_list";
            this.sp00098exporttoGBMfieldlistBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colColumnID,
            this.colInternalColumnName,
            this.colExternalColumnName,
            this.colColumnLabel,
            this.colColumnType,
            this.colColumnLength,
            this.colGroupName,
            this.colTableName,
            this.colColumnOrder,
            this.colPickList,
            this.colPicklistSummery,
            this.colRemarks,
            this.colRequired,
            this.colGroupOrder3,
            this.colEditable,
            this.colSearchable,
            this.colManditory,
            this.colUseLastRecord,
            this.colUseLastRecordAvailable});
            styleFormatCondition4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            styleFormatCondition4.Appearance.Options.UseFont = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colRequired;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 1;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsCustomization.AllowGroup = false;
            this.gridView3.OptionsCustomization.AllowSort = false;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colColumnOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView3_ShowingEditor);
            this.gridView3.HiddenEditor += new System.EventHandler(this.gridView3_HiddenEditor);
            this.gridView3.ShownEditor += new System.EventHandler(this.gridView3_ShownEditor);
            this.gridView3.CustomColumnSort += new DevExpress.XtraGrid.Views.Base.CustomColumnSortEventHandler(this.gridView3_CustomColumnSort);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseDown);
            this.gridView3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseMove);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            this.gridView3.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView3_ValidatingEditor);
            // 
            // colColumnID
            // 
            this.colColumnID.Caption = "Column ID";
            this.colColumnID.FieldName = "ColumnID";
            this.colColumnID.Name = "colColumnID";
            this.colColumnID.OptionsColumn.AllowEdit = false;
            this.colColumnID.OptionsColumn.AllowFocus = false;
            this.colColumnID.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnID.OptionsColumn.ReadOnly = true;
            this.colColumnID.OptionsColumn.TabStop = false;
            this.colColumnID.Width = 61;
            // 
            // colInternalColumnName
            // 
            this.colInternalColumnName.Caption = "Internal Column Name";
            this.colInternalColumnName.FieldName = "InternalColumnName";
            this.colInternalColumnName.Name = "colInternalColumnName";
            this.colInternalColumnName.OptionsColumn.AllowEdit = false;
            this.colInternalColumnName.OptionsColumn.AllowFocus = false;
            this.colInternalColumnName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colInternalColumnName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colInternalColumnName.OptionsColumn.ReadOnly = true;
            this.colInternalColumnName.OptionsColumn.TabStop = false;
            this.colInternalColumnName.Width = 118;
            // 
            // colExternalColumnName
            // 
            this.colExternalColumnName.Caption = "Column Name";
            this.colExternalColumnName.FieldName = "ExternalColumnName";
            this.colExternalColumnName.Name = "colExternalColumnName";
            this.colExternalColumnName.OptionsColumn.AllowEdit = false;
            this.colExternalColumnName.OptionsColumn.AllowFocus = false;
            this.colExternalColumnName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colExternalColumnName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colExternalColumnName.OptionsColumn.ReadOnly = true;
            this.colExternalColumnName.OptionsColumn.TabStop = false;
            this.colExternalColumnName.Visible = true;
            this.colExternalColumnName.VisibleIndex = 0;
            this.colExternalColumnName.Width = 169;
            // 
            // colColumnLabel
            // 
            this.colColumnLabel.Caption = "Screen Label";
            this.colColumnLabel.FieldName = "ColumnLabel";
            this.colColumnLabel.Name = "colColumnLabel";
            this.colColumnLabel.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnLabel.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnLabel.Visible = true;
            this.colColumnLabel.VisibleIndex = 1;
            this.colColumnLabel.Width = 159;
            // 
            // colColumnType
            // 
            this.colColumnType.Caption = "Type";
            this.colColumnType.FieldName = "ColumnType";
            this.colColumnType.Name = "colColumnType";
            this.colColumnType.OptionsColumn.AllowEdit = false;
            this.colColumnType.OptionsColumn.AllowFocus = false;
            this.colColumnType.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnType.OptionsColumn.ReadOnly = true;
            this.colColumnType.OptionsColumn.TabStop = false;
            this.colColumnType.Width = 63;
            // 
            // colColumnLength
            // 
            this.colColumnLength.Caption = "Length";
            this.colColumnLength.FieldName = "ColumnLength";
            this.colColumnLength.Name = "colColumnLength";
            this.colColumnLength.OptionsColumn.AllowEdit = false;
            this.colColumnLength.OptionsColumn.AllowFocus = false;
            this.colColumnLength.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnLength.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnLength.OptionsColumn.ReadOnly = true;
            this.colColumnLength.OptionsColumn.TabStop = false;
            this.colColumnLength.Width = 55;
            // 
            // colGroupName
            // 
            this.colGroupName.Caption = "Group";
            this.colGroupName.FieldName = "GroupName";
            this.colGroupName.Name = "colGroupName";
            this.colGroupName.OptionsColumn.AllowEdit = false;
            this.colGroupName.OptionsColumn.AllowFocus = false;
            this.colGroupName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colGroupName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colGroupName.OptionsColumn.ReadOnly = true;
            this.colGroupName.OptionsColumn.TabStop = false;
            this.colGroupName.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.colGroupName.Width = 100;
            // 
            // colTableName
            // 
            this.colTableName.Caption = "Table";
            this.colTableName.FieldName = "TableName";
            this.colTableName.Name = "colTableName";
            this.colTableName.OptionsColumn.AllowEdit = false;
            this.colTableName.OptionsColumn.AllowFocus = false;
            this.colTableName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colTableName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colTableName.OptionsColumn.ReadOnly = true;
            this.colTableName.OptionsColumn.TabStop = false;
            this.colTableName.Visible = true;
            this.colTableName.VisibleIndex = 3;
            this.colTableName.Width = 88;
            // 
            // colColumnOrder
            // 
            this.colColumnOrder.Caption = "Order";
            this.colColumnOrder.FieldName = "ColumnOrder";
            this.colColumnOrder.Name = "colColumnOrder";
            this.colColumnOrder.OptionsColumn.AllowEdit = false;
            this.colColumnOrder.OptionsColumn.AllowFocus = false;
            this.colColumnOrder.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnOrder.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colColumnOrder.OptionsColumn.ReadOnly = true;
            this.colColumnOrder.OptionsColumn.TabStop = false;
            this.colColumnOrder.Width = 67;
            // 
            // colPickList
            // 
            this.colPickList.Caption = "Picklist ID";
            this.colPickList.FieldName = "PickList";
            this.colPickList.Name = "colPickList";
            this.colPickList.OptionsColumn.AllowEdit = false;
            this.colPickList.OptionsColumn.AllowFocus = false;
            this.colPickList.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colPickList.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPickList.OptionsColumn.ReadOnly = true;
            this.colPickList.OptionsColumn.TabStop = false;
            this.colPickList.Width = 57;
            // 
            // colPicklistSummery
            // 
            this.colPicklistSummery.Caption = "Picklist";
            this.colPicklistSummery.ColumnEdit = this.repositoryItemPopupContainerEdit1;
            this.colPicklistSummery.FieldName = "PicklistSummery";
            this.colPicklistSummery.Name = "colPicklistSummery";
            this.colPicklistSummery.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colPicklistSummery.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPicklistSummery.Visible = true;
            this.colPicklistSummery.VisibleIndex = 2;
            this.colPicklistSummery.Width = 76;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControl2;
            this.repositoryItemPopupContainerEdit1.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit1_QueryResultValue);
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colRemarks.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 7;
            this.colRemarks.Width = 63;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            // 
            // colGroupOrder3
            // 
            this.colGroupOrder3.Caption = "Group Order";
            this.colGroupOrder3.FieldName = "GroupOrder";
            this.colGroupOrder3.Name = "colGroupOrder3";
            this.colGroupOrder3.OptionsColumn.AllowEdit = false;
            this.colGroupOrder3.OptionsColumn.AllowFocus = false;
            this.colGroupOrder3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colGroupOrder3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colGroupOrder3.OptionsColumn.ReadOnly = true;
            this.colGroupOrder3.OptionsColumn.TabStop = false;
            // 
            // colEditable
            // 
            this.colEditable.Caption = "Editable";
            this.colEditable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colEditable.FieldName = "Editable";
            this.colEditable.Name = "colEditable";
            this.colEditable.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colEditable.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colEditable.Visible = true;
            this.colEditable.VisibleIndex = 5;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colSearchable
            // 
            this.colSearchable.Caption = "Searchable";
            this.colSearchable.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSearchable.FieldName = "Searchable";
            this.colSearchable.Name = "colSearchable";
            this.colSearchable.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colSearchable.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colSearchable.Visible = true;
            this.colSearchable.VisibleIndex = 6;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colManditory
            // 
            this.colManditory.Caption = "Mandatory";
            this.colManditory.FieldName = "Manditory";
            this.colManditory.Name = "colManditory";
            this.colManditory.OptionsColumn.AllowEdit = false;
            this.colManditory.OptionsColumn.AllowFocus = false;
            this.colManditory.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colManditory.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colManditory.OptionsColumn.ReadOnly = true;
            this.colManditory.OptionsColumn.TabStop = false;
            // 
            // colUseLastRecord
            // 
            this.colUseLastRecord.Caption = "Use Last Value";
            this.colUseLastRecord.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colUseLastRecord.FieldName = "UseLastRecord";
            this.colUseLastRecord.Name = "colUseLastRecord";
            this.colUseLastRecord.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colUseLastRecord.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colUseLastRecord.Visible = true;
            this.colUseLastRecord.VisibleIndex = 4;
            this.colUseLastRecord.Width = 92;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colUseLastRecordAvailable
            // 
            this.colUseLastRecordAvailable.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colUseLastRecordAvailable.FieldName = "UseLastRecordAvailable";
            this.colUseLastRecordAvailable.Name = "colUseLastRecordAvailable";
            this.colUseLastRecordAvailable.OptionsColumn.AllowEdit = false;
            this.colUseLastRecordAvailable.OptionsColumn.AllowFocus = false;
            this.colUseLastRecordAvailable.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colUseLastRecordAvailable.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colUseLastRecordAvailable.OptionsColumn.ReadOnly = true;
            this.colUseLastRecordAvailable.OptionsColumn.TabStop = false;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // gridSplitContainer6
            // 
            this.gridSplitContainer6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer6.Grid = this.gridControl6;
            this.gridSplitContainer6.Location = new System.Drawing.Point(0, 31);
            this.gridSplitContainer6.Name = "gridSplitContainer6";
            this.gridSplitContainer6.Panel1.Controls.Add(this.gridControl6);
            this.gridSplitContainer6.Size = new System.Drawing.Size(402, 330);
            this.gridSplitContainer6.TabIndex = 7;
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp00131exporttoGBMstylefieldsBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemColorEdit1,
            this.repositoryItemColorEdit2,
            this.repositoryItemColorEdit3,
            this.repositoryItemSpinEdit2,
            this.repositoryItemImageComboBox2,
            this.repositoryItemColorEdit4,
            this.repositoryItemImageComboBox3});
            this.gridControl6.Size = new System.Drawing.Size(402, 330);
            this.gridControl6.TabIndex = 9;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp00131exporttoGBMstylefieldsBindingSource
            // 
            this.sp00131exporttoGBMstylefieldsBindingSource.DataMember = "sp00131_export_to_GBM_style_fields";
            this.sp00131exporttoGBMstylefieldsBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.colSymbol,
            this.colSize,
            this.colSymbolColour,
            this.colPolygonFillColour,
            this.colPolygonLineColour,
            this.colPolygonFillPattern,
            this.colPolygonFillPatternColour,
            this.colPolygonLineWidth});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn16, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseDown);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn11
            // 
            this.gridColumn11.FieldName = "ItemDescription";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsColumn.TabStop = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 193;
            // 
            // gridColumn12
            // 
            this.gridColumn12.FieldName = "ItemCode";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsColumn.TabStop = false;
            this.gridColumn12.Width = 62;
            // 
            // gridColumn13
            // 
            this.gridColumn13.FieldName = "ItemID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsColumn.TabStop = false;
            this.gridColumn13.Width = 48;
            // 
            // gridColumn14
            // 
            this.gridColumn14.FieldName = "HeaderID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsColumn.TabStop = false;
            this.gridColumn14.Width = 61;
            // 
            // gridColumn15
            // 
            this.gridColumn15.FieldName = "HeaderDescription";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.OptionsColumn.TabStop = false;
            this.gridColumn15.Width = 103;
            // 
            // gridColumn16
            // 
            this.gridColumn16.FieldName = "Order";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.OptionsColumn.TabStop = false;
            this.gridColumn16.Width = 53;
            // 
            // colSymbol
            // 
            this.colSymbol.Caption = "Point Symbol";
            this.colSymbol.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colSymbol.FieldName = "Symbol";
            this.colSymbol.Name = "colSymbol";
            this.colSymbol.Visible = true;
            this.colSymbol.VisibleIndex = 1;
            this.colSymbol.Width = 94;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Circle", 35, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Square", 33, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diamond", 34, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 1", 37, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 2", 38, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Star", 36, 5)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.imageList1;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "symbol_circle_16.png");
            this.imageList1.Images.SetKeyName(1, "symbol_square_16.png");
            this.imageList1.Images.SetKeyName(2, "symbol_diamond_16.png");
            this.imageList1.Images.SetKeyName(3, "symbol_triangle1_16.png");
            this.imageList1.Images.SetKeyName(4, "symbol_triangle2_16.png");
            this.imageList1.Images.SetKeyName(5, "symbol_star_16.png");
            // 
            // colSize
            // 
            this.colSize.Caption = "Point Size";
            this.colSize.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colSize.FieldName = "Size";
            this.colSize.Name = "colSize";
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 2;
            this.colSize.Width = 68;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.DisplayFormat.FormatString = "f2";
            this.repositoryItemSpinEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.Mask.EditMask = "f2";
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // colSymbolColour
            // 
            this.colSymbolColour.Caption = "Point Colour";
            this.colSymbolColour.ColumnEdit = this.repositoryItemColorEdit1;
            this.colSymbolColour.FieldName = "SymbolColour";
            this.colSymbolColour.Name = "colSymbolColour";
            this.colSymbolColour.Visible = true;
            this.colSymbolColour.VisibleIndex = 3;
            this.colSymbolColour.Width = 120;
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            this.repositoryItemColorEdit1.StoreColorAsInteger = true;
            // 
            // colPolygonFillColour
            // 
            this.colPolygonFillColour.ColumnEdit = this.repositoryItemColorEdit2;
            this.colPolygonFillColour.FieldName = "PolygonFillColour";
            this.colPolygonFillColour.Name = "colPolygonFillColour";
            this.colPolygonFillColour.Visible = true;
            this.colPolygonFillColour.VisibleIndex = 4;
            this.colPolygonFillColour.Width = 120;
            // 
            // repositoryItemColorEdit2
            // 
            this.repositoryItemColorEdit2.AutoHeight = false;
            this.repositoryItemColorEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit2.Name = "repositoryItemColorEdit2";
            this.repositoryItemColorEdit2.StoreColorAsInteger = true;
            // 
            // colPolygonLineColour
            // 
            this.colPolygonLineColour.ColumnEdit = this.repositoryItemColorEdit3;
            this.colPolygonLineColour.FieldName = "PolygonLineColour";
            this.colPolygonLineColour.Name = "colPolygonLineColour";
            this.colPolygonLineColour.Visible = true;
            this.colPolygonLineColour.VisibleIndex = 8;
            this.colPolygonLineColour.Width = 120;
            // 
            // repositoryItemColorEdit3
            // 
            this.repositoryItemColorEdit3.AutoHeight = false;
            this.repositoryItemColorEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit3.Name = "repositoryItemColorEdit3";
            this.repositoryItemColorEdit3.StoreColorAsInteger = true;
            // 
            // colPolygonFillPattern
            // 
            this.colPolygonFillPattern.ColumnEdit = this.repositoryItemImageComboBox3;
            this.colPolygonFillPattern.FieldName = "PolygonFillPattern";
            this.colPolygonFillPattern.Name = "colPolygonFillPattern";
            this.colPolygonFillPattern.Visible = true;
            this.colPolygonFillPattern.VisibleIndex = 5;
            this.colPolygonFillPattern.Width = 114;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Solid", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Horizontal Lines", 3, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Vertical Lines", 4, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diagonal Lines", 6, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Crosshatch", 8, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dots 1", 14, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dots 2", 17, 6)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            this.repositoryItemImageComboBox3.SmallImages = this.imageList4;
            // 
            // imageList4
            // 
            this.imageList4.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList4.ImageStream")));
            this.imageList4.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList4.Images.SetKeyName(0, "pattern_02.png");
            this.imageList4.Images.SetKeyName(1, "pattern_03.png");
            this.imageList4.Images.SetKeyName(2, "pattern_04.png");
            this.imageList4.Images.SetKeyName(3, "pattern_06.png");
            this.imageList4.Images.SetKeyName(4, "pattern_08.png");
            this.imageList4.Images.SetKeyName(5, "pattern_14.png");
            this.imageList4.Images.SetKeyName(6, "pattern_17.png");
            // 
            // colPolygonFillPatternColour
            // 
            this.colPolygonFillPatternColour.ColumnEdit = this.repositoryItemColorEdit4;
            this.colPolygonFillPatternColour.FieldName = "PolygonFillPatternColour";
            this.colPolygonFillPatternColour.Name = "colPolygonFillPatternColour";
            this.colPolygonFillPatternColour.Visible = true;
            this.colPolygonFillPatternColour.VisibleIndex = 6;
            this.colPolygonFillPatternColour.Width = 148;
            // 
            // repositoryItemColorEdit4
            // 
            this.repositoryItemColorEdit4.AutoHeight = false;
            this.repositoryItemColorEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit4.Name = "repositoryItemColorEdit4";
            this.repositoryItemColorEdit4.StoreColorAsInteger = true;
            // 
            // colPolygonLineWidth
            // 
            this.colPolygonLineWidth.ColumnEdit = this.repositoryItemImageComboBox2;
            this.colPolygonLineWidth.FieldName = "PolygonLineWidth";
            this.colPolygonLineWidth.Name = "colPolygonLineWidth";
            this.colPolygonLineWidth.Visible = true;
            this.colPolygonLineWidth.VisibleIndex = 7;
            this.colPolygonLineWidth.Width = 113;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("1 Pixel", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("2 Pixels", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("3 Pixels", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("4 Pixels", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("5 Pixels", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("6 Pixels", 6, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("7 Pixels", 7, 6)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            this.repositoryItemImageComboBox2.SmallImages = this.imageList3;
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "Border_01.png");
            this.imageList3.Images.SetKeyName(1, "Border_02.png");
            this.imageList3.Images.SetKeyName(2, "Border_03.png");
            this.imageList3.Images.SetKeyName(3, "Border_04.png");
            this.imageList3.Images.SetKeyName(4, "Border_05.png");
            this.imageList3.Images.SetKeyName(5, "Border_06.png");
            this.imageList3.Images.SetKeyName(6, "Border_07.png");
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.Controls.Add(this.glueStyleField);
            this.layoutControl2.Location = new System.Drawing.Point(-2, -1);
            this.layoutControl2.MenuManager = this.barManager1;
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(404, 31);
            this.layoutControl2.TabIndex = 6;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // glueStyleField
            // 
            this.glueStyleField.EditValue = "No Style Field";
            this.glueStyleField.Location = new System.Drawing.Point(61, 4);
            this.glueStyleField.MenuManager = this.barManager1;
            this.glueStyleField.Name = "glueStyleField";
            editorButtonImageOptions1.EnableTransparency = false;
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "No Styling Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>clear all styles</b>.\r\n\r\nNo style information will be exported to " +
    "GBM Mobile.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.glueStyleField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "No Styling", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.glueStyleField.Properties.DataSource = this.sp00098exporttoGBMfieldlistBindingSource;
            this.glueStyleField.Properties.DisplayMember = "ColumnLabel";
            this.glueStyleField.Properties.NullText = "No Style Field";
            this.glueStyleField.Properties.PopupView = this.gridLookUpEdit3View;
            this.glueStyleField.Properties.ValueMember = "ColumnID";
            this.glueStyleField.Size = new System.Drawing.Size(338, 20);
            this.glueStyleField.StyleController = this.layoutControl2;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Style Field - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to select the field which will control thematic styling on the map. \r\n\r\n" +
    "Thematic styling allows you to colour objects on the map according to underlying" +
    " data.";
            toolTipTitleItem4.LeftIndent = 6;
            toolTipTitleItem4.Text = "To clear styles, click the No Styling button.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            superToolTip3.Items.Add(toolTipSeparatorItem1);
            superToolTip3.Items.Add(toolTipTitleItem4);
            this.glueStyleField.SuperTip = superToolTip3;
            this.glueStyleField.TabIndex = 6;
            this.glueStyleField.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.glueStyleField_QueryPopUp);
            this.glueStyleField.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.glueStyleField_ButtonClick);
            this.glueStyleField.EditValueChanged += new System.EventHandler(this.glueStyleField_EditValueChanged);
            // 
            // gridLookUpEdit3View
            // 
            this.gridLookUpEdit3View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35});
            this.gridLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit3View.GroupCount = 1;
            this.gridLookUpEdit3View.Name = "gridLookUpEdit3View";
            this.gridLookUpEdit3View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit3View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit3View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit3View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit3View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit3View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit3View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit3View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn17
            // 
            this.gridColumn17.FieldName = "ColumnID";
            this.gridColumn17.Name = "gridColumn17";
            // 
            // gridColumn18
            // 
            this.gridColumn18.FieldName = "InternalColumnName";
            this.gridColumn18.Name = "gridColumn18";
            // 
            // gridColumn19
            // 
            this.gridColumn19.FieldName = "ExternalColumnName";
            this.gridColumn19.Name = "gridColumn19";
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Style Field  [Screen Label]";
            this.gridColumn20.FieldName = "ColumnLabel";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            this.gridColumn20.Width = 277;
            // 
            // gridColumn21
            // 
            this.gridColumn21.FieldName = "ColumnType";
            this.gridColumn21.Name = "gridColumn21";
            // 
            // gridColumn22
            // 
            this.gridColumn22.FieldName = "ColumnLength";
            this.gridColumn22.Name = "gridColumn22";
            // 
            // gridColumn23
            // 
            this.gridColumn23.FieldName = "GroupName";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            this.gridColumn23.Width = 127;
            // 
            // gridColumn24
            // 
            this.gridColumn24.FieldName = "TableName";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn25
            // 
            this.gridColumn25.FieldName = "ColumnOrder";
            this.gridColumn25.Name = "gridColumn25";
            // 
            // gridColumn26
            // 
            this.gridColumn26.FieldName = "PickList";
            this.gridColumn26.Name = "gridColumn26";
            // 
            // gridColumn27
            // 
            this.gridColumn27.FieldName = "PicklistSummery";
            this.gridColumn27.Name = "gridColumn27";
            // 
            // gridColumn28
            // 
            this.gridColumn28.FieldName = "Remarks";
            this.gridColumn28.Name = "gridColumn28";
            // 
            // gridColumn29
            // 
            this.gridColumn29.FieldName = "Required";
            this.gridColumn29.Name = "gridColumn29";
            // 
            // gridColumn30
            // 
            this.gridColumn30.FieldName = "GroupOrder";
            this.gridColumn30.Name = "gridColumn30";
            // 
            // gridColumn31
            // 
            this.gridColumn31.FieldName = "Editable";
            this.gridColumn31.Name = "gridColumn31";
            // 
            // gridColumn32
            // 
            this.gridColumn32.FieldName = "Searchable";
            this.gridColumn32.Name = "gridColumn32";
            // 
            // gridColumn33
            // 
            this.gridColumn33.FieldName = "Manditory";
            this.gridColumn33.Name = "gridColumn33";
            // 
            // gridColumn34
            // 
            this.gridColumn34.FieldName = "UseLastRecordAvailable";
            this.gridColumn34.Name = "gridColumn34";
            // 
            // gridColumn35
            // 
            this.gridColumn35.FieldName = "UseLastRecord";
            this.gridColumn35.Name = "gridColumn35";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutControlGroup2.Size = new System.Drawing.Size(404, 31);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.glueStyleField;
            this.layoutControlItem6.CustomizationFormText = "Style Field:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 4, 5);
            this.layoutControlItem6.Size = new System.Drawing.Size(404, 31);
            this.layoutControlItem6.Text = "Style Field:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(53, 13);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl7);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1095, 730);
            this.xtraTabPage2.Text = "Import from GBM Mobile";
            // 
            // splitContainerControl7
            // 
            this.splitContainerControl7.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl7.Horizontal = false;
            this.splitContainerControl7.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl7.Name = "splitContainerControl7";
            this.splitContainerControl7.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl7.Panel1.Controls.Add(this.layoutControl5);
            this.splitContainerControl7.Panel1.ShowCaption = true;
            this.splitContainerControl7.Panel1.Text = "Import Work Order Checking";
            this.splitContainerControl7.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl7.Panel2.Controls.Add(this.layoutControl3);
            this.splitContainerControl7.Panel2.ShowCaption = true;
            this.splitContainerControl7.Panel2.Text = "Import Survey";
            this.splitContainerControl7.Size = new System.Drawing.Size(1095, 730);
            this.splitContainerControl7.SplitterPosition = 60;
            this.splitContainerControl7.TabIndex = 3;
            this.splitContainerControl7.Text = "splitContainerControl7";
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.buttonEditImportWorkOrderChecking);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup9;
            this.layoutControl5.Size = new System.Drawing.Size(1091, 36);
            this.layoutControl5.TabIndex = 0;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // buttonEditImportWorkOrderChecking
            // 
            this.buttonEditImportWorkOrderChecking.Location = new System.Drawing.Point(170, 7);
            this.buttonEditImportWorkOrderChecking.MenuManager = this.barManager1;
            this.buttonEditImportWorkOrderChecking.Name = "buttonEditImportWorkOrderChecking";
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem5.Text = "Load and Import File - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to open the Windows File Selection screen to load <b>and import</b> the " +
    "selected Work Order Checking file.";
            superToolTip4.Items.Add(toolTipTitleItem5);
            superToolTip4.Items.Add(toolTipItem4);
            this.buttonEditImportWorkOrderChecking.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Load and Import File", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip4, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditImportWorkOrderChecking.Properties.NullValuePrompt = "Click Load and Import File";
            this.buttonEditImportWorkOrderChecking.Properties.NullValuePromptShowForEmptyValue = true;
            this.buttonEditImportWorkOrderChecking.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditImportWorkOrderChecking.Size = new System.Drawing.Size(914, 22);
            this.buttonEditImportWorkOrderChecking.StyleController = this.layoutControl5;
            this.buttonEditImportWorkOrderChecking.TabIndex = 4;
            this.buttonEditImportWorkOrderChecking.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditImportWorkOrderChecking_ButtonClick);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup9";
            this.layoutControlGroup9.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup9.GroupBordersVisible = false;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem32});
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup9.Size = new System.Drawing.Size(1091, 36);
            this.layoutControlGroup9.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.buttonEditImportWorkOrderChecking;
            this.layoutControlItem32.CustomizationFormText = "Import Work Order Checking File:";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(1081, 26);
            this.layoutControlItem32.Text = "Import Work Order Checking File:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(160, 13);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.splitContainerControl5);
            this.layoutControl3.Controls.Add(this.btnImport);
            this.layoutControl3.Controls.Add(this.ImportFileButtonEdit);
            this.layoutControl3.Controls.Add(this.btnCheckData);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.MenuManager = this.barManager1;
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(1091, 640);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl5.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl5.Location = new System.Drawing.Point(8, 34);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.gridSplitContainer7);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.layoutControl4);
            this.splitContainerControl5.Panel2.Text = "Select Records \\ Block Edit";
            this.splitContainerControl5.Size = new System.Drawing.Size(1075, 598);
            this.splitContainerControl5.SplitterPosition = 199;
            this.splitContainerControl5.TabIndex = 3;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // gridSplitContainer7
            // 
            this.gridSplitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer7.Grid = this.gridControl7;
            this.gridSplitContainer7.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer7.Name = "gridSplitContainer7";
            this.gridSplitContainer7.Panel1.Controls.Add(this.gridControl7);
            this.gridSplitContainer7.Size = new System.Drawing.Size(870, 598);
            this.gridSplitContainer7.TabIndex = 0;
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp00134exporttoGBMexportdataBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEditPolygonXY,
            this.repositoryItemTextEditCoordinates,
            this.repositoryItemGridLookUpEditSpecies,
            this.repositoryItemGridLookUpEditStatus,
            this.repositoryItemGridLookUpEditDistrict,
            this.repositoryItemGridLookUpEditLocality,
            this.repositoryItemDateEdit1,
            this.repositoryItemButtonEditSequenceField,
            this.repositoryItemSpinEditWorkUnits});
            this.gridControl7.Size = new System.Drawing.Size(870, 598);
            this.gridControl7.TabIndex = 1;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp00134exporttoGBMexportdataBindingSource
            // 
            this.sp00134exporttoGBMexportdataBindingSource.DataMember = "sp00134_export_to_GBM_export_data";
            this.sp00134exporttoGBMexportdataBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colintDistrictID,
            this.colintLocalityID,
            this.colstrTreeRef,
            this.colintX,
            this.colintY,
            this.colstrPolygonXY,
            this.colintStatus,
            this.colintAgeClass,
            this.colintDistance,
            this.colstrHouseName,
            this.colintSpeciesID,
            this.colintAccess,
            this.colintVisibility,
            this.colstrContext,
            this.colstrManagement,
            this.colintLegalStatus,
            this.coldtLastInspectionDate,
            this.colintInspectionCycle,
            this.colstrInspectionUnit,
            this.coldtNextInspectionDate,
            this.colintGroundType,
            this.coldecHeight,
            this.colintHeightRange,
            this.colintDBH,
            this.colintDBHRange,
            this.colintProtectionType,
            this.colintCrownDiameter,
            this.colintCrownDepth,
            this.coldtPlantDate,
            this.colintGroupNo,
            this.colintSafetyPriority,
            this.colintSiteHazardClass,
            this.colintPlantSize,
            this.colintPlantSource,
            this.colintPlantMethod,
            this.colstrPostcode,
            this.colstrMapLabel,
            this.colintSize,
            this.colintNearbyObject,
            this.colintNearbyObject2,
            this.colintNearbyObject3,
            this.colstrRemarks_Tree,
            this.colstrUser1_Tree,
            this.colstrUser2_Tree,
            this.colstrUser3_Tree,
            this.coldecAreaHa,
            this.colintType,
            this.colstrHedgeOwner,
            this.colintLength,
            this.coldecGardenSize,
            this.coldecPropertyProximity,
            this.coldecSiteLevel,
            this.colintReplantCount,
            this.coldtSurveyDate,
            this.colintTreeOwnership,
            this.colstrInspectRef,
            this.colintInspector,
            this.coldtInspectDate,
            this.colintIncidentID,
            this.colintStemPhysical,
            this.colintStemPhysical2,
            this.colintStemPhysical3,
            this.colintStemDisease,
            this.colintStemDisease2,
            this.colintStemDisease3,
            this.colintAngleToVertical,
            this.colintCrownPhysical,
            this.colintCrownPhysical2,
            this.colintCrownPhysical3,
            this.colintCrownDisease,
            this.colintCrownDisease2,
            this.colintCrownDisease3,
            this.colintCrownFoliation,
            this.colintCrownFoliation2,
            this.colintCrownFoliation3,
            this.colintSafety,
            this.colintGeneralCondition,
            this.colintRootHeave,
            this.colintRootHeave2,
            this.colintRootHeave3,
            this.colstrUser1_Inspection,
            this.colstrUser2_Inspection,
            this.colstrUser3_Inspection,
            this.colstrRemarks_Inspection,
            this.colintVitality,
            this.coldtDueDate_1,
            this.coldtDoneDate_1,
            this.colintAction_1,
            this.colintActionBy_1,
            this.colintSupervisor_1,
            this.colstrJobNumber_1,
            this.colstrUser1_Action_1,
            this.colstrUser2_Action_1,
            this.colstrUser3_Action_1,
            this.colstrRemarks_Action_1,
            this.colintWorkOrderID_1,
            this.colintPriorityID_1,
            this.colintScheduleOfRates_1,
            this.colintOwnership_1,
            this.coldtDueDate_2,
            this.coldtDoneDate_2,
            this.colintAction_2,
            this.colintActionBy_2,
            this.colintSupervisor_2,
            this.colstrJobNumber_2,
            this.colstrUser1_Action_2,
            this.colstrUser2_Action_2,
            this.colstrUser3_Action_2,
            this.colstrRemarks_Action_2,
            this.colintWorkOrderID_2,
            this.colintPriorityID_2,
            this.colintScheduleOfRates_2,
            this.colintOwnership_2,
            this.coldtDueDate_3,
            this.coldtDoneDate_3,
            this.colintAction_3,
            this.colintActionBy_3,
            this.colintSupervisor_3,
            this.colstrJobNumber_3,
            this.colstrUser1_Action_3,
            this.colstrUser2_Action_3,
            this.colstrUser3_Action_3,
            this.colstrRemarks_Action_3,
            this.colintWorkOrderID_3,
            this.colintPriorityID_3,
            this.colintScheduleOfRates_3,
            this.colintOwnership_3,
            this.coldtDueDate_4,
            this.coldtDoneDate_4,
            this.colintAction_4,
            this.colintActionBy_4,
            this.colintSupervisor_4,
            this.colstrJobNumber_4,
            this.colstrUser1_Action_4,
            this.colstrUser2_Action_4,
            this.colstrUser3_Action_4,
            this.colstrRemarks_Action_4,
            this.colintWorkOrderID_4,
            this.colintPriorityID_4,
            this.colintScheduleOfRates_4,
            this.colintOwnership_4,
            this.coldtDueDate_5,
            this.coldtDoneDate_5,
            this.colintAction_5,
            this.colintActionBy_5,
            this.colintSupervisor_5,
            this.colstrJobNumber_5,
            this.colstrUser1_Action_5,
            this.colstrUser2_Action_5,
            this.colstrUser3_Action_5,
            this.colstrRemarks_Action_5,
            this.colintWorkOrderID_5,
            this.colintPriorityID_5,
            this.colintScheduleOfRates_5,
            this.colintOwnership_5,
            this.colObjectType,
            this.colRecordOrder,
            this.colTreePhotograph,
            this.colInspectionPhotograph,
            this.colCavat,
            this.colStemCount,
            this.colCrownNorth,
            this.colCrownSouth,
            this.colCrownEast,
            this.colCrownWest,
            this.colRetentionCategory,
            this.colSULE,
            this.colUserPicklist1_Tree,
            this.colUserPicklist2_Tree,
            this.colUserPicklist3_Tree,
            this.colSpeciesVariety,
            this.colObjectLength,
            this.colObjectWidth,
            this.colUserPicklist1_Inspection,
            this.colUserPicklist2_Inspection,
            this.colUserPicklist3_Inspection,
            this.colUserPicklist1_Action_1,
            this.colUserPicklist2_Action_1,
            this.colUserPicklist3_Action_1,
            this.colUserPicklist1_Action_2,
            this.colUserPicklist2_Action_2,
            this.colUserPicklist3_Action_2,
            this.colUserPicklist1_Action_3,
            this.colUserPicklist2_Action_3,
            this.colUserPicklist3_Action_3,
            this.colUserPicklist1_Action_4,
            this.colUserPicklist2_Action_4,
            this.colUserPicklist3_Action_4,
            this.colUserPicklist1_Action_5,
            this.colUserPicklist2_Action_5,
            this.colUserPicklist3_Action_5,
            this.colmonJobWorkUnits_1,
            this.colmonJobWorkUnits_2,
            this.colmonJobWorkUnits_3,
            this.colmonJobWorkUnits_4,
            this.colmonJobWorkUnits_5});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView7_RowCellStyle);
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseDown);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            this.gridView7.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView7_ValidatingEditor);
            // 
            // colID
            // 
            this.colID.Caption = "Map ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Visible = true;
            this.colID.VisibleIndex = 1;
            this.colID.Width = 56;
            // 
            // colintDistrictID
            // 
            this.colintDistrictID.Caption = "District";
            this.colintDistrictID.ColumnEdit = this.repositoryItemGridLookUpEditDistrict;
            this.colintDistrictID.FieldName = "intDistrictID";
            this.colintDistrictID.Name = "colintDistrictID";
            this.colintDistrictID.Visible = true;
            this.colintDistrictID.VisibleIndex = 2;
            this.colintDistrictID.Width = 130;
            // 
            // repositoryItemGridLookUpEditDistrict
            // 
            this.repositoryItemGridLookUpEditDistrict.AutoHeight = false;
            this.repositoryItemGridLookUpEditDistrict.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditDistrict.DataSource = this.sp00138importfromGBMdistrictslistwithblankBindingSource;
            this.repositoryItemGridLookUpEditDistrict.DisplayMember = "DistrictName";
            this.repositoryItemGridLookUpEditDistrict.Name = "repositoryItemGridLookUpEditDistrict";
            this.repositoryItemGridLookUpEditDistrict.NullText = "";
            this.repositoryItemGridLookUpEditDistrict.PopupView = this.gridView10;
            this.repositoryItemGridLookUpEditDistrict.ValueMember = "DistrictID";
            this.repositoryItemGridLookUpEditDistrict.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditDistrict_EditValueChanged);
            this.repositoryItemGridLookUpEditDistrict.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.repositoryItemGridLookUpEditDistrict_CustomDisplayText);
            // 
            // sp00138importfromGBMdistrictslistwithblankBindingSource
            // 
            this.sp00138importfromGBMdistrictslistwithblankBindingSource.DataMember = "sp00138_import_from_GBM_districts_list_with_blank";
            this.sp00138importfromGBMdistrictslistwithblankBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDistrictID2,
            this.colDistrictName2,
            this.colOwnershipID,
            this.colOwnershipName});
            this.gridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.colDistrictID2;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridView10.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsBehavior.Editable = false;
            this.gridView10.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView10.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView10.OptionsView.ShowIndicator = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDistrictName2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colOwnershipID
            // 
            this.colOwnershipID.FieldName = "OwnershipID";
            this.colOwnershipID.Name = "colOwnershipID";
            this.colOwnershipID.Width = 87;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 1;
            this.colOwnershipName.Width = 193;
            // 
            // colintLocalityID
            // 
            this.colintLocalityID.Caption = "Locality";
            this.colintLocalityID.ColumnEdit = this.repositoryItemGridLookUpEditLocality;
            this.colintLocalityID.FieldName = "intLocalityID";
            this.colintLocalityID.Name = "colintLocalityID";
            this.colintLocalityID.Visible = true;
            this.colintLocalityID.VisibleIndex = 3;
            this.colintLocalityID.Width = 130;
            // 
            // repositoryItemGridLookUpEditLocality
            // 
            this.repositoryItemGridLookUpEditLocality.AutoHeight = false;
            this.repositoryItemGridLookUpEditLocality.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditLocality.DataSource = this.sp00139importfromGBMlocailitieslistwithblankBindingSource;
            this.repositoryItemGridLookUpEditLocality.DisplayMember = "LocalityName";
            this.repositoryItemGridLookUpEditLocality.Name = "repositoryItemGridLookUpEditLocality";
            this.repositoryItemGridLookUpEditLocality.NullText = "";
            this.repositoryItemGridLookUpEditLocality.PopupView = this.gridView11;
            this.repositoryItemGridLookUpEditLocality.ValueMember = "LocalityID";
            this.repositoryItemGridLookUpEditLocality.Popup += new System.EventHandler(this.repositoryItemGridLookUpEditLocality_Popup);
            this.repositoryItemGridLookUpEditLocality.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditLocality_EditValueChanged);
            this.repositoryItemGridLookUpEditLocality.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.repositoryItemGridLookUpEditLocality_CustomDisplayText);
            // 
            // sp00139importfromGBMlocailitieslistwithblankBindingSource
            // 
            this.sp00139importfromGBMlocailitieslistwithblankBindingSource.DataMember = "sp00139_import_from_GBM_locailities_list_with_blank";
            this.sp00139importfromGBMlocailitieslistwithblankBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDistrictCode2,
            this.colDistrictID3,
            this.colDistrictName3,
            this.colDistrictOwnershipCode1,
            this.colDistrictOwnershipName1,
            this.colLocalityCode1,
            this.colLocalityID1,
            this.colLocalityName1,
            this.colLocalityOwnershipCode1,
            this.colLocalityOwnershipName1});
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.colLocalityID1;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView11.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView11.GroupCount = 1;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsBehavior.Editable = false;
            this.gridView11.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView11.OptionsView.ShowIndicator = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDistrictName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLocalityName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDistrictCode2
            // 
            this.colDistrictCode2.FieldName = "DistrictCode";
            this.colDistrictCode2.Name = "colDistrictCode2";
            this.colDistrictCode2.Width = 162;
            // 
            // colDistrictID3
            // 
            this.colDistrictID3.FieldName = "DistrictID";
            this.colDistrictID3.Name = "colDistrictID3";
            this.colDistrictID3.Width = 69;
            // 
            // colDistrictName3
            // 
            this.colDistrictName3.FieldName = "DistrictName";
            this.colDistrictName3.Name = "colDistrictName3";
            this.colDistrictName3.Visible = true;
            this.colDistrictName3.VisibleIndex = 0;
            this.colDistrictName3.Width = 197;
            // 
            // colDistrictOwnershipCode1
            // 
            this.colDistrictOwnershipCode1.FieldName = "DistrictOwnershipCode";
            this.colDistrictOwnershipCode1.Name = "colDistrictOwnershipCode1";
            this.colDistrictOwnershipCode1.Width = 137;
            // 
            // colDistrictOwnershipName1
            // 
            this.colDistrictOwnershipName1.FieldName = "DistrictOwnershipName";
            this.colDistrictOwnershipName1.Name = "colDistrictOwnershipName1";
            this.colDistrictOwnershipName1.Width = 139;
            // 
            // colLocalityCode1
            // 
            this.colLocalityCode1.FieldName = "LocalityCode";
            this.colLocalityCode1.Name = "colLocalityCode1";
            this.colLocalityCode1.Width = 132;
            // 
            // colLocalityName1
            // 
            this.colLocalityName1.FieldName = "LocalityName";
            this.colLocalityName1.Name = "colLocalityName1";
            this.colLocalityName1.Visible = true;
            this.colLocalityName1.VisibleIndex = 0;
            this.colLocalityName1.Width = 271;
            // 
            // colLocalityOwnershipCode1
            // 
            this.colLocalityOwnershipCode1.FieldName = "LocalityOwnershipCode";
            this.colLocalityOwnershipCode1.Name = "colLocalityOwnershipCode1";
            this.colLocalityOwnershipCode1.Width = 140;
            // 
            // colLocalityOwnershipName1
            // 
            this.colLocalityOwnershipName1.FieldName = "LocalityOwnershipName";
            this.colLocalityOwnershipName1.Name = "colLocalityOwnershipName1";
            this.colLocalityOwnershipName1.Width = 142;
            // 
            // colstrTreeRef
            // 
            this.colstrTreeRef.Caption = "Tree Reference";
            this.colstrTreeRef.ColumnEdit = this.repositoryItemButtonEditSequenceField;
            this.colstrTreeRef.FieldName = "strTreeRef";
            this.colstrTreeRef.Name = "colstrTreeRef";
            this.colstrTreeRef.Visible = true;
            this.colstrTreeRef.VisibleIndex = 4;
            this.colstrTreeRef.Width = 160;
            // 
            // repositoryItemButtonEditSequenceField
            // 
            this.repositoryItemButtonEditSequenceField.AutoHeight = false;
            editorButtonImageOptions3.EnableTransparency = false;
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Text = "Sequence Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip5.Items.Add(toolTipTitleItem6);
            superToolTip5.Items.Add(toolTipItem5);
            editorButtonImageOptions4.EnableTransparency = false;
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Text = "Number Button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip6.Items.Add(toolTipTitleItem7);
            superToolTip6.Items.Add(toolTipItem6);
            this.repositoryItemButtonEditSequenceField.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Seq", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "Sequence", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Num", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "Number", superToolTip6, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditSequenceField.Name = "repositoryItemButtonEditSequenceField";
            this.repositoryItemButtonEditSequenceField.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditSequenceField_ButtonClick);
            // 
            // colintX
            // 
            this.colintX.Caption = "X Coord";
            this.colintX.ColumnEdit = this.repositoryItemTextEditCoordinates;
            this.colintX.FieldName = "intX";
            this.colintX.Name = "colintX";
            this.colintX.Visible = true;
            this.colintX.VisibleIndex = 5;
            this.colintX.Width = 80;
            // 
            // repositoryItemTextEditCoordinates
            // 
            this.repositoryItemTextEditCoordinates.AutoHeight = false;
            this.repositoryItemTextEditCoordinates.Mask.EditMask = "f2";
            this.repositoryItemTextEditCoordinates.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCoordinates.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCoordinates.Name = "repositoryItemTextEditCoordinates";
            // 
            // colintY
            // 
            this.colintY.Caption = "Y Coord";
            this.colintY.ColumnEdit = this.repositoryItemTextEditCoordinates;
            this.colintY.FieldName = "intY";
            this.colintY.Name = "colintY";
            this.colintY.Visible = true;
            this.colintY.VisibleIndex = 6;
            this.colintY.Width = 80;
            // 
            // colstrPolygonXY
            // 
            this.colstrPolygonXY.Caption = "Polygon XY";
            this.colstrPolygonXY.ColumnEdit = this.repositoryItemMemoExEditPolygonXY;
            this.colstrPolygonXY.FieldName = "strPolygonXY";
            this.colstrPolygonXY.Name = "colstrPolygonXY";
            this.colstrPolygonXY.Visible = true;
            this.colstrPolygonXY.VisibleIndex = 7;
            this.colstrPolygonXY.Width = 80;
            // 
            // repositoryItemMemoExEditPolygonXY
            // 
            this.repositoryItemMemoExEditPolygonXY.AutoHeight = false;
            this.repositoryItemMemoExEditPolygonXY.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditPolygonXY.Name = "repositoryItemMemoExEditPolygonXY";
            this.repositoryItemMemoExEditPolygonXY.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEditPolygonXY.ShowIcon = false;
            // 
            // colintStatus
            // 
            this.colintStatus.Caption = "Tree Status";
            this.colintStatus.ColumnEdit = this.repositoryItemGridLookUpEditStatus;
            this.colintStatus.FieldName = "intStatus";
            this.colintStatus.Name = "colintStatus";
            this.colintStatus.Visible = true;
            this.colintStatus.VisibleIndex = 8;
            this.colintStatus.Width = 100;
            // 
            // repositoryItemGridLookUpEditStatus
            // 
            this.repositoryItemGridLookUpEditStatus.AutoHeight = false;
            this.repositoryItemGridLookUpEditStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditStatus.DataSource = this.sp00137importfromGBMltreestatuslistwithblankBindingSource;
            this.repositoryItemGridLookUpEditStatus.DisplayMember = "StatusDescription";
            this.repositoryItemGridLookUpEditStatus.Name = "repositoryItemGridLookUpEditStatus";
            this.repositoryItemGridLookUpEditStatus.NullText = "";
            this.repositoryItemGridLookUpEditStatus.PopupView = this.gridView9;
            this.repositoryItemGridLookUpEditStatus.ValueMember = "StatusID";
            this.repositoryItemGridLookUpEditStatus.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.repositoryItemGridLookUpEditStatus_CustomDisplayText);
            // 
            // sp00137importfromGBMltreestatuslistwithblankBindingSource
            // 
            this.sp00137importfromGBMltreestatuslistwithblankBindingSource.DataMember = "sp00137_import_from_GBM_ltree_status_list_with_blank";
            this.sp00137importfromGBMltreestatuslistwithblankBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStatusDescription,
            this.colStatusID,
            this.colStatusOrder});
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition7.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition7.Appearance.Options.UseForeColor = true;
            styleFormatCondition7.ApplyToRow = true;
            styleFormatCondition7.Column = this.colStatusID;
            styleFormatCondition7.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition7.Value1 = 0;
            this.gridView9.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition7});
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsBehavior.Editable = false;
            this.gridView9.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9.OptionsView.ShowIndicator = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStatusOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colStatusOrder
            // 
            this.colStatusOrder.FieldName = "StatusOrder";
            this.colStatusOrder.Name = "colStatusOrder";
            this.colStatusOrder.Width = 101;
            // 
            // colintAgeClass
            // 
            this.colintAgeClass.Caption = "Age Class";
            this.colintAgeClass.FieldName = "intAgeClass";
            this.colintAgeClass.Name = "colintAgeClass";
            this.colintAgeClass.OptionsColumn.AllowFocus = false;
            this.colintAgeClass.OptionsColumn.ReadOnly = true;
            this.colintAgeClass.Visible = true;
            this.colintAgeClass.VisibleIndex = 11;
            this.colintAgeClass.Width = 69;
            // 
            // colintDistance
            // 
            this.colintDistance.Caption = "Distance";
            this.colintDistance.FieldName = "intDistance";
            this.colintDistance.Name = "colintDistance";
            this.colintDistance.OptionsColumn.AllowFocus = false;
            this.colintDistance.OptionsColumn.ReadOnly = true;
            this.colintDistance.Visible = true;
            this.colintDistance.VisibleIndex = 12;
            this.colintDistance.Width = 78;
            // 
            // colstrHouseName
            // 
            this.colstrHouseName.Caption = "Nearest House";
            this.colstrHouseName.FieldName = "strHouseName";
            this.colstrHouseName.Name = "colstrHouseName";
            this.colstrHouseName.OptionsColumn.AllowFocus = false;
            this.colstrHouseName.OptionsColumn.ReadOnly = true;
            this.colstrHouseName.Visible = true;
            this.colstrHouseName.VisibleIndex = 13;
            this.colstrHouseName.Width = 98;
            // 
            // colintSpeciesID
            // 
            this.colintSpeciesID.Caption = "Species";
            this.colintSpeciesID.ColumnEdit = this.repositoryItemGridLookUpEditSpecies;
            this.colintSpeciesID.FieldName = "intSpeciesID";
            this.colintSpeciesID.Name = "colintSpeciesID";
            this.colintSpeciesID.Visible = true;
            this.colintSpeciesID.VisibleIndex = 14;
            this.colintSpeciesID.Width = 87;
            // 
            // repositoryItemGridLookUpEditSpecies
            // 
            this.repositoryItemGridLookUpEditSpecies.AutoHeight = false;
            this.repositoryItemGridLookUpEditSpecies.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditSpecies.DataSource = this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource;
            this.repositoryItemGridLookUpEditSpecies.DisplayMember = "SpeciesName";
            this.repositoryItemGridLookUpEditSpecies.Name = "repositoryItemGridLookUpEditSpecies";
            this.repositoryItemGridLookUpEditSpecies.NullText = "";
            this.repositoryItemGridLookUpEditSpecies.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditSpecies.ValueMember = "SpeciesID";
            this.repositoryItemGridLookUpEditSpecies.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.repositoryItemGridLookUpEditSpecies_CustomDisplayText);
            // 
            // sp00136importfromGBMlocalityspecieslistwithblankBindingSource
            // 
            this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource.DataMember = "sp00136_import_from_GBM_locality_species_list_with_blank";
            this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colScientificName,
            this.colSpeciesID,
            this.colSpeciesName,
            this.colSpeciesOrder,
            this.colSpeciesType});
            this.repositoryItemGridLookUpEdit1View.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 517, 208, 191);
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition8.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition8.Appearance.Options.UseForeColor = true;
            styleFormatCondition8.ApplyToRow = true;
            styleFormatCondition8.Column = this.colSpeciesID;
            styleFormatCondition8.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition8.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition8});
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsBehavior.Editable = false;
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpeciesOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpeciesName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colScientificName
            // 
            this.colScientificName.FieldName = "ScientificName";
            this.colScientificName.Name = "colScientificName";
            this.colScientificName.Visible = true;
            this.colScientificName.VisibleIndex = 1;
            this.colScientificName.Width = 183;
            // 
            // colSpeciesOrder
            // 
            this.colSpeciesOrder.FieldName = "SpeciesOrder";
            this.colSpeciesOrder.Name = "colSpeciesOrder";
            this.colSpeciesOrder.Width = 101;
            // 
            // colSpeciesType
            // 
            this.colSpeciesType.FieldName = "SpeciesType";
            this.colSpeciesType.Name = "colSpeciesType";
            this.colSpeciesType.Visible = true;
            this.colSpeciesType.VisibleIndex = 2;
            this.colSpeciesType.Width = 85;
            // 
            // colintAccess
            // 
            this.colintAccess.Caption = "Access";
            this.colintAccess.FieldName = "intAccess";
            this.colintAccess.Name = "colintAccess";
            this.colintAccess.OptionsColumn.AllowFocus = false;
            this.colintAccess.OptionsColumn.ReadOnly = true;
            this.colintAccess.Visible = true;
            this.colintAccess.VisibleIndex = 20;
            this.colintAccess.Width = 70;
            // 
            // colintVisibility
            // 
            this.colintVisibility.Caption = "Visibility";
            this.colintVisibility.FieldName = "intVisibility";
            this.colintVisibility.Name = "colintVisibility";
            this.colintVisibility.OptionsColumn.AllowFocus = false;
            this.colintVisibility.OptionsColumn.ReadOnly = true;
            this.colintVisibility.Visible = true;
            this.colintVisibility.VisibleIndex = 21;
            this.colintVisibility.Width = 74;
            // 
            // colstrContext
            // 
            this.colstrContext.Caption = "Context";
            this.colstrContext.FieldName = "strContext";
            this.colstrContext.Name = "colstrContext";
            this.colstrContext.OptionsColumn.AllowFocus = false;
            this.colstrContext.OptionsColumn.ReadOnly = true;
            this.colstrContext.Visible = true;
            this.colstrContext.VisibleIndex = 22;
            this.colstrContext.Width = 77;
            // 
            // colstrManagement
            // 
            this.colstrManagement.Caption = "Management";
            this.colstrManagement.FieldName = "strManagement";
            this.colstrManagement.Name = "colstrManagement";
            this.colstrManagement.OptionsColumn.AllowFocus = false;
            this.colstrManagement.OptionsColumn.ReadOnly = true;
            this.colstrManagement.Visible = true;
            this.colstrManagement.VisibleIndex = 23;
            this.colstrManagement.Width = 100;
            // 
            // colintLegalStatus
            // 
            this.colintLegalStatus.Caption = "Legal Status";
            this.colintLegalStatus.FieldName = "intLegalStatus";
            this.colintLegalStatus.Name = "colintLegalStatus";
            this.colintLegalStatus.OptionsColumn.AllowFocus = false;
            this.colintLegalStatus.OptionsColumn.ReadOnly = true;
            this.colintLegalStatus.Visible = true;
            this.colintLegalStatus.VisibleIndex = 24;
            this.colintLegalStatus.Width = 96;
            // 
            // coldtLastInspectionDate
            // 
            this.coldtLastInspectionDate.Caption = "Last Inspection";
            this.coldtLastInspectionDate.FieldName = "dtLastInspectionDate";
            this.coldtLastInspectionDate.Name = "coldtLastInspectionDate";
            this.coldtLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.coldtLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.coldtLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.coldtLastInspectionDate.Visible = true;
            this.coldtLastInspectionDate.VisibleIndex = 25;
            this.coldtLastInspectionDate.Width = 134;
            // 
            // colintInspectionCycle
            // 
            this.colintInspectionCycle.Caption = "Cycle";
            this.colintInspectionCycle.FieldName = "intInspectionCycle";
            this.colintInspectionCycle.Name = "colintInspectionCycle";
            this.colintInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colintInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colintInspectionCycle.Visible = true;
            this.colintInspectionCycle.VisibleIndex = 26;
            this.colintInspectionCycle.Width = 116;
            // 
            // colstrInspectionUnit
            // 
            this.colstrInspectionUnit.Caption = "Cycle Unit";
            this.colstrInspectionUnit.FieldName = "strInspectionUnit";
            this.colstrInspectionUnit.Name = "colstrInspectionUnit";
            this.colstrInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colstrInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colstrInspectionUnit.Visible = true;
            this.colstrInspectionUnit.VisibleIndex = 27;
            this.colstrInspectionUnit.Width = 110;
            // 
            // coldtNextInspectionDate
            // 
            this.coldtNextInspectionDate.Caption = "Next Inspection";
            this.coldtNextInspectionDate.FieldName = "dtNextInspectionDate";
            this.coldtNextInspectionDate.Name = "coldtNextInspectionDate";
            this.coldtNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.coldtNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.coldtNextInspectionDate.Visible = true;
            this.coldtNextInspectionDate.VisibleIndex = 28;
            this.coldtNextInspectionDate.Width = 137;
            // 
            // colintGroundType
            // 
            this.colintGroundType.Caption = "Ground Type";
            this.colintGroundType.FieldName = "intGroundType";
            this.colintGroundType.Name = "colintGroundType";
            this.colintGroundType.OptionsColumn.AllowFocus = false;
            this.colintGroundType.OptionsColumn.ReadOnly = true;
            this.colintGroundType.Visible = true;
            this.colintGroundType.VisibleIndex = 29;
            this.colintGroundType.Width = 99;
            // 
            // coldecHeight
            // 
            this.coldecHeight.Caption = "Height";
            this.coldecHeight.FieldName = "decHeight";
            this.coldecHeight.Name = "coldecHeight";
            this.coldecHeight.OptionsColumn.AllowFocus = false;
            this.coldecHeight.OptionsColumn.ReadOnly = true;
            this.coldecHeight.Visible = true;
            this.coldecHeight.VisibleIndex = 30;
            this.coldecHeight.Width = 73;
            // 
            // colintHeightRange
            // 
            this.colintHeightRange.Caption = "Height Band";
            this.colintHeightRange.FieldName = "intHeightRange";
            this.colintHeightRange.Name = "colintHeightRange";
            this.colintHeightRange.OptionsColumn.AllowFocus = false;
            this.colintHeightRange.OptionsColumn.ReadOnly = true;
            this.colintHeightRange.Visible = true;
            this.colintHeightRange.VisibleIndex = 31;
            this.colintHeightRange.Width = 102;
            // 
            // colintDBH
            // 
            this.colintDBH.Caption = "DBH";
            this.colintDBH.FieldName = "intDBH";
            this.colintDBH.Name = "colintDBH";
            this.colintDBH.OptionsColumn.AllowFocus = false;
            this.colintDBH.OptionsColumn.ReadOnly = true;
            this.colintDBH.Visible = true;
            this.colintDBH.VisibleIndex = 32;
            this.colintDBH.Width = 57;
            // 
            // colintDBHRange
            // 
            this.colintDBHRange.Caption = "DBH Band";
            this.colintDBHRange.FieldName = "intDBHRange";
            this.colintDBHRange.Name = "colintDBHRange";
            this.colintDBHRange.OptionsColumn.AllowFocus = false;
            this.colintDBHRange.OptionsColumn.ReadOnly = true;
            this.colintDBHRange.Visible = true;
            this.colintDBHRange.VisibleIndex = 33;
            this.colintDBHRange.Width = 91;
            // 
            // colintProtectionType
            // 
            this.colintProtectionType.Caption = "Protection Type";
            this.colintProtectionType.FieldName = "intProtectionType";
            this.colintProtectionType.Name = "colintProtectionType";
            this.colintProtectionType.OptionsColumn.AllowFocus = false;
            this.colintProtectionType.OptionsColumn.ReadOnly = true;
            this.colintProtectionType.Visible = true;
            this.colintProtectionType.VisibleIndex = 34;
            this.colintProtectionType.Width = 113;
            // 
            // colintCrownDiameter
            // 
            this.colintCrownDiameter.Caption = "Crown Diameter";
            this.colintCrownDiameter.FieldName = "intCrownDiameter";
            this.colintCrownDiameter.Name = "colintCrownDiameter";
            this.colintCrownDiameter.OptionsColumn.AllowFocus = false;
            this.colintCrownDiameter.OptionsColumn.ReadOnly = true;
            this.colintCrownDiameter.Visible = true;
            this.colintCrownDiameter.VisibleIndex = 35;
            this.colintCrownDiameter.Width = 114;
            // 
            // colintCrownDepth
            // 
            this.colintCrownDepth.Caption = "Crown Depth";
            this.colintCrownDepth.FieldName = "intCrownDepth";
            this.colintCrownDepth.Name = "colintCrownDepth";
            this.colintCrownDepth.OptionsColumn.AllowFocus = false;
            this.colintCrownDepth.OptionsColumn.ReadOnly = true;
            this.colintCrownDepth.Visible = true;
            this.colintCrownDepth.VisibleIndex = 36;
            this.colintCrownDepth.Width = 100;
            // 
            // coldtPlantDate
            // 
            this.coldtPlantDate.Caption = "Plant Date";
            this.coldtPlantDate.FieldName = "dtPlantDate";
            this.coldtPlantDate.Name = "coldtPlantDate";
            this.coldtPlantDate.OptionsColumn.AllowFocus = false;
            this.coldtPlantDate.OptionsColumn.ReadOnly = true;
            this.coldtPlantDate.Visible = true;
            this.coldtPlantDate.VisibleIndex = 37;
            this.coldtPlantDate.Width = 85;
            // 
            // colintGroupNo
            // 
            this.colintGroupNo.Caption = "Group No";
            this.colintGroupNo.FieldName = "intGroupNo";
            this.colintGroupNo.Name = "colintGroupNo";
            this.colintGroupNo.OptionsColumn.AllowFocus = false;
            this.colintGroupNo.OptionsColumn.ReadOnly = true;
            this.colintGroupNo.Visible = true;
            this.colintGroupNo.VisibleIndex = 38;
            this.colintGroupNo.Width = 82;
            // 
            // colintSafetyPriority
            // 
            this.colintSafetyPriority.Caption = "Safety Priority";
            this.colintSafetyPriority.FieldName = "intSafetyPriority";
            this.colintSafetyPriority.Name = "colintSafetyPriority";
            this.colintSafetyPriority.OptionsColumn.AllowFocus = false;
            this.colintSafetyPriority.OptionsColumn.ReadOnly = true;
            this.colintSafetyPriority.Visible = true;
            this.colintSafetyPriority.VisibleIndex = 39;
            this.colintSafetyPriority.Width = 106;
            // 
            // colintSiteHazardClass
            // 
            this.colintSiteHazardClass.Caption = "Site Hazard Class";
            this.colintSiteHazardClass.FieldName = "intSiteHazardClass";
            this.colintSiteHazardClass.Name = "colintSiteHazardClass";
            this.colintSiteHazardClass.OptionsColumn.AllowFocus = false;
            this.colintSiteHazardClass.OptionsColumn.ReadOnly = true;
            this.colintSiteHazardClass.Visible = true;
            this.colintSiteHazardClass.VisibleIndex = 40;
            this.colintSiteHazardClass.Width = 120;
            // 
            // colintPlantSize
            // 
            this.colintPlantSize.Caption = "Plant Size";
            this.colintPlantSize.FieldName = "intPlantSize";
            this.colintPlantSize.Name = "colintPlantSize";
            this.colintPlantSize.OptionsColumn.AllowFocus = false;
            this.colintPlantSize.OptionsColumn.ReadOnly = true;
            this.colintPlantSize.Visible = true;
            this.colintPlantSize.VisibleIndex = 41;
            this.colintPlantSize.Width = 83;
            // 
            // colintPlantSource
            // 
            this.colintPlantSource.Caption = "Plant Source";
            this.colintPlantSource.FieldName = "intPlantSource";
            this.colintPlantSource.Name = "colintPlantSource";
            this.colintPlantSource.OptionsColumn.AllowFocus = false;
            this.colintPlantSource.OptionsColumn.ReadOnly = true;
            this.colintPlantSource.Visible = true;
            this.colintPlantSource.VisibleIndex = 42;
            this.colintPlantSource.Width = 97;
            // 
            // colintPlantMethod
            // 
            this.colintPlantMethod.Caption = "Plant Method";
            this.colintPlantMethod.FieldName = "intPlantMethod";
            this.colintPlantMethod.Name = "colintPlantMethod";
            this.colintPlantMethod.OptionsColumn.AllowFocus = false;
            this.colintPlantMethod.OptionsColumn.ReadOnly = true;
            this.colintPlantMethod.Visible = true;
            this.colintPlantMethod.VisibleIndex = 43;
            this.colintPlantMethod.Width = 100;
            // 
            // colstrPostcode
            // 
            this.colstrPostcode.Caption = "Postcode";
            this.colstrPostcode.FieldName = "strPostcode";
            this.colstrPostcode.Name = "colstrPostcode";
            this.colstrPostcode.OptionsColumn.AllowFocus = false;
            this.colstrPostcode.OptionsColumn.ReadOnly = true;
            this.colstrPostcode.Visible = true;
            this.colstrPostcode.VisibleIndex = 44;
            this.colstrPostcode.Width = 82;
            // 
            // colstrMapLabel
            // 
            this.colstrMapLabel.Caption = "Map Label";
            this.colstrMapLabel.FieldName = "strMapLabel";
            this.colstrMapLabel.Name = "colstrMapLabel";
            this.colstrMapLabel.OptionsColumn.AllowFocus = false;
            this.colstrMapLabel.OptionsColumn.ReadOnly = true;
            this.colstrMapLabel.Visible = true;
            this.colstrMapLabel.VisibleIndex = 45;
            this.colstrMapLabel.Width = 86;
            // 
            // colintSize
            // 
            this.colintSize.Caption = "Size";
            this.colintSize.FieldName = "intSize";
            this.colintSize.Name = "colintSize";
            this.colintSize.OptionsColumn.AllowFocus = false;
            this.colintSize.OptionsColumn.ReadOnly = true;
            this.colintSize.Visible = true;
            this.colintSize.VisibleIndex = 46;
            this.colintSize.Width = 56;
            // 
            // colintNearbyObject
            // 
            this.colintNearbyObject.Caption = "Nearby Object 1";
            this.colintNearbyObject.FieldName = "intNearbyObject";
            this.colintNearbyObject.Name = "colintNearbyObject";
            this.colintNearbyObject.OptionsColumn.AllowFocus = false;
            this.colintNearbyObject.OptionsColumn.ReadOnly = true;
            this.colintNearbyObject.Visible = true;
            this.colintNearbyObject.VisibleIndex = 47;
            this.colintNearbyObject.Width = 107;
            // 
            // colintNearbyObject2
            // 
            this.colintNearbyObject2.Caption = "Nearby Object 2";
            this.colintNearbyObject2.FieldName = "intNearbyObject2";
            this.colintNearbyObject2.Name = "colintNearbyObject2";
            this.colintNearbyObject2.OptionsColumn.AllowFocus = false;
            this.colintNearbyObject2.OptionsColumn.ReadOnly = true;
            this.colintNearbyObject2.Visible = true;
            this.colintNearbyObject2.VisibleIndex = 48;
            this.colintNearbyObject2.Width = 113;
            // 
            // colintNearbyObject3
            // 
            this.colintNearbyObject3.Caption = "Nearby Object 3";
            this.colintNearbyObject3.FieldName = "intNearbyObject3";
            this.colintNearbyObject3.Name = "colintNearbyObject3";
            this.colintNearbyObject3.OptionsColumn.AllowFocus = false;
            this.colintNearbyObject3.OptionsColumn.ReadOnly = true;
            this.colintNearbyObject3.Visible = true;
            this.colintNearbyObject3.VisibleIndex = 49;
            this.colintNearbyObject3.Width = 113;
            // 
            // colstrRemarks_Tree
            // 
            this.colstrRemarks_Tree.Caption = "Tree Remarks";
            this.colstrRemarks_Tree.FieldName = "strRemarks_Tree";
            this.colstrRemarks_Tree.Name = "colstrRemarks_Tree";
            this.colstrRemarks_Tree.OptionsColumn.AllowFocus = false;
            this.colstrRemarks_Tree.OptionsColumn.ReadOnly = true;
            this.colstrRemarks_Tree.Visible = true;
            this.colstrRemarks_Tree.VisibleIndex = 50;
            this.colstrRemarks_Tree.Width = 107;
            // 
            // colstrUser1_Tree
            // 
            this.colstrUser1_Tree.Caption = "Tree User 1";
            this.colstrUser1_Tree.FieldName = "strUser1_Tree";
            this.colstrUser1_Tree.Name = "colstrUser1_Tree";
            this.colstrUser1_Tree.OptionsColumn.AllowFocus = false;
            this.colstrUser1_Tree.OptionsColumn.ReadOnly = true;
            this.colstrUser1_Tree.Visible = true;
            this.colstrUser1_Tree.VisibleIndex = 51;
            this.colstrUser1_Tree.Width = 94;
            // 
            // colstrUser2_Tree
            // 
            this.colstrUser2_Tree.Caption = "Tree User 2";
            this.colstrUser2_Tree.FieldName = "strUser2_Tree";
            this.colstrUser2_Tree.Name = "colstrUser2_Tree";
            this.colstrUser2_Tree.OptionsColumn.AllowFocus = false;
            this.colstrUser2_Tree.OptionsColumn.ReadOnly = true;
            this.colstrUser2_Tree.Visible = true;
            this.colstrUser2_Tree.VisibleIndex = 52;
            this.colstrUser2_Tree.Width = 94;
            // 
            // colstrUser3_Tree
            // 
            this.colstrUser3_Tree.Caption = "Tree User 3";
            this.colstrUser3_Tree.FieldName = "strUser3_Tree";
            this.colstrUser3_Tree.Name = "colstrUser3_Tree";
            this.colstrUser3_Tree.OptionsColumn.AllowFocus = false;
            this.colstrUser3_Tree.OptionsColumn.ReadOnly = true;
            this.colstrUser3_Tree.Visible = true;
            this.colstrUser3_Tree.VisibleIndex = 53;
            this.colstrUser3_Tree.Width = 94;
            // 
            // coldecAreaHa
            // 
            this.coldecAreaHa.Caption = "Area (M�)";
            this.coldecAreaHa.FieldName = "decAreaHa";
            this.coldecAreaHa.Name = "coldecAreaHa";
            this.coldecAreaHa.OptionsColumn.AllowFocus = false;
            this.coldecAreaHa.OptionsColumn.ReadOnly = true;
            this.coldecAreaHa.Visible = true;
            this.coldecAreaHa.VisibleIndex = 54;
            this.coldecAreaHa.Width = 81;
            // 
            // colintType
            // 
            this.colintType.Caption = "Type";
            this.colintType.FieldName = "intType";
            this.colintType.Name = "colintType";
            this.colintType.OptionsColumn.AllowFocus = false;
            this.colintType.OptionsColumn.ReadOnly = true;
            this.colintType.Visible = true;
            this.colintType.VisibleIndex = 55;
            this.colintType.Width = 61;
            // 
            // colstrHedgeOwner
            // 
            this.colstrHedgeOwner.Caption = "Hedge Owner";
            this.colstrHedgeOwner.FieldName = "strHedgeOwner";
            this.colstrHedgeOwner.Name = "colstrHedgeOwner";
            this.colstrHedgeOwner.OptionsColumn.AllowFocus = false;
            this.colstrHedgeOwner.OptionsColumn.ReadOnly = true;
            this.colstrHedgeOwner.Width = 104;
            // 
            // colintLength
            // 
            this.colintLength.Caption = "Length";
            this.colintLength.FieldName = "intLength";
            this.colintLength.Name = "colintLength";
            this.colintLength.OptionsColumn.AllowFocus = false;
            this.colintLength.OptionsColumn.ReadOnly = true;
            this.colintLength.Width = 70;
            // 
            // coldecGardenSize
            // 
            this.coldecGardenSize.Caption = "Garden Size";
            this.coldecGardenSize.FieldName = "decGardenSize";
            this.coldecGardenSize.Name = "coldecGardenSize";
            this.coldecGardenSize.OptionsColumn.AllowFocus = false;
            this.coldecGardenSize.OptionsColumn.ReadOnly = true;
            this.coldecGardenSize.Width = 99;
            // 
            // coldecPropertyProximity
            // 
            this.coldecPropertyProximity.Caption = "Property Proximity";
            this.coldecPropertyProximity.FieldName = "decPropertyProximity";
            this.coldecPropertyProximity.Name = "coldecPropertyProximity";
            this.coldecPropertyProximity.OptionsColumn.AllowFocus = false;
            this.coldecPropertyProximity.OptionsColumn.ReadOnly = true;
            this.coldecPropertyProximity.Width = 131;
            // 
            // coldecSiteLevel
            // 
            this.coldecSiteLevel.Caption = "Site Level";
            this.coldecSiteLevel.FieldName = "decSiteLevel";
            this.coldecSiteLevel.Name = "coldecSiteLevel";
            this.coldecSiteLevel.OptionsColumn.AllowFocus = false;
            this.coldecSiteLevel.OptionsColumn.ReadOnly = true;
            this.coldecSiteLevel.Width = 88;
            // 
            // colintReplantCount
            // 
            this.colintReplantCount.Caption = "Replant Count";
            this.colintReplantCount.FieldName = "intReplantCount";
            this.colintReplantCount.Name = "colintReplantCount";
            this.colintReplantCount.OptionsColumn.AllowFocus = false;
            this.colintReplantCount.OptionsColumn.ReadOnly = true;
            this.colintReplantCount.Visible = true;
            this.colintReplantCount.VisibleIndex = 56;
            this.colintReplantCount.Width = 106;
            // 
            // coldtSurveyDate
            // 
            this.coldtSurveyDate.Caption = "Survey Date";
            this.coldtSurveyDate.FieldName = "dtSurveyDate";
            this.coldtSurveyDate.Name = "coldtSurveyDate";
            this.coldtSurveyDate.OptionsColumn.AllowFocus = false;
            this.coldtSurveyDate.OptionsColumn.ReadOnly = true;
            this.coldtSurveyDate.Visible = true;
            this.coldtSurveyDate.VisibleIndex = 57;
            this.coldtSurveyDate.Width = 95;
            // 
            // colintTreeOwnership
            // 
            this.colintTreeOwnership.Caption = "Tree Ownership";
            this.colintTreeOwnership.FieldName = "intTreeOwnership";
            this.colintTreeOwnership.Name = "colintTreeOwnership";
            this.colintTreeOwnership.OptionsColumn.AllowFocus = false;
            this.colintTreeOwnership.OptionsColumn.ReadOnly = true;
            this.colintTreeOwnership.Visible = true;
            this.colintTreeOwnership.VisibleIndex = 58;
            this.colintTreeOwnership.Width = 113;
            // 
            // colstrInspectRef
            // 
            this.colstrInspectRef.Caption = "Inspection Reference";
            this.colstrInspectRef.ColumnEdit = this.repositoryItemButtonEditSequenceField;
            this.colstrInspectRef.FieldName = "strInspectRef";
            this.colstrInspectRef.Name = "colstrInspectRef";
            this.colstrInspectRef.Visible = true;
            this.colstrInspectRef.VisibleIndex = 59;
            this.colstrInspectRef.Width = 125;
            // 
            // colintInspector
            // 
            this.colintInspector.Caption = "Inspector";
            this.colintInspector.FieldName = "intInspector";
            this.colintInspector.Name = "colintInspector";
            this.colintInspector.OptionsColumn.AllowFocus = false;
            this.colintInspector.OptionsColumn.ReadOnly = true;
            this.colintInspector.Visible = true;
            this.colintInspector.VisibleIndex = 60;
            this.colintInspector.Width = 83;
            // 
            // coldtInspectDate
            // 
            this.coldtInspectDate.Caption = "Inspection Date";
            this.coldtInspectDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.coldtInspectDate.FieldName = "dtInspectDate";
            this.coldtInspectDate.Name = "coldtInspectDate";
            this.coldtInspectDate.Visible = true;
            this.coldtInspectDate.VisibleIndex = 61;
            this.coldtInspectDate.Width = 97;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colintIncidentID
            // 
            this.colintIncidentID.Caption = "Incident ID";
            this.colintIncidentID.FieldName = "intIncidentID";
            this.colintIncidentID.Name = "colintIncidentID";
            this.colintIncidentID.OptionsColumn.AllowFocus = false;
            this.colintIncidentID.OptionsColumn.ReadOnly = true;
            this.colintIncidentID.Visible = true;
            this.colintIncidentID.VisibleIndex = 62;
            this.colintIncidentID.Width = 90;
            // 
            // colintStemPhysical
            // 
            this.colintStemPhysical.Caption = "Stem Physical 1";
            this.colintStemPhysical.FieldName = "intStemPhysical";
            this.colintStemPhysical.Name = "colintStemPhysical";
            this.colintStemPhysical.OptionsColumn.AllowFocus = false;
            this.colintStemPhysical.OptionsColumn.ReadOnly = true;
            this.colintStemPhysical.Visible = true;
            this.colintStemPhysical.VisibleIndex = 63;
            this.colintStemPhysical.Width = 102;
            // 
            // colintStemPhysical2
            // 
            this.colintStemPhysical2.Caption = "Stem Physical 2";
            this.colintStemPhysical2.FieldName = "intStemPhysical2";
            this.colintStemPhysical2.Name = "colintStemPhysical2";
            this.colintStemPhysical2.OptionsColumn.AllowFocus = false;
            this.colintStemPhysical2.OptionsColumn.ReadOnly = true;
            this.colintStemPhysical2.Visible = true;
            this.colintStemPhysical2.VisibleIndex = 64;
            this.colintStemPhysical2.Width = 108;
            // 
            // colintStemPhysical3
            // 
            this.colintStemPhysical3.Caption = "Stem Physical 3";
            this.colintStemPhysical3.FieldName = "intStemPhysical3";
            this.colintStemPhysical3.Name = "colintStemPhysical3";
            this.colintStemPhysical3.OptionsColumn.AllowFocus = false;
            this.colintStemPhysical3.OptionsColumn.ReadOnly = true;
            this.colintStemPhysical3.Visible = true;
            this.colintStemPhysical3.VisibleIndex = 65;
            this.colintStemPhysical3.Width = 108;
            // 
            // colintStemDisease
            // 
            this.colintStemDisease.Caption = "Stem Disease 1";
            this.colintStemDisease.FieldName = "intStemDisease";
            this.colintStemDisease.Name = "colintStemDisease";
            this.colintStemDisease.OptionsColumn.AllowFocus = false;
            this.colintStemDisease.OptionsColumn.ReadOnly = true;
            this.colintStemDisease.Visible = true;
            this.colintStemDisease.VisibleIndex = 66;
            this.colintStemDisease.Width = 101;
            // 
            // colintStemDisease2
            // 
            this.colintStemDisease2.Caption = "Stem Disease 2";
            this.colintStemDisease2.FieldName = "intStemDisease2";
            this.colintStemDisease2.Name = "colintStemDisease2";
            this.colintStemDisease2.OptionsColumn.AllowFocus = false;
            this.colintStemDisease2.OptionsColumn.ReadOnly = true;
            this.colintStemDisease2.Visible = true;
            this.colintStemDisease2.VisibleIndex = 67;
            this.colintStemDisease2.Width = 107;
            // 
            // colintStemDisease3
            // 
            this.colintStemDisease3.Caption = "Stem Disease 3";
            this.colintStemDisease3.FieldName = "intStemDisease3";
            this.colintStemDisease3.Name = "colintStemDisease3";
            this.colintStemDisease3.OptionsColumn.AllowFocus = false;
            this.colintStemDisease3.OptionsColumn.ReadOnly = true;
            this.colintStemDisease3.Visible = true;
            this.colintStemDisease3.VisibleIndex = 68;
            this.colintStemDisease3.Width = 107;
            // 
            // colintAngleToVertical
            // 
            this.colintAngleToVertical.Caption = "Angle To Vertical";
            this.colintAngleToVertical.FieldName = "intAngleToVertical";
            this.colintAngleToVertical.Name = "colintAngleToVertical";
            this.colintAngleToVertical.OptionsColumn.AllowFocus = false;
            this.colintAngleToVertical.OptionsColumn.ReadOnly = true;
            this.colintAngleToVertical.Visible = true;
            this.colintAngleToVertical.VisibleIndex = 69;
            this.colintAngleToVertical.Width = 117;
            // 
            // colintCrownPhysical
            // 
            this.colintCrownPhysical.Caption = "Crown Physical 1";
            this.colintCrownPhysical.FieldName = "intCrownPhysical";
            this.colintCrownPhysical.Name = "colintCrownPhysical";
            this.colintCrownPhysical.OptionsColumn.AllowFocus = false;
            this.colintCrownPhysical.OptionsColumn.ReadOnly = true;
            this.colintCrownPhysical.Visible = true;
            this.colintCrownPhysical.VisibleIndex = 70;
            this.colintCrownPhysical.Width = 109;
            // 
            // colintCrownPhysical2
            // 
            this.colintCrownPhysical2.Caption = "Crown Physical 2";
            this.colintCrownPhysical2.FieldName = "intCrownPhysical2";
            this.colintCrownPhysical2.Name = "colintCrownPhysical2";
            this.colintCrownPhysical2.OptionsColumn.AllowFocus = false;
            this.colintCrownPhysical2.OptionsColumn.ReadOnly = true;
            this.colintCrownPhysical2.Visible = true;
            this.colintCrownPhysical2.VisibleIndex = 71;
            this.colintCrownPhysical2.Width = 115;
            // 
            // colintCrownPhysical3
            // 
            this.colintCrownPhysical3.Caption = "Crown Physical 3";
            this.colintCrownPhysical3.FieldName = "intCrownPhysical3";
            this.colintCrownPhysical3.Name = "colintCrownPhysical3";
            this.colintCrownPhysical3.OptionsColumn.AllowFocus = false;
            this.colintCrownPhysical3.OptionsColumn.ReadOnly = true;
            this.colintCrownPhysical3.Visible = true;
            this.colintCrownPhysical3.VisibleIndex = 72;
            this.colintCrownPhysical3.Width = 115;
            // 
            // colintCrownDisease
            // 
            this.colintCrownDisease.Caption = "Crown Disease 1";
            this.colintCrownDisease.FieldName = "intCrownDisease";
            this.colintCrownDisease.Name = "colintCrownDisease";
            this.colintCrownDisease.OptionsColumn.AllowFocus = false;
            this.colintCrownDisease.OptionsColumn.ReadOnly = true;
            this.colintCrownDisease.Visible = true;
            this.colintCrownDisease.VisibleIndex = 73;
            this.colintCrownDisease.Width = 108;
            // 
            // colintCrownDisease2
            // 
            this.colintCrownDisease2.Caption = "Crown Disease 2";
            this.colintCrownDisease2.FieldName = "intCrownDisease2";
            this.colintCrownDisease2.Name = "colintCrownDisease2";
            this.colintCrownDisease2.OptionsColumn.AllowFocus = false;
            this.colintCrownDisease2.OptionsColumn.ReadOnly = true;
            this.colintCrownDisease2.Visible = true;
            this.colintCrownDisease2.VisibleIndex = 74;
            this.colintCrownDisease2.Width = 114;
            // 
            // colintCrownDisease3
            // 
            this.colintCrownDisease3.Caption = "Crown Disease 3";
            this.colintCrownDisease3.FieldName = "intCrownDisease3";
            this.colintCrownDisease3.Name = "colintCrownDisease3";
            this.colintCrownDisease3.OptionsColumn.AllowFocus = false;
            this.colintCrownDisease3.OptionsColumn.ReadOnly = true;
            this.colintCrownDisease3.Visible = true;
            this.colintCrownDisease3.VisibleIndex = 75;
            this.colintCrownDisease3.Width = 114;
            // 
            // colintCrownFoliation
            // 
            this.colintCrownFoliation.Caption = "Crown Foliation 1";
            this.colintCrownFoliation.FieldName = "intCrownFoliation";
            this.colintCrownFoliation.Name = "colintCrownFoliation";
            this.colintCrownFoliation.OptionsColumn.AllowFocus = false;
            this.colintCrownFoliation.OptionsColumn.ReadOnly = true;
            this.colintCrownFoliation.Visible = true;
            this.colintCrownFoliation.VisibleIndex = 76;
            this.colintCrownFoliation.Width = 111;
            // 
            // colintCrownFoliation2
            // 
            this.colintCrownFoliation2.Caption = "Crown Foliation 2";
            this.colintCrownFoliation2.FieldName = "intCrownFoliation2";
            this.colintCrownFoliation2.Name = "colintCrownFoliation2";
            this.colintCrownFoliation2.OptionsColumn.AllowFocus = false;
            this.colintCrownFoliation2.OptionsColumn.ReadOnly = true;
            this.colintCrownFoliation2.Visible = true;
            this.colintCrownFoliation2.VisibleIndex = 77;
            this.colintCrownFoliation2.Width = 117;
            // 
            // colintCrownFoliation3
            // 
            this.colintCrownFoliation3.Caption = "Crown Foliation 3";
            this.colintCrownFoliation3.FieldName = "intCrownFoliation3";
            this.colintCrownFoliation3.Name = "colintCrownFoliation3";
            this.colintCrownFoliation3.OptionsColumn.AllowFocus = false;
            this.colintCrownFoliation3.OptionsColumn.ReadOnly = true;
            this.colintCrownFoliation3.Visible = true;
            this.colintCrownFoliation3.VisibleIndex = 78;
            this.colintCrownFoliation3.Width = 117;
            // 
            // colintSafety
            // 
            this.colintSafety.Caption = "Safety";
            this.colintSafety.FieldName = "intSafety";
            this.colintSafety.Name = "colintSafety";
            this.colintSafety.OptionsColumn.AllowFocus = false;
            this.colintSafety.OptionsColumn.ReadOnly = true;
            this.colintSafety.Visible = true;
            this.colintSafety.VisibleIndex = 79;
            this.colintSafety.Width = 69;
            // 
            // colintGeneralCondition
            // 
            this.colintGeneralCondition.Caption = "General Condition";
            this.colintGeneralCondition.FieldName = "intGeneralCondition";
            this.colintGeneralCondition.Name = "colintGeneralCondition";
            this.colintGeneralCondition.OptionsColumn.AllowFocus = false;
            this.colintGeneralCondition.OptionsColumn.ReadOnly = true;
            this.colintGeneralCondition.Visible = true;
            this.colintGeneralCondition.VisibleIndex = 80;
            this.colintGeneralCondition.Width = 122;
            // 
            // colintRootHeave
            // 
            this.colintRootHeave.Caption = "Root Heave 1";
            this.colintRootHeave.FieldName = "intRootHeave";
            this.colintRootHeave.Name = "colintRootHeave";
            this.colintRootHeave.OptionsColumn.AllowFocus = false;
            this.colintRootHeave.OptionsColumn.ReadOnly = true;
            this.colintRootHeave.Visible = true;
            this.colintRootHeave.VisibleIndex = 81;
            this.colintRootHeave.Width = 94;
            // 
            // colintRootHeave2
            // 
            this.colintRootHeave2.Caption = "Root Heave 2";
            this.colintRootHeave2.FieldName = "intRootHeave2";
            this.colintRootHeave2.Name = "colintRootHeave2";
            this.colintRootHeave2.OptionsColumn.AllowFocus = false;
            this.colintRootHeave2.OptionsColumn.ReadOnly = true;
            this.colintRootHeave2.Visible = true;
            this.colintRootHeave2.VisibleIndex = 82;
            this.colintRootHeave2.Width = 100;
            // 
            // colintRootHeave3
            // 
            this.colintRootHeave3.Caption = "Root Heave 3";
            this.colintRootHeave3.FieldName = "intRootHeave3";
            this.colintRootHeave3.Name = "colintRootHeave3";
            this.colintRootHeave3.OptionsColumn.AllowFocus = false;
            this.colintRootHeave3.OptionsColumn.ReadOnly = true;
            this.colintRootHeave3.Visible = true;
            this.colintRootHeave3.VisibleIndex = 83;
            this.colintRootHeave3.Width = 100;
            // 
            // colstrUser1_Inspection
            // 
            this.colstrUser1_Inspection.Caption = "Inspection User 1";
            this.colstrUser1_Inspection.FieldName = "strUser1_Inspection";
            this.colstrUser1_Inspection.Name = "colstrUser1_Inspection";
            this.colstrUser1_Inspection.OptionsColumn.AllowFocus = false;
            this.colstrUser1_Inspection.OptionsColumn.ReadOnly = true;
            this.colstrUser1_Inspection.Visible = true;
            this.colstrUser1_Inspection.VisibleIndex = 85;
            this.colstrUser1_Inspection.Width = 122;
            // 
            // colstrUser2_Inspection
            // 
            this.colstrUser2_Inspection.Caption = "Inspection User 2";
            this.colstrUser2_Inspection.FieldName = "strUser2_Inspection";
            this.colstrUser2_Inspection.Name = "colstrUser2_Inspection";
            this.colstrUser2_Inspection.OptionsColumn.AllowFocus = false;
            this.colstrUser2_Inspection.OptionsColumn.ReadOnly = true;
            this.colstrUser2_Inspection.Visible = true;
            this.colstrUser2_Inspection.VisibleIndex = 86;
            this.colstrUser2_Inspection.Width = 122;
            // 
            // colstrUser3_Inspection
            // 
            this.colstrUser3_Inspection.Caption = "Inspection User 3";
            this.colstrUser3_Inspection.FieldName = "strUser3_Inspection";
            this.colstrUser3_Inspection.Name = "colstrUser3_Inspection";
            this.colstrUser3_Inspection.OptionsColumn.AllowFocus = false;
            this.colstrUser3_Inspection.OptionsColumn.ReadOnly = true;
            this.colstrUser3_Inspection.Visible = true;
            this.colstrUser3_Inspection.VisibleIndex = 87;
            this.colstrUser3_Inspection.Width = 122;
            // 
            // colstrRemarks_Inspection
            // 
            this.colstrRemarks_Inspection.Caption = "Inspection Remarks";
            this.colstrRemarks_Inspection.FieldName = "strRemarks_Inspection";
            this.colstrRemarks_Inspection.Name = "colstrRemarks_Inspection";
            this.colstrRemarks_Inspection.OptionsColumn.AllowFocus = false;
            this.colstrRemarks_Inspection.OptionsColumn.ReadOnly = true;
            this.colstrRemarks_Inspection.Visible = true;
            this.colstrRemarks_Inspection.VisibleIndex = 88;
            this.colstrRemarks_Inspection.Width = 135;
            // 
            // colintVitality
            // 
            this.colintVitality.Caption = "Vitality";
            this.colintVitality.FieldName = "intVitality";
            this.colintVitality.Name = "colintVitality";
            this.colintVitality.OptionsColumn.AllowFocus = false;
            this.colintVitality.OptionsColumn.ReadOnly = true;
            this.colintVitality.Visible = true;
            this.colintVitality.VisibleIndex = 84;
            this.colintVitality.Width = 69;
            // 
            // coldtDueDate_1
            // 
            this.coldtDueDate_1.Caption = "Action 1 Due";
            this.coldtDueDate_1.ColumnEdit = this.repositoryItemDateEdit1;
            this.coldtDueDate_1.FieldName = "dtDueDate_1";
            this.coldtDueDate_1.Name = "coldtDueDate_1";
            this.coldtDueDate_1.Visible = true;
            this.coldtDueDate_1.VisibleIndex = 89;
            this.coldtDueDate_1.Width = 92;
            // 
            // coldtDoneDate_1
            // 
            this.coldtDoneDate_1.Caption = "Action 1 Done";
            this.coldtDoneDate_1.FieldName = "dtDoneDate_1";
            this.coldtDoneDate_1.Name = "coldtDoneDate_1";
            this.coldtDoneDate_1.OptionsColumn.AllowFocus = false;
            this.coldtDoneDate_1.OptionsColumn.ReadOnly = true;
            this.coldtDoneDate_1.Visible = true;
            this.coldtDoneDate_1.VisibleIndex = 90;
            this.coldtDoneDate_1.Width = 98;
            // 
            // colintAction_1
            // 
            this.colintAction_1.Caption = "Action 1";
            this.colintAction_1.FieldName = "intAction_1";
            this.colintAction_1.Name = "colintAction_1";
            this.colintAction_1.OptionsColumn.AllowFocus = false;
            this.colintAction_1.OptionsColumn.ReadOnly = true;
            this.colintAction_1.Visible = true;
            this.colintAction_1.VisibleIndex = 91;
            this.colintAction_1.Width = 79;
            // 
            // colintActionBy_1
            // 
            this.colintActionBy_1.Caption = "Action 1 By";
            this.colintActionBy_1.FieldName = "intActionBy_1";
            this.colintActionBy_1.Name = "colintActionBy_1";
            this.colintActionBy_1.OptionsColumn.AllowFocus = false;
            this.colintActionBy_1.OptionsColumn.ReadOnly = true;
            this.colintActionBy_1.Visible = true;
            this.colintActionBy_1.VisibleIndex = 92;
            this.colintActionBy_1.Width = 94;
            // 
            // colintSupervisor_1
            // 
            this.colintSupervisor_1.Caption = "Action 1 Supervisor";
            this.colintSupervisor_1.FieldName = "intSupervisor_1";
            this.colintSupervisor_1.Name = "colintSupervisor_1";
            this.colintSupervisor_1.OptionsColumn.AllowFocus = false;
            this.colintSupervisor_1.OptionsColumn.ReadOnly = true;
            this.colintSupervisor_1.Visible = true;
            this.colintSupervisor_1.VisibleIndex = 93;
            this.colintSupervisor_1.Width = 115;
            // 
            // colstrJobNumber_1
            // 
            this.colstrJobNumber_1.Caption = "Action 1 Job No";
            this.colstrJobNumber_1.ColumnEdit = this.repositoryItemButtonEditSequenceField;
            this.colstrJobNumber_1.FieldName = "strJobNumber_1";
            this.colstrJobNumber_1.Name = "colstrJobNumber_1";
            this.colstrJobNumber_1.Visible = true;
            this.colstrJobNumber_1.VisibleIndex = 94;
            this.colstrJobNumber_1.Width = 107;
            // 
            // colstrUser1_Action_1
            // 
            this.colstrUser1_Action_1.Caption = "Action 1 User 1";
            this.colstrUser1_Action_1.FieldName = "strUser1_Action_1";
            this.colstrUser1_Action_1.Name = "colstrUser1_Action_1";
            this.colstrUser1_Action_1.OptionsColumn.AllowFocus = false;
            this.colstrUser1_Action_1.OptionsColumn.ReadOnly = true;
            this.colstrUser1_Action_1.Visible = true;
            this.colstrUser1_Action_1.VisibleIndex = 95;
            this.colstrUser1_Action_1.Width = 114;
            // 
            // colstrUser2_Action_1
            // 
            this.colstrUser2_Action_1.Caption = "Action 1 User 2";
            this.colstrUser2_Action_1.FieldName = "strUser2_Action_1";
            this.colstrUser2_Action_1.Name = "colstrUser2_Action_1";
            this.colstrUser2_Action_1.OptionsColumn.AllowFocus = false;
            this.colstrUser2_Action_1.OptionsColumn.ReadOnly = true;
            this.colstrUser2_Action_1.Visible = true;
            this.colstrUser2_Action_1.VisibleIndex = 96;
            this.colstrUser2_Action_1.Width = 114;
            // 
            // colstrUser3_Action_1
            // 
            this.colstrUser3_Action_1.Caption = "Action 1 User 3";
            this.colstrUser3_Action_1.FieldName = "strUser3_Action_1";
            this.colstrUser3_Action_1.Name = "colstrUser3_Action_1";
            this.colstrUser3_Action_1.OptionsColumn.AllowFocus = false;
            this.colstrUser3_Action_1.OptionsColumn.ReadOnly = true;
            this.colstrUser3_Action_1.Visible = true;
            this.colstrUser3_Action_1.VisibleIndex = 97;
            this.colstrUser3_Action_1.Width = 114;
            // 
            // colstrRemarks_Action_1
            // 
            this.colstrRemarks_Action_1.Caption = "Action 1 Remarks";
            this.colstrRemarks_Action_1.FieldName = "strRemarks_Action_1";
            this.colstrRemarks_Action_1.Name = "colstrRemarks_Action_1";
            this.colstrRemarks_Action_1.OptionsColumn.AllowFocus = false;
            this.colstrRemarks_Action_1.OptionsColumn.ReadOnly = true;
            this.colstrRemarks_Action_1.Visible = true;
            this.colstrRemarks_Action_1.VisibleIndex = 98;
            this.colstrRemarks_Action_1.Width = 127;
            // 
            // colintWorkOrderID_1
            // 
            this.colintWorkOrderID_1.Caption = "Action 1 Work Order";
            this.colintWorkOrderID_1.FieldName = "intWorkOrderID_1";
            this.colintWorkOrderID_1.Name = "colintWorkOrderID_1";
            this.colintWorkOrderID_1.OptionsColumn.AllowFocus = false;
            this.colintWorkOrderID_1.OptionsColumn.ReadOnly = true;
            this.colintWorkOrderID_1.Visible = true;
            this.colintWorkOrderID_1.VisibleIndex = 99;
            this.colintWorkOrderID_1.Width = 119;
            // 
            // colintPriorityID_1
            // 
            this.colintPriorityID_1.Caption = "Action 1 Priority";
            this.colintPriorityID_1.FieldName = "intPriorityID_1";
            this.colintPriorityID_1.Name = "colintPriorityID_1";
            this.colintPriorityID_1.OptionsColumn.AllowFocus = false;
            this.colintPriorityID_1.OptionsColumn.ReadOnly = true;
            this.colintPriorityID_1.Visible = true;
            this.colintPriorityID_1.VisibleIndex = 100;
            this.colintPriorityID_1.Width = 97;
            // 
            // colintScheduleOfRates_1
            // 
            this.colintScheduleOfRates_1.Caption = "Action 1 Schedule Of Rates";
            this.colintScheduleOfRates_1.FieldName = "intScheduleOfRates_1";
            this.colintScheduleOfRates_1.Name = "colintScheduleOfRates_1";
            this.colintScheduleOfRates_1.OptionsColumn.AllowFocus = false;
            this.colintScheduleOfRates_1.OptionsColumn.ReadOnly = true;
            this.colintScheduleOfRates_1.Visible = true;
            this.colintScheduleOfRates_1.VisibleIndex = 101;
            this.colintScheduleOfRates_1.Width = 153;
            // 
            // colintOwnership_1
            // 
            this.colintOwnership_1.Caption = "Action 1 Ownership";
            this.colintOwnership_1.FieldName = "intOwnership_1";
            this.colintOwnership_1.Name = "colintOwnership_1";
            this.colintOwnership_1.OptionsColumn.AllowFocus = false;
            this.colintOwnership_1.OptionsColumn.ReadOnly = true;
            this.colintOwnership_1.Visible = true;
            this.colintOwnership_1.VisibleIndex = 103;
            this.colintOwnership_1.Width = 115;
            // 
            // coldtDueDate_2
            // 
            this.coldtDueDate_2.Caption = "Action 2 Due";
            this.coldtDueDate_2.ColumnEdit = this.repositoryItemDateEdit1;
            this.coldtDueDate_2.FieldName = "dtDueDate_2";
            this.coldtDueDate_2.Name = "coldtDueDate_2";
            this.coldtDueDate_2.Visible = true;
            this.coldtDueDate_2.VisibleIndex = 104;
            this.coldtDueDate_2.Width = 92;
            // 
            // coldtDoneDate_2
            // 
            this.coldtDoneDate_2.Caption = "Action 2 Done";
            this.coldtDoneDate_2.FieldName = "dtDoneDate_2";
            this.coldtDoneDate_2.Name = "coldtDoneDate_2";
            this.coldtDoneDate_2.OptionsColumn.AllowFocus = false;
            this.coldtDoneDate_2.OptionsColumn.ReadOnly = true;
            this.coldtDoneDate_2.Visible = true;
            this.coldtDoneDate_2.VisibleIndex = 105;
            this.coldtDoneDate_2.Width = 98;
            // 
            // colintAction_2
            // 
            this.colintAction_2.Caption = "Action 2";
            this.colintAction_2.FieldName = "intAction_2";
            this.colintAction_2.Name = "colintAction_2";
            this.colintAction_2.OptionsColumn.AllowFocus = false;
            this.colintAction_2.OptionsColumn.ReadOnly = true;
            this.colintAction_2.Visible = true;
            this.colintAction_2.VisibleIndex = 106;
            this.colintAction_2.Width = 79;
            // 
            // colintActionBy_2
            // 
            this.colintActionBy_2.Caption = "Action 2 By";
            this.colintActionBy_2.FieldName = "intActionBy_2";
            this.colintActionBy_2.Name = "colintActionBy_2";
            this.colintActionBy_2.OptionsColumn.AllowFocus = false;
            this.colintActionBy_2.OptionsColumn.ReadOnly = true;
            this.colintActionBy_2.Visible = true;
            this.colintActionBy_2.VisibleIndex = 107;
            this.colintActionBy_2.Width = 94;
            // 
            // colintSupervisor_2
            // 
            this.colintSupervisor_2.Caption = "Action 2 Supervisor";
            this.colintSupervisor_2.FieldName = "intSupervisor_2";
            this.colintSupervisor_2.Name = "colintSupervisor_2";
            this.colintSupervisor_2.OptionsColumn.AllowFocus = false;
            this.colintSupervisor_2.OptionsColumn.ReadOnly = true;
            this.colintSupervisor_2.Visible = true;
            this.colintSupervisor_2.VisibleIndex = 108;
            this.colintSupervisor_2.Width = 115;
            // 
            // colstrJobNumber_2
            // 
            this.colstrJobNumber_2.Caption = "Action 2 Job No";
            this.colstrJobNumber_2.ColumnEdit = this.repositoryItemButtonEditSequenceField;
            this.colstrJobNumber_2.FieldName = "strJobNumber_2";
            this.colstrJobNumber_2.Name = "colstrJobNumber_2";
            this.colstrJobNumber_2.Visible = true;
            this.colstrJobNumber_2.VisibleIndex = 109;
            this.colstrJobNumber_2.Width = 107;
            // 
            // colstrUser1_Action_2
            // 
            this.colstrUser1_Action_2.Caption = "Action 2 User 1";
            this.colstrUser1_Action_2.FieldName = "strUser1_Action_2";
            this.colstrUser1_Action_2.Name = "colstrUser1_Action_2";
            this.colstrUser1_Action_2.OptionsColumn.AllowFocus = false;
            this.colstrUser1_Action_2.OptionsColumn.ReadOnly = true;
            this.colstrUser1_Action_2.Visible = true;
            this.colstrUser1_Action_2.VisibleIndex = 110;
            this.colstrUser1_Action_2.Width = 114;
            // 
            // colstrUser2_Action_2
            // 
            this.colstrUser2_Action_2.Caption = "Action 2 User 2";
            this.colstrUser2_Action_2.FieldName = "strUser2_Action_2";
            this.colstrUser2_Action_2.Name = "colstrUser2_Action_2";
            this.colstrUser2_Action_2.OptionsColumn.AllowFocus = false;
            this.colstrUser2_Action_2.OptionsColumn.ReadOnly = true;
            this.colstrUser2_Action_2.Visible = true;
            this.colstrUser2_Action_2.VisibleIndex = 111;
            this.colstrUser2_Action_2.Width = 114;
            // 
            // colstrUser3_Action_2
            // 
            this.colstrUser3_Action_2.Caption = "Action 2 User 3";
            this.colstrUser3_Action_2.FieldName = "strUser3_Action_2";
            this.colstrUser3_Action_2.Name = "colstrUser3_Action_2";
            this.colstrUser3_Action_2.OptionsColumn.AllowFocus = false;
            this.colstrUser3_Action_2.OptionsColumn.ReadOnly = true;
            this.colstrUser3_Action_2.Visible = true;
            this.colstrUser3_Action_2.VisibleIndex = 112;
            this.colstrUser3_Action_2.Width = 114;
            // 
            // colstrRemarks_Action_2
            // 
            this.colstrRemarks_Action_2.Caption = "Action 2 Remarks";
            this.colstrRemarks_Action_2.FieldName = "strRemarks_Action_2";
            this.colstrRemarks_Action_2.Name = "colstrRemarks_Action_2";
            this.colstrRemarks_Action_2.OptionsColumn.AllowFocus = false;
            this.colstrRemarks_Action_2.OptionsColumn.ReadOnly = true;
            this.colstrRemarks_Action_2.Visible = true;
            this.colstrRemarks_Action_2.VisibleIndex = 113;
            this.colstrRemarks_Action_2.Width = 127;
            // 
            // colintWorkOrderID_2
            // 
            this.colintWorkOrderID_2.Caption = "Action 2 Work Order";
            this.colintWorkOrderID_2.FieldName = "intWorkOrderID_2";
            this.colintWorkOrderID_2.Name = "colintWorkOrderID_2";
            this.colintWorkOrderID_2.OptionsColumn.AllowFocus = false;
            this.colintWorkOrderID_2.OptionsColumn.ReadOnly = true;
            this.colintWorkOrderID_2.Visible = true;
            this.colintWorkOrderID_2.VisibleIndex = 114;
            this.colintWorkOrderID_2.Width = 119;
            // 
            // colintPriorityID_2
            // 
            this.colintPriorityID_2.Caption = "Action 2 Priority";
            this.colintPriorityID_2.FieldName = "intPriorityID_2";
            this.colintPriorityID_2.Name = "colintPriorityID_2";
            this.colintPriorityID_2.OptionsColumn.AllowFocus = false;
            this.colintPriorityID_2.OptionsColumn.ReadOnly = true;
            this.colintPriorityID_2.Visible = true;
            this.colintPriorityID_2.VisibleIndex = 115;
            this.colintPriorityID_2.Width = 97;
            // 
            // colintScheduleOfRates_2
            // 
            this.colintScheduleOfRates_2.Caption = "Action 2 Schedule Of Rates";
            this.colintScheduleOfRates_2.FieldName = "intScheduleOfRates_2";
            this.colintScheduleOfRates_2.Name = "colintScheduleOfRates_2";
            this.colintScheduleOfRates_2.OptionsColumn.AllowFocus = false;
            this.colintScheduleOfRates_2.OptionsColumn.ReadOnly = true;
            this.colintScheduleOfRates_2.Visible = true;
            this.colintScheduleOfRates_2.VisibleIndex = 116;
            this.colintScheduleOfRates_2.Width = 153;
            // 
            // colintOwnership_2
            // 
            this.colintOwnership_2.Caption = "Action 2 Ownership";
            this.colintOwnership_2.FieldName = "intOwnership_2";
            this.colintOwnership_2.Name = "colintOwnership_2";
            this.colintOwnership_2.OptionsColumn.AllowFocus = false;
            this.colintOwnership_2.OptionsColumn.ReadOnly = true;
            this.colintOwnership_2.Visible = true;
            this.colintOwnership_2.VisibleIndex = 118;
            this.colintOwnership_2.Width = 115;
            // 
            // coldtDueDate_3
            // 
            this.coldtDueDate_3.Caption = "Action 3 Due";
            this.coldtDueDate_3.ColumnEdit = this.repositoryItemDateEdit1;
            this.coldtDueDate_3.FieldName = "dtDueDate_3";
            this.coldtDueDate_3.Name = "coldtDueDate_3";
            this.coldtDueDate_3.Visible = true;
            this.coldtDueDate_3.VisibleIndex = 119;
            this.coldtDueDate_3.Width = 92;
            // 
            // coldtDoneDate_3
            // 
            this.coldtDoneDate_3.Caption = "Action 3 Done";
            this.coldtDoneDate_3.FieldName = "dtDoneDate_3";
            this.coldtDoneDate_3.Name = "coldtDoneDate_3";
            this.coldtDoneDate_3.OptionsColumn.AllowFocus = false;
            this.coldtDoneDate_3.OptionsColumn.ReadOnly = true;
            this.coldtDoneDate_3.Visible = true;
            this.coldtDoneDate_3.VisibleIndex = 120;
            this.coldtDoneDate_3.Width = 98;
            // 
            // colintAction_3
            // 
            this.colintAction_3.Caption = "Action 3";
            this.colintAction_3.FieldName = "intAction_3";
            this.colintAction_3.Name = "colintAction_3";
            this.colintAction_3.OptionsColumn.AllowFocus = false;
            this.colintAction_3.OptionsColumn.ReadOnly = true;
            this.colintAction_3.Visible = true;
            this.colintAction_3.VisibleIndex = 121;
            this.colintAction_3.Width = 79;
            // 
            // colintActionBy_3
            // 
            this.colintActionBy_3.Caption = "Action 3 By";
            this.colintActionBy_3.FieldName = "intActionBy_3";
            this.colintActionBy_3.Name = "colintActionBy_3";
            this.colintActionBy_3.OptionsColumn.AllowFocus = false;
            this.colintActionBy_3.OptionsColumn.ReadOnly = true;
            this.colintActionBy_3.Visible = true;
            this.colintActionBy_3.VisibleIndex = 122;
            this.colintActionBy_3.Width = 94;
            // 
            // colintSupervisor_3
            // 
            this.colintSupervisor_3.Caption = "Action 3 Supervisor";
            this.colintSupervisor_3.FieldName = "intSupervisor_3";
            this.colintSupervisor_3.Name = "colintSupervisor_3";
            this.colintSupervisor_3.OptionsColumn.AllowFocus = false;
            this.colintSupervisor_3.OptionsColumn.ReadOnly = true;
            this.colintSupervisor_3.Visible = true;
            this.colintSupervisor_3.VisibleIndex = 123;
            this.colintSupervisor_3.Width = 115;
            // 
            // colstrJobNumber_3
            // 
            this.colstrJobNumber_3.Caption = "Action 3 Job No";
            this.colstrJobNumber_3.ColumnEdit = this.repositoryItemButtonEditSequenceField;
            this.colstrJobNumber_3.FieldName = "strJobNumber_3";
            this.colstrJobNumber_3.Name = "colstrJobNumber_3";
            this.colstrJobNumber_3.Visible = true;
            this.colstrJobNumber_3.VisibleIndex = 124;
            this.colstrJobNumber_3.Width = 107;
            // 
            // colstrUser1_Action_3
            // 
            this.colstrUser1_Action_3.Caption = "Action 3 User 1";
            this.colstrUser1_Action_3.FieldName = "strUser1_Action_3";
            this.colstrUser1_Action_3.Name = "colstrUser1_Action_3";
            this.colstrUser1_Action_3.OptionsColumn.AllowFocus = false;
            this.colstrUser1_Action_3.OptionsColumn.ReadOnly = true;
            this.colstrUser1_Action_3.Visible = true;
            this.colstrUser1_Action_3.VisibleIndex = 125;
            this.colstrUser1_Action_3.Width = 114;
            // 
            // colstrUser2_Action_3
            // 
            this.colstrUser2_Action_3.Caption = "Action 3 User 2";
            this.colstrUser2_Action_3.FieldName = "strUser2_Action_3";
            this.colstrUser2_Action_3.Name = "colstrUser2_Action_3";
            this.colstrUser2_Action_3.OptionsColumn.AllowFocus = false;
            this.colstrUser2_Action_3.OptionsColumn.ReadOnly = true;
            this.colstrUser2_Action_3.Visible = true;
            this.colstrUser2_Action_3.VisibleIndex = 126;
            this.colstrUser2_Action_3.Width = 114;
            // 
            // colstrUser3_Action_3
            // 
            this.colstrUser3_Action_3.Caption = "Action 3 User 3";
            this.colstrUser3_Action_3.FieldName = "strUser3_Action_3";
            this.colstrUser3_Action_3.Name = "colstrUser3_Action_3";
            this.colstrUser3_Action_3.OptionsColumn.AllowFocus = false;
            this.colstrUser3_Action_3.OptionsColumn.ReadOnly = true;
            this.colstrUser3_Action_3.Visible = true;
            this.colstrUser3_Action_3.VisibleIndex = 127;
            this.colstrUser3_Action_3.Width = 114;
            // 
            // colstrRemarks_Action_3
            // 
            this.colstrRemarks_Action_3.Caption = "Action 3 Remarks";
            this.colstrRemarks_Action_3.FieldName = "strRemarks_Action_3";
            this.colstrRemarks_Action_3.Name = "colstrRemarks_Action_3";
            this.colstrRemarks_Action_3.OptionsColumn.AllowFocus = false;
            this.colstrRemarks_Action_3.OptionsColumn.ReadOnly = true;
            this.colstrRemarks_Action_3.Visible = true;
            this.colstrRemarks_Action_3.VisibleIndex = 128;
            this.colstrRemarks_Action_3.Width = 127;
            // 
            // colintWorkOrderID_3
            // 
            this.colintWorkOrderID_3.Caption = "Action 3 Work Order";
            this.colintWorkOrderID_3.FieldName = "intWorkOrderID_3";
            this.colintWorkOrderID_3.Name = "colintWorkOrderID_3";
            this.colintWorkOrderID_3.OptionsColumn.AllowFocus = false;
            this.colintWorkOrderID_3.OptionsColumn.ReadOnly = true;
            this.colintWorkOrderID_3.Visible = true;
            this.colintWorkOrderID_3.VisibleIndex = 129;
            this.colintWorkOrderID_3.Width = 119;
            // 
            // colintPriorityID_3
            // 
            this.colintPriorityID_3.Caption = "Action 3  Priority";
            this.colintPriorityID_3.FieldName = "intPriorityID_3";
            this.colintPriorityID_3.Name = "colintPriorityID_3";
            this.colintPriorityID_3.OptionsColumn.AllowFocus = false;
            this.colintPriorityID_3.OptionsColumn.ReadOnly = true;
            this.colintPriorityID_3.Visible = true;
            this.colintPriorityID_3.VisibleIndex = 130;
            this.colintPriorityID_3.Width = 101;
            // 
            // colintScheduleOfRates_3
            // 
            this.colintScheduleOfRates_3.Caption = "Action 3 Schedule Of Rates";
            this.colintScheduleOfRates_3.FieldName = "intScheduleOfRates_3";
            this.colintScheduleOfRates_3.Name = "colintScheduleOfRates_3";
            this.colintScheduleOfRates_3.OptionsColumn.AllowFocus = false;
            this.colintScheduleOfRates_3.OptionsColumn.ReadOnly = true;
            this.colintScheduleOfRates_3.Visible = true;
            this.colintScheduleOfRates_3.VisibleIndex = 131;
            this.colintScheduleOfRates_3.Width = 153;
            // 
            // colintOwnership_3
            // 
            this.colintOwnership_3.Caption = "Action 3 Ownership";
            this.colintOwnership_3.FieldName = "intOwnership_3";
            this.colintOwnership_3.Name = "colintOwnership_3";
            this.colintOwnership_3.OptionsColumn.AllowFocus = false;
            this.colintOwnership_3.OptionsColumn.ReadOnly = true;
            this.colintOwnership_3.Visible = true;
            this.colintOwnership_3.VisibleIndex = 133;
            this.colintOwnership_3.Width = 115;
            // 
            // coldtDueDate_4
            // 
            this.coldtDueDate_4.Caption = "Action 4 Due";
            this.coldtDueDate_4.ColumnEdit = this.repositoryItemDateEdit1;
            this.coldtDueDate_4.FieldName = "dtDueDate_4";
            this.coldtDueDate_4.Name = "coldtDueDate_4";
            this.coldtDueDate_4.Visible = true;
            this.coldtDueDate_4.VisibleIndex = 134;
            this.coldtDueDate_4.Width = 92;
            // 
            // coldtDoneDate_4
            // 
            this.coldtDoneDate_4.Caption = "Action 4 Done";
            this.coldtDoneDate_4.FieldName = "dtDoneDate_4";
            this.coldtDoneDate_4.Name = "coldtDoneDate_4";
            this.coldtDoneDate_4.OptionsColumn.AllowFocus = false;
            this.coldtDoneDate_4.OptionsColumn.ReadOnly = true;
            this.coldtDoneDate_4.Visible = true;
            this.coldtDoneDate_4.VisibleIndex = 135;
            this.coldtDoneDate_4.Width = 98;
            // 
            // colintAction_4
            // 
            this.colintAction_4.Caption = "Action 4";
            this.colintAction_4.FieldName = "intAction_4";
            this.colintAction_4.Name = "colintAction_4";
            this.colintAction_4.OptionsColumn.AllowFocus = false;
            this.colintAction_4.OptionsColumn.ReadOnly = true;
            this.colintAction_4.Visible = true;
            this.colintAction_4.VisibleIndex = 136;
            this.colintAction_4.Width = 79;
            // 
            // colintActionBy_4
            // 
            this.colintActionBy_4.Caption = "Action 4 By";
            this.colintActionBy_4.FieldName = "intActionBy_4";
            this.colintActionBy_4.Name = "colintActionBy_4";
            this.colintActionBy_4.OptionsColumn.AllowFocus = false;
            this.colintActionBy_4.OptionsColumn.ReadOnly = true;
            this.colintActionBy_4.Visible = true;
            this.colintActionBy_4.VisibleIndex = 137;
            this.colintActionBy_4.Width = 94;
            // 
            // colintSupervisor_4
            // 
            this.colintSupervisor_4.Caption = "Action 4 Supervisor";
            this.colintSupervisor_4.FieldName = "intSupervisor_4";
            this.colintSupervisor_4.Name = "colintSupervisor_4";
            this.colintSupervisor_4.OptionsColumn.AllowFocus = false;
            this.colintSupervisor_4.OptionsColumn.ReadOnly = true;
            this.colintSupervisor_4.Visible = true;
            this.colintSupervisor_4.VisibleIndex = 138;
            this.colintSupervisor_4.Width = 115;
            // 
            // colstrJobNumber_4
            // 
            this.colstrJobNumber_4.Caption = "Action 4 Job No";
            this.colstrJobNumber_4.ColumnEdit = this.repositoryItemButtonEditSequenceField;
            this.colstrJobNumber_4.FieldName = "strJobNumber_4";
            this.colstrJobNumber_4.Name = "colstrJobNumber_4";
            this.colstrJobNumber_4.Visible = true;
            this.colstrJobNumber_4.VisibleIndex = 139;
            this.colstrJobNumber_4.Width = 107;
            // 
            // colstrUser1_Action_4
            // 
            this.colstrUser1_Action_4.Caption = "Action 4 User 1";
            this.colstrUser1_Action_4.FieldName = "strUser1_Action_4";
            this.colstrUser1_Action_4.Name = "colstrUser1_Action_4";
            this.colstrUser1_Action_4.OptionsColumn.AllowFocus = false;
            this.colstrUser1_Action_4.OptionsColumn.ReadOnly = true;
            this.colstrUser1_Action_4.Visible = true;
            this.colstrUser1_Action_4.VisibleIndex = 140;
            this.colstrUser1_Action_4.Width = 114;
            // 
            // colstrUser2_Action_4
            // 
            this.colstrUser2_Action_4.Caption = "Action 4 User 2";
            this.colstrUser2_Action_4.FieldName = "strUser2_Action_4";
            this.colstrUser2_Action_4.Name = "colstrUser2_Action_4";
            this.colstrUser2_Action_4.OptionsColumn.AllowFocus = false;
            this.colstrUser2_Action_4.OptionsColumn.ReadOnly = true;
            this.colstrUser2_Action_4.Visible = true;
            this.colstrUser2_Action_4.VisibleIndex = 141;
            this.colstrUser2_Action_4.Width = 114;
            // 
            // colstrUser3_Action_4
            // 
            this.colstrUser3_Action_4.Caption = "Action 4 User 3";
            this.colstrUser3_Action_4.FieldName = "strUser3_Action_4";
            this.colstrUser3_Action_4.Name = "colstrUser3_Action_4";
            this.colstrUser3_Action_4.OptionsColumn.AllowFocus = false;
            this.colstrUser3_Action_4.OptionsColumn.ReadOnly = true;
            this.colstrUser3_Action_4.Visible = true;
            this.colstrUser3_Action_4.VisibleIndex = 142;
            this.colstrUser3_Action_4.Width = 114;
            // 
            // colstrRemarks_Action_4
            // 
            this.colstrRemarks_Action_4.Caption = "Action 4 Remarks";
            this.colstrRemarks_Action_4.FieldName = "strRemarks_Action_4";
            this.colstrRemarks_Action_4.Name = "colstrRemarks_Action_4";
            this.colstrRemarks_Action_4.OptionsColumn.AllowFocus = false;
            this.colstrRemarks_Action_4.OptionsColumn.ReadOnly = true;
            this.colstrRemarks_Action_4.Visible = true;
            this.colstrRemarks_Action_4.VisibleIndex = 143;
            this.colstrRemarks_Action_4.Width = 127;
            // 
            // colintWorkOrderID_4
            // 
            this.colintWorkOrderID_4.Caption = "Action 4 Work Order";
            this.colintWorkOrderID_4.FieldName = "intWorkOrderID_4";
            this.colintWorkOrderID_4.Name = "colintWorkOrderID_4";
            this.colintWorkOrderID_4.OptionsColumn.AllowFocus = false;
            this.colintWorkOrderID_4.OptionsColumn.ReadOnly = true;
            this.colintWorkOrderID_4.Visible = true;
            this.colintWorkOrderID_4.VisibleIndex = 144;
            this.colintWorkOrderID_4.Width = 119;
            // 
            // colintPriorityID_4
            // 
            this.colintPriorityID_4.Caption = "Action 4 Priority";
            this.colintPriorityID_4.FieldName = "intPriorityID_4";
            this.colintPriorityID_4.Name = "colintPriorityID_4";
            this.colintPriorityID_4.OptionsColumn.AllowFocus = false;
            this.colintPriorityID_4.OptionsColumn.ReadOnly = true;
            this.colintPriorityID_4.Visible = true;
            this.colintPriorityID_4.VisibleIndex = 145;
            this.colintPriorityID_4.Width = 97;
            // 
            // colintScheduleOfRates_4
            // 
            this.colintScheduleOfRates_4.Caption = "Action 4 Schedule Of Rates";
            this.colintScheduleOfRates_4.FieldName = "intScheduleOfRates_4";
            this.colintScheduleOfRates_4.Name = "colintScheduleOfRates_4";
            this.colintScheduleOfRates_4.OptionsColumn.AllowFocus = false;
            this.colintScheduleOfRates_4.OptionsColumn.ReadOnly = true;
            this.colintScheduleOfRates_4.Visible = true;
            this.colintScheduleOfRates_4.VisibleIndex = 146;
            this.colintScheduleOfRates_4.Width = 153;
            // 
            // colintOwnership_4
            // 
            this.colintOwnership_4.Caption = "Action 4 Ownership";
            this.colintOwnership_4.FieldName = "intOwnership_4";
            this.colintOwnership_4.Name = "colintOwnership_4";
            this.colintOwnership_4.OptionsColumn.AllowFocus = false;
            this.colintOwnership_4.OptionsColumn.ReadOnly = true;
            this.colintOwnership_4.Visible = true;
            this.colintOwnership_4.VisibleIndex = 148;
            this.colintOwnership_4.Width = 115;
            // 
            // coldtDueDate_5
            // 
            this.coldtDueDate_5.Caption = "Action 5 Due";
            this.coldtDueDate_5.ColumnEdit = this.repositoryItemDateEdit1;
            this.coldtDueDate_5.FieldName = "dtDueDate_5";
            this.coldtDueDate_5.Name = "coldtDueDate_5";
            this.coldtDueDate_5.Visible = true;
            this.coldtDueDate_5.VisibleIndex = 149;
            this.coldtDueDate_5.Width = 92;
            // 
            // coldtDoneDate_5
            // 
            this.coldtDoneDate_5.Caption = "Action 5 Done";
            this.coldtDoneDate_5.FieldName = "dtDoneDate_5";
            this.coldtDoneDate_5.Name = "coldtDoneDate_5";
            this.coldtDoneDate_5.OptionsColumn.AllowFocus = false;
            this.coldtDoneDate_5.OptionsColumn.ReadOnly = true;
            this.coldtDoneDate_5.Visible = true;
            this.coldtDoneDate_5.VisibleIndex = 150;
            this.coldtDoneDate_5.Width = 98;
            // 
            // colintAction_5
            // 
            this.colintAction_5.Caption = "Action 5";
            this.colintAction_5.FieldName = "intAction_5";
            this.colintAction_5.Name = "colintAction_5";
            this.colintAction_5.OptionsColumn.AllowFocus = false;
            this.colintAction_5.OptionsColumn.ReadOnly = true;
            this.colintAction_5.Visible = true;
            this.colintAction_5.VisibleIndex = 151;
            this.colintAction_5.Width = 79;
            // 
            // colintActionBy_5
            // 
            this.colintActionBy_5.Caption = "Action 5 By";
            this.colintActionBy_5.FieldName = "intActionBy_5";
            this.colintActionBy_5.Name = "colintActionBy_5";
            this.colintActionBy_5.OptionsColumn.AllowFocus = false;
            this.colintActionBy_5.OptionsColumn.ReadOnly = true;
            this.colintActionBy_5.Visible = true;
            this.colintActionBy_5.VisibleIndex = 152;
            this.colintActionBy_5.Width = 94;
            // 
            // colintSupervisor_5
            // 
            this.colintSupervisor_5.Caption = "Action 5 Supervisor";
            this.colintSupervisor_5.FieldName = "intSupervisor_5";
            this.colintSupervisor_5.Name = "colintSupervisor_5";
            this.colintSupervisor_5.OptionsColumn.AllowFocus = false;
            this.colintSupervisor_5.OptionsColumn.ReadOnly = true;
            this.colintSupervisor_5.Visible = true;
            this.colintSupervisor_5.VisibleIndex = 153;
            this.colintSupervisor_5.Width = 115;
            // 
            // colstrJobNumber_5
            // 
            this.colstrJobNumber_5.Caption = "Action 5 Job No";
            this.colstrJobNumber_5.ColumnEdit = this.repositoryItemButtonEditSequenceField;
            this.colstrJobNumber_5.FieldName = "strJobNumber_5";
            this.colstrJobNumber_5.Name = "colstrJobNumber_5";
            this.colstrJobNumber_5.Visible = true;
            this.colstrJobNumber_5.VisibleIndex = 154;
            this.colstrJobNumber_5.Width = 107;
            // 
            // colstrUser1_Action_5
            // 
            this.colstrUser1_Action_5.Caption = "Action 5 User 1";
            this.colstrUser1_Action_5.FieldName = "strUser1_Action_5";
            this.colstrUser1_Action_5.Name = "colstrUser1_Action_5";
            this.colstrUser1_Action_5.OptionsColumn.AllowFocus = false;
            this.colstrUser1_Action_5.OptionsColumn.ReadOnly = true;
            this.colstrUser1_Action_5.Visible = true;
            this.colstrUser1_Action_5.VisibleIndex = 155;
            this.colstrUser1_Action_5.Width = 114;
            // 
            // colstrUser2_Action_5
            // 
            this.colstrUser2_Action_5.Caption = "Action 5 User 2";
            this.colstrUser2_Action_5.FieldName = "strUser2_Action_5";
            this.colstrUser2_Action_5.Name = "colstrUser2_Action_5";
            this.colstrUser2_Action_5.OptionsColumn.AllowFocus = false;
            this.colstrUser2_Action_5.OptionsColumn.ReadOnly = true;
            this.colstrUser2_Action_5.Visible = true;
            this.colstrUser2_Action_5.VisibleIndex = 156;
            this.colstrUser2_Action_5.Width = 114;
            // 
            // colstrUser3_Action_5
            // 
            this.colstrUser3_Action_5.Caption = "Action 5 User 3";
            this.colstrUser3_Action_5.FieldName = "strUser3_Action_5";
            this.colstrUser3_Action_5.Name = "colstrUser3_Action_5";
            this.colstrUser3_Action_5.OptionsColumn.AllowFocus = false;
            this.colstrUser3_Action_5.OptionsColumn.ReadOnly = true;
            this.colstrUser3_Action_5.Visible = true;
            this.colstrUser3_Action_5.VisibleIndex = 157;
            this.colstrUser3_Action_5.Width = 114;
            // 
            // colstrRemarks_Action_5
            // 
            this.colstrRemarks_Action_5.Caption = "Action 5 Remarks";
            this.colstrRemarks_Action_5.FieldName = "strRemarks_Action_5";
            this.colstrRemarks_Action_5.Name = "colstrRemarks_Action_5";
            this.colstrRemarks_Action_5.OptionsColumn.AllowFocus = false;
            this.colstrRemarks_Action_5.OptionsColumn.ReadOnly = true;
            this.colstrRemarks_Action_5.Visible = true;
            this.colstrRemarks_Action_5.VisibleIndex = 158;
            this.colstrRemarks_Action_5.Width = 127;
            // 
            // colintWorkOrderID_5
            // 
            this.colintWorkOrderID_5.Caption = "Action 5 Work Order";
            this.colintWorkOrderID_5.FieldName = "intWorkOrderID_5";
            this.colintWorkOrderID_5.Name = "colintWorkOrderID_5";
            this.colintWorkOrderID_5.OptionsColumn.AllowFocus = false;
            this.colintWorkOrderID_5.OptionsColumn.ReadOnly = true;
            this.colintWorkOrderID_5.Visible = true;
            this.colintWorkOrderID_5.VisibleIndex = 159;
            this.colintWorkOrderID_5.Width = 119;
            // 
            // colintPriorityID_5
            // 
            this.colintPriorityID_5.Caption = "Action 5 Priority";
            this.colintPriorityID_5.FieldName = "intPriorityID_5";
            this.colintPriorityID_5.Name = "colintPriorityID_5";
            this.colintPriorityID_5.OptionsColumn.AllowFocus = false;
            this.colintPriorityID_5.OptionsColumn.ReadOnly = true;
            this.colintPriorityID_5.Visible = true;
            this.colintPriorityID_5.VisibleIndex = 160;
            this.colintPriorityID_5.Width = 97;
            // 
            // colintScheduleOfRates_5
            // 
            this.colintScheduleOfRates_5.Caption = "Action 5 Schedule Of Rates";
            this.colintScheduleOfRates_5.FieldName = "intScheduleOfRates_5";
            this.colintScheduleOfRates_5.Name = "colintScheduleOfRates_5";
            this.colintScheduleOfRates_5.OptionsColumn.AllowFocus = false;
            this.colintScheduleOfRates_5.OptionsColumn.ReadOnly = true;
            this.colintScheduleOfRates_5.Visible = true;
            this.colintScheduleOfRates_5.VisibleIndex = 161;
            this.colintScheduleOfRates_5.Width = 153;
            // 
            // colintOwnership_5
            // 
            this.colintOwnership_5.Caption = "Action 5 Ownership";
            this.colintOwnership_5.FieldName = "intOwnership_5";
            this.colintOwnership_5.Name = "colintOwnership_5";
            this.colintOwnership_5.OptionsColumn.AllowFocus = false;
            this.colintOwnership_5.OptionsColumn.ReadOnly = true;
            this.colintOwnership_5.Visible = true;
            this.colintOwnership_5.VisibleIndex = 163;
            this.colintOwnership_5.Width = 115;
            // 
            // colObjectType
            // 
            this.colObjectType.Caption = "Object Type";
            this.colObjectType.FieldName = "ObjectType";
            this.colObjectType.Name = "colObjectType";
            this.colObjectType.OptionsColumn.AllowFocus = false;
            this.colObjectType.OptionsColumn.ReadOnly = true;
            this.colObjectType.Visible = true;
            this.colObjectType.VisibleIndex = 164;
            this.colObjectType.Width = 81;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Visible = true;
            this.colRecordOrder.VisibleIndex = 0;
            this.colRecordOrder.Width = 63;
            // 
            // colTreePhotograph
            // 
            this.colTreePhotograph.Caption = "Tree Photo";
            this.colTreePhotograph.FieldName = "TreePhotograph";
            this.colTreePhotograph.Name = "colTreePhotograph";
            this.colTreePhotograph.OptionsColumn.AllowFocus = false;
            this.colTreePhotograph.OptionsColumn.ReadOnly = true;
            this.colTreePhotograph.Visible = true;
            this.colTreePhotograph.VisibleIndex = 9;
            this.colTreePhotograph.Width = 74;
            // 
            // colInspectionPhotograph
            // 
            this.colInspectionPhotograph.Caption = "Inspection Photo";
            this.colInspectionPhotograph.FieldName = "InspectionPhotograph";
            this.colInspectionPhotograph.Name = "colInspectionPhotograph";
            this.colInspectionPhotograph.OptionsColumn.AllowFocus = false;
            this.colInspectionPhotograph.OptionsColumn.ReadOnly = true;
            this.colInspectionPhotograph.Visible = true;
            this.colInspectionPhotograph.VisibleIndex = 10;
            this.colInspectionPhotograph.Width = 102;
            // 
            // colCavat
            // 
            this.colCavat.Caption = "Cavat";
            this.colCavat.FieldName = "Cavat";
            this.colCavat.Name = "colCavat";
            this.colCavat.OptionsColumn.AllowFocus = false;
            this.colCavat.OptionsColumn.ReadOnly = true;
            this.colCavat.Visible = true;
            this.colCavat.VisibleIndex = 19;
            this.colCavat.Width = 50;
            // 
            // colStemCount
            // 
            this.colStemCount.Caption = "Stem Count";
            this.colStemCount.FieldName = "StemCount";
            this.colStemCount.Name = "colStemCount";
            this.colStemCount.OptionsColumn.AllowFocus = false;
            this.colStemCount.OptionsColumn.ReadOnly = true;
            this.colStemCount.Visible = true;
            this.colStemCount.VisibleIndex = 16;
            this.colStemCount.Width = 77;
            // 
            // colCrownNorth
            // 
            this.colCrownNorth.Caption = "Crown North";
            this.colCrownNorth.FieldName = "CrownNorth";
            this.colCrownNorth.Name = "colCrownNorth";
            this.colCrownNorth.OptionsColumn.AllowFocus = false;
            this.colCrownNorth.OptionsColumn.ReadOnly = true;
            // 
            // colCrownSouth
            // 
            this.colCrownSouth.Caption = "Crown South";
            this.colCrownSouth.FieldName = "CrownSouth";
            this.colCrownSouth.Name = "colCrownSouth";
            this.colCrownSouth.OptionsColumn.AllowFocus = false;
            this.colCrownSouth.OptionsColumn.ReadOnly = true;
            // 
            // colCrownEast
            // 
            this.colCrownEast.Caption = "Crown East";
            this.colCrownEast.FieldName = "CrownEast";
            this.colCrownEast.Name = "colCrownEast";
            this.colCrownEast.OptionsColumn.AllowFocus = false;
            this.colCrownEast.OptionsColumn.ReadOnly = true;
            // 
            // colCrownWest
            // 
            this.colCrownWest.Caption = "Crown West";
            this.colCrownWest.FieldName = "CrownWest";
            this.colCrownWest.Name = "colCrownWest";
            this.colCrownWest.OptionsColumn.AllowFocus = false;
            this.colCrownWest.OptionsColumn.ReadOnly = true;
            // 
            // colRetentionCategory
            // 
            this.colRetentionCategory.Caption = "Retention Category";
            this.colRetentionCategory.FieldName = "RetentionCategory";
            this.colRetentionCategory.Name = "colRetentionCategory";
            this.colRetentionCategory.OptionsColumn.AllowFocus = false;
            this.colRetentionCategory.OptionsColumn.ReadOnly = true;
            this.colRetentionCategory.Visible = true;
            this.colRetentionCategory.VisibleIndex = 17;
            this.colRetentionCategory.Width = 116;
            // 
            // colSULE
            // 
            this.colSULE.Caption = "SULE";
            this.colSULE.FieldName = "SULE";
            this.colSULE.Name = "colSULE";
            this.colSULE.OptionsColumn.AllowFocus = false;
            this.colSULE.OptionsColumn.ReadOnly = true;
            this.colSULE.Visible = true;
            this.colSULE.VisibleIndex = 18;
            this.colSULE.Width = 45;
            // 
            // colUserPicklist1_Tree
            // 
            this.colUserPicklist1_Tree.Caption = "Tree User Picklist 1";
            this.colUserPicklist1_Tree.FieldName = "UserPicklist1_Tree";
            this.colUserPicklist1_Tree.Name = "colUserPicklist1_Tree";
            this.colUserPicklist1_Tree.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1_Tree.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist2_Tree
            // 
            this.colUserPicklist2_Tree.Caption = "Tree User Picklist 2";
            this.colUserPicklist2_Tree.FieldName = "UserPicklist2_Tree";
            this.colUserPicklist2_Tree.Name = "colUserPicklist2_Tree";
            this.colUserPicklist2_Tree.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2_Tree.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist3_Tree
            // 
            this.colUserPicklist3_Tree.Caption = "Tree User Picklist 3";
            this.colUserPicklist3_Tree.FieldName = "UserPicklist3_Tree";
            this.colUserPicklist3_Tree.Name = "colUserPicklist3_Tree";
            this.colUserPicklist3_Tree.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3_Tree.OptionsColumn.ReadOnly = true;
            // 
            // colSpeciesVariety
            // 
            this.colSpeciesVariety.Caption = "Species Variety";
            this.colSpeciesVariety.FieldName = "SpeciesVariety";
            this.colSpeciesVariety.Name = "colSpeciesVariety";
            this.colSpeciesVariety.OptionsColumn.AllowFocus = false;
            this.colSpeciesVariety.OptionsColumn.ReadOnly = true;
            this.colSpeciesVariety.Visible = true;
            this.colSpeciesVariety.VisibleIndex = 15;
            this.colSpeciesVariety.Width = 94;
            // 
            // colObjectLength
            // 
            this.colObjectLength.Caption = "Object Length";
            this.colObjectLength.FieldName = "ObjectLength";
            this.colObjectLength.Name = "colObjectLength";
            this.colObjectLength.OptionsColumn.AllowFocus = false;
            this.colObjectLength.OptionsColumn.ReadOnly = true;
            // 
            // colObjectWidth
            // 
            this.colObjectWidth.Caption = "Object Width";
            this.colObjectWidth.FieldName = "ObjectWidth";
            this.colObjectWidth.Name = "colObjectWidth";
            this.colObjectWidth.OptionsColumn.AllowFocus = false;
            this.colObjectWidth.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist1_Inspection
            // 
            this.colUserPicklist1_Inspection.Caption = "Inspection User Picklist 1";
            this.colUserPicklist1_Inspection.FieldName = "UserPicklist1_Inspection";
            this.colUserPicklist1_Inspection.Name = "colUserPicklist1_Inspection";
            this.colUserPicklist1_Inspection.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1_Inspection.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist2_Inspection
            // 
            this.colUserPicklist2_Inspection.Caption = "Inspection User Picklist 2";
            this.colUserPicklist2_Inspection.FieldName = "UserPicklist2_Inspection";
            this.colUserPicklist2_Inspection.Name = "colUserPicklist2_Inspection";
            this.colUserPicklist2_Inspection.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2_Inspection.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist3_Inspection
            // 
            this.colUserPicklist3_Inspection.Caption = "Inspection User Picklist 3";
            this.colUserPicklist3_Inspection.FieldName = "UserPicklist3_Inspection";
            this.colUserPicklist3_Inspection.Name = "colUserPicklist3_Inspection";
            this.colUserPicklist3_Inspection.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3_Inspection.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist1_Action_1
            // 
            this.colUserPicklist1_Action_1.Caption = "Action 1 User Picklist 1";
            this.colUserPicklist1_Action_1.FieldName = "UserPicklist1_Action_1";
            this.colUserPicklist1_Action_1.Name = "colUserPicklist1_Action_1";
            this.colUserPicklist1_Action_1.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1_Action_1.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist2_Action_1
            // 
            this.colUserPicklist2_Action_1.Caption = "Action 1 User Picklist 2";
            this.colUserPicklist2_Action_1.FieldName = "UserPicklist2_Action_1";
            this.colUserPicklist2_Action_1.Name = "colUserPicklist2_Action_1";
            this.colUserPicklist2_Action_1.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2_Action_1.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist3_Action_1
            // 
            this.colUserPicklist3_Action_1.Caption = "Action 1 User Picklist 3";
            this.colUserPicklist3_Action_1.FieldName = "UserPicklist3_Action_1";
            this.colUserPicklist3_Action_1.Name = "colUserPicklist3_Action_1";
            this.colUserPicklist3_Action_1.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3_Action_1.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist1_Action_2
            // 
            this.colUserPicklist1_Action_2.Caption = "Action 2 User Picklist 1";
            this.colUserPicklist1_Action_2.FieldName = "UserPicklist1_Action_2";
            this.colUserPicklist1_Action_2.Name = "colUserPicklist1_Action_2";
            this.colUserPicklist1_Action_2.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1_Action_2.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist2_Action_2
            // 
            this.colUserPicklist2_Action_2.Caption = "Action 2 User Picklist 2";
            this.colUserPicklist2_Action_2.FieldName = "UserPicklist2_Action_2";
            this.colUserPicklist2_Action_2.Name = "colUserPicklist2_Action_2";
            this.colUserPicklist2_Action_2.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2_Action_2.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist3_Action_2
            // 
            this.colUserPicklist3_Action_2.Caption = "Action 2 User Picklist 3";
            this.colUserPicklist3_Action_2.FieldName = "UserPicklist3_Action_2";
            this.colUserPicklist3_Action_2.Name = "colUserPicklist3_Action_2";
            this.colUserPicklist3_Action_2.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3_Action_2.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist1_Action_3
            // 
            this.colUserPicklist1_Action_3.Caption = "Action 3 User Picklist 1";
            this.colUserPicklist1_Action_3.FieldName = "UserPicklist1_Action_3";
            this.colUserPicklist1_Action_3.Name = "colUserPicklist1_Action_3";
            this.colUserPicklist1_Action_3.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1_Action_3.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist2_Action_3
            // 
            this.colUserPicklist2_Action_3.Caption = "Action 3 User Picklist 2";
            this.colUserPicklist2_Action_3.FieldName = "UserPicklist2_Action_3";
            this.colUserPicklist2_Action_3.Name = "colUserPicklist2_Action_3";
            this.colUserPicklist2_Action_3.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2_Action_3.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist3_Action_3
            // 
            this.colUserPicklist3_Action_3.Caption = "Action 3 User Picklist 3";
            this.colUserPicklist3_Action_3.FieldName = "UserPicklist3_Action_3";
            this.colUserPicklist3_Action_3.Name = "colUserPicklist3_Action_3";
            this.colUserPicklist3_Action_3.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3_Action_3.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist1_Action_4
            // 
            this.colUserPicklist1_Action_4.Caption = "Action 4 User Picklist 1";
            this.colUserPicklist1_Action_4.FieldName = "UserPicklist1_Action_4";
            this.colUserPicklist1_Action_4.Name = "colUserPicklist1_Action_4";
            this.colUserPicklist1_Action_4.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1_Action_4.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist2_Action_4
            // 
            this.colUserPicklist2_Action_4.Caption = "Action 4 User Picklist 2";
            this.colUserPicklist2_Action_4.FieldName = "UserPicklist2_Action_4";
            this.colUserPicklist2_Action_4.Name = "colUserPicklist2_Action_4";
            this.colUserPicklist2_Action_4.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2_Action_4.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist3_Action_4
            // 
            this.colUserPicklist3_Action_4.Caption = "Action 4 User Picklist 3";
            this.colUserPicklist3_Action_4.FieldName = "UserPicklist3_Action_4";
            this.colUserPicklist3_Action_4.Name = "colUserPicklist3_Action_4";
            this.colUserPicklist3_Action_4.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3_Action_4.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist1_Action_5
            // 
            this.colUserPicklist1_Action_5.Caption = "Action 5 User Picklist 1";
            this.colUserPicklist1_Action_5.FieldName = "UserPicklist1_Action_5";
            this.colUserPicklist1_Action_5.Name = "colUserPicklist1_Action_5";
            this.colUserPicklist1_Action_5.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1_Action_5.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist2_Action_5
            // 
            this.colUserPicklist2_Action_5.Caption = "Action 5 User Picklist 2";
            this.colUserPicklist2_Action_5.FieldName = "UserPicklist2_Action_5";
            this.colUserPicklist2_Action_5.Name = "colUserPicklist2_Action_5";
            this.colUserPicklist2_Action_5.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2_Action_5.OptionsColumn.ReadOnly = true;
            // 
            // colUserPicklist3_Action_5
            // 
            this.colUserPicklist3_Action_5.Caption = "Action 5 User Picklist 3";
            this.colUserPicklist3_Action_5.FieldName = "UserPicklist3_Action_5";
            this.colUserPicklist3_Action_5.Name = "colUserPicklist3_Action_5";
            this.colUserPicklist3_Action_5.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3_Action_5.OptionsColumn.ReadOnly = true;
            // 
            // colmonJobWorkUnits_1
            // 
            this.colmonJobWorkUnits_1.Caption = "Action 1 Work Units";
            this.colmonJobWorkUnits_1.ColumnEdit = this.repositoryItemSpinEditWorkUnits;
            this.colmonJobWorkUnits_1.FieldName = "monJobWorkUnits_1";
            this.colmonJobWorkUnits_1.Name = "colmonJobWorkUnits_1";
            this.colmonJobWorkUnits_1.Visible = true;
            this.colmonJobWorkUnits_1.VisibleIndex = 102;
            this.colmonJobWorkUnits_1.Width = 120;
            // 
            // repositoryItemSpinEditWorkUnits
            // 
            this.repositoryItemSpinEditWorkUnits.AutoHeight = false;
            this.repositoryItemSpinEditWorkUnits.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditWorkUnits.Mask.EditMask = "f2";
            this.repositoryItemSpinEditWorkUnits.Name = "repositoryItemSpinEditWorkUnits";
            // 
            // colmonJobWorkUnits_2
            // 
            this.colmonJobWorkUnits_2.Caption = "Action 2 Work Units";
            this.colmonJobWorkUnits_2.ColumnEdit = this.repositoryItemSpinEditWorkUnits;
            this.colmonJobWorkUnits_2.FieldName = "monJobWorkUnits_2";
            this.colmonJobWorkUnits_2.Name = "colmonJobWorkUnits_2";
            this.colmonJobWorkUnits_2.Visible = true;
            this.colmonJobWorkUnits_2.VisibleIndex = 117;
            this.colmonJobWorkUnits_2.Width = 120;
            // 
            // colmonJobWorkUnits_3
            // 
            this.colmonJobWorkUnits_3.Caption = "Action 3 Work Units";
            this.colmonJobWorkUnits_3.ColumnEdit = this.repositoryItemSpinEditWorkUnits;
            this.colmonJobWorkUnits_3.FieldName = "monJobWorkUnits_3";
            this.colmonJobWorkUnits_3.Name = "colmonJobWorkUnits_3";
            this.colmonJobWorkUnits_3.Visible = true;
            this.colmonJobWorkUnits_3.VisibleIndex = 132;
            this.colmonJobWorkUnits_3.Width = 120;
            // 
            // colmonJobWorkUnits_4
            // 
            this.colmonJobWorkUnits_4.Caption = "Action 4 Work Units";
            this.colmonJobWorkUnits_4.ColumnEdit = this.repositoryItemSpinEditWorkUnits;
            this.colmonJobWorkUnits_4.FieldName = "monJobWorkUnits_4";
            this.colmonJobWorkUnits_4.Name = "colmonJobWorkUnits_4";
            this.colmonJobWorkUnits_4.Visible = true;
            this.colmonJobWorkUnits_4.VisibleIndex = 147;
            this.colmonJobWorkUnits_4.Width = 118;
            // 
            // colmonJobWorkUnits_5
            // 
            this.colmonJobWorkUnits_5.Caption = "Action 5 Work Units";
            this.colmonJobWorkUnits_5.ColumnEdit = this.repositoryItemSpinEditWorkUnits;
            this.colmonJobWorkUnits_5.FieldName = "monJobWorkUnits_5";
            this.colmonJobWorkUnits_5.Name = "colmonJobWorkUnits_5";
            this.colmonJobWorkUnits_5.Visible = true;
            this.colmonJobWorkUnits_5.VisibleIndex = 162;
            this.colmonJobWorkUnits_5.Width = 115;
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.btnRollBackImport);
            this.layoutControl4.Controls.Add(this.checkEditClearPriorSelection);
            this.layoutControl4.Controls.Add(this.btnBlockEdit);
            this.layoutControl4.Controls.Add(this.checkEditBlockEditAct5Ref);
            this.layoutControl4.Controls.Add(this.checkEditBlockEditAct4Ref);
            this.layoutControl4.Controls.Add(this.checkEditBlockEditAct3Ref);
            this.layoutControl4.Controls.Add(this.checkEditBlockEditAct2Ref);
            this.layoutControl4.Controls.Add(this.checkEditBlockEditAct1Ref);
            this.layoutControl4.Controls.Add(this.checkEditBlockEditInspRef);
            this.layoutControl4.Controls.Add(this.checkEditBlockEditTreeRef);
            this.layoutControl4.Controls.Add(this.btnShowRecordsAll);
            this.layoutControl4.Controls.Add(this.btnShowRecordsErrorsOnly);
            this.layoutControl4.Controls.Add(this.btnSelectRecords);
            this.layoutControl4.Controls.Add(this.checkEditSelectTreeRef);
            this.layoutControl4.Controls.Add(this.checkEditSelectAct5Ref);
            this.layoutControl4.Controls.Add(this.checkEditSelectInspRef);
            this.layoutControl4.Controls.Add(this.checkEditSelectAct4Ref);
            this.layoutControl4.Controls.Add(this.checkEditSelectAct1Ref);
            this.layoutControl4.Controls.Add(this.checkEditSelectAct3Ref);
            this.layoutControl4.Controls.Add(this.checkEditSelectAct2Ref);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.MenuManager = this.barManager1;
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(199, 598);
            this.layoutControl4.TabIndex = 1;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // btnRollBackImport
            // 
            this.btnRollBackImport.Location = new System.Drawing.Point(16, 639);
            this.btnRollBackImport.Name = "btnRollBackImport";
            this.btnRollBackImport.Size = new System.Drawing.Size(167, 22);
            this.btnRollBackImport.StyleController = this.layoutControl4;
            this.btnRollBackImport.TabIndex = 19;
            this.btnRollBackImport.Text = "Rollback Import";
            this.btnRollBackImport.Click += new System.EventHandler(this.btnRollBackImport_Click);
            // 
            // checkEditClearPriorSelection
            // 
            this.checkEditClearPriorSelection.EditValue = true;
            this.checkEditClearPriorSelection.Location = new System.Drawing.Point(143, 201);
            this.checkEditClearPriorSelection.MenuManager = this.barManager1;
            this.checkEditClearPriorSelection.Name = "checkEditClearPriorSelection";
            this.checkEditClearPriorSelection.Properties.Caption = "";
            this.checkEditClearPriorSelection.Size = new System.Drawing.Size(40, 19);
            this.checkEditClearPriorSelection.StyleController = this.layoutControl4;
            this.checkEditClearPriorSelection.TabIndex = 18;
            // 
            // btnBlockEdit
            // 
            this.btnBlockEdit.Enabled = false;
            this.btnBlockEdit.Location = new System.Drawing.Point(16, 461);
            this.btnBlockEdit.Name = "btnBlockEdit";
            this.btnBlockEdit.Size = new System.Drawing.Size(167, 22);
            this.btnBlockEdit.StyleController = this.layoutControl4;
            this.btnBlockEdit.TabIndex = 17;
            this.btnBlockEdit.Text = "Block Edit";
            this.btnBlockEdit.Click += new System.EventHandler(this.btnBlockEdit_Click);
            // 
            // checkEditBlockEditAct5Ref
            // 
            this.checkEditBlockEditAct5Ref.Location = new System.Drawing.Point(143, 438);
            this.checkEditBlockEditAct5Ref.MenuManager = this.barManager1;
            this.checkEditBlockEditAct5Ref.Name = "checkEditBlockEditAct5Ref";
            this.checkEditBlockEditAct5Ref.Properties.Caption = "";
            this.checkEditBlockEditAct5Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditBlockEditAct5Ref.Properties.RadioGroupIndex = 2;
            this.checkEditBlockEditAct5Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditBlockEditAct5Ref.StyleController = this.layoutControl4;
            this.checkEditBlockEditAct5Ref.TabIndex = 16;
            this.checkEditBlockEditAct5Ref.TabStop = false;
            // 
            // checkEditBlockEditAct4Ref
            // 
            this.checkEditBlockEditAct4Ref.Location = new System.Drawing.Point(143, 415);
            this.checkEditBlockEditAct4Ref.MenuManager = this.barManager1;
            this.checkEditBlockEditAct4Ref.Name = "checkEditBlockEditAct4Ref";
            this.checkEditBlockEditAct4Ref.Properties.Caption = "";
            this.checkEditBlockEditAct4Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditBlockEditAct4Ref.Properties.RadioGroupIndex = 2;
            this.checkEditBlockEditAct4Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditBlockEditAct4Ref.StyleController = this.layoutControl4;
            this.checkEditBlockEditAct4Ref.TabIndex = 15;
            this.checkEditBlockEditAct4Ref.TabStop = false;
            // 
            // checkEditBlockEditAct3Ref
            // 
            this.checkEditBlockEditAct3Ref.Location = new System.Drawing.Point(143, 392);
            this.checkEditBlockEditAct3Ref.MenuManager = this.barManager1;
            this.checkEditBlockEditAct3Ref.Name = "checkEditBlockEditAct3Ref";
            this.checkEditBlockEditAct3Ref.Properties.Caption = "";
            this.checkEditBlockEditAct3Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditBlockEditAct3Ref.Properties.RadioGroupIndex = 2;
            this.checkEditBlockEditAct3Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditBlockEditAct3Ref.StyleController = this.layoutControl4;
            this.checkEditBlockEditAct3Ref.TabIndex = 14;
            this.checkEditBlockEditAct3Ref.TabStop = false;
            // 
            // checkEditBlockEditAct2Ref
            // 
            this.checkEditBlockEditAct2Ref.Location = new System.Drawing.Point(143, 369);
            this.checkEditBlockEditAct2Ref.MenuManager = this.barManager1;
            this.checkEditBlockEditAct2Ref.Name = "checkEditBlockEditAct2Ref";
            this.checkEditBlockEditAct2Ref.Properties.Caption = "";
            this.checkEditBlockEditAct2Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditBlockEditAct2Ref.Properties.RadioGroupIndex = 2;
            this.checkEditBlockEditAct2Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditBlockEditAct2Ref.StyleController = this.layoutControl4;
            this.checkEditBlockEditAct2Ref.TabIndex = 13;
            this.checkEditBlockEditAct2Ref.TabStop = false;
            // 
            // checkEditBlockEditAct1Ref
            // 
            this.checkEditBlockEditAct1Ref.Location = new System.Drawing.Point(143, 346);
            this.checkEditBlockEditAct1Ref.MenuManager = this.barManager1;
            this.checkEditBlockEditAct1Ref.Name = "checkEditBlockEditAct1Ref";
            this.checkEditBlockEditAct1Ref.Properties.Caption = "";
            this.checkEditBlockEditAct1Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditBlockEditAct1Ref.Properties.RadioGroupIndex = 2;
            this.checkEditBlockEditAct1Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditBlockEditAct1Ref.StyleController = this.layoutControl4;
            this.checkEditBlockEditAct1Ref.TabIndex = 12;
            this.checkEditBlockEditAct1Ref.TabStop = false;
            // 
            // checkEditBlockEditInspRef
            // 
            this.checkEditBlockEditInspRef.Location = new System.Drawing.Point(143, 323);
            this.checkEditBlockEditInspRef.MenuManager = this.barManager1;
            this.checkEditBlockEditInspRef.Name = "checkEditBlockEditInspRef";
            this.checkEditBlockEditInspRef.Properties.Caption = "";
            this.checkEditBlockEditInspRef.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditBlockEditInspRef.Properties.RadioGroupIndex = 2;
            this.checkEditBlockEditInspRef.Size = new System.Drawing.Size(40, 19);
            this.checkEditBlockEditInspRef.StyleController = this.layoutControl4;
            this.checkEditBlockEditInspRef.TabIndex = 11;
            this.checkEditBlockEditInspRef.TabStop = false;
            // 
            // checkEditBlockEditTreeRef
            // 
            this.checkEditBlockEditTreeRef.EditValue = true;
            this.checkEditBlockEditTreeRef.Location = new System.Drawing.Point(143, 300);
            this.checkEditBlockEditTreeRef.MenuManager = this.barManager1;
            this.checkEditBlockEditTreeRef.Name = "checkEditBlockEditTreeRef";
            this.checkEditBlockEditTreeRef.Properties.Caption = "";
            this.checkEditBlockEditTreeRef.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditBlockEditTreeRef.Properties.RadioGroupIndex = 2;
            this.checkEditBlockEditTreeRef.Size = new System.Drawing.Size(40, 19);
            this.checkEditBlockEditTreeRef.StyleController = this.layoutControl4;
            this.checkEditBlockEditTreeRef.TabIndex = 10;
            // 
            // btnShowRecordsAll
            // 
            this.btnShowRecordsAll.Location = new System.Drawing.Point(16, 563);
            this.btnShowRecordsAll.Name = "btnShowRecordsAll";
            this.btnShowRecordsAll.Size = new System.Drawing.Size(167, 22);
            this.btnShowRecordsAll.StyleController = this.layoutControl4;
            this.btnShowRecordsAll.TabIndex = 9;
            this.btnShowRecordsAll.Text = "Show All Records";
            this.btnShowRecordsAll.Click += new System.EventHandler(this.btnShowRecordsAll_Click);
            // 
            // btnShowRecordsErrorsOnly
            // 
            this.btnShowRecordsErrorsOnly.Location = new System.Drawing.Point(16, 537);
            this.btnShowRecordsErrorsOnly.Name = "btnShowRecordsErrorsOnly";
            this.btnShowRecordsErrorsOnly.Size = new System.Drawing.Size(167, 22);
            this.btnShowRecordsErrorsOnly.StyleController = this.layoutControl4;
            this.btnShowRecordsErrorsOnly.TabIndex = 8;
            this.btnShowRecordsErrorsOnly.Text = "Only Show Error Records";
            this.btnShowRecordsErrorsOnly.Click += new System.EventHandler(this.btnShowRecordsErrorsOnly_Click);
            // 
            // btnSelectRecords
            // 
            this.btnSelectRecords.Location = new System.Drawing.Point(16, 224);
            this.btnSelectRecords.Name = "btnSelectRecords";
            this.btnSelectRecords.Size = new System.Drawing.Size(167, 22);
            this.btnSelectRecords.StyleController = this.layoutControl4;
            this.btnSelectRecords.TabIndex = 7;
            this.btnSelectRecords.Text = "Select Records";
            this.btnSelectRecords.Click += new System.EventHandler(this.btnSelectRecords_Click);
            // 
            // checkEditSelectTreeRef
            // 
            this.checkEditSelectTreeRef.EditValue = true;
            this.checkEditSelectTreeRef.Location = new System.Drawing.Point(143, 38);
            this.checkEditSelectTreeRef.MenuManager = this.barManager1;
            this.checkEditSelectTreeRef.Name = "checkEditSelectTreeRef";
            this.checkEditSelectTreeRef.Properties.Caption = "";
            this.checkEditSelectTreeRef.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditSelectTreeRef.Properties.RadioGroupIndex = 1;
            this.checkEditSelectTreeRef.Size = new System.Drawing.Size(40, 19);
            this.checkEditSelectTreeRef.StyleController = this.layoutControl4;
            this.checkEditSelectTreeRef.TabIndex = 0;
            this.checkEditSelectTreeRef.CheckedChanged += new System.EventHandler(this.checkEditSelectTreeRef_CheckedChanged);
            // 
            // checkEditSelectAct5Ref
            // 
            this.checkEditSelectAct5Ref.Location = new System.Drawing.Point(143, 176);
            this.checkEditSelectAct5Ref.MenuManager = this.barManager1;
            this.checkEditSelectAct5Ref.Name = "checkEditSelectAct5Ref";
            this.checkEditSelectAct5Ref.Properties.Caption = "";
            this.checkEditSelectAct5Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditSelectAct5Ref.Properties.RadioGroupIndex = 1;
            this.checkEditSelectAct5Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditSelectAct5Ref.StyleController = this.layoutControl4;
            this.checkEditSelectAct5Ref.TabIndex = 6;
            this.checkEditSelectAct5Ref.TabStop = false;
            this.checkEditSelectAct5Ref.CheckedChanged += new System.EventHandler(this.checkEditSelectAct5Ref_CheckedChanged);
            // 
            // checkEditSelectInspRef
            // 
            this.checkEditSelectInspRef.Location = new System.Drawing.Point(143, 61);
            this.checkEditSelectInspRef.MenuManager = this.barManager1;
            this.checkEditSelectInspRef.Name = "checkEditSelectInspRef";
            this.checkEditSelectInspRef.Properties.Caption = "";
            this.checkEditSelectInspRef.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditSelectInspRef.Properties.RadioGroupIndex = 1;
            this.checkEditSelectInspRef.Size = new System.Drawing.Size(40, 19);
            this.checkEditSelectInspRef.StyleController = this.layoutControl4;
            this.checkEditSelectInspRef.TabIndex = 1;
            this.checkEditSelectInspRef.TabStop = false;
            this.checkEditSelectInspRef.CheckedChanged += new System.EventHandler(this.checkEditSelectInspRef_CheckedChanged);
            // 
            // checkEditSelectAct4Ref
            // 
            this.checkEditSelectAct4Ref.Location = new System.Drawing.Point(143, 153);
            this.checkEditSelectAct4Ref.MenuManager = this.barManager1;
            this.checkEditSelectAct4Ref.Name = "checkEditSelectAct4Ref";
            this.checkEditSelectAct4Ref.Properties.Caption = "";
            this.checkEditSelectAct4Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditSelectAct4Ref.Properties.RadioGroupIndex = 1;
            this.checkEditSelectAct4Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditSelectAct4Ref.StyleController = this.layoutControl4;
            this.checkEditSelectAct4Ref.TabIndex = 5;
            this.checkEditSelectAct4Ref.TabStop = false;
            this.checkEditSelectAct4Ref.CheckedChanged += new System.EventHandler(this.checkEditSelectAct4Ref_CheckedChanged);
            // 
            // checkEditSelectAct1Ref
            // 
            this.checkEditSelectAct1Ref.Location = new System.Drawing.Point(143, 84);
            this.checkEditSelectAct1Ref.MenuManager = this.barManager1;
            this.checkEditSelectAct1Ref.Name = "checkEditSelectAct1Ref";
            this.checkEditSelectAct1Ref.Properties.Caption = "";
            this.checkEditSelectAct1Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditSelectAct1Ref.Properties.RadioGroupIndex = 1;
            this.checkEditSelectAct1Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditSelectAct1Ref.StyleController = this.layoutControl4;
            this.checkEditSelectAct1Ref.TabIndex = 2;
            this.checkEditSelectAct1Ref.TabStop = false;
            this.checkEditSelectAct1Ref.CheckedChanged += new System.EventHandler(this.checkEditSelectAct1Ref_CheckedChanged);
            // 
            // checkEditSelectAct3Ref
            // 
            this.checkEditSelectAct3Ref.Location = new System.Drawing.Point(143, 130);
            this.checkEditSelectAct3Ref.MenuManager = this.barManager1;
            this.checkEditSelectAct3Ref.Name = "checkEditSelectAct3Ref";
            this.checkEditSelectAct3Ref.Properties.Caption = "";
            this.checkEditSelectAct3Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditSelectAct3Ref.Properties.RadioGroupIndex = 1;
            this.checkEditSelectAct3Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditSelectAct3Ref.StyleController = this.layoutControl4;
            this.checkEditSelectAct3Ref.TabIndex = 4;
            this.checkEditSelectAct3Ref.TabStop = false;
            this.checkEditSelectAct3Ref.CheckedChanged += new System.EventHandler(this.checkEditSelectAct3Ref_CheckedChanged);
            // 
            // checkEditSelectAct2Ref
            // 
            this.checkEditSelectAct2Ref.Location = new System.Drawing.Point(143, 107);
            this.checkEditSelectAct2Ref.MenuManager = this.barManager1;
            this.checkEditSelectAct2Ref.Name = "checkEditSelectAct2Ref";
            this.checkEditSelectAct2Ref.Properties.Caption = "";
            this.checkEditSelectAct2Ref.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditSelectAct2Ref.Properties.RadioGroupIndex = 1;
            this.checkEditSelectAct2Ref.Size = new System.Drawing.Size(40, 19);
            this.checkEditSelectAct2Ref.StyleController = this.layoutControl4;
            this.checkEditSelectAct2Ref.TabIndex = 3;
            this.checkEditSelectAct2Ref.TabStop = false;
            this.checkEditSelectAct2Ref.CheckedChanged += new System.EventHandler(this.checkEditSelectAct2Ref_CheckedChanged);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Root";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.emptySpaceItem2,
            this.layoutControlGroup7,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.layoutControlGroup8,
            this.emptySpaceItem5});
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup4.Size = new System.Drawing.Size(199, 687);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Select Records - Missing...";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem10,
            this.layoutControlItem28,
            this.simpleSeparator1});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup5.Size = new System.Drawing.Size(187, 250);
            this.layoutControlGroup5.Text = "Select Records - Missing:";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AllowHtmlStringInCaption = true;
            this.layoutControlItem11.Control = this.checkEditSelectInspRef;
            this.layoutControlItem11.CustomizationFormText = "Inspection References:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem11.Text = "Inspection References:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AllowHtmlStringInCaption = true;
            this.layoutControlItem12.Control = this.checkEditSelectAct1Ref;
            this.layoutControlItem12.CustomizationFormText = "Action 1 Job References:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem12.Text = "Action 1 Job References:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AllowHtmlStringInCaption = true;
            this.layoutControlItem13.Control = this.checkEditSelectAct2Ref;
            this.layoutControlItem13.CustomizationFormText = "Action 2 Job References:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 69);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem13.Text = "Action 2 Job References:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AllowHtmlStringInCaption = true;
            this.layoutControlItem14.Control = this.checkEditSelectAct3Ref;
            this.layoutControlItem14.CustomizationFormText = "Action 3 Job References:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 92);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem14.Text = "Action 3 Job References:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AllowHtmlStringInCaption = true;
            this.layoutControlItem15.Control = this.checkEditSelectAct4Ref;
            this.layoutControlItem15.CustomizationFormText = "Action 4 Job References:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 115);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem15.Text = "Action 4 Job References:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AllowHtmlStringInCaption = true;
            this.layoutControlItem16.Control = this.checkEditSelectAct5Ref;
            this.layoutControlItem16.CustomizationFormText = "Action 5 Job References:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 138);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem16.Text = "Action 5 Job References:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.btnSelectRecords;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 186);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(171, 26);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AllowHtmlStringInCaption = true;
            this.layoutControlItem10.Control = this.checkEditSelectTreeRef;
            this.layoutControlItem10.CustomizationFormText = "Tree References:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem10.Text = "Tree References:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AllowHtmlStringInCaption = true;
            this.layoutControlItem28.Control = this.checkEditClearPriorSelection;
            this.layoutControlItem28.CustomizationFormText = "Clear Prior Selection First:";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 163);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem28.Text = "Clear Prior Selection First:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(124, 13);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 161);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(171, 2);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Block Edit - Selected...";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 262);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup6.Size = new System.Drawing.Size(187, 225);
            this.layoutControlGroup6.Text = "Block Edit - Selected:";
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AllowHtmlStringInCaption = true;
            this.layoutControlItem20.Control = this.checkEditBlockEditTreeRef;
            this.layoutControlItem20.CustomizationFormText = "Tree References:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem20.Text = "Tree References:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AllowHtmlStringInCaption = true;
            this.layoutControlItem21.Control = this.checkEditBlockEditInspRef;
            this.layoutControlItem21.CustomizationFormText = "Inspection References:";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem21.Text = "Inspection References:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AllowHtmlStringInCaption = true;
            this.layoutControlItem22.Control = this.checkEditBlockEditAct1Ref;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem22.Text = "Action 1 Job References:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AllowHtmlStringInCaption = true;
            this.layoutControlItem23.Control = this.checkEditBlockEditAct2Ref;
            this.layoutControlItem23.CustomizationFormText = "Action 2 Job References:";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 69);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem23.Text = "Action 2 Job References:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AllowHtmlStringInCaption = true;
            this.layoutControlItem24.Control = this.checkEditBlockEditAct3Ref;
            this.layoutControlItem24.CustomizationFormText = "Action 3 Job References:";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 92);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem24.Text = "Action 3 Job References:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AllowHtmlStringInCaption = true;
            this.layoutControlItem25.Control = this.checkEditBlockEditAct4Ref;
            this.layoutControlItem25.CustomizationFormText = "Action 4 Job References:";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 115);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem25.Text = "Action 4 Job References:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AllowHtmlStringInCaption = true;
            this.layoutControlItem26.Control = this.checkEditBlockEditAct5Ref;
            this.layoutControlItem26.CustomizationFormText = "Action 5 Job References:";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 138);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(171, 23);
            this.layoutControlItem26.Text = "Action 5 Job References:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.btnBlockEdit;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 161);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(171, 26);
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 487);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(187, 12);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Filter Grid";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18,
            this.layoutControlItem19});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 499);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup7.Size = new System.Drawing.Size(187, 90);
            this.layoutControlGroup7.Text = "Filter Grid";
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.btnShowRecordsErrorsOnly;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(171, 26);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.btnShowRecordsAll;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(171, 26);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 250);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(187, 12);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 665);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(187, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Utilities";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem29});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 601);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup8.Size = new System.Drawing.Size(187, 64);
            this.layoutControlGroup8.Text = "Utilities";
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.btnRollBackImport;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(171, 26);
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 589);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(187, 12);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btnImport
            // 
            this.btnImport.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnImport.Appearance.Options.UseFont = true;
            this.btnImport.Enabled = false;
            this.btnImport.Location = new System.Drawing.Point(973, 8);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(110, 22);
            this.btnImport.StyleController = this.layoutControl3;
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem8.Text = "Import Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to import the contents of the grid into the database.";
            superToolTip7.Items.Add(toolTipTitleItem8);
            superToolTip7.Items.Add(toolTipItem7);
            this.btnImport.SuperTip = superToolTip7;
            this.btnImport.TabIndex = 5;
            this.btnImport.Text = "Import Data";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // ImportFileButtonEdit
            // 
            this.ImportFileButtonEdit.Location = new System.Drawing.Point(103, 8);
            this.ImportFileButtonEdit.MenuManager = this.barManager1;
            this.ImportFileButtonEdit.Name = "ImportFileButtonEdit";
            editorButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions5.Image")));
            editorButtonImageOptions5.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem9.Text = "Load File - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to open the Windows File Selection screen then load the selected Survey " +
    "Results file.";
            superToolTip8.Items.Add(toolTipTitleItem9);
            superToolTip8.Items.Add(toolTipItem8);
            this.ImportFileButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Load File", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, superToolTip8, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ImportFileButtonEdit.Properties.NullValuePrompt = "Click Load File";
            this.ImportFileButtonEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.ImportFileButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ImportFileButtonEdit.Size = new System.Drawing.Size(753, 22);
            this.ImportFileButtonEdit.StyleController = this.layoutControl3;
            this.ImportFileButtonEdit.TabIndex = 4;
            this.ImportFileButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ImportFileButtonEdit_ButtonClick);
            // 
            // btnCheckData
            // 
            this.btnCheckData.Enabled = false;
            this.btnCheckData.Location = new System.Drawing.Point(860, 8);
            this.btnCheckData.Name = "btnCheckData";
            this.btnCheckData.Size = new System.Drawing.Size(109, 22);
            this.btnCheckData.StyleController = this.layoutControl3;
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image12")));
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image13")));
            toolTipTitleItem10.Text = "Check Data Button - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to check the contents of the data grid.\r\n\r\nAny <b>errors</b> found will " +
    "be <b>highlighted</b> with a <b>Warning icon</b>.";
            superToolTip9.Items.Add(toolTipTitleItem10);
            superToolTip9.Items.Add(toolTipItem9);
            this.btnCheckData.SuperTip = superToolTip9;
            this.btnCheckData.TabIndex = 0;
            this.btnCheckData.Text = "Check Data";
            this.btnCheckData.Click += new System.EventHandler(this.btnCheckData_Click);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem1,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1091, 640);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AllowHtmlStringInCaption = true;
            this.layoutControlItem5.Control = this.ImportFileButtonEdit;
            this.layoutControlItem5.CustomizationFormText = "Import Survey File:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(852, 26);
            this.layoutControlItem5.Text = "Import Survey File:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnCheckData;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(852, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(113, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnImport;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(965, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(114, 26);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.splitContainerControl5;
            this.layoutControlItem9.CustomizationFormText = "Import Grid:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(1079, 602);
            this.layoutControlItem9.Text = "Import Grid:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // sp01323_AT_import_from_GBM_workorder_checkingBindingSource
            // 
            this.sp01323_AT_import_from_GBM_workorder_checkingBindingSource.DataMember = "sp01323_AT_import_from_GBM_workorder_checking";
            this.sp01323_AT_import_from_GBM_workorder_checkingBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // sp00098_export_to_GBM_field_listTableAdapter
            // 
            this.sp00098_export_to_GBM_field_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00099_export_to_GBM_default_group_listTableAdapter
            // 
            this.sp00099_export_to_GBM_default_group_listTableAdapter.ClearBeforeFill = true;
            // 
            // bbiCheckedx
            // 
            this.bbiCheckedx.Caption = "Set Checked";
            this.bbiCheckedx.Id = 23;
            this.bbiCheckedx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCheckedx.ImageOptions.Image")));
            this.bbiCheckedx.Name = "bbiCheckedx";
            this.bbiCheckedx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChecked_ItemClick);
            // 
            // bbiUncheckedx
            // 
            this.bbiUncheckedx.Caption = "Set Unchecked";
            this.bbiUncheckedx.Id = 24;
            this.bbiUncheckedx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUncheckedx.ImageOptions.Image")));
            this.bbiUncheckedx.Name = "bbiUncheckedx";
            this.bbiUncheckedx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUnchecked_ItemClick);
            // 
            // bbiSetGroup
            // 
            this.bbiSetGroup.Caption = "Set Group";
            this.bbiSetGroup.Id = 25;
            this.bbiSetGroup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSetGroup.ImageOptions.Image")));
            this.bbiSetGroup.Name = "bbiSetGroup";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "TEST";
            this.barStaticItem1.Id = 23;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barLinkContainerItem2
            // 
            this.barLinkContainerItem2.Caption = "test";
            this.barLinkContainerItem2.Id = 23;
            this.barLinkContainerItem2.Name = "barLinkContainerItem2";
            // 
            // barListItem1
            // 
            this.barListItem1.Caption = "test";
            this.barListItem1.Id = 24;
            this.barListItem1.Name = "barListItem1";
            // 
            // sp00100_export_to_GBM_locality_filter_listTableAdapter
            // 
            this.sp00100_export_to_GBM_locality_filter_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00101_export_to_GBM_map_centre_locality_listTableAdapter
            // 
            this.sp00101_export_to_GBM_map_centre_locality_listTableAdapter.ClearBeforeFill = true;
            // 
            // bliSetGroup
            // 
            this.bliSetGroup.Caption = "Set Group";
            this.bliSetGroup.Id = 23;
            this.bliSetGroup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bliSetGroup.ImageOptions.Image")));
            this.bliSetGroup.Name = "bliSetGroup";
            // 
            // beiSetGroup
            // 
            this.beiSetGroup.Caption = "Set Group";
            this.beiSetGroup.Edit = this.repositoryItemGridLookUpEdit2;
            this.beiSetGroup.Id = 24;
            this.beiSetGroup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("beiSetGroup.ImageOptions.Image")));
            this.beiSetGroup.Name = "beiSetGroup";
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DataSource = this.sp00099exporttoGBMdefaultgrouplistBindingSource;
            this.repositoryItemGridLookUpEdit2.DisplayMember = "GroupName";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.PopupView = this.repositoryItemGridLookUpEdit2View;
            this.repositoryItemGridLookUpEdit2.ValueMember = "GroupName";
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGroupName3,
            this.colGroupOrder2});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsBehavior.Editable = false;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowIndicator = false;
            // 
            // colGroupName3
            // 
            this.colGroupName3.Caption = "Group";
            this.colGroupName3.FieldName = "GroupName";
            this.colGroupName3.Name = "colGroupName3";
            this.colGroupName3.OptionsColumn.AllowEdit = false;
            this.colGroupName3.OptionsColumn.ReadOnly = true;
            this.colGroupName3.Visible = true;
            this.colGroupName3.VisibleIndex = 0;
            // 
            // colGroupOrder2
            // 
            this.colGroupOrder2.Caption = "Order";
            this.colGroupOrder2.FieldName = "GroupOrder";
            this.colGroupOrder2.Name = "colGroupOrder2";
            this.colGroupOrder2.OptionsColumn.AllowEdit = false;
            this.colGroupOrder2.OptionsColumn.ReadOnly = true;
            // 
            // sp00102_export_to_GBM_map_scale_listTableAdapter
            // 
            this.sp00102_export_to_GBM_map_scale_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00103_export_to_GBM_picklist_itemsTableAdapter
            // 
            this.sp00103_export_to_GBM_picklist_itemsTableAdapter.ClearBeforeFill = true;
            // 
            // pmGrid3Menu
            // 
            this.pmGrid3Menu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiChecked, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUnchecked),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiGroup),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveTemplate, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveTemplateAs)});
            this.pmGrid3Menu.Manager = this.barManager1;
            this.pmGrid3Menu.MenuCaption = "Export Items Menu";
            this.pmGrid3Menu.Name = "pmGrid3Menu";
            this.pmGrid3Menu.ShowCaption = true;
            // 
            // bbiChecked
            // 
            this.bbiChecked.Caption = "Set Checked";
            this.bbiChecked.Id = 23;
            this.bbiChecked.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiChecked.ImageOptions.Image")));
            this.bbiChecked.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiChecked.ImageOptions.LargeImage")));
            this.bbiChecked.Name = "bbiChecked";
            this.bbiChecked.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChecked_ItemClick);
            // 
            // bbiUnchecked
            // 
            this.bbiUnchecked.Caption = "Set Unchecked";
            this.bbiUnchecked.Id = 24;
            this.bbiUnchecked.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUnchecked.ImageOptions.Image")));
            this.bbiUnchecked.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiUnchecked.ImageOptions.LargeImage")));
            this.bbiUnchecked.Name = "bbiUnchecked";
            this.bbiUnchecked.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUnchecked_ItemClick);
            // 
            // bsiGroup
            // 
            this.bsiGroup.Caption = "Set Group...";
            this.bsiGroup.Id = 25;
            this.bsiGroup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiGroup.ImageOptions.Image")));
            this.bsiGroup.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiGroup.ImageOptions.LargeImage")));
            this.bsiGroup.Name = "bsiGroup";
            // 
            // bbiSaveTemplate
            // 
            this.bbiSaveTemplate.Caption = "Save Template";
            this.bbiSaveTemplate.Id = 26;
            this.bbiSaveTemplate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSaveTemplate.ImageOptions.Image")));
            this.bbiSaveTemplate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiSaveTemplate.ImageOptions.LargeImage")));
            this.bbiSaveTemplate.Name = "bbiSaveTemplate";
            this.bbiSaveTemplate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveTemplate_ItemClick);
            // 
            // bbiSaveTemplateAs
            // 
            this.bbiSaveTemplateAs.Caption = "Save Template As...";
            this.bbiSaveTemplateAs.Id = 27;
            this.bbiSaveTemplateAs.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSaveTemplateAs.ImageOptions.Image")));
            this.bbiSaveTemplateAs.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiSaveTemplateAs.ImageOptions.LargeImage")));
            this.bbiSaveTemplateAs.Name = "bbiSaveTemplateAs";
            this.bbiSaveTemplateAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveTemplateAs_ItemClick);
            // 
            // pmGrid1Menu
            // 
            this.pmGrid1Menu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLoadTemplate),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditTemplate),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteTemplate, true)});
            this.pmGrid1Menu.Manager = this.barManager1;
            this.pmGrid1Menu.MenuCaption = "Saved Templates Menu";
            this.pmGrid1Menu.Name = "pmGrid1Menu";
            this.pmGrid1Menu.ShowCaption = true;
            // 
            // bbiLoadTemplate
            // 
            this.bbiLoadTemplate.Caption = "Load Template";
            this.bbiLoadTemplate.Id = 23;
            this.bbiLoadTemplate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLoadTemplate.ImageOptions.Image")));
            this.bbiLoadTemplate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiLoadTemplate.ImageOptions.LargeImage")));
            this.bbiLoadTemplate.Name = "bbiLoadTemplate";
            this.bbiLoadTemplate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadTemplate_ItemClick);
            // 
            // bbiEditTemplate
            // 
            this.bbiEditTemplate.Caption = "Edit Template...";
            this.bbiEditTemplate.Id = 24;
            this.bbiEditTemplate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEditTemplate.ImageOptions.Image")));
            this.bbiEditTemplate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiEditTemplate.ImageOptions.LargeImage")));
            this.bbiEditTemplate.Name = "bbiEditTemplate";
            this.bbiEditTemplate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditTemplate_ItemClick);
            // 
            // bbiDeleteTemplate
            // 
            this.bbiDeleteTemplate.Caption = "Delete Template";
            this.bbiDeleteTemplate.Id = 25;
            this.bbiDeleteTemplate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDeleteTemplate.ImageOptions.Image")));
            this.bbiDeleteTemplate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDeleteTemplate.ImageOptions.LargeImage")));
            this.bbiDeleteTemplate.Name = "bbiDeleteTemplate";
            this.bbiDeleteTemplate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteTemplate_ItemClick);
            // 
            // sp00112_export_to_GBM_saved_templates_listTableAdapter
            // 
            this.sp00112_export_to_GBM_saved_templates_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00131_export_to_GBM_style_fieldsTableAdapter
            // 
            this.sp00131_export_to_GBM_style_fieldsTableAdapter.ClearBeforeFill = true;
            // 
            // pmGrid6Menu
            // 
            this.pmGrid6Menu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEditStyles),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSaveTemplate, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveTemplateAs)});
            this.pmGrid6Menu.Manager = this.barManager1;
            this.pmGrid6Menu.MenuCaption = "Styles Menu";
            this.pmGrid6Menu.Name = "pmGrid6Menu";
            this.pmGrid6Menu.ShowCaption = true;
            // 
            // bbiBlockEditStyles
            // 
            this.bbiBlockEditStyles.Caption = "Block Edit Styles";
            this.bbiBlockEditStyles.Id = 25;
            this.bbiBlockEditStyles.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditStyles.ImageOptions.Image")));
            this.bbiBlockEditStyles.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditStyles.ImageOptions.LargeImage")));
            this.bbiBlockEditStyles.Name = "bbiBlockEditStyles";
            this.bbiBlockEditStyles.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockEditStyles_ItemClick);
            // 
            // sp00134_export_to_GBM_export_dataTableAdapter
            // 
            this.sp00134_export_to_GBM_export_dataTableAdapter.ClearBeforeFill = true;
            // 
            // sp00135_export_to_GBM_map_background_filesTableAdapter
            // 
            this.sp00135_export_to_GBM_map_background_filesTableAdapter.ClearBeforeFill = true;
            // 
            // sp00136_import_from_GBM_locality_species_list_with_blankTableAdapter
            // 
            this.sp00136_import_from_GBM_locality_species_list_with_blankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00136importfromGBMlocalityspecieslistwithblankBindingSource1
            // 
            this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource1.DataMember = "sp00136_import_from_GBM_locality_species_list_with_blank";
            this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource1.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // sp00137_import_from_GBM_ltree_status_list_with_blankTableAdapter
            // 
            this.sp00137_import_from_GBM_ltree_status_list_with_blankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00138_import_from_GBM_districts_list_with_blankTableAdapter
            // 
            this.sp00138_import_from_GBM_districts_list_with_blankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00139_import_from_GBM_locailities_list_with_blankTableAdapter
            // 
            this.sp00139_import_from_GBM_locailities_list_with_blankTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            this.dxErrorProvider1.DataSource = this.sp00134exporttoGBMexportdataBindingSource;
            // 
            // sp01321_AT_export_to_GBM_available_workordersTableAdapter
            // 
            this.sp01321_AT_export_to_GBM_available_workordersTableAdapter.ClearBeforeFill = true;
            // 
            // sp01323_AT_import_from_GBM_workorder_checkingTableAdapter
            // 
            this.sp01323_AT_import_from_GBM_workorder_checkingTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AT_Data_Transfer_GBM_Mobile
            // 
            this.ClientSize = new System.Drawing.Size(1100, 756);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Data_Transfer_GBM_Mobile";
            this.Text = "Amenity Trees - GBM Mobile Data Interchange";
            this.Activated += new System.EventHandler(this.frm_AT_Data_Transfer_GBM_Mobile_Activated);
            this.Load += new System.EventHandler(this.frm_AT_Data_Transfer_GBM_Mobile_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00112exporttoGBMsavedtemplateslistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataTransfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00099exporttoGBMdefaultgrouplistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl_WorkOrders)).EndInit();
            this.popupContainerControl_WorkOrders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditWorkOrderCheckingIncludeCompleted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWorkOrderCheckingToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWorkOrderCheckingToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWorkOrderCheckingFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWorkOrderCheckingFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01321ATexporttoGBMavailableworkordersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl3)).EndInit();
            this.popupContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00135exporttoGBMmapbackgroundfilesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00102exporttoGBMmapscalelistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetATDataTransferBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00101exporttoGBMmapcentrelocalitylistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).EndInit();
            this.gridSplitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00100exporttoGBMlocalityfilterlistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditExportType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeLastInspectionDetailsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00103exporttoGBMpicklistitemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetATDataTransferBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00098exporttoGBMfieldlistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).EndInit();
            this.gridSplitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00131exporttoGBMstylefieldsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.glueStyleField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit3View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).EndInit();
            this.splitContainerControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditImportWorkOrderChecking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).EndInit();
            this.gridSplitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00134exporttoGBMexportdataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDistrict)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00138importfromGBMdistrictslistwithblankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditLocality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00139importfromGBMlocailitieslistwithblankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditSequenceField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCoordinates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditPolygonXY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00137importfromGBMltreestatuslistwithblankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSpecies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditWorkUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClearPriorSelection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct5Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct4Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct3Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct2Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditAct1Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditInspRef.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlockEditTreeRef.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectTreeRef.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct5Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectInspRef.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct4Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct1Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct3Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSelectAct2Ref.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportFileButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01323_AT_import_from_GBM_workorder_checkingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmGrid3Menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmGrid1Menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmGrid6Menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00136importfromGBMlocalityspecieslistwithblankBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SimpleButton btnExport;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DataSet_AT_DataTransfer dataSet_AT_DataTransfer;
        private System.Windows.Forms.BindingSource sp00098exporttoGBMfieldlistBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00098_export_to_GBM_field_listTableAdapter sp00098_export_to_GBM_field_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colColumnID;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn colColumnLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colColumnType;
        private DevExpress.XtraGrid.Columns.GridColumn colColumnLength;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn colTableName;
        private DevExpress.XtraGrid.Columns.GridColumn colColumnOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colPickList;
        private DevExpress.XtraGrid.Columns.GridColumn colPicklistSummery;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp00099exporttoGBMdefaultgrouplistBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00099_export_to_GBM_default_group_listTableAdapter sp00099_export_to_GBM_default_group_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupName1;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit2View;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraBars.BarButtonItem bbiCheckedx;
        private DevExpress.XtraBars.BarButtonItem bbiUncheckedx;
        private DevExpress.XtraBars.BarButtonItem bbiSetGroup;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem2;
        private DevExpress.XtraBars.BarListItem barListItem1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SimpleButton btnLocalityFilterOK;
        private System.Windows.Forms.BindingSource sp00100exporttoGBMlocalityfilterlistBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00100_export_to_GBM_locality_filter_listTableAdapter sp00100_export_to_GBM_locality_filter_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityID;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityCode;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityName;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictID;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictName;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityOwnershipCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictOwnershipCode;
        private System.Windows.Forms.BindingSource sp00101exporttoGBMmapcentrelocalitylistBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00101_export_to_GBM_map_centre_locality_listTableAdapter sp00101_export_to_GBM_map_centre_locality_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colXcoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYcoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraBars.BarListItem bliSetGroup;
        private DevExpress.XtraBars.BarEditItem beiSetGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupName3;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupOrder2;
        private DevExpress.XtraBars.BarSubItem bsiGroup;
        private System.Windows.Forms.BindingSource dataSetATDataTransferBindingSource;
        private System.Windows.Forms.BindingSource sp00102exporttoGBMmapscalelistBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00102_export_to_GBM_map_scale_listTableAdapter sp00102_export_to_GBM_map_scale_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colScaleDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colScale;
        private DevExpress.XtraGrid.Columns.GridColumn colRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupOrder3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private System.Windows.Forms.BindingSource dataSetATDataTransferBindingSource1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.SimpleButton btnFilterFilterOK;
        private System.Windows.Forms.BindingSource sp00103exporttoGBMpicklistitemsBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00103_export_to_GBM_picklist_itemsTableAdapter sp00103_export_to_GBM_picklist_itemsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCode;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraBars.PopupMenu pmGrid3Menu;
        private DevExpress.XtraBars.BarButtonItem bbiChecked;
        private DevExpress.XtraBars.BarButtonItem bbiSaveTemplate;
        private DevExpress.XtraBars.BarButtonItem bbiSaveTemplateAs;
        private DevExpress.XtraBars.BarButtonItem bbiUnchecked;
        private DevExpress.XtraBars.PopupMenu pmGrid1Menu;
        private DevExpress.XtraBars.BarButtonItem bbiLoadTemplate;
        private DevExpress.XtraBars.BarButtonItem bbiEditTemplate;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteTemplate;
        private System.Windows.Forms.BindingSource sp00112exporttoGBMsavedtemplateslistBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTemplateType;
        private DevExpress.XtraGrid.Columns.GridColumn colTemplateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTemplateRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTemplateID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colShareType;
        private DevExpress.XtraGrid.Columns.GridColumn colColour;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00112_export_to_GBM_saved_templates_listTableAdapter sp00112_export_to_GBM_saved_templates_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colEditable;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSearchable;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colManditory;
        private DevExpress.XtraGrid.Columns.GridColumn colUseLastRecord;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colUseLastRecordAvailable;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private System.Windows.Forms.BindingSource sp00131exporttoGBMstylefieldsBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00131_export_to_GBM_style_fieldsTableAdapter sp00131_export_to_GBM_style_fieldsTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn colSymbol;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colSymbolColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonFillColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonLineColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit3;
        private DevExpress.XtraEditors.GridLookUpEdit glueStyleField;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit3View;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarButtonItem bbiBlockEditStyles;
        private DevExpress.XtraBars.PopupMenu pmGrid6Menu;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonFillPattern;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonFillPatternColour;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonLineWidth;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private System.Windows.Forms.ImageList imageList3;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private System.Windows.Forms.ImageList imageList4;
        private DevExpress.XtraEditors.SimpleButton btnCheckData;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private System.Windows.Forms.BindingSource sp00134exporttoGBMexportdataBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colintDistrictID;
        private DevExpress.XtraGrid.Columns.GridColumn colintLocalityID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrTreeRef;
        private DevExpress.XtraGrid.Columns.GridColumn colintX;
        private DevExpress.XtraGrid.Columns.GridColumn colintY;
        private DevExpress.XtraGrid.Columns.GridColumn colstrPolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colintStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colintAgeClass;
        private DevExpress.XtraGrid.Columns.GridColumn colintDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colstrHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colintSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colintAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colintVisibility;
        private DevExpress.XtraGrid.Columns.GridColumn colstrContext;
        private DevExpress.XtraGrid.Columns.GridColumn colstrManagement;
        private DevExpress.XtraGrid.Columns.GridColumn colintLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn coldtLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colintInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colstrInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn coldtNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colintGroundType;
        private DevExpress.XtraGrid.Columns.GridColumn coldecHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colintHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colintDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colintDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colintProtectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownDepth;
        private DevExpress.XtraGrid.Columns.GridColumn coldtPlantDate;
        private DevExpress.XtraGrid.Columns.GridColumn colintGroupNo;
        private DevExpress.XtraGrid.Columns.GridColumn colintSafetyPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colintSiteHazardClass;
        private DevExpress.XtraGrid.Columns.GridColumn colintPlantSize;
        private DevExpress.XtraGrid.Columns.GridColumn colintPlantSource;
        private DevExpress.XtraGrid.Columns.GridColumn colintPlantMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colstrPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMapLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colintSize;
        private DevExpress.XtraGrid.Columns.GridColumn colintNearbyObject;
        private DevExpress.XtraGrid.Columns.GridColumn colintNearbyObject2;
        private DevExpress.XtraGrid.Columns.GridColumn colintNearbyObject3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks_Tree;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser1_Tree;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser2_Tree;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser3_Tree;
        private DevExpress.XtraGrid.Columns.GridColumn coldecAreaHa;
        private DevExpress.XtraGrid.Columns.GridColumn colintType;
        private DevExpress.XtraGrid.Columns.GridColumn colstrHedgeOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colintLength;
        private DevExpress.XtraGrid.Columns.GridColumn coldecGardenSize;
        private DevExpress.XtraGrid.Columns.GridColumn coldecPropertyProximity;
        private DevExpress.XtraGrid.Columns.GridColumn coldecSiteLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colintReplantCount;
        private DevExpress.XtraGrid.Columns.GridColumn coldtSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colintTreeOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colstrInspectRef;
        private DevExpress.XtraGrid.Columns.GridColumn colintInspector;
        private DevExpress.XtraGrid.Columns.GridColumn coldtInspectDate;
        private DevExpress.XtraGrid.Columns.GridColumn colintIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colintStemPhysical;
        private DevExpress.XtraGrid.Columns.GridColumn colintStemPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colintStemPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colintStemDisease;
        private DevExpress.XtraGrid.Columns.GridColumn colintStemDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colintStemDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colintAngleToVertical;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownPhysical;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownDisease;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownFoliation;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownFoliation2;
        private DevExpress.XtraGrid.Columns.GridColumn colintCrownFoliation3;
        private DevExpress.XtraGrid.Columns.GridColumn colintSafety;
        private DevExpress.XtraGrid.Columns.GridColumn colintGeneralCondition;
        private DevExpress.XtraGrid.Columns.GridColumn colintRootHeave;
        private DevExpress.XtraGrid.Columns.GridColumn colintRootHeave2;
        private DevExpress.XtraGrid.Columns.GridColumn colintRootHeave3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser1_Inspection;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser2_Inspection;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser3_Inspection;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks_Inspection;
        private DevExpress.XtraGrid.Columns.GridColumn colintVitality;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDueDate_1;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDoneDate_1;
        private DevExpress.XtraGrid.Columns.GridColumn colintAction_1;
        private DevExpress.XtraGrid.Columns.GridColumn colintActionBy_1;
        private DevExpress.XtraGrid.Columns.GridColumn colintSupervisor_1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobNumber_1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser1_Action_1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser2_Action_1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser3_Action_1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks_Action_1;
        private DevExpress.XtraGrid.Columns.GridColumn colintWorkOrderID_1;
        private DevExpress.XtraGrid.Columns.GridColumn colintPriorityID_1;
        private DevExpress.XtraGrid.Columns.GridColumn colintScheduleOfRates_1;
        private DevExpress.XtraGrid.Columns.GridColumn colintOwnership_1;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDueDate_2;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDoneDate_2;
        private DevExpress.XtraGrid.Columns.GridColumn colintAction_2;
        private DevExpress.XtraGrid.Columns.GridColumn colintActionBy_2;
        private DevExpress.XtraGrid.Columns.GridColumn colintSupervisor_2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobNumber_2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser1_Action_2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser2_Action_2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser3_Action_2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks_Action_2;
        private DevExpress.XtraGrid.Columns.GridColumn colintWorkOrderID_2;
        private DevExpress.XtraGrid.Columns.GridColumn colintPriorityID_2;
        private DevExpress.XtraGrid.Columns.GridColumn colintScheduleOfRates_2;
        private DevExpress.XtraGrid.Columns.GridColumn colintOwnership_2;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDueDate_3;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDoneDate_3;
        private DevExpress.XtraGrid.Columns.GridColumn colintAction_3;
        private DevExpress.XtraGrid.Columns.GridColumn colintActionBy_3;
        private DevExpress.XtraGrid.Columns.GridColumn colintSupervisor_3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobNumber_3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser1_Action_3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser2_Action_3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser3_Action_3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks_Action_3;
        private DevExpress.XtraGrid.Columns.GridColumn colintWorkOrderID_3;
        private DevExpress.XtraGrid.Columns.GridColumn colintPriorityID_3;
        private DevExpress.XtraGrid.Columns.GridColumn colintScheduleOfRates_3;
        private DevExpress.XtraGrid.Columns.GridColumn colintOwnership_3;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDueDate_4;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDoneDate_4;
        private DevExpress.XtraGrid.Columns.GridColumn colintAction_4;
        private DevExpress.XtraGrid.Columns.GridColumn colintActionBy_4;
        private DevExpress.XtraGrid.Columns.GridColumn colintSupervisor_4;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobNumber_4;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser1_Action_4;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser2_Action_4;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser3_Action_4;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks_Action_4;
        private DevExpress.XtraGrid.Columns.GridColumn colintWorkOrderID_4;
        private DevExpress.XtraGrid.Columns.GridColumn colintPriorityID_4;
        private DevExpress.XtraGrid.Columns.GridColumn colintScheduleOfRates_4;
        private DevExpress.XtraGrid.Columns.GridColumn colintOwnership_4;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDueDate_5;
        private DevExpress.XtraGrid.Columns.GridColumn coldtDoneDate_5;
        private DevExpress.XtraGrid.Columns.GridColumn colintAction_5;
        private DevExpress.XtraGrid.Columns.GridColumn colintActionBy_5;
        private DevExpress.XtraGrid.Columns.GridColumn colintSupervisor_5;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobNumber_5;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser1_Action_5;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser2_Action_5;
        private DevExpress.XtraGrid.Columns.GridColumn colstrUser3_Action_5;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks_Action_5;
        private DevExpress.XtraGrid.Columns.GridColumn colintWorkOrderID_5;
        private DevExpress.XtraGrid.Columns.GridColumn colintPriorityID_5;
        private DevExpress.XtraGrid.Columns.GridColumn colintScheduleOfRates_5;
        private DevExpress.XtraGrid.Columns.GridColumn colintOwnership_5;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00134_export_to_GBM_export_dataTableAdapter sp00134_export_to_GBM_export_dataTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ButtonEdit ImportFileButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.SimpleButton btnMapFilesOK;
        private System.Windows.Forms.BindingSource sp00135exporttoGBMmapbackgroundfilesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colMapLinkID;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictID1;
        private DevExpress.XtraGrid.Columns.GridColumn colFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colFileType;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00135_export_to_GBM_map_background_filesTableAdapter sp00135_export_to_GBM_map_background_filesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictName1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictCode1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton btnImport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditPolygonXY;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCoordinates;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditSpecies;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private System.Windows.Forms.BindingSource sp00136importfromGBMlocalityspecieslistwithblankBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00136_import_from_GBM_locality_species_list_with_blankTableAdapter sp00136_import_from_GBM_locality_species_list_with_blankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colScientificName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesType;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditStatus;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private System.Windows.Forms.BindingSource sp00136importfromGBMlocalityspecieslistwithblankBindingSource1;
        private System.Windows.Forms.BindingSource sp00137importfromGBMltreestatuslistwithblankBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00137_import_from_GBM_ltree_status_list_with_blankTableAdapter sp00137_import_from_GBM_ltree_status_list_with_blankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditDistrict;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private System.Windows.Forms.BindingSource sp00138importfromGBMdistrictslistwithblankBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00138_import_from_GBM_districts_list_with_blankTableAdapter sp00138_import_from_GBM_districts_list_with_blankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictName2;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipID;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditLocality;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private System.Windows.Forms.BindingSource sp00139importfromGBMlocailitieslistwithblankBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp00139_import_from_GBM_locailities_list_with_blankTableAdapter sp00139_import_from_GBM_locailities_list_with_blankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictCode2;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictID3;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictName3;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictOwnershipCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictOwnershipName1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityName1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityOwnershipCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityOwnershipName1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.CheckEdit checkEditSelectTreeRef;
        private DevExpress.XtraEditors.CheckEdit checkEditSelectAct5Ref;
        private DevExpress.XtraEditors.CheckEdit checkEditSelectAct4Ref;
        private DevExpress.XtraEditors.CheckEdit checkEditSelectAct3Ref;
        private DevExpress.XtraEditors.CheckEdit checkEditSelectAct2Ref;
        private DevExpress.XtraEditors.CheckEdit checkEditSelectAct1Ref;
        private DevExpress.XtraEditors.CheckEdit checkEditSelectInspRef;
        private DevExpress.XtraEditors.SimpleButton btnSelectRecords;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.SimpleButton btnShowRecordsAll;
        private DevExpress.XtraEditors.SimpleButton btnShowRecordsErrorsOnly;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.CheckEdit checkEditBlockEditInspRef;
        private DevExpress.XtraEditors.CheckEdit checkEditBlockEditTreeRef;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.CheckEdit checkEditBlockEditAct1Ref;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.CheckEdit checkEditBlockEditAct5Ref;
        private DevExpress.XtraEditors.CheckEdit checkEditBlockEditAct4Ref;
        private DevExpress.XtraEditors.CheckEdit checkEditBlockEditAct3Ref;
        private DevExpress.XtraEditors.CheckEdit checkEditBlockEditAct2Ref;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.SimpleButton btnBlockEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.CheckEdit checkEditClearPriorSelection;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditSequenceField;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.SimpleButton btnRollBackImport;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl_WorkOrders;
        private DevExpress.XtraEditors.SimpleButton btnWorkOrderCheckingOK;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private DevExpress.XtraEditors.DateEdit dateEditWorkOrderCheckingToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditWorkOrderCheckingFromDate;
        private DevExpress.XtraEditors.CheckEdit checkEditWorkOrderCheckingIncludeCompleted;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnWorkOrderCheckingRefresh;
        private System.Windows.Forms.BindingSource sp01321ATexporttoGBMavailableworkordersBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuingBody;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderType;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedPreviousWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colcolor;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailDetails;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidents;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedActions;
        private DevExpress.XtraGrid.Columns.GridColumn colCompleteOverTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colIncompleteActions;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalActions;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp01321_AT_export_to_GBM_available_workordersTableAdapter sp01321_AT_export_to_GBM_available_workordersTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditExportType;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl7;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraEditors.ButtonEdit buttonEditImportWorkOrderChecking;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePhotograph;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionPhotograph;
        private System.Windows.Forms.BindingSource sp01323_AT_import_from_GBM_workorder_checkingBindingSource;
        private WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp01323_AT_import_from_GBM_workorder_checkingTableAdapter sp01323_AT_import_from_GBM_workorder_checkingTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCavat;
        private DevExpress.XtraGrid.Columns.GridColumn colStemCount;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownNorth;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownSouth;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownEast;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownWest;
        private DevExpress.XtraGrid.Columns.GridColumn colRetentionCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colSULE;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1_Tree;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2_Tree;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3_Tree;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesVariety;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectLength;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1_Inspection;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2_Inspection;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3_Inspection;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1_Action_1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2_Action_1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3_Action_1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1_Action_2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2_Action_2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3_Action_2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1_Action_3;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2_Action_3;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3_Action_3;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1_Action_4;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2_Action_4;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3_Action_4;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1_Action_5;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2_Action_5;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3_Action_5;
        private DevExpress.XtraEditors.CheckEdit IncludeLastInspectionDetailsCheckEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobWorkUnits_1;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobWorkUnits_2;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobWorkUnits_3;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobWorkUnits_4;
        private DevExpress.XtraGrid.Columns.GridColumn colmonJobWorkUnits_5;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditWorkUnits;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer5;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer6;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer7;
    }
}

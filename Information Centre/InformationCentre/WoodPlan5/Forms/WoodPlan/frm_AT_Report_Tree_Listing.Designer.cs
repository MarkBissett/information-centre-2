namespace WoodPlan5
{
    partial class frm_AT_Report_Tree_Listing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                // ***** Following wrapped in a Try Catch Block to avoid erro about comonents = null ???, Mark Bissett [19/11/2008] ***** // 
                try
                {
                    components.Dispose();
                }
                catch
                {
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip55 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem56 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem55 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip56 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem57 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem56 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip57 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem58 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem57 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip58 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem59 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem58 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip59 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem60 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem59 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip60 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem61 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem60 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip61 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem62 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem61 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip62 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem63 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem62 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip63 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem64 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem63 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip64 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem65 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem64 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip65 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem66 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem65 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip66 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem67 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem66 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip67 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem68 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem67 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip68 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem69 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem68 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip69 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem70 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem69 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip70 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem71 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem70 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip71 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem72 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem71 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip72 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem73 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem72 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip73 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem74 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem73 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip74 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem75 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem74 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip75 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem76 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem75 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip76 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem77 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem76 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip77 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem78 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem77 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip78 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem79 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem78 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip79 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem80 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem79 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip80 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem81 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem80 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip81 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem82 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem81 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip82 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem83 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem82 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip83 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem84 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem83 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip84 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem85 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem84 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip85 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem86 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem85 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip86 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem87 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem86 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip87 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem88 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem87 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip88 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem89 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem88 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip89 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem90 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem89 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip90 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem91 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem90 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip91 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem92 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem91 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip92 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem93 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem92 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip93 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem94 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem93 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip94 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem95 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem94 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip95 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem96 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem95 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip96 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem97 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem96 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip97 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem98 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem97 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip98 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem99 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem98 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip99 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem100 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem99 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip100 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem101 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem100 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip54 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem55 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem54 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip48 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem48 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem48 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip49 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem49 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem49 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem2 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem103 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip50 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem50 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem50 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip51 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem51 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem51 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip101 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem102 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem101 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Report_Tree_Listing));
            DevExpress.Utils.SuperToolTip superToolTip53 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem54 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem53 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip52 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem52 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem52 = new DevExpress.Utils.ToolTipItem();
            this.colActionCostDifference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.printRibbonController1 = new DevExpress.XtraPrinting.Preview.PrintRibbonController(this.components);
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.printPreviewBarItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem5 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem7 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem8 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem13 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem15 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem16 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem17 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem18 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem19 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem20 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem21 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem22 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem23 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem24 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem25 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem26 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem27 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem28 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem29 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem30 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem31 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem32 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem33 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem34 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem35 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem36 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem37 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem38 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem39 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem40 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem41 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem42 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem43 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem44 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem45 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewStaticItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.progressBarEditItem1 = new DevExpress.XtraPrinting.Preview.ProgressBarEditItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.printPreviewBarItem46 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.printPreviewStaticItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.zoomTrackBarEditItem1 = new DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.printPreviewRibbonPage1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.printPreviewRibbonPageGroup1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup2 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup3 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup4 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup5 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup6 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup7 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.btnLoadTrees = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01220ATReportsListingsTreeFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContext = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroundType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDBHRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeightRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProtectionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteHazardClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarget3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgeClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaHa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReplantCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHedgeLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGardenSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPropertyProximity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSituation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcolor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedInspectionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCavat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownNorth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownSouth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownEast = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownWest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetentionCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSULE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesVariety = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditClientFilter2 = new DevExpress.XtraEditors.ButtonEdit();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp01230ATReportsListingsInspectionFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStemDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAngleToVertical = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownFoliation1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownFoliation2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrownFoliation3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskCategory1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGeneralCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRootHeave1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRootHeave2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRootHeave3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colUser11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcolor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedOutstandingActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoFurtherActionRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1b = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2b = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3b = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnLoadInspections = new DevExpress.XtraEditors.SimpleButton();
            this.deInspectionToDate = new DevExpress.XtraEditors.DateEdit();
            this.deInspectionFromDate = new DevExpress.XtraEditors.DateEdit();
            this.ceLastInspectionOnly = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.buttonEditClientFilter3 = new DevExpress.XtraEditors.ButtonEdit();
            this.deActionToDate = new DevExpress.XtraEditors.DateEdit();
            this.deActionFromDate = new DevExpress.XtraEditors.DateEdit();
            this.btnLoadActions = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01232ATReportsListingsActionFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeGridReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeDistance1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeHouseName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentReferenceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateLastModified1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUser12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcolor2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrderCompleteDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrderIssueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnLayoutAddNew = new DevExpress.XtraEditors.SimpleButton();
            this.btnLayoutOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.sp01206_AT_Report_Available_LayoutsGridControl = new DevExpress.XtraGrid.GridControl();
            this.sp01206_AT_Report_Available_LayoutsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReportLayoutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublishedToWeb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.checkEdit_LoadLinkedData = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_LoadLinkedPictures = new DevExpress.XtraEditors.CheckEdit();
            this.btnView = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp01220_AT_Reports_Listings_Tree_FilterTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01220_AT_Reports_Listings_Tree_FilterTableAdapter();
            this.sp01206_AT_Report_Available_LayoutsTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01206_AT_Report_Available_LayoutsTableAdapter();
            this.sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter();
            this.sp01232_AT_Reports_Listings_Action_FilterTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01232_AT_Reports_Listings_Action_FilterTableAdapter();
            this.bbiPublishToWeb = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUnpublishToWeb = new DevExpress.XtraBars.BarButtonItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printRibbonController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01220ATReportsListingsTreeFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01230ATReportsListingsInspectionFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceLastInspectionOnly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01232ATReportsListingsActionFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp01206_AT_Report_Available_LayoutsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01206_AT_Report_Available_LayoutsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_LoadLinkedData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_LoadLinkedPictures.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPublishToWeb, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1427, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 807);
            this.barDockControlBottom.Size = new System.Drawing.Size(1427, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 807);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1427, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 807);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiPublishToWeb,
            this.bbiUnpublishToWeb});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActionCostDifference
            // 
            this.colActionCostDifference.Caption = "Cost Diff";
            this.colActionCostDifference.FieldName = "ActionCostDifference";
            this.colActionCostDifference.Name = "colActionCostDifference";
            this.colActionCostDifference.OptionsColumn.AllowEdit = false;
            this.colActionCostDifference.OptionsColumn.AllowFocus = false;
            this.colActionCostDifference.OptionsColumn.ReadOnly = true;
            this.colActionCostDifference.Visible = true;
            this.colActionCostDifference.VisibleIndex = 24;
            // 
            // printRibbonController1
            // 
            this.printRibbonController1.PrintControl = this.printControl1;
            this.printRibbonController1.RibbonControl = this.ribbonControl1;
            this.printRibbonController1.RibbonStatusBar = this.ribbonStatusBar1;
            // 
            // printControl1
            // 
            this.printControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printControl1.IsMetric = true;
            this.printControl1.Location = new System.Drawing.Point(444, 142);
            this.printControl1.Name = "printControl1";
            this.printControl1.PrintingSystem = this.printingSystem1;
            this.printControl1.Size = new System.Drawing.Size(983, 642);
            this.printControl1.TabIndex = 6;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonText = null;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.printPreviewBarItem1,
            this.printPreviewBarItem2,
            this.printPreviewBarItem3,
            this.printPreviewBarItem4,
            this.printPreviewBarItem5,
            this.printPreviewBarItem6,
            this.printPreviewBarItem7,
            this.printPreviewBarItem8,
            this.printPreviewBarItem9,
            this.printPreviewBarItem10,
            this.printPreviewBarItem11,
            this.printPreviewBarItem12,
            this.printPreviewBarItem13,
            this.printPreviewBarItem14,
            this.printPreviewBarItem15,
            this.printPreviewBarItem16,
            this.printPreviewBarItem17,
            this.printPreviewBarItem18,
            this.printPreviewBarItem19,
            this.printPreviewBarItem20,
            this.printPreviewBarItem21,
            this.printPreviewBarItem22,
            this.printPreviewBarItem23,
            this.printPreviewBarItem24,
            this.printPreviewBarItem25,
            this.printPreviewBarItem26,
            this.printPreviewBarItem27,
            this.printPreviewBarItem28,
            this.printPreviewBarItem29,
            this.printPreviewBarItem30,
            this.printPreviewBarItem31,
            this.printPreviewBarItem32,
            this.printPreviewBarItem33,
            this.printPreviewBarItem34,
            this.printPreviewBarItem35,
            this.printPreviewBarItem36,
            this.printPreviewBarItem37,
            this.printPreviewBarItem38,
            this.printPreviewBarItem39,
            this.printPreviewBarItem40,
            this.printPreviewBarItem41,
            this.printPreviewBarItem42,
            this.printPreviewBarItem43,
            this.printPreviewBarItem44,
            this.printPreviewBarItem45,
            this.printPreviewStaticItem1,
            this.barStaticItem1,
            this.progressBarEditItem1,
            this.printPreviewBarItem46,
            this.barButtonItem1,
            this.printPreviewStaticItem2,
            this.zoomTrackBarEditItem1});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 56;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.printPreviewRibbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemZoomTrackBar1});
            this.ribbonControl1.Size = new System.Drawing.Size(1427, 142);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.TransparentEditorsMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // printPreviewBarItem1
            // 
            this.printPreviewBarItem1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem1.Caption = "Bookmarks";
            this.printPreviewBarItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap;
            this.printPreviewBarItem1.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem1.Enabled = false;
            this.printPreviewBarItem1.Id = 0;
            this.printPreviewBarItem1.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_DocumentMap;
            this.printPreviewBarItem1.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_DocumentMapLarge;
            this.printPreviewBarItem1.Name = "printPreviewBarItem1";
            superToolTip55.FixedTooltipWidth = true;
            toolTipTitleItem56.Text = "Document Map";
            toolTipItem55.LeftIndent = 6;
            toolTipItem55.Text = "Open the Document Map, which allows you to navigate through a structural view of " +
    "the document.";
            superToolTip55.Items.Add(toolTipTitleItem56);
            superToolTip55.Items.Add(toolTipItem55);
            superToolTip55.MaxWidth = 210;
            this.printPreviewBarItem1.SuperTip = superToolTip55;
            // 
            // printPreviewBarItem2
            // 
            this.printPreviewBarItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem2.Caption = "Parameters";
            this.printPreviewBarItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Parameters;
            this.printPreviewBarItem2.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem2.Enabled = false;
            this.printPreviewBarItem2.Id = 1;
            this.printPreviewBarItem2.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Parameters;
            this.printPreviewBarItem2.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ParametersLarge;
            this.printPreviewBarItem2.Name = "printPreviewBarItem2";
            superToolTip56.FixedTooltipWidth = true;
            toolTipTitleItem57.Text = "Parameters";
            toolTipItem56.LeftIndent = 6;
            toolTipItem56.Text = "Open the Parameters pane, which allows you to enter values for report parameters." +
    "";
            superToolTip56.Items.Add(toolTipTitleItem57);
            superToolTip56.Items.Add(toolTipItem56);
            superToolTip56.MaxWidth = 210;
            this.printPreviewBarItem2.SuperTip = superToolTip56;
            // 
            // printPreviewBarItem3
            // 
            this.printPreviewBarItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem3.Caption = "Find";
            this.printPreviewBarItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Find;
            this.printPreviewBarItem3.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem3.Enabled = false;
            this.printPreviewBarItem3.Id = 2;
            this.printPreviewBarItem3.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Find;
            this.printPreviewBarItem3.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.printPreviewBarItem3.Name = "printPreviewBarItem3";
            superToolTip57.FixedTooltipWidth = true;
            toolTipTitleItem58.Text = "Find";
            toolTipItem57.LeftIndent = 6;
            toolTipItem57.Text = "Show the Find dialog to find text in the document.";
            superToolTip57.Items.Add(toolTipTitleItem58);
            superToolTip57.Items.Add(toolTipItem57);
            superToolTip57.MaxWidth = 210;
            this.printPreviewBarItem3.SuperTip = superToolTip57;
            // 
            // printPreviewBarItem4
            // 
            this.printPreviewBarItem4.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem4.Caption = "Options";
            this.printPreviewBarItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Customize;
            this.printPreviewBarItem4.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem4.Enabled = false;
            this.printPreviewBarItem4.Id = 3;
            this.printPreviewBarItem4.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Customize;
            this.printPreviewBarItem4.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_CustomizeLarge;
            this.printPreviewBarItem4.Name = "printPreviewBarItem4";
            superToolTip58.FixedTooltipWidth = true;
            toolTipTitleItem59.Text = "Options";
            toolTipItem58.LeftIndent = 6;
            toolTipItem58.Text = "Open the Print Options dialog, in which you can change printing options.";
            superToolTip58.Items.Add(toolTipTitleItem59);
            superToolTip58.Items.Add(toolTipItem58);
            superToolTip58.MaxWidth = 210;
            this.printPreviewBarItem4.SuperTip = superToolTip58;
            // 
            // printPreviewBarItem5
            // 
            this.printPreviewBarItem5.Caption = "Print";
            this.printPreviewBarItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Print;
            this.printPreviewBarItem5.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem5.Enabled = false;
            this.printPreviewBarItem5.Id = 4;
            this.printPreviewBarItem5.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Print;
            this.printPreviewBarItem5.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintLarge;
            this.printPreviewBarItem5.Name = "printPreviewBarItem5";
            superToolTip59.FixedTooltipWidth = true;
            toolTipTitleItem60.Text = "Print (Ctrl+P)";
            toolTipItem59.LeftIndent = 6;
            toolTipItem59.Text = "Select a printer, number of copies and other printing options before printing.";
            superToolTip59.Items.Add(toolTipTitleItem60);
            superToolTip59.Items.Add(toolTipItem59);
            superToolTip59.MaxWidth = 210;
            this.printPreviewBarItem5.SuperTip = superToolTip59;
            // 
            // printPreviewBarItem6
            // 
            this.printPreviewBarItem6.Caption = "Quick Print";
            this.printPreviewBarItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect;
            this.printPreviewBarItem6.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem6.Enabled = false;
            this.printPreviewBarItem6.Id = 5;
            this.printPreviewBarItem6.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.printPreviewBarItem6.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirectLarge;
            this.printPreviewBarItem6.Name = "printPreviewBarItem6";
            superToolTip60.FixedTooltipWidth = true;
            toolTipTitleItem61.Text = "Quick Print";
            toolTipItem60.LeftIndent = 6;
            toolTipItem60.Text = "Send the document directly to the default printer without making changes.";
            superToolTip60.Items.Add(toolTipTitleItem61);
            superToolTip60.Items.Add(toolTipItem60);
            superToolTip60.MaxWidth = 210;
            this.printPreviewBarItem6.SuperTip = superToolTip60;
            // 
            // printPreviewBarItem7
            // 
            this.printPreviewBarItem7.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem7.Caption = "Custom Margins...";
            this.printPreviewBarItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup;
            this.printPreviewBarItem7.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem7.Enabled = false;
            this.printPreviewBarItem7.Id = 6;
            this.printPreviewBarItem7.Name = "printPreviewBarItem7";
            superToolTip61.FixedTooltipWidth = true;
            toolTipTitleItem62.Text = "Page Setup";
            toolTipItem61.LeftIndent = 6;
            toolTipItem61.Text = "Show the Page Setup dialog.";
            superToolTip61.Items.Add(toolTipTitleItem62);
            superToolTip61.Items.Add(toolTipItem61);
            superToolTip61.MaxWidth = 210;
            this.printPreviewBarItem7.SuperTip = superToolTip61;
            // 
            // printPreviewBarItem8
            // 
            this.printPreviewBarItem8.Caption = "Header/Footer";
            this.printPreviewBarItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.EditPageHF;
            this.printPreviewBarItem8.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem8.Enabled = false;
            this.printPreviewBarItem8.Id = 7;
            this.printPreviewBarItem8.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_EditPageHF;
            this.printPreviewBarItem8.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_EditPageHFLarge;
            this.printPreviewBarItem8.Name = "printPreviewBarItem8";
            superToolTip62.FixedTooltipWidth = true;
            toolTipTitleItem63.Text = "Header and Footer";
            toolTipItem62.LeftIndent = 6;
            toolTipItem62.Text = "Edit the header and footer of the document.";
            superToolTip62.Items.Add(toolTipTitleItem63);
            superToolTip62.Items.Add(toolTipItem62);
            superToolTip62.MaxWidth = 210;
            this.printPreviewBarItem8.SuperTip = superToolTip62;
            // 
            // printPreviewBarItem9
            // 
            this.printPreviewBarItem9.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem9.Caption = "Scale";
            this.printPreviewBarItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Scale;
            this.printPreviewBarItem9.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem9.Enabled = false;
            this.printPreviewBarItem9.Id = 8;
            this.printPreviewBarItem9.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Scale;
            this.printPreviewBarItem9.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ScaleLarge;
            this.printPreviewBarItem9.Name = "printPreviewBarItem9";
            superToolTip63.FixedTooltipWidth = true;
            toolTipTitleItem64.Text = "Scale";
            toolTipItem63.LeftIndent = 6;
            toolTipItem63.Text = "Stretch or shrink the printed output to a percentage of its actual size.";
            superToolTip63.Items.Add(toolTipTitleItem64);
            superToolTip63.Items.Add(toolTipItem63);
            superToolTip63.MaxWidth = 210;
            this.printPreviewBarItem9.SuperTip = superToolTip63;
            // 
            // printPreviewBarItem10
            // 
            this.printPreviewBarItem10.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem10.Caption = "Pointer";
            this.printPreviewBarItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Pointer;
            this.printPreviewBarItem10.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem10.Down = true;
            this.printPreviewBarItem10.Enabled = false;
            this.printPreviewBarItem10.GroupIndex = 1;
            this.printPreviewBarItem10.Id = 9;
            this.printPreviewBarItem10.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Pointer;
            this.printPreviewBarItem10.Name = "printPreviewBarItem10";
            this.printPreviewBarItem10.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip64.FixedTooltipWidth = true;
            toolTipTitleItem65.Text = "Mouse Pointer";
            toolTipItem64.LeftIndent = 6;
            toolTipItem64.Text = "Show the mouse pointer.";
            superToolTip64.Items.Add(toolTipTitleItem65);
            superToolTip64.Items.Add(toolTipItem64);
            superToolTip64.MaxWidth = 210;
            this.printPreviewBarItem10.SuperTip = superToolTip64;
            // 
            // printPreviewBarItem11
            // 
            this.printPreviewBarItem11.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem11.Caption = "Hand Tool";
            this.printPreviewBarItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HandTool;
            this.printPreviewBarItem11.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem11.Enabled = false;
            this.printPreviewBarItem11.GroupIndex = 1;
            this.printPreviewBarItem11.Id = 10;
            this.printPreviewBarItem11.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_HandTool;
            this.printPreviewBarItem11.Name = "printPreviewBarItem11";
            this.printPreviewBarItem11.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip65.FixedTooltipWidth = true;
            toolTipTitleItem66.Text = "Hand Tool";
            toolTipItem65.LeftIndent = 6;
            toolTipItem65.Text = "Invoke the Hand tool to manually scroll through pages.";
            superToolTip65.Items.Add(toolTipTitleItem66);
            superToolTip65.Items.Add(toolTipItem65);
            superToolTip65.MaxWidth = 210;
            this.printPreviewBarItem11.SuperTip = superToolTip65;
            // 
            // printPreviewBarItem12
            // 
            this.printPreviewBarItem12.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem12.Caption = "Magnifier";
            this.printPreviewBarItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Magnifier;
            this.printPreviewBarItem12.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem12.Enabled = false;
            this.printPreviewBarItem12.GroupIndex = 1;
            this.printPreviewBarItem12.Id = 11;
            this.printPreviewBarItem12.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Magnifier;
            this.printPreviewBarItem12.Name = "printPreviewBarItem12";
            this.printPreviewBarItem12.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip66.FixedTooltipWidth = true;
            toolTipTitleItem67.Text = "Magnifier";
            toolTipItem66.LeftIndent = 6;
            toolTipItem66.Text = "Invoke the Magnifier tool.\r\n\r\nClicking once on a document zooms it so that a sing" +
    "le page becomes entirely visible, while clicking another time zooms it to 100% o" +
    "f the normal size.";
            superToolTip66.Items.Add(toolTipTitleItem67);
            superToolTip66.Items.Add(toolTipItem66);
            superToolTip66.MaxWidth = 210;
            this.printPreviewBarItem12.SuperTip = superToolTip66;
            // 
            // printPreviewBarItem13
            // 
            this.printPreviewBarItem13.Caption = "Zoom Out";
            this.printPreviewBarItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomOut;
            this.printPreviewBarItem13.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem13.Enabled = false;
            this.printPreviewBarItem13.Id = 12;
            this.printPreviewBarItem13.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomOut;
            this.printPreviewBarItem13.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomOutLarge;
            this.printPreviewBarItem13.Name = "printPreviewBarItem13";
            superToolTip67.FixedTooltipWidth = true;
            toolTipTitleItem68.Text = "Zoom Out";
            toolTipItem67.LeftIndent = 6;
            toolTipItem67.Text = "Zoom out to see more of the page at a reduced size.";
            superToolTip67.Items.Add(toolTipTitleItem68);
            superToolTip67.Items.Add(toolTipItem67);
            superToolTip67.MaxWidth = 210;
            this.printPreviewBarItem13.SuperTip = superToolTip67;
            // 
            // printPreviewBarItem14
            // 
            this.printPreviewBarItem14.Caption = "Zoom In";
            this.printPreviewBarItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomIn;
            this.printPreviewBarItem14.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem14.Enabled = false;
            this.printPreviewBarItem14.Id = 13;
            this.printPreviewBarItem14.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomIn;
            this.printPreviewBarItem14.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomInLarge;
            this.printPreviewBarItem14.Name = "printPreviewBarItem14";
            superToolTip68.FixedTooltipWidth = true;
            toolTipTitleItem69.Text = "Zoom In";
            toolTipItem68.LeftIndent = 6;
            toolTipItem68.Text = "Zoom in to get a close-up view of the document.";
            superToolTip68.Items.Add(toolTipTitleItem69);
            superToolTip68.Items.Add(toolTipItem68);
            superToolTip68.MaxWidth = 210;
            this.printPreviewBarItem14.SuperTip = superToolTip68;
            // 
            // printPreviewBarItem15
            // 
            this.printPreviewBarItem15.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem15.Caption = "Zoom";
            this.printPreviewBarItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Zoom;
            this.printPreviewBarItem15.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem15.Enabled = false;
            this.printPreviewBarItem15.Id = 14;
            this.printPreviewBarItem15.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Zoom;
            this.printPreviewBarItem15.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomLarge;
            this.printPreviewBarItem15.Name = "printPreviewBarItem15";
            superToolTip69.FixedTooltipWidth = true;
            toolTipTitleItem70.Text = "Zoom";
            toolTipItem69.LeftIndent = 6;
            toolTipItem69.Text = "Change the zoom level of the document preview.";
            superToolTip69.Items.Add(toolTipTitleItem70);
            superToolTip69.Items.Add(toolTipItem69);
            superToolTip69.MaxWidth = 210;
            this.printPreviewBarItem15.SuperTip = superToolTip69;
            // 
            // printPreviewBarItem16
            // 
            this.printPreviewBarItem16.Caption = "First Page";
            this.printPreviewBarItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowFirstPage;
            this.printPreviewBarItem16.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem16.Enabled = false;
            this.printPreviewBarItem16.Id = 15;
            this.printPreviewBarItem16.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowFirstPage;
            this.printPreviewBarItem16.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowFirstPageLarge;
            this.printPreviewBarItem16.Name = "printPreviewBarItem16";
            superToolTip70.FixedTooltipWidth = true;
            toolTipTitleItem71.Text = "First Page (Ctrl+Home)";
            toolTipItem70.LeftIndent = 6;
            toolTipItem70.Text = "Navigate to the first page of the document.";
            superToolTip70.Items.Add(toolTipTitleItem71);
            superToolTip70.Items.Add(toolTipItem70);
            superToolTip70.MaxWidth = 210;
            this.printPreviewBarItem16.SuperTip = superToolTip70;
            // 
            // printPreviewBarItem17
            // 
            this.printPreviewBarItem17.Caption = "Previous Page";
            this.printPreviewBarItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowPrevPage;
            this.printPreviewBarItem17.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem17.Enabled = false;
            this.printPreviewBarItem17.Id = 16;
            this.printPreviewBarItem17.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowPrevPage;
            this.printPreviewBarItem17.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowPrevPageLarge;
            this.printPreviewBarItem17.Name = "printPreviewBarItem17";
            superToolTip71.FixedTooltipWidth = true;
            toolTipTitleItem72.Text = "Previous Page (PageUp)";
            toolTipItem71.LeftIndent = 6;
            toolTipItem71.Text = "Navigate to the previous page of the document.";
            superToolTip71.Items.Add(toolTipTitleItem72);
            superToolTip71.Items.Add(toolTipItem71);
            superToolTip71.MaxWidth = 210;
            this.printPreviewBarItem17.SuperTip = superToolTip71;
            // 
            // printPreviewBarItem18
            // 
            this.printPreviewBarItem18.Caption = "Next  Page ";
            this.printPreviewBarItem18.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowNextPage;
            this.printPreviewBarItem18.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem18.Enabled = false;
            this.printPreviewBarItem18.Id = 17;
            this.printPreviewBarItem18.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowNextPage;
            this.printPreviewBarItem18.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowNextPageLarge;
            this.printPreviewBarItem18.Name = "printPreviewBarItem18";
            superToolTip72.FixedTooltipWidth = true;
            toolTipTitleItem73.Text = "Next Page (PageDown)";
            toolTipItem72.LeftIndent = 6;
            toolTipItem72.Text = "Navigate to the next page of the document.";
            superToolTip72.Items.Add(toolTipTitleItem73);
            superToolTip72.Items.Add(toolTipItem72);
            superToolTip72.MaxWidth = 210;
            this.printPreviewBarItem18.SuperTip = superToolTip72;
            // 
            // printPreviewBarItem19
            // 
            this.printPreviewBarItem19.Caption = "Last  Page ";
            this.printPreviewBarItem19.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowLastPage;
            this.printPreviewBarItem19.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem19.Enabled = false;
            this.printPreviewBarItem19.Id = 18;
            this.printPreviewBarItem19.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowLastPage;
            this.printPreviewBarItem19.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowLastPageLarge;
            this.printPreviewBarItem19.Name = "printPreviewBarItem19";
            superToolTip73.FixedTooltipWidth = true;
            toolTipTitleItem74.Text = "Last Page (Ctrl+End)";
            toolTipItem73.LeftIndent = 6;
            toolTipItem73.Text = "Navigate to the last page of the document.";
            superToolTip73.Items.Add(toolTipTitleItem74);
            superToolTip73.Items.Add(toolTipItem73);
            superToolTip73.MaxWidth = 210;
            this.printPreviewBarItem19.SuperTip = superToolTip73;
            // 
            // printPreviewBarItem20
            // 
            this.printPreviewBarItem20.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem20.Caption = "Many Pages";
            this.printPreviewBarItem20.Command = DevExpress.XtraPrinting.PrintingSystemCommand.MultiplePages;
            this.printPreviewBarItem20.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem20.Enabled = false;
            this.printPreviewBarItem20.Id = 19;
            this.printPreviewBarItem20.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_MultiplePages;
            this.printPreviewBarItem20.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_MultiplePagesLarge;
            this.printPreviewBarItem20.Name = "printPreviewBarItem20";
            superToolTip74.FixedTooltipWidth = true;
            toolTipTitleItem75.Text = "View Many Pages";
            toolTipItem74.LeftIndent = 6;
            toolTipItem74.Text = "Choose the page layout to arrange the document pages in preview.";
            superToolTip74.Items.Add(toolTipTitleItem75);
            superToolTip74.Items.Add(toolTipItem74);
            superToolTip74.MaxWidth = 210;
            this.printPreviewBarItem20.SuperTip = superToolTip74;
            // 
            // printPreviewBarItem21
            // 
            this.printPreviewBarItem21.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem21.Caption = "Page Color";
            this.printPreviewBarItem21.Command = DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground;
            this.printPreviewBarItem21.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem21.Enabled = false;
            this.printPreviewBarItem21.Id = 20;
            this.printPreviewBarItem21.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FillBackground;
            this.printPreviewBarItem21.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FillBackgroundLarge;
            this.printPreviewBarItem21.Name = "printPreviewBarItem21";
            superToolTip75.FixedTooltipWidth = true;
            toolTipTitleItem76.Text = "Background Color";
            toolTipItem75.LeftIndent = 6;
            toolTipItem75.Text = "Choose a color for the background of the document pages.";
            superToolTip75.Items.Add(toolTipTitleItem76);
            superToolTip75.Items.Add(toolTipItem75);
            superToolTip75.MaxWidth = 210;
            this.printPreviewBarItem21.SuperTip = superToolTip75;
            // 
            // printPreviewBarItem22
            // 
            this.printPreviewBarItem22.Caption = "Watermark";
            this.printPreviewBarItem22.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Watermark;
            this.printPreviewBarItem22.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem22.Enabled = false;
            this.printPreviewBarItem22.Id = 21;
            this.printPreviewBarItem22.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Watermark;
            this.printPreviewBarItem22.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_WatermarkLarge;
            this.printPreviewBarItem22.Name = "printPreviewBarItem22";
            superToolTip76.FixedTooltipWidth = true;
            toolTipTitleItem77.Text = "Watermark";
            toolTipItem76.LeftIndent = 6;
            toolTipItem76.Text = "Insert ghosted text or image behind the content of a page.\r\n\r\nThis is often used " +
    "to indicate that a document is to be treated specially.";
            superToolTip76.Items.Add(toolTipTitleItem77);
            superToolTip76.Items.Add(toolTipItem76);
            superToolTip76.MaxWidth = 210;
            this.printPreviewBarItem22.SuperTip = superToolTip76;
            // 
            // printPreviewBarItem23
            // 
            this.printPreviewBarItem23.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem23.Caption = "Export To";
            this.printPreviewBarItem23.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile;
            this.printPreviewBarItem23.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem23.Enabled = false;
            this.printPreviewBarItem23.Id = 22;
            this.printPreviewBarItem23.Name = "printPreviewBarItem23";
            superToolTip77.FixedTooltipWidth = true;
            toolTipTitleItem78.Text = "Export To...";
            toolTipItem77.LeftIndent = 6;
            toolTipItem77.Text = "Export the current document in one of the available formats, and save it to the f" +
    "ile on a disk.";
            superToolTip77.Items.Add(toolTipTitleItem78);
            superToolTip77.Items.Add(toolTipItem77);
            superToolTip77.MaxWidth = 210;
            this.printPreviewBarItem23.SuperTip = superToolTip77;
            // 
            // printPreviewBarItem24
            // 
            this.printPreviewBarItem24.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem24.Caption = "E-Mail As";
            this.printPreviewBarItem24.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendFile;
            this.printPreviewBarItem24.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem24.Enabled = false;
            this.printPreviewBarItem24.Id = 23;
            this.printPreviewBarItem24.Name = "printPreviewBarItem24";
            superToolTip78.FixedTooltipWidth = true;
            toolTipTitleItem79.Text = "E-Mail As...";
            toolTipItem78.LeftIndent = 6;
            toolTipItem78.Text = "Export the current document in one of the available formats, and attach it to the" +
    " e-mail.";
            superToolTip78.Items.Add(toolTipTitleItem79);
            superToolTip78.Items.Add(toolTipItem78);
            superToolTip78.MaxWidth = 210;
            this.printPreviewBarItem24.SuperTip = superToolTip78;
            // 
            // printPreviewBarItem25
            // 
            this.printPreviewBarItem25.Caption = "Close Print Preview";
            this.printPreviewBarItem25.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview;
            this.printPreviewBarItem25.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem25.Enabled = false;
            this.printPreviewBarItem25.Id = 24;
            this.printPreviewBarItem25.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreview;
            this.printPreviewBarItem25.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreviewLarge;
            this.printPreviewBarItem25.Name = "printPreviewBarItem25";
            superToolTip79.FixedTooltipWidth = true;
            toolTipTitleItem80.Text = "Close Print Preview";
            toolTipItem79.LeftIndent = 6;
            toolTipItem79.Text = "Close Print Preview of the document.";
            superToolTip79.Items.Add(toolTipTitleItem80);
            superToolTip79.Items.Add(toolTipItem79);
            superToolTip79.MaxWidth = 210;
            this.printPreviewBarItem25.SuperTip = superToolTip79;
            // 
            // printPreviewBarItem26
            // 
            this.printPreviewBarItem26.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem26.Caption = "Orientation";
            this.printPreviewBarItem26.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageOrientation;
            this.printPreviewBarItem26.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem26.Enabled = false;
            this.printPreviewBarItem26.Id = 25;
            this.printPreviewBarItem26.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageOrientation;
            this.printPreviewBarItem26.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageOrientationLarge;
            this.printPreviewBarItem26.Name = "printPreviewBarItem26";
            superToolTip80.FixedTooltipWidth = true;
            toolTipTitleItem81.Text = "Page Orientation";
            toolTipItem80.LeftIndent = 6;
            toolTipItem80.Text = "Switch the pages between portrait and landscape layouts.";
            superToolTip80.Items.Add(toolTipTitleItem81);
            superToolTip80.Items.Add(toolTipItem80);
            superToolTip80.MaxWidth = 210;
            this.printPreviewBarItem26.SuperTip = superToolTip80;
            // 
            // printPreviewBarItem27
            // 
            this.printPreviewBarItem27.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem27.Caption = "Size";
            this.printPreviewBarItem27.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PaperSize;
            this.printPreviewBarItem27.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem27.Enabled = false;
            this.printPreviewBarItem27.Id = 26;
            this.printPreviewBarItem27.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PaperSize;
            this.printPreviewBarItem27.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PaperSizeLarge;
            this.printPreviewBarItem27.Name = "printPreviewBarItem27";
            superToolTip81.FixedTooltipWidth = true;
            toolTipTitleItem82.Text = "Page Size";
            toolTipItem81.LeftIndent = 6;
            toolTipItem81.Text = "Choose the paper size of the document.";
            superToolTip81.Items.Add(toolTipTitleItem82);
            superToolTip81.Items.Add(toolTipItem81);
            superToolTip81.MaxWidth = 210;
            this.printPreviewBarItem27.SuperTip = superToolTip81;
            // 
            // printPreviewBarItem28
            // 
            this.printPreviewBarItem28.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem28.Caption = "Margins";
            this.printPreviewBarItem28.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageMargins;
            this.printPreviewBarItem28.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem28.Enabled = false;
            this.printPreviewBarItem28.Id = 27;
            this.printPreviewBarItem28.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageMargins;
            this.printPreviewBarItem28.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageMarginsLarge;
            this.printPreviewBarItem28.Name = "printPreviewBarItem28";
            superToolTip82.FixedTooltipWidth = true;
            toolTipTitleItem83.Text = "Page Margins";
            toolTipItem82.LeftIndent = 6;
            toolTipItem82.Text = "Select the margin sizes for the entire document.\r\n\r\nTo apply specific margin size" +
    "s to the document, click Custom Margins.";
            superToolTip82.Items.Add(toolTipTitleItem83);
            superToolTip82.Items.Add(toolTipItem82);
            superToolTip82.MaxWidth = 210;
            this.printPreviewBarItem28.SuperTip = superToolTip82;
            // 
            // printPreviewBarItem29
            // 
            this.printPreviewBarItem29.Caption = "PDF File";
            this.printPreviewBarItem29.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf;
            this.printPreviewBarItem29.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem29.Description = "Adobe Portable Document Format";
            this.printPreviewBarItem29.Enabled = false;
            this.printPreviewBarItem29.Id = 28;
            this.printPreviewBarItem29.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendPdf;
            this.printPreviewBarItem29.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendPdfLarge;
            this.printPreviewBarItem29.Name = "printPreviewBarItem29";
            superToolTip83.FixedTooltipWidth = true;
            toolTipTitleItem84.Text = "E-Mail As PDF";
            toolTipItem83.LeftIndent = 6;
            toolTipItem83.Text = "Export the document to PDF and attach it to the e-mail.";
            superToolTip83.Items.Add(toolTipTitleItem84);
            superToolTip83.Items.Add(toolTipItem83);
            superToolTip83.MaxWidth = 210;
            this.printPreviewBarItem29.SuperTip = superToolTip83;
            // 
            // printPreviewBarItem30
            // 
            this.printPreviewBarItem30.Caption = "Text File";
            this.printPreviewBarItem30.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt;
            this.printPreviewBarItem30.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem30.Description = "Plain Text";
            this.printPreviewBarItem30.Enabled = false;
            this.printPreviewBarItem30.Id = 29;
            this.printPreviewBarItem30.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendTxt;
            this.printPreviewBarItem30.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendTxtLarge;
            this.printPreviewBarItem30.Name = "printPreviewBarItem30";
            superToolTip84.FixedTooltipWidth = true;
            toolTipTitleItem85.Text = "E-Mail As Text";
            toolTipItem84.LeftIndent = 6;
            toolTipItem84.Text = "Export the document to Text and attach it to the e-mail.";
            superToolTip84.Items.Add(toolTipTitleItem85);
            superToolTip84.Items.Add(toolTipItem84);
            superToolTip84.MaxWidth = 210;
            this.printPreviewBarItem30.SuperTip = superToolTip84;
            // 
            // printPreviewBarItem31
            // 
            this.printPreviewBarItem31.Caption = "CSV File";
            this.printPreviewBarItem31.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv;
            this.printPreviewBarItem31.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem31.Description = "Comma-Separated Values Text";
            this.printPreviewBarItem31.Enabled = false;
            this.printPreviewBarItem31.Id = 30;
            this.printPreviewBarItem31.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendCsv;
            this.printPreviewBarItem31.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendCsvLarge;
            this.printPreviewBarItem31.Name = "printPreviewBarItem31";
            superToolTip85.FixedTooltipWidth = true;
            toolTipTitleItem86.Text = "E-Mail As CSV";
            toolTipItem85.LeftIndent = 6;
            toolTipItem85.Text = "Export the document to CSV and attach it to the e-mail.";
            superToolTip85.Items.Add(toolTipTitleItem86);
            superToolTip85.Items.Add(toolTipItem85);
            superToolTip85.MaxWidth = 210;
            this.printPreviewBarItem31.SuperTip = superToolTip85;
            // 
            // printPreviewBarItem32
            // 
            this.printPreviewBarItem32.Caption = "MHT File";
            this.printPreviewBarItem32.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht;
            this.printPreviewBarItem32.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem32.Description = "Single File Web Page";
            this.printPreviewBarItem32.Enabled = false;
            this.printPreviewBarItem32.Id = 31;
            this.printPreviewBarItem32.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendMht;
            this.printPreviewBarItem32.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendMhtLarge;
            this.printPreviewBarItem32.Name = "printPreviewBarItem32";
            superToolTip86.FixedTooltipWidth = true;
            toolTipTitleItem87.Text = "E-Mail As MHT";
            toolTipItem86.LeftIndent = 6;
            toolTipItem86.Text = "Export the document to MHT and attach it to the e-mail.";
            superToolTip86.Items.Add(toolTipTitleItem87);
            superToolTip86.Items.Add(toolTipItem86);
            superToolTip86.MaxWidth = 210;
            this.printPreviewBarItem32.SuperTip = superToolTip86;
            // 
            // printPreviewBarItem33
            // 
            this.printPreviewBarItem33.Caption = "Excel File";
            this.printPreviewBarItem33.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls;
            this.printPreviewBarItem33.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem33.Description = "Microsoft Excel Workbook";
            this.printPreviewBarItem33.Enabled = false;
            this.printPreviewBarItem33.Id = 32;
            this.printPreviewBarItem33.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendXls;
            this.printPreviewBarItem33.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendXlsLarge;
            this.printPreviewBarItem33.Name = "printPreviewBarItem33";
            superToolTip87.FixedTooltipWidth = true;
            toolTipTitleItem88.Text = "E-Mail As XLS";
            toolTipItem87.LeftIndent = 6;
            toolTipItem87.Text = "Export the document to XLS and attach it to the e-mail.";
            superToolTip87.Items.Add(toolTipTitleItem88);
            superToolTip87.Items.Add(toolTipItem87);
            superToolTip87.MaxWidth = 210;
            this.printPreviewBarItem33.SuperTip = superToolTip87;
            // 
            // printPreviewBarItem34
            // 
            this.printPreviewBarItem34.Caption = "RTF File";
            this.printPreviewBarItem34.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf;
            this.printPreviewBarItem34.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem34.Description = "Rich Text Format";
            this.printPreviewBarItem34.Enabled = false;
            this.printPreviewBarItem34.Id = 33;
            this.printPreviewBarItem34.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendRtf;
            this.printPreviewBarItem34.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendRtfLarge;
            this.printPreviewBarItem34.Name = "printPreviewBarItem34";
            superToolTip88.FixedTooltipWidth = true;
            toolTipTitleItem89.Text = "E-Mail As RTF";
            toolTipItem88.LeftIndent = 6;
            toolTipItem88.Text = "Export the document to RTF and attach it to the e-mail.";
            superToolTip88.Items.Add(toolTipTitleItem89);
            superToolTip88.Items.Add(toolTipItem88);
            superToolTip88.MaxWidth = 210;
            this.printPreviewBarItem34.SuperTip = superToolTip88;
            // 
            // printPreviewBarItem35
            // 
            this.printPreviewBarItem35.Caption = "Image File";
            this.printPreviewBarItem35.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic;
            this.printPreviewBarItem35.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem35.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.printPreviewBarItem35.Enabled = false;
            this.printPreviewBarItem35.Id = 34;
            this.printPreviewBarItem35.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendGraphic;
            this.printPreviewBarItem35.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendGraphicLarge;
            this.printPreviewBarItem35.Name = "printPreviewBarItem35";
            superToolTip89.FixedTooltipWidth = true;
            toolTipTitleItem90.Text = "E-Mail As Image";
            toolTipItem89.LeftIndent = 6;
            toolTipItem89.Text = "Export the document to Image and attach it to the e-mail.";
            superToolTip89.Items.Add(toolTipTitleItem90);
            superToolTip89.Items.Add(toolTipItem89);
            superToolTip89.MaxWidth = 210;
            this.printPreviewBarItem35.SuperTip = superToolTip89;
            // 
            // printPreviewBarItem36
            // 
            this.printPreviewBarItem36.Caption = "PDF File";
            this.printPreviewBarItem36.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            this.printPreviewBarItem36.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem36.Description = "Adobe Portable Document Format";
            this.printPreviewBarItem36.Enabled = false;
            this.printPreviewBarItem36.Id = 35;
            this.printPreviewBarItem36.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportPdf;
            this.printPreviewBarItem36.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportPdfLarge;
            this.printPreviewBarItem36.Name = "printPreviewBarItem36";
            superToolTip90.FixedTooltipWidth = true;
            toolTipTitleItem91.Text = "Export to PDF";
            toolTipItem90.LeftIndent = 6;
            toolTipItem90.Text = "Export the document to PDF and save it to the file on a disk.";
            superToolTip90.Items.Add(toolTipTitleItem91);
            superToolTip90.Items.Add(toolTipItem90);
            superToolTip90.MaxWidth = 210;
            this.printPreviewBarItem36.SuperTip = superToolTip90;
            // 
            // printPreviewBarItem37
            // 
            this.printPreviewBarItem37.Caption = "HTML File";
            this.printPreviewBarItem37.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm;
            this.printPreviewBarItem37.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem37.Description = "Web Page";
            this.printPreviewBarItem37.Enabled = false;
            this.printPreviewBarItem37.Id = 36;
            this.printPreviewBarItem37.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportHtm;
            this.printPreviewBarItem37.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportHtmLarge;
            this.printPreviewBarItem37.Name = "printPreviewBarItem37";
            superToolTip91.FixedTooltipWidth = true;
            toolTipTitleItem92.Text = "Export to HTML";
            toolTipItem91.LeftIndent = 6;
            toolTipItem91.Text = "Export the document to HTML and save it to the file on a disk.";
            superToolTip91.Items.Add(toolTipTitleItem92);
            superToolTip91.Items.Add(toolTipItem91);
            superToolTip91.MaxWidth = 210;
            this.printPreviewBarItem37.SuperTip = superToolTip91;
            // 
            // printPreviewBarItem38
            // 
            this.printPreviewBarItem38.Caption = "Text File";
            this.printPreviewBarItem38.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt;
            this.printPreviewBarItem38.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem38.Description = "Plain Text";
            this.printPreviewBarItem38.Enabled = false;
            this.printPreviewBarItem38.Id = 37;
            this.printPreviewBarItem38.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportTxt;
            this.printPreviewBarItem38.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportTxtLarge;
            this.printPreviewBarItem38.Name = "printPreviewBarItem38";
            superToolTip92.FixedTooltipWidth = true;
            toolTipTitleItem93.Text = "Export to Text";
            toolTipItem92.LeftIndent = 6;
            toolTipItem92.Text = "Export the document to Text and save it to the file on a disk.";
            superToolTip92.Items.Add(toolTipTitleItem93);
            superToolTip92.Items.Add(toolTipItem92);
            superToolTip92.MaxWidth = 210;
            this.printPreviewBarItem38.SuperTip = superToolTip92;
            // 
            // printPreviewBarItem39
            // 
            this.printPreviewBarItem39.Caption = "CSV File";
            this.printPreviewBarItem39.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv;
            this.printPreviewBarItem39.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem39.Description = "Comma-Separated Values Text";
            this.printPreviewBarItem39.Enabled = false;
            this.printPreviewBarItem39.Id = 38;
            this.printPreviewBarItem39.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportCsv;
            this.printPreviewBarItem39.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportCsvLarge;
            this.printPreviewBarItem39.Name = "printPreviewBarItem39";
            superToolTip93.FixedTooltipWidth = true;
            toolTipTitleItem94.Text = "Export to CSV";
            toolTipItem93.LeftIndent = 6;
            toolTipItem93.Text = "Export the document to CSV and save it to the file on a disk.";
            superToolTip93.Items.Add(toolTipTitleItem94);
            superToolTip93.Items.Add(toolTipItem93);
            superToolTip93.MaxWidth = 210;
            this.printPreviewBarItem39.SuperTip = superToolTip93;
            // 
            // printPreviewBarItem40
            // 
            this.printPreviewBarItem40.Caption = "MHT File";
            this.printPreviewBarItem40.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht;
            this.printPreviewBarItem40.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem40.Description = "Single File Web Page";
            this.printPreviewBarItem40.Enabled = false;
            this.printPreviewBarItem40.Id = 39;
            this.printPreviewBarItem40.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportMht;
            this.printPreviewBarItem40.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportMhtLarge;
            this.printPreviewBarItem40.Name = "printPreviewBarItem40";
            superToolTip94.FixedTooltipWidth = true;
            toolTipTitleItem95.Text = "Export to MHT";
            toolTipItem94.LeftIndent = 6;
            toolTipItem94.Text = "Export the document to MHT and save it to the file on a disk.";
            superToolTip94.Items.Add(toolTipTitleItem95);
            superToolTip94.Items.Add(toolTipItem94);
            superToolTip94.MaxWidth = 210;
            this.printPreviewBarItem40.SuperTip = superToolTip94;
            // 
            // printPreviewBarItem41
            // 
            this.printPreviewBarItem41.Caption = "Excel File";
            this.printPreviewBarItem41.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls;
            this.printPreviewBarItem41.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem41.Description = "Microsoft Excel Workbook";
            this.printPreviewBarItem41.Enabled = false;
            this.printPreviewBarItem41.Id = 40;
            this.printPreviewBarItem41.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportXls;
            this.printPreviewBarItem41.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportXlsLarge;
            this.printPreviewBarItem41.Name = "printPreviewBarItem41";
            superToolTip95.FixedTooltipWidth = true;
            toolTipTitleItem96.Text = "Export to XLS";
            toolTipItem95.LeftIndent = 6;
            toolTipItem95.Text = "Export the document to XLS and save it to the file on a disk.";
            superToolTip95.Items.Add(toolTipTitleItem96);
            superToolTip95.Items.Add(toolTipItem95);
            superToolTip95.MaxWidth = 210;
            this.printPreviewBarItem41.SuperTip = superToolTip95;
            // 
            // printPreviewBarItem42
            // 
            this.printPreviewBarItem42.Caption = "RTF File";
            this.printPreviewBarItem42.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf;
            this.printPreviewBarItem42.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem42.Description = "Rich Text Format";
            this.printPreviewBarItem42.Enabled = false;
            this.printPreviewBarItem42.Id = 41;
            this.printPreviewBarItem42.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportRtf;
            this.printPreviewBarItem42.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportRtfLarge;
            this.printPreviewBarItem42.Name = "printPreviewBarItem42";
            superToolTip96.FixedTooltipWidth = true;
            toolTipTitleItem97.Text = "Export to RTF";
            toolTipItem96.LeftIndent = 6;
            toolTipItem96.Text = "Export the document to RTF and save it to the file on a disk.";
            superToolTip96.Items.Add(toolTipTitleItem97);
            superToolTip96.Items.Add(toolTipItem96);
            superToolTip96.MaxWidth = 210;
            this.printPreviewBarItem42.SuperTip = superToolTip96;
            // 
            // printPreviewBarItem43
            // 
            this.printPreviewBarItem43.Caption = "Image File";
            this.printPreviewBarItem43.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic;
            this.printPreviewBarItem43.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem43.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.printPreviewBarItem43.Enabled = false;
            this.printPreviewBarItem43.Id = 42;
            this.printPreviewBarItem43.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportGraphic;
            this.printPreviewBarItem43.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportGraphicLarge;
            this.printPreviewBarItem43.Name = "printPreviewBarItem43";
            superToolTip97.FixedTooltipWidth = true;
            toolTipTitleItem98.Text = "Export to Image";
            toolTipItem97.LeftIndent = 6;
            toolTipItem97.Text = "Export the document to Image and save it to the file on a disk.";
            superToolTip97.Items.Add(toolTipTitleItem98);
            superToolTip97.Items.Add(toolTipItem97);
            superToolTip97.MaxWidth = 210;
            this.printPreviewBarItem43.SuperTip = superToolTip97;
            // 
            // printPreviewBarItem44
            // 
            this.printPreviewBarItem44.Caption = "Open";
            this.printPreviewBarItem44.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Open;
            this.printPreviewBarItem44.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem44.Enabled = false;
            this.printPreviewBarItem44.Id = 43;
            this.printPreviewBarItem44.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Open;
            this.printPreviewBarItem44.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_OpenLarge;
            this.printPreviewBarItem44.Name = "printPreviewBarItem44";
            superToolTip98.FixedTooltipWidth = true;
            toolTipTitleItem99.Text = "Open (Ctrl + O)";
            toolTipItem98.LeftIndent = 6;
            toolTipItem98.Text = "Open a document.";
            superToolTip98.Items.Add(toolTipTitleItem99);
            superToolTip98.Items.Add(toolTipItem98);
            superToolTip98.MaxWidth = 210;
            this.printPreviewBarItem44.SuperTip = superToolTip98;
            // 
            // printPreviewBarItem45
            // 
            this.printPreviewBarItem45.Caption = "Save";
            this.printPreviewBarItem45.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Save;
            this.printPreviewBarItem45.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem45.Enabled = false;
            this.printPreviewBarItem45.Id = 44;
            this.printPreviewBarItem45.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.printPreviewBarItem45.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SaveLarge;
            this.printPreviewBarItem45.Name = "printPreviewBarItem45";
            superToolTip99.FixedTooltipWidth = true;
            toolTipTitleItem100.Text = "Save (Ctrl + S)";
            toolTipItem99.LeftIndent = 6;
            toolTipItem99.Text = "Save the document.";
            superToolTip99.Items.Add(toolTipTitleItem100);
            superToolTip99.Items.Add(toolTipItem99);
            superToolTip99.MaxWidth = 210;
            this.printPreviewBarItem45.SuperTip = superToolTip99;
            // 
            // printPreviewStaticItem1
            // 
            this.printPreviewStaticItem1.Caption = "Nothing";
            this.printPreviewStaticItem1.Id = 49;
            this.printPreviewStaticItem1.LeftIndent = 1;
            this.printPreviewStaticItem1.Name = "printPreviewStaticItem1";
            this.printPreviewStaticItem1.RightIndent = 1;
            this.printPreviewStaticItem1.Type = "PageOfPages";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 50;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // progressBarEditItem1
            // 
            this.progressBarEditItem1.ContextSpecifier = this.printRibbonController1;
            this.progressBarEditItem1.Edit = this.repositoryItemProgressBar1;
            this.progressBarEditItem1.EditHeight = 12;
            this.progressBarEditItem1.EditWidth = 150;
            this.progressBarEditItem1.Id = 51;
            this.progressBarEditItem1.Name = "progressBarEditItem1";
            this.progressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // printPreviewBarItem46
            // 
            this.printPreviewBarItem46.Caption = "Stop";
            this.printPreviewBarItem46.Command = DevExpress.XtraPrinting.PrintingSystemCommand.StopPageBuilding;
            this.printPreviewBarItem46.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem46.Enabled = false;
            this.printPreviewBarItem46.Hint = "Stop";
            this.printPreviewBarItem46.Id = 52;
            this.printPreviewBarItem46.Name = "printPreviewBarItem46";
            this.printPreviewBarItem46.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonItem1.Enabled = false;
            this.barButtonItem1.Id = 53;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // printPreviewStaticItem2
            // 
            this.printPreviewStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.printPreviewStaticItem2.Caption = "100%";
            this.printPreviewStaticItem2.Id = 54;
            this.printPreviewStaticItem2.Name = "printPreviewStaticItem2";
            this.printPreviewStaticItem2.Type = "ZoomFactorText";
            // 
            // zoomTrackBarEditItem1
            // 
            this.zoomTrackBarEditItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.zoomTrackBarEditItem1.ContextSpecifier = this.printRibbonController1;
            this.zoomTrackBarEditItem1.Edit = this.repositoryItemZoomTrackBar1;
            this.zoomTrackBarEditItem1.EditValue = 90;
            this.zoomTrackBarEditItem1.EditWidth = 140;
            this.zoomTrackBarEditItem1.Enabled = false;
            this.zoomTrackBarEditItem1.Id = 55;
            this.zoomTrackBarEditItem1.Name = "zoomTrackBarEditItem1";
            this.zoomTrackBarEditItem1.Range = new int[] {
        10,
        500};
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.AllowFocused = false;
            this.repositoryItemZoomTrackBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemZoomTrackBar1.Maximum = 180;
            this.repositoryItemZoomTrackBar1.Middle = 90;
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            // 
            // printPreviewRibbonPage1
            // 
            this.printPreviewRibbonPage1.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.printPreviewRibbonPageGroup1,
            this.printPreviewRibbonPageGroup2,
            this.printPreviewRibbonPageGroup3,
            this.printPreviewRibbonPageGroup4,
            this.printPreviewRibbonPageGroup5,
            this.printPreviewRibbonPageGroup6,
            this.printPreviewRibbonPageGroup7});
            this.printPreviewRibbonPage1.Name = "printPreviewRibbonPage1";
            this.printPreviewRibbonPage1.Text = "Print Preview";
            // 
            // printPreviewRibbonPageGroup1
            // 
            this.printPreviewRibbonPageGroup1.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup1.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Document;
            this.printPreviewRibbonPageGroup1.ItemLinks.Add(this.printPreviewBarItem44);
            this.printPreviewRibbonPageGroup1.ItemLinks.Add(this.printPreviewBarItem45);
            this.printPreviewRibbonPageGroup1.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Document;
            this.printPreviewRibbonPageGroup1.Name = "printPreviewRibbonPageGroup1";
            this.printPreviewRibbonPageGroup1.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup1.Text = "Document";
            // 
            // printPreviewRibbonPageGroup2
            // 
            this.printPreviewRibbonPageGroup2.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup2.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem5);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem6);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem4);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem2);
            this.printPreviewRibbonPageGroup2.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Print;
            this.printPreviewRibbonPageGroup2.Name = "printPreviewRibbonPageGroup2";
            this.printPreviewRibbonPageGroup2.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup2.Text = "Print";
            // 
            // printPreviewRibbonPageGroup3
            // 
            this.printPreviewRibbonPageGroup3.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup3.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageMargins;
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem8);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem9);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem28);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem26);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem27);
            this.printPreviewRibbonPageGroup3.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.PageSetup;
            this.printPreviewRibbonPageGroup3.Name = "printPreviewRibbonPageGroup3";
            superToolTip100.FixedTooltipWidth = true;
            toolTipTitleItem101.Text = "Page Setup";
            toolTipItem100.Appearance.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageSetupDialog;
            toolTipItem100.Appearance.Options.UseImage = true;
            toolTipItem100.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageSetupDialog;
            toolTipItem100.LeftIndent = 6;
            toolTipItem100.Text = "Show the Page Setup dialog.";
            superToolTip100.Items.Add(toolTipTitleItem101);
            superToolTip100.Items.Add(toolTipItem100);
            superToolTip100.MaxWidth = 318;
            this.printPreviewRibbonPageGroup3.SuperTip = superToolTip100;
            this.printPreviewRibbonPageGroup3.Text = "Page Setup";
            // 
            // printPreviewRibbonPageGroup4
            // 
            this.printPreviewRibbonPageGroup4.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup4.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Find;
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem3);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem1);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem16, true);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem17);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem18);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem19);
            this.printPreviewRibbonPageGroup4.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Navigation;
            this.printPreviewRibbonPageGroup4.Name = "printPreviewRibbonPageGroup4";
            this.printPreviewRibbonPageGroup4.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup4.Text = "Navigation";
            // 
            // printPreviewRibbonPageGroup5
            // 
            this.printPreviewRibbonPageGroup5.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup5.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Zoom;
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem10);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem11);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem12);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem20);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem13);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem15);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem14);
            this.printPreviewRibbonPageGroup5.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Zoom;
            this.printPreviewRibbonPageGroup5.Name = "printPreviewRibbonPageGroup5";
            this.printPreviewRibbonPageGroup5.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup5.Text = "Zoom";
            // 
            // printPreviewRibbonPageGroup6
            // 
            this.printPreviewRibbonPageGroup6.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup6.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Watermark;
            this.printPreviewRibbonPageGroup6.ItemLinks.Add(this.printPreviewBarItem21);
            this.printPreviewRibbonPageGroup6.ItemLinks.Add(this.printPreviewBarItem22);
            this.printPreviewRibbonPageGroup6.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Background;
            this.printPreviewRibbonPageGroup6.Name = "printPreviewRibbonPageGroup6";
            this.printPreviewRibbonPageGroup6.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup6.Text = "Page Background";
            // 
            // printPreviewRibbonPageGroup7
            // 
            this.printPreviewRibbonPageGroup7.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup7.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportFile;
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem23);
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem24);
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem25, true);
            this.printPreviewRibbonPageGroup7.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Export;
            this.printPreviewRibbonPageGroup7.Name = "printPreviewRibbonPageGroup7";
            this.printPreviewRibbonPageGroup7.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup7.Text = "Export";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.printPreviewStaticItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem1, true);
            this.ribbonStatusBar1.ItemLinks.Add(this.progressBarEditItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.printPreviewBarItem46);
            this.ribbonStatusBar1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.printPreviewStaticItem2);
            this.ribbonStatusBar1.ItemLinks.Add(this.zoomTrackBarEditItem1);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 784);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1427, 23);
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("ef9f3537-823d-4b91-84cb-b30d9588a2ef");
            this.dockPanel1.Location = new System.Drawing.Point(0, 142);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(444, 200);
            this.dockPanel1.Size = new System.Drawing.Size(444, 642);
            this.dockPanel1.Text = "Data Supply";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.xtraTabControl1);
            this.dockPanel1_Container.Controls.Add(this.layoutControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(437, 610);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(3, 3);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(436, 552);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.layoutControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(431, 526);
            this.xtraTabPage1.Text = "Trees";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl2.Controls.Add(this.btnLoadTrees);
            this.layoutControl2.Controls.Add(this.gridSplitContainer1);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.MenuManager = this.barManager1;
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(891, 441, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(431, 526);
            this.layoutControl2.TabIndex = 5;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.Location = new System.Drawing.Point(36, 2);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(306, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl2;
            this.buttonEditClientFilter.TabIndex = 30;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // btnLoadTrees
            // 
            this.btnLoadTrees.ImageOptions.ImageIndex = 0;
            this.btnLoadTrees.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadTrees.Location = new System.Drawing.Point(346, 2);
            this.btnLoadTrees.Name = "btnLoadTrees";
            this.btnLoadTrees.Size = new System.Drawing.Size(83, 22);
            this.btnLoadTrees.StyleController = this.layoutControl2;
            this.btnLoadTrees.TabIndex = 6;
            this.btnLoadTrees.Text = "Load Trees";
            this.btnLoadTrees.Click += new System.EventHandler(this.btnLoadTrees_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "refresh_16x16");
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(2, 28);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(427, 496);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01220ATReportsListingsTreeFilterBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(427, 496);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01220ATReportsListingsTreeFilterBindingSource
            // 
            this.sp01220ATReportsListingsTreeFilterBindingSource.DataMember = "sp01220_AT_Reports_Listings_Tree_Filter";
            this.sp01220ATReportsListingsTreeFilterBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeID,
            this.colSiteID,
            this.colClientID1,
            this.colSiteName,
            this.colClientName1,
            this.colTreeReference,
            this.colMappingID,
            this.colGridReference,
            this.colDistance,
            this.colHouseName,
            this.colSpeciesName,
            this.colAccess,
            this.colVisibility,
            this.colContext,
            this.colManagement,
            this.colLegalStatus,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnit,
            this.colNextInspectionDate,
            this.colGroundType,
            this.colDBH,
            this.colDBHRange,
            this.colHeight,
            this.colHeightRange,
            this.colProtectionType,
            this.colCrownDiameter,
            this.colPlantDate,
            this.colGroupNumber,
            this.colRiskCategory,
            this.colSiteHazardClass,
            this.colPlantSize,
            this.colPlantSource,
            this.colPlantMethod,
            this.colPostcode,
            this.colMapLabel,
            this.colStatus,
            this.colSize,
            this.colTarget1,
            this.colTarget2,
            this.colTarget3,
            this.colLastModifiedDate,
            this.colRemarks,
            this.colUser1,
            this.colUser2,
            this.colUser3,
            this.colAgeClass,
            this.colX,
            this.colY,
            this.colAreaHa,
            this.colReplantCount,
            this.colSurveyDate,
            this.colTreeType,
            this.colHedgeOwner,
            this.colHedgeLength,
            this.colGardenSize,
            this.colPropertyProximity,
            this.colSiteLevel,
            this.colSituation,
            this.colRiskFactor,
            this.colPolygonXY,
            this.colcolor,
            this.colOwnershipName,
            this.colLinkedInspectionCount,
            this.Calculated1,
            this.Calculated2,
            this.Calculated3,
            this.colCavat,
            this.colStemCount,
            this.colCrownNorth,
            this.colCrownSouth,
            this.colCrownEast,
            this.colCrownWest,
            this.colRetentionCategory,
            this.colSULE,
            this.colSpeciesVariety,
            this.colUserPicklist1,
            this.colUserPicklist2,
            this.colUserPicklist3,
            this.colObjectLength,
            this.colObjectWidth});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 513, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "SiteID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 0;
            this.colTreeReference.Width = 112;
            // 
            // colMappingID
            // 
            this.colMappingID.Caption = "Map ID";
            this.colMappingID.FieldName = "MappingID";
            this.colMappingID.Name = "colMappingID";
            this.colMappingID.OptionsColumn.AllowEdit = false;
            this.colMappingID.OptionsColumn.AllowFocus = false;
            this.colMappingID.OptionsColumn.ReadOnly = true;
            // 
            // colGridReference
            // 
            this.colGridReference.Caption = "Grid Reference";
            this.colGridReference.FieldName = "GridReference";
            this.colGridReference.Name = "colGridReference";
            this.colGridReference.OptionsColumn.AllowEdit = false;
            this.colGridReference.OptionsColumn.AllowFocus = false;
            this.colGridReference.OptionsColumn.ReadOnly = true;
            this.colGridReference.Visible = true;
            this.colGridReference.VisibleIndex = 5;
            this.colGridReference.Width = 113;
            // 
            // colDistance
            // 
            this.colDistance.Caption = "Distance";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            // 
            // colHouseName
            // 
            this.colHouseName.Caption = "House Name";
            this.colHouseName.FieldName = "HouseName";
            this.colHouseName.Name = "colHouseName";
            this.colHouseName.OptionsColumn.AllowEdit = false;
            this.colHouseName.OptionsColumn.AllowFocus = false;
            this.colHouseName.OptionsColumn.ReadOnly = true;
            this.colHouseName.Visible = true;
            this.colHouseName.VisibleIndex = 3;
            this.colHouseName.Width = 99;
            // 
            // colSpeciesName
            // 
            this.colSpeciesName.Caption = "Species";
            this.colSpeciesName.FieldName = "SpeciesName";
            this.colSpeciesName.Name = "colSpeciesName";
            this.colSpeciesName.OptionsColumn.AllowEdit = false;
            this.colSpeciesName.OptionsColumn.AllowFocus = false;
            this.colSpeciesName.OptionsColumn.ReadOnly = true;
            this.colSpeciesName.Visible = true;
            this.colSpeciesName.VisibleIndex = 14;
            // 
            // colAccess
            // 
            this.colAccess.Caption = "Access";
            this.colAccess.FieldName = "Access";
            this.colAccess.Name = "colAccess";
            this.colAccess.OptionsColumn.AllowEdit = false;
            this.colAccess.OptionsColumn.AllowFocus = false;
            this.colAccess.OptionsColumn.ReadOnly = true;
            // 
            // colVisibility
            // 
            this.colVisibility.Caption = "Visibility";
            this.colVisibility.FieldName = "Visibility";
            this.colVisibility.Name = "colVisibility";
            this.colVisibility.OptionsColumn.AllowEdit = false;
            this.colVisibility.OptionsColumn.AllowFocus = false;
            this.colVisibility.OptionsColumn.ReadOnly = true;
            // 
            // colContext
            // 
            this.colContext.Caption = "Context";
            this.colContext.FieldName = "Context";
            this.colContext.Name = "colContext";
            this.colContext.OptionsColumn.AllowEdit = false;
            this.colContext.OptionsColumn.AllowFocus = false;
            this.colContext.OptionsColumn.ReadOnly = true;
            // 
            // colManagement
            // 
            this.colManagement.Caption = "Management";
            this.colManagement.FieldName = "Management";
            this.colManagement.Name = "colManagement";
            this.colManagement.OptionsColumn.AllowEdit = false;
            this.colManagement.OptionsColumn.AllowFocus = false;
            this.colManagement.OptionsColumn.ReadOnly = true;
            this.colManagement.Width = 88;
            // 
            // colLegalStatus
            // 
            this.colLegalStatus.Caption = "Legal Status";
            this.colLegalStatus.FieldName = "LegalStatus";
            this.colLegalStatus.Name = "colLegalStatus";
            this.colLegalStatus.OptionsColumn.AllowEdit = false;
            this.colLegalStatus.OptionsColumn.AllowFocus = false;
            this.colLegalStatus.OptionsColumn.ReadOnly = true;
            this.colLegalStatus.Visible = true;
            this.colLegalStatus.VisibleIndex = 24;
            this.colLegalStatus.Width = 92;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection";
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 10;
            this.colLastInspectionDate.Width = 98;
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Inspection Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.Width = 99;
            // 
            // colInspectionUnit
            // 
            this.colInspectionUnit.Caption = "Inspection Unit";
            this.colInspectionUnit.FieldName = "InspectionUnit";
            this.colInspectionUnit.Name = "colInspectionUnit";
            this.colInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colInspectionUnit.Width = 96;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection";
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 11;
            this.colNextInspectionDate.Width = 99;
            // 
            // colGroundType
            // 
            this.colGroundType.Caption = "Ground Type";
            this.colGroundType.FieldName = "GroundType";
            this.colGroundType.Name = "colGroundType";
            this.colGroundType.OptionsColumn.AllowEdit = false;
            this.colGroundType.OptionsColumn.AllowFocus = false;
            this.colGroundType.OptionsColumn.ReadOnly = true;
            this.colGroundType.Visible = true;
            this.colGroundType.VisibleIndex = 22;
            this.colGroundType.Width = 91;
            // 
            // colDBH
            // 
            this.colDBH.Caption = "DBH";
            this.colDBH.FieldName = "DBH";
            this.colDBH.Name = "colDBH";
            this.colDBH.OptionsColumn.AllowEdit = false;
            this.colDBH.OptionsColumn.AllowFocus = false;
            this.colDBH.OptionsColumn.ReadOnly = true;
            this.colDBH.Visible = true;
            this.colDBH.VisibleIndex = 15;
            // 
            // colDBHRange
            // 
            this.colDBHRange.Caption = "DBH Range";
            this.colDBHRange.FieldName = "DBHRange";
            this.colDBHRange.Name = "colDBHRange";
            this.colDBHRange.OptionsColumn.AllowEdit = false;
            this.colDBHRange.OptionsColumn.AllowFocus = false;
            this.colDBHRange.OptionsColumn.ReadOnly = true;
            this.colDBHRange.Visible = true;
            this.colDBHRange.VisibleIndex = 16;
            // 
            // colHeight
            // 
            this.colHeight.Caption = "Height";
            this.colHeight.FieldName = "Height";
            this.colHeight.Name = "colHeight";
            this.colHeight.OptionsColumn.AllowEdit = false;
            this.colHeight.OptionsColumn.AllowFocus = false;
            this.colHeight.OptionsColumn.ReadOnly = true;
            this.colHeight.Visible = true;
            this.colHeight.VisibleIndex = 17;
            // 
            // colHeightRange
            // 
            this.colHeightRange.Caption = "Height Range";
            this.colHeightRange.FieldName = "HeightRange";
            this.colHeightRange.Name = "colHeightRange";
            this.colHeightRange.OptionsColumn.AllowEdit = false;
            this.colHeightRange.OptionsColumn.AllowFocus = false;
            this.colHeightRange.OptionsColumn.ReadOnly = true;
            this.colHeightRange.Visible = true;
            this.colHeightRange.VisibleIndex = 18;
            this.colHeightRange.Width = 91;
            // 
            // colProtectionType
            // 
            this.colProtectionType.Caption = "Protection Type";
            this.colProtectionType.FieldName = "ProtectionType";
            this.colProtectionType.Name = "colProtectionType";
            this.colProtectionType.OptionsColumn.AllowEdit = false;
            this.colProtectionType.OptionsColumn.AllowFocus = false;
            this.colProtectionType.OptionsColumn.ReadOnly = true;
            this.colProtectionType.Width = 105;
            // 
            // colCrownDiameter
            // 
            this.colCrownDiameter.Caption = "Crown Diameter";
            this.colCrownDiameter.FieldName = "CrownDiameter";
            this.colCrownDiameter.Name = "colCrownDiameter";
            this.colCrownDiameter.OptionsColumn.AllowEdit = false;
            this.colCrownDiameter.OptionsColumn.AllowFocus = false;
            this.colCrownDiameter.OptionsColumn.ReadOnly = true;
            this.colCrownDiameter.Visible = true;
            this.colCrownDiameter.VisibleIndex = 20;
            this.colCrownDiameter.Width = 111;
            // 
            // colPlantDate
            // 
            this.colPlantDate.Caption = "Plant Date";
            this.colPlantDate.FieldName = "PlantDate";
            this.colPlantDate.Name = "colPlantDate";
            this.colPlantDate.OptionsColumn.AllowEdit = false;
            this.colPlantDate.OptionsColumn.AllowFocus = false;
            this.colPlantDate.OptionsColumn.ReadOnly = true;
            // 
            // colGroupNumber
            // 
            this.colGroupNumber.Caption = "Group Number";
            this.colGroupNumber.FieldName = "GroupNumber";
            this.colGroupNumber.Name = "colGroupNumber";
            this.colGroupNumber.OptionsColumn.AllowEdit = false;
            this.colGroupNumber.OptionsColumn.AllowFocus = false;
            this.colGroupNumber.OptionsColumn.ReadOnly = true;
            this.colGroupNumber.Visible = true;
            this.colGroupNumber.VisibleIndex = 12;
            this.colGroupNumber.Width = 96;
            // 
            // colRiskCategory
            // 
            this.colRiskCategory.Caption = "Risk Category";
            this.colRiskCategory.FieldName = "RiskCategory";
            this.colRiskCategory.Name = "colRiskCategory";
            this.colRiskCategory.OptionsColumn.AllowEdit = false;
            this.colRiskCategory.OptionsColumn.AllowFocus = false;
            this.colRiskCategory.OptionsColumn.ReadOnly = true;
            this.colRiskCategory.Visible = true;
            this.colRiskCategory.VisibleIndex = 2;
            this.colRiskCategory.Width = 95;
            // 
            // colSiteHazardClass
            // 
            this.colSiteHazardClass.Caption = "Site Hazard Class";
            this.colSiteHazardClass.FieldName = "SiteHazardClass";
            this.colSiteHazardClass.Name = "colSiteHazardClass";
            this.colSiteHazardClass.OptionsColumn.AllowEdit = false;
            this.colSiteHazardClass.OptionsColumn.AllowFocus = false;
            this.colSiteHazardClass.OptionsColumn.ReadOnly = true;
            this.colSiteHazardClass.Width = 115;
            // 
            // colPlantSize
            // 
            this.colPlantSize.Caption = "Plant Size";
            this.colPlantSize.FieldName = "PlantSize";
            this.colPlantSize.Name = "colPlantSize";
            this.colPlantSize.OptionsColumn.AllowEdit = false;
            this.colPlantSize.OptionsColumn.AllowFocus = false;
            this.colPlantSize.OptionsColumn.ReadOnly = true;
            // 
            // colPlantSource
            // 
            this.colPlantSource.Caption = "Plant Source";
            this.colPlantSource.FieldName = "PlantSource";
            this.colPlantSource.Name = "colPlantSource";
            this.colPlantSource.OptionsColumn.AllowEdit = false;
            this.colPlantSource.OptionsColumn.AllowFocus = false;
            this.colPlantSource.OptionsColumn.ReadOnly = true;
            this.colPlantSource.Width = 107;
            // 
            // colPlantMethod
            // 
            this.colPlantMethod.Caption = "Plant Method";
            this.colPlantMethod.FieldName = "PlantMethod";
            this.colPlantMethod.Name = "colPlantMethod";
            this.colPlantMethod.OptionsColumn.AllowEdit = false;
            this.colPlantMethod.OptionsColumn.AllowFocus = false;
            this.colPlantMethod.OptionsColumn.ReadOnly = true;
            this.colPlantMethod.Width = 110;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 4;
            // 
            // colMapLabel
            // 
            this.colMapLabel.Caption = "Map Label";
            this.colMapLabel.FieldName = "MapLabel";
            this.colMapLabel.Name = "colMapLabel";
            this.colMapLabel.OptionsColumn.AllowEdit = false;
            this.colMapLabel.OptionsColumn.AllowFocus = false;
            this.colMapLabel.OptionsColumn.ReadOnly = true;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 13;
            // 
            // colSize
            // 
            this.colSize.Caption = "Size";
            this.colSize.FieldName = "Size";
            this.colSize.Name = "colSize";
            this.colSize.OptionsColumn.AllowEdit = false;
            this.colSize.OptionsColumn.AllowFocus = false;
            this.colSize.OptionsColumn.ReadOnly = true;
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 19;
            // 
            // colTarget1
            // 
            this.colTarget1.Caption = "Target 1";
            this.colTarget1.FieldName = "Target1";
            this.colTarget1.Name = "colTarget1";
            this.colTarget1.OptionsColumn.AllowEdit = false;
            this.colTarget1.OptionsColumn.AllowFocus = false;
            this.colTarget1.OptionsColumn.ReadOnly = true;
            this.colTarget1.Visible = true;
            this.colTarget1.VisibleIndex = 7;
            // 
            // colTarget2
            // 
            this.colTarget2.Caption = "Target 2";
            this.colTarget2.FieldName = "Target2";
            this.colTarget2.Name = "colTarget2";
            this.colTarget2.OptionsColumn.AllowEdit = false;
            this.colTarget2.OptionsColumn.AllowFocus = false;
            this.colTarget2.OptionsColumn.ReadOnly = true;
            this.colTarget2.Visible = true;
            this.colTarget2.VisibleIndex = 8;
            // 
            // colTarget3
            // 
            this.colTarget3.Caption = "Target 3";
            this.colTarget3.FieldName = "Target3";
            this.colTarget3.Name = "colTarget3";
            this.colTarget3.OptionsColumn.AllowEdit = false;
            this.colTarget3.OptionsColumn.AllowFocus = false;
            this.colTarget3.OptionsColumn.ReadOnly = true;
            this.colTarget3.Visible = true;
            this.colTarget3.VisibleIndex = 9;
            // 
            // colLastModifiedDate
            // 
            this.colLastModifiedDate.Caption = "Last Modified";
            this.colLastModifiedDate.FieldName = "LastModifiedDate";
            this.colLastModifiedDate.Name = "colLastModifiedDate";
            this.colLastModifiedDate.OptionsColumn.AllowEdit = false;
            this.colLastModifiedDate.OptionsColumn.AllowFocus = false;
            this.colLastModifiedDate.OptionsColumn.ReadOnly = true;
            this.colLastModifiedDate.Width = 112;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 27;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            // 
            // colUser1
            // 
            this.colUser1.Caption = "User 1";
            this.colUser1.FieldName = "User1";
            this.colUser1.Name = "colUser1";
            this.colUser1.OptionsColumn.AllowEdit = false;
            this.colUser1.OptionsColumn.AllowFocus = false;
            this.colUser1.OptionsColumn.ReadOnly = true;
            // 
            // colUser2
            // 
            this.colUser2.Caption = "User 2";
            this.colUser2.FieldName = "User2";
            this.colUser2.Name = "colUser2";
            this.colUser2.OptionsColumn.AllowEdit = false;
            this.colUser2.OptionsColumn.AllowFocus = false;
            this.colUser2.OptionsColumn.ReadOnly = true;
            // 
            // colUser3
            // 
            this.colUser3.Caption = "User 3";
            this.colUser3.FieldName = "User3";
            this.colUser3.Name = "colUser3";
            this.colUser3.OptionsColumn.AllowEdit = false;
            this.colUser3.OptionsColumn.AllowFocus = false;
            this.colUser3.OptionsColumn.ReadOnly = true;
            // 
            // colAgeClass
            // 
            this.colAgeClass.Caption = "Age Class";
            this.colAgeClass.FieldName = "AgeClass";
            this.colAgeClass.Name = "colAgeClass";
            this.colAgeClass.OptionsColumn.AllowEdit = false;
            this.colAgeClass.OptionsColumn.AllowFocus = false;
            this.colAgeClass.OptionsColumn.ReadOnly = true;
            this.colAgeClass.Visible = true;
            this.colAgeClass.VisibleIndex = 21;
            // 
            // colX
            // 
            this.colX.Caption = "X";
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.OptionsColumn.AllowEdit = false;
            this.colX.OptionsColumn.AllowFocus = false;
            this.colX.OptionsColumn.ReadOnly = true;
            // 
            // colY
            // 
            this.colY.Caption = "Y";
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.OptionsColumn.AllowEdit = false;
            this.colY.OptionsColumn.AllowFocus = false;
            this.colY.OptionsColumn.ReadOnly = true;
            // 
            // colAreaHa
            // 
            this.colAreaHa.Caption = "Area [M�]";
            this.colAreaHa.FieldName = "AreaHa";
            this.colAreaHa.Name = "colAreaHa";
            this.colAreaHa.OptionsColumn.AllowEdit = false;
            this.colAreaHa.OptionsColumn.AllowFocus = false;
            this.colAreaHa.OptionsColumn.ReadOnly = true;
            // 
            // colReplantCount
            // 
            this.colReplantCount.Caption = "Replant Count";
            this.colReplantCount.FieldName = "ReplantCount";
            this.colReplantCount.Name = "colReplantCount";
            this.colReplantCount.OptionsColumn.AllowEdit = false;
            this.colReplantCount.OptionsColumn.AllowFocus = false;
            this.colReplantCount.OptionsColumn.ReadOnly = true;
            this.colReplantCount.Width = 106;
            // 
            // colSurveyDate
            // 
            this.colSurveyDate.Caption = "Survey Date";
            this.colSurveyDate.FieldName = "SurveyDate";
            this.colSurveyDate.Name = "colSurveyDate";
            this.colSurveyDate.OptionsColumn.AllowEdit = false;
            this.colSurveyDate.OptionsColumn.AllowFocus = false;
            this.colSurveyDate.OptionsColumn.ReadOnly = true;
            this.colSurveyDate.Width = 115;
            // 
            // colTreeType
            // 
            this.colTreeType.Caption = "Tree Type";
            this.colTreeType.FieldName = "TreeType";
            this.colTreeType.Name = "colTreeType";
            this.colTreeType.OptionsColumn.AllowEdit = false;
            this.colTreeType.OptionsColumn.AllowFocus = false;
            this.colTreeType.OptionsColumn.ReadOnly = true;
            this.colTreeType.Visible = true;
            this.colTreeType.VisibleIndex = 23;
            // 
            // colHedgeOwner
            // 
            this.colHedgeOwner.Caption = "Hedge Owner";
            this.colHedgeOwner.FieldName = "HedgeOwner";
            this.colHedgeOwner.Name = "colHedgeOwner";
            this.colHedgeOwner.OptionsColumn.AllowEdit = false;
            this.colHedgeOwner.OptionsColumn.AllowFocus = false;
            this.colHedgeOwner.OptionsColumn.ReadOnly = true;
            this.colHedgeOwner.Width = 115;
            // 
            // colHedgeLength
            // 
            this.colHedgeLength.Caption = "Hedge Length";
            this.colHedgeLength.FieldName = "HedgeLength";
            this.colHedgeLength.Name = "colHedgeLength";
            this.colHedgeLength.OptionsColumn.AllowEdit = false;
            this.colHedgeLength.OptionsColumn.AllowFocus = false;
            this.colHedgeLength.OptionsColumn.ReadOnly = true;
            this.colHedgeLength.Width = 126;
            // 
            // colGardenSize
            // 
            this.colGardenSize.Caption = "Garden Size";
            this.colGardenSize.FieldName = "GardenSize";
            this.colGardenSize.Name = "colGardenSize";
            this.colGardenSize.OptionsColumn.AllowEdit = false;
            this.colGardenSize.OptionsColumn.AllowFocus = false;
            this.colGardenSize.OptionsColumn.ReadOnly = true;
            this.colGardenSize.Width = 125;
            // 
            // colPropertyProximity
            // 
            this.colPropertyProximity.Caption = "Property Proximity";
            this.colPropertyProximity.FieldName = "PropertyProximity";
            this.colPropertyProximity.Name = "colPropertyProximity";
            this.colPropertyProximity.OptionsColumn.AllowEdit = false;
            this.colPropertyProximity.OptionsColumn.AllowFocus = false;
            this.colPropertyProximity.OptionsColumn.ReadOnly = true;
            this.colPropertyProximity.Width = 117;
            // 
            // colSiteLevel
            // 
            this.colSiteLevel.Caption = "Site Level";
            this.colSiteLevel.FieldName = "SiteLevel";
            this.colSiteLevel.Name = "colSiteLevel";
            this.colSiteLevel.OptionsColumn.AllowEdit = false;
            this.colSiteLevel.OptionsColumn.AllowFocus = false;
            this.colSiteLevel.OptionsColumn.ReadOnly = true;
            // 
            // colSituation
            // 
            this.colSituation.Caption = "Situation";
            this.colSituation.FieldName = "Situation";
            this.colSituation.Name = "colSituation";
            this.colSituation.OptionsColumn.AllowEdit = false;
            this.colSituation.OptionsColumn.AllowFocus = false;
            this.colSituation.OptionsColumn.ReadOnly = true;
            this.colSituation.Visible = true;
            this.colSituation.VisibleIndex = 6;
            this.colSituation.Width = 105;
            // 
            // colRiskFactor
            // 
            this.colRiskFactor.Caption = "Risk Factor";
            this.colRiskFactor.FieldName = "RiskFactor";
            this.colRiskFactor.Name = "colRiskFactor";
            this.colRiskFactor.OptionsColumn.AllowEdit = false;
            this.colRiskFactor.OptionsColumn.AllowFocus = false;
            this.colRiskFactor.OptionsColumn.ReadOnly = true;
            this.colRiskFactor.Visible = true;
            this.colRiskFactor.VisibleIndex = 1;
            // 
            // colPolygonXY
            // 
            this.colPolygonXY.Caption = "Polygon X/Y";
            this.colPolygonXY.FieldName = "PolygonXY";
            this.colPolygonXY.Name = "colPolygonXY";
            this.colPolygonXY.OptionsColumn.AllowEdit = false;
            this.colPolygonXY.OptionsColumn.AllowFocus = false;
            this.colPolygonXY.OptionsColumn.ReadOnly = true;
            // 
            // colcolor
            // 
            this.colcolor.Caption = "Color";
            this.colcolor.FieldName = "color";
            this.colcolor.Name = "colcolor";
            this.colcolor.OptionsColumn.AllowEdit = false;
            this.colcolor.OptionsColumn.AllowFocus = false;
            this.colcolor.OptionsColumn.ReadOnly = true;
            this.colcolor.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.OptionsColumn.AllowEdit = false;
            this.colOwnershipName.OptionsColumn.AllowFocus = false;
            this.colOwnershipName.OptionsColumn.ReadOnly = true;
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 25;
            this.colOwnershipName.Width = 103;
            // 
            // colLinkedInspectionCount
            // 
            this.colLinkedInspectionCount.Caption = "Inspection Count";
            this.colLinkedInspectionCount.FieldName = "LinkedInspectionCount";
            this.colLinkedInspectionCount.Name = "colLinkedInspectionCount";
            this.colLinkedInspectionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedInspectionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedInspectionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedInspectionCount.Visible = true;
            this.colLinkedInspectionCount.VisibleIndex = 26;
            this.colLinkedInspectionCount.Width = 104;
            // 
            // Calculated1
            // 
            this.Calculated1.Caption = "<b>Calculated 1</b>";
            this.Calculated1.FieldName = "Calculated1";
            this.Calculated1.Name = "Calculated1";
            this.Calculated1.OptionsColumn.AllowEdit = false;
            this.Calculated1.OptionsColumn.AllowFocus = false;
            this.Calculated1.OptionsColumn.ReadOnly = true;
            this.Calculated1.ShowUnboundExpressionMenu = true;
            this.Calculated1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1.Visible = true;
            this.Calculated1.VisibleIndex = 28;
            this.Calculated1.Width = 90;
            // 
            // Calculated2
            // 
            this.Calculated2.Caption = "<b>Calculated 2<b>";
            this.Calculated2.FieldName = "Calculated2";
            this.Calculated2.Name = "Calculated2";
            this.Calculated2.OptionsColumn.AllowEdit = false;
            this.Calculated2.OptionsColumn.AllowFocus = false;
            this.Calculated2.OptionsColumn.ReadOnly = true;
            this.Calculated2.ShowUnboundExpressionMenu = true;
            this.Calculated2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2.Visible = true;
            this.Calculated2.VisibleIndex = 29;
            this.Calculated2.Width = 90;
            // 
            // Calculated3
            // 
            this.Calculated3.Caption = "<b>Calculated 3</b>";
            this.Calculated3.FieldName = "Calculated3";
            this.Calculated3.Name = "Calculated3";
            this.Calculated3.OptionsColumn.AllowEdit = false;
            this.Calculated3.OptionsColumn.AllowFocus = false;
            this.Calculated3.OptionsColumn.ReadOnly = true;
            this.Calculated3.ShowUnboundExpressionMenu = true;
            this.Calculated3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3.Visible = true;
            this.Calculated3.VisibleIndex = 30;
            this.Calculated3.Width = 90;
            // 
            // colCavat
            // 
            this.colCavat.Caption = "Cavat";
            this.colCavat.FieldName = "Cavat";
            this.colCavat.Name = "colCavat";
            this.colCavat.OptionsColumn.AllowEdit = false;
            this.colCavat.OptionsColumn.AllowFocus = false;
            this.colCavat.OptionsColumn.ReadOnly = true;
            this.colCavat.Width = 50;
            // 
            // colStemCount
            // 
            this.colStemCount.Caption = "Stem Count";
            this.colStemCount.FieldName = "StemCount";
            this.colStemCount.Name = "colStemCount";
            this.colStemCount.OptionsColumn.AllowEdit = false;
            this.colStemCount.OptionsColumn.AllowFocus = false;
            this.colStemCount.OptionsColumn.ReadOnly = true;
            this.colStemCount.Width = 77;
            // 
            // colCrownNorth
            // 
            this.colCrownNorth.Caption = "Crown North";
            this.colCrownNorth.FieldName = "CrownNorth";
            this.colCrownNorth.Name = "colCrownNorth";
            this.colCrownNorth.OptionsColumn.AllowEdit = false;
            this.colCrownNorth.OptionsColumn.AllowFocus = false;
            this.colCrownNorth.OptionsColumn.ReadOnly = true;
            this.colCrownNorth.Width = 82;
            // 
            // colCrownSouth
            // 
            this.colCrownSouth.Caption = "Crown South";
            this.colCrownSouth.FieldName = "CrownSouth";
            this.colCrownSouth.Name = "colCrownSouth";
            this.colCrownSouth.OptionsColumn.AllowEdit = false;
            this.colCrownSouth.OptionsColumn.AllowFocus = false;
            this.colCrownSouth.OptionsColumn.ReadOnly = true;
            this.colCrownSouth.Width = 83;
            // 
            // colCrownEast
            // 
            this.colCrownEast.Caption = "Crown East";
            this.colCrownEast.FieldName = "CrownEast";
            this.colCrownEast.Name = "colCrownEast";
            this.colCrownEast.OptionsColumn.AllowEdit = false;
            this.colCrownEast.OptionsColumn.AllowFocus = false;
            this.colCrownEast.OptionsColumn.ReadOnly = true;
            this.colCrownEast.Width = 76;
            // 
            // colCrownWest
            // 
            this.colCrownWest.Caption = "Crown West";
            this.colCrownWest.FieldName = "CrownWest";
            this.colCrownWest.Name = "colCrownWest";
            this.colCrownWest.OptionsColumn.AllowEdit = false;
            this.colCrownWest.OptionsColumn.AllowFocus = false;
            this.colCrownWest.OptionsColumn.ReadOnly = true;
            this.colCrownWest.Width = 80;
            // 
            // colRetentionCategory
            // 
            this.colRetentionCategory.Caption = "Retention Category";
            this.colRetentionCategory.FieldName = "RetentionCategory";
            this.colRetentionCategory.Name = "colRetentionCategory";
            this.colRetentionCategory.OptionsColumn.AllowEdit = false;
            this.colRetentionCategory.OptionsColumn.AllowFocus = false;
            this.colRetentionCategory.OptionsColumn.ReadOnly = true;
            this.colRetentionCategory.Width = 116;
            // 
            // colSULE
            // 
            this.colSULE.Caption = "SULE";
            this.colSULE.FieldName = "SULE";
            this.colSULE.Name = "colSULE";
            this.colSULE.OptionsColumn.AllowEdit = false;
            this.colSULE.OptionsColumn.AllowFocus = false;
            this.colSULE.OptionsColumn.ReadOnly = true;
            this.colSULE.Width = 45;
            // 
            // colSpeciesVariety
            // 
            this.colSpeciesVariety.Caption = "Species Variety";
            this.colSpeciesVariety.FieldName = "SpeciesVariety";
            this.colSpeciesVariety.Name = "colSpeciesVariety";
            this.colSpeciesVariety.OptionsColumn.AllowEdit = false;
            this.colSpeciesVariety.OptionsColumn.AllowFocus = false;
            this.colSpeciesVariety.OptionsColumn.ReadOnly = true;
            this.colSpeciesVariety.Width = 94;
            // 
            // colUserPicklist1
            // 
            this.colUserPicklist1.Caption = "User Picklist 1";
            this.colUserPicklist1.FieldName = "UserPicklist1";
            this.colUserPicklist1.Name = "colUserPicklist1";
            this.colUserPicklist1.OptionsColumn.AllowEdit = false;
            this.colUserPicklist1.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1.OptionsColumn.ReadOnly = true;
            this.colUserPicklist1.Width = 86;
            // 
            // colUserPicklist2
            // 
            this.colUserPicklist2.Caption = "User Picklilst 2";
            this.colUserPicklist2.FieldName = "UserPicklist2";
            this.colUserPicklist2.Name = "colUserPicklist2";
            this.colUserPicklist2.OptionsColumn.AllowEdit = false;
            this.colUserPicklist2.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2.OptionsColumn.ReadOnly = true;
            this.colUserPicklist2.Width = 88;
            // 
            // colUserPicklist3
            // 
            this.colUserPicklist3.Caption = "User Picklist 3";
            this.colUserPicklist3.FieldName = "UserPicklist3";
            this.colUserPicklist3.Name = "colUserPicklist3";
            this.colUserPicklist3.OptionsColumn.AllowEdit = false;
            this.colUserPicklist3.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3.OptionsColumn.ReadOnly = true;
            this.colUserPicklist3.Width = 86;
            // 
            // colObjectLength
            // 
            this.colObjectLength.Caption = "Object Length";
            this.colObjectLength.FieldName = "ObjectLength";
            this.colObjectLength.Name = "colObjectLength";
            this.colObjectLength.OptionsColumn.AllowEdit = false;
            this.colObjectLength.OptionsColumn.AllowFocus = false;
            this.colObjectLength.OptionsColumn.ReadOnly = true;
            this.colObjectLength.Width = 89;
            // 
            // colObjectWidth
            // 
            this.colObjectWidth.Caption = "Object Width";
            this.colObjectWidth.FieldName = "ObjectWidth";
            this.colObjectWidth.Name = "colObjectWidth";
            this.colObjectWidth.OptionsColumn.AllowEdit = false;
            this.colObjectWidth.OptionsColumn.AllowFocus = false;
            this.colObjectWidth.OptionsColumn.ReadOnly = true;
            this.colObjectWidth.Width = 84;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem5,
            this.layoutControlItem19});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutControlGroup2.Size = new System.Drawing.Size(431, 526);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridSplitContainer1;
            this.layoutControlItem1.CustomizationFormText = "Grid 1:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(431, 500);
            this.layoutControlItem1.Text = "Tree Grid:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnLoadTrees;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(344, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(87, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(87, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(87, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.buttonEditClientFilter;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(344, 26);
            this.layoutControlItem19.Text = "Client:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(31, 13);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.layoutControl3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(431, 526);
            this.xtraTabPage2.Text = "Inspections";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.buttonEditClientFilter2);
            this.layoutControl3.Controls.Add(this.gridSplitContainer2);
            this.layoutControl3.Controls.Add(this.btnLoadInspections);
            this.layoutControl3.Controls.Add(this.deInspectionToDate);
            this.layoutControl3.Controls.Add(this.deInspectionFromDate);
            this.layoutControl3.Controls.Add(this.ceLastInspectionOnly);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.MenuManager = this.barManager1;
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(431, 526);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // buttonEditClientFilter2
            // 
            this.buttonEditClientFilter2.Location = new System.Drawing.Point(36, 26);
            this.buttonEditClientFilter2.MenuManager = this.barManager1;
            this.buttonEditClientFilter2.Name = "buttonEditClientFilter2";
            this.buttonEditClientFilter2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter2.Size = new System.Drawing.Size(393, 20);
            this.buttonEditClientFilter2.StyleController = this.layoutControl3;
            this.buttonEditClientFilter2.TabIndex = 31;
            this.buttonEditClientFilter2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter2_ButtonClick);
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(2, 76);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(427, 448);
            this.gridSplitContainer2.TabIndex = 9;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp01230ATReportsListingsInspectionFilterBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView4;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.gridControl2.Size = new System.Drawing.Size(427, 448);
            this.gridControl2.TabIndex = 9;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp01230ATReportsListingsInspectionFilterBindingSource
            // 
            this.sp01230ATReportsListingsInspectionFilterBindingSource.DataMember = "sp01230_AT_Reports_Listings_Inspection_Filter";
            this.sp01230ATReportsListingsInspectionFilterBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID,
            this.colTreeID1,
            this.colSiteID1,
            this.colClientID2,
            this.colClientName2,
            this.colSiteName1,
            this.colTreeReference1,
            this.colTreeMappingID,
            this.colTreeGridReference,
            this.colTreeDistance,
            this.colTreeHouseName,
            this.colTreePostcode,
            this.colInspectionReference,
            this.colInspectorName,
            this.colInspectionDate,
            this.colIncidentReferenceNumber,
            this.colIncidentID,
            this.colStemPhysical1,
            this.colStemPhysical2,
            this.colStemPhysical3,
            this.colStemDisease1,
            this.colStemDisease2,
            this.colStemDisease3,
            this.colAngleToVertical,
            this.colCrownPhysical1,
            this.colCrownPhysical2,
            this.colCrownPhysical3,
            this.colCrownDisease1,
            this.colCrownDisease2,
            this.colCrownDisease3,
            this.colCrownFoliation1,
            this.colCrownFoliation2,
            this.colCrownFoliation3,
            this.colRiskCategory1,
            this.colGeneralCondition,
            this.colRootHeave1,
            this.colRootHeave2,
            this.colRootHeave3,
            this.colDateLastModified,
            this.colRemarks1,
            this.colUser11,
            this.colUser21,
            this.colUser31,
            this.colcolor1,
            this.colLinkedActionCount,
            this.colLinkedOutstandingActionCount,
            this.colNoFurtherActionRequired,
            this.Calculated1b,
            this.Calculated2b,
            this.Calculated3b,
            this.colUserPicklist11,
            this.colUserPicklist21,
            this.colUserPicklist31});
            this.gridView4.GridControl = this.gridControl2;
            this.gridView4.GroupCount = 2;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsFind.AlwaysVisible = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseDown);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            this.colInspectionID.Width = 86;
            // 
            // colTreeID1
            // 
            this.colTreeID1.Caption = "Tree ID";
            this.colTreeID1.FieldName = "TreeID";
            this.colTreeID1.Name = "colTreeID1";
            this.colTreeID1.OptionsColumn.AllowEdit = false;
            this.colTreeID1.OptionsColumn.AllowFocus = false;
            this.colTreeID1.OptionsColumn.ReadOnly = true;
            this.colTreeID1.Width = 58;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Width = 72;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 69;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 85;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Width = 88;
            // 
            // colTreeReference1
            // 
            this.colTreeReference1.Caption = "Tree Reference";
            this.colTreeReference1.FieldName = "TreeReference";
            this.colTreeReference1.Name = "colTreeReference1";
            this.colTreeReference1.OptionsColumn.AllowEdit = false;
            this.colTreeReference1.OptionsColumn.AllowFocus = false;
            this.colTreeReference1.OptionsColumn.ReadOnly = true;
            this.colTreeReference1.Visible = true;
            this.colTreeReference1.VisibleIndex = 0;
            this.colTreeReference1.Width = 110;
            // 
            // colTreeMappingID
            // 
            this.colTreeMappingID.Caption = "Tree Mapping ID";
            this.colTreeMappingID.FieldName = "TreeMappingID";
            this.colTreeMappingID.Name = "colTreeMappingID";
            this.colTreeMappingID.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID.Width = 101;
            // 
            // colTreeGridReference
            // 
            this.colTreeGridReference.Caption = "Tree Grid Reference";
            this.colTreeGridReference.FieldName = "TreeGridReference";
            this.colTreeGridReference.Name = "colTreeGridReference";
            this.colTreeGridReference.OptionsColumn.AllowEdit = false;
            this.colTreeGridReference.OptionsColumn.AllowFocus = false;
            this.colTreeGridReference.OptionsColumn.ReadOnly = true;
            this.colTreeGridReference.Width = 119;
            // 
            // colTreeDistance
            // 
            this.colTreeDistance.Caption = "Tree Distance";
            this.colTreeDistance.FieldName = "TreeDistance";
            this.colTreeDistance.Name = "colTreeDistance";
            this.colTreeDistance.OptionsColumn.AllowEdit = false;
            this.colTreeDistance.OptionsColumn.AllowFocus = false;
            this.colTreeDistance.OptionsColumn.ReadOnly = true;
            this.colTreeDistance.Width = 88;
            // 
            // colTreeHouseName
            // 
            this.colTreeHouseName.Caption = "Tree House Name";
            this.colTreeHouseName.FieldName = "TreeHouseName";
            this.colTreeHouseName.Name = "colTreeHouseName";
            this.colTreeHouseName.OptionsColumn.AllowEdit = false;
            this.colTreeHouseName.OptionsColumn.AllowFocus = false;
            this.colTreeHouseName.OptionsColumn.ReadOnly = true;
            this.colTreeHouseName.Visible = true;
            this.colTreeHouseName.VisibleIndex = 1;
            this.colTreeHouseName.Width = 107;
            // 
            // colTreePostcode
            // 
            this.colTreePostcode.Caption = "Tree Postcode";
            this.colTreePostcode.FieldName = "TreePostcode";
            this.colTreePostcode.Name = "colTreePostcode";
            this.colTreePostcode.OptionsColumn.AllowEdit = false;
            this.colTreePostcode.OptionsColumn.AllowFocus = false;
            this.colTreePostcode.OptionsColumn.ReadOnly = true;
            this.colTreePostcode.Visible = true;
            this.colTreePostcode.VisibleIndex = 2;
            this.colTreePostcode.Width = 91;
            // 
            // colInspectionReference
            // 
            this.colInspectionReference.Caption = "Inspection Reference";
            this.colInspectionReference.FieldName = "InspectionReference";
            this.colInspectionReference.Name = "colInspectionReference";
            this.colInspectionReference.OptionsColumn.AllowEdit = false;
            this.colInspectionReference.OptionsColumn.AllowFocus = false;
            this.colInspectionReference.OptionsColumn.ReadOnly = true;
            this.colInspectionReference.Visible = true;
            this.colInspectionReference.VisibleIndex = 3;
            this.colInspectionReference.Width = 125;
            // 
            // colInspectorName
            // 
            this.colInspectorName.Caption = "Inspector Name";
            this.colInspectorName.FieldName = "InspectorName";
            this.colInspectorName.Name = "colInspectorName";
            this.colInspectorName.OptionsColumn.AllowEdit = false;
            this.colInspectorName.OptionsColumn.AllowFocus = false;
            this.colInspectorName.OptionsColumn.ReadOnly = true;
            this.colInspectorName.Visible = true;
            this.colInspectorName.VisibleIndex = 4;
            this.colInspectorName.Width = 98;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Visible = true;
            this.colInspectionDate.VisibleIndex = 5;
            this.colInspectionDate.Width = 111;
            // 
            // colIncidentReferenceNumber
            // 
            this.colIncidentReferenceNumber.Caption = "Incident Ref No";
            this.colIncidentReferenceNumber.FieldName = "IncidentReferenceNumber";
            this.colIncidentReferenceNumber.Name = "colIncidentReferenceNumber";
            this.colIncidentReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colIncidentReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colIncidentReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colIncidentReferenceNumber.Visible = true;
            this.colIncidentReferenceNumber.VisibleIndex = 7;
            this.colIncidentReferenceNumber.Width = 97;
            // 
            // colIncidentID
            // 
            this.colIncidentID.Caption = "Incident ID";
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            // 
            // colStemPhysical1
            // 
            this.colStemPhysical1.Caption = "Stem Physical 1";
            this.colStemPhysical1.FieldName = "StemPhysical1";
            this.colStemPhysical1.Name = "colStemPhysical1";
            this.colStemPhysical1.OptionsColumn.AllowEdit = false;
            this.colStemPhysical1.OptionsColumn.AllowFocus = false;
            this.colStemPhysical1.OptionsColumn.ReadOnly = true;
            this.colStemPhysical1.Visible = true;
            this.colStemPhysical1.VisibleIndex = 9;
            this.colStemPhysical1.Width = 96;
            // 
            // colStemPhysical2
            // 
            this.colStemPhysical2.Caption = "Stem Physical 2";
            this.colStemPhysical2.FieldName = "StemPhysical2";
            this.colStemPhysical2.Name = "colStemPhysical2";
            this.colStemPhysical2.OptionsColumn.AllowEdit = false;
            this.colStemPhysical2.OptionsColumn.AllowFocus = false;
            this.colStemPhysical2.OptionsColumn.ReadOnly = true;
            this.colStemPhysical2.Visible = true;
            this.colStemPhysical2.VisibleIndex = 10;
            this.colStemPhysical2.Width = 96;
            // 
            // colStemPhysical3
            // 
            this.colStemPhysical3.Caption = "Stem Physical 3";
            this.colStemPhysical3.FieldName = "StemPhysical3";
            this.colStemPhysical3.Name = "colStemPhysical3";
            this.colStemPhysical3.OptionsColumn.AllowEdit = false;
            this.colStemPhysical3.OptionsColumn.AllowFocus = false;
            this.colStemPhysical3.OptionsColumn.ReadOnly = true;
            this.colStemPhysical3.Visible = true;
            this.colStemPhysical3.VisibleIndex = 11;
            this.colStemPhysical3.Width = 96;
            // 
            // colStemDisease1
            // 
            this.colStemDisease1.Caption = "Stem Disease 1";
            this.colStemDisease1.FieldName = "StemDisease1";
            this.colStemDisease1.Name = "colStemDisease1";
            this.colStemDisease1.OptionsColumn.AllowEdit = false;
            this.colStemDisease1.OptionsColumn.AllowFocus = false;
            this.colStemDisease1.OptionsColumn.ReadOnly = true;
            this.colStemDisease1.Visible = true;
            this.colStemDisease1.VisibleIndex = 12;
            this.colStemDisease1.Width = 95;
            // 
            // colStemDisease2
            // 
            this.colStemDisease2.Caption = "Stem Disease 2";
            this.colStemDisease2.FieldName = "StemDisease2";
            this.colStemDisease2.Name = "colStemDisease2";
            this.colStemDisease2.OptionsColumn.AllowEdit = false;
            this.colStemDisease2.OptionsColumn.AllowFocus = false;
            this.colStemDisease2.OptionsColumn.ReadOnly = true;
            this.colStemDisease2.Visible = true;
            this.colStemDisease2.VisibleIndex = 13;
            this.colStemDisease2.Width = 95;
            // 
            // colStemDisease3
            // 
            this.colStemDisease3.Caption = "Stem Disease 3";
            this.colStemDisease3.FieldName = "StemDisease3";
            this.colStemDisease3.Name = "colStemDisease3";
            this.colStemDisease3.OptionsColumn.AllowEdit = false;
            this.colStemDisease3.OptionsColumn.AllowFocus = false;
            this.colStemDisease3.OptionsColumn.ReadOnly = true;
            this.colStemDisease3.Visible = true;
            this.colStemDisease3.VisibleIndex = 14;
            this.colStemDisease3.Width = 95;
            // 
            // colAngleToVertical
            // 
            this.colAngleToVertical.Caption = "Angle To Vertical";
            this.colAngleToVertical.FieldName = "AngleToVertical";
            this.colAngleToVertical.Name = "colAngleToVertical";
            this.colAngleToVertical.OptionsColumn.AllowEdit = false;
            this.colAngleToVertical.OptionsColumn.AllowFocus = false;
            this.colAngleToVertical.OptionsColumn.ReadOnly = true;
            this.colAngleToVertical.Visible = true;
            this.colAngleToVertical.VisibleIndex = 15;
            this.colAngleToVertical.Width = 102;
            // 
            // colCrownPhysical1
            // 
            this.colCrownPhysical1.Caption = "Crown Physical 1";
            this.colCrownPhysical1.FieldName = "CrownPhysical1";
            this.colCrownPhysical1.Name = "colCrownPhysical1";
            this.colCrownPhysical1.OptionsColumn.AllowEdit = false;
            this.colCrownPhysical1.OptionsColumn.AllowFocus = false;
            this.colCrownPhysical1.OptionsColumn.ReadOnly = true;
            this.colCrownPhysical1.Visible = true;
            this.colCrownPhysical1.VisibleIndex = 16;
            this.colCrownPhysical1.Width = 103;
            // 
            // colCrownPhysical2
            // 
            this.colCrownPhysical2.Caption = "Crown Physical 2";
            this.colCrownPhysical2.FieldName = "CrownPhysical2";
            this.colCrownPhysical2.Name = "colCrownPhysical2";
            this.colCrownPhysical2.OptionsColumn.AllowEdit = false;
            this.colCrownPhysical2.OptionsColumn.AllowFocus = false;
            this.colCrownPhysical2.OptionsColumn.ReadOnly = true;
            this.colCrownPhysical2.Visible = true;
            this.colCrownPhysical2.VisibleIndex = 17;
            this.colCrownPhysical2.Width = 103;
            // 
            // colCrownPhysical3
            // 
            this.colCrownPhysical3.Caption = "Crown Physical 3";
            this.colCrownPhysical3.FieldName = "CrownPhysical3";
            this.colCrownPhysical3.Name = "colCrownPhysical3";
            this.colCrownPhysical3.OptionsColumn.AllowEdit = false;
            this.colCrownPhysical3.OptionsColumn.AllowFocus = false;
            this.colCrownPhysical3.OptionsColumn.ReadOnly = true;
            this.colCrownPhysical3.Visible = true;
            this.colCrownPhysical3.VisibleIndex = 18;
            this.colCrownPhysical3.Width = 103;
            // 
            // colCrownDisease1
            // 
            this.colCrownDisease1.Caption = "Crown Disease 1";
            this.colCrownDisease1.FieldName = "CrownDisease1";
            this.colCrownDisease1.Name = "colCrownDisease1";
            this.colCrownDisease1.OptionsColumn.AllowEdit = false;
            this.colCrownDisease1.OptionsColumn.AllowFocus = false;
            this.colCrownDisease1.OptionsColumn.ReadOnly = true;
            this.colCrownDisease1.Visible = true;
            this.colCrownDisease1.VisibleIndex = 19;
            this.colCrownDisease1.Width = 102;
            // 
            // colCrownDisease2
            // 
            this.colCrownDisease2.Caption = "Crown Disease 2";
            this.colCrownDisease2.FieldName = "CrownDisease2";
            this.colCrownDisease2.Name = "colCrownDisease2";
            this.colCrownDisease2.OptionsColumn.AllowEdit = false;
            this.colCrownDisease2.OptionsColumn.AllowFocus = false;
            this.colCrownDisease2.OptionsColumn.ReadOnly = true;
            this.colCrownDisease2.Visible = true;
            this.colCrownDisease2.VisibleIndex = 20;
            this.colCrownDisease2.Width = 102;
            // 
            // colCrownDisease3
            // 
            this.colCrownDisease3.Caption = "Crown Disease 3";
            this.colCrownDisease3.FieldName = "CrownDisease3";
            this.colCrownDisease3.Name = "colCrownDisease3";
            this.colCrownDisease3.OptionsColumn.AllowEdit = false;
            this.colCrownDisease3.OptionsColumn.AllowFocus = false;
            this.colCrownDisease3.OptionsColumn.ReadOnly = true;
            this.colCrownDisease3.Visible = true;
            this.colCrownDisease3.VisibleIndex = 21;
            this.colCrownDisease3.Width = 102;
            // 
            // colCrownFoliation1
            // 
            this.colCrownFoliation1.Caption = "Crown Foliation 1";
            this.colCrownFoliation1.FieldName = "CrownFoliation1";
            this.colCrownFoliation1.Name = "colCrownFoliation1";
            this.colCrownFoliation1.OptionsColumn.AllowEdit = false;
            this.colCrownFoliation1.OptionsColumn.AllowFocus = false;
            this.colCrownFoliation1.OptionsColumn.ReadOnly = true;
            this.colCrownFoliation1.Visible = true;
            this.colCrownFoliation1.VisibleIndex = 22;
            this.colCrownFoliation1.Width = 105;
            // 
            // colCrownFoliation2
            // 
            this.colCrownFoliation2.Caption = "Crown Foliation 2";
            this.colCrownFoliation2.FieldName = "CrownFoliation2";
            this.colCrownFoliation2.Name = "colCrownFoliation2";
            this.colCrownFoliation2.OptionsColumn.AllowEdit = false;
            this.colCrownFoliation2.OptionsColumn.AllowFocus = false;
            this.colCrownFoliation2.OptionsColumn.ReadOnly = true;
            this.colCrownFoliation2.Visible = true;
            this.colCrownFoliation2.VisibleIndex = 23;
            this.colCrownFoliation2.Width = 105;
            // 
            // colCrownFoliation3
            // 
            this.colCrownFoliation3.Caption = "Crown Foliation 3";
            this.colCrownFoliation3.FieldName = "CrownFoliation3";
            this.colCrownFoliation3.Name = "colCrownFoliation3";
            this.colCrownFoliation3.OptionsColumn.AllowEdit = false;
            this.colCrownFoliation3.OptionsColumn.AllowFocus = false;
            this.colCrownFoliation3.OptionsColumn.ReadOnly = true;
            this.colCrownFoliation3.Visible = true;
            this.colCrownFoliation3.VisibleIndex = 24;
            this.colCrownFoliation3.Width = 105;
            // 
            // colRiskCategory1
            // 
            this.colRiskCategory1.Caption = "Risk Category";
            this.colRiskCategory1.FieldName = "RiskCategory";
            this.colRiskCategory1.Name = "colRiskCategory1";
            this.colRiskCategory1.OptionsColumn.AllowEdit = false;
            this.colRiskCategory1.OptionsColumn.AllowFocus = false;
            this.colRiskCategory1.OptionsColumn.ReadOnly = true;
            this.colRiskCategory1.Visible = true;
            this.colRiskCategory1.VisibleIndex = 6;
            this.colRiskCategory1.Width = 89;
            // 
            // colGeneralCondition
            // 
            this.colGeneralCondition.Caption = "General Condition";
            this.colGeneralCondition.FieldName = "GeneralCondition";
            this.colGeneralCondition.Name = "colGeneralCondition";
            this.colGeneralCondition.OptionsColumn.AllowEdit = false;
            this.colGeneralCondition.OptionsColumn.AllowFocus = false;
            this.colGeneralCondition.OptionsColumn.ReadOnly = true;
            this.colGeneralCondition.Visible = true;
            this.colGeneralCondition.VisibleIndex = 8;
            this.colGeneralCondition.Width = 107;
            // 
            // colRootHeave1
            // 
            this.colRootHeave1.Caption = "Root Heave 1";
            this.colRootHeave1.FieldName = "RootHeave1";
            this.colRootHeave1.Name = "colRootHeave1";
            this.colRootHeave1.OptionsColumn.AllowEdit = false;
            this.colRootHeave1.OptionsColumn.AllowFocus = false;
            this.colRootHeave1.OptionsColumn.ReadOnly = true;
            this.colRootHeave1.Visible = true;
            this.colRootHeave1.VisibleIndex = 25;
            this.colRootHeave1.Width = 88;
            // 
            // colRootHeave2
            // 
            this.colRootHeave2.Caption = "Root Heave 2";
            this.colRootHeave2.FieldName = "RootHeave2";
            this.colRootHeave2.Name = "colRootHeave2";
            this.colRootHeave2.OptionsColumn.AllowEdit = false;
            this.colRootHeave2.OptionsColumn.AllowFocus = false;
            this.colRootHeave2.OptionsColumn.ReadOnly = true;
            this.colRootHeave2.Visible = true;
            this.colRootHeave2.VisibleIndex = 26;
            this.colRootHeave2.Width = 88;
            // 
            // colRootHeave3
            // 
            this.colRootHeave3.Caption = "Root Heave 3";
            this.colRootHeave3.FieldName = "RootHeave3";
            this.colRootHeave3.Name = "colRootHeave3";
            this.colRootHeave3.OptionsColumn.AllowEdit = false;
            this.colRootHeave3.OptionsColumn.AllowFocus = false;
            this.colRootHeave3.OptionsColumn.ReadOnly = true;
            this.colRootHeave3.Visible = true;
            this.colRootHeave3.VisibleIndex = 27;
            this.colRootHeave3.Width = 88;
            // 
            // colDateLastModified
            // 
            this.colDateLastModified.Caption = "Last Modified";
            this.colDateLastModified.FieldName = "DateLastModified";
            this.colDateLastModified.Name = "colDateLastModified";
            this.colDateLastModified.OptionsColumn.AllowEdit = false;
            this.colDateLastModified.OptionsColumn.AllowFocus = false;
            this.colDateLastModified.OptionsColumn.ReadOnly = true;
            this.colDateLastModified.Width = 85;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 34;
            this.colRemarks1.Width = 63;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // colUser11
            // 
            this.colUser11.Caption = "User 1";
            this.colUser11.FieldName = "User1";
            this.colUser11.Name = "colUser11";
            this.colUser11.OptionsColumn.AllowEdit = false;
            this.colUser11.OptionsColumn.AllowFocus = false;
            this.colUser11.OptionsColumn.ReadOnly = true;
            this.colUser11.Visible = true;
            this.colUser11.VisibleIndex = 28;
            this.colUser11.Width = 53;
            // 
            // colUser21
            // 
            this.colUser21.Caption = "User 2";
            this.colUser21.FieldName = "User2";
            this.colUser21.Name = "colUser21";
            this.colUser21.OptionsColumn.AllowEdit = false;
            this.colUser21.OptionsColumn.AllowFocus = false;
            this.colUser21.OptionsColumn.ReadOnly = true;
            this.colUser21.Visible = true;
            this.colUser21.VisibleIndex = 29;
            this.colUser21.Width = 53;
            // 
            // colUser31
            // 
            this.colUser31.Caption = "User 3";
            this.colUser31.FieldName = "User3";
            this.colUser31.Name = "colUser31";
            this.colUser31.OptionsColumn.AllowEdit = false;
            this.colUser31.OptionsColumn.AllowFocus = false;
            this.colUser31.OptionsColumn.ReadOnly = true;
            this.colUser31.Visible = true;
            this.colUser31.VisibleIndex = 30;
            this.colUser31.Width = 53;
            // 
            // colcolor1
            // 
            this.colcolor1.Caption = "color";
            this.colcolor1.FieldName = "color";
            this.colcolor1.Name = "colcolor1";
            this.colcolor1.OptionsColumn.AllowEdit = false;
            this.colcolor1.OptionsColumn.AllowFocus = false;
            this.colcolor1.OptionsColumn.ReadOnly = true;
            this.colcolor1.OptionsColumn.ShowInCustomizationForm = false;
            this.colcolor1.Width = 35;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Action Count";
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 31;
            this.colLinkedActionCount.Width = 84;
            // 
            // colLinkedOutstandingActionCount
            // 
            this.colLinkedOutstandingActionCount.Caption = "Outstanding Action Count";
            this.colLinkedOutstandingActionCount.FieldName = "LinkedOutstandingActionCount";
            this.colLinkedOutstandingActionCount.Name = "colLinkedOutstandingActionCount";
            this.colLinkedOutstandingActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedOutstandingActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedOutstandingActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedOutstandingActionCount.Visible = true;
            this.colLinkedOutstandingActionCount.VisibleIndex = 32;
            this.colLinkedOutstandingActionCount.Width = 146;
            // 
            // colNoFurtherActionRequired
            // 
            this.colNoFurtherActionRequired.FieldName = "NoFurtherActionRequired";
            this.colNoFurtherActionRequired.Name = "colNoFurtherActionRequired";
            this.colNoFurtherActionRequired.OptionsColumn.AllowEdit = false;
            this.colNoFurtherActionRequired.OptionsColumn.AllowFocus = false;
            this.colNoFurtherActionRequired.OptionsColumn.ReadOnly = true;
            this.colNoFurtherActionRequired.Visible = true;
            this.colNoFurtherActionRequired.VisibleIndex = 33;
            this.colNoFurtherActionRequired.Width = 153;
            // 
            // Calculated1b
            // 
            this.Calculated1b.Caption = "<b>Calculated 1</b>";
            this.Calculated1b.FieldName = "Calculated1";
            this.Calculated1b.Name = "Calculated1b";
            this.Calculated1b.OptionsColumn.AllowEdit = false;
            this.Calculated1b.OptionsColumn.AllowFocus = false;
            this.Calculated1b.OptionsColumn.ReadOnly = true;
            this.Calculated1b.ShowUnboundExpressionMenu = true;
            this.Calculated1b.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1b.Visible = true;
            this.Calculated1b.VisibleIndex = 35;
            this.Calculated1b.Width = 90;
            // 
            // Calculated2b
            // 
            this.Calculated2b.Caption = "<b>Calculated 2</b>";
            this.Calculated2b.FieldName = "Calculated2";
            this.Calculated2b.Name = "Calculated2b";
            this.Calculated2b.OptionsColumn.AllowEdit = false;
            this.Calculated2b.OptionsColumn.AllowFocus = false;
            this.Calculated2b.OptionsColumn.ReadOnly = true;
            this.Calculated2b.ShowUnboundExpressionMenu = true;
            this.Calculated2b.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2b.Visible = true;
            this.Calculated2b.VisibleIndex = 36;
            this.Calculated2b.Width = 90;
            // 
            // Calculated3b
            // 
            this.Calculated3b.Caption = "<b>Calculated 3</b>";
            this.Calculated3b.FieldName = "Calculated3";
            this.Calculated3b.Name = "Calculated3b";
            this.Calculated3b.OptionsColumn.AllowEdit = false;
            this.Calculated3b.OptionsColumn.AllowFocus = false;
            this.Calculated3b.OptionsColumn.ReadOnly = true;
            this.Calculated3b.ShowUnboundExpressionMenu = true;
            this.Calculated3b.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3b.Visible = true;
            this.Calculated3b.VisibleIndex = 37;
            this.Calculated3b.Width = 90;
            // 
            // colUserPicklist11
            // 
            this.colUserPicklist11.Caption = "User Picklist 1";
            this.colUserPicklist11.FieldName = "UserPicklist1";
            this.colUserPicklist11.Name = "colUserPicklist11";
            this.colUserPicklist11.OptionsColumn.AllowEdit = false;
            this.colUserPicklist11.OptionsColumn.AllowFocus = false;
            this.colUserPicklist11.OptionsColumn.ReadOnly = true;
            this.colUserPicklist11.Width = 86;
            // 
            // colUserPicklist21
            // 
            this.colUserPicklist21.Caption = "User Picklist 2";
            this.colUserPicklist21.FieldName = "UserPicklist2";
            this.colUserPicklist21.Name = "colUserPicklist21";
            this.colUserPicklist21.OptionsColumn.AllowEdit = false;
            this.colUserPicklist21.OptionsColumn.AllowFocus = false;
            this.colUserPicklist21.OptionsColumn.ReadOnly = true;
            this.colUserPicklist21.Width = 86;
            // 
            // colUserPicklist31
            // 
            this.colUserPicklist31.Caption = "User Picklist 3";
            this.colUserPicklist31.FieldName = "UserPicklist3";
            this.colUserPicklist31.Name = "colUserPicklist31";
            this.colUserPicklist31.OptionsColumn.AllowEdit = false;
            this.colUserPicklist31.OptionsColumn.AllowFocus = false;
            this.colUserPicklist31.OptionsColumn.ReadOnly = true;
            this.colUserPicklist31.Width = 86;
            // 
            // btnLoadInspections
            // 
            this.btnLoadInspections.ImageOptions.ImageIndex = 0;
            this.btnLoadInspections.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadInspections.Location = new System.Drawing.Point(316, 50);
            this.btnLoadInspections.Name = "btnLoadInspections";
            this.btnLoadInspections.Size = new System.Drawing.Size(113, 22);
            this.btnLoadInspections.StyleController = this.layoutControl3;
            this.btnLoadInspections.TabIndex = 8;
            this.btnLoadInspections.Text = "Load Inspections";
            this.btnLoadInspections.Click += new System.EventHandler(this.btnLoadInspections_Click);
            // 
            // deInspectionToDate
            // 
            this.deInspectionToDate.EditValue = null;
            this.deInspectionToDate.Location = new System.Drawing.Point(262, 2);
            this.deInspectionToDate.Name = "deInspectionToDate";
            this.deInspectionToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deInspectionToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deInspectionToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deInspectionToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deInspectionToDate.Properties.NullText = "Not Used";
            this.deInspectionToDate.Size = new System.Drawing.Size(167, 20);
            this.deInspectionToDate.StyleController = this.layoutControl3;
            superToolTip54.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem55.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem55.Appearance.Options.UseImage = true;
            toolTipTitleItem55.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem55.Text = "Information - To Date";
            toolTipItem54.LeftIndent = 6;
            toolTipItem54.Text = "All inspections with an <b>Inspection Date</b> which fall between the From Date a" +
    "nd To Date will be returned.\r\n";
            superToolTip54.Items.Add(toolTipTitleItem55);
            superToolTip54.Items.Add(toolTipItem54);
            this.deInspectionToDate.SuperTip = superToolTip54;
            this.deInspectionToDate.TabIndex = 7;
            // 
            // deInspectionFromDate
            // 
            this.deInspectionFromDate.EditValue = null;
            this.deInspectionFromDate.Location = new System.Drawing.Point(59, 2);
            this.deInspectionFromDate.Name = "deInspectionFromDate";
            this.deInspectionFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deInspectionFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deInspectionFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deInspectionFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deInspectionFromDate.Properties.NullText = "Not Used";
            this.deInspectionFromDate.Size = new System.Drawing.Size(154, 20);
            this.deInspectionFromDate.StyleController = this.layoutControl3;
            superToolTip48.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem48.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem48.Appearance.Options.UseImage = true;
            toolTipTitleItem48.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem48.Text = "Information - From Date";
            toolTipItem48.LeftIndent = 6;
            toolTipItem48.Text = "All inspections with an <b>Inspection Date</b> which fall between the From Date a" +
    "nd To Date will be returned.\r\n";
            superToolTip48.Items.Add(toolTipTitleItem48);
            superToolTip48.Items.Add(toolTipItem48);
            this.deInspectionFromDate.SuperTip = superToolTip48;
            this.deInspectionFromDate.TabIndex = 6;
            // 
            // ceLastInspectionOnly
            // 
            this.ceLastInspectionOnly.EditValue = true;
            this.ceLastInspectionOnly.Location = new System.Drawing.Point(2, 50);
            this.ceLastInspectionOnly.Name = "ceLastInspectionOnly";
            this.ceLastInspectionOnly.Properties.Caption = "Only show Last Inspection for Each Tree";
            this.ceLastInspectionOnly.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.ceLastInspectionOnly.Size = new System.Drawing.Size(310, 19);
            this.ceLastInspectionOnly.StyleController = this.layoutControl3;
            toolTipTitleItem49.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem49.Appearance.Options.UseImage = true;
            toolTipTitleItem49.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem49.Text = "Information - Only show last Inspection tick box";
            toolTipItem49.LeftIndent = 6;
            toolTipItem49.Text = "Tick to return only one inspection (last recorded) per tree.\r\nLeave unticked to r" +
    "eturn all inspections for each tree.";
            toolTipTitleItem103.LeftIndent = 6;
            toolTipTitleItem103.Text = "Trees without an inspection are NOT shown.";
            superToolTip49.Items.Add(toolTipTitleItem49);
            superToolTip49.Items.Add(toolTipItem49);
            superToolTip49.Items.Add(toolTipSeparatorItem2);
            superToolTip49.Items.Add(toolTipTitleItem103);
            this.ceLastInspectionOnly.SuperTip = superToolTip49;
            this.ceLastInspectionOnly.TabIndex = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem11,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem7,
            this.layoutControlItem4});
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutControlGroup3.Size = new System.Drawing.Size(431, 526);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AllowHtmlStringInCaption = true;
            this.layoutControlItem8.Control = this.deInspectionFromDate;
            this.layoutControlItem8.CustomizationFormText = "From Date:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem8.Text = "From Date:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.gridSplitContainer2;
            this.layoutControlItem11.CustomizationFormText = "Inspection Grid:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(431, 452);
            this.layoutControlItem11.Text = "Inspection Grid:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnLoadInspections;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(314, 48);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(117, 26);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(117, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(117, 26);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AllowHtmlStringInCaption = true;
            this.layoutControlItem9.Control = this.deInspectionToDate;
            this.layoutControlItem9.CustomizationFormText = "To Date:";
            this.layoutControlItem9.Location = new System.Drawing.Point(215, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem9.Text = "To Date:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(42, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.ceLastInspectionOnly;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(314, 26);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonEditClientFilter2;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(431, 24);
            this.layoutControlItem4.Text = "Client:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(31, 13);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.layoutControl4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(431, 526);
            this.xtraTabPage3.Text = "Actions";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.buttonEditClientFilter3);
            this.layoutControl4.Controls.Add(this.deActionToDate);
            this.layoutControl4.Controls.Add(this.deActionFromDate);
            this.layoutControl4.Controls.Add(this.btnLoadActions);
            this.layoutControl4.Controls.Add(this.gridSplitContainer3);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.MenuManager = this.barManager1;
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(529, 346, 250, 350);
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(431, 526);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // buttonEditClientFilter3
            // 
            this.buttonEditClientFilter3.Location = new System.Drawing.Point(36, 26);
            this.buttonEditClientFilter3.MenuManager = this.barManager1;
            this.buttonEditClientFilter3.Name = "buttonEditClientFilter3";
            this.buttonEditClientFilter3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter3.Size = new System.Drawing.Size(301, 20);
            this.buttonEditClientFilter3.StyleController = this.layoutControl4;
            this.buttonEditClientFilter3.TabIndex = 32;
            this.buttonEditClientFilter3.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter3_ButtonClick);
            // 
            // deActionToDate
            // 
            this.deActionToDate.EditValue = null;
            this.deActionToDate.Location = new System.Drawing.Point(263, 2);
            this.deActionToDate.MenuManager = this.barManager1;
            this.deActionToDate.Name = "deActionToDate";
            this.deActionToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deActionToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deActionToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deActionToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deActionToDate.Properties.NullText = "Not Used";
            this.deActionToDate.Size = new System.Drawing.Size(166, 20);
            this.deActionToDate.StyleController = this.layoutControl4;
            superToolTip50.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem50.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem50.Appearance.Options.UseImage = true;
            toolTipTitleItem50.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem50.Text = "Information - To Date";
            toolTipItem50.LeftIndent = 6;
            toolTipItem50.Text = "All actions with an <b>Action Due Date</b> which fall between the From Date and T" +
    "o Date will be returned.";
            superToolTip50.Items.Add(toolTipTitleItem50);
            superToolTip50.Items.Add(toolTipItem50);
            this.deActionToDate.SuperTip = superToolTip50;
            this.deActionToDate.TabIndex = 8;
            // 
            // deActionFromDate
            // 
            this.deActionFromDate.EditValue = null;
            this.deActionFromDate.Location = new System.Drawing.Point(59, 2);
            this.deActionFromDate.MenuManager = this.barManager1;
            this.deActionFromDate.Name = "deActionFromDate";
            this.deActionFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deActionFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deActionFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deActionFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deActionFromDate.Properties.NullText = "Not Used";
            this.deActionFromDate.Size = new System.Drawing.Size(155, 20);
            this.deActionFromDate.StyleController = this.layoutControl4;
            superToolTip51.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem51.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem51.Appearance.Options.UseImage = true;
            toolTipTitleItem51.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem51.Text = "Information - From Date";
            toolTipItem51.LeftIndent = 6;
            toolTipItem51.Text = "All actions with an <b>Action Due Date</b> which fall between the From Date and T" +
    "o Date will be returned.";
            superToolTip51.Items.Add(toolTipTitleItem51);
            superToolTip51.Items.Add(toolTipItem51);
            this.deActionFromDate.SuperTip = superToolTip51;
            this.deActionFromDate.TabIndex = 7;
            // 
            // btnLoadActions
            // 
            this.btnLoadActions.ImageOptions.ImageIndex = 0;
            this.btnLoadActions.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadActions.Location = new System.Drawing.Point(341, 26);
            this.btnLoadActions.Name = "btnLoadActions";
            this.btnLoadActions.Size = new System.Drawing.Size(88, 22);
            this.btnLoadActions.StyleController = this.layoutControl4;
            this.btnLoadActions.TabIndex = 6;
            this.btnLoadActions.Text = "Load Actions";
            this.btnLoadActions.Click += new System.EventHandler(this.btnLoadActions_Click);
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(2, 52);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(427, 472);
            this.gridSplitContainer3.TabIndex = 9;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp01232ATReportsListingsActionFilterBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView7;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl3.Size = new System.Drawing.Size(427, 472);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp01232ATReportsListingsActionFilterBindingSource
            // 
            this.sp01232ATReportsListingsActionFilterBindingSource.DataMember = "sp01232_AT_Reports_Listings_Action_Filter";
            this.sp01232ATReportsListingsActionFilterBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colInspectionID1,
            this.colTreeID2,
            this.colSiteID2,
            this.colClientID3,
            this.colClientName3,
            this.colSiteName2,
            this.colTreeReference2,
            this.colTreeMappingID1,
            this.colTreeGridReference1,
            this.colTreeDistance1,
            this.colTreeHouseName1,
            this.colTreePostcode1,
            this.colInspectionReference1,
            this.colInspectorName1,
            this.colInspectionDate1,
            this.colIncidentReferenceNumber1,
            this.colIncidentID1,
            this.colActionJobNumber,
            this.colAction,
            this.colActionPriority,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colActionBy,
            this.colActionSupervisor,
            this.colActionOwnership,
            this.colActionCostCentre,
            this.colActionBudget,
            this.colActionWorkUnits,
            this.colActionScheduleOfRates,
            this.colActionBudgetedRateDescription,
            this.colActionBudgetedRate,
            this.colActionBudgetedCost,
            this.colActionActualRateDescription,
            this.colActionActualRate,
            this.colActionActualCost,
            this.colDateLastModified1,
            this.colActionWorkOrder,
            this.colRemarks2,
            this.colUser12,
            this.colUser22,
            this.colUser32,
            this.colcolor2,
            this.colActionCostDifference,
            this.colActionCompletionStatus,
            this.colActionTimeliness,
            this.Calculated1c,
            this.Calculated2c,
            this.Calculated3c,
            this.colActionWorkOrderCompleteDate,
            this.colActionWorkOrderIssueDate,
            this.colUserPicklist12,
            this.colUserPicklist22,
            this.colUserPicklist32});
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.Column = this.colActionCostDifference;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Less;
            styleFormatCondition3.Value1 = "0.00";
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Green;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.Column = this.colActionCostDifference;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Greater;
            styleFormatCondition4.Value1 = "0.00";
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3,
            styleFormatCondition4});
            this.gridView7.GridControl = this.gridControl3;
            this.gridView7.GroupCount = 3;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsFind.AlwaysVisible = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionTimeliness, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseDown);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            this.colActionID.Width = 56;
            // 
            // colInspectionID1
            // 
            this.colInspectionID1.Caption = "Inspection ID";
            this.colInspectionID1.FieldName = "InspectionID";
            this.colInspectionID1.Name = "colInspectionID1";
            this.colInspectionID1.OptionsColumn.AllowEdit = false;
            this.colInspectionID1.OptionsColumn.AllowFocus = false;
            this.colInspectionID1.OptionsColumn.ReadOnly = true;
            this.colInspectionID1.Width = 76;
            // 
            // colTreeID2
            // 
            this.colTreeID2.Caption = "Tree ID";
            this.colTreeID2.FieldName = "TreeID";
            this.colTreeID2.Name = "colTreeID2";
            this.colTreeID2.OptionsColumn.AllowEdit = false;
            this.colTreeID2.OptionsColumn.AllowFocus = false;
            this.colTreeID2.OptionsColumn.ReadOnly = true;
            this.colTreeID2.Width = 48;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.Width = 62;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            this.colClientID3.Width = 59;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 98;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Width = 101;
            // 
            // colTreeReference2
            // 
            this.colTreeReference2.Caption = "Tree Ref";
            this.colTreeReference2.FieldName = "TreeReference";
            this.colTreeReference2.Name = "colTreeReference2";
            this.colTreeReference2.OptionsColumn.AllowEdit = false;
            this.colTreeReference2.OptionsColumn.AllowFocus = false;
            this.colTreeReference2.OptionsColumn.ReadOnly = true;
            this.colTreeReference2.Width = 96;
            // 
            // colTreeMappingID1
            // 
            this.colTreeMappingID1.Caption = "Mapping ID";
            this.colTreeMappingID1.FieldName = "TreeMappingID";
            this.colTreeMappingID1.Name = "colTreeMappingID1";
            this.colTreeMappingID1.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID1.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID1.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID1.Width = 66;
            // 
            // colTreeGridReference1
            // 
            this.colTreeGridReference1.Caption = "Grid Reference";
            this.colTreeGridReference1.FieldName = "TreeGridReference";
            this.colTreeGridReference1.Name = "colTreeGridReference1";
            this.colTreeGridReference1.OptionsColumn.AllowEdit = false;
            this.colTreeGridReference1.OptionsColumn.AllowFocus = false;
            this.colTreeGridReference1.OptionsColumn.ReadOnly = true;
            this.colTreeGridReference1.Width = 96;
            // 
            // colTreeDistance1
            // 
            this.colTreeDistance1.Caption = "Distance";
            this.colTreeDistance1.FieldName = "TreeDistance";
            this.colTreeDistance1.Name = "colTreeDistance1";
            this.colTreeDistance1.OptionsColumn.AllowEdit = false;
            this.colTreeDistance1.OptionsColumn.AllowFocus = false;
            this.colTreeDistance1.OptionsColumn.ReadOnly = true;
            this.colTreeDistance1.Width = 63;
            // 
            // colTreeHouseName1
            // 
            this.colTreeHouseName1.Caption = "Nearest House";
            this.colTreeHouseName1.FieldName = "TreeHouseName";
            this.colTreeHouseName1.Name = "colTreeHouseName1";
            this.colTreeHouseName1.OptionsColumn.AllowEdit = false;
            this.colTreeHouseName1.OptionsColumn.AllowFocus = false;
            this.colTreeHouseName1.OptionsColumn.ReadOnly = true;
            this.colTreeHouseName1.Visible = true;
            this.colTreeHouseName1.VisibleIndex = 4;
            this.colTreeHouseName1.Width = 93;
            // 
            // colTreePostcode1
            // 
            this.colTreePostcode1.Caption = "Postcode";
            this.colTreePostcode1.FieldName = "TreePostcode";
            this.colTreePostcode1.Name = "colTreePostcode1";
            this.colTreePostcode1.OptionsColumn.AllowEdit = false;
            this.colTreePostcode1.OptionsColumn.AllowFocus = false;
            this.colTreePostcode1.OptionsColumn.ReadOnly = true;
            this.colTreePostcode1.Width = 66;
            // 
            // colInspectionReference1
            // 
            this.colInspectionReference1.Caption = "Inspection Ref";
            this.colInspectionReference1.FieldName = "InspectionReference";
            this.colInspectionReference1.Name = "colInspectionReference1";
            this.colInspectionReference1.OptionsColumn.AllowEdit = false;
            this.colInspectionReference1.OptionsColumn.AllowFocus = false;
            this.colInspectionReference1.OptionsColumn.ReadOnly = true;
            this.colInspectionReference1.Visible = true;
            this.colInspectionReference1.VisibleIndex = 0;
            this.colInspectionReference1.Width = 105;
            // 
            // colInspectorName1
            // 
            this.colInspectorName1.Caption = "Inspector";
            this.colInspectorName1.FieldName = "InspectorName";
            this.colInspectorName1.Name = "colInspectorName1";
            this.colInspectorName1.OptionsColumn.AllowEdit = false;
            this.colInspectorName1.OptionsColumn.AllowFocus = false;
            this.colInspectorName1.OptionsColumn.ReadOnly = true;
            this.colInspectorName1.Visible = true;
            this.colInspectorName1.VisibleIndex = 2;
            this.colInspectorName1.Width = 68;
            // 
            // colInspectionDate1
            // 
            this.colInspectionDate1.Caption = "Inspection Date";
            this.colInspectionDate1.FieldName = "InspectionDate";
            this.colInspectionDate1.Name = "colInspectionDate1";
            this.colInspectionDate1.OptionsColumn.AllowEdit = false;
            this.colInspectionDate1.OptionsColumn.AllowFocus = false;
            this.colInspectionDate1.OptionsColumn.ReadOnly = true;
            this.colInspectionDate1.Visible = true;
            this.colInspectionDate1.VisibleIndex = 3;
            this.colInspectionDate1.Width = 98;
            // 
            // colIncidentReferenceNumber1
            // 
            this.colIncidentReferenceNumber1.Caption = "Incident Ref";
            this.colIncidentReferenceNumber1.FieldName = "IncidentReferenceNumber";
            this.colIncidentReferenceNumber1.Name = "colIncidentReferenceNumber1";
            this.colIncidentReferenceNumber1.OptionsColumn.AllowEdit = false;
            this.colIncidentReferenceNumber1.OptionsColumn.AllowFocus = false;
            this.colIncidentReferenceNumber1.OptionsColumn.ReadOnly = true;
            this.colIncidentReferenceNumber1.Visible = true;
            this.colIncidentReferenceNumber1.VisibleIndex = 1;
            this.colIncidentReferenceNumber1.Width = 81;
            // 
            // colIncidentID1
            // 
            this.colIncidentID1.Caption = "Incident ID";
            this.colIncidentID1.FieldName = "IncidentID";
            this.colIncidentID1.Name = "colIncidentID1";
            this.colIncidentID1.OptionsColumn.AllowEdit = false;
            this.colIncidentID1.OptionsColumn.AllowFocus = false;
            this.colIncidentID1.OptionsColumn.ReadOnly = true;
            this.colIncidentID1.Width = 65;
            // 
            // colActionJobNumber
            // 
            this.colActionJobNumber.Caption = "Job Number";
            this.colActionJobNumber.FieldName = "ActionJobNumber";
            this.colActionJobNumber.Name = "colActionJobNumber";
            this.colActionJobNumber.OptionsColumn.AllowEdit = false;
            this.colActionJobNumber.OptionsColumn.AllowFocus = false;
            this.colActionJobNumber.OptionsColumn.ReadOnly = true;
            this.colActionJobNumber.Visible = true;
            this.colActionJobNumber.VisibleIndex = 5;
            this.colActionJobNumber.Width = 90;
            // 
            // colAction
            // 
            this.colAction.Caption = "Action";
            this.colAction.FieldName = "Action";
            this.colAction.Name = "colAction";
            this.colAction.OptionsColumn.AllowEdit = false;
            this.colAction.OptionsColumn.AllowFocus = false;
            this.colAction.OptionsColumn.ReadOnly = true;
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 6;
            this.colAction.Width = 125;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 7;
            this.colActionPriority.Width = 56;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Action Due";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 8;
            this.colActionDueDate.Width = 74;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Action Done";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 9;
            this.colActionDoneDate.Width = 80;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action By";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            this.colActionBy.Visible = true;
            this.colActionBy.VisibleIndex = 10;
            this.colActionBy.Width = 97;
            // 
            // colActionSupervisor
            // 
            this.colActionSupervisor.Caption = "Supervisor";
            this.colActionSupervisor.FieldName = "ActionSupervisor";
            this.colActionSupervisor.Name = "colActionSupervisor";
            this.colActionSupervisor.OptionsColumn.AllowEdit = false;
            this.colActionSupervisor.OptionsColumn.AllowFocus = false;
            this.colActionSupervisor.OptionsColumn.ReadOnly = true;
            this.colActionSupervisor.Visible = true;
            this.colActionSupervisor.VisibleIndex = 11;
            this.colActionSupervisor.Width = 104;
            // 
            // colActionOwnership
            // 
            this.colActionOwnership.Caption = "Ownership";
            this.colActionOwnership.FieldName = "ActionOwnership";
            this.colActionOwnership.Name = "colActionOwnership";
            this.colActionOwnership.OptionsColumn.AllowEdit = false;
            this.colActionOwnership.OptionsColumn.AllowFocus = false;
            this.colActionOwnership.OptionsColumn.ReadOnly = true;
            this.colActionOwnership.Visible = true;
            this.colActionOwnership.VisibleIndex = 13;
            this.colActionOwnership.Width = 108;
            // 
            // colActionCostCentre
            // 
            this.colActionCostCentre.Caption = "Cost Centre";
            this.colActionCostCentre.FieldName = "ActionCostCentre";
            this.colActionCostCentre.Name = "colActionCostCentre";
            this.colActionCostCentre.OptionsColumn.AllowEdit = false;
            this.colActionCostCentre.OptionsColumn.AllowFocus = false;
            this.colActionCostCentre.OptionsColumn.ReadOnly = true;
            this.colActionCostCentre.Visible = true;
            this.colActionCostCentre.VisibleIndex = 14;
            this.colActionCostCentre.Width = 117;
            // 
            // colActionBudget
            // 
            this.colActionBudget.Caption = "Budget";
            this.colActionBudget.FieldName = "ActionBudget";
            this.colActionBudget.Name = "colActionBudget";
            this.colActionBudget.OptionsColumn.AllowEdit = false;
            this.colActionBudget.OptionsColumn.AllowFocus = false;
            this.colActionBudget.OptionsColumn.ReadOnly = true;
            this.colActionBudget.Visible = true;
            this.colActionBudget.VisibleIndex = 15;
            this.colActionBudget.Width = 93;
            // 
            // colActionWorkUnits
            // 
            this.colActionWorkUnits.Caption = "Work Units";
            this.colActionWorkUnits.FieldName = "ActionWorkUnits";
            this.colActionWorkUnits.Name = "colActionWorkUnits";
            this.colActionWorkUnits.OptionsColumn.AllowEdit = false;
            this.colActionWorkUnits.OptionsColumn.AllowFocus = false;
            this.colActionWorkUnits.OptionsColumn.ReadOnly = true;
            this.colActionWorkUnits.Visible = true;
            this.colActionWorkUnits.VisibleIndex = 16;
            this.colActionWorkUnits.Width = 74;
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Schedule Of Rates";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 17;
            this.colActionScheduleOfRates.Width = 111;
            // 
            // colActionBudgetedRateDescription
            // 
            this.colActionBudgetedRateDescription.Caption = "Budgeted Rate Desc";
            this.colActionBudgetedRateDescription.FieldName = "ActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.Name = "colActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRateDescription.Visible = true;
            this.colActionBudgetedRateDescription.VisibleIndex = 18;
            this.colActionBudgetedRateDescription.Width = 120;
            // 
            // colActionBudgetedRate
            // 
            this.colActionBudgetedRate.Caption = "Budgeted Rate";
            this.colActionBudgetedRate.FieldName = "ActionBudgetedRate";
            this.colActionBudgetedRate.Name = "colActionBudgetedRate";
            this.colActionBudgetedRate.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRate.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRate.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRate.Visible = true;
            this.colActionBudgetedRate.VisibleIndex = 19;
            this.colActionBudgetedRate.Width = 94;
            // 
            // colActionBudgetedCost
            // 
            this.colActionBudgetedCost.Caption = "Budgeted Cost";
            this.colActionBudgetedCost.FieldName = "ActionBudgetedCost";
            this.colActionBudgetedCost.Name = "colActionBudgetedCost";
            this.colActionBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedCost.Visible = true;
            this.colActionBudgetedCost.VisibleIndex = 22;
            this.colActionBudgetedCost.Width = 93;
            // 
            // colActionActualRateDescription
            // 
            this.colActionActualRateDescription.Caption = "Actual Rate Desc";
            this.colActionActualRateDescription.FieldName = "ActionActualRateDescription";
            this.colActionActualRateDescription.Name = "colActionActualRateDescription";
            this.colActionActualRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionActualRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionActualRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionActualRateDescription.Visible = true;
            this.colActionActualRateDescription.VisibleIndex = 20;
            this.colActionActualRateDescription.Width = 104;
            // 
            // colActionActualRate
            // 
            this.colActionActualRate.Caption = "Actual Rate";
            this.colActionActualRate.FieldName = "ActionActualRate";
            this.colActionActualRate.Name = "colActionActualRate";
            this.colActionActualRate.OptionsColumn.AllowEdit = false;
            this.colActionActualRate.OptionsColumn.AllowFocus = false;
            this.colActionActualRate.OptionsColumn.ReadOnly = true;
            this.colActionActualRate.Visible = true;
            this.colActionActualRate.VisibleIndex = 21;
            this.colActionActualRate.Width = 78;
            // 
            // colActionActualCost
            // 
            this.colActionActualCost.Caption = "Actual Cost";
            this.colActionActualCost.FieldName = "ActionActualCost";
            this.colActionActualCost.Name = "colActionActualCost";
            this.colActionActualCost.OptionsColumn.AllowEdit = false;
            this.colActionActualCost.OptionsColumn.AllowFocus = false;
            this.colActionActualCost.OptionsColumn.ReadOnly = true;
            this.colActionActualCost.Visible = true;
            this.colActionActualCost.VisibleIndex = 23;
            this.colActionActualCost.Width = 77;
            // 
            // colDateLastModified1
            // 
            this.colDateLastModified1.Caption = "Last Modified";
            this.colDateLastModified1.FieldName = "DateLastModified";
            this.colDateLastModified1.Name = "colDateLastModified1";
            this.colDateLastModified1.OptionsColumn.AllowEdit = false;
            this.colDateLastModified1.OptionsColumn.AllowFocus = false;
            this.colDateLastModified1.OptionsColumn.ReadOnly = true;
            // 
            // colActionWorkOrder
            // 
            this.colActionWorkOrder.Caption = "Work Order";
            this.colActionWorkOrder.FieldName = "ActionWorkOrder";
            this.colActionWorkOrder.Name = "colActionWorkOrder";
            this.colActionWorkOrder.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrder.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrder.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrder.Visible = true;
            this.colActionWorkOrder.VisibleIndex = 12;
            this.colActionWorkOrder.Width = 78;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Width = 53;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            // 
            // colUser12
            // 
            this.colUser12.Caption = "User 1";
            this.colUser12.FieldName = "User1";
            this.colUser12.Name = "colUser12";
            this.colUser12.OptionsColumn.AllowEdit = false;
            this.colUser12.OptionsColumn.AllowFocus = false;
            this.colUser12.OptionsColumn.ReadOnly = true;
            this.colUser12.Width = 43;
            // 
            // colUser22
            // 
            this.colUser22.Caption = "User 2";
            this.colUser22.FieldName = "User2";
            this.colUser22.Name = "colUser22";
            this.colUser22.OptionsColumn.AllowEdit = false;
            this.colUser22.OptionsColumn.AllowFocus = false;
            this.colUser22.OptionsColumn.ReadOnly = true;
            this.colUser22.Width = 43;
            // 
            // colUser32
            // 
            this.colUser32.Caption = "Use r3";
            this.colUser32.FieldName = "User3";
            this.colUser32.Name = "colUser32";
            this.colUser32.OptionsColumn.AllowEdit = false;
            this.colUser32.OptionsColumn.AllowFocus = false;
            this.colUser32.OptionsColumn.ReadOnly = true;
            this.colUser32.Width = 43;
            // 
            // colcolor2
            // 
            this.colcolor2.Caption = "color";
            this.colcolor2.FieldName = "color";
            this.colcolor2.Name = "colcolor2";
            this.colcolor2.OptionsColumn.AllowEdit = false;
            this.colcolor2.OptionsColumn.AllowFocus = false;
            this.colcolor2.OptionsColumn.ReadOnly = true;
            this.colcolor2.OptionsColumn.ShowInCustomizationForm = false;
            this.colcolor2.Width = 35;
            // 
            // colActionCompletionStatus
            // 
            this.colActionCompletionStatus.Caption = "Completion Status";
            this.colActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.colActionCompletionStatus.Name = "colActionCompletionStatus";
            this.colActionCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colActionCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colActionCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colActionCompletionStatus.Width = 109;
            // 
            // colActionTimeliness
            // 
            this.colActionTimeliness.Caption = "Timeliness";
            this.colActionTimeliness.FieldName = "ActionTimeliness";
            this.colActionTimeliness.Name = "colActionTimeliness";
            this.colActionTimeliness.OptionsColumn.AllowEdit = false;
            this.colActionTimeliness.OptionsColumn.AllowFocus = false;
            this.colActionTimeliness.OptionsColumn.ReadOnly = true;
            this.colActionTimeliness.Width = 83;
            // 
            // Calculated1c
            // 
            this.Calculated1c.Caption = "<b>Calculated 1</b>";
            this.Calculated1c.FieldName = "Calculated1";
            this.Calculated1c.Name = "Calculated1c";
            this.Calculated1c.OptionsColumn.AllowEdit = false;
            this.Calculated1c.OptionsColumn.AllowFocus = false;
            this.Calculated1c.OptionsColumn.ReadOnly = true;
            this.Calculated1c.ShowUnboundExpressionMenu = true;
            this.Calculated1c.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1c.Visible = true;
            this.Calculated1c.VisibleIndex = 25;
            this.Calculated1c.Width = 90;
            // 
            // Calculated2c
            // 
            this.Calculated2c.Caption = "<b>Calculated 2</b>";
            this.Calculated2c.FieldName = "Calculated2";
            this.Calculated2c.Name = "Calculated2c";
            this.Calculated2c.OptionsColumn.AllowEdit = false;
            this.Calculated2c.OptionsColumn.AllowFocus = false;
            this.Calculated2c.OptionsColumn.ReadOnly = true;
            this.Calculated2c.ShowUnboundExpressionMenu = true;
            this.Calculated2c.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2c.Visible = true;
            this.Calculated2c.VisibleIndex = 26;
            this.Calculated2c.Width = 90;
            // 
            // Calculated3c
            // 
            this.Calculated3c.Caption = "<b>Calculated 3</b>";
            this.Calculated3c.FieldName = "Calculated3";
            this.Calculated3c.Name = "Calculated3c";
            this.Calculated3c.OptionsColumn.AllowEdit = false;
            this.Calculated3c.OptionsColumn.AllowFocus = false;
            this.Calculated3c.OptionsColumn.ReadOnly = true;
            this.Calculated3c.ShowUnboundExpressionMenu = true;
            this.Calculated3c.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3c.Visible = true;
            this.Calculated3c.VisibleIndex = 27;
            this.Calculated3c.Width = 90;
            // 
            // colActionWorkOrderCompleteDate
            // 
            this.colActionWorkOrderCompleteDate.Caption = "Work Order Complete Date";
            this.colActionWorkOrderCompleteDate.FieldName = "ActionWorkOrderCompleteDate";
            this.colActionWorkOrderCompleteDate.Name = "colActionWorkOrderCompleteDate";
            this.colActionWorkOrderCompleteDate.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrderCompleteDate.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrderCompleteDate.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrderCompleteDate.Width = 151;
            // 
            // colActionWorkOrderIssueDate
            // 
            this.colActionWorkOrderIssueDate.Caption = "Work Order Issue Date";
            this.colActionWorkOrderIssueDate.FieldName = "ActionWorkOrderIssueDate";
            this.colActionWorkOrderIssueDate.Name = "colActionWorkOrderIssueDate";
            this.colActionWorkOrderIssueDate.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrderIssueDate.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrderIssueDate.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrderIssueDate.Width = 132;
            // 
            // colUserPicklist12
            // 
            this.colUserPicklist12.Caption = "User Picklist 1";
            this.colUserPicklist12.FieldName = "UserPicklist1";
            this.colUserPicklist12.Name = "colUserPicklist12";
            this.colUserPicklist12.OptionsColumn.AllowEdit = false;
            this.colUserPicklist12.OptionsColumn.AllowFocus = false;
            this.colUserPicklist12.OptionsColumn.ReadOnly = true;
            this.colUserPicklist12.Width = 86;
            // 
            // colUserPicklist22
            // 
            this.colUserPicklist22.Caption = "User Picklist 2";
            this.colUserPicklist22.FieldName = "UserPicklist2";
            this.colUserPicklist22.Name = "colUserPicklist22";
            this.colUserPicklist22.OptionsColumn.AllowEdit = false;
            this.colUserPicklist22.OptionsColumn.AllowFocus = false;
            this.colUserPicklist22.OptionsColumn.ReadOnly = true;
            this.colUserPicklist22.Width = 86;
            // 
            // colUserPicklist32
            // 
            this.colUserPicklist32.Caption = "User Picklist 3";
            this.colUserPicklist32.FieldName = "UserPicklist3";
            this.colUserPicklist32.Name = "colUserPicklist32";
            this.colUserPicklist32.OptionsColumn.AllowEdit = false;
            this.colUserPicklist32.OptionsColumn.AllowFocus = false;
            this.colUserPicklist32.OptionsColumn.ReadOnly = true;
            this.colUserPicklist32.Width = 86;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem6});
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutControlGroup4.Size = new System.Drawing.Size(431, 526);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridSplitContainer3;
            this.layoutControlItem12.CustomizationFormText = "Action Grid:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(431, 476);
            this.layoutControlItem12.Text = "Action Grid:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.btnLoadActions;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(339, 24);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(92, 26);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(92, 26);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(92, 26);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AllowHtmlStringInCaption = true;
            this.layoutControlItem15.Control = this.deActionFromDate;
            this.layoutControlItem15.CustomizationFormText = "From Date:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem15.Text = "From Date:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AllowHtmlStringInCaption = true;
            this.layoutControlItem16.Control = this.deActionToDate;
            this.layoutControlItem16.CustomizationFormText = "To Date:";
            this.layoutControlItem16.Location = new System.Drawing.Point(216, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem16.Text = "To Date:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(42, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonEditClientFilter3;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(339, 26);
            this.layoutControlItem6.Text = "Client:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(31, 13);
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.PageEnabled = false;
            this.xtraTabPage4.Size = new System.Drawing.Size(431, 526);
            this.xtraTabPage4.Text = "Work Orders";
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.PageEnabled = false;
            this.xtraTabPage5.Size = new System.Drawing.Size(431, 526);
            this.xtraTabPage5.Text = "Incidents";
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.PageEnabled = false;
            this.xtraTabPage6.Size = new System.Drawing.Size(431, 526);
            this.xtraTabPage6.Text = "Import History";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.popupContainerEdit1);
            this.layoutControl1.Controls.Add(this.checkEdit_LoadLinkedData);
            this.layoutControl1.Controls.Add(this.checkEdit_LoadLinkedPictures);
            this.layoutControl1.Controls.Add(this.btnView);
            this.layoutControl1.Location = new System.Drawing.Point(1, 557);
            this.layoutControl1.MenuManager = this.barManager1;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(435, 53);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "No Report Layout Selected";
            this.popupContainerEdit1.Location = new System.Drawing.Point(42, 2);
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControl1;
            this.popupContainerEdit1.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit1.Size = new System.Drawing.Size(391, 20);
            this.popupContainerEdit1.StyleController = this.layoutControl1;
            this.popupContainerEdit1.TabIndex = 5;
            this.popupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.btnLayoutAddNew);
            this.popupContainerControl1.Controls.Add(this.btnLayoutOK);
            this.popupContainerControl1.Controls.Add(this.gridSplitContainer4);
            this.popupContainerControl1.Location = new System.Drawing.Point(447, 147);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(597, 250);
            this.popupContainerControl1.TabIndex = 10;
            // 
            // btnLayoutAddNew
            // 
            this.btnLayoutAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLayoutAddNew.Location = new System.Drawing.Point(519, 224);
            this.btnLayoutAddNew.Name = "btnLayoutAddNew";
            this.btnLayoutAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnLayoutAddNew.TabIndex = 4;
            this.btnLayoutAddNew.Text = "Add New";
            this.btnLayoutAddNew.Click += new System.EventHandler(this.btnLayoutAddNew_Click);
            // 
            // btnLayoutOK
            // 
            this.btnLayoutOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLayoutOK.Location = new System.Drawing.Point(3, 224);
            this.btnLayoutOK.Name = "btnLayoutOK";
            this.btnLayoutOK.Size = new System.Drawing.Size(75, 23);
            this.btnLayoutOK.TabIndex = 3;
            this.btnLayoutOK.Text = "OK";
            this.btnLayoutOK.Click += new System.EventHandler(this.btnLayoutOK_Click);
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer4.Grid = this.sp01206_AT_Report_Available_LayoutsGridControl;
            this.gridSplitContainer4.Location = new System.Drawing.Point(3, 3);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.sp01206_AT_Report_Available_LayoutsGridControl);
            this.gridSplitContainer4.Size = new System.Drawing.Size(591, 219);
            this.gridSplitContainer4.TabIndex = 5;
            // 
            // sp01206_AT_Report_Available_LayoutsGridControl
            // 
            this.sp01206_AT_Report_Available_LayoutsGridControl.DataSource = this.sp01206_AT_Report_Available_LayoutsBindingSource;
            this.sp01206_AT_Report_Available_LayoutsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sp01206_AT_Report_Available_LayoutsGridControl.Location = new System.Drawing.Point(0, 0);
            this.sp01206_AT_Report_Available_LayoutsGridControl.MainView = this.gridView2;
            this.sp01206_AT_Report_Available_LayoutsGridControl.MenuManager = this.barManager1;
            this.sp01206_AT_Report_Available_LayoutsGridControl.Name = "sp01206_AT_Report_Available_LayoutsGridControl";
            this.sp01206_AT_Report_Available_LayoutsGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.sp01206_AT_Report_Available_LayoutsGridControl.Size = new System.Drawing.Size(591, 219);
            this.sp01206_AT_Report_Available_LayoutsGridControl.TabIndex = 1;
            this.sp01206_AT_Report_Available_LayoutsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.sp01206_AT_Report_Available_LayoutsGridControl.DoubleClick += new System.EventHandler(this.sp01206_AT_Report_Available_LayoutsGridControl_DoubleClick);
            // 
            // sp01206_AT_Report_Available_LayoutsBindingSource
            // 
            this.sp01206_AT_Report_Available_LayoutsBindingSource.DataMember = "sp01206_AT_Report_Available_Layouts";
            this.sp01206_AT_Report_Available_LayoutsBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReportLayoutID,
            this.colReportLayoutName,
            this.colReportTypeName,
            this.colModuleID,
            this.colCreatedBy,
            this.colPublishedToWeb});
            this.gridView2.GridControl = this.sp01206_AT_Report_Available_LayoutsGridControl;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReportLayoutName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseDown);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colReportLayoutID
            // 
            this.colReportLayoutID.Caption = "Report Layout ID";
            this.colReportLayoutID.FieldName = "ReportLayoutID";
            this.colReportLayoutID.Name = "colReportLayoutID";
            this.colReportLayoutID.OptionsColumn.AllowEdit = false;
            this.colReportLayoutID.OptionsColumn.AllowFocus = false;
            this.colReportLayoutID.OptionsColumn.ReadOnly = true;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.Visible = true;
            this.colReportLayoutName.VisibleIndex = 0;
            this.colReportLayoutName.Width = 232;
            // 
            // colReportTypeName
            // 
            this.colReportTypeName.Caption = "Layout Type";
            this.colReportTypeName.FieldName = "ReportTypeName";
            this.colReportTypeName.Name = "colReportTypeName";
            this.colReportTypeName.OptionsColumn.AllowEdit = false;
            this.colReportTypeName.OptionsColumn.AllowFocus = false;
            this.colReportTypeName.OptionsColumn.ReadOnly = true;
            this.colReportTypeName.Visible = true;
            this.colReportTypeName.VisibleIndex = 1;
            this.colReportTypeName.Width = 94;
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.AllowEdit = false;
            this.colModuleID.OptionsColumn.AllowFocus = false;
            this.colModuleID.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.AllowFocus = false;
            this.colCreatedBy.OptionsColumn.ReadOnly = true;
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 2;
            this.colCreatedBy.Width = 136;
            // 
            // colPublishedToWeb
            // 
            this.colPublishedToWeb.Caption = "Published to Web";
            this.colPublishedToWeb.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPublishedToWeb.FieldName = "PublishedToWeb";
            this.colPublishedToWeb.Name = "colPublishedToWeb";
            this.colPublishedToWeb.OptionsColumn.AllowEdit = false;
            this.colPublishedToWeb.OptionsColumn.AllowFocus = false;
            this.colPublishedToWeb.OptionsColumn.ReadOnly = true;
            this.colPublishedToWeb.Visible = true;
            this.colPublishedToWeb.VisibleIndex = 3;
            this.colPublishedToWeb.Width = 104;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // checkEdit_LoadLinkedData
            // 
            this.checkEdit_LoadLinkedData.Location = new System.Drawing.Point(2, 26);
            this.checkEdit_LoadLinkedData.MenuManager = this.barManager1;
            this.checkEdit_LoadLinkedData.Name = "checkEdit_LoadLinkedData";
            this.checkEdit_LoadLinkedData.Properties.Caption = "Load Linked Data";
            this.checkEdit_LoadLinkedData.Size = new System.Drawing.Size(140, 19);
            this.checkEdit_LoadLinkedData.StyleController = this.layoutControl1;
            superToolTip101.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem102.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem102.Appearance.Options.UseImage = true;
            toolTipTitleItem102.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem102.Text = "Load Linked Data - Information";
            toolTipItem101.LeftIndent = 6;
            toolTipItem101.Text = resources.GetString("toolTipItem101.Text");
            superToolTip101.Items.Add(toolTipTitleItem102);
            superToolTip101.Items.Add(toolTipItem101);
            this.checkEdit_LoadLinkedData.SuperTip = superToolTip101;
            this.checkEdit_LoadLinkedData.TabIndex = 8;
            // 
            // checkEdit_LoadLinkedPictures
            // 
            this.checkEdit_LoadLinkedPictures.Location = new System.Drawing.Point(146, 26);
            this.checkEdit_LoadLinkedPictures.MenuManager = this.barManager1;
            this.checkEdit_LoadLinkedPictures.Name = "checkEdit_LoadLinkedPictures";
            this.checkEdit_LoadLinkedPictures.Properties.Caption = "Load Linked Pictures";
            this.checkEdit_LoadLinkedPictures.Size = new System.Drawing.Size(188, 19);
            this.checkEdit_LoadLinkedPictures.StyleController = this.layoutControl1;
            superToolTip53.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem54.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem54.Appearance.Options.UseImage = true;
            toolTipTitleItem54.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem54.Text = "Load Linked Pictures - Information";
            toolTipItem53.LeftIndent = 6;
            toolTipItem53.Text = resources.GetString("toolTipItem53.Text");
            superToolTip53.Items.Add(toolTipTitleItem54);
            superToolTip53.Items.Add(toolTipItem53);
            this.checkEdit_LoadLinkedPictures.SuperTip = superToolTip53;
            this.checkEdit_LoadLinkedPictures.TabIndex = 7;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(338, 26);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(95, 22);
            this.btnView.StyleController = this.layoutControl1;
            this.btnView.TabIndex = 6;
            this.btnView.Text = "View Report";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18,
            this.layoutControlItem17,
            this.layoutControlItem3,
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(435, 53);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.checkEdit_LoadLinkedData;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(144, 29);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.checkEdit_LoadLinkedPictures;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(144, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(192, 29);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnView;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(336, 24);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(99, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(99, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(99, 29);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHtmlStringInCaption = true;
            this.layoutControlItem2.Control = this.popupContainerEdit1;
            this.layoutControlItem2.CustomizationFormText = "Layout:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(435, 24);
            this.layoutControlItem2.Text = "Layout:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(37, 13);
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp01220_AT_Reports_Listings_Tree_FilterTableAdapter
            // 
            this.sp01220_AT_Reports_Listings_Tree_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp01206_AT_Report_Available_LayoutsTableAdapter
            // 
            this.sp01206_AT_Report_Available_LayoutsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter
            // 
            this.sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp01232_AT_Reports_Listings_Action_FilterTableAdapter
            // 
            this.sp01232_AT_Reports_Listings_Action_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // bbiPublishToWeb
            // 
            this.bbiPublishToWeb.Caption = "Toggle Published To Web";
            this.bbiPublishToWeb.Enabled = false;
            this.bbiPublishToWeb.Id = 27;
            this.bbiPublishToWeb.Name = "bbiPublishToWeb";
            toolTipTitleItem52.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem52.Appearance.Options.UseImage = true;
            toolTipTitleItem52.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem52.Text = "Toggle Published To Web - Information";
            toolTipItem52.LeftIndent = 6;
            toolTipItem52.Text = resources.GetString("toolTipItem52.Text");
            superToolTip52.Items.Add(toolTipTitleItem52);
            superToolTip52.Items.Add(toolTipItem52);
            this.bbiPublishToWeb.SuperTip = superToolTip52;
            this.bbiPublishToWeb.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiPublishToWeb.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPublishToWeb_ItemClick);
            // 
            // bbiUnpublishToWeb
            // 
            this.bbiUnpublishToWeb.Caption = "bbiUnpublishToWeb";
            this.bbiUnpublishToWeb.Id = 28;
            this.bbiUnpublishToWeb.Name = "bbiUnpublishToWeb";
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.sp01206_AT_Report_Available_LayoutsGridControl;
            // 
            // frm_AT_Report_Tree_Listing
            // 
            this.ClientSize = new System.Drawing.Size(1427, 807);
            this.Controls.Add(this.popupContainerControl1);
            this.Controls.Add(this.printControl1);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ribbonControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Report_Tree_Listing";
            this.Text = "Listing Reports";
            this.Activated += new System.EventHandler(this.frm_AT_Report_Tree_Listing_Activated);
            this.Load += new System.EventHandler(this.frm_AT_Report_Tree_Listing_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.ribbonStatusBar1, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.Controls.SetChildIndex(this.printControl1, 0);
            this.Controls.SetChildIndex(this.popupContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printRibbonController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01220ATReportsListingsTreeFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01230ATReportsListingsInspectionFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInspectionFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceLastInspectionOnly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActionFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01232ATReportsListingsActionFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp01206_AT_Report_Available_LayoutsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01206_AT_Report_Available_LayoutsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_LoadLinkedData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_LoadLinkedPictures.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraPrinting.Preview.PrintRibbonController printRibbonController1;
        private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem8;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem9;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem10;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem11;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem12;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem13;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem14;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem15;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem16;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem17;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem18;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem19;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem20;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem21;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem22;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem23;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem24;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem25;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem26;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem27;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem28;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem29;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem30;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem31;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem32;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem33;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem34;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem35;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem36;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem37;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem38;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem39;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem40;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem41;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem42;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem43;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem44;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem45;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage printPreviewRibbonPage1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup7;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnView;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraGrid.GridControl sp01206_AT_Report_Available_LayoutsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraEditors.SimpleButton btnLayoutAddNew;
        private DevExpress.XtraEditors.SimpleButton btnLayoutOK;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_AT dataSet_AT;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraEditors.SimpleButton btnLoadTrees;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private System.Windows.Forms.BindingSource sp01220ATReportsListingsTreeFilterBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colVisibility;
        private DevExpress.XtraGrid.Columns.GridColumn colContext;
        private DevExpress.XtraGrid.Columns.GridColumn colManagement;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroundType;
        private DevExpress.XtraGrid.Columns.GridColumn colDBH;
        private DevExpress.XtraGrid.Columns.GridColumn colDBHRange;
        private DevExpress.XtraGrid.Columns.GridColumn colHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colHeightRange;
        private DevExpress.XtraGrid.Columns.GridColumn colProtectionType;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteHazardClass;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colMapLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget1;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget2;
        private DevExpress.XtraGrid.Columns.GridColumn colTarget3;
        private DevExpress.XtraGrid.Columns.GridColumn colLastModifiedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colAgeClass;
        private DevExpress.XtraGrid.Columns.GridColumn colX;
        private DevExpress.XtraGrid.Columns.GridColumn colY;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaHa;
        private DevExpress.XtraGrid.Columns.GridColumn colReplantCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeType;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colHedgeLength;
        private DevExpress.XtraGrid.Columns.GridColumn colGardenSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPropertyProximity;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSituation;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colcolor;
        private WoodPlan5.DataSet_ATTableAdapters.sp01220_AT_Reports_Listings_Tree_FilterTableAdapter sp01220_AT_Reports_Listings_Tree_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp01206_AT_Report_Available_LayoutsBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp01206_AT_Report_Available_LayoutsTableAdapter sp01206_AT_Report_Available_LayoutsTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.DateEdit deInspectionFromDate;
        private DevExpress.XtraEditors.CheckEdit ceLastInspectionOnly;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SimpleButton btnLoadInspections;
        private DevExpress.XtraEditors.DateEdit deInspectionToDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private System.Windows.Forms.BindingSource sp01230ATReportsListingsInspectionFilterBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectorName;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colStemPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colStemPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colStemPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colStemDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colStemDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colStemDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colAngleToVertical;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownFoliation1;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownFoliation2;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownFoliation3;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskCategory1;
        private DevExpress.XtraGrid.Columns.GridColumn colGeneralCondition;
        private DevExpress.XtraGrid.Columns.GridColumn colRootHeave1;
        private DevExpress.XtraGrid.Columns.GridColumn colRootHeave2;
        private DevExpress.XtraGrid.Columns.GridColumn colRootHeave3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateLastModified;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser11;
        private DevExpress.XtraGrid.Columns.GridColumn colUser21;
        private DevExpress.XtraGrid.Columns.GridColumn colUser31;
        private DevExpress.XtraGrid.Columns.GridColumn colcolor1;
        private WoodPlan5.DataSet_ATTableAdapters.sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter sp01230_AT_Reports_Listings_Inspection_FilterTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.SimpleButton btnLoadActions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private System.Windows.Forms.BindingSource sp01232ATReportsListingsActionFilterBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeGridReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDistance1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeHouseName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectorName1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentReferenceNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colActionOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudget;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colDateLastModified1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colUser12;
        private DevExpress.XtraGrid.Columns.GridColumn colUser22;
        private DevExpress.XtraGrid.Columns.GridColumn colUser32;
        private DevExpress.XtraGrid.Columns.GridColumn colcolor2;
        private WoodPlan5.DataSet_ATTableAdapters.sp01232_AT_Reports_Listings_Action_FilterTableAdapter sp01232_AT_Reports_Listings_Action_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostDifference;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraPrinting.Preview.ProgressBarEditItem progressBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem46;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem2;
        private DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem zoomTrackBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedInspectionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedOutstandingActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTimeliness;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colNoFurtherActionRequired;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.DateEdit deActionToDate;
        private DevExpress.XtraEditors.DateEdit deActionFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1b;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2b;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3b;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1c;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2c;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3c;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrderCompleteDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrderIssueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCavat;
        private DevExpress.XtraGrid.Columns.GridColumn colStemCount;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownNorth;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownSouth;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownEast;
        private DevExpress.XtraGrid.Columns.GridColumn colCrownWest;
        private DevExpress.XtraGrid.Columns.GridColumn colRetentionCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colSULE;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesVariety;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectLength;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist11;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist21;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist31;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist12;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist22;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist32;
        private DevExpress.XtraEditors.CheckEdit checkEdit_LoadLinkedData;
        private DevExpress.XtraEditors.CheckEdit checkEdit_LoadLinkedPictures;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraGrid.Columns.GridColumn colPublishedToWeb;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiPublishToWeb;
        private DevExpress.XtraBars.BarButtonItem bbiUnpublishToWeb;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using DevExpress.Utils;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Layer_Label_Properties : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int intTransparency = 0;
        public int intLineColour = 0;
        public int intLineWidth = 0;
        public string strCaller = "WorkspaceEdit";  // This controls how the data is bound and if any updates occur to the back-end DB //

        GridHitInfo downHitInfo = null;
        public int intParentLayerID = 0;

        private DataSet _dsFontStyle;
        private DataSet _dsLabelStructure;

        #endregion

        public frm_AT_Mapping_Layer_Label_Properties()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Layer_Label_Properties_Load(object sender, EventArgs e)
        {
            this.FormID = 20044;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked at end of event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp01269_AT_Tree_Picker_Workspace_layer_Label_fieldsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01269_AT_Tree_Picker_Workspace_layer_Label_fieldsTableAdapter.Fill(dataSet_AT_TreePicker.sp01269_AT_Tree_Picker_Workspace_layer_Label_fields);

            if (strCaller == "WorkspaceEdit")
            {

                sp01268_AT_Tree_Picker_Workspace_layer_labellingTableAdapter.Connection.ConnectionString = strConnectionString;

                sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_EditTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_EditTableAdapter.Fill(dataSet_AT_TreePicker.sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_Edit, intParentLayerID);
            }
            else  // Called from TreePicker so bind to datasets which were passes by reference //
            {
                dataLayoutControl1.DataSource = _dsFontStyle.Tables[0];
                gridControl3.DataSource = _dsLabelStructure.Tables[0];
            }

            tbcTransparency.EditValue = intTransparency;
            colorEdit1.EditValue = intLineColour;
            spinEdit1.EditValue = intLineWidth;
           
            LoadStructureGrid();

            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

        }

        public DataSet dsFontStyle
        {
            get
            {
                return _dsFontStyle;
            }
            set
            {
                // does not create a new dataset, but gives the variable a reference to the dataset assigned to dsFontStyle  //
                _dsFontStyle = value;
            }
        }

        public DataSet dsLabelStructure
        {
            get
            {
                return _dsLabelStructure;
            }
            set
            {
                // does not create a new dataset, but gives the variable a reference to the dataset assigned to dsLabelStructure  //
                _dsLabelStructure = value;
            }
        }



        private void LoadStructureGrid()
        {
            this.sp01268_AT_Tree_Picker_Workspace_layer_labellingTableAdapter.Fill(dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling, intParentLayerID);
            gridControl3.ForceInitialize();
            GridView view = (GridView)gridControl3.MainView;
            bool boolLabelRowsRequired = true;
            bool boolTooltipRowsRequired = true;
            if (view.DataRowCount > 0)
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "StructureType")) == 0)
                    {
                        boolLabelRowsRequired = false;
                    }
                    else  if (Convert.ToInt32(view.GetRowCellValue(i, "StructureType")) == 1)
                    {
                        boolTooltipRowsRequired = false;
                    }
                }
            }
            DataRow drNewRow;
            if (boolLabelRowsRequired)  // This should never be required! //
            {
                // Add 2 rows for Label Structure //
                drNewRow = dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.NewRow();
                drNewRow["LayerID"] = intParentLayerID;
                drNewRow["StructureType"] = 0;
                drNewRow["StructureTypeDescription"] = "Label";
                drNewRow["Prefix"] = "";
                drNewRow["ColumnName"] = "strTreeRef";
                drNewRow["Suffix"] = "";
                drNewRow["NewLine"] = 1;
                drNewRow["Seperator"] = "";
                drNewRow["FieldOrder"] = 1;
                dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.Rows.Add(drNewRow);

                drNewRow = dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.NewRow();
                drNewRow["LayerID"] = intParentLayerID;
                drNewRow["StructureType"] = 0;
                drNewRow["StructureTypeDescription"] = "Label";
                drNewRow["Prefix"] = "";
                drNewRow["ColumnName"] = "strSpecies";
                drNewRow["Suffix"] = "";
                drNewRow["NewLine"] = 0;
                drNewRow["Seperator"] = "";
                drNewRow["FieldOrder"] = 2;
                dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.Rows.Add(drNewRow);
            }
            if (boolTooltipRowsRequired)
            {
                // Add 2 rows for ToolTip Structure //
                drNewRow = dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.NewRow();
                drNewRow["LayerID"] = intParentLayerID;
                drNewRow["StructureType"] = 1;
                drNewRow["StructureTypeDescription"] = "Tooltip";
                drNewRow["Prefix"] = "";
                drNewRow["ColumnName"] = "strTreeRef";
                drNewRow["Suffix"] = "";
                drNewRow["NewLine"] = 1;
                drNewRow["Seperator"] = "";
                drNewRow["FieldOrder"] = 1;
                dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.Rows.Add(drNewRow);

                drNewRow = dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.NewRow();
                drNewRow["LayerID"] = intParentLayerID;
                drNewRow["StructureType"] = 1;
                drNewRow["StructureTypeDescription"] = "Tooltip";
                drNewRow["Prefix"] = "";
                drNewRow["ColumnName"] = "strSpecies";
                drNewRow["Suffix"] = "";
                drNewRow["NewLine"] = 0;
                drNewRow["Seperator"] = "";
                drNewRow["FieldOrder"] = 2;
                dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.Rows.Add(drNewRow);
            }
        }

        private void popupContainerEditLabelPosition_QueryPopUp(object sender, CancelEventArgs e)
        {
            switch (popupContainerEditLabelPosition.EditValue.ToString())
            {
                case "BottomCentre":
                    alignmentControl1.Alignment = ContentAlignment.BottomCenter;
                    break;
                case "BottomLeft":
                    alignmentControl1.Alignment = ContentAlignment.BottomLeft;
                    break;
                case "BottomRight":
                    alignmentControl1.Alignment = ContentAlignment.BottomRight;
                    break;
                case "CentreCenter":
                    alignmentControl1.Alignment = ContentAlignment.MiddleCenter;  //.CenterCenter;
                    break;
                case "CentreLeft":
                    alignmentControl1.Alignment = ContentAlignment.MiddleLeft;  //.CenterLeft;
                    break;
                case "CentreRight":
                    alignmentControl1.Alignment = ContentAlignment.MiddleRight;  //.CenterRight;
                    break;
                case "TopCentre":
                    alignmentControl1.Alignment = ContentAlignment.TopCenter;
                    break;
                case "TopLeft":
                    alignmentControl1.Alignment = ContentAlignment.TopLeft;
                    break;
                case "TopRight":
                    alignmentControl1.Alignment = ContentAlignment.TopRight;
                    break;
                default:
                    break;
            }
        }

        private void alignmentControl1_AlignmentChanged(object sender, EventArgs e)
        {
            switch (alignmentControl1.Alignment.ToString())
            {
                case "MiddleCenter":
                    popupContainerEditLabelPosition.EditValue = "CenterCenter";
                    break;
                case "MiddleLeft":
                    popupContainerEditLabelPosition.EditValue = "CenterLeft";
                    break;
                case "MiddleRight":
                    popupContainerEditLabelPosition.EditValue = "CenterRight";
                    break;
                default:  // All other choices //
                    popupContainerEditLabelPosition.EditValue = alignmentControl1.Alignment.ToString();
                    break;
            }
            // Close the dropdown accepting the user's choice //
            Control obj = sender as Control;
            (obj.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Commit any outstanding changes to underlying dataset //
            GridView view = (GridView)gridControl3.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow()) return;
        
            intTransparency = Convert.ToInt32(tbcTransparency.EditValue);
            intLineColour = Convert.ToInt32(colorEdit1.EditValue);
            intLineWidth = Convert.ToInt32(spinEdit1.EditValue);

            int intErrorCount = 0;
            for (int i = 0; i < view.DataRowCount ; i++)
            {
            	if (string.IsNullOrEmpty(view.GetRowCellValue(i, "ColumnName").ToString())) intErrorCount++;
            }
            if (intErrorCount > 0)
            {
                XtraMessageBox.Show("Unable to update the current settings... " + intErrorCount.ToString() + " record[s] have a missing " + view.Columns["ColumnName"].Caption + "!\n\nSelect a " + view.Columns["ColumnName"].Caption + " or delete the row(s) before proceeding.\n\nTip: if you cannot see any rows with missing values, a filter may be active on the grid - clear the filter before procceding.", "Update Label Layer Properties", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (strCaller == "WorkspaceEdit")
            {
                // Update Font Settings in Font Record //
                this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource.EndEdit();
                try
                {
                    this.sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_EditTableAdapter.Update(dataSet_AT_TreePicker);  // Update query defined in Table Adapter //
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while saving the Font Settings [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Map Object Label Layer Properties", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // Update Label\Tooltip Structure //
                this.sp01268ATTreePickerWorkspacelayerlabellingBindingSource.EndEdit();
                try
                {
                    this.sp01268_AT_Tree_Picker_Workspace_layer_labellingTableAdapter.Update(dataSet_AT_TreePicker);  // Add, Update, Delete queries defined in Table Adapter //
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while saving the Label \\Tooltip Structure Settings [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Map Object Label Layer Properties", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails any validation tests //
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


        #region GridControl

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    e.Handled = true;
                    DialogResult ldr = XtraMessageBox.Show("Do you wish to add a Label Line?\n\nClick Yes for a Label Line.\nClick No for a Tooltip Line.", "Add Label / Tooltip Line", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    if (ldr == DialogResult.Yes)
                    {
                        AddRecord(0);
                    }
                    else if (ldr == DialogResult.No)
                    {
                        AddRecord(1);
                    }
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    DeleteRecord();
                    e.Handled = true;
                    break;
            }
        }

        private void gridView3_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView3_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No label \\ tooltip structure lines present - right click for menu");
        }

        bool internalRowFocusing;
        private void gridView3_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView3_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            // Disable Show Group By Box Menu Item //
            if (e.Menu == null) return;
            for (int i = e.Menu.Items.Count - 1; i >= 0; i--)
            {
                if (e.Menu.Items[i].Caption == "Show Group By Box")
                {
                    e.Menu.Items[i].Enabled = false;
                    break;
                }
            }
        }

        #endregion


        private void barButtonItemAddLabelLine_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddRecord(0);
        }

        private void barButtonItemAddTooltipLine_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddRecord(2);
        }

        private void barButtonItemDeleteLine_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeleteRecord();
        }

        private void AddRecord(int intType)
        {
            int intMaxOrder = 0;
            GridView view = (GridView)gridControl3.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "StructureType")) == @intType && Convert.ToInt32(view.GetRowCellValue(i, "FieldOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "FieldOrder"));
            }

            DataRow drNewRow;
            if (strCaller == "WorkspaceEdit")
            {
                drNewRow = dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.NewRow();
            }
            else  // TreePicker //
            {
                drNewRow = dsLabelStructure.Tables[0].NewRow();
            }
            drNewRow["LayerID"] = intParentLayerID;
            drNewRow["StructureType"] = intType;
            drNewRow["StructureTypeDescription"] = (intType == 0? "Label" : "Tooltip");
            drNewRow["Prefix"] = "";
            drNewRow["ColumnName"] = "";
            drNewRow["Suffix"] = "";
            drNewRow["NewLine"] = 0;
            drNewRow["Seperator"] = "";
            drNewRow["FieldOrder"] = intMaxOrder + 1;

            if (strCaller == "WorkspaceEdit")
            {
                dataSet_AT_TreePicker.sp01268_AT_Tree_Picker_Workspace_layer_labelling.Rows.Add(drNewRow);
            }
            else  // TreePicker //
            {
                dsLabelStructure.Tables[0].Rows.Add(drNewRow);
            }
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)gridControl3.MainView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                XtraMessageBox.Show("Select the row to be deleted by clicking on it first then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have 1 Label / Tooltip Structure Line selected for delete!\n\nProceed?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                gridControl3.BeginUpdate();
                if (strCaller == "WorkspaceEdit")
                {
                    view.DeleteRow(view.FocusedRowHandle);  // Delete record from grid //
                }
                else  // TreePicker //
                {
                     DataRow row = view.GetDataRow(view.FocusedRowHandle);
                     //dsLabelStructure.Tables[0].Rows.Remove(row);
                     view.DeleteRow(view.FocusedRowHandle);  // Use this delete [not line above] so the row is still available should the user do a SaveAs Workspace on the TreePicker screen //
                }
                gridControl3.EndUpdate();
            }
        }

  



    }
}


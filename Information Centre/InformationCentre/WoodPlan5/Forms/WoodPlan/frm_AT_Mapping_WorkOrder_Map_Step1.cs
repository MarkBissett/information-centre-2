using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;  // Required for Images Hashtable //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_AT_Mapping_WorkOrder_Map_Step1 : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string i_str_selected_workorders = "";

        BaseObjects.GridCheckMarksSelection selection1;
        AutoScrollHelper autoScrollHelper;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        string strMapsPath = "";
        Hashtable Images = new Hashtable();
        public frm_AT_Mapping_Tree_Picker frmParentMappingForm = null;
        public int intCurrentMapScale = 0; 
        public int intDPI = 0;  // DotsPerInch [Passed through to create map screen] // 
  
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //

        int i_int_FocusedGrid = 1;
        bool boolOrderChanged = false;  // Determins if code fires to save record order changes //

        #endregion

        public frm_AT_Mapping_WorkOrder_Map_Step1()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_WorkOrder_Map_Step1_Load(object sender, EventArgs e)
        {
            this.FormID = 200411;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strMapsPath = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderMapLocation")) + "\\";
                if (strMapsPath == null) strMapsPath = "C:\\";
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred when attempting to get the folder used to hold saved map images [" + ex.Message + "]!", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
            RefreshGridViewState1 = new RefreshGridState(gridView1, "WorkOrderMapID");
            RefreshGridViewState2 = new RefreshGridState(gridView2, "WorkOrderMapID");

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            deFromDateFilter.DateTime = this.GlobalSettings.ViewedStartDate;
            deToDateFilter.DateTime = this.GlobalSettings.ViewedEndDate;

            sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            Load_Unlinked_Maps_Grid();
            gridView2.ExpandAllGroups();
            
            sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrdersTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrdersTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrders, this.GlobalSettings.ViewedStartDate, this.GlobalSettings.ViewedEndDate, Convert.ToInt32(ceIncludeCompletedFilter.EditValue));
            gridControl3.ForceInitialize();

            sp01296_AT_Tree_Picker_WorkOrder_Map_ListTableAdapter.Connection.ConnectionString = strConnectionString;

            if (i_str_selected_workorders != "")
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = i_str_selected_workorders.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length > 0)
                {
                    GridView view = (GridView)gridControl3.MainView;
                    GridColumn column = view.Columns["WorkOrderID"];
                    int intFoundRow = 0;
                    view.BeginUpdate();
                    int intLastFoundRow = GridControl.InvalidRowHandle;
                    for (int i = 0; i < strArray.Length; i++)
                    {
                        intFoundRow = view.LocateByValue(0, column, Convert.ToInt32(strArray[i]));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                            intLastFoundRow = intFoundRow;
                        }
                    }
                    view.EndUpdate();
                    if (selection1.SelectedCount > 0)
                    {
                        Load_Maps_Grid();  // Load Saved Maps if WorkOrder ID(s) have been passed in //
                        gridView1.ExpandAllGroups();
                        popupContainerEdit1.EditValue = PopupContainerEdit1_Get_Selected();
                    }
                }
            }

            autoScrollHelper = new AutoScrollHelper(gridView1);  // Add Autoscroll functionality when dragging to adjust record order //

        }

        private void frm_AT_Mapping_WorkOrder_Map_Step1_FormClosing(object sender, FormClosingEventArgs e)
        {           
            base.ImagePreviewClear();
            GC.GetTotalMemory(true);
        }

        private void Load_Maps_Grid()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01296_AT_Tree_Picker_WorkOrder_Map_ListTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01296_AT_Tree_Picker_WorkOrder_Map_List, i_str_selected_workorders);
            view.EndUpdate();
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //

        }

        private void Load_Unlinked_Maps_Grid()
        {
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_ListTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01298_AT_Tree_Picker_WorkOrder_Map_Unallocated_List);
            view.EndUpdate();
            this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //

        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No maps available - right-click or choose the Add button to add maps";
                    break;
                case "gridView2":
                    message = "No maps available - right-click or choose the Add button to add maps";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridControl1

        #region Drag Drop Functionality

        // AutoScrollHelper autoScrollHelper declared under instance vars //
        // autoScrollHelper linked to grid in Form Load Event //
        // gridControl1.AllowDrop = true in it's settings //

        int dropTargetRowHandle = -1;
        int DropTargetRowHandle
        {
            get
            {
                return dropTargetRowHandle;
            }
            set
            {
                dropTargetRowHandle = value;
                gridControl1.Invalidate();
            }
        }
        const string OrderFieldName = "MapOrder";  // Make sure this field is not ReadOnly in DataSet //

        private void gridControl1_DragOver(object sender, DragEventArgs e)
        {
            GridControl grid = (GridControl)sender;
            System.Drawing.Point pt = new System.Drawing.Point(e.X, e.Y);
            pt = grid.PointToClient(pt);
            GridView view = grid.GetViewAt(pt) as GridView;
            if (view == null) return;
            GridHitInfo hitInfo = view.CalcHitInfo(pt);
            if (hitInfo.HitTest == GridHitTest.EmptyRow)
            {
                DropTargetRowHandle = view.DataRowCount;
            }
            else
            {
                DropTargetRowHandle = hitInfo.RowHandle;
            }

            e.Effect = DragDropEffects.None;

            // Prevent row being dragged to a different group //
            GridHitInfo srcHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            int sourceRow = srcHitInfo.RowHandle;
            if (sourceRow < 0 || DropTargetRowHandle < 0) return;
            if (view.GetRowCellValue(sourceRow, "WorkOrderID").ToString() != view.GetRowCellValue(DropTargetRowHandle, "WorkOrderID").ToString()) return;

            GridHitInfo downHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            if (downHitInfo != null)
            {
                if (hitInfo.RowHandle != downHitInfo.RowHandle) e.Effect = DragDropEffects.Move;
            }

            autoScrollHelper.ScrollIfNeeded(hitInfo);
        }

        private void gridControl1_DragDrop(object sender, DragEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.MainView as GridView;
            GridHitInfo srcHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            GridHitInfo hitInfo = view.CalcHitInfo(grid.PointToClient(new System.Drawing.Point(e.X, e.Y)));
            int sourceRow = srcHitInfo.RowHandle;
            int targetRow = (hitInfo.RowHandle == GridControl.InvalidRowHandle ? -1 : hitInfo.RowHandle);

            if (DropTargetRowHandle < 0) return;  // No Valid Place for Dropping //

            DropTargetRowHandle = -1;
            MoveRow(sourceRow, targetRow, view);
        }

        private void gridControl1_DragLeave(object sender, EventArgs e)
        {
            DropTargetRowHandle = -1;
        }

        private void gridControl1_Paint(object sender, PaintEventArgs e)
        {
            if (DropTargetRowHandle < 0) return;
            GridControl grid = (GridControl)sender;
            GridView view = (GridView)grid.MainView;

            bool isBottomLine = DropTargetRowHandle == view.DataRowCount;

            GridViewInfo viewInfo = view.GetViewInfo() as GridViewInfo;
            GridRowInfo rowInfo = viewInfo.GetGridRowInfo(isBottomLine ? DropTargetRowHandle - 1 : DropTargetRowHandle);

            if (rowInfo == null) return;

            System.Drawing.Point p1, p2;
            if (isBottomLine)
            {
                p1 = new System.Drawing.Point(rowInfo.Bounds.Left, rowInfo.Bounds.Bottom - 1);
                p2 = new System.Drawing.Point(rowInfo.Bounds.Right, rowInfo.Bounds.Bottom - 1);
            }
            else
            {
                p1 = new System.Drawing.Point(rowInfo.Bounds.Left, rowInfo.Bounds.Top - 1);
                p2 = new System.Drawing.Point(rowInfo.Bounds.Right, rowInfo.Bounds.Top - 1);
            }
            e.Graphics.DrawLine(Pens.Blue, p1, p2);
        }

        private void gridView1_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == MouseButtons.Left && downHitInfo != null)
            {
                Size dragSize = SystemInformation.DragSize;
                System.Drawing.Rectangle dragRect = new System.Drawing.Rectangle(new System.Drawing.Point(downHitInfo.HitPoint.X - dragSize.Width / 2, downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new System.Drawing.Point(e.X, e.Y)))
                {
                    view.GridControl.DoDragDrop(downHitInfo, DragDropEffects.All);
                    downHitInfo = null;
                }
            }
        }

        private void MoveRow(int sourceRow, int targetRow, GridView view)
        {
            view.BeginDataUpdate();
            view.BeginSort();
            int intNewOrder = Convert.ToInt32(view.GetRowCellValue(targetRow, OrderFieldName));
            if (sourceRow > targetRow && targetRow != -1)
            {
                for (int i = targetRow; i < sourceRow; i++)
                {
                    view.SetRowCellValue(i, OrderFieldName, Convert.ToInt32(view.GetRowCellValue(i, OrderFieldName)) + 1);
                }
                DataRow dragRow = view.GetDataRow(sourceRow);
                dragRow[OrderFieldName] = intNewOrder;
                boolOrderChanged = true;
            }
            else
            {
                if (targetRow > sourceRow && targetRow != -1) // Not trying to position on end of grid //
                {
                    for (int i = targetRow - 1; i > sourceRow; i--)
                    {
                        view.SetRowCellValue(i, OrderFieldName, Convert.ToInt32(view.GetRowCellValue(i, OrderFieldName)) - 1);
                    }
                    DataRow dragRow = view.GetDataRow(sourceRow);
                    dragRow[OrderFieldName] = intNewOrder - 1;
                    boolOrderChanged = true;
                }
                else // Positioning on end of grid //
                {
                    targetRow = view.DataRowCount - 1;
                    for (int i = targetRow; i > sourceRow; i--)
                    {
                        view.SetRowCellValue(i, OrderFieldName, Convert.ToInt32(view.GetRowCellValue(i, OrderFieldName)) - 1);
                    }
                    DataRow dragRow = view.GetDataRow(sourceRow);
                    dragRow[OrderFieldName] = intNewOrder;
                    boolOrderChanged = true;
                }
            }
            view.EndSort();
            view.EndDataUpdate();
            
            SaveChangesToOrder();  // Save any changes to record Orders //
        }

        #endregion

        private void gridView1_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            Bitmap bmpTest = new Bitmap(33, 33);
            if (e.Column.FieldName == "gridColumnThumbNail" && e.IsGetData)
            {
                DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                string strImagePath = strMapsPath + view.GetRowCellValue(rHandle, "WorkOrderMapID").ToString() + "_thumb.jpg";
                strImagePath = strImagePath.ToLower();
                if (!Images.ContainsKey(strImagePath))
                {
                    Image img = null;
                    try
                    {
                        img = Image.FromFile(strImagePath);
                        bmpTest = new Bitmap(img, 33, 33);
                        img = null;
                        img.Dispose();
                    }
                    catch
                    {
                    }
                    Images.Add(strImagePath, bmpTest);
                }
                e.Value = Images[strImagePath];
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /*bbiSave.Enabled = false;
                bbiBlockAdd.Enabled = false;
                bbiBlockEdit.Enabled = false;
                bbiSingleAdd.Enabled = (view.DataRowCount > 10 ? false : true);
                bbiDelete.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                bsiEdit.Enabled = false;
                bbiSingleEdit.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                bsiDataset.Enabled = false;
                bbiPreviewMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);*/
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    e.Handled = true;
                    AddRecord(gridControl1);
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    e.Handled = true;
                    DeleteRecord(gridControl1);
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_MouseMove(object sender, MouseEventArgs e)
        {
            // Note that the form must have the Form frmMain2 as its Owner for the message to be passed through to show the Image in the preview pane. Clear message sent on Form Closing //
            GridView view = (GridView)gridControl1.MainView;

            Point ptMousePosition = new Point(e.X, e.Y);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo ghi = view.CalcHitInfo(ptMousePosition);
            if (ghi.HitTest == DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowCell)
            {
                if (ghi.Column.FieldName == "gridColumnThumbNail")
                {
                    string strImagePath = strMapsPath + view.GetRowCellValue(ghi.RowHandle, "WorkOrderMapID").ToString() + ".gif";
                    try
                    {
                        Image PassedImage = Image.FromFile(strImagePath);
                        base.ImagePreview(PassedImage, "scale", "no");
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        #endregion


        #region GridControl2

        private void gridView2_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            Bitmap bmpTest = new Bitmap(33, 33);
            if (e.Column.FieldName == "gridColumnThumbNail" && e.IsGetData)
            {
                DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                string strImagePath = strMapsPath + view.GetRowCellValue(rHandle, "WorkOrderMapID").ToString() + "_thumb.jpg";
                strImagePath = strImagePath.ToLower();
                if (!Images.ContainsKey(strImagePath))
                {
                    Image img = null;
                    try
                    {
                        img = Image.FromFile(strImagePath);
                        bmpTest = new Bitmap(img, 33, 33);
                        img = null;
                        img.Dispose();
                    }
                    catch
                    {
                    }
                    Images.Add(strImagePath, bmpTest);
                }
                e.Value = Images[strImagePath];
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /* bbiSave.Enabled = false;
                 bbiBlockAdd.Enabled = false;
                 bbiBlockEdit.Enabled = false;
                 bbiSingleAdd.Enabled = (view.DataRowCount > 10 ? false : true);
                 bbiDelete.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                 bsiEdit.Enabled = false;
                 bbiSingleEdit.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                 bsiDataset.Enabled = false;
                 bbiPreviewMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);*/
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    e.Handled = true;
                    AddRecord(gridControl2);
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    e.Handled = true;
                    DeleteRecord(gridControl2);
                    break;
                default:
                    break;
            }
        }

        private void gridControl2_MouseMove(object sender, MouseEventArgs e)
        {
            // Note that the form must have the Form frmMain2 as its Owner for the message to be passed through to show the Image in the preview pane. Clear message sent on Form Closing //
            GridView view = (GridView)gridControl2.MainView;

            Point ptMousePosition = new Point(e.X, e.Y);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo ghi = view.CalcHitInfo(ptMousePosition);
            if (ghi.HitTest == DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowCell)
            {
                if (ghi.Column.FieldName == "gridColumnThumbNail")
                {
                    string strImagePath = strMapsPath + view.GetRowCellValue(ghi.RowHandle, "WorkOrderMapID").ToString() + ".gif";
                    try
                    {
                        Image PassedImage = Image.FromFile(strImagePath);
                        base.ImagePreview(PassedImage, "scale", "no");
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        #endregion


        #region PopupContainer1

        private void btnRefreshWorkOrderFilterList_Click(object sender, EventArgs e)
        {
            sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrdersTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01297_AT_Tree_Picker_WorkOrder_Map_Available_WorkOrders, deFromDateFilter.DateTime, deToDateFilter.DateTime, Convert.ToInt32(ceIncludeCompletedFilter.EditValue));
        }

        private void popupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent.Parent as PopupContainerControl).OwnerEdit.ClosePopup();  // Extra Parent as it resides inside a LayOut control then the Popup Container //
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_workorders = "";    // Reset any prior values first //
            string strSelectedWorkOrders = "";
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_workorders = "";
                return "No Work Order(s)";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_workorders = "";
                return "No Work Order(s)";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_workorders += Convert.ToString(view.GetRowCellValue(i, "WorkOrderID")) + ",";
                        if (intCount == 0)
                        {
                            strSelectedWorkOrders = Convert.ToString(view.GetRowCellValue(i, "PaddedWorkOrderID"));
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedWorkOrders += ", " + Convert.ToString(view.GetRowCellValue(i, "PaddedWorkOrderID"));
                        }
                        intCount++;
                    }
                }
            }
            return strSelectedWorkOrders;
        }

        private void popupContainerEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString())
            {
                case "RefreshList":
                    WaitDialogForm loading = new WaitDialogForm("Loading Saved Maps list...", "WoodPlan Mapping");
                    loading.Show();

                    if (i_str_selected_workorders == null) i_str_selected_workorders = "";
                    Load_Maps_Grid();

                    loading.Close();
                    break;
            }
        }


        #endregion


        private void SaveChangesToOrder()
        {
            if (!boolOrderChanged) return;

            // Save Changes to Workspace //
            this.sp01296ATTreePickerWorkOrderMapListBindingSource.EndEdit();
            try
            {
                this.sp01296_AT_Tree_Picker_WorkOrder_Map_ListTableAdapter.Update(dataSet_AT_WorkOrders);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Unable to save Mpa Order changes - an error occurred while saving [" + ex.Message + "]!", "Save Map Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            boolOrderChanged = false;  // Reset flag //
        }

        public void SetMenuStatus()
        {
            GridView view = null;
            int[] intRowHandles;
            switch (i_int_FocusedGrid)
            {
                case 1: 
                    view = (GridView)gridControl1.MainView;
                    break;
                default:
                    view = (GridView)gridControl2.MainView;
                    break;
            }
            intRowHandles = view.GetSelectedRows();
            bbiSave.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiSingleAdd.Enabled = (view.DataRowCount > 10 ? false : true);
            bbiDelete.Enabled = (intRowHandles.Length > 0 ? true : false);
            bbiSingleEdit.Enabled = false;
            bsiEdit.Enabled = false;
            bsiDataset.Enabled = false;
            bbiPreviewMap.Enabled = (intRowHandles.Length == 1 ? true : false);
            bbiAllocate.Enabled = (intRowHandles.Length == 1 && i_int_FocusedGrid == 2 ? true : false);
            bbiUnallocate.Enabled = (intRowHandles.Length == 1 && i_int_FocusedGrid == 1 ? true : false);
            bsiMapTools.Enabled = (intRowHandles.Length == 1 ? true : false);
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            if (gridControl1.Focused)
            {
                AddRecord(gridControl1);
            }
            else
            {
                AddRecord(gridControl2);
            }
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            if (gridControl1.Focused)
            {
                DeleteRecord(gridControl1);
            }
            else
            {
                DeleteRecord(gridControl2);
            }
        }

        private void AddRecord(GridControl gridControl)
        {           
            int intWorkOrderIDToPass = 0;
            if (gridControl.Name == "gridControl1")
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = i_str_selected_workorders.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length > 1)
                {
                    frm_AT_Mapping_WorkOrder_Map_Step2 frm_child2 = new frm_AT_Mapping_WorkOrder_Map_Step2();
                    frm_child2.strPassedInWorkOrderIDs = i_str_selected_workorders;
                    frm_child2.GlobalSettings = this.GlobalSettings;
                    if (frm_child2.ShowDialog() != DialogResult.OK) return;
                    intWorkOrderIDToPass = frm_child2.intSelectedWorkOrderID;
                }
                else if (strArray.Length == 1)
                {
                    intWorkOrderIDToPass = Convert.ToInt32(strArray[0]);
                }
                else  // No work Orders selected so abort //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add, no Work Order selected.\n\nSelect one or more Work Order from the Work Order drop down list first then try again.", "Add Work Order Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            WaitDialogForm loading = new WaitDialogForm("Generating Map Preview...", "WoodPlan Mapping");
            loading.Show();

            //Image image = frmParentMappingForm.Generate_Work_Order_Map();
            Image image = null;
            frmParentMappingForm.Generate_Work_Order_Map(ref image);

            frm_AT_Mapping_WorkOrder_Map frm_child = new frm_AT_Mapping_WorkOrder_Map();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child.strFormMode = "Add";
            frm_child.imageMap = image;
            frm_child.frmParentMappingForm = this.frmParentMappingForm;
            frm_child.intCurrentMapScale = this.intCurrentMapScale;
            frm_child.intDPI = this.intDPI;
            frm_child.intWorkOrderID = intWorkOrderIDToPass;
            loading.Close();

            if (frm_child.ShowDialog() == DialogResult.OK)
            {
                if (gridControl.Name == "gridControl1")
                {
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    Load_Maps_Grid();
                }
                else
                {
                    this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    Load_Unlinked_Maps_Grid();
                }
            }
            image = null;
            GC.GetTotalMemory(true);
        }

        private void DeleteRecord(GridControl gridControl)
        {
            GridView view = (GridView)gridControl.MainView;
            int[] intRowHandles;
            int intCount = 0;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            int intUpdateProgressThreshhold = intCount / 10;
            int intUpdateProgressTempCount = 0;

            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Work Order Maps to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            string strMessage = (intCount == 1 ? "You have 1 Work Order Map selected for delete!\n\nProceed?" : "You have " + Convert.ToString(intRowHandles.Length) + " Work Order Maps selected for delete!\n\nProceed?");
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {               
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();


                int intRecordID = 0;
                string strRecordIDs = "";
                string strImagePath = "";
                string strImageThumbPath = "";
                gridControl.BeginUpdate();
                int intRowHandle = 0;

                //foreach (int intRowHandle in intRowHandles)
                for (int i = intRowHandles.Length - 1; i >= 0; i--)  // Process backwards //
                {
                    intRowHandle = intRowHandles[i];
                    intRecordID += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "WorkOrderMapID"));
                    strImageThumbPath = strMapsPath + view.GetRowCellValue(intRowHandle, "WorkOrderMapID").ToString() + "_thumb" + ".jpg";
                    strImagePath = strMapsPath + view.GetRowCellValue(intRowHandle, "WorkOrderMapID").ToString() + ".gif";

                    try
                    {

                        Images.Remove(strImageThumbPath);  // Remove Thumbnail from Hashtable used by gridView1_CustomUnboundColumnData and gridView2_CustomUnboundColumnData //
                        
                        base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                        GC.GetTotalMemory(true);

                        System.IO.File.Delete(strImageThumbPath);
                        System.IO.File.Delete(strImagePath);
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete the Work Order Map and\\or Thumbnail Image [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        continue;
                    }

                    strRecordIDs += view.GetRowCellValue(intRowHandle, "WorkOrderMapID").ToString() + ",";

                    view.DeleteRow(intRowHandle);

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                // Remove records from DB in one hit //
                try
                {
                    DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter();
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    RemoveRecords.sp01299_AT_WorkOrder_Map_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete the Work Order Map record(s) [" + ex.Message + "].\n\nTry deleting again. If the problem persists contact technical support.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                gridControl.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bbiPreviewMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
            GridView view = null;
            string strImagePath = "";
            if (gridControl1.Focused)
            {
                view = (GridView)gridControl1.MainView;
            }
            else
            {
                view = (GridView)gridControl2.MainView;
            }
            strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderMapID").ToString() + ".gif";

            frm_preview.strImage = strImagePath;
            frm_preview.ShowDialog();
        }

        private void bbiAllocate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int intMapID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderMapID"));
            
            frm_AT_Mapping_WorkOrder_Map_Allocate frmAllocate = new frm_AT_Mapping_WorkOrder_Map_Allocate();
            frmAllocate.GlobalSettings = this.GlobalSettings;
            frmAllocate.strMapDescription =  view.GetRowCellValue(view.FocusedRowHandle, "MapDescription").ToString();;
            frmAllocate.strMapRemarks =  view.GetRowCellValue(view.FocusedRowHandle, "Remarks").ToString();;
            if (frmAllocate.ShowDialog() == DialogResult.OK)
            {
                int intWorkOrderID = frmAllocate.intSelectedWorkOrderID;
                string strMapDescription = frmAllocate.strMapDescription;
                string strMapRemarks = frmAllocate.strMapRemarks;
                // Allocate the Map to the WorkOrder //
                try
                {
                    DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter AllocateMap = new DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter();
                    AllocateMap.ChangeConnectionString(strConnectionString);
                    AllocateMap.sp01305_AT_Tree_Picker_Allocate_Map_To_WorkOrder(intMapID, intWorkOrderID, strMapDescription, strMapRemarks);
                    
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    Load_Maps_Grid();
                    
                    this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    Load_Unlinked_Maps_Grid();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to allocate the Work Order Map [" + ex.Message + "].\n\nTry allocating again. If the problem persists contact technical support.", "Allocate Work Order Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void bbiUnallocate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int intMapID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderMapID"));

            frm_AT_Mapping_WorkOrder_Map_Unallocate frmDeAllocate = new frm_AT_Mapping_WorkOrder_Map_Unallocate();
            frmDeAllocate.GlobalSettings = this.GlobalSettings;
            frmDeAllocate.strMapDescription = view.GetRowCellValue(view.FocusedRowHandle, "MapDescription").ToString(); ;
            frmDeAllocate.strMapRemarks = view.GetRowCellValue(view.FocusedRowHandle, "Remarks").ToString(); ;
            frmDeAllocate.intWorkOrderID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderID"));
            if (frmDeAllocate.ShowDialog() == DialogResult.OK)
            {               
                string strMapDescription = frmDeAllocate.strMapDescription;
                string strMapRemarks = frmDeAllocate.strMapRemarks;
                // De-allocate the Map from the WorkOrder //
                try
                {
                    DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter AllocateMap = new DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter();
                    AllocateMap.ChangeConnectionString(strConnectionString);
                    AllocateMap.sp01305_AT_Tree_Picker_Allocate_Map_To_WorkOrder(intMapID, 0, strMapDescription, strMapRemarks);
                    
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    Load_Maps_Grid();
                    
                    this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    Load_Unlinked_Maps_Grid();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to de-allocate the Work Order Map [" + ex.Message + "].\n\nTry de-allocating again. If the problem persists contact technical support.", "De-allocate Work Order Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }


        }

        private void bbiRotateAntiClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            if (gridControl1.Focused)
            {
                view = (GridView)gridControl1.MainView;
            }
            else
            {
                view = (GridView)gridControl2.MainView;
            }
            try
            {
                base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                string strImagePath;
                strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderMapID").ToString() + "_thumb.jpg";
                Images.Remove(strImagePath.ToLower());  // Remove old thumbnail image stored in hashtable for grid control column preview //
                GC.GetTotalMemory(true);

                strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderMapID").ToString() + ".gif";
                Image newImage = Image.FromFile(strImagePath);
                newImage = RotateImage(newImage, RotateFlipType.Rotate270FlipNone);
                newImage.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Gif);

                Image newImage2 = newImage.GetThumbnailImage(50, 50, null, new IntPtr());
                strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderMapID").ToString() + "_thumb.jpg";
                newImage2.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);

                Bitmap bmpTest = new Bitmap(33, 33);  // Add thumbnail image to hashtable for grid control column preview with new version //
                bmpTest = new Bitmap(newImage2, 33, 33);
                Images.Add(strImagePath.ToLower(), bmpTest);
                view.RefreshRow(view.FocusedRowHandle);  // Force Unbound image preview to be reloaded //
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred when attempting to rotate the image [" + ex.Message + "]!\n\nThe image may be open elsewhere within the program. Try closing any open versions of the image before trying again.", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        private void bbiRotateClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            if (gridControl1.Focused)
            {
                view = (GridView)gridControl1.MainView;
            }
            else
            {
                view = (GridView)gridControl2.MainView;
            }
            try
            {
            base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
            string strImagePath;
            strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderMapID").ToString() + "_thumb.jpg";
            Images.Remove(strImagePath.ToLower());  // Remove old thumbnail image stored in hashtable for grid control column preview //
            GC.GetTotalMemory(true);

            strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderMapID").ToString() + ".gif";
            Image newImage = Image.FromFile(strImagePath);
            newImage = RotateImage(newImage, RotateFlipType.Rotate90FlipNone);
            newImage.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Gif);

            Image newImage2 = newImage.GetThumbnailImage(50, 50, null, new IntPtr());
            strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderMapID").ToString() + "_thumb.jpg";
            newImage2.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);

            Bitmap bmpTest = new Bitmap(33, 33);  // Add thumbnail image to hashtable for grid control column preview with new version //
            bmpTest = new Bitmap(newImage2, 33, 33);
            Images.Add(strImagePath.ToLower(), bmpTest);
            view.RefreshRow(view.FocusedRowHandle);  // Force Unbound image preview to be reloaded //
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred when attempting to rotate the image [" + ex.Message + "]!\n\nThe image may be open elsewhere within the program. Try closing any open versions of the image before trying again.", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public static Image RotateImage(Image img, RotateFlipType rotate_type)
        {
            img.RotateFlip(rotate_type);
            return img;
        }

    }
}


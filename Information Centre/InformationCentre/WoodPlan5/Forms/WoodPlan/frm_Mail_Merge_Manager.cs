using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using DevExpress.LookAndFeel;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using System.IO;  // Used by File.Delete //
using System.Data.SqlClient;  // Used by Generate Map process //
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Mail_Merge_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        //bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        //private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        public rpt_AT_Report_Layout_Listing_Blank rptReport;
        public string i_str_SavedDirectoryName = "";
        public string i_str_SelectedFilename = "";
        WaitDialogForm loadingForm;
        
        #endregion

        public frm_Mail_Merge_Manager()
        {
            InitializeComponent();
        }

        private void frm_Mail_Merge_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 2016;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            sp00233_Mail_Merge_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "ReportLayoutID");


            // Get default layout file directory from System_Settings table //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_SavedDirectoryName = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesReportLayoutLocation")) + "\\";
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Location of the Mail Merge File Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Mail Merge Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            /*try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }*/

            Load_Data();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            Application.DoEvents();  // Allow Form time to repaint itself //
            SetMenuStatus();
        }

        private void frm_Mail_Merge_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
            }
            SetMenuStatus();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            gridControl1.BeginUpdate();
            sp00233_Mail_Merge_ManagerTableAdapter.Fill(dataSet_Utilities.sp00233_Mail_Merge_Manager, 2, "8,");
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ReportLayoutID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }


        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            bbiRunMailMerge.Enabled = false;

            GridView view = null;
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Mail Merge Layouts //
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    //if (intRowHandles.Length >= 2)
                    //{
                    //    alItems.Add("iBlockEdit");
                    //    bbiBlockEdit.Enabled = true;
                    //}
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    //if (intRowHandles.Length >= 2)
                    //{
                    //    alItems.Add("iBlockEdit");
                    //    bbiBlockEdit.Enabled = true;
                    //}
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            //GridView ParentView = null;
            //int[] intRowHandles;
            //frmProgress fProgress = null;
            //System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Mail Merge Layout //
                    if (!iBool_AllowAdd) return;

                    Form frmMain = this.MdiParent;
                    frm_Mail_Merge_Add_Select_Type fChildForm = new frm_Mail_Merge_Add_Select_Type();
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intModuleID = 1;
                    fChildForm.intMailMergeTypeID = 8;
                    if (fChildForm.ShowDialog() != DialogResult.OK)  // User aborted //
                    {
                        return;
                    }
                    int intMailMergeTypeID = fChildForm.intMailMergeTypeID;

                    AddLayout(intMailMergeTypeID);

                    break;
                default:
                    break;
            }
        }

        private void AddLayout(int intReportType)
        {
            Form frmMain = this.MdiParent;
            frm_AT_Report_Add_Layout fChildForm = new frm_AT_Report_Add_Layout();
            frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intReportType = intReportType;
            fChildForm.boolPublishedToWebEnabled = false;
            int intNewLayoutID = 0;
            string strNewLayoutName = "";
            if (fChildForm.ShowDialog() != DialogResult.Yes) return;
            intNewLayoutID = fChildForm.intReturnedValue;
            strNewLayoutName = fChildForm.strReturnedLayoutName;

            // Save the new Layout and get back it's ID for the physical filename //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intNewID = 0;
            intNewID = Convert.ToInt32(GetSetting.sp01212_AT_Report_Add_Layouts_Create_Layout_Record(1, intReportType, strNewLayoutName, this.GlobalSettings.UserID, 0));
            if (intNewID <= 0 || intReportType <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create new Mail Merge Layout, a problem occurred - contact Technical Support.", "Create Mail Merge Layout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // Load the report - Create a design form and get its panel.
            XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
            //XRDesignFormEx form = new XRDesignFormEx();

            XRDesignPanel panel = form.DesignPanel;
            panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

            switch (intReportType)
            {
                case 8:  // Incidents Mail Merge //
                    rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, "", intReportType, true, true, 0);
                    //rptReport.Name = "Tree Listing";  // Set Document Root Node to meaningful text //
                    break;
                default:
                    return;
            }

            if (intNewLayoutID > 0)
            {
                i_str_SelectedFilename = Convert.ToInt32(intNewLayoutID) + ".repx";
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + i_str_SelectedFilename);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view layout, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            // Set filename to correct one so that when save fires from the report designer, the correct file is created/updated //
            i_str_SelectedFilename = Convert.ToInt32(intNewID) + ".repx";

            // Add a new command handler to the Report Designer which saves the report in a custom way.
            panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, i_str_SelectedFilename));

            // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
            panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

            loadingForm = new WaitDialogForm("Loading Report Builder...", "Mail Merge");
            loadingForm.Show();

            // Load the report into the design form and show the form.
            panel.OpenReport(rptReport);
            form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
            form.WindowState = FormWindowState.Maximized;
            loadingForm.Close();

            form.ShowDialog();
            panel.CloseReport();

            // Reload list of available layouts and pre-select new one //
            //sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, intReportType, "AmenityTreesReportLayoutLocation");
            //GridView view = (GridView)gridControl1.MainView;
            //Int32 intFoundRow = view.LocateByValue(0, view.Columns["ReportLayoutID"], intNewID);
            //if (intFoundRow > -1)
            //{
            //    view.FocusedRowHandle = intFoundRow;
            //}
            i_str_AddedRecordIDs1 = intNewLayoutID.ToString() + ";";
            Load_Data();
        }


        private void Edit_Record()
        {
            GridView view = null;
            //frmProgress fProgress = null;
            //System.Reflection.MethodInfo method = null;
            //string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Mail Merge Layout //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount != 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to edit before proceeding.", "Edit Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    loadingForm = new WaitDialogForm("Loading Report Builder...", "Mail Merge");
                    loadingForm.Show();

                    int intReportID = 0;
                    int intReportType = 0;
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intReportID += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ReportLayoutID"));
                        intReportType += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ReportLayoutTypeID"));
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                    // Load the report - Create a design form and get its panel.
                    XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                    //XRDesignFormEx form = new XRDesignFormEx();
                    XRDesignPanel panel = form.DesignPanel;
                    panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                    panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                    panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);
                    switch (intReportType)
                    {
                        case 8:  // Incidents Mail Merge //
                            rptReport = new rpt_AT_Report_Layout_Listing_Blank(this.GlobalSettings, "", intReportType, true, true, 0);
                            //rptReport.Name = "Tree Listing";  // Set Document Root Node to meaningful text //
                            break;
                        default:
                            return;
                    }
                    i_str_SelectedFilename = Convert.ToInt32(intReportID) + ".repx";
                    try
                    {
                        rptReport.LoadLayout(i_str_SavedDirectoryName + i_str_SelectedFilename);
                    }
                    catch (Exception Ex)
                    {
                        if (loadingForm != null) loadingForm.Close();
                        Console.WriteLine(Ex.Message);
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view layout, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    // Add a new command handler to the Report Designer which saves the report in a custom way.
                    panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, i_str_SelectedFilename));
                    // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                    panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);
                    // Load the report into the design form and show the form.
                    panel.OpenReport(rptReport);
                    form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
                    form.WindowState = FormWindowState.Maximized;
                    loadingForm.Close();
                    form.ShowDialog();
                    panel.CloseReport();              

                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Mail Merge Layout //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Mail Merge Layouts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Mail Merge Layout" : Convert.ToString(intRowHandles.Length) + " Mail Merge Layouts") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Mail Merge Layout" : "these Mail Merge Layouts") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        DataSet_ATTableAdapters.QueriesTableAdapter DeleteLayout = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        DeleteLayout.ChangeConnectionString(strConnectionString);
                        foreach (int intRowHandle in intRowHandles)
                        {                        
                            int? intReportLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
                            //string strReportLayoutName = Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
                            string strReportFileName = Convert.ToString(intReportLayoutID) + ".repx";

                            // Delete the record then the physical file layout //
                            DeleteLayout.sp01213_AT_Report_Layouts_Delete_Layout_Record(intReportLayoutID);

                            try
                            {
                                File.Delete(i_str_SavedDirectoryName + strReportFileName);
                            }
                            catch (Exception Ex)
                            {
                                Console.WriteLine(Ex.Message);
                                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout, it may no longer exist or you may not have the necessary permissions to delete it - contact Technical Support.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                        }
                        if (fProgress != null) fProgress.UpdateProgress(60); // Update Progress Bar //

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        Load_Data();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Mail Merge Layouts Available");
        }

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                //bbiShowMap.Enabled = (view.SelectedRowsCount == 1 ? true : false);
                bbiRunMailMerge.Enabled = (view.SelectedRowsCount == 1 ? true : false);
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        bool internalRowFocusing;
 
        #endregion


        private void bbiRunMailMerge_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a mail merge layout to mail merge to before proceeding.", "Run Mail Merge", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intMailMergeLayoutID = (view.GetFocusedRowCellValue("ReportLayoutID") == null ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID")));
            int intReportType = (view.GetFocusedRowCellValue("ReportLayoutTypeID") == null ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutTypeID")));

            if (intMailMergeLayoutID == 0 || intReportType == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected mail merge layout is of an unknown type - select a different layout before proceeding.", "Run Mail Merge", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strSelectedIDs = "";
            int intCompanyHeaderID = 0;
            Form frmMain = this.MdiParent;
            frm_Mail_Merge_Select_Data_For_Merge_Incidents fChildForm = new frm_Mail_Merge_Select_Data_For_Merge_Incidents();
            frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() != DialogResult.OK)  // User aborted //
            {
                return;
            }
            strSelectedIDs = fChildForm.strSelectedIDs ?? "";
            intCompanyHeaderID = fChildForm.intCompanyHeaderID;
            if (strSelectedIDs == "") return;

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            frm_Mail_Merge_Results fChildForm2 = new frm_Mail_Merge_Results();
            fChildForm2.MdiParent = this.MdiParent;
            fChildForm2.GlobalSettings = this.GlobalSettings;
            fChildForm2.FormPermissions = this.FormPermissions;
            fChildForm2.i_str_SavedDirectoryName = i_str_SavedDirectoryName;
            fChildForm2.i_str_SelectedFilename = intMailMergeLayoutID.ToString();
            fChildForm2.i_str_SelectedIDs = strSelectedIDs;
            fChildForm2.i_int_reportType = intReportType;
            fChildForm2.i_int_company_header = intCompanyHeaderID;
            fChildForm2.fProgress = fProgress;
            fChildForm2.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm2, new object[] { null });
        }

        void form_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }





        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Report...", "Mail Merge");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }

    }
}


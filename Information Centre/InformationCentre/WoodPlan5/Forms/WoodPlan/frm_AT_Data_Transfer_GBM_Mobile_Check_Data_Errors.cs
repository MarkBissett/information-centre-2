using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors : BaseObjects.frmBase
    {

        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intActualErrorCount = 0;
        public int intPossibleErrorCount = 0;
        public int intPictureErrorCount = 0;
        public string strMissingPictures = "";
        #endregion


        public frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors()
        {
            InitializeComponent();
        }

        private void frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (intActualErrorCount > 0)
            {
                labelControlError1.Text = "        " + (intActualErrorCount == 1 ? "1 critical error found." : intActualErrorCount.ToString() + " critical errors found.");
                labelControlError1.Appearance.ImageIndex = 1;
            }
            else
            {
                labelControlError1.Text = "        No critical errors found.";
                labelControlError1.Appearance.ImageIndex = 0;
            }
            if (intPossibleErrorCount > 0)
            {
                labelControlError2.Text = "        " + (intPossibleErrorCount == 1 ? "1 potential error [Invalid Coordinate(s)] found." : intActualErrorCount.ToString() + " potential errors [Invalid Coordinate(s)] found.");
                labelControlError2.Appearance.ImageIndex = 1;
            }
            else
            {
                labelControlError2.Text = "        No potential errors found.";
                labelControlError2.Appearance.ImageIndex = 0;
            }                
            if (intPictureErrorCount > 0)
            {
                memoEditMissingPhotos.Text = "The following photos are missing...\r\n" + strMissingPictures;
                labelControlError3.Appearance.ImageIndex = 1;
                btnClearMissingPhotoLinks.Enabled = true;
            }
            else
            {
                memoEditMissingPhotos.Text = "All photo links are valid.";
                labelControlError3.Appearance.ImageIndex = 0;
                btnClearMissingPhotoLinks.Enabled = false;
            }
            this.UnlockThisWindow();  
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnClearMissingPhotoLinks_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

  
  
    }
}


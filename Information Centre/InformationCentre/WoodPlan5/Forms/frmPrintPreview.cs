using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraPrinting;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraLayout;
using DevExpress.XtraDataLayout;
using DevExpress.XtraCharts;
using DevExpress.XtraScheduler;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraGauges.Win;
using DevExpress.XtraTreeList;
using DevExpress.XtraMap;
using DevExpress.DashboardWin;
using DevExpress.Utils;

namespace WoodPlan5
{
    public partial class frmPrintPreview : BaseObjects.frmBase
    {
        #region InstanceVariables
        private Settings set = Settings.Default;
        public WaitDialogForm loadingForm;
        private GaugeControl gaugeControl1;
        private MapControl mapControl1;
        private ExtendedMapControl extendedMapControl1;
        private DashboardDesigner dashboardControl1;
        public Image passedImage; 
        #endregion

        public frmPrintPreview()
        {
            InitializeComponent();
        }

        private void frmPrintPreview_Load(object sender, EventArgs e)
        {
            if (objObject != null)
            {
                frmBase fBase = (frmBase)objObject;
                PreviewActiveControl(fBase, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            }
        }

        private void PreviewActiveControl(frmBase fBase, GridControl gridControl, PivotGridControl pivotgridControl, VGridControl verticalgridControl, SchedulerControl schedulerControl, ChartControl chartControl, DockPanel dockpanelControl, LayoutControl layoutControl, DataLayoutControl dataLayoutControl, GaugeControl gaugeControl, TreeList treeControl, ExtendedDataLayoutControl extendedDataLayoutControl, MapControl mapControl, ExtendedMapControl extendedMapControl, DashboardDesigner dashboardControl)
        {
            // Note that this method calls itself recursively to drill down through the active control if the active control is a Dock Panel or Layout Control //  
            // Important Note: The Gauge control does not support IPrintable Interface, so it works differently to get the preview. A link [link1] is added under Links inside the PrintingSystem1 object on the form. //
            PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
            pcl.PaperKind = System.Drawing.Printing.PaperKind.A4;

            if (passedImage != null)
            {
                //this.link2.ShowPreview();
                this.link2.CreateDocument();
                return;
            }

            if (fBase != null)
            {
                switch (fBase.ActiveControl.GetType().Name)
                {
                    case "GridControl":
                        gridControl = (GridControl)fBase.ActiveControl;
                        pcl.Component = gridControl;
                        pcl.CreateDocument();
                        this.printControl1.PrintingSystem = pcl.PrintingSystem;
                        break;
                    case "PivotGridControl":
                        pivotgridControl = (PivotGridControl)fBase.ActiveControl;
                        pcl.Component = pivotgridControl;
                        pcl.CreateDocument();
                        this.printControl1.PrintingSystem = pcl.PrintingSystem;
                        break;
                    case "VGridControl":
                        verticalgridControl = (VGridControl)fBase.ActiveControl;
                        pcl.Component = verticalgridControl;
                        pcl.CreateDocument();
                        this.printControl1.PrintingSystem = pcl.PrintingSystem;
                        break;
                    case "ChartControl":
                        chartControl = (ChartControl)fBase.ActiveControl;
                        pcl.Component = chartControl;
                        pcl.CreateDocument();
                        this.printControl1.PrintingSystem = pcl.PrintingSystem;
                        break;
                    case "SchedulerControl":
                        schedulerControl = (SchedulerControl)fBase.ActiveControl;
                        pcl.Component = schedulerControl;
                        pcl.CreateDocument();
                        this.printControl1.PrintingSystem = pcl.PrintingSystem;
                        break;
                    case "GaugeControl":
                        gaugeControl = (GaugeControl)fBase.ActiveControl;
                        gaugeControl1 = gaugeControl;
                        this.link1.CreateDocument();
                        break;
                    case "TreeList":
                        treeControl = (TreeList)fBase.ActiveControl;
                        pcl.Component = treeControl;
                        pcl.CreateDocument();
                        this.printControl1.PrintingSystem = pcl.PrintingSystem;
                        break;
                    case "DockPanel":
                        dockpanelControl = (DockPanel)fBase.ActiveControl;
                        PreviewActiveControl(null, null, null, null, null, null, dockpanelControl, null, null, null, null, null, null, null, null);
                        break;
                    case "LayoutControl":
                        layoutControl = (LayoutControl)fBase.ActiveControl;
                        PreviewActiveControl(null, null, null, null, null, null, null, layoutControl, null, null, null, null, null, null, null);
                        break;
                    case "DataLayoutControl":
                        //dataLayoutControl = (DataLayoutControl)fBase.ActiveControl;
                        //PreviewActiveControl(null, null, null, null, null, null, null, null, dataLayoutControl, null, null, null, null, null, null);
                        dataLayoutControl = (DataLayoutControl)fBase.ActiveControl;
                        pcl.Component = dataLayoutControl;
                        pcl.CreateDocument();
                        this.printControl1.PrintingSystem = pcl.PrintingSystem;
                        break;
                    case "ExtendedDataLayoutControl":
                        extendedDataLayoutControl = (ExtendedDataLayoutControl)fBase.ActiveControl;
                        pcl.Component = extendedDataLayoutControl;
                        pcl.CreateDocument();
                        this.printControl1.PrintingSystem = pcl.PrintingSystem;
                        break;
                    case "MapControl":
                        mapControl = (MapControl)fBase.ActiveControl;
                        mapControl1 = mapControl;
                        //this.link3.ShowPreview();
                        this.link3.CreateDocument();
                        break;
                    case "ExtendedMapControl":
                        mapControl = (ExtendedMapControl)fBase.ActiveControl;
                        mapControl1 = mapControl;
                        //this.link3.ShowPreview();
                        this.link3.CreateDocument();
                        break;
                    case "DashboardDesigner":
                        dashboardControl = (DashboardDesigner)fBase.ActiveControl;
                        dashboardControl1 = dashboardControl;
                        this.link4.CreateDocument();
                        break;
                    default:
                        break;
                }
            }
            else 
            {
                if (gridControl != null)
                {
                    pcl.Component = gridControl;
                    pcl.CreateDocument();
                    this.printControl1.PrintingSystem = pcl.PrintingSystem;
                }
                else if (pivotgridControl != null)
                {
                    pcl.Component = pivotgridControl;
                    pcl.CreateDocument();
                    this.printControl1.PrintingSystem = pcl.PrintingSystem;
                }
                else if (verticalgridControl != null)
                {
                    pcl.Component = verticalgridControl;
                    pcl.CreateDocument();
                    this.printControl1.PrintingSystem = pcl.PrintingSystem;
                }
                else if (chartControl != null)
                {
                    pcl.Component = chartControl;
                    pcl.CreateDocument();
                    this.printControl1.PrintingSystem = pcl.PrintingSystem;
                }
                else if (schedulerControl != null)
                {
                    pcl.Component = schedulerControl;
                    pcl.CreateDocument();
                    this.printControl1.PrintingSystem = pcl.PrintingSystem;
                }
                else if (gaugeControl != null)
                {
                    gaugeControl = (GaugeControl)fBase.ActiveControl;
                    gaugeControl1 = gaugeControl;
                    this.printControl1.PrintingSystem = pcl.PrintingSystem;
                    this.link1.ShowPreview();
                }
                else if (treeControl != null)
                {
                    pcl.Component = treeControl;
                    pcl.CreateDocument();
                    this.printControl1.PrintingSystem = pcl.PrintingSystem;
                }
                else if (mapControl != null)
                {
                    mapControl = (MapControl)fBase.ActiveControl;
                    mapControl1 = mapControl;
                    this.printControl1.PrintingSystem = pcl.PrintingSystem;
                    this.link3.ShowPreview();
                }
                else if (extendedMapControl != null)
                {
                    extendedMapControl = (ExtendedMapControl)fBase.ActiveControl;
                    extendedMapControl1 = extendedMapControl;
                    this.printControl1.PrintingSystem = pcl.PrintingSystem;
                    this.link3.ShowPreview();
                }
                else if (dashboardControl != null)
                {
                    dashboardControl = (DashboardDesigner)fBase.ActiveControl;
                    dashboardControl1 = dashboardControl;
                    this.link4.CreateDocument();
                }
                else if (dockpanelControl != null)
                {
                    switch (dockpanelControl.ActiveControl.GetType().Name)
                    {
                        case "GridControl":
                            gridControl = (GridControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, gridControl, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "PivotGridControl":
                            pivotgridControl = (PivotGridControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, pivotgridControl, null, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "VGridControl":
                            verticalgridControl = (VGridControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, verticalgridControl, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "SchedulerControl":
                            schedulerControl = (SchedulerControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, schedulerControl, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "ChartControl":
                            chartControl = (ChartControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, chartControl, null, null, null, null, null, null, null, null, null);
                            break;
                        case "DockPanel":
                            dockpanelControl = (DockPanel)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, dockpanelControl, null, null, null, null, null, null, null, null);
                            break;
                        case "LayoutControl":
                            layoutControl = (LayoutControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, layoutControl, null, null, null, null, null, null, null);
                            break;
                        case "DataLayoutControl":
                            dataLayoutControl = (DataLayoutControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, dataLayoutControl, null, null, null, null, null, null);
                            break;
                        case "GaugeControl":
                            gaugeControl = (GaugeControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, gaugeControl, null, null, null, null, null);
                            break;
                        case "TreeList":
                            treeControl = (TreeList)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, treeControl, null, null, null, null);
                            break;
                        case "ExtendedDataLayoutControl":
                            extendedDataLayoutControl = (ExtendedDataLayoutControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, extendedDataLayoutControl, null, null, null);
                            break;
                        case "MapControl":
                            mapControl = (MapControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, mapControl, null, null);
                            break;
                        case "ExtendedMapControl":
                            extendedMapControl = (ExtendedMapControl)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, null, extendedMapControl, null);
                            break;
                        case "DashboardDesigner":
                            dashboardControl = (DashboardDesigner)dockpanelControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, null, null, dashboardControl);
                            break;
                        default:
                            break;
                    }
                }
                else if (layoutControl != null)
                {
                    switch (layoutControl.ActiveControl.GetType().Name)
                    {
                        case "GridControl":
                            gridControl = (GridControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, gridControl, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "PivotGridControl":
                            pivotgridControl = (PivotGridControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, pivotgridControl, null, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "VGridControl":
                            verticalgridControl = (VGridControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, verticalgridControl, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "SchedulerControl":
                            schedulerControl = (SchedulerControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, schedulerControl, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "ChartControl":
                            chartControl = (ChartControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, chartControl, null, null, null, null, null, null, null, null, null);
                            break;
                        case "DockPanel":
                            dockpanelControl = (DockPanel)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, dockpanelControl, null, null, null, null, null, null, null, null);
                            break;
                        case "LayoutControl":
                            layoutControl = (LayoutControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, layoutControl, null, null, null, null, null, null, null);
                            break;
                        case "DataLayoutControl":
                            dataLayoutControl = (DataLayoutControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, dataLayoutControl, null, null, null, null, null, null);
                            break;
                        case "GaugeControl":
                            gaugeControl = (GaugeControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, gaugeControl, null, null, null, null, null);
                            break;
                        case "TreeList":
                            treeControl = (TreeList)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, treeControl, null, null, null, null);
                            break;
                        case "ExtendedDataLayoutControl":
                            extendedDataLayoutControl = (ExtendedDataLayoutControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, extendedDataLayoutControl, null, null, null);
                            break;
                        case "MapControl":
                            mapControl = (MapControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, mapControl, null, null);
                            break;
                        case "ExtendedMapControl":
                            extendedMapControl = (ExtendedMapControl)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, null, extendedMapControl, null);
                            break;
                        case "DashboardDesigner":
                            dashboardControl = (DashboardDesigner)layoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, null, null, dashboardControl);
                            break;
                        default:
                            break;
                    }
                }
                else if (dataLayoutControl != null)
                {
                    switch (layoutControl.ActiveControl.GetType().Name)
                    {
                        case "GridControl":
                            gridControl = (GridControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, gridControl, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "PivotGridControl":
                            pivotgridControl = (PivotGridControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, pivotgridControl, null, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "VGridControl":
                            verticalgridControl = (VGridControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, verticalgridControl, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "SchedulerControl":
                            schedulerControl = (SchedulerControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, schedulerControl, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "ChartControl":
                            chartControl = (ChartControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, chartControl, null, null, null, null, null, null, null, null, null);
                            break;
                        case "DockPanel":
                            dockpanelControl = (DockPanel)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, dockpanelControl, null, null, null, null, null, null, null, null);
                            break;
                        case "LayoutControl":
                            layoutControl = (LayoutControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, layoutControl, null, null, null, null, null, null, null);
                            break;
                        case "DataLayoutControl":
                            dataLayoutControl = (DataLayoutControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, dataLayoutControl, null, null, null, null, null, null);
                            break;
                        case "GaugeControl":
                            gaugeControl = (GaugeControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, gaugeControl, null, null, null, null, null);
                            break;
                        case "TreeList":
                            treeControl = (TreeList)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, treeControl, null, null, null, null);
                            break;
                        case "ExtendedDataLayoutControl":
                            extendedDataLayoutControl = (ExtendedDataLayoutControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, extendedDataLayoutControl, null, null, null);
                            break;
                        case "MapControl":
                            mapControl = (MapControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, mapControl, null, null);
                            break;
                        case "ExtendedMapControl":
                            extendedMapControl = (ExtendedMapControl)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, null, extendedMapControl, null);
                            break;
                        case "DashboardDesigner":
                            dashboardControl = (DashboardDesigner)dataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, null, null, dashboardControl);
                            break;
                        default:
                            break;
                    }
                }
                else if (extendedDataLayoutControl != null)
                {
                    switch (layoutControl.ActiveControl.GetType().Name)
                    {
                        case "GridControl":
                            gridControl = (GridControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, gridControl, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "PivotGridControl":
                            pivotgridControl = (PivotGridControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, pivotgridControl, null, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "VGridControl":
                            verticalgridControl = (VGridControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, verticalgridControl, null, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "SchedulerControl":
                            schedulerControl = (SchedulerControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, schedulerControl, null, null, null, null, null, null, null, null, null, null);
                            break;
                        case "ChartControl":
                            chartControl = (ChartControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, chartControl, null, null, null, null, null, null, null, null, null);
                            break;
                        case "DockPanel":
                            dockpanelControl = (DockPanel)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, dockpanelControl, null, null, null, null, null, null, null, null);
                            break;
                        case "LayoutControl":
                            layoutControl = (LayoutControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, layoutControl, null, null, null, null, null, null, null);
                            break;
                        case "DataLayoutControl":
                            dataLayoutControl = (DataLayoutControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, dataLayoutControl, null, null, null, null, null, null);
                            break;
                        case "GaugeControl":
                            gaugeControl = (GaugeControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, gaugeControl, null, null, null, null, null);
                            break;
                        case "TreeList":
                            treeControl = (TreeList)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, treeControl, null, null, null, null);
                            break;
                        case "ExtendedDataLayoutControl":
                            extendedDataLayoutControl = (ExtendedDataLayoutControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, extendedDataLayoutControl, null, null, null);
                            break;
                        case "MapControl":
                            mapControl = (MapControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, mapControl, null, null);
                            break;
                        case "ExtendedMapControl":
                            extendedMapControl = (ExtendedMapControl)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, null, extendedMapControl, null);
                            break;
                        case "DashboardDesigner":
                            dashboardControl = (DashboardDesigner)extendedDataLayoutControl.ActiveControl;
                            PreviewActiveControl(null, null, null, null, null, null, null, null, null, null, null, null, null, null, dashboardControl);
                            break;
                        default:
                            break;
                    }
                }
            }

        }

        public override void PostOpen(object objParameter)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        private void link1_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            Bitmap gauge = new Bitmap(this.gaugeControl1.Width, this.gaugeControl1.Height);
            this.gaugeControl1.DrawToBitmap(gauge, new Rectangle(0, 0, gaugeControl1.Width, gaugeControl1.Height));
            e.Graph.DrawImage(gauge, new RectangleF(0, 0, gaugeControl1.Width, gaugeControl1.Height));
        }

        private void link2_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            // Draw passed image.  Note: Link 2 is a non-visible control on the form //
            e.Graph.DrawImage(passedImage, new RectangleF(0, 0, passedImage.Width, passedImage.Height));
        }

        private void link3_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            var stream = new System.IO.MemoryStream();
            this.mapControl1.ExportToImage(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
            Bitmap mapImage = new Bitmap(stream);
            e.Graph.DrawImage(mapImage, new RectangleF(0, 0, mapControl1.Width, mapControl1.Height));
        }

        private void link4_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            Bitmap dash = new Bitmap(this.dashboardControl1.Width, this.dashboardControl1.Height);
            this.dashboardControl1.DrawToBitmap(dash, new Rectangle(0, 0, dashboardControl1.Width, dashboardControl1.Height));
            e.Graph.DrawImage(dash, new RectangleF(0, 0, dashboardControl1.Width, dashboardControl1.Height));
        }

        private void frmPrintPreview_FormClosing(object sender, FormClosingEventArgs e)
        {
            passedImage = null;
            link1 = null;
            link2 = null;
            link3 = null;
            link4 = null;
            GC.GetTotalMemory(true);  // Clear any links to any created images //
        }






    }
}


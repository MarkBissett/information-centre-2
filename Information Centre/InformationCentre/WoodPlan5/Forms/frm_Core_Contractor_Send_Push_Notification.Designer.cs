namespace WoodPlan5
{
    partial class frm_Core_Contractor_Send_Push_Notification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Contractor_Send_Push_Notification));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01013CorePushNotificationsAllocatedPDAsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAllocatedPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaMake = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaFirmwareVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaMobileTelNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaDatePurchased = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSoftwareVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAllocated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTimeShort = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPDAUserName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDAPassword1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDALoginToken1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDACode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDAVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp01013_Core_Push_Notifications_Allocated_PDAsTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01013_Core_Push_Notifications_Allocated_PDAsTableAdapter();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlMessageTitle = new DevExpress.XtraEditors.LabelControl();
            this.textEditTitle = new DevExpress.XtraEditors.TextEdit();
            this.memoEditMessage = new DevExpress.XtraEditors.MemoEdit();
            this.btnSend = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.numericChartRangeControlClient1 = new DevExpress.XtraEditors.NumericChartRangeControlClient();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01013CorePushNotificationsAllocatedPDAsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTimeShort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditMessage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartRangeControlClient1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(973, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 540);
            this.barDockControlBottom.Size = new System.Drawing.Size(973, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 540);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(973, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 540);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp01013CorePushNotificationsAllocatedPDAsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(5, 25);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditDateTimeShort});
            this.gridControl1.Size = new System.Drawing.Size(939, 318);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01013CorePushNotificationsAllocatedPDAsBindingSource
            // 
            this.sp01013CorePushNotificationsAllocatedPDAsBindingSource.DataMember = "sp01013_Core_Push_Notifications_Allocated_PDAs";
            this.sp01013CorePushNotificationsAllocatedPDAsBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAllocatedPdaID,
            this.colSubContractorID1,
            this.colSubContractorName1,
            this.colPdaID,
            this.colPdaMake,
            this.colPdaModel,
            this.colPdaSerialNumber,
            this.colPdaFirmwareVersion,
            this.colPdaMobileTelNo,
            this.colPdaDatePurchased,
            this.colSoftwareVersion,
            this.colIMEI,
            this.colDateAllocated,
            this.colRemarks4,
            this.colPDAUserName1,
            this.colPDAPassword1,
            this.colPDALoginToken1,
            this.colPDACode,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID1,
            this.colPDAVersion});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIMEI, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colAllocatedPdaID
            // 
            this.colAllocatedPdaID.Caption = "Allocated Device ID";
            this.colAllocatedPdaID.FieldName = "AllocatedPdaID";
            this.colAllocatedPdaID.Name = "colAllocatedPdaID";
            this.colAllocatedPdaID.OptionsColumn.AllowEdit = false;
            this.colAllocatedPdaID.OptionsColumn.AllowFocus = false;
            this.colAllocatedPdaID.OptionsColumn.ReadOnly = true;
            this.colAllocatedPdaID.Width = 102;
            // 
            // colSubContractorID1
            // 
            this.colSubContractorID1.Caption = "Owner ID";
            this.colSubContractorID1.FieldName = "SubContractorID";
            this.colSubContractorID1.Name = "colSubContractorID1";
            this.colSubContractorID1.OptionsColumn.AllowEdit = false;
            this.colSubContractorID1.OptionsColumn.AllowFocus = false;
            this.colSubContractorID1.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName1
            // 
            this.colSubContractorName1.Caption = "Owner Name";
            this.colSubContractorName1.FieldName = "SubContractorName";
            this.colSubContractorName1.Name = "colSubContractorName1";
            this.colSubContractorName1.OptionsColumn.AllowEdit = false;
            this.colSubContractorName1.OptionsColumn.AllowFocus = false;
            this.colSubContractorName1.OptionsColumn.ReadOnly = true;
            this.colSubContractorName1.Width = 195;
            // 
            // colPdaID
            // 
            this.colPdaID.Caption = "Device ID";
            this.colPdaID.FieldName = "PdaID";
            this.colPdaID.Name = "colPdaID";
            this.colPdaID.OptionsColumn.AllowEdit = false;
            this.colPdaID.OptionsColumn.AllowFocus = false;
            this.colPdaID.OptionsColumn.ReadOnly = true;
            // 
            // colPdaMake
            // 
            this.colPdaMake.Caption = "Device Make";
            this.colPdaMake.FieldName = "PdaMake";
            this.colPdaMake.Name = "colPdaMake";
            this.colPdaMake.OptionsColumn.AllowEdit = false;
            this.colPdaMake.OptionsColumn.AllowFocus = false;
            this.colPdaMake.OptionsColumn.ReadOnly = true;
            this.colPdaMake.Visible = true;
            this.colPdaMake.VisibleIndex = 4;
            this.colPdaMake.Width = 93;
            // 
            // colPdaModel
            // 
            this.colPdaModel.Caption = "Device Model";
            this.colPdaModel.FieldName = "PdaModel";
            this.colPdaModel.Name = "colPdaModel";
            this.colPdaModel.OptionsColumn.AllowEdit = false;
            this.colPdaModel.OptionsColumn.AllowFocus = false;
            this.colPdaModel.OptionsColumn.ReadOnly = true;
            this.colPdaModel.Visible = true;
            this.colPdaModel.VisibleIndex = 5;
            this.colPdaModel.Width = 95;
            // 
            // colPdaSerialNumber
            // 
            this.colPdaSerialNumber.Caption = "Serial No";
            this.colPdaSerialNumber.FieldName = "PdaSerialNumber";
            this.colPdaSerialNumber.Name = "colPdaSerialNumber";
            this.colPdaSerialNumber.OptionsColumn.AllowEdit = false;
            this.colPdaSerialNumber.OptionsColumn.AllowFocus = false;
            this.colPdaSerialNumber.OptionsColumn.ReadOnly = true;
            this.colPdaSerialNumber.Visible = true;
            this.colPdaSerialNumber.VisibleIndex = 6;
            this.colPdaSerialNumber.Width = 111;
            // 
            // colPdaFirmwareVersion
            // 
            this.colPdaFirmwareVersion.Caption = "Firmware";
            this.colPdaFirmwareVersion.FieldName = "PdaFirmwareVersion";
            this.colPdaFirmwareVersion.Name = "colPdaFirmwareVersion";
            this.colPdaFirmwareVersion.OptionsColumn.AllowEdit = false;
            this.colPdaFirmwareVersion.OptionsColumn.AllowFocus = false;
            this.colPdaFirmwareVersion.OptionsColumn.ReadOnly = true;
            this.colPdaFirmwareVersion.Visible = true;
            this.colPdaFirmwareVersion.VisibleIndex = 7;
            this.colPdaFirmwareVersion.Width = 99;
            // 
            // colPdaMobileTelNo
            // 
            this.colPdaMobileTelNo.Caption = "Device Mobile Tel";
            this.colPdaMobileTelNo.FieldName = "PdaMobileTelNo";
            this.colPdaMobileTelNo.Name = "colPdaMobileTelNo";
            this.colPdaMobileTelNo.OptionsColumn.AllowEdit = false;
            this.colPdaMobileTelNo.OptionsColumn.AllowFocus = false;
            this.colPdaMobileTelNo.OptionsColumn.ReadOnly = true;
            this.colPdaMobileTelNo.Visible = true;
            this.colPdaMobileTelNo.VisibleIndex = 2;
            this.colPdaMobileTelNo.Width = 96;
            // 
            // colPdaDatePurchased
            // 
            this.colPdaDatePurchased.Caption = "Purchase Date";
            this.colPdaDatePurchased.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colPdaDatePurchased.FieldName = "PdaDatePurchased";
            this.colPdaDatePurchased.Name = "colPdaDatePurchased";
            this.colPdaDatePurchased.OptionsColumn.AllowEdit = false;
            this.colPdaDatePurchased.OptionsColumn.AllowFocus = false;
            this.colPdaDatePurchased.OptionsColumn.ReadOnly = true;
            this.colPdaDatePurchased.Visible = true;
            this.colPdaDatePurchased.VisibleIndex = 9;
            this.colPdaDatePurchased.Width = 114;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colSoftwareVersion
            // 
            this.colSoftwareVersion.Caption = "Device Software Version";
            this.colSoftwareVersion.FieldName = "SoftwareVersion";
            this.colSoftwareVersion.Name = "colSoftwareVersion";
            this.colSoftwareVersion.OptionsColumn.AllowEdit = false;
            this.colSoftwareVersion.OptionsColumn.AllowFocus = false;
            this.colSoftwareVersion.OptionsColumn.ReadOnly = true;
            this.colSoftwareVersion.Visible = true;
            this.colSoftwareVersion.VisibleIndex = 8;
            this.colSoftwareVersion.Width = 136;
            // 
            // colIMEI
            // 
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.AllowFocus = false;
            this.colIMEI.OptionsColumn.ReadOnly = true;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 3;
            this.colIMEI.Width = 98;
            // 
            // colDateAllocated
            // 
            this.colDateAllocated.Caption = "Date Allocated";
            this.colDateAllocated.ColumnEdit = this.repositoryItemTextEditDateTimeShort;
            this.colDateAllocated.FieldName = "DateAllocated";
            this.colDateAllocated.Name = "colDateAllocated";
            this.colDateAllocated.OptionsColumn.AllowEdit = false;
            this.colDateAllocated.OptionsColumn.AllowFocus = false;
            this.colDateAllocated.OptionsColumn.ReadOnly = true;
            this.colDateAllocated.Visible = true;
            this.colDateAllocated.VisibleIndex = 0;
            this.colDateAllocated.Width = 111;
            // 
            // repositoryItemTextEditDateTimeShort
            // 
            this.repositoryItemTextEditDateTimeShort.AutoHeight = false;
            this.repositoryItemTextEditDateTimeShort.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTimeShort.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTimeShort.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTimeShort.Name = "repositoryItemTextEditDateTimeShort";
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colPDAUserName1
            // 
            this.colPDAUserName1.Caption = "Device Username";
            this.colPDAUserName1.FieldName = "PDAUserName";
            this.colPDAUserName1.Name = "colPDAUserName1";
            this.colPDAUserName1.OptionsColumn.AllowEdit = false;
            this.colPDAUserName1.OptionsColumn.AllowFocus = false;
            this.colPDAUserName1.OptionsColumn.ReadOnly = true;
            this.colPDAUserName1.Visible = true;
            this.colPDAUserName1.VisibleIndex = 11;
            this.colPDAUserName1.Width = 102;
            // 
            // colPDAPassword1
            // 
            this.colPDAPassword1.Caption = "Device Password";
            this.colPDAPassword1.FieldName = "PDAPassword";
            this.colPDAPassword1.Name = "colPDAPassword1";
            this.colPDAPassword1.OptionsColumn.AllowEdit = false;
            this.colPDAPassword1.OptionsColumn.AllowFocus = false;
            this.colPDAPassword1.OptionsColumn.ReadOnly = true;
            this.colPDAPassword1.Visible = true;
            this.colPDAPassword1.VisibleIndex = 12;
            this.colPDAPassword1.Width = 100;
            // 
            // colPDALoginToken1
            // 
            this.colPDALoginToken1.Caption = "Device Token";
            this.colPDALoginToken1.FieldName = "PDALoginToken";
            this.colPDALoginToken1.Name = "colPDALoginToken1";
            this.colPDALoginToken1.OptionsColumn.AllowEdit = false;
            this.colPDALoginToken1.OptionsColumn.AllowFocus = false;
            this.colPDALoginToken1.OptionsColumn.ReadOnly = true;
            this.colPDALoginToken1.Visible = true;
            this.colPDALoginToken1.VisibleIndex = 13;
            this.colPDALoginToken1.Width = 231;
            // 
            // colPDACode
            // 
            this.colPDACode.Caption = "Device Code";
            this.colPDACode.FieldName = "PDACode";
            this.colPDACode.Name = "colPDACode";
            this.colPDACode.OptionsColumn.AllowEdit = false;
            this.colPDACode.OptionsColumn.AllowFocus = false;
            this.colPDACode.OptionsColumn.ReadOnly = true;
            this.colPDACode.Visible = true;
            this.colPDACode.VisibleIndex = 1;
            this.colPDACode.Width = 79;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Linked To Person Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Width = 127;
            // 
            // colLinkedToPersonTypeID1
            // 
            this.colLinkedToPersonTypeID1.Caption = "Linked To Person Type ID";
            this.colLinkedToPersonTypeID1.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID1.Name = "colLinkedToPersonTypeID1";
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID1.Width = 141;
            // 
            // colPDAVersion
            // 
            this.colPDAVersion.Caption = "Summer App Version";
            this.colPDAVersion.FieldName = "PDAVersion";
            this.colPDAVersion.Name = "colPDAVersion";
            this.colPDAVersion.OptionsColumn.AllowEdit = false;
            this.colPDAVersion.OptionsColumn.AllowFocus = false;
            this.colPDAVersion.OptionsColumn.ReadOnly = true;
            this.colPDAVersion.Visible = true;
            this.colPDAVersion.VisibleIndex = 14;
            this.colPDAVersion.Width = 117;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // sp01013_Core_Push_Notifications_Allocated_PDAsTableAdapter
            // 
            this.sp01013_Core_Push_Notifications_Allocated_PDAsTableAdapter.ClearBeforeFill = true;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 48);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(949, 347);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "<b>Step 1</b> - Select the handsets to send the Push Notification to.";
            // 
            // groupControl2
            // 
            this.groupControl2.AllowHtmlText = true;
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControlMessageTitle);
            this.groupControl2.Controls.Add(this.textEditTitle);
            this.groupControl2.Controls.Add(this.memoEditMessage);
            this.groupControl2.Location = new System.Drawing.Point(12, 407);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(857, 123);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "<b>Step 2</b> - Enter the <b>Title</b> and <b>Message</b> to send to the selected" +
    " handset(s).";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(4, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(46, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Message:";
            // 
            // labelControlMessageTitle
            // 
            this.labelControlMessageTitle.Location = new System.Drawing.Point(5, 28);
            this.labelControlMessageTitle.Name = "labelControlMessageTitle";
            this.labelControlMessageTitle.Size = new System.Drawing.Size(24, 13);
            this.labelControlMessageTitle.TabIndex = 2;
            this.labelControlMessageTitle.Text = "Title:";
            // 
            // textEditTitle
            // 
            this.textEditTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTitle.Location = new System.Drawing.Point(56, 25);
            this.textEditTitle.MenuManager = this.barManager1;
            this.textEditTitle.Name = "textEditTitle";
            this.textEditTitle.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTitle, true);
            this.textEditTitle.Size = new System.Drawing.Size(796, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTitle, optionsSpelling1);
            this.textEditTitle.TabIndex = 1;
            this.textEditTitle.ToolTip = "100 characters maxuimum.";
            // 
            // memoEditMessage
            // 
            this.memoEditMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditMessage.Location = new System.Drawing.Point(56, 51);
            this.memoEditMessage.MenuManager = this.barManager1;
            this.memoEditMessage.Name = "memoEditMessage";
            this.memoEditMessage.Properties.MaxLength = 256;
            this.scSpellChecker.SetShowSpellCheckMenu(this.memoEditMessage, true);
            this.memoEditMessage.Size = new System.Drawing.Size(796, 67);
            this.scSpellChecker.SetSpellCheckerOptions(this.memoEditMessage, optionsSpelling2);
            this.memoEditMessage.TabIndex = 0;
            this.memoEditMessage.ToolTip = "256 characters maxuimum.";
            // 
            // btnSend
            // 
            this.btnSend.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.ImageOptions.Image = global::WoodPlan5.Properties.Resources.apply_32x32;
            this.btnSend.Location = new System.Drawing.Point(881, 496);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(80, 34);
            this.btnSend.TabIndex = 6;
            this.btnSend.Text = "<b>Send</b>";
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.Info_32x32;
            this.pictureEdit1.Location = new System.Drawing.Point(6, 6);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(36, 36);
            this.pictureEdit1.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl1.Location = new System.Drawing.Point(46, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(910, 26);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = resources.GetString("labelControl1.Text");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_32x32;
            this.btnClose.Location = new System.Drawing.Point(881, 450);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 34);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frm_Core_Contractor_Send_Push_Notification
            // 
            this.ClientSize = new System.Drawing.Size(973, 540);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Contractor_Send_Push_Notification";
            this.Text = "Send Push Notifications";
            this.Activated += new System.EventHandler(this.frm_Core_Contractor_Send_Push_Notification_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Contractor_Send_Push_Notification_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Contractor_Send_Push_Notification_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            this.Controls.SetChildIndex(this.btnSend, 0);
            this.Controls.SetChildIndex(this.pictureEdit1, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01013CorePushNotificationsAllocatedPDAsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTimeShort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditMessage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartRangeControlClient1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private System.Windows.Forms.BindingSource sp01013CorePushNotificationsAllocatedPDAsBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocatedPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMake;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaModel;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaFirmwareVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMobileTelNo;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaDatePurchased;
        private DevExpress.XtraGrid.Columns.GridColumn colSoftwareVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAllocated;
        private DevExpress.XtraGrid.Columns.GridColumn colPDACode;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAVersion;
        private DataSet_Common_FunctionalityTableAdapters.sp01013_Core_Push_Notifications_Allocated_PDAsTableAdapter sp01013_Core_Push_Notifications_Allocated_PDAsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTimeShort;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAUserName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAPassword1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDALoginToken1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnSend;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.NumericChartRangeControlClient numericChartRangeControlClient1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit memoEditMessage;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControlMessageTitle;
        private DevExpress.XtraEditors.TextEdit textEditTitle;
        private DevExpress.XtraEditors.SimpleButton btnClose;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_GC_Import_Weather_Forecasts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Import_Weather_Forecasts));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControlInformation1 = new DevExpress.XtraEditors.LabelControl();
            this.btnImportForecasts = new DevExpress.XtraEditors.SimpleButton();
            this.hyperLinkEditErrorCount1 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.btnSendConfirmationEmails = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04053GCGrittingImportForecastGetReferenceDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteGrittingContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingAuthorisationRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colForecastingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingActivationCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingActivationCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingActivationCodeValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumTemperature = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colClientProactivePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colClientReactivePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientChargedForSalt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSaltPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnMonday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnTuesday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnWednesday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnThursday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnFriday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnSaturday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnSunday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreferredSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreferredTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultGritAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForecastActivationPoint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowForcasted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForecastMinimumTemperature = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiredGritAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHubID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSkippedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDailyGrittingEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForecastForEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOIDDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoubleGrit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp04054GCGrittingImportForecastDummyDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForecastActivationPoint1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowForcasted1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForecastMinimumTemperature1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiredGritAmount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHubID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSkippedReason1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDailyGrittingEmail1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForecastForEmail1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOIDDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoubleGrit1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnMoveRight = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeft = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlInformation3 = new DevExpress.XtraEditors.LabelControl();
            this.hyperLinkEditErrorCount3 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.btnSendFloatingClientEmails = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp04348GCGrittingImportFloatingForecastDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoubleGrit2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04349GCGrittingImportFloatingForecastDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn114 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn115 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoubleGrit3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnMoveRight2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeft2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadFloatingForecast = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp04351GCFloatingSiteJobsImportDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn116 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn117 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn118 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn119 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn120 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn121 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn122 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn123 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn124 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn125 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn126 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn127 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn128 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDegrees = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn129 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn130 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn132 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn133 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn134 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn135 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn136 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn137 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn138 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn139 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn140 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn141 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn142 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn143 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn144 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25KGBags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn145 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn146 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn147 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn148 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn149 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn150 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn151 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn152 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn153 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn154 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn155 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn156 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn157 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMainWorkCtr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoubleGrit4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlInformation4 = new DevExpress.XtraEditors.LabelControl();
            this.hyperLinkEditErrorCount4 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.btnCreateFloatingJobs = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.btnLoadFloatingJobs = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControlInformation2 = new DevExpress.XtraEditors.LabelControl();
            this.hyperLinkEditErrorCount2 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.btnLoadAuthorisation = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditDeleteUnauthorised = new DevExpress.XtraEditors.CheckEdit();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04068GCSpreadsheetAuthorisationDummyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGrit_Required = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGC_Site_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClient_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStore_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClient_Site_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArea1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeather = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnImportAuthorisation = new DevExpress.XtraEditors.SimpleButton();
            this.sp04053_GC_Gritting_Import_Forecast_Get_Reference_DataTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04053_GC_Gritting_Import_Forecast_Get_Reference_DataTableAdapter();
            this.sp04054_GC_Gritting_Import_Forecast_Dummy_DataTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04054_GC_Gritting_Import_Forecast_Dummy_DataTableAdapter();
            this.sp04068_GC_Spreadsheet_Authorisation_DummyTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04068_GC_Spreadsheet_Authorisation_DummyTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp04348_GC_Gritting_Import_Floating_Forecast_DataTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04348_GC_Gritting_Import_Floating_Forecast_DataTableAdapter();
            this.sp04349_GC_Gritting_Import_Floating_Forecast_DataTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04349_GC_Gritting_Import_Floating_Forecast_DataTableAdapter();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp04351_GC_Floating_Site_Jobs_Import_DataTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04351_GC_Floating_Site_Jobs_Import_DataTableAdapter();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEditErrorCount1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04053GCGrittingImportForecastGetReferenceDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04054GCGrittingImportForecastDummyDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEditErrorCount3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04348GCGrittingImportFloatingForecastDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04349GCGrittingImportFloatingForecastDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04351GCFloatingSiteJobsImportDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDegrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KGBags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEditErrorCount4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEditErrorCount2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDeleteUnauthorised.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04068GCSpreadsheetAuthorisationDummyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(884, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(884, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(884, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(884, 537);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage3,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(879, 511);
            this.xtraTabPage1.Text = "Import Weather Forecasts";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.pictureEdit1);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControlInformation1);
            this.splitContainerControl1.Panel1.Controls.Add(this.btnImportForecasts);
            this.splitContainerControl1.Panel1.Controls.Add(this.hyperLinkEditErrorCount1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.btnSendConfirmationEmails);
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(879, 511);
            this.splitContainerControl1.SplitterPosition = 29;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.pictureEdit1.Location = new System.Drawing.Point(104, 7);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(19, 19);
            this.pictureEdit1.TabIndex = 3;
            // 
            // labelControlInformation1
            // 
            this.labelControlInformation1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlInformation1.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlInformation1.Appearance.Options.UseImageAlign = true;
            this.labelControlInformation1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlInformation1.Location = new System.Drawing.Point(125, 8);
            this.labelControlInformation1.Name = "labelControlInformation1";
            this.labelControlInformation1.Size = new System.Drawing.Size(747, 15);
            this.labelControlInformation1.TabIndex = 1;
            this.labelControlInformation1.Text = "No information present at this time.";
            // 
            // btnImportForecasts
            // 
            this.btnImportForecasts.Location = new System.Drawing.Point(3, 3);
            this.btnImportForecasts.Name = "btnImportForecasts";
            this.btnImportForecasts.Size = new System.Drawing.Size(93, 23);
            this.btnImportForecasts.TabIndex = 0;
            this.btnImportForecasts.Text = "Load Forecasts";
            this.btnImportForecasts.Click += new System.EventHandler(this.btnImportForecasts_Click);
            // 
            // hyperLinkEditErrorCount1
            // 
            this.hyperLinkEditErrorCount1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hyperLinkEditErrorCount1.EditValue = "Error Counts";
            this.hyperLinkEditErrorCount1.Location = new System.Drawing.Point(124, 6);
            this.hyperLinkEditErrorCount1.MenuManager = this.barManager1;
            this.hyperLinkEditErrorCount1.Name = "hyperLinkEditErrorCount1";
            this.hyperLinkEditErrorCount1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.hyperLinkEditErrorCount1.Properties.Appearance.Options.UseBackColor = true;
            this.hyperLinkEditErrorCount1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.hyperLinkEditErrorCount1.Size = new System.Drawing.Size(747, 18);
            this.hyperLinkEditErrorCount1.TabIndex = 2;
            this.hyperLinkEditErrorCount1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkEditErrorCount1_OpenLink);
            // 
            // btnSendConfirmationEmails
            // 
            this.btnSendConfirmationEmails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSendConfirmationEmails.Location = new System.Drawing.Point(3, 450);
            this.btnSendConfirmationEmails.Name = "btnSendConfirmationEmails";
            this.btnSendConfirmationEmails.Size = new System.Drawing.Size(229, 23);
            this.btnSendConfirmationEmails.TabIndex = 1;
            this.btnSendConfirmationEmails.Text = "Send Confirmation Emails and Create Callouts";
            this.btnSendConfirmationEmails.Click += new System.EventHandler(this.btnSendConfirmationEmails_Click);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Location = new System.Drawing.Point(3, 2);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Sites Requiring Gritting";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnMoveRight);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnMoveLeft);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Sites NOT Requiring Gritting";
            this.splitContainerControl2.Size = new System.Drawing.Size(873, 445);
            this.splitContainerControl2.SplitterPosition = 434;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp04053GCGrittingImportForecastGetReferenceDataBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit7});
            this.gridControl1.Size = new System.Drawing.Size(430, 421);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04053GCGrittingImportForecastGetReferenceDataBindingSource
            // 
            this.sp04053GCGrittingImportForecastGetReferenceDataBindingSource.DataMember = "sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data";
            this.sp04053GCGrittingImportForecastGetReferenceDataBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "info_16.png");
            this.imageCollection1.Images.SetKeyName(7, "attention_16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteGrittingContractID,
            this.colSiteID,
            this.colSiteName,
            this.colSiteCode,
            this.colClientID,
            this.colClientName,
            this.colGrittingAuthorisationRequired,
            this.colArea,
            this.colForecastingTypeID,
            this.colGrittingActivationCodeID,
            this.colGrittingActivationCode,
            this.colGrittingActivationCodeValue,
            this.colMinimumTemperature,
            this.colClientProactivePrice,
            this.colClientReactivePrice,
            this.colClientChargedForSalt,
            this.colClientSaltPrice,
            this.colGritOnMonday,
            this.colGritOnTuesday,
            this.colGritOnWednesday,
            this.colGritOnThursday,
            this.colGritOnFriday,
            this.colGritOnSaturday,
            this.colGritOnSunday,
            this.colPreferredSubContractorID,
            this.colSubContractorID,
            this.colPreferredTeam,
            this.colHolidayCount,
            this.colDefaultGritAmount,
            this.colLocationX,
            this.colLocationY,
            this.colForecastActivationPoint,
            this.colSnowForcasted,
            this.colForecastMinimumTemperature,
            this.colRequiredGritAmount,
            this.colHubID,
            this.colSkippedReason,
            this.colClientsSiteCode,
            this.colDailyGrittingEmail,
            this.colForecastForEmail,
            this.colClientPOID,
            this.colClientPOIDDescription,
            this.colDoubleGrit});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSiteGrittingContractID
            // 
            this.colSiteGrittingContractID.Caption = "Gritting Contract ID";
            this.colSiteGrittingContractID.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID.Name = "colSiteGrittingContractID";
            this.colSiteGrittingContractID.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID.Width = 115;
            // 
            // colSiteID
            // 
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 153;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Width = 79;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 180;
            // 
            // colGrittingAuthorisationRequired
            // 
            this.colGrittingAuthorisationRequired.Caption = "Gritting Report";
            this.colGrittingAuthorisationRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGrittingAuthorisationRequired.FieldName = "GrittingAuthorisationRequired";
            this.colGrittingAuthorisationRequired.Name = "colGrittingAuthorisationRequired";
            this.colGrittingAuthorisationRequired.OptionsColumn.AllowEdit = false;
            this.colGrittingAuthorisationRequired.OptionsColumn.AllowFocus = false;
            this.colGrittingAuthorisationRequired.OptionsColumn.ReadOnly = true;
            this.colGrittingAuthorisationRequired.Visible = true;
            this.colGrittingAuthorisationRequired.VisibleIndex = 4;
            this.colGrittingAuthorisationRequired.Width = 93;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area";
            this.colArea.ColumnEdit = this.repositoryItemTextEdit3;
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            this.colArea.Visible = true;
            this.colArea.VisibleIndex = 12;
            this.colArea.Width = 60;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "#######0.00  M²";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colForecastingTypeID
            // 
            this.colForecastingTypeID.Caption = "Forecasting Type ID";
            this.colForecastingTypeID.FieldName = "ForecastingTypeID";
            this.colForecastingTypeID.Name = "colForecastingTypeID";
            this.colForecastingTypeID.OptionsColumn.AllowEdit = false;
            this.colForecastingTypeID.OptionsColumn.AllowFocus = false;
            this.colForecastingTypeID.OptionsColumn.ReadOnly = true;
            this.colForecastingTypeID.Width = 118;
            // 
            // colGrittingActivationCodeID
            // 
            this.colGrittingActivationCodeID.Caption = "Activation Code ID";
            this.colGrittingActivationCodeID.FieldName = "GrittingActivationCodeID";
            this.colGrittingActivationCodeID.Name = "colGrittingActivationCodeID";
            this.colGrittingActivationCodeID.OptionsColumn.AllowEdit = false;
            this.colGrittingActivationCodeID.OptionsColumn.AllowFocus = false;
            this.colGrittingActivationCodeID.OptionsColumn.ReadOnly = true;
            this.colGrittingActivationCodeID.Width = 111;
            // 
            // colGrittingActivationCode
            // 
            this.colGrittingActivationCode.Caption = "Activation Code";
            this.colGrittingActivationCode.FieldName = "GrittingActivationCode";
            this.colGrittingActivationCode.Name = "colGrittingActivationCode";
            this.colGrittingActivationCode.OptionsColumn.AllowEdit = false;
            this.colGrittingActivationCode.OptionsColumn.AllowFocus = false;
            this.colGrittingActivationCode.OptionsColumn.ReadOnly = true;
            this.colGrittingActivationCode.Visible = true;
            this.colGrittingActivationCode.VisibleIndex = 9;
            this.colGrittingActivationCode.Width = 97;
            // 
            // colGrittingActivationCodeValue
            // 
            this.colGrittingActivationCodeValue.Caption = "Activation Code Value";
            this.colGrittingActivationCodeValue.FieldName = "GrittingActivationCodeValue";
            this.colGrittingActivationCodeValue.Name = "colGrittingActivationCodeValue";
            this.colGrittingActivationCodeValue.OptionsColumn.AllowEdit = false;
            this.colGrittingActivationCodeValue.OptionsColumn.AllowFocus = false;
            this.colGrittingActivationCodeValue.OptionsColumn.ReadOnly = true;
            this.colGrittingActivationCodeValue.Width = 126;
            // 
            // colMinimumTemperature
            // 
            this.colMinimumTemperature.Caption = "Min Temperature";
            this.colMinimumTemperature.ColumnEdit = this.repositoryItemTextEdit2;
            this.colMinimumTemperature.FieldName = "MinimumTemperature";
            this.colMinimumTemperature.Name = "colMinimumTemperature";
            this.colMinimumTemperature.OptionsColumn.AllowEdit = false;
            this.colMinimumTemperature.OptionsColumn.AllowFocus = false;
            this.colMinimumTemperature.OptionsColumn.ReadOnly = true;
            this.colMinimumTemperature.Visible = true;
            this.colMinimumTemperature.VisibleIndex = 10;
            this.colMinimumTemperature.Width = 102;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "#0.0 degrees";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colClientProactivePrice
            // 
            this.colClientProactivePrice.Caption = "Proactive Price";
            this.colClientProactivePrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colClientProactivePrice.FieldName = "ClientProactivePrice";
            this.colClientProactivePrice.Name = "colClientProactivePrice";
            this.colClientProactivePrice.OptionsColumn.AllowEdit = false;
            this.colClientProactivePrice.OptionsColumn.AllowFocus = false;
            this.colClientProactivePrice.OptionsColumn.ReadOnly = true;
            this.colClientProactivePrice.Width = 92;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colClientReactivePrice
            // 
            this.colClientReactivePrice.Caption = "Reactive Price";
            this.colClientReactivePrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colClientReactivePrice.FieldName = "ClientReactivePrice";
            this.colClientReactivePrice.Name = "colClientReactivePrice";
            this.colClientReactivePrice.OptionsColumn.AllowEdit = false;
            this.colClientReactivePrice.OptionsColumn.AllowFocus = false;
            this.colClientReactivePrice.OptionsColumn.ReadOnly = true;
            this.colClientReactivePrice.Width = 89;
            // 
            // colClientChargedForSalt
            // 
            this.colClientChargedForSalt.Caption = "Charge for Salt";
            this.colClientChargedForSalt.FieldName = "ClientChargedForSalt";
            this.colClientChargedForSalt.Name = "colClientChargedForSalt";
            this.colClientChargedForSalt.OptionsColumn.AllowEdit = false;
            this.colClientChargedForSalt.OptionsColumn.AllowFocus = false;
            this.colClientChargedForSalt.OptionsColumn.ReadOnly = true;
            this.colClientChargedForSalt.Width = 94;
            // 
            // colClientSaltPrice
            // 
            this.colClientSaltPrice.Caption = "Salt Price";
            this.colClientSaltPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colClientSaltPrice.FieldName = "ClientSaltPrice";
            this.colClientSaltPrice.Name = "colClientSaltPrice";
            this.colClientSaltPrice.OptionsColumn.AllowEdit = false;
            this.colClientSaltPrice.OptionsColumn.AllowFocus = false;
            this.colClientSaltPrice.OptionsColumn.ReadOnly = true;
            // 
            // colGritOnMonday
            // 
            this.colGritOnMonday.Caption = "Monday Grit";
            this.colGritOnMonday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnMonday.FieldName = "GritOnMonday";
            this.colGritOnMonday.Name = "colGritOnMonday";
            this.colGritOnMonday.OptionsColumn.AllowEdit = false;
            this.colGritOnMonday.OptionsColumn.AllowFocus = false;
            this.colGritOnMonday.OptionsColumn.ReadOnly = true;
            this.colGritOnMonday.Width = 79;
            // 
            // colGritOnTuesday
            // 
            this.colGritOnTuesday.Caption = "Tuesday Grit";
            this.colGritOnTuesday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnTuesday.FieldName = "GritOnTuesday";
            this.colGritOnTuesday.Name = "colGritOnTuesday";
            this.colGritOnTuesday.OptionsColumn.AllowEdit = false;
            this.colGritOnTuesday.OptionsColumn.AllowFocus = false;
            this.colGritOnTuesday.OptionsColumn.ReadOnly = true;
            this.colGritOnTuesday.Width = 82;
            // 
            // colGritOnWednesday
            // 
            this.colGritOnWednesday.Caption = "Wednesday Grit";
            this.colGritOnWednesday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnWednesday.FieldName = "GritOnWednesday";
            this.colGritOnWednesday.Name = "colGritOnWednesday";
            this.colGritOnWednesday.OptionsColumn.AllowEdit = false;
            this.colGritOnWednesday.OptionsColumn.AllowFocus = false;
            this.colGritOnWednesday.OptionsColumn.ReadOnly = true;
            this.colGritOnWednesday.Width = 98;
            // 
            // colGritOnThursday
            // 
            this.colGritOnThursday.Caption = "Thursday Grit";
            this.colGritOnThursday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnThursday.FieldName = "GritOnThursday";
            this.colGritOnThursday.Name = "colGritOnThursday";
            this.colGritOnThursday.OptionsColumn.AllowEdit = false;
            this.colGritOnThursday.OptionsColumn.AllowFocus = false;
            this.colGritOnThursday.OptionsColumn.ReadOnly = true;
            this.colGritOnThursday.Width = 79;
            // 
            // colGritOnFriday
            // 
            this.colGritOnFriday.Caption = "Friday Grit";
            this.colGritOnFriday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnFriday.FieldName = "GritOnFriday";
            this.colGritOnFriday.Name = "colGritOnFriday";
            this.colGritOnFriday.OptionsColumn.AllowEdit = false;
            this.colGritOnFriday.OptionsColumn.AllowFocus = false;
            this.colGritOnFriday.OptionsColumn.ReadOnly = true;
            // 
            // colGritOnSaturday
            // 
            this.colGritOnSaturday.Caption = "Saturday Grit";
            this.colGritOnSaturday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnSaturday.FieldName = "GritOnSaturday";
            this.colGritOnSaturday.Name = "colGritOnSaturday";
            this.colGritOnSaturday.OptionsColumn.AllowEdit = false;
            this.colGritOnSaturday.OptionsColumn.AllowFocus = false;
            this.colGritOnSaturday.OptionsColumn.ReadOnly = true;
            this.colGritOnSaturday.Width = 82;
            // 
            // colGritOnSunday
            // 
            this.colGritOnSunday.Caption = "Sunday Grit";
            this.colGritOnSunday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnSunday.FieldName = "GritOnSunday";
            this.colGritOnSunday.Name = "colGritOnSunday";
            this.colGritOnSunday.OptionsColumn.AllowEdit = false;
            this.colGritOnSunday.OptionsColumn.AllowFocus = false;
            this.colGritOnSunday.OptionsColumn.ReadOnly = true;
            this.colGritOnSunday.Width = 77;
            // 
            // colPreferredSubContractorID
            // 
            this.colPreferredSubContractorID.Caption = "Preferred Team ID";
            this.colPreferredSubContractorID.FieldName = "PreferredSubContractorID";
            this.colPreferredSubContractorID.Name = "colPreferredSubContractorID";
            this.colPreferredSubContractorID.OptionsColumn.AllowEdit = false;
            this.colPreferredSubContractorID.OptionsColumn.AllowFocus = false;
            this.colPreferredSubContractorID.OptionsColumn.ReadOnly = true;
            this.colPreferredSubContractorID.Width = 110;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colPreferredTeam
            // 
            this.colPreferredTeam.Caption = "Preferred Team";
            this.colPreferredTeam.FieldName = "PreferredTeam";
            this.colPreferredTeam.Name = "colPreferredTeam";
            this.colPreferredTeam.OptionsColumn.AllowEdit = false;
            this.colPreferredTeam.OptionsColumn.AllowFocus = false;
            this.colPreferredTeam.OptionsColumn.ReadOnly = true;
            this.colPreferredTeam.Visible = true;
            this.colPreferredTeam.VisibleIndex = 2;
            this.colPreferredTeam.Width = 143;
            // 
            // colHolidayCount
            // 
            this.colHolidayCount.Caption = "On Holiday";
            this.colHolidayCount.FieldName = "HolidayCount";
            this.colHolidayCount.Name = "colHolidayCount";
            this.colHolidayCount.OptionsColumn.AllowEdit = false;
            this.colHolidayCount.OptionsColumn.AllowFocus = false;
            this.colHolidayCount.OptionsColumn.ReadOnly = true;
            // 
            // colDefaultGritAmount
            // 
            this.colDefaultGritAmount.Caption = "Default Grit Amount";
            this.colDefaultGritAmount.ColumnEdit = this.repositoryItemTextEdit7;
            this.colDefaultGritAmount.FieldName = "DefaultGritAmount";
            this.colDefaultGritAmount.Name = "colDefaultGritAmount";
            this.colDefaultGritAmount.OptionsColumn.AllowEdit = false;
            this.colDefaultGritAmount.OptionsColumn.AllowFocus = false;
            this.colDefaultGritAmount.OptionsColumn.ReadOnly = true;
            this.colDefaultGritAmount.Visible = true;
            this.colDefaultGritAmount.VisibleIndex = 11;
            this.colDefaultGritAmount.Width = 116;
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Mask.EditMask = "#######0.00  25 kg Bags";
            this.repositoryItemTextEdit7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Location X";
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Location Y";
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            // 
            // colForecastActivationPoint
            // 
            this.colForecastActivationPoint.Caption = "Forecast Activation Code";
            this.colForecastActivationPoint.FieldName = "ForecastActivationPoint";
            this.colForecastActivationPoint.Name = "colForecastActivationPoint";
            this.colForecastActivationPoint.OptionsColumn.AllowEdit = false;
            this.colForecastActivationPoint.OptionsColumn.AllowFocus = false;
            this.colForecastActivationPoint.OptionsColumn.ReadOnly = true;
            this.colForecastActivationPoint.Visible = true;
            this.colForecastActivationPoint.VisibleIndex = 5;
            this.colForecastActivationPoint.Width = 142;
            // 
            // colSnowForcasted
            // 
            this.colSnowForcasted.Caption = "Snow";
            this.colSnowForcasted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSnowForcasted.FieldName = "SnowForcasted";
            this.colSnowForcasted.Name = "colSnowForcasted";
            this.colSnowForcasted.OptionsColumn.AllowEdit = false;
            this.colSnowForcasted.OptionsColumn.AllowFocus = false;
            this.colSnowForcasted.OptionsColumn.ReadOnly = true;
            this.colSnowForcasted.Visible = true;
            this.colSnowForcasted.VisibleIndex = 7;
            this.colSnowForcasted.Width = 46;
            // 
            // colForecastMinimumTemperature
            // 
            this.colForecastMinimumTemperature.Caption = "Forecast Temperature";
            this.colForecastMinimumTemperature.ColumnEdit = this.repositoryItemTextEdit2;
            this.colForecastMinimumTemperature.FieldName = "ForecastMinimumTemperature";
            this.colForecastMinimumTemperature.Name = "colForecastMinimumTemperature";
            this.colForecastMinimumTemperature.OptionsColumn.AllowEdit = false;
            this.colForecastMinimumTemperature.OptionsColumn.AllowFocus = false;
            this.colForecastMinimumTemperature.OptionsColumn.ReadOnly = true;
            this.colForecastMinimumTemperature.Visible = true;
            this.colForecastMinimumTemperature.VisibleIndex = 6;
            this.colForecastMinimumTemperature.Width = 128;
            // 
            // colRequiredGritAmount
            // 
            this.colRequiredGritAmount.Caption = "Required Grit Amount";
            this.colRequiredGritAmount.ColumnEdit = this.repositoryItemTextEdit7;
            this.colRequiredGritAmount.FieldName = "RequiredGritAmount";
            this.colRequiredGritAmount.Name = "colRequiredGritAmount";
            this.colRequiredGritAmount.OptionsColumn.AllowEdit = false;
            this.colRequiredGritAmount.OptionsColumn.AllowFocus = false;
            this.colRequiredGritAmount.OptionsColumn.ReadOnly = true;
            this.colRequiredGritAmount.Visible = true;
            this.colRequiredGritAmount.VisibleIndex = 8;
            this.colRequiredGritAmount.Width = 124;
            // 
            // colHubID
            // 
            this.colHubID.Caption = "Hub ID";
            this.colHubID.FieldName = "HubID";
            this.colHubID.Name = "colHubID";
            this.colHubID.OptionsColumn.AllowEdit = false;
            this.colHubID.OptionsColumn.AllowFocus = false;
            this.colHubID.OptionsColumn.ReadOnly = true;
            this.colHubID.Visible = true;
            this.colHubID.VisibleIndex = 0;
            this.colHubID.Width = 59;
            // 
            // colSkippedReason
            // 
            this.colSkippedReason.Caption = "Skipped Reason";
            this.colSkippedReason.FieldName = "SkippedReason";
            this.colSkippedReason.Name = "colSkippedReason";
            this.colSkippedReason.OptionsColumn.AllowEdit = false;
            this.colSkippedReason.OptionsColumn.AllowFocus = false;
            this.colSkippedReason.Width = 120;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Width = 102;
            // 
            // colDailyGrittingEmail
            // 
            this.colDailyGrittingEmail.Caption = "Gritting Email";
            this.colDailyGrittingEmail.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDailyGrittingEmail.FieldName = "DailyGrittingEmail";
            this.colDailyGrittingEmail.Name = "colDailyGrittingEmail";
            this.colDailyGrittingEmail.OptionsColumn.AllowEdit = false;
            this.colDailyGrittingEmail.OptionsColumn.AllowFocus = false;
            this.colDailyGrittingEmail.OptionsColumn.ReadOnly = true;
            this.colDailyGrittingEmail.Visible = true;
            this.colDailyGrittingEmail.VisibleIndex = 3;
            this.colDailyGrittingEmail.Width = 83;
            // 
            // colForecastForEmail
            // 
            this.colForecastForEmail.Caption = "Email Forecast";
            this.colForecastForEmail.FieldName = "ForecastForEmail";
            this.colForecastForEmail.Name = "colForecastForEmail";
            this.colForecastForEmail.OptionsColumn.AllowEdit = false;
            this.colForecastForEmail.OptionsColumn.AllowFocus = false;
            this.colForecastForEmail.OptionsColumn.ReadOnly = true;
            this.colForecastForEmail.Width = 90;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            // 
            // colClientPOIDDescription
            // 
            this.colClientPOIDDescription.Caption = "Client PO";
            this.colClientPOIDDescription.FieldName = "ClientPOIDDescription";
            this.colClientPOIDDescription.Name = "colClientPOIDDescription";
            this.colClientPOIDDescription.OptionsColumn.AllowEdit = false;
            this.colClientPOIDDescription.OptionsColumn.AllowFocus = false;
            this.colClientPOIDDescription.OptionsColumn.ReadOnly = true;
            this.colClientPOIDDescription.Visible = true;
            this.colClientPOIDDescription.VisibleIndex = 13;
            // 
            // colDoubleGrit
            // 
            this.colDoubleGrit.Caption = "Double Grit";
            this.colDoubleGrit.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoubleGrit.FieldName = "DoubleGrit";
            this.colDoubleGrit.Name = "colDoubleGrit";
            this.colDoubleGrit.OptionsColumn.AllowEdit = false;
            this.colDoubleGrit.OptionsColumn.AllowFocus = false;
            this.colDoubleGrit.OptionsColumn.ReadOnly = true;
            this.colDoubleGrit.Visible = true;
            this.colDoubleGrit.VisibleIndex = 14;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp04054GCGrittingImportForecastDummyDataBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(33, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit5,
            this.repositoryItemTextEdit6,
            this.repositoryItemTextEdit8});
            this.gridControl2.Size = new System.Drawing.Size(395, 420);
            this.gridControl2.TabIndex = 2;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp04054GCGrittingImportForecastDummyDataBindingSource
            // 
            this.sp04054GCGrittingImportForecastDummyDataBindingSource.DataMember = "sp04054_GC_Gritting_Import_Forecast_Dummy_Data";
            this.sp04054GCGrittingImportForecastDummyDataBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.colForecastActivationPoint1,
            this.colSnowForcasted1,
            this.colForecastMinimumTemperature1,
            this.colRequiredGritAmount1,
            this.colHubID1,
            this.colSkippedReason1,
            this.colClientsSiteCode1,
            this.colDailyGrittingEmail1,
            this.colForecastForEmail1,
            this.colClientPOID1,
            this.colClientPOIDDescription1,
            this.colDoubleGrit1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Gritting Contract ID";
            this.gridColumn1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn1.FieldName = "SiteGrittingContractID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 115;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.FieldName = "SiteID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Site Name";
            this.gridColumn3.FieldName = "SiteName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 153;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Site Code";
            this.gridColumn4.FieldName = "SiteCode";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 79;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Client ID";
            this.gridColumn5.FieldName = "ClientID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Client Name";
            this.gridColumn6.FieldName = "ClientName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 180;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Gritting Report";
            this.gridColumn7.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn7.FieldName = "GrittingAuthorisationRequired";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 93;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Area";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEdit6;
            this.gridColumn8.FieldName = "Area";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 13;
            this.gridColumn8.Width = 60;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Mask.EditMask = "#######0.00  M²";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Forecasting Type ID";
            this.gridColumn9.FieldName = "ForecastingTypeID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 118;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Activation Code ID";
            this.gridColumn10.FieldName = "GrittingActivationCodeID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 111;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Activation Code";
            this.gridColumn11.FieldName = "GrittingActivationCode";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 10;
            this.gridColumn11.Width = 97;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Activation Code Value";
            this.gridColumn12.FieldName = "GrittingActivationCodeValue";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 126;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Min Temperature";
            this.gridColumn13.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn13.FieldName = "MinimumTemperature";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 11;
            this.gridColumn13.Width = 102;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "#0.0 degrees";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Proactive Price";
            this.gridColumn14.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn14.FieldName = "ClientProactivePrice";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 92;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "c";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Reactive Price";
            this.gridColumn15.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn15.FieldName = "ClientReactivePrice";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 89;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Charge for Salt";
            this.gridColumn16.FieldName = "ClientChargedForSalt";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 94;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Salt Price";
            this.gridColumn17.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn17.FieldName = "ClientSaltPrice";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Monday Grit";
            this.gridColumn18.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn18.FieldName = "GritOnMonday";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 79;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Tuesday Grit";
            this.gridColumn19.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn19.FieldName = "GritOnTuesday";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 82;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Wednesday Grit";
            this.gridColumn20.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn20.FieldName = "GritOnWednesday";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 98;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Thursday Grit";
            this.gridColumn21.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn21.FieldName = "GritOnThursday";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 79;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Friday Grit";
            this.gridColumn22.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn22.FieldName = "GritOnFriday";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Saturday Grit";
            this.gridColumn23.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn23.FieldName = "GritOnSaturday";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 82;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Sunday Grit";
            this.gridColumn24.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn24.FieldName = "GritOnSunday";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 77;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Preferred Team ID";
            this.gridColumn25.FieldName = "PreferredSubContractorID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Width = 110;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Team ID";
            this.gridColumn26.FieldName = "SubContractorID";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Preferred Team";
            this.gridColumn27.FieldName = "PreferredTeam";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 2;
            this.gridColumn27.Width = 143;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "On Holiday";
            this.gridColumn28.FieldName = "HolidayCount";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Default Grit Amount";
            this.gridColumn29.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn29.FieldName = "DefaultGritAmount";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 12;
            this.gridColumn29.Width = 116;
            // 
            // repositoryItemTextEdit8
            // 
            this.repositoryItemTextEdit8.AutoHeight = false;
            this.repositoryItemTextEdit8.Mask.EditMask = "#######0.00  25 kg Bags";
            this.repositoryItemTextEdit8.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit8.Name = "repositoryItemTextEdit8";
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Location X";
            this.gridColumn30.FieldName = "LocationX";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Location Y";
            this.gridColumn31.FieldName = "LocationY";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            // 
            // colForecastActivationPoint1
            // 
            this.colForecastActivationPoint1.Caption = "Forecast Activation Code";
            this.colForecastActivationPoint1.FieldName = "ForecastActivationPoint";
            this.colForecastActivationPoint1.Name = "colForecastActivationPoint1";
            this.colForecastActivationPoint1.OptionsColumn.AllowEdit = false;
            this.colForecastActivationPoint1.OptionsColumn.AllowFocus = false;
            this.colForecastActivationPoint1.OptionsColumn.ReadOnly = true;
            this.colForecastActivationPoint1.Visible = true;
            this.colForecastActivationPoint1.VisibleIndex = 5;
            this.colForecastActivationPoint1.Width = 142;
            // 
            // colSnowForcasted1
            // 
            this.colSnowForcasted1.Caption = "Snow";
            this.colSnowForcasted1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSnowForcasted1.FieldName = "SnowForcasted";
            this.colSnowForcasted1.Name = "colSnowForcasted1";
            this.colSnowForcasted1.OptionsColumn.AllowEdit = false;
            this.colSnowForcasted1.OptionsColumn.AllowFocus = false;
            this.colSnowForcasted1.OptionsColumn.ReadOnly = true;
            this.colSnowForcasted1.Visible = true;
            this.colSnowForcasted1.VisibleIndex = 7;
            this.colSnowForcasted1.Width = 48;
            // 
            // colForecastMinimumTemperature1
            // 
            this.colForecastMinimumTemperature1.Caption = "Forecast Temperature";
            this.colForecastMinimumTemperature1.ColumnEdit = this.repositoryItemTextEdit4;
            this.colForecastMinimumTemperature1.FieldName = "ForecastMinimumTemperature";
            this.colForecastMinimumTemperature1.Name = "colForecastMinimumTemperature1";
            this.colForecastMinimumTemperature1.OptionsColumn.AllowEdit = false;
            this.colForecastMinimumTemperature1.OptionsColumn.AllowFocus = false;
            this.colForecastMinimumTemperature1.OptionsColumn.ReadOnly = true;
            this.colForecastMinimumTemperature1.Visible = true;
            this.colForecastMinimumTemperature1.VisibleIndex = 6;
            this.colForecastMinimumTemperature1.Width = 128;
            // 
            // colRequiredGritAmount1
            // 
            this.colRequiredGritAmount1.Caption = "Required Grit Amount";
            this.colRequiredGritAmount1.ColumnEdit = this.repositoryItemTextEdit8;
            this.colRequiredGritAmount1.FieldName = "RequiredGritAmount";
            this.colRequiredGritAmount1.Name = "colRequiredGritAmount1";
            this.colRequiredGritAmount1.OptionsColumn.AllowEdit = false;
            this.colRequiredGritAmount1.OptionsColumn.AllowFocus = false;
            this.colRequiredGritAmount1.OptionsColumn.ReadOnly = true;
            this.colRequiredGritAmount1.Visible = true;
            this.colRequiredGritAmount1.VisibleIndex = 8;
            this.colRequiredGritAmount1.Width = 124;
            // 
            // colHubID1
            // 
            this.colHubID1.Caption = "Hub ID";
            this.colHubID1.FieldName = "HubID";
            this.colHubID1.Name = "colHubID1";
            this.colHubID1.OptionsColumn.AllowEdit = false;
            this.colHubID1.OptionsColumn.AllowFocus = false;
            this.colHubID1.OptionsColumn.ReadOnly = true;
            this.colHubID1.Visible = true;
            this.colHubID1.VisibleIndex = 0;
            this.colHubID1.Width = 59;
            // 
            // colSkippedReason1
            // 
            this.colSkippedReason1.Caption = "Skipped Reason";
            this.colSkippedReason1.FieldName = "SkippedReason";
            this.colSkippedReason1.Name = "colSkippedReason1";
            this.colSkippedReason1.OptionsColumn.AllowEdit = false;
            this.colSkippedReason1.OptionsColumn.AllowFocus = false;
            this.colSkippedReason1.OptionsColumn.ReadOnly = true;
            this.colSkippedReason1.Visible = true;
            this.colSkippedReason1.VisibleIndex = 9;
            this.colSkippedReason1.Width = 120;
            // 
            // colClientsSiteCode1
            // 
            this.colClientsSiteCode1.Caption = "Clients Site Code";
            this.colClientsSiteCode1.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode1.Name = "colClientsSiteCode1";
            this.colClientsSiteCode1.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode1.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode1.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode1.Width = 102;
            // 
            // colDailyGrittingEmail1
            // 
            this.colDailyGrittingEmail1.Caption = "Gritting Email";
            this.colDailyGrittingEmail1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDailyGrittingEmail1.FieldName = "DailyGrittingEmail";
            this.colDailyGrittingEmail1.Name = "colDailyGrittingEmail1";
            this.colDailyGrittingEmail1.OptionsColumn.AllowEdit = false;
            this.colDailyGrittingEmail1.OptionsColumn.AllowFocus = false;
            this.colDailyGrittingEmail1.OptionsColumn.ReadOnly = true;
            this.colDailyGrittingEmail1.Visible = true;
            this.colDailyGrittingEmail1.VisibleIndex = 3;
            this.colDailyGrittingEmail1.Width = 83;
            // 
            // colForecastForEmail1
            // 
            this.colForecastForEmail1.Caption = "Forecast Email";
            this.colForecastForEmail1.FieldName = "ForecastForEmail";
            this.colForecastForEmail1.Name = "colForecastForEmail1";
            this.colForecastForEmail1.OptionsColumn.AllowEdit = false;
            this.colForecastForEmail1.OptionsColumn.AllowFocus = false;
            this.colForecastForEmail1.OptionsColumn.ReadOnly = true;
            this.colForecastForEmail1.Width = 90;
            // 
            // colClientPOID1
            // 
            this.colClientPOID1.Caption = "Client PO ID";
            this.colClientPOID1.FieldName = "ClientPOID";
            this.colClientPOID1.Name = "colClientPOID1";
            this.colClientPOID1.OptionsColumn.AllowEdit = false;
            this.colClientPOID1.OptionsColumn.AllowFocus = false;
            this.colClientPOID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientPOIDDescription1
            // 
            this.colClientPOIDDescription1.Caption = "Client PO";
            this.colClientPOIDDescription1.FieldName = "ClientPOIDDescription";
            this.colClientPOIDDescription1.Name = "colClientPOIDDescription1";
            this.colClientPOIDDescription1.OptionsColumn.AllowEdit = false;
            this.colClientPOIDDescription1.OptionsColumn.AllowFocus = false;
            this.colClientPOIDDescription1.OptionsColumn.ReadOnly = true;
            this.colClientPOIDDescription1.Visible = true;
            this.colClientPOIDDescription1.VisibleIndex = 14;
            // 
            // colDoubleGrit1
            // 
            this.colDoubleGrit1.Caption = "Double Grit";
            this.colDoubleGrit1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoubleGrit1.FieldName = "DoubleGrit";
            this.colDoubleGrit1.Name = "colDoubleGrit1";
            this.colDoubleGrit1.OptionsColumn.AllowEdit = false;
            this.colDoubleGrit1.OptionsColumn.AllowFocus = false;
            this.colDoubleGrit1.OptionsColumn.ReadOnly = true;
            this.colDoubleGrit1.Visible = true;
            this.colDoubleGrit1.VisibleIndex = 15;
            // 
            // btnMoveRight
            // 
            this.btnMoveRight.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRight.ImageOptions.Image")));
            this.btnMoveRight.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRight.Location = new System.Drawing.Point(0, 34);
            this.btnMoveRight.Name = "btnMoveRight";
            this.btnMoveRight.Size = new System.Drawing.Size(32, 31);
            this.btnMoveRight.TabIndex = 1;
            this.btnMoveRight.Click += new System.EventHandler(this.btnMoveRight_Click);
            // 
            // btnMoveLeft
            // 
            this.btnMoveLeft.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveLeft.ImageOptions.Image")));
            this.btnMoveLeft.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeft.Location = new System.Drawing.Point(0, 0);
            this.btnMoveLeft.Name = "btnMoveLeft";
            this.btnMoveLeft.Size = new System.Drawing.Size(32, 31);
            this.btnMoveLeft.TabIndex = 0;
            this.btnMoveLeft.Click += new System.EventHandler(this.btnMoveLeft_Click);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.splitContainerControl4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(879, 511);
            this.xtraTabPage3.Text = "Floating Site Forecasts";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.simpleButton2);
            this.splitContainerControl4.Panel1.Controls.Add(this.simpleButton1);
            this.splitContainerControl4.Panel1.Controls.Add(this.labelControlInformation3);
            this.splitContainerControl4.Panel1.Controls.Add(this.hyperLinkEditErrorCount3);
            this.splitContainerControl4.Panel1.Controls.Add(this.btnSendFloatingClientEmails);
            this.splitContainerControl4.Panel1.Controls.Add(this.splitContainerControl5);
            this.splitContainerControl4.Panel1.Controls.Add(this.btnLoadFloatingForecast);
            this.splitContainerControl4.Panel1.Controls.Add(this.pictureEdit3);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.gridControl6);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControlInformation4);
            this.splitContainerControl4.Panel2.Controls.Add(this.hyperLinkEditErrorCount4);
            this.splitContainerControl4.Panel2.Controls.Add(this.btnCreateFloatingJobs);
            this.splitContainerControl4.Panel2.Controls.Add(this.pictureEdit4);
            this.splitContainerControl4.Panel2.Controls.Add(this.btnLoadFloatingJobs);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(879, 511);
            this.splitContainerControl4.SplitterPosition = 266;
            this.splitContainerControl4.TabIndex = 8;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Enabled = false;
            this.simpleButton2.Location = new System.Drawing.Point(560, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 12;
            this.simpleButton2.Text = "simpleButton2";
            this.simpleButton2.Visible = false;
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Enabled = false;
            this.simpleButton1.Location = new System.Drawing.Point(641, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 11;
            this.simpleButton1.Text = "Optimisation";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControlInformation3
            // 
            this.labelControlInformation3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlInformation3.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlInformation3.Appearance.Options.UseImageAlign = true;
            this.labelControlInformation3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlInformation3.Location = new System.Drawing.Point(178, 6);
            this.labelControlInformation3.Name = "labelControlInformation3";
            this.labelControlInformation3.Size = new System.Drawing.Size(582, 15);
            this.labelControlInformation3.TabIndex = 5;
            this.labelControlInformation3.Text = "No information present at this time.";
            // 
            // hyperLinkEditErrorCount3
            // 
            this.hyperLinkEditErrorCount3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hyperLinkEditErrorCount3.EditValue = "Error Counts";
            this.hyperLinkEditErrorCount3.Location = new System.Drawing.Point(178, 6);
            this.hyperLinkEditErrorCount3.MenuManager = this.barManager1;
            this.hyperLinkEditErrorCount3.Name = "hyperLinkEditErrorCount3";
            this.hyperLinkEditErrorCount3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.hyperLinkEditErrorCount3.Properties.Appearance.Options.UseBackColor = true;
            this.hyperLinkEditErrorCount3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.hyperLinkEditErrorCount3.Size = new System.Drawing.Size(583, 18);
            this.hyperLinkEditErrorCount3.TabIndex = 10;
            this.hyperLinkEditErrorCount3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkEditErrorCount3_OpenLink);
            // 
            // btnSendFloatingClientEmails
            // 
            this.btnSendFloatingClientEmails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendFloatingClientEmails.Location = new System.Drawing.Point(767, 3);
            this.btnSendFloatingClientEmails.Name = "btnSendFloatingClientEmails";
            this.btnSendFloatingClientEmails.Size = new System.Drawing.Size(111, 23);
            this.btnSendFloatingClientEmails.TabIndex = 9;
            this.btnSendFloatingClientEmails.Text = "Send Client Email(s)";
            this.btnSendFloatingClientEmails.Click += new System.EventHandler(this.btnSendFloatingClientEmails_Click);
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl5.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl5.Location = new System.Drawing.Point(3, 28);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl5.Panel1.Controls.Add(this.gridControl4);
            this.splitContainerControl5.Panel1.ShowCaption = true;
            this.splitContainerControl5.Panel1.Text = "Sites Requiring Gritting";
            this.splitContainerControl5.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl5.Panel2.Controls.Add(this.gridControl5);
            this.splitContainerControl5.Panel2.Controls.Add(this.btnMoveRight2);
            this.splitContainerControl5.Panel2.Controls.Add(this.btnMoveLeft2);
            this.splitContainerControl5.Panel2.ShowCaption = true;
            this.splitContainerControl5.Panel2.Text = "Sites NOT Requiring Gritting";
            this.splitContainerControl5.Size = new System.Drawing.Size(873, 236);
            this.splitContainerControl5.SplitterPosition = 420;
            this.splitContainerControl5.TabIndex = 8;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp04348GCGrittingImportFloatingForecastDataBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEdit11,
            this.repositoryItemTextEdit10,
            this.repositoryItemTextEdit9,
            this.repositoryItemTextEdit12});
            this.gridControl4.Size = new System.Drawing.Size(416, 212);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp04348GCGrittingImportFloatingForecastDataBindingSource
            // 
            this.sp04348GCGrittingImportFloatingForecastDataBindingSource.DataMember = "sp04348_GC_Gritting_Import_Floating_Forecast_Data";
            this.sp04348GCGrittingImportFloatingForecastDataBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.colLocation,
            this.colDoubleGrit2});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn37, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn34, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView4_CustomDrawCell);
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Gritting Contract ID";
            this.gridColumn32.FieldName = "SiteGrittingContractID";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 115;
            // 
            // gridColumn33
            // 
            this.gridColumn33.FieldName = "SiteID";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Site Name";
            this.gridColumn34.FieldName = "SiteName";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 2;
            this.gridColumn34.Width = 153;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Site Code";
            this.gridColumn35.FieldName = "SiteCode";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Width = 79;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Client ID";
            this.gridColumn36.FieldName = "ClientID";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Client Name";
            this.gridColumn37.FieldName = "ClientName";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 1;
            this.gridColumn37.Width = 120;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Gritting Report";
            this.gridColumn38.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn38.FieldName = "GrittingAuthorisationRequired";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 5;
            this.gridColumn38.Width = 93;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Area";
            this.gridColumn39.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn39.FieldName = "Area";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 13;
            this.gridColumn39.Width = 60;
            // 
            // repositoryItemTextEdit9
            // 
            this.repositoryItemTextEdit9.AutoHeight = false;
            this.repositoryItemTextEdit9.Mask.EditMask = "#######0.00  M²";
            this.repositoryItemTextEdit9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit9.Name = "repositoryItemTextEdit9";
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Forecasting Type ID";
            this.gridColumn40.FieldName = "ForecastingTypeID";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Width = 118;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Activation Code ID";
            this.gridColumn41.FieldName = "GrittingActivationCodeID";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 111;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Activation Code";
            this.gridColumn42.FieldName = "GrittingActivationCode";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 10;
            this.gridColumn42.Width = 97;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Activation Code Value";
            this.gridColumn43.FieldName = "GrittingActivationCodeValue";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Width = 126;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Min Temperature";
            this.gridColumn44.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn44.FieldName = "MinimumTemperature";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 11;
            this.gridColumn44.Width = 102;
            // 
            // repositoryItemTextEdit10
            // 
            this.repositoryItemTextEdit10.AutoHeight = false;
            this.repositoryItemTextEdit10.Mask.EditMask = "#0.0 degrees";
            this.repositoryItemTextEdit10.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit10.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit10.Name = "repositoryItemTextEdit10";
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Proactive Price";
            this.gridColumn45.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn45.FieldName = "ClientProactivePrice";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Width = 92;
            // 
            // repositoryItemTextEdit11
            // 
            this.repositoryItemTextEdit11.AutoHeight = false;
            this.repositoryItemTextEdit11.Mask.EditMask = "c";
            this.repositoryItemTextEdit11.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit11.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit11.Name = "repositoryItemTextEdit11";
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Reactive Price";
            this.gridColumn46.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn46.FieldName = "ClientReactivePrice";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Width = 89;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Charge for Salt";
            this.gridColumn47.FieldName = "ClientChargedForSalt";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Width = 94;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Salt Price";
            this.gridColumn48.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn48.FieldName = "ClientSaltPrice";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Monday Grit";
            this.gridColumn49.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn49.FieldName = "GritOnMonday";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Width = 79;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Tuesday Grit";
            this.gridColumn50.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn50.FieldName = "GritOnTuesday";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Width = 82;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Wednesday Grit";
            this.gridColumn51.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn51.FieldName = "GritOnWednesday";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Width = 98;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Thursday Grit";
            this.gridColumn52.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn52.FieldName = "GritOnThursday";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Width = 79;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Friday Grit";
            this.gridColumn53.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn53.FieldName = "GritOnFriday";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Saturday Grit";
            this.gridColumn54.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn54.FieldName = "GritOnSaturday";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 82;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Sunday Grit";
            this.gridColumn55.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn55.FieldName = "GritOnSunday";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Width = 77;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Preferred Team ID";
            this.gridColumn56.FieldName = "PreferredSubContractorID";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Width = 110;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Team ID";
            this.gridColumn57.FieldName = "SubContractorID";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Preferred Team";
            this.gridColumn58.FieldName = "PreferredTeam";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 3;
            this.gridColumn58.Width = 143;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "On Holiday";
            this.gridColumn59.FieldName = "HolidayCount";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Default Grit Amount";
            this.gridColumn60.ColumnEdit = this.repositoryItemTextEdit12;
            this.gridColumn60.FieldName = "DefaultGritAmount";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 12;
            this.gridColumn60.Width = 116;
            // 
            // repositoryItemTextEdit12
            // 
            this.repositoryItemTextEdit12.AutoHeight = false;
            this.repositoryItemTextEdit12.Mask.EditMask = "#######0.00  25 kg Bags";
            this.repositoryItemTextEdit12.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit12.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit12.Name = "repositoryItemTextEdit12";
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Location X";
            this.gridColumn61.FieldName = "LocationX";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Location Y";
            this.gridColumn62.FieldName = "LocationY";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Forecast Activation Code";
            this.gridColumn63.FieldName = "ForecastActivationPoint";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 6;
            this.gridColumn63.Width = 142;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Snow";
            this.gridColumn64.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn64.FieldName = "SnowForcasted";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 8;
            this.gridColumn64.Width = 46;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Forecast Temperature";
            this.gridColumn65.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn65.FieldName = "ForecastMinimumTemperature";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Visible = true;
            this.gridColumn65.VisibleIndex = 7;
            this.gridColumn65.Width = 128;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Required Grit Amount";
            this.gridColumn66.ColumnEdit = this.repositoryItemTextEdit12;
            this.gridColumn66.FieldName = "RequiredGritAmount";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Visible = true;
            this.gridColumn66.VisibleIndex = 9;
            this.gridColumn66.Width = 124;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Hub ID";
            this.gridColumn67.FieldName = "HubID";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Visible = true;
            this.gridColumn67.VisibleIndex = 0;
            this.gridColumn67.Width = 59;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Skipped Reason";
            this.gridColumn68.FieldName = "SkippedReason";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            this.gridColumn68.Width = 120;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Clients Site Code";
            this.gridColumn69.FieldName = "ClientsSiteCode";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Width = 102;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Gritting Email";
            this.gridColumn70.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn70.FieldName = "DailyGrittingEmail";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Visible = true;
            this.gridColumn70.VisibleIndex = 4;
            this.gridColumn70.Width = 83;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Email Forecast";
            this.gridColumn71.FieldName = "ForecastForEmail";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Width = 90;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Client PO ID";
            this.gridColumn72.FieldName = "ClientPOID";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Client PO";
            this.gridColumn73.FieldName = "ClientPOIDDescription";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Visible = true;
            this.gridColumn73.VisibleIndex = 14;
            // 
            // colLocation
            // 
            this.colLocation.Caption = "Location";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 15;
            // 
            // colDoubleGrit2
            // 
            this.colDoubleGrit2.Caption = "Double Grit";
            this.colDoubleGrit2.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colDoubleGrit2.FieldName = "DoubleGrit";
            this.colDoubleGrit2.Name = "colDoubleGrit2";
            this.colDoubleGrit2.OptionsColumn.AllowEdit = false;
            this.colDoubleGrit2.OptionsColumn.AllowFocus = false;
            this.colDoubleGrit2.OptionsColumn.ReadOnly = true;
            this.colDoubleGrit2.Visible = true;
            this.colDoubleGrit2.VisibleIndex = 16;
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp04349GCGrittingImportFloatingForecastDataBindingSource;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record", "delete")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(33, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEdit14,
            this.repositoryItemTextEdit15,
            this.repositoryItemTextEdit13,
            this.repositoryItemTextEdit16});
            this.gridControl5.Size = new System.Drawing.Size(409, 211);
            this.gridControl5.TabIndex = 2;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04349GCGrittingImportFloatingForecastDataBindingSource
            // 
            this.sp04349GCGrittingImportFloatingForecastDataBindingSource.DataMember = "sp04349_GC_Gritting_Import_Floating_Forecast_Data";
            this.sp04349GCGrittingImportFloatingForecastDataBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.gridColumn77,
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn83,
            this.gridColumn84,
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87,
            this.gridColumn88,
            this.gridColumn89,
            this.gridColumn90,
            this.gridColumn91,
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn94,
            this.gridColumn95,
            this.gridColumn96,
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99,
            this.gridColumn100,
            this.gridColumn101,
            this.gridColumn102,
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106,
            this.gridColumn107,
            this.gridColumn108,
            this.gridColumn109,
            this.gridColumn110,
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113,
            this.gridColumn114,
            this.gridColumn115,
            this.colLocation1,
            this.colDoubleGrit3});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn79, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn76, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView5_CustomDrawCell);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Gritting Contract ID";
            this.gridColumn74.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn74.FieldName = "SiteGrittingContractID";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Width = 115;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // gridColumn75
            // 
            this.gridColumn75.FieldName = "SiteID";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Site Name";
            this.gridColumn76.FieldName = "SiteName";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 2;
            this.gridColumn76.Width = 153;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Site Code";
            this.gridColumn77.FieldName = "SiteCode";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.Width = 79;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Client ID";
            this.gridColumn78.FieldName = "ClientID";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Client Name";
            this.gridColumn79.FieldName = "ClientName";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.Visible = true;
            this.gridColumn79.VisibleIndex = 1;
            this.gridColumn79.Width = 120;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Gritting Report";
            this.gridColumn80.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn80.FieldName = "GrittingAuthorisationRequired";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Visible = true;
            this.gridColumn80.VisibleIndex = 5;
            this.gridColumn80.Width = 93;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Area";
            this.gridColumn81.ColumnEdit = this.repositoryItemTextEdit13;
            this.gridColumn81.FieldName = "Area";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Visible = true;
            this.gridColumn81.VisibleIndex = 14;
            this.gridColumn81.Width = 60;
            // 
            // repositoryItemTextEdit13
            // 
            this.repositoryItemTextEdit13.AutoHeight = false;
            this.repositoryItemTextEdit13.Mask.EditMask = "#######0.00  M²";
            this.repositoryItemTextEdit13.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit13.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit13.Name = "repositoryItemTextEdit13";
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Forecasting Type ID";
            this.gridColumn82.FieldName = "ForecastingTypeID";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.Width = 118;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Activation Code ID";
            this.gridColumn83.FieldName = "GrittingActivationCodeID";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Width = 111;
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Activation Code";
            this.gridColumn84.FieldName = "GrittingActivationCode";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Visible = true;
            this.gridColumn84.VisibleIndex = 11;
            this.gridColumn84.Width = 97;
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Activation Code Value";
            this.gridColumn85.FieldName = "GrittingActivationCodeValue";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Width = 126;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Min Temperature";
            this.gridColumn86.ColumnEdit = this.repositoryItemTextEdit14;
            this.gridColumn86.FieldName = "MinimumTemperature";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.Visible = true;
            this.gridColumn86.VisibleIndex = 12;
            this.gridColumn86.Width = 102;
            // 
            // repositoryItemTextEdit14
            // 
            this.repositoryItemTextEdit14.AutoHeight = false;
            this.repositoryItemTextEdit14.Mask.EditMask = "#0.0 degrees";
            this.repositoryItemTextEdit14.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit14.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit14.Name = "repositoryItemTextEdit14";
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Proactive Price";
            this.gridColumn87.ColumnEdit = this.repositoryItemTextEdit15;
            this.gridColumn87.FieldName = "ClientProactivePrice";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Width = 92;
            // 
            // repositoryItemTextEdit15
            // 
            this.repositoryItemTextEdit15.AutoHeight = false;
            this.repositoryItemTextEdit15.Mask.EditMask = "c";
            this.repositoryItemTextEdit15.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit15.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit15.Name = "repositoryItemTextEdit15";
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Reactive Price";
            this.gridColumn88.ColumnEdit = this.repositoryItemTextEdit15;
            this.gridColumn88.FieldName = "ClientReactivePrice";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Width = 89;
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Charge for Salt";
            this.gridColumn89.FieldName = "ClientChargedForSalt";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Width = 94;
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Salt Price";
            this.gridColumn90.ColumnEdit = this.repositoryItemTextEdit15;
            this.gridColumn90.FieldName = "ClientSaltPrice";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Monday Grit";
            this.gridColumn91.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn91.FieldName = "GritOnMonday";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            this.gridColumn91.Width = 79;
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Tuesday Grit";
            this.gridColumn92.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn92.FieldName = "GritOnTuesday";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            this.gridColumn92.Width = 82;
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Wednesday Grit";
            this.gridColumn93.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn93.FieldName = "GritOnWednesday";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Width = 98;
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Thursday Grit";
            this.gridColumn94.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn94.FieldName = "GritOnThursday";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            this.gridColumn94.Width = 79;
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Friday Grit";
            this.gridColumn95.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn95.FieldName = "GritOnFriday";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Saturday Grit";
            this.gridColumn96.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn96.FieldName = "GritOnSaturday";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Width = 82;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Sunday Grit";
            this.gridColumn97.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn97.FieldName = "GritOnSunday";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Width = 77;
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Preferred Team ID";
            this.gridColumn98.FieldName = "PreferredSubContractorID";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Width = 110;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Team ID";
            this.gridColumn99.FieldName = "SubContractorID";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Preferred Team";
            this.gridColumn100.FieldName = "PreferredTeam";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.Visible = true;
            this.gridColumn100.VisibleIndex = 3;
            this.gridColumn100.Width = 143;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "On Holiday";
            this.gridColumn101.FieldName = "HolidayCount";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.AllowEdit = false;
            this.gridColumn101.OptionsColumn.AllowFocus = false;
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "Default Grit Amount";
            this.gridColumn102.ColumnEdit = this.repositoryItemTextEdit16;
            this.gridColumn102.FieldName = "DefaultGritAmount";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.AllowEdit = false;
            this.gridColumn102.OptionsColumn.AllowFocus = false;
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.Visible = true;
            this.gridColumn102.VisibleIndex = 13;
            this.gridColumn102.Width = 116;
            // 
            // repositoryItemTextEdit16
            // 
            this.repositoryItemTextEdit16.AutoHeight = false;
            this.repositoryItemTextEdit16.Mask.EditMask = "#######0.00  25 kg Bags";
            this.repositoryItemTextEdit16.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit16.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit16.Name = "repositoryItemTextEdit16";
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "Location X";
            this.gridColumn103.FieldName = "LocationX";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Location Y";
            this.gridColumn104.FieldName = "LocationY";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Forecast Activation Code";
            this.gridColumn105.FieldName = "ForecastActivationPoint";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Visible = true;
            this.gridColumn105.VisibleIndex = 6;
            this.gridColumn105.Width = 142;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Snow";
            this.gridColumn106.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn106.FieldName = "SnowForcasted";
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowFocus = false;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.Visible = true;
            this.gridColumn106.VisibleIndex = 8;
            this.gridColumn106.Width = 48;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "Forecast Temperature";
            this.gridColumn107.ColumnEdit = this.repositoryItemTextEdit14;
            this.gridColumn107.FieldName = "ForecastMinimumTemperature";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.AllowEdit = false;
            this.gridColumn107.OptionsColumn.AllowFocus = false;
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.Visible = true;
            this.gridColumn107.VisibleIndex = 7;
            this.gridColumn107.Width = 128;
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "Required Grit Amount";
            this.gridColumn108.ColumnEdit = this.repositoryItemTextEdit16;
            this.gridColumn108.FieldName = "RequiredGritAmount";
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowFocus = false;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            this.gridColumn108.Visible = true;
            this.gridColumn108.VisibleIndex = 9;
            this.gridColumn108.Width = 124;
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "Hub ID";
            this.gridColumn109.FieldName = "HubID";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowFocus = false;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.Visible = true;
            this.gridColumn109.VisibleIndex = 0;
            this.gridColumn109.Width = 59;
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "Skipped Reason";
            this.gridColumn110.FieldName = "SkippedReason";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowFocus = false;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            this.gridColumn110.Visible = true;
            this.gridColumn110.VisibleIndex = 10;
            this.gridColumn110.Width = 120;
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "Clients Site Code";
            this.gridColumn111.FieldName = "ClientsSiteCode";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.AllowEdit = false;
            this.gridColumn111.OptionsColumn.AllowFocus = false;
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            this.gridColumn111.Width = 102;
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "Gritting Email";
            this.gridColumn112.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn112.FieldName = "DailyGrittingEmail";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.AllowEdit = false;
            this.gridColumn112.OptionsColumn.AllowFocus = false;
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.Visible = true;
            this.gridColumn112.VisibleIndex = 4;
            this.gridColumn112.Width = 83;
            // 
            // gridColumn113
            // 
            this.gridColumn113.Caption = "Forecast Email";
            this.gridColumn113.FieldName = "ForecastForEmail";
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.OptionsColumn.AllowEdit = false;
            this.gridColumn113.OptionsColumn.AllowFocus = false;
            this.gridColumn113.OptionsColumn.ReadOnly = true;
            this.gridColumn113.Width = 90;
            // 
            // gridColumn114
            // 
            this.gridColumn114.Caption = "Client PO ID";
            this.gridColumn114.FieldName = "ClientPOID";
            this.gridColumn114.Name = "gridColumn114";
            this.gridColumn114.OptionsColumn.AllowEdit = false;
            this.gridColumn114.OptionsColumn.AllowFocus = false;
            this.gridColumn114.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn115
            // 
            this.gridColumn115.Caption = "Client PO";
            this.gridColumn115.FieldName = "ClientPOIDDescription";
            this.gridColumn115.Name = "gridColumn115";
            this.gridColumn115.OptionsColumn.AllowEdit = false;
            this.gridColumn115.OptionsColumn.AllowFocus = false;
            this.gridColumn115.OptionsColumn.ReadOnly = true;
            this.gridColumn115.Visible = true;
            this.gridColumn115.VisibleIndex = 15;
            // 
            // colLocation1
            // 
            this.colLocation1.Caption = "Location";
            this.colLocation1.FieldName = "Location";
            this.colLocation1.Name = "colLocation1";
            this.colLocation1.OptionsColumn.AllowEdit = false;
            this.colLocation1.OptionsColumn.AllowFocus = false;
            this.colLocation1.OptionsColumn.ReadOnly = true;
            this.colLocation1.Visible = true;
            this.colLocation1.VisibleIndex = 16;
            // 
            // colDoubleGrit3
            // 
            this.colDoubleGrit3.Caption = "Double Grit";
            this.colDoubleGrit3.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colDoubleGrit3.FieldName = "DoubleGrit";
            this.colDoubleGrit3.Name = "colDoubleGrit3";
            this.colDoubleGrit3.OptionsColumn.AllowEdit = false;
            this.colDoubleGrit3.OptionsColumn.AllowFocus = false;
            this.colDoubleGrit3.OptionsColumn.ReadOnly = true;
            this.colDoubleGrit3.Visible = true;
            this.colDoubleGrit3.VisibleIndex = 17;
            // 
            // btnMoveRight2
            // 
            this.btnMoveRight2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRight2.ImageOptions.Image")));
            this.btnMoveRight2.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRight2.Location = new System.Drawing.Point(0, 34);
            this.btnMoveRight2.Name = "btnMoveRight2";
            this.btnMoveRight2.Size = new System.Drawing.Size(32, 31);
            this.btnMoveRight2.TabIndex = 1;
            this.btnMoveRight2.Click += new System.EventHandler(this.btnMoveRight2_Click);
            // 
            // btnMoveLeft2
            // 
            this.btnMoveLeft2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveLeft2.ImageOptions.Image")));
            this.btnMoveLeft2.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeft2.Location = new System.Drawing.Point(0, 0);
            this.btnMoveLeft2.Name = "btnMoveLeft2";
            this.btnMoveLeft2.Size = new System.Drawing.Size(32, 31);
            this.btnMoveLeft2.TabIndex = 0;
            this.btnMoveLeft2.Click += new System.EventHandler(this.btnMoveLeft2_Click);
            // 
            // btnLoadFloatingForecast
            // 
            this.btnLoadFloatingForecast.Location = new System.Drawing.Point(1, 3);
            this.btnLoadFloatingForecast.Name = "btnLoadFloatingForecast";
            this.btnLoadFloatingForecast.Size = new System.Drawing.Size(148, 23);
            this.btnLoadFloatingForecast.TabIndex = 0;
            this.btnLoadFloatingForecast.Text = "Load Floating Site Forecast";
            this.btnLoadFloatingForecast.Click += new System.EventHandler(this.btnLoadFloatingForecast_Click);
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.EditValue = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.pictureEdit3.Location = new System.Drawing.Point(157, 5);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Size = new System.Drawing.Size(19, 19);
            this.pictureEdit3.TabIndex = 7;
            // 
            // gridControl6
            // 
            this.gridControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl6.DataSource = this.sp04351GCFloatingSiteJobsImportDataBindingSource;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Row(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Row(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(1, 25);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit5,
            this.repositoryItemTextEdit25KGBags,
            this.repositoryItemTextEditDegrees,
            this.repositoryItemTextEditCurrency});
            this.gridControl6.Size = new System.Drawing.Size(878, 215);
            this.gridControl6.TabIndex = 12;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp04351GCFloatingSiteJobsImportDataBindingSource
            // 
            this.sp04351GCFloatingSiteJobsImportDataBindingSource.DataMember = "sp04351_GC_Floating_Site_Jobs_Import_Data";
            this.sp04351GCFloatingSiteJobsImportDataBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn116,
            this.gridColumn117,
            this.gridColumn118,
            this.gridColumn119,
            this.gridColumn120,
            this.gridColumn121,
            this.gridColumn122,
            this.gridColumn123,
            this.gridColumn124,
            this.gridColumn125,
            this.gridColumn126,
            this.gridColumn127,
            this.gridColumn128,
            this.gridColumn129,
            this.gridColumn130,
            this.gridColumn131,
            this.gridColumn132,
            this.gridColumn133,
            this.gridColumn134,
            this.gridColumn135,
            this.gridColumn136,
            this.gridColumn137,
            this.gridColumn138,
            this.gridColumn139,
            this.gridColumn140,
            this.gridColumn141,
            this.gridColumn142,
            this.gridColumn143,
            this.gridColumn144,
            this.gridColumn145,
            this.gridColumn146,
            this.gridColumn147,
            this.gridColumn148,
            this.gridColumn149,
            this.gridColumn150,
            this.gridColumn151,
            this.gridColumn152,
            this.gridColumn153,
            this.gridColumn154,
            this.gridColumn155,
            this.gridColumn156,
            this.gridColumn157,
            this.colLineID,
            this.colOrderNumber,
            this.colPostCode1,
            this.colMainWorkCtr,
            this.colAddress,
            this.colLocation2,
            this.colDoubleGrit4});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsLayout.StoreFormatRules = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn121, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn118, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView6_CustomDrawCell);
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn116
            // 
            this.gridColumn116.Caption = "Gritting Contract ID";
            this.gridColumn116.FieldName = "SiteGrittingContractID";
            this.gridColumn116.Name = "gridColumn116";
            this.gridColumn116.OptionsColumn.AllowEdit = false;
            this.gridColumn116.OptionsColumn.AllowFocus = false;
            this.gridColumn116.OptionsColumn.ReadOnly = true;
            this.gridColumn116.Width = 115;
            // 
            // gridColumn117
            // 
            this.gridColumn117.FieldName = "SiteID";
            this.gridColumn117.Name = "gridColumn117";
            this.gridColumn117.OptionsColumn.AllowEdit = false;
            this.gridColumn117.OptionsColumn.AllowFocus = false;
            this.gridColumn117.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn118
            // 
            this.gridColumn118.Caption = "Site Name";
            this.gridColumn118.FieldName = "SiteName";
            this.gridColumn118.Name = "gridColumn118";
            this.gridColumn118.OptionsColumn.AllowEdit = false;
            this.gridColumn118.OptionsColumn.AllowFocus = false;
            this.gridColumn118.OptionsColumn.ReadOnly = true;
            this.gridColumn118.Visible = true;
            this.gridColumn118.VisibleIndex = 2;
            this.gridColumn118.Width = 153;
            // 
            // gridColumn119
            // 
            this.gridColumn119.Caption = "Site Code";
            this.gridColumn119.FieldName = "SiteCode";
            this.gridColumn119.Name = "gridColumn119";
            this.gridColumn119.OptionsColumn.AllowEdit = false;
            this.gridColumn119.OptionsColumn.AllowFocus = false;
            this.gridColumn119.OptionsColumn.ReadOnly = true;
            this.gridColumn119.Width = 79;
            // 
            // gridColumn120
            // 
            this.gridColumn120.Caption = "Client ID";
            this.gridColumn120.FieldName = "ClientID";
            this.gridColumn120.Name = "gridColumn120";
            this.gridColumn120.OptionsColumn.AllowEdit = false;
            this.gridColumn120.OptionsColumn.AllowFocus = false;
            this.gridColumn120.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn121
            // 
            this.gridColumn121.Caption = "Client Name";
            this.gridColumn121.FieldName = "ClientName";
            this.gridColumn121.Name = "gridColumn121";
            this.gridColumn121.OptionsColumn.AllowEdit = false;
            this.gridColumn121.OptionsColumn.AllowFocus = false;
            this.gridColumn121.OptionsColumn.ReadOnly = true;
            this.gridColumn121.Visible = true;
            this.gridColumn121.VisibleIndex = 1;
            this.gridColumn121.Width = 120;
            // 
            // gridColumn122
            // 
            this.gridColumn122.Caption = "Gritting Report";
            this.gridColumn122.FieldName = "GrittingAuthorisationRequired";
            this.gridColumn122.Name = "gridColumn122";
            this.gridColumn122.OptionsColumn.AllowEdit = false;
            this.gridColumn122.OptionsColumn.AllowFocus = false;
            this.gridColumn122.OptionsColumn.ReadOnly = true;
            this.gridColumn122.Width = 93;
            // 
            // gridColumn123
            // 
            this.gridColumn123.Caption = "Area";
            this.gridColumn123.FieldName = "Area";
            this.gridColumn123.Name = "gridColumn123";
            this.gridColumn123.OptionsColumn.AllowEdit = false;
            this.gridColumn123.OptionsColumn.AllowFocus = false;
            this.gridColumn123.OptionsColumn.ReadOnly = true;
            this.gridColumn123.Width = 60;
            // 
            // gridColumn124
            // 
            this.gridColumn124.Caption = "Forecasting Type ID";
            this.gridColumn124.FieldName = "ForecastingTypeID";
            this.gridColumn124.Name = "gridColumn124";
            this.gridColumn124.OptionsColumn.AllowEdit = false;
            this.gridColumn124.OptionsColumn.AllowFocus = false;
            this.gridColumn124.OptionsColumn.ReadOnly = true;
            this.gridColumn124.Width = 118;
            // 
            // gridColumn125
            // 
            this.gridColumn125.Caption = "Activation Code ID";
            this.gridColumn125.FieldName = "GrittingActivationCodeID";
            this.gridColumn125.Name = "gridColumn125";
            this.gridColumn125.OptionsColumn.AllowEdit = false;
            this.gridColumn125.OptionsColumn.AllowFocus = false;
            this.gridColumn125.OptionsColumn.ReadOnly = true;
            this.gridColumn125.Width = 111;
            // 
            // gridColumn126
            // 
            this.gridColumn126.Caption = "Activation Code";
            this.gridColumn126.FieldName = "GrittingActivationCode";
            this.gridColumn126.Name = "gridColumn126";
            this.gridColumn126.OptionsColumn.AllowEdit = false;
            this.gridColumn126.OptionsColumn.AllowFocus = false;
            this.gridColumn126.OptionsColumn.ReadOnly = true;
            this.gridColumn126.Width = 97;
            // 
            // gridColumn127
            // 
            this.gridColumn127.Caption = "Activation Code Value";
            this.gridColumn127.FieldName = "GrittingActivationCodeValue";
            this.gridColumn127.Name = "gridColumn127";
            this.gridColumn127.OptionsColumn.AllowEdit = false;
            this.gridColumn127.OptionsColumn.AllowFocus = false;
            this.gridColumn127.OptionsColumn.ReadOnly = true;
            this.gridColumn127.Width = 126;
            // 
            // gridColumn128
            // 
            this.gridColumn128.Caption = "Min Temperature";
            this.gridColumn128.ColumnEdit = this.repositoryItemTextEditDegrees;
            this.gridColumn128.FieldName = "MinimumTemperature";
            this.gridColumn128.Name = "gridColumn128";
            this.gridColumn128.OptionsColumn.AllowEdit = false;
            this.gridColumn128.OptionsColumn.AllowFocus = false;
            this.gridColumn128.OptionsColumn.ReadOnly = true;
            this.gridColumn128.Width = 102;
            // 
            // repositoryItemTextEditDegrees
            // 
            this.repositoryItemTextEditDegrees.AutoHeight = false;
            this.repositoryItemTextEditDegrees.Mask.EditMask = "#0.0 degrees";
            this.repositoryItemTextEditDegrees.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditDegrees.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDegrees.Name = "repositoryItemTextEditDegrees";
            // 
            // gridColumn129
            // 
            this.gridColumn129.Caption = "Proactive Price";
            this.gridColumn129.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.gridColumn129.FieldName = "ClientProactivePrice";
            this.gridColumn129.Name = "gridColumn129";
            this.gridColumn129.OptionsColumn.AllowEdit = false;
            this.gridColumn129.OptionsColumn.AllowFocus = false;
            this.gridColumn129.OptionsColumn.ReadOnly = true;
            this.gridColumn129.Width = 92;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            this.repositoryItemTextEditCurrency.ValidateOnEnterKey = true;
            // 
            // gridColumn130
            // 
            this.gridColumn130.Caption = "Reactive Price";
            this.gridColumn130.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.gridColumn130.FieldName = "ClientReactivePrice";
            this.gridColumn130.Name = "gridColumn130";
            this.gridColumn130.OptionsColumn.AllowEdit = false;
            this.gridColumn130.OptionsColumn.AllowFocus = false;
            this.gridColumn130.OptionsColumn.ReadOnly = true;
            this.gridColumn130.Width = 89;
            // 
            // gridColumn131
            // 
            this.gridColumn131.Caption = "Charge for Salt";
            this.gridColumn131.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn131.FieldName = "ClientChargedForSalt";
            this.gridColumn131.Name = "gridColumn131";
            this.gridColumn131.OptionsColumn.AllowEdit = false;
            this.gridColumn131.OptionsColumn.AllowFocus = false;
            this.gridColumn131.OptionsColumn.ReadOnly = true;
            this.gridColumn131.Width = 94;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // gridColumn132
            // 
            this.gridColumn132.Caption = "Salt Price";
            this.gridColumn132.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.gridColumn132.FieldName = "ClientSaltPrice";
            this.gridColumn132.Name = "gridColumn132";
            this.gridColumn132.OptionsColumn.AllowEdit = false;
            this.gridColumn132.OptionsColumn.AllowFocus = false;
            this.gridColumn132.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn133
            // 
            this.gridColumn133.Caption = "Monday Grit";
            this.gridColumn133.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn133.FieldName = "GritOnMonday";
            this.gridColumn133.Name = "gridColumn133";
            this.gridColumn133.OptionsColumn.AllowEdit = false;
            this.gridColumn133.OptionsColumn.AllowFocus = false;
            this.gridColumn133.OptionsColumn.ReadOnly = true;
            this.gridColumn133.Width = 79;
            // 
            // gridColumn134
            // 
            this.gridColumn134.Caption = "Tuesday Grit";
            this.gridColumn134.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn134.FieldName = "GritOnTuesday";
            this.gridColumn134.Name = "gridColumn134";
            this.gridColumn134.OptionsColumn.AllowEdit = false;
            this.gridColumn134.OptionsColumn.AllowFocus = false;
            this.gridColumn134.OptionsColumn.ReadOnly = true;
            this.gridColumn134.Width = 82;
            // 
            // gridColumn135
            // 
            this.gridColumn135.Caption = "Wednesday Grit";
            this.gridColumn135.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn135.FieldName = "GritOnWednesday";
            this.gridColumn135.Name = "gridColumn135";
            this.gridColumn135.OptionsColumn.AllowEdit = false;
            this.gridColumn135.OptionsColumn.AllowFocus = false;
            this.gridColumn135.OptionsColumn.ReadOnly = true;
            this.gridColumn135.Width = 98;
            // 
            // gridColumn136
            // 
            this.gridColumn136.Caption = "Thursday Grit";
            this.gridColumn136.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn136.FieldName = "GritOnThursday";
            this.gridColumn136.Name = "gridColumn136";
            this.gridColumn136.OptionsColumn.AllowEdit = false;
            this.gridColumn136.OptionsColumn.AllowFocus = false;
            this.gridColumn136.OptionsColumn.ReadOnly = true;
            this.gridColumn136.Width = 79;
            // 
            // gridColumn137
            // 
            this.gridColumn137.Caption = "Friday Grit";
            this.gridColumn137.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn137.FieldName = "GritOnFriday";
            this.gridColumn137.Name = "gridColumn137";
            this.gridColumn137.OptionsColumn.AllowEdit = false;
            this.gridColumn137.OptionsColumn.AllowFocus = false;
            this.gridColumn137.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn138
            // 
            this.gridColumn138.Caption = "Saturday Grit";
            this.gridColumn138.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn138.FieldName = "GritOnSaturday";
            this.gridColumn138.Name = "gridColumn138";
            this.gridColumn138.OptionsColumn.AllowEdit = false;
            this.gridColumn138.OptionsColumn.AllowFocus = false;
            this.gridColumn138.OptionsColumn.ReadOnly = true;
            this.gridColumn138.Width = 82;
            // 
            // gridColumn139
            // 
            this.gridColumn139.Caption = "Sunday Grit";
            this.gridColumn139.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn139.FieldName = "GritOnSunday";
            this.gridColumn139.Name = "gridColumn139";
            this.gridColumn139.OptionsColumn.AllowEdit = false;
            this.gridColumn139.OptionsColumn.AllowFocus = false;
            this.gridColumn139.OptionsColumn.ReadOnly = true;
            this.gridColumn139.Width = 77;
            // 
            // gridColumn140
            // 
            this.gridColumn140.Caption = "Preferred Team ID";
            this.gridColumn140.FieldName = "PreferredSubContractorID";
            this.gridColumn140.Name = "gridColumn140";
            this.gridColumn140.OptionsColumn.AllowEdit = false;
            this.gridColumn140.OptionsColumn.AllowFocus = false;
            this.gridColumn140.OptionsColumn.ReadOnly = true;
            this.gridColumn140.Width = 110;
            // 
            // gridColumn141
            // 
            this.gridColumn141.Caption = "Team ID";
            this.gridColumn141.FieldName = "SubContractorID";
            this.gridColumn141.Name = "gridColumn141";
            this.gridColumn141.OptionsColumn.AllowEdit = false;
            this.gridColumn141.OptionsColumn.AllowFocus = false;
            this.gridColumn141.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn142
            // 
            this.gridColumn142.Caption = "Preferred Team";
            this.gridColumn142.FieldName = "PreferredTeam";
            this.gridColumn142.Name = "gridColumn142";
            this.gridColumn142.OptionsColumn.AllowEdit = false;
            this.gridColumn142.OptionsColumn.AllowFocus = false;
            this.gridColumn142.OptionsColumn.ReadOnly = true;
            this.gridColumn142.Visible = true;
            this.gridColumn142.VisibleIndex = 3;
            this.gridColumn142.Width = 143;
            // 
            // gridColumn143
            // 
            this.gridColumn143.Caption = "On Holiday";
            this.gridColumn143.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn143.FieldName = "HolidayCount";
            this.gridColumn143.Name = "gridColumn143";
            this.gridColumn143.OptionsColumn.AllowEdit = false;
            this.gridColumn143.OptionsColumn.AllowFocus = false;
            this.gridColumn143.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn144
            // 
            this.gridColumn144.Caption = "Default Grit Amount";
            this.gridColumn144.ColumnEdit = this.repositoryItemTextEdit25KGBags;
            this.gridColumn144.FieldName = "DefaultGritAmount";
            this.gridColumn144.Name = "gridColumn144";
            this.gridColumn144.OptionsColumn.AllowEdit = false;
            this.gridColumn144.OptionsColumn.AllowFocus = false;
            this.gridColumn144.OptionsColumn.ReadOnly = true;
            this.gridColumn144.Visible = true;
            this.gridColumn144.VisibleIndex = 10;
            this.gridColumn144.Width = 116;
            // 
            // repositoryItemTextEdit25KGBags
            // 
            this.repositoryItemTextEdit25KGBags.AutoHeight = false;
            this.repositoryItemTextEdit25KGBags.Mask.EditMask = "#######0.00  25 kg Bags";
            this.repositoryItemTextEdit25KGBags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KGBags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KGBags.Name = "repositoryItemTextEdit25KGBags";
            // 
            // gridColumn145
            // 
            this.gridColumn145.Caption = "Location X";
            this.gridColumn145.FieldName = "LocationX";
            this.gridColumn145.Name = "gridColumn145";
            this.gridColumn145.OptionsColumn.AllowEdit = false;
            this.gridColumn145.OptionsColumn.AllowFocus = false;
            this.gridColumn145.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn146
            // 
            this.gridColumn146.Caption = "Location Y";
            this.gridColumn146.FieldName = "LocationY";
            this.gridColumn146.Name = "gridColumn146";
            this.gridColumn146.OptionsColumn.AllowEdit = false;
            this.gridColumn146.OptionsColumn.AllowFocus = false;
            this.gridColumn146.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn147
            // 
            this.gridColumn147.Caption = "Forecast Activation Code";
            this.gridColumn147.FieldName = "ForecastActivationPoint";
            this.gridColumn147.Name = "gridColumn147";
            this.gridColumn147.OptionsColumn.AllowEdit = false;
            this.gridColumn147.OptionsColumn.AllowFocus = false;
            this.gridColumn147.OptionsColumn.ReadOnly = true;
            this.gridColumn147.Width = 142;
            // 
            // gridColumn148
            // 
            this.gridColumn148.Caption = "Snow";
            this.gridColumn148.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn148.FieldName = "SnowForcasted";
            this.gridColumn148.Name = "gridColumn148";
            this.gridColumn148.OptionsColumn.AllowEdit = false;
            this.gridColumn148.OptionsColumn.AllowFocus = false;
            this.gridColumn148.OptionsColumn.ReadOnly = true;
            this.gridColumn148.Visible = true;
            this.gridColumn148.VisibleIndex = 8;
            this.gridColumn148.Width = 48;
            // 
            // gridColumn149
            // 
            this.gridColumn149.Caption = "Forecast Temperature";
            this.gridColumn149.FieldName = "ForecastMinimumTemperature";
            this.gridColumn149.Name = "gridColumn149";
            this.gridColumn149.OptionsColumn.AllowEdit = false;
            this.gridColumn149.OptionsColumn.AllowFocus = false;
            this.gridColumn149.OptionsColumn.ReadOnly = true;
            this.gridColumn149.Visible = true;
            this.gridColumn149.VisibleIndex = 7;
            this.gridColumn149.Width = 128;
            // 
            // gridColumn150
            // 
            this.gridColumn150.Caption = "Required Grit Amount";
            this.gridColumn150.ColumnEdit = this.repositoryItemTextEdit25KGBags;
            this.gridColumn150.FieldName = "RequiredGritAmount";
            this.gridColumn150.Name = "gridColumn150";
            this.gridColumn150.OptionsColumn.AllowEdit = false;
            this.gridColumn150.OptionsColumn.AllowFocus = false;
            this.gridColumn150.OptionsColumn.ReadOnly = true;
            this.gridColumn150.Visible = true;
            this.gridColumn150.VisibleIndex = 9;
            this.gridColumn150.Width = 124;
            // 
            // gridColumn151
            // 
            this.gridColumn151.Caption = "Hub ID";
            this.gridColumn151.FieldName = "HubID";
            this.gridColumn151.Name = "gridColumn151";
            this.gridColumn151.OptionsColumn.AllowEdit = false;
            this.gridColumn151.OptionsColumn.AllowFocus = false;
            this.gridColumn151.OptionsColumn.ReadOnly = true;
            this.gridColumn151.Visible = true;
            this.gridColumn151.VisibleIndex = 0;
            this.gridColumn151.Width = 59;
            // 
            // gridColumn152
            // 
            this.gridColumn152.Caption = "Skipped Reason";
            this.gridColumn152.FieldName = "SkippedReason";
            this.gridColumn152.Name = "gridColumn152";
            this.gridColumn152.OptionsColumn.AllowEdit = false;
            this.gridColumn152.OptionsColumn.AllowFocus = false;
            this.gridColumn152.OptionsColumn.ReadOnly = true;
            this.gridColumn152.Width = 120;
            // 
            // gridColumn153
            // 
            this.gridColumn153.Caption = "Clients Site Code";
            this.gridColumn153.FieldName = "ClientsSiteCode";
            this.gridColumn153.Name = "gridColumn153";
            this.gridColumn153.OptionsColumn.AllowEdit = false;
            this.gridColumn153.OptionsColumn.AllowFocus = false;
            this.gridColumn153.OptionsColumn.ReadOnly = true;
            this.gridColumn153.Width = 102;
            // 
            // gridColumn154
            // 
            this.gridColumn154.Caption = "Gritting Email";
            this.gridColumn154.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn154.FieldName = "DailyGrittingEmail";
            this.gridColumn154.Name = "gridColumn154";
            this.gridColumn154.OptionsColumn.AllowEdit = false;
            this.gridColumn154.OptionsColumn.AllowFocus = false;
            this.gridColumn154.OptionsColumn.ReadOnly = true;
            this.gridColumn154.Width = 83;
            // 
            // gridColumn155
            // 
            this.gridColumn155.Caption = "Forecast Email";
            this.gridColumn155.FieldName = "ForecastForEmail";
            this.gridColumn155.Name = "gridColumn155";
            this.gridColumn155.OptionsColumn.AllowEdit = false;
            this.gridColumn155.OptionsColumn.AllowFocus = false;
            this.gridColumn155.OptionsColumn.ReadOnly = true;
            this.gridColumn155.Width = 90;
            // 
            // gridColumn156
            // 
            this.gridColumn156.Caption = "Client PO ID";
            this.gridColumn156.FieldName = "ClientPOID";
            this.gridColumn156.Name = "gridColumn156";
            this.gridColumn156.OptionsColumn.AllowEdit = false;
            this.gridColumn156.OptionsColumn.AllowFocus = false;
            this.gridColumn156.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn157
            // 
            this.gridColumn157.Caption = "Client PO";
            this.gridColumn157.FieldName = "ClientPOIDDescription";
            this.gridColumn157.Name = "gridColumn157";
            this.gridColumn157.OptionsColumn.AllowEdit = false;
            this.gridColumn157.OptionsColumn.AllowFocus = false;
            this.gridColumn157.OptionsColumn.ReadOnly = true;
            // 
            // colLineID
            // 
            this.colLineID.Caption = "Line ID";
            this.colLineID.FieldName = "LineID";
            this.colLineID.Name = "colLineID";
            this.colLineID.OptionsColumn.AllowEdit = false;
            this.colLineID.OptionsColumn.AllowFocus = false;
            this.colLineID.OptionsColumn.ReadOnly = true;
            this.colLineID.Visible = true;
            this.colLineID.VisibleIndex = 11;
            // 
            // colOrderNumber
            // 
            this.colOrderNumber.Caption = "Order #";
            this.colOrderNumber.FieldName = "OrderNumber";
            this.colOrderNumber.Name = "colOrderNumber";
            this.colOrderNumber.OptionsColumn.AllowEdit = false;
            this.colOrderNumber.OptionsColumn.AllowFocus = false;
            this.colOrderNumber.OptionsColumn.ReadOnly = true;
            this.colOrderNumber.Visible = true;
            this.colOrderNumber.VisibleIndex = 4;
            // 
            // colPostCode1
            // 
            this.colPostCode1.Caption = "Postcode";
            this.colPostCode1.FieldName = "PostCode";
            this.colPostCode1.Name = "colPostCode1";
            this.colPostCode1.OptionsColumn.AllowEdit = false;
            this.colPostCode1.OptionsColumn.AllowFocus = false;
            this.colPostCode1.OptionsColumn.ReadOnly = true;
            this.colPostCode1.Visible = true;
            this.colPostCode1.VisibleIndex = 5;
            // 
            // colMainWorkCtr
            // 
            this.colMainWorkCtr.Caption = "Main WorkCtr";
            this.colMainWorkCtr.FieldName = "MainWorkCtr";
            this.colMainWorkCtr.Name = "colMainWorkCtr";
            this.colMainWorkCtr.OptionsColumn.AllowEdit = false;
            this.colMainWorkCtr.OptionsColumn.AllowFocus = false;
            this.colMainWorkCtr.OptionsColumn.ReadOnly = true;
            this.colMainWorkCtr.Visible = true;
            this.colMainWorkCtr.VisibleIndex = 12;
            this.colMainWorkCtr.Width = 83;
            // 
            // colAddress
            // 
            this.colAddress.Caption = "Address";
            this.colAddress.FieldName = "Address";
            this.colAddress.Name = "colAddress";
            this.colAddress.OptionsColumn.AllowEdit = false;
            this.colAddress.OptionsColumn.AllowFocus = false;
            this.colAddress.OptionsColumn.ReadOnly = true;
            this.colAddress.Visible = true;
            this.colAddress.VisibleIndex = 6;
            this.colAddress.Width = 268;
            // 
            // colLocation2
            // 
            this.colLocation2.Caption = "Location";
            this.colLocation2.FieldName = "Location";
            this.colLocation2.Name = "colLocation2";
            this.colLocation2.OptionsColumn.AllowEdit = false;
            this.colLocation2.OptionsColumn.AllowFocus = false;
            this.colLocation2.OptionsColumn.ReadOnly = true;
            this.colLocation2.Visible = true;
            this.colLocation2.VisibleIndex = 13;
            // 
            // colDoubleGrit4
            // 
            this.colDoubleGrit4.Caption = "Double Grit";
            this.colDoubleGrit4.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colDoubleGrit4.FieldName = "DoubleGrit";
            this.colDoubleGrit4.Name = "colDoubleGrit4";
            this.colDoubleGrit4.OptionsColumn.AllowEdit = false;
            this.colDoubleGrit4.OptionsColumn.AllowFocus = false;
            this.colDoubleGrit4.OptionsColumn.ReadOnly = true;
            this.colDoubleGrit4.Visible = true;
            this.colDoubleGrit4.VisibleIndex = 14;
            // 
            // labelControlInformation4
            // 
            this.labelControlInformation4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlInformation4.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlInformation4.Appearance.Options.UseImageAlign = true;
            this.labelControlInformation4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlInformation4.Location = new System.Drawing.Point(113, 4);
            this.labelControlInformation4.Name = "labelControlInformation4";
            this.labelControlInformation4.Size = new System.Drawing.Size(647, 15);
            this.labelControlInformation4.TabIndex = 8;
            this.labelControlInformation4.Text = "No information present at this time.";
            // 
            // hyperLinkEditErrorCount4
            // 
            this.hyperLinkEditErrorCount4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hyperLinkEditErrorCount4.EditValue = "Error Counts";
            this.hyperLinkEditErrorCount4.Location = new System.Drawing.Point(113, 4);
            this.hyperLinkEditErrorCount4.MenuManager = this.barManager1;
            this.hyperLinkEditErrorCount4.Name = "hyperLinkEditErrorCount4";
            this.hyperLinkEditErrorCount4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.hyperLinkEditErrorCount4.Properties.Appearance.Options.UseBackColor = true;
            this.hyperLinkEditErrorCount4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.hyperLinkEditErrorCount4.Size = new System.Drawing.Size(646, 18);
            this.hyperLinkEditErrorCount4.TabIndex = 11;
            this.hyperLinkEditErrorCount4.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkEditErrorCount4_OpenLink);
            // 
            // btnCreateFloatingJobs
            // 
            this.btnCreateFloatingJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateFloatingJobs.Location = new System.Drawing.Point(767, 0);
            this.btnCreateFloatingJobs.Name = "btnCreateFloatingJobs";
            this.btnCreateFloatingJobs.Size = new System.Drawing.Size(111, 23);
            this.btnCreateFloatingJobs.TabIndex = 10;
            this.btnCreateFloatingJobs.Text = "Create Callouts";
            this.btnCreateFloatingJobs.Click += new System.EventHandler(this.btnCreateFloatingJobs_Click);
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.EditValue = ((object)(resources.GetObject("pictureEdit4.EditValue")));
            this.pictureEdit4.Location = new System.Drawing.Point(92, 3);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Size = new System.Drawing.Size(19, 19);
            this.pictureEdit4.TabIndex = 9;
            // 
            // btnLoadFloatingJobs
            // 
            this.btnLoadFloatingJobs.Location = new System.Drawing.Point(1, 0);
            this.btnLoadFloatingJobs.Name = "btnLoadFloatingJobs";
            this.btnLoadFloatingJobs.Size = new System.Drawing.Size(83, 23);
            this.btnLoadFloatingJobs.TabIndex = 1;
            this.btnLoadFloatingJobs.Text = "Load Jobs";
            this.btnLoadFloatingJobs.Click += new System.EventHandler(this.btnLoadFloatingJobs_Click);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(879, 511);
            this.xtraTabPage2.Text = "Import Client Authorisation";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.pictureEdit2);
            this.splitContainerControl3.Panel1.Controls.Add(this.labelControlInformation2);
            this.splitContainerControl3.Panel1.Controls.Add(this.hyperLinkEditErrorCount2);
            this.splitContainerControl3.Panel1.Controls.Add(this.memoEdit1);
            this.splitContainerControl3.Panel1.Controls.Add(this.btnLoadAuthorisation);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl3.Panel2.Controls.Add(this.checkEditDeleteUnauthorised);
            this.splitContainerControl3.Panel2.Controls.Add(this.gridControl3);
            this.splitContainerControl3.Panel2.Controls.Add(this.btnImportAuthorisation);
            this.splitContainerControl3.Panel2.ShowCaption = true;
            this.splitContainerControl3.Panel2.Text = "Authorised Sites";
            this.splitContainerControl3.Size = new System.Drawing.Size(879, 511);
            this.splitContainerControl3.SplitterPosition = 78;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.pictureEdit2.Location = new System.Drawing.Point(191, 7);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Size = new System.Drawing.Size(19, 19);
            this.pictureEdit2.TabIndex = 6;
            // 
            // labelControlInformation2
            // 
            this.labelControlInformation2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlInformation2.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlInformation2.Appearance.Options.UseImageAlign = true;
            this.labelControlInformation2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlInformation2.Location = new System.Drawing.Point(212, 8);
            this.labelControlInformation2.Name = "labelControlInformation2";
            this.labelControlInformation2.Size = new System.Drawing.Size(667, 15);
            this.labelControlInformation2.TabIndex = 4;
            this.labelControlInformation2.Text = "No information present at this time.";
            // 
            // hyperLinkEditErrorCount2
            // 
            this.hyperLinkEditErrorCount2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hyperLinkEditErrorCount2.EditValue = "Error Counts";
            this.hyperLinkEditErrorCount2.Location = new System.Drawing.Point(211, 6);
            this.hyperLinkEditErrorCount2.MenuManager = this.barManager1;
            this.hyperLinkEditErrorCount2.Name = "hyperLinkEditErrorCount2";
            this.hyperLinkEditErrorCount2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.hyperLinkEditErrorCount2.Properties.Appearance.Options.UseBackColor = true;
            this.hyperLinkEditErrorCount2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.hyperLinkEditErrorCount2.Size = new System.Drawing.Size(665, 18);
            this.hyperLinkEditErrorCount2.TabIndex = 5;
            this.hyperLinkEditErrorCount2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkEditErrorCount2_OpenLink);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEdit1.Location = new System.Drawing.Point(3, 29);
            this.memoEdit1.MenuManager = this.barManager1;
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.memoEdit1, true);
            this.memoEdit1.Size = new System.Drawing.Size(873, 47);
            this.scSpellChecker.SetSpellCheckerOptions(this.memoEdit1, optionsSpelling1);
            this.memoEdit1.TabIndex = 1;
            // 
            // btnLoadAuthorisation
            // 
            this.btnLoadAuthorisation.Location = new System.Drawing.Point(3, 3);
            this.btnLoadAuthorisation.Name = "btnLoadAuthorisation";
            this.btnLoadAuthorisation.Size = new System.Drawing.Size(180, 23);
            this.btnLoadAuthorisation.TabIndex = 0;
            this.btnLoadAuthorisation.Text = "Load Authorisation Spreadsheets";
            this.btnLoadAuthorisation.Click += new System.EventHandler(this.btnLoadAuthorisation_Click);
            // 
            // checkEditDeleteUnauthorised
            // 
            this.checkEditDeleteUnauthorised.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditDeleteUnauthorised.EditValue = true;
            this.checkEditDeleteUnauthorised.Location = new System.Drawing.Point(615, 381);
            this.checkEditDeleteUnauthorised.MenuManager = this.barManager1;
            this.checkEditDeleteUnauthorised.Name = "checkEditDeleteUnauthorised";
            this.checkEditDeleteUnauthorised.Properties.Caption = "Delete Unauthorised Grits";
            this.checkEditDeleteUnauthorised.Size = new System.Drawing.Size(152, 19);
            this.checkEditDeleteUnauthorised.TabIndex = 2;
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp04068GCSpreadsheetAuthorisationDummyBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(875, 376);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp04068GCSpreadsheetAuthorisationDummyBindingSource
            // 
            this.sp04068GCSpreadsheetAuthorisationDummyBindingSource.DataMember = "sp04068_GC_Spreadsheet_Authorisation_Dummy";
            this.sp04068GCSpreadsheetAuthorisationDummyBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGrit_Required,
            this.colGC_Site_ID,
            this.colClient_Name,
            this.colStore_Name,
            this.colPostcode,
            this.colClient_Site_ID,
            this.colArea1,
            this.colWeather});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colGrit_Required
            // 
            this.colGrit_Required.Caption = "Grit Required";
            this.colGrit_Required.FieldName = "Grit_Required";
            this.colGrit_Required.Name = "colGrit_Required";
            this.colGrit_Required.OptionsColumn.AllowEdit = false;
            this.colGrit_Required.OptionsColumn.AllowFocus = false;
            this.colGrit_Required.OptionsColumn.ReadOnly = true;
            this.colGrit_Required.Visible = true;
            this.colGrit_Required.VisibleIndex = 0;
            this.colGrit_Required.Width = 84;
            // 
            // colGC_Site_ID
            // 
            this.colGC_Site_ID.Caption = "GC Site ID";
            this.colGC_Site_ID.FieldName = "GC_Site_ID";
            this.colGC_Site_ID.Name = "colGC_Site_ID";
            this.colGC_Site_ID.OptionsColumn.AllowEdit = false;
            this.colGC_Site_ID.OptionsColumn.AllowFocus = false;
            this.colGC_Site_ID.OptionsColumn.ReadOnly = true;
            this.colGC_Site_ID.Visible = true;
            this.colGC_Site_ID.VisibleIndex = 1;
            // 
            // colClient_Name
            // 
            this.colClient_Name.Caption = "Client Name";
            this.colClient_Name.FieldName = "Client_Name";
            this.colClient_Name.Name = "colClient_Name";
            this.colClient_Name.OptionsColumn.AllowEdit = false;
            this.colClient_Name.OptionsColumn.AllowFocus = false;
            this.colClient_Name.OptionsColumn.ReadOnly = true;
            this.colClient_Name.Visible = true;
            this.colClient_Name.VisibleIndex = 2;
            this.colClient_Name.Width = 220;
            // 
            // colStore_Name
            // 
            this.colStore_Name.Caption = "Store Name";
            this.colStore_Name.FieldName = "Store_Name";
            this.colStore_Name.Name = "colStore_Name";
            this.colStore_Name.OptionsColumn.AllowEdit = false;
            this.colStore_Name.OptionsColumn.AllowFocus = false;
            this.colStore_Name.OptionsColumn.ReadOnly = true;
            this.colStore_Name.Visible = true;
            this.colStore_Name.VisibleIndex = 3;
            this.colStore_Name.Width = 299;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 4;
            // 
            // colClient_Site_ID
            // 
            this.colClient_Site_ID.Caption = "Client Site ID";
            this.colClient_Site_ID.FieldName = "Client_Site_ID";
            this.colClient_Site_ID.Name = "colClient_Site_ID";
            this.colClient_Site_ID.OptionsColumn.AllowEdit = false;
            this.colClient_Site_ID.OptionsColumn.AllowFocus = false;
            this.colClient_Site_ID.OptionsColumn.ReadOnly = true;
            this.colClient_Site_ID.Visible = true;
            this.colClient_Site_ID.VisibleIndex = 5;
            this.colClient_Site_ID.Width = 83;
            // 
            // colArea1
            // 
            this.colArea1.Caption = "Area";
            this.colArea1.FieldName = "Area";
            this.colArea1.Name = "colArea1";
            this.colArea1.OptionsColumn.AllowEdit = false;
            this.colArea1.OptionsColumn.AllowFocus = false;
            this.colArea1.OptionsColumn.ReadOnly = true;
            this.colArea1.Visible = true;
            this.colArea1.VisibleIndex = 6;
            this.colArea1.Width = 93;
            // 
            // colWeather
            // 
            this.colWeather.Caption = "Weather";
            this.colWeather.FieldName = "Weather";
            this.colWeather.Name = "colWeather";
            this.colWeather.OptionsColumn.AllowEdit = false;
            this.colWeather.OptionsColumn.AllowFocus = false;
            this.colWeather.OptionsColumn.ReadOnly = true;
            this.colWeather.Visible = true;
            this.colWeather.VisibleIndex = 7;
            // 
            // btnImportAuthorisation
            // 
            this.btnImportAuthorisation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportAuthorisation.Location = new System.Drawing.Point(773, 379);
            this.btnImportAuthorisation.Name = "btnImportAuthorisation";
            this.btnImportAuthorisation.Size = new System.Drawing.Size(101, 23);
            this.btnImportAuthorisation.TabIndex = 0;
            this.btnImportAuthorisation.Text = "Import Data";
            this.btnImportAuthorisation.Click += new System.EventHandler(this.btnImportAuthorisation_Click);
            // 
            // sp04053_GC_Gritting_Import_Forecast_Get_Reference_DataTableAdapter
            // 
            this.sp04053_GC_Gritting_Import_Forecast_Get_Reference_DataTableAdapter.ClearBeforeFill = true;
            // 
            // sp04054_GC_Gritting_Import_Forecast_Dummy_DataTableAdapter
            // 
            this.sp04054_GC_Gritting_Import_Forecast_Dummy_DataTableAdapter.ClearBeforeFill = true;
            // 
            // sp04068_GC_Spreadsheet_Authorisation_DummyTableAdapter
            // 
            this.sp04068_GC_Spreadsheet_Authorisation_DummyTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // sp04348_GC_Gritting_Import_Floating_Forecast_DataTableAdapter
            // 
            this.sp04348_GC_Gritting_Import_Floating_Forecast_DataTableAdapter.ClearBeforeFill = true;
            // 
            // sp04349_GC_Gritting_Import_Floating_Forecast_DataTableAdapter
            // 
            this.sp04349_GC_Gritting_Import_Floating_Forecast_DataTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // sp04351_GC_Floating_Site_Jobs_Import_DataTableAdapter
            // 
            this.sp04351_GC_Floating_Site_Jobs_Import_DataTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // frm_GC_Import_Weather_Forecasts
            // 
            this.ClientSize = new System.Drawing.Size(884, 537);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Import_Weather_Forecasts";
            this.Text = "Import Weather Forecasts and Client Client Authorisation";
            this.Load += new System.EventHandler(this.frm_GC_Import_Weather_Forecasts_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEditErrorCount1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04053GCGrittingImportForecastGetReferenceDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04054GCGrittingImportForecastDummyDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEditErrorCount3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04348GCGrittingImportFloatingForecastDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04349GCGrittingImportFloatingForecastDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04351GCFloatingSiteJobsImportDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDegrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KGBags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEditErrorCount4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEditErrorCount2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDeleteUnauthorised.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04068GCSpreadsheetAuthorisationDummyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnImportForecasts;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControlInformation1;
        private DevExpress.XtraEditors.HyperLinkEdit hyperLinkEditErrorCount1;
        private DevExpress.XtraEditors.SimpleButton btnSendConfirmationEmails;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SimpleButton btnMoveRight;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeft;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingAuthorisationRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingActivationCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingActivationCode;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingActivationCodeValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumTemperature;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientProactivePrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientReactivePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colClientChargedForSalt;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSaltPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnMonday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnTuesday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnWednesday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnThursday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnFriday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnSaturday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnSunday;
        private DevExpress.XtraGrid.Columns.GridColumn colPreferredSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colPreferredTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayCount;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultGritAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private System.Windows.Forms.BindingSource sp04053GCGrittingImportForecastGetReferenceDataBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DataSet_GC_CoreTableAdapters.sp04053_GC_Gritting_Import_Forecast_Get_Reference_DataTableAdapter sp04053_GC_Gritting_Import_Forecast_Get_Reference_DataTableAdapter;
        private System.Windows.Forms.BindingSource sp04054GCGrittingImportForecastDummyDataBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04054_GC_Gritting_Import_Forecast_Dummy_DataTableAdapter sp04054_GC_Gritting_Import_Forecast_Dummy_DataTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastActivationPoint;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowForcasted;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastActivationPoint1;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowForcasted1;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastMinimumTemperature;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiredGritAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastMinimumTemperature1;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiredGritAmount1;
        private DevExpress.XtraGrid.Columns.GridColumn colHubID;
        private DevExpress.XtraGrid.Columns.GridColumn colHubID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSkippedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colSkippedReason1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDailyGrittingEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDailyGrittingEmail1;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastForEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastForEmail1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.SimpleButton btnLoadAuthorisation;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControlInformation2;
        private DevExpress.XtraEditors.HyperLinkEdit hyperLinkEditErrorCount2;
        private DevExpress.XtraEditors.SimpleButton btnImportAuthorisation;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.CheckEdit checkEditDeleteUnauthorised;
        private System.Windows.Forms.BindingSource sp04068GCSpreadsheetAuthorisationDummyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGrit_Required;
        private DevExpress.XtraGrid.Columns.GridColumn colGC_Site_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colClient_Name;
        private DevExpress.XtraGrid.Columns.GridColumn colStore_Name;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colClient_Site_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colArea1;
        private DevExpress.XtraGrid.Columns.GridColumn colWeather;
        private DataSet_GC_CoreTableAdapters.sp04068_GC_Spreadsheet_Authorisation_DummyTableAdapter sp04068_GC_Spreadsheet_Authorisation_DummyTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOIDDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOIDDescription1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.SimpleButton btnLoadFloatingForecast;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.LabelControl labelControlInformation3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn114;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn115;
        private DevExpress.XtraEditors.SimpleButton btnMoveRight2;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeft2;
        private DevExpress.XtraEditors.SimpleButton btnLoadFloatingJobs;
        private DevExpress.XtraEditors.SimpleButton btnSendFloatingClientEmails;
        private DevExpress.XtraEditors.LabelControl labelControlInformation4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.SimpleButton btnCreateFloatingJobs;
        private System.Windows.Forms.BindingSource sp04348GCGrittingImportFloatingForecastDataBindingSource;
        private System.Windows.Forms.BindingSource sp04349GCGrittingImportFloatingForecastDataBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04348_GC_Gritting_Import_Floating_Forecast_DataTableAdapter sp04348_GC_Gritting_Import_Floating_Forecast_DataTableAdapter;
        private DataSet_GC_CoreTableAdapters.sp04349_GC_Gritting_Import_Floating_Forecast_DataTableAdapter sp04349_GC_Gritting_Import_Floating_Forecast_DataTableAdapter;
        private DevExpress.XtraEditors.HyperLinkEdit hyperLinkEditErrorCount3;
        private DevExpress.XtraEditors.HyperLinkEdit hyperLinkEditErrorCount4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private System.Windows.Forms.BindingSource sp04351GCFloatingSiteJobsImportDataBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04351_GC_Floating_Site_Jobs_Import_DataTableAdapter sp04351_GC_Floating_Site_Jobs_Import_DataTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn116;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn117;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn118;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn119;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn120;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn121;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn122;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn123;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn124;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn125;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn126;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn127;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn128;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn129;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn130;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn131;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn132;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn133;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn134;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn135;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn136;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn137;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn138;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn139;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn140;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn141;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn142;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn143;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn144;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn145;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn146;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn147;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn148;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn149;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn150;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn151;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn152;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn153;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn154;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn155;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn156;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn157;
        private DevExpress.XtraGrid.Columns.GridColumn colLineID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPostCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colMainWorkCtr;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDegrees;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KGBags;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation2;
        private DevExpress.XtraGrid.Columns.GridColumn colDoubleGrit;
        private DevExpress.XtraGrid.Columns.GridColumn colDoubleGrit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDoubleGrit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDoubleGrit3;
        private DevExpress.XtraGrid.Columns.GridColumn colDoubleGrit4;
    }
}

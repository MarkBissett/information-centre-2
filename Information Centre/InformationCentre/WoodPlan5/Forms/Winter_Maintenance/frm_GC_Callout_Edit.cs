using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraBars;
using DevExpress.Skins;
using System.Data.SqlClient;
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_GC_Callout_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        public string strSiteName = "";
        public string strClientName = "";
        public int intSiteID = 0;
        public int intClientID = 0;
        public int intSiteGrittingContractID = 0;
        public int intSubContractorID = 0;
        public string strSubContractorName = "";
         
        string strImagePath = "";
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Rate Calculation //
        private bool ibool_FormStillLoading = true;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        #endregion

        public frm_GC_Callout_Edit()
        {
            InitializeComponent();
        }

        private void frm_GC_Callout_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 400018;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04085_GC_Gritting_Callout_Status_List_With_Blank, "");
            
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Populate Main Dataset //
            
            sp04076_GC_Gritting_Callout_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            
            // Get Default VAT Rate //
            decimal decVatRate = (decimal)0.00;
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                decVatRate = Convert.ToDecimal(GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate"));
            }
            catch (Exception)
            {
                decVatRate = (decimal)0.00;
            }
 

            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["SiteGrittingContractID"] = intSiteGrittingContractID;
                        drNewRow["SiteName"] = strSiteName;
                        drNewRow["ClientName"] = strClientName;
                        drNewRow["SiteID"] = intSiteID;
                        drNewRow["ClientID"] = intClientID;
                        drNewRow["SubContractorID"] = intSubContractorID;
                        drNewRow["SubContractorName"] = strSubContractorName;
                        drNewRow["OriginalSubContractorID"] = 0;
                        drNewRow["Reactive"] = 1;  // Manually added records [not gererated from a forecast] are always Reactive //
                        drNewRow["JobStatusID"] = 30; // 30 = Started - To Be Completed - Authorised //
                        drNewRow["CallOutDateTime"] = DateTime.Now;
                        drNewRow["RecordedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["RecordedByName"] = (string.IsNullOrEmpty(this.GlobalSettings.UserSurname) ? "" : this.GlobalSettings.UserSurname + ": ") + (string.IsNullOrEmpty(this.GlobalSettings.UserForename) ? "" : this.GlobalSettings.UserForename);
                        drNewRow["SaltUsed"] = (decimal)0.00;
                        drNewRow["SaltCost"] = (decimal)0.00;
                        drNewRow["SaltSell"] = (decimal)0.00;
                        drNewRow["SaltVatRate"] = (decimal)0.00;
                        drNewRow["HoursWorked"] = (decimal)0.00;
                        drNewRow["TeamHourlyRate"] = (decimal)0.00;
                        drNewRow["TeamCharge"] = (decimal)0.00;
                        drNewRow["LabourCost"] = (decimal)0.00;
                        drNewRow["LabourVatRate"] = decVatRate;
                        drNewRow["OtherCost"] = (decimal)0.00;
                        drNewRow["OtherSell"] = (decimal)0.00;
                        drNewRow["TotalCost"] = (decimal)0.00;
                        drNewRow["TotalSell"] = (decimal)0.00;
                        drNewRow["ClientPrice"] = (decimal)0.00;
                        drNewRow["GritSourceLocationID"] = 0;
                        drNewRow["GritSourceLocationTypeID"] = 0;
                        drNewRow["NoAccessAbortedRate"] = (decimal)0.00;
                        drNewRow["NoAccess"] = 0;
                        drNewRow["VisitAborted"] = 0;
                        drNewRow["FinanceTeamPaymentExported"] = 0;
                        drNewRow["SnowCleared"] = 0;
                        drNewRow["SnowClearedSatisfactory"] = 0;
                        drNewRow["SiteLat"] = (double)0.00;
                        drNewRow["SiteLong"] = (double)0.00;
                        drNewRow["IsFloatingSite"] = 0;
                        drNewRow["AttendanceOrder"] = 0;
                        drNewRow["CompletionEmailSent"] = 0;
                        this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.NewRow();
                        drNewRow["strMode"] = "blockadd";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SiteGrittingContractID"] = 0;
                        drNewRow["SiteName"] = "Block Adding - N\\A";
                        drNewRow["ClientName"] = "Block Adding - N\\A";
                        drNewRow["SiteID"] = 0;
                        drNewRow["ClientID"] = 0;
                        drNewRow["SubContractorID"] = 0;
                        drNewRow["SubContractorName"] = 0;
                        drNewRow["OriginalSubContractorID"] = 0;
                        drNewRow["NoAccessAbortedRate"] = (decimal)0.00;
                        drNewRow["NoAccess"] = 0;
                        drNewRow["VisitAborted"] = 0;
                        drNewRow["FinanceTeamPaymentExported"] = 0;
                        drNewRow["SnowCleared"] = 0;
                        drNewRow["SnowClearedSatisfactory"] = 0;
                        drNewRow["SiteLat"] = (double)0.00;
                        drNewRow["SiteLong"] = (double)0.00;
                        drNewRow["IsFloatingSite"] = 0;
                        drNewRow["AttendanceOrder"] = 0;
                        drNewRow["CompletionEmailSent"] = 0;
                        this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.Rows.Add(drNewRow);
                        this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp04076_GC_Gritting_Callout_EditTableAdapter.Fill(this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit, strRecordIDs, strFormMode, strImagePath);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Gritting CalloutDateTimeDateEdit", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            if (strFormMode == "blockedit")
            {
                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                // Get BarButtons Location //
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, 0, 0);
                foreach (BarItemLink link in bar3.ItemLinks)
                {
                    if (link.Item.Name == "barStaticItemInformation")  // Find the Correct button then call function to gets it position using Reflection (as this info is not publicly available //
                    {
                        rect = GetLinksScreenRect(link);
                        break;
                    }
                }
                if (rect.X > 0 && rect.Y > 0)
                {
                    // Draw Locus Effect on object so user can see it //
                    System.Drawing.Point screenPoint = new System.Drawing.Point(rect.X + rect.Width - 10, rect.Y + rect.Height - 10);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                }
            }
        }
        private System.Drawing.Rectangle GetLinksScreenRect(BarItemLink link)
        {
            System.Reflection.PropertyInfo info = typeof(BarItemLink).GetProperty("BarControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            Control c = (Control)info.GetValue(link, null);
            return c.RectangleToScreen(link.Bounds);
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            ibool_FormStillLoading = true;  // Prevent Rate Check firing on HoursWorkedSpinEdit_Validated, ReactiveCheckEdit_Validated, StartTimeDateEdit_Validated, EndTimeDateEdit_Validated event //
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;

            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientPONumberTextEdit.Focus();

                        SiteNameButtonEdit.Properties.ReadOnly = true;
                        SiteNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SiteNameButtonEdit.Properties.Buttons[1].Enabled = true;
                        
                        SubContractorNameButtonEdit.Properties.ReadOnly = true;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        OriginalSubContractorNameButtonEdit.Properties.ReadOnly = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        Weather1CheckEdit.Properties.ReadOnly = false;
                        Weather2CheckEdit.Properties.ReadOnly = false;
                        Weather3CheckEdit.Properties.ReadOnly = false;
                        Weather4CheckEdit.Properties.ReadOnly = false;
                        Weather5CheckEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientPONumberTextEdit.Focus();

                        SiteNameButtonEdit.Properties.ReadOnly = true;
                        SiteNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        SiteNameButtonEdit.Properties.Buttons[1].Enabled = false;

                        SubContractorNameButtonEdit.Properties.ReadOnly = true;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        OriginalSubContractorNameButtonEdit.Properties.ReadOnly = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        Weather1CheckEdit.Properties.ReadOnly = false;
                        Weather2CheckEdit.Properties.ReadOnly = false;
                        Weather3CheckEdit.Properties.ReadOnly = false;
                        Weather4CheckEdit.Properties.ReadOnly = false;
                        Weather5CheckEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientPONumberTextEdit.Focus();

                        SiteNameButtonEdit.Properties.ReadOnly = true;
                        SiteNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SiteNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        SubContractorNameButtonEdit.Properties.ReadOnly = true;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        OriginalSubContractorNameButtonEdit.Properties.ReadOnly = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        Weather1CheckEdit.Properties.ReadOnly = false;
                        Weather2CheckEdit.Properties.ReadOnly = false;
                        Weather3CheckEdit.Properties.ReadOnly = false;
                        Weather4CheckEdit.Properties.ReadOnly = false;
                        Weather5CheckEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientPONumberTextEdit.Focus();

                        SiteNameButtonEdit.Properties.ReadOnly = true;
                        SiteNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        SiteNameButtonEdit.Properties.Buttons[1].Enabled = false;

                        SubContractorNameButtonEdit.Properties.ReadOnly = true;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        OriginalSubContractorNameButtonEdit.Properties.ReadOnly = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        Weather1CheckEdit.Properties.ReadOnly = true;
                        Weather2CheckEdit.Properties.ReadOnly = true;
                        Weather3CheckEdit.Properties.ReadOnly = true;
                        Weather4CheckEdit.Properties.ReadOnly = true;
                        Weather5CheckEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            
            Set_Control_Readonly_Status("");
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_GC_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_GC_Callout_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_GC_Callout_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
            try
            {
                this.sp04076_GC_Gritting_Callout_EditTableAdapter.Update(dataSet_GC_DataEntry);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["GrittingCallOutID"]) + ";";
                    // Switch mode to Edit so than any subsequent changes update this record //
                    this.strFormMode = "edit";
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
                    currentRow["strMode"] = "blockedit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of Parent Manager they will need to refresh their data on activating //
            switch (strCaller)
            {
                case "frm_GC_Callout_Manager":
                    {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                        frm_GC_Callout_Manager fParentForm;
                        foreach (Form frmChild in this.ParentForm.MdiChildren)
                        {
                            if (frmChild.Name == "frm_GC_Callout_Manager")
                            {
                                fParentForm = (frm_GC_Callout_Manager)frmChild;
                                fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "", "");
                            }
                        }
                    }
                    break;
                case "frm_GC_Gritting_Text_Message_Errors":
                    {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                        frm_GC_Gritting_Text_Message_Errors fParentForm;
                        foreach (Form frmChild in this.ParentForm.MdiChildren)
                        {
                            if (frmChild.Name == "frm_GC_Gritting_Text_Message_Errors")
                            {
                                fParentForm = (frm_GC_Gritting_Text_Message_Errors)frmChild;
                                fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                            }
                        }
                    }
                    break;
                case "frm_GC_Finance_Invoice_Jobs":
                    {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                        frm_GC_Finance_Invoice_Jobs fParentForm;
                        foreach (Form frmChild in this.ParentForm.MdiChildren)
                        {
                            if (frmChild.Name == "frm_GC_Finance_Invoice_Jobs")
                            {
                                fParentForm = (frm_GC_Finance_Invoice_Jobs)frmChild;
                                fParentForm.UpdateFormRefreshStatus(2, "", "", "");
                            }
                        }
                    }
                    break;
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Set_Control_Readonly_Status("");
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void SiteNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Site Button //
            {
                frm_GC_Select_Site_Gritting_Contract_Active_Only fChildForm = new frm_GC_Select_Site_Gritting_Contract_Active_Only();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    
                    DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                    if (currentRow != null)
                    {
                        int intOldSiteGrittingContractID = 0;
                        if (!string.IsNullOrEmpty(currentRow["SiteGrittingContractID"].ToString())) intOldSiteGrittingContractID = Convert.ToInt32(currentRow["SiteGrittingContractID"]);
                        currentRow["SiteGrittingContractID"] = fChildForm.intSiteGrittingContractID;
                        currentRow["SiteName"] = fChildForm.strSelectedSite;
                        currentRow["SiteID"] = fChildForm.intSelectedSiteID;
                        currentRow["ClientName"] = fChildForm.strSelectedClient;
                        currentRow["SaltUsed"] = fChildForm.decDefaultGritAmount;
                        currentRow["SaltSell"] = (fChildForm.intClientChargedForSalt == 1 ? fChildForm.decClientSaltPrice : (decimal)0.00);
                        currentRow["SaltVatRate"] = fChildForm.decClientSaltVatRate;
                        currentRow["ClientPrice"] = (Convert.ToInt32(currentRow["Reactive"]) == 1 ? fChildForm.decTeamReactiveRate : fChildForm.decTeamProactiveRate);
                        // Clear Linked Client Purchase Order number as it may not be correct for the current site //
                        currentRow["ClientPOID"] = 0;
                        currentRow["ClientPOIDDescription"] = "";
                        currentRow["EveningRateModifier"] = fChildForm.decEveningRateModifier;
                        currentRow["GrittingEveningStartTime"] = fChildForm.decGrittingEveningStartTime;
                        currentRow["GrittingEveningEndTime"] = fChildForm.decGrittingEveningEndTime;

                        currentRow["SiteAddress1"] = fChildForm.SiteAddressLine1;
                        currentRow["SiteAddress2"] = fChildForm.SiteAddressLine2;
                        currentRow["SiteAddress3"] = fChildForm.SiteAddressLine3;
                        currentRow["SiteAddress4"] = fChildForm.SiteAddressLine4;
                        currentRow["SiteAddress5"] = fChildForm.SiteAddressLine5;
                        currentRow["SitePostcode"] = fChildForm.SitePostcode;
                        currentRow["SiteLat"] = fChildForm.SiteLatitude;
                        currentRow["SiteLong"] = fChildForm.SiteLongitude;
                      
                        // Check if Site gritting contract ID has changes if original value not 0 //
                        if (intOldSiteGrittingContractID != fChildForm.intSiteGrittingContractID && intOldSiteGrittingContractID > 0)
                        {
                            // Ask what the user wants to do next //
                            frm_GC_Callout_SiteGrittingContract_Changed_Next fChild = new frm_GC_Callout_SiteGrittingContract_Changed_Next();
                            fChild.ShowDialog();
                            switch (fChild.strReturnedValue)
                            {
                                case "Nothing":
                                    // Set Non-Standard Cost and Sell flags to true //
                                    currentRow["NonStandardCost"] = 1;
                                    currentRow["NonStandardSell"] = 1;
                                    this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
                                    Calculate_Total_Costs();
                                    break;
                                case "Clear":
                                    // Set Non-Standard Cost and Sell flags to false //
                                    currentRow["NonStandardCost"] = 0;
                                    currentRow["NonStandardSell"] = 0;

                                    currentRow["SubContractorID"] = 0;
                                    currentRow["SubContractorName"] = "-- No Team Selected --";
                                    currentRow["SaltCost"] = (decimal)0.00;
                                    currentRow["TeamHourlyRate"] = (decimal)0.00;
                                    currentRow["TeamCharge"] = (decimal)0.00;
                                    currentRow["GritSourceLocationID"] = (decimal)0.00;
                                    currentRow["GritSourceLocationTypeID"] = (decimal)0.00;
                                    currentRow["LabourCost"] = (decimal)0.00;
                                    currentRow["TotalCost"] = (decimal)0.00;
                                    // Adjust Job Status //
                                    int intJobStatus = 0;
                                    if (!string.IsNullOrEmpty(currentRow["JobStatusID"].ToString())) intJobStatus = Convert.ToInt32(currentRow["JobStatusID"]);
                                    switch (intJobStatus)
                                    {
                                        case 40:  // Completed - Awaiting Authorisation //
                                            currentRow["JobStatusID"] = 20; // Started - To Be Completed - Awaiting Authorisation //
                                            break;
                                        case 50:  // Completed - Ready To Send //
                                            currentRow["JobStatusID"] = 30; // Started - To Be Completed - Authorised //
                                            break;
                                        default:
                                            break;
                                    }
                                    this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
                                    Calculate_Total_Costs();
                                    break;
                                case "Open":
                                    Open_Select_Team_Screen();  // This will also fire Rate change checks if necessary //
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Site Button //
            {
                DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intSiteGrittingContractID = (string.IsNullOrEmpty(currentRow["SiteGrittingContractID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SiteGrittingContractID"]));
                    if (intSiteGrittingContractID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Site Gritting Contract - no Site Gritting Contract has been linked.", "View Linked Site Gritting Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_GC_Site_Gritting_contract_Edit fChildForm = new frm_GC_Site_Gritting_contract_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intSiteGrittingContractID + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void SiteNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(SiteNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SiteNameButtonEdit, "");
            }
        }

        private void SubContractorNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Tree Button //
            {
                Open_Select_Team_Screen();
            }
            else if (e.Button.Tag.ToString() == "view")  // View Contractor Button //
            {
                DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intSiteGrittingContractID = (string.IsNullOrEmpty(currentRow["SubContractorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SubContractorID"]));
                    if (intSiteGrittingContractID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Gritting Team - no Gritting Team has been linked.", "View Linked Gritting Team", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_Core_Contractor_Edit fChildForm = new frm_Core_Contractor_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intSiteGrittingContractID + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void SubContractorNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            /*ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(SubContractorNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SubContractorNameButtonEdit, "");
            }*/
        }

        private void OriginalSubContractorNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                string strRecordIDs = "";
                foreach (DataRow dr in this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.Rows)
                {
                    strRecordIDs += dr["SiteGrittingContractID"].ToString() + ",";
                }
                frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                fChildForm.strFormMode = strFormMode;
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = intRecordCount;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["OriginalSubContractorID"] = fChildForm.intSubContractorID;
                        currentRow["OriginalSubContractorName"] = fChildForm.strSelectedTeamName;
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Button //
            {
                DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intSiteGrittingContractID = (string.IsNullOrEmpty(currentRow["OriginalSubContractorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["OriginalSubContractorID"]));
                    if (intSiteGrittingContractID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Original Gritting Team - no Original Gritting Team has been linked.", "View Linked Original Gritting Team", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_Core_Contractor_Edit fChildForm = new frm_Core_Contractor_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intSiteGrittingContractID + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void ClientPOIDDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                if (currentRow == null) return;
                int intSiteID = (string.IsNullOrEmpty(currentRow["SiteID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SiteID"]));
                int intPOID = (string.IsNullOrEmpty(currentRow["ClientPOID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientPOID"]));
                frm_GC_Select_Client_PO fChildForm = new frm_GC_Select_Client_PO();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intIncludeNonActive = 1;
                fChildForm.intPassedInSiteID = intSiteID;
                fChildForm.intOriginalClientPOID = intPOID;
                fChildForm.strPassedInSubJoin = (strFormMode != "blockedit" ? "IN" : "BLOCKEDIT");
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["ClientPOID"] = fChildForm.intSelectedID;
                    currentRow["ClientPOIDDescription"] = fChildForm.strSelectedValue;
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Button //
            {
                DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intClientPO = (string.IsNullOrEmpty(currentRow["ClientPOID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientPOID"]));
                    if (intClientPO <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Client Purchase Order - no Client Purchase Order has been linked.", "View Linked Client Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_GC_Client_PO_Edit fChildForm = new frm_GC_Client_PO_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intClientPO + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void PDACodeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                if (e.Button.Tag.ToString() == "choose")  // Choose Button //
                {
                    int intTeamID = Convert.ToInt32(currentRow["SubContractorID"]);
                    if (intTeamID == 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select a Gritting Team before proceeding.", "Select Assigned To Device", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    frm_GC_Select_Default_PDA_For_Grit_Jobs fChildForm = new frm_GC_Select_Default_PDA_For_Grit_Jobs();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intPassedInTeamID = intTeamID;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                    {
                        currentRow["PdaID"] = fChildForm.intSelectedPDAID;
                        currentRow["PDACode"] = fChildForm.strSelectedValue;
                    }
                }
                else if (e.Button.Tag.ToString() == "clear")
                {
                    if (XtraMessageBox.Show("You are about to clear the selected Assigned To Device!\n\nAre you sure you wish to proceed?", "Clear Assigned To Device", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        currentRow["PdaID"] = 0;
                        currentRow["PDACode"] = "";
                    }
                }
            }
        }

        private void JobStatusIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("JobStatusID");
        }

        private void JobStatusIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(JobStatusIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(JobStatusIDGridLookUpEdit, "");
            }
        }


        private void HoursWorkedSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void HoursWorkedSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        private void TeamHourlyRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void TeamHourlyRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        private void TeamChargeSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void TeamChargeSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        private void LabourVatRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void LabourVatRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        private void SaltUsedSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SaltUsedSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        private void SaltCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SaltCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        private void SaltSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SaltSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        private void SaltVatRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SaltVatRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        private void StartTimeDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void StartTimeDateEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Time_On_Site();
        }

        private void CompletedTimeDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CompletedTimeDateEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Time_On_Site();
        }

        private void ReactiveCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ReactiveCheckEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                if ((this.strFormMode == "add" || this.strFormMode == "edit"))
                {
                    this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
                    Get_Rate(true);  // Pick up the rest of the related sub-contractor info such as rates and grit source etc //
                }
            }
        }

        private void JobStatusIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                if ((this.strFormMode == "add" || this.strFormMode == "edit"))
                {
                    int intSiteGrittingContractID = 0;
                    if (!string.IsNullOrEmpty(currentRow["SiteGrittingContractID"].ToString())) intSiteGrittingContractID = Convert.ToInt32(currentRow["SiteGrittingContractID"]);

                    int intSubContractorID = 0;
                    if (!string.IsNullOrEmpty(currentRow["SubContractorID"].ToString())) intSubContractorID = Convert.ToInt32(currentRow["SubContractorID"]);

                    if (intSiteGrittingContractID <= 0 || intSubContractorID <= 0)
                    {
                        GridLookUpEdit glue = (GridLookUpEdit)sender;
                        GridView view = glue.Properties.View;
                        view.BeginUpdate();
                        view.ActiveFilter.Clear();
                        view.ActiveFilter.NonColumnFilter = "[CalloutStatusID] <= 30 or [CalloutStatusID] = 60";
                        view.MakeRowVisible(-1, true);
                        view.EndUpdate();
                    }
                }
            }
        }
        private void JobStatusIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                if ((this.strFormMode == "add" || this.strFormMode == "edit"))
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    GridView view = glue.Properties.View;
                    view.BeginUpdate();
                    view.ActiveFilter.Clear();
                    view.EndUpdate();
                }
            }
        }

 
        private void NonStandardCostCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("NonStandardCost");
        }
        private void NonStandardSellCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("NonStandardSell");
        }


        private void VisitAbortedCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("NoAccessAbortedRate");
            if (!ibool_FormStillLoading) Calculate_Total_Costs();

        }

        private void NoAccessCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("NoAccessAbortedRate");
            if (!ibool_FormStillLoading) Calculate_Total_Costs();
        }
        private void NoAccessAbortedRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_FormStillLoading) Calculate_Total_Costs();
        }

        private void SitePostcodeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "lookup")  // Lookup Latitude \ Longitude Button //
            {
                DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    string  strSitePostcode = "";
                    if (!string.IsNullOrEmpty(currentRow["SitePostcode"].ToString())) strSitePostcode = currentRow["SitePostcode"].ToString();
                    frm_GC_Get_Lat_Long_From_Postcode fChildForm = new frm_GC_Get_Lat_Long_From_Postcode();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strPassedInPostcode = strSitePostcode;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                    {
                        currentRow["SiteLat"] = fChildForm.dblSelectedLatitude;
                        currentRow["SiteLong"] = fChildForm.dblSelectedLongitude;
                        this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
                    }
                }
            }
        }

        private void LabourCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void LabourCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        private void ClientPriceSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ClientPriceSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs();
        }

        #endregion


        private void Open_Select_Team_Screen()
        {
            string strRecordIDs = "";
            foreach (DataRow dr in this.dataSet_GC_DataEntry.sp04076_GC_Gritting_Callout_Edit.Rows)
            {
                strRecordIDs += dr["SiteGrittingContractID"].ToString() + ",";
            }
            frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
            fChildForm.strFormMode = strFormMode;
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = intRecordCount;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
            {
                DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    currentRow["SubContractorID"] = fChildForm.intSubContractorID;
                    currentRow["SubContractorName"] = fChildForm.strSelectedTeamName;
                    currentRow["PdaID"] = 0;  // Clear //
                    currentRow["PDACode"] = "";  // Clear //
                    this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
                    
                    Get_Rate(true);  // Pick up the rest of the related sub-contractor info such as rates and grit source etc //
                }
            }
        }

        private void Get_Rate(bool boolRequestAuthorisation)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;  // Not applicable to these modes //
           
            DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                int intSiteGrittingContractID = 0;
                if (!string.IsNullOrEmpty(currentRow["SiteGrittingContractID"].ToString())) intSiteGrittingContractID = Convert.ToInt32(currentRow["SiteGrittingContractID"]);
                if (intSiteGrittingContractID <= 0) return;

                int intSubContractorID = 0;
                if (!string.IsNullOrEmpty(currentRow["SubContractorID"].ToString())) intSubContractorID = Convert.ToInt32(currentRow["SubContractorID"]);
                if (intSubContractorID <= 0) return;

                int intReactive = 0;
                if (!string.IsNullOrEmpty(currentRow["Reactive"].ToString())) intReactive = Convert.ToInt32(currentRow["Reactive"]);

                DevExpress.Utils.WaitDialogForm loading = new DevExpress.Utils.WaitDialogForm("Checking Rates, Please Wait...", "Winter Maintenance");
                loading.Show();

                // Get Rate and other related info //
                SqlDataAdapter sdaRates = new SqlDataAdapter();
                DataSet dsRates = new DataSet("NewDataSet");
                try
                {
                    SqlConnection SQlConn = new SqlConnection(strConnectionString);
                    SqlCommand cmd = null;
                    cmd = new SqlCommand("sp04086_GC_Gritting_Callout_Get_Rates", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@SubContractorID", intSubContractorID));
                    cmd.Parameters.Add(new SqlParameter("@SiteGrittingContractID", intSiteGrittingContractID));
                    cmd.Parameters.Add(new SqlParameter("@Reactive", intReactive));
                    sdaRates = new SqlDataAdapter(cmd);
                    sdaRates.Fill(dsRates, "Table");
                    SQlConn.Close();
                    SQlConn.Dispose();
                    if (dsRates.Tables[0].Rows.Count == 0)
                    {
                        if (loading != null)
                        {
                            loading.Close();
                            loading = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("No Rates Found for Gritting Team!", "Check Rates", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    DataRow dr = dsRates.Tables[0].Rows[0];
                    decimal decSaltCost = Convert.ToDecimal(dr["SaltCost"]);
                    decimal decTeamHourlyRate = Convert.ToDecimal(dr["TeamHourlyRate"]);
                    decimal decTeamCharge = Convert.ToDecimal(dr["TeamCharge"]);
                    int intGritSourceLocationID = Convert.ToInt32(dr["GritSourceLocationID"]);
                    int intGritSourceLocationTypeID = Convert.ToInt32(dr["GritSourceLocationTypeID"]);
                    decimal decClientPrice = Convert.ToDecimal(dr["ClientPrice"]);

                    decimal decCurrentSaltCost = Convert.ToDecimal(currentRow["SaltCost"]);
                    decimal decCurrentTeamHourlyRate = Convert.ToDecimal(currentRow["TeamHourlyRate"]);
                    decimal decCurrentTeamCharge = Convert.ToDecimal(currentRow["TeamCharge"]);
                    int intCurrentGritSourceLocationID = Convert.ToInt32(currentRow["GritSourceLocationID"]);
                    int intCurrentGritSourceLocationTypeID = Convert.ToInt32(currentRow["GritSourceLocationTypeID"]);
                    decimal decCurrentClientPrice = Convert.ToDecimal(currentRow["ClientPrice"]);

                    if (loading != null)
                    {
                        loading.Close();
                        loading = null;
                    }
                    // Check if anything has changed - if yes display message to user allowing them to confirm overwrite of existing values //
                    if ((decSaltCost != decCurrentSaltCost && decCurrentSaltCost != (decimal)0.00) ||
                            (decTeamHourlyRate != decCurrentTeamHourlyRate && decCurrentTeamHourlyRate != (decimal)0.00) ||
                            (decTeamCharge != decCurrentTeamCharge && decCurrentTeamCharge != (decimal)0.00) ||
                            (intGritSourceLocationID != intCurrentGritSourceLocationID && intCurrentGritSourceLocationID != (decimal)0.00) ||
                            (intGritSourceLocationTypeID != intCurrentGritSourceLocationTypeID && intCurrentGritSourceLocationTypeID != (decimal)0.00) ||
                            (decClientPrice != decCurrentClientPrice))
                    {
                        if (boolRequestAuthorisation)
                        {
                            if (DevExpress.XtraEditors.XtraMessageBox.Show("The Rate and\\or other related team and\\or client price information for the currently selected team differs from what is stored in the current gritting callout record.\n\nDo you wish to update the gritting callout record?", "Check Rates", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            {
                                // Set Non-Standard Cost and Sell flags to true //
                                currentRow["NonStandardCost"] = 1;
                                currentRow["NonStandardSell"] = 1;
                                this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
                                return;  // Abort changes //
                            }
                        }
                    }
                    // If we are here, either no authorisation was required or the user said yes to update //
                    currentRow["NonStandardCost"] = 0;  // Clear flag as we are calculating the value //
                    currentRow["NonStandardSell"] = 0;  // Clear flag as we are calculating the value //
                    if (decSaltCost != decCurrentSaltCost) currentRow["SaltCost"] = decSaltCost;
                    if (decTeamHourlyRate != decCurrentTeamHourlyRate) currentRow["TeamHourlyRate"] = decTeamHourlyRate;
                    if (decTeamCharge != decCurrentTeamCharge) currentRow["TeamCharge"] = decTeamCharge;
                    if (intGritSourceLocationID != intCurrentGritSourceLocationID) currentRow["GritSourceLocationID"] = intGritSourceLocationID;
                    if (intGritSourceLocationTypeID != intCurrentGritSourceLocationTypeID) currentRow["GritSourceLocationTypeID"] = intGritSourceLocationTypeID;
                    if (decClientPrice != decCurrentClientPrice) currentRow["ClientPrice"] = decClientPrice;
                    this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
                    
                    Calculate_Total_Costs();
                }
                catch (Exception ex)
                {
                    if (loading != null)
                    {
                        loading.Close();
                        loading = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while checking rates.\n\nMessage = [" + ex.Message + "].\n\nTry clicking the Get Rate button. If the problem persists, contact Technical Support.", "Check Rates", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void Calculate_Total_Costs()
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;  // Not applicable to these modes //
           
            DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                // Calculate Other Costs //
                decimal decOtherCost = (decimal)0.00;
                decimal decOtherSell = (decimal)0.00;
                int intGrittingCallOutID = 0;
                if (!string.IsNullOrEmpty(currentRow["OtherCost"].ToString())) decOtherCost = Convert.ToDecimal(currentRow["OtherCost"]);
                if (!string.IsNullOrEmpty(currentRow["OtherSell"].ToString())) decOtherSell = Convert.ToDecimal(currentRow["OtherSell"]);
                if (!string.IsNullOrEmpty(currentRow["GrittingCallOutID"].ToString())) intGrittingCallOutID = Convert.ToInt32(currentRow["GrittingCallOutID"]);
                //if ((decimal)decOtherCost == (decimal)0.00 && (decimal)decOtherSell == (decimal)0.00 && intGrittingCallOutID > 0)  // both blank and record has been saved [GrittingCallOutID > 0] so attempt to calculate these //
                if (intGrittingCallOutID > 0)  // both blank and record has been saved [GrittingCallOutID > 0] so attempt to calculate these //
                {
                    SqlDataAdapter sdaRates = new SqlDataAdapter();
                    DataSet dsRates = new DataSet("NewDataSet");
                    try
                    {
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp04090_GC_Gritting_Callout_Get_Total_Other_Costs", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@GrittingCallOutID", intGrittingCallOutID));
                        sdaRates = new SqlDataAdapter(cmd);
                        sdaRates.Fill(dsRates, "Table");
                        SQlConn.Close();
                        SQlConn.Dispose();
                        if (dsRates.Tables[0].Rows.Count == 1)
                        {
                            DataRow dr = dsRates.Tables[0].Rows[0];
                            decOtherCost = Convert.ToDecimal(dr["ExtraCostTotal"]);
                            decOtherSell = Convert.ToDecimal(dr["ExtraSellTotal"]);
                            currentRow["OtherCost"] = decOtherCost;
                            currentRow["OtherSell"] = decOtherSell;
                        }
                    }
                    catch (Exception)
                    {
                    }
                }

                // Calculate Labour Cost //
                decimal decTotalLabourCost = (decimal)0.00;
                decimal decTeamCharge = (decimal)0.00;
                decimal decTeamHourlyRate = (decimal)0.00;
                decimal decLabourVatRate = (decimal)0.00;

                if (!string.IsNullOrEmpty(currentRow["TeamCharge"].ToString())) decTeamCharge = Convert.ToDecimal(currentRow["TeamCharge"]);
                if (!string.IsNullOrEmpty(currentRow["TeamHourlyRate"].ToString())) decTeamHourlyRate = Convert.ToDecimal(currentRow["TeamHourlyRate"]);
                if (!string.IsNullOrEmpty(currentRow["LabourVatRate"].ToString())) decLabourVatRate = Convert.ToDecimal(currentRow["LabourVatRate"]);
                if (decTeamCharge != (decimal)0.00)
                {
                    decTotalLabourCost = decTeamCharge; // *(1 + (decLabourVatRate / 100));
                }
                else
                {
                    decimal decHoursWorked = (decimal)0.00;
                    if (!string.IsNullOrEmpty(currentRow["HoursWorked"].ToString())) decHoursWorked = Convert.ToDecimal(currentRow["HoursWorked"]);
                    decTotalLabourCost = (decTeamHourlyRate * decHoursWorked); // *(1 + (decLabourVatRate / 100));
                }

                if (Convert.ToInt32(currentRow["NonStandardCost"]) == 0)
                {
                    // Check if we need to override calculated Labour Cost with Aborted \ No Access Rate \\
                    if (Convert.ToInt32(currentRow["NoAccess"]) == 1 || Convert.ToInt32(currentRow["VisitAborted"]) == 1)
                    {
                        currentRow["LabourCost"] = Convert.ToDecimal(currentRow["NoAccessAbortedRate"]);
                    }
                    else
                    {
                        currentRow["LabourCost"] = decTotalLabourCost;
                    }
                }

                // Calculate Total Sell //
                decimal decClientPrice = (decimal)0.00;
                if (!string.IsNullOrEmpty(currentRow["ClientPrice"].ToString())) decClientPrice = Convert.ToDecimal(currentRow["ClientPrice"]);

                decimal decTotalSell = (decimal)0.00;
                decimal decSaltCost = (decimal)0.00;
                if (!string.IsNullOrEmpty(currentRow["SaltCost"].ToString())) decSaltCost = Convert.ToDecimal(currentRow["SaltCost"]);
                decimal decSaltSell = (decimal)0.00;
                if (!string.IsNullOrEmpty(currentRow["SaltSell"].ToString())) decSaltSell = Convert.ToDecimal(currentRow["SaltSell"]);
                decimal decSaltVatRate = (decimal)0.00;
                if (!string.IsNullOrEmpty(currentRow["SaltVatRate"].ToString())) decSaltVatRate = Convert.ToDecimal(currentRow["SaltVatRate"]);
                decimal decSaltUsed = (decimal)0.00;
                if (!string.IsNullOrEmpty(currentRow["SaltUsed"].ToString())) decSaltUsed = Convert.ToDecimal(currentRow["SaltUsed"]);

                // Apply Rate Modifier if required //
                decimal decUsedRateModifier = (decimal)1.00;
                decimal decEveningRateModifier = (decimal)1.00;
                if (!string.IsNullOrEmpty(currentRow["EveningRateModifier"].ToString())) decEveningRateModifier = Convert.ToDecimal(currentRow["EveningRateModifier"]);

                DateTime dtGrittingEveningStartTime = Convert.ToDateTime("1900-01-01");
                if (!string.IsNullOrEmpty(currentRow["GrittingEveningStartTime"].ToString())) dtGrittingEveningStartTime = Convert.ToDateTime(currentRow["GrittingEveningStartTime"]);
                DateTime dtGrittingEveningEndTime = Convert.ToDateTime("1900-01-01");
                if (!string.IsNullOrEmpty(currentRow["GrittingEveningEndTime"].ToString())) dtGrittingEveningEndTime = Convert.ToDateTime(currentRow["GrittingEveningEndTime"]);

                DateTime dtStartTime = Convert.ToDateTime("1900-01-01");
                if (!string.IsNullOrEmpty(currentRow["StartTime"].ToString())) dtStartTime = DateTime.Parse("1900-01-01 " + Convert.ToDateTime(currentRow["StartTime"]).TimeOfDay.ToString());
                if (dtStartTime.TimeOfDay < dtGrittingEveningEndTime.TimeOfDay && Convert.ToDateTime(dtGrittingEveningStartTime.ToShortDateString()) < Convert.ToDateTime(dtGrittingEveningEndTime.ToShortDateString()))
                {
                    dtStartTime = dtStartTime.AddDays(1);
                    if (dtStartTime >= dtGrittingEveningStartTime && dtStartTime <= dtGrittingEveningEndTime) decUsedRateModifier = decEveningRateModifier;
                }
                else if (dtStartTime >= dtGrittingEveningStartTime && dtStartTime <= dtGrittingEveningEndTime) decUsedRateModifier = decEveningRateModifier;
                {
                    dtStartTime = dtStartTime.AddDays(1);
                    if (dtStartTime >= dtGrittingEveningStartTime && dtStartTime <= dtGrittingEveningEndTime) decUsedRateModifier = decEveningRateModifier;
               }
                // End of Apply Rate Modifier if required //

                decTotalSell = (decClientPrice * decUsedRateModifier) + decOtherSell + (decSaltUsed * decSaltSell); //* (1 + (decSaltVatRate / 100)));
                currentRow["TotalSell"] = decTotalSell;

                // Calculate Total Cost //
                decimal decTotalCost = (decimal)0.00;
                decTotalCost = Convert.ToDecimal(currentRow["LabourCost"]) + decOtherCost + (decSaltUsed * decSaltCost); //* (1 + (decSaltVatRate / 100)));
                currentRow["TotalCost"] = decTotalCost;

                this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
            }
        }

        private void Calculate_Time_On_Site()
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;  // Not applicable to these modes //
           
            DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                decimal decHoursElapsed = (decimal)0.00;
                DateTime dtStart;
                if (currentRow["StartTime"].ToString() == "01/01/0001 00:00:00" || currentRow["StartTime"].ToString() == "")
                {
                    currentRow["HoursWorked"] = decHoursElapsed;
                    this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
                    Calculate_Total_Costs();
                    return;
                }
                dtStart = Convert.ToDateTime(currentRow["StartTime"]);
                DateTime dtEnd;
                if (currentRow["CompletedTime"].ToString() == "01/01/0001 00:00:00" || currentRow["CompletedTime"].ToString() == "")
                {
                    currentRow["HoursWorked"] = decHoursElapsed;
                    this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();
                    Calculate_Total_Costs();
                    return;
                }
                dtEnd = Convert.ToDateTime(currentRow["CompletedTime"]);
                TimeSpan elapsedTime = dtEnd.Subtract(dtStart);
                int hours = (int)Math.Floor(elapsedTime.TotalHours), minutes = elapsedTime.Minutes;
                TimeSpan ts2 = new TimeSpan(hours, minutes, 0);
                decHoursElapsed = (decimal)ts2.TotalMinutes / 60;
                currentRow["HoursWorked"] = decHoursElapsed;
                this.sp04076GCGrittingCalloutEditBindingSource.EndEdit();

                Calculate_Total_Costs();
            }





        }

        private void Set_Control_Readonly_Status(string strCheckWhat)
        {
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "JobStatusID")
            {
                int intJobStatusID = 0;
                if (!string.IsNullOrEmpty(JobStatusIDGridLookUpEdit.EditValue.ToString())) intJobStatusID = Convert.ToInt32(JobStatusIDGridLookUpEdit.EditValue);
                if ((intJobStatusID < 80 || intJobStatusID == 90) && (strFormMode == "add" || strFormMode == "edit"))
                {
                    SiteNameButtonEdit.Properties.Buttons[0].Enabled = true;  // Enable Site Selection //
                    SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;  // Enable Team Selection //
                }
                else
                {
                    SiteNameButtonEdit.Properties.Buttons[0].Enabled = false;  // Disable Site Selection //
                    SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = false;   // Disable Team Selection //
                }
            }
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardCost")
            {
                LabourCostSpinEdit.Properties.ReadOnly = !NonStandardCostCheckEdit.Checked;
            }
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardSell")
            {
                ClientPriceSpinEdit.Properties.ReadOnly = !NonStandardSellCheckEdit.Checked;
            }

            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NoAccessAbortedRate")
            {
                if (string.IsNullOrEmpty(NoAccessCheckEdit.EditValue.ToString()) || string.IsNullOrEmpty(VisitAbortedCheckEdit.EditValue.ToString())) return;
                if (Convert.ToInt32(NoAccessCheckEdit.EditValue) == 1 || Convert.ToInt32(VisitAbortedCheckEdit.EditValue) == 1)
                {
                    NoAccessAbortedRateSpinEdit.Properties.ReadOnly = false;
                }
                else
                {
                    NoAccessAbortedRateSpinEdit.Properties.ReadOnly = true;
                    NoAccessAbortedRateSpinEdit.EditValue = (decimal)0.00;
                }
            }
        }

        private void btnViewOnMap_Click(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
            if (currentRow == null) return;
            double dStartLat = (double)0.00;
            double dStartLong = (double)0.00;
            List<Mapping_Objects> mapObjectsList = new List<Mapping_Objects>();
            dStartLat = (!string.IsNullOrEmpty(currentRow["SiteLat"].ToString()) ? Convert.ToDouble(currentRow["SiteLat"]) : (double)0.00);
            dStartLong = (!string.IsNullOrEmpty(currentRow["SiteLong"].ToString()) ? Convert.ToDouble(currentRow["SiteLong"]) : (double)0.00);
            if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
            {
                Mapping_Objects mapObject = new Mapping_Objects();
                mapObject.doubleLat = dStartLat;
                mapObject.doubleLong = dStartLong;
                mapObject.stringToolTip = "Client: " + (string.IsNullOrEmpty(currentRow["ClientName"].ToString()) ? "" : currentRow["ClientName"].ToString()) + "\nSite: " + (string.IsNullOrEmpty(currentRow["SiteName"].ToString()) ? "" : currentRow["SiteName"].ToString());
                mapObject.stringID = "Site|" + (string.IsNullOrEmpty(currentRow["SiteID"].ToString()) ? "0" : currentRow["SiteID"].ToString());
                mapObjectsList.Add(mapObject);
            }

            if (mapObjectsList.Count == 0)  // No objects added due to missing coordinates //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter the Latitude and Longitude for the site before proceeding.\n\nTip: If you are unsure of the Latitude and Longitude, look them up via the Lookup button.", "View Site on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                Mapping_Functions mapFunctions = new Mapping_Functions();
                mapFunctions.View_Object_In_Internet_Mapping(this.GlobalSettings, this.strConnectionString, this, mapObjectsList);
            }
        }

        private void btnShowOnMap_Click(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp04076GCGrittingCalloutEditBindingSource.Current;
            if (currentRow == null) return;
            double dStartLat = (double)0.00;
            double dStartLong = (double)0.00;
            List<Mapping_Objects> mapObjectsList = new List<Mapping_Objects>();
            dStartLat = (!string.IsNullOrEmpty(currentRow["StartLatitude"].ToString()) ? Convert.ToDouble(currentRow["StartLatitude"]) : (double)0.00);
            dStartLong = (!string.IsNullOrEmpty(currentRow["StartLongitude"].ToString()) ? Convert.ToDouble(currentRow["StartLongitude"]) : (double)0.00);
            if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
            {
                Mapping_Objects mapObject = new Mapping_Objects();
                mapObject.doubleLat = dStartLat;
                mapObject.doubleLong = dStartLong;
                mapObject.stringToolTip = "Job Start";
                mapObject.stringID = "Start";
                mapObjectsList.Add(mapObject);
            }

            dStartLat = (!string.IsNullOrEmpty(currentRow["FinishedLatitude"].ToString()) ? Convert.ToDouble(currentRow["FinishedLatitude"]) : (double)0.00);
            dStartLong = (!string.IsNullOrEmpty(currentRow["FinishedLongitude"].ToString()) ? Convert.ToDouble(currentRow["FinishedLongitude"]) : (double)0.00);
            if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
            {
                Mapping_Objects mapObject = new Mapping_Objects();
                mapObject.doubleLat = dStartLat;
                mapObject.doubleLong = dStartLong;
                mapObject.stringToolTip = "Job Finish";
                mapObject.stringID = "Finish";
                mapObjectsList.Add(mapObject);
            }

            if (mapObjectsList.Count == 0)  // No objects added due to missing coordinates //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Start and\\or Finish Latitude and Longitude entered - unable to view on map.", "View Start and Finish on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                Mapping_Functions mapFunctions = new Mapping_Functions();
                mapFunctions.View_Object_In_Internet_Mapping(this.GlobalSettings, this.strConnectionString, this, mapObjectsList);
            }

        }



 
   
    }
}


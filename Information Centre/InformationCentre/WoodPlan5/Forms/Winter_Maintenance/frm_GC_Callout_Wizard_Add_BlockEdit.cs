﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Callout_Wizard_Add_BlockEdit : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int? intReactive = null;
        public string strClientPONumber = null;
        public int? intNonStandardCost = null;
        public int? intNonStandardSell = null;
        public decimal? decSaltUsed = null;
        public int? intServiceFailure = null;
        public int? intSnowOnSite = null;

        #endregion

        public frm_GC_Callout_Wizard_Add_BlockEdit()
        {
            InitializeComponent();
        }

        private void frm_GC_Callout_Wizard_Add_BlockEdit_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400046;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            SaltUsedSpinEdit.EditValue = null;
            ReactiveCheckEdit.EditValue = null;
            ClientPONumberTextEdit.EditValue = null;
            NonStandardCostCheckEdit.EditValue = null;
            NonStandardSellCheckEdit.EditValue = null;
            ServiceFailureCheckEdit.EditValue = null;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SaltUsedSpinEdit.EditValue != null) decSaltUsed = Convert.ToDecimal(SaltUsedSpinEdit.EditValue);
            if (ReactiveCheckEdit.EditValue != null) intReactive = Convert.ToInt32(ReactiveCheckEdit.EditValue);
            if (ClientPONumberTextEdit.EditValue != null) strClientPONumber = ClientPONumberTextEdit.EditValue.ToString();
            if (NonStandardCostCheckEdit.EditValue != null) intNonStandardCost = Convert.ToInt32(NonStandardCostCheckEdit.EditValue);
            if (NonStandardSellCheckEdit.EditValue != null) intNonStandardSell = Convert.ToInt32(NonStandardSellCheckEdit.EditValue);
            if (ServiceFailureCheckEdit.EditValue != null) intServiceFailure = Convert.ToInt32(ServiceFailureCheckEdit.EditValue);
            if (SnowOnSiteCheckEdit.EditValue != null) intSnowOnSite = Convert.ToInt32(SnowOnSiteCheckEdit.EditValue);
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }






    }
}

namespace WoodPlan5
{
    partial class frm_GC_Snow_Callout_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Snow_Callout_Manager));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip25 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem25 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem25 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip26 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem26 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem26 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip29 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem29 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem29 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip30 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem30 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem30 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip31 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem31 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem31 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip32 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem32 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem32 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip23 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem23 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem23 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip27 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem27 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem27 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip28 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem28 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem28 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip24 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem24 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem24 = new DevExpress.Utils.ToolTipItem();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEditTeamPOFile = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04160GCSnowCalloutManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Snow_Core = new WoodPlan5.DataSet_GC_Snow_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSnowClearanceCallOutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCPONumberSuffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritMobileTelephoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorETA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitAborted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbortedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorContactedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractorReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClientReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarkup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientHowSoon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoursWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccessAbortedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamPOFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimesheetSubmitted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedBySubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedBySubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimesheetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceLineID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceTeamPaymentExported = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedOnPDA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.popupContainerControlLinkedGrittingCalloutsFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnGritCalloutFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04001GCJobCallOutStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp04161GCSnowClearanceCalloutLinkedExtraCostsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colExtraCostID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedFromDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCostTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colChargedPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSnowClearanceCallOutID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamAddressLine11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDayTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSnowClearanceCallOutID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceCallOutRateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceSiteContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMachineCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInt = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMachineCount2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineCount3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineCount4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineRate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineRate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineRate4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHours2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp04340GCServiceAreasLinkedToSnowCalloutBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteServiceAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWasServiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCreatedFromDefault1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceCalloutID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceServicedSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPictureID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingCallOutID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPicturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDateTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteGrittingContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl15 = new DevExpress.XtraGrid.GridControl();
            this.sp05089CRMContactsLinkedToRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCRMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDueDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContactMadeDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.VisitIDsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.checkEditColourCode = new DevExpress.XtraEditors.CheckEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroupFilterType = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupFilterCriteria = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiAddJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSend = new DevExpress.XtraBars.BarSubItem();
            this.bsiSetReadyToSend1 = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectSetReadyToSend = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetReadyToSend2 = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSendJobs = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectJobsReadyToSend = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendJobs2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReassignJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAuthorise = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPay = new DevExpress.XtraBars.BarButtonItem();
            this.beiLoadingProgress = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemMarqueeProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.bbiLoadingCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPreviewTeamPO = new DevExpress.XtraBars.BarButtonItem();
            this.pmEditPOLayout = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiEditTeamPOLayout = new DevExpress.XtraBars.BarButtonItem();
            this.bsiViewOnMap = new DevExpress.XtraBars.BarSubItem();
            this.bbiViewSiteOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewJobOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bsiFilterSelected = new DevExpress.XtraBars.BarSubItem();
            this.bciFilterVisitsSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount = new DevExpress.XtraBars.BarStaticItem();
            this.bbiSetReadyToSend1 = new DevExpress.XtraBars.BarButtonItem();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.sp04160_GC_Snow_Callout_ManagerTableAdapter = new WoodPlan5.DataSet_GC_Snow_CoreTableAdapters.sp04160_GC_Snow_Callout_ManagerTableAdapter();
            this.sp04161_GC_Snow_Clearance_Callout_Linked_Extra_CostsTableAdapter = new WoodPlan5.DataSet_GC_Snow_CoreTableAdapters.sp04161_GC_Snow_Clearance_Callout_Linked_Extra_CostsTableAdapter();
            this.sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter = new WoodPlan5.DataSet_GC_Snow_CoreTableAdapters.sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter();
            this.sp04340_GC_Service_Areas_Linked_To_Snow_CalloutTableAdapter = new WoodPlan5.DataSet_GC_Snow_CoreTableAdapters.sp04340_GC_Service_Areas_Linked_To_Snow_CalloutTableAdapter();
            this.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.btnFormatData = new DevExpress.XtraEditors.SimpleButton();
            this.ItemforformatBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditTeamPOFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04160GCSnowCalloutManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).BeginInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04161GCSnowClearanceCalloutLinkedExtraCostsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP3)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04340GCServiceAreasLinkedToSnowCalloutBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05089CRMContactsLinkedToRecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditColourCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilterType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFilterCriteria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditPOLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforformatBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1313, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(1313, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1313, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiReassignJobs,
            this.bbiViewSiteOnMap,
            this.bbiPay,
            this.bsiViewOnMap,
            this.bbiViewJobOnMap,
            this.bbiAddJobs,
            this.bbiSetReadyToSend1,
            this.bbiSelectSetReadyToSend,
            this.bbiSetReadyToSend2,
            this.bbiSelectJobsReadyToSend,
            this.bbiSendJobs2,
            this.bbiAuthorise,
            this.bbiPreviewTeamPO,
            this.bbiEditTeamPOLayout,
            this.bsiSend,
            this.bsiSendJobs,
            this.bsiSetReadyToSend1,
            this.bsiSelectedCount,
            this.bbiRefresh,
            this.bbiLoadingCancel,
            this.beiLoadingProgress,
            this.bsiFilterSelected,
            this.bciFilterVisitsSelected});
            this.barManager1.MaxItemId = 61;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMarqueeProgressBar1});
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // repositoryItemHyperLinkEditTeamPOFile
            // 
            this.repositoryItemHyperLinkEditTeamPOFile.AutoHeight = false;
            this.repositoryItemHyperLinkEditTeamPOFile.Name = "repositoryItemHyperLinkEditTeamPOFile";
            this.repositoryItemHyperLinkEditTeamPOFile.SingleClick = true;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl1.BackgroundImage")));
            this.gridControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gridControl1.DataSource = this.sp04160GCSnowCalloutManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Selected Records Only", "reload_selected")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1286, 322);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04160GCSnowCalloutManagerBindingSource
            // 
            this.sp04160GCSnowCalloutManagerBindingSource.DataMember = "sp04160_GC_Snow_Callout_Manager";
            this.sp04160GCSnowCalloutManagerBindingSource.DataSource = this.dataSet_GC_Snow_Core;
            // 
            // dataSet_GC_Snow_Core
            // 
            this.dataSet_GC_Snow_Core.DataSetName = "DataSet_GC_Snow_Core";
            this.dataSet_GC_Snow_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.InsertGalleryImage("convert_16x16.png", "images/actions/convert_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/convert_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "convert_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSnowClearanceCallOutID,
            this.colSnowClearanceSiteContractID,
            this.colGCPONumberSuffix,
            this.colSiteID,
            this.colSiteName,
            this.colClientID,
            this.colClientName,
            this.colCompanyID,
            this.colCompanyName,
            this.colSiteXCoordinate,
            this.colSiteYCoordinate,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colSiteTelephone,
            this.colSiteEmail,
            this.colClientsSiteCode,
            this.colSubContractorID,
            this.colSubContractorName,
            this.colGritMobileTelephoneNumber,
            this.colOriginalSubContractorID,
            this.colOriginalSubContractorName,
            this.colReactive,
            this.colJobStatusID,
            this.colCalloutStatusDescription,
            this.colCalloutStatusOrder,
            this.colCallOutDateTime,
            this.colSubContractorETA,
            this.colCompletedTime,
            this.colRecordedByStaffID,
            this.colVisitAborted,
            this.colAbortedReason,
            this.colClientPONumber,
            this.colLabourCost,
            this.colLabourVatRate,
            this.colLabourSell,
            this.colOtherCost,
            this.colOtherSell,
            this.colTotalCost,
            this.colTotalSell,
            this.colClientInvoiceNumber,
            this.colSubContractorPaid,
            this.colRecordedByName,
            this.colSubContractorContactedByStaffID,
            this.colSubContractorContactedByStaffName,
            this.colPaidByStaffID,
            this.colPaidByName,
            this.colDoNotPaySubContractor,
            this.colDoNotPaySubContractorReason,
            this.colDoNotInvoiceClient,
            this.colDoNotInvoiceClientReason,
            this.colNoAccess,
            this.colStartTime,
            this.colProfit,
            this.colMarkup,
            this.colClientPOID,
            this.colNonStandardCost,
            this.colNonStandardSell,
            this.colAccessComments,
            this.colClientHowSoon,
            this.colHoursWorked,
            this.colRemarks2,
            this.colNoAccessAbortedRate,
            this.colTeamInvoiceNumber,
            this.colTeamPOFileName,
            this.colTimesheetSubmitted,
            this.colRecordedBySubContractorID,
            this.colRecordedBySubContractorName,
            this.colTimesheetNumber,
            this.colAddedByWebsite,
            this.colClientInvoiceLineID,
            this.colFinanceInvoiceNumber,
            this.colFinanceTeamPaymentExported,
            this.colSiteManagerName,
            this.colInternalRemarks,
            this.colAddedOnPDA,
            this.colSiteCode,
            this.colSelected});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCallOutDateTime, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSnowClearanceCallOutID
            // 
            this.colSnowClearanceCallOutID.Caption = "GC PO No";
            this.colSnowClearanceCallOutID.FieldName = "SnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.Name = "colSnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutID.Visible = true;
            this.colSnowClearanceCallOutID.VisibleIndex = 2;
            this.colSnowClearanceCallOutID.Width = 71;
            // 
            // colSnowClearanceSiteContractID
            // 
            this.colSnowClearanceSiteContractID.Caption = "Snow Clearance Contract ID";
            this.colSnowClearanceSiteContractID.FieldName = "SnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.Name = "colSnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSiteContractID.Width = 157;
            // 
            // colGCPONumberSuffix
            // 
            this.colGCPONumberSuffix.Caption = "GC PO Suffix";
            this.colGCPONumberSuffix.FieldName = "GCPONumberSuffix";
            this.colGCPONumberSuffix.Name = "colGCPONumberSuffix";
            this.colGCPONumberSuffix.OptionsColumn.AllowEdit = false;
            this.colGCPONumberSuffix.OptionsColumn.AllowFocus = false;
            this.colGCPONumberSuffix.OptionsColumn.ReadOnly = true;
            this.colGCPONumberSuffix.Visible = true;
            this.colGCPONumberSuffix.VisibleIndex = 3;
            this.colGCPONumberSuffix.Width = 84;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 10;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 9;
            this.colSiteName.Width = 157;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 8;
            this.colClientName.Width = 120;
            // 
            // colCompanyID
            // 
            this.colCompanyID.Caption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 80;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 20;
            this.colCompanyName.Width = 137;
            // 
            // colSiteXCoordinate
            // 
            this.colSiteXCoordinate.Caption = "Site X Coordinate";
            this.colSiteXCoordinate.FieldName = "SiteXCoordinate";
            this.colSiteXCoordinate.Name = "colSiteXCoordinate";
            this.colSiteXCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteXCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteXCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteXCoordinate.Width = 104;
            // 
            // colSiteYCoordinate
            // 
            this.colSiteYCoordinate.Caption = "Site Y Coordinate";
            this.colSiteYCoordinate.FieldName = "SiteYCoordinate";
            this.colSiteYCoordinate.Name = "colSiteYCoordinate";
            this.colSiteYCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteYCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteYCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteYCoordinate.Width = 104;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Location X";
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 91;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Location Y";
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 91;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Site Telephone ";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Width = 95;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Site Email";
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.AllowEdit = false;
            this.colSiteEmail.OptionsColumn.AllowFocus = false;
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            this.colSiteEmail.Visible = true;
            this.colSiteEmail.VisibleIndex = 19;
            this.colSiteEmail.Width = 116;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Width = 105;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Team Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 5;
            this.colSubContractorName.Width = 122;
            // 
            // colGritMobileTelephoneNumber
            // 
            this.colGritMobileTelephoneNumber.Caption = "Team Mobile No";
            this.colGritMobileTelephoneNumber.FieldName = "GritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.Name = "colGritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.OptionsColumn.AllowEdit = false;
            this.colGritMobileTelephoneNumber.OptionsColumn.AllowFocus = false;
            this.colGritMobileTelephoneNumber.OptionsColumn.ReadOnly = true;
            this.colGritMobileTelephoneNumber.Width = 96;
            // 
            // colOriginalSubContractorID
            // 
            this.colOriginalSubContractorID.Caption = "Original Team ID";
            this.colOriginalSubContractorID.FieldName = "OriginalSubContractorID";
            this.colOriginalSubContractorID.Name = "colOriginalSubContractorID";
            this.colOriginalSubContractorID.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorID.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorID.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorID.Width = 100;
            // 
            // colOriginalSubContractorName
            // 
            this.colOriginalSubContractorName.Caption = "Original Team Name";
            this.colOriginalSubContractorName.FieldName = "OriginalSubContractorName";
            this.colOriginalSubContractorName.Name = "colOriginalSubContractorName";
            this.colOriginalSubContractorName.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorName.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorName.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorName.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 4;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colCalloutStatusDescription
            // 
            this.colCalloutStatusDescription.Caption = "Status Description";
            this.colCalloutStatusDescription.FieldName = "CalloutStatusDescription";
            this.colCalloutStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCalloutStatusDescription.Name = "colCalloutStatusDescription";
            this.colCalloutStatusDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusDescription.Visible = true;
            this.colCalloutStatusDescription.VisibleIndex = 0;
            this.colCalloutStatusDescription.Width = 196;
            // 
            // colCalloutStatusOrder
            // 
            this.colCalloutStatusOrder.Caption = "Status Order";
            this.colCalloutStatusOrder.FieldName = "CalloutStatusOrder";
            this.colCalloutStatusOrder.Name = "colCalloutStatusOrder";
            this.colCalloutStatusOrder.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusOrder.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusOrder.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusOrder.Width = 83;
            // 
            // colCallOutDateTime
            // 
            this.colCallOutDateTime.Caption = "Callout Date\\Time";
            this.colCallOutDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCallOutDateTime.FieldName = "CallOutDateTime";
            this.colCallOutDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCallOutDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCallOutDateTime.Name = "colCallOutDateTime";
            this.colCallOutDateTime.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCallOutDateTime.Visible = true;
            this.colCallOutDateTime.VisibleIndex = 1;
            this.colCallOutDateTime.Width = 121;
            // 
            // colSubContractorETA
            // 
            this.colSubContractorETA.Caption = "Team ETA";
            this.colSubContractorETA.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSubContractorETA.FieldName = "SubContractorETA";
            this.colSubContractorETA.Name = "colSubContractorETA";
            this.colSubContractorETA.OptionsColumn.AllowEdit = false;
            this.colSubContractorETA.OptionsColumn.AllowFocus = false;
            this.colSubContractorETA.OptionsColumn.ReadOnly = true;
            this.colSubContractorETA.Visible = true;
            this.colSubContractorETA.VisibleIndex = 6;
            this.colSubContractorETA.Width = 117;
            // 
            // colCompletedTime
            // 
            this.colCompletedTime.Caption = "Completed Date\\Time";
            this.colCompletedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCompletedTime.FieldName = "CompletedTime";
            this.colCompletedTime.Name = "colCompletedTime";
            this.colCompletedTime.OptionsColumn.AllowEdit = false;
            this.colCompletedTime.OptionsColumn.AllowFocus = false;
            this.colCompletedTime.OptionsColumn.ReadOnly = true;
            this.colCompletedTime.Visible = true;
            this.colCompletedTime.VisibleIndex = 17;
            this.colCompletedTime.Width = 124;
            // 
            // colRecordedByStaffID
            // 
            this.colRecordedByStaffID.Caption = "Recorded By Staff ID";
            this.colRecordedByStaffID.FieldName = "RecordedByStaffID";
            this.colRecordedByStaffID.Name = "colRecordedByStaffID";
            this.colRecordedByStaffID.OptionsColumn.AllowEdit = false;
            this.colRecordedByStaffID.OptionsColumn.AllowFocus = false;
            this.colRecordedByStaffID.OptionsColumn.ReadOnly = true;
            this.colRecordedByStaffID.Width = 123;
            // 
            // colVisitAborted
            // 
            this.colVisitAborted.Caption = "Aborted";
            this.colVisitAborted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colVisitAborted.FieldName = "VisitAborted";
            this.colVisitAborted.Name = "colVisitAborted";
            this.colVisitAborted.OptionsColumn.AllowEdit = false;
            this.colVisitAborted.OptionsColumn.AllowFocus = false;
            this.colVisitAborted.OptionsColumn.ReadOnly = true;
            this.colVisitAborted.Visible = true;
            this.colVisitAborted.VisibleIndex = 14;
            // 
            // colAbortedReason
            // 
            this.colAbortedReason.Caption = "Aborted Reason";
            this.colAbortedReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colAbortedReason.FieldName = "AbortedReason";
            this.colAbortedReason.Name = "colAbortedReason";
            this.colAbortedReason.OptionsColumn.ReadOnly = true;
            this.colAbortedReason.Visible = true;
            this.colAbortedReason.VisibleIndex = 15;
            this.colAbortedReason.Width = 99;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO Number";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 12;
            this.colClientPONumber.Width = 105;
            // 
            // colLabourCost
            // 
            this.colLabourCost.Caption = "Labour Cost";
            this.colLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colLabourCost.FieldName = "LabourCost";
            this.colLabourCost.Name = "colLabourCost";
            this.colLabourCost.OptionsColumn.AllowEdit = false;
            this.colLabourCost.OptionsColumn.AllowFocus = false;
            this.colLabourCost.OptionsColumn.ReadOnly = true;
            this.colLabourCost.Visible = true;
            this.colLabourCost.VisibleIndex = 22;
            this.colLabourCost.Width = 79;
            // 
            // colLabourVatRate
            // 
            this.colLabourVatRate.Caption = "Labout VAT Rate";
            this.colLabourVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colLabourVatRate.FieldName = "LabourVatRate";
            this.colLabourVatRate.Name = "colLabourVatRate";
            this.colLabourVatRate.OptionsColumn.AllowEdit = false;
            this.colLabourVatRate.OptionsColumn.AllowFocus = false;
            this.colLabourVatRate.OptionsColumn.ReadOnly = true;
            this.colLabourVatRate.Visible = true;
            this.colLabourVatRate.VisibleIndex = 23;
            this.colLabourVatRate.Width = 102;
            // 
            // colLabourSell
            // 
            this.colLabourSell.Caption = "Labour Sell";
            this.colLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colLabourSell.FieldName = "LabourSell";
            this.colLabourSell.Name = "colLabourSell";
            this.colLabourSell.OptionsColumn.AllowEdit = false;
            this.colLabourSell.OptionsColumn.AllowFocus = false;
            this.colLabourSell.OptionsColumn.ReadOnly = true;
            this.colLabourSell.Visible = true;
            this.colLabourSell.VisibleIndex = 24;
            // 
            // colOtherCost
            // 
            this.colOtherCost.Caption = "Other Cost";
            this.colOtherCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherCost.FieldName = "OtherCost";
            this.colOtherCost.Name = "colOtherCost";
            this.colOtherCost.OptionsColumn.AllowEdit = false;
            this.colOtherCost.OptionsColumn.AllowFocus = false;
            this.colOtherCost.OptionsColumn.ReadOnly = true;
            this.colOtherCost.Visible = true;
            this.colOtherCost.VisibleIndex = 25;
            // 
            // colOtherSell
            // 
            this.colOtherSell.Caption = "Other Sell";
            this.colOtherSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherSell.FieldName = "OtherSell";
            this.colOtherSell.Name = "colOtherSell";
            this.colOtherSell.OptionsColumn.AllowEdit = false;
            this.colOtherSell.OptionsColumn.AllowFocus = false;
            this.colOtherSell.OptionsColumn.ReadOnly = true;
            this.colOtherSell.Visible = true;
            this.colOtherSell.VisibleIndex = 26;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 27;
            // 
            // colTotalSell
            // 
            this.colTotalSell.Caption = "Total Sell ";
            this.colTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalSell.FieldName = "TotalSell";
            this.colTotalSell.Name = "colTotalSell";
            this.colTotalSell.OptionsColumn.AllowEdit = false;
            this.colTotalSell.OptionsColumn.AllowFocus = false;
            this.colTotalSell.OptionsColumn.ReadOnly = true;
            this.colTotalSell.Visible = true;
            this.colTotalSell.VisibleIndex = 28;
            // 
            // colClientInvoiceNumber
            // 
            this.colClientInvoiceNumber.Caption = "Client Invoice Number";
            this.colClientInvoiceNumber.FieldName = "ClientInvoiceNumber";
            this.colClientInvoiceNumber.Name = "colClientInvoiceNumber";
            this.colClientInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceNumber.Visible = true;
            this.colClientInvoiceNumber.VisibleIndex = 33;
            this.colClientInvoiceNumber.Width = 126;
            // 
            // colSubContractorPaid
            // 
            this.colSubContractorPaid.Caption = "Team Paid";
            this.colSubContractorPaid.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSubContractorPaid.FieldName = "SubContractorPaid";
            this.colSubContractorPaid.Name = "colSubContractorPaid";
            this.colSubContractorPaid.OptionsColumn.AllowEdit = false;
            this.colSubContractorPaid.OptionsColumn.AllowFocus = false;
            this.colSubContractorPaid.OptionsColumn.ReadOnly = true;
            this.colSubContractorPaid.Visible = true;
            this.colSubContractorPaid.VisibleIndex = 35;
            // 
            // colRecordedByName
            // 
            this.colRecordedByName.Caption = "Recorded By Staff";
            this.colRecordedByName.FieldName = "RecordedByName";
            this.colRecordedByName.Name = "colRecordedByName";
            this.colRecordedByName.OptionsColumn.AllowEdit = false;
            this.colRecordedByName.OptionsColumn.AllowFocus = false;
            this.colRecordedByName.OptionsColumn.ReadOnly = true;
            this.colRecordedByName.Visible = true;
            this.colRecordedByName.VisibleIndex = 36;
            this.colRecordedByName.Width = 112;
            // 
            // colSubContractorContactedByStaffID
            // 
            this.colSubContractorContactedByStaffID.Caption = "Team Contacted By Staff ID";
            this.colSubContractorContactedByStaffID.FieldName = "SubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.Name = "colSubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.OptionsColumn.AllowEdit = false;
            this.colSubContractorContactedByStaffID.OptionsColumn.AllowFocus = false;
            this.colSubContractorContactedByStaffID.OptionsColumn.ReadOnly = true;
            this.colSubContractorContactedByStaffID.Width = 156;
            // 
            // colSubContractorContactedByStaffName
            // 
            this.colSubContractorContactedByStaffName.Caption = "Team Contacted By Staff Name";
            this.colSubContractorContactedByStaffName.FieldName = "SubContractorContactedByStaffName";
            this.colSubContractorContactedByStaffName.Name = "colSubContractorContactedByStaffName";
            this.colSubContractorContactedByStaffName.OptionsColumn.AllowEdit = false;
            this.colSubContractorContactedByStaffName.OptionsColumn.AllowFocus = false;
            this.colSubContractorContactedByStaffName.OptionsColumn.ReadOnly = true;
            this.colSubContractorContactedByStaffName.Visible = true;
            this.colSubContractorContactedByStaffName.VisibleIndex = 38;
            this.colSubContractorContactedByStaffName.Width = 172;
            // 
            // colPaidByStaffID
            // 
            this.colPaidByStaffID.Caption = "Paid By Staff ID";
            this.colPaidByStaffID.FieldName = "PaidByStaffID";
            this.colPaidByStaffID.Name = "colPaidByStaffID";
            this.colPaidByStaffID.OptionsColumn.AllowEdit = false;
            this.colPaidByStaffID.OptionsColumn.AllowFocus = false;
            this.colPaidByStaffID.OptionsColumn.ReadOnly = true;
            this.colPaidByStaffID.Width = 97;
            // 
            // colPaidByName
            // 
            this.colPaidByName.Caption = "Paid By Staff Name";
            this.colPaidByName.FieldName = "PaidByName";
            this.colPaidByName.Name = "colPaidByName";
            this.colPaidByName.OptionsColumn.AllowEdit = false;
            this.colPaidByName.OptionsColumn.AllowFocus = false;
            this.colPaidByName.OptionsColumn.ReadOnly = true;
            this.colPaidByName.Visible = true;
            this.colPaidByName.VisibleIndex = 39;
            this.colPaidByName.Width = 113;
            // 
            // colDoNotPaySubContractor
            // 
            this.colDoNotPaySubContractor.Caption = "Do Not Pay Team";
            this.colDoNotPaySubContractor.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPaySubContractor.FieldName = "DoNotPaySubContractor";
            this.colDoNotPaySubContractor.Name = "colDoNotPaySubContractor";
            this.colDoNotPaySubContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPaySubContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPaySubContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractor.Visible = true;
            this.colDoNotPaySubContractor.VisibleIndex = 40;
            this.colDoNotPaySubContractor.Width = 104;
            // 
            // colDoNotPaySubContractorReason
            // 
            this.colDoNotPaySubContractorReason.Caption = "Do Not Pay Team Reason";
            this.colDoNotPaySubContractorReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDoNotPaySubContractorReason.FieldName = "DoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.Name = "colDoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractorReason.Visible = true;
            this.colDoNotPaySubContractorReason.VisibleIndex = 41;
            this.colDoNotPaySubContractorReason.Width = 143;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 42;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colDoNotInvoiceClientReason
            // 
            this.colDoNotInvoiceClientReason.Caption = "Do Not Invoice Client Reason";
            this.colDoNotInvoiceClientReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDoNotInvoiceClientReason.FieldName = "DoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.Name = "colDoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClientReason.Visible = true;
            this.colDoNotInvoiceClientReason.VisibleIndex = 43;
            this.colDoNotInvoiceClientReason.Width = 161;
            // 
            // colNoAccess
            // 
            this.colNoAccess.Caption = "No Access";
            this.colNoAccess.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoAccess.FieldName = "NoAccess";
            this.colNoAccess.Name = "colNoAccess";
            this.colNoAccess.OptionsColumn.AllowEdit = false;
            this.colNoAccess.OptionsColumn.AllowFocus = false;
            this.colNoAccess.OptionsColumn.ReadOnly = true;
            this.colNoAccess.Visible = true;
            this.colNoAccess.VisibleIndex = 13;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Date\\Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 16;
            this.colStartTime.Width = 105;
            // 
            // colProfit
            // 
            this.colProfit.Caption = "Profit";
            this.colProfit.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colProfit.FieldName = "Profit";
            this.colProfit.Name = "colProfit";
            this.colProfit.OptionsColumn.AllowEdit = false;
            this.colProfit.OptionsColumn.AllowFocus = false;
            this.colProfit.OptionsColumn.ReadOnly = true;
            this.colProfit.Visible = true;
            this.colProfit.VisibleIndex = 29;
            // 
            // colMarkup
            // 
            this.colMarkup.Caption = "% Markup";
            this.colMarkup.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colMarkup.FieldName = "Markup";
            this.colMarkup.Name = "colMarkup";
            this.colMarkup.OptionsColumn.AllowEdit = false;
            this.colMarkup.OptionsColumn.AllowFocus = false;
            this.colMarkup.OptionsColumn.ReadOnly = true;
            this.colMarkup.Visible = true;
            this.colMarkup.VisibleIndex = 30;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 79;
            // 
            // colNonStandardCost
            // 
            this.colNonStandardCost.Caption = "Non Standard Cost";
            this.colNonStandardCost.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardCost.FieldName = "NonStandardCost";
            this.colNonStandardCost.Name = "colNonStandardCost";
            this.colNonStandardCost.OptionsColumn.AllowEdit = false;
            this.colNonStandardCost.OptionsColumn.AllowFocus = false;
            this.colNonStandardCost.OptionsColumn.ReadOnly = true;
            this.colNonStandardCost.Visible = true;
            this.colNonStandardCost.VisibleIndex = 31;
            this.colNonStandardCost.Width = 112;
            // 
            // colNonStandardSell
            // 
            this.colNonStandardSell.Caption = "Non Standard Sell";
            this.colNonStandardSell.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardSell.FieldName = "NonStandardSell";
            this.colNonStandardSell.Name = "colNonStandardSell";
            this.colNonStandardSell.OptionsColumn.AllowEdit = false;
            this.colNonStandardSell.OptionsColumn.AllowFocus = false;
            this.colNonStandardSell.OptionsColumn.ReadOnly = true;
            this.colNonStandardSell.Visible = true;
            this.colNonStandardSell.VisibleIndex = 32;
            this.colNonStandardSell.Width = 106;
            // 
            // colAccessComments
            // 
            this.colAccessComments.Caption = "Access Comments";
            this.colAccessComments.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colAccessComments.FieldName = "AccessComments";
            this.colAccessComments.Name = "colAccessComments";
            this.colAccessComments.OptionsColumn.ReadOnly = true;
            this.colAccessComments.Visible = true;
            this.colAccessComments.VisibleIndex = 44;
            this.colAccessComments.Width = 107;
            // 
            // colClientHowSoon
            // 
            this.colClientHowSoon.Caption = "How Soon";
            this.colClientHowSoon.FieldName = "ClientHowSoon";
            this.colClientHowSoon.Name = "colClientHowSoon";
            this.colClientHowSoon.OptionsColumn.AllowEdit = false;
            this.colClientHowSoon.OptionsColumn.AllowFocus = false;
            this.colClientHowSoon.OptionsColumn.ReadOnly = true;
            this.colClientHowSoon.Visible = true;
            this.colClientHowSoon.VisibleIndex = 7;
            // 
            // colHoursWorked
            // 
            this.colHoursWorked.Caption = "Hours Worked";
            this.colHoursWorked.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colHoursWorked.FieldName = "HoursWorked";
            this.colHoursWorked.Name = "colHoursWorked";
            this.colHoursWorked.OptionsColumn.AllowEdit = false;
            this.colHoursWorked.OptionsColumn.AllowFocus = false;
            this.colHoursWorked.OptionsColumn.ReadOnly = true;
            this.colHoursWorked.Visible = true;
            this.colHoursWorked.VisibleIndex = 18;
            this.colHoursWorked.Width = 89;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 51;
            // 
            // colNoAccessAbortedRate
            // 
            this.colNoAccessAbortedRate.Caption = "No Access \\ Aborted Rate";
            this.colNoAccessAbortedRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colNoAccessAbortedRate.FieldName = "NoAccessAbortedRate";
            this.colNoAccessAbortedRate.Name = "colNoAccessAbortedRate";
            this.colNoAccessAbortedRate.OptionsColumn.AllowEdit = false;
            this.colNoAccessAbortedRate.OptionsColumn.AllowFocus = false;
            this.colNoAccessAbortedRate.OptionsColumn.ReadOnly = true;
            this.colNoAccessAbortedRate.Visible = true;
            this.colNoAccessAbortedRate.VisibleIndex = 21;
            this.colNoAccessAbortedRate.Width = 145;
            // 
            // colTeamInvoiceNumber
            // 
            this.colTeamInvoiceNumber.Caption = "Team Invoice No.";
            this.colTeamInvoiceNumber.FieldName = "TeamInvoiceNumber";
            this.colTeamInvoiceNumber.Name = "colTeamInvoiceNumber";
            this.colTeamInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colTeamInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colTeamInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colTeamInvoiceNumber.Visible = true;
            this.colTeamInvoiceNumber.VisibleIndex = 47;
            this.colTeamInvoiceNumber.Width = 105;
            // 
            // colTeamPOFileName
            // 
            this.colTeamPOFileName.Caption = "Team P.O. File";
            this.colTeamPOFileName.ColumnEdit = this.repositoryItemHyperLinkEditTeamPOFile;
            this.colTeamPOFileName.FieldName = "TeamPOFileName";
            this.colTeamPOFileName.Name = "colTeamPOFileName";
            this.colTeamPOFileName.Visible = true;
            this.colTeamPOFileName.VisibleIndex = 48;
            this.colTeamPOFileName.Width = 91;
            // 
            // colTimesheetSubmitted
            // 
            this.colTimesheetSubmitted.Caption = "Timesheet Submitted";
            this.colTimesheetSubmitted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTimesheetSubmitted.FieldName = "TimesheetSubmitted";
            this.colTimesheetSubmitted.Name = "colTimesheetSubmitted";
            this.colTimesheetSubmitted.Visible = true;
            this.colTimesheetSubmitted.VisibleIndex = 45;
            this.colTimesheetSubmitted.Width = 121;
            // 
            // colRecordedBySubContractorID
            // 
            this.colRecordedBySubContractorID.Caption = "Recorded By Team ID";
            this.colRecordedBySubContractorID.FieldName = "RecordedBySubContractorID";
            this.colRecordedBySubContractorID.Name = "colRecordedBySubContractorID";
            this.colRecordedBySubContractorID.OptionsColumn.AllowEdit = false;
            this.colRecordedBySubContractorID.OptionsColumn.AllowFocus = false;
            this.colRecordedBySubContractorID.OptionsColumn.ReadOnly = true;
            this.colRecordedBySubContractorID.Width = 125;
            // 
            // colRecordedBySubContractorName
            // 
            this.colRecordedBySubContractorName.Caption = "Recorded By Team";
            this.colRecordedBySubContractorName.FieldName = "RecordedBySubContractorName";
            this.colRecordedBySubContractorName.Name = "colRecordedBySubContractorName";
            this.colRecordedBySubContractorName.OptionsColumn.AllowEdit = false;
            this.colRecordedBySubContractorName.OptionsColumn.AllowFocus = false;
            this.colRecordedBySubContractorName.OptionsColumn.ReadOnly = true;
            this.colRecordedBySubContractorName.Visible = true;
            this.colRecordedBySubContractorName.VisibleIndex = 37;
            this.colRecordedBySubContractorName.Width = 111;
            // 
            // colTimesheetNumber
            // 
            this.colTimesheetNumber.Caption = "Timesheet Number";
            this.colTimesheetNumber.FieldName = "TimesheetNumber";
            this.colTimesheetNumber.Name = "colTimesheetNumber";
            this.colTimesheetNumber.OptionsColumn.ReadOnly = true;
            this.colTimesheetNumber.Visible = true;
            this.colTimesheetNumber.VisibleIndex = 46;
            this.colTimesheetNumber.Width = 110;
            // 
            // colAddedByWebsite
            // 
            this.colAddedByWebsite.Caption = "Added By Website";
            this.colAddedByWebsite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAddedByWebsite.FieldName = "AddedByWebsite";
            this.colAddedByWebsite.Name = "colAddedByWebsite";
            this.colAddedByWebsite.OptionsColumn.AllowEdit = false;
            this.colAddedByWebsite.OptionsColumn.AllowFocus = false;
            this.colAddedByWebsite.OptionsColumn.ReadOnly = true;
            this.colAddedByWebsite.Visible = true;
            this.colAddedByWebsite.VisibleIndex = 49;
            this.colAddedByWebsite.Width = 109;
            // 
            // colClientInvoiceLineID
            // 
            this.colClientInvoiceLineID.Caption = "Finance Invoice Line ID";
            this.colClientInvoiceLineID.FieldName = "ClientInvoiceLineID";
            this.colClientInvoiceLineID.Name = "colClientInvoiceLineID";
            this.colClientInvoiceLineID.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceLineID.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceLineID.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceLineID.Width = 129;
            // 
            // colFinanceInvoiceNumber
            // 
            this.colFinanceInvoiceNumber.Caption = "Finance Invoice Number";
            this.colFinanceInvoiceNumber.FieldName = "FinanceInvoiceNumber";
            this.colFinanceInvoiceNumber.Name = "colFinanceInvoiceNumber";
            this.colFinanceInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colFinanceInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colFinanceInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colFinanceInvoiceNumber.Visible = true;
            this.colFinanceInvoiceNumber.VisibleIndex = 34;
            this.colFinanceInvoiceNumber.Width = 133;
            // 
            // colFinanceTeamPaymentExported
            // 
            this.colFinanceTeamPaymentExported.Caption = "Team Payment Exported To Finance";
            this.colFinanceTeamPaymentExported.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colFinanceTeamPaymentExported.FieldName = "FinanceTeamPaymentExported";
            this.colFinanceTeamPaymentExported.Name = "colFinanceTeamPaymentExported";
            this.colFinanceTeamPaymentExported.OptionsColumn.AllowEdit = false;
            this.colFinanceTeamPaymentExported.OptionsColumn.AllowFocus = false;
            this.colFinanceTeamPaymentExported.OptionsColumn.ReadOnly = true;
            this.colFinanceTeamPaymentExported.Width = 191;
            // 
            // colSiteManagerName
            // 
            this.colSiteManagerName.Caption = "Site Manager Name";
            this.colSiteManagerName.FieldName = "SiteManagerName";
            this.colSiteManagerName.Name = "colSiteManagerName";
            this.colSiteManagerName.OptionsColumn.AllowEdit = false;
            this.colSiteManagerName.OptionsColumn.AllowFocus = false;
            this.colSiteManagerName.OptionsColumn.ReadOnly = true;
            this.colSiteManagerName.Width = 114;
            // 
            // colInternalRemarks
            // 
            this.colInternalRemarks.Caption = "Internal Remarks";
            this.colInternalRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colInternalRemarks.FieldName = "InternalRemarks";
            this.colInternalRemarks.Name = "colInternalRemarks";
            this.colInternalRemarks.OptionsColumn.AllowEdit = false;
            this.colInternalRemarks.OptionsColumn.AllowFocus = false;
            this.colInternalRemarks.OptionsColumn.ReadOnly = true;
            this.colInternalRemarks.Visible = true;
            this.colInternalRemarks.VisibleIndex = 52;
            this.colInternalRemarks.Width = 103;
            // 
            // colAddedOnPDA
            // 
            this.colAddedOnPDA.Caption = "Added By Device";
            this.colAddedOnPDA.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAddedOnPDA.FieldName = "AddedOnPDA";
            this.colAddedOnPDA.Name = "colAddedOnPDA";
            this.colAddedOnPDA.OptionsColumn.AllowEdit = false;
            this.colAddedOnPDA.OptionsColumn.AllowFocus = false;
            this.colAddedOnPDA.OptionsColumn.ReadOnly = true;
            this.colAddedOnPDA.Visible = true;
            this.colAddedOnPDA.VisibleIndex = 50;
            this.colAddedOnPDA.Width = 90;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 11;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Width = 60;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlLinkedGrittingCalloutsFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Text = "SNOW CLEARANCE Callouts";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1290, 537);
            this.splitContainerControl1.SplitterPosition = 162;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1286, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // popupContainerControlLinkedGrittingCalloutsFilter
            // 
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.btnGritCalloutFilterOK);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.gridControl3);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Location = new System.Drawing.Point(139, 82);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Name = "popupContainerControlLinkedGrittingCalloutsFilter";
            this.popupContainerControlLinkedGrittingCalloutsFilter.Size = new System.Drawing.Size(289, 220);
            this.popupContainerControlLinkedGrittingCalloutsFilter.TabIndex = 5;
            // 
            // btnGritCalloutFilterOK
            // 
            this.btnGritCalloutFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGritCalloutFilterOK.Location = new System.Drawing.Point(3, 194);
            this.btnGritCalloutFilterOK.Name = "btnGritCalloutFilterOK";
            this.btnGritCalloutFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnGritCalloutFilterOK.TabIndex = 12;
            this.btnGritCalloutFilterOK.Text = "OK";
            this.btnGritCalloutFilterOK.Click += new System.EventHandler(this.btnGritCalloutFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp04001GCJobCallOutStatusesBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(283, 189);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp04001GCJobCallOutStatusesBindingSource
            // 
            this.sp04001GCJobCallOutStatusesBindingSource.DataMember = "sp04001_GC_Job_CallOut_Statuses";
            this.sp04001GCJobCallOutStatusesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colValue,
            this.colOrder});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Callout Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 263;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(-1, 43);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1286, 322);
            this.gridSplitContainer1.TabIndex = 6;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(1290, 162);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage1,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage3});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1285, 133);
            this.xtraTabPage2.Text = "Linked Extra Costs";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1285, 133);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp04161GCSnowClearanceCalloutLinkedExtraCostsBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit2DP2,
            this.repositoryItemTextEditCurrency3});
            this.gridControl2.Size = new System.Drawing.Size(1285, 133);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp04161GCSnowClearanceCalloutLinkedExtraCostsBindingSource
            // 
            this.sp04161GCSnowClearanceCalloutLinkedExtraCostsBindingSource.DataMember = "sp04161_GC_Snow_Clearance_Callout_Linked_Extra_Costs";
            this.sp04161GCSnowClearanceCalloutLinkedExtraCostsBindingSource.DataSource = this.dataSet_GC_Snow_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colExtraCostID,
            this.colCostTypeID,
            this.colDescription,
            this.colVatRate,
            this.colRemarks,
            this.colCreatedFromDefault,
            this.colCostTypeDescription,
            this.colSiteName1,
            this.colClientName1,
            this.colSubContractorID1,
            this.colTeamName,
            this.colTeamAddressLine1,
            this.colCalloutDescription,
            this.colCallOutDateTime1,
            this.colChargedPerHour,
            this.colHours,
            this.colSnowClearanceCallOutID1,
            this.colSnowClearanceSiteContractID1,
            this.colTotalCost1,
            this.colTotalSell1,
            this.colUnitCost,
            this.colUnitSell});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCalloutDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseDown);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colExtraCostID
            // 
            this.colExtraCostID.Caption = "Extra Cost ID";
            this.colExtraCostID.FieldName = "ExtraCostID";
            this.colExtraCostID.Name = "colExtraCostID";
            this.colExtraCostID.OptionsColumn.AllowEdit = false;
            this.colExtraCostID.OptionsColumn.AllowFocus = false;
            this.colExtraCostID.OptionsColumn.ReadOnly = true;
            this.colExtraCostID.Width = 86;
            // 
            // colCostTypeID
            // 
            this.colCostTypeID.Caption = "Cost Type ID";
            this.colCostTypeID.FieldName = "CostTypeID";
            this.colCostTypeID.Name = "colCostTypeID";
            this.colCostTypeID.OptionsColumn.AllowEdit = false;
            this.colCostTypeID.OptionsColumn.AllowFocus = false;
            this.colCostTypeID.OptionsColumn.ReadOnly = true;
            this.colCostTypeID.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 245;
            // 
            // colVatRate
            // 
            this.colVatRate.Caption = "VAT Rate";
            this.colVatRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colVatRate.FieldName = "VatRate";
            this.colVatRate.Name = "colVatRate";
            this.colVatRate.OptionsColumn.AllowEdit = false;
            this.colVatRate.OptionsColumn.AllowFocus = false;
            this.colVatRate.OptionsColumn.ReadOnly = true;
            this.colVatRate.Visible = true;
            this.colVatRate.VisibleIndex = 6;
            this.colVatRate.Width = 66;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colCreatedFromDefault
            // 
            this.colCreatedFromDefault.Caption = "System Generated";
            this.colCreatedFromDefault.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colCreatedFromDefault.FieldName = "CreatedFromDefault";
            this.colCreatedFromDefault.Name = "colCreatedFromDefault";
            this.colCreatedFromDefault.OptionsColumn.AllowEdit = false;
            this.colCreatedFromDefault.OptionsColumn.AllowFocus = false;
            this.colCreatedFromDefault.OptionsColumn.ReadOnly = true;
            this.colCreatedFromDefault.Visible = true;
            this.colCreatedFromDefault.VisibleIndex = 10;
            this.colCreatedFromDefault.Width = 110;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colCostTypeDescription
            // 
            this.colCostTypeDescription.Caption = "Cost Type";
            this.colCostTypeDescription.FieldName = "CostTypeDescription";
            this.colCostTypeDescription.Name = "colCostTypeDescription";
            this.colCostTypeDescription.OptionsColumn.AllowEdit = false;
            this.colCostTypeDescription.OptionsColumn.AllowFocus = false;
            this.colCostTypeDescription.OptionsColumn.ReadOnly = true;
            this.colCostTypeDescription.Visible = true;
            this.colCostTypeDescription.VisibleIndex = 1;
            this.colCostTypeDescription.Width = 112;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 78;
            // 
            // colSubContractorID1
            // 
            this.colSubContractorID1.Caption = "Team ID";
            this.colSubContractorID1.FieldName = "SubContractorID";
            this.colSubContractorID1.Name = "colSubContractorID1";
            this.colSubContractorID1.OptionsColumn.AllowEdit = false;
            this.colSubContractorID1.OptionsColumn.AllowFocus = false;
            this.colSubContractorID1.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName
            // 
            this.colTeamName.Caption = "Team Name";
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.Visible = true;
            this.colTeamName.VisibleIndex = 11;
            this.colTeamName.Width = 179;
            // 
            // colTeamAddressLine1
            // 
            this.colTeamAddressLine1.Caption = "Team Address Line 1";
            this.colTeamAddressLine1.FieldName = "TeamAddressLine1";
            this.colTeamAddressLine1.Name = "colTeamAddressLine1";
            this.colTeamAddressLine1.OptionsColumn.AllowEdit = false;
            this.colTeamAddressLine1.OptionsColumn.AllowFocus = false;
            this.colTeamAddressLine1.OptionsColumn.ReadOnly = true;
            this.colTeamAddressLine1.Width = 120;
            // 
            // colCalloutDescription
            // 
            this.colCalloutDescription.Caption = "Callout Description";
            this.colCalloutDescription.FieldName = "CalloutDescription";
            this.colCalloutDescription.Name = "colCalloutDescription";
            this.colCalloutDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutDescription.Width = 291;
            // 
            // colCallOutDateTime1
            // 
            this.colCallOutDateTime1.Caption = "Callout Date\\Time";
            this.colCallOutDateTime1.ColumnEdit = this.repositoryItemTextEdit2;
            this.colCallOutDateTime1.FieldName = "CallOutDateTime";
            this.colCallOutDateTime1.Name = "colCallOutDateTime1";
            this.colCallOutDateTime1.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime1.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime1.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime1.Visible = true;
            this.colCallOutDateTime1.VisibleIndex = 12;
            this.colCallOutDateTime1.Width = 106;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colChargedPerHour
            // 
            this.colChargedPerHour.Caption = "Charged Per Hour";
            this.colChargedPerHour.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colChargedPerHour.FieldName = "ChargedPerHour";
            this.colChargedPerHour.Name = "colChargedPerHour";
            this.colChargedPerHour.OptionsColumn.AllowEdit = false;
            this.colChargedPerHour.OptionsColumn.AllowFocus = false;
            this.colChargedPerHour.OptionsColumn.ReadOnly = true;
            this.colChargedPerHour.Visible = true;
            this.colChargedPerHour.VisibleIndex = 2;
            this.colChargedPerHour.Width = 107;
            // 
            // colHours
            // 
            this.colHours.Caption = "Hours";
            this.colHours.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colHours.FieldName = "Hours";
            this.colHours.Name = "colHours";
            this.colHours.OptionsColumn.AllowEdit = false;
            this.colHours.OptionsColumn.AllowFocus = false;
            this.colHours.OptionsColumn.ReadOnly = true;
            this.colHours.Visible = true;
            this.colHours.VisibleIndex = 3;
            this.colHours.Width = 51;
            // 
            // repositoryItemTextEdit2DP2
            // 
            this.repositoryItemTextEdit2DP2.AutoHeight = false;
            this.repositoryItemTextEdit2DP2.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP2.Name = "repositoryItemTextEdit2DP2";
            // 
            // colSnowClearanceCallOutID1
            // 
            this.colSnowClearanceCallOutID1.Caption = "Snow Clearance Callout ID";
            this.colSnowClearanceCallOutID1.FieldName = "SnowClearanceCallOutID";
            this.colSnowClearanceCallOutID1.Name = "colSnowClearanceCallOutID1";
            this.colSnowClearanceCallOutID1.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutID1.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutID1.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutID1.Width = 148;
            // 
            // colSnowClearanceSiteContractID1
            // 
            this.colSnowClearanceSiteContractID1.Caption = "Snow Clearance Contract ID";
            this.colSnowClearanceSiteContractID1.FieldName = "SnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID1.Name = "colSnowClearanceSiteContractID1";
            this.colSnowClearanceSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSiteContractID1.Width = 157;
            // 
            // colTotalCost1
            // 
            this.colTotalCost1.Caption = "Total Cost";
            this.colTotalCost1.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colTotalCost1.FieldName = "TotalCost";
            this.colTotalCost1.Name = "colTotalCost1";
            this.colTotalCost1.OptionsColumn.AllowEdit = false;
            this.colTotalCost1.OptionsColumn.AllowFocus = false;
            this.colTotalCost1.OptionsColumn.ReadOnly = true;
            this.colTotalCost1.Visible = true;
            this.colTotalCost1.VisibleIndex = 7;
            this.colTotalCost1.Width = 72;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colTotalSell1
            // 
            this.colTotalSell1.Caption = "Total Sell";
            this.colTotalSell1.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colTotalSell1.FieldName = "TotalSell";
            this.colTotalSell1.Name = "colTotalSell1";
            this.colTotalSell1.OptionsColumn.AllowEdit = false;
            this.colTotalSell1.OptionsColumn.AllowFocus = false;
            this.colTotalSell1.OptionsColumn.ReadOnly = true;
            this.colTotalSell1.Visible = true;
            this.colTotalSell1.VisibleIndex = 8;
            this.colTotalSell1.Width = 63;
            // 
            // colUnitCost
            // 
            this.colUnitCost.Caption = "Unit Cost";
            this.colUnitCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colUnitCost.FieldName = "UnitCost";
            this.colUnitCost.Name = "colUnitCost";
            this.colUnitCost.OptionsColumn.AllowEdit = false;
            this.colUnitCost.OptionsColumn.AllowFocus = false;
            this.colUnitCost.OptionsColumn.ReadOnly = true;
            this.colUnitCost.Visible = true;
            this.colUnitCost.VisibleIndex = 4;
            this.colUnitCost.Width = 64;
            // 
            // colUnitSell
            // 
            this.colUnitSell.Caption = "Unit Sell";
            this.colUnitSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colUnitSell.FieldName = "UnitSell";
            this.colUnitSell.Name = "colUnitSell";
            this.colUnitSell.OptionsColumn.AllowEdit = false;
            this.colUnitSell.OptionsColumn.AllowFocus = false;
            this.colUnitSell.OptionsColumn.ReadOnly = true;
            this.colUnitSell.Visible = true;
            this.colUnitSell.VisibleIndex = 5;
            this.colUnitSell.Width = 60;
            // 
            // repositoryItemTextEditCurrency3
            // 
            this.repositoryItemTextEditCurrency3.AutoHeight = false;
            this.repositoryItemTextEditCurrency3.Name = "repositoryItemTextEditCurrency3";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1285, 133);
            this.xtraTabPage1.Text = "Linked Rates";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl4;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1285, 133);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemTextEditCurrency4,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit2DP3,
            this.repositoryItemTextEditTime,
            this.repositoryItemTextEditInt});
            this.gridControl4.Size = new System.Drawing.Size(1285, 133);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp04162GCSnowClearanceCalloutLinkedRatesBindingSource
            // 
            this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource.DataMember = "sp04162_GC_Snow_Clearance_Callout_Linked_Rates";
            this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource.DataSource = this.dataSet_GC_Snow_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRemarks1,
            this.colSiteName2,
            this.colClientName2,
            this.colSubContractorID2,
            this.colTeamName1,
            this.colTeamAddressLine11,
            this.colCalloutDescription1,
            this.colCallOutDateTime2,
            this.colDayTypeDescription,
            this.colDayTypeID,
            this.colEndDateTime,
            this.colSnowClearanceCallOutID2,
            this.colSnowClearanceCallOutRateID,
            this.colSnowClearanceSiteContractID2,
            this.colStartDateTime,
            this.colTotalCost2,
            this.colMachineCount1,
            this.colMachineCount2,
            this.colMachineCount3,
            this.colMachineCount4,
            this.colRate,
            this.colMachineRate2,
            this.colMachineRate3,
            this.colMachineRate4,
            this.colHours1,
            this.colHours2,
            this.colHours3,
            this.colHours4,
            this.colMinimumHours,
            this.colMinimumHours2,
            this.colMinimumHours3,
            this.colMinimumHours4});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCalloutDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDayTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDateTime, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseDown);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 19;
            this.colRemarks1.Width = 254;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 78;
            // 
            // colSubContractorID2
            // 
            this.colSubContractorID2.Caption = "Team ID";
            this.colSubContractorID2.FieldName = "SubContractorID";
            this.colSubContractorID2.Name = "colSubContractorID2";
            this.colSubContractorID2.OptionsColumn.AllowEdit = false;
            this.colSubContractorID2.OptionsColumn.AllowFocus = false;
            this.colSubContractorID2.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName1
            // 
            this.colTeamName1.Caption = "Team Name";
            this.colTeamName1.FieldName = "TeamName";
            this.colTeamName1.Name = "colTeamName1";
            this.colTeamName1.OptionsColumn.AllowEdit = false;
            this.colTeamName1.OptionsColumn.AllowFocus = false;
            this.colTeamName1.OptionsColumn.ReadOnly = true;
            this.colTeamName1.Width = 77;
            // 
            // colTeamAddressLine11
            // 
            this.colTeamAddressLine11.Caption = "Team Address Line 1";
            this.colTeamAddressLine11.FieldName = "TeamAddressLine1";
            this.colTeamAddressLine11.Name = "colTeamAddressLine11";
            this.colTeamAddressLine11.OptionsColumn.AllowEdit = false;
            this.colTeamAddressLine11.OptionsColumn.AllowFocus = false;
            this.colTeamAddressLine11.OptionsColumn.ReadOnly = true;
            this.colTeamAddressLine11.Width = 120;
            // 
            // colCalloutDescription1
            // 
            this.colCalloutDescription1.Caption = "Callout Description";
            this.colCalloutDescription1.FieldName = "CalloutDescription";
            this.colCalloutDescription1.Name = "colCalloutDescription1";
            this.colCalloutDescription1.OptionsColumn.AllowEdit = false;
            this.colCalloutDescription1.OptionsColumn.AllowFocus = false;
            this.colCalloutDescription1.OptionsColumn.ReadOnly = true;
            this.colCalloutDescription1.Width = 123;
            // 
            // colCallOutDateTime2
            // 
            this.colCallOutDateTime2.Caption = "Callout Time";
            this.colCallOutDateTime2.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colCallOutDateTime2.FieldName = "CallOutDateTime";
            this.colCallOutDateTime2.Name = "colCallOutDateTime2";
            this.colCallOutDateTime2.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime2.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime2.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime2.Width = 79;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colDayTypeDescription
            // 
            this.colDayTypeDescription.Caption = "Day Type";
            this.colDayTypeDescription.FieldName = "DayTypeDescription";
            this.colDayTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDayTypeDescription.Name = "colDayTypeDescription";
            this.colDayTypeDescription.OptionsColumn.AllowEdit = false;
            this.colDayTypeDescription.OptionsColumn.AllowFocus = false;
            this.colDayTypeDescription.OptionsColumn.ReadOnly = true;
            this.colDayTypeDescription.Visible = true;
            this.colDayTypeDescription.VisibleIndex = 0;
            this.colDayTypeDescription.Width = 115;
            // 
            // colDayTypeID
            // 
            this.colDayTypeID.Caption = "Day Type ID";
            this.colDayTypeID.FieldName = "DayTypeID";
            this.colDayTypeID.Name = "colDayTypeID";
            this.colDayTypeID.OptionsColumn.AllowEdit = false;
            this.colDayTypeID.OptionsColumn.AllowFocus = false;
            this.colDayTypeID.OptionsColumn.ReadOnly = true;
            this.colDayTypeID.Width = 81;
            // 
            // colEndDateTime
            // 
            this.colEndDateTime.Caption = "End Time";
            this.colEndDateTime.ColumnEdit = this.repositoryItemTextEditTime;
            this.colEndDateTime.FieldName = "EndDateTime";
            this.colEndDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDateTime.Name = "colEndDateTime";
            this.colEndDateTime.OptionsColumn.AllowEdit = false;
            this.colEndDateTime.OptionsColumn.AllowFocus = false;
            this.colEndDateTime.OptionsColumn.ReadOnly = true;
            this.colEndDateTime.Visible = true;
            this.colEndDateTime.VisibleIndex = 2;
            this.colEndDateTime.Width = 65;
            // 
            // repositoryItemTextEditTime
            // 
            this.repositoryItemTextEditTime.AutoHeight = false;
            this.repositoryItemTextEditTime.Mask.EditMask = "t";
            this.repositoryItemTextEditTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditTime.Name = "repositoryItemTextEditTime";
            // 
            // colSnowClearanceCallOutID2
            // 
            this.colSnowClearanceCallOutID2.Caption = "Snow Clearance Callout ID";
            this.colSnowClearanceCallOutID2.FieldName = "SnowClearanceCallOutID";
            this.colSnowClearanceCallOutID2.Name = "colSnowClearanceCallOutID2";
            this.colSnowClearanceCallOutID2.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutID2.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutID2.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutID2.Width = 148;
            // 
            // colSnowClearanceCallOutRateID
            // 
            this.colSnowClearanceCallOutRateID.Caption = "Snow Clearance Callout Rate ID";
            this.colSnowClearanceCallOutRateID.FieldName = "SnowClearanceCallOutRateID";
            this.colSnowClearanceCallOutRateID.Name = "colSnowClearanceCallOutRateID";
            this.colSnowClearanceCallOutRateID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutRateID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutRateID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutRateID.Width = 174;
            // 
            // colSnowClearanceSiteContractID2
            // 
            this.colSnowClearanceSiteContractID2.Caption = "Snow Clearance Contract ID";
            this.colSnowClearanceSiteContractID2.FieldName = "SnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID2.Name = "colSnowClearanceSiteContractID2";
            this.colSnowClearanceSiteContractID2.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSiteContractID2.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSiteContractID2.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSiteContractID2.Width = 157;
            // 
            // colStartDateTime
            // 
            this.colStartDateTime.Caption = "Start Time";
            this.colStartDateTime.ColumnEdit = this.repositoryItemTextEditTime;
            this.colStartDateTime.FieldName = "StartDateTime";
            this.colStartDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDateTime.Name = "colStartDateTime";
            this.colStartDateTime.OptionsColumn.AllowEdit = false;
            this.colStartDateTime.OptionsColumn.AllowFocus = false;
            this.colStartDateTime.OptionsColumn.ReadOnly = true;
            this.colStartDateTime.Visible = true;
            this.colStartDateTime.VisibleIndex = 1;
            this.colStartDateTime.Width = 83;
            // 
            // colTotalCost2
            // 
            this.colTotalCost2.Caption = "Total Cost";
            this.colTotalCost2.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colTotalCost2.FieldName = "TotalCost";
            this.colTotalCost2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colTotalCost2.Name = "colTotalCost2";
            this.colTotalCost2.OptionsColumn.AllowEdit = false;
            this.colTotalCost2.OptionsColumn.AllowFocus = false;
            this.colTotalCost2.OptionsColumn.ReadOnly = true;
            this.colTotalCost2.Visible = true;
            this.colTotalCost2.VisibleIndex = 20;
            // 
            // repositoryItemTextEditCurrency4
            // 
            this.repositoryItemTextEditCurrency4.AutoHeight = false;
            this.repositoryItemTextEditCurrency4.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency4.Name = "repositoryItemTextEditCurrency4";
            // 
            // colMachineCount1
            // 
            this.colMachineCount1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMachineCount1.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMachineCount1.AppearanceCell.Options.UseBackColor = true;
            this.colMachineCount1.AppearanceCell.Options.UseForeColor = true;
            this.colMachineCount1.Caption = "Machine Count 1";
            this.colMachineCount1.ColumnEdit = this.repositoryItemTextEditInt;
            this.colMachineCount1.FieldName = "MachineCount1";
            this.colMachineCount1.Name = "colMachineCount1";
            this.colMachineCount1.OptionsColumn.AllowEdit = false;
            this.colMachineCount1.OptionsColumn.AllowFocus = false;
            this.colMachineCount1.OptionsColumn.ReadOnly = true;
            this.colMachineCount1.Visible = true;
            this.colMachineCount1.VisibleIndex = 6;
            this.colMachineCount1.Width = 101;
            // 
            // repositoryItemTextEditInt
            // 
            this.repositoryItemTextEditInt.AutoHeight = false;
            this.repositoryItemTextEditInt.Mask.EditMask = "n0";
            this.repositoryItemTextEditInt.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInt.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInt.Name = "repositoryItemTextEditInt";
            // 
            // colMachineCount2
            // 
            this.colMachineCount2.Caption = "Machine Count 2";
            this.colMachineCount2.ColumnEdit = this.repositoryItemTextEditInt;
            this.colMachineCount2.FieldName = "MachineCount2";
            this.colMachineCount2.Name = "colMachineCount2";
            this.colMachineCount2.OptionsColumn.AllowEdit = false;
            this.colMachineCount2.OptionsColumn.AllowFocus = false;
            this.colMachineCount2.OptionsColumn.ReadOnly = true;
            this.colMachineCount2.Visible = true;
            this.colMachineCount2.VisibleIndex = 10;
            this.colMachineCount2.Width = 101;
            // 
            // colMachineCount3
            // 
            this.colMachineCount3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMachineCount3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMachineCount3.AppearanceCell.Options.UseBackColor = true;
            this.colMachineCount3.AppearanceCell.Options.UseForeColor = true;
            this.colMachineCount3.Caption = "Machine Count 3";
            this.colMachineCount3.ColumnEdit = this.repositoryItemTextEditInt;
            this.colMachineCount3.FieldName = "MachineCount3";
            this.colMachineCount3.Name = "colMachineCount3";
            this.colMachineCount3.OptionsColumn.AllowEdit = false;
            this.colMachineCount3.OptionsColumn.AllowFocus = false;
            this.colMachineCount3.OptionsColumn.ReadOnly = true;
            this.colMachineCount3.Visible = true;
            this.colMachineCount3.VisibleIndex = 14;
            this.colMachineCount3.Width = 101;
            // 
            // colMachineCount4
            // 
            this.colMachineCount4.Caption = "Machine Count 4";
            this.colMachineCount4.ColumnEdit = this.repositoryItemTextEditInt;
            this.colMachineCount4.FieldName = "MachineCount4";
            this.colMachineCount4.Name = "colMachineCount4";
            this.colMachineCount4.OptionsColumn.AllowEdit = false;
            this.colMachineCount4.OptionsColumn.AllowFocus = false;
            this.colMachineCount4.OptionsColumn.ReadOnly = true;
            this.colMachineCount4.Visible = true;
            this.colMachineCount4.VisibleIndex = 18;
            this.colMachineCount4.Width = 101;
            // 
            // colRate
            // 
            this.colRate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colRate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colRate.AppearanceCell.Options.UseBackColor = true;
            this.colRate.AppearanceCell.Options.UseForeColor = true;
            this.colRate.Caption = "Machine Rate 1";
            this.colRate.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colRate.FieldName = "Rate";
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowEdit = false;
            this.colRate.OptionsColumn.AllowFocus = false;
            this.colRate.OptionsColumn.ReadOnly = true;
            this.colRate.Visible = true;
            this.colRate.VisibleIndex = 3;
            this.colRate.Width = 95;
            // 
            // colMachineRate2
            // 
            this.colMachineRate2.Caption = "Machine Rate 2";
            this.colMachineRate2.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colMachineRate2.FieldName = "MachineRate2";
            this.colMachineRate2.Name = "colMachineRate2";
            this.colMachineRate2.OptionsColumn.AllowEdit = false;
            this.colMachineRate2.OptionsColumn.AllowFocus = false;
            this.colMachineRate2.OptionsColumn.ReadOnly = true;
            this.colMachineRate2.Visible = true;
            this.colMachineRate2.VisibleIndex = 7;
            this.colMachineRate2.Width = 95;
            // 
            // colMachineRate3
            // 
            this.colMachineRate3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMachineRate3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMachineRate3.AppearanceCell.Options.UseBackColor = true;
            this.colMachineRate3.AppearanceCell.Options.UseForeColor = true;
            this.colMachineRate3.Caption = "Machine Rate 3";
            this.colMachineRate3.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colMachineRate3.FieldName = "MachineRate3";
            this.colMachineRate3.Name = "colMachineRate3";
            this.colMachineRate3.OptionsColumn.AllowEdit = false;
            this.colMachineRate3.OptionsColumn.AllowFocus = false;
            this.colMachineRate3.OptionsColumn.ReadOnly = true;
            this.colMachineRate3.Visible = true;
            this.colMachineRate3.VisibleIndex = 11;
            this.colMachineRate3.Width = 95;
            // 
            // colMachineRate4
            // 
            this.colMachineRate4.Caption = "Machine Rate 4";
            this.colMachineRate4.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colMachineRate4.FieldName = "MachineRate4";
            this.colMachineRate4.Name = "colMachineRate4";
            this.colMachineRate4.OptionsColumn.AllowEdit = false;
            this.colMachineRate4.OptionsColumn.AllowFocus = false;
            this.colMachineRate4.OptionsColumn.ReadOnly = true;
            this.colMachineRate4.Visible = true;
            this.colMachineRate4.VisibleIndex = 15;
            this.colMachineRate4.Width = 95;
            // 
            // colHours1
            // 
            this.colHours1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colHours1.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colHours1.AppearanceCell.Options.UseBackColor = true;
            this.colHours1.AppearanceCell.Options.UseForeColor = true;
            this.colHours1.Caption = "Machine Hours 1";
            this.colHours1.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colHours1.FieldName = "Hours";
            this.colHours1.Name = "colHours1";
            this.colHours1.OptionsColumn.AllowEdit = false;
            this.colHours1.OptionsColumn.AllowFocus = false;
            this.colHours1.OptionsColumn.ReadOnly = true;
            this.colHours1.Visible = true;
            this.colHours1.VisibleIndex = 5;
            this.colHours1.Width = 100;
            // 
            // repositoryItemTextEdit2DP3
            // 
            this.repositoryItemTextEdit2DP3.AutoHeight = false;
            this.repositoryItemTextEdit2DP3.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP3.Name = "repositoryItemTextEdit2DP3";
            // 
            // colHours2
            // 
            this.colHours2.Caption = "Machine Hours 2";
            this.colHours2.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colHours2.FieldName = "Hours2";
            this.colHours2.Name = "colHours2";
            this.colHours2.OptionsColumn.AllowEdit = false;
            this.colHours2.OptionsColumn.AllowFocus = false;
            this.colHours2.OptionsColumn.ReadOnly = true;
            this.colHours2.Visible = true;
            this.colHours2.VisibleIndex = 9;
            this.colHours2.Width = 100;
            // 
            // colHours3
            // 
            this.colHours3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colHours3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colHours3.AppearanceCell.Options.UseBackColor = true;
            this.colHours3.AppearanceCell.Options.UseForeColor = true;
            this.colHours3.Caption = "Machine Hours 3";
            this.colHours3.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colHours3.FieldName = "Hours3";
            this.colHours3.Name = "colHours3";
            this.colHours3.OptionsColumn.AllowEdit = false;
            this.colHours3.OptionsColumn.AllowFocus = false;
            this.colHours3.OptionsColumn.ReadOnly = true;
            this.colHours3.Visible = true;
            this.colHours3.VisibleIndex = 13;
            this.colHours3.Width = 100;
            // 
            // colHours4
            // 
            this.colHours4.Caption = "Machine Hours 4";
            this.colHours4.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colHours4.FieldName = "Hours4";
            this.colHours4.Name = "colHours4";
            this.colHours4.OptionsColumn.AllowEdit = false;
            this.colHours4.OptionsColumn.AllowFocus = false;
            this.colHours4.OptionsColumn.ReadOnly = true;
            this.colHours4.Visible = true;
            this.colHours4.VisibleIndex = 17;
            this.colHours4.Width = 100;
            // 
            // colMinimumHours
            // 
            this.colMinimumHours.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMinimumHours.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMinimumHours.AppearanceCell.Options.UseBackColor = true;
            this.colMinimumHours.AppearanceCell.Options.UseForeColor = true;
            this.colMinimumHours.Caption = "Minimum Hours 1";
            this.colMinimumHours.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colMinimumHours.FieldName = "MinimumHours";
            this.colMinimumHours.Name = "colMinimumHours";
            this.colMinimumHours.OptionsColumn.AllowEdit = false;
            this.colMinimumHours.OptionsColumn.AllowFocus = false;
            this.colMinimumHours.OptionsColumn.ReadOnly = true;
            this.colMinimumHours.Visible = true;
            this.colMinimumHours.VisibleIndex = 4;
            this.colMinimumHours.Width = 101;
            // 
            // colMinimumHours2
            // 
            this.colMinimumHours2.Caption = "Minimum Hours 2";
            this.colMinimumHours2.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colMinimumHours2.FieldName = "MinimumHours2";
            this.colMinimumHours2.Name = "colMinimumHours2";
            this.colMinimumHours2.OptionsColumn.AllowEdit = false;
            this.colMinimumHours2.OptionsColumn.AllowFocus = false;
            this.colMinimumHours2.OptionsColumn.ReadOnly = true;
            this.colMinimumHours2.Visible = true;
            this.colMinimumHours2.VisibleIndex = 8;
            this.colMinimumHours2.Width = 101;
            // 
            // colMinimumHours3
            // 
            this.colMinimumHours3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMinimumHours3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMinimumHours3.AppearanceCell.Options.UseBackColor = true;
            this.colMinimumHours3.AppearanceCell.Options.UseForeColor = true;
            this.colMinimumHours3.Caption = "Minimum Hours 3";
            this.colMinimumHours3.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colMinimumHours3.FieldName = "MinimumHours3";
            this.colMinimumHours3.Name = "colMinimumHours3";
            this.colMinimumHours3.OptionsColumn.AllowEdit = false;
            this.colMinimumHours3.OptionsColumn.AllowFocus = false;
            this.colMinimumHours3.OptionsColumn.ReadOnly = true;
            this.colMinimumHours3.Visible = true;
            this.colMinimumHours3.VisibleIndex = 12;
            this.colMinimumHours3.Width = 101;
            // 
            // colMinimumHours4
            // 
            this.colMinimumHours4.Caption = "Minimum Hours 4";
            this.colMinimumHours4.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colMinimumHours4.FieldName = "MinimumHours4";
            this.colMinimumHours4.Name = "colMinimumHours4";
            this.colMinimumHours4.OptionsColumn.AllowEdit = false;
            this.colMinimumHours4.OptionsColumn.AllowFocus = false;
            this.colMinimumHours4.OptionsColumn.ReadOnly = true;
            this.colMinimumHours4.Visible = true;
            this.colMinimumHours4.VisibleIndex = 16;
            this.colMinimumHours4.Width = 101;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridControl6);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1285, 133);
            this.xtraTabPage4.Text = "Linked Serviced Areas";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp04340GCServiceAreasLinkedToSnowCalloutBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3});
            this.gridControl6.Size = new System.Drawing.Size(1285, 133);
            this.gridControl6.TabIndex = 2;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp04340GCServiceAreasLinkedToSnowCalloutBindingSource
            // 
            this.sp04340GCServiceAreasLinkedToSnowCalloutBindingSource.DataMember = "sp04340_GC_Service_Areas_Linked_To_Snow_Callout";
            this.sp04340GCServiceAreasLinkedToSnowCalloutBindingSource.DataSource = this.dataSet_GC_Snow_Core;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteServiceAreaID,
            this.colDescriptionID,
            this.colOrderID,
            this.colWasServiced,
            this.colCreatedFromDefault1,
            this.colCalloutDescription2,
            this.colAreaDescription,
            this.colSnowClearanceCalloutID3,
            this.colSnowClearanceServicedSiteID,
            this.colGroupID});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 1;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsLayout.StoreFormatRules = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCalloutDescription2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrderID, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAreaDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseDown);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colSiteServiceAreaID
            // 
            this.colSiteServiceAreaID.Caption = "Site Service Area";
            this.colSiteServiceAreaID.FieldName = "SiteServiceAreaID";
            this.colSiteServiceAreaID.Name = "colSiteServiceAreaID";
            this.colSiteServiceAreaID.OptionsColumn.AllowEdit = false;
            this.colSiteServiceAreaID.OptionsColumn.AllowFocus = false;
            this.colSiteServiceAreaID.OptionsColumn.ReadOnly = true;
            this.colSiteServiceAreaID.Width = 103;
            // 
            // colDescriptionID
            // 
            this.colDescriptionID.Caption = "Description ID";
            this.colDescriptionID.FieldName = "DescriptionID";
            this.colDescriptionID.Name = "colDescriptionID";
            this.colDescriptionID.OptionsColumn.AllowEdit = false;
            this.colDescriptionID.OptionsColumn.AllowFocus = false;
            this.colDescriptionID.OptionsColumn.ReadOnly = true;
            this.colDescriptionID.Width = 88;
            // 
            // colOrderID
            // 
            this.colOrderID.Caption = "Order";
            this.colOrderID.FieldName = "OrderID";
            this.colOrderID.Name = "colOrderID";
            this.colOrderID.OptionsColumn.AllowEdit = false;
            this.colOrderID.OptionsColumn.AllowFocus = false;
            this.colOrderID.OptionsColumn.ReadOnly = true;
            this.colOrderID.Visible = true;
            this.colOrderID.VisibleIndex = 4;
            this.colOrderID.Width = 64;
            // 
            // colWasServiced
            // 
            this.colWasServiced.Caption = "Was Serviced";
            this.colWasServiced.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colWasServiced.FieldName = "WasServiced";
            this.colWasServiced.Name = "colWasServiced";
            this.colWasServiced.OptionsColumn.AllowEdit = false;
            this.colWasServiced.OptionsColumn.AllowFocus = false;
            this.colWasServiced.OptionsColumn.ReadOnly = true;
            this.colWasServiced.Visible = true;
            this.colWasServiced.VisibleIndex = 1;
            this.colWasServiced.Width = 86;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colCreatedFromDefault1
            // 
            this.colCreatedFromDefault1.Caption = "Created From Default";
            this.colCreatedFromDefault1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCreatedFromDefault1.FieldName = "CreatedFromDefault";
            this.colCreatedFromDefault1.Name = "colCreatedFromDefault1";
            this.colCreatedFromDefault1.OptionsColumn.AllowEdit = false;
            this.colCreatedFromDefault1.OptionsColumn.AllowFocus = false;
            this.colCreatedFromDefault1.OptionsColumn.ReadOnly = true;
            this.colCreatedFromDefault1.Visible = true;
            this.colCreatedFromDefault1.VisibleIndex = 2;
            this.colCreatedFromDefault1.Width = 125;
            // 
            // colCalloutDescription2
            // 
            this.colCalloutDescription2.Caption = "Callout Description";
            this.colCalloutDescription2.FieldName = "CalloutDescription";
            this.colCalloutDescription2.Name = "colCalloutDescription2";
            this.colCalloutDescription2.OptionsColumn.AllowEdit = false;
            this.colCalloutDescription2.OptionsColumn.AllowFocus = false;
            this.colCalloutDescription2.OptionsColumn.ReadOnly = true;
            this.colCalloutDescription2.Visible = true;
            this.colCalloutDescription2.VisibleIndex = 7;
            this.colCalloutDescription2.Width = 392;
            // 
            // colAreaDescription
            // 
            this.colAreaDescription.Caption = "Service Area";
            this.colAreaDescription.FieldName = "AreaDescription";
            this.colAreaDescription.Name = "colAreaDescription";
            this.colAreaDescription.OptionsColumn.AllowEdit = false;
            this.colAreaDescription.OptionsColumn.AllowFocus = false;
            this.colAreaDescription.OptionsColumn.ReadOnly = true;
            this.colAreaDescription.Visible = true;
            this.colAreaDescription.VisibleIndex = 0;
            this.colAreaDescription.Width = 334;
            // 
            // colSnowClearanceCalloutID3
            // 
            this.colSnowClearanceCalloutID3.Caption = "Snow Clearance Callout ID";
            this.colSnowClearanceCalloutID3.FieldName = "SnowClearanceCalloutID";
            this.colSnowClearanceCalloutID3.Name = "colSnowClearanceCalloutID3";
            this.colSnowClearanceCalloutID3.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCalloutID3.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCalloutID3.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCalloutID3.Width = 148;
            // 
            // colSnowClearanceServicedSiteID
            // 
            this.colSnowClearanceServicedSiteID.Caption = "Snow Clearance Serviced Site ID";
            this.colSnowClearanceServicedSiteID.FieldName = "SnowClearanceServicedSiteID";
            this.colSnowClearanceServicedSiteID.Name = "colSnowClearanceServicedSiteID";
            this.colSnowClearanceServicedSiteID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceServicedSiteID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceServicedSiteID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceServicedSiteID.Width = 177;
            // 
            // colGroupID
            // 
            this.colGroupID.Caption = "Group ID";
            this.colGroupID.FieldName = "GroupID";
            this.colGroupID.Name = "colGroupID";
            this.colGroupID.OptionsColumn.AllowEdit = false;
            this.colGroupID.OptionsColumn.AllowFocus = false;
            this.colGroupID.OptionsColumn.ReadOnly = true;
            this.colGroupID.Visible = true;
            this.colGroupID.VisibleIndex = 3;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.gridControl5);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1285, 133);
            this.xtraTabPage5.Text = "Linked Pictures";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemMemoExEdit4});
            this.gridControl5.Size = new System.Drawing.Size(1285, 133);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04092GCGrittingCalloutPicturesForCalloutBindingSource
            // 
            this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource.DataMember = "sp04092_GC_Gritting_Callout_Pictures_For_Callout";
            this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPictureID,
            this.colGrittingCallOutID2,
            this.colPicturePath,
            this.colDateTimeTaken,
            this.gridColumn1,
            this.colSiteGrittingContractID2,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTimeTaken, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseDown);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colPictureID
            // 
            this.colPictureID.Caption = "Picture ID";
            this.colPictureID.FieldName = "PictureID";
            this.colPictureID.Name = "colPictureID";
            this.colPictureID.OptionsColumn.AllowEdit = false;
            this.colPictureID.OptionsColumn.AllowFocus = false;
            this.colPictureID.OptionsColumn.ReadOnly = true;
            // 
            // colGrittingCallOutID2
            // 
            this.colGrittingCallOutID2.Caption = "Callout ID";
            this.colGrittingCallOutID2.FieldName = "GrittingCallOutID";
            this.colGrittingCallOutID2.Name = "colGrittingCallOutID2";
            this.colGrittingCallOutID2.OptionsColumn.AllowEdit = false;
            this.colGrittingCallOutID2.OptionsColumn.AllowFocus = false;
            this.colGrittingCallOutID2.OptionsColumn.ReadOnly = true;
            // 
            // colPicturePath
            // 
            this.colPicturePath.Caption = "Picture";
            this.colPicturePath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colPicturePath.FieldName = "PicturePath";
            this.colPicturePath.Name = "colPicturePath";
            this.colPicturePath.OptionsColumn.ReadOnly = true;
            this.colPicturePath.Visible = true;
            this.colPicturePath.VisibleIndex = 1;
            this.colPicturePath.Width = 469;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDateTimeTaken
            // 
            this.colDateTimeTaken.Caption = "Date\\Time Taken";
            this.colDateTimeTaken.ColumnEdit = this.repositoryItemTextEdit4;
            this.colDateTimeTaken.FieldName = "DateTimeTaken";
            this.colDateTimeTaken.Name = "colDateTimeTaken";
            this.colDateTimeTaken.OptionsColumn.AllowEdit = false;
            this.colDateTimeTaken.OptionsColumn.AllowFocus = false;
            this.colDateTimeTaken.OptionsColumn.ReadOnly = true;
            this.colDateTimeTaken.Visible = true;
            this.colDateTimeTaken.VisibleIndex = 0;
            this.colDateTimeTaken.Width = 119;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Remarks";
            this.gridColumn1.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn1.FieldName = "Remarks";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 161;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colSiteGrittingContractID2
            // 
            this.colSiteGrittingContractID2.Caption = "Site Gritting Contract ID";
            this.colSiteGrittingContractID2.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID2.Name = "colSiteGrittingContractID2";
            this.colSiteGrittingContractID2.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID2.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID2.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID2.Width = 136;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Site Name";
            this.gridColumn6.FieldName = "SiteName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Client Name";
            this.gridColumn7.FieldName = "ClientName";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 78;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Team ID";
            this.gridColumn8.FieldName = "SubContractorID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Team Name";
            this.gridColumn9.FieldName = "TeamName";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 77;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Team Address Line 1";
            this.gridColumn10.FieldName = "TeamAddressLine1";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 120;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Callout Description";
            this.gridColumn11.FieldName = "CalloutDescription";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Callout Time";
            this.gridColumn12.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn12.FieldName = "CallOutDateTime";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 79;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridSplitContainer4);
            this.xtraTabPage3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage3.ImageOptions.Image")));
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1285, 133);
            this.xtraTabPage3.Text = "CRM Contacts";
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControl15;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControl15);
            this.gridSplitContainer4.Size = new System.Drawing.Size(1285, 133);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControl15
            // 
            this.gridControl15.DataSource = this.sp05089CRMContactsLinkedToRecordBindingSource;
            this.gridControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl15.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl15.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl15.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl15.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl15_EmbeddedNavigator_ButtonClick);
            this.gridControl15.Location = new System.Drawing.Point(0, 0);
            this.gridControl15.MainView = this.gridView15;
            this.gridControl15.MenuManager = this.barManager1;
            this.gridControl15.Name = "gridControl15";
            this.gridControl15.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit12,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextDateTime});
            this.gridControl15.Size = new System.Drawing.Size(1285, 133);
            this.gridControl15.TabIndex = 7;
            this.gridControl15.UseEmbeddedNavigator = true;
            this.gridControl15.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView15});
            // 
            // sp05089CRMContactsLinkedToRecordBindingSource
            // 
            this.sp05089CRMContactsLinkedToRecordBindingSource.DataMember = "sp05089_CRM_Contacts_Linked_To_Record";
            this.sp05089CRMContactsLinkedToRecordBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCRMID,
            this.gridColumn2,
            this.colClientContactID,
            this.colLinkedToRecordTypeID1,
            this.colLinkedToRecordID1,
            this.colContactDueDateTime,
            this.colContactMadeDateTime,
            this.colContactMethodID,
            this.colContactTypeID,
            this.gridColumn3,
            this.gridColumn4,
            this.colStatusID,
            this.colCreatedByStaffID,
            this.colContactedByStaffID,
            this.colContactDirectionID,
            this.colLinkedRecordDescription1,
            this.colCreatedByStaffName,
            this.colContactedByStaffName,
            this.colContactMethod,
            this.colContactType,
            this.colStatusDescription,
            this.colContactDirectionDescription,
            this.gridColumn5,
            this.colLinkedRecordTypeDescription,
            this.colDateCreated1,
            this.colClientContact});
            this.gridView15.GridControl = this.gridControl15;
            this.gridView15.GroupCount = 1;
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView15.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView15.OptionsLayout.StoreAppearance = true;
            this.gridView15.OptionsLayout.StoreFormatRules = true;
            this.gridView15.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView15.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView15.OptionsSelection.MultiSelect = true;
            this.gridView15.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView15.OptionsView.ColumnAutoWidth = false;
            this.gridView15.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            this.gridView15.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateCreated1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView15.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView15.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView15.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView15.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView15.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView15_MouseDown);
            this.gridView15.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView15_MouseUp);
            this.gridView15.DoubleClick += new System.EventHandler(this.gridView15_DoubleClick);
            this.gridView15.GotFocus += new System.EventHandler(this.gridView15_GotFocus);
            // 
            // colCRMID
            // 
            this.colCRMID.Caption = "CRM ID";
            this.colCRMID.FieldName = "CRMID";
            this.colCRMID.Name = "colCRMID";
            this.colCRMID.OptionsColumn.AllowEdit = false;
            this.colCRMID.OptionsColumn.AllowFocus = false;
            this.colCRMID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Client ID";
            this.gridColumn2.FieldName = "ClientID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // colClientContactID
            // 
            this.colClientContactID.Caption = "Client Contact ID";
            this.colClientContactID.FieldName = "ClientContactID";
            this.colClientContactID.Name = "colClientContactID";
            this.colClientContactID.OptionsColumn.AllowEdit = false;
            this.colClientContactID.OptionsColumn.AllowFocus = false;
            this.colClientContactID.OptionsColumn.ReadOnly = true;
            this.colClientContactID.Width = 100;
            // 
            // colLinkedToRecordTypeID1
            // 
            this.colLinkedToRecordTypeID1.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID1.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID1.Name = "colLinkedToRecordTypeID1";
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID1.Width = 141;
            // 
            // colLinkedToRecordID1
            // 
            this.colLinkedToRecordID1.Caption = "Linked To Record ID";
            this.colLinkedToRecordID1.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID1.Name = "colLinkedToRecordID1";
            this.colLinkedToRecordID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID1.Width = 114;
            // 
            // colContactDueDateTime
            // 
            this.colContactDueDateTime.Caption = "Contact Due";
            this.colContactDueDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactDueDateTime.FieldName = "ContactDueDateTime";
            this.colContactDueDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactDueDateTime.Name = "colContactDueDateTime";
            this.colContactDueDateTime.OptionsColumn.AllowEdit = false;
            this.colContactDueDateTime.OptionsColumn.AllowFocus = false;
            this.colContactDueDateTime.OptionsColumn.ReadOnly = true;
            this.colContactDueDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactDueDateTime.Visible = true;
            this.colContactDueDateTime.VisibleIndex = 3;
            this.colContactDueDateTime.Width = 108;
            // 
            // repositoryItemTextDateTime
            // 
            this.repositoryItemTextDateTime.AutoHeight = false;
            this.repositoryItemTextDateTime.Mask.EditMask = "g";
            this.repositoryItemTextDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextDateTime.Name = "repositoryItemTextDateTime";
            // 
            // colContactMadeDateTime
            // 
            this.colContactMadeDateTime.Caption = "Contact Made";
            this.colContactMadeDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactMadeDateTime.FieldName = "ContactMadeDateTime";
            this.colContactMadeDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactMadeDateTime.Name = "colContactMadeDateTime";
            this.colContactMadeDateTime.OptionsColumn.AllowEdit = false;
            this.colContactMadeDateTime.OptionsColumn.AllowFocus = false;
            this.colContactMadeDateTime.OptionsColumn.ReadOnly = true;
            this.colContactMadeDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactMadeDateTime.Visible = true;
            this.colContactMadeDateTime.VisibleIndex = 4;
            this.colContactMadeDateTime.Width = 102;
            // 
            // colContactMethodID
            // 
            this.colContactMethodID.Caption = "Contact Method ID";
            this.colContactMethodID.FieldName = "ContactMethodID";
            this.colContactMethodID.Name = "colContactMethodID";
            this.colContactMethodID.OptionsColumn.AllowEdit = false;
            this.colContactMethodID.OptionsColumn.AllowFocus = false;
            this.colContactMethodID.OptionsColumn.ReadOnly = true;
            this.colContactMethodID.Width = 109;
            // 
            // colContactTypeID
            // 
            this.colContactTypeID.Caption = "Contact Type ID";
            this.colContactTypeID.FieldName = "ContactTypeID";
            this.colContactTypeID.Name = "colContactTypeID";
            this.colContactTypeID.OptionsColumn.AllowEdit = false;
            this.colContactTypeID.OptionsColumn.AllowFocus = false;
            this.colContactTypeID.OptionsColumn.ReadOnly = true;
            this.colContactTypeID.Width = 97;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Description";
            this.gridColumn3.FieldName = "Description";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 7;
            this.gridColumn3.Width = 260;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Remarks";
            this.gridColumn4.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.gridColumn4.FieldName = "Remarks";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 113;
            // 
            // colContactedByStaffID
            // 
            this.colContactedByStaffID.Caption = "Contacted By";
            this.colContactedByStaffID.FieldName = "ContactedByStaffID";
            this.colContactedByStaffID.Name = "colContactedByStaffID";
            this.colContactedByStaffID.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffID.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffID.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffID.Width = 83;
            // 
            // colContactDirectionID
            // 
            this.colContactDirectionID.Caption = "Contact Direction ID";
            this.colContactDirectionID.FieldName = "ContactDirectionID";
            this.colContactDirectionID.Name = "colContactDirectionID";
            this.colContactDirectionID.OptionsColumn.AllowEdit = false;
            this.colContactDirectionID.OptionsColumn.AllowFocus = false;
            this.colContactDirectionID.OptionsColumn.ReadOnly = true;
            this.colContactDirectionID.Width = 101;
            // 
            // colLinkedRecordDescription1
            // 
            this.colLinkedRecordDescription1.Caption = "Linked Record";
            this.colLinkedRecordDescription1.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription1.Name = "colLinkedRecordDescription1";
            this.colLinkedRecordDescription1.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription1.Width = 191;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 11;
            this.colCreatedByStaffName.Width = 121;
            // 
            // colContactedByStaffName
            // 
            this.colContactedByStaffName.Caption = "GC Contact Person";
            this.colContactedByStaffName.FieldName = "ContactedByStaffName";
            this.colContactedByStaffName.Name = "colContactedByStaffName";
            this.colContactedByStaffName.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffName.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffName.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffName.Visible = true;
            this.colContactedByStaffName.VisibleIndex = 5;
            this.colContactedByStaffName.Width = 133;
            // 
            // colContactMethod
            // 
            this.colContactMethod.Caption = "Contact Method";
            this.colContactMethod.FieldName = "ContactMethod";
            this.colContactMethod.Name = "colContactMethod";
            this.colContactMethod.OptionsColumn.AllowEdit = false;
            this.colContactMethod.OptionsColumn.AllowFocus = false;
            this.colContactMethod.OptionsColumn.ReadOnly = true;
            this.colContactMethod.Visible = true;
            this.colContactMethod.VisibleIndex = 8;
            this.colContactMethod.Width = 104;
            // 
            // colContactType
            // 
            this.colContactType.Caption = "Contact Type";
            this.colContactType.FieldName = "ContactType";
            this.colContactType.Name = "colContactType";
            this.colContactType.OptionsColumn.AllowEdit = false;
            this.colContactType.OptionsColumn.AllowFocus = false;
            this.colContactType.OptionsColumn.ReadOnly = true;
            this.colContactType.Visible = true;
            this.colContactType.VisibleIndex = 9;
            this.colContactType.Width = 102;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 1;
            // 
            // colContactDirectionDescription
            // 
            this.colContactDirectionDescription.Caption = "Contact Direction";
            this.colContactDirectionDescription.FieldName = "ContactDirectionDescription";
            this.colContactDirectionDescription.Name = "colContactDirectionDescription";
            this.colContactDirectionDescription.OptionsColumn.AllowEdit = false;
            this.colContactDirectionDescription.OptionsColumn.AllowFocus = false;
            this.colContactDirectionDescription.OptionsColumn.ReadOnly = true;
            this.colContactDirectionDescription.Visible = true;
            this.colContactDirectionDescription.VisibleIndex = 10;
            this.colContactDirectionDescription.Width = 101;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Client";
            this.gridColumn5.FieldName = "ClientName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 204;
            // 
            // colLinkedRecordTypeDescription
            // 
            this.colLinkedRecordTypeDescription.Caption = "Linked Record Type";
            this.colLinkedRecordTypeDescription.FieldName = "LinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.Name = "colLinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordTypeDescription.Width = 112;
            // 
            // colDateCreated1
            // 
            this.colDateCreated1.Caption = "Date Created";
            this.colDateCreated1.ColumnEdit = this.repositoryItemTextDateTime;
            this.colDateCreated1.FieldName = "DateCreated";
            this.colDateCreated1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateCreated1.Name = "colDateCreated1";
            this.colDateCreated1.OptionsColumn.AllowEdit = false;
            this.colDateCreated1.OptionsColumn.AllowFocus = false;
            this.colDateCreated1.OptionsColumn.ReadOnly = true;
            this.colDateCreated1.Visible = true;
            this.colDateCreated1.VisibleIndex = 0;
            this.colDateCreated1.Width = 120;
            // 
            // colClientContact
            // 
            this.colClientContact.Caption = "Client Contact Person";
            this.colClientContact.FieldName = "ClientContact";
            this.colClientContact.Name = "colClientContact";
            this.colClientContact.OptionsColumn.AllowEdit = false;
            this.colClientContact.OptionsColumn.AllowFocus = false;
            this.colClientContact.OptionsColumn.ReadOnly = true;
            this.colClientContact.Visible = true;
            this.colClientContact.VisibleIndex = 6;
            this.colClientContact.Width = 122;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Callout Status Filter";
            this.popupContainerEdit2.Location = new System.Drawing.Point(98, 93);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControlLinkedGrittingCalloutsFilter;
            this.popupContainerEdit2.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit2.Size = new System.Drawing.Size(215, 20);
            this.popupContainerEdit2.StyleController = this.layoutControl2;
            this.popupContainerEdit2.TabIndex = 13;
            this.popupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btnFormatData);
            this.layoutControl2.Controls.Add(this.VisitIDsMemoEdit);
            this.layoutControl2.Controls.Add(this.checkEditColourCode);
            this.layoutControl2.Controls.Add(this.btnLoad);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Controls.Add(this.popupContainerEdit2);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(743, 334, 647, 457);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(337, 505);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // VisitIDsMemoEdit
            // 
            this.VisitIDsMemoEdit.Location = new System.Drawing.Point(24, 45);
            this.VisitIDsMemoEdit.MenuManager = this.barManager1;
            this.VisitIDsMemoEdit.Name = "VisitIDsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDsMemoEdit, true);
            this.VisitIDsMemoEdit.Size = new System.Drawing.Size(289, 384);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDsMemoEdit, optionsSpelling2);
            this.VisitIDsMemoEdit.StyleController = this.layoutControl2;
            this.VisitIDsMemoEdit.TabIndex = 17;
            // 
            // checkEditColourCode
            // 
            this.checkEditColourCode.Location = new System.Drawing.Point(98, 117);
            this.checkEditColourCode.MenuManager = this.barManager1;
            this.checkEditColourCode.Name = "checkEditColourCode";
            this.checkEditColourCode.Properties.Caption = "[Tick if Yes]";
            this.checkEditColourCode.Size = new System.Drawing.Size(76, 19);
            this.checkEditColourCode.StyleController = this.layoutControl2;
            this.checkEditColourCode.TabIndex = 15;
            this.checkEditColourCode.CheckedChanged += new System.EventHandler(this.checkEditColourCode_CheckedChanged);
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnLoad.Location = new System.Drawing.Point(248, 471);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(77, 22);
            this.btnLoad.StyleController = this.layoutControl2;
            toolTipTitleItem25.Text = "Load Data - Information";
            toolTipItem25.LeftIndent = 6;
            toolTipItem25.Text = "Click me to Load Callout Data.\r\n\r\nOnly data matching the specified Filter Criteri" +
    "a (From and To Dates and Callout Status) will be loaded.";
            superToolTip25.Items.Add(toolTipTitleItem25);
            superToolTip25.Items.Add(toolTipItem25);
            this.btnLoad.SuperTip = superToolTip25;
            this.btnLoad.TabIndex = 12;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(98, 45);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip26.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem26.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem26.Appearance.Options.UseImage = true;
            toolTipTitleItem26.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem26.Text = "Clear Date - Information";
            toolTipItem26.LeftIndent = 6;
            toolTipItem26.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip26.Items.Add(toolTipTitleItem26);
            superToolTip26.Items.Add(toolTipItem26);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, superToolTip26, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(215, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            this.dateEditFromDate.EditValueChanged += new System.EventHandler(this.dateEditFromDate_EditValueChanged);
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(98, 69);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip15.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem15.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Appearance.Options.UseImage = true;
            toolTipTitleItem15.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Text = "Clear Date - Information";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip15, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(215, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 11;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            this.dateEditToDate.EditValueChanged += new System.EventHandler(this.dateEditToDate_EditValueChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroupFilterType,
            this.layoutControlItem3,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(337, 505);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // tabbedControlGroupFilterType
            // 
            this.tabbedControlGroupFilterType.AllowHide = false;
            this.tabbedControlGroupFilterType.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroupFilterType.Name = "tabbedControlGroupFilterType";
            this.tabbedControlGroupFilterType.SelectedTabPage = this.layoutControlGroupFilterCriteria;
            this.tabbedControlGroupFilterType.SelectedTabPageIndex = 0;
            this.tabbedControlGroupFilterType.Size = new System.Drawing.Size(317, 459);
            this.tabbedControlGroupFilterType.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupFilterCriteria,
            this.layoutControlGroup4});
            // 
            // layoutControlGroupFilterCriteria
            // 
            this.layoutControlGroupFilterCriteria.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.emptySpaceItem3});
            this.layoutControlGroupFilterCriteria.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupFilterCriteria.Name = "layoutControlGroupFilterCriteria";
            this.layoutControlGroupFilterCriteria.Size = new System.Drawing.Size(293, 414);
            this.layoutControlGroupFilterCriteria.Text = "Filter Criteria";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDate;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem1.Text = "From Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDate;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem2.Text = "To Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.popupContainerEdit2;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem4.Text = "Callout Status:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.checkEditColourCode;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(154, 23);
            this.layoutControlItem5.Text = "Colour Code:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(71, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 95);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(293, 319);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(154, 72);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(139, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.ItemforformatBtn,
            this.emptySpaceItem4});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(293, 414);
            this.layoutControlGroup4.Text = "Specific Callout IDs";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.VisitIDsMemoEdit;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(293, 388);
            this.layoutControlItem6.Text = "Callout IDs:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnLoad;
            this.layoutControlItem3.Location = new System.Drawing.Point(236, 459);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(81, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 459);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(236, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp04001_GC_Job_CallOut_StatusesTableAdapter
            // 
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Gritting Callout Toolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(651, 289);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddJobs, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSend, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReassignJobs, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAuthorise, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPay, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiLoadingProgress, "", true, true, true, 86),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLoadingCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPreviewTeamPO, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiViewOnMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiFilterSelected, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount, true)});
            this.bar1.OptionsBar.AllowCollapse = true;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // bbiAddJobs
            // 
            this.bbiAddJobs.Caption = "Wizard";
            this.bbiAddJobs.Id = 42;
            this.bbiAddJobs.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddJobs.ImageOptions.Image")));
            this.bbiAddJobs.Name = "bbiAddJobs";
            this.bbiAddJobs.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiAddJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddJobs_ItemClick);
            // 
            // bsiSend
            // 
            this.bsiSend.Caption = "Send";
            this.bsiSend.Id = 52;
            this.bsiSend.ImageOptions.ImageIndex = 5;
            this.bsiSend.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSetReadyToSend1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSendJobs, true)});
            this.bsiSend.Name = "bsiSend";
            this.bsiSend.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bsiSetReadyToSend1
            // 
            this.bsiSetReadyToSend1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSetReadyToSend1.Caption = "Set <b>Ready To Send</b>";
            this.bsiSetReadyToSend1.Id = 54;
            this.bsiSetReadyToSend1.ImageOptions.ImageIndex = 6;
            this.bsiSetReadyToSend1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectSetReadyToSend),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetReadyToSend2, true)});
            this.bsiSetReadyToSend1.Name = "bsiSetReadyToSend1";
            this.bsiSetReadyToSend1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectSetReadyToSend
            // 
            this.bbiSelectSetReadyToSend.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectSetReadyToSend.Caption = "<b>Select</b> Jobs for Setting \'Ready To Send\'";
            this.bbiSelectSetReadyToSend.Id = 44;
            this.bbiSelectSetReadyToSend.ImageOptions.ImageIndex = 2;
            this.bbiSelectSetReadyToSend.Name = "bbiSelectSetReadyToSend";
            superToolTip29.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem29.Text = "Select Jobs to Set \'Ready To Send\' - Information";
            toolTipItem29.LeftIndent = 6;
            superToolTip29.Items.Add(toolTipTitleItem29);
            superToolTip29.Items.Add(toolTipItem29);
            this.bbiSelectSetReadyToSend.SuperTip = superToolTip29;
            this.bbiSelectSetReadyToSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectSetReadyToSend_ItemClick);
            // 
            // bbiSetReadyToSend2
            // 
            this.bbiSetReadyToSend2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSetReadyToSend2.Caption = "<b>Set</b> Jobs as \'Ready To Send\'";
            this.bbiSetReadyToSend2.Id = 45;
            this.bbiSetReadyToSend2.ImageOptions.ImageIndex = 6;
            this.bbiSetReadyToSend2.Name = "bbiSetReadyToSend2";
            superToolTip30.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem30.Text = "Set as \'Ready To Send\' - Information";
            toolTipItem30.LeftIndent = 6;
            superToolTip30.Items.Add(toolTipTitleItem30);
            superToolTip30.Items.Add(toolTipItem30);
            this.bbiSetReadyToSend2.SuperTip = superToolTip30;
            this.bbiSetReadyToSend2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetReadyToSend2_ItemClick);
            // 
            // bsiSendJobs
            // 
            this.bsiSendJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSendJobs.Caption = "<b>Send</b> Jobs";
            this.bsiSendJobs.Id = 53;
            this.bsiSendJobs.ImageOptions.ImageIndex = 5;
            this.bsiSendJobs.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectJobsReadyToSend),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendJobs2, true)});
            this.bsiSendJobs.Name = "bsiSendJobs";
            this.bsiSendJobs.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectJobsReadyToSend
            // 
            this.bbiSelectJobsReadyToSend.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectJobsReadyToSend.Caption = "<b>Select</b> \'Ready To Send\' Jobs";
            this.bbiSelectJobsReadyToSend.Id = 47;
            this.bbiSelectJobsReadyToSend.ImageOptions.ImageIndex = 2;
            this.bbiSelectJobsReadyToSend.Name = "bbiSelectJobsReadyToSend";
            superToolTip31.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem31.Text = "Select \'Ready To Send\' Jobs - Information";
            toolTipItem31.LeftIndent = 6;
            superToolTip31.Items.Add(toolTipTitleItem31);
            superToolTip31.Items.Add(toolTipItem31);
            this.bbiSelectJobsReadyToSend.SuperTip = superToolTip31;
            this.bbiSelectJobsReadyToSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectJobsReadyToSend_ItemClick);
            // 
            // bbiSendJobs2
            // 
            this.bbiSendJobs2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendJobs2.Caption = "<b>Send</b> Jobs";
            this.bbiSendJobs2.Id = 48;
            this.bbiSendJobs2.ImageOptions.ImageIndex = 5;
            this.bbiSendJobs2.Name = "bbiSendJobs2";
            superToolTip32.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem32.Text = "Send Jobs - Information";
            toolTipItem32.LeftIndent = 6;
            superToolTip32.Items.Add(toolTipTitleItem32);
            superToolTip32.Items.Add(toolTipItem32);
            this.bbiSendJobs2.SuperTip = superToolTip32;
            this.bbiSendJobs2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendJobs2_ItemClick);
            // 
            // bbiReassignJobs
            // 
            this.bbiReassignJobs.Caption = "Reassign";
            this.bbiReassignJobs.Id = 29;
            this.bbiReassignJobs.ImageOptions.ImageIndex = 9;
            this.bbiReassignJobs.Name = "bbiReassignJobs";
            this.bbiReassignJobs.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip18.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem18.Text = "Reassign Jobs - Information";
            toolTipItem18.LeftIndent = 6;
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            this.bbiReassignJobs.SuperTip = superToolTip18;
            this.bbiReassignJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReassignJobs_ItemClick);
            // 
            // bbiAuthorise
            // 
            this.bbiAuthorise.Caption = "Authorise";
            this.bbiAuthorise.Id = 49;
            this.bbiAuthorise.ImageOptions.ImageIndex = 10;
            this.bbiAuthorise.Name = "bbiAuthorise";
            this.bbiAuthorise.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip19.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem19.Text = "Authorise Payment - Information";
            toolTipItem19.LeftIndent = 6;
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            this.bbiAuthorise.SuperTip = superToolTip19;
            this.bbiAuthorise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAuthorise_ItemClick);
            // 
            // bbiPay
            // 
            this.bbiPay.Caption = "Pay";
            this.bbiPay.Id = 32;
            this.bbiPay.ImageOptions.Image = global::WoodPlan5.Properties.Resources.team_invoice_32x32;
            this.bbiPay.Name = "bbiPay";
            this.bbiPay.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip20.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem20.Text = "Pay - Information";
            toolTipItem20.LeftIndent = 6;
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem20);
            this.bbiPay.SuperTip = superToolTip20;
            this.bbiPay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPay_ItemClick);
            // 
            // beiLoadingProgress
            // 
            this.beiLoadingProgress.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.beiLoadingProgress.Caption = "Loading Data Progress";
            this.beiLoadingProgress.Edit = this.repositoryItemMarqueeProgressBar1;
            this.beiLoadingProgress.EditValue = "Loading Data...";
            this.beiLoadingProgress.Id = 58;
            this.beiLoadingProgress.Name = "beiLoadingProgress";
            this.beiLoadingProgress.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemMarqueeProgressBar1
            // 
            this.repositoryItemMarqueeProgressBar1.MarqueeWidth = 20;
            this.repositoryItemMarqueeProgressBar1.Name = "repositoryItemMarqueeProgressBar1";
            this.repositoryItemMarqueeProgressBar1.ShowTitle = true;
            // 
            // bbiLoadingCancel
            // 
            this.bbiLoadingCancel.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiLoadingCancel.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiLoadingCancel.Caption = "Cancel Load";
            this.bbiLoadingCancel.Id = 57;
            this.bbiLoadingCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLoadingCancel.ImageOptions.Image")));
            this.bbiLoadingCancel.Name = "bbiLoadingCancel";
            toolTipTitleItem21.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem21.Appearance.Options.UseImage = true;
            toolTipTitleItem21.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem21.Text = "Cancel Load - Information";
            toolTipItem21.LeftIndent = 6;
            toolTipItem21.Text = "Click me to cancel the current data loading operation.";
            superToolTip21.Items.Add(toolTipTitleItem21);
            superToolTip21.Items.Add(toolTipItem21);
            this.bbiLoadingCancel.SuperTip = superToolTip21;
            this.bbiLoadingCancel.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiLoadingCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadingCancel_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 56;
            this.bbiRefresh.ImageOptions.ImageIndex = 12;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiPreviewTeamPO
            // 
            this.bbiPreviewTeamPO.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiPreviewTeamPO.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiPreviewTeamPO.Caption = "Preview Team P.O.";
            this.bbiPreviewTeamPO.DropDownControl = this.pmEditPOLayout;
            this.bbiPreviewTeamPO.Id = 50;
            this.bbiPreviewTeamPO.ImageOptions.ImageIndex = 14;
            this.bbiPreviewTeamPO.Name = "bbiPreviewTeamPO";
            this.bbiPreviewTeamPO.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip23.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem23.Text = "Preview Team P.O. - Information";
            toolTipItem23.LeftIndent = 6;
            toolTipItem23.Text = "Click me to preview the selected snow clearance callout in Team Purchase Order fo" +
    "rmat.";
            superToolTip23.Items.Add(toolTipTitleItem23);
            superToolTip23.Items.Add(toolTipItem23);
            this.bbiPreviewTeamPO.SuperTip = superToolTip23;
            this.bbiPreviewTeamPO.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPreviewTeamPO_ItemClick);
            // 
            // pmEditPOLayout
            // 
            this.pmEditPOLayout.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditTeamPOLayout)});
            this.pmEditPOLayout.Manager = this.barManager1;
            this.pmEditPOLayout.Name = "pmEditPOLayout";
            // 
            // bbiEditTeamPOLayout
            // 
            this.bbiEditTeamPOLayout.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiEditTeamPOLayout.Caption = "<b>Edit</b> Team P.O. <b>Layout</b>";
            this.bbiEditTeamPOLayout.Id = 51;
            this.bbiEditTeamPOLayout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEditTeamPOLayout.ImageOptions.Image")));
            this.bbiEditTeamPOLayout.Name = "bbiEditTeamPOLayout";
            toolTipTitleItem22.Text = "Edit Team P.O. Layout - Information";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "Click me to load the Team P.O. Layout into the Report Builder for editing.";
            superToolTip22.Items.Add(toolTipTitleItem22);
            superToolTip22.Items.Add(toolTipItem22);
            this.bbiEditTeamPOLayout.SuperTip = superToolTip22;
            this.bbiEditTeamPOLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditTeamPOLayout_ItemClick);
            // 
            // bsiViewOnMap
            // 
            this.bsiViewOnMap.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiViewOnMap.Caption = "View On Map";
            this.bsiViewOnMap.Id = 36;
            this.bsiViewOnMap.ImageOptions.ImageIndex = 8;
            this.bsiViewOnMap.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewSiteOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewJobOnMap, true)});
            this.bsiViewOnMap.Name = "bsiViewOnMap";
            this.bsiViewOnMap.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiViewSiteOnMap
            // 
            this.bbiViewSiteOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewSiteOnMap.Caption = "View <b>Site</b> On Map";
            this.bbiViewSiteOnMap.Id = 30;
            this.bbiViewSiteOnMap.ImageOptions.ImageIndex = 8;
            this.bbiViewSiteOnMap.Name = "bbiViewSiteOnMap";
            superToolTip16.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem16.Text = "View Site On Map - Information";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Click me to view the Site location within Google Maps.\r\n\r\n<b>Note:</b> An Interne" +
    "t connection is required for this.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            this.bbiViewSiteOnMap.SuperTip = superToolTip16;
            this.bbiViewSiteOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewSiteOnMap_ItemClick);
            // 
            // bbiViewJobOnMap
            // 
            this.bbiViewJobOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewJobOnMap.Caption = "View <b>Callout</b> Start and Finish Position On Map";
            this.bbiViewJobOnMap.Enabled = false;
            this.bbiViewJobOnMap.Id = 37;
            this.bbiViewJobOnMap.ImageOptions.ImageIndex = 8;
            this.bbiViewJobOnMap.Name = "bbiViewJobOnMap";
            superToolTip27.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem27.Text = "View Callout Start and Finish Position on Map - Information";
            toolTipItem27.LeftIndent = 6;
            toolTipItem27.Text = "Click me to view the Starting and Finishing locations (as submitted by the Device) f" +
    "or the callout within Google Maps.\r\n\r\n<b>Note:</b> An Internet connection is req" +
    "uired for this.";
            superToolTip27.Items.Add(toolTipTitleItem27);
            superToolTip27.Items.Add(toolTipItem27);
            this.bbiViewJobOnMap.SuperTip = superToolTip27;
            this.bbiViewJobOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewJobOnMap_ItemClick);
            // 
            // bsiFilterSelected
            // 
            this.bsiFilterSelected.Caption = "Filter Selected";
            this.bsiFilterSelected.Id = 59;
            this.bsiFilterSelected.ImageOptions.ImageIndex = 13;
            this.bsiFilterSelected.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterVisitsSelected)});
            this.bsiFilterSelected.Name = "bsiFilterSelected";
            this.bsiFilterSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // bciFilterVisitsSelected
            // 
            this.bciFilterVisitsSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterVisitsSelected.Caption = "Filter Selected <b>Callouts</b>";
            this.bciFilterVisitsSelected.Id = 60;
            this.bciFilterVisitsSelected.ImageOptions.ImageIndex = 13;
            this.bciFilterVisitsSelected.Name = "bciFilterVisitsSelected";
            this.bciFilterVisitsSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bciFilterVisitsSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterVisitsSelected_CheckedChanged);
            // 
            // bsiSelectedCount
            // 
            this.bsiSelectedCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount.Caption = "0 Callouts Selected";
            this.bsiSelectedCount.Id = 55;
            this.bsiSelectedCount.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount.Name = "bsiSelectedCount";
            this.bsiSelectedCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiSetReadyToSend1
            // 
            this.bbiSetReadyToSend1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiSetReadyToSend1.Caption = "Set as \'Ready To Send\'";
            this.bbiSetReadyToSend1.Id = 43;
            this.bbiSetReadyToSend1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSetReadyToSend1.ImageOptions.Image")));
            this.bbiSetReadyToSend1.Name = "bbiSetReadyToSend1";
            this.bbiSetReadyToSend1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip28.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem28.Text = "Set as \'Ready To Send\' - Information";
            toolTipItem28.LeftIndent = 6;
            superToolTip28.Items.Add(toolTipTitleItem28);
            superToolTip28.Items.Add(toolTipItem28);
            this.bbiSetReadyToSend1.SuperTip = superToolTip28;
            this.bbiSetReadyToSend1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetReadyToSend1_ItemClick);
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // sp04160_GC_Snow_Callout_ManagerTableAdapter
            // 
            this.sp04160_GC_Snow_Callout_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp04161_GC_Snow_Clearance_Callout_Linked_Extra_CostsTableAdapter
            // 
            this.sp04161_GC_Snow_Clearance_Callout_Linked_Extra_CostsTableAdapter.ClearBeforeFill = true;
            // 
            // sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter
            // 
            this.sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp05089_CRM_Contacts_Linked_To_RecordTableAdapter
            // 
            this.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // sp04340_GC_Service_Areas_Linked_To_Snow_CalloutTableAdapter
            // 
            this.sp04340_GC_Service_Areas_Linked_To_Snow_CalloutTableAdapter.ClearBeforeFill = true;
            // 
            // sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter
            // 
            this.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(0, "route_optimize_send.png");
            this.imageCollection2.Images.SetKeyName(1, "route_optimize_get.png");
            this.imageCollection2.InsertGalleryImage("contentarrangeinrows_32x32.png", "images/alignment/contentarrangeinrows_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/alignment/contentarrangeinrows_32x32.png"), 2);
            this.imageCollection2.Images.SetKeyName(2, "contentarrangeinrows_32x32.png");
            this.imageCollection2.Images.SetKeyName(3, "completion_sheet_32.png");
            this.imageCollection2.Images.SetKeyName(4, "Visit_Manually_Complete_32x32.png");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_send_32x32, "team_send_32x32", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection2.Images.SetKeyName(5, "team_send_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_send2_32x32, "team_send2_32x32", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection2.Images.SetKeyName(6, "team_send2_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_send_disabled_32x32, "team_send_disabled_32x32", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection2.Images.SetKeyName(7, "team_send_disabled_32x32");
            this.imageCollection2.InsertGalleryImage("geopointmap_32x32.png", "images/maps/geopointmap_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopointmap_32x32.png"), 8);
            this.imageCollection2.Images.SetKeyName(8, "geopointmap_32x32.png");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_reassign_32, "team_reassign_32", typeof(global::WoodPlan5.Properties.Resources), 9);
            this.imageCollection2.Images.SetKeyName(9, "team_reassign_32");
            this.imageCollection2.InsertGalleryImage("apply_32x32.png", "images/actions/apply_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_32x32.png"), 10);
            this.imageCollection2.Images.SetKeyName(10, "apply_32x32.png");
            this.imageCollection2.InsertGalleryImage("knowledgebasearticle_32x32.png", "images/support/knowledgebasearticle_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/knowledgebasearticle_32x32.png"), 11);
            this.imageCollection2.Images.SetKeyName(11, "knowledgebasearticle_32x32.png");
            this.imageCollection2.InsertGalleryImage("refresh_32x32.png", "images/actions/refresh_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_32x32.png"), 12);
            this.imageCollection2.Images.SetKeyName(12, "refresh_32x32.png");
            this.imageCollection2.InsertGalleryImage("masterfilter_32x32.png", "images/filter/masterfilter_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter/masterfilter_32x32.png"), 13);
            this.imageCollection2.Images.SetKeyName(13, "masterfilter_32x32.png");
            this.imageCollection2.InsertGalleryImage("preview_32x32.png", "images/print/preview_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_32x32.png"), 14);
            this.imageCollection2.Images.SetKeyName(14, "preview_32x32.png");
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.ID = new System.Guid("fd10b225-7d8a-428e-bece-f0c8a5bc706f");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(344, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(344, 537);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(337, 505);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // btnFormatData
            // 
            this.btnFormatData.Location = new System.Drawing.Point(244, 433);
            this.btnFormatData.Name = "btnFormatData";
            this.btnFormatData.Size = new System.Drawing.Size(69, 22);
            this.btnFormatData.StyleController = this.layoutControl2;
            toolTipTitleItem24.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem24.Text = "Format data - Information";
            toolTipItem24.LeftIndent = 6;
            toolTipItem24.Text = "Click me to convert carriage returns into commas and clean up any spaces within t" +
    "he Specific Callout IDs box.";
            superToolTip24.Items.Add(toolTipTitleItem24);
            superToolTip24.Items.Add(toolTipItem24);
            this.btnFormatData.SuperTip = superToolTip24;
            this.btnFormatData.TabIndex = 18;
            this.btnFormatData.Text = "Format Data";
            this.btnFormatData.Click += new System.EventHandler(this.btnFormatData_Click);
            // 
            // ItemforformatBtn
            // 
            this.ItemforformatBtn.Control = this.btnFormatData;
            this.ItemforformatBtn.Location = new System.Drawing.Point(220, 388);
            this.ItemforformatBtn.MaxSize = new System.Drawing.Size(73, 26);
            this.ItemforformatBtn.MinSize = new System.Drawing.Size(73, 26);
            this.ItemforformatBtn.Name = "ItemforformatBtn";
            this.ItemforformatBtn.Size = new System.Drawing.Size(73, 26);
            this.ItemforformatBtn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemforformatBtn.TextSize = new System.Drawing.Size(0, 0);
            this.ItemforformatBtn.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 388);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(220, 26);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 537);
            // 
            // frm_GC_Snow_Callout_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1313, 537);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Snow_Callout_Manager";
            this.Text = "SNOW CLEARANCE Callout Manager";
            this.Activated += new System.EventHandler(this.frm_GC_Snow_Callout_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Snow_Callout_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Snow_Callout_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditTeamPOFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04160GCSnowCalloutManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).EndInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04161GCSnowClearanceCalloutLinkedExtraCostsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP3)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04340GCServiceAreasLinkedToSnowCalloutBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05089CRMContactsLinkedToRecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditColourCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilterType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFilterCriteria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditPOLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemforformatBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLinkedGrittingCalloutsFilter;
        private DevExpress.XtraEditors.SimpleButton btnGritCalloutFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private System.Windows.Forms.BindingSource sp04001GCJobCallOutStatusesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter sp04001_GC_Job_CallOut_StatusesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraCostID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraGrid.Columns.GridColumn colVatRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromDefault;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiReassignJobs;
        private DevExpress.XtraBars.BarButtonItem bbiViewSiteOnMap;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraBars.BarButtonItem bbiPay;
        private DevExpress.XtraBars.BarSubItem bsiViewOnMap;
        private DevExpress.XtraBars.BarButtonItem bbiViewJobOnMap;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency4;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamAddressLine11;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private System.Windows.Forms.BindingSource sp04160GCSnowCalloutManagerBindingSource;
        private DataSet_GC_Snow_Core dataSet_GC_Snow_Core;
        private DataSet_GC_Snow_CoreTableAdapters.sp04160_GC_Snow_Callout_ManagerTableAdapter sp04160_GC_Snow_Callout_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutID;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCPONumberSuffix;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colGritMobileTelephoneNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorETA;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitAborted;
        private DevExpress.XtraGrid.Columns.GridColumn colAbortedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCost;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherSell;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorContactedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByName;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractorReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClientReason;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colProfit;
        private DevExpress.XtraGrid.Columns.GridColumn colMarkup;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardSell;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientHowSoon;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraBars.BarButtonItem bbiAddJobs;
        private WoodPlan5.DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp04161GCSnowClearanceCalloutLinkedExtraCostsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colChargedPerHour;
        private DevExpress.XtraGrid.Columns.GridColumn colHours;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell1;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitSell;
        private DataSet_GC_Snow_CoreTableAdapters.sp04161_GC_Snow_Clearance_Callout_Linked_Extra_CostsTableAdapter sp04161_GC_Snow_Clearance_Callout_Linked_Extra_CostsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency3;
        private System.Windows.Forms.BindingSource sp04162GCSnowClearanceCalloutLinkedRatesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDayTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDayTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colHours1;
        private DevExpress.XtraGrid.Columns.GridColumn colRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutRateID;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSiteContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost2;
        private DataSet_GC_Snow_CoreTableAdapters.sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP3;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccessAbortedRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditTime;
        private DevExpress.XtraBars.BarButtonItem bbiSetReadyToSend1;
        private DevExpress.XtraBars.BarButtonItem bbiSelectSetReadyToSend;
        private DevExpress.XtraBars.BarButtonItem bbiSetReadyToSend2;
        private DevExpress.XtraBars.BarButtonItem bbiSelectJobsReadyToSend;
        private DevExpress.XtraBars.BarButtonItem bbiSendJobs2;
        private DevExpress.XtraBars.BarButtonItem bbiAuthorise;
        private DevExpress.XtraBars.BarButtonItem bbiPreviewTeamPO;
        private DevExpress.XtraBars.BarButtonItem bbiEditTeamPOLayout;
        private DevExpress.XtraBars.PopupMenu pmEditPOLayout;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamPOFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colTimesheetSubmitted;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedBySubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedBySubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colTimesheetNumber;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByWebsite;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceLineID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceTeamPaymentExported;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl15;
        private System.Windows.Forms.BindingSource sp05089CRMContactsLinkedToRecordBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraGrid.Columns.GridColumn colCRMID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDueDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMadeDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colContactType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private WoodPlanDataSetTableAdapters.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter sp05089_CRM_Contacts_Linked_To_RecordTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContact;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineCount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInt;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineCount2;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineCount3;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineCount4;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineRate2;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineRate3;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineRate4;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private System.Windows.Forms.BindingSource sp04340GCServiceAreasLinkedToSnowCalloutBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteServiceAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptionID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colWasServiced;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromDefault1;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCalloutID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceServicedSiteID;
        private DataSet_GC_Snow_CoreTableAdapters.sp04340_GC_Service_Areas_Linked_To_Snow_CalloutTableAdapter sp04340_GC_Service_Areas_Linked_To_Snow_CalloutTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteManagerName;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCallOutID2;
        private DevExpress.XtraGrid.Columns.GridColumn colPicturePath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeTaken;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private System.Windows.Forms.BindingSource sp04092GCGrittingCalloutPicturesForCalloutBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colHours2;
        private DevExpress.XtraGrid.Columns.GridColumn colHours3;
        private DevExpress.XtraGrid.Columns.GridColumn colHours4;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours2;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours3;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedOnPDA;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraBars.BarSubItem bsiSend;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraBars.BarSubItem bsiSendJobs;
        private DevExpress.XtraBars.BarSubItem bsiSetReadyToSend1;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiLoadingCancel;
        private DevExpress.XtraBars.BarEditItem beiLoadingProgress;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar repositoryItemMarqueeProgressBar1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.CheckEdit checkEditColourCode;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupFilterType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupFilterCriteria;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.MemoEdit VisitIDsMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraBars.BarSubItem bsiFilterSelected;
        private DevExpress.XtraBars.BarCheckItem bciFilterVisitsSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditTeamPOFile;
        private DevExpress.XtraEditors.SimpleButton btnFormatData;
        private DevExpress.XtraLayout.LayoutControlItem ItemforformatBtn;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
    }
}

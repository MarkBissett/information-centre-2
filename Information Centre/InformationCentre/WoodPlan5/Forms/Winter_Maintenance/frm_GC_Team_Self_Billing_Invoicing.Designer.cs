﻿namespace WoodPlan5
{
    partial class frm_GC_Team_Self_Billing_Invoicing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Team_Self_Billing_Invoicing));
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04119GCAuthorisationTeamsSelfBillingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubContractorGritInformationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colHoldsStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25kgBags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colWarningGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUrgentGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStockSuppliedByDepotID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellBillingInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingFrequencyDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingFrequencyDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsDirectLabour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colHourlyProactiveRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHourlyReactiveRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUninvoicedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp04120GCAuthorisationTeamsNonSelfBillingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25kgbags2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedJobCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlCompanies = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCompanyFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04237GCCompanyFilterListBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04121GCCalloutsForInvoicingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGrittingCallOutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGrittingContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorIsDirectLabour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContarctorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colImportedWeatherForecastID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextSentTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorReceivedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorRespondedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorETA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitAborted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbortedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishedLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishedLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25kgbags3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSaltCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSaltSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditVATRate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHoursWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamHourlyRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamCharge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractorReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSourceLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSourceLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteWeather = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTemperature = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamPresent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceFailure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritJobTransferMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarkup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClientReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritMobileTelephoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.standaloneBarDockControl3 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp04131GCTeamSelfBillingInvoicesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedCalloutCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp04272GCCalloutsForInvoiceHeadersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25KgBags4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellBillingInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiAuthorise1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmAuthorise = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiAuthorise2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUnAuthorise = new DevExpress.XtraBars.BarButtonItem();
            this.bbiQuery1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmQuery = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiQuery2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiQueryClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDontPay1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmDoNotPay = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiDontPay2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDoPay = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemCompanyFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditCompanyFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemShowQueries = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditShowQueries = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barEditItemShowDontPay = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditShowDontPay = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiReloadCalloutData = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bbiInvoice1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmInvoice = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiInvoiceSelectRecords = new DevExpress.XtraBars.BarButtonItem();
            this.bbiInvoice2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditLayout = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReloadTeamData = new DevExpress.XtraBars.BarButtonItem();
            this.barLinkContainerItem2 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barLinkContainerItem3 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp04119_GC_Authorisation_Teams_Self_BillingTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04119_GC_Authorisation_Teams_Self_BillingTableAdapter();
            this.sp04120_GC_Authorisation_Teams_Non_Self_BillingTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04120_GC_Authorisation_Teams_Non_Self_BillingTableAdapter();
            this.sp04121_GC_Callouts_For_InvoicingTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04121_GC_Callouts_For_InvoicingTableAdapter();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.sp04131_GC_Team_Self_Billing_InvoicesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04131_GC_Team_Self_Billing_InvoicesTableAdapter();
            this.sp04237GCCompanyFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp04237_GC_Company_Filter_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter();
            this.sp04272_GC_Callouts_For_Invoice_HeadersTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04272_GC_Callouts_For_Invoice_HeadersTableAdapter();
            this.pmCalloutsList = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiSelectTeamsOnSelectedCallouts = new DevExpress.XtraBars.BarButtonItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.beiFromDate = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEditFrom = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.beiToDate = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEditTo = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bbiLoadData = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRegeneratePDF = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEmail = new DevExpress.XtraBars.BarButtonItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04119GCAuthorisationTeamsSelfBillingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgBags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04120GCAuthorisationTeamsNonSelfBillingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgbags2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).BeginInit();
            this.popupContainerControlCompanies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04121GCCalloutsForInvoicingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgbags3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVATRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04131GCTeamSelfBillingInvoicesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04272GCCalloutsForInvoiceHeadersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmAuthorise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmQuery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDoNotPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowQueries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowDontPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmCalloutsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditFrom.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditTo.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1129, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 515);
            this.barDockControlBottom.Size = new System.Drawing.Size(1129, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 515);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1129, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 515);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar4});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl3);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAuthorise2,
            this.bbiUnAuthorise,
            this.barSubItem2,
            this.bbiQuery2,
            this.barButtonItem4,
            this.bbiDontPay2,
            this.bbiDoPay,
            this.bbiQueryClear,
            this.bbiAuthorise1,
            this.bbiQuery1,
            this.bbiDontPay1,
            this.bbiInvoice1,
            this.bbiInvoice2,
            this.bbiInvoiceSelectRecords,
            this.bbiReloadCalloutData,
            this.barLinkContainerItem2,
            this.barLinkContainerItem3,
            this.barCheckItem1,
            this.barEditItemShowQueries,
            this.barEditItemShowDontPay,
            this.bbiReloadTeamData,
            this.bbiEditLayout,
            this.barEditItemCompanyFilter,
            this.bbiSelectTeamsOnSelectedCallouts,
            this.beiFromDate,
            this.beiToDate,
            this.bbiLoadData,
            this.bbiRegeneratePDF,
            this.bbiEmail});
            this.barManager1.MaxItemId = 59;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEditShowQueries,
            this.repositoryItemCheckEditShowDontPay,
            this.repositoryItemPopupContainerEditCompanyFilter,
            this.repositoryItemDateEditFrom,
            this.repositoryItemDateEditTo});
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1129, 515);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1124, 489);
            this.xtraTabPage1.Text = "Authorisation";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel 1";
            this.splitContainerControl1.Panel2.Controls.Add(this.popupContainerControlCompanies);
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl3);
            this.splitContainerControl1.Panel2.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1124, 489);
            this.splitContainerControl1.SplitterPosition = 368;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.Controls.Add(this.standaloneBarDockControl2);
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Self-Billing Invoice Teams";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "NON Self-Billing Teams";
            this.splitContainerControl2.Size = new System.Drawing.Size(368, 489);
            this.splitContainerControl2.SplitterPosition = 290;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl2.Manager = this.barManager1;
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(364, 42);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp04119GCAuthorisationTeamsSelfBillingBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 43);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEdit25kgBags});
            this.gridControl1.Size = new System.Drawing.Size(364, 223);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04119GCAuthorisationTeamsSelfBillingBindingSource
            // 
            this.sp04119GCAuthorisationTeamsSelfBillingBindingSource.DataMember = "sp04119_GC_Authorisation_Teams_Self_Billing";
            this.sp04119GCAuthorisationTeamsSelfBillingBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubContractorGritInformationID,
            this.colSubContractorID,
            this.colGrittingActive,
            this.colHoldsStock,
            this.colCurrentGritLevel,
            this.colWarningGritLevel,
            this.colUrgentGritLevel,
            this.colStockSuppliedByDepotID,
            this.colSellBillingInvoice,
            this.colSelfBillingFrequency,
            this.colSelfBillingFrequencyDescriptorID,
            this.colSelfBillingFrequencyDescriptor,
            this.colIsDirectLabour,
            this.colRemarks,
            this.colHourlyProactiveRate,
            this.colHourlyReactiveRate,
            this.colTeamName,
            this.colAddressLine1,
            this.colLastInvoiceDate,
            this.colNextInvoiceDate,
            this.colInvoiceCount,
            this.colUninvoicedJobCount,
            this.colAuthorisedJobCount,
            this.colEmailPassword});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSubContractorGritInformationID
            // 
            this.colSubContractorGritInformationID.Caption = "Sub-Contractor Grit Information ID";
            this.colSubContractorGritInformationID.FieldName = "SubContractorGritInformationID";
            this.colSubContractorGritInformationID.Name = "colSubContractorGritInformationID";
            this.colSubContractorGritInformationID.OptionsColumn.AllowEdit = false;
            this.colSubContractorGritInformationID.OptionsColumn.AllowFocus = false;
            this.colSubContractorGritInformationID.OptionsColumn.ReadOnly = true;
            this.colSubContractorGritInformationID.Width = 188;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Sub-Contractor ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            this.colSubContractorID.Width = 109;
            // 
            // colGrittingActive
            // 
            this.colGrittingActive.Caption = "Gritting Active";
            this.colGrittingActive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colGrittingActive.FieldName = "GrittingActive";
            this.colGrittingActive.Name = "colGrittingActive";
            this.colGrittingActive.OptionsColumn.AllowEdit = false;
            this.colGrittingActive.OptionsColumn.AllowFocus = false;
            this.colGrittingActive.OptionsColumn.ReadOnly = true;
            this.colGrittingActive.Visible = true;
            this.colGrittingActive.VisibleIndex = 1;
            this.colGrittingActive.Width = 89;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colHoldsStock
            // 
            this.colHoldsStock.Caption = "Holds Stock";
            this.colHoldsStock.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colHoldsStock.FieldName = "HoldsStock";
            this.colHoldsStock.Name = "colHoldsStock";
            this.colHoldsStock.OptionsColumn.AllowEdit = false;
            this.colHoldsStock.OptionsColumn.AllowFocus = false;
            this.colHoldsStock.OptionsColumn.ReadOnly = true;
            this.colHoldsStock.Visible = true;
            this.colHoldsStock.VisibleIndex = 9;
            this.colHoldsStock.Width = 76;
            // 
            // colCurrentGritLevel
            // 
            this.colCurrentGritLevel.Caption = "Current Grit Level";
            this.colCurrentGritLevel.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colCurrentGritLevel.FieldName = "CurrentGritLevel";
            this.colCurrentGritLevel.Name = "colCurrentGritLevel";
            this.colCurrentGritLevel.OptionsColumn.AllowEdit = false;
            this.colCurrentGritLevel.OptionsColumn.AllowFocus = false;
            this.colCurrentGritLevel.OptionsColumn.ReadOnly = true;
            this.colCurrentGritLevel.Visible = true;
            this.colCurrentGritLevel.VisibleIndex = 10;
            this.colCurrentGritLevel.Width = 106;
            // 
            // repositoryItemTextEdit25kgBags
            // 
            this.repositoryItemTextEdit25kgBags.AutoHeight = false;
            this.repositoryItemTextEdit25kgBags.Mask.EditMask = "######0.00 25kg Bags";
            this.repositoryItemTextEdit25kgBags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25kgBags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25kgBags.Name = "repositoryItemTextEdit25kgBags";
            // 
            // colWarningGritLevel
            // 
            this.colWarningGritLevel.Caption = "Warning Grit Level";
            this.colWarningGritLevel.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colWarningGritLevel.FieldName = "WarningGritLevel";
            this.colWarningGritLevel.Name = "colWarningGritLevel";
            this.colWarningGritLevel.OptionsColumn.AllowEdit = false;
            this.colWarningGritLevel.OptionsColumn.AllowFocus = false;
            this.colWarningGritLevel.OptionsColumn.ReadOnly = true;
            this.colWarningGritLevel.Visible = true;
            this.colWarningGritLevel.VisibleIndex = 11;
            this.colWarningGritLevel.Width = 109;
            // 
            // colUrgentGritLevel
            // 
            this.colUrgentGritLevel.Caption = "Urgent Grit Level";
            this.colUrgentGritLevel.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colUrgentGritLevel.FieldName = "UrgentGritLevel";
            this.colUrgentGritLevel.Name = "colUrgentGritLevel";
            this.colUrgentGritLevel.OptionsColumn.AllowEdit = false;
            this.colUrgentGritLevel.OptionsColumn.AllowFocus = false;
            this.colUrgentGritLevel.OptionsColumn.ReadOnly = true;
            this.colUrgentGritLevel.Visible = true;
            this.colUrgentGritLevel.VisibleIndex = 12;
            this.colUrgentGritLevel.Width = 102;
            // 
            // colStockSuppliedByDepotID
            // 
            this.colStockSuppliedByDepotID.Caption = "Stock Supplied By Depot ID";
            this.colStockSuppliedByDepotID.FieldName = "StockSuppliedByDepotID";
            this.colStockSuppliedByDepotID.Name = "colStockSuppliedByDepotID";
            this.colStockSuppliedByDepotID.OptionsColumn.AllowEdit = false;
            this.colStockSuppliedByDepotID.OptionsColumn.AllowFocus = false;
            this.colStockSuppliedByDepotID.OptionsColumn.ReadOnly = true;
            this.colStockSuppliedByDepotID.Width = 151;
            // 
            // colSellBillingInvoice
            // 
            this.colSellBillingInvoice.Caption = "Self-Billing Invoice";
            this.colSellBillingInvoice.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSellBillingInvoice.FieldName = "SellBillingInvoice";
            this.colSellBillingInvoice.Name = "colSellBillingInvoice";
            this.colSellBillingInvoice.OptionsColumn.AllowEdit = false;
            this.colSellBillingInvoice.OptionsColumn.AllowFocus = false;
            this.colSellBillingInvoice.OptionsColumn.ReadOnly = true;
            this.colSellBillingInvoice.Width = 107;
            // 
            // colSelfBillingFrequency
            // 
            this.colSelfBillingFrequency.Caption = "Billing Frequency";
            this.colSelfBillingFrequency.FieldName = "SelfBillingFrequency";
            this.colSelfBillingFrequency.Name = "colSelfBillingFrequency";
            this.colSelfBillingFrequency.OptionsColumn.AllowEdit = false;
            this.colSelfBillingFrequency.OptionsColumn.AllowFocus = false;
            this.colSelfBillingFrequency.OptionsColumn.ReadOnly = true;
            this.colSelfBillingFrequency.Visible = true;
            this.colSelfBillingFrequency.VisibleIndex = 4;
            this.colSelfBillingFrequency.Width = 101;
            // 
            // colSelfBillingFrequencyDescriptorID
            // 
            this.colSelfBillingFrequencyDescriptorID.Caption = "Frequency Descriptor ID";
            this.colSelfBillingFrequencyDescriptorID.FieldName = "SelfBillingFrequencyDescriptorID";
            this.colSelfBillingFrequencyDescriptorID.Name = "colSelfBillingFrequencyDescriptorID";
            this.colSelfBillingFrequencyDescriptorID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingFrequencyDescriptorID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingFrequencyDescriptorID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingFrequencyDescriptorID.Width = 138;
            // 
            // colSelfBillingFrequencyDescriptor
            // 
            this.colSelfBillingFrequencyDescriptor.Caption = "Frequency Descriptor";
            this.colSelfBillingFrequencyDescriptor.FieldName = "SelfBillingFrequencyDescriptor";
            this.colSelfBillingFrequencyDescriptor.Name = "colSelfBillingFrequencyDescriptor";
            this.colSelfBillingFrequencyDescriptor.OptionsColumn.AllowEdit = false;
            this.colSelfBillingFrequencyDescriptor.OptionsColumn.AllowFocus = false;
            this.colSelfBillingFrequencyDescriptor.OptionsColumn.ReadOnly = true;
            this.colSelfBillingFrequencyDescriptor.Visible = true;
            this.colSelfBillingFrequencyDescriptor.VisibleIndex = 5;
            this.colSelfBillingFrequencyDescriptor.Width = 124;
            // 
            // colIsDirectLabour
            // 
            this.colIsDirectLabour.Caption = "Is Direct Labour";
            this.colIsDirectLabour.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsDirectLabour.FieldName = "IsDirectLabour";
            this.colIsDirectLabour.Name = "colIsDirectLabour";
            this.colIsDirectLabour.OptionsColumn.AllowEdit = false;
            this.colIsDirectLabour.OptionsColumn.AllowFocus = false;
            this.colIsDirectLabour.OptionsColumn.ReadOnly = true;
            this.colIsDirectLabour.Width = 97;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colHourlyProactiveRate
            // 
            this.colHourlyProactiveRate.Caption = "Hourly Proactive Rate";
            this.colHourlyProactiveRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colHourlyProactiveRate.FieldName = "HourlyProactiveRate";
            this.colHourlyProactiveRate.Name = "colHourlyProactiveRate";
            this.colHourlyProactiveRate.OptionsColumn.AllowEdit = false;
            this.colHourlyProactiveRate.OptionsColumn.AllowFocus = false;
            this.colHourlyProactiveRate.OptionsColumn.ReadOnly = true;
            this.colHourlyProactiveRate.Width = 126;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colHourlyReactiveRate
            // 
            this.colHourlyReactiveRate.Caption = "Hourly Reactive Rate";
            this.colHourlyReactiveRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colHourlyReactiveRate.FieldName = "HourlyReactiveRate";
            this.colHourlyReactiveRate.Name = "colHourlyReactiveRate";
            this.colHourlyReactiveRate.OptionsColumn.AllowEdit = false;
            this.colHourlyReactiveRate.OptionsColumn.AllowFocus = false;
            this.colHourlyReactiveRate.OptionsColumn.ReadOnly = true;
            this.colHourlyReactiveRate.Width = 123;
            // 
            // colTeamName
            // 
            this.colTeamName.Caption = "Team Name";
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.Visible = true;
            this.colTeamName.VisibleIndex = 0;
            this.colTeamName.Width = 177;
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Width = 91;
            // 
            // colLastInvoiceDate
            // 
            this.colLastInvoiceDate.Caption = "Last Invoice Date";
            this.colLastInvoiceDate.FieldName = "LastInvoiceDate";
            this.colLastInvoiceDate.Name = "colLastInvoiceDate";
            this.colLastInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colLastInvoiceDate.OptionsColumn.AllowFocus = false;
            this.colLastInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colLastInvoiceDate.Visible = true;
            this.colLastInvoiceDate.VisibleIndex = 2;
            this.colLastInvoiceDate.Width = 105;
            // 
            // colNextInvoiceDate
            // 
            this.colNextInvoiceDate.Caption = "Next Due Date";
            this.colNextInvoiceDate.FieldName = "NextInvoiceDate";
            this.colNextInvoiceDate.Name = "colNextInvoiceDate";
            this.colNextInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colNextInvoiceDate.OptionsColumn.AllowFocus = false;
            this.colNextInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colNextInvoiceDate.Visible = true;
            this.colNextInvoiceDate.VisibleIndex = 3;
            this.colNextInvoiceDate.Width = 92;
            // 
            // colInvoiceCount
            // 
            this.colInvoiceCount.Caption = "Invoice Count";
            this.colInvoiceCount.FieldName = "InvoiceCount";
            this.colInvoiceCount.Name = "colInvoiceCount";
            this.colInvoiceCount.OptionsColumn.AllowEdit = false;
            this.colInvoiceCount.OptionsColumn.AllowFocus = false;
            this.colInvoiceCount.OptionsColumn.ReadOnly = true;
            this.colInvoiceCount.Visible = true;
            this.colInvoiceCount.VisibleIndex = 8;
            this.colInvoiceCount.Width = 88;
            // 
            // colUninvoicedJobCount
            // 
            this.colUninvoicedJobCount.Caption = "Jobs To Be Invoiced";
            this.colUninvoicedJobCount.FieldName = "UninvoicedJobCount";
            this.colUninvoicedJobCount.Name = "colUninvoicedJobCount";
            this.colUninvoicedJobCount.OptionsColumn.AllowEdit = false;
            this.colUninvoicedJobCount.OptionsColumn.AllowFocus = false;
            this.colUninvoicedJobCount.OptionsColumn.ReadOnly = true;
            this.colUninvoicedJobCount.Visible = true;
            this.colUninvoicedJobCount.VisibleIndex = 6;
            this.colUninvoicedJobCount.Width = 117;
            // 
            // colAuthorisedJobCount
            // 
            this.colAuthorisedJobCount.Caption = "Jobs Authorised";
            this.colAuthorisedJobCount.FieldName = "AuthorisedJobCount";
            this.colAuthorisedJobCount.Name = "colAuthorisedJobCount";
            this.colAuthorisedJobCount.OptionsColumn.AllowEdit = false;
            this.colAuthorisedJobCount.OptionsColumn.AllowFocus = false;
            this.colAuthorisedJobCount.OptionsColumn.ReadOnly = true;
            this.colAuthorisedJobCount.Visible = true;
            this.colAuthorisedJobCount.VisibleIndex = 7;
            this.colAuthorisedJobCount.Width = 98;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Visible = true;
            this.colEmailPassword.VisibleIndex = 13;
            this.colEmailPassword.Width = 94;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp04120GCAuthorisationTeamsNonSelfBillingBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEdit25kgbags2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditCurrency2});
            this.gridControl2.Size = new System.Drawing.Size(364, 169);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp04120GCAuthorisationTeamsNonSelfBillingBindingSource
            // 
            this.sp04120GCAuthorisationTeamsNonSelfBillingBindingSource.DataMember = "sp04120_GC_Authorisation_Teams_Non_Self_Billing";
            this.sp04120GCAuthorisationTeamsNonSelfBillingBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn22,
            this.colAuthorisedJobCount1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Sub-Contractor Grit Information ID";
            this.gridColumn1.FieldName = "SubContractorGritInformationID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 188;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Sub-Contractor ID";
            this.gridColumn2.FieldName = "SubContractorID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 109;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Gritting Active";
            this.gridColumn3.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn3.FieldName = "GrittingActive";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 89;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Holds Stock";
            this.gridColumn4.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn4.FieldName = "HoldsStock";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            this.gridColumn4.Width = 76;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Current Grit Level";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEdit25kgbags2;
            this.gridColumn5.FieldName = "CurrentGritLevel";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            this.gridColumn5.Width = 106;
            // 
            // repositoryItemTextEdit25kgbags2
            // 
            this.repositoryItemTextEdit25kgbags2.AutoHeight = false;
            this.repositoryItemTextEdit25kgbags2.Mask.EditMask = "######0.00 25kg bags";
            this.repositoryItemTextEdit25kgbags2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25kgbags2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25kgbags2.Name = "repositoryItemTextEdit25kgbags2";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Warning Grit Level";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEdit25kgbags2;
            this.gridColumn6.FieldName = "WarningGritLevel";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            this.gridColumn6.Width = 109;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Urgent Grit Level";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEdit25kgbags2;
            this.gridColumn7.FieldName = "UrgentGritLevel";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            this.gridColumn7.Width = 102;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Stock Supplied By Depot ID";
            this.gridColumn8.FieldName = "StockSuppliedByDepotID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 151;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Self-Billing Invoice";
            this.gridColumn9.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn9.FieldName = "SellBillingInvoice";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 107;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Is Direct Labour";
            this.gridColumn13.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn13.FieldName = "IsDirectLabour";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 97;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Remarks";
            this.gridColumn14.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn14.FieldName = "Remarks";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 8;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Hourly Proactive Rate";
            this.gridColumn15.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn15.FieldName = "HourlyProactiveRate";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 126;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Hourly Reactive Rate";
            this.gridColumn16.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn16.FieldName = "HourlyReactiveRate";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 123;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Team Name";
            this.gridColumn17.FieldName = "TeamName";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 177;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Address Line 1";
            this.gridColumn18.FieldName = "AddressLine1";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 91;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Jobs To Be Invoiced";
            this.gridColumn22.FieldName = "UninvoicedJobCount";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            this.gridColumn22.Width = 117;
            // 
            // colAuthorisedJobCount1
            // 
            this.colAuthorisedJobCount1.Caption = "Authorised Jobs";
            this.colAuthorisedJobCount1.FieldName = "AuthorisedJobCount";
            this.colAuthorisedJobCount1.Name = "colAuthorisedJobCount1";
            this.colAuthorisedJobCount1.OptionsColumn.AllowEdit = false;
            this.colAuthorisedJobCount1.OptionsColumn.AllowFocus = false;
            this.colAuthorisedJobCount1.OptionsColumn.ReadOnly = true;
            this.colAuthorisedJobCount1.Visible = true;
            this.colAuthorisedJobCount1.VisibleIndex = 3;
            this.colAuthorisedJobCount1.Width = 98;
            // 
            // popupContainerControlCompanies
            // 
            this.popupContainerControlCompanies.Controls.Add(this.btnCompanyFilterOK);
            this.popupContainerControlCompanies.Controls.Add(this.gridControl5);
            this.popupContainerControlCompanies.Location = new System.Drawing.Point(29, 193);
            this.popupContainerControlCompanies.Name = "popupContainerControlCompanies";
            this.popupContainerControlCompanies.Size = new System.Drawing.Size(189, 163);
            this.popupContainerControlCompanies.TabIndex = 16;
            // 
            // btnCompanyFilterOK
            // 
            this.btnCompanyFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCompanyFilterOK.Location = new System.Drawing.Point(3, 138);
            this.btnCompanyFilterOK.Name = "btnCompanyFilterOK";
            this.btnCompanyFilterOK.Size = new System.Drawing.Size(77, 22);
            this.btnCompanyFilterOK.TabIndex = 18;
            this.btnCompanyFilterOK.Text = "OK";
            this.btnCompanyFilterOK.Click += new System.EventHandler(this.btnCompanyFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp04237GCCompanyFilterListBindingSource1;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(183, 133);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04237GCCompanyFilterListBindingSource1
            // 
            this.sp04237GCCompanyFilterListBindingSource1.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource1.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn24,
            this.gridColumn25,
            this.colCompanyCode,
            this.colCompanyOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Company ID";
            this.gridColumn24.FieldName = "CompanyID";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 80;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Company Name";
            this.gridColumn25.FieldName = "CompanyName";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 0;
            this.gridColumn25.Width = 152;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyOrder
            // 
            this.colCompanyOrder.Caption = "Order";
            this.colCompanyOrder.FieldName = "CompanyOrder";
            this.colCompanyOrder.Name = "colCompanyOrder";
            this.colCompanyOrder.OptionsColumn.AllowEdit = false;
            this.colCompanyOrder.OptionsColumn.AllowFocus = false;
            this.colCompanyOrder.OptionsColumn.ReadOnly = true;
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp04121GCCalloutsForInvoicingBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 40);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEdit25kgbags3,
            this.repositoryItemTextEditCurrency3,
            this.repositoryItemTextEditVATRate,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemPictureEdit1});
            this.gridControl3.Size = new System.Drawing.Size(749, 448);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp04121GCCalloutsForInvoicingBindingSource
            // 
            this.sp04121GCCalloutsForInvoicingBindingSource.DataMember = "sp04121_GC_Callouts_For_Invoicing";
            this.sp04121GCCalloutsForInvoicingBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGrittingCallOutID,
            this.colSiteGrittingContractID,
            this.colSiteID,
            this.colSiteName,
            this.colClientID,
            this.colClientName,
            this.colCompanyID,
            this.colCompanyName,
            this.colSiteXCoordinate,
            this.colSiteYCoordinate,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colSiteTelephone,
            this.colSiteEmail,
            this.colClientsSiteCode,
            this.gridColumn10,
            this.colSubContractorName,
            this.colSubContractorIsDirectLabour,
            this.colOriginalSubContractorID,
            this.colOriginalSubContarctorName,
            this.colReactive,
            this.colJobStatusID,
            this.colCalloutStatusDescription,
            this.colCalloutStatusOrder,
            this.colCallOutDateTime,
            this.colImportedWeatherForecastID,
            this.colTextSentTime,
            this.colSubContractorReceivedTime,
            this.colSubContractorRespondedTime,
            this.colSubContractorETA,
            this.colCompletedTime,
            this.colAuthorisedByStaffID,
            this.colAuthorisedByName,
            this.colVisitAborted,
            this.colAbortedReason,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishedLatitude,
            this.colFinishedLongitude,
            this.colClientPONumber,
            this.colSaltUsed,
            this.colSaltCost,
            this.colSaltSell,
            this.colSaltVatRate,
            this.colHoursWorked,
            this.colTeamHourlyRate,
            this.colTeamCharge,
            this.colLabourCost,
            this.colLabourVatRate,
            this.colOtherCost,
            this.colOtherSell,
            this.colTotalCost,
            this.colTotalSell,
            this.colClientInvoiceNumber,
            this.colSubContractorPaid,
            this.colGrittingInvoiceID,
            this.colRecordedByStaffID,
            this.colRecordedByName,
            this.colPaidByStaffID,
            this.colPaidByName,
            this.colDoNotPaySubContractor,
            this.colDoNotPaySubContractorReason,
            this.colGritSourceLocationID,
            this.colGritSourceLocationTypeID,
            this.colNoAccess,
            this.colSiteWeather,
            this.colSiteTemperature,
            this.colPdaID,
            this.colTeamPresent,
            this.colServiceFailure,
            this.colComments,
            this.colClientPrice,
            this.colGritJobTransferMethod,
            this.colSnowOnSite,
            this.colStartTime,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn19,
            this.colProfit,
            this.colMarkup,
            this.colClientPOID,
            this.colNonStandardCost,
            this.colNonStandardSell,
            this.colDoNotInvoiceClient,
            this.colDoNotInvoiceClientReason,
            this.colGritMobileTelephoneNumber,
            this.colSelfBillingInvoice,
            this.gridColumn26});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 2;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSelfBillingInvoice, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCallOutDateTime, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colGrittingCallOutID
            // 
            this.colGrittingCallOutID.Caption = "Callout ID";
            this.colGrittingCallOutID.FieldName = "GrittingCallOutID";
            this.colGrittingCallOutID.Name = "colGrittingCallOutID";
            this.colGrittingCallOutID.OptionsColumn.AllowEdit = false;
            this.colGrittingCallOutID.OptionsColumn.AllowFocus = false;
            this.colGrittingCallOutID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteGrittingContractID
            // 
            this.colSiteGrittingContractID.Caption = "Site Gritting Contract ID";
            this.colSiteGrittingContractID.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID.Name = "colSiteGrittingContractID";
            this.colSiteGrittingContractID.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID.Width = 136;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 5;
            this.colSiteName.Width = 173;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 4;
            this.colClientName.Width = 108;
            // 
            // colCompanyID
            // 
            this.colCompanyID.Caption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 80;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 6;
            this.colCompanyName.Width = 107;
            // 
            // colSiteXCoordinate
            // 
            this.colSiteXCoordinate.Caption = "Site X Coord.";
            this.colSiteXCoordinate.FieldName = "SiteXCoordinate";
            this.colSiteXCoordinate.Name = "colSiteXCoordinate";
            this.colSiteXCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteXCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteXCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteXCoordinate.Width = 84;
            // 
            // colSiteYCoordinate
            // 
            this.colSiteYCoordinate.Caption = "Site Y Coord.";
            this.colSiteYCoordinate.FieldName = "SiteYCoordinate";
            this.colSiteYCoordinate.Name = "colSiteYCoordinate";
            this.colSiteYCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteYCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteYCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteYCoordinate.Width = 84;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Location X";
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 91;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Location Y";
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 91;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Site Telephone";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Width = 92;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Site Email";
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.AllowEdit = false;
            this.colSiteEmail.OptionsColumn.AllowFocus = false;
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Width = 102;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Team ID";
            this.gridColumn10.FieldName = "SubContractorID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Team Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.Width = 218;
            // 
            // colSubContractorIsDirectLabour
            // 
            this.colSubContractorIsDirectLabour.Caption = "Is Direct Labour";
            this.colSubContractorIsDirectLabour.FieldName = "SubContractorIsDirectLabour";
            this.colSubContractorIsDirectLabour.Name = "colSubContractorIsDirectLabour";
            this.colSubContractorIsDirectLabour.OptionsColumn.AllowEdit = false;
            this.colSubContractorIsDirectLabour.OptionsColumn.AllowFocus = false;
            this.colSubContractorIsDirectLabour.OptionsColumn.ReadOnly = true;
            this.colSubContractorIsDirectLabour.Visible = true;
            this.colSubContractorIsDirectLabour.VisibleIndex = 15;
            this.colSubContractorIsDirectLabour.Width = 97;
            // 
            // colOriginalSubContractorID
            // 
            this.colOriginalSubContractorID.Caption = "Original Team ID";
            this.colOriginalSubContractorID.FieldName = "OriginalSubContractorID";
            this.colOriginalSubContractorID.Name = "colOriginalSubContractorID";
            this.colOriginalSubContractorID.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorID.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorID.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorID.Width = 100;
            // 
            // colOriginalSubContarctorName
            // 
            this.colOriginalSubContarctorName.Caption = "Original Team Name";
            this.colOriginalSubContarctorName.FieldName = "OriginalSubContarctorName";
            this.colOriginalSubContarctorName.Name = "colOriginalSubContarctorName";
            this.colOriginalSubContarctorName.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContarctorName.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContarctorName.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContarctorName.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 3;
            this.colReactive.Width = 62;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 88;
            // 
            // colCalloutStatusDescription
            // 
            this.colCalloutStatusDescription.Caption = "Status";
            this.colCalloutStatusDescription.FieldName = "CalloutStatusDescription";
            this.colCalloutStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCalloutStatusDescription.Name = "colCalloutStatusDescription";
            this.colCalloutStatusDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusDescription.Visible = true;
            this.colCalloutStatusDescription.VisibleIndex = 1;
            this.colCalloutStatusDescription.Width = 182;
            // 
            // colCalloutStatusOrder
            // 
            this.colCalloutStatusOrder.Caption = "Status Order";
            this.colCalloutStatusOrder.FieldName = "CalloutStatusOrder";
            this.colCalloutStatusOrder.Name = "colCalloutStatusOrder";
            this.colCalloutStatusOrder.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusOrder.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusOrder.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusOrder.Width = 83;
            // 
            // colCallOutDateTime
            // 
            this.colCallOutDateTime.Caption = "Callout Date\\Time";
            this.colCallOutDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCallOutDateTime.FieldName = "CallOutDateTime";
            this.colCallOutDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCallOutDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCallOutDateTime.Name = "colCallOutDateTime";
            this.colCallOutDateTime.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCallOutDateTime.Visible = true;
            this.colCallOutDateTime.VisibleIndex = 2;
            this.colCallOutDateTime.Width = 117;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colImportedWeatherForecastID
            // 
            this.colImportedWeatherForecastID.Caption = "Imported Forecast ID";
            this.colImportedWeatherForecastID.FieldName = "ImportedWeatherForecastID";
            this.colImportedWeatherForecastID.Name = "colImportedWeatherForecastID";
            this.colImportedWeatherForecastID.OptionsColumn.AllowEdit = false;
            this.colImportedWeatherForecastID.OptionsColumn.AllowFocus = false;
            this.colImportedWeatherForecastID.OptionsColumn.ReadOnly = true;
            this.colImportedWeatherForecastID.Width = 124;
            // 
            // colTextSentTime
            // 
            this.colTextSentTime.Caption = "Text Sent Time";
            this.colTextSentTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colTextSentTime.FieldName = "TextSentTime";
            this.colTextSentTime.Name = "colTextSentTime";
            this.colTextSentTime.OptionsColumn.AllowEdit = false;
            this.colTextSentTime.OptionsColumn.AllowFocus = false;
            this.colTextSentTime.OptionsColumn.ReadOnly = true;
            this.colTextSentTime.Width = 93;
            // 
            // colSubContractorReceivedTime
            // 
            this.colSubContractorReceivedTime.Caption = "Team Received Time";
            this.colSubContractorReceivedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSubContractorReceivedTime.FieldName = "SubContractorReceivedTime";
            this.colSubContractorReceivedTime.Name = "colSubContractorReceivedTime";
            this.colSubContractorReceivedTime.OptionsColumn.AllowEdit = false;
            this.colSubContractorReceivedTime.OptionsColumn.AllowFocus = false;
            this.colSubContractorReceivedTime.OptionsColumn.ReadOnly = true;
            this.colSubContractorReceivedTime.Width = 119;
            // 
            // colSubContractorRespondedTime
            // 
            this.colSubContractorRespondedTime.Caption = "Team Responded Time";
            this.colSubContractorRespondedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSubContractorRespondedTime.FieldName = "SubContractorRespondedTime";
            this.colSubContractorRespondedTime.Name = "colSubContractorRespondedTime";
            this.colSubContractorRespondedTime.OptionsColumn.AllowEdit = false;
            this.colSubContractorRespondedTime.OptionsColumn.AllowFocus = false;
            this.colSubContractorRespondedTime.OptionsColumn.ReadOnly = true;
            this.colSubContractorRespondedTime.Width = 129;
            // 
            // colSubContractorETA
            // 
            this.colSubContractorETA.Caption = "Team ETA";
            this.colSubContractorETA.FieldName = "SubContractorETA";
            this.colSubContractorETA.Name = "colSubContractorETA";
            this.colSubContractorETA.OptionsColumn.AllowEdit = false;
            this.colSubContractorETA.OptionsColumn.AllowFocus = false;
            this.colSubContractorETA.OptionsColumn.ReadOnly = true;
            // 
            // colCompletedTime
            // 
            this.colCompletedTime.Caption = "Completed Time";
            this.colCompletedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCompletedTime.FieldName = "CompletedTime";
            this.colCompletedTime.Name = "colCompletedTime";
            this.colCompletedTime.OptionsColumn.AllowEdit = false;
            this.colCompletedTime.OptionsColumn.AllowFocus = false;
            this.colCompletedTime.OptionsColumn.ReadOnly = true;
            this.colCompletedTime.Visible = true;
            this.colCompletedTime.VisibleIndex = 7;
            this.colCompletedTime.Width = 97;
            // 
            // colAuthorisedByStaffID
            // 
            this.colAuthorisedByStaffID.Caption = "Authorised By ID";
            this.colAuthorisedByStaffID.FieldName = "AuthorisedByStaffID";
            this.colAuthorisedByStaffID.Name = "colAuthorisedByStaffID";
            this.colAuthorisedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByStaffID.Width = 102;
            // 
            // colAuthorisedByName
            // 
            this.colAuthorisedByName.Caption = "Authorised By";
            this.colAuthorisedByName.FieldName = "AuthorisedByName";
            this.colAuthorisedByName.Name = "colAuthorisedByName";
            this.colAuthorisedByName.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByName.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByName.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByName.Visible = true;
            this.colAuthorisedByName.VisibleIndex = 12;
            this.colAuthorisedByName.Width = 88;
            // 
            // colVisitAborted
            // 
            this.colVisitAborted.Caption = "Aborted";
            this.colVisitAborted.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colVisitAborted.FieldName = "VisitAborted";
            this.colVisitAborted.Name = "colVisitAborted";
            this.colVisitAborted.OptionsColumn.AllowEdit = false;
            this.colVisitAborted.OptionsColumn.AllowFocus = false;
            this.colVisitAborted.OptionsColumn.ReadOnly = true;
            this.colVisitAborted.Visible = true;
            this.colVisitAborted.VisibleIndex = 13;
            // 
            // colAbortedReason
            // 
            this.colAbortedReason.Caption = "Aborted Reason";
            this.colAbortedReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colAbortedReason.FieldName = "AbortedReason";
            this.colAbortedReason.Name = "colAbortedReason";
            this.colAbortedReason.OptionsColumn.ReadOnly = true;
            this.colAbortedReason.Visible = true;
            this.colAbortedReason.VisibleIndex = 14;
            this.colAbortedReason.Width = 99;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Lat";
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Long";
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            // 
            // colFinishedLatitude
            // 
            this.colFinishedLatitude.Caption = "Finished Lat";
            this.colFinishedLatitude.FieldName = "FinishedLatitude";
            this.colFinishedLatitude.Name = "colFinishedLatitude";
            this.colFinishedLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishedLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishedLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishedLatitude.Width = 78;
            // 
            // colFinishedLongitude
            // 
            this.colFinishedLongitude.Caption = "Finished Long";
            this.colFinishedLongitude.FieldName = "FinishedLongitude";
            this.colFinishedLongitude.Name = "colFinishedLongitude";
            this.colFinishedLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishedLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishedLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishedLongitude.Width = 86;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "P.O. Number";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Width = 83;
            // 
            // colSaltUsed
            // 
            this.colSaltUsed.Caption = "Salt Used";
            this.colSaltUsed.ColumnEdit = this.repositoryItemTextEdit25kgbags3;
            this.colSaltUsed.FieldName = "SaltUsed";
            this.colSaltUsed.Name = "colSaltUsed";
            this.colSaltUsed.OptionsColumn.AllowEdit = false;
            this.colSaltUsed.OptionsColumn.AllowFocus = false;
            this.colSaltUsed.OptionsColumn.ReadOnly = true;
            this.colSaltUsed.Visible = true;
            this.colSaltUsed.VisibleIndex = 21;
            // 
            // repositoryItemTextEdit25kgbags3
            // 
            this.repositoryItemTextEdit25kgbags3.AutoHeight = false;
            this.repositoryItemTextEdit25kgbags3.Mask.EditMask = "######0.00 25kg bags";
            this.repositoryItemTextEdit25kgbags3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25kgbags3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25kgbags3.Name = "repositoryItemTextEdit25kgbags3";
            // 
            // colSaltCost
            // 
            this.colSaltCost.Caption = "Salt Cost";
            this.colSaltCost.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colSaltCost.FieldName = "SaltCost";
            this.colSaltCost.Name = "colSaltCost";
            this.colSaltCost.OptionsColumn.AllowEdit = false;
            this.colSaltCost.OptionsColumn.AllowFocus = false;
            this.colSaltCost.OptionsColumn.ReadOnly = true;
            this.colSaltCost.Visible = true;
            this.colSaltCost.VisibleIndex = 22;
            // 
            // repositoryItemTextEditCurrency3
            // 
            this.repositoryItemTextEditCurrency3.AutoHeight = false;
            this.repositoryItemTextEditCurrency3.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency3.Name = "repositoryItemTextEditCurrency3";
            // 
            // colSaltSell
            // 
            this.colSaltSell.Caption = "Salt Sell";
            this.colSaltSell.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colSaltSell.FieldName = "SaltSell";
            this.colSaltSell.Name = "colSaltSell";
            this.colSaltSell.OptionsColumn.AllowEdit = false;
            this.colSaltSell.OptionsColumn.AllowFocus = false;
            this.colSaltSell.OptionsColumn.ReadOnly = true;
            this.colSaltSell.Visible = true;
            this.colSaltSell.VisibleIndex = 23;
            // 
            // colSaltVatRate
            // 
            this.colSaltVatRate.Caption = "Salt VAT Rate";
            this.colSaltVatRate.ColumnEdit = this.repositoryItemTextEditVATRate;
            this.colSaltVatRate.FieldName = "SaltVatRate";
            this.colSaltVatRate.Name = "colSaltVatRate";
            this.colSaltVatRate.OptionsColumn.AllowEdit = false;
            this.colSaltVatRate.OptionsColumn.AllowFocus = false;
            this.colSaltVatRate.OptionsColumn.ReadOnly = true;
            this.colSaltVatRate.Visible = true;
            this.colSaltVatRate.VisibleIndex = 24;
            this.colSaltVatRate.Width = 87;
            // 
            // repositoryItemTextEditVATRate
            // 
            this.repositoryItemTextEditVATRate.AutoHeight = false;
            this.repositoryItemTextEditVATRate.Mask.EditMask = "P";
            this.repositoryItemTextEditVATRate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditVATRate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditVATRate.Name = "repositoryItemTextEditVATRate";
            // 
            // colHoursWorked
            // 
            this.colHoursWorked.Caption = "Hours Worked";
            this.colHoursWorked.FieldName = "HoursWorked";
            this.colHoursWorked.Name = "colHoursWorked";
            this.colHoursWorked.OptionsColumn.AllowEdit = false;
            this.colHoursWorked.OptionsColumn.AllowFocus = false;
            this.colHoursWorked.OptionsColumn.ReadOnly = true;
            this.colHoursWorked.Visible = true;
            this.colHoursWorked.VisibleIndex = 16;
            this.colHoursWorked.Width = 89;
            // 
            // colTeamHourlyRate
            // 
            this.colTeamHourlyRate.Caption = "Hourly Rate";
            this.colTeamHourlyRate.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colTeamHourlyRate.FieldName = "TeamHourlyRate";
            this.colTeamHourlyRate.Name = "colTeamHourlyRate";
            this.colTeamHourlyRate.OptionsColumn.AllowEdit = false;
            this.colTeamHourlyRate.OptionsColumn.AllowFocus = false;
            this.colTeamHourlyRate.OptionsColumn.ReadOnly = true;
            this.colTeamHourlyRate.Visible = true;
            this.colTeamHourlyRate.VisibleIndex = 17;
            this.colTeamHourlyRate.Width = 78;
            // 
            // colTeamCharge
            // 
            this.colTeamCharge.Caption = "Team Charge";
            this.colTeamCharge.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colTeamCharge.FieldName = "TeamCharge";
            this.colTeamCharge.Name = "colTeamCharge";
            this.colTeamCharge.OptionsColumn.AllowEdit = false;
            this.colTeamCharge.OptionsColumn.AllowFocus = false;
            this.colTeamCharge.OptionsColumn.ReadOnly = true;
            this.colTeamCharge.Visible = true;
            this.colTeamCharge.VisibleIndex = 18;
            this.colTeamCharge.Width = 85;
            // 
            // colLabourCost
            // 
            this.colLabourCost.Caption = "Labour Cost";
            this.colLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colLabourCost.FieldName = "LabourCost";
            this.colLabourCost.Name = "colLabourCost";
            this.colLabourCost.OptionsColumn.AllowEdit = false;
            this.colLabourCost.OptionsColumn.AllowFocus = false;
            this.colLabourCost.OptionsColumn.ReadOnly = true;
            this.colLabourCost.Visible = true;
            this.colLabourCost.VisibleIndex = 19;
            this.colLabourCost.Width = 79;
            // 
            // colLabourVatRate
            // 
            this.colLabourVatRate.Caption = "Labour VAT Rate";
            this.colLabourVatRate.ColumnEdit = this.repositoryItemTextEditVATRate;
            this.colLabourVatRate.FieldName = "LabourVatRate";
            this.colLabourVatRate.Name = "colLabourVatRate";
            this.colLabourVatRate.OptionsColumn.AllowEdit = false;
            this.colLabourVatRate.OptionsColumn.AllowFocus = false;
            this.colLabourVatRate.OptionsColumn.ReadOnly = true;
            this.colLabourVatRate.Visible = true;
            this.colLabourVatRate.VisibleIndex = 20;
            this.colLabourVatRate.Width = 102;
            // 
            // colOtherCost
            // 
            this.colOtherCost.Caption = "Other Cost";
            this.colOtherCost.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colOtherCost.FieldName = "OtherCost";
            this.colOtherCost.Name = "colOtherCost";
            this.colOtherCost.OptionsColumn.AllowEdit = false;
            this.colOtherCost.OptionsColumn.AllowFocus = false;
            this.colOtherCost.OptionsColumn.ReadOnly = true;
            this.colOtherCost.Visible = true;
            this.colOtherCost.VisibleIndex = 25;
            // 
            // colOtherSell
            // 
            this.colOtherSell.Caption = "Other Sell";
            this.colOtherSell.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colOtherSell.FieldName = "OtherSell";
            this.colOtherSell.Name = "colOtherSell";
            this.colOtherSell.OptionsColumn.AllowEdit = false;
            this.colOtherSell.OptionsColumn.AllowFocus = false;
            this.colOtherSell.OptionsColumn.ReadOnly = true;
            this.colOtherSell.Visible = true;
            this.colOtherSell.VisibleIndex = 26;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 27;
            // 
            // colTotalSell
            // 
            this.colTotalSell.Caption = "Total Sell";
            this.colTotalSell.FieldName = "TotalSell";
            this.colTotalSell.Name = "colTotalSell";
            this.colTotalSell.OptionsColumn.AllowEdit = false;
            this.colTotalSell.OptionsColumn.AllowFocus = false;
            this.colTotalSell.OptionsColumn.ReadOnly = true;
            this.colTotalSell.Visible = true;
            this.colTotalSell.VisibleIndex = 29;
            // 
            // colClientInvoiceNumber
            // 
            this.colClientInvoiceNumber.Caption = "Invoice Number";
            this.colClientInvoiceNumber.FieldName = "ClientInvoiceNumber";
            this.colClientInvoiceNumber.Name = "colClientInvoiceNumber";
            this.colClientInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceNumber.Width = 96;
            // 
            // colSubContractorPaid
            // 
            this.colSubContractorPaid.Caption = "Team Paid";
            this.colSubContractorPaid.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colSubContractorPaid.FieldName = "SubContractorPaid";
            this.colSubContractorPaid.Name = "colSubContractorPaid";
            this.colSubContractorPaid.OptionsColumn.AllowEdit = false;
            this.colSubContractorPaid.OptionsColumn.AllowFocus = false;
            this.colSubContractorPaid.OptionsColumn.ReadOnly = true;
            // 
            // colGrittingInvoiceID
            // 
            this.colGrittingInvoiceID.Caption = "Gritting Invoice ID";
            this.colGrittingInvoiceID.FieldName = "GrittingInvoiceID";
            this.colGrittingInvoiceID.Name = "colGrittingInvoiceID";
            this.colGrittingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colGrittingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colGrittingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colGrittingInvoiceID.Width = 108;
            // 
            // colRecordedByStaffID
            // 
            this.colRecordedByStaffID.Caption = "Recorded By Staff ID";
            this.colRecordedByStaffID.FieldName = "RecordedByStaffID";
            this.colRecordedByStaffID.Name = "colRecordedByStaffID";
            this.colRecordedByStaffID.OptionsColumn.AllowEdit = false;
            this.colRecordedByStaffID.OptionsColumn.AllowFocus = false;
            this.colRecordedByStaffID.OptionsColumn.ReadOnly = true;
            this.colRecordedByStaffID.Width = 123;
            // 
            // colRecordedByName
            // 
            this.colRecordedByName.Caption = "Recorded By";
            this.colRecordedByName.FieldName = "RecordedByName";
            this.colRecordedByName.Name = "colRecordedByName";
            this.colRecordedByName.OptionsColumn.AllowEdit = false;
            this.colRecordedByName.OptionsColumn.AllowFocus = false;
            this.colRecordedByName.OptionsColumn.ReadOnly = true;
            this.colRecordedByName.Width = 118;
            // 
            // colPaidByStaffID
            // 
            this.colPaidByStaffID.Caption = "Paid By Staff ID";
            this.colPaidByStaffID.FieldName = "PaidByStaffID";
            this.colPaidByStaffID.Name = "colPaidByStaffID";
            this.colPaidByStaffID.OptionsColumn.AllowEdit = false;
            this.colPaidByStaffID.OptionsColumn.AllowFocus = false;
            this.colPaidByStaffID.OptionsColumn.ReadOnly = true;
            this.colPaidByStaffID.Width = 97;
            // 
            // colPaidByName
            // 
            this.colPaidByName.Caption = "Paid By";
            this.colPaidByName.FieldName = "PaidByName";
            this.colPaidByName.Name = "colPaidByName";
            this.colPaidByName.OptionsColumn.AllowEdit = false;
            this.colPaidByName.OptionsColumn.AllowFocus = false;
            this.colPaidByName.OptionsColumn.ReadOnly = true;
            this.colPaidByName.Width = 93;
            // 
            // colDoNotPaySubContractor
            // 
            this.colDoNotPaySubContractor.Caption = "Don\'t Pay Team";
            this.colDoNotPaySubContractor.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colDoNotPaySubContractor.FieldName = "DoNotPaySubContractor";
            this.colDoNotPaySubContractor.Name = "colDoNotPaySubContractor";
            this.colDoNotPaySubContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPaySubContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPaySubContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractor.Visible = true;
            this.colDoNotPaySubContractor.VisibleIndex = 8;
            this.colDoNotPaySubContractor.Width = 96;
            // 
            // colDoNotPaySubContractorReason
            // 
            this.colDoNotPaySubContractorReason.Caption = "Don\'t Pay Team Reason";
            this.colDoNotPaySubContractorReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDoNotPaySubContractorReason.FieldName = "DoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.Name = "colDoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractorReason.Visible = true;
            this.colDoNotPaySubContractorReason.VisibleIndex = 9;
            this.colDoNotPaySubContractorReason.Width = 135;
            // 
            // colGritSourceLocationID
            // 
            this.colGritSourceLocationID.Caption = "Grit Source Location ID";
            this.colGritSourceLocationID.FieldName = "GritSourceLocationID";
            this.colGritSourceLocationID.Name = "colGritSourceLocationID";
            this.colGritSourceLocationID.OptionsColumn.AllowEdit = false;
            this.colGritSourceLocationID.OptionsColumn.AllowFocus = false;
            this.colGritSourceLocationID.OptionsColumn.ReadOnly = true;
            this.colGritSourceLocationID.Width = 131;
            // 
            // colGritSourceLocationTypeID
            // 
            this.colGritSourceLocationTypeID.Caption = "Grit Source Location Type ID";
            this.colGritSourceLocationTypeID.FieldName = "GritSourceLocationTypeID";
            this.colGritSourceLocationTypeID.Name = "colGritSourceLocationTypeID";
            this.colGritSourceLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colGritSourceLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colGritSourceLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colGritSourceLocationTypeID.Width = 158;
            // 
            // colNoAccess
            // 
            this.colNoAccess.Caption = "No Access";
            this.colNoAccess.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colNoAccess.FieldName = "NoAccess";
            this.colNoAccess.Name = "colNoAccess";
            this.colNoAccess.OptionsColumn.AllowEdit = false;
            this.colNoAccess.OptionsColumn.AllowFocus = false;
            this.colNoAccess.OptionsColumn.ReadOnly = true;
            this.colNoAccess.Visible = true;
            this.colNoAccess.VisibleIndex = 34;
            // 
            // colSiteWeather
            // 
            this.colSiteWeather.Caption = "Site Weather";
            this.colSiteWeather.FieldName = "SiteWeather";
            this.colSiteWeather.Name = "colSiteWeather";
            this.colSiteWeather.OptionsColumn.AllowEdit = false;
            this.colSiteWeather.OptionsColumn.AllowFocus = false;
            this.colSiteWeather.OptionsColumn.ReadOnly = true;
            this.colSiteWeather.Width = 84;
            // 
            // colSiteTemperature
            // 
            this.colSiteTemperature.Caption = "Site Temperature";
            this.colSiteTemperature.FieldName = "SiteTemperature";
            this.colSiteTemperature.Name = "colSiteTemperature";
            this.colSiteTemperature.OptionsColumn.AllowEdit = false;
            this.colSiteTemperature.OptionsColumn.AllowFocus = false;
            this.colSiteTemperature.OptionsColumn.ReadOnly = true;
            this.colSiteTemperature.Width = 104;
            // 
            // colPdaID
            // 
            this.colPdaID.Caption = "Device ID";
            this.colPdaID.FieldName = "PdaID";
            this.colPdaID.Name = "colPdaID";
            this.colPdaID.OptionsColumn.AllowEdit = false;
            this.colPdaID.OptionsColumn.AllowFocus = false;
            this.colPdaID.OptionsColumn.ReadOnly = true;
            // 
            // colTeamPresent
            // 
            this.colTeamPresent.Caption = "Team Present";
            this.colTeamPresent.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colTeamPresent.FieldName = "TeamPresent";
            this.colTeamPresent.Name = "colTeamPresent";
            this.colTeamPresent.OptionsColumn.AllowEdit = false;
            this.colTeamPresent.OptionsColumn.AllowFocus = false;
            this.colTeamPresent.OptionsColumn.ReadOnly = true;
            this.colTeamPresent.Width = 87;
            // 
            // colServiceFailure
            // 
            this.colServiceFailure.Caption = "Service Failure";
            this.colServiceFailure.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colServiceFailure.FieldName = "ServiceFailure";
            this.colServiceFailure.Name = "colServiceFailure";
            this.colServiceFailure.OptionsColumn.AllowEdit = false;
            this.colServiceFailure.OptionsColumn.AllowFocus = false;
            this.colServiceFailure.OptionsColumn.ReadOnly = true;
            this.colServiceFailure.Visible = true;
            this.colServiceFailure.VisibleIndex = 35;
            this.colServiceFailure.Width = 91;
            // 
            // colComments
            // 
            this.colComments.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colComments.FieldName = "Comments";
            this.colComments.Name = "colComments";
            this.colComments.OptionsColumn.ReadOnly = true;
            this.colComments.Visible = true;
            this.colComments.VisibleIndex = 36;
            // 
            // colClientPrice
            // 
            this.colClientPrice.Caption = "Client Price";
            this.colClientPrice.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colClientPrice.FieldName = "ClientPrice";
            this.colClientPrice.Name = "colClientPrice";
            this.colClientPrice.OptionsColumn.AllowEdit = false;
            this.colClientPrice.OptionsColumn.AllowFocus = false;
            this.colClientPrice.OptionsColumn.ReadOnly = true;
            this.colClientPrice.Visible = true;
            this.colClientPrice.VisibleIndex = 28;
            // 
            // colGritJobTransferMethod
            // 
            this.colGritJobTransferMethod.Caption = "Transfer Method";
            this.colGritJobTransferMethod.FieldName = "GritJobTransferMethod";
            this.colGritJobTransferMethod.Name = "colGritJobTransferMethod";
            this.colGritJobTransferMethod.OptionsColumn.AllowEdit = false;
            this.colGritJobTransferMethod.OptionsColumn.AllowFocus = false;
            this.colGritJobTransferMethod.OptionsColumn.ReadOnly = true;
            this.colGritJobTransferMethod.Width = 101;
            // 
            // colSnowOnSite
            // 
            this.colSnowOnSite.Caption = "Snow On Site";
            this.colSnowOnSite.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colSnowOnSite.FieldName = "SnowOnSite";
            this.colSnowOnSite.Name = "colSnowOnSite";
            this.colSnowOnSite.OptionsColumn.AllowEdit = false;
            this.colSnowOnSite.OptionsColumn.AllowFocus = false;
            this.colSnowOnSite.OptionsColumn.ReadOnly = true;
            this.colSnowOnSite.Width = 85;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "<b>Calculated 1</b>";
            this.gridColumn11.FieldName = "Calculated1";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.ShowUnboundExpressionMenu = true;
            this.gridColumn11.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 37;
            this.gridColumn11.Width = 90;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "<b>Calculated 2</b>";
            this.gridColumn12.FieldName = "Calculated2";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.ShowUnboundExpressionMenu = true;
            this.gridColumn12.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 38;
            this.gridColumn12.Width = 90;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "<b>Calculated 3</b>";
            this.gridColumn19.FieldName = "Calculated3";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.ShowUnboundExpressionMenu = true;
            this.gridColumn19.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 39;
            this.gridColumn19.Width = 90;
            // 
            // colProfit
            // 
            this.colProfit.Caption = "Profit";
            this.colProfit.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colProfit.FieldName = "Profit";
            this.colProfit.Name = "colProfit";
            this.colProfit.OptionsColumn.AllowEdit = false;
            this.colProfit.OptionsColumn.AllowFocus = false;
            this.colProfit.OptionsColumn.ReadOnly = true;
            this.colProfit.Visible = true;
            this.colProfit.VisibleIndex = 30;
            // 
            // colMarkup
            // 
            this.colMarkup.Caption = "Markup";
            this.colMarkup.FieldName = "Markup";
            this.colMarkup.Name = "colMarkup";
            this.colMarkup.OptionsColumn.AllowEdit = false;
            this.colMarkup.OptionsColumn.AllowFocus = false;
            this.colMarkup.OptionsColumn.ReadOnly = true;
            this.colMarkup.Visible = true;
            this.colMarkup.VisibleIndex = 31;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "P.O. Linked ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 90;
            // 
            // colNonStandardCost
            // 
            this.colNonStandardCost.Caption = "Non-Standard Cost";
            this.colNonStandardCost.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colNonStandardCost.FieldName = "NonStandardCost";
            this.colNonStandardCost.Name = "colNonStandardCost";
            this.colNonStandardCost.OptionsColumn.AllowEdit = false;
            this.colNonStandardCost.OptionsColumn.AllowFocus = false;
            this.colNonStandardCost.OptionsColumn.ReadOnly = true;
            this.colNonStandardCost.Visible = true;
            this.colNonStandardCost.VisibleIndex = 32;
            this.colNonStandardCost.Width = 113;
            // 
            // colNonStandardSell
            // 
            this.colNonStandardSell.Caption = "Non-Standard Sell";
            this.colNonStandardSell.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colNonStandardSell.FieldName = "NonStandardSell";
            this.colNonStandardSell.Name = "colNonStandardSell";
            this.colNonStandardSell.OptionsColumn.AllowEdit = false;
            this.colNonStandardSell.OptionsColumn.AllowFocus = false;
            this.colNonStandardSell.OptionsColumn.ReadOnly = true;
            this.colNonStandardSell.Visible = true;
            this.colNonStandardSell.VisibleIndex = 33;
            this.colNonStandardSell.Width = 107;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 10;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colDoNotInvoiceClientReason
            // 
            this.colDoNotInvoiceClientReason.Caption = "Do Not Invoice Client Reason";
            this.colDoNotInvoiceClientReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDoNotInvoiceClientReason.FieldName = "DoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.Name = "colDoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClientReason.Visible = true;
            this.colDoNotInvoiceClientReason.VisibleIndex = 11;
            this.colDoNotInvoiceClientReason.Width = 161;
            // 
            // colGritMobileTelephoneNumber
            // 
            this.colGritMobileTelephoneNumber.Caption = "Grit Mobile Telephone";
            this.colGritMobileTelephoneNumber.FieldName = "GritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.Name = "colGritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.OptionsColumn.ReadOnly = true;
            this.colGritMobileTelephoneNumber.Width = 124;
            // 
            // colSelfBillingInvoice
            // 
            this.colSelfBillingInvoice.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colSelfBillingInvoice.FieldName = "SelfBillingInvoice";
            this.colSelfBillingInvoice.Name = "colSelfBillingInvoice";
            this.colSelfBillingInvoice.Width = 106;
            // 
            // gridColumn26
            // 
            this.gridColumn26.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumn26.CustomizationCaption = "Alert";
            this.gridColumn26.FieldName = "Alert";
            this.gridColumn26.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.AllowSize = false;
            this.gridColumn26.OptionsColumn.FixedWidth = true;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.OptionsColumn.ShowCaption = false;
            this.gridColumn26.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn26.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 0;
            this.gridColumn26.Width = 20;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(750, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.standaloneBarDockControl3);
            this.xtraTabPage2.Controls.Add(this.splitContainerControl4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1124, 489);
            this.xtraTabPage2.Text = "Historical Self-Billing Invoices";
            // 
            // standaloneBarDockControl3
            // 
            this.standaloneBarDockControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl3.CausesValidation = false;
            this.standaloneBarDockControl3.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl3.Manager = this.barManager1;
            this.standaloneBarDockControl3.Name = "standaloneBarDockControl3";
            this.standaloneBarDockControl3.Size = new System.Drawing.Size(1125, 42);
            this.standaloneBarDockControl3.Text = "standaloneBarDockControl3";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl4.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl4.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 42);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl4.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl4.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl4.Panel1.Controls.Add(this.gridControl4);
            this.splitContainerControl4.Panel1.ShowCaption = true;
            this.splitContainerControl4.Panel1.Text = "Invoices";
            this.splitContainerControl4.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl4.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl4.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl4.Panel2.Controls.Add(this.gridControl6);
            this.splitContainerControl4.Panel2.ShowCaption = true;
            this.splitContainerControl4.Panel2.Text = "Invoice Callouts";
            this.splitContainerControl4.Size = new System.Drawing.Size(1124, 448);
            this.splitContainerControl4.SplitterPosition = 177;
            this.splitContainerControl4.TabIndex = 1;
            this.splitContainerControl4.Text = "splitContainerControl4";
            this.splitContainerControl4.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl4_SplitGroupPanelCollapsed);
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp04131GCTeamSelfBillingInvoicesBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl4.Size = new System.Drawing.Size(1099, 262);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp04131GCTeamSelfBillingInvoicesBindingSource
            // 
            this.sp04131GCTeamSelfBillingInvoicesBindingSource.DataMember = "sp04131_GC_Team_Self_Billing_Invoices";
            this.sp04131GCTeamSelfBillingInvoicesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "team_disabled_16.png");
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn20,
            this.gridColumn21,
            this.colInvoiceDate,
            this.colLinkedFileName,
            this.gridColumn23,
            this.colLinkedCalloutCount,
            this.colEmailPassword1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView4_CustomRowCellEdit);
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView4_ShowingEditor);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Invoice ID";
            this.gridColumn20.FieldName = "GrittingInvoiceID";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Team ID";
            this.gridColumn21.FieldName = "SubContractorID";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            // 
            // colInvoiceDate
            // 
            this.colInvoiceDate.Caption = "Invoice Date";
            this.colInvoiceDate.FieldName = "InvoiceDate";
            this.colInvoiceDate.Name = "colInvoiceDate";
            this.colInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colInvoiceDate.OptionsColumn.AllowFocus = false;
            this.colInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colInvoiceDate.Visible = true;
            this.colInvoiceDate.VisibleIndex = 1;
            this.colInvoiceDate.Width = 102;
            // 
            // colLinkedFileName
            // 
            this.colLinkedFileName.Caption = "Linked File Name";
            this.colLinkedFileName.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedFileName.FieldName = "LinkedFileName";
            this.colLinkedFileName.Name = "colLinkedFileName";
            this.colLinkedFileName.OptionsColumn.ReadOnly = true;
            this.colLinkedFileName.Visible = true;
            this.colLinkedFileName.VisibleIndex = 3;
            this.colLinkedFileName.Width = 557;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Team Name";
            this.gridColumn23.FieldName = "TeamName";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 234;
            // 
            // colLinkedCalloutCount
            // 
            this.colLinkedCalloutCount.Caption = "Linked Callouts";
            this.colLinkedCalloutCount.FieldName = "LinkedCalloutCount";
            this.colLinkedCalloutCount.Name = "colLinkedCalloutCount";
            this.colLinkedCalloutCount.OptionsColumn.AllowEdit = false;
            this.colLinkedCalloutCount.OptionsColumn.AllowFocus = false;
            this.colLinkedCalloutCount.OptionsColumn.ReadOnly = true;
            this.colLinkedCalloutCount.Visible = true;
            this.colLinkedCalloutCount.VisibleIndex = 2;
            this.colLinkedCalloutCount.Width = 92;
            // 
            // colEmailPassword1
            // 
            this.colEmailPassword1.Caption = "Email Password";
            this.colEmailPassword1.FieldName = "EmailPassword";
            this.colEmailPassword1.Name = "colEmailPassword1";
            this.colEmailPassword1.OptionsColumn.AllowEdit = false;
            this.colEmailPassword1.OptionsColumn.AllowFocus = false;
            this.colEmailPassword1.OptionsColumn.ReadOnly = true;
            this.colEmailPassword1.Width = 128;
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp04272GCCalloutsForInvoiceHeadersBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemCheckEdit5,
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEdit25KgBags4,
            this.repositoryItemTextEditCurrency4,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEdit2DP});
            this.gridControl6.Size = new System.Drawing.Size(1099, 174);
            this.gridControl6.TabIndex = 0;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp04272GCCalloutsForInvoiceHeadersBindingSource
            // 
            this.sp04272GCCalloutsForInvoiceHeadersBindingSource.DataMember = "sp04272_GC_Callouts_For_Invoice_Headers";
            this.sp04272GCCalloutsForInvoiceHeadersBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.gridColumn77,
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn83,
            this.gridColumn84,
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87,
            this.gridColumn88,
            this.gridColumn89,
            this.gridColumn90,
            this.gridColumn91,
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn94,
            this.gridColumn95,
            this.gridColumn96,
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99,
            this.gridColumn100,
            this.gridColumn101,
            this.gridColumn102,
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106,
            this.gridColumn107,
            this.gridColumn108,
            this.gridColumn109,
            this.gridColumn110,
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113,
            this.colSellBillingInvoiceDate});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 2;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsLayout.StoreFormatRules = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn43, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSellBillingInvoiceDate, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn51, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn32, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn30, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Callout ID";
            this.gridColumn27.FieldName = "GrittingCallOutID";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Site Gritting Contract ID";
            this.gridColumn28.FieldName = "SiteGrittingContractID";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Width = 136;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Site ID";
            this.gridColumn29.FieldName = "SiteID";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Site Name";
            this.gridColumn30.FieldName = "SiteName";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 4;
            this.gridColumn30.Width = 173;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Client ID";
            this.gridColumn31.FieldName = "ClientID";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Client Name";
            this.gridColumn32.FieldName = "ClientName";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 3;
            this.gridColumn32.Width = 108;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Company ID";
            this.gridColumn33.FieldName = "CompanyID";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 80;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Company Name";
            this.gridColumn34.FieldName = "CompanyName";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 5;
            this.gridColumn34.Width = 107;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Site X Coord.";
            this.gridColumn35.FieldName = "SiteXCoordinate";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Width = 84;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Site Y Coord.";
            this.gridColumn36.FieldName = "SiteYCoordinate";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 84;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Site Location X";
            this.gridColumn37.FieldName = "SiteLocationX";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 91;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Site Location Y";
            this.gridColumn38.FieldName = "SiteLocationY";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 91;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Site Telephone";
            this.gridColumn39.FieldName = "SiteTelephone";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 92;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Site Email";
            this.gridColumn40.FieldName = "SiteEmail";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Clients Site Code";
            this.gridColumn41.FieldName = "ClientsSiteCode";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 102;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Team ID";
            this.gridColumn42.FieldName = "SubContractorID";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Team Name";
            this.gridColumn43.FieldName = "SubContractorName";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Width = 218;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Is Direct Labour";
            this.gridColumn44.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn44.FieldName = "SubContractorIsDirectLabour";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 14;
            this.gridColumn44.Width = 97;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Original Team ID";
            this.gridColumn45.FieldName = "OriginalSubContractorID";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Width = 100;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Original Team Name";
            this.gridColumn46.FieldName = "OriginalSubContarctorName";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Width = 116;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Reactive";
            this.gridColumn47.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn47.FieldName = "Reactive";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 2;
            this.gridColumn47.Width = 62;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Job Status ID";
            this.gridColumn48.FieldName = "JobStatusID";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Width = 88;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Status";
            this.gridColumn49.FieldName = "CalloutStatusDescription";
            this.gridColumn49.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 0;
            this.gridColumn49.Width = 182;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Status Order";
            this.gridColumn50.FieldName = "CalloutStatusOrder";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Width = 83;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Callout Date\\Time";
            this.gridColumn51.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn51.FieldName = "CallOutDateTime";
            this.gridColumn51.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn51.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 1;
            this.gridColumn51.Width = 117;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Imported Forecast ID";
            this.gridColumn52.FieldName = "ImportedWeatherForecastID";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Width = 124;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Text Sent Time";
            this.gridColumn53.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn53.FieldName = "TextSentTime";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Width = 93;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Team Received Time";
            this.gridColumn54.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn54.FieldName = "SubContractorReceivedTime";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 119;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Team Responded Time";
            this.gridColumn55.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn55.FieldName = "SubContractorRespondedTime";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Width = 129;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Team ETA";
            this.gridColumn56.FieldName = "SubContractorETA";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Completed Time";
            this.gridColumn57.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn57.FieldName = "CompletedTime";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 6;
            this.gridColumn57.Width = 97;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Authorised By ID";
            this.gridColumn58.FieldName = "AuthorisedByStaffID";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Width = 102;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Authorised By";
            this.gridColumn59.FieldName = "AuthorisedByName";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 11;
            this.gridColumn59.Width = 88;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Aborted";
            this.gridColumn60.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn60.FieldName = "VisitAborted";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 12;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Aborted Reason";
            this.gridColumn61.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn61.FieldName = "AbortedReason";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.Visible = true;
            this.gridColumn61.VisibleIndex = 13;
            this.gridColumn61.Width = 99;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Start Lat";
            this.gridColumn62.FieldName = "StartLatitude";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Start Long";
            this.gridColumn63.FieldName = "StartLongitude";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Finished Lat";
            this.gridColumn64.FieldName = "FinishedLatitude";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.Width = 78;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Finished Long";
            this.gridColumn65.FieldName = "FinishedLongitude";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Width = 86;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "P.O. Number";
            this.gridColumn66.FieldName = "ClientPONumber";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Width = 83;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Salt Used";
            this.gridColumn67.ColumnEdit = this.repositoryItemTextEdit25KgBags4;
            this.gridColumn67.FieldName = "SaltUsed";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Visible = true;
            this.gridColumn67.VisibleIndex = 20;
            // 
            // repositoryItemTextEdit25KgBags4
            // 
            this.repositoryItemTextEdit25KgBags4.AutoHeight = false;
            this.repositoryItemTextEdit25KgBags4.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25KgBags4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KgBags4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KgBags4.Name = "repositoryItemTextEdit25KgBags4";
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Salt Cost";
            this.gridColumn68.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn68.FieldName = "SaltCost";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            this.gridColumn68.Visible = true;
            this.gridColumn68.VisibleIndex = 21;
            // 
            // repositoryItemTextEditCurrency4
            // 
            this.repositoryItemTextEditCurrency4.AutoHeight = false;
            this.repositoryItemTextEditCurrency4.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency4.Name = "repositoryItemTextEditCurrency4";
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Salt Sell";
            this.gridColumn69.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn69.FieldName = "SaltSell";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Visible = true;
            this.gridColumn69.VisibleIndex = 22;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Salt VAT Rate";
            this.gridColumn70.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.gridColumn70.FieldName = "SaltVatRate";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Visible = true;
            this.gridColumn70.VisibleIndex = 23;
            this.gridColumn70.Width = 87;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Hours Worked";
            this.gridColumn71.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.gridColumn71.FieldName = "HoursWorked";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Visible = true;
            this.gridColumn71.VisibleIndex = 15;
            this.gridColumn71.Width = 89;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Hourly Rate";
            this.gridColumn72.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn72.FieldName = "TeamHourlyRate";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Visible = true;
            this.gridColumn72.VisibleIndex = 16;
            this.gridColumn72.Width = 78;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Team Charge";
            this.gridColumn73.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn73.FieldName = "TeamCharge";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Visible = true;
            this.gridColumn73.VisibleIndex = 17;
            this.gridColumn73.Width = 85;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Labour Cost";
            this.gridColumn74.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn74.FieldName = "LabourCost";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Visible = true;
            this.gridColumn74.VisibleIndex = 18;
            this.gridColumn74.Width = 79;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Labour VAT Rate";
            this.gridColumn75.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.gridColumn75.FieldName = "LabourVatRate";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Visible = true;
            this.gridColumn75.VisibleIndex = 19;
            this.gridColumn75.Width = 102;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Other Cost";
            this.gridColumn76.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn76.FieldName = "OtherCost";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 24;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Other Sell";
            this.gridColumn77.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn77.FieldName = "OtherSell";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.Visible = true;
            this.gridColumn77.VisibleIndex = 25;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Total Cost";
            this.gridColumn78.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn78.FieldName = "TotalCost";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.Visible = true;
            this.gridColumn78.VisibleIndex = 26;
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Total Sell";
            this.gridColumn79.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn79.FieldName = "TotalSell";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.Visible = true;
            this.gridColumn79.VisibleIndex = 28;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Invoice Number";
            this.gridColumn80.FieldName = "ClientInvoiceNumber";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Width = 96;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Team Paid";
            this.gridColumn81.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn81.FieldName = "SubContractorPaid";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Gritting Invoice ID";
            this.gridColumn82.FieldName = "GrittingInvoiceID";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.Width = 108;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Recorded By Staff ID";
            this.gridColumn83.FieldName = "RecordedByStaffID";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Width = 123;
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Recorded By";
            this.gridColumn84.FieldName = "RecordedByName";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Width = 118;
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Paid By Staff ID";
            this.gridColumn85.FieldName = "PaidByStaffID";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Width = 97;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Paid By";
            this.gridColumn86.FieldName = "PaidByName";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.Width = 93;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Don\'t Pay Team";
            this.gridColumn87.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn87.FieldName = "DoNotPaySubContractor";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Visible = true;
            this.gridColumn87.VisibleIndex = 7;
            this.gridColumn87.Width = 96;
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Don\'t Pay Team Reason";
            this.gridColumn88.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn88.FieldName = "DoNotPaySubContractorReason";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Visible = true;
            this.gridColumn88.VisibleIndex = 8;
            this.gridColumn88.Width = 135;
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Grit Source Location ID";
            this.gridColumn89.FieldName = "GritSourceLocationID";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Width = 131;
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Grit Source Location Type ID";
            this.gridColumn90.FieldName = "GritSourceLocationTypeID";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Width = 158;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "No Access";
            this.gridColumn91.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn91.FieldName = "NoAccess";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            this.gridColumn91.Visible = true;
            this.gridColumn91.VisibleIndex = 33;
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Site Weather";
            this.gridColumn92.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn92.FieldName = "SiteWeather";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            this.gridColumn92.Width = 84;
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Site Temperature";
            this.gridColumn93.FieldName = "SiteTemperature";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Width = 104;
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Device ID";
            this.gridColumn94.FieldName = "PdaID";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Team Present";
            this.gridColumn95.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn95.FieldName = "TeamPresent";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            this.gridColumn95.Width = 87;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Service Failure";
            this.gridColumn96.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn96.FieldName = "ServiceFailure";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Visible = true;
            this.gridColumn96.VisibleIndex = 34;
            this.gridColumn96.Width = 91;
            // 
            // gridColumn97
            // 
            this.gridColumn97.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn97.FieldName = "Comments";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Visible = true;
            this.gridColumn97.VisibleIndex = 35;
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Client Price";
            this.gridColumn98.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn98.FieldName = "ClientPrice";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Visible = true;
            this.gridColumn98.VisibleIndex = 27;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Transfer Method";
            this.gridColumn99.FieldName = "GritJobTransferMethod";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            this.gridColumn99.Width = 101;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Snow On Site";
            this.gridColumn100.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn100.FieldName = "SnowOnSite";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.Width = 85;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Start Time";
            this.gridColumn101.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn101.FieldName = "StartTime";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.AllowEdit = false;
            this.gridColumn101.OptionsColumn.AllowFocus = false;
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "<b>Calculated 1</b>";
            this.gridColumn102.FieldName = "Calculated1";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.AllowEdit = false;
            this.gridColumn102.OptionsColumn.AllowFocus = false;
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.ShowUnboundExpressionMenu = true;
            this.gridColumn102.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn102.Visible = true;
            this.gridColumn102.VisibleIndex = 36;
            this.gridColumn102.Width = 90;
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "<b>Calculated 2</b>";
            this.gridColumn103.FieldName = "Calculated2";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            this.gridColumn103.ShowUnboundExpressionMenu = true;
            this.gridColumn103.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn103.Visible = true;
            this.gridColumn103.VisibleIndex = 37;
            this.gridColumn103.Width = 90;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "<b>Calculated 3</b>";
            this.gridColumn104.FieldName = "Calculated3";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            this.gridColumn104.ShowUnboundExpressionMenu = true;
            this.gridColumn104.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn104.Visible = true;
            this.gridColumn104.VisibleIndex = 38;
            this.gridColumn104.Width = 90;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Profit";
            this.gridColumn105.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn105.FieldName = "Profit";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Visible = true;
            this.gridColumn105.VisibleIndex = 29;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Markup";
            this.gridColumn106.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.gridColumn106.FieldName = "Markup";
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowFocus = false;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.Visible = true;
            this.gridColumn106.VisibleIndex = 30;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "P.O. Linked ID";
            this.gridColumn107.FieldName = "ClientPOID";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.AllowEdit = false;
            this.gridColumn107.OptionsColumn.AllowFocus = false;
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.Width = 90;
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "Non-Standard Cost";
            this.gridColumn108.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn108.FieldName = "NonStandardCost";
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowFocus = false;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            this.gridColumn108.Visible = true;
            this.gridColumn108.VisibleIndex = 31;
            this.gridColumn108.Width = 113;
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "Non-Standard Sell";
            this.gridColumn109.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn109.FieldName = "NonStandardSell";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowFocus = false;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.Visible = true;
            this.gridColumn109.VisibleIndex = 32;
            this.gridColumn109.Width = 107;
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "Do Not Invoice Client";
            this.gridColumn110.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn110.FieldName = "DoNotInvoiceClient";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowFocus = false;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            this.gridColumn110.Visible = true;
            this.gridColumn110.VisibleIndex = 9;
            this.gridColumn110.Width = 122;
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "Do Not Invoice Client Reason";
            this.gridColumn111.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn111.FieldName = "DoNotInvoiceClientReason";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            this.gridColumn111.Visible = true;
            this.gridColumn111.VisibleIndex = 10;
            this.gridColumn111.Width = 161;
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "Grit Mobile Telephone";
            this.gridColumn112.FieldName = "GritMobileTelephoneNumber";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.Width = 124;
            // 
            // gridColumn113
            // 
            this.gridColumn113.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn113.FieldName = "SelfBillingInvoice";
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.Width = 106;
            // 
            // colSellBillingInvoiceDate
            // 
            this.colSellBillingInvoiceDate.Caption = "Invoice Date";
            this.colSellBillingInvoiceDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colSellBillingInvoiceDate.FieldName = "SellBillingInvoiceDate";
            this.colSellBillingInvoiceDate.Name = "colSellBillingInvoiceDate";
            this.colSellBillingInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colSellBillingInvoiceDate.OptionsColumn.AllowFocus = false;
            this.colSellBillingInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colSellBillingInvoiceDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSellBillingInvoiceDate.Width = 115;
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bar1
            // 
            this.bar1.BarName = "Gritting Callout Toolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(501, 185);
            this.bar1.OptionsBar.AllowCollapse = true;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // bar2
            // 
            this.bar2.BarName = "Authorisation Toolbar";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(818, 251);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAuthorise1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiQuery1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDontPay1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemCompanyFilter, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemShowQueries),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemShowDontPay),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadCalloutData)});
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "Authorisation Toolbar";
            // 
            // bbiAuthorise1
            // 
            this.bbiAuthorise1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiAuthorise1.Caption = "Authorise";
            this.bbiAuthorise1.DropDownControl = this.pmAuthorise;
            this.bbiAuthorise1.Id = 37;
            this.bbiAuthorise1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAuthorise1.ImageOptions.Image")));
            this.bbiAuthorise1.Name = "bbiAuthorise1";
            this.bbiAuthorise1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Authorise Callout - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to mark the Callout record as Authorised for Payment.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiAuthorise1.SuperTip = superToolTip3;
            this.bbiAuthorise1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAuthorise1_ItemClick);
            // 
            // pmAuthorise
            // 
            this.pmAuthorise.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAuthorise2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUnAuthorise)});
            this.pmAuthorise.Manager = this.barManager1;
            this.pmAuthorise.MenuCaption = "Authorise Menu";
            this.pmAuthorise.Name = "pmAuthorise";
            // 
            // bbiAuthorise2
            // 
            this.bbiAuthorise2.Caption = "Authorise Callout";
            this.bbiAuthorise2.Id = 27;
            this.bbiAuthorise2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAuthorise2.ImageOptions.Image")));
            this.bbiAuthorise2.Name = "bbiAuthorise2";
            this.bbiAuthorise2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Authorise Callout - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to mark the Callout record as Authorised for Payment.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiAuthorise2.SuperTip = superToolTip1;
            this.bbiAuthorise2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAuthorise2_ItemClick);
            // 
            // bbiUnAuthorise
            // 
            this.bbiUnAuthorise.Caption = "Un-authorise Callout";
            this.bbiUnAuthorise.Id = 29;
            this.bbiUnAuthorise.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUnAuthorise.ImageOptions.Image")));
            this.bbiUnAuthorise.Name = "bbiUnAuthorise";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Un-authorise Callout - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>clear</b> the Authorise Payment Status of the the Callout record.\r" +
    "\nThe Status will then be changed back to Completed.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiUnAuthorise.SuperTip = superToolTip2;
            this.bbiUnAuthorise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUnAuthorise_ItemClick);
            // 
            // bbiQuery1
            // 
            this.bbiQuery1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiQuery1.Caption = "Query";
            this.bbiQuery1.DropDownControl = this.pmQuery;
            this.bbiQuery1.Id = 38;
            this.bbiQuery1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiQuery1.ImageOptions.Image")));
            this.bbiQuery1.Name = "bbiQuery1";
            this.bbiQuery1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiQuery1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiQuery1_ItemClick);
            // 
            // pmQuery
            // 
            this.pmQuery.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiQuery2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiQueryClear)});
            this.pmQuery.Manager = this.barManager1;
            this.pmQuery.MenuCaption = "Query Menu";
            this.pmQuery.Name = "pmQuery";
            // 
            // bbiQuery2
            // 
            this.bbiQuery2.Caption = "Callout - Query";
            this.bbiQuery2.Id = 31;
            this.bbiQuery2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiQuery2.ImageOptions.Image")));
            this.bbiQuery2.Name = "bbiQuery2";
            this.bbiQuery2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiQuery2_ItemClick);
            // 
            // bbiQueryClear
            // 
            this.bbiQueryClear.Caption = "Callout - Clear Query";
            this.bbiQueryClear.Id = 36;
            this.bbiQueryClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiQueryClear.ImageOptions.Image")));
            this.bbiQueryClear.Name = "bbiQueryClear";
            this.bbiQueryClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiQueryClear_ItemClick);
            // 
            // bbiDontPay1
            // 
            this.bbiDontPay1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiDontPay1.Caption = "Do Not Pay";
            this.bbiDontPay1.DropDownControl = this.pmDoNotPay;
            this.bbiDontPay1.Id = 39;
            this.bbiDontPay1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDontPay1.ImageOptions.Image")));
            this.bbiDontPay1.Name = "bbiDontPay1";
            this.bbiDontPay1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiDontPay1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDontPay1_ItemClick);
            // 
            // pmDoNotPay
            // 
            this.pmDoNotPay.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDontPay2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDoPay)});
            this.pmDoNotPay.Manager = this.barManager1;
            this.pmDoNotPay.MenuCaption = "Don\'t Pay Menu";
            this.pmDoNotPay.Name = "pmDoNotPay";
            // 
            // bbiDontPay2
            // 
            this.bbiDontPay2.Caption = "Callout - Do Not Pay";
            this.bbiDontPay2.Id = 34;
            this.bbiDontPay2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDontPay2.ImageOptions.Image")));
            this.bbiDontPay2.Name = "bbiDontPay2";
            this.bbiDontPay2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDontPay2_ItemClick);
            // 
            // bbiDoPay
            // 
            this.bbiDoPay.Caption = "Callout - Clear Do Not Pay";
            this.bbiDoPay.Id = 35;
            this.bbiDoPay.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDoPay.ImageOptions.Image")));
            this.bbiDoPay.Name = "bbiDoPay";
            this.bbiDoPay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDoPay_ItemClick);
            // 
            // barEditItemCompanyFilter
            // 
            this.barEditItemCompanyFilter.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItemCompanyFilter.Caption = "Company Filter";
            this.barEditItemCompanyFilter.Edit = this.repositoryItemPopupContainerEditCompanyFilter;
            this.barEditItemCompanyFilter.EditValue = "No Company Filter";
            this.barEditItemCompanyFilter.EditWidth = 112;
            this.barEditItemCompanyFilter.Id = 52;
            this.barEditItemCompanyFilter.Name = "barEditItemCompanyFilter";
            // 
            // repositoryItemPopupContainerEditCompanyFilter
            // 
            this.repositoryItemPopupContainerEditCompanyFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditCompanyFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditCompanyFilter.Name = "repositoryItemPopupContainerEditCompanyFilter";
            this.repositoryItemPopupContainerEditCompanyFilter.PopupControl = this.popupContainerControlCompanies;
            this.repositoryItemPopupContainerEditCompanyFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditCompanyFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditCompanyFilter_QueryResultValue);
            // 
            // barEditItemShowQueries
            // 
            this.barEditItemShowQueries.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItemShowQueries.Edit = this.repositoryItemCheckEditShowQueries;
            this.barEditItemShowQueries.EditWidth = 90;
            this.barEditItemShowQueries.Id = 48;
            this.barEditItemShowQueries.Name = "barEditItemShowQueries";
            // 
            // repositoryItemCheckEditShowQueries
            // 
            this.repositoryItemCheckEditShowQueries.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowQueries.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowQueries.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowQueries.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowQueries.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowQueries.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowQueries.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowQueries.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowQueries.AutoHeight = false;
            this.repositoryItemCheckEditShowQueries.Caption = "Show Queries:";
            this.repositoryItemCheckEditShowQueries.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowQueries.Name = "repositoryItemCheckEditShowQueries";
            this.repositoryItemCheckEditShowQueries.ValueChecked = 1;
            this.repositoryItemCheckEditShowQueries.ValueUnchecked = 0;
            // 
            // barEditItemShowDontPay
            // 
            this.barEditItemShowDontPay.Edit = this.repositoryItemCheckEditShowDontPay;
            this.barEditItemShowDontPay.EditWidth = 98;
            this.barEditItemShowDontPay.Id = 49;
            this.barEditItemShowDontPay.Name = "barEditItemShowDontPay";
            // 
            // repositoryItemCheckEditShowDontPay
            // 
            this.repositoryItemCheckEditShowDontPay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowDontPay.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowDontPay.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowDontPay.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowDontPay.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowDontPay.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowDontPay.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowDontPay.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowDontPay.AutoHeight = false;
            this.repositoryItemCheckEditShowDontPay.Caption = "Show Don\'t Pay:";
            this.repositoryItemCheckEditShowDontPay.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowDontPay.Name = "repositoryItemCheckEditShowDontPay";
            this.repositoryItemCheckEditShowDontPay.ValueChecked = 1;
            this.repositoryItemCheckEditShowDontPay.ValueUnchecked = 0;
            // 
            // bbiReloadCalloutData
            // 
            this.bbiReloadCalloutData.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiReloadCalloutData.Caption = "Reload Data";
            this.bbiReloadCalloutData.Id = 43;
            this.bbiReloadCalloutData.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiReloadCalloutData.ImageOptions.Image")));
            this.bbiReloadCalloutData.Name = "bbiReloadCalloutData";
            this.bbiReloadCalloutData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem4.Text = "Reload Data - information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to reload the list of Gritting Callouts.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiReloadCalloutData.SuperTip = superToolTip4;
            this.bbiReloadCalloutData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadCalloutData_ItemClick);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Query";
            this.barSubItem2.Id = 30;
            this.barSubItem2.Name = "barSubItem2";
            this.barSubItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Callout - Clear Query";
            this.barButtonItem4.Id = 32;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 3";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar3.FloatLocation = new System.Drawing.Point(794, 341);
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiInvoice1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditLayout, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadTeamData)});
            this.bar3.OptionsBar.DisableClose = true;
            this.bar3.OptionsBar.DisableCustomization = true;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar3.Text = "Custom 3";
            // 
            // bbiInvoice1
            // 
            this.bbiInvoice1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiInvoice1.Caption = "Invoice";
            this.bbiInvoice1.DropDownControl = this.pmInvoice;
            this.bbiInvoice1.Id = 40;
            this.bbiInvoice1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiInvoice1.ImageOptions.Image")));
            this.bbiInvoice1.Name = "bbiInvoice1";
            this.bbiInvoice1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem6.Text = "Invoice - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to generate and send Self-Billing Invoices to <b>selected</b> Gritting T" +
    "eams.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiInvoice1.SuperTip = superToolTip6;
            this.bbiInvoice1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiInvoice1_ItemClick);
            // 
            // pmInvoice
            // 
            this.pmInvoice.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiInvoiceSelectRecords),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiInvoice2)});
            this.pmInvoice.Manager = this.barManager1;
            this.pmInvoice.MenuCaption = "Invoice Menu";
            this.pmInvoice.Name = "pmInvoice";
            // 
            // bbiInvoiceSelectRecords
            // 
            this.bbiInvoiceSelectRecords.Caption = "Select Teams Requiring Invoice";
            this.bbiInvoiceSelectRecords.Id = 42;
            this.bbiInvoiceSelectRecords.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiInvoiceSelectRecords.ImageOptions.Image")));
            this.bbiInvoiceSelectRecords.Name = "bbiInvoiceSelectRecords";
            this.bbiInvoiceSelectRecords.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiInvoiceSelectRecords_ItemClick);
            // 
            // bbiInvoice2
            // 
            this.bbiInvoice2.Caption = "Invoice";
            this.bbiInvoice2.Id = 41;
            this.bbiInvoice2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiInvoice2.ImageOptions.Image")));
            this.bbiInvoice2.Name = "bbiInvoice2";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem5.Text = "Invoice - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to generate and send Self-Billing Invoices to <b>selected</b> Gritting T" +
    "eams.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiInvoice2.SuperTip = superToolTip5;
            this.bbiInvoice2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiInvoice2_ItemClick);
            // 
            // bbiEditLayout
            // 
            this.bbiEditLayout.Caption = "Edit Layout";
            this.bbiEditLayout.Id = 51;
            this.bbiEditLayout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEditLayout.ImageOptions.Image")));
            this.bbiEditLayout.Name = "bbiEditLayout";
            this.bbiEditLayout.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image12")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image13")));
            toolTipTitleItem7.Text = "Edit Layout - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = resources.GetString("toolTipItem7.Text");
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiEditLayout.SuperTip = superToolTip7;
            this.bbiEditLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditLayout_ItemClick);
            // 
            // bbiReloadTeamData
            // 
            this.bbiReloadTeamData.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiReloadTeamData.Caption = "Reload Data";
            this.bbiReloadTeamData.Id = 50;
            this.bbiReloadTeamData.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiReloadTeamData.Name = "bbiReloadTeamData";
            this.bbiReloadTeamData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image14")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image15")));
            toolTipTitleItem8.Text = "Reload Data - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to reload the Team lists.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiReloadTeamData.SuperTip = superToolTip8;
            this.bbiReloadTeamData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadTeamData_ItemClick);
            // 
            // barLinkContainerItem2
            // 
            this.barLinkContainerItem2.Caption = "barLinkContainerItem2";
            this.barLinkContainerItem2.Id = 44;
            this.barLinkContainerItem2.Name = "barLinkContainerItem2";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // barLinkContainerItem3
            // 
            this.barLinkContainerItem3.Caption = "barLinkContainerItem3";
            this.barLinkContainerItem3.Id = 46;
            this.barLinkContainerItem3.Name = "barLinkContainerItem3";
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 47;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp04119_GC_Authorisation_Teams_Self_BillingTableAdapter
            // 
            this.sp04119_GC_Authorisation_Teams_Self_BillingTableAdapter.ClearBeforeFill = true;
            // 
            // sp04120_GC_Authorisation_Teams_Non_Self_BillingTableAdapter
            // 
            this.sp04120_GC_Authorisation_Teams_Non_Self_BillingTableAdapter.ClearBeforeFill = true;
            // 
            // sp04121_GC_Callouts_For_InvoicingTableAdapter
            // 
            this.sp04121_GC_Callouts_For_InvoicingTableAdapter.ClearBeforeFill = true;
            // 
            // sp04131_GC_Team_Self_Billing_InvoicesTableAdapter
            // 
            this.sp04131_GC_Team_Self_Billing_InvoicesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04237GCCompanyFilterListBindingSource
            // 
            this.sp04237GCCompanyFilterListBindingSource.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // sp04237_GC_Company_Filter_ListTableAdapter
            // 
            this.sp04237_GC_Company_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04272_GC_Callouts_For_Invoice_HeadersTableAdapter
            // 
            this.sp04272_GC_Callouts_For_Invoice_HeadersTableAdapter.ClearBeforeFill = true;
            // 
            // pmCalloutsList
            // 
            this.pmCalloutsList.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectTeamsOnSelectedCallouts)});
            this.pmCalloutsList.Manager = this.barManager1;
            this.pmCalloutsList.MenuCaption = "Callouts - Menu";
            this.pmCalloutsList.Name = "pmCalloutsList";
            this.pmCalloutsList.ShowCaption = true;
            // 
            // bbiSelectTeamsOnSelectedCallouts
            // 
            this.bbiSelectTeamsOnSelectedCallouts.Caption = "Select Teams based on Selected Callouts";
            this.bbiSelectTeamsOnSelectedCallouts.Id = 53;
            this.bbiSelectTeamsOnSelectedCallouts.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectTeamsOnSelectedCallouts.ImageOptions.Image")));
            this.bbiSelectTeamsOnSelectedCallouts.Name = "bbiSelectTeamsOnSelectedCallouts";
            superToolTip13.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem13.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Text = "Select Teams Based on Selected Callouts - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = resources.GetString("toolTipItem13.Text");
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.bbiSelectTeamsOnSelectedCallouts.SuperTip = superToolTip13;
            this.bbiSelectTeamsOnSelectedCallouts.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectTeamsOnSelectedCallouts_ItemClick);
            // 
            // bar4
            // 
            this.bar4.BarName = "Historical Self Billing";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar4.FloatLocation = new System.Drawing.Point(492, 195);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiFromDate),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiToDate),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLoadData, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRegeneratePDF, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEmail, true)});
            this.bar4.OptionsBar.DisableClose = true;
            this.bar4.OptionsBar.DisableCustomization = true;
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.StandaloneBarDockControl = this.standaloneBarDockControl3;
            this.bar4.Text = "Historical Self Billing";
            // 
            // beiFromDate
            // 
            this.beiFromDate.Caption = "From Date:";
            this.beiFromDate.Edit = this.repositoryItemDateEditFrom;
            this.beiFromDate.EditWidth = 150;
            this.beiFromDate.Id = 54;
            this.beiFromDate.Name = "beiFromDate";
            this.beiFromDate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemDateEditFrom
            // 
            this.repositoryItemDateEditFrom.AutoHeight = false;
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image16")));
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image17")));
            toolTipTitleItem9.Text = "Clear Date - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all re" +
    "cords will be loaded.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.repositoryItemDateEditFrom.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "delete", superToolTip9, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemDateEditFrom.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditFrom.Mask.EditMask = "g";
            this.repositoryItemDateEditFrom.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEditFrom.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.repositoryItemDateEditFrom.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.repositoryItemDateEditFrom.Name = "repositoryItemDateEditFrom";
            this.repositoryItemDateEditFrom.NullValuePrompt = "No Date";
            this.repositoryItemDateEditFrom.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemDateEditFrom_ButtonClick);
            // 
            // beiToDate
            // 
            this.beiToDate.Caption = "To Date:";
            this.beiToDate.Edit = this.repositoryItemDateEditTo;
            this.beiToDate.EditWidth = 150;
            this.beiToDate.Id = 55;
            this.beiToDate.Name = "beiToDate";
            this.beiToDate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemDateEditTo
            // 
            this.repositoryItemDateEditTo.AutoHeight = false;
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image18")));
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image19")));
            toolTipTitleItem10.Text = "Clear Date - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all re" +
    "cords will be loaded.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.repositoryItemDateEditTo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "delete", superToolTip10, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemDateEditTo.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditTo.Mask.EditMask = "g";
            this.repositoryItemDateEditTo.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEditTo.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.repositoryItemDateEditTo.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.repositoryItemDateEditTo.Name = "repositoryItemDateEditTo";
            this.repositoryItemDateEditTo.NullValuePrompt = "No Date";
            this.repositoryItemDateEditTo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemDateEditTo_ButtonClick);
            // 
            // bbiLoadData
            // 
            this.bbiLoadData.Caption = "Load Data";
            this.bbiLoadData.Id = 56;
            this.bbiLoadData.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiLoadData.Name = "bbiLoadData";
            this.bbiLoadData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiLoadData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadData_ItemClick);
            // 
            // bbiRegeneratePDF
            // 
            this.bbiRegeneratePDF.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRegeneratePDF.Caption = "Recreate PDF";
            this.bbiRegeneratePDF.Id = 57;
            this.bbiRegeneratePDF.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRegeneratePDF.ImageOptions.Image")));
            this.bbiRegeneratePDF.Name = "bbiRegeneratePDF";
            this.bbiRegeneratePDF.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem11.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image20")));
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image21")));
            toolTipTitleItem11.Text = "Recreate PDF - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to regenerate the Historical Self-Billing Invoice PDF file for selected " +
    "Invoice Header(s).";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.bbiRegeneratePDF.SuperTip = superToolTip11;
            this.bbiRegeneratePDF.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRegeneratePDF_ItemClick);
            // 
            // bbiEmail
            // 
            this.bbiEmail.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiEmail.Caption = "Email";
            this.bbiEmail.Id = 58;
            this.bbiEmail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEmail.ImageOptions.Image")));
            this.bbiEmail.Name = "bbiEmail";
            this.bbiEmail.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem12.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image22")));
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image23")));
            toolTipTitleItem12.Text = "Email Team - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to re-email the selected Self-Billing Invoice(s) to the team(s).";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.bbiEmail.SuperTip = superToolTip12;
            this.bbiEmail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEmail_ItemClick);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // frm_GC_Team_Self_Billing_Invoicing
            // 
            this.ClientSize = new System.Drawing.Size(1129, 515);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Team_Self_Billing_Invoicing";
            this.Text = "Gritting Callout Authorisation and Self Billing Invoice Manager";
            this.Activated += new System.EventHandler(this.frm_GC_Team_Self_Billing_Invoicing_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Team_Self_Billing_Invoicing_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Team_Self_Billing_Invoicing_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04119GCAuthorisationTeamsSelfBillingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgBags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04120GCAuthorisationTeamsNonSelfBillingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgbags2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).EndInit();
            this.popupContainerControlCompanies.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04121GCCalloutsForInvoicingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgbags3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVATRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04131GCTeamSelfBillingInvoicesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04272GCCalloutsForInvoiceHeadersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmAuthorise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmQuery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDoNotPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowQueries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowDontPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmCalloutsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditFrom.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditTo.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditTo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAuthorise2;
        private DevExpress.XtraBars.BarButtonItem bbiUnAuthorise;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem bbiQuery2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem bbiDontPay2;
        private DevExpress.XtraBars.BarButtonItem bbiDoPay;
        private DevExpress.XtraBars.BarButtonItem bbiQueryClear;
        private DevExpress.XtraBars.BarButtonItem bbiAuthorise1;
        private DevExpress.XtraBars.PopupMenu pmAuthorise;
        private DevExpress.XtraBars.BarButtonItem bbiQuery1;
        private DevExpress.XtraBars.PopupMenu pmQuery;
        private DevExpress.XtraBars.BarButtonItem bbiDontPay1;
        private DevExpress.XtraBars.PopupMenu pmDoNotPay;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarButtonItem bbiInvoice1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraBars.PopupMenu pmInvoice;
        private DevExpress.XtraBars.BarButtonItem bbiInvoiceSelectRecords;
        private DevExpress.XtraBars.BarButtonItem bbiInvoice2;
        private DevExpress.XtraBars.BarButtonItem bbiReloadCalloutData;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem2;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem3;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.BarEditItem barEditItemShowQueries;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowQueries;
        private DevExpress.XtraBars.BarEditItem barEditItemShowDontPay;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowDontPay;
        private DevExpress.XtraBars.BarButtonItem bbiReloadTeamData;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource sp04119GCAuthorisationTeamsSelfBillingBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorGritInformationID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colHoldsStock;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentGritLevel;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25kgBags;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colUrgentGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colStockSuppliedByDepotID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellBillingInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingFrequencyDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingFrequencyDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colIsDirectLabour;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colHourlyProactiveRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colHourlyReactiveRate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceCount;
        private DevExpress.XtraGrid.Columns.GridColumn colUninvoicedJobCount;
        private DataSet_GC_CoreTableAdapters.sp04119_GC_Authorisation_Teams_Self_BillingTableAdapter sp04119_GC_Authorisation_Teams_Self_BillingTableAdapter;
        private System.Windows.Forms.BindingSource sp04120GCAuthorisationTeamsNonSelfBillingBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25kgbags2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DataSet_GC_CoreTableAdapters.sp04120_GC_Authorisation_Teams_Non_Self_BillingTableAdapter sp04120_GC_Authorisation_Teams_Non_Self_BillingTableAdapter;
        private System.Windows.Forms.BindingSource sp04121GCCalloutsForInvoicingBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCallOutID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorIsDirectLabour;
        private DevExpress.XtraGrid.Columns.GridColumn colGritMobileTelephoneNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContarctorName;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colImportedWeatherForecastID;
        private DevExpress.XtraGrid.Columns.GridColumn colTextSentTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorReceivedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorRespondedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorETA;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitAborted;
        private DevExpress.XtraGrid.Columns.GridColumn colAbortedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishedLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishedLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltSell;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamHourlyRate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamCharge;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCost;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherSell;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByName;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractorReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClientReason;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSourceLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSourceLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteWeather;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTemperature;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamPresent;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceFailure;
        private DevExpress.XtraGrid.Columns.GridColumn colComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colGritJobTransferMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colProfit;
        private DevExpress.XtraGrid.Columns.GridColumn colMarkup;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardSell;
        private DataSet_GC_CoreTableAdapters.sp04121_GC_Callouts_For_InvoicingTableAdapter sp04121_GC_Callouts_For_InvoicingTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25kgbags3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedJobCount1;
        private DevExpress.XtraBars.BarButtonItem bbiEditLayout;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.BindingSource sp04131GCTeamSelfBillingInvoicesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedFileName;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedCalloutCount;
        private DataSet_GC_CoreTableAdapters.sp04131_GC_Team_Self_Billing_InvoicesTableAdapter sp04131_GC_Team_Self_Billing_InvoicesTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCompanies;
        private DevExpress.XtraEditors.SimpleButton btnCompanyFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOrder;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource;
        private DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter sp04237_GC_Company_Filter_ListTableAdapter;
        private DevExpress.XtraBars.BarEditItem barEditItemCompanyFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditCompanyFilter;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private System.Windows.Forms.BindingSource sp04272GCCalloutsForInvoiceHeadersBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KgBags4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraGrid.Columns.GridColumn colSellBillingInvoiceDate;
        private DataSet_GC_CoreTableAdapters.sp04272_GC_Callouts_For_Invoice_HeadersTableAdapter sp04272_GC_Callouts_For_Invoice_HeadersTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiSelectTeamsOnSelectedCallouts;
        private DevExpress.XtraBars.PopupMenu pmCalloutsList;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarEditItem beiFromDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditFrom;
        private DevExpress.XtraBars.BarEditItem beiToDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditTo;
        private DevExpress.XtraBars.BarButtonItem bbiLoadData;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl3;
        private DevExpress.XtraBars.BarButtonItem bbiRegeneratePDF;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword1;
        private DevExpress.XtraBars.BarButtonItem bbiEmail;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Callout_Wizard_Add : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        RepositoryItemTextEdit disabledAddressEditor;

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        decimal decVatRate = (decimal)0.00;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        private string i_str_selected_CallOutType_ids = "";
        private string i_str_selected_CallOutType_names = "";
        public string strLinkedRecordType = "No Linked Records";
        int intLinkedRecordType = 0;  // No Linked Records //
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        public string i_str_SiteIDsToLink = "";  // Populated via Snow Clearance Manager (passed through the Gritting Callout Manager) when it needs to create Gritting Callouts for linking to the Snow Callouts //

        string i_str_selected_Company_ids = "";
        string i_str_selected_Company_names = "";
        BaseObjects.GridCheckMarksSelection selection3;

        #endregion

        public frm_GC_Callout_Wizard_Add()
        {
            InitializeComponent();
        }

        private void frm_GC_Callout_Wizard_Add_Load(object sender, EventArgs e)
        {
            this.FormID = 400045;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;  // Hide Table Page headers so it doesn't look like a pageframe //
            xtraTabControl1.SelectedTabPage = this.xtraTabPage1;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04186_GC_Gritting_Callout_Wizard_Add_Select_SitesTableAdapter.Connection.ConnectionString = strConnectionString;
            // Add record selection checkboxes to popup Callout Type grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04187_GC_Gritting_Callout_Wizard_Add_Callout_TemplateTableAdapter.Connection.ConnectionString = strConnectionString;
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Add record selection checkboxes to popup Callout Type grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            sp04001_GC_Job_CallOut_StatusesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04001_GC_Job_CallOut_StatusesTableAdapter.Fill(dataSet_GC_Core.sp04001_GC_Job_CallOut_Statuses);
            gridControl5.ForceInitialize();
            dateEditFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditToDate.DateTime = this.GlobalSettings.ViewedEndDate;
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Get Default VAT Rate //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                decVatRate = Convert.ToDecimal(GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate"));
            }
            catch (Exception)
            {
                decVatRate = (decimal)0.00;
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            emptyEditor = new RepositoryItem();
            disabledAddressEditor = new RepositoryItemTextEdit();
            disabledAddressEditor.ReadOnly = true;

            // Add record selection checkboxes to popup Company Filter grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;
            sp04237_GC_Company_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04237_GC_Company_Filter_ListTableAdapter.Fill(dataSet_GC_Reports.sp04237_GC_Company_Filter_List);
            gridControl3.ForceInitialize();

        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            labelLinkingRecordsInfo.Visible = false;
            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();
            ConfigureFormAccordingToMode();
            Load_Site_Contracts();

            if (!string.IsNullOrEmpty(this.i_str_SiteIDsToLink))  // Site IDs to select have been passed in from the Snow Clearance Manager via the Gritting Callout Manager //
            {
                gridControl1.ForceInitialize();
                // Pre-select Sites //
                Array arrayItems = i_str_SiteIDsToLink.Split(',');  // Single quotes because char expected for delimeter //
                GridView view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayItems)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["SiteID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
                labelLinkingRecordsInfo.Visible = true;
                btnNext1.PerformClick();  // Move to Step 1 Page so ticked Sites are visible //
            }
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }


        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_GC_Callout_Wizard_Add_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // From Date //
                string strFromDate = default_screen_settings.RetrieveSetting("FromDate");
                if (!string.IsNullOrEmpty(strFromDate)) dateEditFromDate.DateTime = Convert.ToDateTime(strFromDate);

                // To Date //
                string strToDate = default_screen_settings.RetrieveSetting("ToDate");
                if (!string.IsNullOrEmpty(strToDate)) dateEditToDate.DateTime = Convert.ToDateTime(strToDate);

                // Linked Record Type //
                strLinkedRecordType = default_screen_settings.RetrieveSetting("LinkedRecordType");
                if (string.IsNullOrEmpty(strLinkedRecordType)) strLinkedRecordType = "Gritting Callouts";
                barEditItemLinkedRecordType.EditValue = strLinkedRecordType;

                 // Gritting Callout Filter //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("CallOutFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["Value"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    barEditItemCalloutStatusFilter.EditValue = PopupContainerEdit2_Get_Selected();
                }
                // Company Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("CompanyFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl3.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["CompanyID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    barEditItemCompanyFilter.EditValue = PopupContainerEditCompanies_Get_Selected();
                }

                //LoadContractorsBtn.PerformClick();
            }
            intLinkedRecordType = (strLinkedRecordType == "Gritting Callouts" ? 2 : 1);
         }

        private void frm_GC_Callout_Wizard_Add_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate", dateEditFromDate.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDate", dateEditToDate.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "LinkedRecordType", strLinkedRecordType);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CallOutFilter", i_str_selected_CallOutType_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }


        #region Navigation Buttons

        private void btnNext1_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
        }

        private void btnPrevious2_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
        }

        private void bntNext2_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         
            
            if (selection1.SelectedCount <= 0)
            {
                XtraMessageBox.Show("Select at least 1 site to add a callout to before proceeding.", "Add Callout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            Load_Selected_Site_contracts();
            gridControl2.ForceInitialize();
            CheckRows((GridView)gridControl2.MainView);
            xtraTabControl1.SelectedTabPage = this.xtraTabPage3;
        }

        private void btnPrevious3_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
        }

        private void btnNext3_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         
           
            int intInvalidRows = CheckRows((GridView)gridControl2.MainView);
            if (intInvalidRows > 0)
            {
                XtraMessageBox.Show("You have one or more records with errors in them (errors have a cross icon next to them). Please correct before proceeding!", "Add Callout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check how many jobs have a site and team (ready for sending) //
            int intJobsAvailableForSending = 0;
            foreach (DataRow dr in dataSet_GC_Core.sp04187_GC_Gritting_Callout_Wizard_Add_Callout_Template.Rows)
            {
                if (Convert.ToInt32(dr["SubContractorID"]) != 0 && Convert.ToInt32(dr["SiteID"]) != 0) intJobsAvailableForSending++;
                dr["JobStatusID"] = 30;  // Force all statuses back to 30 (Started - To Be Completed - Authorised) in case someone has cleared a required value. On Finish, these will be recalculated //
            }
            checkEdit2.Enabled = (intJobsAvailableForSending > 0 ? true : false);
            if (checkEdit2.Enabled)
            {
                checkEdit2.Checked = true;
            }
            else
            {
                checkEdit1.Checked = true;
            }
            labelControlNumberOfJobsReadyToSend.Text = "        " + intJobsAvailableForSending.ToString() +" of " + gridControl2.MainView.DataRowCount.ToString() + " New Callouts Ready To Send";
            if (intJobsAvailableForSending != gridControl2.MainView.DataRowCount || intJobsAvailableForSending <= 0)
            {
                labelControlNumberOfJobsReadyToSend.Appearance.ForeColor = Color.Red;
                labelControlNumberOfJobsReadyToSend.Appearance.ImageIndex = 1;
            }
            else
            {
                Skin skin = CommonSkins.GetSkin(this.LookAndFeel);
                SkinElement element = skin[CommonSkins.SkinLabel];
                Color color = element.Color.ForeColor;
                labelControlNumberOfJobsReadyToSend.Appearance.ForeColor = color;
                labelControlNumberOfJobsReadyToSend.Appearance.ImageIndex = 0;
            }
            xtraTabControl1.SelectedTabPage = this.xtraTabPage4;
        }

        private void btnPrevious4_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPage3;
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                view.EndUpdate();
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("Column Error on current row in the Callout Grid - Correct before procceeding.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (checkEdit2.Checked)  // Need to complete the jobs so we can send them from the Callout Manager screen //
            {
                foreach (DataRow dr in dataSet_GC_Core.sp04187_GC_Gritting_Callout_Wizard_Add_Callout_Template.Rows)
                {
                    if (Convert.ToInt32(dr["SubContractorID"]) != 0 && Convert.ToInt32(dr["SiteID"]) != 0)
                    {
                        dr["JobStatusID"] = 50;  // Ready To Send //
                    }
                }
            }

            try
            {
                this.sp04187_GC_Gritting_Callout_Wizard_Add_Callout_TemplateTableAdapter.Update(dataSet_GC_Core);  // Insert query defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                view.EndUpdate();
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the callout [" + ex.Message + "]!\n\nTry clicking Finish again - if the problem persists, contact Technical Support.", "Save Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.EndUpdate();

            // Get New IDs so we can pass them back to the Callout Manager //
            string strNewIDs = "";
            if (checkEdit2.Checked)  // Just the Completed new jobs //
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 50) strNewIDs += view.GetRowCellValue(i, "GrittingCallOutID").ToString() + ";";
                }
            }
            else  // all new jobs //
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    strNewIDs += view.GetRowCellValue(i, "GrittingCallOutID").ToString() + ";";
                }
            }
            // Work Out What the User wants to do next... //
            string strDoWhat = "";
            if (checkEdit3.Checked)  // Open new callouts for editing //
            {
                strDoWhat = "edit";
            }
            else if (checkEdit4.Checked)
            {
                strDoWhat = "blockedit";
            }
            else if (checkEdit2.Checked)
            {
                strDoWhat = "send";
            }

            // Notify any open instances of Snow Clearance Callout Manager they will need to refresh their data on activating //
            frm_GC_Callout_Manager fParentForm;
            foreach (Form frmChild in this.ParentForm.MdiChildren)
            {
                if (frmChild.Name == "frm_GC_Callout_Manager")
                {
                    fParentForm = (frm_GC_Callout_Manager)frmChild;
                    fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", strDoWhat, "", "");
                }
            }

            fProgress.SetProgressValue(100);
            fProgress.UpdateCaption("Changes Saved");
            Application.DoEvents();
            System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();

            this.Close();
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Site Gritting Contracts Available";
                    break;
                case "gridView2":
                    message = "No Sites selected for adding callouts to";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;

            if (e.Column.FieldName == "LastCalloutDateTime")
            {
                if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "LastCalloutDateTime").ToString())) return;
                DateTime? dtLastCallout = Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "LastCalloutDateTime"));
                if (dtLastCallout.HasValue)
                {
                    DateTime currentDateTime = DateTime.Now;
                    TimeSpan span = currentDateTime.Subtract((DateTime)dtLastCallout);
                    if (span.Days < 1 && span.Hours < 24)
                    {
                        //e.Appearance.BackColor = Color.LightCoral;
                        //e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                }
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedRecordCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedRecordCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedRecordCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedRecordCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strSiteIDs = view.GetRowCellValue(view.FocusedRowHandle, "SiteGrittingContractID").ToString() + ",";
            if (barEditItemLinkedRecordType.EditValue.ToString() == "Gritting Callouts")
            {
                DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                DateTime FromDate = (string.IsNullOrEmpty(dateEditFromDate.DateTime.ToString()) ? Convert.ToDateTime("2011-01-01") : dateEditFromDate.DateTime);
                DateTime ToDate = (string.IsNullOrEmpty(dateEditToDate.DateTime.ToString()) ? Convert.ToDateTime("2500-12-31") : dateEditToDate.DateTime.AddDays(1).AddSeconds(-1));
                string strRecordIDs = GetSetting.sp04077_GC_DrillDown_Get_Linked_Callouts(strSiteIDs, "site_contract", i_str_selected_CallOutType_ids, FromDate, ToDate).ToString();
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "site_gritting_callouts");
            }
        }

        #endregion


        #region GridView2

        private void gridView2_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;

            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.Color.Gray;
            //if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.BackColor = SystemColors.ControlLight;
            if (e.Column.FieldName == "SubContractorName")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SubContractorID")) == 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0xFC, 0x80);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x7E, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "SiteAddressLine1")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsFloatingSite")) == 1 && string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "SiteAddressLine1").ToString()))
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0xFC, 0x80);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x7E, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "SiteLat")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsFloatingSite")) == 1 && Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "SiteLat")) == (double)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0xFC, 0x80);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x7E, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "SiteLong")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsFloatingSite")) == 1 && Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "SiteLong")) == (double)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0xFC, 0x80);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x7E, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles;
                intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length <= 1)
                {
                    bbiBlockEditTeam.Enabled = false;
                    bbiBlockEditClientPO.Enabled = false;
                    bbiBlockEditOtherValues.Enabled = false;
                }
                else
                {
                    bbiBlockEditTeam.Enabled = true;
                    bbiBlockEditClientPO.Enabled = true;
                    bbiBlockEditOtherValues.Enabled = true;
                }
                bsiSaltUsed.Enabled = (intRowHandles.Length < 1 ? false : true);
                bbiResetSalt.Enabled = (intRowHandles.Length < 1 ? false : true);
                bbiDoubleSalt.Enabled = (intRowHandles.Length < 1 ? false : true);
                pmBlockEdit.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Code also in bntNext2_Click event to validate all the rows initially //
            GridView view = (GridView)sender;
            int intFocusedRow = view.FocusedRowHandle;
            switch (view.FocusedColumn.Name)
            {
                case "colClientPONumber":
                    try
                    {
                        DataRow dr = view.GetDataRow(intFocusedRow);
                        if (string.IsNullOrEmpty(e.Value.ToString().Trim()) && Convert.ToInt32(dr["ClientPOID"]) == 0 && Convert.ToInt32(dr["ClientPONumberRequired"]) == 1)
                        {
                            dr.SetColumnError("ClientPONumber", view.Columns["ClientPONumber"].Caption + ": Missing value!");  // Flag row as error until a PO Number has been entered //
                            dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        else
                        {
                            dr.SetColumnError("ClientPONumber", "");  // Flag row as error until a PO Number has been entered //
                            dr.RowError = "";
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "colClientPOID":
                    try
                    {
                        DataRow dr = view.GetDataRow(intFocusedRow);
                        if (Convert.ToInt32(e.Value) == 0 && string.IsNullOrEmpty(dr["ClientPONumber"].ToString().Trim()) && Convert.ToInt32(dr["ClientPONumberRequired"]) == 1)
                        {
                            dr.SetColumnError("ClientPONumber", view.Columns["ClientPONumber"].Caption + ": Missing value!");  // Flag row as error until a PO Number has been entered //
                            dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        else
                        {
                            dr.SetColumnError("ClientPONumber", "");  // Flag row as error until a PO Number has been entered //
                            dr.RowError = "";
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
        }

        private void gridView2_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "SiteAddressLine1":
                case "SiteAddressLine2":
                case "SiteAddressLine3":
                case "SiteAddressLine4":
                case "SiteAddressLine5":
                case "SitePostcode":
                case "SiteLat":
                case "SiteLong":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsFloatingSite")) == 0) e.RepositoryItem = disabledAddressEditor;
                    break;
                default:
                    break;
            }
        }
        
        private void gridView2_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "SiteAddressLine1":
                case "SiteAddressLine2":
                case "SiteAddressLine3":
                case "SiteAddressLine4":
                case "SiteAddressLine5":
                case "SitePostcode":
                case "SiteLat":
                case "SiteLong":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("IsFloatingSite")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Callout Status Filter Panel

        private void btnGritCalloutFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_CallOutType_ids = "";    // Reset any prior values first //
            i_str_selected_CallOutType_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CallOutType_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CallOutType_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CallOutType_names;
        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        private void repositoryItemComboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit cbe = (ComboBoxEdit)sender;
            switch (cbe.Text)
            {
                case "No Linked Records":
                    intLinkedRecordType = 1;
                    barEditItemCalloutStatusFilter.Enabled = false;
                    break;
                case "Gritting Callouts":
                    intLinkedRecordType = 2;
                    barEditItemCalloutStatusFilter.Enabled = true;
                    break;
                default:
                    break;
            }
            strLinkedRecordType = cbe.Text;
        }

        private void bbiReloadGrid_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Site_Contracts();
        }

        #endregion


        #region Company Filter Panel

        private void repositoryItemPopupContainerEditCompanyFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCompanies_Get_Selected();
        }

        private void btnCompanyFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditCompanies_Get_Selected()
        {
            i_str_selected_Company_ids = "";    // Reset any prior values first //
            i_str_selected_Company_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Company_ids += Convert.ToString(view.GetRowCellValue(i, "CompanyID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Company_names = Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Company_names += ", " + Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Company_names;
        }

        #endregion


        private void Load_Site_Contracts()
        {
            WaitDialogForm loading = new WaitDialogForm("Loading Sites...", "Gritting Callout Wizard");
            if (fProgress == null)
            {
                loading.Show();
            }
            else
            {
                loading.Hide();
                loading.Dispose();
            }
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            try
            {
                sp04186_GC_Gritting_Callout_Wizard_Add_Select_SitesTableAdapter.Fill(this.dataSet_GC_Core.sp04186_GC_Gritting_Callout_Wizard_Add_Select_Sites, intLinkedRecordType, dtFromDate, dtToDate, i_str_selected_CallOutType_ids, i_str_selected_Company_ids);
            }
            catch (Exception)
            {
            }
            view.EndUpdate();
            if (fProgress == null)
            {
                loading.Close();
                loading.Dispose();
            }
        }

        private void Load_Selected_Site_contracts()
        {
            WaitDialogForm loading = new WaitDialogForm("Loading Selected Site Details...", "Gritting Callout Wizard");
            loading.Show();

            gridControl2.MainView.BeginUpdate();
            this.dataSet_GC_Core.sp04187_GC_Gritting_Callout_Wizard_Add_Callout_Template.Rows.Clear();
            GridView view = (GridView)gridControl1.MainView;
            int intCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    int intGrittingContractID = Convert.ToInt32(view.GetRowCellValue(i, "SiteGrittingContractID"));
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_Core.sp04187_GC_Gritting_Callout_Wizard_Add_Callout_Template.NewRow();
                        drNewRow["SiteGrittingContractID"] = intGrittingContractID;
                        drNewRow["SubContractorID"] = 0;
                        drNewRow["SiteName"] = view.GetRowCellValue(i, "SiteName").ToString();
                        drNewRow["SiteID"] = Convert.ToInt32(view.GetRowCellValue(i, "SiteID"));
                        drNewRow["ClientName"] = view.GetRowCellValue(i, "ClientName").ToString();
                        drNewRow["Reactive"] = 1;  // Manually added records [not generated from a forecast] are always Reactive //
                        drNewRow["JobStatusID"] = 30;  // 30 = Started - To Be Completed - Authorised //
                        drNewRow["CallOutDateTime"] = DateTime.Now;
                        drNewRow["RecordedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["RecordedByName"] = (string.IsNullOrEmpty(this.GlobalSettings.UserSurname) ? "" : this.GlobalSettings.UserSurname + ": ") + (string.IsNullOrEmpty(this.GlobalSettings.UserForename) ? "" : this.GlobalSettings.UserForename);
                        drNewRow["SaltUsed"] = Convert.ToInt32(view.GetRowCellValue(i, "DefaultGritAmount"));
                        drNewRow["OriginalSaltUsed"] = Convert.ToInt32(view.GetRowCellValue(i, "DefaultGritAmount"));
                        drNewRow["ServiceFailure"] = 0;
                        drNewRow["SiteAddressLine1"] = view.GetRowCellValue(i, "SiteAddressLine1").ToString();
                        drNewRow["SiteAddressLine2"] = view.GetRowCellValue(i, "SiteAddressLine2").ToString();
                        drNewRow["SiteAddressLine3"] = view.GetRowCellValue(i, "SiteAddressLine3").ToString();
                        drNewRow["SiteAddressLine4"] = view.GetRowCellValue(i, "SiteAddressLine4").ToString();
                        drNewRow["SiteAddressLine5"] = view.GetRowCellValue(i, "SiteAddressLine5").ToString();
                        drNewRow["SitePostcode"] = view.GetRowCellValue(i, "SitePostcode").ToString();
                        drNewRow["SiteCode"] = view.GetRowCellValue(i, "SiteCode").ToString();
                        drNewRow["ClientsSiteID"] = view.GetRowCellValue(i, "ClientsSiteID").ToString();
                        drNewRow["ClientsSiteCode"] = view.GetRowCellValue(i, "ClientsSiteCode").ToString();
                        drNewRow["ClientPONumberRequired"] = Convert.ToInt32(view.GetRowCellValue(i, "ClientPONumberRequired"));
                        drNewRow["SnowOnSite"] = 0;
                        drNewRow["EveningRateModifier"] = Convert.ToDecimal(view.GetRowCellValue(i, "EveningRateModifier"));
                        drNewRow["ClientPOID"] = view.GetRowCellValue(i, "ClientPOID").ToString();
                        drNewRow["ClientPOIDDescription"] = view.GetRowCellValue(i, "ClientPOIDDescription").ToString();
                        drNewRow["SaltSell"] = Convert.ToDecimal(view.GetRowCellValue(i, "ClientSaltPrice"));
                        drNewRow["SiteLat"] = Convert.ToDouble(view.GetRowCellValue(i, "SiteLat"));
                        drNewRow["SiteLong"] = Convert.ToDouble(view.GetRowCellValue(i, "SiteLong"));
                        drNewRow["IsFloatingSite"] = Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite"));
                        drNewRow["AttendanceOrder"] = 0;
                        Get_Default_Team(ref drNewRow, intGrittingContractID);  // DataRow passed by Reference //
                        this.dataSet_GC_Core.sp04187_GC_Gritting_Callout_Wizard_Add_Callout_Template.Rows.Add(drNewRow);
                        //drNewRow.SetColumnError("ClientPONumber", view.Columns["ClientPONumber"].Caption + ": Missing value!");  // Flag row as error until a PO Number has been entered //
                        drNewRow.RowError = "Error(s) Present - correct before proceeding.";
                    }
                    catch (Exception ex)
                    {
                    }
                    intCount++;
                    if (intCount >= selection1.SelectedCount) break;
                }
            }   
            gridControl2.MainView.EndUpdate();
            gridControl2.MainView.RefreshData();  // Force validation to fire on all rows so error icon shows up in ClientPONumber Column on every row //
            loading.Close();
            loading.Dispose();
        }

        private int CheckRows(GridView view)
        {
            int intInvalidRowCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intInvalidRowCount += CheckRow(view, i);
            }
            return intInvalidRowCount;
        }
        private int CheckRow(GridView view, int RowHandle)
        {
            try
            {
                DataRow dr = view.GetDataRow(RowHandle);
                if (string.IsNullOrEmpty(dr["ClientPONumber"].ToString().Trim()) && Convert.ToInt32(dr["ClientPOID"]) == 0 && Convert.ToInt32(dr["ClientPONumberRequired"]) == 1)
                {
                    dr.SetColumnError("ClientPONumber", view.Columns["ClientPONumber"].Caption + ": Missing value!");  // Flag row as error until a PO Number has been entered //
                    dr.RowError = "Error(s) Present - correct before proceeding.";
                    return 1;
                }
                else
                {
                    dr.SetColumnError("ClientPONumber", "");  // Flag row as error until a PO Number has been entered //
                    dr.RowError = "";
                }
            }
            catch (Exception)
            {
            }
            return 0;
        }

        private void Get_Default_Team(ref DataRow drNewRow, int intGrittingSiteContract)
        {
            string strAccess = "";
            DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetValue = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            GetValue.ChangeConnectionString(strConnectionString);
            try
            {
                strAccess = GetValue.sp04188_GC_Gritting_Get_Preferred_Team(intGrittingSiteContract).ToString();
            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while retrieving Default Teams.\n\nException:" + Ex.Message, "Get Default Team", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (string.IsNullOrEmpty(strAccess)) return;

            char[] delimiters = new char[] { '|' };
            string[] strArray = strAccess.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length != 5) return;

            drNewRow["SubContractorID"] = Convert.ToInt32(strArray[0]);
            drNewRow["SubContractorName"] = strArray[1];
            drNewRow["LabourVatRate"] = (Convert.ToInt32(strArray[2]) == 1 ? decVatRate : (decimal)0.00);
            drNewRow["JobRateProactive"] = Convert.ToDecimal(strArray[3]);
            drNewRow["JobRateReactive"] = Convert.ToDecimal(strArray[4]);
            return;
        }

        private void Delete_Record()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more new callouts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + (intRowHandles.Length == 1 ? " New Callout" : " New Callouts") + " selected for delete!\n\nProceed?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(0);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();
                int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                int intUpdateProgressTempCount = 0;

                gridControl2.BeginUpdate();
                for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                {
                    view.DeleteRow(intRowHandles[intRowHandle]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                gridControl2.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
            }
        }

        private void repositoryItemButtonEditTeam_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Tree Button //
            {
                Open_Select_Team_Screen("1");
            }
            else if (e.Button.Tag.ToString() == "view")  // View Contractor Button //
            {
                GridView view = (GridView)gridControl2.MainView;
                if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
                int intSiteGrittingContractID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SubContractorID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("SubContractorID").ToString()));
                if (intSiteGrittingContractID <= 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Gritting Team - no Gritting Team has been linked.", "View Linked Gritting Team", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                fProgress = new frmProgress(10);
                this.AddOwnedForm(fProgress);
                fProgress.Show();  // ***** Closed in PostOpen event ***** //
                Application.DoEvents();

                frm_Core_Contractor_Edit fChildForm = new frm_Core_Contractor_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = intSiteGrittingContractID + ",";
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 1;
                fChildForm.FormPermissions = this.FormPermissions;
                fChildForm.fProgress = fProgress;
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
        }
        private void Open_Select_Team_Screen(string strHowMany)
        {
            string strRecordIDs = "";
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            int intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0) return;
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += view.GetRowCellValue(intRowHandle, "SiteGrittingContractID").ToString() + ",";
            }

            frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
            fChildForm.strFormMode = strFormMode;
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = intRecordCount;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                    view.SetRowCellValue(intRowHandle, "SubContractorName", fChildForm.strSelectedTeamName);
                    view.SetRowCellValue(intRowHandle, "JobRateProactive", fChildForm.decProactiveRate);
                    view.SetRowCellValue(intRowHandle, "JobRateReactive", fChildForm.decReactiveRate);
                    view.SetRowCellValue(intRowHandle, "SaltCost", fChildForm.decSaltCost);
                }
            }
        }

        private void repositoryItemButtonEditClientPO_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                Open_Select_ClientPO_Screen("1");
            }
            else if (e.Button.Tag.ToString() == "view")  // View Button //
            {
                GridView view = (GridView)gridControl2.MainView;
                if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
                int intClientPO = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("ClientPOID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("ClientPOID").ToString()));
                if (intClientPO <= 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Client Purchase Order - no Client Purchase Order has been linked.", "View Linked Client Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                fProgress = new frmProgress(10);
                this.AddOwnedForm(fProgress);
                fProgress.Show();  // ***** Closed in PostOpen event ***** //
                Application.DoEvents();

                frm_GC_Client_PO_Edit fChildForm = new frm_GC_Client_PO_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = intClientPO + ",";
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 1;
                fChildForm.FormPermissions = this.FormPermissions;
                fChildForm.fProgress = fProgress;
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
        }
        private void Open_Select_ClientPO_Screen(string strHowMany)
        {
            int intSiteID = 0;
            int intPOID = 0;
            int intRecordCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            if (strHowMany == "1")
            {
                if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
                intRecordCount = 1;
                intSiteID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SiteID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("SiteID")));
                intPOID = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("ClientPOID").ToString()) ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("ClientPOID")));
            }
            else
            {
                int[] intRowHandles;
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                intSiteID = 0;
                intPOID = 0;
            }
            frm_GC_Select_Client_PO fChildForm = new frm_GC_Select_Client_PO();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intIncludeNonActive = 1;
            fChildForm.intPassedInSiteID = intSiteID;
            fChildForm.intOriginalClientPOID = intPOID;
            fChildForm.strPassedInSubJoin = (strHowMany == "1" ? "IN" : "BLOCKEDIT");
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                if (strHowMany == "1")
                {
                    view.SetFocusedRowCellValue("ClientPOID", fChildForm.intSelectedID);
                    view.SetFocusedRowCellValue("ClientPOIDDescription", fChildForm.strSelectedValue);
                    CheckRow(view, view.FocusedRowHandle);
                }
                else
                {
                    int[] intRowHandles;
                    intRowHandles = view.GetSelectedRows();
                    intRecordCount = intRowHandles.Length;
                    if (intRowHandles.Length <= 0) return;
                    foreach (int intRowHandle in intRowHandles)
                    {
                        view.SetRowCellValue(intRowHandle, "ClientPOID", fChildForm.intSelectedID);
                        view.SetRowCellValue(intRowHandle, "ClientPOIDDescription", fChildForm.strSelectedValue);
                        CheckRow(view, intRowHandle);
                    }
                }
            }
        }

        private void bbiBlockEditTeam_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Open_Select_Team_Screen("Many");
        }

        private void bbiBlockEditClientPO_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Open_Select_ClientPO_Screen("Many");
        }

        private void bbiBlockEditOtherValues_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0) return;

            frm_GC_Callout_Wizard_Add_BlockEdit fChildForm = new frm_GC_Callout_Wizard_Add_BlockEdit();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (fChildForm.intReactive != null) view.SetRowCellValue(intRowHandle, "Reactive", fChildForm.intReactive);
                    if (fChildForm.strClientPONumber != null) view.SetRowCellValue(intRowHandle, "ClientPONumber", fChildForm.strClientPONumber);
                    if (fChildForm.intNonStandardCost != null) view.SetRowCellValue(intRowHandle, "NonStandardCost", fChildForm.intNonStandardCost);
                    if (fChildForm.intNonStandardSell != null) view.SetRowCellValue(intRowHandle, "NonStandardSell", fChildForm.intNonStandardSell);
                    if (fChildForm.intSnowOnSite != null)
                    {
                        view.SetRowCellValue(intRowHandle, "SnowOnSite", fChildForm.intSnowOnSite);
                        if (fChildForm.intSnowOnSite == 1)  // Double Original Salt Value //
                        {
                             view.SetRowCellValue(intRowHandle, "SaltUsed", Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "OriginalSaltUsed")) * (decimal)2.00);
                        }
                        else  // Take User Specified Salt Required Value //
                        {
                            if (fChildForm.decSaltUsed != null)  // User has specified a value so use it //
                            {
                                view.SetRowCellValue(intRowHandle, "SaltUsed", fChildForm.decSaltUsed); 
                            }
                            else  // No value specified so reset to original values //
                            {
                                 view.SetRowCellValue(intRowHandle, "SaltUsed", Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "OriginalSaltUsed")));
                            }
                        }
                    }
                    else  // Take User Specified Salt Required Value //
                    {
                        if (fChildForm.decSaltUsed != null) view.SetRowCellValue(intRowHandle, "SaltUsed", fChildForm.decSaltUsed);
                    }
                    if (fChildForm.intServiceFailure != null) view.SetRowCellValue(intRowHandle, "ServiceFailure", fChildForm.intServiceFailure);
                    CheckRow(view, intRowHandle);
                }
                view.EndUpdate();
            }
        }

        private void bbiDoubleSalt_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0) return;

            view.BeginUpdate();
            foreach (int intRowHandle in intRowHandles)
            {
                view.SetRowCellValue(intRowHandle, "SaltUsed", Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "OriginalSaltUsed")) * (decimal)2.00);
                view.SetRowCellValue(intRowHandle, "SnowOnSite", 1);
            }
            view.EndUpdate();
        }

        private void bbiResetSalt_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0) return;

            view.BeginUpdate();
            foreach (int intRowHandle in intRowHandles)
            {
                view.SetRowCellValue(intRowHandle, "SaltUsed", Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "OriginalSaltUsed")));
                view.SetRowCellValue(intRowHandle, "SnowOnSite", 0);
            }
            view.EndUpdate();
        }

        private void repositoryItemCheckEditSnowOnSite_CheckedChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int intRowHandle = view.FocusedRowHandle;
            if (intRowHandle == GridControl.InvalidRowHandle) return;
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)  // Double Salt Used //
            {
                view.SetRowCellValue(intRowHandle, "SaltUsed", Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "OriginalSaltUsed")) * (decimal)2.00);
            }
            else  // No snow so reset salt to original value //
            {
                view.SetRowCellValue(intRowHandle, "SaltUsed", Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "OriginalSaltUsed")));
            }
        }

        private void repositoryItemButtonEditPostcode_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "lookup")  // Lookup Latitude \ Longitude Button //
            {
                GridView view = (GridView)gridControl2.MainView;
                if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
                string strSitePostcode = view.GetFocusedRowCellValue("SitePostcode").ToString();

                frm_GC_Get_Lat_Long_From_Postcode fChildForm = new frm_GC_Get_Lat_Long_From_Postcode();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInPostcode = strSitePostcode;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    view.SetFocusedRowCellValue("SiteLat", fChildForm.dblSelectedLatitude);
                    view.SetFocusedRowCellValue("SiteLong", fChildForm.dblSelectedLongitude);
                }
            }
        }

        private void repositoryItemButtonEditMapping_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            double dStartLat = (double)0.00;
            double dStartLong = (double)0.00;
            List<Mapping_Objects> mapObjectsList = new List<Mapping_Objects>();
            dStartLat = (!string.IsNullOrEmpty(view.GetFocusedRowCellValue("SiteLat").ToString()) ? Convert.ToDouble(view.GetFocusedRowCellValue("SiteLat")) : (double)0.00);
            dStartLong = (!string.IsNullOrEmpty(view.GetFocusedRowCellValue("SiteLong").ToString()) ? Convert.ToDouble(view.GetFocusedRowCellValue("SiteLong")) : (double)0.00);
            if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
            {
                Mapping_Objects mapObject = new Mapping_Objects();
                mapObject.doubleLat = dStartLat;
                mapObject.doubleLong = dStartLong;
                mapObject.stringToolTip = "Client: " + (string.IsNullOrEmpty(view.GetFocusedRowCellValue("ClientName").ToString()) ? "" :view.GetFocusedRowCellValue("ClientName").ToString()) + "\nSite: " + (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SiteName").ToString()) ? "" : view.GetFocusedRowCellValue("SiteName").ToString());
                mapObject.stringID = "Site|" + (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SiteID").ToString()) ? "0" : view.GetFocusedRowCellValue("SiteID").ToString());
                mapObjectsList.Add(mapObject);
            }

            if (mapObjectsList.Count == 0)  // No objects added due to missing coordinates //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter the Latitude and Longitude for the site before proceeding.\n\nTip: If you are unsure of the Latitude and Longitude, look them up via the Lookup button.", "View Site on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                Mapping_Functions mapFunctions = new Mapping_Functions();
                mapFunctions.View_Object_In_Internet_Mapping(this.GlobalSettings, this.strConnectionString, this, mapObjectsList);
            }
        }






    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data.SqlClient;

namespace WoodPlan5
{
    public partial class frm_GC_Salt_Order_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Rate Calculation //
        private bool ibool_FormStillLoading = true;

        public string strSiteName = "";
        public string strClientName = "";
        public int intSiteID = 0;
        public int intClientID = 0;
        public int intSnowClearanceSiteContractID = 0;
        public int intSubContractorID = 0;
        public string strSubContractorName = "";
        decimal decVatRate = (decimal)0.00;
       
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
 
        #endregion

        public frm_GC_Salt_Order_Edit()
        {
            InitializeComponent();
        }

        private void frm_GC_Salt_Order_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 400055;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Get Default VAT Rate //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                decVatRate = Convert.ToDecimal(GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate"));
            }
            catch (Exception)
            {
                decVatRate = (decimal)0.00;
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04226_GC_Grit_Order_Status_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04226_GC_Grit_Order_Status_List_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04226_GC_Grit_Order_Status_List_With_Blank, "");

            sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04209_GC_Salt_Unit_Conversion_List_With_Blank);

            sp04218_GC_Salt_Colours_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04218_GC_Salt_Colours_List_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04218_GC_Salt_Colours_List_With_Blank);

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Populate Main Dataset //
            
            sp04224_GC_Salt_Order_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["OrderDate"] = DateTime.Now;
                        drNewRow["GritSupplierID"] = 0;
                        drNewRow["PlacedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["strStaffName"] = (string.IsNullOrEmpty(this.GlobalSettings.UserSurname) ? "" : this.GlobalSettings.UserSurname + ": ") + (string.IsNullOrEmpty(this.GlobalSettings.UserForename) ? "" : this.GlobalSettings.UserForename);
                        drNewRow["TotalAmountConverted"] = (decimal)0.00;
                        drNewRow["OtherCostVatRate"] = decVatRate;
                        drNewRow["OtherCostExVat"] = (decimal)0.00;
                        drNewRow["OtherCostIncVat"] = (decimal)0.00;
                        drNewRow["TotalCostExVat"] = (decimal)0.00;
                        drNewRow["UnitCostExVat"] = (decimal)0.00;
                        drNewRow["TotalCostIncVat"] = (decimal)0.00;
                        drNewRow["UnitCostIncVat"] = (decimal)0.00;
                        drNewRow["OrderStatusID"] = 10;  // Order Started //
                        drNewRow["GritSupplierName"] = "";
                        this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["OrderDate"] = DateTime.Now;
                        drNewRow["GritSupplierID"] = 0;
                        drNewRow["PlacedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["RecordedByName"] = (string.IsNullOrEmpty(this.GlobalSettings.UserSurname) ? "" : this.GlobalSettings.UserSurname + ": ") + (string.IsNullOrEmpty(this.GlobalSettings.UserForename) ? "" : this.GlobalSettings.UserForename);
                        drNewRow["TotalAmountConverted"] = (decimal)0.00;
                        drNewRow["OtherCostVatRate"] = decVatRate;
                        drNewRow["OtherCostExVat"] = (decimal)0.00;
                        drNewRow["OtherCostIncVat"] = (decimal)0.00;
                        drNewRow["TotalCostExVat"] = (decimal)0.00;
                        drNewRow["UnitCostExVat"] = (decimal)0.00;
                        drNewRow["TotalCostIncVat"] = (decimal)0.00;
                        drNewRow["UnitCostIncVat"] = (decimal)0.00;
                        drNewRow["OrderStatusID"] = 10;  // Order Started //
                        drNewRow["GritSupplierName"] = "";
                        this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["OrderStatusID"] = 0;
                        this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.Rows.Add(drNewRow);
                        this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp04224_GC_Salt_Order_EditTableAdapter.Fill(this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            if (strFormMode == "add" || strFormMode == "edit")
            {
                sp04227_GC_Salt_Order_Line_EditTableAdapter.Connection.ConnectionString = strConnectionString;
                RefreshGridViewState1 = new RefreshGridState(gridView1, "GritOrderLineID");
                gridControl1.BeginUpdate();
                sp04227_GC_Salt_Order_Line_EditTableAdapter.Fill(dataSet_GC_DataEntry.sp04227_GC_Salt_Order_Line_Edit, strRecordIDs, strFormMode);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                gridControl1.EndUpdate();
            }


            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Salt Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        GCPONumberButtonEdit.Focus();

                        GCPONumberButtonEdit.Properties.ReadOnly = false;
                        GCPONumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        GCPONumberButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        GCPONumberButtonEdit.Focus();

                        GCPONumberButtonEdit.Properties.ReadOnly = false;
                        GCPONumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        GCPONumberButtonEdit.Properties.Buttons[1].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        GCPONumberButtonEdit.Focus();

                        GCPONumberButtonEdit.Properties.ReadOnly = false;
                        GCPONumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        GCPONumberButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        OrderDateDateEdit.Focus();

                        GCPONumberButtonEdit.Properties.ReadOnly = true;
                        GCPONumberButtonEdit.Properties.Buttons[0].Enabled = false;
                        GCPONumberButtonEdit.Properties.Buttons[1].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            Set_Control_Readonly_Status("");
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_GC_DataEntry.GetChanges();

            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            this.sp04227GCSaltOrderLineEditBindingSource.EndEdit();  // Force any pending save from Grid to underlying table adapter //
            DataSet dsChanges2 = dataSet_GC_DataEntry.GetChanges();
            if (dsChanges != null || dsChanges2 != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }
        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intOrderStatus = 0;
            DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                intOrderStatus = Convert.ToInt32(currentRow["OrderStatusID"]);
            }

            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            if (intOrderStatus < 20)  // Not yet completed //
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiDelete.Enabled = true;
            }
            else
            {
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
                bbiDelete.Enabled = false;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //

            // Get Status of Order to see if we can still add rows... //

            if (intOrderStatus < 20)  // Not yet completed //
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
        }

        private void frm_GC_Salt_Order_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_GC_Salt_Order_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            this.sp04227GCSaltOrderLineEditBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp04224GCSaltOrderEditBindingSource.EndEdit();
            try
            {
                this.sp04224_GC_Salt_Order_EditTableAdapter.Update(dataSet_GC_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["GritOrderHeaderID"]) + ";";
                    // Switch mode to Edit so than any subsequent changes update this record //
                    this.strFormMode = "edit";
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //

                    // Pass new ID to any records in the linked Order Lines grid //
                    GridView view = (GridView)gridControl1.MainView;
                    int intOrderHeaderID = Convert.ToInt32(currentRow["GritOrderHeaderID"]);
                    view.BeginUpdate();
                    foreach (DataRow dr in dataSet_GC_DataEntry.sp04227_GC_Salt_Order_Line_Edit.Rows)
                    {
                        dr["GritOrderHeaderID"] = intOrderHeaderID;
                    }
                    this.sp04227GCSaltOrderLineEditBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                    view.EndUpdate();
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
                    currentRow["strMode"] = "blockedit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Attempt to save any changes to Linked Order Lines... //
            try
            {
                sp04227_GC_Salt_Order_Line_EditTableAdapter.Update(dataSet_GC_DataEntry);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked order line record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }


            // Notify any open instances of Callout Manager they will need to refresh their data on activating //
            switch (strCaller)
            {
                case "frm_GC_Salt_Order_Manager":
                    {
                        frm_GC_Salt_Order_Manager fParentForm;
                        foreach (Form frmChild in this.ParentForm.MdiChildren)
                        {
                            if (frmChild.Name == "frm_GC_Salt_Order_Manager")
                            {
                                fParentForm = (frm_GC_Salt_Order_Manager)frmChild;
                                fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "");
                            }
                        }
                    }
                    break;
             }


            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            int intRateRecordNew = 0;
            int intRateRecordModified = 0;
            int intRateRecordDeleted = 0;
            this.sp04227GCSaltOrderLineEditBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_GC_DataEntry.sp04227_GC_Salt_Order_Line_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_GC_DataEntry.sp04227_GC_Salt_Order_Line_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRateRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRateRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRateRecordDeleted++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intRateRecordNew > 0 || intRateRecordModified > 0 || intRateRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New order record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated order record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted order record(s)\n";
                if (intRateRecordNew > 0) strMessage += Convert.ToString(intRateRecordNew) + " New order line record(s)\n";
                if (intRateRecordModified > 0) strMessage += Convert.ToString(intRateRecordModified) + " Updated order lines record(s)\n";
                if (intRateRecordDeleted > 0) strMessage += Convert.ToString(intRateRecordDeleted) + " Deleted order lines record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    FilterGrid();
                    GridView view = (GridView)gridControl1.MainView;
                    view.ExpandAllGroups();
                }
                Set_Control_Readonly_Status("");
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void GritSupplierIDButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            string strValue = be.EditValue.ToString();
            if (this.strFormMode != "blockadd" && this.strFormMode != "blockedit" && String.IsNullOrEmpty(strValue))
            {
                dxErrorProvider1.SetError(GritSupplierNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(GritSupplierNameButtonEdit, "");
            }

        }

        private void GritSupplierNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frm_GC_Select_Salt_Supplier fChildForm1 = new frm_GC_Select_Salt_Supplier();
            fChildForm1.GlobalSettings = this.GlobalSettings;
            if (fChildForm1.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
            {
                DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
                if (currentRow != null)
                {
                    currentRow["GritSupplierID"] = fChildForm1.intSelectedID;
                    currentRow["GritSupplierName"] = fChildForm1.strSelectedValue;
                    this.sp04224GCSaltOrderEditBindingSource.EndEdit();
                }
            }
        }

        private void GCPONumberButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string strTableName = "GC_Grit_Order_Header";
            string strFieldName = "GCPONumber";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    GCPONumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    //i_strLastUsedSequencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = GCPONumberButtonEdit.EditValue.ToString() ?? "";

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";


                // Check if value is already present within dataset //
                string strOrderHeaderID = GritOrderHeaderIDTextEdit.EditValue.ToString();
                if (string.IsNullOrEmpty(strOrderHeaderID)) strOrderHeaderID = "";
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_GC_DataEntry.sp04224_GC_Salt_Order_Edit.Rows)
                    {
                        if (dr["GCPONumber"].ToString() == (strSequence + strTempNumber))
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                GCPONumberButtonEdit.EditValue = strSequence + strTempNumber;
            }
        }

        private void OtherCostExVatSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void OtherCostExVatSpinEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_ignoreValidation) Calculate_Other_Cost_Totals("CostExVat");
            ibool_ignoreValidation = true;
        }

        private void OtherCostVatRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void OtherCostVatRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_ignoreValidation) Calculate_Other_Cost_Totals("VatRate");
            ibool_ignoreValidation = true;
        }

        private void OtherCostIncVatSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void OtherCostIncVatSpinEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_ignoreValidation) Calculate_Other_Cost_Totals("CostIncVat");
            ibool_ignoreValidation = true;
        }

        private void OrderStatusIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ((this.strFormMode != "blockadd" && this.strFormMode != "blockedit") && (glue.EditValue == DBNull.Value || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(OrderStatusIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(OrderStatusIDGridLookUpEdit, "");
            }
        }
        private void OrderStatusIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                if ((this.strFormMode == "add" || this.strFormMode == "edit"))
                {
                    // Check it the order can be moved past a started status //
                    bool boolAllowComplete = true;
                    if (currentRow != null)
                    {
                        int intOrderID = Convert.ToInt32(currentRow["GritOrderHeaderID"]);
                        DataRow[] drOrderLines = dataSet_GC_DataEntry.sp04227_GC_Salt_Order_Line_Edit.Select("GritOrderHeaderID = " + intOrderID);
                        if (drOrderLines.Length <= 0)  // No Order Lines so not valid to set as completed //
                        {
                            boolAllowComplete = false;
                        }
                        else
                        {
                            foreach (DataRow dr in drOrderLines)
                            {
                                if (Convert.ToInt32(dr["DeliverToLocationID"]) <= 0)
                                {
                                    boolAllowComplete = false;
                                    break;
                                }
                                if (Convert.ToInt32(dr["SaltColourOrdered"]) <= 0)
                                {
                                    boolAllowComplete = false;
                                    break;
                                }
                                if (Convert.ToDecimal(dr["AmountOrderedConverted"]) <= (decimal)0.00)
                                {
                                    boolAllowComplete = false;
                                    break;
                                }
                            }
                        }
                    }

                    if (!boolAllowComplete)
                    {
                        GridLookUpEdit glue = (GridLookUpEdit)sender;
                        GridView view = glue.Properties.View;
                        view.BeginUpdate();
                        view.ActiveFilter.Clear();
                        view.ActiveFilter.NonColumnFilter = "[OrderStatusID] <= 10";
                        view.MakeRowVisible(-1, true);
                        view.EndUpdate();
                    }
                    else
                    {
                        if (this.strFormMode == "add" || this.strFormMode == "edit")
                        {
                            GridLookUpEdit glue = (GridLookUpEdit)sender;
                            GridView view = glue.Properties.View;
                            view.BeginUpdate();
                            view.ActiveFilter.Clear();
                            view.EndUpdate();
                        }
                    }
                }
            }
        }
        private void OrderStatusIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                if (this.strFormMode == "add" || this.strFormMode == "edit")
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    GridView view = glue.Properties.View;
                    view.BeginUpdate();
                    view.ActiveFilter.Clear();
                    view.EndUpdate();
                }
            }
        }

        #endregion


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            string message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Order Lines NOT Shown When Block Adding and Block Editing" : "No Linked Order Lines Available - Click the Add button to Create Linked Order Lines");
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.Color.Gray;
        }

        private void FilterGrid()
        {
            string strGritOrderHeaderID = "";
            DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                strGritOrderHeaderID = (currentRow["GritOrderHeaderID"] == null ? "0" : currentRow["GritOrderHeaderID"].ToString());
            }

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[GritOrderHeaderID] = " + Convert.ToString(strGritOrderHeaderID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void repositoryItemGridLookUpEditAmountOrderedDescriptorID_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            int intValue = 0;
            GridLookUpEdit glue = (GridLookUpEdit)sender;

            if (glue.EditValue == DBNull.Value || glue.EditValue.ToString() == "0")
            {
                view.SetFocusedRowCellValue("AmountOrderedConversionFactor", (decimal)0.00);
                view.SetFocusedRowCellValue("AmountOrderedConverted", (decimal)0.00);
                return;
            }
            else
            {
                intValue = Convert.ToInt32(glue.EditValue);
            }
            GridView childView = glue.Properties.View;
            int intFoundRow = 0;
            intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : childView.LocateByValue(0, childView.Columns["SaltUnitConversionID"], Convert.ToInt32(glue.EditValue)));
            if (intFoundRow != GridControl.InvalidRowHandle)
            {
                decimal decAmountOrdered = Convert.ToDecimal(view.GetFocusedRowCellValue("AmountOrdered") ?? (decimal)0.00);
                view.SetFocusedRowCellValue("AmountOrderedConverted", Convert.ToDecimal(childView.GetRowCellValue(intFoundRow, "ConversionValue")) * decAmountOrdered);
                view.SetFocusedRowCellValue("AmountOrderedConversionFactor", Convert.ToDecimal(childView.GetRowCellValue(intFoundRow, "ConversionValue")));
                Calculate_Line_Costs("colVatRate");  // Just use Vat Calculation process //
            }

            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemSpinEdit2dp_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            if (view.FocusedColumn.Name == "colAmountOrdered")
            {
                decimal decValue = 0;
                SpinEdit se = (SpinEdit)sender;
                if (se.EditValue == DBNull.Value || se.EditValue.ToString() == "0")
                {
                    view.SetFocusedRowCellValue("AmountOrderedConverted", (decimal)0.00);
                    return;
                }
                else
                {
                    decValue = Convert.ToDecimal(se.EditValue);
                }
                decimal decConversionFactor = Convert.ToDecimal(view.GetFocusedRowCellValue("AmountOrderedConversionFactor") ?? (decimal)0.00);
                view.SetFocusedRowCellValue("AmountOrderedConverted", decValue * decConversionFactor);
                Calculate_Line_Costs("colVatRate");  // Just use Vat Calculation process //
            }
            else if (view.FocusedColumn.Name == "colAmountDelivered")
            {
                decimal decValue = 0;
                SpinEdit se = (SpinEdit)sender;
                if (se.EditValue == DBNull.Value || se.EditValue.ToString() == "0")
                {
                    view.SetFocusedRowCellValue("AmountDeliveredConverted", (decimal)0.00);
                    return;
                }
                else
                {
                    decValue = Convert.ToDecimal(se.EditValue);
                }
                decimal decConversionFactor = Convert.ToDecimal(view.GetFocusedRowCellValue("AmountDeliveredConversionFactor") ?? (decimal)0.00);
                view.SetFocusedRowCellValue("AmountDeliveredConverted", decValue * decConversionFactor);
                //Calculate_Line_Costs("colVatRate");  // Just use Vat Calculation process //
            }

            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }
        private void repositoryItemSpinEdit2dp_ValueChanged(object sender, EventArgs e)
        {
        }

        private void repositoryItemButtonEditDeliverToLocationName_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Site Button //
            {
                frm_GC_Select_Salt_Depot fChildForm = new frm_GC_Select_Salt_Depot();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intIncludeTeams = 1;
                // No Postcode search //
                fChildForm.strPassedInRecordIDs = "";
                fChildForm.strPassedInRecordType = "";
                if (fChildForm.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                {
                    view.SetFocusedRowCellValue("DeliverToLocationID", fChildForm.intSelectedID);
                    view.SetFocusedRowCellValue("DeliverToLocationTypeID", fChildForm.intSelectedTypeID);
                    view.SetFocusedRowCellValue("DeliverToLocationName", fChildForm.strSelectedValue);
                    view.SetFocusedRowCellValue("DeliverToLocationType", fChildForm.strSelectedTypeDescription);
                }
            }
        }

        private void repositoryItemButtonEditRedirectFromLocationName_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Site Button //
            {
                frm_GC_Select_Salt_Depot fChildForm = new frm_GC_Select_Salt_Depot();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intIncludeTeams = 1;
                // No Postcode search //
                fChildForm.strPassedInRecordIDs = "";
                fChildForm.strPassedInRecordType = "";
                if (fChildForm.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                {
                    view.SetFocusedRowCellValue("RedirectFromLocationID", fChildForm.intSelectedID);
                    view.SetFocusedRowCellValue("RedirectFromLocationTypeID", fChildForm.intSelectedTypeID);
                    view.SetFocusedRowCellValue("RedirectFromLocationName", fChildForm.strSelectedValue);
                    view.SetFocusedRowCellValue("RedirectFromLocationType", fChildForm.strSelectedTypeDescription);
                }
            }
        }


        private void repositoryItemSpinEditPercentage_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            if (view.FocusedColumn.Name == "colVatRate")
            {
                Calculate_Line_Costs(view.FocusedColumn.Name);
            }

            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemSpinEditCurrency_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
           
            Calculate_Line_Costs(view.FocusedColumn.Name);
 
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemGridLookUpEditAmountDeliveredDescriptorID_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            int intValue = 0;
            GridLookUpEdit glue = (GridLookUpEdit)sender;

            if (glue.EditValue == DBNull.Value || glue.EditValue.ToString() == "0")
            {
                view.SetFocusedRowCellValue("AmountDeliveredConversionFactor", (decimal)0.00);
                view.SetFocusedRowCellValue("AmountDeliveredConverted", (decimal)0.00);
                return;
            }
            else
            {
                intValue = Convert.ToInt32(glue.EditValue);
            }
            GridView childView = glue.Properties.View;
            int intFoundRow = 0;
            intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : childView.LocateByValue(0, childView.Columns["SaltUnitConversionID"], Convert.ToInt32(glue.EditValue)));
            if (intFoundRow != GridControl.InvalidRowHandle)
            {
                decimal decAmountDelivered = Convert.ToDecimal(view.GetFocusedRowCellValue("AmountDelivered") ?? (decimal)0.00);
                view.SetFocusedRowCellValue("AmountDeliveredConverted", Convert.ToDecimal(childView.GetRowCellValue(intFoundRow, "ConversionValue")) * decAmountDelivered);
                view.SetFocusedRowCellValue("AmountDeliveredConversionFactor", Convert.ToDecimal(childView.GetRowCellValue(intFoundRow, "ConversionValue")));
                //Calculate_Line_Costs("colVatRate");  // Just use Vat Calculation process //
            }

            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        private void repositoryItemMemoExEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //

            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            SetMenuStatus();
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        #endregion


        private void Set_Control_Readonly_Status(string strCheckWhat)
        {
            /*if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "JobStatusID")
            {
                int intJobStatusID = 0;
                if (!string.IsNullOrEmpty(OrderStatusIDGridLookUpEdit.EditValue.ToString())) intJobStatusID = Convert.ToInt32(OrderStatusIDGridLookUpEdit.EditValue);
                if ((intJobStatusID < 80 || intJobStatusID == 90) && (strFormMode == "add" || strFormMode == "edit"))
                {
                    GritSupplierIDButtonEdit.Properties.Buttons[0].Enabled = true;  // Enable Site Selection //
                    SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;  // Enable Team Selection //
                }
                else
                {
                    GritSupplierIDButtonEdit.Properties.Buttons[0].Enabled = false;  // Disable Site Selection //
                    SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = false;   // Disable Team Selection //
                }
            }

            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardCost")
            {
                OtherCostExVatSpinEdit.Properties.ReadOnly = !NonStandardCostCheckEdit.Checked;
            }
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardSell")
            {
                LabourSellSpinEdit.Properties.ReadOnly = !NonStandardSellCheckEdit.Checked;
            }

            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardCost")
            {
                OtherCostExVatSpinEdit.Properties.ReadOnly = !NonStandardCostCheckEdit.Checked;
            }

            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardSell")
            {
                LabourSellSpinEdit.Properties.ReadOnly = !NonStandardSellCheckEdit.Checked;
            }

            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NoAccessAbortedRate")
            {
                if (string.IsNullOrEmpty(NoAccessCheckEdit.EditValue.ToString()) || string.IsNullOrEmpty(VisitAbortedCheckEdit.EditValue.ToString())) return;
                if (Convert.ToInt32(NoAccessCheckEdit.EditValue) == 1 || Convert.ToInt32(VisitAbortedCheckEdit.EditValue) == 1)
                {
                    NoAccessAbortedRateSpinEdit.Properties.ReadOnly = false;
                }
                else
                {
                    NoAccessAbortedRateSpinEdit.Properties.ReadOnly = true;
                    NoAccessAbortedRateSpinEdit.EditValue = (decimal)0.00;
                }
            }*/
        }
        
        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            try
            {
                DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intGritOrderHeaderID = 0;
                    if (!string.IsNullOrEmpty(currentRow["GritOrderHeaderID"].ToString())) intGritOrderHeaderID = Convert.ToInt32(currentRow["GritOrderHeaderID"]);
  
                    DataRow drNewRow;
                    drNewRow = this.dataSet_GC_DataEntry.sp04227_GC_Salt_Order_Line_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["GritOrderHeaderID"] = intGritOrderHeaderID;
                    drNewRow["DeliverToLocationID"] = 0;
                    drNewRow["DeliverToLocationTypeID"] = 0;
                    drNewRow["RedirectFromLocationID"] = 0;
                    drNewRow["RedirectFromLocationTypeID"] = 0;
                    drNewRow["SaltColourOrdered"] = 0;
                    drNewRow["AmountOrdered"] = (decimal)0.00;
                    drNewRow["AmountOrderedDescriptorID"] = 0;
                    drNewRow["AmountOrderedConverted"] = (decimal)0.00;
                    drNewRow["AmountDelivered"] = (decimal)0.00;
                    drNewRow["AmountDeliveredDescriptorID"] = 0;
                    drNewRow["AmountDeliveredConverted"] = (decimal)0.00;
                    drNewRow["SaltColourDelivered"] = 0;
                    drNewRow["VatRate"] = decVatRate;
                    drNewRow["UnitCostExVat"] = (decimal)0.00;
                    drNewRow["TotalCostExVat"] = (decimal)0.00;
                    drNewRow["UnitCostIncVat"] = (decimal)0.00;
                    drNewRow["TotalCostIncVat"] = (decimal)0.00;
                    drNewRow["AmountOrderedConversionFactor"] = (decimal)0.00;
                    drNewRow["AmountDeliveredConversionFactor"] = (decimal)0.00;
                    drNewRow["DeliverToLocationName"] = "";
                    drNewRow["DeliverToLocationType"] = "";
                    drNewRow["RedirectFromLocationName"] = "";
                    drNewRow["RedirectFromLocationType"] = "";
                    this.dataSet_GC_DataEntry.sp04227_GC_Salt_Order_Line_Edit.Rows.Add(drNewRow);
                }
            }
            catch (Exception)
            {
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Order Lines to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Order Line" : Convert.ToString(intRowHandles.Length) + " Order Lines") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Order Line" : "these Order Lines") + " will no longer be available for selection!";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                view.BeginUpdate();
                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    view.DeleteRow(intRowHandles[i]);
                }
                view.EndUpdate();


                Calculate_Line_Costs("");
                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " order line(s) deleted.\n\nImportant Note: Deleted order lines are only physically removed from the order on saving changes.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        private void Calculate_Other_Cost_Totals(string strCaller)
        {
           DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
           if (currentRow != null)
           {
               decimal OtherCostExVat = (decimal)0.00;
               decimal OtherCostVatRate = (decimal)0.00;
               decimal OtherCostIncVat = (decimal)0.00;
               if (!string.IsNullOrEmpty(currentRow["OtherCostExVat"].ToString())) OtherCostExVat = Convert.ToDecimal(currentRow["OtherCostExVat"]);
               if (!string.IsNullOrEmpty(currentRow["OtherCostVatRate"].ToString())) OtherCostVatRate = Convert.ToDecimal(currentRow["OtherCostVatRate"]);
               if (!string.IsNullOrEmpty(currentRow["OtherCostIncVat"].ToString())) OtherCostIncVat = Convert.ToDecimal(currentRow["OtherCostIncVat"]);

               if (strCaller == "VatRate")
               {
                   if (OtherCostExVat > (decimal)0.00)  // Calc Cost Inc Vat //
                   {
                       OtherCostIncVat = OtherCostExVat * ((decimal)1.00 + (OtherCostVatRate / (decimal)100.00));
                       currentRow["OtherCostIncVat"] = OtherCostIncVat;
                       this.sp04224GCSaltOrderEditBindingSource.EndEdit();
                   }
                   else if (OtherCostIncVat > (decimal)0.00)  // Calc Cost Ex Vat //
                   {
                       OtherCostExVat = OtherCostIncVat * ((decimal)1.00 - (OtherCostVatRate / (decimal)100.00));
                       currentRow["OtherCostExVat"] = OtherCostExVat;
                       this.sp04224GCSaltOrderEditBindingSource.EndEdit();
                   }
               }
               else if (strCaller == "CostExVat")
               {
                   if (OtherCostIncVat > (decimal)0.00 && OtherCostExVat > (decimal)0.00)  // Calc VAT Rate //
                   {
                       OtherCostVatRate = ((OtherCostIncVat / OtherCostExVat) - (decimal)1.00) * (decimal)100.00;
                       currentRow["OtherCostVatRate"] = OtherCostVatRate;
                       this.sp04224GCSaltOrderEditBindingSource.EndEdit();
                   }
                   else  // Calc Cost Inc VAT //
                   {
                       OtherCostIncVat = OtherCostExVat * ((decimal)1.00 + (OtherCostVatRate / (decimal)100.00));
                        currentRow["OtherCostIncVat"] = OtherCostIncVat;
                       this.sp04224GCSaltOrderEditBindingSource.EndEdit();
                   }
               }
               else if (strCaller == "CostIncVat" && OtherCostIncVat > (decimal)0.00)
               {
                   if (OtherCostExVat > (decimal)0.00)  // Calc VAT Rate //
                   {
                       OtherCostVatRate = ((OtherCostIncVat / OtherCostExVat) - (decimal)1.00) * (decimal)100.00;
                       currentRow["OtherCostVatRate"] = OtherCostVatRate;
                       this.sp04224GCSaltOrderEditBindingSource.EndEdit();
                   }
                   else  // Calc Cost Ex VAT //
                   {
                       OtherCostExVat = OtherCostIncVat * ((decimal)1.00 - (OtherCostVatRate / (decimal)100.00));
                       currentRow["OtherCostExVat"] = OtherCostExVat;
                       this.sp04224GCSaltOrderEditBindingSource.EndEdit();
                   }
               }
            }
        }

        private void Calculate_Line_Costs(string strCaller)
        {
            GridView view = (GridView)gridControl1.MainView;
            decimal decAmountOrdered = Convert.ToDecimal(view.GetFocusedRowCellValue("AmountOrdered") ?? (decimal)0.00);
            decimal decAmountOrderedConverted = Convert.ToDecimal(view.GetFocusedRowCellValue("AmountOrderedConverted") ?? (decimal)0.00);
            decimal decVatRate = Convert.ToDecimal(view.GetFocusedRowCellValue("VatRate") ?? (decimal)0.00);
            decimal decUnitCostExVat = Convert.ToDecimal(view.GetFocusedRowCellValue("UnitCostExVat") ?? (decimal)0.00);
            decimal decUnitCostIncVat = Convert.ToDecimal(view.GetFocusedRowCellValue("UnitCostIncVat") ?? (decimal)0.00);
            decimal decTotalCostExVat = Convert.ToDecimal(view.GetFocusedRowCellValue("TotalCostExVat") ?? (decimal)0.00);
            decimal decTotalCostIncVat = Convert.ToDecimal(view.GetFocusedRowCellValue("TotalCostIncVat") ?? (decimal)0.00);
            if (strCaller == "colVatRate")
            {
                if (decTotalCostExVat > (decimal)0.00)
                {
                    decTotalCostIncVat = decTotalCostExVat * ((decimal)1.00 + (decVatRate / (decimal)100.00));  // Calc Total cost Inc VAT //
                    if (decAmountOrderedConverted > (decimal)0.00)
                    {
                        decUnitCostExVat = decTotalCostExVat / decAmountOrderedConverted;  // Calc Unit Cost Ex VAT //
                        decUnitCostIncVat = decTotalCostIncVat / decAmountOrderedConverted;  // Calc Unit Cost Inc VAT //
                    }
                }
                else if (decTotalCostIncVat > (decimal)0.00)
                {
                    decTotalCostExVat = decTotalCostIncVat * ((decimal)1.00 - (decVatRate / (decimal)100.00));  // Calc Total cost Inc VAT //
                    if (decAmountOrderedConverted > (decimal)0.00)
                    {
                        decUnitCostExVat = decTotalCostExVat / decAmountOrderedConverted;  // Calc Unit Cost Ex VAT //
                        decUnitCostIncVat = decTotalCostIncVat / decAmountOrderedConverted;  // Calc Unit Cost Inc VAT //
                    }
                }
                else if (decUnitCostExVat > (decimal)0.00)
                {
                    decUnitCostIncVat = decUnitCostExVat * ((decimal)1.00 + (decVatRate / (decimal)100.00));  // Calc Total cost Inc VAT //
                    if (decAmountOrderedConverted > (decimal)0.00)
                    {
                        decTotalCostExVat = decUnitCostExVat * decAmountOrderedConverted;  // Calc Unit Cost Ex VAT //
                        decTotalCostIncVat = decUnitCostIncVat * decAmountOrderedConverted;  // Calc Unit Cost Inc VAT //
                    }
                }
                else if (decUnitCostIncVat > (decimal)0.00)
                {
                    decUnitCostExVat = decUnitCostIncVat * ((decimal)1.00 - (decVatRate / (decimal)100.00));  // Calc Total cost Inc VAT //
                    if (decAmountOrderedConverted > (decimal)0.00)
                    {
                        decTotalCostExVat = decUnitCostExVat * decAmountOrderedConverted;  // Calc Unit Cost Ex VAT //
                        decTotalCostIncVat = decUnitCostIncVat * decAmountOrderedConverted;  // Calc Unit Cost Inc VAT //
                    }
                }
            }
            else if (strCaller == "colUnitCostExVat")
            {
                if (decUnitCostExVat > (decimal)0.00)
                {
                    decUnitCostIncVat = decUnitCostExVat * ((decimal)1.00 + (decVatRate / (decimal)100.00));  // Calc Total cost Inc VAT //
                    if (decAmountOrderedConverted > (decimal)0.00)
                    {
                        decTotalCostExVat = decUnitCostExVat * decAmountOrderedConverted;  // Calc Unit Cost Ex VAT //
                        decTotalCostIncVat = decUnitCostIncVat * decAmountOrderedConverted;  // Calc Unit Cost Inc VAT //
                    }
                }
            }
            else if (strCaller == "colUnitCostIncVat")
            {
                if (decUnitCostIncVat > (decimal)0.00)
                {
                    decUnitCostExVat = decUnitCostIncVat * ((decimal)1.00 - (decVatRate / (decimal)100.00));  // Calc Total cost Inc VAT //
                    if (decAmountOrderedConverted > (decimal)0.00)
                    {
                        decTotalCostExVat = decUnitCostExVat * decAmountOrderedConverted;  // Calc Unit Cost Ex VAT //
                        decTotalCostIncVat = decUnitCostIncVat * decAmountOrderedConverted;  // Calc Unit Cost Inc VAT //
                    }
                }
            }
            else if (strCaller == "colTotalCostExVat")
            {
                if (decTotalCostExVat > (decimal)0.00)
                {
                    decTotalCostIncVat = decTotalCostExVat * ((decimal)1.00 + (decVatRate / (decimal)100.00));  // Calc Total cost Inc VAT //
                    if (decAmountOrderedConverted > (decimal)0.00)
                    {
                        decUnitCostExVat = decTotalCostExVat / decAmountOrderedConverted;  // Calc Unit Cost Ex VAT //
                        decUnitCostIncVat = decTotalCostIncVat / decAmountOrderedConverted;  // Calc Unit Cost Inc VAT //
                    }
                }
            }
            else if (strCaller == "colTotalCostIncVat")
            {
                if (decTotalCostIncVat > (decimal)0.00)
                {
                    decTotalCostExVat = decTotalCostIncVat * ((decimal)1.00 - (decVatRate / (decimal)100.00));  // Calc Total cost Inc VAT //
                    if (decAmountOrderedConverted > (decimal)0.00)
                    {
                        decUnitCostExVat = decTotalCostExVat / decAmountOrderedConverted;  // Calc Unit Cost Ex VAT //
                        decUnitCostIncVat = decTotalCostIncVat / decAmountOrderedConverted;  // Calc Unit Cost Inc VAT //
                    }
                }
            }
            //if (decVatRate != Convert.ToDecimal(view.GetFocusedRowCellValue("VatRate"))) view.SetFocusedRowCellValue("VatRate", decVatRate);
            if (decUnitCostExVat != Convert.ToDecimal(view.GetFocusedRowCellValue("UnitCostExVat"))) view.SetFocusedRowCellValue("UnitCostExVat", decUnitCostExVat);
            if (decUnitCostIncVat != Convert.ToDecimal(view.GetFocusedRowCellValue("UnitCostIncVat"))) view.SetFocusedRowCellValue("UnitCostIncVat", decUnitCostIncVat);
            if (decTotalCostExVat != Convert.ToDecimal(view.GetFocusedRowCellValue("TotalCostExVat"))) view.SetFocusedRowCellValue("TotalCostExVat", decTotalCostExVat);
            if (decTotalCostIncVat != Convert.ToDecimal(view.GetFocusedRowCellValue("TotalCostIncVat"))) view.SetFocusedRowCellValue("TotalCostIncVat", decTotalCostIncVat);

            // Calculate Order Totals //
            DataRowView currentRow = (DataRowView)sp04224GCSaltOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                int intOrderID = Convert.ToInt32(currentRow["GritOrderHeaderID"]);
                decimal decConvertedAmount = (decimal)0.00;
                decUnitCostExVat = (decimal)0.00;
                decUnitCostIncVat = (decimal)0.00;
                decTotalCostExVat = (decimal)0.00;
                decTotalCostIncVat = (decimal)0.00;
                DataRow[] drOrderLines = dataSet_GC_DataEntry.sp04227_GC_Salt_Order_Line_Edit.Select("GritOrderHeaderID = " + intOrderID);
                foreach (DataRow dr in drOrderLines)
                {
                    decConvertedAmount += Convert.ToDecimal(dr["AmountOrderedConverted"]);
                    decTotalCostExVat += Convert.ToDecimal(dr["TotalCostExVat"]);
                    decTotalCostIncVat += Convert.ToDecimal(dr["TotalCostIncVat"]);
                }
                currentRow["TotalAmountConverted"] = decConvertedAmount;
                currentRow["TotalCostExVat"] = decTotalCostExVat;
                currentRow["TotalCostIncVat"] = decTotalCostIncVat;
                currentRow["UnitCostExVat"] = (decTotalCostExVat > (decimal)0.00 && decConvertedAmount > (decimal)0.00 ? decTotalCostExVat / decConvertedAmount : (decimal)0.00);
                currentRow["UnitCostIncVat"] = (decTotalCostIncVat > (decimal)0.00 && decConvertedAmount > (decimal)0.00 ? decTotalCostIncVat / decConvertedAmount : (decimal)0.00);

                this.sp04224GCSaltOrderEditBindingSource.EndEdit();
            }
  

        }


 


    }
}


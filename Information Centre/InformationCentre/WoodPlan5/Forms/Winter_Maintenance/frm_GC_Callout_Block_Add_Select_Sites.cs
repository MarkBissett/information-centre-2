﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Callout_Block_Add_Select_Sites : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public int i_int_selected_count = 0;

        BaseObjects.GridCheckMarksSelection selection1;

        private DataSet _dsSites;
        public int intReactive = 1;
        #endregion

        public frm_GC_Callout_Block_Add_Select_Sites()
        {
            InitializeComponent();
        }

        private void frm_GC_Callout_Block_Add_Select_Sites_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 400021;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            gridControl1.DataSource = _dsSites.Tables[0];  // Bind Grid to Data //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            gridControl1.ForceInitialize();

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }
 

        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Sites Available");
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {

        }
 
        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "PreferredTeam")
            {
                switch (view.GetRowCellValue(e.RowHandle, "PreferredTeam").ToString())
                {
                    case "":
                        e.Appearance.BackColor = Color.Khaki;
                        e.Appearance.BackColor2 = Color.DarkOrange;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion


        private void Store_Selected_Sites_Gritting_Contracts()
        {
            // Delete any rows not selected for block adding //
            i_int_selected_count = 0;
            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An invalid value is present within a column on the current row of the grid - Correct before procceeding.", "Select Sites for Block Add", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                i_int_selected_count = 0;  // Stop user saving and closing screen //
                return;
            }
            if (view.DataRowCount <= 0)
            {
                return;
            }
            else if (selection1.SelectedCount <= 0)
            {
                return;
            }
            else
            {
                view.BeginUpdate();
                for (int i = view.DataRowCount - 1; i >= 0; i--)  // Process backwards as we are deleting //
                {
                    if (!Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        _dsSites.Tables[0].Rows.Remove(view.GetDataRow(i));
                    }
                }
                i_int_selected_count = selection1.SelectedCount;
                view.EndUpdate();
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Store_Selected_Sites_Gritting_Contracts();  // Remove any sites not selected for block adding so original passed in dataset is updated //
            if (i_int_selected_count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites by ticking them before proceeding.", "Select Sites for Block Add", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnDoubleGrit_Click(object sender, EventArgs e)
        {
            if (selection1.SelectedCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records by ticking them before procceeding.", "Double Required Grid for Ticked Sites", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    view.SetRowCellValue(i, "SaltUsed", Convert.ToDecimal(view.GetRowCellValue(i, "SaltUsed")) * (decimal)2.00);
                }
            }
            view.EndUpdate();
        }


        public DataSet dsSites
        {
            get
            {
                return _dsSites;
            }
            set
            {
                // does not create a new dataset, but gives the variable a reference to the dataset assigned to dsSites  //
                _dsSites = value;
            }
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            intReactive = (ce.Checked ? 1 : 0);
        }


    }
}

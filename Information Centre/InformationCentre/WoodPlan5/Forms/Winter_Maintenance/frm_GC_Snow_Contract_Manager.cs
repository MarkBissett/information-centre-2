using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Snow_Contract_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState6;  // Used by Grid View State Facilities //

        int i_int_FocusedGrid = 1;
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs4 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs6 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        public string strCaller = "";

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        private string i_str_selected_CallOutType_ids = "";
        private string i_str_selected_Site_Names = "";
        private string i_str_selected_TabPages = "";
        private string i_str_selected_CallOutType_names = "";   
        private string i_str_selected_SiteIDs = "";

        public string strLinkedRecordType = "Snow Clearance Callouts";
        int intLinkedRecordType = 0;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        private string strCertificatePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table for Linked Documents //

        string i_str_selected_Company_ids = "";
        string i_str_selected_Company_names = "";
        BaseObjects.GridCheckMarksSelection selection4;

        #endregion

        public frm_GC_Snow_Contract_Manager()
        {
            InitializeComponent();
        }

        private void frm_GC_Snow_Contract_Manager_Load(object sender, EventArgs e)
        {

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4009;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            #region populate_Available_TabPages_List

            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            gridControl4.BeginUpdate();
            this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.Rows.Clear();
            foreach (XtraTabPage i in xtraTabControl1.TabPages)
            {
                DataRow drNewRow;
                drNewRow = this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.NewRow();
                drNewRow["TabPageName"] = i.Text;
                this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.Rows.Add(drNewRow);
            }
            gridControl4.EndUpdate();
            gridControl4.ForceInitialize();

            #endregion

            // Add record selection checkboxes to popup Callout Type grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp04001_GC_Job_CallOut_StatusesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04001_GC_Job_CallOut_StatusesTableAdapter.Fill(dataSet_GC_Core.sp04001_GC_Job_CallOut_Statuses);
            gridControl5.ForceInitialize();
            dateEditFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditToDate.DateTime = this.GlobalSettings.ViewedEndDate;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Add record selection checkboxes to popup Site Selection grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl8.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            sp04057_GC_Site_Filter_DropDown_Just_Griting_SitesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04057_GC_Site_Filter_DropDown_Just_Griting_SitesTableAdapter.Fill(dataSet_GC_Core.sp04057_GC_Site_Filter_DropDown_Just_Griting_Sites, "");
            gridControl8.ForceInitialize();

            sp04132_GC_Snow_Clearance_Site_Contract_Manager_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "SnowClearanceSiteContractID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04134_GC_Preferred_SnowClearance_TeamsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "PreferredSubContractorID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView4, "LinkedDocumentID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04135_GC_Preffered_Snow_Clearance_Team_Extra_CostsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView7, "ExtraCostID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04136_GC_Snow_Clearance_Contract_Site_AccessTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState6 = new RefreshGridState(gridView6, "SnowClearanceSiteContractAccessID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Add record selection checkboxes to popup Company Filter grid control //
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl9.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;
            sp04237_GC_Company_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04237_GC_Company_Filter_ListTableAdapter.Fill(dataSet_GC_Reports.sp04237_GC_Company_Filter_List);
            gridControl9.ForceInitialize();

            Load_Data();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar // 
            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();
            SetMenuStatus();
        }

        private void frm_GC_Snow_Contract_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                else if (UpdateRefreshStatus == 2)
                {
                    LoadLinkedRecords();
                }
                else if (UpdateRefreshStatus == 3)
                {
                    LoadLinkedRecords2();
                }
            }
            SetMenuStatus();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Site Gritting Contracts //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = true;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 2)  // Preferred Teams //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Linked Documents //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 4)  // Extra Default Costs //
            {
                view = (GridView)gridControl7.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl2.MainView;  // Parent is Preferred Gritting Contractors, NOT gridControl1 //
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 6)  // Access Considerations //
            {
                view = (GridView)gridControl6.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }


            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView7 navigator custom buttons //
            view = (GridView)gridControl7.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl7.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl7.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl7.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl7.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl7.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl7.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView6 navigator custom buttons //
            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        private void frm_GC_Snow_Contract_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter", i_str_selected_SiteIDs);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShownTabPages", i_str_selected_TabPages);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShowActiveOnly", (ShowActiveOnlyCheckEdit.Checked ? "1" : "0"));
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate", dateEditFromDate.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDate", dateEditToDate.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "LinkedRecordType", strLinkedRecordType);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CallOutFilter", i_str_selected_CallOutType_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // From Date //
                string strFromDate = default_screen_settings.RetrieveSetting("FromDate");
                if (!string.IsNullOrEmpty(strFromDate)) dateEditFromDate.DateTime = Convert.ToDateTime(strFromDate);

                // To Date //
                string strToDate = default_screen_settings.RetrieveSetting("ToDate");
                if (!string.IsNullOrEmpty(strToDate)) dateEditToDate.DateTime = Convert.ToDateTime(strToDate);

                // Linked Record Type //
                strLinkedRecordType = default_screen_settings.RetrieveSetting("LinkedRecordType");
                if (string.IsNullOrEmpty(strLinkedRecordType)) strLinkedRecordType = "Snow Clearance Callouts";
                comboBoxEditLinkedRecordType.Text = strLinkedRecordType;

                // Show Active Only //
                string strShowActiveOnly = default_screen_settings.RetrieveSetting("ShowActiveOnly");
                if (!string.IsNullOrEmpty(strShowActiveOnly)) ShowActiveOnlyCheckEdit.Checked = (Convert.ToInt32(strShowActiveOnly) == 1 ? true : false);


                // Shown Tab Pages //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("ShownTabPages");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl4.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["TabPageName"], strElement.Trim());
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit1.Text = PopupContainerEdit1_Get_Selected();
                }

                // Site Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("SiteFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl8.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["SiteID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit3.Text = PopupContainerEdit3_Get_Selected();
                }

                // Gritting Callout Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("CallOutFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["Value"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit2.Text = PopupContainerEdit2_Get_Selected();
                }

                // Company Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("CompanyFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl9.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["CompanyID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditCompanies.Text = PopupContainerEditCompanies_Get_Selected();
                }

                LoadContractorsBtn.PerformClick();
            }
        }

        private void LoadContractorsBtn_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            int intShowActiveOnly = (ShowActiveOnlyCheckEdit.Checked ? 1 : 0);
            gridControl1.BeginUpdate();
            sp04132_GC_Snow_Clearance_Site_Contract_Manager_ListTableAdapter.Fill(dataSet_GC_Snow_Core.sp04132_GC_Snow_Clearance_Site_Contract_Manager_List, i_str_selected_SiteIDs, "", intLinkedRecordType, dtFromDate, dtToDate, i_str_selected_CallOutType_ids, intShowActiveOnly, i_str_selected_Company_ids);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SnowClearanceSiteContractID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["SnowClearanceSiteContractID"])) + ',';
            }

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            // Populate Linked Preferred Gritting Teams //
            if (xtraTabPage1.PageVisible)
            {
                gridControl2.MainView.BeginUpdate();
                if (intCount == 0)
                {
                    this.dataSet_GC_Snow_Core.sp04134_GC_Preferred_SnowClearance_Teams.Clear();
                }
                else
                {
                    sp04134_GC_Preferred_SnowClearance_TeamsTableAdapter.Fill(dataSet_GC_Snow_Core.sp04134_GC_Preferred_SnowClearance_Teams, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl2.MainView.EndUpdate();

                strArray = null;
                intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs2 != "")
                {
                    strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl2.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["PreferredSubContractorID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs2 = "";
                }
            }
          
            // Populate Linked Documents //
            if (xtraTabPage2.PageVisible)
            {
                gridControl3.MainView.BeginUpdate();
                if (intCount == 0)
                {
                    this.dataSet_AT.sp00220_Linked_Documents_List.Clear();
                }
                else
                {
                    sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 15, strDefaultPath);
                    this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl3.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs3 != "")
                {
                    strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl3.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs3 = "";
                }
            }

            // Populate Linked Access Considerations //
            if (xtraTabPage3.PageVisible)
            {
                gridControl6.MainView.BeginUpdate();
                if (intCount == 0)
                {
                    this.dataSet_GC_Snow_Core.sp04136_GC_Snow_Clearance_Contract_Site_Access.Clear();
                }
                else
                {
                    sp04136_GC_Snow_Clearance_Contract_Site_AccessTableAdapter.Fill(dataSet_GC_Snow_Core.sp04136_GC_Snow_Clearance_Contract_Site_Access, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState6.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl6.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs6 != "")
                {
                    strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl6.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SnowClearanceSiteContractAccessID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs6 = "";
                }
            }            

        }

        private void LoadLinkedRecords2()
        {
            // Get Selected Preferred Gritting Team IDs so linked Extra Costs can be added //
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["PreferredSubContractorID"])) + ',';
            }

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            // Populate Linked Preferred Gritting Teams //
            if (xtraTabPage1.PageVisible)
            {
                gridControl7.MainView.BeginUpdate();
                if (intCount == 0)
                {
                    this.dataSet_GC_Snow_Core.sp04135_GC_Preffered_Snow_Clearance_Team_Extra_Costs.Clear();
                }
                else
                {
                    sp04135_GC_Preffered_Snow_Clearance_Team_Extra_CostsTableAdapter.Fill(dataSet_GC_Snow_Core.sp04135_GC_Preffered_Snow_Clearance_Team_Extra_Costs, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl7.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs4 != "")
                {
                    strArray = i_str_AddedRecordIDs4.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl7.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["ExtraCostID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs4 = "";
                }
            }
        }


        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3, string strNewIDs4, string strNewIDs5, string strNewIDs6)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            i_str_AddedRecordIDs1 = strNewIDs1;
            i_str_AddedRecordIDs2 = strNewIDs2;
            i_str_AddedRecordIDs3 = strNewIDs3;
            i_str_AddedRecordIDs4 = strNewIDs4;
            i_str_AddedRecordIDs6 = strNewIDs6;
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Site Snow Clearance Contracts Available";
                    break;
                case "gridView2":
                    message = "No Preferred Teams Available - Select one or more Snow Clearance Contracts to view linked Preferred Teams";
                    break;
                case "gridView3":
                    message = "No Linked Documents Available - Select one or more Site Snow Clearance Contracts to view Linked Documents";
                    break;
                case "gridView7":
                    message = "No Preferred Gritting Team Extra Costs Available - Select one or more Preferred Teams to view linked Extra Costs";
                    break;
                case "gridView6":
                    message = "No Access Considerations Available - Select one or more Snow Clearance Contracts to view linked Access Consigerations";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedRecords();
                    break;
                case "gridView2":
                    LoadLinkedRecords2();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strSiteIDs = view.GetRowCellValue(view.FocusedRowHandle, "SnowClearanceSiteContractID").ToString() + ",";
            if (comboBoxEditLinkedRecordType.EditValue.ToString() == "Snow Clearance Callouts")
            {
                DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                DateTime FromDate = (string.IsNullOrEmpty(dateEditFromDate.DateTime.ToString()) ? Convert.ToDateTime("2011-01-01") : dateEditFromDate.DateTime);
                DateTime ToDate = (string.IsNullOrEmpty(dateEditToDate.DateTime.ToString()) ? Convert.ToDateTime("2500-12-31") : dateEditToDate.DateTime.AddDays(1).AddSeconds(-1));
                string strRecordIDs = GetSetting.sp04196_GC_Snow_DrillDown_Get_Linked_Callouts(strSiteIDs, "site_contract", i_str_selected_CallOutType_ids, FromDate, ToDate).ToString();
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "site_snow_clearance_callouts");
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedRecordCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedRecordCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedRecordCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedRecordCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "Email":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "Email").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView2_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "Email":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("Email").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit4_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strEmailAddress = view.GetRowCellValue(view.FocusedRowHandle, "Email").ToString();
            if (string.IsNullOrEmpty(strEmailAddress))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Email Address Available - unable to proceed.", "Email Contact", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start("mailto:" + strEmailAddress);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to email: " + strEmailAddress + ".", "Email Contact", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView2;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 0) return;
                        if (Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "PreferrenceOrder")) <= 1) return;  // Stop attempting to move order outwith group //
 
                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "PreferrenceOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "PreferrenceOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "PreferrenceOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "PreferrenceOrder")) + 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow >= view.DataRowCount - 1) return;
                        if (Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "PreferrenceOrder")) - 1 == 0) return;   // Stop attempting to move order outwith group //

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "PreferrenceOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "PreferrenceOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "PreferrenceOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "PreferrenceOrder")) - 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("set_order".Equals(e.Button.Tag))
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Saving Sort Order...");
                        fProgress.Show();
                        Application.DoEvents();
                        int intUpdateProgressThreshhold = view.DataRowCount / 10;
                        int intUpdateProgressTempCount = 0;

                        DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter SaveSortOrder = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();        
                        SaveSortOrder.ChangeConnectionString(strConnectionString);

                        gridControl1.BeginUpdate();
                        int intLastParentID = 0;
                        int intOrder = 0;
                        view.BeginSort();
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(i, "SnowClearanceSiteContractID")) != intLastParentID)
                            {
                                intOrder = 0;
                                intLastParentID = Convert.ToInt32(view.GetRowCellValue(i, "SnowClearanceSiteContractID"));
                            }
                            intOrder++;
                            view.SetRowCellValue(i, "PreferrenceOrder", intOrder);
                            try
                            {
                                SaveSortOrder.sp04137_GC_Preferred_Snow_Clearance_Teams_Save_Sort_Order(Convert.ToInt32(view.GetRowCellValue(i, "PreferredSubContractorID")), i + 1);  // Save Changes to DB //
                            }
                            catch (Exception Ex)
                            {
                                view.EndSort();
                                gridControl1.EndUpdate();
                                fProgress.Close();
                                fProgress.Dispose();
                                DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while saving. Please try again.\n\nException:" + Ex.Message, "Save Sort Order", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }
                        view.EndSort();
                        gridControl1.EndUpdate();
                        fProgress.SetProgressValue(100);  // Show full progress //
                        fProgress.UpdateCaption("Sort Order Saved");
                        Application.DoEvents();
                        System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
                        fProgress.Close();
                        fProgress.Dispose();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView3_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "CertificatePath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView3_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("CertificatePath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView7

        private void gridView7_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 4;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl7_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView6

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 6;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView2;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Site Panel

        private void btnOK3_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void popupContainerEdit3_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit3_Get_Selected();
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_SiteIDs = "";    // Reset any prior values first //
            i_str_selected_Site_Names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl8.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_SiteIDs = "";
                return "No Site Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_SiteIDs = "";
                return "No Site Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_SiteIDs += Convert.ToString(view.GetRowCellValue(i, "SiteID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Site_Names = Convert.ToString(view.GetRowCellValue(i, "SiteName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Site_Names += ", " + Convert.ToString(view.GetRowCellValue(i, "SiteName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Site_Names;
        }

        #endregion


        #region Callout Status Filter Panel

        private void btnGritCalloutFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void popupContainerEdit2_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_CallOutType_ids = "";    // Reset any prior values first //
            i_str_selected_CallOutType_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CallOutType_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CallOutType_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CallOutType_names;
        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        #endregion


        #region Tab Pages Shown Panel

        private void btnOK2_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void popupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_TabPages = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_TabPages = "";
                foreach (XtraTabPage i in xtraTabControl1.TabPages)
                {
                    i.PageVisible = true;
                }
                return "No Related Data";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_TabPages = "";
                foreach (XtraTabPage i in xtraTabControl1.TabPages)
                {
                    i.PageVisible = true;
                }
                return "No Related Data";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        //i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_TabPages = Convert.ToString(view.GetRowCellValue(i, "TabPageName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_TabPages += ", " + Convert.ToString(view.GetRowCellValue(i, "TabPageName"));
                        }
                        intCount++;
                    }
                }
            }

            foreach (XtraTabPage i in xtraTabControl1.TabPages)
            {
                string strText = i.Text;
                i.PageVisible = i_str_selected_TabPages.Contains(strText);
            }

            return i_str_selected_TabPages;
        }

        #endregion


        #region Company Filter Panel

        private void popupContainerEditCompanies_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCompanies_Get_Selected();
        }

        private void btnCompanyFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditCompanies_Get_Selected()
        {
            i_str_selected_Company_ids = "";    // Reset any prior values first //
            i_str_selected_Company_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl9.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Company_ids += Convert.ToString(view.GetRowCellValue(i, "CompanyID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Company_names = Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Company_names += ", " + Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Company_names;
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
            if (!iBool_AllowAdd) return;

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            GridView view = null;
            System.Reflection.MethodInfo method = null;
            int[] intRowHandles = null;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Site Gritting Contract //
                    {
                        view = (GridView)gridControl1.MainView;
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Contract_Edit fChildForm = new frm_GC_Snow_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:  // Preferred Gritting Team //
                    {
                        // Check if only one parent record selected - if yes, pass it to child screen otherwise pass no record id //
                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Preferred_Team_Edit fChildForm2 = new frm_GC_Snow_Preferred_Team_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        int intParentID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SnowClearanceSiteContractID")) : 0);
                        string strParentDescription = (intRowHandles.Length == 1 ?
                                        (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(view.GetRowCellValue(intRowHandles[0], "SiteName"))) + "  [" +
                                        (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(intRowHandles[0], "StartDate"))) ? "No Start" : Convert.ToString(view.GetRowCellValue(intRowHandles[0], "StartDate"))) + " - " +
                                        (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(intRowHandles[0], "EndDate"))) ? "No End" : Convert.ToString(view.GetRowCellValue(intRowHandles[0], "EndDate"))) + "]  Active = " +
                                        (Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "Active")) == 0 ? "No" : "Yes") : "");

                        fChildForm2.intLinkedToRecordID = intParentID;
                        fChildForm2.strLinkedToRecordDescription = strParentDescription;
                        int intMaxOrder = 0;
                        view = (GridView)gridControl2.MainView;
                        if (intParentID != 0)
                        {
                            for (int i = 0; i < view.DataRowCount; i++)
                            {
                                if (Convert.ToInt32(view.GetRowCellValue(i, "SiteGrittingContractID")) == intParentID && Convert.ToInt32(view.GetRowCellValue(i, "PreferrenceOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "PreferrenceOrder"));
                            }
                            fChildForm2.intMaxOrder = intMaxOrder;
                        }
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 3:  // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 15;  // Site Snow Clearance Contract //

                        GridView ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SnowClearanceSiteContractID"));
                            fChildForm2.strLinkedToRecordDesc = (intRowHandles.Length == 1 ?
                                          (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) + "  [" +
                                          (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "StartDate"))) ? "No Start" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "StartDate"))) + " - " +
                                          (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "EndDate"))) ? "No End" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "EndDate"))) + "]  Active = " +
                                          (Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "Active")) == 0 ? "No" : "Yes") : "");
                        }
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 4:  // Linked Extra Costs //
                    {
                        // Check if only one parent preferred contractor selected - if yes, pass it to child screen otherwise pass none //
                        view = (GridView)gridControl2.MainView;  // Parent GridView - GridView 2 [Prefered Gritting Team] //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Preferred_Team_Extra_Cost_Edit fChildForm3 = new frm_GC_Snow_Preferred_Team_Extra_Cost_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = "";
                        fChildForm3.strFormMode = "add";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = 0;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        int intParentID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "PreferredSubContractorID")) : 0);
                        string strParentDescription = (intRowHandles.Length == 1 ? view.GetRowCellValue(intRowHandles[0], "TeamName").ToString() : "");

                        fChildForm3.intLinkedToRecordID = intParentID;
                        fChildForm3.strLinkedToRecordDescription = strParentDescription;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 6:  // Access Considerations //
                    {
                        // Check if only one parent record selected - if yes, pass it to child screen otherwise pass no record id //
                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Access_Consideration_Edit fChildForm2 = new frm_GC_Snow_Access_Consideration_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        int intParentID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SnowClearanceSiteContractID")) : 0);
                        string strParentDescription = (intRowHandles.Length == 1 ?
                                        (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(view.GetRowCellValue(intRowHandles[0], "SiteName"))) + "  [" +
                                        (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(intRowHandles[0], "StartDate"))) ? "No Start" : Convert.ToString(view.GetRowCellValue(intRowHandles[0], "StartDate"))) + " - " +
                                        (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(intRowHandles[0], "EndDate"))) ? "No End" : Convert.ToString(view.GetRowCellValue(intRowHandles[0], "EndDate"))) + "]  Active = " +
                                        (Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "Active")) == 0 ? "No" : "Yes") : "");

                        fChildForm2.intLinkedToRecordID = intParentID;
                        fChildForm2.strLinkedToRecordDescription = strParentDescription;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
            }
        }

        private void Block_Add_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Site Gritting Contract //
                    {
                        if (!iBool_AllowAdd) return;

                        frm_GC_Snow_Contract_Manager_Block_Add_Select_Sites fChildForm = new frm_GC_Snow_Contract_Manager_Block_Add_Select_Sites();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;
                        strRecordIDs = fChildForm.i_str_selected_SiteIDs;
                        string strRecordDescriptors = fChildForm.i_str_selected_Site_Names;
                        intCount = fChildForm.i_int_selected_count;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Contract_Edit fChildForm2 = new frm_GC_Snow_Contract_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 2:     // Related Preferred Gritting Teams //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Preferred_Team_Edit fChildForm2 = new frm_GC_Snow_Preferred_Team_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 3:     // Related Documents Link //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 15;  // Site Snow Clearance Contracts //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 4:     // Related Extra Costs //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl2.MainView;  // Parent GridView - GridView 2 [Prefered Gritting Team] //
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PreferredSubContractorID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Preferred_Team_Extra_Cost_Edit fChildForm2 = new frm_GC_Snow_Preferred_Team_Extra_Cost_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 6:     // Related Access Considerations //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Access_Consideration_Edit fChildForm2 = new frm_GC_Snow_Access_Consideration_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            if (!iBool_AllowEdit) return;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            frmProgress fProgress = null;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Site Gritting Contract //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Contract_Edit fChildForm = new frm_GC_Snow_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:  // Preferred Gritting Team //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PreferredSubContractorID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Preferred_Team_Edit fChildForm2 = new frm_GC_Snow_Preferred_Team_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Block Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 15;  // Site Snow Clearance Contracts //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 4:  // Extra Costs //
                    {
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ExtraCostID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Preferred_Team_Extra_Cost_Edit fChildForm3 = new frm_GC_Snow_Preferred_Team_Extra_Cost_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "edit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 6:  // Access Considerations //
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractAccessID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Access_Consideration_Edit fChildForm2 = new frm_GC_Snow_Access_Consideration_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
            }
        }

        private void Block_Edit_Record()
        {
            if (!iBool_AllowEdit) return;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            frmProgress fProgress = null;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Records", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Contract_Edit fChildForm = new frm_GC_Snow_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:  // Preferred Gritting Team //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PreferredSubContractorID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Preferred_Team_Edit fChildForm2 = new frm_GC_Snow_Preferred_Team_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 15;  // Site Snow Clearance Contracts //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 4:  // Extra Costs //
                    {
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ExtraCostID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Preferred_Team_Extra_Cost_Edit fChildForm3 = new frm_GC_Snow_Preferred_Team_Extra_Cost_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "blockedit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 6:  // Access Considerations //
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractAccessID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Access_Consideration_Edit fChildForm2 = new frm_GC_Snow_Access_Consideration_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
            }
        }

        private void Delete_Record()
        {
            if (!iBool_AllowDelete) return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Site Snow Clearance Contract //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Snow Clearance Contracts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Site Snow Clearance Contract" : Convert.ToString(intRowHandles.Length) + " Site Snow Clearance Contracts") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Site Snow Clearance Contract" : "these Site Snow Clearance Contracts") + " will no longer be available for selection AND any linked records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp03002_EP_Client_Delete("gc_snow_clearance_site_contract", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            Load_Data();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 2:  // Preferred Snow Clearance Team //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Preferred Snow Clearance Teams to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Preferred Snow Clearance Team" : Convert.ToString(intRowHandles.Length) + " Preferred Snow Clearance Teams") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Preferred Snow Clearance Team" : "these Preferred Snow Clearance Teams") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PreferredSubContractorID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp03002_EP_Client_Delete("gc_snow_clearance_preferred_team", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 3:  // Linked Documents //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 4:  // Extra Costs //
                    {
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Snow Clearance Team Extra Costs to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Snow Clearance Team Extra Cost" : Convert.ToString(intRowHandles.Length) + " Snow Clearance Team Extra Costs") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Snow Clearance Team Extra Cost" : "these Snow Clearance Team Extra Costs") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ExtraCostID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp03002_EP_Client_Delete("gc_snow_clearance_preferred_team_extra_cost", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords2();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 6:  // Access Considerations //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Access Considerations to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Access Consideration" : Convert.ToString(intRowHandles.Length) + " Access Considerations") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Access Consideration" : "these Access Considerations") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractAccessID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp03002_EP_Client_Delete("gc_snow_clearance_site_contract_access", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }


        private void comboBoxEditLinkedRecordType_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit cbe = (ComboBoxEdit)sender;
            switch (cbe.Text)
            {
                case "No Linked Records":
                    intLinkedRecordType = 1;
                    popupContainerEdit2.Properties.ReadOnly = true;
                    break;
                case "Snow Clearance Callouts":
                    intLinkedRecordType = 2;
                    popupContainerEdit2.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
            strLinkedRecordType = cbe.Text;
        }


   
    }
}


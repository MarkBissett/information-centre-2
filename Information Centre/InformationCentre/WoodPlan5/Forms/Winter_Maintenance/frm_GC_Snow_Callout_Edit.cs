using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraBars;
using DevExpress.Skins;
using System.Data.SqlClient;

using WoodPlan5.Properties;
using BaseObjects;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_GC_Snow_Callout_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Rate Calculation //
        private bool ibool_FormStillLoading = true;

        public string strSiteName = "";
        public string strClientName = "";
        public int intSiteID = 0;
        public int intClientID = 0;
        public int intSnowClearanceSiteContractID = 0;
        public int intSubContractorID = 0;
        public string strSubContractorName = "";
        decimal decVatRate = (decimal)0.00;
       
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        #endregion

        public frm_GC_Snow_Callout_Edit()
        {
            InitializeComponent();
        }

        private void frm_GC_Snow_Callout_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 400038;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Get Default VAT Rate //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                decVatRate = Convert.ToDecimal(GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate"));
            }
            catch (Exception)
            {
                decVatRate = (decimal)0.00;
            }

            sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04085_GC_Gritting_Callout_Status_List_With_Blank, "");
            
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp00226_Staff_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00226_Staff_List_With_BlankTableAdapter.Fill(dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);

            sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter.Fill(dataSet_GC_Snow_DataEntry.sp04165_GC_Client_How_Soon_Types_With_Blank);

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Populate Main Dataset //
            sp04163_GC_Snow_Callout_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SnowClearanceSiteContractID"] = intSnowClearanceSiteContractID;
                        drNewRow["SiteName"] = strSiteName;
                        drNewRow["SiteID"] = intSiteID;
                        drNewRow["AccessComments"] = Get_Access_Considerations(intSnowClearanceSiteContractID);
                        drNewRow["ClientName"] = strClientName;
                        drNewRow["SubContractorID"] = intSubContractorID;
                        drNewRow["SubContractorName"] = strSubContractorName;
                        drNewRow["OriginalSubContractorID"] = 0;
                        drNewRow["Reactive"] = 0;
                        drNewRow["JobStatusID"] = 30; // 30 = Started - To Be Completed - Authorised //
                        drNewRow["CallOutDateTime"] = DateTime.Now;
                        drNewRow["RecordedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["RecordedByName"] = (string.IsNullOrEmpty(this.GlobalSettings.UserSurname) ? "" : this.GlobalSettings.UserSurname + ": ") + (string.IsNullOrEmpty(this.GlobalSettings.UserForename) ? "" : this.GlobalSettings.UserForename);
                        drNewRow["HoursWorked"] = (decimal)0.00;
                        drNewRow["NoAccessAbortedRate"] = (decimal)0.00;
                        drNewRow["LabourCost"] = (decimal)0.00;
                        drNewRow["LabourSell"] = (decimal)0.00;
                        drNewRow["LabourVatRate"] = decVatRate;
                        drNewRow["OtherCost"] = (decimal)0.00;
                        drNewRow["OtherSell"] = (decimal)0.00;
                        drNewRow["TotalCost"] = (decimal)0.00;
                        drNewRow["TotalSell"] = (decimal)0.00;
                        drNewRow["NonStandardCost"] = 0;
                        drNewRow["NonStandardSell"] = 0;
                        drNewRow["ClientHowSoonID"] = 0;
                        drNewRow["ChargeMethodID"] = 0;
                        drNewRow["ChargeFixedPrice"] = (decimal)0.00;
                        drNewRow["ChargeFixedNumberOfHours"] = (decimal)0.00;
                        drNewRow["ChargeFixedHourlyRate"] = (decimal)0.00;
                        drNewRow["ChargeExtraHourlyRate"] = (decimal)0.00;
                        drNewRow["ChargeMarkupPercentage"] = (decimal)0.00;
                        drNewRow["NoAccess"] = 0;
                        drNewRow["VisitAborted"] = 0;
                        drNewRow["TimesheetSubmitted"] = 0;
                        drNewRow["FinanceTeamPaymentExported"] = 0;
                        this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.NewRow();
                        drNewRow["strMode"] = "blockadd";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SnowClearanceSiteContractID"] = 0;
                        drNewRow["SiteName"] = "Block Adding - N\\A";
                        drNewRow["ClientName"] = "Block Adding - N\\A";
                        drNewRow["SiteID"] = 0;
                        drNewRow["ClientID"] = 0;
                        drNewRow["SubContractorID"] = 0;
                        drNewRow["SubContractorName"] = 0;
                        drNewRow["OriginalSubContractorID"] = 0;
                        drNewRow["ChargeMethodID"] = 0;
                        drNewRow["ChargeFixedPrice"] = (decimal)0.00;
                        drNewRow["ChargeFixedNumberOfHours"] = (decimal)0.00;
                        drNewRow["ChargeFixedHourlyRate"] = (decimal)0.00;
                        drNewRow["ChargeExtraHourlyRate"] = (decimal)0.00;
                        drNewRow["ChargeMarkupPercentage"] = (decimal)0.00;
                        drNewRow["NoAccessAbortedRate"] = (decimal)0.00;
                        drNewRow["NoAccess"] = 0;
                        drNewRow["VisitAborted"] = 0;
                        drNewRow["TimesheetSubmitted"] = 0;
                        drNewRow["FinanceTeamPaymentExported"] = 0;
                        this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                         this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.Rows.Add(drNewRow);
                        this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp04163_GC_Snow_Callout_EditTableAdapter.Fill(this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            if (strFormMode == "add" || strFormMode == "edit")
            {
                sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter.Connection.ConnectionString = strConnectionString;
                RefreshGridViewState1 = new RefreshGridState(gridView1, "SnowClearanceCallOutRateID");
                gridControl1.BeginUpdate();
                sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter.Fill(dataSet_GC_Snow_Core.sp04162_GC_Snow_Clearance_Callout_Linked_Rates, strRecordIDs);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                gridControl1.EndUpdate();
            }


            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Company", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
            
            if (strFormMode == "blockedit")
            {
                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                // Get BarButtons Location //
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, 0, 0);
                foreach (BarItemLink link in bar3.ItemLinks)
                {
                    if (link.Item.Name == "barStaticItemInformation")  // Find the Correct button then call function to gets it position using Reflection (as this info is not publicly available //
                    {
                        rect = GetLinksScreenRect(link);
                        break;
                    }
                }
                if (rect.X > 0 && rect.Y > 0)
                {
                    // Draw Locus Effect on object so user can see it //
                    System.Drawing.Point screenPoint = new System.Drawing.Point(rect.X + rect.Width - 10, rect.Y + rect.Height - 10);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                }
            }
        }
        private System.Drawing.Rectangle GetLinksScreenRect(BarItemLink link)
        {
            System.Reflection.PropertyInfo info = typeof(BarItemLink).GetProperty("BarControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            Control c = (Control)info.GetValue(link, null);
            return c.RectangleToScreen(link.Bounds);
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientPONumberTextEdit.Focus();

                        SiteNameButtonEdit.Properties.ReadOnly = true;
                        SiteNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SiteNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        SubContractorNameButtonEdit.Properties.ReadOnly = true;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        OriginalSubContractorNameButtonEdit.Properties.ReadOnly = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientPONumberTextEdit.Focus();

                        SiteNameButtonEdit.Properties.ReadOnly = true;
                        SiteNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        SiteNameButtonEdit.Properties.Buttons[1].Enabled = false;

                        SubContractorNameButtonEdit.Properties.ReadOnly = true;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        OriginalSubContractorNameButtonEdit.Properties.ReadOnly = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientPONumberTextEdit.Focus();

                        SiteNameButtonEdit.Properties.ReadOnly = true;
                        SiteNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SiteNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        SubContractorNameButtonEdit.Properties.ReadOnly = true;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        OriginalSubContractorNameButtonEdit.Properties.ReadOnly = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientPONumberTextEdit.Focus();

                        SiteNameButtonEdit.Properties.ReadOnly = true;
                        SiteNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        SiteNameButtonEdit.Properties.Buttons[1].Enabled = false;

                        SubContractorNameButtonEdit.Properties.ReadOnly = true;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        SubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;

                        OriginalSubContractorNameButtonEdit.Properties.ReadOnly = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        OriginalSubContractorNameButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            Set_Control_Readonly_Status("");
            Set_Grit_Warning_Label();
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_GC_Snow_DataEntry.GetChanges();

            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource.EndEdit();  // Force any pending save from Grid to underlying table adapter //
            DataSet dsChanges2 = dataSet_GC_Snow_Core.GetChanges();
            if (dsChanges != null || dsChanges2 != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_GC_Snow_Callout_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_GC_Snow_Callout_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp04163GCSnowCalloutEditBindingSource.EndEdit();
            try
            {
                this.sp04163_GC_Snow_Callout_EditTableAdapter.Update(dataSet_GC_Snow_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["SnowClearanceCallOutID"]) + ";";
                    // Switch mode to Edit so than any subsequent changes update this record //
                    this.strFormMode = "edit";
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //

                    // Pass new ID to any records in the linked Rates grid //
                    GridView view = (GridView)gridControl1.MainView;
                    int intSnowClearanceCallOutID = Convert.ToInt32(currentRow["SnowClearanceCallOutID"]);
                    view.BeginUpdate();
                    foreach (DataRow dr in dataSet_GC_Snow_Core.sp04162_GC_Snow_Clearance_Callout_Linked_Rates.Rows)
                    {
                        dr["SnowClearanceCallOutID"] = intSnowClearanceCallOutID;
                    }
                    this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                    view.EndUpdate();
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
                    currentRow["strMode"] = "blockedit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Attempt to save any changes to Linked Rates... //
            try
            {
                this.sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter.Update(dataSet_GC_Snow_Core);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked rate record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }


            // Notify any open instances of Callout Manager they will need to refresh their data on activating //
            switch (strCaller)
            {
                case "frm_GC_Snow_Callout_Manager":
                    {
                        frm_GC_Snow_Callout_Manager fParentForm;
                        foreach (Form frmChild in this.ParentForm.MdiChildren)
                        {
                            if (frmChild.Name == "frm_GC_Snow_Callout_Manager")
                            {
                                fParentForm = (frm_GC_Snow_Callout_Manager)frmChild;
                                fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "", "", "");
                            }
                        }
                    }
                    break;
                case "frm_GC_Snow_Authorise_Callouts":
                    {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                        frm_GC_Snow_Authorise_Callouts fParentForm;
                        foreach (Form frmChild in this.ParentForm.MdiChildren)
                        {
                            if (frmChild.Name == "frm_GC_Snow_Authorise_Callouts")
                            {
                                fParentForm = (frm_GC_Snow_Authorise_Callouts)frmChild;
                                fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                            }
                        }
                    }
                    break;
                case "frm_GC_Finance_Invoice_Jobs":
                    {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                        frm_GC_Finance_Invoice_Jobs fParentForm;
                        foreach (Form frmChild in this.ParentForm.MdiChildren)
                        {
                            if (frmChild.Name == "frm_GC_Finance_Invoice_Jobs")
                            {
                                fParentForm = (frm_GC_Finance_Invoice_Jobs)frmChild;
                                fParentForm.UpdateFormRefreshStatus(2, "","", "");
                            }
                        }
                    }
                    break;
            }


            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            int intRateRecordNew = 0;
            int intRateRecordModified = 0;
            int intRateRecordDeleted = 0;
            this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_GC_Snow_Core.sp04162_GC_Snow_Clearance_Callout_Linked_Rates.Rows.Count; i++)
            {
                switch (this.dataSet_GC_Snow_Core.sp04162_GC_Snow_Clearance_Callout_Linked_Rates.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRateRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRateRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRateRecordDeleted++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intRateRecordNew > 0 || intRateRecordModified > 0 || intRateRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New callout record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated callout record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted callout record(s)\n";
                if (intRateRecordNew > 0) strMessage += Convert.ToString(intRateRecordNew) + " New rate record(s)\n";
                if (intRateRecordModified > 0) strMessage += Convert.ToString(intRateRecordModified) + " Updated rate record(s)\n";
                if (intRateRecordDeleted > 0) strMessage += Convert.ToString(intRateRecordDeleted) + " Deleted rate record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    FilterGrid();
                    GridView view = (GridView)gridControl1.MainView;
                    view.ExpandAllGroups();
                }
                Set_Control_Readonly_Status("");
                Set_Grit_Warning_Label();
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void SiteNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Site Button //
            {
                frm_GC_Select_Snow_Site_Contract_Active_Only fChildForm = new frm_GC_Select_Snow_Site_Contract_Active_Only();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {

                    DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                    if (currentRow != null)
                    {
                        int intOldSiteSnowClearanceContractID = 0;
                        if (!string.IsNullOrEmpty(currentRow["SnowClearanceSiteContractID"].ToString())) intOldSiteSnowClearanceContractID = Convert.ToInt32(currentRow["SnowClearanceSiteContractID"]);
                        currentRow["SnowClearanceSiteContractID"] = fChildForm.intSiteSnowClearanceContractID;
                        currentRow["SiteName"] = fChildForm.strSelectedSite;
                        currentRow["SiteID"] = fChildForm.intSelectedSiteID;
                        currentRow["ClientName"] = fChildForm.strSelectedClient;
                        currentRow["ChargeMethodID"] = fChildForm.ChargeMethodID;
                        currentRow["ChargeFixedPrice"] = fChildForm.ChargeFixedPrice;
                        currentRow["ChargeFixedNumberOfHours"] = fChildForm.ChargeFixedNumberOfHours;
                        currentRow["ChargeFixedHourlyRate"] = fChildForm.ChargeFixedHourlyRate;
                        currentRow["ChargeExtraHourlyRate"] = fChildForm.ChargeExtraHourlyRate;
                        currentRow["ChargeMarkupPercentage"] = fChildForm.ChargeMarkupPercentage;
                        currentRow["ChargeMethodDescription"] = fChildForm.ChargeMethodDescription;
                        currentRow["AccessComments"] = Get_Access_Considerations(fChildForm.intSiteSnowClearanceContractID);

                        currentRow["SiteAddressLine1"] = fChildForm.SiteAddressLine1;
                        currentRow["SiteAddressLine2"] = fChildForm.SiteAddressLine2;
                        currentRow["SiteAddressLine3"] = fChildForm.SiteAddressLine3;
                        currentRow["SiteAddressLine4"] = fChildForm.SiteAddressLine4;
                        currentRow["SiteAddressLine5"] = fChildForm.SiteAddressLine5;
                        currentRow["SitePostcode"] = fChildForm.SitePostcode;

                        // Clear Linked Client Purchase Order number as it may not be correct for the current site //
                        currentRow["ClientPOID"] = 0;
                        currentRow["ClientPOIDDescription"] = "";

                        if (intOldSiteSnowClearanceContractID <= 0) currentRow["JobStatusID"] = 10;  // Started //
                        this.sp04163GCSnowCalloutEditBindingSource.EndEdit();
                        Calculate_Total_Costs(0, "", (decimal)0.00);

                    }
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Site Button //
            {
                DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intSnowClearanceSiteContractID = (string.IsNullOrEmpty(currentRow["SnowClearanceSiteContractID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SnowClearanceSiteContractID"]));
                    if (intSnowClearanceSiteContractID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Site Snow Clearance Contract - no Site Snow Clearance Contract has been linked.", "View Linked Site Snow Clearance Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_GC_Snow_Contract_Edit fChildForm = new frm_GC_Snow_Contract_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intSnowClearanceSiteContractID + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }
        private void SiteNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(SiteNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SiteNameButtonEdit, "");
            }
        }

        private void SubContractorNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Tree Button //
            {
                Open_Select_Team_Screen();
            }
            else if (e.Button.Tag.ToString() == "view")  // View Contractor Button //
            {
                DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intSiteGrittingContractID = (string.IsNullOrEmpty(currentRow["SubContractorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SubContractorID"]));
                    if (intSiteGrittingContractID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Snow Clearance Team - no Snow Clearance Team has been linked.", "View Linked Snow Clearance Team", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_Core_Contractor_Edit fChildForm = new frm_Core_Contractor_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intSiteGrittingContractID + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void OriginalSubContractorNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                string strRecordIDs = "";
                foreach (DataRow dr in this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.Rows)
                {
                    strRecordIDs += dr["SnowClearanceSiteContractID"].ToString() + ",";
                }
                frm_GC_Snow_Choose_Team fChildForm = new frm_GC_Snow_Choose_Team();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInSiteSnowClearanceContractIDs = strRecordIDs;
                fChildForm.strFormMode = strFormMode;
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = intRecordCount;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["OriginalSubContractorID"] = fChildForm.intSubContractorID;
                        currentRow["OriginalSubContractorName"] = fChildForm.strSelectedTeamName;
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Button //
            {
                DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intSiteGrittingContractID = (string.IsNullOrEmpty(currentRow["OriginalSubContractorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["OriginalSubContractorID"]));
                    if (intSiteGrittingContractID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Original Snow Clearance Team - no Original Snow Clearance Team has been linked.", "View Linked Original Snow Clearance Team", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_Core_Contractor_Edit fChildForm = new frm_Core_Contractor_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intSiteGrittingContractID + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void ClientPOIDDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                if (currentRow == null) return;
                int intSiteID = (string.IsNullOrEmpty(currentRow["SiteID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SiteID"]));
                int intPOID = (string.IsNullOrEmpty(currentRow["ClientPOID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientPOID"]));
                frm_GC_Select_Client_PO fChildForm = new frm_GC_Select_Client_PO();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intIncludeNonActive = 1;
                fChildForm.intPassedInSiteID = intSiteID;
                fChildForm.intOriginalClientPOID = intPOID;
                fChildForm.strPassedInSubJoin = (strFormMode != "blockedit" ? "IN" : "BLOCKEDIT");
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["ClientPOID"] = fChildForm.intSelectedID;
                    currentRow["ClientPOIDDescription"] = fChildForm.strSelectedValue;
                    this.sp04163GCSnowCalloutEditBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Button //
            {
                DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intClientPO = (string.IsNullOrEmpty(currentRow["ClientPOID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientPOID"]));
                    if (intClientPO <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display linked Client Purchase Order - no Client Purchase Order has been linked.", "View Linked Client Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    frm_GC_Client_PO_Edit fChildForm = new frm_GC_Client_PO_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intClientPO + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void JobStatusIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("JobStatusID");
        }
        private void JobStatusIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(JobStatusIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(JobStatusIDGridLookUpEdit, "");
            }
        }
        private void JobStatusIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                if ((this.strFormMode == "add" || this.strFormMode == "edit"))
                {
                    int intSiteGrittingContractID = 0;
                    if (!string.IsNullOrEmpty(currentRow["SnowClearanceSiteContractID"].ToString())) intSiteGrittingContractID = Convert.ToInt32(currentRow["SnowClearanceSiteContractID"]);

                    int intSubContractorID = 0;
                    if (!string.IsNullOrEmpty(currentRow["SubContractorID"].ToString())) intSubContractorID = Convert.ToInt32(currentRow["SubContractorID"]);

                    if (intSiteGrittingContractID <= 0 || intSubContractorID <= 0)
                    {
                        GridLookUpEdit glue = (GridLookUpEdit)sender;
                        GridView view = glue.Properties.View;
                        view.BeginUpdate();
                        view.ActiveFilter.Clear();
                        view.ActiveFilter.NonColumnFilter = "[CalloutStatusID] <= 10";
                        view.MakeRowVisible(-1, true);
                        view.EndUpdate();
                    }
                    else
                    {
                        GridLookUpEdit glue = (GridLookUpEdit)sender;
                        GridView view = glue.Properties.View;
                        view.BeginUpdate();
                        view.ActiveFilter.Clear();
                        //view.ActiveFilter.NonColumnFilter = "[CalloutStatusID] <= 10 or [CalloutStatusID] = 50  or [CalloutStatusID] >= 70";
                        view.ActiveFilter.NonColumnFilter = "[CalloutStatusID] <= 10 or [CalloutStatusID] >= 50";
                        view.MakeRowVisible(-1, true);
                        view.EndUpdate();
                    }
                }
            }
        }
        private void JobStatusIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                if ((this.strFormMode == "add" || this.strFormMode == "edit"))
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    GridView view = glue.Properties.View;
                    view.BeginUpdate();
                    view.ActiveFilter.Clear();
                    view.EndUpdate();
                }
            }
        }

        private void StartTimeDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void StartTimeDateEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Time_On_Site();
        }

        private void CompletedTimeDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CompletedTimeDateEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Time_On_Site();
        }

        private void NonStandardCostCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("NonStandardCost");
        }
        private void NonStandardCostCheckEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_FormStillLoading) Calculate_Total_Costs(0, "", (decimal)0.00);
        }
        
        private void NonStandardSellCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("NonStandardSell");
        }
        private void NonStandardSellCheckEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_FormStillLoading) Calculate_Total_Costs(0, "", (decimal)0.00);

        }

        private void NoAccessCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("NoAccessAbortedRate");
            if (!ibool_FormStillLoading) Calculate_Total_Costs(0, "", (decimal)0.00);
        }

        private void VisitAbortedCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("NoAccessAbortedRate");
            if (!ibool_FormStillLoading) Calculate_Total_Costs(0, "", (decimal)0.00);
        }

        private void NoAccessAbortedRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_FormStillLoading) Calculate_Total_Costs(0, "", (decimal)0.00);
        }

        private void LabourCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void LabourCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs(0, "", (decimal)0.00);
        }

        private void LabourSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void LabourSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Total_Costs(0, "", (decimal)0.00);
        }


        #endregion


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            string message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Callout Linked Rates NOT Shown When Block Adding and Block Editing" : "No Callout Linked Rates Available - Select a Team to see Linked Rates");
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.Color.Gray;
        }

        private void FilterGrid()
        {
            string strSnowClearanceCalloutID = "";
            DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                strSnowClearanceCalloutID = (currentRow["SnowClearanceCallOutID"] == null ? "0" : currentRow["SnowClearanceCallOutID"].ToString());
            }

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[SnowClearanceCallOutID] = " + Convert.ToString(strSnowClearanceCalloutID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void repositoryItemSpinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            /*decimal decValue = (decimal)0.00;
            SpinEdit se = (SpinEdit)sender;
            decimal decRate = Convert.ToDecimal(view.GetFocusedRowCellValue("Rate"));
            decimal decHours = Convert.ToDecimal(se.EditValue);
            decValue = decRate * decHours;
            view.SetFocusedRowCellValue("TotalCost", decValue);
            */
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }
        private void repositoryItemSpinEdit1_ValueChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!view.FocusedColumn.FieldName.StartsWith("Hours")) return;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Grid_Row_Totals(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);  // This is also done later for rows after this one //
            Calculate_Total_Costs(view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
        }

        private void repositoryItemMemoExEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //

            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemSpinEditMachineCount_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }
        private void repositoryItemSpinEditMachineCount_Validating(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!view.FocusedColumn.FieldName.StartsWith("MachineCount")) return;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Grid_Row_Totals(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);  // This is also done later for rows after this one //
            Calculate_Total_Costs(view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
        }


        private void Calculate_Grid_Row_Totals(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decTotal = (decimal)0.00;
            decimal decHoursWorked1 = (strCurrentColumn == "Hours" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Hours")));
            decimal decHoursWorked2 = (strCurrentColumn == "Hours2" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Hours2")));
            decimal decHoursWorked3 = (strCurrentColumn == "Hours3" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Hours3")));
            decimal decHoursWorked4 = (strCurrentColumn == "Hours4" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Hours4")));
            decimal decMinimumHours1 = (strCurrentColumn == "MinimumHours" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MinimumHours")));
            decimal decMinimumHours2 = (strCurrentColumn == "MinimumHours2" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MinimumHours2")));
            decimal decMinimumHours3 = (strCurrentColumn == "MinimumHours3" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MinimumHours3")));
            decimal decMinimumHours4 = (strCurrentColumn == "MinimumHours4" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MinimumHours4")));
            decimal decHours1 = (decHoursWorked1 > decMinimumHours1 ? decHoursWorked1 : decMinimumHours1);
            decimal decHours2 = (decHoursWorked2 > decMinimumHours2 ? decHoursWorked2 : decMinimumHours2);
            decimal decHours3 = (decHoursWorked3 > decMinimumHours3 ? decHoursWorked3 : decMinimumHours3);
            decimal decHours4 = (decHoursWorked4 > decMinimumHours4 ? decHoursWorked4 : decMinimumHours4);

            decTotal = ((strCurrentColumn == "MachineCount1" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MachineCount1"))) * (strCurrentColumn == "Rate" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Rate"))) * decHours1) +
                        ((strCurrentColumn == "MachineCount2" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MachineCount2"))) * (strCurrentColumn == "MachineRate2" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MachineRate2"))) * decHours2) +
                        ((strCurrentColumn == "MachineCount3" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MachineCount3"))) * (strCurrentColumn == "MachineRate3" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MachineRate3"))) * decHours3) +
                        ((strCurrentColumn == "MachineCount4" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MachineCount4"))) * (strCurrentColumn == "MachineRate4" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "MachineRate4"))) * decHours4);
            view.SetRowCellValue(intRow, "TotalCost", decTotal);
        }

        #endregion


        private void Set_Control_Readonly_Status(string strCheckWhat)
        {
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "JobStatusID")
            {
                int intJobStatusID = 0;
                if (!string.IsNullOrEmpty(JobStatusIDGridLookUpEdit.EditValue.ToString())) intJobStatusID = Convert.ToInt32(JobStatusIDGridLookUpEdit.EditValue);
                if ((intJobStatusID < 80 || intJobStatusID == 90) && (strFormMode == "add" || strFormMode == "edit"))
                {
                    SiteNameButtonEdit.Properties.Buttons[0].Enabled = true;  // Enable Site Selection //
                    SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;  // Enable Team Selection //
                }
                else
                {
                    SiteNameButtonEdit.Properties.Buttons[0].Enabled = false;  // Disable Site Selection //
                    SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = false;   // Disable Team Selection //
                }
            }

            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardCost")
            {
                LabourCostSpinEdit.Properties.ReadOnly = !NonStandardCostCheckEdit.Checked;
            }
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardSell")
            {
                LabourSellSpinEdit.Properties.ReadOnly = !NonStandardSellCheckEdit.Checked;
            }

            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardCost")
            {
                LabourCostSpinEdit.Properties.ReadOnly = !NonStandardCostCheckEdit.Checked;
            }

            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NonStandardSell")
            {
                LabourSellSpinEdit.Properties.ReadOnly = !NonStandardSellCheckEdit.Checked;
            }

            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "NoAccessAbortedRate")
            {
                if (string.IsNullOrEmpty(NoAccessCheckEdit.EditValue.ToString()) || string.IsNullOrEmpty(VisitAbortedCheckEdit.EditValue.ToString())) return;
                if (Convert.ToInt32(NoAccessCheckEdit.EditValue) == 1 || Convert.ToInt32(VisitAbortedCheckEdit.EditValue) == 1)
                {
                    NoAccessAbortedRateSpinEdit.Properties.ReadOnly = false;
                }
                else
                {
                    NoAccessAbortedRateSpinEdit.Properties.ReadOnly = true;
                    NoAccessAbortedRateSpinEdit.EditValue = (decimal)0.00;
                }
            }
        }

        private void Calculate_Total_Costs(int intRow, string strCurrentColumn, decimal decValue)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;  // Not applicable to these modes //
            
            // NO VAT CALCULATED //
            DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                // Calculate Other Costs //
                decimal decOtherCost = (decimal)0.00;
                decimal decOtherSell = (decimal)0.00;
                int intSnowClearanceCallOutID = 0;
                if (!string.IsNullOrEmpty(currentRow["OtherCost"].ToString())) decOtherCost = Convert.ToDecimal(currentRow["OtherCost"]);
                if (!string.IsNullOrEmpty(currentRow["OtherSell"].ToString())) decOtherSell = Convert.ToDecimal(currentRow["OtherSell"]);
                if (!string.IsNullOrEmpty(currentRow["SnowClearanceCallOutID"].ToString())) intSnowClearanceCallOutID = Convert.ToInt32(currentRow["SnowClearanceCallOutID"]);
                if (intSnowClearanceCallOutID > 0)  // both blank and record has been saved [SnowClearanceCallOutID > 0] so attempt to calculate these //
                {
                    SqlDataAdapter sdaRates = new SqlDataAdapter();
                    DataSet dsRates = new DataSet("NewDataSet");
                    try
                    {
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp04167_GC_Snow_Callout_Get_Total_Other_Costs", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutID", intSnowClearanceCallOutID));
                        sdaRates = new SqlDataAdapter(cmd);
                        sdaRates.Fill(dsRates, "Table");
                        SQlConn.Close();
                        SQlConn.Dispose();
                        if (dsRates.Tables[0].Rows.Count == 1)
                        {
                            DataRow dr = dsRates.Tables[0].Rows[0];
                            decOtherCost = Convert.ToDecimal(dr["ExtraCostTotal"]);
                            decOtherSell = Convert.ToDecimal(dr["ExtraSellTotal"]);
                            currentRow["OtherCost"] = decOtherCost;
                            currentRow["OtherSell"] = decOtherSell;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }

                // Calculate Labour Cost //              
                decimal decTotalLabourCost = (decimal)0.00;
                decimal decLabourVatRate = (decimal)0.00;
                decimal decTotalHours = (decimal)0.00;
                if (!string.IsNullOrEmpty(currentRow["LabourVatRate"].ToString())) decLabourVatRate = Convert.ToDecimal(currentRow["LabourVatRate"]);
                GridView view = (GridView)gridControl1.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {                      
                    if (Convert.ToInt32(view.GetRowCellValue(i, "SnowClearanceCallOutID")) == Convert.ToInt32(currentRow["SnowClearanceCallOutID"]) && Convert.ToDecimal(view.GetRowCellValue(i, "TotalCost")) > (decimal)0.00)
                    {
                        decTotalLabourCost += Convert.ToDecimal(view.GetRowCellValue(i, "TotalCost")); // * (1 + (decLabourVatRate / 100)));

                        decTotalHours += ((strCurrentColumn == "Hours" && i == intRow ? decValue : Convert.ToDecimal(view.GetRowCellValue(i, "Hours"))) * (strCurrentColumn == "MachineCount1" && i == intRow ? decValue : Convert.ToDecimal(view.GetRowCellValue(i, "MachineCount1")))) +
                                        ((strCurrentColumn == "Hours2" && i == intRow ? decValue : Convert.ToDecimal(view.GetRowCellValue(i, "Hours2"))) * (strCurrentColumn == "MachineCount2" && i == intRow ? decValue : Convert.ToDecimal(view.GetRowCellValue(i, "MachineCount2")))) +
                                        ((strCurrentColumn == "Hours3" && i == intRow ? decValue : Convert.ToDecimal(view.GetRowCellValue(i, "Hours3"))) * (strCurrentColumn == "MachineCount3" && i == intRow ? decValue : Convert.ToDecimal(view.GetRowCellValue(i, "MachineCount3")))) +
                                        ((strCurrentColumn == "Hours4" && i == intRow ? decValue : Convert.ToDecimal(view.GetRowCellValue(i, "Hours4"))) * (strCurrentColumn == "MachineCount4" && i == intRow ? decValue : Convert.ToDecimal(view.GetRowCellValue(i, "MachineCount4"))));
                        
                        
                        //decTotalHours += (Convert.ToDecimal(view.GetRowCellValue(i, "Hours")) * Convert.ToDecimal(view.GetRowCellValue(i, "MachineCount1"))) +
                        //    (Convert.ToDecimal(view.GetRowCellValue(i, "Hours2")) * Convert.ToDecimal(view.GetRowCellValue(i, "MachineCount2"))) +
                        //    (Convert.ToDecimal(view.GetRowCellValue(i, "Hours3")) * Convert.ToDecimal(view.GetRowCellValue(i, "MachineCount3"))) +
                        //    (Convert.ToDecimal(view.GetRowCellValue(i, "Hours4")) * Convert.ToDecimal(view.GetRowCellValue(i, "MachineCount4")));
                    }
                }

                if (Convert.ToInt32(currentRow["NonStandardCost"]) == 0)
                {
                    // Check if we need to override calculated Labour Cost with Aborted \ No Access Rate \\
                    if (Convert.ToInt32(currentRow["NoAccess"]) == 1 || Convert.ToInt32(currentRow["VisitAborted"]) == 1)
                    {
                        currentRow["LabourCost"] = Convert.ToDecimal(currentRow["NoAccessAbortedRate"]);
                    }
                    else
                    {
                        currentRow["LabourCost"] = decTotalLabourCost;
                    }
                }
                //currentRow["HoursWorked"] = decTotalHours;

                int intChargeMethod = Convert.ToInt32(currentRow["ChargeMethodID"]);
                decimal decChargeFixedPrice = Convert.ToDecimal(currentRow["ChargeFixedPrice"]);
                decimal decChargeFixedNumberOfHours = Convert.ToDecimal(currentRow["ChargeFixedNumberOfHours"]);
                decimal decChargeFixedHourlyRate = Convert.ToDecimal(currentRow["ChargeFixedHourlyRate"]);
                decimal decChargeExtraHourlyRate = Convert.ToDecimal(currentRow["ChargeExtraHourlyRate"]);
                decimal decChargeMarkupPercentage = Convert.ToDecimal(currentRow["ChargeMarkupPercentage"]);
                decimal decTotalLabourSell = (decimal)0.00;
                if (Convert.ToInt32(currentRow["NonStandardSell"]) == 0)
                {
                    if (intChargeMethod > 0)
                    {
                        switch (intChargeMethod)
                        {
                            case 1:  // Fixed Rate //
                                decTotalLabourSell = decChargeFixedPrice;
                                break;
                            case 2:  // Client Fixed Hours //
                                decTotalLabourSell = decChargeFixedNumberOfHours * decChargeFixedHourlyRate;
                                if (decChargeExtraHourlyRate > (decimal)0.00)  // See if we can charge for extra hours over fixed hours amount  - if yes then add it to total labour cost //
                                {
                                    if (decTotalHours > decChargeFixedNumberOfHours)
                                    {
                                        decTotalLabourSell += (decTotalHours - decChargeFixedNumberOfHours) * decChargeExtraHourlyRate;
                                    }
                                }
                                break;
                            case 3:  // Client Price Per Hour //
                                decTotalLabourSell = decTotalHours * decChargeFixedHourlyRate;
                                break;
                            case 4:  // Client Markup Percentage on Costs //
                                decTotalLabourSell = decTotalLabourCost * (1 + (decChargeMarkupPercentage / 100));
                                break;
                            default:
                                break;
                        }
                        currentRow["LabourSell"] = decTotalLabourSell;
                    }
                }
                else
                {
                    decTotalLabourSell = Convert.ToDecimal(currentRow["LabourSell"]);
                }
                currentRow["TotalCost"] = Convert.ToDecimal(currentRow["LabourCost"]) + decOtherCost;
                currentRow["TotalSell"] = decTotalLabourSell + decOtherSell;
                this.sp04163GCSnowCalloutEditBindingSource.EndEdit();
                Set_Grit_Warning_Label();
            }
        }

        private void Open_Select_Team_Screen()
        {
            string strRecordIDs = "";
            foreach (DataRow dr in this.dataSet_GC_Snow_DataEntry.sp04163_GC_Snow_Callout_Edit.Rows)
            {
                strRecordIDs += dr["SnowClearanceSiteContractID"].ToString() + ",";
            }
            frm_GC_Snow_Choose_Team fChildForm = new frm_GC_Snow_Choose_Team();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInSiteSnowClearanceContractIDs = strRecordIDs;
            fChildForm.strFormMode = strFormMode;
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = intRecordCount;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
            {
                DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intSnowClearanceCallOutID = 0;
                    if (! string.IsNullOrEmpty(currentRow["SnowClearanceCallOutID"].ToString())) intSnowClearanceCallOutID = Convert.ToInt32(currentRow["SnowClearanceCallOutID"]);
                    currentRow["SubContractorID"] = fChildForm.intSubContractorID;
                    currentRow["SubContractorName"] = fChildForm.strSelectedTeamName;
                    currentRow["LabourVatRate"] = (fChildForm.intIsVatRegistered == 1 ? decVatRate : (decimal)0.00);
                    if (Convert.ToDecimal(currentRow["HoursWorked"]) <= (decimal) 0.00) currentRow["HoursWorked"] = fChildForm.decDefaultHours;
                    
                    if (fChildForm.intSubContractorID <= 0) currentRow["JobStatusID"] = 10;  // Started //
                    this.sp04163GCSnowCalloutEditBindingSource.EndEdit();

                    // Remove any Rate Bands in case they are different...//
                    GridView view = (GridView)gridControl1.MainView;
                    view.BeginUpdate();
                    if (strFormMode == "add")
                    {
                        for (int i = view.DataRowCount - 1; i >= 0 ; i--)
                        {
                            view.DeleteRow(i);
                        }
                    }
                    else if (strFormMode == "edit")
                    {
                        for (int i = view.DataRowCount - 1; i >= 0; i--)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(i, "SnowClearanceCallOutID")) == Convert.ToInt32(currentRow["SnowClearanceCallOutID"]))
                            {
                                view.DeleteRow(i);
                            }
                        }
                    }
                    // Now re-add them... //
                    SqlDataAdapter sdaLinkedRates = new SqlDataAdapter();
                    DataSet dsLinkedRates = new DataSet("NewDataSet");
                    SqlConnection SQlConn = new SqlConnection(strConnectionString);
                    SqlCommand cmd = null;
                    cmd = new SqlCommand("sp04156_GC_Snow_Clearance_Team_Rates", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strRecordIDs", fChildForm.intSubContractorID.ToString() + ","));
                    sdaLinkedRates = new SqlDataAdapter(cmd);
                    try
                    {
                        sdaLinkedRates.Fill(dsLinkedRates, "Table");
                    }
                    catch (Exception ex)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Linked Rates.\n\nMessage = [" + ex.Message + "].\n\nPlease try selecting the Team again. If the problem persists, contact Technical Support.", "Load Linked Rates", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    SQlConn.Close();
                    SQlConn.Dispose();
                    foreach (DataRow dr in dsLinkedRates.Tables[0].Rows)
                    {
                        DataRow drNewRow = dataSet_GC_Snow_Core.sp04162_GC_Snow_Clearance_Callout_Linked_Rates.NewRow();
                        drNewRow["SnowClearanceCallOutID"] = intSnowClearanceCallOutID;
                        drNewRow["DayTypeID"] = dr["DayTypeID"];
                        drNewRow["DayTypeDescription"] = dr["DayTypeDescription"];
                        drNewRow["StartDateTime"] = dr["StartTime"];
                        drNewRow["EndDateTime"] = dr["EndTime"];
                        drNewRow["Hours"] = (decimal)0.00;
                        drNewRow["Rate"] = dr["Rate"];
                        drNewRow["TotalCost"] = (decimal)0.00;
                        drNewRow["Remarks"] = dr["Remarks"];
                        drNewRow["MinimumHours"] = dr["MinimumHours"];
                        drNewRow["MachineRate2"] = dr["MachineRate2"];
                        drNewRow["MachineRate3"] = dr["MachineRate3"];
                        drNewRow["MachineRate4"] = dr["MachineRate4"];
                        drNewRow["MachineCount1"] = 0;
                        drNewRow["MachineCount2"] = 0;
                        drNewRow["MachineCount3"] = 0;
                        drNewRow["MachineCount4"] = 0;
                        dataSet_GC_Snow_Core.sp04162_GC_Snow_Clearance_Callout_Linked_Rates.Rows.Add(drNewRow);
                    }
                    view.EndUpdate();
                }
                Calculate_Total_Costs(0, "", (decimal)0.00);
            }
        }

        private void Calculate_Time_On_Site()
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;  // Not applicable to these modes //

            DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
 
            if (currentRow != null)
            {
                decimal decHoursElapsed = (decimal)0.00;
                DateTime dtStart;
                if (currentRow["StartTime"].ToString() == "01/01/0001 00:00:00" || currentRow["StartTime"].ToString() == "")
                {
                    //currentRow["HoursWorked"] = decHoursElapsed;
                    this.sp04163GCSnowCalloutEditBindingSource.EndEdit();
                    Calculate_Total_Costs(0, "", (decimal)0.00);
                    return;
                }
                dtStart = Convert.ToDateTime(currentRow["StartTime"]);
                DateTime dtEnd;
                if (currentRow["CompletedTime"].ToString() == "01/01/0001 00:00:00" || currentRow["CompletedTime"].ToString() == "")
                {
                    //currentRow["HoursWorked"] = decHoursElapsed;
                    this.sp04163GCSnowCalloutEditBindingSource.EndEdit();
                    Calculate_Total_Costs(0, "", (decimal)0.00);
                    return;
                }
                dtEnd = Convert.ToDateTime(currentRow["CompletedTime"]);
                TimeSpan elapsedTime = dtEnd.Subtract(dtStart);
                int hours = (int)Math.Floor(elapsedTime.TotalHours), minutes = elapsedTime.Minutes;
                TimeSpan ts2 = new TimeSpan(hours, minutes, 0);
                decHoursElapsed = (decimal)ts2.TotalMinutes / 60;
                currentRow["HoursWorked"] = decHoursElapsed;
                this.sp04163GCSnowCalloutEditBindingSource.EndEdit();

                Calculate_Total_Costs(0, "", (decimal)0.00);
            }





        }

        private void Set_Grit_Warning_Label()
        {
            //  Set warning message if Hours Don't Match //
            if (strFormMode == "blockedit")
            {
                ItemForWarningLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                return;
            }
            DataRowView currentRow = (DataRowView)sp04163GCSnowCalloutEditBindingSource.Current;
            if (currentRow != null)
            {
                decimal decTotalHours = (decimal)0.00;
                GridView view = (GridView)gridControl1.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "SnowClearanceCallOutID")) == Convert.ToInt32(currentRow["SnowClearanceCallOutID"]) && Convert.ToDecimal(view.GetRowCellValue(i, "TotalCost")) > (decimal)0.00)
                    {
                        decTotalHours += Convert.ToDecimal(view.GetRowCellValue(i, "Hours"));
                    }
                }
                ItemForWarningLabel.Visibility = (Convert.ToDecimal(currentRow["HoursWorked"]) != decTotalHours ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
            }
        }

        private string Get_Access_Considerations(int intSnowClearanceSiteContract)
        {
            string strAccess = "";
            DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter GetValue = new DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter();
            GetValue.ChangeConnectionString(strConnectionString);
            try
            {
                strAccess = GetValue.sp04173_GC_Snow_Callout_Get_Site_Considerations(intSnowClearanceSiteContract).ToString();
            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while retrieving Site Access Considerations.\n\nException:" + Ex.Message, "Get Site Access Considerations", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return "";
            }

            return strAccess;
        }




 

 

    }
}


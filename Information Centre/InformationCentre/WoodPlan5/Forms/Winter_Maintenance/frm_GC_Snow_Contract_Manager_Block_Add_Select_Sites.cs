﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Snow_Contract_Manager_Block_Add_Select_Sites : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string i_str_selected_SiteIDs = "";
        public string i_str_selected_Site_Names = "";
        public int i_int_selected_count = 0;

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_GC_Snow_Contract_Manager_Block_Add_Select_Sites()
        {
            InitializeComponent();
        }

        private void frm_GC_Snow_Contract_Manager_Block_Add_Select_Sites_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 400032;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp04143_GC_Site_Filter_DropDown_Just_Snow_Clear_SitesTableAdapter.Connection.ConnectionString = strConnectionString;
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            LoadData();
            gridControl1.ForceInitialize();

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();

            sp04143_GC_Site_Filter_DropDown_Just_Snow_Clear_SitesTableAdapter.Fill(this.dataSet_GC_Snow_Core.sp04143_GC_Site_Filter_DropDown_Just_Snow_Clear_Sites, "");
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Sites Available");
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void Get_Selected_Sites()
        {
            i_str_selected_SiteIDs = "";    // Reset any prior values first //
            i_str_selected_Site_Names = "";  // Reset any prior values first //
            i_int_selected_count = 0;  // Reset any prior values first //
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_SiteIDs = "";
                return;
            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_SiteIDs = "";
                return;
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_SiteIDs += Convert.ToString(view.GetRowCellValue(i, "SiteID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Site_Names = Convert.ToString(view.GetRowCellValue(i, "SiteName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Site_Names += ", " + Convert.ToString(view.GetRowCellValue(i, "SiteName"));
                        }
                        intCount++;
                    }
                }
                i_int_selected_count = selection1.SelectedCount;
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Get_Selected_Sites();
            if (string.IsNullOrEmpty(i_str_selected_SiteIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites by ticking them before proceeding.", "Select Sites", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void SpecialSelectBtn_Click(object sender, EventArgs e)
        {
            WaitDialogForm loading = new WaitDialogForm("Locating Sites, Please Wait...", "Get Sites Without Active Contract");
            loading.Show();

            DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter GetMatchingRecords = new DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter();
            GetMatchingRecords.ChangeConnectionString(strConnectionString);
            string strMatchingRecords = "";
            try
            {
                strMatchingRecords = GetMatchingRecords.sp04144_GC_Snow_Clearance_Get_Sites_Without_Active_Contract().ToString();
            }
            catch (Exception Ex)
            {
                loading.Close();
                DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while locating Sites without an Active Snow clearance Contract. Please try again.\n\nException:" + Ex.Message, "Get Sites Without Active Snow Clearance Contract", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            selection1.ClearSelection();  // Clear any selected records first //

            int intFoundRow = 0;
            int intCount = 0;
            if (!string.IsNullOrEmpty(strMatchingRecords))
            {
                char[] delimiters = new char[] { ';' };
                Array arrayItems = strMatchingRecords.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                GridView viewFilter = (GridView)gridControl1.MainView;
                viewFilter.BeginUpdate();
                intFoundRow = 0;
                foreach (string strElement in arrayItems)
                {
                    if (strElement == "") break;
                    intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["SiteID"], Convert.ToInt32(strElement.Trim()));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        viewFilter.MakeRowVisible(intFoundRow, false);
                        intCount++;
                    }
                }
                viewFilter.EndUpdate();
            }
            loading.Close();
            DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Site(s) Found and Ticked.", "Get Sites Without Active Snow Clearance Contract", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }






    }
}

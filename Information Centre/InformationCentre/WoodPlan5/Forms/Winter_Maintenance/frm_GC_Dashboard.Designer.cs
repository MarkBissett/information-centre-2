﻿namespace WoodPlan5
{
    partial class frm_GC_Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState1 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState2 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState3 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState4 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState5 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState6 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState1 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState2 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState3 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState4 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState5 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState6 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState7 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState8 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState9 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState10 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState11 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState12 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState13 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState14 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState15 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState16 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            this.sp04285GCGrittingDashboardBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gaugeControlJobCompletion = new DevExpress.XtraGauges.Win.GaugeControl();
            this.popupContainerControlDates = new DevExpress.XtraEditors.PopupContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dateEditToDateSeason = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDateSeason = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDateLastNight = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDateLastNight = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDateReactiveJobs = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDateReactiveJobs = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDateJobAcceptance = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDateJobAcceptance = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnDateFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditRecalculateOnTimer = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerControlCompanies = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCompanyFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04237GCCompanyFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.circularGauge1 = new DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge();
            this.arcScaleBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleComponentCompletion = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleBackgroundLayerComponent2 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleStateIndicatorComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleStateIndicatorComponent();
            this.labelComponent1 = new DevExpress.XtraGauges.Win.Base.LabelComponent();
            this.labelComponent2 = new DevExpress.XtraGauges.Win.Base.LabelComponent();
            this.arcScaleNeedleComponentCompletion = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleNeedleComponentCompletionNo = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleComponentCompletionNo = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleComponentCompletionTotal = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleSpindleCapComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent();
            this.digitalGaugeCompletion = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.sp04285_GC_Gritting_DashboardTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04285_GC_Gritting_DashboardTableAdapter();
            this.gaugeControlJobAcceptance = new DevExpress.XtraGauges.Win.GaugeControl();
            this.circularGauge2 = new DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge();
            this.arcScaleBackgroundLayerComponent3 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleComponentAccepted = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleBackgroundLayerComponent4 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleStateIndicatorComponent2 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleStateIndicatorComponent();
            this.labelComponent3 = new DevExpress.XtraGauges.Win.Base.LabelComponent();
            this.labelComponent4 = new DevExpress.XtraGauges.Win.Base.LabelComponent();
            this.arcScaleNeedleComponentAccepted = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleNeedleComponentAcceptedNo = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleComponentAcceptedNo = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleComponentAcceptedTotal = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleSpindleCapComponent2 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent();
            this.digitalGaugeAcceptance = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent2 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.gaugeControl5 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.gaugeControl7 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.digitalGaugeSeason = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent3 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemCompanyFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditCompanyFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiReload = new DevExpress.XtraBars.BarButtonItem();
            this.gaugeControl8 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.digitalGaugeYesterday = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent4 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl8 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl12 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gaugeControl2 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.digitalGaugeReactiveTotal = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent6 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.gaugeControl11 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.digitalGaugeReactiveRejected = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent7 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.splitContainerControl11 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gaugeControl12 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.digitalGaugeReactiveAccepted = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent8 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.gaugeControl13 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.digitalGaugeReactiveOutstanding = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent9 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl7 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge2 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent2 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl9 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gaugeControl4 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge3 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent3 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.gaugeControl3 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge4 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent4 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.splitContainerControl10 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gaugeControlCurrentTime = new DevExpress.XtraGauges.Win.GaugeControl();
            this.digitalGaugeCurrentTime = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent10 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.digitalGauge5 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent5 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.sp04237_GC_Company_Filter_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.pmGaugeStyle = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiStyleChooser = new DevExpress.XtraBars.BarButtonItem();
            this.bbiStyleManager = new DevExpress.XtraBars.BarButtonItem();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04285GCGrittingDashboardBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            this.gaugeControlJobCompletion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDates)).BeginInit();
            this.popupContainerControlDates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateSeason.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateSeason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateSeason.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateSeason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateLastNight.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateLastNight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateLastNight.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateLastNight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateReactiveJobs.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateReactiveJobs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateReactiveJobs.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateReactiveJobs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateJobAcceptance.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateJobAcceptance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateJobAcceptance.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateJobAcceptance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRecalculateOnTimer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).BeginInit();
            this.popupContainerControlCompanies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentCompletion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleStateIndicatorComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponentCompletion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponentCompletionNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentCompletionNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentCompletionTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleSpindleCapComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeCompletion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularGauge2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentAccepted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleStateIndicatorComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponentAccepted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponentAcceptedNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentAcceptedNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentAcceptedTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleSpindleCapComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeAcceptance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeSeason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeYesterday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).BeginInit();
            this.splitContainerControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl12)).BeginInit();
            this.splitContainerControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeReactiveTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeReactiveRejected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl11)).BeginInit();
            this.splitContainerControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeReactiveAccepted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeReactiveOutstanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).BeginInit();
            this.splitContainerControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl9)).BeginInit();
            this.splitContainerControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl10)).BeginInit();
            this.splitContainerControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeCurrentTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGauge5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmGaugeStyle)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1088, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 533);
            this.barDockControlBottom.Size = new System.Drawing.Size(1088, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 533);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1088, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 533);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiReload,
            this.barEditItemCompanyFilter,
            this.barEditItemDateFilter,
            this.bbiStyleChooser,
            this.bbiStyleManager});
            this.barManager1.MaxItemId = 33;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditCompanyFilter,
            this.repositoryItemTextEdit1,
            this.repositoryItemPopupContainerEditDateFilter});
            // 
            // sp04285GCGrittingDashboardBindingSource
            // 
            this.sp04285GCGrittingDashboardBindingSource.DataMember = "sp04285_GC_Gritting_Dashboard";
            this.sp04285GCGrittingDashboardBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gaugeControlJobCompletion
            // 
            this.gaugeControlJobCompletion.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControlJobCompletion.Controls.Add(this.popupContainerControlDates);
            this.gaugeControlJobCompletion.Controls.Add(this.popupContainerControlCompanies);
            this.gaugeControlJobCompletion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControlJobCompletion.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.circularGauge1,
            this.digitalGaugeCompletion});
            this.gaugeControlJobCompletion.Location = new System.Drawing.Point(0, 0);
            this.gaugeControlJobCompletion.Name = "gaugeControlJobCompletion";
            this.gaugeControlJobCompletion.Size = new System.Drawing.Size(509, 464);
            this.gaugeControlJobCompletion.TabIndex = 0;
            // 
            // popupContainerControlDates
            // 
            this.popupContainerControlDates.Controls.Add(this.layoutControl1);
            this.popupContainerControlDates.Controls.Add(this.btnDateFilterOK);
            this.popupContainerControlDates.Controls.Add(this.checkEditRecalculateOnTimer);
            this.popupContainerControlDates.Location = new System.Drawing.Point(13, 10);
            this.popupContainerControlDates.Name = "popupContainerControlDates";
            this.popupContainerControlDates.Size = new System.Drawing.Size(281, 449);
            this.popupContainerControlDates.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.dateEditToDateSeason);
            this.layoutControl1.Controls.Add(this.dateEditFromDateSeason);
            this.layoutControl1.Controls.Add(this.dateEditToDateLastNight);
            this.layoutControl1.Controls.Add(this.dateEditFromDateLastNight);
            this.layoutControl1.Controls.Add(this.dateEditToDateReactiveJobs);
            this.layoutControl1.Controls.Add(this.dateEditFromDateReactiveJobs);
            this.layoutControl1.Controls.Add(this.dateEditToDateJobAcceptance);
            this.layoutControl1.Controls.Add(this.dateEditFromDateJobAcceptance);
            this.layoutControl1.Location = new System.Drawing.Point(3, 3);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1324, 299, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(276, 420);
            this.layoutControl1.TabIndex = 21;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dateEditToDateSeason
            // 
            this.dateEditToDateSeason.EditValue = null;
            this.dateEditToDateSeason.Location = new System.Drawing.Point(101, 368);
            this.dateEditToDateSeason.MenuManager = this.barManager1;
            this.dateEditToDateSeason.Name = "dateEditToDateSeason";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDateSeason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, true)});
            this.dateEditToDateSeason.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDateSeason.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDateSeason.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDateSeason.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDateSeason.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDateSeason.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDateSeason.Properties.NullValuePrompt = "No Date";
            this.dateEditToDateSeason.Size = new System.Drawing.Size(157, 20);
            this.dateEditToDateSeason.StyleController = this.layoutControl1;
            this.dateEditToDateSeason.TabIndex = 15;
            this.dateEditToDateSeason.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDateSeason_ButtonClick);
            // 
            // dateEditFromDateSeason
            // 
            this.dateEditFromDateSeason.EditValue = null;
            this.dateEditFromDateSeason.Location = new System.Drawing.Point(101, 344);
            this.dateEditFromDateSeason.MenuManager = this.barManager1;
            this.dateEditFromDateSeason.Name = "dateEditFromDateSeason";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDateSeason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, true)});
            this.dateEditFromDateSeason.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDateSeason.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDateSeason.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDateSeason.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDateSeason.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDateSeason.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDateSeason.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDateSeason.Size = new System.Drawing.Size(157, 20);
            this.dateEditFromDateSeason.StyleController = this.layoutControl1;
            this.dateEditFromDateSeason.TabIndex = 22;
            this.dateEditFromDateSeason.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDateSeason_ButtonClick);
            // 
            // dateEditToDateLastNight
            // 
            this.dateEditToDateLastNight.EditValue = null;
            this.dateEditToDateLastNight.Location = new System.Drawing.Point(101, 266);
            this.dateEditToDateLastNight.MenuManager = this.barManager1;
            this.dateEditToDateLastNight.Name = "dateEditToDateLastNight";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Clear Date - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.dateEditToDateLastNight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, superToolTip3, true)});
            this.dateEditToDateLastNight.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDateLastNight.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDateLastNight.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDateLastNight.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDateLastNight.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDateLastNight.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDateLastNight.Properties.NullValuePrompt = "No Date";
            this.dateEditToDateLastNight.Size = new System.Drawing.Size(157, 20);
            this.dateEditToDateLastNight.StyleController = this.layoutControl1;
            this.dateEditToDateLastNight.TabIndex = 14;
            this.dateEditToDateLastNight.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDateLastNight_ButtonClick);
            // 
            // dateEditFromDateLastNight
            // 
            this.dateEditFromDateLastNight.EditValue = null;
            this.dateEditFromDateLastNight.Location = new System.Drawing.Point(101, 242);
            this.dateEditFromDateLastNight.MenuManager = this.barManager1;
            this.dateEditFromDateLastNight.Name = "dateEditFromDateLastNight";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Clear Date - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.dateEditFromDateLastNight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, superToolTip4, true)});
            this.dateEditFromDateLastNight.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDateLastNight.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDateLastNight.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDateLastNight.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDateLastNight.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDateLastNight.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDateLastNight.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDateLastNight.Size = new System.Drawing.Size(157, 20);
            this.dateEditFromDateLastNight.StyleController = this.layoutControl1;
            this.dateEditFromDateLastNight.TabIndex = 22;
            this.dateEditFromDateLastNight.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDateLastNight_ButtonClick);
            // 
            // dateEditToDateReactiveJobs
            // 
            this.dateEditToDateReactiveJobs.EditValue = null;
            this.dateEditToDateReactiveJobs.Location = new System.Drawing.Point(101, 164);
            this.dateEditToDateReactiveJobs.MenuManager = this.barManager1;
            this.dateEditToDateReactiveJobs.Name = "dateEditToDateReactiveJobs";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Clear Date - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.dateEditToDateReactiveJobs.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, superToolTip5, true)});
            this.dateEditToDateReactiveJobs.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDateReactiveJobs.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDateReactiveJobs.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDateReactiveJobs.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDateReactiveJobs.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDateReactiveJobs.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDateReactiveJobs.Properties.NullValuePrompt = "No Date";
            this.dateEditToDateReactiveJobs.Size = new System.Drawing.Size(157, 20);
            this.dateEditToDateReactiveJobs.StyleController = this.layoutControl1;
            this.dateEditToDateReactiveJobs.TabIndex = 13;
            this.dateEditToDateReactiveJobs.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDateReactiveJobs_ButtonClick);
            // 
            // dateEditFromDateReactiveJobs
            // 
            this.dateEditFromDateReactiveJobs.EditValue = null;
            this.dateEditFromDateReactiveJobs.Location = new System.Drawing.Point(101, 140);
            this.dateEditFromDateReactiveJobs.MenuManager = this.barManager1;
            this.dateEditFromDateReactiveJobs.Name = "dateEditFromDateReactiveJobs";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Clear Date - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.dateEditFromDateReactiveJobs.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, superToolTip6, true)});
            this.dateEditFromDateReactiveJobs.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDateReactiveJobs.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDateReactiveJobs.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDateReactiveJobs.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDateReactiveJobs.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDateReactiveJobs.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDateReactiveJobs.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDateReactiveJobs.Size = new System.Drawing.Size(157, 20);
            this.dateEditFromDateReactiveJobs.StyleController = this.layoutControl1;
            this.dateEditFromDateReactiveJobs.TabIndex = 21;
            this.dateEditFromDateReactiveJobs.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDateReactiveJobs_ButtonClick);
            // 
            // dateEditToDateJobAcceptance
            // 
            this.dateEditToDateJobAcceptance.EditValue = null;
            this.dateEditToDateJobAcceptance.Location = new System.Drawing.Point(101, 62);
            this.dateEditToDateJobAcceptance.MenuManager = this.barManager1;
            this.dateEditToDateJobAcceptance.Name = "dateEditToDateJobAcceptance";
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Clear Date - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.dateEditToDateJobAcceptance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", null, superToolTip7, true)});
            this.dateEditToDateJobAcceptance.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDateJobAcceptance.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDateJobAcceptance.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDateJobAcceptance.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDateJobAcceptance.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDateJobAcceptance.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDateJobAcceptance.Properties.NullValuePrompt = "No Date";
            this.dateEditToDateJobAcceptance.Size = new System.Drawing.Size(157, 20);
            this.dateEditToDateJobAcceptance.StyleController = this.layoutControl1;
            this.dateEditToDateJobAcceptance.TabIndex = 12;
            this.dateEditToDateJobAcceptance.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDateJobAcceptance_ButtonClick);
            // 
            // dateEditFromDateJobAcceptance
            // 
            this.dateEditFromDateJobAcceptance.EditValue = null;
            this.dateEditFromDateJobAcceptance.Location = new System.Drawing.Point(101, 38);
            this.dateEditFromDateJobAcceptance.MenuManager = this.barManager1;
            this.dateEditFromDateJobAcceptance.Name = "dateEditFromDateJobAcceptance";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Clear Date - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.dateEditFromDateJobAcceptance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", null, superToolTip8, true)});
            this.dateEditFromDateJobAcceptance.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDateJobAcceptance.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDateJobAcceptance.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDateJobAcceptance.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDateJobAcceptance.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDateJobAcceptance.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDateJobAcceptance.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDateJobAcceptance.Size = new System.Drawing.Size(157, 20);
            this.dateEditFromDateJobAcceptance.StyleController = this.layoutControl1;
            this.dateEditFromDateJobAcceptance.TabIndex = 20;
            this.dateEditFromDateJobAcceptance.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDateJobAcceptance_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.emptySpaceItem1,
            this.layoutControlGroup3,
            this.emptySpaceItem2,
            this.layoutControlGroup4,
            this.emptySpaceItem3,
            this.layoutControlGroup5,
            this.emptySpaceItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlGroup1.Size = new System.Drawing.Size(276, 420);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Job Acceptance";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(268, 92);
            this.layoutControlGroup2.Text = "Proactive Jobs - Acceptance and Completion";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDateJobAcceptance;
            this.layoutControlItem1.CustomizationFormText = "From Date\\Time:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem1.Text = "From Date\\Time:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDateJobAcceptance;
            this.layoutControlItem2.CustomizationFormText = "To Date\\Time:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem2.Text = "To Date\\Time:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(80, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 398);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(268, 14);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Job Completion";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 102);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(268, 92);
            this.layoutControlGroup3.Text = "Reactive Jobs - Rolling 24 Hours";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dateEditFromDateReactiveJobs;
            this.layoutControlItem3.CustomizationFormText = "From Date\\Time:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem3.Text = "From Date\\Time:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateEditToDateReactiveJobs;
            this.layoutControlItem4.CustomizationFormText = "To Date\\Time:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem4.Text = "To Date\\Time:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(80, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 92);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(268, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Last Night";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 204);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(268, 92);
            this.layoutControlGroup4.Text = "Last Night";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dateEditFromDateLastNight;
            this.layoutControlItem5.CustomizationFormText = "From Date\\Time:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem5.Text = "From Date\\Time:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dateEditToDateLastNight;
            this.layoutControlItem6.CustomizationFormText = "To Date\\Time:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem6.Text = "To Date\\Time:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(80, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 194);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(268, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Season";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 306);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(268, 92);
            this.layoutControlGroup5.Text = "Season";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.dateEditFromDateSeason;
            this.layoutControlItem7.CustomizationFormText = "From Date\\Time:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem7.Text = "From Date\\Time:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.dateEditToDateSeason;
            this.layoutControlItem8.CustomizationFormText = "To Date\\Time:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem8.Text = "To Date\\Time:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(80, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 296);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(268, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btnDateFilterOK
            // 
            this.btnDateFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateFilterOK.Location = new System.Drawing.Point(3, 424);
            this.btnDateFilterOK.Name = "btnDateFilterOK";
            this.btnDateFilterOK.Size = new System.Drawing.Size(77, 22);
            this.btnDateFilterOK.TabIndex = 19;
            this.btnDateFilterOK.Text = "OK";
            this.btnDateFilterOK.Click += new System.EventHandler(this.btnDateFilterOK_Click);
            // 
            // checkEditRecalculateOnTimer
            // 
            this.checkEditRecalculateOnTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditRecalculateOnTimer.EditValue = 1;
            this.checkEditRecalculateOnTimer.Location = new System.Drawing.Point(104, 426);
            this.checkEditRecalculateOnTimer.MenuManager = this.barManager1;
            this.checkEditRecalculateOnTimer.Name = "checkEditRecalculateOnTimer";
            this.checkEditRecalculateOnTimer.Properties.Caption = "(Recalculate on Timer)";
            this.checkEditRecalculateOnTimer.Properties.ValueChecked = 1;
            this.checkEditRecalculateOnTimer.Properties.ValueUnchecked = 0;
            this.checkEditRecalculateOnTimer.Size = new System.Drawing.Size(140, 19);
            this.checkEditRecalculateOnTimer.TabIndex = 23;
            // 
            // popupContainerControlCompanies
            // 
            this.popupContainerControlCompanies.Controls.Add(this.btnCompanyFilterOK);
            this.popupContainerControlCompanies.Controls.Add(this.gridControl5);
            this.popupContainerControlCompanies.Location = new System.Drawing.Point(300, 10);
            this.popupContainerControlCompanies.Name = "popupContainerControlCompanies";
            this.popupContainerControlCompanies.Size = new System.Drawing.Size(189, 163);
            this.popupContainerControlCompanies.TabIndex = 16;
            // 
            // btnCompanyFilterOK
            // 
            this.btnCompanyFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCompanyFilterOK.Location = new System.Drawing.Point(3, 138);
            this.btnCompanyFilterOK.Name = "btnCompanyFilterOK";
            this.btnCompanyFilterOK.Size = new System.Drawing.Size(77, 22);
            this.btnCompanyFilterOK.TabIndex = 18;
            this.btnCompanyFilterOK.Text = "OK";
            this.btnCompanyFilterOK.Click += new System.EventHandler(this.btnCompanyFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp04237GCCompanyFilterListBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(183, 133);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04237GCCompanyFilterListBindingSource
            // 
            this.sp04237GCCompanyFilterListBindingSource.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.colCompanyCode,
            this.colCompanyOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Company ID";
            this.gridColumn5.FieldName = "CompanyID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 80;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Company Name";
            this.gridColumn6.FieldName = "CompanyName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 152;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyOrder
            // 
            this.colCompanyOrder.Caption = "Order";
            this.colCompanyOrder.FieldName = "CompanyOrder";
            this.colCompanyOrder.Name = "colCompanyOrder";
            this.colCompanyOrder.OptionsColumn.AllowEdit = false;
            this.colCompanyOrder.OptionsColumn.AllowFocus = false;
            this.colCompanyOrder.OptionsColumn.ReadOnly = true;
            // 
            // circularGauge1
            // 
            this.circularGauge1.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent[] {
            this.arcScaleBackgroundLayerComponent1,
            this.arcScaleBackgroundLayerComponent2});
            this.circularGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 346, 346);
            this.circularGauge1.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleStateIndicatorComponent[] {
            this.arcScaleStateIndicatorComponent1});
            this.circularGauge1.Labels.AddRange(new DevExpress.XtraGauges.Win.Base.LabelComponent[] {
            this.labelComponent1,
            this.labelComponent2});
            this.circularGauge1.Name = "circularGauge1";
            this.circularGauge1.Needles.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent[] {
            this.arcScaleNeedleComponentCompletion,
            this.arcScaleNeedleComponentCompletionNo});
            this.circularGauge1.Scales.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent[] {
            this.arcScaleComponentCompletion,
            this.arcScaleComponentCompletionTotal,
            this.arcScaleComponentCompletionNo});
            this.circularGauge1.SpindleCaps.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent[] {
            this.arcScaleSpindleCapComponent1});
            // 
            // arcScaleBackgroundLayerComponent1
            // 
            this.arcScaleBackgroundLayerComponent1.ArcScale = this.arcScaleComponentCompletion;
            this.arcScaleBackgroundLayerComponent1.Name = "bg1";
            this.arcScaleBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularFull_Style4;
            this.arcScaleBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // arcScaleComponentCompletion
            // 
            this.arcScaleComponentCompletion.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletion.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletion.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletion.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletion.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 12F);
            this.arcScaleComponentCompletion.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponentCompletion.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.arcScaleComponentCompletion.EndAngle = 60F;
            this.arcScaleComponentCompletion.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponentCompletion.MajorTickmark.ShapeOffset = -12F;
            this.arcScaleComponentCompletion.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 0.8F);
            this.arcScaleComponentCompletion.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_2;
            this.arcScaleComponentCompletion.MajorTickmark.TextOffset = -27F;
            this.arcScaleComponentCompletion.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponentCompletion.MaxValue = 100F;
            this.arcScaleComponentCompletion.MinorTickCount = 4;
            this.arcScaleComponentCompletion.MinorTickmark.ShapeOffset = -5F;
            this.arcScaleComponentCompletion.MinorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 1F);
            this.arcScaleComponentCompletion.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_1;
            this.arcScaleComponentCompletion.Name = "scale1";
            this.arcScaleComponentCompletion.RadiusX = 122F;
            this.arcScaleComponentCompletion.RadiusY = 122F;
            this.arcScaleComponentCompletion.StartAngle = -240F;
            // 
            // arcScaleBackgroundLayerComponent2
            // 
            this.arcScaleBackgroundLayerComponent2.ArcScale = this.arcScaleComponentCompletion;
            this.arcScaleBackgroundLayerComponent2.Name = "bg2";
            this.arcScaleBackgroundLayerComponent2.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularFull_Style4_1;
            this.arcScaleBackgroundLayerComponent2.Size = new System.Drawing.SizeF(150F, 150F);
            this.arcScaleBackgroundLayerComponent2.ZOrder = 999;
            // 
            // arcScaleStateIndicatorComponent1
            // 
            this.arcScaleStateIndicatorComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 235F);
            this.arcScaleStateIndicatorComponent1.IndicatorScale = this.arcScaleComponentCompletion;
            this.arcScaleStateIndicatorComponent1.Name = "circularGauge1_Indicator1";
            this.arcScaleStateIndicatorComponent1.Size = new System.Drawing.SizeF(25F, 25F);
            scaleIndicatorState1.IntervalLength = 74F;
            scaleIndicatorState1.Name = "Default";
            scaleIndicatorState1.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            scaleIndicatorState2.IntervalLength = 24F;
            scaleIndicatorState2.Name = "State1";
            scaleIndicatorState2.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            scaleIndicatorState2.StartValue = 75F;
            scaleIndicatorState3.Name = "State2";
            scaleIndicatorState3.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            scaleIndicatorState3.StartValue = 100F;
            this.arcScaleStateIndicatorComponent1.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState1,
            scaleIndicatorState2,
            scaleIndicatorState3});
            this.arcScaleStateIndicatorComponent1.ZOrder = -100;
            // 
            // labelComponent1
            // 
            this.labelComponent1.AppearanceText.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labelComponent1.AppearanceText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.labelComponent1.Name = "circularGauge1_Label1";
            this.labelComponent1.Position = new DevExpress.XtraGauges.Core.Base.PointF2D(120F, 210F);
            this.labelComponent1.Size = new System.Drawing.SizeF(100F, 25F);
            this.labelComponent1.Text = "% Complete";
            this.labelComponent1.ZOrder = -1001;
            // 
            // labelComponent2
            // 
            this.labelComponent2.AppearanceText.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labelComponent2.AppearanceText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Lavender");
            this.labelComponent2.Name = "labelComponent1Copy0";
            this.labelComponent2.Position = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 185F);
            this.labelComponent2.Size = new System.Drawing.SizeF(100F, 25F);
            this.labelComponent2.Text = "Total";
            this.labelComponent2.ZOrder = -1001;
            // 
            // arcScaleNeedleComponentCompletion
            // 
            this.arcScaleNeedleComponentCompletion.ArcScale = this.arcScaleComponentCompletion;
            this.arcScaleNeedleComponentCompletion.EndOffset = 5F;
            this.arcScaleNeedleComponentCompletion.Name = "needle1";
            this.arcScaleNeedleComponentCompletion.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_Style4;
            this.arcScaleNeedleComponentCompletion.StartOffset = -21F;
            this.arcScaleNeedleComponentCompletion.ZOrder = -50;
            // 
            // arcScaleNeedleComponentCompletionNo
            // 
            this.arcScaleNeedleComponentCompletionNo.ArcScale = this.arcScaleComponentCompletionNo;
            this.arcScaleNeedleComponentCompletionNo.EndOffset = -5F;
            this.arcScaleNeedleComponentCompletionNo.Name = "circularGauge1_Needle2";
            this.arcScaleNeedleComponentCompletionNo.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_ClockSecond;
            this.arcScaleNeedleComponentCompletionNo.StartOffset = -4.4F;
            this.arcScaleNeedleComponentCompletionNo.ZOrder = -49;
            // 
            // arcScaleComponentCompletionNo
            // 
            this.arcScaleComponentCompletionNo.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletionNo.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletionNo.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletionNo.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletionNo.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 12F);
            this.arcScaleComponentCompletionNo.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponentCompletionNo.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.arcScaleComponentCompletionNo.EndAngle = 60F;
            this.arcScaleComponentCompletionNo.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponentCompletionNo.MajorTickmark.ShapeOffset = -12F;
            this.arcScaleComponentCompletionNo.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 0.8F);
            this.arcScaleComponentCompletionNo.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_2;
            this.arcScaleComponentCompletionNo.MajorTickmark.TextOffset = -27F;
            this.arcScaleComponentCompletionNo.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponentCompletionNo.MaxValue = 100F;
            this.arcScaleComponentCompletionNo.MinorTickCount = 4;
            this.arcScaleComponentCompletionNo.MinorTickmark.ShapeOffset = -5F;
            this.arcScaleComponentCompletionNo.MinorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 1F);
            this.arcScaleComponentCompletionNo.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_1;
            this.arcScaleComponentCompletionNo.Name = "scale1Copy0";
            this.arcScaleComponentCompletionNo.RadiusX = 122F;
            this.arcScaleComponentCompletionNo.RadiusY = 122F;
            this.arcScaleComponentCompletionNo.StartAngle = -240F;
            // 
            // arcScaleComponentCompletionTotal
            // 
            this.arcScaleComponentCompletionTotal.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletionTotal.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletionTotal.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletionTotal.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentCompletionTotal.AppearanceTickmarkText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.arcScaleComponentCompletionTotal.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:LightBlue");
            this.arcScaleComponentCompletionTotal.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.arcScaleComponentCompletionTotal.EndAngle = 60F;
            this.arcScaleComponentCompletionTotal.MajorTickCount = 8;
            this.arcScaleComponentCompletionTotal.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponentCompletionTotal.MajorTickmark.ShapeOffset = -6F;
            this.arcScaleComponentCompletionTotal.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_3;
            this.arcScaleComponentCompletionTotal.MajorTickmark.TextOffset = -20F;
            this.arcScaleComponentCompletionTotal.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponentCompletionTotal.MaxValue = 700F;
            this.arcScaleComponentCompletionTotal.MinorTickCount = 4;
            this.arcScaleComponentCompletionTotal.MinorTickmark.ShapeOffset = -1.5F;
            this.arcScaleComponentCompletionTotal.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_4;
            this.arcScaleComponentCompletionTotal.Name = "scale2";
            this.arcScaleComponentCompletionTotal.RadiusX = 70F;
            this.arcScaleComponentCompletionTotal.RadiusY = 70F;
            this.arcScaleComponentCompletionTotal.StartAngle = -240F;
            this.arcScaleComponentCompletionTotal.ZOrder = -1;
            // 
            // arcScaleSpindleCapComponent1
            // 
            this.arcScaleSpindleCapComponent1.ArcScale = this.arcScaleComponentCompletion;
            this.arcScaleSpindleCapComponent1.Name = "cap1";
            this.arcScaleSpindleCapComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.SpindleCapShapeType.CircularFull_Style4;
            this.arcScaleSpindleCapComponent1.Size = new System.Drawing.SizeF(16F, 16F);
            this.arcScaleSpindleCapComponent1.ZOrder = -100;
            // 
            // digitalGaugeCompletion
            // 
            this.digitalGaugeCompletion.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#34000000");
            this.digitalGaugeCompletion.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.digitalGaugeCompletion.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent1});
            this.digitalGaugeCompletion.Bounds = new System.Drawing.Rectangle(6, 358, 497, 100);
            this.digitalGaugeCompletion.Name = "digitalGaugeCompletion";
            this.digitalGaugeCompletion.Text = "987 / 1200";
            // 
            // digitalBackgroundLayerComponent1
            // 
            this.digitalBackgroundLayerComponent1.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(499.625F, 99.9625F);
            this.digitalBackgroundLayerComponent1.Name = "Gauge0_BackgroundLayer1";
            this.digitalBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style4;
            this.digitalBackgroundLayerComponent1.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // sp04285_GC_Gritting_DashboardTableAdapter
            // 
            this.sp04285_GC_Gritting_DashboardTableAdapter.ClearBeforeFill = true;
            // 
            // gaugeControlJobAcceptance
            // 
            this.gaugeControlJobAcceptance.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControlJobAcceptance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControlJobAcceptance.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.circularGauge2,
            this.digitalGaugeAcceptance});
            this.gaugeControlJobAcceptance.Location = new System.Drawing.Point(0, 0);
            this.gaugeControlJobAcceptance.Name = "gaugeControlJobAcceptance";
            this.gaugeControlJobAcceptance.Size = new System.Drawing.Size(318, 243);
            this.gaugeControlJobAcceptance.TabIndex = 1;
            // 
            // circularGauge2
            // 
            this.circularGauge2.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent[] {
            this.arcScaleBackgroundLayerComponent3,
            this.arcScaleBackgroundLayerComponent4});
            this.circularGauge2.Bounds = new System.Drawing.Rectangle(6, 6, 125, 125);
            this.circularGauge2.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleStateIndicatorComponent[] {
            this.arcScaleStateIndicatorComponent2});
            this.circularGauge2.Labels.AddRange(new DevExpress.XtraGauges.Win.Base.LabelComponent[] {
            this.labelComponent3,
            this.labelComponent4});
            this.circularGauge2.Name = "circularGauge2";
            this.circularGauge2.Needles.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent[] {
            this.arcScaleNeedleComponentAccepted,
            this.arcScaleNeedleComponentAcceptedNo});
            this.circularGauge2.Scales.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent[] {
            this.arcScaleComponentAccepted,
            this.arcScaleComponentAcceptedTotal,
            this.arcScaleComponentAcceptedNo});
            this.circularGauge2.SpindleCaps.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent[] {
            this.arcScaleSpindleCapComponent2});
            // 
            // arcScaleBackgroundLayerComponent3
            // 
            this.arcScaleBackgroundLayerComponent3.ArcScale = this.arcScaleComponentAccepted;
            this.arcScaleBackgroundLayerComponent3.Name = "bg1";
            this.arcScaleBackgroundLayerComponent3.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularFull_Style4;
            this.arcScaleBackgroundLayerComponent3.ZOrder = 1000;
            // 
            // arcScaleComponentAccepted
            // 
            this.arcScaleComponentAccepted.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAccepted.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAccepted.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAccepted.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAccepted.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 12F);
            this.arcScaleComponentAccepted.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponentAccepted.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.arcScaleComponentAccepted.EndAngle = 60F;
            this.arcScaleComponentAccepted.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponentAccepted.MajorTickmark.ShapeOffset = -12F;
            this.arcScaleComponentAccepted.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 0.8F);
            this.arcScaleComponentAccepted.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_2;
            this.arcScaleComponentAccepted.MajorTickmark.TextOffset = -27F;
            this.arcScaleComponentAccepted.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponentAccepted.MaxValue = 100F;
            this.arcScaleComponentAccepted.MinorTickCount = 4;
            this.arcScaleComponentAccepted.MinorTickmark.ShapeOffset = -5F;
            this.arcScaleComponentAccepted.MinorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 1F);
            this.arcScaleComponentAccepted.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_1;
            this.arcScaleComponentAccepted.Name = "scale1";
            this.arcScaleComponentAccepted.RadiusX = 122F;
            this.arcScaleComponentAccepted.RadiusY = 122F;
            this.arcScaleComponentAccepted.StartAngle = -240F;
            // 
            // arcScaleBackgroundLayerComponent4
            // 
            this.arcScaleBackgroundLayerComponent4.ArcScale = this.arcScaleComponentAccepted;
            this.arcScaleBackgroundLayerComponent4.Name = "bg2";
            this.arcScaleBackgroundLayerComponent4.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularFull_Style4_1;
            this.arcScaleBackgroundLayerComponent4.Size = new System.Drawing.SizeF(150F, 150F);
            this.arcScaleBackgroundLayerComponent4.ZOrder = 999;
            // 
            // arcScaleStateIndicatorComponent2
            // 
            this.arcScaleStateIndicatorComponent2.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 235F);
            this.arcScaleStateIndicatorComponent2.IndicatorScale = this.arcScaleComponentAccepted;
            this.arcScaleStateIndicatorComponent2.Name = "circularGauge1_Indicator1";
            this.arcScaleStateIndicatorComponent2.Size = new System.Drawing.SizeF(25F, 25F);
            scaleIndicatorState4.IntervalLength = 74F;
            scaleIndicatorState4.Name = "Default";
            scaleIndicatorState4.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            scaleIndicatorState5.IntervalLength = 24F;
            scaleIndicatorState5.Name = "State1";
            scaleIndicatorState5.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            scaleIndicatorState5.StartValue = 75F;
            scaleIndicatorState6.Name = "State2";
            scaleIndicatorState6.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            scaleIndicatorState6.StartValue = 100F;
            this.arcScaleStateIndicatorComponent2.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState4,
            scaleIndicatorState5,
            scaleIndicatorState6});
            this.arcScaleStateIndicatorComponent2.ZOrder = -100;
            // 
            // labelComponent3
            // 
            this.labelComponent3.AppearanceText.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labelComponent3.AppearanceText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.labelComponent3.Name = "circularGauge1_Label1";
            this.labelComponent3.Position = new DevExpress.XtraGauges.Core.Base.PointF2D(120F, 210F);
            this.labelComponent3.Size = new System.Drawing.SizeF(100F, 25F);
            this.labelComponent3.Text = "% Accepted";
            this.labelComponent3.ZOrder = -1001;
            // 
            // labelComponent4
            // 
            this.labelComponent4.AppearanceText.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labelComponent4.AppearanceText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Lavender");
            this.labelComponent4.Name = "labelComponent1Copy0";
            this.labelComponent4.Position = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 185F);
            this.labelComponent4.Size = new System.Drawing.SizeF(100F, 25F);
            this.labelComponent4.Text = "Total";
            this.labelComponent4.ZOrder = -1001;
            // 
            // arcScaleNeedleComponentAccepted
            // 
            this.arcScaleNeedleComponentAccepted.ArcScale = this.arcScaleComponentAccepted;
            this.arcScaleNeedleComponentAccepted.EndOffset = 5F;
            this.arcScaleNeedleComponentAccepted.Name = "needle1";
            this.arcScaleNeedleComponentAccepted.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_Style4;
            this.arcScaleNeedleComponentAccepted.StartOffset = -21F;
            this.arcScaleNeedleComponentAccepted.ZOrder = -50;
            // 
            // arcScaleNeedleComponentAcceptedNo
            // 
            this.arcScaleNeedleComponentAcceptedNo.ArcScale = this.arcScaleComponentAcceptedNo;
            this.arcScaleNeedleComponentAcceptedNo.EndOffset = -5F;
            this.arcScaleNeedleComponentAcceptedNo.Name = "circularGauge2_Needle2";
            this.arcScaleNeedleComponentAcceptedNo.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_ClockSecond;
            this.arcScaleNeedleComponentAcceptedNo.StartOffset = -4.4F;
            this.arcScaleNeedleComponentAcceptedNo.ZOrder = -49;
            // 
            // arcScaleComponentAcceptedNo
            // 
            this.arcScaleComponentAcceptedNo.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAcceptedNo.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAcceptedNo.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAcceptedNo.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAcceptedNo.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 12F);
            this.arcScaleComponentAcceptedNo.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponentAcceptedNo.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.arcScaleComponentAcceptedNo.EndAngle = 60F;
            this.arcScaleComponentAcceptedNo.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponentAcceptedNo.MajorTickmark.ShapeOffset = -12F;
            this.arcScaleComponentAcceptedNo.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 0.8F);
            this.arcScaleComponentAcceptedNo.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_2;
            this.arcScaleComponentAcceptedNo.MajorTickmark.TextOffset = -27F;
            this.arcScaleComponentAcceptedNo.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponentAcceptedNo.MaxValue = 100F;
            this.arcScaleComponentAcceptedNo.MinorTickCount = 4;
            this.arcScaleComponentAcceptedNo.MinorTickmark.ShapeOffset = -5F;
            this.arcScaleComponentAcceptedNo.MinorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 1F);
            this.arcScaleComponentAcceptedNo.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_1;
            this.arcScaleComponentAcceptedNo.Name = "scale1Copy0";
            this.arcScaleComponentAcceptedNo.RadiusX = 122F;
            this.arcScaleComponentAcceptedNo.RadiusY = 122F;
            this.arcScaleComponentAcceptedNo.StartAngle = -240F;
            // 
            // arcScaleComponentAcceptedTotal
            // 
            this.arcScaleComponentAcceptedTotal.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAcceptedTotal.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAcceptedTotal.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAcceptedTotal.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponentAcceptedTotal.AppearanceTickmarkText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.arcScaleComponentAcceptedTotal.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:LightBlue");
            this.arcScaleComponentAcceptedTotal.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.arcScaleComponentAcceptedTotal.EndAngle = 60F;
            this.arcScaleComponentAcceptedTotal.MajorTickCount = 8;
            this.arcScaleComponentAcceptedTotal.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponentAcceptedTotal.MajorTickmark.ShapeOffset = -6F;
            this.arcScaleComponentAcceptedTotal.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_3;
            this.arcScaleComponentAcceptedTotal.MajorTickmark.TextOffset = -20F;
            this.arcScaleComponentAcceptedTotal.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponentAcceptedTotal.MaxValue = 700F;
            this.arcScaleComponentAcceptedTotal.MinorTickCount = 4;
            this.arcScaleComponentAcceptedTotal.MinorTickmark.ShapeOffset = -1.5F;
            this.arcScaleComponentAcceptedTotal.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_4;
            this.arcScaleComponentAcceptedTotal.Name = "scale2";
            this.arcScaleComponentAcceptedTotal.RadiusX = 70F;
            this.arcScaleComponentAcceptedTotal.RadiusY = 70F;
            this.arcScaleComponentAcceptedTotal.StartAngle = -240F;
            this.arcScaleComponentAcceptedTotal.ZOrder = -1;
            // 
            // arcScaleSpindleCapComponent2
            // 
            this.arcScaleSpindleCapComponent2.ArcScale = this.arcScaleComponentAccepted;
            this.arcScaleSpindleCapComponent2.Name = "cap1";
            this.arcScaleSpindleCapComponent2.ShapeType = DevExpress.XtraGauges.Core.Model.SpindleCapShapeType.CircularFull_Style4;
            this.arcScaleSpindleCapComponent2.Size = new System.Drawing.SizeF(16F, 16F);
            this.arcScaleSpindleCapComponent2.ZOrder = -100;
            // 
            // digitalGaugeAcceptance
            // 
            this.digitalGaugeAcceptance.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#34000000");
            this.digitalGaugeAcceptance.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.digitalGaugeAcceptance.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent2});
            this.digitalGaugeAcceptance.Bounds = new System.Drawing.Rectangle(6, 137, 306, 100);
            this.digitalGaugeAcceptance.Name = "digitalGaugeAcceptance";
            this.digitalGaugeAcceptance.Text = "987 / 1200";
            // 
            // digitalBackgroundLayerComponent2
            // 
            this.digitalBackgroundLayerComponent2.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(499.625F, 99.9625F);
            this.digitalBackgroundLayerComponent2.Name = "digitalGauge1_BackgroundLayer1";
            this.digitalBackgroundLayerComponent2.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style4;
            this.digitalBackgroundLayerComponent2.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent2.ZOrder = 1000;
            // 
            // gaugeControl5
            // 
            this.gaugeControl5.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl5.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge1});
            this.gaugeControl5.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl5.Name = "gaugeControl5";
            this.gaugeControl5.Size = new System.Drawing.Size(208, 42);
            this.gaugeControl5.TabIndex = 1;
            this.gaugeControl5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gaugeControl5_MouseClick);
            // 
            // stateIndicatorGauge1
            // 
            this.stateIndicatorGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 196, 30);
            this.stateIndicatorGauge1.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent1});
            this.stateIndicatorGauge1.Name = "stateIndicatorGauge1";
            // 
            // stateIndicatorComponent1
            // 
            this.stateIndicatorComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent1.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent1.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent1.StateIndex = 1;
            indicatorState1.Name = "State1";
            indicatorState1.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState2.Name = "State2";
            indicatorState2.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState3.Name = "State3";
            indicatorState3.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState4.Name = "State4";
            indicatorState4.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent1.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState1,
            indicatorState2,
            indicatorState3,
            indicatorState4});
            // 
            // gaugeControl7
            // 
            this.gaugeControl7.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl7.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.digitalGaugeSeason});
            this.gaugeControl7.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl7.Name = "gaugeControl7";
            this.gaugeControl7.Size = new System.Drawing.Size(237, 60);
            this.gaugeControl7.TabIndex = 4;
            // 
            // digitalGaugeSeason
            // 
            this.digitalGaugeSeason.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#34000000");
            this.digitalGaugeSeason.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.digitalGaugeSeason.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent3});
            this.digitalGaugeSeason.Bounds = new System.Drawing.Rectangle(6, 6, 225, 48);
            this.digitalGaugeSeason.DigitCount = 6;
            this.digitalGaugeSeason.Name = "digitalGaugeSeason";
            this.digitalGaugeSeason.Text = "000000";
            // 
            // digitalBackgroundLayerComponent3
            // 
            this.digitalBackgroundLayerComponent3.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(307.775F, 99.9625F);
            this.digitalBackgroundLayerComponent3.Name = "bg1";
            this.digitalBackgroundLayerComponent3.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style4;
            this.digitalBackgroundLayerComponent3.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent3.ZOrder = 1000;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(841, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(524, 200);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemCompanyFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReload)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // barEditItemDateFilter
            // 
            this.barEditItemDateFilter.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barEditItemDateFilter.Caption = "Date Parameters";
            this.barEditItemDateFilter.Edit = this.repositoryItemPopupContainerEditDateFilter;
            this.barEditItemDateFilter.EditValue = "Date Parameters";
            this.barEditItemDateFilter.EditWidth = 106;
            this.barEditItemDateFilter.Id = 30;
            this.barEditItemDateFilter.Name = "barEditItemDateFilter";
            // 
            // repositoryItemPopupContainerEditDateFilter
            // 
            this.repositoryItemPopupContainerEditDateFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateFilter.Name = "repositoryItemPopupContainerEditDateFilter";
            this.repositoryItemPopupContainerEditDateFilter.PopupControl = this.popupContainerControlDates;
            this.repositoryItemPopupContainerEditDateFilter.ShowPopupCloseButton = false;
            // 
            // barEditItemCompanyFilter
            // 
            this.barEditItemCompanyFilter.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barEditItemCompanyFilter.Caption = "Company Filter";
            this.barEditItemCompanyFilter.Edit = this.repositoryItemPopupContainerEditCompanyFilter;
            this.barEditItemCompanyFilter.EditValue = "No Company Filter";
            this.barEditItemCompanyFilter.EditWidth = 126;
            this.barEditItemCompanyFilter.Id = 28;
            this.barEditItemCompanyFilter.Name = "barEditItemCompanyFilter";
            // 
            // repositoryItemPopupContainerEditCompanyFilter
            // 
            this.repositoryItemPopupContainerEditCompanyFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditCompanyFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditCompanyFilter.Name = "repositoryItemPopupContainerEditCompanyFilter";
            this.repositoryItemPopupContainerEditCompanyFilter.PopupControl = this.popupContainerControlCompanies;
            this.repositoryItemPopupContainerEditCompanyFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditCompanyFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditCompanyFilter_QueryResultValue);
            // 
            // bbiReload
            // 
            this.bbiReload.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bbiReload.Caption = "Reload";
            this.bbiReload.Glyph = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiReload.Id = 27;
            this.bbiReload.Name = "bbiReload";
            this.bbiReload.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiReload.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReload_ItemClick);
            // 
            // gaugeControl8
            // 
            this.gaugeControl8.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl8.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.digitalGaugeYesterday});
            this.gaugeControl8.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl8.Name = "gaugeControl8";
            this.gaugeControl8.Size = new System.Drawing.Size(237, 16);
            this.gaugeControl8.TabIndex = 5;
            // 
            // digitalGaugeYesterday
            // 
            this.digitalGaugeYesterday.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#34000000");
            this.digitalGaugeYesterday.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.digitalGaugeYesterday.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent4});
            this.digitalGaugeYesterday.Bounds = new System.Drawing.Rectangle(6, 6, 225, 20);
            this.digitalGaugeYesterday.DigitCount = 5;
            this.digitalGaugeYesterday.Name = "digitalGaugeYesterday";
            this.digitalGaugeYesterday.Text = "00,000";
            // 
            // digitalBackgroundLayerComponent4
            // 
            this.digitalBackgroundLayerComponent4.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(259.8125F, 99.9625F);
            this.digitalBackgroundLayerComponent4.Name = "bg1";
            this.digitalBackgroundLayerComponent4.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style4;
            this.digitalBackgroundLayerComponent4.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent4.ZOrder = 1000;
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.splitContainerControl4);
            this.splitContainerControl3.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1088, 533);
            this.splitContainerControl3.SplitterPosition = 241;
            this.splitContainerControl3.TabIndex = 7;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 41);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl4.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl4.Panel1.Controls.Add(this.splitContainerControl6);
            this.splitContainerControl4.Panel2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.splitContainerControl4.Panel2.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl4.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl4.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl4.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel2.Controls.Add(this.gaugeControlJobCompletion);
            this.splitContainerControl4.Panel2.ShowCaption = true;
            this.splitContainerControl4.Panel2.Text = "Proactive Job Completion";
            this.splitContainerControl4.Size = new System.Drawing.Size(841, 492);
            this.splitContainerControl4.SplitterPosition = 322;
            this.splitContainerControl4.TabIndex = 7;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl6.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl6.Horizontal = false;
            this.splitContainerControl6.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.splitContainerControl6.Panel1.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl6.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl6.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl6.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl6.Panel1.Controls.Add(this.gaugeControlJobAcceptance);
            this.splitContainerControl6.Panel1.ShowCaption = true;
            this.splitContainerControl6.Panel1.Text = "Proactive Job Acceptance";
            this.splitContainerControl6.Panel2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl6.Panel2.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl6.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl6.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl6.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl6.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl6.Panel2.Controls.Add(this.splitContainerControl8);
            this.splitContainerControl6.Panel2.ShowCaption = true;
            this.splitContainerControl6.Panel2.Text = "Reactive Jobs - Rolling 24 Hours";
            this.splitContainerControl6.Size = new System.Drawing.Size(322, 492);
            this.splitContainerControl6.SplitterPosition = 215;
            this.splitContainerControl6.TabIndex = 2;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // splitContainerControl8
            // 
            this.splitContainerControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl8.Horizontal = false;
            this.splitContainerControl8.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl8.Name = "splitContainerControl8";
            this.splitContainerControl8.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl8.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl8.Panel1.Controls.Add(this.splitContainerControl12);
            this.splitContainerControl8.Panel2.Controls.Add(this.splitContainerControl11);
            this.splitContainerControl8.Panel2.Text = "Panel2";
            this.splitContainerControl8.Size = new System.Drawing.Size(296, 212);
            this.splitContainerControl8.SplitterPosition = 94;
            this.splitContainerControl8.TabIndex = 0;
            this.splitContainerControl8.Text = "splitContainerControl8";
            // 
            // splitContainerControl12
            // 
            this.splitContainerControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl12.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl12.Name = "splitContainerControl12";
            this.splitContainerControl12.Panel1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl12.Panel1.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl12.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl12.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl12.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl12.Panel1.Controls.Add(this.gaugeControl2);
            this.splitContainerControl12.Panel1.ShowCaption = true;
            this.splitContainerControl12.Panel1.Text = "Total";
            this.splitContainerControl12.Panel2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl12.Panel2.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl12.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl12.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl12.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl12.Panel2.Controls.Add(this.gaugeControl11);
            this.splitContainerControl12.Panel2.ShowCaption = true;
            this.splitContainerControl12.Panel2.Text = "Rejected";
            this.splitContainerControl12.Size = new System.Drawing.Size(296, 94);
            this.splitContainerControl12.SplitterPosition = 147;
            this.splitContainerControl12.TabIndex = 7;
            this.splitContainerControl12.Text = "splitContainerControl12";
            // 
            // gaugeControl2
            // 
            this.gaugeControl2.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl2.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.digitalGaugeReactiveTotal});
            this.gaugeControl2.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl2.Name = "gaugeControl2";
            this.gaugeControl2.Size = new System.Drawing.Size(143, 69);
            this.gaugeControl2.TabIndex = 6;
            // 
            // digitalGaugeReactiveTotal
            // 
            this.digitalGaugeReactiveTotal.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#00FFFFFF");
            this.digitalGaugeReactiveTotal.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:WhiteSmoke");
            this.digitalGaugeReactiveTotal.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent6});
            this.digitalGaugeReactiveTotal.Bounds = new System.Drawing.Rectangle(6, 6, 131, 57);
            this.digitalGaugeReactiveTotal.DigitCount = 5;
            this.digitalGaugeReactiveTotal.Name = "digitalGaugeReactiveTotal";
            this.digitalGaugeReactiveTotal.Text = "00,000";
            // 
            // digitalBackgroundLayerComponent6
            // 
            this.digitalBackgroundLayerComponent6.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(259.8125F, 99.9625F);
            this.digitalBackgroundLayerComponent6.Name = "bg1";
            this.digitalBackgroundLayerComponent6.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style7;
            this.digitalBackgroundLayerComponent6.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent6.ZOrder = 1000;
            // 
            // gaugeControl11
            // 
            this.gaugeControl11.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl11.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.digitalGaugeReactiveRejected});
            this.gaugeControl11.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl11.Name = "gaugeControl11";
            this.gaugeControl11.Size = new System.Drawing.Size(139, 69);
            this.gaugeControl11.TabIndex = 7;
            // 
            // digitalGaugeReactiveRejected
            // 
            this.digitalGaugeReactiveRejected.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#00FFFFFF");
            this.digitalGaugeReactiveRejected.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:WhiteSmoke");
            this.digitalGaugeReactiveRejected.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent7});
            this.digitalGaugeReactiveRejected.Bounds = new System.Drawing.Rectangle(6, 6, 127, 57);
            this.digitalGaugeReactiveRejected.DigitCount = 5;
            this.digitalGaugeReactiveRejected.Name = "digitalGaugeReactiveRejected";
            this.digitalGaugeReactiveRejected.Text = "00,000";
            // 
            // digitalBackgroundLayerComponent7
            // 
            this.digitalBackgroundLayerComponent7.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(259.8125F, 99.9625F);
            this.digitalBackgroundLayerComponent7.Name = "bg1";
            this.digitalBackgroundLayerComponent7.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style7;
            this.digitalBackgroundLayerComponent7.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent7.ZOrder = 1000;
            // 
            // splitContainerControl11
            // 
            this.splitContainerControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl11.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl11.Name = "splitContainerControl11";
            this.splitContainerControl11.Panel1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl11.Panel1.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl11.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl11.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl11.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl11.Panel1.Controls.Add(this.gaugeControl12);
            this.splitContainerControl11.Panel1.ShowCaption = true;
            this.splitContainerControl11.Panel1.Text = "Accepted";
            this.splitContainerControl11.Panel2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl11.Panel2.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl11.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl11.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl11.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl11.Panel2.Controls.Add(this.gaugeControl13);
            this.splitContainerControl11.Panel2.ShowCaption = true;
            this.splitContainerControl11.Panel2.Text = "Outstanding";
            this.splitContainerControl11.Size = new System.Drawing.Size(296, 112);
            this.splitContainerControl11.SplitterPosition = 147;
            this.splitContainerControl11.TabIndex = 3;
            this.splitContainerControl11.Text = "splitContainerControl9";
            // 
            // gaugeControl12
            // 
            this.gaugeControl12.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl12.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.digitalGaugeReactiveAccepted});
            this.gaugeControl12.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl12.Name = "gaugeControl12";
            this.gaugeControl12.Size = new System.Drawing.Size(143, 87);
            this.gaugeControl12.TabIndex = 7;
            // 
            // digitalGaugeReactiveAccepted
            // 
            this.digitalGaugeReactiveAccepted.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#00FFFFFF");
            this.digitalGaugeReactiveAccepted.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:WhiteSmoke");
            this.digitalGaugeReactiveAccepted.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent8});
            this.digitalGaugeReactiveAccepted.Bounds = new System.Drawing.Rectangle(6, 6, 131, 75);
            this.digitalGaugeReactiveAccepted.DigitCount = 5;
            this.digitalGaugeReactiveAccepted.Name = "digitalGaugeReactiveAccepted";
            this.digitalGaugeReactiveAccepted.Text = "00,000";
            // 
            // digitalBackgroundLayerComponent8
            // 
            this.digitalBackgroundLayerComponent8.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(259.8125F, 99.9625F);
            this.digitalBackgroundLayerComponent8.Name = "bg1";
            this.digitalBackgroundLayerComponent8.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style7;
            this.digitalBackgroundLayerComponent8.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent8.ZOrder = 1000;
            // 
            // gaugeControl13
            // 
            this.gaugeControl13.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl13.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.digitalGaugeReactiveOutstanding});
            this.gaugeControl13.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl13.Name = "gaugeControl13";
            this.gaugeControl13.Size = new System.Drawing.Size(139, 87);
            this.gaugeControl13.TabIndex = 7;
            // 
            // digitalGaugeReactiveOutstanding
            // 
            this.digitalGaugeReactiveOutstanding.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#00FFFFFF");
            this.digitalGaugeReactiveOutstanding.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:WhiteSmoke");
            this.digitalGaugeReactiveOutstanding.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent9});
            this.digitalGaugeReactiveOutstanding.Bounds = new System.Drawing.Rectangle(6, 6, 127, 75);
            this.digitalGaugeReactiveOutstanding.DigitCount = 5;
            this.digitalGaugeReactiveOutstanding.Name = "digitalGaugeReactiveOutstanding";
            this.digitalGaugeReactiveOutstanding.Text = "00,000";
            // 
            // digitalBackgroundLayerComponent9
            // 
            this.digitalBackgroundLayerComponent9.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(259.8125F, 99.9625F);
            this.digitalBackgroundLayerComponent9.Name = "bg1";
            this.digitalBackgroundLayerComponent9.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style7;
            this.digitalBackgroundLayerComponent9.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent9.ZOrder = 1000;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gaugeControl7);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Total Callouts For Season";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl7);
            this.splitContainerControl1.Size = new System.Drawing.Size(241, 533);
            this.splitContainerControl1.SplitterPosition = 85;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl7
            // 
            this.splitContainerControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl7.Horizontal = false;
            this.splitContainerControl7.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl7.Name = "splitContainerControl7";
            this.splitContainerControl7.Panel1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.splitContainerControl7.Panel1.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl7.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl7.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl7.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl7.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl7.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl7.Panel1.ShowCaption = true;
            this.splitContainerControl7.Panel1.Text = "Team Metrics";
            this.splitContainerControl7.Panel2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl7.Panel2.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl7.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl7.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl7.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl7.Panel2.Controls.Add(this.splitContainerControl10);
            this.splitContainerControl7.Panel2.ShowCaption = true;
            this.splitContainerControl7.Panel2.Text = "Total Proactive Callouts for Yesterday";
            this.splitContainerControl7.Size = new System.Drawing.Size(241, 442);
            this.splitContainerControl7.SplitterPosition = 293;
            this.splitContainerControl7.TabIndex = 1;
            this.splitContainerControl7.Text = "splitContainerControl7";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.Controls.Add(this.gaugeControl1);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Weather Imported";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl5);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(212, 290);
            this.splitContainerControl2.SplitterPosition = 67;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge2});
            this.gaugeControl1.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(208, 42);
            this.gaugeControl1.TabIndex = 2;
            this.gaugeControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gaugeControl1_MouseClick);
            // 
            // stateIndicatorGauge2
            // 
            this.stateIndicatorGauge2.Bounds = new System.Drawing.Rectangle(6, 6, 196, 30);
            this.stateIndicatorGauge2.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent2});
            this.stateIndicatorGauge2.Name = "stateIndicatorGauge2";
            // 
            // stateIndicatorComponent2
            // 
            this.stateIndicatorComponent2.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent2.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent2.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent2.StateIndex = 1;
            indicatorState5.Name = "State1";
            indicatorState5.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState6.Name = "State2";
            indicatorState6.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState7.Name = "State3";
            indicatorState7.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState8.Name = "State4";
            indicatorState8.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent2.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState5,
            indicatorState6,
            indicatorState7,
            indicatorState8});
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl5.Horizontal = false;
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl5.Panel1.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl5.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl5.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl5.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl5.Panel1.Controls.Add(this.gaugeControl5);
            this.splitContainerControl5.Panel1.ShowCaption = true;
            this.splitContainerControl5.Panel1.Text = "Client Reports Sent";
            this.splitContainerControl5.Panel2.Controls.Add(this.splitContainerControl9);
            this.splitContainerControl5.Size = new System.Drawing.Size(212, 217);
            this.splitContainerControl5.SplitterPosition = 67;
            this.splitContainerControl5.TabIndex = 0;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // splitContainerControl9
            // 
            this.splitContainerControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl9.Horizontal = false;
            this.splitContainerControl9.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl9.Name = "splitContainerControl9";
            this.splitContainerControl9.Panel1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl9.Panel1.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl9.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl9.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl9.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl9.Panel1.Controls.Add(this.gaugeControl4);
            this.splitContainerControl9.Panel1.ShowCaption = true;
            this.splitContainerControl9.Panel1.Text = "Text 100 % of Teams";
            this.splitContainerControl9.Panel2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl9.Panel2.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl9.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl9.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl9.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl9.Panel2.Controls.Add(this.gaugeControl3);
            this.splitContainerControl9.Panel2.ShowCaption = true;
            this.splitContainerControl9.Panel2.Text = "Chase Teams";
            this.splitContainerControl9.Size = new System.Drawing.Size(212, 144);
            this.splitContainerControl9.SplitterPosition = 68;
            this.splitContainerControl9.TabIndex = 3;
            this.splitContainerControl9.Text = "splitContainerControl9";
            // 
            // gaugeControl4
            // 
            this.gaugeControl4.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl4.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge3});
            this.gaugeControl4.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl4.Name = "gaugeControl4";
            this.gaugeControl4.Size = new System.Drawing.Size(208, 43);
            this.gaugeControl4.TabIndex = 2;
            this.gaugeControl4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gaugeControl4_MouseClick);
            // 
            // stateIndicatorGauge3
            // 
            this.stateIndicatorGauge3.Bounds = new System.Drawing.Rectangle(6, 6, 196, 31);
            this.stateIndicatorGauge3.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent3});
            this.stateIndicatorGauge3.Name = "stateIndicatorGauge3";
            // 
            // stateIndicatorComponent3
            // 
            this.stateIndicatorComponent3.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent3.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent3.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent3.StateIndex = 1;
            indicatorState9.Name = "State1";
            indicatorState9.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState10.Name = "State2";
            indicatorState10.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState11.Name = "State3";
            indicatorState11.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState12.Name = "State4";
            indicatorState12.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent3.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState9,
            indicatorState10,
            indicatorState11,
            indicatorState12});
            // 
            // gaugeControl3
            // 
            this.gaugeControl3.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl3.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge4});
            this.gaugeControl3.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl3.Name = "gaugeControl3";
            this.gaugeControl3.Size = new System.Drawing.Size(208, 45);
            this.gaugeControl3.TabIndex = 3;
            this.gaugeControl3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gaugeControl3_MouseClick);
            // 
            // stateIndicatorGauge4
            // 
            this.stateIndicatorGauge4.Bounds = new System.Drawing.Rectangle(6, 6, 196, 33);
            this.stateIndicatorGauge4.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent4});
            this.stateIndicatorGauge4.Name = "stateIndicatorGauge4";
            // 
            // stateIndicatorComponent4
            // 
            this.stateIndicatorComponent4.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent4.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent4.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent4.StateIndex = 1;
            indicatorState13.Name = "State1";
            indicatorState13.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState14.Name = "State2";
            indicatorState14.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState15.Name = "State3";
            indicatorState15.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState16.Name = "State4";
            indicatorState16.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent4.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState13,
            indicatorState14,
            indicatorState15,
            indicatorState16});
            // 
            // splitContainerControl10
            // 
            this.splitContainerControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl10.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl10.Horizontal = false;
            this.splitContainerControl10.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl10.Name = "splitContainerControl10";
            this.splitContainerControl10.Panel1.Controls.Add(this.gaugeControl8);
            this.splitContainerControl10.Panel1.Text = "Panel1";
            this.splitContainerControl10.Panel2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.splitContainerControl10.Panel2.Appearance.Options.UseFont = true;
            this.splitContainerControl10.Panel2.Appearance.Options.UseTextOptions = true;
            this.splitContainerControl10.Panel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl10.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl10.Panel2.Controls.Add(this.gaugeControlCurrentTime);
            this.splitContainerControl10.Panel2.ShowCaption = true;
            this.splitContainerControl10.Panel2.Text = "Current Time";
            this.splitContainerControl10.Size = new System.Drawing.Size(237, 118);
            this.splitContainerControl10.SplitterPosition = 96;
            this.splitContainerControl10.TabIndex = 6;
            this.splitContainerControl10.Text = "splitContainerControl10";
            // 
            // gaugeControlCurrentTime
            // 
            this.gaugeControlCurrentTime.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControlCurrentTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControlCurrentTime.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.digitalGaugeCurrentTime});
            this.gaugeControlCurrentTime.Location = new System.Drawing.Point(0, 0);
            this.gaugeControlCurrentTime.Name = "gaugeControlCurrentTime";
            this.gaugeControlCurrentTime.Size = new System.Drawing.Size(233, 72);
            this.gaugeControlCurrentTime.TabIndex = 0;
            // 
            // digitalGaugeCurrentTime
            // 
            this.digitalGaugeCurrentTime.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#00FFFFFF");
            this.digitalGaugeCurrentTime.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:WhiteSmoke");
            this.digitalGaugeCurrentTime.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent10});
            this.digitalGaugeCurrentTime.Bounds = new System.Drawing.Rectangle(6, 6, 221, 60);
            this.digitalGaugeCurrentTime.DigitCount = 6;
            this.digitalGaugeCurrentTime.LetterSpacing = 2F;
            this.digitalGaugeCurrentTime.Name = "digitalGaugeCurrentTime";
            this.digitalGaugeCurrentTime.Text = "09:00:00";
            // 
            // digitalBackgroundLayerComponent10
            // 
            this.digitalBackgroundLayerComponent10.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(319.775F, 99.9625F);
            this.digitalBackgroundLayerComponent10.Name = "bg1";
            this.digitalBackgroundLayerComponent10.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style7;
            this.digitalBackgroundLayerComponent10.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent10.ZOrder = 1000;
            // 
            // digitalGauge5
            // 
            this.digitalGauge5.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#34000000");
            this.digitalGauge5.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.digitalGauge5.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent5});
            this.digitalGauge5.Bounds = new System.Drawing.Rectangle(6, 356, 458, 100);
            this.digitalGauge5.Name = "digitalGauge5";
            this.digitalGauge5.Text = "987 / 1200";
            // 
            // digitalBackgroundLayerComponent5
            // 
            this.digitalBackgroundLayerComponent5.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(499.625F, 99.9625F);
            this.digitalBackgroundLayerComponent5.Name = "digitalGauge1_BackgroundLayer1";
            this.digitalBackgroundLayerComponent5.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style4;
            this.digitalBackgroundLayerComponent5.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent5.ZOrder = 1000;
            // 
            // sp04237_GC_Company_Filter_ListTableAdapter
            // 
            this.sp04237_GC_Company_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // timer1
            // 
            this.timer1.Interval = 120000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // pmGaugeStyle
            // 
            this.pmGaugeStyle.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiStyleChooser),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiStyleManager)});
            this.pmGaugeStyle.Manager = this.barManager1;
            this.pmGaugeStyle.MenuCaption = "Gauge Menu";
            this.pmGaugeStyle.Name = "pmGaugeStyle";
            this.pmGaugeStyle.ShowCaption = true;
            // 
            // bbiStyleChooser
            // 
            this.bbiStyleChooser.Caption = "Style Chooser";
            this.bbiStyleChooser.Id = 31;
            this.bbiStyleChooser.Name = "bbiStyleChooser";
            this.bbiStyleChooser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStyleChooser_ItemClick);
            // 
            // bbiStyleManager
            // 
            this.bbiStyleManager.Caption = "Style Manager";
            this.bbiStyleManager.Id = 32;
            this.bbiStyleManager.Name = "bbiStyleManager";
            this.bbiStyleManager.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStyleManager_ItemClick);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // frm_GC_Dashboard
            // 
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1088, 533);
            this.Controls.Add(this.splitContainerControl3);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_GC_Dashboard";
            this.Text = "Winter Maintenance - Dashboard";
            this.Activated += new System.EventHandler(this.frm_GC_Dashboard_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Dashboard_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Dashboard_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04285GCGrittingDashboardBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            this.gaugeControlJobCompletion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDates)).EndInit();
            this.popupContainerControlDates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateSeason.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateSeason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateSeason.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateSeason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateLastNight.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateLastNight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateLastNight.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateLastNight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateReactiveJobs.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateReactiveJobs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateReactiveJobs.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateReactiveJobs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateJobAcceptance.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDateJobAcceptance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateJobAcceptance.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDateJobAcceptance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRecalculateOnTimer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).EndInit();
            this.popupContainerControlCompanies.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentCompletion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleStateIndicatorComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponentCompletion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponentCompletionNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentCompletionNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentCompletionTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleSpindleCapComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeCompletion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularGauge2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentAccepted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleStateIndicatorComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponentAccepted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponentAcceptedNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentAcceptedNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponentAcceptedTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleSpindleCapComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeAcceptance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeSeason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeYesterday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).EndInit();
            this.splitContainerControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl12)).EndInit();
            this.splitContainerControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeReactiveTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeReactiveRejected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl11)).EndInit();
            this.splitContainerControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeReactiveAccepted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeReactiveOutstanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).EndInit();
            this.splitContainerControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl9)).EndInit();
            this.splitContainerControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl10)).EndInit();
            this.splitContainerControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.digitalGaugeCurrentTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGauge5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmGaugeStyle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp04285GCGrittingDashboardBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private DataSet_GC_CoreTableAdapters.sp04285_GC_Gritting_DashboardTableAdapter sp04285_GC_Gritting_DashboardTableAdapter;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControlJobCompletion;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControlJobAcceptance;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl5;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent1;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl7;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGaugeSeason;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent3;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiReload;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl8;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGaugeYesterday;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge2;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl4;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge3;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl7;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGauge5;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent5;
        private DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge circularGauge1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponentCompletion;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent2;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleStateIndicatorComponent arcScaleStateIndicatorComponent1;
        private DevExpress.XtraGauges.Win.Base.LabelComponent labelComponent1;
        private DevExpress.XtraGauges.Win.Base.LabelComponent labelComponent2;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponentCompletion;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponentCompletionNo;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponentCompletionNo;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponentCompletionTotal;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent arcScaleSpindleCapComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGaugeCompletion;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent1;
        private DevExpress.XtraBars.BarEditItem barEditItemCompanyFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditCompanyFilter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCompanies;
        private DevExpress.XtraEditors.SimpleButton btnCompanyFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOrder;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource;
        private DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter sp04237_GC_Company_Filter_ListTableAdapter;
        private DevExpress.XtraBars.BarEditItem barEditItemDateFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDates;
        private DevExpress.XtraEditors.SimpleButton btnDateFilterOK;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDateJobAcceptance;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DateEdit dateEditToDateReactiveJobs;
        private DevExpress.XtraEditors.DateEdit dateEditFromDateReactiveJobs;
        private DevExpress.XtraEditors.DateEdit dateEditToDateJobAcceptance;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.DateEdit dateEditToDateLastNight;
        private DevExpress.XtraEditors.DateEdit dateEditFromDateLastNight;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.DateEdit dateEditToDateSeason;
        private DevExpress.XtraEditors.DateEdit dateEditFromDateSeason;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl8;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl9;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl3;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge4;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl12;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl11;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGaugeReactiveRejected;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent7;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl11;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl12;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGaugeReactiveAccepted;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent8;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl13;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGaugeReactiveOutstanding;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent9;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGaugeReactiveTotal;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent6;
        private DevExpress.XtraEditors.CheckEdit checkEditRecalculateOnTimer;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiStyleChooser;
        private DevExpress.XtraBars.BarButtonItem bbiStyleManager;
        private DevExpress.XtraBars.PopupMenu pmGaugeStyle;
        private DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge circularGauge2;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent3;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponentAccepted;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent4;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleStateIndicatorComponent arcScaleStateIndicatorComponent2;
        private DevExpress.XtraGauges.Win.Base.LabelComponent labelComponent3;
        private DevExpress.XtraGauges.Win.Base.LabelComponent labelComponent4;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponentAccepted;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponentAcceptedNo;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponentAcceptedNo;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponentAcceptedTotal;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent arcScaleSpindleCapComponent2;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGaugeAcceptance;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl10;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControlCurrentTime;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGaugeCurrentTime;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent10;
        private System.Windows.Forms.Timer timer2;
    }
}

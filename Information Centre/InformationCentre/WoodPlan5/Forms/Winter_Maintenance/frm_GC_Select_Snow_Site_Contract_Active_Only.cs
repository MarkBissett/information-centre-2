using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_GC_Select_Snow_Site_Contract_Active_Only : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedSite = "";
        public int intSelectedSiteID = 0;
        public string strSelectedClient = "";
        public int intSiteSnowClearanceContractID = 0;
        public int ChargeMethodID = 0;
        public decimal ChargeFixedPrice = (decimal)0.00;
        public decimal ChargeFixedNumberOfHours = (decimal)0.00;
        public decimal ChargeFixedHourlyRate = (decimal)0.00;
        public decimal ChargeExtraHourlyRate = (decimal)0.00;
        public decimal ChargeMarkupPercentage = (decimal)0.00;
        public string ChargeMethodDescription = "";
        public string SiteAddressLine1 = "";
        public string SiteAddressLine2 = "";
        public string SiteAddressLine3 = "";
        public string SiteAddressLine4 = "";
        public string SiteAddressLine5 = "";
        public string SitePostcode = "";
 
        GridHitInfo downHitInfo = null;

        #endregion

        // Important Note: Ensure all Lists are set to Single Selection //
        public frm_GC_Select_Snow_Site_Contract_Active_Only()
        {
            InitializeComponent();
        }

        private void frm_GC_Select_Snow_Site_Contract_Active_Only_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400039;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04166_GC_Snow_Clearance_Sites_List_SimpleTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                this.sp03042_EP_Client_Manager_List_SimpleTableAdapter.Connection.ConnectionString = strConnectionString;
                this.sp03042_EP_Client_Manager_List_SimpleTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03042_EP_Client_Manager_List_Simple);
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the clients list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        bool internalRowFocusing;

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Site Snow Clearance Contracts Available For Selection - Select a Client to see Related Contracts";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ClientID"])) + ',';
            }

            //Populate Linked Map Links //
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_GC_DataEntry.sp04059_GC_Gritting_Sites_List_Simple.Clear();
            }
            else
            {
                try
                {

                    sp04166_GC_Snow_Clearance_Sites_List_SimpleTableAdapter.Fill(dataSet_GC_Snow_DataEntry.sp04166_GC_Snow_Clearance_Sites_List_Simple, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related contracts.\n\nTry selecting a client again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSiteSnowClearanceContractID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl2.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedSite = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteName"))) ? "Unknown Site" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteName")));
                intSelectedSiteID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteID"));
                strSelectedClient = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName")));
                intSiteSnowClearanceContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteGrittingContractID"));
                ChargeMethodID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ChargeMethodID"));
                ChargeFixedPrice = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "ChargeFixedPrice"));
                ChargeFixedNumberOfHours = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "ChargeFixedNumberOfHours"));
                ChargeFixedHourlyRate = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "ChargeFixedHourlyRate"));
                ChargeExtraHourlyRate = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "ChargeExtraHourlyRate"));
                ChargeMarkupPercentage = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "ChargeMarkupPercentage"));
                ChargeMethodDescription = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ChargeMethodDescription"));

                SiteAddressLine1 = view.GetRowCellValue(view.FocusedRowHandle, "SiteAddressLine1").ToString();
                SiteAddressLine2 = view.GetRowCellValue(view.FocusedRowHandle, "SiteAddressLine2").ToString();
                SiteAddressLine3 = view.GetRowCellValue(view.FocusedRowHandle, "SiteAddressLine3").ToString();
                SiteAddressLine4 = view.GetRowCellValue(view.FocusedRowHandle, "SiteAddressLine4").ToString();
                SiteAddressLine5 = view.GetRowCellValue(view.FocusedRowHandle, "SiteAddressLine5").ToString();
                SitePostcode = view.GetRowCellValue(view.FocusedRowHandle, "SitePostcode").ToString();
            }
        }
 
    
    
    
    }
}


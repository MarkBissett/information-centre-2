using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Select_Salt_Supplier : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedValue = "";
        public int intSelectedID = 0;
        GridHitInfo downHitInfo = null;

        public string strPassedInRecordIDs = "";
        public string strPassedInRecordType = "";  // Needs to be "site" or "saltdepot" for the Postcode Search Slider to work //

        private string strPostcode = "";
        private int intDistance = 0;
        
        #endregion
    
        public frm_GC_Select_Salt_Supplier()
        {
            InitializeComponent();
        }

        private void frm_GC_Select_Salt_Supplier_Load(object sender, EventArgs e)
        {
            
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400048;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            LoadData();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            int RecordID = 0;
            char[] delimiters = new char[] { ',' };
            string[] strArray = strPassedInRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length == 1 && !string.IsNullOrEmpty(strPassedInRecordType))
            {
                RecordID = Convert.ToInt32(strArray[0]);
                try
                {
                    DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter GetPostcode = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
                    GetPostcode.ChangeConnectionString(strConnectionString);
                    strPostcode = GetPostcode.sp04204_GC_Get_Postcode(strPassedInRecordType, RecordID).ToString().ToUpper().Replace(" ", "");
                    strPostcode = (strPostcode.Length == 7 ? strPostcode.Substring(0, 4) : strPostcode.Substring(0, 3));
                    strPostcode = (strPostcode.Length == 7 ? strPostcode.Substring(0, 4) : (strPostcode.Length == 6 ? strPostcode.Substring(0, 3) : strPostcode.Substring(0, 2)));
                }
                catch (Exception)
                {
                }
            }

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            if (string.IsNullOrEmpty(strPostcode))
            {
                barEditItemSearchRadius.Enabled = false;
            }
            else
            {
                barEditItemSearchRadius.Enabled = true;
                radiusLabel.Text = "Postcode: " + strPostcode + " - Seach Radius: 0 miles.";
            }
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            try
            {
                sp04206_GC_Salt_Supplier_Select_ListTableAdapter.Connection.ConnectionString = strConnectionString;
                sp04206_GC_Salt_Supplier_Select_ListTableAdapter.Fill(dataSet_GC_Core.sp04206_GC_Salt_Supplier_Select_List, strPostcode, intDistance);
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the salt suppliers list!\n\nIf the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Records Available For Selection");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedValue = Convert.ToString(gridView1.GetRowCellValue(view.FocusedRowHandle, "SupplierName"));
                intSelectedID = Convert.ToInt32(gridView1.GetRowCellValue(view.FocusedRowHandle, "GritSupplierID"));
            }
        }


        #region Search Radius

        private void trackBarControlRadius_EditValueChanged(object sender, EventArgs e)
        {
            TrackBarControl tbc = (TrackBarControl)sender;
            if (tbc.Value == 0)
            {
                radiusLabel.Text = "Disabled";
                intDistance = 0;
            }
            else
            {
                radiusLabel.Text = "Postcode: " + strPostcode + " - Seach Radius: " + tbc.Value.ToString() + " miles.";
                intDistance = tbc.Value;
            }
        }

        private void btnOKSearchRadius_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditSearchRadius_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = popupContainerControlSearchRadius_Get_Selected();
        }

        private string popupContainerControlSearchRadius_Get_Selected()
        {
            if (intDistance > 0)
            {
                return strPostcode + ": " + intDistance.ToString() + " miles";
            }
            else
            {
                return "Disabled";
            }
        }

        private void trackBarControlRadius_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        #endregion

    }
}


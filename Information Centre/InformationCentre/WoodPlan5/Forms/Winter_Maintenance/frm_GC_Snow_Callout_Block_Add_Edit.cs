﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Snow_Callout_Block_Add_Edit : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strGCPONumberSuffix = null;
        public int? intReactive = null;
        public int? intSubContractorContactedByStaffID = null;
        public DateTime? dtSubContractorETA = null;
        public int? intClientHowSoonID = null;
        public string strClientPONumber = null;
        public int? intNonStandardCost = null;
        public int? intNonStandardSell = null;
        public string strRemarks = null;
        public decimal? decHoursWorked = null;
        #endregion

        public frm_GC_Snow_Callout_Block_Add_Edit()
        {
            InitializeComponent();
        }

        private void frm_GC_Snow_Callout_Block_Add_Edit_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400044;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter.Fill(dataSet_GC_Snow_DataEntry.sp04165_GC_Client_How_Soon_Types_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp00226_Staff_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00226_Staff_List_With_BlankTableAdapter.Fill(dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            GCPONumberSuffixTextEdit.EditValue = null;
            ReactiveCheckEdit.EditValue = null;
            SubContractorContactedByStaffIDGridLookUpEdit.EditValue = null;
            SubContractorETADateEdit.EditValue = null;
            ClientHowSoonIDGridLookUpEdit.EditValue = null;
            ClientPONumberTextEdit.EditValue = null;
            NonStandardCostCheckEdit.EditValue = null;
            NonStandardSellCheckEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            HoursWorkedSpinEdit.EditValue = null;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (GCPONumberSuffixTextEdit.EditValue != null) strGCPONumberSuffix = GCPONumberSuffixTextEdit.EditValue.ToString();
            if (ReactiveCheckEdit.EditValue != null) intReactive = Convert.ToInt32(ReactiveCheckEdit.EditValue);
            if (SubContractorContactedByStaffIDGridLookUpEdit.EditValue != null) intSubContractorContactedByStaffID = Convert.ToInt32(SubContractorContactedByStaffIDGridLookUpEdit.EditValue);
            if (SubContractorETADateEdit.DateTime != DateTime.MinValue) dtSubContractorETA = SubContractorETADateEdit.DateTime;
            if (ClientHowSoonIDGridLookUpEdit.EditValue != null) intClientHowSoonID = Convert.ToInt32(ClientHowSoonIDGridLookUpEdit.EditValue);
            if (ClientPONumberTextEdit.EditValue != null) strClientPONumber = ClientPONumberTextEdit.EditValue.ToString();
            if (NonStandardCostCheckEdit.EditValue != null) intNonStandardCost = Convert.ToInt32(NonStandardCostCheckEdit.EditValue);
            if (NonStandardSellCheckEdit.EditValue != null) intNonStandardSell = Convert.ToInt32(NonStandardSellCheckEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
            if (HoursWorkedSpinEdit.EditValue != null) decHoursWorked = Convert.ToDecimal(HoursWorkedSpinEdit.EditValue);
 
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }






    }
}

namespace WoodPlan5
{
    partial class frm_GC_Client_PO_Site_Link_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Client_PO_Site_Link_Edit));
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp04099GCClientPOSiteLinkEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.SnowClearanceDefaultCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GrittingDefaultCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SnowClearanceAllowedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GrittingAllowedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.PONumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ClientPOSiteLinkIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ClientPOIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForClientPOSiteLinkID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPOID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingAllowed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearanceAllowed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingDefault = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSnowClearanceDefault = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04099_GC_Client_PO_Site_Link_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04099_GC_Client_PO_Site_Link_EditTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04099GCClientPOSiteLinkEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceDefaultCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingDefaultCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceAllowedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingAllowedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PONumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOSiteLinkIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOSiteLinkID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingAllowed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceAllowed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 502);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Save Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiFormSave.SuperTip = superToolTip4;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Cancel Button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiFormCancel.SuperTip = superToolTip6;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Form Mode - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.barStaticItemFormMode.SuperTip = superToolTip7;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 502);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 476);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearanceDefaultCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingDefaultCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearanceAllowedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingAllowedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.PONumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.ClientPOSiteLinkIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPOIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp04099GCClientPOSiteLinkEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientPOSiteLinkID,
            this.ItemForSiteID,
            this.ItemForClientPOID,
            this.ItemForClientID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(972, 324, 461, 368);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 476);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(136, 196);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(480, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling4);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 40;
            // 
            // sp04099GCClientPOSiteLinkEditBindingSource
            // 
            this.sp04099GCClientPOSiteLinkEditBindingSource.DataMember = "sp04099_GC_Client_PO_Site_Link_Edit";
            this.sp04099GCClientPOSiteLinkEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SnowClearanceDefaultCheckEdit
            // 
            this.SnowClearanceDefaultCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "SnowClearanceDefault", true));
            this.SnowClearanceDefaultCheckEdit.Location = new System.Drawing.Point(136, 173);
            this.SnowClearanceDefaultCheckEdit.MenuManager = this.barManager1;
            this.SnowClearanceDefaultCheckEdit.Name = "SnowClearanceDefaultCheckEdit";
            this.SnowClearanceDefaultCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearanceDefaultCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearanceDefaultCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearanceDefaultCheckEdit.Size = new System.Drawing.Size(123, 19);
            this.SnowClearanceDefaultCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearanceDefaultCheckEdit.TabIndex = 39;
            this.SnowClearanceDefaultCheckEdit.EditValueChanged += new System.EventHandler(this.SnowClearanceDefaultCheckEdit_EditValueChanged);
            this.SnowClearanceDefaultCheckEdit.Validated += new System.EventHandler(this.SnowClearanceDefaultCheckEdit_Validated);
            // 
            // GrittingDefaultCheckEdit
            // 
            this.GrittingDefaultCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "GrittingDefault", true));
            this.GrittingDefaultCheckEdit.Location = new System.Drawing.Point(136, 117);
            this.GrittingDefaultCheckEdit.MenuManager = this.barManager1;
            this.GrittingDefaultCheckEdit.Name = "GrittingDefaultCheckEdit";
            this.GrittingDefaultCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GrittingDefaultCheckEdit.Properties.ValueChecked = 1;
            this.GrittingDefaultCheckEdit.Properties.ValueUnchecked = 0;
            this.GrittingDefaultCheckEdit.Size = new System.Drawing.Size(123, 19);
            this.GrittingDefaultCheckEdit.StyleController = this.dataLayoutControl1;
            this.GrittingDefaultCheckEdit.TabIndex = 38;
            this.GrittingDefaultCheckEdit.EditValueChanged += new System.EventHandler(this.GrittingDefaultCheckEdit_EditValueChanged);
            this.GrittingDefaultCheckEdit.Validated += new System.EventHandler(this.GrittingDefaultCheckEdit_Validated);
            // 
            // SnowClearanceAllowedCheckEdit
            // 
            this.SnowClearanceAllowedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "SnowClearanceAllowed", true));
            this.SnowClearanceAllowedCheckEdit.Location = new System.Drawing.Point(136, 150);
            this.SnowClearanceAllowedCheckEdit.MenuManager = this.barManager1;
            this.SnowClearanceAllowedCheckEdit.Name = "SnowClearanceAllowedCheckEdit";
            this.SnowClearanceAllowedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearanceAllowedCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearanceAllowedCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearanceAllowedCheckEdit.Size = new System.Drawing.Size(123, 19);
            this.SnowClearanceAllowedCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearanceAllowedCheckEdit.TabIndex = 37;
            this.SnowClearanceAllowedCheckEdit.EditValueChanged += new System.EventHandler(this.SnowClearanceAllowedCheckEdit_EditValueChanged);
            this.SnowClearanceAllowedCheckEdit.Validated += new System.EventHandler(this.SnowClearanceAllowedCheckEdit_Validated);
            // 
            // GrittingAllowedCheckEdit
            // 
            this.GrittingAllowedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "GrittingAllowed", true));
            this.GrittingAllowedCheckEdit.Location = new System.Drawing.Point(136, 94);
            this.GrittingAllowedCheckEdit.MenuManager = this.barManager1;
            this.GrittingAllowedCheckEdit.Name = "GrittingAllowedCheckEdit";
            this.GrittingAllowedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GrittingAllowedCheckEdit.Properties.ValueChecked = 1;
            this.GrittingAllowedCheckEdit.Properties.ValueUnchecked = 0;
            this.GrittingAllowedCheckEdit.Size = new System.Drawing.Size(123, 19);
            this.GrittingAllowedCheckEdit.StyleController = this.dataLayoutControl1;
            this.GrittingAllowedCheckEdit.TabIndex = 36;
            this.GrittingAllowedCheckEdit.EditValueChanged += new System.EventHandler(this.GrittingAllowedCheckEdit_EditValueChanged);
            this.GrittingAllowedCheckEdit.Validated += new System.EventHandler(this.GrittingAllowedCheckEdit_Validated);
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "SiteID", true));
            this.SiteIDTextEdit.Location = new System.Drawing.Point(108, 134);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(508, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling1);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 35;
            // 
            // SiteNameButtonEdit
            // 
            this.SiteNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "SiteName", true));
            this.SiteNameButtonEdit.Location = new System.Drawing.Point(136, 36);
            this.SiteNameButtonEdit.MenuManager = this.barManager1;
            this.SiteNameButtonEdit.Name = "SiteNameButtonEdit";
            this.SiteNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "view", null, true)});
            this.SiteNameButtonEdit.Properties.ReadOnly = true;
            this.SiteNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SiteNameButtonEdit.Size = new System.Drawing.Size(480, 20);
            this.SiteNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameButtonEdit.TabIndex = 34;
            this.SiteNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteNameButtonEdit_ButtonClick);
            this.SiteNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteNameButtonEdit_Validating);
            // 
            // PONumberButtonEdit
            // 
            this.PONumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "PONumber", true));
            this.PONumberButtonEdit.Location = new System.Drawing.Point(136, 60);
            this.PONumberButtonEdit.MenuManager = this.barManager1;
            this.PONumberButtonEdit.Name = "PONumberButtonEdit";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "View Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>open</b> the <b>Linked Client Purchase Order</b> record for the cu" +
    "rrently linked site";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.PONumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to Select Client Purchase Order Number", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "view", superToolTip5, true)});
            this.PONumberButtonEdit.Properties.ReadOnly = true;
            this.PONumberButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.PONumberButtonEdit.Size = new System.Drawing.Size(480, 20);
            this.PONumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.PONumberButtonEdit.TabIndex = 29;
            this.PONumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PONumberButtonEdit_ButtonClick);
            this.PONumberButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PONumberButtonEdit_Validating);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04099GCClientPOSiteLinkEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(137, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(176, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ClientPOSiteLinkIDSpinEdit
            // 
            this.ClientPOSiteLinkIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "ClientPOSiteLinkID", true));
            this.ClientPOSiteLinkIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientPOSiteLinkIDSpinEdit.Location = new System.Drawing.Point(133, 110);
            this.ClientPOSiteLinkIDSpinEdit.MenuManager = this.barManager1;
            this.ClientPOSiteLinkIDSpinEdit.Name = "ClientPOSiteLinkIDSpinEdit";
            this.ClientPOSiteLinkIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ClientPOSiteLinkIDSpinEdit.Properties.ReadOnly = true;
            this.ClientPOSiteLinkIDSpinEdit.Size = new System.Drawing.Size(483, 20);
            this.ClientPOSiteLinkIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.ClientPOSiteLinkIDSpinEdit.TabIndex = 4;
            // 
            // ClientPOIDTextEdit
            // 
            this.ClientPOIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04099GCClientPOSiteLinkEditBindingSource, "ClientPOID", true));
            this.ClientPOIDTextEdit.Location = new System.Drawing.Point(136, 196);
            this.ClientPOIDTextEdit.MenuManager = this.barManager1;
            this.ClientPOIDTextEdit.Name = "ClientPOIDTextEdit";
            this.ClientPOIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPOIDTextEdit, true);
            this.ClientPOIDTextEdit.Size = new System.Drawing.Size(480, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPOIDTextEdit, optionsSpelling2);
            this.ClientPOIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPOIDTextEdit.TabIndex = 16;
            // 
            // ItemForClientPOSiteLinkID
            // 
            this.ItemForClientPOSiteLinkID.Control = this.ClientPOSiteLinkIDSpinEdit;
            this.ItemForClientPOSiteLinkID.CustomizationFormText = "Client PO Site Link ID:";
            this.ItemForClientPOSiteLinkID.Location = new System.Drawing.Point(0, 98);
            this.ItemForClientPOSiteLinkID.Name = "ItemForClientPOSiteLinkID";
            this.ItemForClientPOSiteLinkID.Size = new System.Drawing.Size(608, 24);
            this.ItemForClientPOSiteLinkID.Text = "Client PO Site Link ID:";
            this.ItemForClientPOSiteLinkID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.CustomizationFormText = "Site ID:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 122);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(608, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientPOID
            // 
            this.ItemForClientPOID.Control = this.ClientPOIDTextEdit;
            this.ItemForClientPOID.CustomizationFormText = "Client PO ID:";
            this.ItemForClientPOID.Location = new System.Drawing.Point(0, 184);
            this.ItemForClientPOID.Name = "ItemForClientPOID";
            this.ItemForClientPOID.Size = new System.Drawing.Size(608, 24);
            this.ItemForClientPOID.Text = "Client PO ID:";
            this.ItemForClientPOID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 184);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(608, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 476);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.ItemForPONumber,
            this.ItemForSiteName,
            this.ItemForGrittingAllowed,
            this.ItemForSnowClearanceAllowed,
            this.emptySpaceItem2,
            this.emptySpaceItem4,
            this.emptySpaceItem6,
            this.ItemForGrittingDefault,
            this.emptySpaceItem7,
            this.ItemForSnowClearanceDefault});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 456);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 184);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 272);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(305, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(303, 24);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(125, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(180, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(125, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(125, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(125, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPONumber
            // 
            this.ItemForPONumber.Control = this.PONumberButtonEdit;
            this.ItemForPONumber.CustomizationFormText = "Client P.O. Number:";
            this.ItemForPONumber.Location = new System.Drawing.Point(0, 48);
            this.ItemForPONumber.Name = "ItemForPONumber";
            this.ItemForPONumber.Size = new System.Drawing.Size(608, 24);
            this.ItemForPONumber.Text = "Client P.O Number:";
            this.ItemForPONumber.TextSize = new System.Drawing.Size(121, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameButtonEdit;
            this.ItemForSiteName.CustomizationFormText = "Linked Site:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(608, 24);
            this.ItemForSiteName.Text = "Linked Site:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(121, 13);
            // 
            // ItemForGrittingAllowed
            // 
            this.ItemForGrittingAllowed.Control = this.GrittingAllowedCheckEdit;
            this.ItemForGrittingAllowed.CustomizationFormText = "layoutControlItem3";
            this.ItemForGrittingAllowed.Location = new System.Drawing.Point(0, 82);
            this.ItemForGrittingAllowed.MaxSize = new System.Drawing.Size(0, 23);
            this.ItemForGrittingAllowed.MinSize = new System.Drawing.Size(206, 23);
            this.ItemForGrittingAllowed.Name = "ItemForGrittingAllowed";
            this.ItemForGrittingAllowed.Size = new System.Drawing.Size(251, 23);
            this.ItemForGrittingAllowed.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGrittingAllowed.Text = "Gritting Allowed:";
            this.ItemForGrittingAllowed.TextSize = new System.Drawing.Size(121, 13);
            // 
            // ItemForSnowClearanceAllowed
            // 
            this.ItemForSnowClearanceAllowed.Control = this.SnowClearanceAllowedCheckEdit;
            this.ItemForSnowClearanceAllowed.CustomizationFormText = "layoutControlItem4";
            this.ItemForSnowClearanceAllowed.Location = new System.Drawing.Point(0, 138);
            this.ItemForSnowClearanceAllowed.Name = "ItemForSnowClearanceAllowed";
            this.ItemForSnowClearanceAllowed.Size = new System.Drawing.Size(251, 23);
            this.ItemForSnowClearanceAllowed.Text = "Snow Clearance Allowed:";
            this.ItemForSnowClearanceAllowed.TextSize = new System.Drawing.Size(121, 13);
            // 
            // ItemForGrittingDefault
            // 
            this.ItemForGrittingDefault.Control = this.GrittingDefaultCheckEdit;
            this.ItemForGrittingDefault.CustomizationFormText = "Gritting Default:";
            this.ItemForGrittingDefault.Location = new System.Drawing.Point(0, 105);
            this.ItemForGrittingDefault.Name = "ItemForGrittingDefault";
            this.ItemForGrittingDefault.Size = new System.Drawing.Size(251, 23);
            this.ItemForGrittingDefault.Text = "Gritting Default:";
            this.ItemForGrittingDefault.TextSize = new System.Drawing.Size(121, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 128);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSnowClearanceDefault
            // 
            this.ItemForSnowClearanceDefault.Control = this.SnowClearanceDefaultCheckEdit;
            this.ItemForSnowClearanceDefault.CustomizationFormText = "Snow Clearance Default:";
            this.ItemForSnowClearanceDefault.Location = new System.Drawing.Point(0, 161);
            this.ItemForSnowClearanceDefault.Name = "ItemForSnowClearanceDefault";
            this.ItemForSnowClearanceDefault.Size = new System.Drawing.Size(251, 23);
            this.ItemForSnowClearanceDefault.Text = "Snow Clearance Default:";
            this.ItemForSnowClearanceDefault.TextSize = new System.Drawing.Size(121, 13);
            // 
            // sp04099_GC_Client_PO_Site_Link_EditTableAdapter
            // 
            this.sp04099_GC_Client_PO_Site_Link_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(251, 82);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(357, 46);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(251, 138);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(357, 46);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_GC_Client_PO_Site_Link_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 532);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Client_PO_Site_Link_Edit";
            this.Text = "Edit Client Purchase Order Site Link";
            this.Activated += new System.EventHandler(this.frm_GC_Client_PO_Site_Link_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Client_PO_Site_Link_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Client_PO_Site_Link_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04099GCClientPOSiteLinkEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceDefaultCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingDefaultCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceAllowedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingAllowedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PONumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOSiteLinkIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOSiteLinkID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingAllowed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceAllowed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SpinEdit ClientPOSiteLinkIDSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOSiteLinkID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.TextEdit ClientPOIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit PONumberButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPONumber;
        private System.Windows.Forms.BindingSource sp04099GCClientPOSiteLinkEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04099_GC_Client_PO_Site_Link_EditTableAdapter sp04099_GC_Client_PO_Site_Link_EditTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit SiteNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraEditors.CheckEdit SnowClearanceAllowedCheckEdit;
        private DevExpress.XtraEditors.CheckEdit GrittingAllowedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingAllowed;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearanceAllowed;
        private DevExpress.XtraEditors.CheckEdit SnowClearanceDefaultCheckEdit;
        private DevExpress.XtraEditors.CheckEdit GrittingDefaultCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingDefault;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearanceDefault;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
    }
}

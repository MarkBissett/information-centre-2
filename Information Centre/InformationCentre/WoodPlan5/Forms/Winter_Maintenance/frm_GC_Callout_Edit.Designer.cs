namespace WoodPlan5
{
    partial class frm_GC_Callout_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Callout_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling28 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling29 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling30 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling31 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling32 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling33 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling34 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling35 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling36 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling37 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colCalloutStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.DoubleGritCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.sp04076GCGrittingCalloutEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.btnViewOnMap = new DevExpress.XtraEditors.SimpleButton();
            this.CompletionEmailSentCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AttendanceOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.IsFloatingSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SiteLongSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SiteLatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SnowClearedSatisfactoryCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SnowClearedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.InternalRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.Weather5CheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.Weather2CheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.Weather4CheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.Weather3CheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.Weather1CheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PDACodeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SnowOnSiteOver5cmCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SiteManagerNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CalloutInvoicedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.FinanceTeamPaymentExportedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.JobSheetNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddress5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddress4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddress3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddress2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddress1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TeamMembersPresentMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SnowInfoMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.NoAccessAbortedRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PPEWornCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientPOIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NonStandardSellCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientPOIDDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.StartTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.AuthorisedByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NonStandardCostCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PaidByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SnowOnSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientPriceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.SiteGrittingContractIDGridLookUpEdit = new DevExpress.XtraEditors.TextEdit();
            this.GrittingCallOutIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SubContractorNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.OriginalSubContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.btnShowOnMap = new DevExpress.XtraEditors.SimpleButton();
            this.CallOutDateTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ImportedWeatherForecastIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TextSentTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SubContractorReceivedTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SubContractorRespondedTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SubContractorETATextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CompletedTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.AuthorisedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitAbortedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.StartLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinishedLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinishedLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RecordedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SaltUsedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SaltCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SaltSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SaltVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.HoursWorkedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TeamHourlyRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TeamChargeSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LabourCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LabourVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.OtherCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.OtherSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TotalSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ClientInvoiceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubContractorPaidCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RecordedByNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.GritSourceLocationIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.GritSourceLocationTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PaidByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteWeatherTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DoNotPaySubContractorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DoNotPaySubContractorReasonMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DoNotInvoiceClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DoNotInvoiceClientReasonMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SiteTemperatureSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PdaIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TeamPresentCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NoAccessCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ServiceFailureCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.CommentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.OriginalSubContractorNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.JobStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCalloutStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GrittingInvoiceIDButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AbortedReasonMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.EveningRateModifierSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SitePostcodeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForGrittingCallOutID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOriginalSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForImportedWeatherForecastID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAuthorisedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteGrittingContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRecordedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritSourceLocationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritSourceLocationTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPaidByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPOID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteWeather = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPdaID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSubContractorName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOriginalSubContractorName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForServiceFailure = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPOIDDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDACode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRecordedByName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWeather1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWeather2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWeather3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWeather4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWeather5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteTemperature = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowOnSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowOnSiteOver5cm = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowCleared = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearedSatisfactory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup23 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNoAccess = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForVisitAborted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoAccessAbortedRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForJobSheetNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteManagerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsFloatingSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAttendanceOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDoubleGrit = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem25 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCallOutDateTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTextSentTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCompletedTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorReceivedTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorRespondedTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorETA = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTeamPresent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPPEWorn = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStartLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinishedLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinishedLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem26 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCompletionEmailSent = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHoursWorked = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamHourlyRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLabourVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEveningRateModifier = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForOtherCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOtherSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForNonStandardCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNonStandardSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem3 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup21 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSaltUsed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSaltCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSaltSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSaltVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup18 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSubContractorPaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingInvoiceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotPaySubContractor = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotPaySubContractorReason = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPaidByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAuthorisedByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinanceTeamPaymentExported = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup20 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClientInvoiceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotInvoiceClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDoNotInvoiceClientReason = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCalloutInvoiced = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAbortedReason = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInternalRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup22 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteAddress1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddress2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddress3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddress4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddress5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSitePostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteLat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteLong = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSnowInfo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamMembersPresent = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp04076_GC_Gritting_Callout_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04076_GC_Gritting_Callout_EditTableAdapter();
            this.sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DoubleGritCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04076GCGrittingCalloutEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletionEmailSentCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttendanceOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFloatingSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLongSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearedSatisfactoryCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternalRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather5CheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather2CheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather4CheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather3CheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather1CheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDACodeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowOnSiteOver5cmCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteManagerNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalloutInvoicedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceTeamPaymentExportedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSheetNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamMembersPresentMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowInfoMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoAccessAbortedRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PPEWornCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardSellCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AuthorisedByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardCostCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowOnSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPriceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteGrittingContractIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingCallOutIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalSubContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallOutDateTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallOutDateTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportedWeatherForecastIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSentTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSentTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorReceivedTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorReceivedTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorRespondedTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorRespondedTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETATextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AuthorisedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitAbortedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishedLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishedLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltUsedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursWorkedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamHourlyRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamChargeSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorPaidCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedByNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritSourceLocationIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritSourceLocationTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteWeatherTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPaySubContractorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPaySubContractorReasonMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientReasonMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteTemperatureSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PdaIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamPresentCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoAccessCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceFailureCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalSubContractorNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingInvoiceIDButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbortedReasonMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EveningRateModifierSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingCallOutID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOriginalSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForImportedWeatherForecastID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAuthorisedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteGrittingContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSourceLocationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSourceLocationTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteWeather)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPdaID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOriginalSubContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceFailure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOIDDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDACode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedByName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteTemperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowOnSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowOnSiteOver5cm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowCleared)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearedSatisfactory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoAccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitAborted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoAccessAbortedRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSheetNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteManagerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsFloatingSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAttendanceOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoubleGrit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCallOutDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTextSentTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletedTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorReceivedTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorRespondedTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorETA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamPresent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPPEWorn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishedLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishedLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletionEmailSent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursWorked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamHourlyRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEveningRateModifier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingInvoiceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPaySubContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPaySubContractorReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAuthorisedByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceTeamPaymentExported)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInvoiceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClientReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalloutInvoiced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbortedReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInternalRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMembersPresent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1139, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 660);
            this.barDockControlBottom.Size = new System.Drawing.Size(1139, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 634);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1139, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 634);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colCalloutStatusID
            // 
            this.colCalloutStatusID.Caption = "Status ID";
            this.colCalloutStatusID.FieldName = "CalloutStatusID";
            this.colCalloutStatusID.Name = "colCalloutStatusID";
            this.colCalloutStatusID.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusID.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusID.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(483, 132);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Save_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1139, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 660);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1139, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 634);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1139, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 634);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.DoubleGritCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.btnViewOnMap);
            this.dataLayoutControl1.Controls.Add(this.CompletionEmailSentCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.AttendanceOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.IsFloatingSiteCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteLongSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteLatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearedSatisfactoryCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.InternalRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.Weather5CheckEdit);
            this.dataLayoutControl1.Controls.Add(this.Weather2CheckEdit);
            this.dataLayoutControl1.Controls.Add(this.Weather4CheckEdit);
            this.dataLayoutControl1.Controls.Add(this.Weather3CheckEdit);
            this.dataLayoutControl1.Controls.Add(this.Weather1CheckEdit);
            this.dataLayoutControl1.Controls.Add(this.PDACodeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowOnSiteOver5cmCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteManagerNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CalloutInvoicedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.FinanceTeamPaymentExportedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSheetNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddress5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddress4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddress3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddress2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddress1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamMembersPresentMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowInfoMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.NoAccessAbortedRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PPEWornCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPOIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NonStandardSellCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPOIDDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.StartTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.AuthorisedByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NonStandardCostCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.PaidByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowOnSiteCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPriceSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SiteGrittingContractIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingCallOutIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.OriginalSubContractorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.btnShowOnMap);
            this.dataLayoutControl1.Controls.Add(this.CallOutDateTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ImportedWeatherForecastIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TextSentTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorReceivedTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorRespondedTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorETATextEdit);
            this.dataLayoutControl1.Controls.Add(this.CompletedTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.AuthorisedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitAbortedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.StartLatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StartLongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinishedLatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinishedLongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPONumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RecordedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SaltUsedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SaltCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SaltSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SaltVatRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.HoursWorkedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamHourlyRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamChargeSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LabourCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LabourVatRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.OtherCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.OtherSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientInvoiceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorPaidCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.RecordedByNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.GritSourceLocationIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.GritSourceLocationTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PaidByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteWeatherTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotPaySubContractorCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotPaySubContractorReasonMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotInvoiceClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotInvoiceClientReasonMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteTemperatureSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PdaIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamPresentCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.NoAccessCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ServiceFailureCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.CommentsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.OriginalSubContractorNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.JobStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingInvoiceIDButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AbortedReasonMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.EveningRateModifierSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SitePostcodeButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp04076GCGrittingCalloutEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGrittingCallOutID,
            this.ItemForOriginalSubContractorID,
            this.ItemForImportedWeatherForecastID,
            this.ItemForAuthorisedByStaffID,
            this.ItemForSiteGrittingContractID,
            this.ItemForRecordedByStaffID,
            this.ItemForGritSourceLocationID,
            this.ItemForGritSourceLocationTypeID,
            this.ItemForPaidByStaffID,
            this.ItemForClientPOID,
            this.ItemForSiteWeather,
            this.ItemForPdaID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(61, 175, 624, 622);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1139, 634);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // DoubleGritCheckEdit
            // 
            this.DoubleGritCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "DoubleGrit", true));
            this.DoubleGritCheckEdit.Location = new System.Drawing.Point(672, 179);
            this.DoubleGritCheckEdit.MenuManager = this.barManager1;
            this.DoubleGritCheckEdit.Name = "DoubleGritCheckEdit";
            this.DoubleGritCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.DoubleGritCheckEdit.Properties.ValueChecked = 1;
            this.DoubleGritCheckEdit.Properties.ValueUnchecked = 0;
            this.DoubleGritCheckEdit.Size = new System.Drawing.Size(97, 19);
            this.DoubleGritCheckEdit.StyleController = this.dataLayoutControl1;
            this.DoubleGritCheckEdit.TabIndex = 136;
            // 
            // sp04076GCGrittingCalloutEditBindingSource
            // 
            this.sp04076GCGrittingCalloutEditBindingSource.DataMember = "sp04076_GC_Gritting_Callout_Edit";
            this.sp04076GCGrittingCalloutEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnViewOnMap
            // 
            this.btnViewOnMap.Location = new System.Drawing.Point(36, 577);
            this.btnViewOnMap.Name = "btnViewOnMap";
            this.btnViewOnMap.Size = new System.Drawing.Size(139, 22);
            this.btnViewOnMap.StyleController = this.dataLayoutControl1;
            this.btnViewOnMap.TabIndex = 135;
            this.btnViewOnMap.Text = "View Site On Map";
            this.btnViewOnMap.Click += new System.EventHandler(this.btnViewOnMap_Click);
            // 
            // CompletionEmailSentCheckEdit
            // 
            this.CompletionEmailSentCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "CompletionEmailSent", true));
            this.CompletionEmailSentCheckEdit.Location = new System.Drawing.Point(1034, 179);
            this.CompletionEmailSentCheckEdit.MenuManager = this.barManager1;
            this.CompletionEmailSentCheckEdit.Name = "CompletionEmailSentCheckEdit";
            this.CompletionEmailSentCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.CompletionEmailSentCheckEdit.Properties.ValueChecked = 1;
            this.CompletionEmailSentCheckEdit.Properties.ValueUnchecked = 0;
            this.CompletionEmailSentCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.CompletionEmailSentCheckEdit.StyleController = this.dataLayoutControl1;
            this.CompletionEmailSentCheckEdit.TabIndex = 134;
            // 
            // AttendanceOrderSpinEdit
            // 
            this.AttendanceOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "AttendanceOrder", true));
            this.AttendanceOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AttendanceOrderSpinEdit.Location = new System.Drawing.Point(151, 179);
            this.AttendanceOrderSpinEdit.MenuManager = this.barManager1;
            this.AttendanceOrderSpinEdit.Name = "AttendanceOrderSpinEdit";
            this.AttendanceOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AttendanceOrderSpinEdit.Properties.IsFloatValue = false;
            this.AttendanceOrderSpinEdit.Properties.Mask.EditMask = "n0";
            this.AttendanceOrderSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AttendanceOrderSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.AttendanceOrderSpinEdit.Size = new System.Drawing.Size(378, 20);
            this.AttendanceOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.AttendanceOrderSpinEdit.TabIndex = 133;
            // 
            // IsFloatingSiteCheckEdit
            // 
            this.IsFloatingSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "IsFloatingSite", true));
            this.IsFloatingSiteCheckEdit.Location = new System.Drawing.Point(387, 131);
            this.IsFloatingSiteCheckEdit.MenuManager = this.barManager1;
            this.IsFloatingSiteCheckEdit.Name = "IsFloatingSiteCheckEdit";
            this.IsFloatingSiteCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsFloatingSiteCheckEdit.Properties.ValueChecked = 1;
            this.IsFloatingSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.IsFloatingSiteCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.IsFloatingSiteCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsFloatingSiteCheckEdit.TabIndex = 132;
            // 
            // SiteLongSpinEdit
            // 
            this.SiteLongSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteLong", true));
            this.SiteLongSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SiteLongSpinEdit.Location = new System.Drawing.Point(696, 747);
            this.SiteLongSpinEdit.MenuManager = this.barManager1;
            this.SiteLongSpinEdit.Name = "SiteLongSpinEdit";
            this.SiteLongSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SiteLongSpinEdit.Size = new System.Drawing.Size(390, 20);
            this.SiteLongSpinEdit.StyleController = this.dataLayoutControl1;
            this.SiteLongSpinEdit.TabIndex = 131;
            // 
            // SiteLatSpinEdit
            // 
            this.SiteLatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteLat", true));
            this.SiteLatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SiteLatSpinEdit.Location = new System.Drawing.Point(175, 747);
            this.SiteLatSpinEdit.MenuManager = this.barManager1;
            this.SiteLatSpinEdit.Name = "SiteLatSpinEdit";
            this.SiteLatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SiteLatSpinEdit.Size = new System.Drawing.Size(378, 20);
            this.SiteLatSpinEdit.StyleController = this.dataLayoutControl1;
            this.SiteLatSpinEdit.TabIndex = 130;
            // 
            // SnowClearedSatisfactoryCheckEdit
            // 
            this.SnowClearedSatisfactoryCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SnowClearedSatisfactory", true));
            this.SnowClearedSatisfactoryCheckEdit.Location = new System.Drawing.Point(163, 455);
            this.SnowClearedSatisfactoryCheckEdit.MenuManager = this.barManager1;
            this.SnowClearedSatisfactoryCheckEdit.Name = "SnowClearedSatisfactoryCheckEdit";
            this.SnowClearedSatisfactoryCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearedSatisfactoryCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearedSatisfactoryCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearedSatisfactoryCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.SnowClearedSatisfactoryCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearedSatisfactoryCheckEdit.TabIndex = 129;
            // 
            // SnowClearedCheckEdit
            // 
            this.SnowClearedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SnowCleared", true));
            this.SnowClearedCheckEdit.Location = new System.Drawing.Point(163, 432);
            this.SnowClearedCheckEdit.MenuManager = this.barManager1;
            this.SnowClearedCheckEdit.Name = "SnowClearedCheckEdit";
            this.SnowClearedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearedCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearedCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearedCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.SnowClearedCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearedCheckEdit.TabIndex = 128;
            // 
            // InternalRemarksMemoEdit
            // 
            this.InternalRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "InternalRemarks", true));
            this.InternalRemarksMemoEdit.Location = new System.Drawing.Point(175, 698);
            this.InternalRemarksMemoEdit.MenuManager = this.barManager1;
            this.InternalRemarksMemoEdit.Name = "InternalRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.InternalRemarksMemoEdit, true);
            this.InternalRemarksMemoEdit.Size = new System.Drawing.Size(911, 149);
            this.scSpellChecker.SetSpellCheckerOptions(this.InternalRemarksMemoEdit, optionsSpelling1);
            this.InternalRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.InternalRemarksMemoEdit.TabIndex = 127;
            // 
            // Weather5CheckEdit
            // 
            this.Weather5CheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "Weather5", true));
            this.Weather5CheckEdit.Location = new System.Drawing.Point(163, 339);
            this.Weather5CheckEdit.MenuManager = this.barManager1;
            this.Weather5CheckEdit.Name = "Weather5CheckEdit";
            this.Weather5CheckEdit.Properties.Caption = "(Tick if Yes)";
            this.Weather5CheckEdit.Properties.ValueChecked = 1;
            this.Weather5CheckEdit.Properties.ValueUnchecked = 0;
            this.Weather5CheckEdit.Size = new System.Drawing.Size(76, 19);
            this.Weather5CheckEdit.StyleController = this.dataLayoutControl1;
            this.Weather5CheckEdit.TabIndex = 126;
            // 
            // Weather2CheckEdit
            // 
            this.Weather2CheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "Weather2", true));
            this.Weather2CheckEdit.Location = new System.Drawing.Point(163, 270);
            this.Weather2CheckEdit.MenuManager = this.barManager1;
            this.Weather2CheckEdit.Name = "Weather2CheckEdit";
            this.Weather2CheckEdit.Properties.Caption = "(Tick if Yes)";
            this.Weather2CheckEdit.Properties.ValueChecked = 1;
            this.Weather2CheckEdit.Properties.ValueUnchecked = 0;
            this.Weather2CheckEdit.Size = new System.Drawing.Size(76, 19);
            this.Weather2CheckEdit.StyleController = this.dataLayoutControl1;
            this.Weather2CheckEdit.TabIndex = 125;
            // 
            // Weather4CheckEdit
            // 
            this.Weather4CheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "Weather4", true));
            this.Weather4CheckEdit.Location = new System.Drawing.Point(163, 316);
            this.Weather4CheckEdit.MenuManager = this.barManager1;
            this.Weather4CheckEdit.Name = "Weather4CheckEdit";
            this.Weather4CheckEdit.Properties.Caption = "(Tick if Yes)";
            this.Weather4CheckEdit.Properties.ValueChecked = 1;
            this.Weather4CheckEdit.Properties.ValueUnchecked = 0;
            this.Weather4CheckEdit.Size = new System.Drawing.Size(76, 19);
            this.Weather4CheckEdit.StyleController = this.dataLayoutControl1;
            this.Weather4CheckEdit.TabIndex = 124;
            // 
            // Weather3CheckEdit
            // 
            this.Weather3CheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "Weather3", true));
            this.Weather3CheckEdit.Location = new System.Drawing.Point(163, 293);
            this.Weather3CheckEdit.MenuManager = this.barManager1;
            this.Weather3CheckEdit.Name = "Weather3CheckEdit";
            this.Weather3CheckEdit.Properties.Caption = "(Tick if Yes)";
            this.Weather3CheckEdit.Properties.ValueChecked = 1;
            this.Weather3CheckEdit.Properties.ValueUnchecked = 0;
            this.Weather3CheckEdit.Size = new System.Drawing.Size(76, 19);
            this.Weather3CheckEdit.StyleController = this.dataLayoutControl1;
            this.Weather3CheckEdit.TabIndex = 123;
            // 
            // Weather1CheckEdit
            // 
            this.Weather1CheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "Weather1", true));
            this.Weather1CheckEdit.Location = new System.Drawing.Point(163, 247);
            this.Weather1CheckEdit.MenuManager = this.barManager1;
            this.Weather1CheckEdit.Name = "Weather1CheckEdit";
            this.Weather1CheckEdit.Properties.Caption = "(Tick if Yes)";
            this.Weather1CheckEdit.Properties.ValueChecked = 1;
            this.Weather1CheckEdit.Properties.ValueUnchecked = 0;
            this.Weather1CheckEdit.Size = new System.Drawing.Size(76, 19);
            this.Weather1CheckEdit.StyleController = this.dataLayoutControl1;
            this.Weather1CheckEdit.TabIndex = 122;
            // 
            // PDACodeButtonEdit
            // 
            this.PDACodeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "PDACode", true));
            this.PDACodeButtonEdit.Location = new System.Drawing.Point(151, 83);
            this.PDACodeButtonEdit.MenuManager = this.barManager1;
            this.PDACodeButtonEdit.Name = "PDACodeButtonEdit";
            this.PDACodeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Selected Value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.PDACodeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.PDACodeButtonEdit.Size = new System.Drawing.Size(378, 20);
            this.PDACodeButtonEdit.StyleController = this.dataLayoutControl1;
            this.PDACodeButtonEdit.TabIndex = 121;
            this.PDACodeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PDACodeButtonEdit_ButtonClick);
            // 
            // SnowOnSiteOver5cmCheckEdit
            // 
            this.SnowOnSiteOver5cmCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SnowOnSiteOver5cm", true));
            this.SnowOnSiteOver5cmCheckEdit.Location = new System.Drawing.Point(163, 409);
            this.SnowOnSiteOver5cmCheckEdit.MenuManager = this.barManager1;
            this.SnowOnSiteOver5cmCheckEdit.Name = "SnowOnSiteOver5cmCheckEdit";
            this.SnowOnSiteOver5cmCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowOnSiteOver5cmCheckEdit.Properties.ValueChecked = 1;
            this.SnowOnSiteOver5cmCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowOnSiteOver5cmCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.SnowOnSiteOver5cmCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowOnSiteOver5cmCheckEdit.TabIndex = 120;
            // 
            // SiteManagerNameTextEdit
            // 
            this.SiteManagerNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteManagerName", true));
            this.SiteManagerNameTextEdit.Location = new System.Drawing.Point(672, 155);
            this.SiteManagerNameTextEdit.MenuManager = this.barManager1;
            this.SiteManagerNameTextEdit.Name = "SiteManagerNameTextEdit";
            this.SiteManagerNameTextEdit.Properties.MaxLength = 50;
            this.SiteManagerNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteManagerNameTextEdit, true);
            this.SiteManagerNameTextEdit.Size = new System.Drawing.Size(438, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteManagerNameTextEdit, optionsSpelling2);
            this.SiteManagerNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteManagerNameTextEdit.TabIndex = 119;
            // 
            // CalloutInvoicedCheckEdit
            // 
            this.CalloutInvoicedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "CalloutInvoiced", true));
            this.CalloutInvoicedCheckEdit.Location = new System.Drawing.Point(680, 725);
            this.CalloutInvoicedCheckEdit.MenuManager = this.barManager1;
            this.CalloutInvoicedCheckEdit.Name = "CalloutInvoicedCheckEdit";
            this.CalloutInvoicedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.CalloutInvoicedCheckEdit.Properties.ValueChecked = 1;
            this.CalloutInvoicedCheckEdit.Properties.ValueUnchecked = 0;
            this.CalloutInvoicedCheckEdit.Size = new System.Drawing.Size(394, 19);
            this.CalloutInvoicedCheckEdit.StyleController = this.dataLayoutControl1;
            this.CalloutInvoicedCheckEdit.TabIndex = 118;
            // 
            // FinanceTeamPaymentExportedCheckEdit
            // 
            this.FinanceTeamPaymentExportedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "FinanceTeamPaymentExported", true));
            this.FinanceTeamPaymentExportedCheckEdit.Location = new System.Drawing.Point(187, 796);
            this.FinanceTeamPaymentExportedCheckEdit.MenuManager = this.barManager1;
            this.FinanceTeamPaymentExportedCheckEdit.Name = "FinanceTeamPaymentExportedCheckEdit";
            this.FinanceTeamPaymentExportedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.FinanceTeamPaymentExportedCheckEdit.Properties.ValueChecked = 1;
            this.FinanceTeamPaymentExportedCheckEdit.Properties.ValueUnchecked = 0;
            this.FinanceTeamPaymentExportedCheckEdit.Size = new System.Drawing.Size(320, 19);
            this.FinanceTeamPaymentExportedCheckEdit.StyleController = this.dataLayoutControl1;
            this.FinanceTeamPaymentExportedCheckEdit.TabIndex = 117;
            this.FinanceTeamPaymentExportedCheckEdit.ToolTip = "Team Payment Exported To Finance";
            // 
            // JobSheetNumberTextEdit
            // 
            this.JobSheetNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "JobSheetNumber", true));
            this.JobSheetNumberTextEdit.Location = new System.Drawing.Point(151, 155);
            this.JobSheetNumberTextEdit.MenuManager = this.barManager1;
            this.JobSheetNumberTextEdit.Name = "JobSheetNumberTextEdit";
            this.JobSheetNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSheetNumberTextEdit, true);
            this.JobSheetNumberTextEdit.Size = new System.Drawing.Size(378, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSheetNumberTextEdit, optionsSpelling3);
            this.JobSheetNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.JobSheetNumberTextEdit.TabIndex = 116;
            // 
            // SiteAddress5TextEdit
            // 
            this.SiteAddress5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteAddress5", true));
            this.SiteAddress5TextEdit.Location = new System.Drawing.Point(175, 699);
            this.SiteAddress5TextEdit.MenuManager = this.barManager1;
            this.SiteAddress5TextEdit.Name = "SiteAddress5TextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddress5TextEdit, true);
            this.SiteAddress5TextEdit.Size = new System.Drawing.Size(911, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddress5TextEdit, optionsSpelling4);
            this.SiteAddress5TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddress5TextEdit.TabIndex = 113;
            // 
            // SiteAddress4TextEdit
            // 
            this.SiteAddress4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteAddress4", true));
            this.SiteAddress4TextEdit.Location = new System.Drawing.Point(175, 675);
            this.SiteAddress4TextEdit.MenuManager = this.barManager1;
            this.SiteAddress4TextEdit.Name = "SiteAddress4TextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddress4TextEdit, true);
            this.SiteAddress4TextEdit.Size = new System.Drawing.Size(911, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddress4TextEdit, optionsSpelling5);
            this.SiteAddress4TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddress4TextEdit.TabIndex = 112;
            // 
            // SiteAddress3TextEdit
            // 
            this.SiteAddress3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteAddress3", true));
            this.SiteAddress3TextEdit.Location = new System.Drawing.Point(175, 651);
            this.SiteAddress3TextEdit.MenuManager = this.barManager1;
            this.SiteAddress3TextEdit.Name = "SiteAddress3TextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddress3TextEdit, true);
            this.SiteAddress3TextEdit.Size = new System.Drawing.Size(911, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddress3TextEdit, optionsSpelling6);
            this.SiteAddress3TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddress3TextEdit.TabIndex = 111;
            // 
            // SiteAddress2TextEdit
            // 
            this.SiteAddress2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteAddress2", true));
            this.SiteAddress2TextEdit.Location = new System.Drawing.Point(175, 627);
            this.SiteAddress2TextEdit.MenuManager = this.barManager1;
            this.SiteAddress2TextEdit.Name = "SiteAddress2TextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddress2TextEdit, true);
            this.SiteAddress2TextEdit.Size = new System.Drawing.Size(911, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddress2TextEdit, optionsSpelling7);
            this.SiteAddress2TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddress2TextEdit.TabIndex = 110;
            // 
            // SiteAddress1TextEdit
            // 
            this.SiteAddress1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteAddress1", true));
            this.SiteAddress1TextEdit.Location = new System.Drawing.Point(175, 603);
            this.SiteAddress1TextEdit.MenuManager = this.barManager1;
            this.SiteAddress1TextEdit.Name = "SiteAddress1TextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddress1TextEdit, true);
            this.SiteAddress1TextEdit.Size = new System.Drawing.Size(911, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddress1TextEdit, optionsSpelling8);
            this.SiteAddress1TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddress1TextEdit.TabIndex = 109;
            // 
            // TeamMembersPresentMemoEdit
            // 
            this.TeamMembersPresentMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "TeamMembersPresent", true));
            this.TeamMembersPresentMemoEdit.Location = new System.Drawing.Point(175, 705);
            this.TeamMembersPresentMemoEdit.MenuManager = this.barManager1;
            this.TeamMembersPresentMemoEdit.Name = "TeamMembersPresentMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.TeamMembersPresentMemoEdit, true);
            this.TeamMembersPresentMemoEdit.Size = new System.Drawing.Size(911, 142);
            this.scSpellChecker.SetSpellCheckerOptions(this.TeamMembersPresentMemoEdit, optionsSpelling9);
            this.TeamMembersPresentMemoEdit.StyleController = this.dataLayoutControl1;
            this.TeamMembersPresentMemoEdit.TabIndex = 108;
            // 
            // SnowInfoMemoEdit
            // 
            this.SnowInfoMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SnowInfo", true));
            this.SnowInfoMemoEdit.Location = new System.Drawing.Point(175, 577);
            this.SnowInfoMemoEdit.MenuManager = this.barManager1;
            this.SnowInfoMemoEdit.Name = "SnowInfoMemoEdit";
            this.SnowInfoMemoEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SnowInfoMemoEdit, true);
            this.SnowInfoMemoEdit.Size = new System.Drawing.Size(911, 124);
            this.scSpellChecker.SetSpellCheckerOptions(this.SnowInfoMemoEdit, optionsSpelling10);
            this.SnowInfoMemoEdit.StyleController = this.dataLayoutControl1;
            this.SnowInfoMemoEdit.TabIndex = 107;
            // 
            // NoAccessAbortedRateSpinEdit
            // 
            this.NoAccessAbortedRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "NoAccessAbortedRate", true));
            this.NoAccessAbortedRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NoAccessAbortedRateSpinEdit.Location = new System.Drawing.Point(406, 293);
            this.NoAccessAbortedRateSpinEdit.MenuManager = this.barManager1;
            this.NoAccessAbortedRateSpinEdit.Name = "NoAccessAbortedRateSpinEdit";
            this.NoAccessAbortedRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.NoAccessAbortedRateSpinEdit.Properties.Mask.EditMask = "c";
            this.NoAccessAbortedRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.NoAccessAbortedRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.NoAccessAbortedRateSpinEdit.Size = new System.Drawing.Size(76, 20);
            this.NoAccessAbortedRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.NoAccessAbortedRateSpinEdit.TabIndex = 106;
            this.NoAccessAbortedRateSpinEdit.Validated += new System.EventHandler(this.NoAccessAbortedRateSpinEdit_Validated);
            // 
            // PPEWornCheckEdit
            // 
            this.PPEWornCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "PPEWorn", true));
            this.PPEWornCheckEdit.Location = new System.Drawing.Point(649, 438);
            this.PPEWornCheckEdit.MenuManager = this.barManager1;
            this.PPEWornCheckEdit.Name = "PPEWornCheckEdit";
            this.PPEWornCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.PPEWornCheckEdit.Properties.ReadOnly = true;
            this.PPEWornCheckEdit.Properties.ValueChecked = 1;
            this.PPEWornCheckEdit.Properties.ValueUnchecked = 0;
            this.PPEWornCheckEdit.Size = new System.Drawing.Size(114, 19);
            this.PPEWornCheckEdit.StyleController = this.dataLayoutControl1;
            this.PPEWornCheckEdit.TabIndex = 101;
            // 
            // ClientPOIDTextEdit
            // 
            this.ClientPOIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "ClientPOID", true));
            this.ClientPOIDTextEdit.Location = new System.Drawing.Point(143, 202);
            this.ClientPOIDTextEdit.MenuManager = this.barManager1;
            this.ClientPOIDTextEdit.Name = "ClientPOIDTextEdit";
            this.ClientPOIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPOIDTextEdit, true);
            this.ClientPOIDTextEdit.Size = new System.Drawing.Size(785, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPOIDTextEdit, optionsSpelling11);
            this.ClientPOIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPOIDTextEdit.TabIndex = 98;
            // 
            // NonStandardSellCheckEdit
            // 
            this.NonStandardSellCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "NonStandardSell", true));
            this.NonStandardSellCheckEdit.Location = new System.Drawing.Point(896, 692);
            this.NonStandardSellCheckEdit.MenuManager = this.barManager1;
            this.NonStandardSellCheckEdit.Name = "NonStandardSellCheckEdit";
            this.NonStandardSellCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NonStandardSellCheckEdit.Properties.ValueChecked = 1;
            this.NonStandardSellCheckEdit.Properties.ValueUnchecked = 0;
            this.NonStandardSellCheckEdit.Size = new System.Drawing.Size(178, 19);
            this.NonStandardSellCheckEdit.StyleController = this.dataLayoutControl1;
            this.NonStandardSellCheckEdit.TabIndex = 100;
            this.NonStandardSellCheckEdit.EditValueChanged += new System.EventHandler(this.NonStandardSellCheckEdit_EditValueChanged);
            // 
            // ClientPOIDDescriptionButtonEdit
            // 
            this.ClientPOIDDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "ClientPOIDDescription", true));
            this.ClientPOIDDescriptionButtonEdit.Location = new System.Drawing.Point(151, 107);
            this.ClientPOIDDescriptionButtonEdit.MenuManager = this.barManager1;
            this.ClientPOIDDescriptionButtonEdit.Name = "ClientPOIDDescriptionButtonEdit";
            this.ClientPOIDDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientPOIDDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientPOIDDescriptionButtonEdit.Size = new System.Drawing.Size(378, 20);
            this.ClientPOIDDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientPOIDDescriptionButtonEdit.TabIndex = 97;
            this.ClientPOIDDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientPOIDDescriptionButtonEdit_ButtonClick);
            // 
            // StartTimeDateEdit
            // 
            this.StartTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "StartTime", true));
            this.StartTimeDateEdit.EditValue = null;
            this.StartTimeDateEdit.Location = new System.Drawing.Point(649, 390);
            this.StartTimeDateEdit.MenuManager = this.barManager1;
            this.StartTimeDateEdit.Name = "StartTimeDateEdit";
            this.StartTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StartTimeDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.StartTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartTimeDateEdit.Size = new System.Drawing.Size(114, 20);
            this.StartTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.StartTimeDateEdit.TabIndex = 96;
            this.StartTimeDateEdit.EditValueChanged += new System.EventHandler(this.StartTimeDateEdit_EditValueChanged);
            this.StartTimeDateEdit.Validated += new System.EventHandler(this.StartTimeDateEdit_Validated);
            // 
            // AuthorisedByStaffNameTextEdit
            // 
            this.AuthorisedByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "AuthorisedByStaffName", true));
            this.AuthorisedByStaffNameTextEdit.Location = new System.Drawing.Point(187, 658);
            this.AuthorisedByStaffNameTextEdit.MenuManager = this.barManager1;
            this.AuthorisedByStaffNameTextEdit.Name = "AuthorisedByStaffNameTextEdit";
            this.AuthorisedByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AuthorisedByStaffNameTextEdit, true);
            this.AuthorisedByStaffNameTextEdit.Size = new System.Drawing.Size(320, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AuthorisedByStaffNameTextEdit, optionsSpelling12);
            this.AuthorisedByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.AuthorisedByStaffNameTextEdit.TabIndex = 95;
            // 
            // NonStandardCostCheckEdit
            // 
            this.NonStandardCostCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "NonStandardCost", true));
            this.NonStandardCostCheckEdit.Location = new System.Drawing.Point(896, 669);
            this.NonStandardCostCheckEdit.MenuManager = this.barManager1;
            this.NonStandardCostCheckEdit.Name = "NonStandardCostCheckEdit";
            this.NonStandardCostCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NonStandardCostCheckEdit.Properties.ValueChecked = 1;
            this.NonStandardCostCheckEdit.Properties.ValueUnchecked = 0;
            this.NonStandardCostCheckEdit.Size = new System.Drawing.Size(178, 19);
            this.NonStandardCostCheckEdit.StyleController = this.dataLayoutControl1;
            this.NonStandardCostCheckEdit.TabIndex = 99;
            this.NonStandardCostCheckEdit.EditValueChanged += new System.EventHandler(this.NonStandardCostCheckEdit_EditValueChanged);
            // 
            // PaidByStaffNameTextEdit
            // 
            this.PaidByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "PaidByStaffName", true));
            this.PaidByStaffNameTextEdit.Location = new System.Drawing.Point(187, 682);
            this.PaidByStaffNameTextEdit.MenuManager = this.barManager1;
            this.PaidByStaffNameTextEdit.Name = "PaidByStaffNameTextEdit";
            this.PaidByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PaidByStaffNameTextEdit, true);
            this.PaidByStaffNameTextEdit.Size = new System.Drawing.Size(320, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PaidByStaffNameTextEdit, optionsSpelling13);
            this.PaidByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PaidByStaffNameTextEdit.TabIndex = 94;
            // 
            // SnowOnSiteCheckEdit
            // 
            this.SnowOnSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SnowOnSite", true));
            this.SnowOnSiteCheckEdit.Location = new System.Drawing.Point(163, 386);
            this.SnowOnSiteCheckEdit.MenuManager = this.barManager1;
            this.SnowOnSiteCheckEdit.Name = "SnowOnSiteCheckEdit";
            this.SnowOnSiteCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowOnSiteCheckEdit.Properties.ValueChecked = 1;
            this.SnowOnSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowOnSiteCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.SnowOnSiteCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowOnSiteCheckEdit.TabIndex = 93;
            // 
            // ClientPriceSpinEdit
            // 
            this.ClientPriceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "ClientPrice", true));
            this.ClientPriceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientPriceSpinEdit.Location = new System.Drawing.Point(187, 731);
            this.ClientPriceSpinEdit.MenuManager = this.barManager1;
            this.ClientPriceSpinEdit.Name = "ClientPriceSpinEdit";
            this.ClientPriceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ClientPriceSpinEdit.Properties.Mask.EditMask = "c";
            this.ClientPriceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ClientPriceSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ClientPriceSpinEdit.Properties.ReadOnly = true;
            this.ClientPriceSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.ClientPriceSpinEdit.StyleController = this.dataLayoutControl1;
            this.ClientPriceSpinEdit.TabIndex = 92;
            this.ClientPriceSpinEdit.EditValueChanged += new System.EventHandler(this.ClientPriceSpinEdit_EditValueChanged);
            this.ClientPriceSpinEdit.Validated += new System.EventHandler(this.ClientPriceSpinEdit_Validated);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04076GCGrittingCalloutEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(152, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(169, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // SiteGrittingContractIDGridLookUpEdit
            // 
            this.SiteGrittingContractIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteGrittingContractID", true));
            this.SiteGrittingContractIDGridLookUpEdit.Location = new System.Drawing.Point(185, 242);
            this.SiteGrittingContractIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SiteGrittingContractIDGridLookUpEdit.Name = "SiteGrittingContractIDGridLookUpEdit";
            this.SiteGrittingContractIDGridLookUpEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteGrittingContractIDGridLookUpEdit, true);
            this.SiteGrittingContractIDGridLookUpEdit.Size = new System.Drawing.Size(597, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteGrittingContractIDGridLookUpEdit, optionsSpelling14);
            this.SiteGrittingContractIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SiteGrittingContractIDGridLookUpEdit.TabIndex = 16;
            // 
            // GrittingCallOutIDTextEdit
            // 
            this.GrittingCallOutIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "GrittingCallOutID", true));
            this.GrittingCallOutIDTextEdit.Location = new System.Drawing.Point(185, 82);
            this.GrittingCallOutIDTextEdit.MenuManager = this.barManager1;
            this.GrittingCallOutIDTextEdit.Name = "GrittingCallOutIDTextEdit";
            this.GrittingCallOutIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GrittingCallOutIDTextEdit, true);
            this.GrittingCallOutIDTextEdit.Size = new System.Drawing.Size(414, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GrittingCallOutIDTextEdit, optionsSpelling15);
            this.GrittingCallOutIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GrittingCallOutIDTextEdit.TabIndex = 30;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(672, 35);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(438, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling16);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 31;
            // 
            // SiteNameButtonEdit
            // 
            this.SiteNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteName", true));
            this.SiteNameButtonEdit.Location = new System.Drawing.Point(151, 35);
            this.SiteNameButtonEdit.MenuManager = this.barManager1;
            this.SiteNameButtonEdit.Name = "SiteNameButtonEdit";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Text = "Choose Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click to open the <b>Select Site Gritting Contract</b> screen from where the Site" +
    " and Client for this gritting callout can be specified.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Text = "View Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click to <B>View</b> the linked <b>Site Gritting Contract</b>.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.SiteNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "choose", superToolTip4, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "view", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SiteNameButtonEdit.Size = new System.Drawing.Size(378, 20);
            this.SiteNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameButtonEdit.TabIndex = 32;
            this.SiteNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteNameButtonEdit_ButtonClick);
            this.SiteNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteNameButtonEdit_Validating);
            // 
            // SubContractorNameButtonEdit
            // 
            this.SubContractorNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SubContractorName", true));
            this.SubContractorNameButtonEdit.Location = new System.Drawing.Point(151, 59);
            this.SubContractorNameButtonEdit.MenuManager = this.barManager1;
            this.SubContractorNameButtonEdit.Name = "SubContractorNameButtonEdit";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Text = "Choose Button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click to open the <b>Select Gritting Team</b> screen where the Gritting Team for " +
    "this callout can be selected.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Text = "View Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click to view the <b>linked Gritting Team</b> details.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.SubContractorNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", "choose", superToolTip6, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", "view", superToolTip7, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SubContractorNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SubContractorNameButtonEdit.Size = new System.Drawing.Size(378, 20);
            this.SubContractorNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorNameButtonEdit.TabIndex = 34;
            this.SubContractorNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SubContractorNameButtonEdit_ButtonClick);
            this.SubContractorNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SubContractorNameButtonEdit_Validating);
            // 
            // OriginalSubContractorIDTextEdit
            // 
            this.OriginalSubContractorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "OriginalSubContractorID", true));
            this.OriginalSubContractorIDTextEdit.Location = new System.Drawing.Point(185, 130);
            this.OriginalSubContractorIDTextEdit.MenuManager = this.barManager1;
            this.OriginalSubContractorIDTextEdit.Name = "OriginalSubContractorIDTextEdit";
            this.OriginalSubContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OriginalSubContractorIDTextEdit, true);
            this.OriginalSubContractorIDTextEdit.Size = new System.Drawing.Size(414, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OriginalSubContractorIDTextEdit, optionsSpelling17);
            this.OriginalSubContractorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.OriginalSubContractorIDTextEdit.TabIndex = 35;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "Reactive", true));
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(151, 131);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(90, 19);
            this.ReactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCheckEdit.TabIndex = 36;
            this.ReactiveCheckEdit.EditValueChanged += new System.EventHandler(this.ReactiveCheckEdit_EditValueChanged);
            this.ReactiveCheckEdit.Validated += new System.EventHandler(this.ReactiveCheckEdit_Validated);
            // 
            // btnShowOnMap
            // 
            this.btnShowOnMap.Location = new System.Drawing.Point(1021, 343);
            this.btnShowOnMap.Name = "btnShowOnMap";
            this.btnShowOnMap.Size = new System.Drawing.Size(77, 22);
            this.btnShowOnMap.StyleController = this.dataLayoutControl1;
            this.btnShowOnMap.TabIndex = 91;
            this.btnShowOnMap.Text = "Show On Map";
            this.btnShowOnMap.Click += new System.EventHandler(this.btnShowOnMap_Click);
            // 
            // CallOutDateTimeDateEdit
            // 
            this.CallOutDateTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "CallOutDateTime", true));
            this.CallOutDateTimeDateEdit.EditValue = null;
            this.CallOutDateTimeDateEdit.Location = new System.Drawing.Point(649, 247);
            this.CallOutDateTimeDateEdit.MenuManager = this.barManager1;
            this.CallOutDateTimeDateEdit.Name = "CallOutDateTimeDateEdit";
            this.CallOutDateTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CallOutDateTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CallOutDateTimeDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.CallOutDateTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CallOutDateTimeDateEdit.Properties.ReadOnly = true;
            this.CallOutDateTimeDateEdit.Size = new System.Drawing.Size(114, 20);
            this.CallOutDateTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.CallOutDateTimeDateEdit.TabIndex = 38;
            // 
            // ImportedWeatherForecastIDTextEdit
            // 
            this.ImportedWeatherForecastIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "ImportedWeatherForecastID", true));
            this.ImportedWeatherForecastIDTextEdit.Location = new System.Drawing.Point(185, 201);
            this.ImportedWeatherForecastIDTextEdit.MenuManager = this.barManager1;
            this.ImportedWeatherForecastIDTextEdit.Name = "ImportedWeatherForecastIDTextEdit";
            this.ImportedWeatherForecastIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ImportedWeatherForecastIDTextEdit, true);
            this.ImportedWeatherForecastIDTextEdit.Size = new System.Drawing.Size(414, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ImportedWeatherForecastIDTextEdit, optionsSpelling18);
            this.ImportedWeatherForecastIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ImportedWeatherForecastIDTextEdit.TabIndex = 39;
            // 
            // TextSentTimeDateEdit
            // 
            this.TextSentTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "TextSentTime", true));
            this.TextSentTimeDateEdit.EditValue = null;
            this.TextSentTimeDateEdit.Location = new System.Drawing.Point(649, 271);
            this.TextSentTimeDateEdit.MenuManager = this.barManager1;
            this.TextSentTimeDateEdit.Name = "TextSentTimeDateEdit";
            this.TextSentTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TextSentTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TextSentTimeDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.TextSentTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TextSentTimeDateEdit.Properties.ReadOnly = true;
            this.TextSentTimeDateEdit.Size = new System.Drawing.Size(114, 20);
            this.TextSentTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.TextSentTimeDateEdit.TabIndex = 40;
            // 
            // SubContractorReceivedTimeDateEdit
            // 
            this.SubContractorReceivedTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SubContractorReceivedTime", true));
            this.SubContractorReceivedTimeDateEdit.EditValue = null;
            this.SubContractorReceivedTimeDateEdit.Location = new System.Drawing.Point(649, 295);
            this.SubContractorReceivedTimeDateEdit.MenuManager = this.barManager1;
            this.SubContractorReceivedTimeDateEdit.Name = "SubContractorReceivedTimeDateEdit";
            this.SubContractorReceivedTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubContractorReceivedTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SubContractorReceivedTimeDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.SubContractorReceivedTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SubContractorReceivedTimeDateEdit.Properties.ReadOnly = true;
            this.SubContractorReceivedTimeDateEdit.Size = new System.Drawing.Size(114, 20);
            this.SubContractorReceivedTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorReceivedTimeDateEdit.TabIndex = 41;
            // 
            // SubContractorRespondedTimeDateEdit
            // 
            this.SubContractorRespondedTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SubContractorRespondedTime", true));
            this.SubContractorRespondedTimeDateEdit.EditValue = null;
            this.SubContractorRespondedTimeDateEdit.Location = new System.Drawing.Point(649, 319);
            this.SubContractorRespondedTimeDateEdit.MenuManager = this.barManager1;
            this.SubContractorRespondedTimeDateEdit.Name = "SubContractorRespondedTimeDateEdit";
            this.SubContractorRespondedTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubContractorRespondedTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SubContractorRespondedTimeDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.SubContractorRespondedTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SubContractorRespondedTimeDateEdit.Properties.ReadOnly = true;
            this.SubContractorRespondedTimeDateEdit.Size = new System.Drawing.Size(114, 20);
            this.SubContractorRespondedTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorRespondedTimeDateEdit.TabIndex = 42;
            // 
            // SubContractorETATextEdit
            // 
            this.SubContractorETATextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SubContractorETA", true));
            this.SubContractorETATextEdit.Location = new System.Drawing.Point(649, 343);
            this.SubContractorETATextEdit.MenuManager = this.barManager1;
            this.SubContractorETATextEdit.Name = "SubContractorETATextEdit";
            this.SubContractorETATextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubContractorETATextEdit, true);
            this.SubContractorETATextEdit.Size = new System.Drawing.Size(114, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubContractorETATextEdit, optionsSpelling19);
            this.SubContractorETATextEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorETATextEdit.TabIndex = 43;
            // 
            // CompletedTimeDateEdit
            // 
            this.CompletedTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "CompletedTime", true));
            this.CompletedTimeDateEdit.EditValue = null;
            this.CompletedTimeDateEdit.Location = new System.Drawing.Point(649, 414);
            this.CompletedTimeDateEdit.MenuManager = this.barManager1;
            this.CompletedTimeDateEdit.Name = "CompletedTimeDateEdit";
            this.CompletedTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CompletedTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CompletedTimeDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.CompletedTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CompletedTimeDateEdit.Size = new System.Drawing.Size(114, 20);
            this.CompletedTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.CompletedTimeDateEdit.TabIndex = 44;
            this.CompletedTimeDateEdit.EditValueChanged += new System.EventHandler(this.CompletedTimeDateEdit_EditValueChanged);
            this.CompletedTimeDateEdit.Validated += new System.EventHandler(this.CompletedTimeDateEdit_Validated);
            // 
            // AuthorisedByStaffIDTextEdit
            // 
            this.AuthorisedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "AuthorisedByStaffID", true));
            this.AuthorisedByStaffIDTextEdit.Location = new System.Drawing.Point(185, 321);
            this.AuthorisedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.AuthorisedByStaffIDTextEdit.Name = "AuthorisedByStaffIDTextEdit";
            this.AuthorisedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AuthorisedByStaffIDTextEdit, true);
            this.AuthorisedByStaffIDTextEdit.Size = new System.Drawing.Size(414, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AuthorisedByStaffIDTextEdit, optionsSpelling20);
            this.AuthorisedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AuthorisedByStaffIDTextEdit.TabIndex = 45;
            // 
            // VisitAbortedCheckEdit
            // 
            this.VisitAbortedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "VisitAborted", true));
            this.VisitAbortedCheckEdit.Location = new System.Drawing.Point(406, 270);
            this.VisitAbortedCheckEdit.MenuManager = this.barManager1;
            this.VisitAbortedCheckEdit.Name = "VisitAbortedCheckEdit";
            this.VisitAbortedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.VisitAbortedCheckEdit.Properties.ValueChecked = 1;
            this.VisitAbortedCheckEdit.Properties.ValueUnchecked = 0;
            this.VisitAbortedCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.VisitAbortedCheckEdit.StyleController = this.dataLayoutControl1;
            this.VisitAbortedCheckEdit.TabIndex = 46;
            this.VisitAbortedCheckEdit.Validated += new System.EventHandler(this.VisitAbortedCheckEdit_Validated);
            // 
            // StartLatitudeTextEdit
            // 
            this.StartLatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "StartLatitude", true));
            this.StartLatitudeTextEdit.Location = new System.Drawing.Point(930, 247);
            this.StartLatitudeTextEdit.MenuManager = this.barManager1;
            this.StartLatitudeTextEdit.Name = "StartLatitudeTextEdit";
            this.StartLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartLatitudeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLatitudeTextEdit, true);
            this.StartLatitudeTextEdit.Size = new System.Drawing.Size(168, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLatitudeTextEdit, optionsSpelling21);
            this.StartLatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.StartLatitudeTextEdit.TabIndex = 48;
            // 
            // StartLongitudeTextEdit
            // 
            this.StartLongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "StartLongitude", true));
            this.StartLongitudeTextEdit.Location = new System.Drawing.Point(930, 271);
            this.StartLongitudeTextEdit.MenuManager = this.barManager1;
            this.StartLongitudeTextEdit.Name = "StartLongitudeTextEdit";
            this.StartLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartLongitudeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLongitudeTextEdit, true);
            this.StartLongitudeTextEdit.Size = new System.Drawing.Size(168, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLongitudeTextEdit, optionsSpelling22);
            this.StartLongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.StartLongitudeTextEdit.TabIndex = 49;
            // 
            // FinishedLatitudeTextEdit
            // 
            this.FinishedLatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "FinishedLatitude", true));
            this.FinishedLatitudeTextEdit.Location = new System.Drawing.Point(930, 295);
            this.FinishedLatitudeTextEdit.MenuManager = this.barManager1;
            this.FinishedLatitudeTextEdit.Name = "FinishedLatitudeTextEdit";
            this.FinishedLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishedLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FinishedLatitudeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishedLatitudeTextEdit, true);
            this.FinishedLatitudeTextEdit.Size = new System.Drawing.Size(168, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishedLatitudeTextEdit, optionsSpelling23);
            this.FinishedLatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinishedLatitudeTextEdit.TabIndex = 50;
            // 
            // FinishedLongitudeTextEdit
            // 
            this.FinishedLongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "FinishedLongitude", true));
            this.FinishedLongitudeTextEdit.Location = new System.Drawing.Point(930, 319);
            this.FinishedLongitudeTextEdit.MenuManager = this.barManager1;
            this.FinishedLongitudeTextEdit.Name = "FinishedLongitudeTextEdit";
            this.FinishedLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishedLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FinishedLongitudeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishedLongitudeTextEdit, true);
            this.FinishedLongitudeTextEdit.Size = new System.Drawing.Size(168, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishedLongitudeTextEdit, optionsSpelling24);
            this.FinishedLongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinishedLongitudeTextEdit.TabIndex = 51;
            // 
            // ClientPONumberTextEdit
            // 
            this.ClientPONumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "ClientPONumber", true));
            this.ClientPONumberTextEdit.Location = new System.Drawing.Point(672, 107);
            this.ClientPONumberTextEdit.MenuManager = this.barManager1;
            this.ClientPONumberTextEdit.Name = "ClientPONumberTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPONumberTextEdit, true);
            this.ClientPONumberTextEdit.Size = new System.Drawing.Size(438, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPONumberTextEdit, optionsSpelling25);
            this.ClientPONumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPONumberTextEdit.TabIndex = 53;
            // 
            // RecordedByStaffIDTextEdit
            // 
            this.RecordedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "RecordedByStaffID", true));
            this.RecordedByStaffIDTextEdit.Location = new System.Drawing.Point(185, 158);
            this.RecordedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.RecordedByStaffIDTextEdit.Name = "RecordedByStaffIDTextEdit";
            this.RecordedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RecordedByStaffIDTextEdit, true);
            this.RecordedByStaffIDTextEdit.Size = new System.Drawing.Size(597, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RecordedByStaffIDTextEdit, optionsSpelling26);
            this.RecordedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RecordedByStaffIDTextEdit.TabIndex = 72;
            // 
            // SaltUsedSpinEdit
            // 
            this.SaltUsedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SaltUsed", true));
            this.SaltUsedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SaltUsedSpinEdit.Location = new System.Drawing.Point(532, 611);
            this.SaltUsedSpinEdit.MenuManager = this.barManager1;
            this.SaltUsedSpinEdit.Name = "SaltUsedSpinEdit";
            this.SaltUsedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SaltUsedSpinEdit.Properties.Mask.EditMask = "######0.00 25kg Bags";
            this.SaltUsedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SaltUsedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.SaltUsedSpinEdit.Size = new System.Drawing.Size(191, 20);
            this.SaltUsedSpinEdit.StyleController = this.dataLayoutControl1;
            this.SaltUsedSpinEdit.TabIndex = 54;
            this.SaltUsedSpinEdit.EditValueChanged += new System.EventHandler(this.SaltUsedSpinEdit_EditValueChanged);
            this.SaltUsedSpinEdit.Validated += new System.EventHandler(this.SaltUsedSpinEdit_Validated);
            // 
            // SaltCostSpinEdit
            // 
            this.SaltCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SaltCost", true));
            this.SaltCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SaltCostSpinEdit.Location = new System.Drawing.Point(532, 635);
            this.SaltCostSpinEdit.MenuManager = this.barManager1;
            this.SaltCostSpinEdit.Name = "SaltCostSpinEdit";
            this.SaltCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SaltCostSpinEdit.Properties.Mask.EditMask = "c";
            this.SaltCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SaltCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.SaltCostSpinEdit.Size = new System.Drawing.Size(191, 20);
            this.SaltCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.SaltCostSpinEdit.TabIndex = 55;
            this.SaltCostSpinEdit.EditValueChanged += new System.EventHandler(this.SaltCostSpinEdit_EditValueChanged);
            this.SaltCostSpinEdit.Validated += new System.EventHandler(this.SaltCostSpinEdit_Validated);
            // 
            // SaltSellSpinEdit
            // 
            this.SaltSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SaltSell", true));
            this.SaltSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SaltSellSpinEdit.Location = new System.Drawing.Point(532, 659);
            this.SaltSellSpinEdit.MenuManager = this.barManager1;
            this.SaltSellSpinEdit.Name = "SaltSellSpinEdit";
            this.SaltSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SaltSellSpinEdit.Properties.Mask.EditMask = "c";
            this.SaltSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SaltSellSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.SaltSellSpinEdit.Size = new System.Drawing.Size(191, 20);
            this.SaltSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.SaltSellSpinEdit.TabIndex = 56;
            this.SaltSellSpinEdit.EditValueChanged += new System.EventHandler(this.SaltSellSpinEdit_EditValueChanged);
            this.SaltSellSpinEdit.Validated += new System.EventHandler(this.SaltSellSpinEdit_Validated);
            // 
            // SaltVatRateSpinEdit
            // 
            this.SaltVatRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SaltVatRate", true));
            this.SaltVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SaltVatRateSpinEdit.Location = new System.Drawing.Point(532, 683);
            this.SaltVatRateSpinEdit.MenuManager = this.barManager1;
            this.SaltVatRateSpinEdit.Name = "SaltVatRateSpinEdit";
            this.SaltVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SaltVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.SaltVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SaltVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.SaltVatRateSpinEdit.Size = new System.Drawing.Size(191, 20);
            this.SaltVatRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.SaltVatRateSpinEdit.TabIndex = 57;
            this.SaltVatRateSpinEdit.EditValueChanged += new System.EventHandler(this.SaltVatRateSpinEdit_EditValueChanged);
            this.SaltVatRateSpinEdit.Validated += new System.EventHandler(this.SaltVatRateSpinEdit_Validated);
            // 
            // HoursWorkedSpinEdit
            // 
            this.HoursWorkedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "HoursWorked", true));
            this.HoursWorkedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.HoursWorkedSpinEdit.Location = new System.Drawing.Point(187, 611);
            this.HoursWorkedSpinEdit.MenuManager = this.barManager1;
            this.HoursWorkedSpinEdit.Name = "HoursWorkedSpinEdit";
            this.HoursWorkedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.HoursWorkedSpinEdit.Properties.Mask.EditMask = "f";
            this.HoursWorkedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HoursWorkedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.HoursWorkedSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.HoursWorkedSpinEdit.StyleController = this.dataLayoutControl1;
            this.HoursWorkedSpinEdit.TabIndex = 58;
            this.HoursWorkedSpinEdit.EditValueChanged += new System.EventHandler(this.HoursWorkedSpinEdit_EditValueChanged);
            this.HoursWorkedSpinEdit.Validated += new System.EventHandler(this.HoursWorkedSpinEdit_Validated);
            // 
            // TeamHourlyRateSpinEdit
            // 
            this.TeamHourlyRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "TeamHourlyRate", true));
            this.TeamHourlyRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TeamHourlyRateSpinEdit.Location = new System.Drawing.Point(187, 635);
            this.TeamHourlyRateSpinEdit.MenuManager = this.barManager1;
            this.TeamHourlyRateSpinEdit.Name = "TeamHourlyRateSpinEdit";
            this.TeamHourlyRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TeamHourlyRateSpinEdit.Properties.Mask.EditMask = "c";
            this.TeamHourlyRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TeamHourlyRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TeamHourlyRateSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.TeamHourlyRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.TeamHourlyRateSpinEdit.TabIndex = 59;
            this.TeamHourlyRateSpinEdit.EditValueChanged += new System.EventHandler(this.TeamHourlyRateSpinEdit_EditValueChanged);
            this.TeamHourlyRateSpinEdit.Validated += new System.EventHandler(this.TeamHourlyRateSpinEdit_Validated);
            // 
            // TeamChargeSpinEdit
            // 
            this.TeamChargeSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "TeamCharge", true));
            this.TeamChargeSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TeamChargeSpinEdit.Location = new System.Drawing.Point(187, 659);
            this.TeamChargeSpinEdit.MenuManager = this.barManager1;
            this.TeamChargeSpinEdit.Name = "TeamChargeSpinEdit";
            this.TeamChargeSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TeamChargeSpinEdit.Properties.Mask.EditMask = "c";
            this.TeamChargeSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TeamChargeSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TeamChargeSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.TeamChargeSpinEdit.StyleController = this.dataLayoutControl1;
            this.TeamChargeSpinEdit.TabIndex = 60;
            this.TeamChargeSpinEdit.EditValueChanged += new System.EventHandler(this.TeamChargeSpinEdit_EditValueChanged);
            this.TeamChargeSpinEdit.Validated += new System.EventHandler(this.TeamChargeSpinEdit_Validated);
            // 
            // LabourCostSpinEdit
            // 
            this.LabourCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "LabourCost", true));
            this.LabourCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LabourCostSpinEdit.Location = new System.Drawing.Point(187, 707);
            this.LabourCostSpinEdit.MenuManager = this.barManager1;
            this.LabourCostSpinEdit.Name = "LabourCostSpinEdit";
            this.LabourCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LabourCostSpinEdit.Properties.Mask.EditMask = "c";
            this.LabourCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LabourCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.LabourCostSpinEdit.Properties.ReadOnly = true;
            this.LabourCostSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.LabourCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.LabourCostSpinEdit.TabIndex = 61;
            this.LabourCostSpinEdit.EditValueChanged += new System.EventHandler(this.LabourCostSpinEdit_EditValueChanged);
            this.LabourCostSpinEdit.Validated += new System.EventHandler(this.LabourCostSpinEdit_Validated);
            // 
            // LabourVatRateSpinEdit
            // 
            this.LabourVatRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "LabourVatRate", true));
            this.LabourVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LabourVatRateSpinEdit.Location = new System.Drawing.Point(187, 683);
            this.LabourVatRateSpinEdit.MenuManager = this.barManager1;
            this.LabourVatRateSpinEdit.Name = "LabourVatRateSpinEdit";
            this.LabourVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LabourVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.LabourVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LabourVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.LabourVatRateSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.LabourVatRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.LabourVatRateSpinEdit.TabIndex = 63;
            this.LabourVatRateSpinEdit.EditValueChanged += new System.EventHandler(this.LabourVatRateSpinEdit_EditValueChanged);
            this.LabourVatRateSpinEdit.Validated += new System.EventHandler(this.LabourVatRateSpinEdit_Validated);
            // 
            // OtherCostSpinEdit
            // 
            this.OtherCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "OtherCost", true));
            this.OtherCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OtherCostSpinEdit.Location = new System.Drawing.Point(532, 753);
            this.OtherCostSpinEdit.MenuManager = this.barManager1;
            this.OtherCostSpinEdit.Name = "OtherCostSpinEdit";
            this.OtherCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OtherCostSpinEdit.Properties.Mask.EditMask = "c";
            this.OtherCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OtherCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.OtherCostSpinEdit.Properties.ReadOnly = true;
            this.OtherCostSpinEdit.Size = new System.Drawing.Size(191, 20);
            this.OtherCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.OtherCostSpinEdit.TabIndex = 64;
            // 
            // OtherSellSpinEdit
            // 
            this.OtherSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "OtherSell", true));
            this.OtherSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OtherSellSpinEdit.Location = new System.Drawing.Point(532, 777);
            this.OtherSellSpinEdit.MenuManager = this.barManager1;
            this.OtherSellSpinEdit.Name = "OtherSellSpinEdit";
            this.OtherSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OtherSellSpinEdit.Properties.Mask.EditMask = "c";
            this.OtherSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OtherSellSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.OtherSellSpinEdit.Properties.ReadOnly = true;
            this.OtherSellSpinEdit.Size = new System.Drawing.Size(191, 20);
            this.OtherSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.OtherSellSpinEdit.TabIndex = 65;
            // 
            // TotalCostSpinEdit
            // 
            this.TotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "TotalCost", true));
            this.TotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalCostSpinEdit.Location = new System.Drawing.Point(896, 611);
            this.TotalCostSpinEdit.MenuManager = this.barManager1;
            this.TotalCostSpinEdit.Name = "TotalCostSpinEdit";
            this.TotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TotalCostSpinEdit.Properties.ReadOnly = true;
            this.TotalCostSpinEdit.Size = new System.Drawing.Size(178, 20);
            this.TotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostSpinEdit.TabIndex = 67;
            // 
            // TotalSellSpinEdit
            // 
            this.TotalSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "TotalSell", true));
            this.TotalSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalSellSpinEdit.Location = new System.Drawing.Point(896, 635);
            this.TotalSellSpinEdit.MenuManager = this.barManager1;
            this.TotalSellSpinEdit.Name = "TotalSellSpinEdit";
            this.TotalSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalSellSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSellSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TotalSellSpinEdit.Properties.ReadOnly = true;
            this.TotalSellSpinEdit.Size = new System.Drawing.Size(178, 20);
            this.TotalSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalSellSpinEdit.TabIndex = 68;
            // 
            // ClientInvoiceNumberTextEdit
            // 
            this.ClientInvoiceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "ClientInvoiceNumber", true));
            this.ClientInvoiceNumberTextEdit.Location = new System.Drawing.Point(680, 611);
            this.ClientInvoiceNumberTextEdit.MenuManager = this.barManager1;
            this.ClientInvoiceNumberTextEdit.Name = "ClientInvoiceNumberTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientInvoiceNumberTextEdit, true);
            this.ClientInvoiceNumberTextEdit.Size = new System.Drawing.Size(394, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientInvoiceNumberTextEdit, optionsSpelling27);
            this.ClientInvoiceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientInvoiceNumberTextEdit.TabIndex = 69;
            // 
            // SubContractorPaidCheckEdit
            // 
            this.SubContractorPaidCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SubContractorPaid", true));
            this.SubContractorPaidCheckEdit.Location = new System.Drawing.Point(187, 611);
            this.SubContractorPaidCheckEdit.MenuManager = this.barManager1;
            this.SubContractorPaidCheckEdit.Name = "SubContractorPaidCheckEdit";
            this.SubContractorPaidCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SubContractorPaidCheckEdit.Properties.ValueChecked = 1;
            this.SubContractorPaidCheckEdit.Properties.ValueUnchecked = 0;
            this.SubContractorPaidCheckEdit.Size = new System.Drawing.Size(320, 19);
            this.SubContractorPaidCheckEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorPaidCheckEdit.TabIndex = 70;
            // 
            // RecordedByNameTextEdit
            // 
            this.RecordedByNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "RecordedByName", true));
            this.RecordedByNameTextEdit.Location = new System.Drawing.Point(672, 83);
            this.RecordedByNameTextEdit.MenuManager = this.barManager1;
            this.RecordedByNameTextEdit.Name = "RecordedByNameTextEdit";
            this.RecordedByNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RecordedByNameTextEdit, true);
            this.RecordedByNameTextEdit.Size = new System.Drawing.Size(438, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RecordedByNameTextEdit, optionsSpelling28);
            this.RecordedByNameTextEdit.StyleController = this.dataLayoutControl1;
            this.RecordedByNameTextEdit.TabIndex = 73;
            // 
            // GritSourceLocationIDTextEdit
            // 
            this.GritSourceLocationIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "GritSourceLocationID", true));
            this.GritSourceLocationIDTextEdit.Location = new System.Drawing.Point(143, 312);
            this.GritSourceLocationIDTextEdit.MenuManager = this.barManager1;
            this.GritSourceLocationIDTextEdit.Name = "GritSourceLocationIDTextEdit";
            this.GritSourceLocationIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GritSourceLocationIDTextEdit, true);
            this.GritSourceLocationIDTextEdit.Size = new System.Drawing.Size(785, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GritSourceLocationIDTextEdit, optionsSpelling29);
            this.GritSourceLocationIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GritSourceLocationIDTextEdit.TabIndex = 79;
            // 
            // GritSourceLocationTypeIDTextEdit
            // 
            this.GritSourceLocationTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "GritSourceLocationTypeID", true));
            this.GritSourceLocationTypeIDTextEdit.Location = new System.Drawing.Point(143, 326);
            this.GritSourceLocationTypeIDTextEdit.MenuManager = this.barManager1;
            this.GritSourceLocationTypeIDTextEdit.Name = "GritSourceLocationTypeIDTextEdit";
            this.GritSourceLocationTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GritSourceLocationTypeIDTextEdit, true);
            this.GritSourceLocationTypeIDTextEdit.Size = new System.Drawing.Size(785, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GritSourceLocationTypeIDTextEdit, optionsSpelling30);
            this.GritSourceLocationTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GritSourceLocationTypeIDTextEdit.TabIndex = 80;
            // 
            // PaidByStaffIDTextEdit
            // 
            this.PaidByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "PaidByStaffID", true));
            this.PaidByStaffIDTextEdit.Location = new System.Drawing.Point(179, 287);
            this.PaidByStaffIDTextEdit.MenuManager = this.barManager1;
            this.PaidByStaffIDTextEdit.Name = "PaidByStaffIDTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.PaidByStaffIDTextEdit, true);
            this.PaidByStaffIDTextEdit.Size = new System.Drawing.Size(255, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PaidByStaffIDTextEdit, optionsSpelling31);
            this.PaidByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PaidByStaffIDTextEdit.TabIndex = 74;
            // 
            // SiteWeatherTextEdit
            // 
            this.SiteWeatherTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteWeather", true));
            this.SiteWeatherTextEdit.Location = new System.Drawing.Point(163, 222);
            this.SiteWeatherTextEdit.MenuManager = this.barManager1;
            this.SiteWeatherTextEdit.Name = "SiteWeatherTextEdit";
            this.SiteWeatherTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteWeatherTextEdit, true);
            this.SiteWeatherTextEdit.Size = new System.Drawing.Size(267, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteWeatherTextEdit, optionsSpelling32);
            this.SiteWeatherTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteWeatherTextEdit.TabIndex = 82;
            // 
            // DoNotPaySubContractorCheckEdit
            // 
            this.DoNotPaySubContractorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "DoNotPaySubContractor", true));
            this.DoNotPaySubContractorCheckEdit.Location = new System.Drawing.Point(187, 706);
            this.DoNotPaySubContractorCheckEdit.MenuManager = this.barManager1;
            this.DoNotPaySubContractorCheckEdit.Name = "DoNotPaySubContractorCheckEdit";
            this.DoNotPaySubContractorCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DoNotPaySubContractorCheckEdit.Properties.ValueChecked = 1;
            this.DoNotPaySubContractorCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotPaySubContractorCheckEdit.Size = new System.Drawing.Size(320, 19);
            this.DoNotPaySubContractorCheckEdit.StyleController = this.dataLayoutControl1;
            this.DoNotPaySubContractorCheckEdit.TabIndex = 75;
            // 
            // DoNotPaySubContractorReasonMemoEdit
            // 
            this.DoNotPaySubContractorReasonMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "DoNotPaySubContractorReason", true));
            this.DoNotPaySubContractorReasonMemoEdit.Location = new System.Drawing.Point(187, 729);
            this.DoNotPaySubContractorReasonMemoEdit.MenuManager = this.barManager1;
            this.DoNotPaySubContractorReasonMemoEdit.Name = "DoNotPaySubContractorReasonMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.DoNotPaySubContractorReasonMemoEdit, true);
            this.DoNotPaySubContractorReasonMemoEdit.Size = new System.Drawing.Size(320, 63);
            this.scSpellChecker.SetSpellCheckerOptions(this.DoNotPaySubContractorReasonMemoEdit, optionsSpelling33);
            this.DoNotPaySubContractorReasonMemoEdit.StyleController = this.dataLayoutControl1;
            this.DoNotPaySubContractorReasonMemoEdit.TabIndex = 76;
            // 
            // DoNotInvoiceClientCheckEdit
            // 
            this.DoNotInvoiceClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "DoNotInvoiceClient", true));
            this.DoNotInvoiceClientCheckEdit.Location = new System.Drawing.Point(680, 635);
            this.DoNotInvoiceClientCheckEdit.MenuManager = this.barManager1;
            this.DoNotInvoiceClientCheckEdit.Name = "DoNotInvoiceClientCheckEdit";
            this.DoNotInvoiceClientCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DoNotInvoiceClientCheckEdit.Properties.ValueChecked = 1;
            this.DoNotInvoiceClientCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotInvoiceClientCheckEdit.Size = new System.Drawing.Size(394, 19);
            this.DoNotInvoiceClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.DoNotInvoiceClientCheckEdit.TabIndex = 77;
            // 
            // DoNotInvoiceClientReasonMemoEdit
            // 
            this.DoNotInvoiceClientReasonMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "DoNotInvoiceClientReason", true));
            this.DoNotInvoiceClientReasonMemoEdit.Location = new System.Drawing.Point(680, 658);
            this.DoNotInvoiceClientReasonMemoEdit.MenuManager = this.barManager1;
            this.DoNotInvoiceClientReasonMemoEdit.Name = "DoNotInvoiceClientReasonMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.DoNotInvoiceClientReasonMemoEdit, true);
            this.DoNotInvoiceClientReasonMemoEdit.Size = new System.Drawing.Size(394, 63);
            this.scSpellChecker.SetSpellCheckerOptions(this.DoNotInvoiceClientReasonMemoEdit, optionsSpelling34);
            this.DoNotInvoiceClientReasonMemoEdit.StyleController = this.dataLayoutControl1;
            this.DoNotInvoiceClientReasonMemoEdit.TabIndex = 78;
            // 
            // SiteTemperatureSpinEdit
            // 
            this.SiteTemperatureSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SiteTemperature", true));
            this.SiteTemperatureSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SiteTemperatureSpinEdit.Location = new System.Drawing.Point(163, 362);
            this.SiteTemperatureSpinEdit.MenuManager = this.barManager1;
            this.SiteTemperatureSpinEdit.Name = "SiteTemperatureSpinEdit";
            this.SiteTemperatureSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SiteTemperatureSpinEdit.Properties.Mask.EditMask = "f";
            this.SiteTemperatureSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SiteTemperatureSpinEdit.Properties.MaxValue = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.SiteTemperatureSpinEdit.Properties.MinValue = new decimal(new int[] {
            60,
            0,
            0,
            -2147483648});
            this.SiteTemperatureSpinEdit.Size = new System.Drawing.Size(76, 20);
            this.SiteTemperatureSpinEdit.StyleController = this.dataLayoutControl1;
            this.SiteTemperatureSpinEdit.TabIndex = 83;
            // 
            // PdaIDTextEdit
            // 
            this.PdaIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "PdaID", true));
            this.PdaIDTextEdit.Location = new System.Drawing.Point(597, 393);
            this.PdaIDTextEdit.MenuManager = this.barManager1;
            this.PdaIDTextEdit.Name = "PdaIDTextEdit";
            this.PdaIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PdaIDTextEdit, true);
            this.PdaIDTextEdit.Size = new System.Drawing.Size(319, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PdaIDTextEdit, optionsSpelling35);
            this.PdaIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PdaIDTextEdit.TabIndex = 84;
            // 
            // TeamPresentCheckEdit
            // 
            this.TeamPresentCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "TeamPresent", true));
            this.TeamPresentCheckEdit.Location = new System.Drawing.Point(649, 367);
            this.TeamPresentCheckEdit.MenuManager = this.barManager1;
            this.TeamPresentCheckEdit.Name = "TeamPresentCheckEdit";
            this.TeamPresentCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.TeamPresentCheckEdit.Properties.ReadOnly = true;
            this.TeamPresentCheckEdit.Properties.ValueChecked = 1;
            this.TeamPresentCheckEdit.Properties.ValueUnchecked = 0;
            this.TeamPresentCheckEdit.Size = new System.Drawing.Size(114, 19);
            this.TeamPresentCheckEdit.StyleController = this.dataLayoutControl1;
            this.TeamPresentCheckEdit.TabIndex = 85;
            // 
            // NoAccessCheckEdit
            // 
            this.NoAccessCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "NoAccess", true));
            this.NoAccessCheckEdit.Location = new System.Drawing.Point(406, 247);
            this.NoAccessCheckEdit.MenuManager = this.barManager1;
            this.NoAccessCheckEdit.Name = "NoAccessCheckEdit";
            this.NoAccessCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NoAccessCheckEdit.Properties.ValueChecked = 1;
            this.NoAccessCheckEdit.Properties.ValueUnchecked = 0;
            this.NoAccessCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.NoAccessCheckEdit.StyleController = this.dataLayoutControl1;
            this.NoAccessCheckEdit.TabIndex = 81;
            this.NoAccessCheckEdit.Validated += new System.EventHandler(this.NoAccessCheckEdit_Validated);
            // 
            // ServiceFailureCheckEdit
            // 
            this.ServiceFailureCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "ServiceFailure", true));
            this.ServiceFailureCheckEdit.Location = new System.Drawing.Point(1030, 12);
            this.ServiceFailureCheckEdit.MenuManager = this.barManager1;
            this.ServiceFailureCheckEdit.Name = "ServiceFailureCheckEdit";
            this.ServiceFailureCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ServiceFailureCheckEdit.Properties.ValueChecked = 1;
            this.ServiceFailureCheckEdit.Properties.ValueUnchecked = 0;
            this.ServiceFailureCheckEdit.Size = new System.Drawing.Size(80, 19);
            this.ServiceFailureCheckEdit.StyleController = this.dataLayoutControl1;
            this.ServiceFailureCheckEdit.TabIndex = 86;
            // 
            // CommentsMemoEdit
            // 
            this.CommentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "Comments", true));
            this.CommentsMemoEdit.Location = new System.Drawing.Point(175, 577);
            this.CommentsMemoEdit.MenuManager = this.barManager1;
            this.CommentsMemoEdit.Name = "CommentsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.CommentsMemoEdit, true);
            this.CommentsMemoEdit.Size = new System.Drawing.Size(911, 117);
            this.scSpellChecker.SetSpellCheckerOptions(this.CommentsMemoEdit, optionsSpelling36);
            this.CommentsMemoEdit.StyleController = this.dataLayoutControl1;
            this.CommentsMemoEdit.TabIndex = 87;
            // 
            // OriginalSubContractorNameButtonEdit
            // 
            this.OriginalSubContractorNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "OriginalSubContractorName", true));
            this.OriginalSubContractorNameButtonEdit.Location = new System.Drawing.Point(672, 59);
            this.OriginalSubContractorNameButtonEdit.MenuManager = this.barManager1;
            this.OriginalSubContractorNameButtonEdit.Name = "OriginalSubContractorNameButtonEdit";
            this.OriginalSubContractorNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.OriginalSubContractorNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.OriginalSubContractorNameButtonEdit.Size = new System.Drawing.Size(438, 20);
            this.OriginalSubContractorNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.OriginalSubContractorNameButtonEdit.TabIndex = 90;
            this.OriginalSubContractorNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.OriginalSubContractorNameButtonEdit_ButtonClick);
            // 
            // JobStatusIDGridLookUpEdit
            // 
            this.JobStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "JobStatusID", true));
            this.JobStatusIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.JobStatusIDGridLookUpEdit.Location = new System.Drawing.Point(672, 131);
            this.JobStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.JobStatusIDGridLookUpEdit.Name = "JobStatusIDGridLookUpEdit";
            this.JobStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.JobStatusIDGridLookUpEdit.Properties.DataSource = this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource;
            this.JobStatusIDGridLookUpEdit.Properties.DisplayMember = "CalloutStatusDescription";
            this.JobStatusIDGridLookUpEdit.Properties.NullText = "";
            this.JobStatusIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.JobStatusIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.JobStatusIDGridLookUpEdit.Properties.ValueMember = "CalloutStatusID";
            this.JobStatusIDGridLookUpEdit.Size = new System.Drawing.Size(438, 20);
            this.JobStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.JobStatusIDGridLookUpEdit.TabIndex = 37;
            this.JobStatusIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.JobStatusIDGridLookUpEdit_EditValueChanged);
            this.JobStatusIDGridLookUpEdit.Enter += new System.EventHandler(this.JobStatusIDGridLookUpEdit_Enter);
            this.JobStatusIDGridLookUpEdit.Leave += new System.EventHandler(this.JobStatusIDGridLookUpEdit_Leave);
            this.JobStatusIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobStatusIDGridLookUpEdit_Validating);
            // 
            // sp04085GCGrittingCalloutStatusListWithBlankBindingSource
            // 
            this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource.DataMember = "sp04085_GC_Gritting_Callout_Status_List_With_Blank";
            this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCalloutStatusDescription,
            this.colCalloutStatusID,
            this.colCalloutStatusOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colCalloutStatusID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowFilterEditor = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCalloutStatusOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCalloutStatusDescription
            // 
            this.colCalloutStatusDescription.Caption = "Callout Status";
            this.colCalloutStatusDescription.FieldName = "CalloutStatusDescription";
            this.colCalloutStatusDescription.Name = "colCalloutStatusDescription";
            this.colCalloutStatusDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusDescription.Visible = true;
            this.colCalloutStatusDescription.VisibleIndex = 0;
            this.colCalloutStatusDescription.Width = 271;
            // 
            // colCalloutStatusOrder
            // 
            this.colCalloutStatusOrder.Caption = "Status Order";
            this.colCalloutStatusOrder.FieldName = "CalloutStatusOrder";
            this.colCalloutStatusOrder.Name = "colCalloutStatusOrder";
            this.colCalloutStatusOrder.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusOrder.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusOrder.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusOrder.Width = 97;
            // 
            // GrittingInvoiceIDButtonEdit
            // 
            this.GrittingInvoiceIDButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "GrittingInvoiceID", true));
            this.GrittingInvoiceIDButtonEdit.Location = new System.Drawing.Point(187, 634);
            this.GrittingInvoiceIDButtonEdit.MenuManager = this.barManager1;
            this.GrittingInvoiceIDButtonEdit.Name = "GrittingInvoiceIDButtonEdit";
            this.GrittingInvoiceIDButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.GrittingInvoiceIDButtonEdit.Properties.ReadOnly = true;
            this.GrittingInvoiceIDButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.GrittingInvoiceIDButtonEdit.Size = new System.Drawing.Size(320, 20);
            this.GrittingInvoiceIDButtonEdit.StyleController = this.dataLayoutControl1;
            this.GrittingInvoiceIDButtonEdit.TabIndex = 71;
            // 
            // AbortedReasonMemoEdit
            // 
            this.AbortedReasonMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "AbortedReason", true));
            this.AbortedReasonMemoEdit.Location = new System.Drawing.Point(36, 577);
            this.AbortedReasonMemoEdit.MenuManager = this.barManager1;
            this.AbortedReasonMemoEdit.Name = "AbortedReasonMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbortedReasonMemoEdit, true);
            this.AbortedReasonMemoEdit.Size = new System.Drawing.Size(1050, 270);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbortedReasonMemoEdit, optionsSpelling37);
            this.AbortedReasonMemoEdit.StyleController = this.dataLayoutControl1;
            this.AbortedReasonMemoEdit.TabIndex = 47;
            // 
            // EveningRateModifierSpinEdit
            // 
            this.EveningRateModifierSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "EveningRateModifier", true));
            this.EveningRateModifierSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EveningRateModifierSpinEdit.Location = new System.Drawing.Point(187, 755);
            this.EveningRateModifierSpinEdit.MenuManager = this.barManager1;
            this.EveningRateModifierSpinEdit.Name = "EveningRateModifierSpinEdit";
            this.EveningRateModifierSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EveningRateModifierSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.EveningRateModifierSpinEdit.Properties.Mask.EditMask = "f2";
            this.EveningRateModifierSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EveningRateModifierSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.EveningRateModifierSpinEdit.Properties.ReadOnly = true;
            this.EveningRateModifierSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.EveningRateModifierSpinEdit.StyleController = this.dataLayoutControl1;
            this.EveningRateModifierSpinEdit.TabIndex = 115;
            // 
            // SitePostcodeButtonEdit
            // 
            this.SitePostcodeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04076GCGrittingCalloutEditBindingSource, "SitePostcode", true));
            this.SitePostcodeButtonEdit.Location = new System.Drawing.Point(175, 723);
            this.SitePostcodeButtonEdit.MenuManager = this.barManager1;
            this.SitePostcodeButtonEdit.Name = "SitePostcodeButtonEdit";
            this.SitePostcodeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Lookup Latitude \\ Longitude", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "", "lookup", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SitePostcodeButtonEdit.Size = new System.Drawing.Size(911, 20);
            this.SitePostcodeButtonEdit.StyleController = this.dataLayoutControl1;
            this.SitePostcodeButtonEdit.TabIndex = 114;
            this.SitePostcodeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SitePostcodeButtonEdit_ButtonClick);
            // 
            // ItemForGrittingCallOutID
            // 
            this.ItemForGrittingCallOutID.Control = this.GrittingCallOutIDTextEdit;
            this.ItemForGrittingCallOutID.CustomizationFormText = "Gritting Call Out ID";
            this.ItemForGrittingCallOutID.Location = new System.Drawing.Point(0, 0);
            this.ItemForGrittingCallOutID.Name = "ItemForGrittingCallOutID";
            this.ItemForGrittingCallOutID.Size = new System.Drawing.Size(591, 24);
            this.ItemForGrittingCallOutID.Text = "Gritting Callout ID:";
            this.ItemForGrittingCallOutID.TextSize = new System.Drawing.Size(169, 13);
            // 
            // ItemForOriginalSubContractorID
            // 
            this.ItemForOriginalSubContractorID.Control = this.OriginalSubContractorIDTextEdit;
            this.ItemForOriginalSubContractorID.CustomizationFormText = "Original Team ID:";
            this.ItemForOriginalSubContractorID.Location = new System.Drawing.Point(0, 0);
            this.ItemForOriginalSubContractorID.Name = "ItemForOriginalSubContractorID";
            this.ItemForOriginalSubContractorID.Size = new System.Drawing.Size(591, 24);
            this.ItemForOriginalSubContractorID.Text = "Original Team ID:";
            this.ItemForOriginalSubContractorID.TextSize = new System.Drawing.Size(169, 13);
            // 
            // ItemForImportedWeatherForecastID
            // 
            this.ItemForImportedWeatherForecastID.Control = this.ImportedWeatherForecastIDTextEdit;
            this.ItemForImportedWeatherForecastID.CustomizationFormText = "Imported Weather Forecast ID:";
            this.ItemForImportedWeatherForecastID.Location = new System.Drawing.Point(0, 23);
            this.ItemForImportedWeatherForecastID.Name = "ItemForImportedWeatherForecastID";
            this.ItemForImportedWeatherForecastID.Size = new System.Drawing.Size(591, 24);
            this.ItemForImportedWeatherForecastID.Text = "Imported Weather Forecast ID:";
            this.ItemForImportedWeatherForecastID.TextSize = new System.Drawing.Size(169, 13);
            // 
            // ItemForAuthorisedByStaffID
            // 
            this.ItemForAuthorisedByStaffID.Control = this.AuthorisedByStaffIDTextEdit;
            this.ItemForAuthorisedByStaffID.CustomizationFormText = "Authorised By Staff ID:";
            this.ItemForAuthorisedByStaffID.Location = new System.Drawing.Point(0, 143);
            this.ItemForAuthorisedByStaffID.Name = "ItemForAuthorisedByStaffID";
            this.ItemForAuthorisedByStaffID.Size = new System.Drawing.Size(591, 24);
            this.ItemForAuthorisedByStaffID.Text = "Authorised By Staff ID:";
            this.ItemForAuthorisedByStaffID.TextSize = new System.Drawing.Size(169, 13);
            // 
            // ItemForSiteGrittingContractID
            // 
            this.ItemForSiteGrittingContractID.Control = this.SiteGrittingContractIDGridLookUpEdit;
            this.ItemForSiteGrittingContractID.CustomizationFormText = "Site Gritting Contract ID:";
            this.ItemForSiteGrittingContractID.Location = new System.Drawing.Point(0, 377);
            this.ItemForSiteGrittingContractID.Name = "ItemForSiteGrittingContractID";
            this.ItemForSiteGrittingContractID.Size = new System.Drawing.Size(774, 24);
            this.ItemForSiteGrittingContractID.Text = "Site Gritting Contract ID:";
            this.ItemForSiteGrittingContractID.TextSize = new System.Drawing.Size(169, 13);
            // 
            // ItemForRecordedByStaffID
            // 
            this.ItemForRecordedByStaffID.Control = this.RecordedByStaffIDTextEdit;
            this.ItemForRecordedByStaffID.CustomizationFormText = "Recorded By Staff ID:";
            this.ItemForRecordedByStaffID.Location = new System.Drawing.Point(0, 191);
            this.ItemForRecordedByStaffID.Name = "ItemForRecordedByStaffID";
            this.ItemForRecordedByStaffID.Size = new System.Drawing.Size(774, 24);
            this.ItemForRecordedByStaffID.Text = "Recorded By Staff ID:";
            this.ItemForRecordedByStaffID.TextSize = new System.Drawing.Size(169, 13);
            // 
            // ItemForGritSourceLocationID
            // 
            this.ItemForGritSourceLocationID.Control = this.GritSourceLocationIDTextEdit;
            this.ItemForGritSourceLocationID.CustomizationFormText = "Grit Source Location ID:";
            this.ItemForGritSourceLocationID.Location = new System.Drawing.Point(0, 0);
            this.ItemForGritSourceLocationID.Name = "ItemForGritSourceLocationID";
            this.ItemForGritSourceLocationID.Size = new System.Drawing.Size(920, 24);
            this.ItemForGritSourceLocationID.Text = "Grit Source Location ID:";
            this.ItemForGritSourceLocationID.TextSize = new System.Drawing.Size(169, 13);
            // 
            // ItemForGritSourceLocationTypeID
            // 
            this.ItemForGritSourceLocationTypeID.Control = this.GritSourceLocationTypeIDTextEdit;
            this.ItemForGritSourceLocationTypeID.CustomizationFormText = "Grit Source Location Type:";
            this.ItemForGritSourceLocationTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForGritSourceLocationTypeID.Name = "ItemForGritSourceLocationTypeID";
            this.ItemForGritSourceLocationTypeID.Size = new System.Drawing.Size(920, 24);
            this.ItemForGritSourceLocationTypeID.Text = "Grit Source Location Type:";
            this.ItemForGritSourceLocationTypeID.TextSize = new System.Drawing.Size(169, 13);
            // 
            // ItemForPaidByStaffID
            // 
            this.ItemForPaidByStaffID.Control = this.PaidByStaffIDTextEdit;
            this.ItemForPaidByStaffID.CustomizationFormText = "Paid By Staff ID";
            this.ItemForPaidByStaffID.Location = new System.Drawing.Point(0, 47);
            this.ItemForPaidByStaffID.Name = "ItemForPaidByStaffID";
            this.ItemForPaidByStaffID.Size = new System.Drawing.Size(390, 24);
            this.ItemForPaidByStaffID.Text = "Paid By Staff ID";
            this.ItemForPaidByStaffID.TextSize = new System.Drawing.Size(127, 13);
            // 
            // ItemForClientPOID
            // 
            this.ItemForClientPOID.Control = this.ClientPOIDTextEdit;
            this.ItemForClientPOID.CustomizationFormText = "Linked Client PO ID:";
            this.ItemForClientPOID.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientPOID.Name = "ItemForClientPOID";
            this.ItemForClientPOID.Size = new System.Drawing.Size(920, 24);
            this.ItemForClientPOID.Text = "Linked Client PO ID:";
            this.ItemForClientPOID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteWeather
            // 
            this.ItemForSiteWeather.Control = this.SiteWeatherTextEdit;
            this.ItemForSiteWeather.CustomizationFormText = "Site Weather:";
            this.ItemForSiteWeather.Location = new System.Drawing.Point(0, 0);
            this.ItemForSiteWeather.Name = "ItemForSiteWeather";
            this.ItemForSiteWeather.Size = new System.Drawing.Size(410, 24);
            this.ItemForSiteWeather.Text = "Site Weather:";
            this.ItemForSiteWeather.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForPdaID
            // 
            this.ItemForPdaID.Control = this.PdaIDTextEdit;
            this.ItemForPdaID.CustomizationFormText = "Device ID:";
            this.ItemForPdaID.Location = new System.Drawing.Point(0, 171);
            this.ItemForPdaID.Name = "ItemForPdaID";
            this.ItemForPdaID.Size = new System.Drawing.Size(462, 24);
            this.ItemForPdaID.Text = "Device ID:";
            this.ItemForPdaID.TextSize = new System.Drawing.Size(169, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.layoutControlGroup9});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1122, 893);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.ItemForSubContractorName,
            this.ItemForOriginalSubContractorName,
            this.ItemForReactive,
            this.ItemForServiceFailure,
            this.ItemForSiteName,
            this.ItemForClientName,
            this.ItemForClientPOIDDescription,
            this.ItemForClientPONumber,
            this.ItemForJobStatusID,
            this.ItemForPDACode,
            this.ItemForRecordedByName,
            this.layoutControlGroup5,
            this.layoutControlGroup23,
            this.emptySpaceItem21,
            this.ItemForJobSheetNumber,
            this.ItemForSiteManagerName,
            this.ItemForIsFloatingSite,
            this.ItemForAttendanceOrder,
            this.emptySpaceItem20,
            this.ItemForDoubleGrit,
            this.emptySpaceItem25,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.emptySpaceItem26,
            this.ItemForCompletionEmailSent});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1102, 488);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(313, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(626, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(140, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(173, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(140, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(140, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(140, 23);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSubContractorName
            // 
            this.ItemForSubContractorName.Control = this.SubContractorNameButtonEdit;
            this.ItemForSubContractorName.CustomizationFormText = "Team Name:";
            this.ItemForSubContractorName.Location = new System.Drawing.Point(0, 47);
            this.ItemForSubContractorName.MaxSize = new System.Drawing.Size(521, 24);
            this.ItemForSubContractorName.MinSize = new System.Drawing.Size(521, 24);
            this.ItemForSubContractorName.Name = "ItemForSubContractorName";
            this.ItemForSubContractorName.Size = new System.Drawing.Size(521, 24);
            this.ItemForSubContractorName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSubContractorName.Text = "Team Name:";
            this.ItemForSubContractorName.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForOriginalSubContractorName
            // 
            this.ItemForOriginalSubContractorName.Control = this.OriginalSubContractorNameButtonEdit;
            this.ItemForOriginalSubContractorName.CustomizationFormText = "Original Team Name:";
            this.ItemForOriginalSubContractorName.Location = new System.Drawing.Point(521, 47);
            this.ItemForOriginalSubContractorName.Name = "ItemForOriginalSubContractorName";
            this.ItemForOriginalSubContractorName.Size = new System.Drawing.Size(581, 24);
            this.ItemForOriginalSubContractorName.Text = "Original Team Name:";
            this.ItemForOriginalSubContractorName.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 119);
            this.ItemForReactive.MaxSize = new System.Drawing.Size(233, 23);
            this.ItemForReactive.MinSize = new System.Drawing.Size(233, 23);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(233, 24);
            this.ItemForReactive.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForServiceFailure
            // 
            this.ItemForServiceFailure.Control = this.ServiceFailureCheckEdit;
            this.ItemForServiceFailure.CustomizationFormText = "Service Failure:";
            this.ItemForServiceFailure.Location = new System.Drawing.Point(939, 0);
            this.ItemForServiceFailure.MaxSize = new System.Drawing.Size(163, 23);
            this.ItemForServiceFailure.MinSize = new System.Drawing.Size(163, 23);
            this.ItemForServiceFailure.Name = "ItemForServiceFailure";
            this.ItemForServiceFailure.Size = new System.Drawing.Size(163, 23);
            this.ItemForServiceFailure.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForServiceFailure.Text = "Service Failure:";
            this.ItemForServiceFailure.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForServiceFailure.TextSize = new System.Drawing.Size(74, 13);
            this.ItemForServiceFailure.TextToControlDistance = 5;
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameButtonEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 23);
            this.ItemForSiteName.MaxSize = new System.Drawing.Size(521, 24);
            this.ItemForSiteName.MinSize = new System.Drawing.Size(521, 24);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(521, 24);
            this.ItemForSiteName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name";
            this.ItemForClientName.Location = new System.Drawing.Point(521, 23);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(581, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForClientPOIDDescription
            // 
            this.ItemForClientPOIDDescription.Control = this.ClientPOIDDescriptionButtonEdit;
            this.ItemForClientPOIDDescription.CustomizationFormText = "Linked Client PO Number:";
            this.ItemForClientPOIDDescription.Location = new System.Drawing.Point(0, 95);
            this.ItemForClientPOIDDescription.MaxSize = new System.Drawing.Size(521, 24);
            this.ItemForClientPOIDDescription.MinSize = new System.Drawing.Size(521, 24);
            this.ItemForClientPOIDDescription.Name = "ItemForClientPOIDDescription";
            this.ItemForClientPOIDDescription.Size = new System.Drawing.Size(521, 24);
            this.ItemForClientPOIDDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientPOIDDescription.Text = "Linked Client PO Number:";
            this.ItemForClientPOIDDescription.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberTextEdit;
            this.ItemForClientPONumber.CustomizationFormText = "Client PO Number:";
            this.ItemForClientPONumber.Location = new System.Drawing.Point(521, 95);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(581, 24);
            this.ItemForClientPONumber.Text = "Client PO Number:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForJobStatusID
            // 
            this.ItemForJobStatusID.Control = this.JobStatusIDGridLookUpEdit;
            this.ItemForJobStatusID.CustomizationFormText = "Callout Status:";
            this.ItemForJobStatusID.Location = new System.Drawing.Point(521, 119);
            this.ItemForJobStatusID.Name = "ItemForJobStatusID";
            this.ItemForJobStatusID.Size = new System.Drawing.Size(581, 24);
            this.ItemForJobStatusID.Text = "Callout Status:";
            this.ItemForJobStatusID.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForPDACode
            // 
            this.ItemForPDACode.Control = this.PDACodeButtonEdit;
            this.ItemForPDACode.CustomizationFormText = "Assigned To Device:";
            this.ItemForPDACode.Location = new System.Drawing.Point(0, 71);
            this.ItemForPDACode.MaxSize = new System.Drawing.Size(521, 24);
            this.ItemForPDACode.MinSize = new System.Drawing.Size(521, 24);
            this.ItemForPDACode.Name = "ItemForPDACode";
            this.ItemForPDACode.Size = new System.Drawing.Size(521, 24);
            this.ItemForPDACode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPDACode.Text = "Assigned To Device:";
            this.ItemForPDACode.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForRecordedByName
            // 
            this.ItemForRecordedByName.Control = this.RecordedByNameTextEdit;
            this.ItemForRecordedByName.CustomizationFormText = "Recorded By Name:";
            this.ItemForRecordedByName.Location = new System.Drawing.Point(521, 71);
            this.ItemForRecordedByName.Name = "ItemForRecordedByName";
            this.ItemForRecordedByName.Size = new System.Drawing.Size(581, 24);
            this.ItemForRecordedByName.Text = "Recorded By Name:";
            this.ItemForRecordedByName.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Weather";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem23,
            this.ItemForWeather1,
            this.ItemForWeather2,
            this.ItemForWeather3,
            this.ItemForWeather4,
            this.ItemForWeather5,
            this.ItemForSiteTemperature,
            this.ItemForSnowOnSite,
            this.ItemForSnowOnSiteOver5cm,
            this.ItemForSnowCleared,
            this.ItemForSnowClearedSatisfactory});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 201);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(243, 287);
            this.layoutControlGroup5.Text = "Weather";
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.CustomizationFormText = "emptySpaceItem23";
            this.emptySpaceItem23.Location = new System.Drawing.Point(0, 231);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(219, 10);
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWeather1
            // 
            this.ItemForWeather1.Control = this.Weather1CheckEdit;
            this.ItemForWeather1.CustomizationFormText = "Snow:";
            this.ItemForWeather1.Location = new System.Drawing.Point(0, 0);
            this.ItemForWeather1.MaxSize = new System.Drawing.Size(219, 23);
            this.ItemForWeather1.MinSize = new System.Drawing.Size(219, 23);
            this.ItemForWeather1.Name = "ItemForWeather1";
            this.ItemForWeather1.Size = new System.Drawing.Size(219, 23);
            this.ItemForWeather1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWeather1.Text = "Snow:";
            this.ItemForWeather1.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForWeather2
            // 
            this.ItemForWeather2.Control = this.Weather2CheckEdit;
            this.ItemForWeather2.CustomizationFormText = "Ice:";
            this.ItemForWeather2.Location = new System.Drawing.Point(0, 23);
            this.ItemForWeather2.Name = "ItemForWeather2";
            this.ItemForWeather2.Size = new System.Drawing.Size(219, 23);
            this.ItemForWeather2.Text = "Ice:";
            this.ItemForWeather2.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForWeather3
            // 
            this.ItemForWeather3.Control = this.Weather3CheckEdit;
            this.ItemForWeather3.CustomizationFormText = "Rain:";
            this.ItemForWeather3.Location = new System.Drawing.Point(0, 46);
            this.ItemForWeather3.Name = "ItemForWeather3";
            this.ItemForWeather3.Size = new System.Drawing.Size(219, 23);
            this.ItemForWeather3.Text = "Rain:";
            this.ItemForWeather3.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForWeather4
            // 
            this.ItemForWeather4.Control = this.Weather4CheckEdit;
            this.ItemForWeather4.CustomizationFormText = "Dry:";
            this.ItemForWeather4.Location = new System.Drawing.Point(0, 69);
            this.ItemForWeather4.Name = "ItemForWeather4";
            this.ItemForWeather4.Size = new System.Drawing.Size(219, 23);
            this.ItemForWeather4.Text = "Dry:";
            this.ItemForWeather4.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForWeather5
            // 
            this.ItemForWeather5.Control = this.Weather5CheckEdit;
            this.ItemForWeather5.CustomizationFormText = "Wet:";
            this.ItemForWeather5.Location = new System.Drawing.Point(0, 92);
            this.ItemForWeather5.Name = "ItemForWeather5";
            this.ItemForWeather5.Size = new System.Drawing.Size(219, 23);
            this.ItemForWeather5.Text = "Wet:";
            this.ItemForWeather5.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSiteTemperature
            // 
            this.ItemForSiteTemperature.Control = this.SiteTemperatureSpinEdit;
            this.ItemForSiteTemperature.CustomizationFormText = "Site Temperature:";
            this.ItemForSiteTemperature.Location = new System.Drawing.Point(0, 115);
            this.ItemForSiteTemperature.Name = "ItemForSiteTemperature";
            this.ItemForSiteTemperature.Size = new System.Drawing.Size(219, 24);
            this.ItemForSiteTemperature.Text = "Site Temperature:";
            this.ItemForSiteTemperature.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSnowOnSite
            // 
            this.ItemForSnowOnSite.Control = this.SnowOnSiteCheckEdit;
            this.ItemForSnowOnSite.CustomizationFormText = "Snow On Site:";
            this.ItemForSnowOnSite.Location = new System.Drawing.Point(0, 139);
            this.ItemForSnowOnSite.Name = "ItemForSnowOnSite";
            this.ItemForSnowOnSite.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowOnSite.Text = "Snow On Site:";
            this.ItemForSnowOnSite.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSnowOnSiteOver5cm
            // 
            this.ItemForSnowOnSiteOver5cm.Control = this.SnowOnSiteOver5cmCheckEdit;
            this.ItemForSnowOnSiteOver5cm.CustomizationFormText = "Snow Over5 cm:";
            this.ItemForSnowOnSiteOver5cm.Location = new System.Drawing.Point(0, 162);
            this.ItemForSnowOnSiteOver5cm.Name = "ItemForSnowOnSiteOver5cm";
            this.ItemForSnowOnSiteOver5cm.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowOnSiteOver5cm.Text = "Snow Over5 cm:";
            this.ItemForSnowOnSiteOver5cm.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSnowCleared
            // 
            this.ItemForSnowCleared.Control = this.SnowClearedCheckEdit;
            this.ItemForSnowCleared.CustomizationFormText = "Snow Cleared:";
            this.ItemForSnowCleared.Location = new System.Drawing.Point(0, 185);
            this.ItemForSnowCleared.Name = "ItemForSnowCleared";
            this.ItemForSnowCleared.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowCleared.Text = "Snow Cleared:";
            this.ItemForSnowCleared.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSnowClearedSatisfactory
            // 
            this.ItemForSnowClearedSatisfactory.Control = this.SnowClearedSatisfactoryCheckEdit;
            this.ItemForSnowClearedSatisfactory.CustomizationFormText = "Snow Cleared Ok:";
            this.ItemForSnowClearedSatisfactory.Location = new System.Drawing.Point(0, 208);
            this.ItemForSnowClearedSatisfactory.Name = "ItemForSnowClearedSatisfactory";
            this.ItemForSnowClearedSatisfactory.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearedSatisfactory.Text = "Snow Cleared Ok:";
            this.ItemForSnowClearedSatisfactory.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroup23
            // 
            this.layoutControlGroup23.CustomizationFormText = "Access";
            this.layoutControlGroup23.ExpandButtonVisible = true;
            this.layoutControlGroup23.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup23.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNoAccess,
            this.emptySpaceItem22,
            this.ItemForVisitAborted,
            this.ItemForNoAccessAbortedRate});
            this.layoutControlGroup23.Location = new System.Drawing.Point(243, 201);
            this.layoutControlGroup23.Name = "layoutControlGroup23";
            this.layoutControlGroup23.Size = new System.Drawing.Size(243, 287);
            this.layoutControlGroup23.Text = "Access";
            // 
            // ItemForNoAccess
            // 
            this.ItemForNoAccess.Control = this.NoAccessCheckEdit;
            this.ItemForNoAccess.CustomizationFormText = "No Access:";
            this.ItemForNoAccess.Location = new System.Drawing.Point(0, 0);
            this.ItemForNoAccess.MaxSize = new System.Drawing.Size(219, 23);
            this.ItemForNoAccess.MinSize = new System.Drawing.Size(219, 23);
            this.ItemForNoAccess.Name = "ItemForNoAccess";
            this.ItemForNoAccess.Size = new System.Drawing.Size(219, 23);
            this.ItemForNoAccess.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNoAccess.Text = "No Access:";
            this.ItemForNoAccess.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.CustomizationFormText = "emptySpaceItem22";
            this.emptySpaceItem22.Location = new System.Drawing.Point(0, 70);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(219, 171);
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForVisitAborted
            // 
            this.ItemForVisitAborted.Control = this.VisitAbortedCheckEdit;
            this.ItemForVisitAborted.CustomizationFormText = "Visit Aborted";
            this.ItemForVisitAborted.Location = new System.Drawing.Point(0, 23);
            this.ItemForVisitAborted.Name = "ItemForVisitAborted";
            this.ItemForVisitAborted.Size = new System.Drawing.Size(219, 23);
            this.ItemForVisitAborted.Text = "Visit Aborted:";
            this.ItemForVisitAborted.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForNoAccessAbortedRate
            // 
            this.ItemForNoAccessAbortedRate.Control = this.NoAccessAbortedRateSpinEdit;
            this.ItemForNoAccessAbortedRate.CustomizationFormText = "No Access \\ Aborted Rate:";
            this.ItemForNoAccessAbortedRate.Location = new System.Drawing.Point(0, 46);
            this.ItemForNoAccessAbortedRate.Name = "ItemForNoAccessAbortedRate";
            this.ItemForNoAccessAbortedRate.Size = new System.Drawing.Size(219, 24);
            this.ItemForNoAccessAbortedRate.Text = "No Access \\ Aborted Rate:";
            this.ItemForNoAccessAbortedRate.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.CustomizationFormText = "emptySpaceItem21";
            this.emptySpaceItem21.Location = new System.Drawing.Point(0, 191);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(1102, 10);
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForJobSheetNumber
            // 
            this.ItemForJobSheetNumber.Control = this.JobSheetNumberTextEdit;
            this.ItemForJobSheetNumber.CustomizationFormText = "Job Sheet Number:";
            this.ItemForJobSheetNumber.Location = new System.Drawing.Point(0, 143);
            this.ItemForJobSheetNumber.MaxSize = new System.Drawing.Size(521, 24);
            this.ItemForJobSheetNumber.MinSize = new System.Drawing.Size(521, 24);
            this.ItemForJobSheetNumber.Name = "ItemForJobSheetNumber";
            this.ItemForJobSheetNumber.Size = new System.Drawing.Size(521, 24);
            this.ItemForJobSheetNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForJobSheetNumber.Text = "Job Sheet Number:";
            this.ItemForJobSheetNumber.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSiteManagerName
            // 
            this.ItemForSiteManagerName.Control = this.SiteManagerNameTextEdit;
            this.ItemForSiteManagerName.CustomizationFormText = "Site Manager Name:";
            this.ItemForSiteManagerName.Location = new System.Drawing.Point(521, 143);
            this.ItemForSiteManagerName.Name = "ItemForSiteManagerName";
            this.ItemForSiteManagerName.Size = new System.Drawing.Size(581, 24);
            this.ItemForSiteManagerName.Text = "Site Manager Name:";
            this.ItemForSiteManagerName.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForIsFloatingSite
            // 
            this.ItemForIsFloatingSite.Control = this.IsFloatingSiteCheckEdit;
            this.ItemForIsFloatingSite.CustomizationFormText = "Floating Site:";
            this.ItemForIsFloatingSite.Location = new System.Drawing.Point(307, 119);
            this.ItemForIsFloatingSite.MaxSize = new System.Drawing.Size(148, 23);
            this.ItemForIsFloatingSite.MinSize = new System.Drawing.Size(148, 23);
            this.ItemForIsFloatingSite.Name = "ItemForIsFloatingSite";
            this.ItemForIsFloatingSite.Size = new System.Drawing.Size(148, 24);
            this.ItemForIsFloatingSite.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIsFloatingSite.Text = "Floating Site:";
            this.ItemForIsFloatingSite.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForIsFloatingSite.TextSize = new System.Drawing.Size(63, 13);
            this.ItemForIsFloatingSite.TextToControlDistance = 5;
            // 
            // ItemForAttendanceOrder
            // 
            this.ItemForAttendanceOrder.Control = this.AttendanceOrderSpinEdit;
            this.ItemForAttendanceOrder.CustomizationFormText = "Attendance Order:";
            this.ItemForAttendanceOrder.Location = new System.Drawing.Point(0, 167);
            this.ItemForAttendanceOrder.MaxSize = new System.Drawing.Size(521, 24);
            this.ItemForAttendanceOrder.MinSize = new System.Drawing.Size(521, 24);
            this.ItemForAttendanceOrder.Name = "ItemForAttendanceOrder";
            this.ItemForAttendanceOrder.Size = new System.Drawing.Size(521, 24);
            this.ItemForAttendanceOrder.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAttendanceOrder.Text = "Attendance Order:";
            this.ItemForAttendanceOrder.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.CustomizationFormText = "emptySpaceItem20";
            this.emptySpaceItem20.Location = new System.Drawing.Point(761, 167);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(122, 24);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDoubleGrit
            // 
            this.ItemForDoubleGrit.Control = this.DoubleGritCheckEdit;
            this.ItemForDoubleGrit.Location = new System.Drawing.Point(521, 167);
            this.ItemForDoubleGrit.MaxSize = new System.Drawing.Size(240, 23);
            this.ItemForDoubleGrit.MinSize = new System.Drawing.Size(240, 23);
            this.ItemForDoubleGrit.Name = "ItemForDoubleGrit";
            this.ItemForDoubleGrit.Size = new System.Drawing.Size(240, 24);
            this.ItemForDoubleGrit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDoubleGrit.Text = "Double Grit:";
            this.ItemForDoubleGrit.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem25
            // 
            this.emptySpaceItem25.AllowHotTrack = false;
            this.emptySpaceItem25.Location = new System.Drawing.Point(455, 119);
            this.emptySpaceItem25.MaxSize = new System.Drawing.Size(66, 0);
            this.emptySpaceItem25.MinSize = new System.Drawing.Size(66, 10);
            this.emptySpaceItem25.Name = "emptySpaceItem25";
            this.emptySpaceItem25.Size = new System.Drawing.Size(66, 24);
            this.emptySpaceItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem25.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Timings";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCallOutDateTime,
            this.ItemForTextSentTime,
            this.ItemForCompletedTime,
            this.ItemForSubContractorReceivedTime,
            this.ItemForSubContractorRespondedTime,
            this.ItemForSubContractorETA,
            this.emptySpaceItem6,
            this.ItemForTeamPresent,
            this.ItemForStartTime,
            this.ItemForPPEWorn});
            this.layoutControlGroup3.Location = new System.Drawing.Point(486, 201);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(281, 287);
            this.layoutControlGroup3.Text = "Timings";
            // 
            // ItemForCallOutDateTime
            // 
            this.ItemForCallOutDateTime.Control = this.CallOutDateTimeDateEdit;
            this.ItemForCallOutDateTime.CustomizationFormText = "Callout Date \\ Time:";
            this.ItemForCallOutDateTime.Location = new System.Drawing.Point(0, 0);
            this.ItemForCallOutDateTime.MaxSize = new System.Drawing.Size(257, 24);
            this.ItemForCallOutDateTime.MinSize = new System.Drawing.Size(257, 24);
            this.ItemForCallOutDateTime.Name = "ItemForCallOutDateTime";
            this.ItemForCallOutDateTime.Size = new System.Drawing.Size(257, 24);
            this.ItemForCallOutDateTime.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCallOutDateTime.Text = "Callout Date \\ Time:";
            this.ItemForCallOutDateTime.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForTextSentTime
            // 
            this.ItemForTextSentTime.Control = this.TextSentTimeDateEdit;
            this.ItemForTextSentTime.CustomizationFormText = "Text Sent Time:";
            this.ItemForTextSentTime.Location = new System.Drawing.Point(0, 24);
            this.ItemForTextSentTime.Name = "ItemForTextSentTime";
            this.ItemForTextSentTime.Size = new System.Drawing.Size(257, 24);
            this.ItemForTextSentTime.Text = "Text Sent Time:";
            this.ItemForTextSentTime.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForCompletedTime
            // 
            this.ItemForCompletedTime.Control = this.CompletedTimeDateEdit;
            this.ItemForCompletedTime.CustomizationFormText = "Completed Time:";
            this.ItemForCompletedTime.Location = new System.Drawing.Point(0, 167);
            this.ItemForCompletedTime.Name = "ItemForCompletedTime";
            this.ItemForCompletedTime.Size = new System.Drawing.Size(257, 24);
            this.ItemForCompletedTime.Text = "Completed Time:";
            this.ItemForCompletedTime.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSubContractorReceivedTime
            // 
            this.ItemForSubContractorReceivedTime.Control = this.SubContractorReceivedTimeDateEdit;
            this.ItemForSubContractorReceivedTime.CustomizationFormText = "Team Received Time:";
            this.ItemForSubContractorReceivedTime.Location = new System.Drawing.Point(0, 48);
            this.ItemForSubContractorReceivedTime.Name = "ItemForSubContractorReceivedTime";
            this.ItemForSubContractorReceivedTime.Size = new System.Drawing.Size(257, 24);
            this.ItemForSubContractorReceivedTime.Text = "Team Received Time:";
            this.ItemForSubContractorReceivedTime.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSubContractorRespondedTime
            // 
            this.ItemForSubContractorRespondedTime.Control = this.SubContractorRespondedTimeDateEdit;
            this.ItemForSubContractorRespondedTime.CustomizationFormText = "Team Responded Time:";
            this.ItemForSubContractorRespondedTime.Location = new System.Drawing.Point(0, 72);
            this.ItemForSubContractorRespondedTime.Name = "ItemForSubContractorRespondedTime";
            this.ItemForSubContractorRespondedTime.Size = new System.Drawing.Size(257, 24);
            this.ItemForSubContractorRespondedTime.Text = "Team Responded Time:";
            this.ItemForSubContractorRespondedTime.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSubContractorETA
            // 
            this.ItemForSubContractorETA.Control = this.SubContractorETATextEdit;
            this.ItemForSubContractorETA.CustomizationFormText = "Team ETA:";
            this.ItemForSubContractorETA.Location = new System.Drawing.Point(0, 96);
            this.ItemForSubContractorETA.Name = "ItemForSubContractorETA";
            this.ItemForSubContractorETA.Size = new System.Drawing.Size(257, 24);
            this.ItemForSubContractorETA.Text = "Team ETA:";
            this.ItemForSubContractorETA.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 214);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(257, 27);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTeamPresent
            // 
            this.ItemForTeamPresent.Control = this.TeamPresentCheckEdit;
            this.ItemForTeamPresent.CustomizationFormText = "Team Present:";
            this.ItemForTeamPresent.Location = new System.Drawing.Point(0, 120);
            this.ItemForTeamPresent.Name = "ItemForTeamPresent";
            this.ItemForTeamPresent.Size = new System.Drawing.Size(257, 23);
            this.ItemForTeamPresent.Text = "Team Present:";
            this.ItemForTeamPresent.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForStartTime
            // 
            this.ItemForStartTime.Control = this.StartTimeDateEdit;
            this.ItemForStartTime.CustomizationFormText = "Start Time:";
            this.ItemForStartTime.Location = new System.Drawing.Point(0, 143);
            this.ItemForStartTime.Name = "ItemForStartTime";
            this.ItemForStartTime.Size = new System.Drawing.Size(257, 24);
            this.ItemForStartTime.Text = "Start Time:";
            this.ItemForStartTime.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForPPEWorn
            // 
            this.ItemForPPEWorn.Control = this.PPEWornCheckEdit;
            this.ItemForPPEWorn.CustomizationFormText = "PPE Worn:";
            this.ItemForPPEWorn.Location = new System.Drawing.Point(0, 191);
            this.ItemForPPEWorn.Name = "ItemForPPEWorn";
            this.ItemForPPEWorn.Size = new System.Drawing.Size(257, 23);
            this.ItemForPPEWorn.Text = "PPE Worn:";
            this.ItemForPPEWorn.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Location Coordinates";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartLatitude,
            this.ItemForStartLongitude,
            this.ItemForFinishedLatitude,
            this.ItemForFinishedLongitude,
            this.layoutControlItem2,
            this.emptySpaceItem18,
            this.emptySpaceItem27});
            this.layoutControlGroup4.Location = new System.Drawing.Point(767, 201);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(335, 287);
            this.layoutControlGroup4.Text = "Location Coordinates  [Read Only from Device]";
            // 
            // ItemForStartLatitude
            // 
            this.ItemForStartLatitude.Control = this.StartLatitudeTextEdit;
            this.ItemForStartLatitude.CustomizationFormText = "Start Latitude:";
            this.ItemForStartLatitude.Location = new System.Drawing.Point(0, 0);
            this.ItemForStartLatitude.Name = "ItemForStartLatitude";
            this.ItemForStartLatitude.Size = new System.Drawing.Size(311, 24);
            this.ItemForStartLatitude.Text = "Start Latitude:";
            this.ItemForStartLatitude.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForStartLongitude
            // 
            this.ItemForStartLongitude.Control = this.StartLongitudeTextEdit;
            this.ItemForStartLongitude.CustomizationFormText = "Start Longitude:";
            this.ItemForStartLongitude.Location = new System.Drawing.Point(0, 24);
            this.ItemForStartLongitude.Name = "ItemForStartLongitude";
            this.ItemForStartLongitude.Size = new System.Drawing.Size(311, 24);
            this.ItemForStartLongitude.Text = "Start Longitude:";
            this.ItemForStartLongitude.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForFinishedLatitude
            // 
            this.ItemForFinishedLatitude.Control = this.FinishedLatitudeTextEdit;
            this.ItemForFinishedLatitude.CustomizationFormText = "Finished Latitude:";
            this.ItemForFinishedLatitude.Location = new System.Drawing.Point(0, 48);
            this.ItemForFinishedLatitude.Name = "ItemForFinishedLatitude";
            this.ItemForFinishedLatitude.Size = new System.Drawing.Size(311, 24);
            this.ItemForFinishedLatitude.Text = "Finished Latitude:";
            this.ItemForFinishedLatitude.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForFinishedLongitude
            // 
            this.ItemForFinishedLongitude.Control = this.FinishedLongitudeTextEdit;
            this.ItemForFinishedLongitude.CustomizationFormText = "Finished Longitude:";
            this.ItemForFinishedLongitude.Location = new System.Drawing.Point(0, 72);
            this.ItemForFinishedLongitude.Name = "ItemForFinishedLongitude";
            this.ItemForFinishedLongitude.Size = new System.Drawing.Size(311, 24);
            this.ItemForFinishedLongitude.Text = "Finished Longitude:";
            this.ItemForFinishedLongitude.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnShowOnMap;
            this.layoutControlItem2.CustomizationFormText = "Show On Map Button";
            this.layoutControlItem2.Location = new System.Drawing.Point(230, 96);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(81, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Show On Map:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.CustomizationFormText = "emptySpaceItem18";
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(230, 26);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.Location = new System.Drawing.Point(0, 122);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(311, 119);
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem26
            // 
            this.emptySpaceItem26.AllowHotTrack = false;
            this.emptySpaceItem26.Location = new System.Drawing.Point(233, 119);
            this.emptySpaceItem26.MaxSize = new System.Drawing.Size(74, 0);
            this.emptySpaceItem26.MinSize = new System.Drawing.Size(74, 10);
            this.emptySpaceItem26.Name = "emptySpaceItem26";
            this.emptySpaceItem26.Size = new System.Drawing.Size(74, 24);
            this.emptySpaceItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem26.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCompletionEmailSent
            // 
            this.ItemForCompletionEmailSent.Control = this.CompletionEmailSentCheckEdit;
            this.ItemForCompletionEmailSent.CustomizationFormText = "Completion Email Sent:";
            this.ItemForCompletionEmailSent.Location = new System.Drawing.Point(883, 167);
            this.ItemForCompletionEmailSent.MaxSize = new System.Drawing.Size(219, 23);
            this.ItemForCompletionEmailSent.MinSize = new System.Drawing.Size(219, 23);
            this.ItemForCompletionEmailSent.Name = "ItemForCompletionEmailSent";
            this.ItemForCompletionEmailSent.Size = new System.Drawing.Size(219, 24);
            this.ItemForCompletionEmailSent.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCompletionEmailSent.Text = "Completion Email Sent:";
            this.ItemForCompletionEmailSent.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AllowDrawBackground = false;
            this.layoutControlGroup6.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem19});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 488);
            this.layoutControlGroup6.Name = "item0";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1102, 10);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.CustomizationFormText = "emptySpaceItem19";
            this.emptySpaceItem19.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem19.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem19.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(1102, 10);
            this.emptySpaceItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AllowDrawBackground = false;
            this.layoutControlGroup7.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.GroupBordersVisible = false;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup15});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 498);
            this.layoutControlGroup7.Name = "autoGeneratedGroup1";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1102, 365);
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.CustomizationFormText = "Other Details";
            this.layoutControlGroup15.ExpandButtonVisible = true;
            this.layoutControlGroup15.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(1102, 365);
            this.layoutControlGroup15.Text = "Other Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup11;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1078, 319);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup11,
            this.layoutControlGroup18,
            this.layoutControlGroup16,
            this.layoutControlGroup17,
            this.layoutControlGroup19,
            this.layoutControlGroup22,
            this.layoutControlGroup10});
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Costs";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup12,
            this.layoutControlGroup13,
            this.layoutControlGroup14,
            this.emptySpaceItem15,
            this.splitterItem3,
            this.layoutControlGroup21,
            this.splitterItem2});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(1054, 274);
            this.layoutControlGroup11.Text = "Costs";
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Labour";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLabourCost,
            this.emptySpaceItem9,
            this.ItemForClientPrice,
            this.ItemForHoursWorked,
            this.ItemForTeamHourlyRate,
            this.ItemForTeamCharge,
            this.emptySpaceItem13,
            this.ItemForLabourVatRate,
            this.ItemForEveningRateModifier});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(339, 246);
            this.layoutControlGroup12.Text = "Labour";
            // 
            // ItemForLabourCost
            // 
            this.ItemForLabourCost.Control = this.LabourCostSpinEdit;
            this.ItemForLabourCost.CustomizationFormText = "Labour Cost:";
            this.ItemForLabourCost.Location = new System.Drawing.Point(0, 96);
            this.ItemForLabourCost.Name = "ItemForLabourCost";
            this.ItemForLabourCost.Size = new System.Drawing.Size(315, 24);
            this.ItemForLabourCost.Text = "Labour Cost:";
            this.ItemForLabourCost.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 168);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(315, 17);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientPrice
            // 
            this.ItemForClientPrice.Control = this.ClientPriceSpinEdit;
            this.ItemForClientPrice.CustomizationFormText = "Client Price:";
            this.ItemForClientPrice.Location = new System.Drawing.Point(0, 120);
            this.ItemForClientPrice.Name = "ItemForClientPrice";
            this.ItemForClientPrice.Size = new System.Drawing.Size(315, 24);
            this.ItemForClientPrice.Text = "Client Price:";
            this.ItemForClientPrice.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForHoursWorked
            // 
            this.ItemForHoursWorked.Control = this.HoursWorkedSpinEdit;
            this.ItemForHoursWorked.CustomizationFormText = "Hours Worked:";
            this.ItemForHoursWorked.Location = new System.Drawing.Point(0, 0);
            this.ItemForHoursWorked.Name = "ItemForHoursWorked";
            this.ItemForHoursWorked.Size = new System.Drawing.Size(315, 24);
            this.ItemForHoursWorked.Text = "Hours Worked:";
            this.ItemForHoursWorked.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForTeamHourlyRate
            // 
            this.ItemForTeamHourlyRate.Control = this.TeamHourlyRateSpinEdit;
            this.ItemForTeamHourlyRate.CustomizationFormText = "Team Hourly Rate:";
            this.ItemForTeamHourlyRate.Location = new System.Drawing.Point(0, 24);
            this.ItemForTeamHourlyRate.Name = "ItemForTeamHourlyRate";
            this.ItemForTeamHourlyRate.Size = new System.Drawing.Size(315, 24);
            this.ItemForTeamHourlyRate.Text = "Team Hourly Rate:";
            this.ItemForTeamHourlyRate.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForTeamCharge
            // 
            this.ItemForTeamCharge.Control = this.TeamChargeSpinEdit;
            this.ItemForTeamCharge.CustomizationFormText = "Team Charge:";
            this.ItemForTeamCharge.Location = new System.Drawing.Point(0, 48);
            this.ItemForTeamCharge.Name = "ItemForTeamCharge";
            this.ItemForTeamCharge.Size = new System.Drawing.Size(315, 24);
            this.ItemForTeamCharge.Text = "Team Charge:";
            this.ItemForTeamCharge.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 185);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(315, 15);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLabourVatRate
            // 
            this.ItemForLabourVatRate.Control = this.LabourVatRateSpinEdit;
            this.ItemForLabourVatRate.CustomizationFormText = "Labour VAT Rate:";
            this.ItemForLabourVatRate.Location = new System.Drawing.Point(0, 72);
            this.ItemForLabourVatRate.Name = "ItemForLabourVatRate";
            this.ItemForLabourVatRate.Size = new System.Drawing.Size(315, 24);
            this.ItemForLabourVatRate.Text = "Labour VAT Rate:";
            this.ItemForLabourVatRate.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForEveningRateModifier
            // 
            this.ItemForEveningRateModifier.Control = this.EveningRateModifierSpinEdit;
            this.ItemForEveningRateModifier.CustomizationFormText = "Evening Rate Modifier:";
            this.ItemForEveningRateModifier.Location = new System.Drawing.Point(0, 144);
            this.ItemForEveningRateModifier.Name = "ItemForEveningRateModifier";
            this.ItemForEveningRateModifier.Size = new System.Drawing.Size(315, 24);
            this.ItemForEveningRateModifier.Text = "Evening Rate Modifier:";
            this.ItemForEveningRateModifier.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Other";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForOtherCost,
            this.ItemForOtherSell,
            this.emptySpaceItem8});
            this.layoutControlGroup13.Location = new System.Drawing.Point(345, 142);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(358, 104);
            this.layoutControlGroup13.Text = "Other Cost Totals";
            // 
            // ItemForOtherCost
            // 
            this.ItemForOtherCost.Control = this.OtherCostSpinEdit;
            this.ItemForOtherCost.CustomizationFormText = "Other Cost:";
            this.ItemForOtherCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForOtherCost.Name = "ItemForOtherCost";
            this.ItemForOtherCost.Size = new System.Drawing.Size(334, 24);
            this.ItemForOtherCost.Text = "Other Cost:";
            this.ItemForOtherCost.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForOtherSell
            // 
            this.ItemForOtherSell.Control = this.OtherSellSpinEdit;
            this.ItemForOtherSell.CustomizationFormText = "Other Sell:";
            this.ItemForOtherSell.Location = new System.Drawing.Point(0, 24);
            this.ItemForOtherSell.Name = "ItemForOtherSell";
            this.ItemForOtherSell.Size = new System.Drawing.Size(334, 24);
            this.ItemForOtherSell.Text = "Other Sell:";
            this.ItemForOtherSell.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(334, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "Totals";
            this.layoutControlGroup14.ExpandButtonVisible = true;
            this.layoutControlGroup14.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTotalCost,
            this.ItemForTotalSell,
            this.emptySpaceItem7,
            this.ItemForNonStandardCost,
            this.ItemForNonStandardSell,
            this.emptySpaceItem14});
            this.layoutControlGroup14.Location = new System.Drawing.Point(709, 0);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(345, 246);
            this.layoutControlGroup14.Text = "Callout Totals";
            // 
            // ItemForTotalCost
            // 
            this.ItemForTotalCost.Control = this.TotalCostSpinEdit;
            this.ItemForTotalCost.CustomizationFormText = "Total Cost:";
            this.ItemForTotalCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForTotalCost.Name = "ItemForTotalCost";
            this.ItemForTotalCost.Size = new System.Drawing.Size(321, 24);
            this.ItemForTotalCost.Text = "Total Cost:";
            this.ItemForTotalCost.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForTotalSell
            // 
            this.ItemForTotalSell.Control = this.TotalSellSpinEdit;
            this.ItemForTotalSell.CustomizationFormText = "Total Sell:";
            this.ItemForTotalSell.Location = new System.Drawing.Point(0, 24);
            this.ItemForTotalSell.Name = "ItemForTotalSell";
            this.ItemForTotalSell.Size = new System.Drawing.Size(321, 24);
            this.ItemForTotalSell.Text = "Total Sell:";
            this.ItemForTotalSell.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 104);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 95);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 95);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(321, 96);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForNonStandardCost
            // 
            this.ItemForNonStandardCost.Control = this.NonStandardCostCheckEdit;
            this.ItemForNonStandardCost.CustomizationFormText = "Non-Standard Cost:";
            this.ItemForNonStandardCost.Location = new System.Drawing.Point(0, 58);
            this.ItemForNonStandardCost.Name = "ItemForNonStandardCost";
            this.ItemForNonStandardCost.Size = new System.Drawing.Size(321, 23);
            this.ItemForNonStandardCost.Text = "Non-Standard Cost:";
            this.ItemForNonStandardCost.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForNonStandardSell
            // 
            this.ItemForNonStandardSell.Control = this.NonStandardSellCheckEdit;
            this.ItemForNonStandardSell.CustomizationFormText = "Non-Standard Sell:";
            this.ItemForNonStandardSell.Location = new System.Drawing.Point(0, 81);
            this.ItemForNonStandardSell.Name = "ItemForNonStandardSell";
            this.ItemForNonStandardSell.Size = new System.Drawing.Size(321, 23);
            this.ItemForNonStandardSell.Text = "Non-Standard Sell:";
            this.ItemForNonStandardSell.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(321, 10);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.CustomizationFormText = "emptySpaceItem15";
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 246);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(1054, 28);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem3
            // 
            this.splitterItem3.AllowHotTrack = true;
            this.splitterItem3.CustomizationFormText = "splitterItem3";
            this.splitterItem3.Location = new System.Drawing.Point(703, 0);
            this.splitterItem3.Name = "splitterItem3";
            this.splitterItem3.Size = new System.Drawing.Size(6, 246);
            // 
            // layoutControlGroup21
            // 
            this.layoutControlGroup21.CustomizationFormText = "Salt";
            this.layoutControlGroup21.ExpandButtonVisible = true;
            this.layoutControlGroup21.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup21.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSaltUsed,
            this.ItemForSaltCost,
            this.ItemForSaltSell,
            this.ItemForSaltVatRate});
            this.layoutControlGroup21.Location = new System.Drawing.Point(345, 0);
            this.layoutControlGroup21.Name = "layoutControlGroup21";
            this.layoutControlGroup21.Size = new System.Drawing.Size(358, 142);
            this.layoutControlGroup21.Text = "Salt";
            // 
            // ItemForSaltUsed
            // 
            this.ItemForSaltUsed.Control = this.SaltUsedSpinEdit;
            this.ItemForSaltUsed.CustomizationFormText = "Salt Used:";
            this.ItemForSaltUsed.Location = new System.Drawing.Point(0, 0);
            this.ItemForSaltUsed.Name = "ItemForSaltUsed";
            this.ItemForSaltUsed.Size = new System.Drawing.Size(334, 24);
            this.ItemForSaltUsed.Text = "Salt Used:";
            this.ItemForSaltUsed.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSaltCost
            // 
            this.ItemForSaltCost.Control = this.SaltCostSpinEdit;
            this.ItemForSaltCost.CustomizationFormText = "Salt Cost Per 25Kg Bag:";
            this.ItemForSaltCost.Location = new System.Drawing.Point(0, 24);
            this.ItemForSaltCost.Name = "ItemForSaltCost";
            this.ItemForSaltCost.Size = new System.Drawing.Size(334, 24);
            this.ItemForSaltCost.Text = "Salt Cost Per 25Kg Bag:";
            this.ItemForSaltCost.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSaltSell
            // 
            this.ItemForSaltSell.Control = this.SaltSellSpinEdit;
            this.ItemForSaltSell.CustomizationFormText = "Salt Sell Per 25Kg Bag:";
            this.ItemForSaltSell.Location = new System.Drawing.Point(0, 48);
            this.ItemForSaltSell.Name = "ItemForSaltSell";
            this.ItemForSaltSell.Size = new System.Drawing.Size(334, 24);
            this.ItemForSaltSell.Text = "Salt Sell Per 25Kg Bag:";
            this.ItemForSaltSell.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSaltVatRate
            // 
            this.ItemForSaltVatRate.Control = this.SaltVatRateSpinEdit;
            this.ItemForSaltVatRate.CustomizationFormText = "Salt Vat Rate:";
            this.ItemForSaltVatRate.Location = new System.Drawing.Point(0, 72);
            this.ItemForSaltVatRate.Name = "ItemForSaltVatRate";
            this.ItemForSaltVatRate.Size = new System.Drawing.Size(334, 24);
            this.ItemForSaltVatRate.Text = "Salt VAT Rate:";
            this.ItemForSaltVatRate.TextSize = new System.Drawing.Size(136, 13);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(339, 0);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(6, 246);
            // 
            // layoutControlGroup18
            // 
            this.layoutControlGroup18.CustomizationFormText = "Payment";
            this.layoutControlGroup18.ExpandButtonVisible = true;
            this.layoutControlGroup18.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup8,
            this.layoutControlGroup20,
            this.splitterItem1,
            this.emptySpaceItem12});
            this.layoutControlGroup18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup18.Name = "layoutControlGroup18";
            this.layoutControlGroup18.Size = new System.Drawing.Size(1054, 274);
            this.layoutControlGroup18.Text = "Payment";
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Gritting Team";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSubContractorPaid,
            this.ItemForGrittingInvoiceID,
            this.ItemForDoNotPaySubContractor,
            this.ItemForDoNotPaySubContractorReason,
            this.emptySpaceItem16,
            this.ItemForPaidByStaffName,
            this.ItemForAuthorisedByStaffName,
            this.ItemForFinanceTeamPaymentExported});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(487, 264);
            this.layoutControlGroup8.Text = "Gritting Team";
            // 
            // ItemForSubContractorPaid
            // 
            this.ItemForSubContractorPaid.Control = this.SubContractorPaidCheckEdit;
            this.ItemForSubContractorPaid.CustomizationFormText = "Team Paid:";
            this.ItemForSubContractorPaid.Location = new System.Drawing.Point(0, 0);
            this.ItemForSubContractorPaid.Name = "ItemForSubContractorPaid";
            this.ItemForSubContractorPaid.Size = new System.Drawing.Size(463, 23);
            this.ItemForSubContractorPaid.Text = "Team Paid:";
            this.ItemForSubContractorPaid.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForGrittingInvoiceID
            // 
            this.ItemForGrittingInvoiceID.Control = this.GrittingInvoiceIDButtonEdit;
            this.ItemForGrittingInvoiceID.CustomizationFormText = "Gritting Invoice ID:";
            this.ItemForGrittingInvoiceID.Location = new System.Drawing.Point(0, 23);
            this.ItemForGrittingInvoiceID.Name = "ItemForGrittingInvoiceID";
            this.ItemForGrittingInvoiceID.Size = new System.Drawing.Size(463, 24);
            this.ItemForGrittingInvoiceID.Text = "Gritting Invoice ID:";
            this.ItemForGrittingInvoiceID.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForDoNotPaySubContractor
            // 
            this.ItemForDoNotPaySubContractor.Control = this.DoNotPaySubContractorCheckEdit;
            this.ItemForDoNotPaySubContractor.CustomizationFormText = "Don\'t Pay Team:";
            this.ItemForDoNotPaySubContractor.Location = new System.Drawing.Point(0, 95);
            this.ItemForDoNotPaySubContractor.Name = "ItemForDoNotPaySubContractor";
            this.ItemForDoNotPaySubContractor.Size = new System.Drawing.Size(463, 23);
            this.ItemForDoNotPaySubContractor.Text = "Don\'t Pay Team:";
            this.ItemForDoNotPaySubContractor.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForDoNotPaySubContractorReason
            // 
            this.ItemForDoNotPaySubContractorReason.Control = this.DoNotPaySubContractorReasonMemoEdit;
            this.ItemForDoNotPaySubContractorReason.CustomizationFormText = "Don\'t Pay Team Reason:n";
            this.ItemForDoNotPaySubContractorReason.Location = new System.Drawing.Point(0, 118);
            this.ItemForDoNotPaySubContractorReason.MaxSize = new System.Drawing.Size(0, 67);
            this.ItemForDoNotPaySubContractorReason.MinSize = new System.Drawing.Size(145, 67);
            this.ItemForDoNotPaySubContractorReason.Name = "ItemForDoNotPaySubContractorReason";
            this.ItemForDoNotPaySubContractorReason.Size = new System.Drawing.Size(463, 67);
            this.ItemForDoNotPaySubContractorReason.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDoNotPaySubContractorReason.Text = "Don\'t Pay Team Reason:";
            this.ItemForDoNotPaySubContractorReason.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.CustomizationFormText = "emptySpaceItem16";
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 208);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(463, 10);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPaidByStaffName
            // 
            this.ItemForPaidByStaffName.Control = this.PaidByStaffNameTextEdit;
            this.ItemForPaidByStaffName.CustomizationFormText = "Paid By Staff:";
            this.ItemForPaidByStaffName.Location = new System.Drawing.Point(0, 71);
            this.ItemForPaidByStaffName.Name = "ItemForPaidByStaffName";
            this.ItemForPaidByStaffName.Size = new System.Drawing.Size(463, 24);
            this.ItemForPaidByStaffName.Text = "Paid By Staff:";
            this.ItemForPaidByStaffName.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForAuthorisedByStaffName
            // 
            this.ItemForAuthorisedByStaffName.Control = this.AuthorisedByStaffNameTextEdit;
            this.ItemForAuthorisedByStaffName.CustomizationFormText = "Payment Authorised By:";
            this.ItemForAuthorisedByStaffName.Location = new System.Drawing.Point(0, 47);
            this.ItemForAuthorisedByStaffName.Name = "ItemForAuthorisedByStaffName";
            this.ItemForAuthorisedByStaffName.Size = new System.Drawing.Size(463, 24);
            this.ItemForAuthorisedByStaffName.Text = "Payment Authorised By:";
            this.ItemForAuthorisedByStaffName.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForFinanceTeamPaymentExported
            // 
            this.ItemForFinanceTeamPaymentExported.Control = this.FinanceTeamPaymentExportedCheckEdit;
            this.ItemForFinanceTeamPaymentExported.CustomizationFormText = "Team Payment Exported:";
            this.ItemForFinanceTeamPaymentExported.Location = new System.Drawing.Point(0, 185);
            this.ItemForFinanceTeamPaymentExported.Name = "ItemForFinanceTeamPaymentExported";
            this.ItemForFinanceTeamPaymentExported.Size = new System.Drawing.Size(463, 23);
            this.ItemForFinanceTeamPaymentExported.Text = "Team Payment Exported:";
            this.ItemForFinanceTeamPaymentExported.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroup20
            // 
            this.layoutControlGroup20.CustomizationFormText = "Client";
            this.layoutControlGroup20.ExpandButtonVisible = true;
            this.layoutControlGroup20.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup20.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientInvoiceNumber,
            this.ItemForDoNotInvoiceClient,
            this.emptySpaceItem11,
            this.ItemForDoNotInvoiceClientReason,
            this.ItemForCalloutInvoiced});
            this.layoutControlGroup20.Location = new System.Drawing.Point(493, 0);
            this.layoutControlGroup20.Name = "layoutControlGroup20";
            this.layoutControlGroup20.Size = new System.Drawing.Size(561, 264);
            this.layoutControlGroup20.Text = "Client";
            // 
            // ItemForClientInvoiceNumber
            // 
            this.ItemForClientInvoiceNumber.Control = this.ClientInvoiceNumberTextEdit;
            this.ItemForClientInvoiceNumber.CustomizationFormText = "Client Invoice Number:";
            this.ItemForClientInvoiceNumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientInvoiceNumber.Name = "ItemForClientInvoiceNumber";
            this.ItemForClientInvoiceNumber.Size = new System.Drawing.Size(537, 24);
            this.ItemForClientInvoiceNumber.Text = "Client Invoice Number:";
            this.ItemForClientInvoiceNumber.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForDoNotInvoiceClient
            // 
            this.ItemForDoNotInvoiceClient.Control = this.DoNotInvoiceClientCheckEdit;
            this.ItemForDoNotInvoiceClient.CustomizationFormText = "Don\'t Invoice Client:";
            this.ItemForDoNotInvoiceClient.Location = new System.Drawing.Point(0, 24);
            this.ItemForDoNotInvoiceClient.Name = "ItemForDoNotInvoiceClient";
            this.ItemForDoNotInvoiceClient.Size = new System.Drawing.Size(537, 23);
            this.ItemForDoNotInvoiceClient.Text = "Don\'t Invoice Client:";
            this.ItemForDoNotInvoiceClient.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 137);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(0, 47);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(10, 47);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(537, 81);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDoNotInvoiceClientReason
            // 
            this.ItemForDoNotInvoiceClientReason.Control = this.DoNotInvoiceClientReasonMemoEdit;
            this.ItemForDoNotInvoiceClientReason.CustomizationFormText = "Don\'t Invoice Client Reason:";
            this.ItemForDoNotInvoiceClientReason.Location = new System.Drawing.Point(0, 47);
            this.ItemForDoNotInvoiceClientReason.MaxSize = new System.Drawing.Size(0, 67);
            this.ItemForDoNotInvoiceClientReason.MinSize = new System.Drawing.Size(145, 67);
            this.ItemForDoNotInvoiceClientReason.Name = "ItemForDoNotInvoiceClientReason";
            this.ItemForDoNotInvoiceClientReason.Size = new System.Drawing.Size(537, 67);
            this.ItemForDoNotInvoiceClientReason.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDoNotInvoiceClientReason.Text = "Don\'t Invoice Client Reason:";
            this.ItemForDoNotInvoiceClientReason.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForCalloutInvoiced
            // 
            this.ItemForCalloutInvoiced.Control = this.CalloutInvoicedCheckEdit;
            this.ItemForCalloutInvoiced.CustomizationFormText = "Callout Invoiced:";
            this.ItemForCalloutInvoiced.Location = new System.Drawing.Point(0, 114);
            this.ItemForCalloutInvoiced.Name = "ItemForCalloutInvoiced";
            this.ItemForCalloutInvoiced.Size = new System.Drawing.Size(537, 23);
            this.ItemForCalloutInvoiced.Text = "Callout Invoiced:";
            this.ItemForCalloutInvoiced.TextSize = new System.Drawing.Size(136, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(487, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 264);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 264);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(1054, 10);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.CustomizationFormText = "Aborted Reason";
            this.layoutControlGroup16.ExpandButtonVisible = true;
            this.layoutControlGroup16.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAbortedReason});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Size = new System.Drawing.Size(1054, 274);
            this.layoutControlGroup16.Text = "Aborted Reason";
            // 
            // ItemForAbortedReason
            // 
            this.ItemForAbortedReason.Control = this.AbortedReasonMemoEdit;
            this.ItemForAbortedReason.CustomizationFormText = "Aborted Reason:";
            this.ItemForAbortedReason.Location = new System.Drawing.Point(0, 0);
            this.ItemForAbortedReason.Name = "ItemForAbortedReason";
            this.ItemForAbortedReason.Size = new System.Drawing.Size(1054, 274);
            this.ItemForAbortedReason.Text = "Aborted Reason:";
            this.ItemForAbortedReason.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForAbortedReason.TextVisible = false;
            // 
            // layoutControlGroup17
            // 
            this.layoutControlGroup17.CustomizationFormText = "Comments";
            this.layoutControlGroup17.ExpandButtonVisible = true;
            this.layoutControlGroup17.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForComments,
            this.ItemForInternalRemarks});
            this.layoutControlGroup17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup17.Name = "layoutControlGroup17";
            this.layoutControlGroup17.Size = new System.Drawing.Size(1054, 274);
            this.layoutControlGroup17.Text = "Comments";
            // 
            // ItemForComments
            // 
            this.ItemForComments.Control = this.CommentsMemoEdit;
            this.ItemForComments.CustomizationFormText = "Comments:";
            this.ItemForComments.Location = new System.Drawing.Point(0, 0);
            this.ItemForComments.Name = "ItemForComments";
            this.ItemForComments.Size = new System.Drawing.Size(1054, 121);
            this.ItemForComments.Text = "Comments:";
            this.ItemForComments.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForInternalRemarks
            // 
            this.ItemForInternalRemarks.Control = this.InternalRemarksMemoEdit;
            this.ItemForInternalRemarks.CustomizationFormText = "Internal Comments:";
            this.ItemForInternalRemarks.Location = new System.Drawing.Point(0, 121);
            this.ItemForInternalRemarks.Name = "ItemForInternalRemarks";
            this.ItemForInternalRemarks.Size = new System.Drawing.Size(1054, 153);
            this.ItemForInternalRemarks.Text = "Internal Comments:";
            this.ItemForInternalRemarks.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroup19
            // 
            this.layoutControlGroup19.CustomizationFormText = "Picture";
            this.layoutControlGroup19.ExpandButtonVisible = true;
            this.layoutControlGroup19.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem17});
            this.layoutControlGroup19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup19.Name = "layoutControlGroup19";
            this.layoutControlGroup19.Size = new System.Drawing.Size(1054, 274);
            this.layoutControlGroup19.Text = "Picture";
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.CustomizationFormText = "emptySpaceItem17";
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(1054, 274);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup22
            // 
            this.layoutControlGroup22.CustomizationFormText = "Site Address";
            this.layoutControlGroup22.ExpandButtonVisible = true;
            this.layoutControlGroup22.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup22.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteAddress1,
            this.ItemForSiteAddress2,
            this.ItemForSiteAddress3,
            this.ItemForSiteAddress4,
            this.ItemForSiteAddress5,
            this.ItemForSitePostcode,
            this.ItemForSiteLat,
            this.ItemForSiteLong,
            this.layoutControlItem3,
            this.emptySpaceItem24});
            this.layoutControlGroup22.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup22.Name = "layoutControlGroup22";
            this.layoutControlGroup22.Size = new System.Drawing.Size(1054, 274);
            this.layoutControlGroup22.Text = "Site Address";
            // 
            // ItemForSiteAddress1
            // 
            this.ItemForSiteAddress1.Control = this.SiteAddress1TextEdit;
            this.ItemForSiteAddress1.CustomizationFormText = "Address Line 1:";
            this.ItemForSiteAddress1.Location = new System.Drawing.Point(0, 26);
            this.ItemForSiteAddress1.Name = "ItemForSiteAddress1";
            this.ItemForSiteAddress1.Size = new System.Drawing.Size(1054, 24);
            this.ItemForSiteAddress1.Text = "Address Line 1:";
            this.ItemForSiteAddress1.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSiteAddress2
            // 
            this.ItemForSiteAddress2.Control = this.SiteAddress2TextEdit;
            this.ItemForSiteAddress2.CustomizationFormText = "Address Line 2:";
            this.ItemForSiteAddress2.Location = new System.Drawing.Point(0, 50);
            this.ItemForSiteAddress2.Name = "ItemForSiteAddress2";
            this.ItemForSiteAddress2.Size = new System.Drawing.Size(1054, 24);
            this.ItemForSiteAddress2.Text = "Address Line 2:";
            this.ItemForSiteAddress2.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSiteAddress3
            // 
            this.ItemForSiteAddress3.Control = this.SiteAddress3TextEdit;
            this.ItemForSiteAddress3.CustomizationFormText = "Address Line 3:";
            this.ItemForSiteAddress3.Location = new System.Drawing.Point(0, 74);
            this.ItemForSiteAddress3.Name = "ItemForSiteAddress3";
            this.ItemForSiteAddress3.Size = new System.Drawing.Size(1054, 24);
            this.ItemForSiteAddress3.Text = "Address Line 3:";
            this.ItemForSiteAddress3.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSiteAddress4
            // 
            this.ItemForSiteAddress4.Control = this.SiteAddress4TextEdit;
            this.ItemForSiteAddress4.CustomizationFormText = "Address Line 4:";
            this.ItemForSiteAddress4.Location = new System.Drawing.Point(0, 98);
            this.ItemForSiteAddress4.Name = "ItemForSiteAddress4";
            this.ItemForSiteAddress4.Size = new System.Drawing.Size(1054, 24);
            this.ItemForSiteAddress4.Text = "Address Line 4:";
            this.ItemForSiteAddress4.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSiteAddress5
            // 
            this.ItemForSiteAddress5.Control = this.SiteAddress5TextEdit;
            this.ItemForSiteAddress5.CustomizationFormText = "Address Line 5:";
            this.ItemForSiteAddress5.Location = new System.Drawing.Point(0, 122);
            this.ItemForSiteAddress5.Name = "ItemForSiteAddress5";
            this.ItemForSiteAddress5.Size = new System.Drawing.Size(1054, 24);
            this.ItemForSiteAddress5.Text = "Address Line 5:";
            this.ItemForSiteAddress5.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSitePostcode
            // 
            this.ItemForSitePostcode.Control = this.SitePostcodeButtonEdit;
            this.ItemForSitePostcode.CustomizationFormText = "Postcode:";
            this.ItemForSitePostcode.Location = new System.Drawing.Point(0, 146);
            this.ItemForSitePostcode.Name = "ItemForSitePostcode";
            this.ItemForSitePostcode.Size = new System.Drawing.Size(1054, 24);
            this.ItemForSitePostcode.Text = "Postcode:";
            this.ItemForSitePostcode.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSiteLat
            // 
            this.ItemForSiteLat.Control = this.SiteLatSpinEdit;
            this.ItemForSiteLat.CustomizationFormText = "Latitude:";
            this.ItemForSiteLat.Location = new System.Drawing.Point(0, 170);
            this.ItemForSiteLat.Name = "ItemForSiteLat";
            this.ItemForSiteLat.Size = new System.Drawing.Size(521, 104);
            this.ItemForSiteLat.Text = "Latitude:";
            this.ItemForSiteLat.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSiteLong
            // 
            this.ItemForSiteLong.Control = this.SiteLongSpinEdit;
            this.ItemForSiteLong.CustomizationFormText = "Longitude:";
            this.ItemForSiteLong.Location = new System.Drawing.Point(521, 170);
            this.ItemForSiteLong.Name = "ItemForSiteLong";
            this.ItemForSiteLong.Size = new System.Drawing.Size(533, 104);
            this.ItemForSiteLong.Text = "Longitude:";
            this.ItemForSiteLong.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnViewOnMap;
            this.layoutControlItem3.CustomizationFormText = "View On Map Button";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(143, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(143, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(143, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "View On Map Button";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.CustomizationFormText = "emptySpaceItem24";
            this.emptySpaceItem24.Location = new System.Drawing.Point(143, 0);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(911, 26);
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Other";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSnowInfo,
            this.ItemForTeamMembersPresent});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(1054, 274);
            this.layoutControlGroup10.Text = "Other";
            // 
            // ItemForSnowInfo
            // 
            this.ItemForSnowInfo.Control = this.SnowInfoMemoEdit;
            this.ItemForSnowInfo.CustomizationFormText = "Snow Info:";
            this.ItemForSnowInfo.Location = new System.Drawing.Point(0, 0);
            this.ItemForSnowInfo.Name = "ItemForSnowInfo";
            this.ItemForSnowInfo.Size = new System.Drawing.Size(1054, 128);
            this.ItemForSnowInfo.Text = "Snow Info:";
            this.ItemForSnowInfo.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForTeamMembersPresent
            // 
            this.ItemForTeamMembersPresent.Control = this.TeamMembersPresentMemoEdit;
            this.ItemForTeamMembersPresent.CustomizationFormText = "Team Members Present:";
            this.ItemForTeamMembersPresent.Location = new System.Drawing.Point(0, 128);
            this.ItemForTeamMembersPresent.Name = "ItemForTeamMembersPresent";
            this.ItemForTeamMembersPresent.Size = new System.Drawing.Size(1054, 146);
            this.ItemForTeamMembersPresent.Text = "Team Members Present:";
            this.ItemForTeamMembersPresent.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.AllowDrawBackground = false;
            this.layoutControlGroup9.CustomizationFormText = "autoGeneratedGroup3";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.GroupBordersVisible = false;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 863);
            this.layoutControlGroup9.Name = "autoGeneratedGroup3";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1102, 10);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1102, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp04076_GC_Gritting_Callout_EditTableAdapter
            // 
            this.sp04076_GC_Gritting_Callout_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter
            // 
            this.sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Callout_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1139, 690);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Callout_Edit";
            this.Text = "Edit Gritting Callout";
            this.Activated += new System.EventHandler(this.frm_GC_Callout_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Callout_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Callout_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DoubleGritCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04076GCGrittingCalloutEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletionEmailSentCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttendanceOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFloatingSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLongSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearedSatisfactoryCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternalRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather5CheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather2CheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather4CheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather3CheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather1CheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDACodeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowOnSiteOver5cmCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteManagerNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalloutInvoicedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceTeamPaymentExportedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSheetNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddress1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamMembersPresentMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowInfoMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoAccessAbortedRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PPEWornCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardSellCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AuthorisedByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardCostCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowOnSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPriceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteGrittingContractIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingCallOutIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalSubContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallOutDateTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallOutDateTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportedWeatherForecastIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSentTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSentTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorReceivedTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorReceivedTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorRespondedTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorRespondedTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETATextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AuthorisedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitAbortedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishedLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishedLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltUsedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursWorkedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamHourlyRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamChargeSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorPaidCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedByNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritSourceLocationIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritSourceLocationTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteWeatherTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPaySubContractorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPaySubContractorReasonMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientReasonMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteTemperatureSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PdaIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamPresentCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoAccessCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceFailureCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalSubContractorNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingInvoiceIDButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbortedReasonMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EveningRateModifierSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingCallOutID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOriginalSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForImportedWeatherForecastID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAuthorisedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteGrittingContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSourceLocationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSourceLocationTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteWeather)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPdaID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOriginalSubContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceFailure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOIDDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDACode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedByName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeather5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteTemperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowOnSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowOnSiteOver5cm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowCleared)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearedSatisfactory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoAccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitAborted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoAccessAbortedRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSheetNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteManagerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsFloatingSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAttendanceOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoubleGrit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCallOutDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTextSentTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletedTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorReceivedTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorRespondedTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorETA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamPresent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPPEWorn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishedLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishedLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletionEmailSent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursWorked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamHourlyRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEveningRateModifier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingInvoiceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPaySubContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPaySubContractorReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAuthorisedByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceTeamPaymentExported)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInvoiceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClientReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalloutInvoiced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbortedReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInternalRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMembersPresent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteGrittingContractID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.TextEdit SiteGrittingContractIDGridLookUpEdit;
        private System.Windows.Forms.BindingSource sp04076GCGrittingCalloutEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04076_GC_Gritting_Callout_EditTableAdapter sp04076_GC_Gritting_Callout_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit GrittingCallOutIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraEditors.ButtonEdit SiteNameButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit SubContractorNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit OriginalSubContractorIDTextEdit;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraEditors.DateEdit CallOutDateTimeDateEdit;
        private DevExpress.XtraEditors.TextEdit ImportedWeatherForecastIDTextEdit;
        private DevExpress.XtraEditors.DateEdit TextSentTimeDateEdit;
        private DevExpress.XtraEditors.DateEdit SubContractorReceivedTimeDateEdit;
        private DevExpress.XtraEditors.DateEdit SubContractorRespondedTimeDateEdit;
        private DevExpress.XtraEditors.TextEdit SubContractorETATextEdit;
        private DevExpress.XtraEditors.DateEdit CompletedTimeDateEdit;
        private DevExpress.XtraEditors.TextEdit AuthorisedByStaffIDTextEdit;
        private DevExpress.XtraEditors.CheckEdit VisitAbortedCheckEdit;
        private DevExpress.XtraEditors.TextEdit StartLatitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit StartLongitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit FinishedLatitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit FinishedLongitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientPONumberTextEdit;
        private DevExpress.XtraEditors.SpinEdit SaltUsedSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SaltCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SaltSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SaltVatRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit HoursWorkedSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TeamHourlyRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TeamChargeSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LabourCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LabourVatRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit OtherCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit OtherSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TotalCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TotalSellSpinEdit;
        private DevExpress.XtraEditors.TextEdit ClientInvoiceNumberTextEdit;
        private DevExpress.XtraEditors.CheckEdit SubContractorPaidCheckEdit;
        private DevExpress.XtraEditors.TextEdit RecordedByStaffIDTextEdit;
        private DevExpress.XtraEditors.TextEdit RecordedByNameTextEdit;
        private DevExpress.XtraEditors.TextEdit PaidByStaffIDTextEdit;
        private DevExpress.XtraEditors.CheckEdit DoNotPaySubContractorCheckEdit;
        private DevExpress.XtraEditors.MemoEdit DoNotPaySubContractorReasonMemoEdit;
        private DevExpress.XtraEditors.CheckEdit DoNotInvoiceClientCheckEdit;
        private DevExpress.XtraEditors.MemoEdit DoNotInvoiceClientReasonMemoEdit;
        private DevExpress.XtraEditors.TextEdit GritSourceLocationIDTextEdit;
        private DevExpress.XtraEditors.TextEdit GritSourceLocationTypeIDTextEdit;
        private DevExpress.XtraEditors.CheckEdit NoAccessCheckEdit;
        private DevExpress.XtraEditors.TextEdit SiteWeatherTextEdit;
        private DevExpress.XtraEditors.SpinEdit SiteTemperatureSpinEdit;
        private DevExpress.XtraEditors.TextEdit PdaIDTextEdit;
        private DevExpress.XtraEditors.CheckEdit TeamPresentCheckEdit;
        private DevExpress.XtraEditors.CheckEdit ServiceFailureCheckEdit;
        private DevExpress.XtraEditors.MemoEdit CommentsMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit OriginalSubContractorNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingCallOutID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOriginalSubContractorID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCallOutDateTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForImportedWeatherForecastID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTextSentTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorReceivedTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorRespondedTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorETA;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompletedTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAuthorisedByStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitAborted;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbortedReason;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLatitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLongitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishedLatitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishedLongitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHoursWorked;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamHourlyRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamCharge;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourVatRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOtherCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOtherSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientInvoiceNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorPaid;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingInvoiceID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRecordedByStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRecordedByName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPaidByStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotPaySubContractor;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotPaySubContractorReason;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotInvoiceClient;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotInvoiceClientReason;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritSourceLocationID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritSourceLocationTypeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoAccess;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteWeather;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteTemperature;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPdaID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamPresent;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceFailure;
        private DevExpress.XtraLayout.LayoutControlItem ItemForComments;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOriginalSubContractorName;
        private DevExpress.XtraEditors.GridLookUpEdit JobStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.SimpleButton btnShowOnMap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ButtonEdit GrittingInvoiceIDButtonEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSaltUsed;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSaltCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSaltSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSaltVatRate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.MemoEdit AbortedReasonMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup18;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup19;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.SplitterItem splitterItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private System.Windows.Forms.BindingSource sp04085GCGrittingCalloutStatusListWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusOrder;
        private DataSet_GC_DataEntryTableAdapters.sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.SpinEdit ClientPriceSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPrice;
        private DevExpress.XtraEditors.TextEdit PaidByStaffNameTextEdit;
        private DevExpress.XtraEditors.CheckEdit SnowOnSiteCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowOnSite;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPaidByStaffName;
        private DevExpress.XtraEditors.TextEdit AuthorisedByStaffNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAuthorisedByStaffName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.DateEdit StartTimeDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartTime;
        private DevExpress.XtraEditors.ButtonEdit ClientPOIDDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOIDDescription;
        private DevExpress.XtraEditors.TextEdit ClientPOIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOID;
        private DevExpress.XtraEditors.CheckEdit NonStandardSellCheckEdit;
        private DevExpress.XtraEditors.CheckEdit NonStandardCostCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonStandardCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonStandardSell;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraEditors.CheckEdit PPEWornCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPPEWorn;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraEditors.SpinEdit NoAccessAbortedRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoAccessAbortedRate;
        private DevExpress.XtraEditors.MemoEdit SnowInfoMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowInfo;
        private DevExpress.XtraEditors.MemoEdit TeamMembersPresentMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamMembersPresent;
        private DevExpress.XtraEditors.TextEdit SiteAddress1TextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup22;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress1;
        private DevExpress.XtraEditors.TextEdit SiteAddress5TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddress4TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddress3TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddress2TextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSitePostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEveningRateModifier;
        private DevExpress.XtraEditors.SpinEdit EveningRateModifierSpinEdit;
        private DevExpress.XtraEditors.TextEdit JobSheetNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSheetNumber;
        private DevExpress.XtraEditors.CheckEdit FinanceTeamPaymentExportedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinanceTeamPaymentExported;
        private DevExpress.XtraEditors.TextEdit SiteManagerNameTextEdit;
        private DevExpress.XtraEditors.CheckEdit CalloutInvoicedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteManagerName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCalloutInvoiced;
        private DevExpress.XtraEditors.CheckEdit SnowOnSiteOver5cmCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowOnSiteOver5cm;
        private DevExpress.XtraEditors.ButtonEdit PDACodeButtonEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDACode;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraEditors.CheckEdit Weather5CheckEdit;
        private DevExpress.XtraEditors.CheckEdit Weather2CheckEdit;
        private DevExpress.XtraEditors.CheckEdit Weather4CheckEdit;
        private DevExpress.XtraEditors.CheckEdit Weather3CheckEdit;
        private DevExpress.XtraEditors.CheckEdit Weather1CheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWeather1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWeather2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWeather3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWeather4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWeather5;
        private DevExpress.XtraEditors.MemoEdit InternalRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInternalRemarks;
        private DevExpress.XtraEditors.CheckEdit SnowClearedSatisfactoryCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SnowClearedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowCleared;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearedSatisfactory;
        private DevExpress.XtraEditors.SpinEdit SiteLongSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SiteLatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteLat;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteLong;
        private DevExpress.XtraEditors.CheckEdit IsFloatingSiteCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsFloatingSite;
        private DevExpress.XtraEditors.SpinEdit AttendanceOrderSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAttendanceOrder;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraEditors.CheckEdit CompletionEmailSentCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompletionEmailSent;
        private DevExpress.XtraEditors.ButtonEdit SitePostcodeButtonEdit;
        private DevExpress.XtraEditors.SimpleButton btnViewOnMap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit DoubleGritCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoubleGrit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem25;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem26;
    }
}

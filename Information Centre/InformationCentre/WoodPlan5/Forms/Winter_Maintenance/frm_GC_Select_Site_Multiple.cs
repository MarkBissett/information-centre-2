using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;


namespace WoodPlan5
{
    public partial class frm_GC_Select_Site_Multiple : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;
        bool isRunning = false;

        public string strSelectedValues = "";
        public string strSelectedIDs = "";
        public int intSelectedCount = 0;
        public int intOriginalClientID = 0;

        BaseObjects.GridCheckMarksSelection selection1;

        public int intFilterActiveClient = 0;
        public int intFilterUtilityArbClient = 0;
        public int intFilterSummerMaintenanceClient = 0;
        public int intWinterMaintenanceClient = 0;
        public int intAmenityArbClient = 0;

        #endregion

        public frm_GC_Select_Site_Multiple()
        {
            InitializeComponent();
        }

        private void frm_GC_Select_Site_Multiple_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400026;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp03055_EP_Select_Sites_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl1.ForceInitialize();
            gridControl2.ForceInitialize();

            try
            {
                sp03054_EP_Select_Client_ListTableAdapter.Connection.ConnectionString = strConnectionString;
                sp03054_EP_Select_Client_ListTableAdapter.Fill(dataSet_EP.sp03054_EP_Select_Client_List, intFilterActiveClient, intFilterUtilityArbClient, intFilterSummerMaintenanceClient, intWinterMaintenanceClient, intAmenityArbClient);
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the client list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //return;
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            if (intOriginalClientID != 0)  // Client selected so try to find and highlight //
            {
                GridView view = (GridView)gridControl1.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["ClientID"], intOriginalClientID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.BeginSelection();
                    view.ClearSelection();
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                    view.SelectRow(intFoundRow);
                    view.EndSelection();
                }
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
       }

        public override void PostLoadView(object objParameter)
        {
        }


        bool internalRowFocusing;

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Sites Available For Selection - Select a Client to see Related Sites";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                 //pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            LoadLinkedData1();
            GridView view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
         }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ClientID"])) + ',';
            }

            //Populate Linked Records //
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_EP.sp03055_EP_Select_Sites_List.Clear();
            }
            else
            {
                try
                {
                    sp03055_EP_Select_Sites_ListTableAdapter.Fill(dataSet_EP.sp03055_EP_Select_Sites_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related sites.\n\nTry selecting a client again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedIDs == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            strSelectedValues = "";
            strSelectedIDs = "";
            intSelectedCount = 0;
            GridView view = (GridView)gridControl2.MainView;

            if (view.DataRowCount <= 0) return;
            if (selection1.SelectedCount <= 0) return;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "SiteID")) + ',';
                    strSelectedValues += Convert.ToString(view.GetRowCellValue(i, "SiteName")) + ',';
                    intSelectedCount++;
                }
            }
        }



    }
}


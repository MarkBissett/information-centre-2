using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_GC_Site_Gritting_contract_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public int intRecordTypeID = 0;
        
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Rate Calculation //
        private bool ibool_FormStillLoading = true;
      
        #endregion

        public frm_GC_Site_Gritting_contract_Edit()
        {
            InitializeComponent();
        }

        private void frm_GC_Site_Gritting_contract_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 40009;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00226_Staff_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00226_Staff_List_With_BlankTableAdapter.Fill(dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //


            sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_SitesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_SitesTableAdapter.Fill(dataSet_GC_DataEntry.sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_Sites, "");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04051_GC_Gritting_Forecast_Type_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04051_GC_Gritting_Forecast_Type_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04051_GC_Gritting_Forecast_Type_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04052_GC_Gritting_Activation_Codes_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04052_GC_Gritting_Activation_Codes_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04052_GC_Gritting_Activation_Codes_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04343_GC_Gritting_Forecast_BandingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04343_GC_Gritting_Forecast_BandingsTableAdapter.Fill(dataSet_GC_Core.sp04343_GC_Gritting_Forecast_Bandings);

            sp04373_WM_Season_Periods_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04373_WM_Season_Periods_With_BlankTableAdapter.Fill(dataSet_GC_Core.sp04373_WM_Season_Periods_With_Blank, 1);

            // Populate Main Dataset //
            sp04038_GC_Site_Gritting_Contract_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["SiteGrittingContractID"] = 0;
                        drNewRow["SiteID"] = intLinkedToRecordID;
                        drNewRow["ForecastingTypeID"] = 0;
                        drNewRow["GrittingActivationCodeID"] = 0;
                        drNewRow["MinimumTemperature"] = 0;
                        drNewRow["StartDate"] = DateTime.Today;
                        drNewRow["EndDate"] = DateTime.Today.AddYears(1);
                        drNewRow["Area"] = 0.00;
                        drNewRow["Proactive"] = 0;
                        drNewRow["Reactive"] = 0;
                        drNewRow["ClientProactivePrice"] = 0.00;
                        drNewRow["ClientReactivePrice"] = 0.00;
                        drNewRow["ClientChargedForSalt"] = 0;
                        drNewRow["ClientSaltPrice"] = 0.00;
                        drNewRow["GritOnMonday"] = 0;
                        drNewRow["GritOnTuesday"] = 0;
                        drNewRow["GritOnWednesday"] = 0;
                        drNewRow["GritOnThursday"] = 0;
                        drNewRow["GritOnFriday"] = 0;
                        drNewRow["GritOnSaturday"] = 0;
                        drNewRow["GritOnSunday"] = 0;
                        drNewRow["ClientEveningRateModifier"] = (decimal)1.00;
                        drNewRow["PrioritySite"] = 0;
                        drNewRow["ClientSaltVatRate"] = (decimal)20.00;
                        drNewRow["RedOverridesBanding"] = 1;
                        drNewRow["BandingStart"] = 1;
                        drNewRow["BandingEnd"] = 24;
                        drNewRow["IsFloatingSite"] = 0;
                        drNewRow["GrittingCompletionEmail"] = 0;
                        drNewRow["GrittingTimeOnSite"] = 0.00;
                        drNewRow["SiteOpenTime1"] = new TimeSpan(00, 00, 00);
                        drNewRow["SiteClosedTime1"] = new TimeSpan(00, 00, 00);
                        drNewRow["SiteOpenTime2"] = new TimeSpan(00, 00, 00);
                        drNewRow["SiteClosedTime2"] = new TimeSpan(00, 00, 00);
                        drNewRow["GrittingCompletionEmailLinkedPictures"] = 0;
                        drNewRow["AnnualCost"] = (decimal)0.00;
                        drNewRow["AccessRestrictions"] = 0;
                        drNewRow["Annual_Contract"] = 0;
                        drNewRow["InvoiceClient"] = 1;
                        drNewRow["MinimumPictureCount"] = 2;
                        drNewRow["SeasonPeriodID"] = 0;
                        this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.NewRow();
                        drNewRow["strMode"] = "blockadd";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SiteGrittingContractID"] = 0;
                        drNewRow["ForecastingTypeID"] = 0;
                        drNewRow["GrittingActivationCodeID"] = 0;
                        drNewRow["MinimumTemperature"] = 0;
                        drNewRow["StartDate"] = DateTime.Today;
                        drNewRow["EndDate"] = DateTime.Today.AddYears(1);
                        drNewRow["Area"] = 0.00;
                        drNewRow["Proactive"] = 0;
                        drNewRow["Reactive"] = 0;
                        drNewRow["ClientProactivePrice"] = 0.00;
                        drNewRow["ClientReactivePrice"] = 0.00;
                        drNewRow["ClientChargedForSalt"] = 0;
                        drNewRow["ClientSaltPrice"] = 0.00;
                        drNewRow["GritOnMonday"] = 0;
                        drNewRow["GritOnTuesday"] = 0;
                        drNewRow["GritOnWednesday"] = 0;
                        drNewRow["GritOnThursday"] = 0;
                        drNewRow["GritOnFriday"] = 0;
                        drNewRow["GritOnSaturday"] = 0;
                        drNewRow["GritOnSunday"] = 0;
                        drNewRow["ClientEveningRateModifier"] = (decimal)1.00;
                        drNewRow["PrioritySite"] = 0;
                        drNewRow["ClientSaltVatRate"] = (decimal)20.00;
                        drNewRow["RedOverridesBanding"] = 1;
                        drNewRow["BandingStart"] = 1;
                        drNewRow["BandingEnd"] = 24;
                        drNewRow["IsFloatingSite"] = 0;
                        drNewRow["GrittingCompletionEmail"] = 0;
                        drNewRow["GrittingTimeOnSite"] = 0.00;
                        drNewRow["SiteOpenTime1"] = new TimeSpan(00, 00, 00);
                        drNewRow["SiteClosedTime1"] = new TimeSpan(00,00,00);
                        drNewRow["SiteOpenTime2"] = new TimeSpan(00, 00, 00);
                        drNewRow["SiteClosedTime2"] = new TimeSpan(00, 00, 00);
                        drNewRow["GrittingCompletionEmailLinkedPictures"] = 0;
                        drNewRow["AnnualCost"] = (decimal)0.00;
                        drNewRow["AccessRestrictions"] = 0;
                        drNewRow["Annual_Contract"] = 0;
                        drNewRow["InvoiceClient"] = 1;
                        drNewRow["SeasonPeriodID"] = 0;
                        this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.Rows.Add(drNewRow);
                        this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp04038_GC_Site_Gritting_Contract_EditTableAdapter.Fill(this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Linked Site Gritting Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            ibool_FormStillLoading = true;  // Prevent Rate Check firing on HoursWorkedSpinEdit_Validated, ReactiveCheckEdit_Validated, StartTimeDateEdit_Validated, EndTimeDateEdit_Validated event //
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        GrittingActivationCodeIDGridLookUpEdit.Focus();

                        SiteIDGridLookUpEdit.Properties.ReadOnly = false;
                        SiteIDGridLookUpEdit.Properties.Buttons[0].Enabled = true;
                        SiteIDGridLookUpEdit.Properties.Buttons[1].Enabled = true;
                        SiteIDGridLookUpEdit.Properties.Buttons[2].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        GrittingActivationCodeIDGridLookUpEdit.Focus();

                        SiteIDGridLookUpEdit.Properties.ReadOnly = true;
                        SiteIDGridLookUpEdit.Properties.Buttons[0].Enabled = false;
                        SiteIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
                        SiteIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        GrittingActivationCodeIDGridLookUpEdit.Focus();

                        SiteIDGridLookUpEdit.Properties.ReadOnly = false;
                        SiteIDGridLookUpEdit.Properties.Buttons[0].Enabled = true;
                        SiteIDGridLookUpEdit.Properties.Buttons[1].Enabled = true;
                        SiteIDGridLookUpEdit.Properties.Buttons[2].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        GrittingActivationCodeIDGridLookUpEdit.Focus();

                        SiteIDGridLookUpEdit.Properties.ReadOnly = true;
                        SiteIDGridLookUpEdit.Properties.Buttons[0].Enabled = false;
                        SiteIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
                        SiteIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            Set_Control_Readonly_Status("");
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_GC_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_GC_Site_Gritting_contract_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_GC_Site_Gritting_contract_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            GrittingActivationCodeIDGridLookUpEdit.Focus();  // Change Focus from current control so Validation code fired if ForecastingTypeIDGridLookUpEdit control was active prior to saving as next line willprevent this //
            ibool_ignoreValidation = true; 
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp04038GCSiteGrittingContractEditBindingSource.EndEdit();
            try
            {
                this.sp04038_GC_Site_Gritting_Contract_EditTableAdapter.Update(dataSet_GC_DataEntry);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp04038GCSiteGrittingContractEditBindingSource.Current;
                if (currentRow != null) 
                {
                    strNewIDs = Convert.ToInt32(currentRow["SiteGrittingContractID"]) + ";";
                    // Switch mode to Edit so than any subsequent changes update this record //
                    this.strFormMode = "edit";
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp04038GCSiteGrittingContractEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
                    currentRow["strMode"] = "blockedit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of Parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                switch (strCaller)
                {
                    case "frm_GC_Gritting_Contract_Manager":
                        {
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_Gritting_Contract_Manager")
                                {
                                    frm_GC_Gritting_Contract_Manager fParentForm = (frm_GC_Gritting_Contract_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "");
                                }
                            }
                        }
                        break;
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_GC_DataEntry.sp04038_GC_Site_Gritting_Contract_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Set_Control_Readonly_Status("");
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void SiteIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(SiteIDGridLookUpEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SiteIDGridLookUpEdit, "");
            }
        }

        private void SiteIDGridLookUpEdit_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 3002, "Sites");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_SitesTableAdapter.Fill(dataSet_GC_DataEntry.sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_Sites, "");
                }
            }
        }

        private void GrittingActivationCodeIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            string strActivationCodeID = ForecastingTypeIDGridLookUpEdit.EditValue.ToString();
            if (string.IsNullOrEmpty(strActivationCodeID) || strActivationCodeID == "0")
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                GridView view = glue.Properties.View;
                view.BeginUpdate();
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[ForecastingTypeID] = 0";
                view.MakeRowVisible(-1, true);
                view.EndUpdate();
                return;
            }
            else
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                GridView view = glue.Properties.View;
                view.BeginUpdate();
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[ForecastingTypeID] = " + strActivationCodeID + " or [ForecastingTypeID] = 0";
                view.MakeRowVisible(-1, true);
                view.EndUpdate();
            }
        }

        private void GrittingActivationCodeIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void ProactiveCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("Proactive");
        }

        private void ReactiveCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("Reactive");
        }

        private void ClientChargedForSaltCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("ClientChargedForSalt");
        }

        private void ForecastingTypeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            GrittingActivationCodeIDGridLookUpEdit.EditValue = 0;   // Clear Existing Value //
        }

        private void ForecastingTypeIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("ForecastingTypeID");
        }

        private void IsFloatingSiteCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }

        private void IsFloatingSiteCheckEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                ForecastingTypeIDGridLookUpEdit.EditValue = 3;  // Forecast CSV - Using Temperature //
                GrittingActivationCodeIDGridLookUpEdit.EditValue = 11;  // Dry //
                MinimumTemperatureSpinEdit.EditValue = (decimal)-0.5;
                BandingStartGridLookUpEdit.EditValue = 13;  // 00:00 Time //
                BandingEndGridLookUpEdit.EditValue = 13;  // 00:00 Time //
            }
            Set_Control_Readonly_Status("ForecastingTypeID");
        }

        private void Set_Control_Readonly_Status(string strCheckWhat)
        {
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "Proactive")
            {
                ClientProactivePriceSpinEdit.Properties.ReadOnly = !ProactiveCheckEdit.Checked;
            }
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "Reactive")
            {
                ClientReactivePriceSpinEdit.Properties.ReadOnly = !ReactiveCheckEdit.Checked;
            }
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "ClientChargedForSalt")
            {
                ClientSaltPriceSpinEdit.Properties.ReadOnly = !ClientChargedForSaltCheckEdit.Checked;
                ClientSaltVatRateSpinEdit.Properties.ReadOnly = !ClientChargedForSaltCheckEdit.Checked;
            }
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "ForecastingTypeID")
            {
                MinimumTemperatureSpinEdit.Properties.ReadOnly = (ForecastingTypeIDGridLookUpEdit.EditValue.ToString() == "3" ? false : true);
                BandingStartGridLookUpEdit.Properties.ReadOnly = (ForecastingTypeIDGridLookUpEdit.EditValue.ToString() == "3" ? false : true);
                BandingEndGridLookUpEdit.Properties.ReadOnly = (ForecastingTypeIDGridLookUpEdit.EditValue.ToString() == "3" ? false : true);
                RedOverridesBandingCheckEdit.Properties.ReadOnly = (ForecastingTypeIDGridLookUpEdit.EditValue.ToString() == "3" ? false : true);
                if (IsFloatingSiteCheckEdit.Checked)
                {
                    ForecastingTypeIDGridLookUpEdit.Properties.ReadOnly = true;
                    BandingStartGridLookUpEdit.Properties.ReadOnly = true;
                    BandingEndGridLookUpEdit.Properties.ReadOnly = true;
                }
                else
                {
                    ForecastingTypeIDGridLookUpEdit.Properties.ReadOnly = false;
                }
            }

        }

        private void SeasonPeriodIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(SeasonPeriodIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SeasonPeriodIDGridLookUpEdit, "");
            }
        }

        #endregion

  
  

   
    }
}


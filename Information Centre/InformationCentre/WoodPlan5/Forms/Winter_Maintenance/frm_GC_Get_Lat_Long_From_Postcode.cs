﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Get_Lat_Long_From_Postcode : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";  
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;

        public string strPassedInPostcode = "";
        public string strSelectedPostcode = "";
        public double dblSelectedLatitude = (double)0.00;
        public double dblSelectedLongitude = (double)0.00;
        public int intSelectedEasting = 0;
        public int intSelectedNorthing = 0;
        public string strSelectedGridRef = "";

        #endregion

        public frm_GC_Get_Lat_Long_From_Postcode()
        {
            InitializeComponent();
        }

        private void frm_GC_Get_Lat_Long_From_Postcode_Load(object sender, EventArgs e)
        {
            this.FormID = 400150;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            textEditPostcode.EditValue = strPassedInPostcode;
            if (!string.IsNullOrEmpty(textEditPostcode.EditValue.ToString())) LoadData();
            
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {

        }

        private void LoadData()
        {
            if (string.IsNullOrEmpty(textEditPostcode.EditValue.ToString()))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Please enter a post code or a partial postcode surrounded by '%' before proceeding.", "Postcode Search", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            try
            {
                sp04353_GC_Find_PostcodesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp04353_GC_Find_PostcodesTableAdapter.Fill(dataSet_GC_Core.sp04353_GC_Find_Postcodes, textEditPostcode.EditValue.ToString());
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while searching for matching postcodes.\n\nPlease try again. If the problem persists, contact Technical Support.", "Postcode Search", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Matches Found - try adjusting the postcode or performing a partial postcode search");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        #endregion


        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedPostcode == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedPostcode = gridView1.GetRowCellValue(view.FocusedRowHandle, "Postcode").ToString();
                dblSelectedLatitude = Convert.ToDouble(gridView1.GetRowCellValue(view.FocusedRowHandle, "Latitude"));
                dblSelectedLongitude = Convert.ToDouble(gridView1.GetRowCellValue(view.FocusedRowHandle, "Longitude"));
                intSelectedEasting = Convert.ToInt32(gridView1.GetRowCellValue(view.FocusedRowHandle, "Easting"));
                intSelectedNorthing = Convert.ToInt32(gridView1.GetRowCellValue(view.FocusedRowHandle, "Northing"));
                strSelectedGridRef = gridView1.GetRowCellValue(view.FocusedRowHandle, "GridRef").ToString();
            }
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;  // Required for uploading file to optimiser //
using System.Collections.Specialized;  // Required for uploading file to optimiser //
using System.IO;  // Required for uploading file to optimiser //
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Security.Cryptography;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5

{
    public partial class frm_GC_Gritting_Callouts_Optimizer_Results : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public bool boolRefreshCallingScreen = false;

        #endregion
        
        public frm_GC_Gritting_Callouts_Optimizer_Results()
        {
            InitializeComponent();
        }

        private void frm_GC_Gritting_Callouts_Optimizer_Results_Load(object sender, EventArgs e)
        {
            this.FormID = 400152;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp04362_GC_Route_Optimization_ResultsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04363_GC_Route_Optimization_ErrorsTableAdapter.Connection.ConnectionString = strConnectionString;
            
            LoadData();

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (splashScreenManager != null)
            {
                splashScreenManager.CloseWaitForm();
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {

        }

        private void LoadData()
        {
            #region GetConfigurationSettings

            string strGrittingRouteOptimisationURL = "";
            string strGrittingRouteOptimisationUsername = "";
            string strGrittingRouteOptimisationPassword = "";
            string strGrittingRouteOptimisationJobFileSavePath = "";
            string strGrittingRouteOptimisationIniFileSavePath = "";
            string strGrittingRouteOptimisationOptimisedRouteSavePath = "";
            string strGrittingRouteOptimisationProject = "";
            string strGrittingRouteOptimisationProfile = "";
            try
            {
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp04356_GC_Route_optimization_Get_Settings", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
                DataSet dsSettings = new DataSet("NewDataSet");
                sdaSettings.Fill(dsSettings, "Table");
                conn.Close();
                conn.Dispose();
                if (dsSettings.Tables[0].Rows.Count != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Route Optimization Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Route Optimization Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                DataRow dr = dsSettings.Tables[0].Rows[0];
                strGrittingRouteOptimisationURL = dr["GrittingRouteOptimisationURL"].ToString();
                strGrittingRouteOptimisationUsername = dr["GrittingRouteOptimisationUsername"].ToString();
                strGrittingRouteOptimisationPassword = dr["GrittingRouteOptimisationPassword"].ToString();
                strGrittingRouteOptimisationJobFileSavePath = dr["GrittingRouteOptimisationJobFileSavePath"].ToString();
                strGrittingRouteOptimisationIniFileSavePath = dr["GrittingRouteOptimisationIniFileSavePath"].ToString();
                strGrittingRouteOptimisationOptimisedRouteSavePath = dr["GrittingRouteOptimisationOptimisedRouteSavePath"].ToString();
                strGrittingRouteOptimisationProject = dr["GrittingRouteOptimisationProject"].ToString();
                strGrittingRouteOptimisationProfile = dr["GrittingRouteOptimisationProfile"].ToString();
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Route Optimization Settings' (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Route Optimization Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            #endregion

            bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
            if (!boolNoInternet) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send jobs for route optimization.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Encrypt password with MD5 as web service requires this //
            string strHashedPassword = "";
            try
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    strHashedPassword = GetMd5Hash(md5Hash, strGrittingRouteOptimisationPassword);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("An error occurred while attempting to encrypt the password for the Route Optimizer web servicer - [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support!", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // Get a token from the web site //
            string strMessage = "";
            string strToken = "";
            string strServer = "";
            try
            {
                // Create a 'WebRequest' object with the specified url //
                string strURL = strGrittingRouteOptimisationURL + "/api/getToken?username=" + strGrittingRouteOptimisationUsername + "&password=" + strHashedPassword + "&project=" + strGrittingRouteOptimisationProject + "&profile=" + strGrittingRouteOptimisationProfile;
                System.Net.WebRequest myWebRequest = System.Net.WebRequest.Create(strURL);


                System.Net.WebResponse myWebResponse = myWebRequest.GetResponse();  // Send the 'WebRequest' and wait for response //
                Stream ReceiveStream = myWebResponse.GetResponseStream();  // Obtain a 'Stream' object associated with the response object //

                XmlDocument xDoc = new XmlDocument(); 
                xDoc.Load(ReceiveStream);

                foreach (XmlNode node in xDoc.DocumentElement.ChildNodes)  // cycle through each child node // 
                {
                        // thereare a couple child nodes here so only take data from node named loc 
                    switch (node.Name.ToLower())
                        {
                        case "message":
                            {
                                strMessage = node.InnerText;
                            }
                            break;
                        case "token":
                            {
                                strToken = node.InnerText;
                            }
                            break;
                        case "server":
                            {
                                strServer = node.InnerText;
                            }
                            break;
                        default:
                            break;
                    }
                }

                myWebResponse.Close();  // Release the resources of response object //
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("An error occurred while attempting to get a token from the Route Optimizer web servicer - [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support!", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (string.IsNullOrWhiteSpace(strToken))
            {
                XtraMessageBox.Show("Unable to get token from the Route Optimizer web servicer - Server Message: " + strMessage + ".\n\nPlease try again. If the problem persists contact Technical Support!", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            try
            {
                string strURL = strGrittingRouteOptimisationURL + "/api/getOutput?token=" + strToken;
                System.Net.WebRequest myWebRequest = System.Net.WebRequest.Create(strURL);

                // Send the 'WebRequest' and wait for response //
                System.Net.WebResponse myWebResponse = myWebRequest.GetResponse();

                // Obtain a 'Stream' object associated with the response object //
                Stream ReceiveStream = myWebResponse.GetResponseStream();

                StreamReader readStream = new StreamReader(ReceiveStream);
                string responseString = readStream.ReadToEnd();

                // Get Jobs //
                List<Callout> jobList = XDocument.Parse(responseString)
                //.Descendants("USEROUT")
                //.Descendants("result")
                //.Where(el => string.Equals((string)el.Attribute("filename"), "User_out.csv", StringComparison.OrdinalIgnoreCase))
                .Descendants("row")
                .Select(item => new Callout
                {
                    CalloutID = (string)item.Element("ORDER"),
                    TaskType = (string)item.Element("TASK_TYPE"),
                    ArrDay = (string)item.Element("ARR_DAY"),
                    ArrTime = (string)item.Element("ARR_TIME"),
                    StartDep = (string)item.Element("START_DEP"),
                    VehicleName = (string)item.Element("VEHICLE_NAME"),
                    Sequence = (string)item.Element("SEQUENCE")
                })
                .ToList();

                // ***** Following no longer does anything - Since Web Service doesn't return errors in the XML anny more ***** //
                // Get Errors //
                List<Error> errorList = XDocument.Parse(responseString)
                .Descendants("USEROUT")
                .Where(el => string.Equals((string)el.Attribute("filename"), "Config-Complete.csv", StringComparison.OrdinalIgnoreCase))
                .Descendants("row")
                .Select(item => new Error
                {
                    ErrorID = (int)item.Element("Error"),
                    Description = (string)item.Element("Description"),
                    Details = (string)item.Element("Details"),
                })
                .ToList();

                // Close the Token //
                strURL = strGrittingRouteOptimisationURL + "/api/closeToken?token=" + strToken;
                myWebRequest = System.Net.WebRequest.Create(strURL);
                myWebRequest.GetResponse();  // Send the 'WebRequest' and wait for response //

                readStream.Close();  // Release the resources of stream object //
                myWebResponse.Close();  // Release the resources of response object //

                if (jobList.Count <= 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Route Optimized callouts available yet.", "Route Optimization - Get Results", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                string strCalloutIDs = "";
                string strCurrentID = "";
                foreach (var item in jobList)
                {
                    strCurrentID = item.CalloutID.Replace("\"", string.Empty);
                    if (string.IsNullOrEmpty(strCurrentID)) continue;
                    strCurrentID += ",";
                    if (!strCalloutIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                    {
                        strCalloutIDs += strCurrentID;
                    }
                }
                if (string.IsNullOrEmpty(strCalloutIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Route Optimized callouts available yet.", "Route Optimization - Get Results", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                GridView view = (GridView)gridControl2.MainView;
                view.BeginUpdate();
                sp04362_GC_Route_Optimization_ResultsTableAdapter.Fill(dataSet_GC_Core.sp04362_GC_Route_Optimization_Results, strCalloutIDs);
                view.EndUpdate();
                
                // Update Grid Rows with results from optimisation //
                DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetTeamName = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                GetTeamName.ChangeConnectionString(strConnectionString);

                view.BeginDataUpdate();
                int intFoundRow = 0;
                foreach (var item in jobList)
                {
                    if (item.TaskType != "2") continue;
                    if (string.IsNullOrEmpty(Convert.ToString(item.CalloutID.Replace("\"", string.Empty)))) continue;
                    intFoundRow = view.LocateByValue(0, view.Columns["GrittingCallOutID"], Convert.ToInt32(item.CalloutID.Replace("\"", string.Empty)));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "AttendanceOrder", Convert.ToInt32(item.Sequence) - 1);
                        view.SetRowCellValue(intFoundRow, "SubContractorID", Convert.ToInt32(item.StartDep.Replace("\"", string.Empty).Substring(1)));
                        view.SetRowCellValue(intFoundRow, "PdaID", Convert.ToInt32(item.VehicleName.Replace("\"", string.Empty).Substring(3)));

                        // Resolve SubContractor Name and put it in grid //
                        view.SetRowCellValue(intFoundRow, "SubContractorName", GetTeamName.sp04364_GC_Route_Optimization_Get_Team_Name(Convert.ToInt32(item.StartDep.Replace("\"", string.Empty).Substring(1))).ToString());
                    }
                }
                view.EndDataUpdate();

                // Write XML into richEditControl1 //
                richEditControl1.Text = responseString;

                // Update Error Grid //
                view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                this.dataSet_GC_Core.sp04363_GC_Route_Optimization_Errors.Rows.Clear();
                foreach (var item in errorList)
                {
                    DataRow drNewRow = this.dataSet_GC_Core.sp04363_GC_Route_Optimization_Errors.NewRow();
                    drNewRow["Error"] = item.ErrorID;
                    drNewRow["Description"] = item.Description;
                    drNewRow["Details"] = item.Details;
                    this.dataSet_GC_Core.sp04363_GC_Route_Optimization_Errors.Rows.Add(drNewRow);
                }
                view.EndUpdate();

            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred fetching the Route Optimized results...\n\nError: " + Ex.Message, "Route Optimization - Get Results", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

        }

        /*  ***** HISTORICAL OLD VERSION OF CODE *****
        private void LoadData()
        {
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                string strRequestText = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingRouteOptimisationURL").ToString();
                strRequestText += "?username=" + GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingRouteOptimisationUsername").ToString().ToLower();

                // Create a 'WebRequest' object with the specified url //
                //System.Net.WebRequest myWebRequest = System.Net.WebRequest.Create("http://api.logixcentral.net/api/lxcdirect?username=grdcntrl");
                System.Net.WebRequest myWebRequest = System.Net.WebRequest.Create(strRequestText);

                // Send the 'WebRequest' and wait for response //
                System.Net.WebResponse myWebResponse = myWebRequest.GetResponse();

                // Obtain a 'Stream' object associated with the response object //
                Stream ReceiveStream = myWebResponse.GetResponseStream();

                StreamReader readStream = new StreamReader(ReceiveStream);
                string responseString = readStream.ReadToEnd();
                //string responseString = File.ReadAllText(@"c:\Results.xml");

                // Get Jobs //
                List<Callout> jobList = XDocument.Parse(responseString)
                .Descendants("USEROUT")
                .Where(el => string.Equals((string)el.Attribute("filename"), "User_out.csv", StringComparison.OrdinalIgnoreCase))
                .Descendants("row")
                .Select(item => new Callout
                {
                    CalloutID = (string)item.Element("ORDER"),
                    TaskType = (string)item.Element("TASK_TYPE"),
                    ArrDay = (string)item.Element("ARR_DAY"),
                    ArrTime = (string)item.Element("ARR_TIME"),
                    StartDep = (string)item.Element("START_DEP"),
                    VehicleName = (string)item.Element("VEHICLE_NAME"),
                    Sequence = (string)item.Element("SEQUENCE")
                })
                .ToList();

                // Get Errors //
                List<Error> errorList = XDocument.Parse(responseString)
                .Descendants("USEROUT")
                .Where(el => string.Equals((string)el.Attribute("filename"), "Config-Complete.csv", StringComparison.OrdinalIgnoreCase))
                .Descendants("row")
                .Select(item => new Error
                {
                    ErrorID = (int)item.Element("Error"),
                    Description = (string)item.Element("Description"),
                    Details = (string)item.Element("Details"),
                })
                .ToList();

                readStream.Close();  // Release the resources of stream object //
                myWebResponse.Close();  // Release the resources of response object //

                if (jobList.Count <= 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Route Optimized callouts available yet.", "Route Optimization - Get Results", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                string strCalloutIDs = "";
                string strCurrentID = "";
                foreach (var item in jobList)
                {
                    strCurrentID = item.CalloutID.Replace("\"", string.Empty);
                    if (string.IsNullOrEmpty(strCurrentID)) continue;
                    strCurrentID += ",";
                    if (!strCalloutIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                    {
                        strCalloutIDs += strCurrentID;
                    }
                }
                if (string.IsNullOrEmpty(strCalloutIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Route Optimized callouts available yet.", "Route Optimization - Get Results", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                GridView view = (GridView)gridControl2.MainView;
                view.BeginUpdate();
                sp04362_GC_Route_Optimization_ResultsTableAdapter.Fill(dataSet_GC_Core.sp04362_GC_Route_Optimization_Results, strCalloutIDs);
                view.EndUpdate();

                // Update Grid Rows with results from optimisation //
                DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetTeamName = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                GetTeamName.ChangeConnectionString(strConnectionString);

                view.BeginDataUpdate();
                int intFoundRow = 0;
                foreach (var item in jobList)
                {
                    if (item.TaskType != "2") continue;
                    if (string.IsNullOrEmpty(Convert.ToString(item.CalloutID.Replace("\"", string.Empty)))) continue;
                    intFoundRow = view.LocateByValue(0, view.Columns["GrittingCallOutID"], Convert.ToInt32(item.CalloutID.Replace("\"", string.Empty)));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "AttendanceOrder", Convert.ToInt32(item.Sequence) - 1);
                        view.SetRowCellValue(intFoundRow, "SubContractorID", Convert.ToInt32(item.StartDep.Replace("\"", string.Empty).Substring(1)));
                        view.SetRowCellValue(intFoundRow, "PdaID", Convert.ToInt32(item.VehicleName.Replace("\"", string.Empty).Substring(3)));

                        // Resolve SubContractor Name and put it in grid //
                        view.SetRowCellValue(intFoundRow, "SubContractorName", GetTeamName.sp04364_GC_Route_Optimization_Get_Team_Name(Convert.ToInt32(item.StartDep.Replace("\"", string.Empty).Substring(1))).ToString());
                    }
                }
                view.EndDataUpdate();

                // Write XML into richEditControl1 //
                richEditControl1.Text = responseString;

                // Update Error Grid //
                view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                this.dataSet_GC_Core.sp04363_GC_Route_Optimization_Errors.Rows.Clear();
                foreach (var item in errorList)
                {
                    DataRow drNewRow = this.dataSet_GC_Core.sp04363_GC_Route_Optimization_Errors.NewRow();
                    drNewRow["Error"] = item.ErrorID;
                    drNewRow["Description"] = item.Description;
                    drNewRow["Details"] = item.Details;
                    this.dataSet_GC_Core.sp04363_GC_Route_Optimization_Errors.Rows.Add(drNewRow);
                }
                view.EndUpdate();

            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred fetching the Route Optimized results...\n\nError: " + Ex.Message, "Route Optimization - Get Results", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

        }*/


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Errors";
                    break;
                case "gridView2":
                    message = "No Optimized Jobs";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        #endregion


        #region GridView1

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView2

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            string strMessage = "";         
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) from route //
            strMessage = "You have one or more callouts selected for delete!\n\nProceed?\n\nIf you proceed these records will be removed from the optimized route only - they will NOT be deleted from the database.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                gridControl1.BeginUpdate();
                for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                {
                    view.DeleteRow(intRowHandles[intRowHandle]);
                }
                gridControl1.EndUpdate();
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted from optimized route.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            view.PostEditor();  // Commit any outstanding changes to underlying dataset //
            int intCount = view.DataRowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Optimized callouts are available. Click the Reload Data button to check for Route Optimized Callouts.", "Update Callouts with Optimized Route", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to update " + (intCount == 1 ? "1 Callout" : Convert.ToString(intCount) + " Callouts") + ".\n\nAre you sure you wish to proceed?", "Update Callouts with Optimized Route", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            DataSet_GC_CoreTableAdapters.QueriesTableAdapter UpdateJobs = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            UpdateJobs.ChangeConnectionString(strConnectionString);
            try
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    UpdateJobs.sp04365_GC_Route_Optimization_Update_Jobs(Convert.ToInt32(view.GetRowCellValue(i, "GrittingCallOutID")), Convert.ToInt32(view.GetRowCellValue(i, "AttendanceOrder")), Convert.ToInt32(view.GetRowCellValue(i, "SubContractorID")), Convert.ToInt32(view.GetRowCellValue(i, "PdaID")));
                }
            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred updating the callouts with the Optimized Route...\n\nError: " + Ex.Message, "Update Callouts with Optimized Route", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;

            }
            boolRefreshCallingScreen = true;
            DevExpress.XtraEditors.XtraMessageBox.Show("Callouts Updated Successfully with Optimized Route.", "Update Callouts with Optimized Route", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash //
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes and create a string //
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data and format each one as a hexadecimal string //
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();  // Return the hexadecimal string //
        }


        public class Callout
        {
            public string CalloutID { get; set; }
            public string TaskType { get; set; }
            public string ArrDay { get; set; }
            public string ArrTime { get; set; }
            public string StartDep { get; set; }
            public string VehicleName { get; set; }
            public string Sequence { get; set; }
        }

        public class Error
        {
            public int ErrorID { get; set; }
            public string Description { get; set; }
            public string Details { get; set; }
        }


    }
}

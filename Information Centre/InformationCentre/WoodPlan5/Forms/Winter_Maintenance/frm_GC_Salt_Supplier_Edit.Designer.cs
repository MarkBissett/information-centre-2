namespace WoodPlan5
{
    partial class frm_GC_Salt_Supplier_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Salt_Supplier_Edit));
            this.colSaltUnitConversionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.EmailPasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp04207GCSaltSupplierEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.SupplierConvertedCostPerUnitSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SupplierCostUnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04209GCSaltUnitConversionListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colConversionValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUnitDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SupplierCostUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SupplierCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactNamePositionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WebSiteTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TextTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FaxTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MobileTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.GritSupplierIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForGritSupplierID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForMobile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFax = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForText = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWebSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactNamePosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEmailPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSupplierCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSupplierCostUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSupplierCostUnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSupplierConvertedCostPerUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04207_GC_Salt_Supplier_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04207_GC_Salt_Supplier_EditTableAdapter();
            this.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmailPasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04207GCSaltSupplierEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierConvertedCostPerUnitSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierCostUnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04209GCSaltUnitConversionListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierCostUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNamePositionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSiteTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaxTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritSupplierIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSupplierID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactNamePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierCostUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierCostUnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierConvertedCostPerUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colSaltUnitConversionID
            // 
            this.colSaltUnitConversionID.Caption = "Salt Unit Conversion ID";
            this.colSaltUnitConversionID.FieldName = "SaltUnitConversionID";
            this.colSaltUnitConversionID.Name = "colSaltUnitConversionID";
            this.colSaltUnitConversionID.OptionsColumn.AllowEdit = false;
            this.colSaltUnitConversionID.OptionsColumn.AllowFocus = false;
            this.colSaltUnitConversionID.OptionsColumn.ReadOnly = true;
            this.colSaltUnitConversionID.Width = 132;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.EmailPasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SupplierConvertedCostPerUnitSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SupplierCostUnitDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SupplierCostUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SupplierCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactNamePositionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WebSiteTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TextTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FaxTextEdit);
            this.dataLayoutControl1.Controls.Add(this.MobileTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.GritSupplierIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.DataSource = this.sp04207GCSaltSupplierEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGritSupplierID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(830, 347, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // EmailPasswordTextEdit
            // 
            this.EmailPasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "EmailPassword", true));
            this.EmailPasswordTextEdit.Location = new System.Drawing.Point(151, 260);
            this.EmailPasswordTextEdit.MenuManager = this.barManager1;
            this.EmailPasswordTextEdit.Name = "EmailPasswordTextEdit";
            this.EmailPasswordTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmailPasswordTextEdit, true);
            this.EmailPasswordTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmailPasswordTextEdit, optionsSpelling1);
            this.EmailPasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailPasswordTextEdit.TabIndex = 32;
            // 
            // sp04207GCSaltSupplierEditBindingSource
            // 
            this.sp04207GCSaltSupplierEditBindingSource.DataMember = "sp04207_GC_Salt_Supplier_Edit";
            this.sp04207GCSaltSupplierEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SupplierConvertedCostPerUnitSpinEdit
            // 
            this.SupplierConvertedCostPerUnitSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "SupplierConvertedCostPerUnit", true));
            this.SupplierConvertedCostPerUnitSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SupplierConvertedCostPerUnitSpinEdit.Location = new System.Drawing.Point(151, 212);
            this.SupplierConvertedCostPerUnitSpinEdit.MenuManager = this.barManager1;
            this.SupplierConvertedCostPerUnitSpinEdit.Name = "SupplierConvertedCostPerUnitSpinEdit";
            this.SupplierConvertedCostPerUnitSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SupplierConvertedCostPerUnitSpinEdit.Properties.Mask.EditMask = "c";
            this.SupplierConvertedCostPerUnitSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SupplierConvertedCostPerUnitSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.SupplierConvertedCostPerUnitSpinEdit.Properties.ReadOnly = true;
            this.SupplierConvertedCostPerUnitSpinEdit.Size = new System.Drawing.Size(424, 20);
            this.SupplierConvertedCostPerUnitSpinEdit.StyleController = this.dataLayoutControl1;
            this.SupplierConvertedCostPerUnitSpinEdit.TabIndex = 31;
            // 
            // SupplierCostUnitDescriptorIDGridLookUpEdit
            // 
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "SupplierCostUnitDescriptorID", true));
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(151, 188);
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Name = "SupplierCostUnitDescriptorIDGridLookUpEdit";
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp04209GCSaltUnitConversionListWithBlankBindingSource;
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "SaltUnitDescription";
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "SaltUnitConversionID";
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(424, 20);
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.TabIndex = 30;
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.SupplierCostUnitDescriptorIDGridLookUpEdit_EditValueChanged);
            this.SupplierCostUnitDescriptorIDGridLookUpEdit.Validated += new System.EventHandler(this.SupplierCostUnitDescriptorIDGridLookUpEdit_Validated);
            // 
            // sp04209GCSaltUnitConversionListWithBlankBindingSource
            // 
            this.sp04209GCSaltUnitConversionListWithBlankBindingSource.DataMember = "sp04209_GC_Salt_Unit_Conversion_List_With_Blank";
            this.sp04209GCSaltUnitConversionListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colConversionValue,
            this.colSaltUnitConversionID,
            this.colSaltUnitDescription});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colSaltUnitConversionID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSaltUnitDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colConversionValue
            // 
            this.colConversionValue.Caption = "Conversion Value";
            this.colConversionValue.ColumnEdit = this.repositoryItemTextEdit1;
            this.colConversionValue.FieldName = "ConversionValue";
            this.colConversionValue.Name = "colConversionValue";
            this.colConversionValue.OptionsColumn.AllowEdit = false;
            this.colConversionValue.OptionsColumn.AllowFocus = false;
            this.colConversionValue.OptionsColumn.ReadOnly = true;
            this.colConversionValue.Visible = true;
            this.colConversionValue.VisibleIndex = 1;
            this.colConversionValue.Width = 107;
            // 
            // colSaltUnitDescription
            // 
            this.colSaltUnitDescription.Caption = "Salt Unit Description";
            this.colSaltUnitDescription.FieldName = "SaltUnitDescription";
            this.colSaltUnitDescription.Name = "colSaltUnitDescription";
            this.colSaltUnitDescription.OptionsColumn.AllowEdit = false;
            this.colSaltUnitDescription.OptionsColumn.AllowFocus = false;
            this.colSaltUnitDescription.OptionsColumn.ReadOnly = true;
            this.colSaltUnitDescription.Visible = true;
            this.colSaltUnitDescription.VisibleIndex = 0;
            this.colSaltUnitDescription.Width = 166;
            // 
            // SupplierCostUnitsSpinEdit
            // 
            this.SupplierCostUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "SupplierCostUnits", true));
            this.SupplierCostUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SupplierCostUnitsSpinEdit.Location = new System.Drawing.Point(151, 164);
            this.SupplierCostUnitsSpinEdit.MenuManager = this.barManager1;
            this.SupplierCostUnitsSpinEdit.Name = "SupplierCostUnitsSpinEdit";
            this.SupplierCostUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SupplierCostUnitsSpinEdit.Properties.Mask.EditMask = "n2";
            this.SupplierCostUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SupplierCostUnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.SupplierCostUnitsSpinEdit.Size = new System.Drawing.Size(424, 20);
            this.SupplierCostUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.SupplierCostUnitsSpinEdit.TabIndex = 29;
            this.SupplierCostUnitsSpinEdit.EditValueChanged += new System.EventHandler(this.SupplierCostUnitsSpinEdit_EditValueChanged);
            this.SupplierCostUnitsSpinEdit.Validated += new System.EventHandler(this.SupplierCostUnitsSpinEdit_Validated);
            // 
            // SupplierCostSpinEdit
            // 
            this.SupplierCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "SupplierCost", true));
            this.SupplierCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SupplierCostSpinEdit.Location = new System.Drawing.Point(151, 140);
            this.SupplierCostSpinEdit.MenuManager = this.barManager1;
            this.SupplierCostSpinEdit.Name = "SupplierCostSpinEdit";
            this.SupplierCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SupplierCostSpinEdit.Properties.Mask.EditMask = "c";
            this.SupplierCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SupplierCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.SupplierCostSpinEdit.Size = new System.Drawing.Size(424, 20);
            this.SupplierCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.SupplierCostSpinEdit.TabIndex = 28;
            this.SupplierCostSpinEdit.EditValueChanged += new System.EventHandler(this.SupplierCostSpinEdit_EditValueChanged);
            this.SupplierCostSpinEdit.Validated += new System.EventHandler(this.SupplierCostSpinEdit_Validated);
            // 
            // LongitudeTextEdit
            // 
            this.LongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Longitude", true));
            this.LongitudeTextEdit.Location = new System.Drawing.Point(127, 424);
            this.LongitudeTextEdit.MenuManager = this.barManager1;
            this.LongitudeTextEdit.Name = "LongitudeTextEdit";
            this.LongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.LongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LongitudeTextEdit, true);
            this.LongitudeTextEdit.Size = new System.Drawing.Size(472, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LongitudeTextEdit, optionsSpelling2);
            this.LongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.LongitudeTextEdit.TabIndex = 27;
            // 
            // LatitudeTextEdit
            // 
            this.LatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Latitude", true));
            this.LatitudeTextEdit.Location = new System.Drawing.Point(127, 400);
            this.LatitudeTextEdit.MenuManager = this.barManager1;
            this.LatitudeTextEdit.Name = "LatitudeTextEdit";
            this.LatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.LatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LatitudeTextEdit, true);
            this.LatitudeTextEdit.Size = new System.Drawing.Size(472, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LatitudeTextEdit, optionsSpelling3);
            this.LatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.LatitudeTextEdit.TabIndex = 26;
            // 
            // ContactNamePositionTextEdit
            // 
            this.ContactNamePositionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "ContactNamePosition", true));
            this.ContactNamePositionTextEdit.Location = new System.Drawing.Point(151, 342);
            this.ContactNamePositionTextEdit.MenuManager = this.barManager1;
            this.ContactNamePositionTextEdit.Name = "ContactNamePositionTextEdit";
            this.ContactNamePositionTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactNamePositionTextEdit, true);
            this.ContactNamePositionTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactNamePositionTextEdit, optionsSpelling4);
            this.ContactNamePositionTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactNamePositionTextEdit.TabIndex = 25;
            // 
            // ContactNameTextEdit
            // 
            this.ContactNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "ContactName", true));
            this.ContactNameTextEdit.Location = new System.Drawing.Point(151, 318);
            this.ContactNameTextEdit.MenuManager = this.barManager1;
            this.ContactNameTextEdit.Name = "ContactNameTextEdit";
            this.ContactNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactNameTextEdit, true);
            this.ContactNameTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactNameTextEdit, optionsSpelling5);
            this.ContactNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactNameTextEdit.TabIndex = 24;
            // 
            // WebSiteTextEdit
            // 
            this.WebSiteTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "WebSite", true));
            this.WebSiteTextEdit.Location = new System.Drawing.Point(151, 284);
            this.WebSiteTextEdit.MenuManager = this.barManager1;
            this.WebSiteTextEdit.Name = "WebSiteTextEdit";
            this.WebSiteTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WebSiteTextEdit, true);
            this.WebSiteTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WebSiteTextEdit, optionsSpelling6);
            this.WebSiteTextEdit.StyleController = this.dataLayoutControl1;
            this.WebSiteTextEdit.TabIndex = 23;
            // 
            // EmailTextEdit
            // 
            this.EmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Email", true));
            this.EmailTextEdit.Location = new System.Drawing.Point(151, 236);
            this.EmailTextEdit.MenuManager = this.barManager1;
            this.EmailTextEdit.Name = "EmailTextEdit";
            this.EmailTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmailTextEdit, true);
            this.EmailTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmailTextEdit, optionsSpelling7);
            this.EmailTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailTextEdit.TabIndex = 22;
            // 
            // TextTextEdit
            // 
            this.TextTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Text", true));
            this.TextTextEdit.Location = new System.Drawing.Point(151, 212);
            this.TextTextEdit.MenuManager = this.barManager1;
            this.TextTextEdit.Name = "TextTextEdit";
            this.TextTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TextTextEdit, true);
            this.TextTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TextTextEdit, optionsSpelling8);
            this.TextTextEdit.StyleController = this.dataLayoutControl1;
            this.TextTextEdit.TabIndex = 21;
            // 
            // FaxTextEdit
            // 
            this.FaxTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Fax", true));
            this.FaxTextEdit.Location = new System.Drawing.Point(151, 188);
            this.FaxTextEdit.MenuManager = this.barManager1;
            this.FaxTextEdit.Name = "FaxTextEdit";
            this.FaxTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FaxTextEdit, true);
            this.FaxTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FaxTextEdit, optionsSpelling9);
            this.FaxTextEdit.StyleController = this.dataLayoutControl1;
            this.FaxTextEdit.TabIndex = 20;
            // 
            // MobileTextEdit
            // 
            this.MobileTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Mobile", true));
            this.MobileTextEdit.Location = new System.Drawing.Point(151, 164);
            this.MobileTextEdit.MenuManager = this.barManager1;
            this.MobileTextEdit.Name = "MobileTextEdit";
            this.MobileTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MobileTextEdit, true);
            this.MobileTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MobileTextEdit, optionsSpelling10);
            this.MobileTextEdit.StyleController = this.dataLayoutControl1;
            this.MobileTextEdit.TabIndex = 19;
            // 
            // TelephoneTextEdit
            // 
            this.TelephoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Telephone", true));
            this.TelephoneTextEdit.Location = new System.Drawing.Point(151, 140);
            this.TelephoneTextEdit.MenuManager = this.barManager1;
            this.TelephoneTextEdit.Name = "TelephoneTextEdit";
            this.TelephoneTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TelephoneTextEdit, true);
            this.TelephoneTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TelephoneTextEdit, optionsSpelling11);
            this.TelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.TelephoneTextEdit.TabIndex = 18;
            // 
            // PostcodeTextEdit
            // 
            this.PostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Postcode", true));
            this.PostcodeTextEdit.Location = new System.Drawing.Point(151, 260);
            this.PostcodeTextEdit.MenuManager = this.barManager1;
            this.PostcodeTextEdit.Name = "PostcodeTextEdit";
            this.PostcodeTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PostcodeTextEdit, true);
            this.PostcodeTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PostcodeTextEdit, optionsSpelling12);
            this.PostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.PostcodeTextEdit.TabIndex = 17;
            // 
            // AddressLine5TextEdit
            // 
            this.AddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "AddressLine5", true));
            this.AddressLine5TextEdit.Location = new System.Drawing.Point(151, 236);
            this.AddressLine5TextEdit.MenuManager = this.barManager1;
            this.AddressLine5TextEdit.Name = "AddressLine5TextEdit";
            this.AddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine5TextEdit, true);
            this.AddressLine5TextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine5TextEdit, optionsSpelling13);
            this.AddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine5TextEdit.TabIndex = 16;
            // 
            // AddressLine4TextEdit
            // 
            this.AddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "AddressLine4", true));
            this.AddressLine4TextEdit.Location = new System.Drawing.Point(151, 212);
            this.AddressLine4TextEdit.MenuManager = this.barManager1;
            this.AddressLine4TextEdit.Name = "AddressLine4TextEdit";
            this.AddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine4TextEdit, true);
            this.AddressLine4TextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine4TextEdit, optionsSpelling14);
            this.AddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine4TextEdit.TabIndex = 15;
            // 
            // AddressLine3TextEdit
            // 
            this.AddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "AddressLine3", true));
            this.AddressLine3TextEdit.Location = new System.Drawing.Point(151, 188);
            this.AddressLine3TextEdit.MenuManager = this.barManager1;
            this.AddressLine3TextEdit.Name = "AddressLine3TextEdit";
            this.AddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine3TextEdit, true);
            this.AddressLine3TextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine3TextEdit, optionsSpelling15);
            this.AddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine3TextEdit.TabIndex = 14;
            // 
            // AddressLine2TextEdit
            // 
            this.AddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "AddressLine2", true));
            this.AddressLine2TextEdit.Location = new System.Drawing.Point(151, 164);
            this.AddressLine2TextEdit.MenuManager = this.barManager1;
            this.AddressLine2TextEdit.Name = "AddressLine2TextEdit";
            this.AddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine2TextEdit, true);
            this.AddressLine2TextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine2TextEdit, optionsSpelling16);
            this.AddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine2TextEdit.TabIndex = 13;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04207GCSaltSupplierEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(128, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(207, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 9;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // GritSupplierIDTextEdit
            // 
            this.GritSupplierIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "GritSupplierID", true));
            this.GritSupplierIDTextEdit.Location = new System.Drawing.Point(96, 35);
            this.GritSupplierIDTextEdit.MenuManager = this.barManager1;
            this.GritSupplierIDTextEdit.Name = "GritSupplierIDTextEdit";
            this.GritSupplierIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GritSupplierIDTextEdit, true);
            this.GritSupplierIDTextEdit.Size = new System.Drawing.Size(520, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GritSupplierIDTextEdit, optionsSpelling17);
            this.GritSupplierIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GritSupplierIDTextEdit.TabIndex = 4;
            // 
            // AddressLine1TextEdit
            // 
            this.AddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "AddressLine1", true));
            this.AddressLine1TextEdit.Location = new System.Drawing.Point(151, 140);
            this.AddressLine1TextEdit.MenuManager = this.barManager1;
            this.AddressLine1TextEdit.Name = "AddressLine1TextEdit";
            this.AddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine1TextEdit, true);
            this.AddressLine1TextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine1TextEdit, optionsSpelling18);
            this.AddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine1TextEdit.TabIndex = 5;
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Name", true));
            this.NameTextEdit.Location = new System.Drawing.Point(127, 36);
            this.NameTextEdit.MenuManager = this.barManager1;
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NameTextEdit, true);
            this.NameTextEdit.Size = new System.Drawing.Size(472, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NameTextEdit, optionsSpelling19);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 6;
            this.NameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.NameTextEdit_Validating);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04207GCSaltSupplierEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 140);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(539, 222);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling20);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 8;
            // 
            // ItemForGritSupplierID
            // 
            this.ItemForGritSupplierID.Control = this.GritSupplierIDTextEdit;
            this.ItemForGritSupplierID.CustomizationFormText = "Salt Supplier ID:";
            this.ItemForGritSupplierID.Location = new System.Drawing.Point(0, 23);
            this.ItemForGritSupplierID.Name = "ItemForGritSupplierID";
            this.ItemForGritSupplierID.Size = new System.Drawing.Size(608, 24);
            this.ItemForGritSupplierID.Text = "Salt Supplier ID:";
            this.ItemForGritSupplierID.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(611, 542);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.ItemForName,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem1,
            this.emptySpaceItem5,
            this.ItemForLongitude,
            this.ItemForLatitude});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(591, 522);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 58);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(591, 320);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(567, 274);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup4,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImage")));
            this.layoutControlGroup5.CustomizationFormText = "Contact Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForMobile,
            this.ItemForFax,
            this.ItemForText,
            this.ItemForEmail,
            this.ItemForWebSite,
            this.ItemForContactName,
            this.ItemForContactNamePosition,
            this.emptySpaceItem6,
            this.ItemForEmailPassword,
            this.ItemForTelephone});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(543, 226);
            this.layoutControlGroup5.Text = "Contact Details";
            // 
            // ItemForMobile
            // 
            this.ItemForMobile.Control = this.MobileTextEdit;
            this.ItemForMobile.CustomizationFormText = "Mobile:";
            this.ItemForMobile.Location = new System.Drawing.Point(0, 24);
            this.ItemForMobile.Name = "ItemForMobile";
            this.ItemForMobile.Size = new System.Drawing.Size(543, 24);
            this.ItemForMobile.Text = "Mobile:";
            this.ItemForMobile.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForFax
            // 
            this.ItemForFax.Control = this.FaxTextEdit;
            this.ItemForFax.CustomizationFormText = "Fax:";
            this.ItemForFax.Location = new System.Drawing.Point(0, 48);
            this.ItemForFax.Name = "ItemForFax";
            this.ItemForFax.Size = new System.Drawing.Size(543, 24);
            this.ItemForFax.Text = "Fax:";
            this.ItemForFax.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForText
            // 
            this.ItemForText.Control = this.TextTextEdit;
            this.ItemForText.CustomizationFormText = "Text:";
            this.ItemForText.Location = new System.Drawing.Point(0, 72);
            this.ItemForText.Name = "ItemForText";
            this.ItemForText.Size = new System.Drawing.Size(543, 24);
            this.ItemForText.Text = "Text:";
            this.ItemForText.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailTextEdit;
            this.ItemForEmail.CustomizationFormText = "Email:";
            this.ItemForEmail.Location = new System.Drawing.Point(0, 96);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(543, 24);
            this.ItemForEmail.Text = "Email:";
            this.ItemForEmail.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForWebSite
            // 
            this.ItemForWebSite.Control = this.WebSiteTextEdit;
            this.ItemForWebSite.CustomizationFormText = "Website:";
            this.ItemForWebSite.Location = new System.Drawing.Point(0, 144);
            this.ItemForWebSite.Name = "ItemForWebSite";
            this.ItemForWebSite.Size = new System.Drawing.Size(543, 24);
            this.ItemForWebSite.Text = "Website:";
            this.ItemForWebSite.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForContactName
            // 
            this.ItemForContactName.Control = this.ContactNameTextEdit;
            this.ItemForContactName.CustomizationFormText = "Contact Name:";
            this.ItemForContactName.Location = new System.Drawing.Point(0, 178);
            this.ItemForContactName.Name = "ItemForContactName";
            this.ItemForContactName.Size = new System.Drawing.Size(543, 24);
            this.ItemForContactName.Text = "Contact Name:";
            this.ItemForContactName.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForContactNamePosition
            // 
            this.ItemForContactNamePosition.Control = this.ContactNamePositionTextEdit;
            this.ItemForContactNamePosition.CustomizationFormText = "Contact Name Position:";
            this.ItemForContactNamePosition.Location = new System.Drawing.Point(0, 202);
            this.ItemForContactNamePosition.Name = "ItemForContactNamePosition";
            this.ItemForContactNamePosition.Size = new System.Drawing.Size(543, 24);
            this.ItemForContactNamePosition.Text = "Contact Name Position:";
            this.ItemForContactNamePosition.TextSize = new System.Drawing.Size(112, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 168);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(543, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEmailPassword
            // 
            this.ItemForEmailPassword.Control = this.EmailPasswordTextEdit;
            this.ItemForEmailPassword.CustomizationFormText = "Email Password:";
            this.ItemForEmailPassword.Location = new System.Drawing.Point(0, 120);
            this.ItemForEmailPassword.Name = "ItemForEmailPassword";
            this.ItemForEmailPassword.Size = new System.Drawing.Size(543, 24);
            this.ItemForEmailPassword.Text = "Email Password:";
            this.ItemForEmailPassword.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForTelephone
            // 
            this.ItemForTelephone.Control = this.TelephoneTextEdit;
            this.ItemForTelephone.CustomizationFormText = "Telephone:";
            this.ItemForTelephone.Location = new System.Drawing.Point(0, 0);
            this.ItemForTelephone.Name = "ItemForTelephone";
            this.ItemForTelephone.Size = new System.Drawing.Size(543, 24);
            this.ItemForTelephone.Text = "Telephone:";
            this.ItemForTelephone.TextSize = new System.Drawing.Size(112, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CaptionImage = global::WoodPlan5.Properties.Resources.Home_16x16;
            this.layoutControlGroup6.CustomizationFormText = "Address";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAddressLine1,
            this.ItemForAddressLine2,
            this.ItemForAddressLine3,
            this.ItemForAddressLine4,
            this.ItemForAddressLine5,
            this.ItemForPostcode});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(543, 226);
            this.layoutControlGroup6.Text = "Address";
            // 
            // ItemForAddressLine1
            // 
            this.ItemForAddressLine1.Control = this.AddressLine1TextEdit;
            this.ItemForAddressLine1.CustomizationFormText = "Address Line 1:";
            this.ItemForAddressLine1.Location = new System.Drawing.Point(0, 0);
            this.ItemForAddressLine1.Name = "ItemForAddressLine1";
            this.ItemForAddressLine1.Size = new System.Drawing.Size(543, 24);
            this.ItemForAddressLine1.Text = "Address Line 1:";
            this.ItemForAddressLine1.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForAddressLine2
            // 
            this.ItemForAddressLine2.Control = this.AddressLine2TextEdit;
            this.ItemForAddressLine2.CustomizationFormText = "Address Line 2:";
            this.ItemForAddressLine2.Location = new System.Drawing.Point(0, 24);
            this.ItemForAddressLine2.Name = "ItemForAddressLine2";
            this.ItemForAddressLine2.Size = new System.Drawing.Size(543, 24);
            this.ItemForAddressLine2.Text = "Address Line 2:";
            this.ItemForAddressLine2.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForAddressLine3
            // 
            this.ItemForAddressLine3.Control = this.AddressLine3TextEdit;
            this.ItemForAddressLine3.CustomizationFormText = "Address Line 3:";
            this.ItemForAddressLine3.Location = new System.Drawing.Point(0, 48);
            this.ItemForAddressLine3.Name = "ItemForAddressLine3";
            this.ItemForAddressLine3.Size = new System.Drawing.Size(543, 24);
            this.ItemForAddressLine3.Text = "Address Line 3:";
            this.ItemForAddressLine3.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForAddressLine4
            // 
            this.ItemForAddressLine4.Control = this.AddressLine4TextEdit;
            this.ItemForAddressLine4.CustomizationFormText = "Address Line 4:";
            this.ItemForAddressLine4.Location = new System.Drawing.Point(0, 72);
            this.ItemForAddressLine4.Name = "ItemForAddressLine4";
            this.ItemForAddressLine4.Size = new System.Drawing.Size(543, 24);
            this.ItemForAddressLine4.Text = "Address Line 4:";
            this.ItemForAddressLine4.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForAddressLine5
            // 
            this.ItemForAddressLine5.Control = this.AddressLine5TextEdit;
            this.ItemForAddressLine5.CustomizationFormText = "Address Line 5:";
            this.ItemForAddressLine5.Location = new System.Drawing.Point(0, 96);
            this.ItemForAddressLine5.Name = "ItemForAddressLine5";
            this.ItemForAddressLine5.Size = new System.Drawing.Size(543, 24);
            this.ItemForAddressLine5.Text = "Address Line 5:";
            this.ItemForAddressLine5.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForPostcode
            // 
            this.ItemForPostcode.Control = this.PostcodeTextEdit;
            this.ItemForPostcode.CustomizationFormText = "Postcode:";
            this.ItemForPostcode.Location = new System.Drawing.Point(0, 120);
            this.ItemForPostcode.Name = "ItemForPostcode";
            this.ItemForPostcode.Size = new System.Drawing.Size(543, 106);
            this.ItemForPostcode.Text = "Postcode:";
            this.ItemForPostcode.TextSize = new System.Drawing.Size(112, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(543, 226);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(543, 226);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Costs";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSupplierCost,
            this.ItemForSupplierCostUnits,
            this.ItemForSupplierCostUnitDescriptorID,
            this.ItemForSupplierConvertedCostPerUnit});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(543, 226);
            this.layoutControlGroup8.Text = "Costs";
            // 
            // ItemForSupplierCost
            // 
            this.ItemForSupplierCost.Control = this.SupplierCostSpinEdit;
            this.ItemForSupplierCost.CustomizationFormText = "Supplier Cost:";
            this.ItemForSupplierCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForSupplierCost.Name = "ItemForSupplierCost";
            this.ItemForSupplierCost.Size = new System.Drawing.Size(543, 24);
            this.ItemForSupplierCost.Text = "Supplier Cost:";
            this.ItemForSupplierCost.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForSupplierCostUnits
            // 
            this.ItemForSupplierCostUnits.Control = this.SupplierCostUnitsSpinEdit;
            this.ItemForSupplierCostUnits.CustomizationFormText = "Units:";
            this.ItemForSupplierCostUnits.Location = new System.Drawing.Point(0, 24);
            this.ItemForSupplierCostUnits.Name = "ItemForSupplierCostUnits";
            this.ItemForSupplierCostUnits.Size = new System.Drawing.Size(543, 24);
            this.ItemForSupplierCostUnits.Text = "Units:";
            this.ItemForSupplierCostUnits.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForSupplierCostUnitDescriptorID
            // 
            this.ItemForSupplierCostUnitDescriptorID.Control = this.SupplierCostUnitDescriptorIDGridLookUpEdit;
            this.ItemForSupplierCostUnitDescriptorID.CustomizationFormText = "Unit Descriptor:";
            this.ItemForSupplierCostUnitDescriptorID.Location = new System.Drawing.Point(0, 48);
            this.ItemForSupplierCostUnitDescriptorID.Name = "ItemForSupplierCostUnitDescriptorID";
            this.ItemForSupplierCostUnitDescriptorID.Size = new System.Drawing.Size(543, 24);
            this.ItemForSupplierCostUnitDescriptorID.Text = "Unit Descriptor:";
            this.ItemForSupplierCostUnitDescriptorID.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForSupplierConvertedCostPerUnit
            // 
            this.ItemForSupplierConvertedCostPerUnit.Control = this.SupplierConvertedCostPerUnitSpinEdit;
            this.ItemForSupplierConvertedCostPerUnit.CustomizationFormText = "Converted Unit Cost:";
            this.ItemForSupplierConvertedCostPerUnit.Location = new System.Drawing.Point(0, 72);
            this.ItemForSupplierConvertedCostPerUnit.Name = "ItemForSupplierConvertedCostPerUnit";
            this.ItemForSupplierConvertedCostPerUnit.Size = new System.Drawing.Size(543, 154);
            this.ItemForSupplierConvertedCostPerUnit.Text = "Converted Unit Cost:";
            this.ItemForSupplierConvertedCostPerUnit.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForName
            // 
            this.ItemForName.AllowHide = false;
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.CustomizationFormText = "Supplier Name:";
            this.ItemForName.Location = new System.Drawing.Point(0, 24);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(591, 24);
            this.ItemForName.Text = "Supplier Name:";
            this.ItemForName.TextSize = new System.Drawing.Size(112, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 436);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 86);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 86);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(591, 86);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(116, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(116, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(116, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(116, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(327, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(264, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(591, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 378);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(591, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLongitude
            // 
            this.ItemForLongitude.Control = this.LongitudeTextEdit;
            this.ItemForLongitude.CustomizationFormText = "Longitude:";
            this.ItemForLongitude.Location = new System.Drawing.Point(0, 412);
            this.ItemForLongitude.Name = "ItemForLongitude";
            this.ItemForLongitude.Size = new System.Drawing.Size(591, 24);
            this.ItemForLongitude.Text = "Longitude:";
            this.ItemForLongitude.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForLatitude
            // 
            this.ItemForLatitude.Control = this.LatitudeTextEdit;
            this.ItemForLatitude.CustomizationFormText = "Latitude:";
            this.ItemForLatitude.Location = new System.Drawing.Point(0, 388);
            this.ItemForLatitude.Name = "ItemForLatitude";
            this.ItemForLatitude.Size = new System.Drawing.Size(591, 24);
            this.ItemForLatitude.Text = "Latitude:";
            this.ItemForLatitude.TextSize = new System.Drawing.Size(112, 13);
            // 
            // sp04207_GC_Salt_Supplier_EditTableAdapter
            // 
            this.sp04207_GC_Salt_Supplier_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter
            // 
            this.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // frm_GC_Salt_Supplier_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Salt_Supplier_Edit";
            this.Text = "Edit Salt Supplier";
            this.Activated += new System.EventHandler(this.frm_GC_Salt_Supplier_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Salt_Supplier_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Salt_Supplier_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EmailPasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04207GCSaltSupplierEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierConvertedCostPerUnitSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierCostUnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04209GCSaltUnitConversionListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierCostUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNamePositionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSiteTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaxTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritSupplierIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSupplierID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactNamePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierCostUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierCostUnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierConvertedCostPerUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit GritSupplierIDTextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine1TextEdit;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritSupplierID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private System.Windows.Forms.BindingSource sp04207GCSaltSupplierEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04207_GC_Salt_Supplier_EditTableAdapter sp04207_GC_Salt_Supplier_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit AddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine2TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit PostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit TelephoneTextEdit;
        private DevExpress.XtraEditors.TextEdit MobileTextEdit;
        private DevExpress.XtraEditors.TextEdit TextTextEdit;
        private DevExpress.XtraEditors.TextEdit FaxTextEdit;
        private DevExpress.XtraEditors.TextEdit WebSiteTextEdit;
        private DevExpress.XtraEditors.TextEdit EmailTextEdit;
        private DevExpress.XtraEditors.TextEdit ContactNameTextEdit;
        private DevExpress.XtraEditors.TextEdit LongitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit LatitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit ContactNamePositionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLatitude;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLongitude;
        private DevExpress.XtraEditors.SpinEdit SupplierCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplierCost;
        private DevExpress.XtraEditors.SpinEdit SupplierCostUnitsSpinEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplierCostUnits;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTelephone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMobile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFax;
        private DevExpress.XtraLayout.LayoutControlItem ItemForText;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWebSite;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactNamePosition;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostcode;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.GridLookUpEdit SupplierCostUnitDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplierCostUnitDescriptorID;
        private DevExpress.XtraEditors.SpinEdit SupplierConvertedCostPerUnitSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplierConvertedCostPerUnit;
        private DevExpress.XtraEditors.TextEdit EmailPasswordTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailPassword;
        private System.Windows.Forms.BindingSource sp04209GCSaltUnitConversionListWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colConversionValue;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUnitConversionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUnitDescription;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

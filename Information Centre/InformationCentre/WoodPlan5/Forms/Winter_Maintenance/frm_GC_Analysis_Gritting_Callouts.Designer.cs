namespace WoodPlan5
{
    partial class frm_GC_Analysis_Gritting_Callouts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Analysis_Gritting_Callouts));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup1 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup2 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup3 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.fieldCalloutYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.dataSet_AT_Reports = new WoodPlan5.DataSet_AT_Reports();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04324GCAnalysisGrittingCalloutsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGrittingCallOutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGrittingContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorIsDirectLabour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContarctorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colImportedWeatherForecastID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextSentTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorReceivedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorRespondedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorETA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitAborted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbortedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishedLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishedLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25KgBags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSaltCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSaltSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHoursWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTeamHourlyRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamCharge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractorReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSourceLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSourceLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteWeather = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTemperature = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamPresent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceFailure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritJobTransferMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarkup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClientReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritMobileTelephoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSignatureFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPPEWorn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccessAbortedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMembersPresent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowInfo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEveningRateModifier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedOnPDA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSheetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowOnSiteOver5cm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDACode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlCompanies = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCompanyFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04237GCCompanyFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlStatuses = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnStatusFilterOK2 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.deFromDate = new DevExpress.XtraEditors.DateEdit();
            this.deToDate = new DevExpress.XtraEditors.DateEdit();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04001GCJobCallOutStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.sp04325GCAnalysisGrittingCalloutsForAnalysisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldSiteGrittingContractID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompanyName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientsSiteCode1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorIsDirectLabour1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOriginalSubContarctorName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldReactive1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutStatusDescription1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutStatusOrder1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCallOutDateTime1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldImportedWeatherForecastID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTextSentTime1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorReceivedTime1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorRespondedTime1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorETA1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedTime1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAuthorisedByName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldVisitAborted1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAbortedReason1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartLatitude1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartLongitude1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldFinishedLatitude1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldFinishedLongitude1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientPONumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSaltUsed1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSaltCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSaltSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSaltVatRate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHoursWorked1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTeamHourlyRate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTeamCharge1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLabourCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLabourVatRate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOtherCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOtherSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTotalCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTotalSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientInvoiceNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorPaid1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldGrittingInvoiceID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRecordedByName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPaidByName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDoNotPaySubContractor1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDoNotPaySubContractorReason1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDoNotInvoiceClient1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDoNotInvoiceClientReason1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNoAccess1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteWeather1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteTemperature1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPdaID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTeamPresent1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldServiceFailure1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldComments1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientPrice1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSnowOnSite1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldGritJobTransferMethod1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartTime1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldProfit1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMarkup1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientPOID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNonStandardCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNonStandardSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPPEWorn1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNoAccessAbortedRate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTeamMembersPresent1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSnowInfo1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEveningRateModifier1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobSheetNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedOnPDA1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldFinanceInvoiceNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutCount1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOperativeName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSnowOnSiteOver5cm = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutInvoiced = new DevExpress.XtraPivotGrid.PivotGridField();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.sp04237_GC_Company_Filter_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiToggleAvailableColumnsVisibility = new DevExpress.XtraBars.BarButtonItem();
            this.pmChart = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiRotateAxis = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChartWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemStatusFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditStatusFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemCompanyFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditCompanyFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiReloadData = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAnalyse = new DevExpress.XtraBars.BarButtonItem();
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter();
            this.sp04324_GC_Analysis_Gritting_Callouts_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04324_GC_Analysis_Gritting_Callouts_ListTableAdapter();
            this.sp04325_GC_Analysis_Gritting_Callouts_For_AnalysisTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04325_GC_Analysis_Gritting_Callouts_For_AnalysisTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04324GCAnalysisGrittingCalloutsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).BeginInit();
            this.popupContainerControlCompanies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlStatuses)).BeginInit();
            this.popupContainerControlStatuses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04325GCAnalysisGrittingCalloutsForAnalysisBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 494);
            this.barDockControlBottom.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 494);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1293, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 494);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.bbiChartWizard,
            this.bbiRotateAxis,
            this.barEditItemStatusFilter,
            this.barEditItemCompanyFilter,
            this.bbiReloadData,
            this.bbiAnalyse,
            this.bbiToggleAvailableColumnsVisibility});
            this.barManager1.MaxItemId = 33;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditStatusFilter,
            this.repositoryItemPopupContainerEditCompanyFilter});
            // 
            // fieldCalloutYear
            // 
            this.fieldCalloutYear.AreaIndex = 8;
            this.fieldCalloutYear.Caption = "Callout Year";
            this.fieldCalloutYear.ExpandedInFieldsGroup = false;
            this.fieldCalloutYear.FieldName = "CallOutDateTime";
            this.fieldCalloutYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldCalloutYear.Name = "fieldCalloutYear";
            this.fieldCalloutYear.UnboundFieldName = "fieldCalloutYear";
            // 
            // fieldCalloutQuarter
            // 
            this.fieldCalloutQuarter.AreaIndex = 1;
            this.fieldCalloutQuarter.Caption = "Callout Quarter";
            this.fieldCalloutQuarter.ExpandedInFieldsGroup = false;
            this.fieldCalloutQuarter.FieldName = "CallOutDateTime";
            this.fieldCalloutQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldCalloutQuarter.Name = "fieldCalloutQuarter";
            this.fieldCalloutQuarter.UnboundFieldName = "fieldCalloutQuarter";
            this.fieldCalloutQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldCalloutQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCalloutQuarter.Visible = false;
            // 
            // fieldCalloutMonth
            // 
            this.fieldCalloutMonth.AreaIndex = 2;
            this.fieldCalloutMonth.Caption = "Callout Month";
            this.fieldCalloutMonth.ExpandedInFieldsGroup = false;
            this.fieldCalloutMonth.FieldName = "CallOutDateTime";
            this.fieldCalloutMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldCalloutMonth.Name = "fieldCalloutMonth";
            this.fieldCalloutMonth.UnboundFieldName = "fieldCalloutMonth";
            this.fieldCalloutMonth.Visible = false;
            // 
            // fieldCalloutWeek
            // 
            this.fieldCalloutWeek.AreaIndex = 0;
            this.fieldCalloutWeek.Caption = "Callout Week";
            this.fieldCalloutWeek.ExpandedInFieldsGroup = false;
            this.fieldCalloutWeek.FieldName = "CallOutDateTime";
            this.fieldCalloutWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldCalloutWeek.Name = "fieldCalloutWeek";
            this.fieldCalloutWeek.UnboundFieldName = "fieldCalloutWeek";
            this.fieldCalloutWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldCalloutWeek.Visible = false;
            // 
            // fieldStartYear
            // 
            this.fieldStartYear.AreaIndex = 13;
            this.fieldStartYear.Caption = "Start Year";
            this.fieldStartYear.ExpandedInFieldsGroup = false;
            this.fieldStartYear.FieldName = "StartTime";
            this.fieldStartYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldStartYear.Name = "fieldStartYear";
            this.fieldStartYear.UnboundFieldName = "fieldStartYear";
            // 
            // fieldStartQuarter
            // 
            this.fieldStartQuarter.AreaIndex = 1;
            this.fieldStartQuarter.Caption = "Start Quarter";
            this.fieldStartQuarter.ExpandedInFieldsGroup = false;
            this.fieldStartQuarter.FieldName = "StartTime";
            this.fieldStartQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldStartQuarter.Name = "fieldStartQuarter";
            this.fieldStartQuarter.UnboundFieldName = "fieldStartQuarter";
            this.fieldStartQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldStartQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldStartQuarter.Visible = false;
            // 
            // fieldStartMonth
            // 
            this.fieldStartMonth.AreaIndex = 1;
            this.fieldStartMonth.Caption = "Start Month";
            this.fieldStartMonth.ExpandedInFieldsGroup = false;
            this.fieldStartMonth.FieldName = "StartTime";
            this.fieldStartMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldStartMonth.Name = "fieldStartMonth";
            this.fieldStartMonth.UnboundFieldName = "fieldStartMonth";
            this.fieldStartMonth.Visible = false;
            // 
            // fieldStartWeek
            // 
            this.fieldStartWeek.AreaIndex = 0;
            this.fieldStartWeek.Caption = "Start Week";
            this.fieldStartWeek.ExpandedInFieldsGroup = false;
            this.fieldStartWeek.FieldName = "StartTime";
            this.fieldStartWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldStartWeek.Name = "fieldStartWeek";
            this.fieldStartWeek.UnboundFieldName = "fieldStartWeek";
            this.fieldStartWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldStartWeek.Visible = false;
            // 
            // fieldCompletedYear
            // 
            this.fieldCompletedYear.AreaIndex = 15;
            this.fieldCompletedYear.Caption = "Completed Year";
            this.fieldCompletedYear.ExpandedInFieldsGroup = false;
            this.fieldCompletedYear.FieldName = "CompletedTime";
            this.fieldCompletedYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldCompletedYear.Name = "fieldCompletedYear";
            this.fieldCompletedYear.UnboundFieldName = "fieldCompletedYear";
            // 
            // fieldCompletedQuarter
            // 
            this.fieldCompletedQuarter.AreaIndex = 2;
            this.fieldCompletedQuarter.Caption = "Completed Quarter";
            this.fieldCompletedQuarter.ExpandedInFieldsGroup = false;
            this.fieldCompletedQuarter.FieldName = "CompletedTime";
            this.fieldCompletedQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldCompletedQuarter.Name = "fieldCompletedQuarter";
            this.fieldCompletedQuarter.UnboundFieldName = "fieldCompletedQuarter";
            this.fieldCompletedQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldCompletedQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCompletedQuarter.Visible = false;
            // 
            // fieldCompletedMonth
            // 
            this.fieldCompletedMonth.AreaIndex = 2;
            this.fieldCompletedMonth.Caption = "Completed Month";
            this.fieldCompletedMonth.ExpandedInFieldsGroup = false;
            this.fieldCompletedMonth.FieldName = "CompletedTime";
            this.fieldCompletedMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldCompletedMonth.Name = "fieldCompletedMonth";
            this.fieldCompletedMonth.UnboundFieldName = "fieldCompletedMonth";
            this.fieldCompletedMonth.Visible = false;
            // 
            // fieldCompletedWeek
            // 
            this.fieldCompletedWeek.AreaIndex = 0;
            this.fieldCompletedWeek.Caption = "Completed Week";
            this.fieldCompletedWeek.ExpandedInFieldsGroup = false;
            this.fieldCompletedWeek.FieldName = "CompletedTime";
            this.fieldCompletedWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldCompletedWeek.Name = "fieldCompletedWeek";
            this.fieldCompletedWeek.UnboundFieldName = "fieldCompletedWeek";
            this.fieldCompletedWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldCompletedWeek.Visible = false;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "ProjectManDataSet";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_Reports
            // 
            this.dataSet_AT_Reports.DataSetName = "DataSet_AT_Reports";
            this.dataSet_AT_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("eca7f263-f03a-476e-be2c-7e2164986cdc");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(434, 200);
            this.dockPanel1.Size = new System.Drawing.Size(434, 494);
            this.dockPanel1.Text = "Data Supply";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.standaloneBarDockControl1);
            this.dockPanel1_Container.Controls.Add(this.gridControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(428, 462);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(428, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp04324GCAnalysisGrittingCalloutsListBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 42);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEdit25KgBags,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditNumeric2DP});
            this.gridControl1.Size = new System.Drawing.Size(428, 420);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04324GCAnalysisGrittingCalloutsListBindingSource
            // 
            this.sp04324GCAnalysisGrittingCalloutsListBindingSource.DataMember = "sp04324_GC_Analysis_Gritting_Callouts_List";
            this.sp04324GCAnalysisGrittingCalloutsListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGrittingCallOutID,
            this.colSiteGrittingContractID,
            this.colSiteID,
            this.colSiteName,
            this.colClientID,
            this.colClientName,
            this.colCompanyID,
            this.colCompanyName,
            this.colSiteXCoordinate,
            this.colSiteYCoordinate,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colSiteTelephone,
            this.colSiteEmail,
            this.colClientsSiteCode,
            this.colSubContractorID,
            this.colSubContractorName,
            this.colSubContractorIsDirectLabour,
            this.colOriginalSubContractorID,
            this.colOriginalSubContarctorName,
            this.colReactive,
            this.colJobStatusID,
            this.colCalloutStatusDescription,
            this.colCalloutStatusOrder,
            this.colCallOutDateTime,
            this.colImportedWeatherForecastID,
            this.colTextSentTime,
            this.colSubContractorReceivedTime,
            this.colSubContractorRespondedTime,
            this.colSubContractorETA,
            this.colCompletedTime,
            this.colAuthorisedByStaffID,
            this.colAuthorisedByName,
            this.colVisitAborted,
            this.colAbortedReason,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishedLatitude,
            this.colFinishedLongitude,
            this.colClientPONumber,
            this.colSaltUsed,
            this.colSaltCost,
            this.colSaltSell,
            this.colSaltVatRate,
            this.colHoursWorked,
            this.colTeamHourlyRate,
            this.colTeamCharge,
            this.colLabourCost,
            this.colLabourVatRate,
            this.colOtherCost,
            this.colOtherSell,
            this.colTotalCost,
            this.colTotalSell,
            this.colClientInvoiceNumber,
            this.colSubContractorPaid,
            this.colGrittingInvoiceID,
            this.colRecordedByStaffID,
            this.colRecordedByName,
            this.colPaidByStaffID,
            this.colPaidByName,
            this.colDoNotPaySubContractor,
            this.colDoNotPaySubContractorReason,
            this.colGritSourceLocationID,
            this.colGritSourceLocationTypeID,
            this.colNoAccess,
            this.colSiteWeather,
            this.colSiteTemperature,
            this.colPdaID,
            this.colTeamPresent,
            this.colServiceFailure,
            this.colComments,
            this.colClientPrice,
            this.colGritJobTransferMethod,
            this.colSnowOnSite,
            this.colStartTime,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.colProfit,
            this.colMarkup,
            this.colClientPOID,
            this.colNonStandardCost,
            this.colNonStandardSell,
            this.colDoNotInvoiceClient,
            this.colDoNotInvoiceClientReason,
            this.colGritMobileTelephoneNumber,
            this.colClientSignatureFileName,
            this.colPPEWorn,
            this.colNoAccessAbortedRate,
            this.colTeamMembersPresent,
            this.colSnowInfo,
            this.colEveningRateModifier,
            this.colCompletedOnPDA,
            this.colJobSheetNumber,
            this.colFinanceInvoiceNumber,
            this.colCalloutInvoiced,
            this.colSiteManagerName,
            this.colSnowOnSiteOver5cm,
            this.colInternalRemarks,
            this.colPDACode});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 513, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCallOutDateTime, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colGrittingCallOutID
            // 
            this.colGrittingCallOutID.Caption = "Callout ID";
            this.colGrittingCallOutID.FieldName = "GrittingCallOutID";
            this.colGrittingCallOutID.Name = "colGrittingCallOutID";
            this.colGrittingCallOutID.OptionsColumn.AllowEdit = false;
            this.colGrittingCallOutID.OptionsColumn.AllowFocus = false;
            this.colGrittingCallOutID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteGrittingContractID
            // 
            this.colSiteGrittingContractID.Caption = "Site Gritting Contract ID";
            this.colSiteGrittingContractID.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID.Name = "colSiteGrittingContractID";
            this.colSiteGrittingContractID.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID.Width = 136;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 4;
            this.colSiteName.Width = 173;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 3;
            this.colClientName.Width = 108;
            // 
            // colCompanyID
            // 
            this.colCompanyID.Caption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 80;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 5;
            this.colCompanyName.Width = 107;
            // 
            // colSiteXCoordinate
            // 
            this.colSiteXCoordinate.Caption = "Site X Coord.";
            this.colSiteXCoordinate.FieldName = "SiteXCoordinate";
            this.colSiteXCoordinate.Name = "colSiteXCoordinate";
            this.colSiteXCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteXCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteXCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteXCoordinate.Width = 84;
            // 
            // colSiteYCoordinate
            // 
            this.colSiteYCoordinate.Caption = "Site Y Coord.";
            this.colSiteYCoordinate.FieldName = "SiteYCoordinate";
            this.colSiteYCoordinate.Name = "colSiteYCoordinate";
            this.colSiteYCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteYCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteYCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteYCoordinate.Width = 84;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Location X";
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 91;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Location Y";
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 91;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Site Telephone";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Visible = true;
            this.colSiteTelephone.VisibleIndex = 16;
            this.colSiteTelephone.Width = 92;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Site Email";
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.AllowEdit = false;
            this.colSiteEmail.OptionsColumn.AllowFocus = false;
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            this.colSiteEmail.Visible = true;
            this.colSiteEmail.VisibleIndex = 17;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Visible = true;
            this.colClientsSiteCode.VisibleIndex = 10;
            this.colClientsSiteCode.Width = 102;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Team Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 6;
            this.colSubContractorName.Width = 131;
            // 
            // colSubContractorIsDirectLabour
            // 
            this.colSubContractorIsDirectLabour.Caption = "Is Direct Labour";
            this.colSubContractorIsDirectLabour.FieldName = "SubContractorIsDirectLabour";
            this.colSubContractorIsDirectLabour.Name = "colSubContractorIsDirectLabour";
            this.colSubContractorIsDirectLabour.OptionsColumn.AllowEdit = false;
            this.colSubContractorIsDirectLabour.OptionsColumn.AllowFocus = false;
            this.colSubContractorIsDirectLabour.OptionsColumn.ReadOnly = true;
            this.colSubContractorIsDirectLabour.Visible = true;
            this.colSubContractorIsDirectLabour.VisibleIndex = 22;
            this.colSubContractorIsDirectLabour.Width = 97;
            // 
            // colOriginalSubContractorID
            // 
            this.colOriginalSubContractorID.Caption = "Original Team ID";
            this.colOriginalSubContractorID.FieldName = "OriginalSubContractorID";
            this.colOriginalSubContractorID.Name = "colOriginalSubContractorID";
            this.colOriginalSubContractorID.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorID.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorID.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorID.Width = 100;
            // 
            // colOriginalSubContarctorName
            // 
            this.colOriginalSubContarctorName.Caption = "Original Team Name";
            this.colOriginalSubContarctorName.FieldName = "OriginalSubContarctorName";
            this.colOriginalSubContarctorName.Name = "colOriginalSubContarctorName";
            this.colOriginalSubContarctorName.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContarctorName.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContarctorName.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContarctorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOriginalSubContarctorName.Visible = true;
            this.colOriginalSubContarctorName.VisibleIndex = 58;
            this.colOriginalSubContarctorName.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 2;
            this.colReactive.Width = 62;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 88;
            // 
            // colCalloutStatusDescription
            // 
            this.colCalloutStatusDescription.Caption = "Status";
            this.colCalloutStatusDescription.FieldName = "CalloutStatusDescription";
            this.colCalloutStatusDescription.Name = "colCalloutStatusDescription";
            this.colCalloutStatusDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCalloutStatusDescription.Visible = true;
            this.colCalloutStatusDescription.VisibleIndex = 1;
            this.colCalloutStatusDescription.Width = 250;
            // 
            // colCalloutStatusOrder
            // 
            this.colCalloutStatusOrder.Caption = "Status Order";
            this.colCalloutStatusOrder.FieldName = "CalloutStatusOrder";
            this.colCalloutStatusOrder.Name = "colCalloutStatusOrder";
            this.colCalloutStatusOrder.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusOrder.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusOrder.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusOrder.Width = 83;
            // 
            // colCallOutDateTime
            // 
            this.colCallOutDateTime.Caption = "Callout Date\\Time";
            this.colCallOutDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCallOutDateTime.FieldName = "CallOutDateTime";
            this.colCallOutDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCallOutDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCallOutDateTime.Name = "colCallOutDateTime";
            this.colCallOutDateTime.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCallOutDateTime.Visible = true;
            this.colCallOutDateTime.VisibleIndex = 0;
            this.colCallOutDateTime.Width = 117;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colImportedWeatherForecastID
            // 
            this.colImportedWeatherForecastID.Caption = "Imported Forecast ID";
            this.colImportedWeatherForecastID.FieldName = "ImportedWeatherForecastID";
            this.colImportedWeatherForecastID.Name = "colImportedWeatherForecastID";
            this.colImportedWeatherForecastID.OptionsColumn.AllowEdit = false;
            this.colImportedWeatherForecastID.OptionsColumn.AllowFocus = false;
            this.colImportedWeatherForecastID.OptionsColumn.ReadOnly = true;
            this.colImportedWeatherForecastID.Width = 124;
            // 
            // colTextSentTime
            // 
            this.colTextSentTime.Caption = "Job Sent Time";
            this.colTextSentTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colTextSentTime.FieldName = "TextSentTime";
            this.colTextSentTime.Name = "colTextSentTime";
            this.colTextSentTime.OptionsColumn.AllowEdit = false;
            this.colTextSentTime.OptionsColumn.AllowFocus = false;
            this.colTextSentTime.OptionsColumn.ReadOnly = true;
            this.colTextSentTime.Visible = true;
            this.colTextSentTime.VisibleIndex = 11;
            this.colTextSentTime.Width = 93;
            // 
            // colSubContractorReceivedTime
            // 
            this.colSubContractorReceivedTime.Caption = "Team Received Time";
            this.colSubContractorReceivedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSubContractorReceivedTime.FieldName = "SubContractorReceivedTime";
            this.colSubContractorReceivedTime.Name = "colSubContractorReceivedTime";
            this.colSubContractorReceivedTime.OptionsColumn.AllowEdit = false;
            this.colSubContractorReceivedTime.OptionsColumn.AllowFocus = false;
            this.colSubContractorReceivedTime.OptionsColumn.ReadOnly = true;
            this.colSubContractorReceivedTime.Visible = true;
            this.colSubContractorReceivedTime.VisibleIndex = 12;
            this.colSubContractorReceivedTime.Width = 119;
            // 
            // colSubContractorRespondedTime
            // 
            this.colSubContractorRespondedTime.Caption = "Team Responded Time";
            this.colSubContractorRespondedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSubContractorRespondedTime.FieldName = "SubContractorRespondedTime";
            this.colSubContractorRespondedTime.Name = "colSubContractorRespondedTime";
            this.colSubContractorRespondedTime.OptionsColumn.AllowEdit = false;
            this.colSubContractorRespondedTime.OptionsColumn.AllowFocus = false;
            this.colSubContractorRespondedTime.OptionsColumn.ReadOnly = true;
            this.colSubContractorRespondedTime.Visible = true;
            this.colSubContractorRespondedTime.VisibleIndex = 13;
            this.colSubContractorRespondedTime.Width = 129;
            // 
            // colSubContractorETA
            // 
            this.colSubContractorETA.Caption = "Team ETA";
            this.colSubContractorETA.FieldName = "SubContractorETA";
            this.colSubContractorETA.Name = "colSubContractorETA";
            this.colSubContractorETA.OptionsColumn.AllowEdit = false;
            this.colSubContractorETA.OptionsColumn.AllowFocus = false;
            this.colSubContractorETA.OptionsColumn.ReadOnly = true;
            this.colSubContractorETA.Visible = true;
            this.colSubContractorETA.VisibleIndex = 8;
            // 
            // colCompletedTime
            // 
            this.colCompletedTime.Caption = "Completed Time";
            this.colCompletedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCompletedTime.FieldName = "CompletedTime";
            this.colCompletedTime.Name = "colCompletedTime";
            this.colCompletedTime.OptionsColumn.AllowEdit = false;
            this.colCompletedTime.OptionsColumn.AllowFocus = false;
            this.colCompletedTime.OptionsColumn.ReadOnly = true;
            this.colCompletedTime.Visible = true;
            this.colCompletedTime.VisibleIndex = 15;
            this.colCompletedTime.Width = 97;
            // 
            // colAuthorisedByStaffID
            // 
            this.colAuthorisedByStaffID.Caption = "Authorised By ID";
            this.colAuthorisedByStaffID.FieldName = "AuthorisedByStaffID";
            this.colAuthorisedByStaffID.Name = "colAuthorisedByStaffID";
            this.colAuthorisedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByStaffID.Width = 102;
            // 
            // colAuthorisedByName
            // 
            this.colAuthorisedByName.Caption = "Authorised By";
            this.colAuthorisedByName.FieldName = "AuthorisedByName";
            this.colAuthorisedByName.Name = "colAuthorisedByName";
            this.colAuthorisedByName.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByName.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByName.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAuthorisedByName.Visible = true;
            this.colAuthorisedByName.VisibleIndex = 18;
            this.colAuthorisedByName.Width = 88;
            // 
            // colVisitAborted
            // 
            this.colVisitAborted.Caption = "Aborted";
            this.colVisitAborted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colVisitAborted.FieldName = "VisitAborted";
            this.colVisitAborted.Name = "colVisitAborted";
            this.colVisitAborted.OptionsColumn.AllowEdit = false;
            this.colVisitAborted.OptionsColumn.AllowFocus = false;
            this.colVisitAborted.OptionsColumn.ReadOnly = true;
            this.colVisitAborted.Visible = true;
            this.colVisitAborted.VisibleIndex = 19;
            // 
            // colAbortedReason
            // 
            this.colAbortedReason.Caption = "Aborted Reason";
            this.colAbortedReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colAbortedReason.FieldName = "AbortedReason";
            this.colAbortedReason.Name = "colAbortedReason";
            this.colAbortedReason.OptionsColumn.ReadOnly = true;
            this.colAbortedReason.Visible = true;
            this.colAbortedReason.VisibleIndex = 20;
            this.colAbortedReason.Width = 99;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Lat";
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Long";
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            // 
            // colFinishedLatitude
            // 
            this.colFinishedLatitude.Caption = "Finished Lat";
            this.colFinishedLatitude.FieldName = "FinishedLatitude";
            this.colFinishedLatitude.Name = "colFinishedLatitude";
            this.colFinishedLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishedLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishedLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishedLatitude.Width = 78;
            // 
            // colFinishedLongitude
            // 
            this.colFinishedLongitude.Caption = "Finished Long";
            this.colFinishedLongitude.FieldName = "FinishedLongitude";
            this.colFinishedLongitude.Name = "colFinishedLongitude";
            this.colFinishedLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishedLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishedLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishedLongitude.Width = 86;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "P.O. Number";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 21;
            this.colClientPONumber.Width = 83;
            // 
            // colSaltUsed
            // 
            this.colSaltUsed.Caption = "Salt Used";
            this.colSaltUsed.ColumnEdit = this.repositoryItemTextEdit25KgBags;
            this.colSaltUsed.FieldName = "SaltUsed";
            this.colSaltUsed.Name = "colSaltUsed";
            this.colSaltUsed.OptionsColumn.AllowEdit = false;
            this.colSaltUsed.OptionsColumn.AllowFocus = false;
            this.colSaltUsed.OptionsColumn.ReadOnly = true;
            this.colSaltUsed.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSaltUsed.Visible = true;
            this.colSaltUsed.VisibleIndex = 29;
            // 
            // repositoryItemTextEdit25KgBags
            // 
            this.repositoryItemTextEdit25KgBags.AutoHeight = false;
            this.repositoryItemTextEdit25KgBags.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25KgBags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KgBags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KgBags.Name = "repositoryItemTextEdit25KgBags";
            // 
            // colSaltCost
            // 
            this.colSaltCost.Caption = "Salt Cost";
            this.colSaltCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSaltCost.FieldName = "SaltCost";
            this.colSaltCost.Name = "colSaltCost";
            this.colSaltCost.OptionsColumn.AllowEdit = false;
            this.colSaltCost.OptionsColumn.AllowFocus = false;
            this.colSaltCost.OptionsColumn.ReadOnly = true;
            this.colSaltCost.Visible = true;
            this.colSaltCost.VisibleIndex = 30;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colSaltSell
            // 
            this.colSaltSell.Caption = "Salt Sell";
            this.colSaltSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSaltSell.FieldName = "SaltSell";
            this.colSaltSell.Name = "colSaltSell";
            this.colSaltSell.OptionsColumn.AllowEdit = false;
            this.colSaltSell.OptionsColumn.AllowFocus = false;
            this.colSaltSell.OptionsColumn.ReadOnly = true;
            this.colSaltSell.Visible = true;
            this.colSaltSell.VisibleIndex = 31;
            // 
            // colSaltVatRate
            // 
            this.colSaltVatRate.Caption = "Salt VAT Rate";
            this.colSaltVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSaltVatRate.FieldName = "SaltVatRate";
            this.colSaltVatRate.Name = "colSaltVatRate";
            this.colSaltVatRate.OptionsColumn.AllowEdit = false;
            this.colSaltVatRate.OptionsColumn.AllowFocus = false;
            this.colSaltVatRate.OptionsColumn.ReadOnly = true;
            this.colSaltVatRate.Visible = true;
            this.colSaltVatRate.VisibleIndex = 32;
            this.colSaltVatRate.Width = 87;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P2";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colHoursWorked
            // 
            this.colHoursWorked.Caption = "Hours Worked";
            this.colHoursWorked.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colHoursWorked.FieldName = "HoursWorked";
            this.colHoursWorked.Name = "colHoursWorked";
            this.colHoursWorked.OptionsColumn.AllowEdit = false;
            this.colHoursWorked.OptionsColumn.AllowFocus = false;
            this.colHoursWorked.OptionsColumn.ReadOnly = true;
            this.colHoursWorked.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHoursWorked.Visible = true;
            this.colHoursWorked.VisibleIndex = 23;
            this.colHoursWorked.Width = 89;
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // colTeamHourlyRate
            // 
            this.colTeamHourlyRate.Caption = "Hourly Rate";
            this.colTeamHourlyRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTeamHourlyRate.FieldName = "TeamHourlyRate";
            this.colTeamHourlyRate.Name = "colTeamHourlyRate";
            this.colTeamHourlyRate.OptionsColumn.AllowEdit = false;
            this.colTeamHourlyRate.OptionsColumn.AllowFocus = false;
            this.colTeamHourlyRate.OptionsColumn.ReadOnly = true;
            this.colTeamHourlyRate.Visible = true;
            this.colTeamHourlyRate.VisibleIndex = 24;
            this.colTeamHourlyRate.Width = 78;
            // 
            // colTeamCharge
            // 
            this.colTeamCharge.Caption = "Team Charge";
            this.colTeamCharge.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTeamCharge.FieldName = "TeamCharge";
            this.colTeamCharge.Name = "colTeamCharge";
            this.colTeamCharge.OptionsColumn.AllowEdit = false;
            this.colTeamCharge.OptionsColumn.AllowFocus = false;
            this.colTeamCharge.OptionsColumn.ReadOnly = true;
            this.colTeamCharge.Visible = true;
            this.colTeamCharge.VisibleIndex = 26;
            this.colTeamCharge.Width = 85;
            // 
            // colLabourCost
            // 
            this.colLabourCost.Caption = "Labour Cost";
            this.colLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colLabourCost.FieldName = "LabourCost";
            this.colLabourCost.Name = "colLabourCost";
            this.colLabourCost.OptionsColumn.AllowEdit = false;
            this.colLabourCost.OptionsColumn.AllowFocus = false;
            this.colLabourCost.OptionsColumn.ReadOnly = true;
            this.colLabourCost.Visible = true;
            this.colLabourCost.VisibleIndex = 27;
            this.colLabourCost.Width = 79;
            // 
            // colLabourVatRate
            // 
            this.colLabourVatRate.Caption = "Labour VAT Rate";
            this.colLabourVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colLabourVatRate.FieldName = "LabourVatRate";
            this.colLabourVatRate.Name = "colLabourVatRate";
            this.colLabourVatRate.OptionsColumn.AllowEdit = false;
            this.colLabourVatRate.OptionsColumn.AllowFocus = false;
            this.colLabourVatRate.OptionsColumn.ReadOnly = true;
            this.colLabourVatRate.Visible = true;
            this.colLabourVatRate.VisibleIndex = 28;
            this.colLabourVatRate.Width = 102;
            // 
            // colOtherCost
            // 
            this.colOtherCost.Caption = "Other Cost";
            this.colOtherCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherCost.FieldName = "OtherCost";
            this.colOtherCost.Name = "colOtherCost";
            this.colOtherCost.OptionsColumn.AllowEdit = false;
            this.colOtherCost.OptionsColumn.AllowFocus = false;
            this.colOtherCost.OptionsColumn.ReadOnly = true;
            this.colOtherCost.Visible = true;
            this.colOtherCost.VisibleIndex = 33;
            // 
            // colOtherSell
            // 
            this.colOtherSell.Caption = "Other Sell";
            this.colOtherSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherSell.FieldName = "OtherSell";
            this.colOtherSell.Name = "colOtherSell";
            this.colOtherSell.OptionsColumn.AllowEdit = false;
            this.colOtherSell.OptionsColumn.AllowFocus = false;
            this.colOtherSell.OptionsColumn.ReadOnly = true;
            this.colOtherSell.Visible = true;
            this.colOtherSell.VisibleIndex = 34;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 35;
            // 
            // colTotalSell
            // 
            this.colTotalSell.Caption = "Total Sell";
            this.colTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalSell.FieldName = "TotalSell";
            this.colTotalSell.Name = "colTotalSell";
            this.colTotalSell.OptionsColumn.AllowEdit = false;
            this.colTotalSell.OptionsColumn.AllowFocus = false;
            this.colTotalSell.OptionsColumn.ReadOnly = true;
            this.colTotalSell.Visible = true;
            this.colTotalSell.VisibleIndex = 37;
            // 
            // colClientInvoiceNumber
            // 
            this.colClientInvoiceNumber.Caption = "Invoice Number";
            this.colClientInvoiceNumber.FieldName = "ClientInvoiceNumber";
            this.colClientInvoiceNumber.Name = "colClientInvoiceNumber";
            this.colClientInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceNumber.Visible = true;
            this.colClientInvoiceNumber.VisibleIndex = 42;
            this.colClientInvoiceNumber.Width = 96;
            // 
            // colSubContractorPaid
            // 
            this.colSubContractorPaid.Caption = "Team Paid";
            this.colSubContractorPaid.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSubContractorPaid.FieldName = "SubContractorPaid";
            this.colSubContractorPaid.Name = "colSubContractorPaid";
            this.colSubContractorPaid.OptionsColumn.AllowEdit = false;
            this.colSubContractorPaid.OptionsColumn.AllowFocus = false;
            this.colSubContractorPaid.OptionsColumn.ReadOnly = true;
            this.colSubContractorPaid.Visible = true;
            this.colSubContractorPaid.VisibleIndex = 44;
            // 
            // colGrittingInvoiceID
            // 
            this.colGrittingInvoiceID.Caption = "Gritting Invoice ID";
            this.colGrittingInvoiceID.FieldName = "GrittingInvoiceID";
            this.colGrittingInvoiceID.Name = "colGrittingInvoiceID";
            this.colGrittingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colGrittingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colGrittingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colGrittingInvoiceID.Width = 108;
            // 
            // colRecordedByStaffID
            // 
            this.colRecordedByStaffID.Caption = "Recorded By Staff ID";
            this.colRecordedByStaffID.FieldName = "RecordedByStaffID";
            this.colRecordedByStaffID.Name = "colRecordedByStaffID";
            this.colRecordedByStaffID.OptionsColumn.AllowEdit = false;
            this.colRecordedByStaffID.OptionsColumn.AllowFocus = false;
            this.colRecordedByStaffID.OptionsColumn.ReadOnly = true;
            this.colRecordedByStaffID.Width = 123;
            // 
            // colRecordedByName
            // 
            this.colRecordedByName.Caption = "Recorded By";
            this.colRecordedByName.FieldName = "RecordedByName";
            this.colRecordedByName.Name = "colRecordedByName";
            this.colRecordedByName.OptionsColumn.AllowEdit = false;
            this.colRecordedByName.OptionsColumn.AllowFocus = false;
            this.colRecordedByName.OptionsColumn.ReadOnly = true;
            this.colRecordedByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRecordedByName.Visible = true;
            this.colRecordedByName.VisibleIndex = 45;
            this.colRecordedByName.Width = 118;
            // 
            // colPaidByStaffID
            // 
            this.colPaidByStaffID.Caption = "Paid By Staff ID";
            this.colPaidByStaffID.FieldName = "PaidByStaffID";
            this.colPaidByStaffID.Name = "colPaidByStaffID";
            this.colPaidByStaffID.OptionsColumn.AllowEdit = false;
            this.colPaidByStaffID.OptionsColumn.AllowFocus = false;
            this.colPaidByStaffID.OptionsColumn.ReadOnly = true;
            this.colPaidByStaffID.Width = 97;
            // 
            // colPaidByName
            // 
            this.colPaidByName.Caption = "Paid By";
            this.colPaidByName.FieldName = "PaidByName";
            this.colPaidByName.Name = "colPaidByName";
            this.colPaidByName.OptionsColumn.AllowEdit = false;
            this.colPaidByName.OptionsColumn.AllowFocus = false;
            this.colPaidByName.OptionsColumn.ReadOnly = true;
            this.colPaidByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPaidByName.Visible = true;
            this.colPaidByName.VisibleIndex = 46;
            this.colPaidByName.Width = 93;
            // 
            // colDoNotPaySubContractor
            // 
            this.colDoNotPaySubContractor.Caption = "Don\'t Pay Team";
            this.colDoNotPaySubContractor.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPaySubContractor.FieldName = "DoNotPaySubContractor";
            this.colDoNotPaySubContractor.Name = "colDoNotPaySubContractor";
            this.colDoNotPaySubContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPaySubContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPaySubContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractor.Visible = true;
            this.colDoNotPaySubContractor.VisibleIndex = 47;
            this.colDoNotPaySubContractor.Width = 96;
            // 
            // colDoNotPaySubContractorReason
            // 
            this.colDoNotPaySubContractorReason.Caption = "Don\'t Pay Team Reason";
            this.colDoNotPaySubContractorReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDoNotPaySubContractorReason.FieldName = "DoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.Name = "colDoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.OptionsColumn.AllowEdit = false;
            this.colDoNotPaySubContractorReason.OptionsColumn.AllowFocus = false;
            this.colDoNotPaySubContractorReason.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractorReason.Visible = true;
            this.colDoNotPaySubContractorReason.VisibleIndex = 48;
            this.colDoNotPaySubContractorReason.Width = 135;
            // 
            // colGritSourceLocationID
            // 
            this.colGritSourceLocationID.Caption = "Grit Source Location ID";
            this.colGritSourceLocationID.FieldName = "GritSourceLocationID";
            this.colGritSourceLocationID.Name = "colGritSourceLocationID";
            this.colGritSourceLocationID.OptionsColumn.AllowEdit = false;
            this.colGritSourceLocationID.OptionsColumn.AllowFocus = false;
            this.colGritSourceLocationID.OptionsColumn.ReadOnly = true;
            this.colGritSourceLocationID.Width = 131;
            // 
            // colGritSourceLocationTypeID
            // 
            this.colGritSourceLocationTypeID.Caption = "Grit Source Location Type ID";
            this.colGritSourceLocationTypeID.FieldName = "GritSourceLocationTypeID";
            this.colGritSourceLocationTypeID.Name = "colGritSourceLocationTypeID";
            this.colGritSourceLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colGritSourceLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colGritSourceLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colGritSourceLocationTypeID.Width = 158;
            // 
            // colNoAccess
            // 
            this.colNoAccess.Caption = "No Access";
            this.colNoAccess.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoAccess.FieldName = "NoAccess";
            this.colNoAccess.Name = "colNoAccess";
            this.colNoAccess.OptionsColumn.AllowEdit = false;
            this.colNoAccess.OptionsColumn.AllowFocus = false;
            this.colNoAccess.OptionsColumn.ReadOnly = true;
            this.colNoAccess.Visible = true;
            this.colNoAccess.VisibleIndex = 49;
            // 
            // colSiteWeather
            // 
            this.colSiteWeather.Caption = "Site Weather";
            this.colSiteWeather.FieldName = "SiteWeather";
            this.colSiteWeather.Name = "colSiteWeather";
            this.colSiteWeather.OptionsColumn.AllowEdit = false;
            this.colSiteWeather.OptionsColumn.AllowFocus = false;
            this.colSiteWeather.OptionsColumn.ReadOnly = true;
            this.colSiteWeather.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteWeather.Visible = true;
            this.colSiteWeather.VisibleIndex = 50;
            this.colSiteWeather.Width = 84;
            // 
            // colSiteTemperature
            // 
            this.colSiteTemperature.Caption = "Site Temperature";
            this.colSiteTemperature.FieldName = "SiteTemperature";
            this.colSiteTemperature.Name = "colSiteTemperature";
            this.colSiteTemperature.OptionsColumn.AllowEdit = false;
            this.colSiteTemperature.OptionsColumn.AllowFocus = false;
            this.colSiteTemperature.OptionsColumn.ReadOnly = true;
            this.colSiteTemperature.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteTemperature.Visible = true;
            this.colSiteTemperature.VisibleIndex = 51;
            this.colSiteTemperature.Width = 104;
            // 
            // colPdaID
            // 
            this.colPdaID.Caption = "Device ID";
            this.colPdaID.FieldName = "PdaID";
            this.colPdaID.Name = "colPdaID";
            this.colPdaID.OptionsColumn.AllowEdit = false;
            this.colPdaID.OptionsColumn.AllowFocus = false;
            this.colPdaID.OptionsColumn.ReadOnly = true;
            this.colPdaID.Visible = true;
            this.colPdaID.VisibleIndex = 56;
            // 
            // colTeamPresent
            // 
            this.colTeamPresent.Caption = "Team Present";
            this.colTeamPresent.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTeamPresent.FieldName = "TeamPresent";
            this.colTeamPresent.Name = "colTeamPresent";
            this.colTeamPresent.OptionsColumn.AllowEdit = false;
            this.colTeamPresent.OptionsColumn.AllowFocus = false;
            this.colTeamPresent.OptionsColumn.ReadOnly = true;
            this.colTeamPresent.Visible = true;
            this.colTeamPresent.VisibleIndex = 7;
            this.colTeamPresent.Width = 87;
            // 
            // colServiceFailure
            // 
            this.colServiceFailure.Caption = "Service Failure";
            this.colServiceFailure.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colServiceFailure.FieldName = "ServiceFailure";
            this.colServiceFailure.Name = "colServiceFailure";
            this.colServiceFailure.OptionsColumn.AllowEdit = false;
            this.colServiceFailure.OptionsColumn.AllowFocus = false;
            this.colServiceFailure.OptionsColumn.ReadOnly = true;
            this.colServiceFailure.Visible = true;
            this.colServiceFailure.VisibleIndex = 57;
            this.colServiceFailure.Width = 91;
            // 
            // colComments
            // 
            this.colComments.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colComments.FieldName = "Comments";
            this.colComments.Name = "colComments";
            this.colComments.OptionsColumn.ReadOnly = true;
            this.colComments.Visible = true;
            this.colComments.VisibleIndex = 60;
            // 
            // colClientPrice
            // 
            this.colClientPrice.Caption = "Client Price";
            this.colClientPrice.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colClientPrice.FieldName = "ClientPrice";
            this.colClientPrice.Name = "colClientPrice";
            this.colClientPrice.OptionsColumn.AllowEdit = false;
            this.colClientPrice.OptionsColumn.AllowFocus = false;
            this.colClientPrice.OptionsColumn.ReadOnly = true;
            this.colClientPrice.Visible = true;
            this.colClientPrice.VisibleIndex = 36;
            // 
            // colGritJobTransferMethod
            // 
            this.colGritJobTransferMethod.Caption = "Transfer Method";
            this.colGritJobTransferMethod.FieldName = "GritJobTransferMethod";
            this.colGritJobTransferMethod.Name = "colGritJobTransferMethod";
            this.colGritJobTransferMethod.OptionsColumn.AllowEdit = false;
            this.colGritJobTransferMethod.OptionsColumn.AllowFocus = false;
            this.colGritJobTransferMethod.OptionsColumn.ReadOnly = true;
            this.colGritJobTransferMethod.Visible = true;
            this.colGritJobTransferMethod.VisibleIndex = 9;
            this.colGritJobTransferMethod.Width = 101;
            // 
            // colSnowOnSite
            // 
            this.colSnowOnSite.Caption = "Snow On Site";
            this.colSnowOnSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSnowOnSite.FieldName = "SnowOnSite";
            this.colSnowOnSite.Name = "colSnowOnSite";
            this.colSnowOnSite.OptionsColumn.AllowEdit = false;
            this.colSnowOnSite.OptionsColumn.AllowFocus = false;
            this.colSnowOnSite.OptionsColumn.ReadOnly = true;
            this.colSnowOnSite.Visible = true;
            this.colSnowOnSite.VisibleIndex = 52;
            this.colSnowOnSite.Width = 85;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 14;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "<b>Calculated 1</b>";
            this.gridColumn1.FieldName = "Calculated1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.ShowUnboundExpressionMenu = true;
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 68;
            this.gridColumn1.Width = 90;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "<b>Calculated 2</b>";
            this.gridColumn2.FieldName = "Calculated2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.ShowUnboundExpressionMenu = true;
            this.gridColumn2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 69;
            this.gridColumn2.Width = 90;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "<b>Calculated 3</b>";
            this.gridColumn3.FieldName = "Calculated3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.ShowUnboundExpressionMenu = true;
            this.gridColumn3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 70;
            this.gridColumn3.Width = 90;
            // 
            // colProfit
            // 
            this.colProfit.Caption = "Profit";
            this.colProfit.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colProfit.FieldName = "Profit";
            this.colProfit.Name = "colProfit";
            this.colProfit.OptionsColumn.AllowEdit = false;
            this.colProfit.OptionsColumn.AllowFocus = false;
            this.colProfit.OptionsColumn.ReadOnly = true;
            this.colProfit.Visible = true;
            this.colProfit.VisibleIndex = 38;
            // 
            // colMarkup
            // 
            this.colMarkup.Caption = "Markup";
            this.colMarkup.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colMarkup.FieldName = "Markup";
            this.colMarkup.Name = "colMarkup";
            this.colMarkup.OptionsColumn.AllowEdit = false;
            this.colMarkup.OptionsColumn.AllowFocus = false;
            this.colMarkup.OptionsColumn.ReadOnly = true;
            this.colMarkup.Visible = true;
            this.colMarkup.VisibleIndex = 39;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "P.O. Linked ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 90;
            // 
            // colNonStandardCost
            // 
            this.colNonStandardCost.Caption = "Non-Standard Cost";
            this.colNonStandardCost.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardCost.FieldName = "NonStandardCost";
            this.colNonStandardCost.Name = "colNonStandardCost";
            this.colNonStandardCost.OptionsColumn.AllowEdit = false;
            this.colNonStandardCost.OptionsColumn.AllowFocus = false;
            this.colNonStandardCost.OptionsColumn.ReadOnly = true;
            this.colNonStandardCost.Visible = true;
            this.colNonStandardCost.VisibleIndex = 61;
            this.colNonStandardCost.Width = 113;
            // 
            // colNonStandardSell
            // 
            this.colNonStandardSell.Caption = "Non-Standard Sell";
            this.colNonStandardSell.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardSell.FieldName = "NonStandardSell";
            this.colNonStandardSell.Name = "colNonStandardSell";
            this.colNonStandardSell.OptionsColumn.AllowEdit = false;
            this.colNonStandardSell.OptionsColumn.AllowFocus = false;
            this.colNonStandardSell.OptionsColumn.ReadOnly = true;
            this.colNonStandardSell.Visible = true;
            this.colNonStandardSell.VisibleIndex = 62;
            this.colNonStandardSell.Width = 107;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 40;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colDoNotInvoiceClientReason
            // 
            this.colDoNotInvoiceClientReason.Caption = "Do Not Invoice Client Reason";
            this.colDoNotInvoiceClientReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDoNotInvoiceClientReason.FieldName = "DoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.Name = "colDoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClientReason.Width = 161;
            // 
            // colGritMobileTelephoneNumber
            // 
            this.colGritMobileTelephoneNumber.Caption = "Grit Mobile Telephone";
            this.colGritMobileTelephoneNumber.FieldName = "GritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.Name = "colGritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.OptionsColumn.ReadOnly = true;
            this.colGritMobileTelephoneNumber.Visible = true;
            this.colGritMobileTelephoneNumber.VisibleIndex = 55;
            this.colGritMobileTelephoneNumber.Width = 124;
            // 
            // colClientSignatureFileName
            // 
            this.colClientSignatureFileName.Caption = "Client Signature";
            this.colClientSignatureFileName.FieldName = "ClientSignatureFileName";
            this.colClientSignatureFileName.Name = "colClientSignatureFileName";
            this.colClientSignatureFileName.OptionsColumn.ReadOnly = true;
            this.colClientSignatureFileName.Visible = true;
            this.colClientSignatureFileName.VisibleIndex = 63;
            this.colClientSignatureFileName.Width = 97;
            // 
            // colPPEWorn
            // 
            this.colPPEWorn.Caption = "PPE Worn";
            this.colPPEWorn.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPPEWorn.FieldName = "PPEWorn";
            this.colPPEWorn.Name = "colPPEWorn";
            this.colPPEWorn.OptionsColumn.AllowEdit = false;
            this.colPPEWorn.OptionsColumn.AllowFocus = false;
            this.colPPEWorn.OptionsColumn.ReadOnly = true;
            // 
            // colNoAccessAbortedRate
            // 
            this.colNoAccessAbortedRate.Caption = "No Access\\Aborted Rate";
            this.colNoAccessAbortedRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colNoAccessAbortedRate.FieldName = "NoAccessAbortedRate";
            this.colNoAccessAbortedRate.Name = "colNoAccessAbortedRate";
            this.colNoAccessAbortedRate.OptionsColumn.AllowEdit = false;
            this.colNoAccessAbortedRate.OptionsColumn.AllowFocus = false;
            this.colNoAccessAbortedRate.OptionsColumn.ReadOnly = true;
            this.colNoAccessAbortedRate.Visible = true;
            this.colNoAccessAbortedRate.VisibleIndex = 25;
            this.colNoAccessAbortedRate.Width = 139;
            // 
            // colTeamMembersPresent
            // 
            this.colTeamMembersPresent.Caption = "Team Members Present";
            this.colTeamMembersPresent.FieldName = "TeamMembersPresent";
            this.colTeamMembersPresent.Name = "colTeamMembersPresent";
            this.colTeamMembersPresent.OptionsColumn.ReadOnly = true;
            this.colTeamMembersPresent.Visible = true;
            this.colTeamMembersPresent.VisibleIndex = 59;
            this.colTeamMembersPresent.Width = 133;
            // 
            // colSnowInfo
            // 
            this.colSnowInfo.Caption = "Snow Info";
            this.colSnowInfo.FieldName = "SnowInfo";
            this.colSnowInfo.Name = "colSnowInfo";
            this.colSnowInfo.OptionsColumn.ReadOnly = true;
            this.colSnowInfo.Visible = true;
            this.colSnowInfo.VisibleIndex = 54;
            // 
            // colEveningRateModifier
            // 
            this.colEveningRateModifier.Caption = "Evening Rate Modifier";
            this.colEveningRateModifier.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEveningRateModifier.FieldName = "EveningRateModifier";
            this.colEveningRateModifier.Name = "colEveningRateModifier";
            this.colEveningRateModifier.OptionsColumn.AllowEdit = false;
            this.colEveningRateModifier.OptionsColumn.AllowFocus = false;
            this.colEveningRateModifier.OptionsColumn.ReadOnly = true;
            this.colEveningRateModifier.Width = 126;
            // 
            // colCompletedOnPDA
            // 
            this.colCompletedOnPDA.Caption = "Completed on Device";
            this.colCompletedOnPDA.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCompletedOnPDA.FieldName = "CompletedOnPDA";
            this.colCompletedOnPDA.Name = "colCompletedOnPDA";
            this.colCompletedOnPDA.OptionsColumn.AllowEdit = false;
            this.colCompletedOnPDA.OptionsColumn.AllowFocus = false;
            this.colCompletedOnPDA.OptionsColumn.ReadOnly = true;
            this.colCompletedOnPDA.Visible = true;
            this.colCompletedOnPDA.VisibleIndex = 64;
            this.colCompletedOnPDA.Width = 110;
            // 
            // colJobSheetNumber
            // 
            this.colJobSheetNumber.Caption = "Job Sheet Number";
            this.colJobSheetNumber.FieldName = "JobSheetNumber";
            this.colJobSheetNumber.Name = "colJobSheetNumber";
            this.colJobSheetNumber.OptionsColumn.AllowEdit = false;
            this.colJobSheetNumber.OptionsColumn.AllowFocus = false;
            this.colJobSheetNumber.OptionsColumn.ReadOnly = true;
            this.colJobSheetNumber.Visible = true;
            this.colJobSheetNumber.VisibleIndex = 65;
            this.colJobSheetNumber.Width = 109;
            // 
            // colFinanceInvoiceNumber
            // 
            this.colFinanceInvoiceNumber.Caption = "Finance Invoice No";
            this.colFinanceInvoiceNumber.FieldName = "FinanceInvoiceNumber";
            this.colFinanceInvoiceNumber.Name = "colFinanceInvoiceNumber";
            this.colFinanceInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colFinanceInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colFinanceInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colFinanceInvoiceNumber.Visible = true;
            this.colFinanceInvoiceNumber.VisibleIndex = 43;
            this.colFinanceInvoiceNumber.Width = 112;
            // 
            // colCalloutInvoiced
            // 
            this.colCalloutInvoiced.Caption = "Invoiced";
            this.colCalloutInvoiced.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCalloutInvoiced.FieldName = "CalloutInvoiced";
            this.colCalloutInvoiced.Name = "colCalloutInvoiced";
            this.colCalloutInvoiced.OptionsColumn.AllowEdit = false;
            this.colCalloutInvoiced.OptionsColumn.AllowFocus = false;
            this.colCalloutInvoiced.OptionsColumn.ReadOnly = true;
            this.colCalloutInvoiced.Visible = true;
            this.colCalloutInvoiced.VisibleIndex = 41;
            this.colCalloutInvoiced.Width = 62;
            // 
            // colSiteManagerName
            // 
            this.colSiteManagerName.Caption = "Site Manager";
            this.colSiteManagerName.FieldName = "SiteManagerName";
            this.colSiteManagerName.Name = "colSiteManagerName";
            this.colSiteManagerName.OptionsColumn.AllowEdit = false;
            this.colSiteManagerName.OptionsColumn.AllowFocus = false;
            this.colSiteManagerName.OptionsColumn.ReadOnly = true;
            this.colSiteManagerName.Width = 84;
            // 
            // colSnowOnSiteOver5cm
            // 
            this.colSnowOnSiteOver5cm.Caption = "Snow Over 5 cm";
            this.colSnowOnSiteOver5cm.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSnowOnSiteOver5cm.FieldName = "SnowOnSiteOver5cm";
            this.colSnowOnSiteOver5cm.Name = "colSnowOnSiteOver5cm";
            this.colSnowOnSiteOver5cm.OptionsColumn.AllowEdit = false;
            this.colSnowOnSiteOver5cm.OptionsColumn.AllowFocus = false;
            this.colSnowOnSiteOver5cm.OptionsColumn.ReadOnly = true;
            this.colSnowOnSiteOver5cm.Visible = true;
            this.colSnowOnSiteOver5cm.VisibleIndex = 53;
            this.colSnowOnSiteOver5cm.Width = 99;
            // 
            // colInternalRemarks
            // 
            this.colInternalRemarks.Caption = "Internal Remarks";
            this.colInternalRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colInternalRemarks.FieldName = "InternalRemarks";
            this.colInternalRemarks.Name = "colInternalRemarks";
            this.colInternalRemarks.OptionsColumn.AllowEdit = false;
            this.colInternalRemarks.OptionsColumn.AllowFocus = false;
            this.colInternalRemarks.OptionsColumn.ReadOnly = true;
            this.colInternalRemarks.Visible = true;
            this.colInternalRemarks.VisibleIndex = 66;
            this.colInternalRemarks.Width = 103;
            // 
            // colPDACode
            // 
            this.colPDACode.Caption = "Device Code";
            this.colPDACode.FieldName = "PDACode";
            this.colPDACode.Name = "colPDACode";
            this.colPDACode.OptionsColumn.AllowEdit = false;
            this.colPDACode.OptionsColumn.AllowFocus = false;
            this.colPDACode.OptionsColumn.ReadOnly = true;
            this.colPDACode.Visible = true;
            this.colPDACode.VisibleIndex = 67;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(434, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlCompanies);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlStatuses);
            this.splitContainerControl1.Panel1.Controls.Add(this.pivotGridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.chartControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(859, 494);
            this.splitContainerControl1.SplitterPosition = 388;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl2";
            // 
            // popupContainerControlCompanies
            // 
            this.popupContainerControlCompanies.Controls.Add(this.btnCompanyFilterOK);
            this.popupContainerControlCompanies.Controls.Add(this.gridControl5);
            this.popupContainerControlCompanies.Location = new System.Drawing.Point(363, 215);
            this.popupContainerControlCompanies.Name = "popupContainerControlCompanies";
            this.popupContainerControlCompanies.Size = new System.Drawing.Size(189, 163);
            this.popupContainerControlCompanies.TabIndex = 35;
            // 
            // btnCompanyFilterOK
            // 
            this.btnCompanyFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCompanyFilterOK.Location = new System.Drawing.Point(3, 138);
            this.btnCompanyFilterOK.Name = "btnCompanyFilterOK";
            this.btnCompanyFilterOK.Size = new System.Drawing.Size(77, 22);
            this.btnCompanyFilterOK.TabIndex = 18;
            this.btnCompanyFilterOK.Text = "OK";
            this.btnCompanyFilterOK.Click += new System.EventHandler(this.btnCompanyFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp04237GCCompanyFilterListBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(183, 133);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04237GCCompanyFilterListBindingSource
            // 
            this.sp04237GCCompanyFilterListBindingSource.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.colCompanyCode,
            this.colCompanyOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Company ID";
            this.gridColumn5.FieldName = "CompanyID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 80;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Company Name";
            this.gridColumn6.FieldName = "CompanyName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 152;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyOrder
            // 
            this.colCompanyOrder.Caption = "Order";
            this.colCompanyOrder.FieldName = "CompanyOrder";
            this.colCompanyOrder.Name = "colCompanyOrder";
            this.colCompanyOrder.OptionsColumn.AllowEdit = false;
            this.colCompanyOrder.OptionsColumn.AllowFocus = false;
            this.colCompanyOrder.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerControlStatuses
            // 
            this.popupContainerControlStatuses.Controls.Add(this.btnStatusFilterOK2);
            this.popupContainerControlStatuses.Controls.Add(this.groupControl2);
            this.popupContainerControlStatuses.Controls.Add(this.gridControl3);
            this.popupContainerControlStatuses.Location = new System.Drawing.Point(33, 97);
            this.popupContainerControlStatuses.Name = "popupContainerControlStatuses";
            this.popupContainerControlStatuses.Size = new System.Drawing.Size(312, 268);
            this.popupContainerControlStatuses.TabIndex = 15;
            // 
            // btnStatusFilterOK2
            // 
            this.btnStatusFilterOK2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStatusFilterOK2.Location = new System.Drawing.Point(3, 243);
            this.btnStatusFilterOK2.Name = "btnStatusFilterOK2";
            this.btnStatusFilterOK2.Size = new System.Drawing.Size(77, 22);
            this.btnStatusFilterOK2.TabIndex = 19;
            this.btnStatusFilterOK2.Text = "OK";
            this.btnStatusFilterOK2.Click += new System.EventHandler(this.btnStatusFilterOK);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.deFromDate);
            this.groupControl2.Controls.Add(this.deToDate);
            this.groupControl2.Location = new System.Drawing.Point(3, 186);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(305, 51);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "Date Filter";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(170, 28);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "From:";
            // 
            // deFromDate
            // 
            this.deFromDate.EditValue = null;
            this.deFromDate.Location = new System.Drawing.Point(41, 25);
            this.deFromDate.Name = "deFromDate";
            this.deFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deFromDate.Properties.NullText = "Not Used";
            this.deFromDate.Size = new System.Drawing.Size(108, 20);
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "From Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "All gritting and snow clearance records with a <b>Creation Date</b> which fall be" +
    "tween the From Date and To Date will be returned.\r\n";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.deFromDate.SuperTip = superToolTip1;
            this.deFromDate.TabIndex = 10;
            // 
            // deToDate
            // 
            this.deToDate.EditValue = null;
            this.deToDate.Location = new System.Drawing.Point(192, 25);
            this.deToDate.Name = "deToDate";
            this.deToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deToDate.Properties.NullText = "Not Used";
            this.deToDate.Size = new System.Drawing.Size(108, 20);
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "To Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "All gritting and snow clearance records with a <b>Creation Date</b> which fall be" +
    "tween the From Date and To Date will be returned.\r\n";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.deToDate.SuperTip = superToolTip2;
            this.deToDate.TabIndex = 7;
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp04001GCJobCallOutStatusesBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl3.Size = new System.Drawing.Size(305, 179);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp04001GCJobCallOutStatusesBindingSource
            // 
            this.sp04001GCJobCallOutStatusesBindingSource.DataMember = "sp04001_GC_Job_CallOut_Statuses";
            this.sp04001GCJobCallOutStatusesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colValue,
            this.colOrder});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Status";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 278;
            // 
            // colValue
            // 
            this.colValue.Caption = "Status ID";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.DataSource = this.sp04325GCAnalysisGrittingCalloutsForAnalysisBindingSource;
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldSiteGrittingContractID1,
            this.fieldSiteName1,
            this.fieldClientName1,
            this.fieldCompanyName1,
            this.fieldClientsSiteCode1,
            this.fieldSubContractorName1,
            this.fieldSubContractorIsDirectLabour1,
            this.fieldOriginalSubContarctorName1,
            this.fieldReactive1,
            this.fieldCalloutStatusDescription1,
            this.fieldCalloutStatusOrder1,
            this.fieldCallOutDateTime1,
            this.fieldImportedWeatherForecastID1,
            this.fieldTextSentTime1,
            this.fieldSubContractorReceivedTime1,
            this.fieldSubContractorRespondedTime1,
            this.fieldSubContractorETA1,
            this.fieldCompletedTime1,
            this.fieldAuthorisedByName1,
            this.fieldVisitAborted1,
            this.fieldAbortedReason1,
            this.fieldStartLatitude1,
            this.fieldStartLongitude1,
            this.fieldFinishedLatitude1,
            this.fieldFinishedLongitude1,
            this.fieldClientPONumber1,
            this.fieldSaltUsed1,
            this.fieldSaltCost1,
            this.fieldSaltSell1,
            this.fieldSaltVatRate1,
            this.fieldHoursWorked1,
            this.fieldTeamHourlyRate1,
            this.fieldTeamCharge1,
            this.fieldLabourCost1,
            this.fieldLabourVatRate1,
            this.fieldOtherCost1,
            this.fieldOtherSell1,
            this.fieldTotalCost1,
            this.fieldTotalSell1,
            this.fieldClientInvoiceNumber1,
            this.fieldSubContractorPaid1,
            this.fieldGrittingInvoiceID1,
            this.fieldRecordedByName1,
            this.fieldPaidByName1,
            this.fieldDoNotPaySubContractor1,
            this.fieldDoNotPaySubContractorReason1,
            this.fieldDoNotInvoiceClient1,
            this.fieldDoNotInvoiceClientReason1,
            this.fieldNoAccess1,
            this.fieldSiteWeather1,
            this.fieldSiteTemperature1,
            this.fieldPdaID1,
            this.fieldTeamPresent1,
            this.fieldServiceFailure1,
            this.fieldComments1,
            this.fieldClientPrice1,
            this.fieldSnowOnSite1,
            this.fieldGritJobTransferMethod1,
            this.fieldStartTime1,
            this.fieldProfit1,
            this.fieldMarkup1,
            this.fieldClientPOID1,
            this.fieldNonStandardCost1,
            this.fieldNonStandardSell1,
            this.fieldPPEWorn1,
            this.fieldNoAccessAbortedRate1,
            this.fieldTeamMembersPresent1,
            this.fieldSnowInfo1,
            this.fieldEveningRateModifier1,
            this.fieldJobSheetNumber1,
            this.fieldCompletedOnPDA1,
            this.fieldFinanceInvoiceNumber1,
            this.fieldCalloutCount1,
            this.fieldCalloutYear,
            this.fieldCalloutQuarter,
            this.fieldCalloutMonth,
            this.fieldCalloutWeek,
            this.fieldStartYear,
            this.fieldStartQuarter,
            this.fieldStartMonth,
            this.fieldStartWeek,
            this.fieldCompletedYear,
            this.fieldCompletedQuarter,
            this.fieldCompletedMonth,
            this.fieldCompletedWeek,
            this.fieldOperativeName,
            this.fieldSnowOnSiteOver5cm,
            this.fieldCalloutInvoiced});
            pivotGridGroup1.Caption = "Callout Year - Quarter - Month";
            pivotGridGroup1.Fields.Add(this.fieldCalloutYear);
            pivotGridGroup1.Fields.Add(this.fieldCalloutQuarter);
            pivotGridGroup1.Fields.Add(this.fieldCalloutMonth);
            pivotGridGroup1.Fields.Add(this.fieldCalloutWeek);
            pivotGridGroup1.Hierarchy = null;
            pivotGridGroup1.ShowNewValues = true;
            pivotGridGroup2.Caption = "Start Year - Quarter - Month";
            pivotGridGroup2.Fields.Add(this.fieldStartYear);
            pivotGridGroup2.Fields.Add(this.fieldStartQuarter);
            pivotGridGroup2.Fields.Add(this.fieldStartMonth);
            pivotGridGroup2.Fields.Add(this.fieldStartWeek);
            pivotGridGroup2.Hierarchy = null;
            pivotGridGroup2.ShowNewValues = true;
            pivotGridGroup3.Caption = "Completed Year - Quarter - Month";
            pivotGridGroup3.Fields.Add(this.fieldCompletedYear);
            pivotGridGroup3.Fields.Add(this.fieldCompletedQuarter);
            pivotGridGroup3.Fields.Add(this.fieldCompletedMonth);
            pivotGridGroup3.Fields.Add(this.fieldCompletedWeek);
            pivotGridGroup3.Hierarchy = null;
            pivotGridGroup3.ShowNewValues = true;
            this.pivotGridControl1.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup1,
            pivotGridGroup2,
            pivotGridGroup3});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl1.MenuManager = this.barManager1;
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pivotGridControl1.OptionsLayout.Columns.StoreAllOptions = true;
            this.pivotGridControl1.OptionsLayout.StoreAppearance = true;
            this.pivotGridControl1.OptionsLayout.StoreFormatRules = true;
            this.pivotGridControl1.OptionsMenu.EnableFormatRulesMenu = true;
            this.pivotGridControl1.Size = new System.Drawing.Size(859, 388);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.PopupMenuShowing += new DevExpress.XtraPivotGrid.PopupMenuShowingEventHandler(this.pivotGridControl1_PopupMenuShowing);
            this.pivotGridControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pivotGridControl1_MouseUp);
            // 
            // sp04325GCAnalysisGrittingCalloutsForAnalysisBindingSource
            // 
            this.sp04325GCAnalysisGrittingCalloutsForAnalysisBindingSource.DataMember = "sp04325_GC_Analysis_Gritting_Callouts_For_Analysis";
            this.sp04325GCAnalysisGrittingCalloutsForAnalysisBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // fieldSiteGrittingContractID1
            // 
            this.fieldSiteGrittingContractID1.AreaIndex = 0;
            this.fieldSiteGrittingContractID1.Caption = "Site Gritting Contract ID";
            this.fieldSiteGrittingContractID1.FieldName = "SiteGrittingContractID";
            this.fieldSiteGrittingContractID1.Name = "fieldSiteGrittingContractID1";
            this.fieldSiteGrittingContractID1.Visible = false;
            // 
            // fieldSiteName1
            // 
            this.fieldSiteName1.AreaIndex = 0;
            this.fieldSiteName1.Caption = "Site Name";
            this.fieldSiteName1.FieldName = "SiteName";
            this.fieldSiteName1.Name = "fieldSiteName1";
            // 
            // fieldClientName1
            // 
            this.fieldClientName1.AreaIndex = 1;
            this.fieldClientName1.Caption = "Client Name";
            this.fieldClientName1.FieldName = "ClientName";
            this.fieldClientName1.Name = "fieldClientName1";
            // 
            // fieldCompanyName1
            // 
            this.fieldCompanyName1.AreaIndex = 2;
            this.fieldCompanyName1.Caption = "Company Name";
            this.fieldCompanyName1.FieldName = "CompanyName";
            this.fieldCompanyName1.Name = "fieldCompanyName1";
            // 
            // fieldClientsSiteCode1
            // 
            this.fieldClientsSiteCode1.AreaIndex = 3;
            this.fieldClientsSiteCode1.Caption = "Clients Site Code";
            this.fieldClientsSiteCode1.FieldName = "ClientsSiteCode";
            this.fieldClientsSiteCode1.Name = "fieldClientsSiteCode1";
            this.fieldClientsSiteCode1.Visible = false;
            // 
            // fieldSubContractorName1
            // 
            this.fieldSubContractorName1.AreaIndex = 3;
            this.fieldSubContractorName1.Caption = "SubContractor Name";
            this.fieldSubContractorName1.FieldName = "SubContractorName";
            this.fieldSubContractorName1.Name = "fieldSubContractorName1";
            // 
            // fieldSubContractorIsDirectLabour1
            // 
            this.fieldSubContractorIsDirectLabour1.AreaIndex = 4;
            this.fieldSubContractorIsDirectLabour1.Caption = "SubContractor Is Direct Labour";
            this.fieldSubContractorIsDirectLabour1.FieldName = "SubContractorIsDirectLabour";
            this.fieldSubContractorIsDirectLabour1.Name = "fieldSubContractorIsDirectLabour1";
            this.fieldSubContractorIsDirectLabour1.Visible = false;
            // 
            // fieldOriginalSubContarctorName1
            // 
            this.fieldOriginalSubContarctorName1.AreaIndex = 4;
            this.fieldOriginalSubContarctorName1.Caption = "Original SubContractor Name";
            this.fieldOriginalSubContarctorName1.FieldName = "OriginalSubContarctorName";
            this.fieldOriginalSubContarctorName1.Name = "fieldOriginalSubContarctorName1";
            this.fieldOriginalSubContarctorName1.Visible = false;
            // 
            // fieldReactive1
            // 
            this.fieldReactive1.AreaIndex = 5;
            this.fieldReactive1.Caption = "Reactive";
            this.fieldReactive1.FieldName = "Reactive";
            this.fieldReactive1.Name = "fieldReactive1";
            // 
            // fieldCalloutStatusDescription1
            // 
            this.fieldCalloutStatusDescription1.AreaIndex = 6;
            this.fieldCalloutStatusDescription1.Caption = "Callout Status";
            this.fieldCalloutStatusDescription1.FieldName = "CalloutStatusDescription";
            this.fieldCalloutStatusDescription1.Name = "fieldCalloutStatusDescription1";
            // 
            // fieldCalloutStatusOrder1
            // 
            this.fieldCalloutStatusOrder1.AreaIndex = 6;
            this.fieldCalloutStatusOrder1.Caption = "Callout Status Order";
            this.fieldCalloutStatusOrder1.FieldName = "CalloutStatusOrder";
            this.fieldCalloutStatusOrder1.Name = "fieldCalloutStatusOrder1";
            this.fieldCalloutStatusOrder1.Visible = false;
            // 
            // fieldCallOutDateTime1
            // 
            this.fieldCallOutDateTime1.AreaIndex = 7;
            this.fieldCallOutDateTime1.Caption = "Callout Date";
            this.fieldCallOutDateTime1.FieldName = "CallOutDateTime";
            this.fieldCallOutDateTime1.Name = "fieldCallOutDateTime1";
            // 
            // fieldImportedWeatherForecastID1
            // 
            this.fieldImportedWeatherForecastID1.AreaIndex = 7;
            this.fieldImportedWeatherForecastID1.Caption = "Imported Weather Forecast ID";
            this.fieldImportedWeatherForecastID1.FieldName = "ImportedWeatherForecastID";
            this.fieldImportedWeatherForecastID1.Name = "fieldImportedWeatherForecastID1";
            this.fieldImportedWeatherForecastID1.Visible = false;
            // 
            // fieldTextSentTime1
            // 
            this.fieldTextSentTime1.AreaIndex = 9;
            this.fieldTextSentTime1.Caption = "Text Sent Time";
            this.fieldTextSentTime1.FieldName = "TextSentTime";
            this.fieldTextSentTime1.Name = "fieldTextSentTime1";
            // 
            // fieldSubContractorReceivedTime1
            // 
            this.fieldSubContractorReceivedTime1.AreaIndex = 10;
            this.fieldSubContractorReceivedTime1.Caption = "SubContractor Received Time";
            this.fieldSubContractorReceivedTime1.FieldName = "SubContractorReceivedTime";
            this.fieldSubContractorReceivedTime1.Name = "fieldSubContractorReceivedTime1";
            // 
            // fieldSubContractorRespondedTime1
            // 
            this.fieldSubContractorRespondedTime1.AreaIndex = 11;
            this.fieldSubContractorRespondedTime1.Caption = "SubContractor Responded Time";
            this.fieldSubContractorRespondedTime1.FieldName = "SubContractorRespondedTime";
            this.fieldSubContractorRespondedTime1.Name = "fieldSubContractorRespondedTime1";
            // 
            // fieldSubContractorETA1
            // 
            this.fieldSubContractorETA1.AreaIndex = 10;
            this.fieldSubContractorETA1.Caption = "SubContractor ETA";
            this.fieldSubContractorETA1.FieldName = "SubContractorETA";
            this.fieldSubContractorETA1.Name = "fieldSubContractorETA1";
            this.fieldSubContractorETA1.Visible = false;
            // 
            // fieldCompletedTime1
            // 
            this.fieldCompletedTime1.AreaIndex = 14;
            this.fieldCompletedTime1.Caption = "Completed Time";
            this.fieldCompletedTime1.FieldName = "CompletedTime";
            this.fieldCompletedTime1.Name = "fieldCompletedTime1";
            // 
            // fieldAuthorisedByName1
            // 
            this.fieldAuthorisedByName1.AreaIndex = 16;
            this.fieldAuthorisedByName1.Caption = "Authorised By Name";
            this.fieldAuthorisedByName1.FieldName = "AuthorisedByName";
            this.fieldAuthorisedByName1.Name = "fieldAuthorisedByName1";
            // 
            // fieldVisitAborted1
            // 
            this.fieldVisitAborted1.AreaIndex = 17;
            this.fieldVisitAborted1.Caption = "Visit Aborted";
            this.fieldVisitAborted1.FieldName = "VisitAborted";
            this.fieldVisitAborted1.Name = "fieldVisitAborted1";
            // 
            // fieldAbortedReason1
            // 
            this.fieldAbortedReason1.AreaIndex = 18;
            this.fieldAbortedReason1.Caption = "Aborted Reason";
            this.fieldAbortedReason1.FieldName = "AbortedReason";
            this.fieldAbortedReason1.Name = "fieldAbortedReason1";
            // 
            // fieldStartLatitude1
            // 
            this.fieldStartLatitude1.AreaIndex = 14;
            this.fieldStartLatitude1.Caption = "Start Latitude";
            this.fieldStartLatitude1.FieldName = "StartLatitude";
            this.fieldStartLatitude1.Name = "fieldStartLatitude1";
            this.fieldStartLatitude1.Visible = false;
            // 
            // fieldStartLongitude1
            // 
            this.fieldStartLongitude1.AreaIndex = 14;
            this.fieldStartLongitude1.Caption = "Start Longitude";
            this.fieldStartLongitude1.FieldName = "StartLongitude";
            this.fieldStartLongitude1.Name = "fieldStartLongitude1";
            this.fieldStartLongitude1.Visible = false;
            // 
            // fieldFinishedLatitude1
            // 
            this.fieldFinishedLatitude1.AreaIndex = 14;
            this.fieldFinishedLatitude1.Caption = "Finished Latitude";
            this.fieldFinishedLatitude1.FieldName = "FinishedLatitude";
            this.fieldFinishedLatitude1.Name = "fieldFinishedLatitude1";
            this.fieldFinishedLatitude1.Visible = false;
            // 
            // fieldFinishedLongitude1
            // 
            this.fieldFinishedLongitude1.AreaIndex = 14;
            this.fieldFinishedLongitude1.Caption = "Finished Longitude";
            this.fieldFinishedLongitude1.FieldName = "FinishedLongitude";
            this.fieldFinishedLongitude1.Name = "fieldFinishedLongitude1";
            this.fieldFinishedLongitude1.Visible = false;
            // 
            // fieldClientPONumber1
            // 
            this.fieldClientPONumber1.AreaIndex = 19;
            this.fieldClientPONumber1.Caption = "Client PO Number";
            this.fieldClientPONumber1.FieldName = "ClientPONumber";
            this.fieldClientPONumber1.Name = "fieldClientPONumber1";
            // 
            // fieldSaltUsed1
            // 
            this.fieldSaltUsed1.AreaIndex = 20;
            this.fieldSaltUsed1.Caption = "Salt Used";
            this.fieldSaltUsed1.CellFormat.FormatString = "######0.00 25 Kg Bags";
            this.fieldSaltUsed1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltUsed1.FieldName = "SaltUsed";
            this.fieldSaltUsed1.GrandTotalCellFormat.FormatString = "######0.00 25 Kg Bags";
            this.fieldSaltUsed1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltUsed1.Name = "fieldSaltUsed1";
            this.fieldSaltUsed1.TotalCellFormat.FormatString = "######0.00 25 Kg Bags";
            this.fieldSaltUsed1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltUsed1.TotalValueFormat.FormatString = "######0.00 25 Kg Bags";
            this.fieldSaltUsed1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltUsed1.ValueFormat.FormatString = "######0.00 25 Kg Bags";
            this.fieldSaltUsed1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldSaltCost1
            // 
            this.fieldSaltCost1.AreaIndex = 21;
            this.fieldSaltCost1.Caption = "Salt Cost";
            this.fieldSaltCost1.CellFormat.FormatString = "c";
            this.fieldSaltCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltCost1.FieldName = "SaltCost";
            this.fieldSaltCost1.GrandTotalCellFormat.FormatString = "c";
            this.fieldSaltCost1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltCost1.Name = "fieldSaltCost1";
            this.fieldSaltCost1.TotalCellFormat.FormatString = "c";
            this.fieldSaltCost1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltCost1.TotalValueFormat.FormatString = "c";
            this.fieldSaltCost1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltCost1.ValueFormat.FormatString = "c";
            this.fieldSaltCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldSaltSell1
            // 
            this.fieldSaltSell1.AreaIndex = 22;
            this.fieldSaltSell1.Caption = "Salt Sell";
            this.fieldSaltSell1.CellFormat.FormatString = "c";
            this.fieldSaltSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltSell1.FieldName = "SaltSell";
            this.fieldSaltSell1.GrandTotalCellFormat.FormatString = "c";
            this.fieldSaltSell1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltSell1.Name = "fieldSaltSell1";
            this.fieldSaltSell1.TotalCellFormat.FormatString = "c";
            this.fieldSaltSell1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltSell1.TotalValueFormat.FormatString = "c";
            this.fieldSaltSell1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltSell1.ValueFormat.FormatString = "c";
            this.fieldSaltSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldSaltVatRate1
            // 
            this.fieldSaltVatRate1.AreaIndex = 23;
            this.fieldSaltVatRate1.Caption = "Salt Vat Rate";
            this.fieldSaltVatRate1.CellFormat.FormatString = "P2";
            this.fieldSaltVatRate1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltVatRate1.FieldName = "SaltVatRate";
            this.fieldSaltVatRate1.GrandTotalCellFormat.FormatString = "P2";
            this.fieldSaltVatRate1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltVatRate1.Name = "fieldSaltVatRate1";
            this.fieldSaltVatRate1.TotalCellFormat.FormatString = "P2";
            this.fieldSaltVatRate1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltVatRate1.TotalValueFormat.FormatString = "P2";
            this.fieldSaltVatRate1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSaltVatRate1.ValueFormat.FormatString = "P2";
            this.fieldSaltVatRate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldHoursWorked1
            // 
            this.fieldHoursWorked1.AreaIndex = 24;
            this.fieldHoursWorked1.Caption = "Hours Worked";
            this.fieldHoursWorked1.CellFormat.FormatString = "f2";
            this.fieldHoursWorked1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHoursWorked1.FieldName = "HoursWorked";
            this.fieldHoursWorked1.GrandTotalCellFormat.FormatString = "f2";
            this.fieldHoursWorked1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHoursWorked1.Name = "fieldHoursWorked1";
            this.fieldHoursWorked1.TotalCellFormat.FormatString = "f2";
            this.fieldHoursWorked1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHoursWorked1.TotalValueFormat.FormatString = "f2";
            this.fieldHoursWorked1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHoursWorked1.ValueFormat.FormatString = "f2";
            this.fieldHoursWorked1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTeamHourlyRate1
            // 
            this.fieldTeamHourlyRate1.AreaIndex = 25;
            this.fieldTeamHourlyRate1.Caption = "Team Hourly Rate";
            this.fieldTeamHourlyRate1.CellFormat.FormatString = "c";
            this.fieldTeamHourlyRate1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTeamHourlyRate1.FieldName = "TeamHourlyRate";
            this.fieldTeamHourlyRate1.GrandTotalCellFormat.FormatString = "c";
            this.fieldTeamHourlyRate1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTeamHourlyRate1.Name = "fieldTeamHourlyRate1";
            this.fieldTeamHourlyRate1.TotalCellFormat.FormatString = "c";
            this.fieldTeamHourlyRate1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTeamHourlyRate1.TotalValueFormat.FormatString = "c";
            this.fieldTeamHourlyRate1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTeamHourlyRate1.ValueFormat.FormatString = "c";
            this.fieldTeamHourlyRate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTeamCharge1
            // 
            this.fieldTeamCharge1.AreaIndex = 26;
            this.fieldTeamCharge1.Caption = "Team Charge";
            this.fieldTeamCharge1.CellFormat.FormatString = "c";
            this.fieldTeamCharge1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTeamCharge1.FieldName = "TeamCharge";
            this.fieldTeamCharge1.GrandTotalCellFormat.FormatString = "c";
            this.fieldTeamCharge1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTeamCharge1.Name = "fieldTeamCharge1";
            this.fieldTeamCharge1.TotalCellFormat.FormatString = "c";
            this.fieldTeamCharge1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTeamCharge1.TotalValueFormat.FormatString = "c";
            this.fieldTeamCharge1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTeamCharge1.ValueFormat.FormatString = "c";
            this.fieldTeamCharge1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldLabourCost1
            // 
            this.fieldLabourCost1.AreaIndex = 27;
            this.fieldLabourCost1.Caption = "Labour Cost";
            this.fieldLabourCost1.CellFormat.FormatString = "c";
            this.fieldLabourCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourCost1.FieldName = "LabourCost";
            this.fieldLabourCost1.GrandTotalCellFormat.FormatString = "c";
            this.fieldLabourCost1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourCost1.Name = "fieldLabourCost1";
            this.fieldLabourCost1.TotalCellFormat.FormatString = "c";
            this.fieldLabourCost1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourCost1.TotalValueFormat.FormatString = "c";
            this.fieldLabourCost1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourCost1.ValueFormat.FormatString = "c";
            this.fieldLabourCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldLabourVatRate1
            // 
            this.fieldLabourVatRate1.AreaIndex = 28;
            this.fieldLabourVatRate1.Caption = "Labour Vat Rate";
            this.fieldLabourVatRate1.CellFormat.FormatString = "P2";
            this.fieldLabourVatRate1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourVatRate1.FieldName = "LabourVatRate";
            this.fieldLabourVatRate1.GrandTotalCellFormat.FormatString = "P2";
            this.fieldLabourVatRate1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourVatRate1.Name = "fieldLabourVatRate1";
            this.fieldLabourVatRate1.TotalCellFormat.FormatString = "P2";
            this.fieldLabourVatRate1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourVatRate1.TotalValueFormat.FormatString = "P2";
            this.fieldLabourVatRate1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourVatRate1.ValueFormat.FormatString = "P2";
            this.fieldLabourVatRate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldOtherCost1
            // 
            this.fieldOtherCost1.AreaIndex = 29;
            this.fieldOtherCost1.Caption = "Other Cost";
            this.fieldOtherCost1.CellFormat.FormatString = "c";
            this.fieldOtherCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherCost1.FieldName = "OtherCost";
            this.fieldOtherCost1.GrandTotalCellFormat.FormatString = "c";
            this.fieldOtherCost1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherCost1.Name = "fieldOtherCost1";
            this.fieldOtherCost1.TotalCellFormat.FormatString = "c";
            this.fieldOtherCost1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherCost1.TotalValueFormat.FormatString = "c";
            this.fieldOtherCost1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherCost1.ValueFormat.FormatString = "c";
            this.fieldOtherCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldOtherSell1
            // 
            this.fieldOtherSell1.AreaIndex = 30;
            this.fieldOtherSell1.Caption = "Other Sell";
            this.fieldOtherSell1.CellFormat.FormatString = "c";
            this.fieldOtherSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherSell1.FieldName = "OtherSell";
            this.fieldOtherSell1.GrandTotalCellFormat.FormatString = "c";
            this.fieldOtherSell1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherSell1.Name = "fieldOtherSell1";
            this.fieldOtherSell1.TotalCellFormat.FormatString = "c";
            this.fieldOtherSell1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherSell1.TotalValueFormat.FormatString = "c";
            this.fieldOtherSell1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherSell1.ValueFormat.FormatString = "c";
            this.fieldOtherSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTotalCost1
            // 
            this.fieldTotalCost1.AreaIndex = 31;
            this.fieldTotalCost1.Caption = "Total Cost";
            this.fieldTotalCost1.CellFormat.FormatString = "c";
            this.fieldTotalCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalCost1.FieldName = "TotalCost";
            this.fieldTotalCost1.GrandTotalCellFormat.FormatString = "c";
            this.fieldTotalCost1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalCost1.Name = "fieldTotalCost1";
            this.fieldTotalCost1.TotalCellFormat.FormatString = "c";
            this.fieldTotalCost1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalCost1.TotalValueFormat.FormatString = "c";
            this.fieldTotalCost1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalCost1.ValueFormat.FormatString = "c";
            this.fieldTotalCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTotalSell1
            // 
            this.fieldTotalSell1.AreaIndex = 32;
            this.fieldTotalSell1.Caption = "Total Sell";
            this.fieldTotalSell1.CellFormat.FormatString = "c";
            this.fieldTotalSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalSell1.FieldName = "TotalSell";
            this.fieldTotalSell1.GrandTotalCellFormat.FormatString = "c";
            this.fieldTotalSell1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalSell1.Name = "fieldTotalSell1";
            this.fieldTotalSell1.TotalCellFormat.FormatString = "c";
            this.fieldTotalSell1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalSell1.TotalValueFormat.FormatString = "c";
            this.fieldTotalSell1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalSell1.ValueFormat.FormatString = "c";
            this.fieldTotalSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldClientInvoiceNumber1
            // 
            this.fieldClientInvoiceNumber1.AreaIndex = 33;
            this.fieldClientInvoiceNumber1.Caption = "Client Invoice Number";
            this.fieldClientInvoiceNumber1.FieldName = "ClientInvoiceNumber";
            this.fieldClientInvoiceNumber1.Name = "fieldClientInvoiceNumber1";
            // 
            // fieldSubContractorPaid1
            // 
            this.fieldSubContractorPaid1.AreaIndex = 34;
            this.fieldSubContractorPaid1.Caption = "SubContractor Paid";
            this.fieldSubContractorPaid1.FieldName = "SubContractorPaid";
            this.fieldSubContractorPaid1.Name = "fieldSubContractorPaid1";
            // 
            // fieldGrittingInvoiceID1
            // 
            this.fieldGrittingInvoiceID1.AreaIndex = 30;
            this.fieldGrittingInvoiceID1.Caption = "Gritting Invoice ID";
            this.fieldGrittingInvoiceID1.FieldName = "GrittingInvoiceID";
            this.fieldGrittingInvoiceID1.Name = "fieldGrittingInvoiceID1";
            this.fieldGrittingInvoiceID1.Visible = false;
            // 
            // fieldRecordedByName1
            // 
            this.fieldRecordedByName1.AreaIndex = 35;
            this.fieldRecordedByName1.Caption = "Recorded By Name";
            this.fieldRecordedByName1.FieldName = "RecordedByName";
            this.fieldRecordedByName1.Name = "fieldRecordedByName1";
            // 
            // fieldPaidByName1
            // 
            this.fieldPaidByName1.AreaIndex = 36;
            this.fieldPaidByName1.Caption = "Paid By Name";
            this.fieldPaidByName1.FieldName = "PaidByName";
            this.fieldPaidByName1.Name = "fieldPaidByName1";
            // 
            // fieldDoNotPaySubContractor1
            // 
            this.fieldDoNotPaySubContractor1.AreaIndex = 37;
            this.fieldDoNotPaySubContractor1.Caption = "Do Not Pay Sub Contractor";
            this.fieldDoNotPaySubContractor1.FieldName = "DoNotPaySubContractor";
            this.fieldDoNotPaySubContractor1.Name = "fieldDoNotPaySubContractor1";
            // 
            // fieldDoNotPaySubContractorReason1
            // 
            this.fieldDoNotPaySubContractorReason1.AreaIndex = 34;
            this.fieldDoNotPaySubContractorReason1.Caption = "Do Not Pay Sub Contractor Reason";
            this.fieldDoNotPaySubContractorReason1.FieldName = "DoNotPaySubContractorReason";
            this.fieldDoNotPaySubContractorReason1.Name = "fieldDoNotPaySubContractorReason1";
            this.fieldDoNotPaySubContractorReason1.Visible = false;
            // 
            // fieldDoNotInvoiceClient1
            // 
            this.fieldDoNotInvoiceClient1.AreaIndex = 38;
            this.fieldDoNotInvoiceClient1.Caption = "Do Not Invoice Client";
            this.fieldDoNotInvoiceClient1.FieldName = "DoNotInvoiceClient";
            this.fieldDoNotInvoiceClient1.Name = "fieldDoNotInvoiceClient1";
            // 
            // fieldDoNotInvoiceClientReason1
            // 
            this.fieldDoNotInvoiceClientReason1.AreaIndex = 35;
            this.fieldDoNotInvoiceClientReason1.Caption = "Do Not Invoice Client Reason";
            this.fieldDoNotInvoiceClientReason1.FieldName = "DoNotInvoiceClientReason";
            this.fieldDoNotInvoiceClientReason1.Name = "fieldDoNotInvoiceClientReason1";
            this.fieldDoNotInvoiceClientReason1.Visible = false;
            // 
            // fieldNoAccess1
            // 
            this.fieldNoAccess1.AreaIndex = 40;
            this.fieldNoAccess1.Caption = "No Access";
            this.fieldNoAccess1.FieldName = "NoAccess";
            this.fieldNoAccess1.Name = "fieldNoAccess1";
            // 
            // fieldSiteWeather1
            // 
            this.fieldSiteWeather1.AreaIndex = 41;
            this.fieldSiteWeather1.Caption = "Site Weather";
            this.fieldSiteWeather1.FieldName = "SiteWeather";
            this.fieldSiteWeather1.Name = "fieldSiteWeather1";
            // 
            // fieldSiteTemperature1
            // 
            this.fieldSiteTemperature1.AreaIndex = 42;
            this.fieldSiteTemperature1.Caption = "Site Temperature";
            this.fieldSiteTemperature1.FieldName = "SiteTemperature";
            this.fieldSiteTemperature1.Name = "fieldSiteTemperature1";
            // 
            // fieldPdaID1
            // 
            this.fieldPdaID1.AreaIndex = 38;
            this.fieldPdaID1.Caption = "Device ID";
            this.fieldPdaID1.FieldName = "PdaID";
            this.fieldPdaID1.Name = "fieldPdaID1";
            this.fieldPdaID1.Visible = false;
            // 
            // fieldTeamPresent1
            // 
            this.fieldTeamPresent1.AreaIndex = 38;
            this.fieldTeamPresent1.Caption = "Team Present";
            this.fieldTeamPresent1.FieldName = "TeamPresent";
            this.fieldTeamPresent1.Name = "fieldTeamPresent1";
            this.fieldTeamPresent1.Visible = false;
            // 
            // fieldServiceFailure1
            // 
            this.fieldServiceFailure1.AreaIndex = 43;
            this.fieldServiceFailure1.Caption = "Service Failure";
            this.fieldServiceFailure1.FieldName = "ServiceFailure";
            this.fieldServiceFailure1.Name = "fieldServiceFailure1";
            // 
            // fieldComments1
            // 
            this.fieldComments1.AreaIndex = 39;
            this.fieldComments1.Caption = "Comments";
            this.fieldComments1.FieldName = "Comments";
            this.fieldComments1.Name = "fieldComments1";
            this.fieldComments1.Visible = false;
            // 
            // fieldClientPrice1
            // 
            this.fieldClientPrice1.AreaIndex = 43;
            this.fieldClientPrice1.Caption = "Client Price";
            this.fieldClientPrice1.CellFormat.FormatString = "c";
            this.fieldClientPrice1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldClientPrice1.FieldName = "ClientPrice";
            this.fieldClientPrice1.GrandTotalCellFormat.FormatString = "c";
            this.fieldClientPrice1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldClientPrice1.Name = "fieldClientPrice1";
            this.fieldClientPrice1.TotalCellFormat.FormatString = "c";
            this.fieldClientPrice1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldClientPrice1.TotalValueFormat.FormatString = "c";
            this.fieldClientPrice1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldClientPrice1.ValueFormat.FormatString = "c";
            this.fieldClientPrice1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldClientPrice1.Visible = false;
            // 
            // fieldSnowOnSite1
            // 
            this.fieldSnowOnSite1.AreaIndex = 44;
            this.fieldSnowOnSite1.Caption = "Snow On Site";
            this.fieldSnowOnSite1.FieldName = "SnowOnSite";
            this.fieldSnowOnSite1.Name = "fieldSnowOnSite1";
            // 
            // fieldGritJobTransferMethod1
            // 
            this.fieldGritJobTransferMethod1.AreaIndex = 44;
            this.fieldGritJobTransferMethod1.Caption = "Job Transfer Method";
            this.fieldGritJobTransferMethod1.FieldName = "GritJobTransferMethod";
            this.fieldGritJobTransferMethod1.Name = "fieldGritJobTransferMethod1";
            this.fieldGritJobTransferMethod1.Visible = false;
            // 
            // fieldStartTime1
            // 
            this.fieldStartTime1.AreaIndex = 12;
            this.fieldStartTime1.Caption = "Start Time";
            this.fieldStartTime1.FieldName = "StartTime";
            this.fieldStartTime1.Name = "fieldStartTime1";
            // 
            // fieldProfit1
            // 
            this.fieldProfit1.AreaIndex = 46;
            this.fieldProfit1.Caption = "Profit";
            this.fieldProfit1.CellFormat.FormatString = "c";
            this.fieldProfit1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldProfit1.FieldName = "Profit";
            this.fieldProfit1.GrandTotalCellFormat.FormatString = "c";
            this.fieldProfit1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldProfit1.Name = "fieldProfit1";
            this.fieldProfit1.TotalCellFormat.FormatString = "c";
            this.fieldProfit1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldProfit1.TotalValueFormat.FormatString = "c";
            this.fieldProfit1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldProfit1.ValueFormat.FormatString = "c";
            this.fieldProfit1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldMarkup1
            // 
            this.fieldMarkup1.AreaIndex = 47;
            this.fieldMarkup1.Caption = "Markup %";
            this.fieldMarkup1.CellFormat.FormatString = "P2";
            this.fieldMarkup1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldMarkup1.FieldName = "Markup";
            this.fieldMarkup1.GrandTotalCellFormat.FormatString = "P2";
            this.fieldMarkup1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldMarkup1.Name = "fieldMarkup1";
            this.fieldMarkup1.TotalCellFormat.FormatString = "P2";
            this.fieldMarkup1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldMarkup1.TotalValueFormat.FormatString = "P2";
            this.fieldMarkup1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldMarkup1.ValueFormat.FormatString = "P2";
            this.fieldMarkup1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldClientPOID1
            // 
            this.fieldClientPOID1.AreaIndex = 42;
            this.fieldClientPOID1.Caption = "Client PO ID";
            this.fieldClientPOID1.FieldName = "ClientPOID";
            this.fieldClientPOID1.Name = "fieldClientPOID1";
            this.fieldClientPOID1.Visible = false;
            // 
            // fieldNonStandardCost1
            // 
            this.fieldNonStandardCost1.AreaIndex = 48;
            this.fieldNonStandardCost1.Caption = "Non-Standard Cost";
            this.fieldNonStandardCost1.FieldName = "NonStandardCost";
            this.fieldNonStandardCost1.Name = "fieldNonStandardCost1";
            // 
            // fieldNonStandardSell1
            // 
            this.fieldNonStandardSell1.AreaIndex = 49;
            this.fieldNonStandardSell1.Caption = "Non-Standard Sell";
            this.fieldNonStandardSell1.FieldName = "NonStandardSell";
            this.fieldNonStandardSell1.Name = "fieldNonStandardSell1";
            // 
            // fieldPPEWorn1
            // 
            this.fieldPPEWorn1.AreaIndex = 44;
            this.fieldPPEWorn1.Caption = "PPE Worn";
            this.fieldPPEWorn1.FieldName = "PPEWorn";
            this.fieldPPEWorn1.Name = "fieldPPEWorn1";
            this.fieldPPEWorn1.Visible = false;
            // 
            // fieldNoAccessAbortedRate1
            // 
            this.fieldNoAccessAbortedRate1.AreaIndex = 44;
            this.fieldNoAccessAbortedRate1.Caption = "No Access Aborted Rate";
            this.fieldNoAccessAbortedRate1.FieldName = "NoAccessAbortedRate";
            this.fieldNoAccessAbortedRate1.Name = "fieldNoAccessAbortedRate1";
            this.fieldNoAccessAbortedRate1.Visible = false;
            // 
            // fieldTeamMembersPresent1
            // 
            this.fieldTeamMembersPresent1.AreaIndex = 50;
            this.fieldTeamMembersPresent1.Caption = "Team Members Present";
            this.fieldTeamMembersPresent1.FieldName = "TeamMembersPresent";
            this.fieldTeamMembersPresent1.Name = "fieldTeamMembersPresent1";
            this.fieldTeamMembersPresent1.Visible = false;
            // 
            // fieldSnowInfo1
            // 
            this.fieldSnowInfo1.AreaIndex = 50;
            this.fieldSnowInfo1.Caption = "Snow Info";
            this.fieldSnowInfo1.FieldName = "SnowInfo";
            this.fieldSnowInfo1.Name = "fieldSnowInfo1";
            // 
            // fieldEveningRateModifier1
            // 
            this.fieldEveningRateModifier1.AreaIndex = 45;
            this.fieldEveningRateModifier1.Caption = "Evening Rate Modifier";
            this.fieldEveningRateModifier1.FieldName = "EveningRateModifier";
            this.fieldEveningRateModifier1.Name = "fieldEveningRateModifier1";
            this.fieldEveningRateModifier1.Visible = false;
            // 
            // fieldJobSheetNumber1
            // 
            this.fieldJobSheetNumber1.AreaIndex = 51;
            this.fieldJobSheetNumber1.Caption = "Job Sheet Number";
            this.fieldJobSheetNumber1.FieldName = "JobSheetNumber";
            this.fieldJobSheetNumber1.Name = "fieldJobSheetNumber1";
            // 
            // fieldCompletedOnPDA1
            // 
            this.fieldCompletedOnPDA1.AreaIndex = 52;
            this.fieldCompletedOnPDA1.Caption = "Completed On Device";
            this.fieldCompletedOnPDA1.FieldName = "CompletedOnPDA";
            this.fieldCompletedOnPDA1.Name = "fieldCompletedOnPDA1";
            // 
            // fieldFinanceInvoiceNumber1
            // 
            this.fieldFinanceInvoiceNumber1.AreaIndex = 53;
            this.fieldFinanceInvoiceNumber1.Caption = "Finance Invoice Number";
            this.fieldFinanceInvoiceNumber1.FieldName = "FinanceInvoiceNumber";
            this.fieldFinanceInvoiceNumber1.Name = "fieldFinanceInvoiceNumber1";
            // 
            // fieldCalloutCount1
            // 
            this.fieldCalloutCount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldCalloutCount1.AreaIndex = 0;
            this.fieldCalloutCount1.Caption = "Callout Count";
            this.fieldCalloutCount1.FieldName = "CalloutCount";
            this.fieldCalloutCount1.Name = "fieldCalloutCount1";
            // 
            // fieldOperativeName
            // 
            this.fieldOperativeName.AreaIndex = 4;
            this.fieldOperativeName.Caption = "Operative Name";
            this.fieldOperativeName.FieldName = "OperativeName";
            this.fieldOperativeName.Name = "fieldOperativeName";
            // 
            // fieldSnowOnSiteOver5cm
            // 
            this.fieldSnowOnSiteOver5cm.AreaIndex = 45;
            this.fieldSnowOnSiteOver5cm.Caption = "Snow Over 5 cm";
            this.fieldSnowOnSiteOver5cm.FieldName = "SnowOnSiteOver5cm";
            this.fieldSnowOnSiteOver5cm.Name = "fieldSnowOnSiteOver5cm";
            // 
            // fieldCalloutInvoiced
            // 
            this.fieldCalloutInvoiced.AreaIndex = 39;
            this.fieldCalloutInvoiced.Caption = "Invoiced";
            this.fieldCalloutInvoiced.FieldName = "CalloutInvoiced";
            this.fieldCalloutInvoiced.Name = "fieldCalloutInvoiced";
            // 
            // chartControl1
            // 
            this.chartControl1.DataAdapter = this.sp04237_GC_Company_Filter_ListTableAdapter;
            this.chartControl1.DataSource = this.dataSet_GC_Reports.sp04237_GC_Company_Filter_List;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl1.EmptyChartText.Text = "No Data To Graph - Try Selecting Data from the Analysis Grid.";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            sideBySideBarSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl1.SeriesTemplate.Label = sideBySideBarSeriesLabel1;
            this.chartControl1.Size = new System.Drawing.Size(859, 100);
            this.chartControl1.TabIndex = 1;
            // 
            // sp04237_GC_Company_Filter_ListTableAdapter
            // 
            this.sp04237_GC_Company_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiToggleAvailableColumnsVisibility, true)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Copy to Clipboard";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Hint = "Copy the selected cells to the Clipboard for pasting to external application.";
            this.barButtonItem1.Id = 25;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // bbiToggleAvailableColumnsVisibility
            // 
            this.bbiToggleAvailableColumnsVisibility.Caption = "Toggle Available Columns Visibility";
            this.bbiToggleAvailableColumnsVisibility.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiToggleAvailableColumnsVisibility.Glyph")));
            this.bbiToggleAvailableColumnsVisibility.Id = 32;
            this.bbiToggleAvailableColumnsVisibility.Name = "bbiToggleAvailableColumnsVisibility";
            this.bbiToggleAvailableColumnsVisibility.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiToggleAvailableColumnsVisibility_ItemClick);
            // 
            // pmChart
            // 
            this.pmChart.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateAxis),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChartWizard, true)});
            this.pmChart.Manager = this.barManager1;
            this.pmChart.MenuCaption = "Chart Menu";
            this.pmChart.Name = "pmChart";
            // 
            // bbiRotateAxis
            // 
            this.bbiRotateAxis.Caption = "Rotate Axis";
            this.bbiRotateAxis.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRotateAxis.Glyph")));
            this.bbiRotateAxis.Hint = "Rotate Chart Axis";
            this.bbiRotateAxis.Id = 27;
            this.bbiRotateAxis.Name = "bbiRotateAxis";
            this.bbiRotateAxis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAxis_ItemClick);
            // 
            // bbiChartWizard
            // 
            this.bbiChartWizard.Caption = "Chart Wizard";
            this.bbiChartWizard.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiChartWizard.Glyph")));
            this.bbiChartWizard.Hint = "Open Chart Wizard";
            this.bbiChartWizard.Id = 26;
            this.bbiChartWizard.Name = "bbiChartWizard";
            this.bbiChartWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChartWizard_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(580, 206);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemStatusFilter),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.barEditItemCompanyFilter, "", false, true, true, 107),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadData),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAnalyse, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // barEditItemStatusFilter
            // 
            this.barEditItemStatusFilter.Caption = "Status Filter";
            this.barEditItemStatusFilter.Edit = this.repositoryItemPopupContainerEditStatusFilter;
            this.barEditItemStatusFilter.EditValue = "All Statuses";
            this.barEditItemStatusFilter.EditWidth = 83;
            this.barEditItemStatusFilter.Id = 28;
            this.barEditItemStatusFilter.Name = "barEditItemStatusFilter";
            // 
            // repositoryItemPopupContainerEditStatusFilter
            // 
            this.repositoryItemPopupContainerEditStatusFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditStatusFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditStatusFilter.Name = "repositoryItemPopupContainerEditStatusFilter";
            this.repositoryItemPopupContainerEditStatusFilter.PopupControl = this.popupContainerControlStatuses;
            this.repositoryItemPopupContainerEditStatusFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditStatusFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditStatusFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditStatusFilter_QueryResultValue);
            // 
            // barEditItemCompanyFilter
            // 
            this.barEditItemCompanyFilter.Caption = "Company Filter";
            this.barEditItemCompanyFilter.Edit = this.repositoryItemPopupContainerEditCompanyFilter;
            this.barEditItemCompanyFilter.EditValue = "All Companies";
            this.barEditItemCompanyFilter.EditWidth = 93;
            this.barEditItemCompanyFilter.Id = 29;
            this.barEditItemCompanyFilter.Name = "barEditItemCompanyFilter";
            // 
            // repositoryItemPopupContainerEditCompanyFilter
            // 
            this.repositoryItemPopupContainerEditCompanyFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditCompanyFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditCompanyFilter.Name = "repositoryItemPopupContainerEditCompanyFilter";
            this.repositoryItemPopupContainerEditCompanyFilter.PopupControl = this.popupContainerControlCompanies;
            this.repositoryItemPopupContainerEditCompanyFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditCompanyFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditCompanyFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditCompanyFilter_QueryResultValue);
            // 
            // bbiReloadData
            // 
            this.bbiReloadData.Caption = "Reload";
            this.bbiReloadData.Glyph = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiReloadData.Id = 30;
            this.bbiReloadData.Name = "bbiReloadData";
            this.bbiReloadData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Reload Data - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to reload the Data Supply List using the data supplied in the Filter.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiReloadData.SuperTip = superToolTip3;
            this.bbiReloadData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadData_ItemClick);
            // 
            // bbiAnalyse
            // 
            this.bbiAnalyse.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiAnalyse.Caption = "Analyse";
            this.bbiAnalyse.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAnalyse.Glyph")));
            this.bbiAnalyse.Id = 31;
            this.bbiAnalyse.Name = "bbiAnalyse";
            this.bbiAnalyse.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Text = "Analyse - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to load the Analysis Grid and Chart with the records selected in the Dat" +
    "a Supply list.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiAnalyse.SuperTip = superToolTip4;
            this.bbiAnalyse.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAnalyse_ItemClick);
            // 
            // sp04001_GC_Job_CallOut_StatusesTableAdapter
            // 
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04324_GC_Analysis_Gritting_Callouts_ListTableAdapter
            // 
            this.sp04324_GC_Analysis_Gritting_Callouts_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04325_GC_Analysis_Gritting_Callouts_For_AnalysisTableAdapter
            // 
            this.sp04325_GC_Analysis_Gritting_Callouts_For_AnalysisTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Analysis_Gritting_Callouts
            // 
            this.ClientSize = new System.Drawing.Size(1293, 494);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.dockPanel1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Analysis_Gritting_Callouts";
            this.Text = "Gritting Callout Analysis";
            this.Activated += new System.EventHandler(this.frm_GC_Analysis_Gritting_Callouts_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_GC_Analysis_Gritting_Callouts_FormClosed);
            this.Load += new System.EventHandler(this.frm_GC_Analysis_Gritting_Callouts_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04324GCAnalysisGrittingCalloutsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).EndInit();
            this.popupContainerControlCompanies.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlStatuses)).EndInit();
            this.popupContainerControlStatuses.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04325GCAnalysisGrittingCalloutsForAnalysisBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT_Reports dataSet_AT_Reports;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiChartWizard;
        private DevExpress.XtraBars.PopupMenu pmChart;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAxis;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemStatusFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditStatusFilter;
        private DevExpress.XtraBars.BarEditItem barEditItemCompanyFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditCompanyFilter;
        private DevExpress.XtraBars.BarButtonItem bbiReloadData;
        private DevExpress.XtraBars.BarButtonItem bbiAnalyse;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlStatuses;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit deFromDate;
        private DevExpress.XtraEditors.DateEdit deToDate;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCompanies;
        private DevExpress.XtraEditors.SimpleButton btnCompanyFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOrder;
        private DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter sp04237_GC_Company_Filter_ListTableAdapter;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private System.Windows.Forms.BindingSource sp04001GCJobCallOutStatusesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter sp04001_GC_Job_CallOut_StatusesTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnStatusFilterOK2;
        private System.Windows.Forms.BindingSource sp04324GCAnalysisGrittingCalloutsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCallOutID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorIsDirectLabour;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContarctorName;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colImportedWeatherForecastID;
        private DevExpress.XtraGrid.Columns.GridColumn colTextSentTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorReceivedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorRespondedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorETA;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitAborted;
        private DevExpress.XtraGrid.Columns.GridColumn colAbortedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishedLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishedLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUsed;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KgBags;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltSell;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltVatRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursWorked;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamHourlyRate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamCharge;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCost;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherSell;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByName;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractorReason;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSourceLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSourceLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteWeather;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTemperature;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamPresent;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceFailure;
        private DevExpress.XtraGrid.Columns.GridColumn colComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colGritJobTransferMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colProfit;
        private DevExpress.XtraGrid.Columns.GridColumn colMarkup;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardSell;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClientReason;
        private DevExpress.XtraGrid.Columns.GridColumn colGritMobileTelephoneNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSignatureFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colPPEWorn;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccessAbortedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMembersPresent;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowInfo;
        private DevExpress.XtraGrid.Columns.GridColumn colEveningRateModifier;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedOnPDA;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSheetNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceInvoiceNumber;
        private DataSet_GC_ReportsTableAdapters.sp04324_GC_Analysis_Gritting_Callouts_ListTableAdapter sp04324_GC_Analysis_Gritting_Callouts_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp04325GCAnalysisGrittingCalloutsForAnalysisBindingSource;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteGrittingContractID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompanyName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientsSiteCode1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorIsDirectLabour1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOriginalSubContarctorName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldReactive1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutStatusDescription1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutStatusOrder1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCallOutDateTime1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldImportedWeatherForecastID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTextSentTime1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorReceivedTime1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorRespondedTime1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorETA1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedTime1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAuthorisedByName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldVisitAborted1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAbortedReason1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartLatitude1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartLongitude1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldFinishedLatitude1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldFinishedLongitude1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientPONumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSaltUsed1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSaltCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSaltSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSaltVatRate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHoursWorked1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTeamHourlyRate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTeamCharge1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLabourCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLabourVatRate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOtherCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOtherSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTotalCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTotalSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientInvoiceNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorPaid1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldGrittingInvoiceID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRecordedByName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPaidByName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDoNotPaySubContractor1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDoNotPaySubContractorReason1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDoNotInvoiceClient1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDoNotInvoiceClientReason1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNoAccess1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteWeather1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteTemperature1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPdaID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTeamPresent1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldServiceFailure1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldComments1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientPrice1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSnowOnSite1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldGritJobTransferMethod1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartTime1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldProfit1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMarkup1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientPOID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNonStandardCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNonStandardSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPPEWorn1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNoAccessAbortedRate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTeamMembersPresent1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSnowInfo1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEveningRateModifier1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobSheetNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedOnPDA1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldFinanceInvoiceNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutCount1;
        private DataSet_GC_ReportsTableAdapters.sp04325_GC_Analysis_Gritting_Callouts_For_AnalysisTableAdapter sp04325_GC_Analysis_Gritting_Callouts_For_AnalysisTableAdapter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedMonth;
        private DevExpress.XtraBars.BarButtonItem bbiToggleAvailableColumnsVisibility;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedWeek;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowOnSiteOver5cm;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOperativeName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSnowOnSiteOver5cm;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colPDACode;
    }
}

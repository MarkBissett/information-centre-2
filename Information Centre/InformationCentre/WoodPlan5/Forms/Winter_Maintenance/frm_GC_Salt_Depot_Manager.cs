using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Salt_Depot_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        BaseObjects.GridCheckMarksSelection selection2;
        private string i_str_selected_CallOutType_ids = "";
        private string i_str_selected_CallOutType_names = "";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        private ExtendedGridViewFunctions extGridViewFunction;
      
        #endregion

        public frm_GC_Salt_Depot_Manager()
        {
            InitializeComponent();
        }

        private void frm_GC_Salt_Depot_Manager_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4015;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            
            sp04210_GC_Salt_Depot_Manager_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "GritDepotID");

            // Add record selection checkboxes to popup Callout Type grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            
            sp04001_GC_Job_CallOut_StatusesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04001_GC_Job_CallOut_StatusesTableAdapter.Fill(dataSet_GC_Core.sp04001_GC_Job_CallOut_Statuses);
            gridControl5.ForceInitialize();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            dateEditFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditToDate.DateTime = this.GlobalSettings.ViewedEndDate;

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "LinkedDocumentID");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            Load_Data();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();
            SetMenuStatus();
        }

        private void frm_GC_Salt_Depot_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                else if (UpdateRefreshStatus == 2)
                {
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_GC_Salt_Depot_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate", dateEditFromDate.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDate", dateEditToDate.DateTime.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CallOutFilter", i_str_selected_CallOutType_ids);
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }
        
        private void btnLoad_Click(object sender, EventArgs e)
        {
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            Load_Data();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // From Date //
                string strFromDate = default_screen_settings.RetrieveSetting("FromDate");
                if (!string.IsNullOrEmpty(strFromDate)) dateEditFromDate.DateTime = Convert.ToDateTime(strFromDate);

                // To Date //
                string strToDate = default_screen_settings.RetrieveSetting("ToDate");
                if (!string.IsNullOrEmpty(strToDate)) dateEditToDate.DateTime = Convert.ToDateTime(strToDate);

                // Gritting Callout Filter //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("CallOutFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["Value"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit2.Text = PopupContainerEdit2_Get_Selected();
                }
                btnLoad.PerformClick();
            }
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            WaitDialogForm loading = new WaitDialogForm("Loading Depots...", "Salt Depot Manager");
            if (fProgress == null)
            {
                loading.Show();
            }
            else
            {
                loading.Hide();
                loading.Dispose();
            }

            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;

            // When highlighting new rows - This version does a match on 2 columns using Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //
            gridControl1.BeginUpdate();
            sp04210_GC_Salt_Depot_Manager_ListTableAdapter.Fill(dataSet_GC_Core.sp04210_GC_Salt_Depot_Manager_List, dtFromDate, dtToDate, i_str_selected_CallOutType_ids);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();
            // Highlight any recently added new rows [needs to check 2 different columns for a match (ID and DepotType ) //
            if (i_str_AddedRecordIDs1 != "")
            {
                GridColumn[] cols = new GridColumn[] { colGritDepotID, colDepotType };  // Array of columns to search for a match on //
                object[] values; // Array of objects [values] to look for //
                extGridViewFunction = new ExtendedGridViewFunctions();

                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //

                 foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    values = new object[] { intID, "Depot" };
                    intRowHandle = extGridViewFunction.LocateRowByMultipleValues(view, cols, values, 0);  // Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
            if (fProgress == null)
            {
                loading.Close();
                loading.Dispose();
            }
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GritDepotID"])) + ',';
            }

            //Populate Linked Documents //
            gridControl2.MainView.BeginUpdate();
            this.RefreshGridViewState2.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_AT.sp00220_Linked_Documents_List.Clear();
            }
            else
            {
                sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 17, strDefaultPath);
                this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl2.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl2.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Clients //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 2)  // Linked Documents //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Salt Supplier //
                    if (!iBool_AllowAdd) return;

                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                    //this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    frm_GC_Salt_Depot_Edit fChildForm = new frm_GC_Salt_Depot_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                case 2:     // Linked Documents //
                    if (!iBool_AllowAdd) return;

                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    view = (GridView)gridControl2.MainView;
                    view.PostEditor();
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                    fChildForm2.MdiParent = this.MdiParent;
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.strRecordIDs = "";
                    fChildForm2.strFormMode = "add";
                    fChildForm2.strCaller = this.Name;
                    fChildForm2.intRecordCount = 0;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intRecordTypeID = 17;  // Salt Depots //

                    ParentView = (GridView)gridControl1.MainView;
                    intRowHandles = ParentView.GetSelectedRows();
                    if (intRowHandles.Length == 1)
                    {
                        fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "GritDepotID"));
                        fChildForm2.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "DepotName"));
                    }

                    fChildForm2.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 2:     // Related Documents Link //
                    if (!iBool_AllowAdd) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GritDepotID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                    fChildForm2.MdiParent = this.MdiParent;
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.strRecordIDs = strRecordIDs;
                    fChildForm2.strFormMode = "blockadd";
                    fChildForm2.strCaller = this.Name;
                    fChildForm2.intRecordCount = intCount;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intRecordTypeID = 17;  // Salt Depots //
                    fChildForm2.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Salt Supplier //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GritDepotID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    //this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    frm_GC_Salt_Depot_Edit fChildForm = new frm_GC_Salt_Depot_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "blockedit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                case 2:     // Linked Documents //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl2.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                    fChildForm2.MdiParent = this.MdiParent;
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.strRecordIDs = strRecordIDs;
                    fChildForm2.strFormMode = "blockedit";
                    fChildForm2.strCaller = this.Name;
                    fChildForm2.intRecordCount = intCount;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intRecordTypeID = 17;  // Salt Depots //
                    fChildForm2.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Salt Supplier //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GritDepotID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    //this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    frm_GC_Salt_Depot_Edit fChildForm = new frm_GC_Salt_Depot_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                case 2:     // Linked Documents //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl2.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                    fChildForm2.MdiParent = this.MdiParent;
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.strRecordIDs = strRecordIDs;
                    fChildForm2.strFormMode = "edit";
                    fChildForm2.strCaller = this.Name;
                    fChildForm2.intRecordCount = intCount;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intRecordTypeID = 17;  // Salt Depots //
                    fChildForm2.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Salt Depots to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Salt Depot" : Convert.ToString(intRowHandles.Length) + " Salt Depots") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Salt Depot" : "these Salt Depots") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GritDepotID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //


                        DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp03002_EP_Client_Delete("gc_salt_depot", strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        Load_Data();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
        }



        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Salt Depots Available";
                    break;
                case "gridView2":
                    message = "No Linked Documents Available - Select one or more Salt Depots to view Linked Documents";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                bbiRecalculateStock.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            // This grid does not allow depot types = "Teams" to be selected //
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add)
            {
                if (View.IsGroupRow(e.ControllerRow))
                {
                    View.UnselectRow(e.ControllerRow);
                }
                else if (e.ControllerRow >= 0 && View.GetRowCellValue(e.ControllerRow, "DepotType").ToString() == "Team")
                {
                    View.UnselectRow(e.ControllerRow);
                }
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                    if (Row >= 0 && View.GetRowCellValue(Row, "DepotType").ToString() == "Team") View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            LoadLinkedRecords();
            GridView view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
            SetMenuStatus();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit3_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strEmailAddress = view.GetRowCellValue(view.FocusedRowHandle, "Email").ToString();
            if (string.IsNullOrEmpty(strEmailAddress))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Email Address Available - unable to proceed.", "Email Supplier", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start("mailto:" + strEmailAddress);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to email: " + strEmailAddress + ".", "Email Supplier", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void repositoryItemHyperLinkEditOrderCount_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strParentIDs = view.GetRowCellValue(view.FocusedRowHandle, "GritDepotID").ToString() + ",";
            string strParentType = view.GetRowCellValue(view.FocusedRowHandle, "DepotType").ToString().ToLower();
            DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            DateTime FromDate = (string.IsNullOrEmpty(dateEditFromDate.DateTime.ToString()) ? Convert.ToDateTime("2011-01-01") : dateEditFromDate.DateTime);
            DateTime ToDate = (string.IsNullOrEmpty(dateEditToDate.DateTime.ToString()) ? Convert.ToDateTime("2500-12-31") : dateEditToDate.DateTime.AddDays(1).AddSeconds(-1));
            string strRecordIDs = GetSetting.sp04236_GC_DrillDown_Get_Linked_Salt_Orders(strParentIDs, strParentType, FromDate, ToDate).ToString();
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "salt_orders");
        }

        private void repositoryItemHyperLinkEditCalloutCount_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strIDs = view.GetRowCellValue(view.FocusedRowHandle, "GritDepotID").ToString() + ",";  // This is actually ContractorID //
            DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            DateTime FromDate = (string.IsNullOrEmpty(dateEditFromDate.DateTime.ToString()) ? Convert.ToDateTime("2011-01-01") : dateEditFromDate.DateTime);
            DateTime ToDate = (string.IsNullOrEmpty(dateEditToDate.DateTime.ToString()) ? Convert.ToDateTime("2500-12-31") : dateEditToDate.DateTime.AddDays(1).AddSeconds(-1));
            string strRecordIDs = GetSetting.sp04077_GC_DrillDown_Get_Linked_Callouts(strIDs, "contractor", i_str_selected_CallOutType_ids, FromDate, ToDate).ToString();
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "site_gritting_callouts");
        }

        private void repositoryItemHyperLinkEditTransferIn_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strParentIDs = view.GetRowCellValue(view.FocusedRowHandle, "GritDepotID").ToString() + ",";
            string strParentType = view.GetRowCellValue(view.FocusedRowHandle, "DepotType").ToString().ToLower();
            DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            DateTime FromDate = (string.IsNullOrEmpty(dateEditFromDate.DateTime.ToString()) ? Convert.ToDateTime("2011-01-01") : dateEditFromDate.DateTime);
            DateTime ToDate = (string.IsNullOrEmpty(dateEditToDate.DateTime.ToString()) ? Convert.ToDateTime("2500-12-31") : dateEditToDate.DateTime.AddDays(1).AddSeconds(-1));
            string strRecordIDs = GetSetting.sp04219_GC_DrillDown_Get_Linked_Salt_Tranfers(strParentIDs, strParentType, "to", FromDate, ToDate).ToString();
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "salt_transfers");
        }

        private void repositoryItemHyperLinkEditTransferOut_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strParentIDs = view.GetRowCellValue(view.FocusedRowHandle, "GritDepotID").ToString() + ",";
            string strParentType = view.GetRowCellValue(view.FocusedRowHandle, "DepotType").ToString().ToLower();
            DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            DateTime FromDate = (string.IsNullOrEmpty(dateEditFromDate.DateTime.ToString()) ? Convert.ToDateTime("2011-01-01") : dateEditFromDate.DateTime);
            DateTime ToDate = (string.IsNullOrEmpty(dateEditToDate.DateTime.ToString()) ? Convert.ToDateTime("2500-12-31") : dateEditToDate.DateTime.AddDays(1).AddSeconds(-1));
            string strRecordIDs = GetSetting.sp04219_GC_DrillDown_Get_Linked_Salt_Tranfers(strParentIDs, strParentType, "from", FromDate, ToDate).ToString();
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "salt_transfers");
        }


        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedOrderCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedOrderCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedCalloutCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedCalloutCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedGritTransferInCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedGritTransferInCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedGritTransferOutCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedGritTransferOutCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "Email":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "Email").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }
        
        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedOrderCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedOrderCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedCalloutCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedCalloutCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedGritTransferInCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedGritTransferInCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedGritTransferOutCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedGritTransferOutCount")) == 0) e.Cancel = true;
                    break;
                case "Email":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("Email").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "CurrentGritLevel")
            {
                if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "CurrentGritLevel")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "UrgentGritLevel")))
                {
                    e.Appearance.BackColor = Color.LightCoral;
                    e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    return;
                }
                else if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "CurrentGritLevel")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WarningGritLevel")))
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    return;
                }
                else
                {
                    //e.Appearance.BackColor = Color.Transparent;
                    //e.Appearance.BackColor2 = Color.Transparent;
                    //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    return;
                }
            }
        }



        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Callout Status Filter Panel

        private void btnGritCalloutFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void popupContainerEdit2_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_CallOutType_ids = "";    // Reset any prior values first //
            i_str_selected_CallOutType_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CallOutType_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CallOutType_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CallOutType_names;
        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        #endregion


        private void bbiRecalculateStock_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            string strMessage = "";
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Salt Depots to recalculate stock levels for by clicking on them then try again.", "Recalculate Stock Levels", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Salt Depot" : Convert.ToString(intRowHandles.Length) + " Salt Depots") + " selected for recalculating the stock levels.\n\nProceed?";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Recalculating...");
                fProgress.Show();
                Application.DoEvents();

                int intRecordID = 0;
                int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                int intUpdateProgressTempCount = 0;
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter UpdateRecords = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                UpdateRecords.ChangeConnectionString(strConnectionString);
                foreach (int intRowHandle in intRowHandles)
                {
                    intRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "GritDepotID"));
                    UpdateRecords.sp04232_GC_Stock_Update_All_Totals(intRecordID, 0);  // 0 = Depot, 1 = Team //
                    
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                Load_Data();

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) recalculated.", "Recalculate Stock Levels", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }






    }
}


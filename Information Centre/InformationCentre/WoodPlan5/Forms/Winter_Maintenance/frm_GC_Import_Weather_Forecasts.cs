﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

using System.Runtime.InteropServices;  // Required for Excel Generation //
using Excel;
using GoogleDirections;

namespace WoodPlan5
{
    public partial class frm_GC_Import_Weather_Forecasts : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = true;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = true;

        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        private string strGrittingForecastType1File = "";  // Retrieved from the System_Settings table //
        private string strGrittingForecastType2File = "";  // Retrieved from the System_Settings table //
        private string strGrittingForecastFloatingFile = "";  // Retrieved from the System_Settings table //

        SqlDataAdapter sdaErrors = new SqlDataAdapter();
        DataSet dsErrors = new DataSet("NewDataSet");

        SqlDataAdapter sdaWeatherActivationCodes = new SqlDataAdapter();
        DataSet dsWeatherActivationCodes = new DataSet("NewDataSet");

        SqlDataAdapter sdaErrors2 = new SqlDataAdapter();
        DataSet dsErrors2 = new DataSet("NewDataSet");

        SqlDataAdapter sdaErrors3 = new SqlDataAdapter();
        DataSet dsErrors3 = new DataSet("NewDataSet");

        SqlDataAdapter sdaErrors4 = new SqlDataAdapter();
        DataSet dsErrors4 = new DataSet("NewDataSet");

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        
        #endregion

        public frm_GC_Import_Weather_Forecasts()
        {
            InitializeComponent();
        }

        private void frm_GC_Import_Weather_Forecasts_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4006;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04053_GC_Gritting_Import_Forecast_Get_Reference_DataTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04054_GC_Gritting_Import_Forecast_Dummy_DataTableAdapter.Connection.ConnectionString = strConnectionString;

            sp04348_GC_Gritting_Import_Floating_Forecast_DataTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04349_GC_Gritting_Import_Floating_Forecast_DataTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04351_GC_Floating_Site_Jobs_Import_DataTableAdapter.Connection.ConnectionString = strConnectionString;
            
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp04055_GC_Gritting_Import_Weather_Errors_Dummy", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ImportDate", DateTime.Today));
            sdaErrors = new SqlDataAdapter(cmd);
            sdaErrors.Fill(dsErrors, "Table");
            dsErrors.Tables[0].Rows.Clear();  // Get rid of any data as it will be populated manually later //
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            cmd = null;
            cmd = new SqlCommand("sp04056_GC_Gritting_Import_Weather_Forecast_Activation_Codes", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            sdaWeatherActivationCodes = new SqlDataAdapter(cmd);
            sdaWeatherActivationCodes.Fill(dsWeatherActivationCodes, "Table");

            cmd = null;
            cmd = new SqlCommand("sp04069_GC_Gritting_Load_Authorisation_Errors_Dummy", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            sdaErrors2 = new SqlDataAdapter(cmd);
            sdaErrors2.Fill(dsErrors2, "Table");
            dsErrors2.Tables[0].Rows.Clear();  // Get rid of any data as it will be populated manually later //

            cmd = null;  // Following handles Floating Site Forecast Import //
            cmd = new SqlCommand("sp04055_GC_Gritting_Import_Weather_Errors_Dummy", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ImportDate", DateTime.Today));
            sdaErrors3 = new SqlDataAdapter(cmd);
            sdaErrors3.Fill(dsErrors3, "Table");
            dsErrors3.Tables[0].Rows.Clear();  // Get rid of any data as it will be populated manually later //

            cmd = null;  // Following handles Floating Site Forecast Import //
            cmd = new SqlCommand("sp04055_GC_Gritting_Import_Weather_Errors_Dummy", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ImportDate", DateTime.Today));
            sdaErrors4 = new SqlDataAdapter(cmd);
            sdaErrors4.Fill(dsErrors4, "Table");
            dsErrors4.Tables[0].Rows.Clear();  // Get rid of any data as it will be populated manually later //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            SQlConn.Close();
            SQlConn.Dispose();

            btnSendFloatingClientEmails.Enabled = false;
            btnLoadFloatingJobs.Enabled = false;
            btnCreateFloatingJobs.Enabled = false;
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            System.Windows.Forms.Application.DoEvents();  // Allow Form time to repaint itself //
            SetMenuStatus();
            splitContainerControl2.SplitterPosition = this.Width / 2 - 10;  // Equally size left and right panel //
            splitContainerControl5.SplitterPosition = this.Width / 2 - 10;  // Equally size left and right panel //

            labelControlInformation1.Text = "No information present at this time.";
            hyperLinkEditErrorCount1.Text = "0 errors in Loaded File(s).";
            labelControlInformation1.Visible = true;
            hyperLinkEditErrorCount1.Visible = false;
            pictureEdit1.Image = imageCollection1.Images[6];

            labelControlInformation2.Text = "No information present at this time.";
            hyperLinkEditErrorCount2.Text = "0 errors in Loaded File(s).";
            labelControlInformation2.Visible = true;
            hyperLinkEditErrorCount2.Visible = false;
            pictureEdit2.Image = imageCollection1.Images[6];

            labelControlInformation3.Text = "No information present at this time.";
            hyperLinkEditErrorCount3.Text = "0 errors in Loaded File(s).";
            labelControlInformation3.Visible = true;
            hyperLinkEditErrorCount3.Visible = false;
            pictureEdit3.Image = imageCollection1.Images[6];

            labelControlInformation4.Text = "No information present at this time.";
            hyperLinkEditErrorCount4.Text = "0 errors in Loaded File(s).";
            labelControlInformation4.Visible = true;
            hyperLinkEditErrorCount4.Visible = false;
            pictureEdit4.Image = imageCollection1.Images[6];

        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;

            if (i_int_FocusedGrid == 1)  // Site Gritting Contracts //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
                bbiBlockAdd.Enabled = false;

                if (intRowHandles.Length >= 1)
                {
                    if (intRowHandles.Length == 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.AddRange(new string[] { "iBlockEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiBlockEdit.Enabled = true;
                    }
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 2)  // Preferred Teams //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
                bbiBlockAdd.Enabled = false;

                if (intRowHandles.Length >= 1)
                {
                    if (intRowHandles.Length == 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.AddRange(new string[] { "iBlockEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiBlockEdit.Enabled = true;
                    }
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Authorisation Grid //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
                bbiBlockAdd.Enabled = false;

                if (intRowHandles.Length >= 1)
                {
                    /*if (intRowHandles.Length == 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.AddRange(new string[] { "iBlockEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiBlockEdit.Enabled = true;
                    }*/
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 4)
            {
                view = (GridView)gridControl4.MainView;
                intRowHandles = view.GetSelectedRows();
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
                bbiBlockAdd.Enabled = false;

                if (intRowHandles.Length >= 1)
                {
                    if (intRowHandles.Length == 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.AddRange(new string[] { "iBlockEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiBlockEdit.Enabled = true;
                    }
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 5)
            {
                view = (GridView)gridControl5.MainView;
                intRowHandles = view.GetSelectedRows();
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
                bbiBlockAdd.Enabled = false;

                if (intRowHandles.Length >= 1)
                {
                    if (intRowHandles.Length == 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.AddRange(new string[] { "iBlockEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiBlockEdit.Enabled = true;
                    }
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 6)
            {
                view = (GridView)gridControl6.MainView;
                intRowHandles = view.GetSelectedRows();
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
                bbiBlockAdd.Enabled = false;

                if (intRowHandles.Length >= 1)
                {
                    if (intRowHandles.Length == 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.AddRange(new string[] { "iBlockEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiBlockEdit.Enabled = true;
                    }
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length == 1 ? true : false);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length == 1 ? true : false);
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            
            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView4 navigator custom buttons //
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length == 1 ? true : false);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView5 navigator custom buttons //
            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length == 1 ? true : false);
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView6 navigator custom buttons //
            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length == 1 ? true : false);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Gritting Sites Available - Click Load Forecast";
                    break;
                case "gridView2":
                    message = "No Gritting Sites Available - Click Load Forecast";
                    break;
                case "gridView3":
                    message = "No Site Authorisation records - Click Load Authorisation Spreadsheets";
                    break;
                case "gridView4":
                    message = "No Gritting Sites Available - Click Load Floating Site Forecast";
                    break;
                case "gridView5":
                    message = "No Gritting Sites Available - Click Load Floating Site Forecast";
                    break;
                case "gridView6":
                    message = "No Jobs Available - Click Load Jobs";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    btnMoveRight.Enabled = (view.SelectedRowsCount > 0);
                    break;
                case "gridView2":
                    btnMoveLeft.Enabled = (view.SelectedRowsCount > 0);
                    break;
                case "gridView4":
                    btnMoveRight.Enabled = (view.SelectedRowsCount > 0);
                    break;
                case "gridView5":
                    btnMoveLeft.Enabled = (view.SelectedRowsCount > 0);
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {

        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "PreferredTeam")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SubContractorID")) == 0)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    //e.Appearance.BackColor = Color.LightCoral;
                    //e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

         #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView2_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "PreferredTeam")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SubContractorID")) == 0)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    //e.Appearance.BackColor = Color.LightCoral;
                    //e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }

        }

        #endregion


        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Block_Edit()
        {
            GridView view = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Grit List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                case 2:     // No Grit List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                case 4:     // Floating Grit List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                case 5:     // Floating No Grit List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl5.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                case 6:     // Floating Job List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Grit List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                case 2:     // No Grit List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                case 4:     // Floating Grit List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                case 5:     // Floating No Grit List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl5.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                case 6:     // Floating Job List //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) + ',';
                        }
                        frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                view.SetRowCellValue(intRowHandle, "PreferredSubContractorID", fChildForm.intPreferredSubContractorID);
                                view.SetRowCellValue(intRowHandle, "PreferredTeam", fChildForm.strSelectedTeamName);
                                view.SetRowCellValue(intRowHandle, "SubContractorID", fChildForm.intSubContractorID);
                            }
                        }
                        break;
                    }
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 record" : Convert.ToString(intRowHandles.Length) + " records") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed no proactive gritting callout will be generated for" + (intCount == 1 ? "this site" : "these sites") + "!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(0);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            System.Windows.Forms.Application.DoEvents();
                            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                            int intUpdateProgressTempCount = 0;

                            gridControl1.BeginUpdate();
                            for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                            {
                                view.DeleteRow(intRowHandles[intRowHandle]);
                                intUpdateProgressTempCount++;
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    fProgress.UpdateProgress(10);
                                }
                            }
                            gridControl1.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 2:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 record" : Convert.ToString(intRowHandles.Length) + " records") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed no proactive gritting callout will be generated for" + (intCount == 1 ? "this site" : "these sites") + "!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(0);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            System.Windows.Forms.Application.DoEvents();
                            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                            int intUpdateProgressTempCount = 0;

                            gridControl2.BeginUpdate();
                            for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                            {
                                view.DeleteRow(intRowHandles[intRowHandle]);
                                intUpdateProgressTempCount++;
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    fProgress.UpdateProgress(10);
                                }
                            }
                            gridControl2.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 3:  // Spreadsheet Authorisation //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 record" : Convert.ToString(intRowHandles.Length) + " records") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed no authorisation will be generated for" + (intCount == 1 ? "this site" : "these sites") + "!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(0);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            System.Windows.Forms.Application.DoEvents();
                            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                            int intUpdateProgressTempCount = 0;

                            gridControl3.BeginUpdate();
                            for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                            {
                                view.DeleteRow(intRowHandles[intRowHandle]);
                                intUpdateProgressTempCount++;
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    fProgress.UpdateProgress(10);
                                }
                            }
                            gridControl3.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 4:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 record" : Convert.ToString(intRowHandles.Length) + " records") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed no proactive gritting callout will be generated for" + (intCount == 1 ? "this site" : "these sites") + "!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(0);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            System.Windows.Forms.Application.DoEvents();
                            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                            int intUpdateProgressTempCount = 0;

                            gridControl4.BeginUpdate();
                            for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                            {
                                view.DeleteRow(intRowHandles[intRowHandle]);
                                intUpdateProgressTempCount++;
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    fProgress.UpdateProgress(10);
                                }
                            }
                            gridControl4.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 5:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl5.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 record" : Convert.ToString(intRowHandles.Length) + " records") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed no proactive gritting callout will be generated for" + (intCount == 1 ? "this site" : "these sites") + "!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(0);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            System.Windows.Forms.Application.DoEvents();
                            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                            int intUpdateProgressTempCount = 0;

                            gridControl5.BeginUpdate();
                            for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                            {
                                view.DeleteRow(intRowHandles[intRowHandle]);
                                intUpdateProgressTempCount++;
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    fProgress.UpdateProgress(10);
                                }
                            }
                            gridControl5.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 6:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 record" : Convert.ToString(intRowHandles.Length) + " records") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed no proactive gritting callout will be generated for" + (intCount == 1 ? "this site" : "these sites") + "!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(0);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            System.Windows.Forms.Application.DoEvents();
                            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                            int intUpdateProgressTempCount = 0;

                            gridControl6.BeginUpdate();
                            for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                            {
                                view.DeleteRow(intRowHandles[intRowHandle]);
                                intUpdateProgressTempCount++;
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    fProgress.UpdateProgress(10);
                                }
                            }
                            gridControl6.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
            }
        }


        private void btnImportForecasts_Click(object sender, EventArgs e)
        {

            #region Check Files

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Forecast Import Files...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            // Check if an import had already been done today //
            int intForecastImportCount = 0;
            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter GetImportedCount = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
            GetImportedCount.ChangeConnectionString(strConnectionString);
            try
            {
                intForecastImportCount = Convert.ToInt32(GetImportedCount.sp04065_GC_Gritting_Imported_Forecast_Count_For_Date(DateTime.Today));  // Add Row to database //
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Import Count from the database.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Weather Forecasting Import Count", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intForecastImportCount > 0)
            {
                if (fProgress != null) fProgress.Hide();
                if (DevExpress.XtraEditors.XtraMessageBox.Show("WARNING...  WARNING...  WARNING...  WARNING...  WARNING...\n=========================================\n\nYou have already imported " + intForecastImportCount + (intForecastImportCount == 1 ? " Forecast" : " Forecasts") + " today!\n\nAre you sure you wish to proceed?", "Check Weather Forecasting Import Count", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                fProgress.Show();
                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            }

            // Get Forecasting filenames from System_Settings table //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strGrittingForecastType1File = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingForecastType1FilePath").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the name of Import Weather Forecast File Type 1 (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Weather Forecasting File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strGrittingForecastType2File = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingForecastType2FilePath").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the name of Import Weather Forecast File Type 2 (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Weather Forecasting File Name", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Check Files exist //
            if (!File.Exists(strGrittingForecastType1File))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The Weather Forecast File [" + strGrittingForecastType1File + "] does not exist.\n\nPlease ensure today's foecasting files have been saved to the correct location and that they have been named correctly before trying again.", "Check Weather Forecasting File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!File.Exists(strGrittingForecastType2File))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The Weather Forecast File [" + strGrittingForecastType2File + "] does not exist.\n\nPlease ensure today's foecasting files have been saved to the correct location and that they have been named correctly before trying again.", "Check Weather Forecasting File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Check that files were last modified today //
            DateTime lastWriteTime = File.GetLastWriteTime(strGrittingForecastType1File);
            //DateTime creationTime = File.GetCreationTime(strGrittingForecastType1File);
            //DateTime lastAccessTime = File.GetLastAccessTime(strGrittingForecastType1File);
            if (lastWriteTime.Date != DateTime.Today)
            {
                if (fProgress != null) fProgress.Hide();
                if (DevExpress.XtraEditors.XtraMessageBox.Show("WARNING...  WARNING...  WARNING...  WARNING...  WARNING...\n=========================================\n\nThe last modified date of [" + strGrittingForecastType1File + "] is not equal to todays date.\n\nThis file may be a historical file!\n\nAre you sure you wish to proceed?", "Check Weather Forecasting File Last Modified Date", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                fProgress.Show();
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            }

            lastWriteTime = File.GetLastWriteTime(strGrittingForecastType2File);
            //creationTime = File.GetCreationTime(strGrittingForecastType2File);
            //lastAccessTime = File.GetLastAccessTime(strGrittingForecastType2File);
            if (lastWriteTime.Date != DateTime.Today)
            {
                if (fProgress != null) fProgress.Hide();
                if (DevExpress.XtraEditors.XtraMessageBox.Show("WARNING...  WARNING...  WARNING...  WARNING...  WARNING...\n=========================================\n\nThe last modified date of [" + strGrittingForecastType2File + "] is not equal to todays date.\n\nThis file may be a historical file!\n\nAre you sure you wish to proceed?", "Check Weather Forecasting File Last Modified Date", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                fProgress.Show();
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            System.Windows.Forms.Application.DoEvents();  // Allow Form time to repaint itself //

            #endregion

            // Forecast Filenames present and dates last modified dates are valid so attempt to parse them //
            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Importing Forecast Files [File 1]...");
            fProgress.Show();
            DateTime dtToday = DateTime.Today;
            System.Windows.Forms.Application.DoEvents();

            SqlDataAdapter sdaSites = new SqlDataAdapter();
            DataSet dsSites = new DataSet("NewDataSet");
            try
            {
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ImportDate", dtToday));
                dsSites.Clear();  // Remove old values first //
                sdaSites = new SqlDataAdapter(cmd);
                sdaSites.Fill(dsSites, "Table");
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Site Gritting Contract Information [" + strGrittingForecastType1File + "].\n\nMessage = [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Import Forecasting Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            // File type 1 (GCML Winter Hazard.csv)//

            gridControl1.BeginUpdate();
            gridControl2.BeginUpdate();

            dsErrors.Tables[0].Rows.Clear();  // Clear Errors //

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.BeginDataUpdate();
            dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data.Rows.Clear();
            view.EndDataUpdate();
            view.EndUpdate();

            view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.BeginDataUpdate();
            dataSet_GC_Core.sp04054_GC_Gritting_Import_Forecast_Dummy_Data.Rows.Clear();
            view.EndDataUpdate();
            view.EndUpdate();

            labelControlInformation1.Text = "No information present at this time.";
            hyperLinkEditErrorCount1.Text = "No errors.";
            labelControlInformation1.Visible = true;
            hyperLinkEditErrorCount1.Visible = false;
            pictureEdit1.Image = imageCollection1.Images[6];

            System.Windows.Forms.Application.DoEvents();  // Allow Form time to repaint itself //
            
            int intImportedFile1Count = 0;
            int intImportedFile2Count = 0;
            int intImportedFile1ErrorCount = 0;
            int intImportedFile2ErrorCount = 0;
            int intLineNumber = 0;
            bool boolGritRequired = false;
            string strDayOfWeek = DateTime.Today.DayOfWeek.ToString().ToUpper();  // Used for comparisons on Gritting Pattern //
            string strDayOfWeekColumnToUse = "";
            switch (strDayOfWeek)
            {
                case "MONDAY":
                    strDayOfWeekColumnToUse = "GritOnMonday";
                    break;
                case "TUESDAY":
                    strDayOfWeekColumnToUse = "GritOnTuesday";
                    break;
                case "WEDNESDAY":
                    strDayOfWeekColumnToUse = "GritOnWednesday";
                    break;
                case "THURSDAY":
                    strDayOfWeekColumnToUse = "GritOnThursday";
                    break;
                case "FRIDAY":
                    strDayOfWeekColumnToUse = "GritOnFriday";
                    break;
                case "SATURDAY":
                    strDayOfWeekColumnToUse = "GritOnSaturday";
                    break;
                case "SUNDAY":
                    strDayOfWeekColumnToUse = "GritOnSunday";
                    break;
                default:
                    break;
            }


            #region Import First Spreadsheet

            try
            {
                string[] lines = File.ReadAllLines(strGrittingForecastType1File);

                int intUpdateProgressThreshhold = lines.Length / 10;
                int intUpdateProgressTempCount = 0;

                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                ArrayList arrayImportStructure = new ArrayList();
                bool boolEndOfSS = false;
                foreach (string line in lines)
                {
                    string[] columns = r.Split(line);  // Get Columns into Array //
                    if (columns.Length == 0) break;

                    // Check if we have reached end of SS //
                    for (int i = 0; i < columns.Length; i++)
                    {
                        if (columns[i].ToString() != "") break;  // Valid row so abort this check right away so no more time is wasted //
                        if (i == columns.Length-1) 
                        {
                            boolEndOfSS = true;  // If we are here all columns are blank so spreadsheet end has been reached //
                        }
                    }
                    if (boolEndOfSS) break;

                    intLineNumber++;
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }

                    if (line.StartsWith("Store Name")) continue;  // Skip line //
                    if (columns.Length != 9)
                    {
                        continue;  // Skip Row //
                    }

                    string strStoreNumber = columns[1].ToString();  // 0 based so number is 1 less than physical column number //
                    string strActivationCode = columns[7].ToString();  // 0 based so number is 1 less than physical column number //
                    
                    if (string.IsNullOrEmpty(strStoreNumber))
                    {
                        // Add record into Errors datatable [Invalid Spreadsheet line] //
                        StoreImportError(dsErrors, strGrittingForecastType1File, strStoreNumber, "", strActivationCode, "Line Number " + intLineNumber.ToString() + " - No Store Number");
                        intImportedFile1ErrorCount++;                    
                        continue;
                    }
                    if (string.IsNullOrEmpty(strActivationCode))
                    {
                        // Add record into Errors datatable [Invalid Spreadsheet line] //
                        StoreImportError(dsErrors, strGrittingForecastType1File, strStoreNumber, "", strActivationCode, "Line Number " + intLineNumber.ToString() + " - No Activation Code");
                        intImportedFile1ErrorCount++;
                        continue;
                    }

                    DataRow[] drRows = dsSites.Tables[0].Select("ForecastingTypeID = 1 and HubID = '" + strStoreNumber + "'");
                    if (drRows.Length > 0)
                    {
                        //for (int i = drRows.Length - 1; i >= 0; i--)

                        foreach (DataRow drMatch in drRows)
                        {
                            boolGritRequired = false;  // Reset flag //
                            // Database row found so see if a grit is required //
                            // Find Activation Point in database for spreadsheet activation code and gets it's numeric corresponding value to see if it is greater than that for the site //
                            DataRow[] drFilteredActivationCodes = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivationCode + "'");
                            if (drFilteredActivationCodes.Length > 0)
                            {
                                DataRow dr = drFilteredActivationCodes[0];
                                //if (Convert.ToInt32(dr["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()))
                                if (!DBNull.Value.Equals(dr["GrittingActivationCodeValue"]) && !DBNull.Value.Equals(drMatch["GrittingActivationCodeValue"]) && Convert.ToInt32(dr["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()))
                                {
                                    if (Convert.ToInt32(drMatch["HolidayCount"]) > 0)  // Site on holiday so No Gritting Required //
                                    {
                                        boolGritRequired = false;
                                    }
                                    else if (Convert.ToInt32(drMatch[strDayOfWeekColumnToUse]) == 0)  // Site doesn't have a site gritting pattern for today so No Gritting Required //
                                    {
                                        boolGritRequired = false;
                                    }
                                    else  // Grit //
                                    {
                                        drMatch["ForecastActivationPoint"] = strActivationCode;
                                        if (strActivationCode.ToUpper() == "D" || strActivationCode.ToUpper() == "E")
                                        {
                                            drMatch["SnowForcasted"] = 1;
                                            drMatch["RequiredGritAmount"] = Convert.ToDecimal(drMatch["DefaultGritAmount"]) * (decimal)2.00;
                                            drMatch["DoubleGrit"] = 1;
                                        }
                                        else
                                        {
                                            drMatch["SnowForcasted"] = 0;
                                            drMatch["RequiredGritAmount"] = drMatch["DefaultGritAmount"];
                                            drMatch["DoubleGrit"] = 0;
                                        }
                                        drMatch["ForecastForEmail"] = strActivationCode;
                                        dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data.ImportRow(drMatch);  // Only add first row //
                                        intImportedFile1Count++;
                                        boolGritRequired = true;
                                    }
                                }
                                if (!boolGritRequired)  // No Gritting Required //
                                {
                                    drMatch["ForecastActivationPoint"] = strActivationCode;
                                    if (strActivationCode.ToUpper() == "D" || strActivationCode.ToUpper() == "E")
                                    {
                                        drMatch["SnowForcasted"] = 1;
                                        drMatch["RequiredGritAmount"] = Convert.ToDecimal(drMatch["DefaultGritAmount"]) * (decimal)2.00;
                                        drMatch["DoubleGrit"] = 1;
                                    }
                                    else
                                    {
                                        drMatch["SnowForcasted"] = 0;
                                        drMatch["RequiredGritAmount"] = drMatch["DefaultGritAmount"];
                                        drMatch["DoubleGrit"] = 0;
                                    }
                                    if (string.IsNullOrEmpty(drMatch["SkippedReason"].ToString())) drMatch["SkippedReason"] = "Weather Not Activated";
                                    dataSet_GC_Core.sp04054_GC_Gritting_Import_Forecast_Dummy_Data.ImportRow(drMatch);  // Only add first row //
                                    intImportedFile1Count++;
                                }
                            }
                            dsSites.Tables[0].Rows.Remove(drMatch);  // Remove any matches from original dataset as it's been processed and this will speed up subsequent records as it will be a smaller dataset to search //
                        }
                    }
                    else
                    {
                        // Add record into Errors datatable [Not present in Database] //
                        StoreImportError(dsErrors, strGrittingForecastType1File, strStoreNumber, "", strActivationCode, "Site Not Present in Database");
                        intImportedFile1ErrorCount++;
                        continue;
                    }

                }
                // Check if any records left which have not been moved to the import process (if yes, they are missing from Import Spreadsheet) //
                DataRow[] drRemainingRows = dsSites.Tables[0].Select("ForecastingTypeID = 1");  // GCML Winter Hazard CSV //
                if (drRemainingRows.Length > 0)
                {
                    // Add record into Errors datatable [Not present in Spreadsheet] //
                    foreach (DataRow dr in drRemainingRows)
                    {
                        StoreImportError(dsErrors, strGrittingForecastType1File, dr["HubID"].ToString(), dr["SiteName"].ToString(), dr["GrittingActivationCode"].ToString(), "Site Not Present in Import Spreadsheet");
                        intImportedFile1ErrorCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                gridControl1.EndUpdate();
                gridControl2.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Forecast File [" + strGrittingForecastType1File + "].\n\nMessage = [" + ex.Message + "].\n\nThere may be a fault with the file. Please close this screen then try again. If the problem persists, contact Technical Support.", "Load Forecasting Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            #endregion


            #region Import Second Spreadsheet

            intLineNumber = 0;
            try
            {
                string[] lines = File.ReadAllLines(strGrittingForecastType2File);

                int intUpdateProgressThreshhold = lines.Length / 10;
                int intUpdateProgressTempCount = 0;

                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                ArrayList arrayImportStructure = new ArrayList();
                foreach (string line in lines)
                {
                    boolGritRequired = false; 
                    intLineNumber++;
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                    
                    //if (intLineNumber <= 5) continue;  // Skip first 5 lines //
                    string[] columns = r.Split(line);  // Get Columns into Array //
                    if (columns.Length < 120) continue;  // was 18 //
                    if (columns[0].ToString() == "" && columns[1].ToString().StartsWith("Region")) continue;
                    if (columns[0].ToString() == "" && columns[1].ToString() == "" && columns[2].ToString() == "") continue;

                    // Check for Snow //
                    bool boolSnowForecast = false;
                    /*if (!(string.IsNullOrEmpty(columns[5].ToString()) || columns[5].ToString() == "0") ||
                        !(string.IsNullOrEmpty(columns[10].ToString()) || columns[10].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[15].ToString()) || columns[15].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[20].ToString()) || columns[20].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[25].ToString()) || columns[25].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[30].ToString()) || columns[30].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[35].ToString()) || columns[35].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[40].ToString()) || columns[40].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[45].ToString()) || columns[45].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[50].ToString()) || columns[50].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[55].ToString()) || columns[55].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[60].ToString()) || columns[60].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[65].ToString()) || columns[65].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[70].ToString()) || columns[70].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[75].ToString()) || columns[75].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[80].ToString()) || columns[80].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[85].ToString()) || columns[85].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[90].ToString()) || columns[90].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[95].ToString()) || columns[95].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[100].ToString()) || columns[100].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[105].ToString()) || columns[105].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[110].ToString()) || columns[110].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[115].ToString()) || columns[115].ToString() == "0") ||   
                        !(string.IsNullOrEmpty(columns[120].ToString()) || columns[120].ToString() == "0") )*/
                    if ((!string.IsNullOrEmpty(columns[6].ToString()) && columns[6].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[11].ToString()) && columns[11].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[16].ToString()) && columns[16].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[21].ToString()) && columns[21].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[26].ToString()) && columns[26].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[31].ToString()) && columns[31].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[36].ToString()) && columns[36].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[41].ToString()) && columns[41].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[46].ToString()) && columns[46].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[51].ToString()) && columns[51].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[56].ToString()) && columns[56].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[61].ToString()) && columns[61].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[66].ToString()) && columns[66].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[71].ToString()) && columns[71].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[76].ToString()) && columns[76].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[81].ToString()) && columns[81].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[86].ToString()) && columns[86].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[91].ToString()) && columns[91].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[96].ToString()) && columns[96].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[101].ToString()) && columns[101].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[106].ToString()) && columns[106].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[111].ToString()) && columns[111].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[116].ToString()) && columns[116].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[121].ToString()) && columns[121].ToString().ToUpper() == "SNOW"))
                   
                    {
                        boolSnowForecast = true;
                    }

                    string strStoreNumber = columns[0].ToString();

                    // Following is for testing //
                    //if (strStoreNumber == "DD2")
                    //{
                    //    string strTest = "";
                    //}

                    string strActivation1Code = "";
                    string strActivationCodeExtra = "";
                    if (string.IsNullOrEmpty(strStoreNumber))
                    {
                        // Add record into Errors datatable [Invalid Spreadsheet line] //
                        StoreImportError(dsErrors, strGrittingForecastType2File, strStoreNumber, "", strActivation1Code, "Line Number " + intLineNumber.ToString() + " - No Store Number");
                        intImportedFile2ErrorCount++;
                        continue;
                    }
                     
                    DataRow[] drRows = dsSites.Tables[0].Select("(ForecastingTypeID = 2 or ForecastingTypeID = 3) and HubID = '" + strStoreNumber + "'");
                    if (drRows.Length > 0)
                    {
                        foreach (DataRow drMatch in drRows)
                        {
                            // Database row found so see if a grit is required //
                            int intForecastType = Convert.ToInt32(drMatch["ForecastingTypeID"].ToString());
                            if (intForecastType == 2)  // Using Colour only //
                            {
                                strActivation1Code = columns[2].ToString();
                                if (string.IsNullOrEmpty(strActivation1Code))
                                {
                                    // Add record into Errors datatable [Invalid Spreadsheet line] //
                                    StoreImportError(dsErrors, strGrittingForecastType2File, strStoreNumber, "", strActivation1Code, "Line Number " + intLineNumber.ToString() + " - No Activation Code");
                                    intImportedFile2ErrorCount++;
                                    continue;
                                }
                                drMatch["ForecastActivationPoint"] = strActivation1Code;
                                // Find Activation Point in database for spreadsheet activation code and gets it's numeric corresponding value to see if it is greater than that for the site //
                                DataRow[] drFilteredActivationCodes = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation1Code + "'");
                                if (drFilteredActivationCodes.Length > 0)
                                {
                                    // Match found so see if a grit is required //
                                    DataRow dr = drFilteredActivationCodes[0];
                                    if (Convert.ToInt32(dr["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()))
                                    {
                                        if (Convert.ToInt32(drMatch["HolidayCount"]) > 0)  // Site on holiday so No Gritting Required //
                                        {
                                            boolGritRequired = false;
                                        }
                                        else if (Convert.ToInt32(drMatch[strDayOfWeekColumnToUse]) == 0)  // Site doesn't have a site gritting pattern for today so No Gritting Required //
                                        {
                                            boolGritRequired = false;
                                        }
                                        else  // Grit //
                                        {
                                            drMatch["SnowForcasted"] = (boolSnowForecast ? 1 : 0);
                                            drMatch["RequiredGritAmount"] = (boolSnowForecast ? Convert.ToDecimal(drMatch["DefaultGritAmount"]) * (decimal)2.00 : Convert.ToDecimal(drMatch["DefaultGritAmount"]));
                                            drMatch["DoubleGrit"] = (boolSnowForecast ? 1 : 0);
                                            drMatch["ForecastForEmail"] = strActivation1Code;

                                            dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data.ImportRow(drMatch);  // Only add first row //
                                            intImportedFile1Count++;
                                            boolGritRequired = true;
                                        }
                                    }
                                    else
                                    {
                                        boolGritRequired = false;
                                    }
                                }

                            }
                            else  // intForecastType = 3 - Using Minimum Temperature and Road State - only uses [00:00 - 12:00] sets of data. Commented out code is [18:00 - 06:00] (sets of data) kept in case Lizzie changes her mind again!!!) //  
                            {
                                strActivationCodeExtra = columns[2].ToString();  // Get Colour - change requested by Lizzie on 07/12/2011 //
                                if (string.IsNullOrEmpty(strActivationCodeExtra)) strActivationCodeExtra = "";
                                int intRedOverridesBanding = Convert.ToInt32(drMatch["RedOverridesBanding"]);  // This requested by Lizzie on 05/08/2012 //

                                /*strActivation1Code = columns[36].ToString();  // Road State 18:00 //
                                string strActivation2Code = columns[41].ToString();  // Road State 19:00 //
                                string strActivation3Code = columns[46].ToString();
                                string strActivation4Code = columns[51].ToString();
                                string strActivation5Code = columns[56].ToString();
                                string strActivation6Code = columns[61].ToString();
                                string strActivation7Code = columns[66].ToString();
                                string strActivation8Code = columns[71].ToString();
                                string strActivation9Code = columns[76].ToString();
                                string strActivation10Code = columns[81].ToString();
                                string strActivation11Code = columns[86].ToString();
                                string strActivation12Code = columns[91].ToString();
                                string strActivation13Code = columns[96].ToString();

                                string strMinTemperature1 = columns[34].ToString();  // Min Temp, 18:00 //
                                string strMinTemperature2 = columns[39].ToString();  // Min Temp, 19:00 //
                                string strMinTemperature3 = columns[44].ToString();
                                string strMinTemperature4 = columns[49].ToString();
                                string strMinTemperature5 = columns[54].ToString();
                                string strMinTemperature6 = columns[59].ToString();
                                string strMinTemperature7 = columns[64].ToString();
                                string strMinTemperature8 = columns[69].ToString();
                                string strMinTemperature9 = columns[74].ToString();
                                string strMinTemperature10 = columns[79].ToString();
                                string strMinTemperature11 = columns[84].ToString();
                                string strMinTemperature12 = columns[89].ToString();
                                string strMinTemperature13 = columns[94].ToString();*/

                                strActivation1Code = columns[66].ToString();  // Road State 00:00 //
                                string strActivation2Code = columns[71].ToString();  // Road State 01:00 //
                                string strActivation3Code = columns[76].ToString();
                                string strActivation4Code = columns[81].ToString();
                                string strActivation5Code = columns[86].ToString();
                                string strActivation6Code = columns[91].ToString();
                                string strActivation7Code = columns[96].ToString();
                                string strActivation8Code = columns[101].ToString();
                                string strActivation9Code = columns[106].ToString();
                                string strActivation10Code = columns[111].ToString();
                                string strActivation11Code = columns[116].ToString();
                                string strActivation12Code = columns[121].ToString();
                                string strActivation13Code = columns[6].ToString();

                                string strMinTemperature1 = columns[64].ToString();  // Min Temp, 00:00 //
                                string strMinTemperature2 = columns[69].ToString();  // Min Temp, 01:00 //
                                string strMinTemperature3 = columns[74].ToString();
                                string strMinTemperature4 = columns[79].ToString();
                                string strMinTemperature5 = columns[84].ToString();
                                string strMinTemperature6 = columns[89].ToString();
                                string strMinTemperature7 = columns[94].ToString();
                                string strMinTemperature8 = columns[99].ToString();
                                string strMinTemperature9 = columns[104].ToString();
                                string strMinTemperature10 = columns[109].ToString();
                                string strMinTemperature11 = columns[114].ToString();
                                string strMinTemperature12 = columns[119].ToString();
                                string strMinTemperature13 = columns[4].ToString();

                                if (string.IsNullOrEmpty(strActivation1Code) || 
                                    string.IsNullOrEmpty(strActivation2Code) || 
                                    string.IsNullOrEmpty(strActivation3Code) || 
                                    string.IsNullOrEmpty(strActivation4Code) || 
                                    string.IsNullOrEmpty(strActivation5Code) || 
                                    string.IsNullOrEmpty(strActivation6Code) || 
                                    string.IsNullOrEmpty(strActivation7Code) || 
                                    string.IsNullOrEmpty(strActivation8Code) || 
                                    string.IsNullOrEmpty(strActivation9Code) || 
                                    string.IsNullOrEmpty(strActivation10Code) || 
                                    string.IsNullOrEmpty(strActivation11Code) || 
                                    string.IsNullOrEmpty(strActivation12Code) || 
                                    string.IsNullOrEmpty(strActivation13Code) )
                                {
                                    // Add record into Errors datatable [Invalid Spreadsheet line] //
                                    StoreImportError(dsErrors, strGrittingForecastType2File, strStoreNumber, "", "", "Line Number " + intLineNumber.ToString() + " - No Road State");
                                    intImportedFile2ErrorCount++;
                                    continue;
                                }
                                drMatch["ForecastActivationPoint"] = strActivationCodeExtra + ", " + strActivation1Code + ", " + strActivation2Code + ", " + strActivation3Code + ", " + strActivation4Code + ", " + strActivation5Code + ", " + strActivation6Code + ", " + strActivation7Code + ", " + strActivation8Code + ", " + strActivation9Code + ", " + strActivation10Code + ", " + strActivation11Code + ", " + strActivation12Code + ", " + strActivation13Code;
                                if (string.IsNullOrEmpty(strMinTemperature1) || 
                                    string.IsNullOrEmpty(strMinTemperature2) ||
                                    string.IsNullOrEmpty(strMinTemperature3) ||
                                    string.IsNullOrEmpty(strMinTemperature4) ||
                                    string.IsNullOrEmpty(strMinTemperature5) ||
                                    string.IsNullOrEmpty(strMinTemperature6) ||
                                    string.IsNullOrEmpty(strMinTemperature7) ||
                                    string.IsNullOrEmpty(strMinTemperature8) ||
                                    string.IsNullOrEmpty(strMinTemperature9) ||
                                    string.IsNullOrEmpty(strMinTemperature10) ||
                                    string.IsNullOrEmpty(strMinTemperature11) ||
                                    string.IsNullOrEmpty(strMinTemperature12) ||
                                    string.IsNullOrEmpty(strMinTemperature13) )
                                {
                                    // Add record into Errors datatable [Invalid Spreadsheet line] //
                                    StoreImportError(dsErrors, strGrittingForecastType2File, strStoreNumber, "", "", "Line Number " + intLineNumber.ToString() + " - No Temperature");
                                    intImportedFile2ErrorCount++;
                                    continue;
                                }
                                drMatch["ForecastActivationPoint"] += ", " + strMinTemperature1 + ", " + strMinTemperature2 + ", " + strMinTemperature3 + ", " + strMinTemperature4 + ", " + strMinTemperature5 + ", " + strMinTemperature6 + ", " + strMinTemperature7 + ", " + strMinTemperature8 + ", " + strMinTemperature9 + ", " + strMinTemperature10 + ", " + strMinTemperature11 + ", " + strMinTemperature12 + ", " + strMinTemperature13;
                                // Check All temperatures are numeric decimals //
                                decimal decMinTemperature1;
                                bool result1 = decimal.TryParse(strMinTemperature1, out decMinTemperature1);
                                decimal decMinTemperature2;
                                bool result2 = decimal.TryParse(strMinTemperature2, out decMinTemperature2);
                                decimal decMinTemperature3;
                                bool result3 = decimal.TryParse(strMinTemperature3, out decMinTemperature3);
                                decimal decMinTemperature4;
                                bool result4 = decimal.TryParse(strMinTemperature4, out decMinTemperature4);
                                decimal decMinTemperature5;
                                bool result5 = decimal.TryParse(strMinTemperature5, out decMinTemperature5);
                                decimal decMinTemperature6;
                                bool result6 = decimal.TryParse(strMinTemperature6, out decMinTemperature6);
                                decimal decMinTemperature7;
                                bool result7 = decimal.TryParse(strMinTemperature7, out decMinTemperature7);
                                decimal decMinTemperature8;
                                bool result8 = decimal.TryParse(strMinTemperature8, out decMinTemperature8);
                                decimal decMinTemperature9;
                                bool result9 = decimal.TryParse(strMinTemperature9, out decMinTemperature9);
                                decimal decMinTemperature10;
                                bool result10 = decimal.TryParse(strMinTemperature10, out decMinTemperature10);
                                decimal decMinTemperature11;
                                bool result11 = decimal.TryParse(strMinTemperature11, out decMinTemperature11);
                                decimal decMinTemperature12;
                                bool result12 = decimal.TryParse(strMinTemperature12, out decMinTemperature12);
                                decimal decMinTemperature13;
                                bool result13 = decimal.TryParse(strMinTemperature13, out decMinTemperature13);
                                if (!result1 || !result2 || !result3 || !result4 || !result5 || !result6 || !result7 || !result8 || !result9 || !result10 || !result11 || !result12 || !result13)
                                {
                                    // Add record into Errors datatable [Invalid Spreadsheet line] //
                                    StoreImportError(dsErrors, strGrittingForecastType2File, strStoreNumber, "", "", "Line Number " + intLineNumber.ToString() + " - No Temperature");
                                    intImportedFile2ErrorCount++;
                                    continue;
                                }

                                // Find Activation Point in database for spreadsheet activation code and gets it's numeric corresponding value to see if it is greater than that for the site //
                                DataRow[] drFilteredActivationCodes = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation1Code + "'");
                                DataRow[] drFilteredActivationCodes2 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation2Code + "'");
                                DataRow[] drFilteredActivationCodes3 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation3Code + "'");
                                DataRow[] drFilteredActivationCodes4 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation4Code + "'");
                                DataRow[] drFilteredActivationCodes5 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation5Code + "'");
                                DataRow[] drFilteredActivationCodes6 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation6Code + "'");
                                DataRow[] drFilteredActivationCodes7 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation7Code + "'");
                                DataRow[] drFilteredActivationCodes8 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation8Code + "'");
                                DataRow[] drFilteredActivationCodes9 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation9Code + "'");
                                DataRow[] drFilteredActivationCodes10 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation10Code + "'");
                                DataRow[] drFilteredActivationCodes11 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation11Code + "'");
                                DataRow[] drFilteredActivationCodes12 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation12Code + "'");
                                DataRow[] drFilteredActivationCodes13 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation13Code + "'");
                                if (drFilteredActivationCodes.Length > 0 && 
                                    drFilteredActivationCodes2.Length > 0 &&
                                    drFilteredActivationCodes3.Length > 0 &&
                                    drFilteredActivationCodes4.Length > 0 &&
                                    drFilteredActivationCodes5.Length > 0 &&
                                    drFilteredActivationCodes6.Length > 0 &&
                                    drFilteredActivationCodes7.Length > 0 &&
                                    drFilteredActivationCodes8.Length > 0 &&
                                    drFilteredActivationCodes9.Length > 0 &&
                                    drFilteredActivationCodes10.Length > 0 &&
                                    drFilteredActivationCodes11.Length > 0 &&
                                    drFilteredActivationCodes12.Length > 0 &&
                                    drFilteredActivationCodes13.Length > 0 )
                                {
                                    // Match found so see if a grit is required //
                                    DataRow dr = drFilteredActivationCodes[0];
                                    DataRow dr2 = drFilteredActivationCodes2[0];
                                    DataRow dr3 = drFilteredActivationCodes3[0];
                                    DataRow dr4 = drFilteredActivationCodes4[0];
                                    DataRow dr5 = drFilteredActivationCodes5[0];
                                    DataRow dr6 = drFilteredActivationCodes6[0];
                                    DataRow dr7 = drFilteredActivationCodes7[0];
                                    DataRow dr8 = drFilteredActivationCodes8[0];
                                    DataRow dr9 = drFilteredActivationCodes9[0];
                                    DataRow dr10 = drFilteredActivationCodes10[0];
                                    DataRow dr11 = drFilteredActivationCodes11[0];
                                    DataRow dr12 = drFilteredActivationCodes12[0];
                                    DataRow dr13 = drFilteredActivationCodes13[0];
                                    if ((Convert.ToInt32(dr["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature1) ||
                                       (Convert.ToInt32(dr2["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature2) ||
                                       (Convert.ToInt32(dr3["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature3) ||
                                       (Convert.ToInt32(dr4["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature4) ||
                                       (Convert.ToInt32(dr5["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature5) ||
                                       (Convert.ToInt32(dr6["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature6) ||
                                       (Convert.ToInt32(dr7["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature7) ||
                                       (Convert.ToInt32(dr8["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature8) ||
                                       (Convert.ToInt32(dr9["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature9) ||
                                       (Convert.ToInt32(dr10["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature10) ||
                                       (Convert.ToInt32(dr11["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature11) ||
                                       (Convert.ToInt32(dr12["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature12) ||
                                       (Convert.ToInt32(dr13["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature13) ||
                                        (strActivationCodeExtra.ToLower() == "red" && intRedOverridesBanding == 1))
                                    {
                                        if (Convert.ToInt32(drMatch["HolidayCount"]) > 0)  // Site on holiday so No Gritting Required //
                                        {
                                            boolGritRequired = false;
                                        }
                                        else if (Convert.ToInt32(drMatch[strDayOfWeekColumnToUse]) == 0)  // Site doesn't have a site gritting pattern for today so No Gritting Required //
                                        {
                                            boolGritRequired = false;
                                        }
                                        else  // Grit //
                                        {
                                            drMatch["SnowForcasted"] = (boolSnowForecast ? 1 : 0);
                                            drMatch["RequiredGritAmount"] = (boolSnowForecast ? Convert.ToDecimal(drMatch["DefaultGritAmount"]) * (decimal)2.00 : Convert.ToDecimal(drMatch["DefaultGritAmount"]));
                                            drMatch["DoubleGrit"] = (boolSnowForecast ? 1 : 0);
                                            drMatch["ForecastForEmail"] = columns[2].ToString();  // Store Colour not Temperature and Road State here //
                                            dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data.ImportRow(drMatch);  // Only add first row //
                                            intImportedFile1Count++;
                                            boolGritRequired = true;
                                        }
                                    }
                                    else
                                    {
                                        boolGritRequired = false;
                                    }
                                }
                            }
                            if (!boolGritRequired)  // No Gritting Required //
                            {
                                drMatch["SnowForcasted"] = (boolSnowForecast ? 1 : 0);
                                drMatch["RequiredGritAmount"] = (boolSnowForecast ? Convert.ToDecimal(drMatch["DefaultGritAmount"]) * (decimal)2.00 : Convert.ToDecimal(drMatch["DefaultGritAmount"]));
                                drMatch["DoubleGrit"] = (boolSnowForecast ? 1 : 0);
                                drMatch["ForecastForEmail"] = columns[2].ToString();  // Store Colour not Temperature and Road State here //
                                if (string.IsNullOrEmpty(drMatch["SkippedReason"].ToString())) drMatch["SkippedReason"] = "Weather Not Activated";
                                dataSet_GC_Core.sp04054_GC_Gritting_Import_Forecast_Dummy_Data.ImportRow(drMatch);  // Only add first row //
                                intImportedFile1Count++;
                            }
                            dsSites.Tables[0].Rows.Remove(drMatch);  // Remove any matches from original dataset as it's been processed and this will speed up subsequent records as it will be a smaller dataset to search //
                        }
                    }
                    else
                    {
                        // Add record into Errors datatable [Not present in Database] //
                        StoreImportError(dsErrors, strGrittingForecastType2File, strStoreNumber, "", strActivation1Code, "Site Not Present in Database");
                        intImportedFile2ErrorCount++;
                        continue;
                    }
                    

                }
                // Check if any records left which have not been moved to the import process (if yes, they are missing from Import Spreadsheet) //
                DataRow[] drRemainingRows = dsSites.Tables[0].Select("ForecastingTypeID = 2 or ForecastingTypeID = 3");  // GCML Winter Hazard CSV //
                if (drRemainingRows.Length > 0)
                {
                    // Add record into Errors datatable [Not present in Spreadsheet] //
                    foreach (DataRow dr in drRemainingRows)
                    {
                        StoreImportError(dsErrors, strGrittingForecastType2File, dr["HubID"].ToString(), dr["SiteName"].ToString(), dr["GrittingActivationCode"].ToString(), "Site Not Present in Import Spreadsheet");
                        intImportedFile2ErrorCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                gridControl1.EndUpdate();
                gridControl2.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Importing Forecast File [" + strGrittingForecastType1File + "].\n\nMessage = [" + ex.Message + "].\n\nThere may be a fault with the file. Please close this screen then try again. If the problem persists, contact Technical Support.", "Import Forecasting Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            #endregion


            view = (GridView)gridControl1.MainView;
            view.ExpandAllGroups();
            view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
            gridControl1.EndUpdate();
            gridControl2.EndUpdate();

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            if (intImportedFile1ErrorCount == 0 && intImportedFile2ErrorCount == 0)
            {
                labelControlInformation1.Text = intImportedFile1Count.ToString() + (intImportedFile1Count == 1 ? " record" : " records") + " loaded from File 1. " + intImportedFile2Count.ToString() + (intImportedFile1Count == 2 ? " record" : " records") + " loaded from File 2.";
                hyperLinkEditErrorCount1.Text = "No errors.";
                labelControlInformation1.Visible = true;
                hyperLinkEditErrorCount1.Visible = false;
                pictureEdit1.Image = imageCollection1.Images[6];
            }
            else
            {
                labelControlInformation1.Text = "No information present at this time.";
                hyperLinkEditErrorCount1.Text = intImportedFile1ErrorCount.ToString() + (intImportedFile1ErrorCount == 1 ? " error" : " errors") + " in File 1. " + intImportedFile2ErrorCount.ToString() + (intImportedFile2ErrorCount == 2 ? " error" : " errors") + " in File 2.";
                labelControlInformation1.Visible = false;
                hyperLinkEditErrorCount1.Visible = true;
                pictureEdit1.Image = imageCollection1.Images[7];
            }
        }

        private void StoreImportError(DataSet ds, string strFileName, string HubID, string strSiteName, string strActivationPoint, string strReason)
        {
            DataRow drNewRow = ds.Tables[0].NewRow();
            drNewRow["FileName"] = strFileName;
            drNewRow["HubID"] = HubID;
            drNewRow["SiteName"] = strSiteName;
            drNewRow["ActivationCode"] = strActivationPoint;
            drNewRow["ErrorReason"] = strReason;
            ds.Tables[0].Rows.Add(drNewRow);
        }



        private void hyperLinkEditErrorCount1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            frm_GC_Import_Weather_Forecasts_Import_Errors frm_child= new frm_GC_Import_Weather_Forecasts_Import_Errors();
            //frm_styles.MdiParent = this.MdiParent;
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child.dsErrors = this.dsErrors;
            frm_child.ShowDialog();
        }

        private void btnMoveRight_Click(object sender, EventArgs e)
        {
            GridView viewFrom = (GridView)gridControl1.MainView;
            GridView viewTo = (GridView)gridControl2.MainView;
            int[] intRowHandles = viewFrom.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to move between lists before proceeding.", "Move Record(s) Between Lists", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Record" : intRowHandles.Length.ToString() + " Records") + " selected for moving between lists!\n\nProceed?", "Move Record(s) Between Lists", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Deleting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = (intRowHandles.Length * 2) / 10;  // Multiplied by 2 as we need 2 passes first to move rows in correct order, second pass (in reverse) to delete rows from source //
            int intUpdateProgressTempCount = 0;

            System.Data.DataTable dataTableTo = this.dataSet_GC_Core.sp04054_GC_Gritting_Import_Forecast_Dummy_Data;
            viewFrom.BeginUpdate();
            viewTo.BeginUpdate();
            viewTo.BeginSelection();
            viewTo.ClearSelection();  // Remove any selected records //
            int intID = 0;
            int intFoundNewRow = 0;
            //for (int i = intRowHandles.Length - 1; i >= 0; i--)

            // 2 passes first to move rows in correct order, second pass (in reverse) to delete rows from source //
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                DataRow row = viewFrom.GetDataRow(intRowHandles[i]);
                intID = Convert.ToInt32(row["SiteGrittingContractID"]);
                if (row != null && row.Table != dataTableTo)
                {
                    dataTableTo.ImportRow(row);
                    //row.Delete();
                    // Highlight and make visible moved row //
                    intFoundNewRow = viewTo.LocateByValue(0, viewTo.Columns["SiteGrittingContractID"], intID);
                    if (intFoundNewRow != GridControl.InvalidRowHandle)
                    {
                        viewTo.SelectRow(intFoundNewRow);
                        viewTo.MakeRowVisible(intFoundNewRow, false);
                    }
                }
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            for (int i = intRowHandles.Length - 1; i >= 0; i--)
            {
                DataRow row = viewFrom.GetDataRow(intRowHandles[i]);
                row.Delete();
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            viewTo.EndSelection();
            viewTo.EndUpdate();
            viewFrom.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void btnMoveLeft_Click(object sender, EventArgs e)
        {
            GridView viewFrom = (GridView)gridControl2.MainView;
            GridView viewTo = (GridView)gridControl1.MainView;
            int[] intRowHandles = viewFrom.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to move between lists before proceeding.", "Move Record(s) Between Lists", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Record" : intRowHandles.Length.ToString() + " Records") + " selected for moving between lists!\n\nProceed?", "Move Record(s) Between Lists", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Deleting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = (intRowHandles.Length * 2) / 10;  // Multiplied by 2 as we need 2 passes first to move rows in correct order, second pass (in reverse) to delete rows from source //
            int intUpdateProgressTempCount = 0;

            System.Data.DataTable dataTableTo = this.dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data;
            viewFrom.BeginUpdate();
            viewTo.BeginUpdate();
            viewTo.BeginSelection();
            viewTo.ClearSelection();  // Remove any selected records //
            int intID = 0;
            int intFoundNewRow = 0;
            //for (int i = intRowHandles.Length - 1; i >= 0; i--)

            // 2 passes first to move rows in correct order, second pass (in reverse) to delete rows from source //
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                DataRow row = viewFrom.GetDataRow(intRowHandles[i]);
                intID = Convert.ToInt32(row["SiteGrittingContractID"]);
                if (row != null && row.Table != dataTableTo)
                {
                    dataTableTo.ImportRow(row);
                    //row.Delete();
                    // Highlight and make visible moved row //
                    intFoundNewRow = viewTo.LocateByValue(0, viewTo.Columns["SiteGrittingContractID"], intID);
                    if (intFoundNewRow != GridControl.InvalidRowHandle)
                    {
                        viewTo.SelectRow(intFoundNewRow);
                        viewTo.MakeRowVisible(intFoundNewRow, false);
                    }
                }
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            for (int i = intRowHandles.Length - 1; i >= 0; i--)
            {
                DataRow row = viewFrom.GetDataRow(intRowHandles[i]);
                row.Delete();
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }

            viewTo.EndSelection();
            viewTo.EndUpdate();
            viewFrom.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void btnSendConfirmationEmails_Click(object sender, EventArgs e)
        {
            // Check for internet Connectivity //
            bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
            if (!boolNoInternet) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send emails.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            #region GetConfigurationSettings
            string strAuthorisationGeneratedFileType = "Excel";
            int intOddBackColour = 0;
            int intEvenBackColour = 0;
            try
            {
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp04073_GC_Import_Spreadsheets_Create_Emails_Get_Settings", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
                DataSet dsSettings = new DataSet("NewDataSet");
                sdaSettings.Fill(dsSettings, "Table");
                conn.Close();
                conn.Dispose();
                if (dsSettings.Tables[0].Rows.Count != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                DataRow dr = dsSettings.Tables[0].Rows[0];
                strAuthorisationGeneratedFileType = dr["GrittingAuthorisationGeneratedFileType"].ToString();
                intOddBackColour = Convert.ToInt32(dr["GrittingAuthorisationOddRowColour"]);
                intEvenBackColour = Convert.ToInt32(dr["GrittingAuthorisationEvenRowColour"]);

            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Email Settings' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            /*string strAuthorisationGeneratedFileType = "Excel";
            // Get Authorisation Generated File Type from System_Settings table //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strAuthorisationGeneratedFileType = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingAuthorisationGeneratedFileType").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Authorisation Generated File Type' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Authorisation Generated File Type", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strAuthorisationGeneratedFileType)) strAuthorisationGeneratedFileType = "Excel";
            */

            // Get Daily Report Subject Line from System_Settings table //
            string strDailyReportSubjectLine = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDailyReportSubjectLine = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingClientEmailDailyReportSubjectLine").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Client Daily Report Subject Line' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Daily Report Subject Line", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Authorisation Report Subject Line from System_Settings table //
            string strAuthorisationReportSubjectLine = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strAuthorisationReportSubjectLine = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingClientEmailAuthorisationSubjectLine").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Client Authorisation Report Subject Line' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Authorisation Report Subject Line", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Emails From value from System_Settings table    ***** This value is not currently used ***** //
            string strGrittingEmailsFromName = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strGrittingEmailsFromName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingEmailsFromName").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Gritting Emails From' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Gritting Emails From", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get CC address from System_Settings table //
            string strCCToEmailAddress = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strCCToEmailAddress = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingConfirmationCCToEmailAddress").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'CC to Email Address' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get CC To Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get VAT Rate from System_Settings table //
            string strDefaultVATRate = "";
            decimal decDefaultVatRate = (decimal)0.00;
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultVATRate = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate").ToString();
                if (!string.IsNullOrEmpty(strDefaultVATRate)) decDefaultVatRate = Convert.ToDecimal(strDefaultVATRate);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Default VAT Rate' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default VAT Rate", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get SMTP Mail Server Address from System_Settings table //
            string strSMTPMailServerAddress = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSMTPMailServerAddress = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerAddress").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'SMTP Mail Server Address' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get SMTP Mail Server Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Get SMTP Mail Server Username from System_Settings table //
            string strSMTPMailServerUsername = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSMTPMailServerUsername = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerUsername").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'SMTP Mail Server Username' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get SMTP Mail Server Username", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Get SMTP Mail Server Password from System_Settings table //
            string strSMTPMailServerPassword = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSMTPMailServerPassword = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerPassword").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'SMTP Mail Server Password' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get SMTP Mail Server Password", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Get SMTP Mail Server Port from System_Settings //
            string strSMTPMailServerPort = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSMTPMailServerPort = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerPort").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'SMTP Mail Server Port' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get SMTP Mail Server Port", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

            // Get Email Authorisation Layout File from System_Settings table //
            string strAuthorisationBodyFile = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strAuthorisationBodyFile = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingAuthorisationSentHtmlLayoutFile").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Gritting Authorisation Email Layout File (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Authoriation Email Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strAuthorisationBodyFile) || !File.Exists(strAuthorisationBodyFile))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The Gritting Authorisation Email Layout File [" + strAuthorisationBodyFile + "] does not exist.\n\nPlease select a valid file using the System Configuration screen before proceeding.", "Check File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Email Confirmation Layout File from System_Settings table //
            string strConfirmationBodyFile = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strConfirmationBodyFile = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingConfirmationSentHtmlLayoutFile").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Gritting Conformation Email Layout File (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Confirmation Email Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strConfirmationBodyFile) || !File.Exists(strConfirmationBodyFile))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The Gritting Confirmation Email Layout File [" + strConfirmationBodyFile + "] does not exist.\n\nPlease select a valid file using the System Configuration screen before proceeding.", "Check File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Email Authorisation CSV File Storage Location from System_Settings table //
            string strCreatedAuthorisationFilesLocation = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strCreatedAuthorisationFilesLocation = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingConfirmationSentPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Gritting Confirmation Sent Path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Gritting Confirmation Sent Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strCreatedAuthorisationFilesLocation))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The Gritting Confirmation Sent Path has not been set in System Settings table.\n\nPlease set it using the System Configuration screen before proceeding.", "Get  Gritting Confirmation Sent Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strCreatedAuthorisationFilesLocation.EndsWith("\\")) strCreatedAuthorisationFilesLocation += "\\";  // Add trailing backslash to end //
            #endregion

            GridView view = (GridView)gridControl1.MainView;
            view.ActiveFilter.Clear(); 
            int intCount = view.DataRowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Sites Requiring Gritting list contains no rows - unable to send emails or generate Gritting Callouts.\n\nTry importing todays weather forecasts before proceeding.", "Send Emails and Generate Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about send out gritting emails and authorisation spreadsheets. The process will also generate gritting callouts for those sites not requiring authorisation\n\nAre you sure you wish to continue?", "Send Emails and Generate Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            int intNewRecordID = 0;
            DateTime dtCalloutDateTime = DateTime.Now;
            int intRecordedByStaffID = this.GlobalSettings.UserID;
            int intImportedWeatherForecastID = 0;
            // Create Record in ImportedWeatherForecast table //
            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter AddForecastRecord = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
            AddForecastRecord.ChangeConnectionString(strConnectionString);
            try
            {
                intImportedWeatherForecastID = Convert.ToInt32(AddForecastRecord.sp04064_GC_Gritting_Imported_Weather_Forecast_Insert(dtCalloutDateTime, intRecordedByStaffID));  // Add Row to database //
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating a record in the Imported Weather Forecast Table.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Emails and Generate Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Step 1 - Create gritting callouts for those sites which don't receive a Daily Email or Daily Report //
            int intSiteGrittingContractID = 0;
            int intSubContractorID = 0;
            int intPreferredSubContractorID = 0;
            int intSiteID = 0;
            int intJobStatusID = 50;  // CalloutStatus 50 = "Completed - Ready To Send" //
            decimal decSaltUsed = (decimal)0.00;
            decimal decClientProactivePrice = (decimal)0.00;
            decimal decEveningRateModifier = (decimal)0.00;
            int intClientPOID = 0;
            int intDoubleGrit = 0;
            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter AddRecord = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
            AddRecord.ChangeConnectionString(strConnectionString);
            try
            {
                DataRow[] drRows = dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data.Select("GrittingAuthorisationRequired = 0 and DailyGrittingEmail = 0");
                foreach (DataRow row in drRows)
                {
                    intSiteGrittingContractID = Convert.ToInt32(row["SiteGrittingContractID"]);
                    intSubContractorID = Convert.ToInt32(row["SubContractorID"]);
                    intPreferredSubContractorID = Convert.ToInt32(row["PreferredSubContractorID"]);
                    intSiteID = Convert.ToInt32(row["SiteID"]);
                    decSaltUsed = Convert.ToDecimal(row["RequiredGritAmount"]);
                    decClientProactivePrice = Convert.ToDecimal(row["ClientProactivePrice"]);
                    decEveningRateModifier = Convert.ToDecimal(row["EveningRateModifier"]);
                    intClientPOID = Convert.ToInt32(row["ClientPOID"]);
                    intDoubleGrit = Convert.ToInt32(row["DoubleGrit"]);
                    intNewRecordID = Convert.ToInt32(AddRecord.sp04063_GC_Gritting_Callout_Insert_From_Forecast(intSiteGrittingContractID, intSubContractorID, intPreferredSubContractorID, intSiteID, intJobStatusID, dtCalloutDateTime, intImportedWeatherForecastID, decSaltUsed, intRecordedByStaffID, decDefaultVatRate, decClientProactivePrice, decEveningRateModifier, intClientPOID, intDoubleGrit));  // Add Row to database //
                    row.Delete(); // Remove row now it has been added to the database //
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating Gritting Callouts.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Emails and Generate Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Step 2 - Create gritting confirmation emails [embedded list of gritted sites] for those sites which receive a Daily Report //
            intJobStatusID = 50;  // CalloutStatus 50 = "Completed - Ready To Send" //
            string strClientIDs = ",";
            try
            {
                DataRow[] drRows = dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data.Select("GrittingAuthorisationRequired = 0 and DailyGrittingEmail = 1", "ClientID DESC");
                foreach (DataRow row in drRows)
                {
                    if (!strClientIDs.Contains(',' + row["ClientID"].ToString() + ',')) strClientIDs += row["ClientID"].ToString() + ',';
                }
                drRows = null;  // Clear so it can be reused later in process //
                if (!string.IsNullOrEmpty(strClientIDs))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] strArray = strClientIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    char[] delimiters2 = new char[] { ';' };

                    if (strArray.Length > 0)
                    {
                        int intClientID = 0;
                        //SqlDataAdapter sdaClientContactDetails = new SqlDataAdapter();
                        //DataSet dsClientContactDetails = new DataSet("NewDataSet");
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);

                        foreach (string strClientID in strArray)
                        {
                            // Unique list of Clients so process each one. Add callout to database while building list of sites to be gritted then once done email to client //
                            drRows = dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data.Select("GrittingAuthorisationRequired = 0 and DailyGrittingEmail = 1 and ClientID = " + strClientID, "SiteName ASC");
                            
                            // See if we also need to build a list of sites not being gritted for the client //
                            int intDailyGrittingEmailShowUnGritted = 0;
                            if (drRows.Length > 0)
                            {
                                DataRow row = drRows[0];
                                intDailyGrittingEmailShowUnGritted = Convert.ToInt32(row["DailyGrittingEmailShowUnGritted"]);
                            }

                            string strSitesBeingGrittedIDs = "";
                            string strGrittedSites = "<table  border=\"1\">\r\n <tr> <th>Site ID</th> <th>Site Name</th> <th>Weather</th> </tr>";
                            foreach (DataRow row in drRows)
                            {
                                // Add Record //
                                intSiteGrittingContractID = Convert.ToInt32(row["SiteGrittingContractID"]);
                                intSubContractorID = Convert.ToInt32(row["SubContractorID"]);
                                intPreferredSubContractorID = Convert.ToInt32(row["PreferredSubContractorID"]);
                                intSiteID = Convert.ToInt32(row["SiteID"]);
                                decSaltUsed = Convert.ToDecimal(row["RequiredGritAmount"]);
                                decClientProactivePrice = Convert.ToDecimal(row["ClientProactivePrice"]);
                                decEveningRateModifier = Convert.ToDecimal(row["EveningRateModifier"]);
                                intClientPOID = Convert.ToInt32(row["ClientPOID"]);
                                intDoubleGrit = Convert.ToInt32(row["DoubleGrit"]);
                                intNewRecordID = Convert.ToInt32(AddRecord.sp04063_GC_Gritting_Callout_Insert_From_Forecast(intSiteGrittingContractID, intSubContractorID, intPreferredSubContractorID, intSiteID, intJobStatusID, dtCalloutDateTime, intImportedWeatherForecastID, decSaltUsed, intRecordedByStaffID, decDefaultVatRate, decClientProactivePrice, decEveningRateModifier, intClientPOID, intDoubleGrit));  // Add Row to database //
                                // Build Email //
                                strGrittedSites += "<tr>\r\n<td>" + Convert.ToString(intSiteID) + "</td>\r\n" + "<td>" + (string.IsNullOrEmpty(row["SiteName"].ToString()) ? "Unknown Site Name" : row["SiteName"].ToString()) + "</td>\r\n" + "<td>" + (string.IsNullOrEmpty(row["ForecastForEmail"].ToString()) ? "Unknown Forecast" : row["ForecastForEmail"].ToString()) + "</td>\r\n</tr>";
                                strSitesBeingGrittedIDs += intSiteID.ToString() + ",";
                                row.Delete(); // Remove row now it has been added to the database //
                            }
                            strGrittedSites+= "\r\n</table>";

                            // Build list of sites not being gritted if required by client //
                            if (intDailyGrittingEmailShowUnGritted == 1)
                            {
                                DataRow[] drRowsSkipped = dataSet_GC_Core.sp04054_GC_Gritting_Import_Forecast_Dummy_Data.Select("ClientID = " + strClientID, "SiteName ASC");
                                if (drRowsSkipped.Length > 0)
                                {
                                    strGrittedSites += "<br/><br/>Ground Control will <b>not</b> be gritting the following sites this evening:<br/><br/>";
                                    strGrittedSites +="<table  border=\"1\">\r\n <tr> <th>Site ID</th> <th>Site Name</th> <th>Weather</th> </tr>";
                                    foreach (DataRow row in drRowsSkipped)
                                    {
                                        strGrittedSites += "<tr>\r\n<td>" + Convert.ToString(row["SiteID"]) + "</td>\r\n" + "<td>" + (string.IsNullOrEmpty(row["SiteName"].ToString()) ? "Unknown Site Name" : row["SiteName"].ToString()) + "</td>\r\n" + "<td>" + (string.IsNullOrEmpty(row["ForecastForEmail"].ToString()) ? "Unknown Forecast" : row["ForecastForEmail"].ToString()) + "</td>\r\n</tr>";
                                    }
                                    strGrittedSites += "\r\n</table>";
                                }
                                else
                                {
                                    //strGrittedSites += "<br/><br/>Ground Control will be gritting all your sites this evening.";
                                }
                            }

                            // Send Email //
                            intClientID = Convert.ToInt32(strClientID);

                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp04066_GC_Gritting_Send_Grit_Email_Get_Client_Details", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@intClientID", intClientID));
                            SqlDataAdapter sdaClientContactDetails = new SqlDataAdapter();
                            DataSet dsClientContactDetails = new DataSet("NewDataSet");
                            dsClientContactDetails.Clear();  // Reset //
                            sdaClientContactDetails = new SqlDataAdapter(cmd);
                            sdaClientContactDetails.Fill(dsClientContactDetails, "Table");
                            
                            if (dsClientContactDetails.Tables[0].Rows.Count > 0)
                            {
                                DataRow dr = dsClientContactDetails.Tables[0].Rows[0];

                                string strBody = System.IO.File.ReadAllText(strConfirmationBodyFile);
                                strBody = strBody.Replace("%Sites%", strGrittedSites);

                                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                                msg.From = new System.Net.Mail.MailAddress(dr["EmailFrom"].ToString());
                                string[] strEmailTo = dr["EmailTo"].ToString().Split(delimiters2, StringSplitOptions.RemoveEmptyEntries);
                                if (strEmailTo.Length > 0)
                                {
                                    foreach (string strEmailAddress in strEmailTo)
                                    {
                                        msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                                    }
                                }
                                else
                                {
                                    msg.To.Add(new System.Net.Mail.MailAddress(dr["EmailTo"].ToString()));
                                }
                                msg.Subject = strDailyReportSubjectLine;
                                if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                                msg.Priority = System.Net.Mail.MailPriority.High;
                                msg.IsBodyHtml = true;
 
                                System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                                //create the LinkedResource (embedded image)
                                System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                                logo.ContentId = "companylogo";
                                //add the LinkedResource to the appropriate view
                                htmlView.LinkedResources.Add(logo);

                                //create the LinkedResource (embedded image)
                                System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                                logo2.ContentId = "companyfooter";
                                //add the LinkedResource to the appropriate view
                                htmlView.LinkedResources.Add(logo2);

                                msg.AlternateViews.Add(plainView);
                                msg.AlternateViews.Add(htmlView);

                                object userState = msg;
                                System.Net.Mail.SmtpClient emailClient = null;
                                if (intSMTPMailServerPort != 0)
                                {
                                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                                }
                                else
                                {
                                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                                }
                                if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                                {
                                    System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                                    emailClient.UseDefaultCredentials = false;
                                    emailClient.Credentials = basicCredential;
                                }
                                emailClient.SendAsync(msg, userState);
                            }
                        }
                        SQlConn.Close();
                        SQlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating Gritting Callouts.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Emails and Generate Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }


            // Step 3 - Create gritting authorisation emails [Linked SpreadSheet] for those sites which receive an Authorisation Report //
            strClientIDs = ",";
            intJobStatusID = 40;  // CalloutStatus 40 = "Completed - Awaiting Authorisation" //
            try
            {
                DataRow[] drRows = dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data.Select("GrittingAuthorisationRequired = 1", "ClientID DESC");
                foreach (DataRow row in drRows)
                {
                    if (!strClientIDs.Contains(',' + row["ClientID"].ToString() + ',')) strClientIDs += row["ClientID"].ToString() + ',';
                }
                drRows = null;  // Clear so it can be reused later in process //
                if (!(string.IsNullOrEmpty(strClientIDs) || strClientIDs == ","))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] strArray = strClientIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    char[] delimiters2 = new char[] { ';' };

                    if (strArray.Length > 0)
                    {
                        int intClientID = 0;
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        string strExportFileName = "";
                        string strClientName = "";
                        foreach (string strClientID in strArray)
                        {
                            // Unique list of Clients so process each one. Add callout to database while building list of sites [and create CSV file] to be gritted then once done email to client with attached CSV //
                            drRows = dataSet_GC_Core.sp04053_GC_Gritting_Import_Forecast_Get_Reference_Data.Select("GrittingAuthorisationRequired = 1 and ClientID = " + strClientID, "SiteName ASC");
                            DataRow drFirst = drRows[0];
                            strClientName = drFirst["ClientName"].ToString();
                            if (string.IsNullOrEmpty(strClientName)) strClientName = drFirst["ClientID"].ToString();  // Missing Client Name so use Client ID instead //

                            if (strAuthorisationGeneratedFileType != "Excel")
                            {
                                #region Create CSV File

                                strExportFileName = strCreatedAuthorisationFilesLocation + strClientName.ToUpper() + " DAILY GRITTING LIST " + DateTime.Today.Day.ToString().PadLeft(2, '0') + "-" + DateTime.Today.Month.ToString().PadLeft(2, '0') + "-" + DateTime.Today.Year.ToString().PadLeft(4, '0') + ".csv";
                                StreamWriter sw = new StreamWriter(strExportFileName, false);
                                sw.Write("\"Grit Required\",\"GC Site ID\",\"Client Name\",\"Store Name\",\"Postcode\",\"Client Site ID\",\"Area\",\"Weather\",");
                                foreach (DataRow row in drRows)
                                {
                                    // Add Record //
                                    intSiteGrittingContractID = Convert.ToInt32(row["SiteGrittingContractID"]);
                                    intSubContractorID = Convert.ToInt32(row["SubContractorID"]);
                                    intPreferredSubContractorID = Convert.ToInt32(row["PreferredSubContractorID"]);
                                    intSiteID = Convert.ToInt32(row["SiteID"]);
                                    decSaltUsed = Convert.ToDecimal(row["RequiredGritAmount"]);
                                    decClientProactivePrice = Convert.ToDecimal(row["ClientProactivePrice"]);
                                    decEveningRateModifier = Convert.ToDecimal(row["EveningRateModifier"]);
                                    intClientPOID = Convert.ToInt32(row["ClientPOID"]);
                                    intDoubleGrit = Convert.ToInt32(row["DoubleGrit"]);
                                    intNewRecordID = Convert.ToInt32(AddRecord.sp04063_GC_Gritting_Callout_Insert_From_Forecast(intSiteGrittingContractID, intSubContractorID, intPreferredSubContractorID, intSiteID, intJobStatusID, dtCalloutDateTime, intImportedWeatherForecastID, decSaltUsed, intRecordedByStaffID, decDefaultVatRate, decClientProactivePrice, decEveningRateModifier, intClientPOID, intDoubleGrit));  // Add Row to database //
                                    // Build Email //
                                    sw.Write(sw.NewLine);
                                    sw.Write("\"Yes\"," + "\"" + row["SiteID"].ToString() + "\",");
                                    sw.Write("\"" + row["ClientName"].ToString() + "\",");
                                    sw.Write("\"" + row["SiteName"].ToString() + "\",");
                                    sw.Write("\"" + row["SitePostcode"].ToString() + "\",");
                                    sw.Write("\"" + row["ClientsSiteID"].ToString() + "\",");
                                    sw.Write("\"" + row["ClientsSiteCode"].ToString() + "\",");
                                    sw.Write("\"" + row["ForecastForEmail"].ToString() + "\",");

                                    row.Delete(); // Remove row now it has been added to the database //
                                }
                                sw.Close();

                                #endregion;
                            }
                            else
                            {
                                #region Create Excel File
                                strExportFileName = strCreatedAuthorisationFilesLocation + strClientName.ToUpper() + " DAILY GRITTING LIST " + DateTime.Today.Day.ToString().PadLeft(2, '0') + "-" + DateTime.Today.Month.ToString().PadLeft(2, '0') + "-" + DateTime.Today.Year.ToString().PadLeft(4, '0') + ".xls";

                                Excel.Application xl = null;
                                Excel._Workbook wb = null;
                                Excel._Worksheet sheet = null;
                                bool SaveChanges = false;
                                try
                                {

                                    if (File.Exists(strExportFileName)) { File.Delete(strExportFileName); }

                                    GC.Collect();

                                    // Create a new instance of Excel from scratch

                                    xl = new Excel.Application();
                                    xl.Visible = false;

                                    // Add one workbook to the instance of Excel
                                    wb = (Excel._Workbook)(xl.Workbooks.Add(Missing.Value));
                                    wb.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                                    // Get a reference to the one and only worksheet in our workbook
                                    //sheet = (Excel._Worksheet)wb.ActiveSheet;
                                    sheet = (Excel._Worksheet)(wb.Sheets[1]);

                                    // Fill spreadsheet with data
                                    //sheet.Name = "Test";

                                    // Set column heading names //
                                    sheet.Name = "Gritting";
                                    sheet.Cells[1, 1] = "Grit Required";
                                    sheet.Cells[1, 2] = "GC Site ID";
                                    sheet.Cells[1, 3] = "Client Name";
                                    sheet.Cells[1, 4] = "Store Name";
                                    sheet.Cells[1, 5] = "Postcode";
                                    sheet.Cells[1, 6] = "Client Site ID";
                                    sheet.Cells[1, 7] = "Area";
                                    sheet.Cells[1, 8] = "Weather";
                                    int intRowCount = 2;  // start at 2 to skip past header row //
                                    foreach (DataRow row in drRows)
                                    {
                                        // Add Record //
                                        intSiteGrittingContractID = Convert.ToInt32(row["SiteGrittingContractID"]);
                                        intSubContractorID = Convert.ToInt32(row["SubContractorID"]);
                                        intPreferredSubContractorID = Convert.ToInt32(row["PreferredSubContractorID"]);
                                        intSiteID = Convert.ToInt32(row["SiteID"]);
                                        decSaltUsed = Convert.ToDecimal(row["RequiredGritAmount"]);
                                        decClientProactivePrice = Convert.ToDecimal(row["ClientProactivePrice"]);
                                        decEveningRateModifier = Convert.ToDecimal(row["EveningRateModifier"]);
                                        intClientPOID = Convert.ToInt32(row["ClientPOID"]);
                                        intDoubleGrit = Convert.ToInt32(row["DoubleGrit"]);
                                        intNewRecordID = Convert.ToInt32(AddRecord.sp04063_GC_Gritting_Callout_Insert_From_Forecast(intSiteGrittingContractID, intSubContractorID, intPreferredSubContractorID, intSiteID, intJobStatusID, dtCalloutDateTime, intImportedWeatherForecastID, decSaltUsed, intRecordedByStaffID, decDefaultVatRate, decClientProactivePrice, decEveningRateModifier, intClientPOID, intDoubleGrit));  // Add Row to database //
                                        // Build Email //
                                        sheet.Cells[intRowCount, 1] = "Yes";
                                        sheet.Cells[intRowCount, 2] = row["SiteID"].ToString();
                                        sheet.Cells[intRowCount, 3] = row["ClientName"].ToString();
                                        sheet.Cells[intRowCount, 4] = row["SiteName"].ToString();
                                        sheet.Cells[intRowCount, 5] = row["SitePostcode"].ToString();
                                        sheet.Cells[intRowCount, 6] = row["ClientsSiteID"].ToString();
                                        sheet.Cells[intRowCount, 7] = row["ClientsSiteCode"].ToString();
                                        sheet.Cells[intRowCount, 8] = row["ForecastForEmail"].ToString();

                                        Excel.Range range2 = sheet.get_Range("A" + intRowCount.ToString(), "H" + intRowCount.ToString());
                                        if ((intRowCount & 1) == 1)  // 1 = Odd row //
                                        {
                                            //range2.Interior.Color = System.Drawing.Color.LightGreen.ToArgb();
                                            range2.Interior.Color = intOddBackColour;
                                           
                                        }
                                        else  //  0 = Even row //
                                        {
                                            //range2.Interior.Color = System.Drawing.Color.LightGreen.ToArgb();
                                            range2.Interior.Color = intEvenBackColour;
                                        }
                                        intRowCount++;
                                        row.Delete(); // Remove row now it has been added to the database //
                                    }
                                    AutoFitColumn(sheet, 1);
                                    AutoFitColumn(sheet, 2);
                                    AutoFitColumn(sheet, 3);
                                    AutoFitColumn(sheet, 4);
                                    AutoFitColumn(sheet, 5);
                                    AutoFitColumn(sheet, 6);
                                    AutoFitColumn(sheet, 7);
                                    AutoFitColumn(sheet, 8);

                                    // Set column header backColour //
                                    Excel.Range range = sheet.get_Range("A1", "H1");
                                    //range.Font.Name = "Time New Roman";
                                    //range.Font.Size = 10;
                                    //range.Font.ColorIndex = 3;
                                    range.Interior.Color = System.Drawing.Color.LightGray.ToArgb();

                                    //sheet = (Excel._Worksheet)(wb.Sheets[2]);
                                    //sheet.Name = "Feb";
                                    //sheet.Cells[1, 1] = "Heading";

                                    // Save and Close of the Excel instance
                                    xl.Visible = false;
                                    xl.UserControl = false;

                                    // Set a flag saying that all is well and it is ok to save our changes to a file.
                                    SaveChanges = true;
                                    xl.DisplayAlerts = false;  // Prevent Compatibility Checked dialogue displaying due to cell colours etc //

                                    //  Save the file to disk
                                    wb.SaveAs(strExportFileName, Excel.XlFileFormat.xlWorkbookNormal,
                                              null, null, false, false, Excel.XlSaveAsAccessMode.xlShared,
                                              false, false, null, null);  //Excel.XlFileFormat.xlWorkbookNormal

                                }
                                catch (Exception ex)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Excel Spreadsheet [" + strExportFileName + "]. Message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Emails and Generate Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                }
                                finally
                                {
                                    try
                                    {
                                        // Repeat xl.Visible and xl.UserControl releases just to be sure
                                        // we didn't error out ahead of time.

                                        xl.Visible = false;
                                        xl.UserControl = false;
                                        // Close the document and avoid user prompts to save if our method failed.
                                        wb.Close(SaveChanges, null, null);
                                        xl.Workbooks.Close();
                                    }
                                    catch { }

                                    // Gracefully exit out and destroy all COM objects to avoid hanging instances
                                    // of Excel.exe whether our method failed or not.

                                    xl.Quit();

                                    //if (module != null) { Marshal.ReleaseComObject(module); }
                                    if (sheet != null) { Marshal.ReleaseComObject(sheet); }
                                    if (wb != null) { Marshal.ReleaseComObject(wb); }
                                    if (xl != null) { Marshal.ReleaseComObject(xl); }

                                    //module = null;
                                    sheet = null;
                                    wb = null;
                                    xl = null;
                                    GC.Collect();
                                }
                                #endregion
                            }
                            // Send Email //
                            intClientID = Convert.ToInt32(strClientID);

                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp04066_GC_Gritting_Send_Grit_Email_Get_Client_Details", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@intClientID", intClientID));
                            SqlDataAdapter sdaClientContactDetails = new SqlDataAdapter();
                            DataSet dsClientContactDetails = new DataSet("NewDataSet");
                            dsClientContactDetails.Clear();  // Reset //
                            sdaClientContactDetails = new SqlDataAdapter(cmd);
                            sdaClientContactDetails.Fill(dsClientContactDetails, "Table");
                            if (dsClientContactDetails.Tables[0].Rows.Count > 0)
                            {
                                DataRow dr = dsClientContactDetails.Tables[0].Rows[0];

                                string strBody = System.IO.File.ReadAllText(strAuthorisationBodyFile);
                    
                                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                                msg.From = new System.Net.Mail.MailAddress(dr["EmailFrom"].ToString());
                                string[] strEmailTo = dr["EmailTo"].ToString().Split(delimiters2, StringSplitOptions.RemoveEmptyEntries);
                                if (strEmailTo.Length > 0)
                                {
                                    foreach (string strEmailAddress in strEmailTo)
                                    {
                                        msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                                    }
                                }
                                else
                                {
                                    msg.To.Add(new System.Net.Mail.MailAddress(dr["EmailTo"].ToString()));
                                }
                                msg.Subject = strAuthorisationReportSubjectLine;
                                if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                                msg.Priority = System.Net.Mail.MailPriority.High;
                                msg.IsBodyHtml = true;

                                System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strExportFileName); //create the attachment
                                msg.Attachments.Add(mailAttachment);

                                System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                                //create the LinkedResource (embedded image)
                                System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                                logo.ContentId = "companylogo";
                                //add the LinkedResource to the appropriate view
                                htmlView.LinkedResources.Add(logo);

                                //create the LinkedResource (embedded image)
                                System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                                logo2.ContentId = "companyfooter";
                                //add the LinkedResource to the appropriate view
                                htmlView.LinkedResources.Add(logo2);

                                msg.AlternateViews.Add(plainView);
                                msg.AlternateViews.Add(htmlView);

                                object userState = msg;
                                System.Net.Mail.SmtpClient emailClient = null;
                                if (intSMTPMailServerPort != 0)
                                {
                                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                                }
                                else
                                {
                                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                                }
                                if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                                {
                                    System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                                    emailClient.UseDefaultCredentials = false;
                                    emailClient.Credentials = basicCredential;
                                }
                                emailClient.SendAsync(msg, userState);
                            }
                        }
                        SQlConn.Close();
                        SQlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating Gritting Callouts.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Emails and Generate Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Process Completed Successfully.\n\nWould you like to open the Gritting Callout Manager so you can view and send any generated completed callouts to teams?", "Send Emails and Generate Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                // Open Callout Manager //
                frm_GC_Callout_Manager frmInstance = new frm_GC_Callout_Manager();
                
                frmProgress fProgress = new frmProgress(10);
                this.AddOwnedForm(fProgress);
                fProgress.Show();  // ***** Closed in PostOpen event ***** //
                System.Windows.Forms.Application.DoEvents();

                frmInstance.MdiParent = this.MdiParent;
                frmInstance.GlobalSettings = this.GlobalSettings;
                frmInstance.fProgress = fProgress;
                frmInstance.Show();

                MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(frmInstance, new object[] { null });
            }
        }
        

        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "PreferredTeam")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SubContractorID")) == 0)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    //e.Appearance.BackColor = Color.LightCoral;
                    //e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }*/
        }

        #endregion


        private void btnLoadAuthorisation_Click(object sender, EventArgs e)
        {
            string strDefaultPath = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingConfirmationReceivedPath").ToString().ToLower();
            }
            catch (Exception)
            {
            }
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.DefaultExt = "csv";
                dlg.Filter = "CSV Files (*.csv)|*.csv"; ;
                if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                dlg.Multiselect = true;
                if (dlg.ShowDialog() != DialogResult.OK) return;

                int intImportedFileCount = 0;
                int intImportedFileErrorCount = 0;
                
                frmProgress fProgress = new frmProgress(0);
                fProgress.UpdateCaption("Loading Authorisation Spreadsheet(s)...");
                fProgress.Show();
                System.Windows.Forms.Application.DoEvents();

                memoEdit1.Text = "";
                int intFilesReadCount = 0;

                SqlDataAdapter sdaAuthorisation = new SqlDataAdapter();
                DataSet dsAuthorisation = new DataSet("NewDataSet");
                try
                {
                    SqlConnection SQlConn = new SqlConnection(strConnectionString);
                    SqlCommand cmd = new SqlCommand("sp04068_GC_Spreadsheet_Authorisation_Dummy", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    dsAuthorisation.Clear();  // Remove old values first //
                    sdaAuthorisation = new SqlDataAdapter(cmd);
                    sdaAuthorisation.Fill(dsAuthorisation, "Table");
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while preparing the data for loading.\n\nMessage = [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Load Authorisation Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                gridControl1.BeginUpdate();

                dsErrors2.Tables[0].Rows.Clear();  // Clear Errors //

                GridView view = (GridView)gridControl1.MainView;
                view.BeginUpdate();
                view.BeginDataUpdate();
                dataSet_GC_Core.sp04068_GC_Spreadsheet_Authorisation_Dummy.Rows.Clear();  // Clear Import Grid //
                view.EndDataUpdate();
                view.EndUpdate();

                labelControlInformation2.Text = "No information present at this time.";
                hyperLinkEditErrorCount2.Text = "No errors.";
                labelControlInformation2.Visible = true;
                hyperLinkEditErrorCount2.Visible = false;
                pictureEdit2.Image = imageCollection1.Images[6];
                
                foreach (String file in dlg.FileNames)  // Read the files //
                {
                    intFilesReadCount++;
                        // Check if filename has already been imported - if yes, alert user and allow them to skip the file if they wish...//
                    try
                    {
                        
                        DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter CheckIfImported = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                        CheckIfImported.ChangeConnectionString(strConnectionString);
                        int intImportCount = Convert.ToInt32(CheckIfImported.sp04067_GC_Load_Authorisation_Check_If_Imported(file));
                        if (intImportCount > 0)
                        {
                            if (DevExpress.XtraEditors.XtraMessageBox.Show("WARNING...  WARNING...  WARNING...  WARNING...  WARNING...\n=========================================\n\nYou have already imported file [" + file + "]!\n\nIf you proceed and import this data you may overwrite data!\n\nProceed with loading file?", "Load Authorisation Data   *** IMPORTANT INFORMATION ***", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No) continue;
                        }
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while checking if an import has already been made from the selected file!\n\nYou can still import from the file however you should manually check you have not already imported the file.", "Load Authorisation Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                  
                    // Load CSV file rows into dataset //
                    try
                    {
                        string[] lines = File.ReadAllLines(file);

                        int intUpdateProgressThreshhold = lines.Length / 10;
                        int intUpdateProgressTempCount = 0;

                        System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                        ArrayList arrayImportStructure = new ArrayList();

                        bool boolEndOfSS = false;
                        int intLineNumber = 0;
                        foreach (string line in lines)
                        {
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                            intLineNumber++;
                            if (intLineNumber == 1) continue;  // Skip first row as it contains column headers //

                            string[] columns = r.Split(line);  // Get Columns into Array //
                            if (columns.Length == 0) break;

                            // Check if we have reached end of SS //
                            for (int i = 0; i < columns.Length; i++)
                            {
                                if (columns[i].ToString() != "") break;  // Valid row so abort this check right away so no more time is wasted //
                                if (i == columns.Length - 1)
                                {
                                    boolEndOfSS = true;  // If we are here all columns are blank so spreadsheet end has been reached //
                                }
                            }
                            if (boolEndOfSS) break;
                            if (columns.Length <= 3) continue;  // Skip Row //

                            string strAuthorise = columns[0].ToString().Replace("\"", string.Empty);  // 0 based so number is 1 less than physical column number //
                            string strSiteID = columns[1].ToString().Replace("\"", string.Empty);
                            string strClientName = columns[2].ToString().Replace("\"", string.Empty);
                            string strSiteName = columns[3].ToString().Replace("\"", string.Empty);                          
                            string strPostcode = "";
                            if (columns.Length >= 5) strPostcode = columns[4].ToString().Replace("\"", string.Empty);                        
                            string strClientSiteID = "";
                            if (columns.Length >= 6) strClientSiteID = columns[5].ToString().Replace("\"", string.Empty);
                            string strArea = "";
                            if (columns.Length >= 7) strArea = columns[6].ToString().Replace("\"", string.Empty);
                            string strWeather = "";
                            if (columns.Length >= 8) strWeather = columns[7].ToString().Replace("\"", string.Empty);
                            int intSiteID = 0;
                            if (string.IsNullOrEmpty(strSiteID))
                            {
                                // Add record into Errors datatable [Invalid Spreadsheet line] //
                                StoreImportError2(file, intLineNumber, strClientName, strSiteName, "Missing Site ID");
                                intImportedFileErrorCount++;
                                continue;
                            }
                            else
                            {
                                bool boolConvert = Int32.TryParse(strSiteID, out intSiteID);
                                if (!boolConvert)
                                {
                                    // Add record into Errors datatable [Invalid Spreadsheet line] //
                                    StoreImportError2(file, intLineNumber, strClientName, strSiteName, "Non-numeric Site ID");
                                    intImportedFileErrorCount++;
                                    continue;
                                }
                            }
                            if (string.IsNullOrEmpty(strSiteName))
                            {
                                // Add record into Errors datatable [Invalid Spreadsheet line] //
                                StoreImportError2(file, intLineNumber, strClientName, strSiteName, "Missing Store Name");
                                intImportedFileErrorCount++;
                                continue;
                            }
                            if (string.IsNullOrEmpty(strAuthorise))
                            {
                                // Add record into Errors datatable [Invalid Spreadsheet line] //
                                StoreImportError2(file, intLineNumber, strClientName, strSiteName, "Missing Grit Required");
                                intImportedFileErrorCount++;
                                continue;
                            }
                            else if (!(strAuthorise.ToUpper() == "YES" || strAuthorise.ToUpper() == "NO"))
                            {
                                // Add record into Errors datatable [Invalid Spreadsheet line] //
                                StoreImportError2(file, intLineNumber, strClientName, strSiteName, "Invalid Grit Required");
                                intImportedFileErrorCount++;
                                continue;
                            }
                            // Check Gritting Callout Record Has already been created for this import line. If not then reject it //
                            try
                            {
                        
                                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter GetCalloutCount = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                                GetCalloutCount.ChangeConnectionString(strConnectionString);
                                int intImportCount = Convert.ToInt32(GetCalloutCount.sp04070_GC_Load_Authorisation_Check_For_Matching_Callout(intSiteID, strSiteName, DateTime.Today));
                                if (intImportCount == 0)
                                {
                                    StoreImportError2(file, intLineNumber, strClientName, strSiteName, "No Matching Callout Record");
                                    intImportedFileErrorCount++;
                                    continue;
                                }
                            }
                            catch (Exception Ex)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while checking for a matching gritting callout record!\n\nYou can still import from this file but if any extra records have been added to the spreadsheet since it was generated, these will not be be imported.", "Load Authorisation Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            // Checks all passed so add row to grid for importing //
                            DataRow drNewRow;
                            drNewRow = dataSet_GC_Core.sp04068_GC_Spreadsheet_Authorisation_Dummy.NewRow();
                            drNewRow["Grit_Required"] = strAuthorise;
                            drNewRow["GC_Site_ID"] = strSiteID;
                            drNewRow["Client_Name"] = strClientName;
                            drNewRow["Store_Name"] = strSiteName;
                            drNewRow["Postcode"] = strPostcode;
                            drNewRow["Client_Site_ID"] = strClientSiteID;
                            drNewRow["Area"] = strArea;
                            drNewRow["Weather"] = strWeather;
                            dataSet_GC_Core.sp04068_GC_Spreadsheet_Authorisation_Dummy.Rows.Add(drNewRow);
                            intImportedFileCount++;
                        }
                    }
                    catch (Exception ex)
                    {
                        gridControl1.EndUpdate();
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Forecast File [" + strGrittingForecastType1File + "].\n\nMessage = [" + ex.Message + "].\n\nThere may be a fault with the file. Please close this screen then try again. If the problem persists, contact Technical Support.", "Import Forecasting Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    memoEdit1.Text += file + "\r\n";
                }

                view = (GridView)gridControl1.MainView;
                view.ExpandAllGroups();
                gridControl1.EndUpdate();
 
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                if (intImportedFileErrorCount == 0)
                {
                    labelControlInformation2.Text = intImportedFileCount.ToString() + (intImportedFileCount == 1 ? " record" : " records") + " loaded from " + (intFilesReadCount == 1 ? "File." : "Files.");
                    hyperLinkEditErrorCount2.Text = "No errors.";
                    labelControlInformation2.Visible = true;
                    hyperLinkEditErrorCount2.Visible = false;
                    pictureEdit2.Image = imageCollection1.Images[6];
                }
                else
                {
                    labelControlInformation2.Text = "No information present at this time.";
                    hyperLinkEditErrorCount2.Text = intImportedFileErrorCount.ToString() + (intImportedFileErrorCount == 1 ? " error" : " errors") + " in Loaded " + (intFilesReadCount == 1 ? "File." : "Files.");
                    labelControlInformation2.Visible = false;
                    hyperLinkEditErrorCount2.Visible = true;
                    pictureEdit2.Image = imageCollection1.Images[7];
                }
            }
        }

        private void StoreImportError2(string FileName, int LineNumber, string ClientName, string StoreName, string ErrorReason)
        {
            DataRow drNewRow;
            drNewRow = dsErrors2.Tables[0].NewRow();
            drNewRow["FileName"] = FileName;
            drNewRow["LineNumber"] = LineNumber;
            drNewRow["ClientName"] = ClientName;
            drNewRow["StoreName"] = StoreName;
            drNewRow["ErrorReason"] = ErrorReason;
            dsErrors2.Tables[0].Rows.Add(drNewRow);
        }

        public void AutoFitColumn(Excel._Worksheet ws, int col)
        {
            ((Range)ws.Cells[1, col]).EntireColumn.AutoFit();
        }

        private void hyperLinkEditErrorCount2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            frm_GC_Import_Weather_Forecasts_Authorisation_Errors frm_child = new frm_GC_Import_Weather_Forecasts_Authorisation_Errors();
            //frm_styles.MdiParent = this.MdiParent;
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child.dsErrors = this.dsErrors2;
            frm_child.ShowDialog();
        }

        private void btnImportAuthorisation_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            view.ActiveFilter.Clear(); 
            int intCount = view.DataRowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Authorisation Spreadsheet contents available for importing - click the Load Authorisation Spreadsheets button before proceeding.", "Import Authorisation Spreadsheet Contents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to import the contents of the loaded authorisation spreadsheets.\n\nIf you proceed, the gritting callout records associated with these authorisation records will be updated.\n\nAre you sure you wish to continue?", "Import Authorisation Spreadsheet Contents", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            CheckEdit ce = (CheckEdit)checkEditDeleteUnauthorised;
            bool boolDeleteUnauthorised = ce.Checked;
            if (!boolDeleteUnauthorised)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("WARNING... Delete Unauthorised Grit checkbox is unticked.\n\nIf you proceed, any callouts which have not been authorised on the srpeadsheet will be left [the default is to remove these]. If you want to remove these you will need to use the Callout Manager screen.\n\nAre you sure you wish to continue?", "Import Authorisation Spreadsheet Contents", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
            }

            int intRecordedByStaffID = this.GlobalSettings.UserID;
            int intImportedAuthorisationID = 0;
            // Create Record in ImportedAuthorisation table //
            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter AddForecastRecord = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
            AddForecastRecord.ChangeConnectionString(strConnectionString);

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Importing Authorisation Spreadsheet(s)...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intCount / 10;
            int intUpdateProgressTempCount = 0;

            try
            {
                String[] strMemoLines = memoEdit1.Lines;
                foreach (string strLine in strMemoLines)
                {
                    if (string.IsNullOrEmpty(strLine)) continue;
                    intImportedAuthorisationID = Convert.ToInt32(AddForecastRecord.sp04071_GC_Gritting_Imported_Authorisation_Insert(strLine, intRecordedByStaffID));  // Add Row to database //
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating a record in the Imported Authorisation Table.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Import Authorisation Spreadsheet Contents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.BeginUpdate();
            // Import Authorisation records set up, so update the callout records changing there status as a result of the authorisation - optionally delete unauthorised callouts... //
            int intSiteID = 0;
            string strAuthorised = "";
            int intRecordID = 0;
            DateTime dtToday = DateTime.Today;
            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter UpdateCallout = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
            UpdateCallout.ChangeConnectionString(strConnectionString);

            DataRow[] drRows =  dataSet_GC_Core.sp04068_GC_Spreadsheet_Authorisation_Dummy.Select("GC_Site_ID >= 0");  // This will return all rows regardless - need this so when we deleted row in the foreach clause below doesn't throw an error about violating order //
            try
            {
                foreach (DataRow row in drRows)
                {
                    // Update Record //
                    intSiteID = Convert.ToInt32(row["GC_Site_ID"]);
                    strAuthorised = row["Grit_Required"].ToString();
                    intRecordID = Convert.ToInt32(UpdateCallout.sp04072_GC_Set_Callout_Authorised_Status(intSiteID, strAuthorised, (boolDeleteUnauthorised ? 1 : 0), dtToday));  // Add Row to databaseUpdate Staus of Callout in DB //

                    row.Delete();  // Clear the row from the Authorisation grid //

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            catch (Exception ex)
            {
                gridControl1.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while importing Authorisation Spreadsheet Contents.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Import Authorisation Spreadsheet Contents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            gridControl1.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Spreadsheet Authorisation Contents Imported.\n\nWould you like to open the Gritting Callout Manager so you can view and send these callouts to teams?", "Import Authorisation Spreadsheet Contents", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                // Open Callout Manager //
                frm_GC_Callout_Manager frmInstance = new frm_GC_Callout_Manager();
                
                frmProgress fProgress2 = new frmProgress(10);
                this.AddOwnedForm(fProgress2);
                fProgress2.Show();  // ***** Closed in PostOpen event ***** //
                System.Windows.Forms.Application.DoEvents();

                frmInstance.MdiParent = this.MdiParent;
                frmInstance.GlobalSettings = this.GlobalSettings;
                frmInstance.fProgress = fProgress2;
                frmInstance.Show();

                MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(frmInstance, new object[] { null });
            }
        }


        #region Thames Water Import

        private void btnLoadFloatingForecast_Click(object sender, EventArgs e)
        {
            #region Check Files

            frmProgress fProgress = new frmProgress(30);
            fProgress.UpdateCaption("Checking Floating Site Forecast Import File...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            // Get Forecasting filenames from System_Settings table //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strGrittingForecastFloatingFile = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingForecastTypeFloatingFilePath").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the name of Floating Site Import Weather Forecast File (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Weather Forecasting File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(30); // Update Progress Bar //

            // Check Files exist //
            if (!File.Exists(strGrittingForecastFloatingFile))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The Weather Forecast File [" + strGrittingForecastFloatingFile + "] does not exist.\n\nPlease ensure today's forecasting files have been saved to the correct location and that they have been named correctly before trying again.", "Check Weather Forecasting File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
 
            // Check that files were last modified today //
            DateTime lastWriteTime = File.GetLastWriteTime(strGrittingForecastFloatingFile);
            //DateTime creationTime = File.GetCreationTime(strGrittingForecastType1File);
            //DateTime lastAccessTime = File.GetLastAccessTime(strGrittingForecastType1File);
            if (lastWriteTime.Date != DateTime.Today)
            {
                if (fProgress != null) fProgress.Hide();
                if (DevExpress.XtraEditors.XtraMessageBox.Show("WARNING...  WARNING...  WARNING...  WARNING...  WARNING...\n=========================================\n\nThe last modified date of [" + strGrittingForecastFloatingFile + "] is not equal to todays date.\n\nThis file may be a historical file!\n\nAre you sure you wish to proceed?", "Check Weather Forecasting File Last Modified Date", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                fProgress.Show();
                if (fProgress != null) fProgress.UpdateProgress(40); // Update Progress Bar //
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            System.Windows.Forms.Application.DoEvents();  // Allow Form time to repaint itself //

            #endregion

            btnSendFloatingClientEmails.Enabled = false;
            btnLoadFloatingJobs.Enabled = false;
            btnCreateFloatingJobs.Enabled = false;

            // Forecast Filenames present and dates last modified dates are valid so attempt to parse them //
            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Importing Forecast Files [File 1]...");
            fProgress.Show();
            DateTime dtToday = DateTime.Today;
            System.Windows.Forms.Application.DoEvents();

            SqlDataAdapter sdaSites = new SqlDataAdapter();
            DataSet dsSites = new DataSet("NewDataSet");
            try
            {
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp04348_GC_Gritting_Import_Floating_Forecast_Data", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ImportDate", dtToday));
                dsSites.Clear();  // Remove old values first //
                sdaSites = new SqlDataAdapter(cmd);
                sdaSites.Fill(dsSites, "Table");
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Site Gritting Contract Information [" + strGrittingForecastFloatingFile + "].\n\nMessage = [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Load Forecasting Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // File type (Thames Water CSV) //

            gridControl4.BeginUpdate();
            gridControl5.BeginUpdate();

            dsErrors3.Tables[0].Rows.Clear();  // Clear Errors //

            GridView view = (GridView)gridControl4.MainView;
            view.BeginUpdate();
            view.BeginDataUpdate();
            dataSet_GC_Core.sp04348_GC_Gritting_Import_Floating_Forecast_Data.Rows.Clear();
            view.EndDataUpdate();
            view.EndUpdate();

            view = (GridView)gridControl5.MainView;
            view.BeginUpdate();
            view.BeginDataUpdate();
            dataSet_GC_Core.sp04349_GC_Gritting_Import_Floating_Forecast_Data.Rows.Clear();
            view.EndDataUpdate();
            view.EndUpdate();

            labelControlInformation3.Text = "No information present at this time.";
            hyperLinkEditErrorCount3.Text = "No errors.";
            labelControlInformation3.Visible = true;
            hyperLinkEditErrorCount3.Visible = false;
            pictureEdit3.Image = imageCollection1.Images[6];

            System.Windows.Forms.Application.DoEvents();  // Allow Form time to repaint itself //

            int intImportedFile1Count = 0;
            int intImportedFile1ErrorCount = 0;
            int intLineNumber = 0;
            bool boolGritRequired = false;
            string strDayOfWeek = DateTime.Today.DayOfWeek.ToString().ToUpper();  // Used for comparisons on Gritting Pattern //
            string strDayOfWeekColumnToUse = "";
            switch (strDayOfWeek)
            {
                case "MONDAY":
                    strDayOfWeekColumnToUse = "GritOnMonday";
                    break;
                case "TUESDAY":
                    strDayOfWeekColumnToUse = "GritOnTuesday";
                    break;
                case "WEDNESDAY":
                    strDayOfWeekColumnToUse = "GritOnWednesday";
                    break;
                case "THURSDAY":
                    strDayOfWeekColumnToUse = "GritOnThursday";
                    break;
                case "FRIDAY":
                    strDayOfWeekColumnToUse = "GritOnFriday";
                    break;
                case "SATURDAY":
                    strDayOfWeekColumnToUse = "GritOnSaturday";
                    break;
                case "SUNDAY":
                    strDayOfWeekColumnToUse = "GritOnSunday";
                    break;
                default:
                    break;
            }

            #region Import Floating Forecast Spreadsheet

            intLineNumber = 0;
            try
            {
                string[] lines = File.ReadAllLines(strGrittingForecastFloatingFile);

                int intUpdateProgressThreshhold = lines.Length / 10;
                int intUpdateProgressTempCount = 0;

                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                ArrayList arrayImportStructure = new ArrayList();
                foreach (string line in lines)
                {
                    boolGritRequired = false;
                    intLineNumber++;
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }

                    //if (intLineNumber <= 5) continue;  // Skip first 5 lines //
                    string[] columns = r.Split(line);  // Get Columns into Array //
                    if (columns.Length < 120) continue;  // was 18 //
                    if (columns[1].ToString() == "" && columns[2].ToString() == "") continue;

                    // Check for Snow //
                    bool boolSnowForecast = false;
                    if ((!string.IsNullOrEmpty(columns[6].ToString()) && columns[6].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[11].ToString()) && columns[11].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[16].ToString()) && columns[16].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[21].ToString()) && columns[21].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[26].ToString()) && columns[26].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[31].ToString()) && columns[31].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[36].ToString()) && columns[36].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[41].ToString()) && columns[41].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[46].ToString()) && columns[46].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[51].ToString()) && columns[51].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[56].ToString()) && columns[56].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[61].ToString()) && columns[61].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[66].ToString()) && columns[66].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[71].ToString()) && columns[71].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[76].ToString()) && columns[76].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[81].ToString()) && columns[81].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[86].ToString()) && columns[86].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[91].ToString()) && columns[91].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[96].ToString()) && columns[96].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[101].ToString()) && columns[101].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[106].ToString()) && columns[106].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[111].ToString()) && columns[111].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[116].ToString()) && columns[116].ToString().ToUpper() == "SNOW") ||
                       (!string.IsNullOrEmpty(columns[121].ToString()) && columns[121].ToString().ToUpper() == "SNOW"))
                    {
                        boolSnowForecast = true;
                    }

                    string strStoreNumber = columns[0].ToString();
                    string strActivation1Code = "";
                    string strActivationCodeExtra = "";
                    if (string.IsNullOrEmpty(strStoreNumber))
                    {
                        // Add record into Errors datatable [Invalid Spreadsheet line] //
                        StoreImportError(dsErrors3, strGrittingForecastFloatingFile, strStoreNumber, "", strActivation1Code, "Line Number " + intLineNumber.ToString() + " - No Store Number");
                        intImportedFile1ErrorCount++;
                        continue;
                    }

                    DataRow[] drRows = dsSites.Tables[0].Select("(ForecastingTypeID = 2 or ForecastingTypeID = 3) and HubID = '" + strStoreNumber + "'");
                    if (drRows.Length > 0)
                    {
                        foreach (DataRow drMatch in drRows)
                        {
                            // Database row found so see if a grit is required //
                            int intForecastType = Convert.ToInt32(drMatch["ForecastingTypeID"].ToString());
                            if (intForecastType == 2)  // Using Colour only //
                            {
                                strActivation1Code = columns[2].ToString();
                                if (string.IsNullOrEmpty(strActivation1Code))
                                {
                                    // Add record into Errors datatable [Invalid Spreadsheet line] //
                                    StoreImportError(dsErrors, strGrittingForecastFloatingFile, strStoreNumber, "", strActivation1Code, "Line Number " + intLineNumber.ToString() + " - No Activation Code");
                                    intImportedFile1ErrorCount++;
                                    continue;
                                }
                                drMatch["ForecastActivationPoint"] = strActivation1Code;
                                // Find Activation Point in database for spreadsheet activation code and gets it's numeric corresponding value to see if it is greater than that for the site //
                                DataRow[] drFilteredActivationCodes = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation1Code + "'");
                                if (drFilteredActivationCodes.Length > 0)
                                {
                                    // Match found so see if a grit is required //
                                    DataRow dr = drFilteredActivationCodes[0];
                                    if (Convert.ToInt32(dr["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()))
                                    {
                                        if (Convert.ToInt32(drMatch["HolidayCount"]) > 0)  // Site on holiday so No Gritting Required //
                                        {
                                            boolGritRequired = false;
                                        }
                                        else if (Convert.ToInt32(drMatch[strDayOfWeekColumnToUse]) == 0)  // Site doesn't have a site gritting pattern for today so No Gritting Required //
                                        {
                                            boolGritRequired = false;
                                        }
                                        else  // Grit //
                                        {
                                            drMatch["SnowForcasted"] = (boolSnowForecast ? 1 : 0);
                                            drMatch["RequiredGritAmount"] = (boolSnowForecast ? Convert.ToDecimal(drMatch["DefaultGritAmount"]) * (decimal)2.00 : Convert.ToDecimal(drMatch["DefaultGritAmount"]));
                                            drMatch["DoubleGrit"] = (boolSnowForecast ? 1 : 0);
                                            drMatch["ForecastForEmail"] = strActivation1Code;

                                            dataSet_GC_Core.sp04348_GC_Gritting_Import_Floating_Forecast_Data.ImportRow(drMatch);  // Only add first row //
                                            intImportedFile1Count++;
                                            boolGritRequired = true;
                                        }
                                    }
                                }

                            }
                            else  // intForecastType = 3 - Using Minimum Temperature and Road State - only uses [00:00 - 12:00] sets of data. Commented out code is [18:00 - 06:00] (sets of data) kept in case Lizzie changes her mind again!!!) //  
                            {
                                strActivationCodeExtra = columns[2].ToString();  // Get Colour - change requested by Lizzie on 07/12/2011 //
                                if (string.IsNullOrEmpty(strActivationCodeExtra)) strActivationCodeExtra = "";
                                int intRedOverridesBanding = Convert.ToInt32(drMatch["RedOverridesBanding"]);  // This requested by Lizzie on 05/08/2012 //

                                strActivation1Code = columns[6].ToString();
                                string strActivation2Code = columns[11].ToString();
                                string strActivation3Code = columns[16].ToString();
                                string strActivation4Code = columns[21].ToString();
                                string strActivation5Code = columns[26].ToString();
                                string strActivation6Code = columns[31].ToString();
                                string strActivation7Code = columns[36].ToString();
                                string strActivation8Code = columns[41].ToString();
                                string strActivation9Code = columns[46].ToString();
                                string strActivation10Code = columns[51].ToString();
                                string strActivation11Code = columns[56].ToString();
                                string strActivation12Code = columns[61].ToString();
                                string strActivation13Code = columns[66].ToString();  // Road State 00:00 //
                                string strActivation14Code = columns[71].ToString();  // Road State 01:00 //
                                string strActivation15Code = columns[76].ToString();
                                string strActivation16Code = columns[81].ToString();
                                string strActivation17Code = columns[86].ToString();
                                string strActivation18Code = columns[91].ToString();
                                string strActivation19Code = columns[96].ToString();
                                string strActivation20Code = columns[101].ToString();
                                string strActivation21Code = columns[106].ToString();
                                string strActivation22Code = columns[111].ToString();
                                string strActivation23Code = columns[116].ToString();
                                string strActivation24Code = columns[121].ToString();

                                string strMinTemperature1 = columns[4].ToString();
                                string strMinTemperature2 = columns[9].ToString();
                                string strMinTemperature3 = columns[14].ToString();
                                string strMinTemperature4 = columns[19].ToString();
                                string strMinTemperature5 = columns[24].ToString();
                                string strMinTemperature6 = columns[29].ToString();
                                string strMinTemperature7 = columns[34].ToString();
                                string strMinTemperature8 = columns[39].ToString();
                                string strMinTemperature9 = columns[44].ToString();
                                string strMinTemperature10 = columns[49].ToString();
                                string strMinTemperature11 = columns[54].ToString();
                                string strMinTemperature12 = columns[59].ToString();
                                string strMinTemperature13 = columns[64].ToString();  // Min Temp, 00:00 //
                                string strMinTemperature14 = columns[69].ToString();  // Min Temp, 01:00 //
                                string strMinTemperature15 = columns[74].ToString();
                                string strMinTemperature16 = columns[79].ToString();
                                string strMinTemperature17 = columns[84].ToString();
                                string strMinTemperature18 = columns[89].ToString();
                                string strMinTemperature19 = columns[94].ToString();
                                string strMinTemperature20 = columns[99].ToString();
                                string strMinTemperature21 = columns[104].ToString();
                                string strMinTemperature22 = columns[109].ToString();
                                string strMinTemperature23 = columns[114].ToString();
                                string strMinTemperature24 = columns[119].ToString();

                                if (string.IsNullOrEmpty(strActivation1Code) ||
                                    string.IsNullOrEmpty(strActivation2Code) ||
                                    string.IsNullOrEmpty(strActivation3Code) ||
                                    string.IsNullOrEmpty(strActivation4Code) ||
                                    string.IsNullOrEmpty(strActivation5Code) ||
                                    string.IsNullOrEmpty(strActivation6Code) ||
                                    string.IsNullOrEmpty(strActivation7Code) ||
                                    string.IsNullOrEmpty(strActivation8Code) ||
                                    string.IsNullOrEmpty(strActivation9Code) ||
                                    string.IsNullOrEmpty(strActivation10Code) ||
                                    string.IsNullOrEmpty(strActivation11Code) ||
                                    string.IsNullOrEmpty(strActivation12Code) ||
                                    string.IsNullOrEmpty(strActivation13Code) ||
                                    string.IsNullOrEmpty(strActivation14Code) ||
                                    string.IsNullOrEmpty(strActivation15Code) ||
                                    string.IsNullOrEmpty(strActivation16Code) ||
                                    string.IsNullOrEmpty(strActivation17Code) ||
                                    string.IsNullOrEmpty(strActivation18Code) ||
                                    string.IsNullOrEmpty(strActivation19Code) ||
                                    string.IsNullOrEmpty(strActivation20Code) ||
                                    string.IsNullOrEmpty(strActivation21Code) ||
                                    string.IsNullOrEmpty(strActivation22Code) ||
                                    string.IsNullOrEmpty(strActivation23Code) ||
                                    string.IsNullOrEmpty(strActivation24Code) )
                                {
                                    // Add record into Errors datatable [Invalid Spreadsheet line] //
                                    StoreImportError(dsErrors3, strGrittingForecastFloatingFile, strStoreNumber, "", "", "Line Number " + intLineNumber.ToString() + " - No Road State");
                                    intImportedFile1ErrorCount++;
                                    continue;
                                }
                                drMatch["ForecastActivationPoint"] = strActivationCodeExtra + ", " + strActivation1Code + ", " + strActivation2Code + ", " + strActivation3Code + ", " + strActivation4Code + ", " + strActivation5Code + ", " + strActivation6Code + ", " + strActivation7Code + ", " + strActivation8Code + ", " + strActivation9Code + ", " + strActivation10Code + ", " + strActivation11Code + ", " + strActivation12Code + ", " + strActivation13Code + ", " + strActivation14Code + ", " + strActivation15Code + ", " + strActivation16Code + ", " + strActivation17Code + ", " + strActivation18Code + ", " + strActivation19Code + ", " + strActivation20Code + ", " + strActivation21Code + ", " + strActivation22Code + ", " + strActivation23Code + ", " + strActivation24Code;
                                if (string.IsNullOrEmpty(strMinTemperature1) ||
                                    string.IsNullOrEmpty(strMinTemperature2) ||
                                    string.IsNullOrEmpty(strMinTemperature3) ||
                                    string.IsNullOrEmpty(strMinTemperature4) ||
                                    string.IsNullOrEmpty(strMinTemperature5) ||
                                    string.IsNullOrEmpty(strMinTemperature6) ||
                                    string.IsNullOrEmpty(strMinTemperature7) ||
                                    string.IsNullOrEmpty(strMinTemperature8) ||
                                    string.IsNullOrEmpty(strMinTemperature9) ||
                                    string.IsNullOrEmpty(strMinTemperature10) ||
                                    string.IsNullOrEmpty(strMinTemperature11) ||
                                    string.IsNullOrEmpty(strMinTemperature12) ||
                                    string.IsNullOrEmpty(strMinTemperature13) ||
                                    string.IsNullOrEmpty(strMinTemperature14) ||
                                    string.IsNullOrEmpty(strMinTemperature15) ||
                                    string.IsNullOrEmpty(strMinTemperature16) ||
                                    string.IsNullOrEmpty(strMinTemperature17) ||
                                    string.IsNullOrEmpty(strMinTemperature18) ||
                                    string.IsNullOrEmpty(strMinTemperature19) ||
                                    string.IsNullOrEmpty(strMinTemperature20) ||
                                    string.IsNullOrEmpty(strMinTemperature21) ||
                                    string.IsNullOrEmpty(strMinTemperature22) ||
                                    string.IsNullOrEmpty(strMinTemperature23) ||
                                    string.IsNullOrEmpty(strMinTemperature24) )
                                {
                                    // Add record into Errors datatable [Invalid Spreadsheet line] //
                                    StoreImportError(dsErrors3, strGrittingForecastFloatingFile, strStoreNumber, "", "", "Line Number " + intLineNumber.ToString() + " - No Temperature");
                                    intImportedFile1ErrorCount++;
                                    continue;
                                }
                                drMatch["ForecastActivationPoint"] += ", " + strMinTemperature1 + ", " + strMinTemperature2 + ", " + strMinTemperature3 + ", " + strMinTemperature4 + ", " + strMinTemperature5 + ", " + strMinTemperature6 + ", " + strMinTemperature7 + ", " + strMinTemperature8 + ", " + strMinTemperature9 + ", " + strMinTemperature10 + ", " + strMinTemperature11 + ", " + strMinTemperature12 + ", " + strMinTemperature13 + ", " + strMinTemperature14 + ", " + strMinTemperature15 + ", " + strMinTemperature16 + ", " + strMinTemperature17 + ", " + strMinTemperature18 + ", " + strMinTemperature19 + ", " + strMinTemperature20 + ", " + strMinTemperature21 + ", " + strMinTemperature22 + ", " + strMinTemperature23 + ", " + strMinTemperature24;
                                // Check All temperatures are numeric decimals //
                                decimal decMinTemperature1;
                                bool result1 = decimal.TryParse(strMinTemperature1, out decMinTemperature1);
                                decimal decMinTemperature2;
                                bool result2 = decimal.TryParse(strMinTemperature2, out decMinTemperature2);
                                decimal decMinTemperature3;
                                bool result3 = decimal.TryParse(strMinTemperature3, out decMinTemperature3);
                                decimal decMinTemperature4;
                                bool result4 = decimal.TryParse(strMinTemperature4, out decMinTemperature4);
                                decimal decMinTemperature5;
                                bool result5 = decimal.TryParse(strMinTemperature5, out decMinTemperature5);
                                decimal decMinTemperature6;
                                bool result6 = decimal.TryParse(strMinTemperature6, out decMinTemperature6);
                                decimal decMinTemperature7;
                                bool result7 = decimal.TryParse(strMinTemperature7, out decMinTemperature7);
                                decimal decMinTemperature8;
                                bool result8 = decimal.TryParse(strMinTemperature8, out decMinTemperature8);
                                decimal decMinTemperature9;
                                bool result9 = decimal.TryParse(strMinTemperature9, out decMinTemperature9);
                                decimal decMinTemperature10;
                                bool result10 = decimal.TryParse(strMinTemperature10, out decMinTemperature10);
                                decimal decMinTemperature11;
                                bool result11 = decimal.TryParse(strMinTemperature11, out decMinTemperature11);
                                decimal decMinTemperature12;
                                bool result12 = decimal.TryParse(strMinTemperature12, out decMinTemperature12);
                                decimal decMinTemperature13;
                                bool result13 = decimal.TryParse(strMinTemperature13, out decMinTemperature13);
                                decimal decMinTemperature14;
                                bool result14 = decimal.TryParse(strMinTemperature14, out decMinTemperature14);
                                decimal decMinTemperature15;
                                bool result15 = decimal.TryParse(strMinTemperature15, out decMinTemperature15);
                                decimal decMinTemperature16;
                                bool result16 = decimal.TryParse(strMinTemperature16, out decMinTemperature16);
                                decimal decMinTemperature17;
                                bool result17 = decimal.TryParse(strMinTemperature17, out decMinTemperature17);
                                decimal decMinTemperature18;
                                bool result18 = decimal.TryParse(strMinTemperature18, out decMinTemperature18);
                                decimal decMinTemperature19;
                                bool result19 = decimal.TryParse(strMinTemperature19, out decMinTemperature19);
                                decimal decMinTemperature20;
                                bool result20 = decimal.TryParse(strMinTemperature20, out decMinTemperature20);
                                decimal decMinTemperature21;
                                bool result21 = decimal.TryParse(strMinTemperature21, out decMinTemperature21);
                                decimal decMinTemperature22;
                                bool result22 = decimal.TryParse(strMinTemperature22, out decMinTemperature22);
                                decimal decMinTemperature23;
                                bool result23 = decimal.TryParse(strMinTemperature23, out decMinTemperature23);
                                decimal decMinTemperature24;
                                bool result24 = decimal.TryParse(strMinTemperature24, out decMinTemperature24);
                                if (!result1 || !result2 || !result3 || !result4 || !result5 || !result6 || !result7 || !result8 || !result9 || !result10 || !result11 || !result12 || !result13 || !result14 || !result15 || !result16 || !result17 || !result18 || !result19 || !result20 || !result21 || !result22 || !result23 || !result24)
                                {
                                    // Add record into Errors datatable [Invalid Spreadsheet line] //
                                    StoreImportError(dsErrors3, strGrittingForecastFloatingFile, strStoreNumber, "", "", "Line Number " + intLineNumber.ToString() + " - No Temperature");
                                    intImportedFile1ErrorCount++;
                                    continue;
                                }

                                // Find Activation Point in database for spreadsheet activation code and gets it's numeric corresponding value to see if it is greater than that for the site //
                                DataRow[] drFilteredActivationCodes = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation1Code + "'");
                                DataRow[] drFilteredActivationCodes2 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation2Code + "'");
                                DataRow[] drFilteredActivationCodes3 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation3Code + "'");
                                DataRow[] drFilteredActivationCodes4 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation4Code + "'");
                                DataRow[] drFilteredActivationCodes5 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation5Code + "'");
                                DataRow[] drFilteredActivationCodes6 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation6Code + "'");
                                DataRow[] drFilteredActivationCodes7 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation7Code + "'");
                                DataRow[] drFilteredActivationCodes8 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation8Code + "'");
                                DataRow[] drFilteredActivationCodes9 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation9Code + "'");
                                DataRow[] drFilteredActivationCodes10 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation10Code + "'");
                                DataRow[] drFilteredActivationCodes11 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation11Code + "'");
                                DataRow[] drFilteredActivationCodes12 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation12Code + "'");
                                DataRow[] drFilteredActivationCodes13 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation13Code + "'");
                                DataRow[] drFilteredActivationCodes14 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation14Code + "'");
                                DataRow[] drFilteredActivationCodes15 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation15Code + "'");
                                DataRow[] drFilteredActivationCodes16 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation16Code + "'");
                                DataRow[] drFilteredActivationCodes17 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation17Code + "'");
                                DataRow[] drFilteredActivationCodes18 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation18Code + "'");
                                DataRow[] drFilteredActivationCodes19 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation19Code + "'");
                                DataRow[] drFilteredActivationCodes20 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation20Code + "'");
                                DataRow[] drFilteredActivationCodes21 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation21Code + "'");
                                DataRow[] drFilteredActivationCodes22 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation22Code + "'");
                                DataRow[] drFilteredActivationCodes23 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation23Code + "'");
                                DataRow[] drFilteredActivationCodes24 = dsWeatherActivationCodes.Tables[0].Select("GrittingActivationCode = '" + strActivation24Code + "'");
                                if (drFilteredActivationCodes.Length > 0 &&
                                    drFilteredActivationCodes2.Length > 0 &&
                                    drFilteredActivationCodes3.Length > 0 &&
                                    drFilteredActivationCodes4.Length > 0 &&
                                    drFilteredActivationCodes5.Length > 0 &&
                                    drFilteredActivationCodes6.Length > 0 &&
                                    drFilteredActivationCodes7.Length > 0 &&
                                    drFilteredActivationCodes8.Length > 0 &&
                                    drFilteredActivationCodes9.Length > 0 &&
                                    drFilteredActivationCodes10.Length > 0 &&
                                    drFilteredActivationCodes11.Length > 0 &&
                                    drFilteredActivationCodes12.Length > 0 &&
                                    drFilteredActivationCodes13.Length > 0 &&
                                    drFilteredActivationCodes14.Length > 0 &&
                                    drFilteredActivationCodes15.Length > 0 &&
                                    drFilteredActivationCodes16.Length > 0 &&
                                    drFilteredActivationCodes17.Length > 0 &&
                                    drFilteredActivationCodes18.Length > 0 &&
                                    drFilteredActivationCodes19.Length > 0 &&
                                    drFilteredActivationCodes20.Length > 0 &&
                                    drFilteredActivationCodes21.Length > 0 &&
                                    drFilteredActivationCodes22.Length > 0 &&
                                    drFilteredActivationCodes23.Length > 0 &&
                                    drFilteredActivationCodes24.Length > 0 )
                                {
                                    // Match found so see if a grit is required //
                                    DataRow dr = drFilteredActivationCodes[0];
                                    DataRow dr2 = drFilteredActivationCodes2[0];
                                    DataRow dr3 = drFilteredActivationCodes3[0];
                                    DataRow dr4 = drFilteredActivationCodes4[0];
                                    DataRow dr5 = drFilteredActivationCodes5[0];
                                    DataRow dr6 = drFilteredActivationCodes6[0];
                                    DataRow dr7 = drFilteredActivationCodes7[0];
                                    DataRow dr8 = drFilteredActivationCodes8[0];
                                    DataRow dr9 = drFilteredActivationCodes9[0];
                                    DataRow dr10 = drFilteredActivationCodes10[0];
                                    DataRow dr11 = drFilteredActivationCodes11[0];
                                    DataRow dr12 = drFilteredActivationCodes12[0];
                                    DataRow dr13 = drFilteredActivationCodes13[0];
                                    DataRow dr14 = drFilteredActivationCodes14[0];
                                    DataRow dr15 = drFilteredActivationCodes15[0];
                                    DataRow dr16 = drFilteredActivationCodes16[0];
                                    DataRow dr17 = drFilteredActivationCodes17[0];
                                    DataRow dr18 = drFilteredActivationCodes18[0];
                                    DataRow dr19 = drFilteredActivationCodes19[0];
                                    DataRow dr20 = drFilteredActivationCodes20[0];
                                    DataRow dr21 = drFilteredActivationCodes21[0];
                                    DataRow dr22 = drFilteredActivationCodes22[0];
                                    DataRow dr23 = drFilteredActivationCodes23[0];
                                    DataRow dr24 = drFilteredActivationCodes24[0];
                                    if ((Convert.ToInt32(dr["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature1) ||
                                       (Convert.ToInt32(dr2["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature2) ||
                                       (Convert.ToInt32(dr3["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature3) ||
                                       (Convert.ToInt32(dr4["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature4) ||
                                       (Convert.ToInt32(dr5["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature5) ||
                                       (Convert.ToInt32(dr6["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature6) ||
                                       (Convert.ToInt32(dr7["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature7) ||
                                       (Convert.ToInt32(dr8["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature8) ||
                                       (Convert.ToInt32(dr9["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature9) ||
                                       (Convert.ToInt32(dr10["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature10) ||
                                       (Convert.ToInt32(dr11["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature11) ||
                                       (Convert.ToInt32(dr12["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature12) ||
                                       (Convert.ToInt32(dr13["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature13) ||
                                       (Convert.ToInt32(dr14["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature14) ||
                                       (Convert.ToInt32(dr15["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature15) ||
                                       (Convert.ToInt32(dr16["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature16) ||
                                       (Convert.ToInt32(dr17["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature17) ||
                                       (Convert.ToInt32(dr18["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature18) ||
                                       (Convert.ToInt32(dr19["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature19) ||
                                       (Convert.ToInt32(dr20["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature20) ||
                                       (Convert.ToInt32(dr21["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature21) ||
                                       (Convert.ToInt32(dr22["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature22) ||
                                       (Convert.ToInt32(dr23["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature23) ||
                                       (Convert.ToInt32(dr24["GrittingActivationCodeValue"].ToString()) >= Convert.ToInt32(drMatch["GrittingActivationCodeValue"].ToString()) && Convert.ToDecimal(drMatch["MinimumTemperature"].ToString()) >= decMinTemperature24) ||
                                        (strActivationCodeExtra.ToLower() == "red" && intRedOverridesBanding == 1))
                                    {
                                        if (Convert.ToInt32(drMatch["HolidayCount"]) > 0)  // Site on holiday so No Gritting Required //
                                        {
                                            boolGritRequired = false;
                                        }
                                        else if (Convert.ToInt32(drMatch[strDayOfWeekColumnToUse]) == 0)  // Site doesn't have a site gritting pattern for today so No Gritting Required //
                                        {
                                            boolGritRequired = false;
                                        }
                                        else  // Grit //
                                        {
                                            drMatch["SnowForcasted"] = (boolSnowForecast ? 1 : 0);
                                            drMatch["RequiredGritAmount"] = (boolSnowForecast ? Convert.ToDecimal(drMatch["DefaultGritAmount"]) * (decimal)2.00 : Convert.ToDecimal(drMatch["DefaultGritAmount"]));
                                            drMatch["DoubleGrit"] = (boolSnowForecast ? 1 : 0);
                                            drMatch["ForecastForEmail"] = columns[2].ToString();  // Store Colour not Temperature and Road State here //
                                            dataSet_GC_Core.sp04348_GC_Gritting_Import_Floating_Forecast_Data.ImportRow(drMatch);  // Only add first row //
                                            intImportedFile1Count++;
                                            boolGritRequired = true;
                                        }
                                    }
                                }
                            }
                            if (!boolGritRequired)  // No Gritting Required //
                            {
                                drMatch["SnowForcasted"] = (boolSnowForecast ? 1 : 0);
                                drMatch["RequiredGritAmount"] = (boolSnowForecast ? Convert.ToDecimal(drMatch["DefaultGritAmount"]) * (decimal)2.00 : Convert.ToDecimal(drMatch["DefaultGritAmount"]));
                                drMatch["DoubleGrit"] = (boolSnowForecast ? 1 : 0);
                                drMatch["ForecastForEmail"] = columns[2].ToString();  // Store Colour not Temperature and Road State here //
                                if (string.IsNullOrEmpty(drMatch["SkippedReason"].ToString())) drMatch["SkippedReason"] = "Weather Not Activated";
                                dataSet_GC_Core.sp04349_GC_Gritting_Import_Floating_Forecast_Data.ImportRow(drMatch);  // Only add first row //
                                intImportedFile1Count++;
                            }
                            dsSites.Tables[0].Rows.Remove(drMatch);  // Remove any matches from original dataset as it's been processed and this will speed up subsequent records as it will be a smaller dataset to search //
                        }
                    }
                    else
                    {
                        // Add record into Errors datatable [Not present in Database] //
                        StoreImportError(dsErrors3, strGrittingForecastFloatingFile, strStoreNumber, "", strActivation1Code, "Site Not Present in Database");
                        intImportedFile1ErrorCount++;
                        continue;
                    }


                }
                // Check if any records left which have not been moved to the import process (if yes, they are missing from Import Spreadsheet) //
                DataRow[] drRemainingRows = dsSites.Tables[0].Select("ForecastingTypeID = 2 or ForecastingTypeID = 3");  // GCML Winter Hazard CSV //
                if (drRemainingRows.Length > 0)
                {
                    // Add record into Errors datatable [Not present in Spreadsheet] //
                    foreach (DataRow dr in drRemainingRows)
                    {
                        StoreImportError(dsErrors3, strGrittingForecastFloatingFile, dr["HubID"].ToString(), dr["SiteName"].ToString(), dr["GrittingActivationCode"].ToString(), "Site Not Present in Import Spreadsheet");
                        intImportedFile1ErrorCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                gridControl1.EndUpdate();
                gridControl2.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Importing Forecast File [" + strGrittingForecastType1File + "].\n\nMessage = [" + ex.Message + "].\n\nThere may be a fault with the file. Please close this screen then try again. If the problem persists, contact Technical Support.", "Import Forecasting Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            #endregion

            view = (GridView)gridControl4.MainView;
            view.ExpandAllGroups();
            view = (GridView)gridControl5.MainView;
            view.ExpandAllGroups();
            gridControl4.EndUpdate();
            gridControl5.EndUpdate();

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            if (intImportedFile1ErrorCount == 0)
            {
                labelControlInformation3.Text = intImportedFile1Count.ToString() + (intImportedFile1Count == 1 ? " record" : " records") + " loaded from Floating Sites Forecast.";
                hyperLinkEditErrorCount3.Text = "No errors.";
                labelControlInformation3.Visible = true;
                hyperLinkEditErrorCount3.Visible = false;
                pictureEdit3.Image = imageCollection1.Images[6];
            }
            else
            {
                labelControlInformation3.Text = "No information present at this time.";
                hyperLinkEditErrorCount3.Text = intImportedFile1ErrorCount.ToString() + (intImportedFile1ErrorCount == 1 ? " error" : " errors") + " in Floating Sites Forecast.";
                labelControlInformation3.Visible = false;
                hyperLinkEditErrorCount3.Visible = true;
                pictureEdit3.Image = imageCollection1.Images[7];
            }
            Set_Floating_Site_Button_Enabled_Status();
        }

        private void hyperLinkEditErrorCount3_OpenLink(object sender, OpenLinkEventArgs e)
        {
            frm_GC_Import_Weather_Forecasts_Import_Errors frm_child = new frm_GC_Import_Weather_Forecasts_Import_Errors();
            //frm_styles.MdiParent = this.MdiParent;
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child.dsErrors = this.dsErrors3;
            frm_child.ShowDialog();
        }

        private void btnMoveRight2_Click(object sender, EventArgs e)
        {
            GridView viewFrom = (GridView)gridControl4.MainView;
            GridView viewTo = (GridView)gridControl5.MainView;
            int[] intRowHandles = viewFrom.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to move between lists before proceeding.", "Move Record(s) Between Lists", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Record" : intRowHandles.Length.ToString() + " Records") + " selected for moving between lists!\n\nProceed?", "Move Record(s) Between Lists", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Deleting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = (intRowHandles.Length * 2) / 10;  // Multiplied by 2 as we need 2 passes first to move rows in correct order, second pass (in reverse) to delete rows from source //
            int intUpdateProgressTempCount = 0;

            System.Data.DataTable dataTableTo = this.dataSet_GC_Core.sp04349_GC_Gritting_Import_Floating_Forecast_Data;
            viewFrom.BeginUpdate();
            viewTo.BeginUpdate();
            viewTo.BeginSelection();
            viewTo.ClearSelection();  // Remove any selected records //
            int intID = 0;
            int intFoundNewRow = 0;
            //for (int i = intRowHandles.Length - 1; i >= 0; i--)

            // 2 passes first to move rows in correct order, second pass (in reverse) to delete rows from source //
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                DataRow row = viewFrom.GetDataRow(intRowHandles[i]);
                intID = Convert.ToInt32(row["SiteGrittingContractID"]);
                if (row != null && row.Table != dataTableTo)
                {
                    dataTableTo.ImportRow(row);
                    //row.Delete();
                    // Highlight and make visible moved row //
                    intFoundNewRow = viewTo.LocateByValue(0, viewTo.Columns["SiteGrittingContractID"], intID);
                    if (intFoundNewRow != GridControl.InvalidRowHandle)
                    {
                        viewTo.SelectRow(intFoundNewRow);
                        viewTo.MakeRowVisible(intFoundNewRow, false);
                    }
                }
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            for (int i = intRowHandles.Length - 1; i >= 0; i--)
            {
                DataRow row = viewFrom.GetDataRow(intRowHandles[i]);
                row.Delete();
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            viewTo.EndSelection();
            viewTo.EndUpdate();
            viewFrom.EndUpdate();

            viewFrom.ActiveFilter.Clear();
            viewTo.ActiveFilter.Clear();

            dataSet_GC_Core.sp04348_GC_Gritting_Import_Floating_Forecast_Data.AcceptChanges();  // Commit the chnages and remove the deleted rows //
            dataSet_GC_Core.sp04349_GC_Gritting_Import_Floating_Forecast_Data.AcceptChanges();  // Commit the chnages and remove the deleted rows //
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Set_Floating_Site_Button_Enabled_Status();
        }

        private void btnMoveLeft2_Click(object sender, EventArgs e)
        {
            GridView viewFrom = (GridView)gridControl5.MainView;
            GridView viewTo = (GridView)gridControl4.MainView;
            int[] intRowHandles = viewFrom.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to move between lists before proceeding.", "Move Record(s) Between Lists", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Record" : intRowHandles.Length.ToString() + " Records") + " selected for moving between lists!\n\nProceed?", "Move Record(s) Between Lists", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Deleting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = (intRowHandles.Length * 2) / 10;  // Multiplied by 2 as we need 2 passes first to move rows in correct order, second pass (in reverse) to delete rows from source //
            int intUpdateProgressTempCount = 0;

            System.Data.DataTable dataTableTo = this.dataSet_GC_Core.sp04348_GC_Gritting_Import_Floating_Forecast_Data;
            viewFrom.BeginUpdate();
            viewTo.BeginUpdate();
            viewTo.BeginSelection();
            viewTo.ClearSelection();  // Remove any selected records //
            int intID = 0;
            int intFoundNewRow = 0;
            //for (int i = intRowHandles.Length - 1; i >= 0; i--)

            // 2 passes first to move rows in correct order, second pass (in reverse) to delete rows from source //
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                DataRow row = viewFrom.GetDataRow(intRowHandles[i]);
                intID = Convert.ToInt32(row["SiteGrittingContractID"]);
                if (row != null && row.Table != dataTableTo)
                {
                    dataTableTo.ImportRow(row);
                    //row.Delete();
                    // Highlight and make visible moved row //
                    intFoundNewRow = viewTo.LocateByValue(0, viewTo.Columns["SiteGrittingContractID"], intID);
                    if (intFoundNewRow != GridControl.InvalidRowHandle)
                    {
                        viewTo.SelectRow(intFoundNewRow);
                        viewTo.MakeRowVisible(intFoundNewRow, false);
                    }
                }
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            for (int i = intRowHandles.Length - 1; i >= 0; i--)
            {
                DataRow row = viewFrom.GetDataRow(intRowHandles[i]);
                row.Delete();
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }

            viewTo.EndSelection();
            viewTo.EndUpdate();
            viewFrom.EndUpdate();

            viewFrom.ActiveFilter.Clear();
            viewTo.ActiveFilter.Clear();

            dataSet_GC_Core.sp04348_GC_Gritting_Import_Floating_Forecast_Data.AcceptChanges();  // Commit the chnages and remove the deleted rows //
            dataSet_GC_Core.sp04349_GC_Gritting_Import_Floating_Forecast_Data.AcceptChanges();  // Commit the chnages and remove the deleted rows //
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Set_Floating_Site_Button_Enabled_Status();

        }

        private void Set_Floating_Site_Button_Enabled_Status()
        {
            GridView view = (GridView)gridControl4.MainView;
            btnSendFloatingClientEmails.Enabled = (view.DataRowCount > 0 ? true : false);
            btnLoadFloatingJobs.Enabled = (view.DataRowCount > 0 ? true : false);

            view = (GridView)gridControl6.MainView;
            btnCreateFloatingJobs.Enabled = (view.DataRowCount > 0 ? true : false);
        }

        private void btnSendFloatingClientEmails_Click(object sender, EventArgs e)
        {
            // Check for internet Connectivity //
            bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
            if (!boolNoInternet) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send emails.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            #region GetConfigurationSettings
            
            string strEmailSubjectLine = "";
            string strGrittingEmailsFromName = "";
            string strCCToEmailAddress = "";
            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strEmailLayoutFile = "";
            string strSMTPMailServerPort = "";
            try
            {
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp04350_GC_Floating_Emails_Get_Settings", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
                DataSet dsSettings = new DataSet("NewDataSet");
                sdaSettings.Fill(dsSettings, "Table");
                conn.Close();
                conn.Dispose();
                if (dsSettings.Tables[0].Rows.Count != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                DataRow dr = dsSettings.Tables[0].Rows[0];
                strEmailSubjectLine = dr["GrittingFloatingSiteEmailSubjectLine"].ToString();
                strGrittingEmailsFromName = dr["GrittingEmailsFromName"].ToString();
                strCCToEmailAddress = dr["GrittingFloatingSiteEmailCCToEmailAddress"].ToString();
                strSMTPMailServerAddress = dr["SMTPMailServerAddress"].ToString();
                strSMTPMailServerUsername = dr["SMTPMailServerUsername"].ToString();
                strSMTPMailServerPassword = dr["SMTPMailServerPassword"].ToString();
                strEmailLayoutFile = dr["GrittingFloatingSiteEmailHtmlLayoutFile"].ToString();
                strSMTPMailServerPort = dr["SMTPMailServerPort"].ToString();
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Email Settings' (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

            if (string.IsNullOrEmpty(strEmailLayoutFile) || !File.Exists(strEmailLayoutFile))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The Gritting Email Layout File [" + strSMTPMailServerPort + "] does not exist.\n\nPlease select a valid file using the System Configuration screen before proceeding.", "Check File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            #endregion

            GridView view = (GridView)gridControl4.MainView;
            view.ActiveFilter.Clear();
            int intCount = view.DataRowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Sites are likely to need gritting tonight or you have not yet loaded the Floating Site Forecast - unable to send emails.\n\nTry importing todays FLoating Site weather forecasts before proceeding.", "Send Emails", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about send out gritting emails.\n\nAre you sure you wish to continue?", "Send Emails", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            string strClientIDs = ",";
            try
            {
                foreach (DataRow row in dataSet_GC_Core.sp04348_GC_Gritting_Import_Floating_Forecast_Data)
                {
                    if (!strClientIDs.Contains(',' + row["ClientID"].ToString() + ',')) strClientIDs += row["ClientID"].ToString() + ',';
                }
                if (!string.IsNullOrEmpty(strClientIDs))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] strArray = strClientIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    char[] delimiters2 = new char[] { ';' };

                    if (strArray.Length > 0)
                    {
                        int intClientID = 0;
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);

                        foreach (string strClientID in strArray)
                        {
                            // Send Email //
                            intClientID = Convert.ToInt32(strClientID);

                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp04066_GC_Gritting_Send_Grit_Email_Get_Client_Details", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@intClientID", intClientID));
                            SqlDataAdapter sdaClientContactDetails = new SqlDataAdapter();
                            DataSet dsClientContactDetails = new DataSet("NewDataSet");
                            dsClientContactDetails.Clear();  // Reset //
                            sdaClientContactDetails = new SqlDataAdapter(cmd);
                            sdaClientContactDetails.Fill(dsClientContactDetails, "Table");

                            if (dsClientContactDetails.Tables[0].Rows.Count > 0)
                            {
                                DataRow dr = dsClientContactDetails.Tables[0].Rows[0];

                                string strBody = System.IO.File.ReadAllText(strEmailLayoutFile);

                                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                                msg.From = new System.Net.Mail.MailAddress(dr["EmailFrom"].ToString());
                                string[] strEmailTo = dr["EmailTo"].ToString().Split(delimiters2, StringSplitOptions.RemoveEmptyEntries);
                                if (strEmailTo.Length > 0)
                                {
                                    foreach (string strEmailAddress in strEmailTo)
                                    {
                                        msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                                    }
                                }
                                else
                                {
                                    msg.To.Add(new System.Net.Mail.MailAddress(dr["EmailTo"].ToString()));
                                }
                                msg.Subject = strEmailSubjectLine;
                                if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                                msg.Priority = System.Net.Mail.MailPriority.High;
                                msg.IsBodyHtml = true;

                                System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                                //create the LinkedResource (embedded image)
                                System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                                logo.ContentId = "companylogo";
                                //add the LinkedResource to the appropriate view
                                htmlView.LinkedResources.Add(logo);

                                //create the LinkedResource (embedded image)
                                System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                                logo2.ContentId = "companyfooter";
                                //add the LinkedResource to the appropriate view
                                htmlView.LinkedResources.Add(logo2);

                                msg.AlternateViews.Add(plainView);
                                msg.AlternateViews.Add(htmlView);

                                object userState = msg;
                                System.Net.Mail.SmtpClient emailClient = null;
                                if (intSMTPMailServerPort != 0)
                                {
                                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                                }
                                else
                                {
                                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                                }
                                if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                                {
                                    System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                                    emailClient.UseDefaultCredentials = false;
                                    emailClient.Credentials = basicCredential;
                                }
                                //emailClient.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(SendCompletedCallback);
                                emailClient.SendAsync(msg, userState);
                            }
                        }
                        SQlConn.Close();
                        SQlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while sending the Emails.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Emails", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            DevExpress.XtraEditors.XtraMessageBox.Show("Email(s) Sent Successfully.", "Send Emails", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            //String token = (string)e.UserState;

            if (e.Cancelled)
            {
                //Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                //Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                //Console.WriteLine("Message sent.");
            }
            //mailSent = true;
        }

        private void btnLoadFloatingJobs_Click(object sender, EventArgs e)
        {
            GridView siteView = (GridView)gridControl4.MainView;
            if (siteView.DataRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load jobs, import the Floating Site Forecast first.", "Load Floating Site Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            #region Check Files

            frmProgress fProgress = new frmProgress(30);
            fProgress.UpdateCaption("Checking Floating Site Job Import File...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            // Get Forecasting filenames from System_Settings table //
            string strFloatingSiteJobFile = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strFloatingSiteJobFile = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingFloatingJobsFilePath").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the name of Floating Site Job Import File (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Job File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(30); // Update Progress Bar //

            // Check Files exist //
            if (!File.Exists(strFloatingSiteJobFile))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The Floating Site Job Import File [" + strFloatingSiteJobFile + "] does not exist.\n\nPlease ensure today's job files have been saved to the correct location and that they have been named correctly before trying again.", "Check Job File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check that files were last modified today //
            DateTime lastWriteTime = File.GetLastWriteTime(strGrittingForecastFloatingFile);
            //DateTime creationTime = File.GetCreationTime(strGrittingForecastType1File);
            //DateTime lastAccessTime = File.GetLastAccessTime(strGrittingForecastType1File);
            if (lastWriteTime.Date != DateTime.Today)
            {
                if (fProgress != null) fProgress.Hide();
                if (DevExpress.XtraEditors.XtraMessageBox.Show("WARNING...  WARNING...  WARNING...  WARNING...  WARNING...\n=========================================\n\nThe last modified date of [" + strFloatingSiteJobFile + "] is not equal to todays date.\n\nThis file may be a historical file!\n\nAre you sure you wish to proceed?", "Check Job File Last Modified Date", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                fProgress.Show();
                if (fProgress != null) fProgress.UpdateProgress(40); // Update Progress Bar //
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            System.Windows.Forms.Application.DoEvents();  // Allow Form time to repaint itself //

            #endregion

            // Job Filename present and date last modified dates is valid so attempt to parse it //
            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Importing Job Files [File 1]...");
            fProgress.Show();
            DateTime dtToday = DateTime.Today;
            System.Windows.Forms.Application.DoEvents();

            gridControl6.BeginUpdate();

            dsErrors3.Tables[0].Rows.Clear();  // Clear Errors //
            GridView jobView = (GridView)gridControl6.MainView;
            jobView.BeginUpdate();
            jobView.BeginDataUpdate();
            dataSet_GC_Core.sp04351_GC_Floating_Site_Jobs_Import_Data.Rows.Clear();
            jobView.EndDataUpdate();
            jobView.EndUpdate();

            labelControlInformation4.Text = "No information present at this time.";
            hyperLinkEditErrorCount4.Text = "No errors.";
            labelControlInformation4.Visible = true;
            hyperLinkEditErrorCount4.Visible = false;
            pictureEdit4.Image = imageCollection1.Images[6];

            System.Windows.Forms.Application.DoEvents();  // Allow Form time to repaint itself //

            int intImportedFile1ErrorCount = 0;
            int intImportedFile1Count = 0;

            #region Import Floating Forecast Spreadsheet

            int intLineNumber = 0;
            try
            {
                string[] lines = File.ReadAllLines(strFloatingSiteJobFile);

                int intUpdateProgressThreshhold = lines.Length / 10;
                int intUpdateProgressTempCount = 0;

                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                ArrayList arrayImportStructure = new ArrayList();
                foreach (string line in lines)
                {
                    intLineNumber++;

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }

                    if (intLineNumber == 1) continue;  // Skip first row (column headers) //

                    string[] columns = r.Split(line);  // Get Columns into Array //
                    if (columns.Length < 19) continue;
                    if (columns[1].ToString() == "" && columns[2].ToString() == "") continue;

                    string strPostcode = columns[14].ToString();
                    string strPostcodeFirstPart = "";
                    char[] delimiters = new char[] { ' ' };
                    string[] strArray = columns[14].ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray.Length == 2)
                    {
                        strPostcodeFirstPart = strArray[0];
                    }
                    else if (strArray.Length == 1)  // Postcode is all in one (no space between first bit and last) //
                    {
                        strPostcodeFirstPart = strArray[0];
                        if (strPostcodeFirstPart.Length > 4) strPostcodeFirstPart = strPostcodeFirstPart.Remove(strPostcodeFirstPart.Length - 3);

                    }
                    if (string.IsNullOrEmpty(strPostcodeFirstPart))
                    {
                        // Add record into Errors datatable [Invalid Spreadsheet line] //
                        StoreImportError(dsErrors4, strGrittingForecastFloatingFile, strPostcode, "", "", "Line Number " + intLineNumber.ToString() + " - No Valid Postcode");
                        intImportedFile1ErrorCount++;
                        continue;
                    }
                    int intRowHandle = siteView.LocateByValue(0, siteView.Columns["HubID"], strPostcodeFirstPart.ToUpper());
                    if (intRowHandle == GridControl.InvalidRowHandle) continue;

                    DataRow dr = this.dataSet_GC_Core.sp04348_GC_Gritting_Import_Floating_Forecast_Data.Rows[siteView.GetDataSourceRowIndex(intRowHandle)];
                    dr["LineID"] = intLineNumber;
                    dr["OrderNumber"] = columns[1].ToString();
                    dr["MainWorkCtr"] = columns[2].ToString();
                    dr["Address"] = columns[13].ToString().Replace("\"", string.Empty).Replace(",", string.Empty).Replace("&", "and");
                    dr["Address"] = columns[13].ToString().Replace("\"", string.Empty).Replace(",", string.Empty).Replace("&", "and");
                    dr["PostCode"] = columns[14].ToString().Replace("\"", string.Empty).Replace(",", string.Empty);
                    dr["Location"] = columns[7].ToString().Replace("\"", string.Empty).Replace(",", string.Empty);
                    dataSet_GC_Core.sp04351_GC_Floating_Site_Jobs_Import_Data.ImportRow(dr);
                    intImportedFile1Count++;
                }
                this.dataSet_GC_Core.sp04351_GC_Floating_Site_Jobs_Import_Data.AcceptChanges();
            }
            catch (Exception ex)
            {
                gridControl6.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Importing Forecast File [" + strGrittingForecastType1File + "].\n\nMessage = [" + ex.Message + "].\n\nThere may be a fault with the file. Please close this screen then try again. If the problem persists, contact Technical Support.", "Import Forecasting Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            #endregion

            jobView.ExpandAllGroups();
            gridControl6.EndUpdate();

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            if (intImportedFile1ErrorCount == 0)
            {
                labelControlInformation4.Text = intImportedFile1Count.ToString() + (intImportedFile1Count == 1 ? " record" : " records") + " loaded from Floating Site Jobs.";
                hyperLinkEditErrorCount4.Text = "No errors.";
                labelControlInformation4.Visible = true;
                hyperLinkEditErrorCount4.Visible = false;
                pictureEdit4.Image = imageCollection1.Images[6];
            }
            else
            {
                labelControlInformation4.Text = "No information present at this time.";
                hyperLinkEditErrorCount4.Text = intImportedFile1ErrorCount.ToString() + (intImportedFile1ErrorCount == 1 ? " error" : " errors") + " in Floating Site Jobs.";
                labelControlInformation4.Visible = false;
                hyperLinkEditErrorCount4.Visible = true;
                pictureEdit4.Image = imageCollection1.Images[7];
            }
            Set_Floating_Site_Button_Enabled_Status();
        }


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {

        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 4;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView4_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "PreferredTeam")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SubContractorID")) == 0)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    //e.Appearance.BackColor = Color.LightCoral;
                    //e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        #endregion


        #region GridView5

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 5;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView5_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "PreferredTeam")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SubContractorID")) == 0)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    //e.Appearance.BackColor = Color.LightCoral;
                    //e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }

        }

        #endregion


        #region GridView6

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 6;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView6_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "PreferredTeam")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SubContractorID")) == 0)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    //e.Appearance.BackColor = Color.LightCoral;
                    //e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }

        }

        #endregion


        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Route route = RouteDirections.GetRoute(true,
          new Location("Cromdale, UK"),
          new Location("Grantown-on-Spey, UK"),
          new Location("Aviemore, UK"),
          new Location("Forres, UK"),
          new Location("Elgin, UK"),
          new Location("Inverness, UK"));
        }

        private void hyperLinkEditErrorCount4_OpenLink(object sender, OpenLinkEventArgs e)
        {
            frm_GC_Import_Weather_Forecasts_Import_Errors frm_child = new frm_GC_Import_Weather_Forecasts_Import_Errors();
            //frm_styles.MdiParent = this.MdiParent;
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child.dsErrors = this.dsErrors4;
            frm_child.ShowDialog();

        }

        private void btnCreateFloatingJobs_Click(object sender, EventArgs e)
        {
            int intNewRecordID = 0;
            DateTime dtCalloutDateTime = DateTime.Now;
            int intRecordedByStaffID = this.GlobalSettings.UserID;
            GridView view = (GridView)gridControl6.MainView;
            int intJobCount = view.DataRowCount;
            if (intJobCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Floating Jobs available for creation.\n\nTry importing the Floating Forecast and Floating Job speadsheets before proceeding.", "Generate Floating Site Gritting Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intUpdateProgressThreshhold = intJobCount / 10;
            int intUpdateProgressTempCount = 0;

            frmProgress fProgress = new frmProgress(30);
            fProgress.UpdateCaption("Creating Floating Site Gritting Callouts...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            // Step 1 - Create floating gritting callouts //
            int intSiteGrittingContractID = 0;
            int intSubContractorID = 0;
            int intPreferredSubContractorID = 0;
            int intSiteID = 0;
            int intJobStatusID = 50;  // CalloutStatus 50 = "Completed - Ready To Send" //
            decimal decSaltUsed = (decimal)0.00;
            decimal decClientProactivePrice = (decimal)0.00;
            decimal decEveningRateModifier = (decimal)0.00;
            int intClientPOID = 0;
            string strSiteAddress1 = "";
            string strSitePostcode = "";
            int intAttendanceOrder = 0;
            int intIsFloatingSite = 1;  // Hardcoded to 1 //
            string strClientPONumber = "";
            string strLocation = "";
            int intDoubleGrit = 0;

            // Get VAT Rate from System_Settings table //
            string strDefaultVATRate = "";
            decimal decDefaultVatRate = (decimal)0.00;
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultVATRate = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate").ToString();
                if (!string.IsNullOrEmpty(strDefaultVATRate)) decDefaultVatRate = Convert.ToDecimal(strDefaultVATRate);
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Default VAT Rate' (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default VAT Rate", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter AddRecord = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
            AddRecord.ChangeConnectionString(strConnectionString);
            try
            {
                DataRow[] drRows = dataSet_GC_Core.sp04351_GC_Floating_Site_Jobs_Import_Data.Select();
                foreach (DataRow row in drRows)
                {
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }

                    intSiteGrittingContractID = Convert.ToInt32(row["SiteGrittingContractID"]);
                    intSubContractorID = Convert.ToInt32(row["SubContractorID"]);
                    intPreferredSubContractorID = Convert.ToInt32(row["PreferredSubContractorID"]);
                    intSiteID = Convert.ToInt32(row["SiteID"]);
                    decSaltUsed = Convert.ToDecimal(row["RequiredGritAmount"]);
                    decClientProactivePrice = Convert.ToDecimal(row["ClientProactivePrice"]);
                    decEveningRateModifier = Convert.ToDecimal(row["EveningRateModifier"]);
                    intClientPOID = Convert.ToInt32(row["ClientPOID"]);
                    strSiteAddress1 = row["Address"].ToString();
                    strSitePostcode = row["Postcode"].ToString();
                    strClientPONumber = row["OrderNumber"].ToString();
                    strLocation = row["Location"].ToString();
                    intDoubleGrit = Convert.ToInt32(row["DoubleGrit"]);
                    // Lat \ Long is looked up and set within Add Record SP //

                    intNewRecordID = Convert.ToInt32(AddRecord.sp04352_GC_Create_Floating_Griting_Callout_From_Forecast(intSiteGrittingContractID, intSubContractorID, intPreferredSubContractorID, intSiteID, intJobStatusID, dtCalloutDateTime, 0, decSaltUsed, intRecordedByStaffID, decDefaultVatRate, decClientProactivePrice, decEveningRateModifier, intClientPOID, strSiteAddress1, strSitePostcode, intAttendanceOrder, intIsFloatingSite, strClientPONumber, strLocation, intDoubleGrit));  // Add Row to database //
                    row.Delete(); // Remove row now it has been added to the database //
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating Gritting Callouts.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Generate Floating Site Gritting Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Process Completed Successfully.\n\nWould you like to open the Gritting Callout Manager so you can view and send any generated completed callouts to teams?", "Generate Floating Site Gritting Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                // Open Callout Manager //
                frm_GC_Callout_Manager frmInstance = new frm_GC_Callout_Manager();

                frmInstance.MdiParent = this.MdiParent;
                frmInstance.GlobalSettings = this.GlobalSettings;
                frmInstance.fProgress = fProgress;
                frmInstance.Show();

                MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(frmInstance, new object[] { null });
            }

        }


        #endregion

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp04358_GC_Jobs_Requiring_Confirmation_Email", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaCallouts = new SqlDataAdapter(cmd);
                DataSet dsCallouts = new DataSet("NewDataSet");
                sdaCallouts.Fill(dsCallouts, "Table");

                string strCalloutIDs = "";
                foreach (DataRow dr in dsCallouts.Tables[0].Rows)
                {
                    strCalloutIDs += dr["GrittingCallOutID"].ToString() + ",";
                }
                string strPicturePath = @"C:\Mark_Development\Sim_Images";

                SqlCommand cmd2 = null;
                cmd2 = new SqlCommand("sp04366_GC_Jobs_Requiring_Confirmation_Email_Pictures", conn);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
                cmd2.Parameters.Add(new SqlParameter("@CalloutIDs", strCalloutIDs));
                SqlDataAdapter sdaPictures = new SqlDataAdapter(cmd2);
                DataSet dsPictures = new DataSet("NewDataSet");
                sdaPictures.Fill(dsPictures, "Table");

                conn.Close();
                conn.Dispose();

                if (dsCallouts.Tables[0].Rows.Count <= 0)
                {

                    return;
                }
                int intCalloutID = 0;
                string strSiteName = "";
                string strSiteEmail = "";
                string strCalloutCompletedTime;
                int intEmailsSentCount = 0;
                int intEmailLinkedPictures = 0;
                char[] delimiters1 = new char[] { ',' };

                foreach (DataRow dr in dsCallouts.Tables[0].Rows)
                {
                    intCalloutID = Convert.ToInt32(dr["GrittingCallOutID"]);
                    strSiteName = dr["SiteName"].ToString();
                    strSiteEmail = dr["SiteEmail"].ToString();
                    if (string.IsNullOrEmpty(dr["CompletedTime"].ToString()))
                    {
                        strCalloutCompletedTime = "Unknown Completion Time";
                    }
                    else
                    {
                        strCalloutCompletedTime = Convert.ToDateTime(dr["CompletedTime"]).ToString("dd/MM/yyyy HH:mm");
                    }
                    intEmailLinkedPictures = Convert.ToInt32(dr["EmailLinkedPictures"]);

                    // Send Email //
                    string strBody = System.IO.File.ReadAllText(@"C:\Mark_Development\WoodPlan5\GrittingCompletionEmailHTML_Layout.htm");
                    strBody = strBody.Replace("%Sites%", strSiteName);
                    strBody = strBody.Replace("%DateTime%", strCalloutCompletedTime);

                    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                    msg.From = new System.Net.Mail.MailAddress("mark.bissett@ground-control.co.uk");
                    string[] strEmailTo = strSiteEmail.Split(delimiters1, StringSplitOptions.RemoveEmptyEntries);
                    if (strEmailTo.Length > 0)
                    {
                        foreach (string strEmailAddress in strEmailTo)
                        {
                            msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                        }
                    }
                    else
                    {
                        msg.To.Add(new System.Net.Mail.MailAddress(strSiteEmail));
                    }
                   
                    System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                    System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                    msg.AlternateViews.Add(plainView);
                    msg.AlternateViews.Add(htmlView);

                    // Attach any Pictures //
                    if (intEmailLinkedPictures > 0)
                    {
                        DataRow[] drRows = dsPictures.Tables[0].Select("GrittingCallOutID = " + intCalloutID.ToString());
                        foreach (DataRow row in drRows)
                        {
                            if (File.Exists(row["PicturePath"].ToString()))
                            {
                                System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(row["PicturePath"].ToString());
                                logo.ContentId = "Work_Picture_" + row["PictureID"].ToString();
                                //add the LinkedResource to the appropriate view
                                htmlView.LinkedResources.Add(logo);
                            }
                        }
                    }

                    object userState = msg;
                    System.Net.Mail.SmtpClient emailClient = null;
                    emailClient = new System.Net.Mail.SmtpClient("smtp.messagestream.com");
                    System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential("ground-control_co_uk", "s6j4l8");
                    emailClient.UseDefaultCredentials = false;
                    emailClient.Credentials = basicCredential;

                    emailClient.SendAsync(msg, userState);
                    intEmailsSentCount++;
                }


            }
            catch (Exception xxe)
            {
            }

        }






    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // Used by File.Delete and FileSystemWatcher //
using System.Data.SqlClient;  // Used by Generate Map process //
using System.Reflection;

using DevExpress.LookAndFeel;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using DevExpress.XtraEditors.Repository;  // Required by Hyperlink Repository Items //
using DevExpress.XtraPrinting;

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace WoodPlan5
{
    public partial class frm_GC_Gritting_Callout_Completion_Sheet : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        
        public string _strSignaturePath = "";
        public string _strPicturePath = "";
        public string _strMapImagePath = "";
        public string _ReportLayoutFolder = "";
        
        public string _strExistingPDFFileName = "";
        public string _strPDFFolder = "";
        public int _ShowMaps = 0;
 
        public string _strGrittingCalloutID = "";
        //public int _intClientID = 0;
        //public int _intClientCount = 0;
        public bool _iBool_CompletionSheetEditLayoutButtonEnabled = false;

        rpt_WM_Callout_Completion_Sheet rptReport;
        WaitDialogForm loadingForm = null;

        #endregion
       
        public frm_GC_Gritting_Callout_Completion_Sheet()
        {
            InitializeComponent();
        }

        private void frm_GC_Gritting_Callout_Completion_Sheet_Load(object sender, EventArgs e)
        {
            this.FormID = 500293;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            ribbonControl1.Minimized = false;
            //bbiEmailToClient.Enabled = (_intClientID > 0 && _intClientCount == 1);
            //bsiInformation.Visibility = (bbiEmailToClient.Enabled ? DevExpress.XtraBars.BarItemVisibility.Never : DevExpress.XtraBars.BarItemVisibility.Always);
            bbiEditLayout.Enabled = _iBool_CompletionSheetEditLayoutButtonEnabled;
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _ReportLayoutFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Report Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!_ReportLayoutFolder.EndsWith("\\")) _ReportLayoutFolder += "\\";  // Add Backslash to end //

            // Get Save Folder //
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _strPDFFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutCompletionSheetsFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Completion Sheet Folder (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!_strPDFFolder.EndsWith("\\")) _strPDFFolder += "\\";
        }

        public override void PostOpen(object objParameter)
        {           
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            
            bbiView.PerformClick();  // Load Document //
        }

        private void frm_GC_Gritting_Callout_Completion_Sheet_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_GC_Gritting_Callout_Completion_Sheet_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (string.IsNullOrEmpty(printControl1.PrintingSystem.Document.Name)) return;
            /*if (printControl1.PrintingSystem.Document.Name != "Document")
            {
                printControl1.PrintingSystem.ClearContent(); // This should free up any map jpg if open //
                rptReport.Dispose();
                rptReport = null;
                GC.GetTotalMemory(true);
            }*/
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] {"iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void bbiView_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                this.splashScreenManager = splashScreenManager1;
                this.splashScreenManager.ShowWaitForm();
            }

            string strReportFileName = "GrittingCalloutCompletionSheet.repx";
            var rptReport = new rpt_WM_Callout_Completion_Sheet(this.GlobalSettings, _strGrittingCalloutID, _strMapImagePath, _strSignaturePath, _strPicturePath, _ShowMaps);
            try
            {
                rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);

                AdjustEventHandlers(rptReport);  // Tie in custom Sorting //

                printControl1.PrintingSystem = rptReport.PrintingSystem;
                printingSystem1.Begin();
                rptReport.CreateDocument();
                //printControl1.Zoom = 0.90f;  // 90% //

                //printControl1.ExecCommand(PrintingSystemCommand.DocumentMap, new object[] { false });  // Hide the Document Map //
                printingSystem1.End();
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void bbiSaveDocument_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            // Check how many Visits we have loaded //
            DialogResult dr = DialogResult.Yes;
            char[] delimiters = new char[] { ',' };
            string[] strArray = _intGrittingCalloutID.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length > 1)
            {
                dr = XtraMessageBox.Show("You have " + strArray.Length.ToString() + " Visits Loaded.\n\nDo you wish to save each visit as a separate PDF file or save all visits in one PDF file?\n\nClick <b>Yes</b> to save each visit to <b>individual files.</b>.\n\nClick <b>No</b> to save all visits as <b>one file</b>.", "Save Visit Completion Sheets", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, DefaultBoolean.True);
            }
            switch (dr)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.No:
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        string strPDFName = SaveReport(fProgress, _intGrittingCalloutID, Convert.ToInt32(strArray[0]), 1, 0, strDateTime, 0, 0);
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (!string.IsNullOrWhiteSpace(strPDFName)) DevExpress.XtraEditors.XtraMessageBox.Show("PDF File Saved Successfully.\n\nSaved File Location: " + strPDFName, "Save Visit Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case DialogResult.Yes:
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();
                        int intUpdateProgressThreshhold = strArray.Length / 10;
                        int intUpdateProgressTempCount = 0;

                        foreach (string strElement in strArray)
                        {
                            string strPDFName = SaveReport(fProgress, strElement + ",", Convert.ToInt32(strElement), 0, 0, strDateTime, 0, 0);
                            if (string.IsNullOrWhiteSpace(strPDFName))
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                return;
                            }
                            intUpdateProgressTempCount++;  // Update Progress Bar //
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                if (fProgress != null) fProgress.UpdateProgress(10);
                            }
                        }
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("PDF Files Saved Successfully.\n\nSaved File Folder Location: " + _strPDFFolder, "Save Visit Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                default:
                    return;
            } */
        }
        private string SaveReport(frmProgress fProgress, string strVisitIDs, int intVisitID, int intNameType, int intRecordID, string strDateTime, int intTeamID, int intLabourTypeID)
        {
            // Load Report into memory //
            /*string strReportFileName = "GrittingCalloutCompletionSheet.repx";
            var rptReport = new rpt_WM_Callout_Completion_Sheet(this.GlobalSettings, _intGrittingCalloutID, strMapImagePath,  strSignaturePath, strPicturePath, _ShowMaps);
            try
            {
                rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);
            }
            catch (Exception Ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Save Visit Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }
            // Set security options of report so when it is exported, it can't be edited and is password protected //
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
            rptReport.ExportOptions.Pdf.Compressed = true;
            rptReport.ExportOptions.Pdf.ImageQuality = PdfJpegImageQuality.Low;
            //rptReport.ExportOptions.Pdf.NeverEmbeddedFonts = "";

            // Get Filename //
            string strSavedFileName = "";
            try
            {
                BaseObjects.ExtensionFunctions extFunctions = new BaseObjects.ExtensionFunctions();
                var GetFilename = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                GetFilename.ChangeConnectionString(strConnectionString);
                strSavedFileName = extFunctions.RemoveSpecialCharacters(GetFilename.sp06384_OM_Completion_Sheet_Report_Get_Saved_File_Name(intVisitID, intNameType, intRecordID).ToString());

                fProgress.UpdateCaption("Saving - " + strSavedFileName + "...");
                System.Windows.Forms.Application.DoEvents();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Filename for the Saved PDF File - Save process aborted.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Save Visit Completion Sheet - Get Filename", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }

            // Save physical PDF file //
            string strPDFName = _strPDFFolder + strSavedFileName + "_" + strDateTime + ".pdf"; ;
            try
            {
                rptReport.ExportToPdf(strPDFName);
            }
            catch (Exception Ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save PDF File - an error occurred while generating the PDF File [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Save Visit Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }
            return strPDFName; */
            return "";
        }

        private void bbiEmailToClient_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Get Save Folder //
            /*string strSubject = "";
            string strCCToEmailAddress = "";
            string strEmailAddress = "";
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSubject = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitCompletionSheetSubjectLine").ToString();
                strCCToEmailAddress = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitCompletionSheetCCToEmailAddress").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Email Subject Line and\\or Email CC To Address (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Open Select Receipient List to find who to send the email to //
            var fChildForm = new frm_Core_Select_Client_Contact();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm.strPassedInClientIDsFilter = _intClientID.ToString() + ",";
            fChildForm.strPassedInContactTypeIDsFilter = "4,";  // 4: Email //

            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            strEmailAddress = fChildForm.strSelectedDescription1;

            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            // Check how many Visits we have loaded //
            DialogResult dr = DialogResult.Yes;
            char[] delimiters = new char[] { ',' };
            string[] strArray = _strVisitIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            Array.Sort(strArray, StringComparer.InvariantCulture); // Sort by VisitID //

            if (strArray.Length > 1)
            {
                dr = XtraMessageBox.Show("You have " + strArray.Length.ToString() + " Visits Loaded.\n\nDo you wish to save each visit as a separate PDF file or save all visits in one PDF file?\n\nClick <b>Yes</b> to save each visit to <b>individual files.</b>.\n\nClick <b>No</b> to save all visits as <b>one file</b>.", "Save Visit Completion Sheets", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, DefaultBoolean.True);
            }
            switch (dr)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.No:
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        string strPDFName = SaveReport(fProgress, _strVisitIDs, Convert.ToInt32(strArray[0]), 1, 0, strDateTime, 0, 0);
                        int intCounter = 0;  // Check if PDF file has completed writting //
                        do
                        {
                            System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                            intCounter++;
                            if (intCounter >= 60) break;
                        } while (!File.Exists(@strPDFName));
                        if (intCounter >= 60)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments so use custom MapiMailMessage class //
                        try
                        {
                            string strBody = "";
                            //MapiMailMessage message = new MapiMailMessage(strSubject, strBody);
                            Outlook.Application outlookApp = new Outlook.Application();
                            Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                            Outlook.Inspector oInspector = oMailItem.GetInspector;
                            Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                            Array arrayItems = strEmailAddress.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string strElement in arrayItems)
                            {
                                //message.Recipients.Add(strElement);
                                Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(strElement);
                                oRecip.Resolve();
                            }
                            //if (!string.IsNullOrWhiteSpace(strCCToEmailAddress)) message.Recipients.Add(strCCToEmailAddress, MapiMailMessage.RecipientType.CC);
                            if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                            {
                                //Add CC
                                Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                oCCRecip.Resolve();
                            }
                            if (fProgress != null)
                            {
                                fProgress.SetProgressValue(100);
                                fProgress.Close();
                                fProgress = null;
                            }
                            //message.Files.Add(@strPDFName);
                            oMailItem.Attachments.Add(@strPDFName);
                            oMailItem.Subject = strSubject;
                            oMailItem.Body = strBody;
                            oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //

                            //message.ShowDialog();  // Display Email Window to User //
                        }
                        catch (Exception ex) 
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case DialogResult.Yes:
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        int intUpdateProgressThreshhold = strArray.Length / 10;
                        int intUpdateProgressTempCount = 0;

                        // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments so use custom MapiMailMessage class //
                        try
                        {
                            string strBody = "";
                            //MapiMailMessage message = new MapiMailMessage(strSubject, strBody);
                            Outlook.Application outlookApp = new Outlook.Application();
                            Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                            Outlook.Inspector oInspector = oMailItem.GetInspector;
                            Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                            Array arrayItems = strEmailAddress.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string strElement in arrayItems)
                            {
                                //message.Recipients.Add(strElement);
                                Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(strElement);
                                oRecip.Resolve();
                            }
                            //if (!string.IsNullOrWhiteSpace(strCCToEmailAddress)) message.Recipients.Add(strCCToEmailAddress, MapiMailMessage.RecipientType.CC);
                            if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                            {
                                //Add CC
                                Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                oCCRecip.Resolve();
                            }

                            foreach (string strElement in strArray)
                            {
                                string strPDFName = SaveReport(fProgress, strElement + ",", Convert.ToInt32(strElement), 0, 0, strDateTime, 0, 0);
                                int intCounter = 0;  // Check if PDF file has completed writting //
                                do
                                {
                                    System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                                    intCounter++;
                                    if (intCounter >= 60) break;
                                } while (!File.Exists(@strPDFName));
                                if (intCounter >= 60)
                                {
                                    if (fProgress != null)
                                    {
                                        fProgress.Close();
                                        fProgress = null;
                                    }
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                                
                                if (string.IsNullOrWhiteSpace(strPDFName))
                                {
                                    if (fProgress != null)
                                    {
                                        fProgress.Close();
                                        fProgress = null;
                                    }
                                    return;
                                }
                                //message.Files.Add(@strPDFName);
                                oMailItem.Attachments.Add(@strPDFName);
                               
                                intUpdateProgressTempCount++;  // Update Progress Bar //
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    if (fProgress != null) fProgress.UpdateProgress(10);
                                }
                            }
                            if (fProgress != null)
                            {
                                fProgress.SetProgressValue(100);
                                fProgress.Close();
                                fProgress = null;
                            }
                            //message.ShowDialog();  // Display Email Window to User //
                            oMailItem.Subject = strSubject;
                            oMailItem.Body = strBody;
                            oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                default:
                    return;
            } */
        }

        private void bbiEmailToTeam_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Get Save Folder //
            /*string strSubject = "";
            string strCCToEmailAddress = "";
            string strPersonIDs = "";
            string strPersonTypeIDs = "";
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSubject = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_rpt_WM_Callout_Completion_SheetSubjectLine").ToString();
                strCCToEmailAddress = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_rpt_WM_Callout_Completion_SheetCCToEmailAddress").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Email Subject Line and\\or Email CC To Address (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            // Get unique list of TeamIDs from VisitIDs //
            SqlDataAdapter sdaData_Recipients = new SqlDataAdapter();
            DataSet dsData_Recipients = new DataSet("NewDataSet");
            try
            {
                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "sp06426_OM_Get_Unique_Teams_From_VisitIDs";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@VisitIDs", _strVisitIDs));
                        cmd.Connection = conn;
                        sdaData_Recipients = new SqlDataAdapter(cmd);
                        sdaData_Recipients.Fill(dsData_Recipients, "Table");
                    }
                    conn.Close();
                }
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the TeamIDs from the loaded Visits.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Team Email Addresses", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            switch (dsData_Recipients.Tables[0].Rows.Count)
            {
                case 0:  // No Teams loaded //
                    {
                        XtraMessageBox.Show("The Selected Visits have no Teams working on them. Unable to generate Completion Sheets", "Save Visit Completion Sheets", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                        return;
                    }
                case 1:  // 1 Team - No action required //
                    break;
                default:  // Greater than one team loaded //
                    {
                        if (XtraMessageBox.Show("You have Visits for multiple teams loaded. If you proceed, separate emails will be generated for each team showing only thoses visits specific to that team. Are you sure you wish to proceed?", "Save Visit Completion Sheets", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                    }
                    break;
            }

            // Open Select Recipient List to find who to send the email to //
            var fChildForm = new frm_GC_Gritting_Callout_Completion_Sheet_Select_Recipient();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm.dsData_Recipients = dsData_Recipients;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            if (fChildForm._SelectedCount <= 0) return;
            strPersonIDs = fChildForm._strParentIDs;
            strPersonTypeIDs = fChildForm._strParentTypeIDs;
            
            int intOwnersWithMultipleVisits = 0;
            int intLabourID = 0;
            int intLabourTypeID = 0;
            int intUniqueLabourCount = 0;
            GridView RecipientView = (GridView)fChildForm.gridControl1.MainView;

            char[] delimiters = new char[] { ',' };
            string[] strArrayPersons = strPersonIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            string[] strArrayPersonTypes = strPersonTypeIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            int intPosition = 0;
            foreach (string strLabourID in strArrayPersons)
            {
                intUniqueLabourCount++;
                intLabourID = Convert.ToInt32(strLabourID);
                intLabourTypeID = Convert.ToInt32(strArrayPersonTypes[intPosition]);
                try
                {
                    var GetCount = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetCount.ChangeConnectionString(strConnectionString);
                    int intCount = Convert.ToInt32(GetCount.sp06424_OM_Get_Visit_Count_From_Team_IDs_And_VisitIDs(_strVisitIDs, intLabourID, intLabourTypeID));
                    if (intCount > 1) intOwnersWithMultipleVisits++;
                }
                catch (Exception) { }
                intPosition++;
            }

            DialogResult dialogResult = DialogResult.No;
            if (intOwnersWithMultipleVisits > 0)
            {
                dialogResult = XtraMessageBox.Show("You have " + intOwnersWithMultipleVisits.ToString() + " Teams with multiple Visits Loaded.\n\nDo you wish to save each visit as a separate PDF file or save all visits for each Team in one PDF file?\n\nClick <b>Yes</b> to save each visit to <b>individual files.</b>.\n\nClick <b>No</b> to save all visits as <b>one file</b>.", "Save Visit Completion Sheets", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, DefaultBoolean.True);
            }
            
            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            
            string strVisitIDs = "";
            switch (dialogResult)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.No: // One PDF file for all visits per team //
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        int intUpdateProgressThreshhold = intUniqueLabourCount / 10;
                        int intUpdateProgressTempCount = 0;

                        intPosition = 0;
                        foreach (string strLabourID in strArrayPersons)
                        {
                            intLabourID = Convert.ToInt32(strLabourID);
                            intLabourTypeID = Convert.ToInt32(strArrayPersonTypes[intPosition]);

                            intUpdateProgressTempCount++;  // Update Progress Bar //
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                if (fProgress != null) fProgress.UpdateProgress(10);
                            }
                            
                            try
                            {
                                var GetVisits = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                                GetVisits.ChangeConnectionString(strConnectionString);
                                strVisitIDs = GetVisits.sp06425_OM_Get_VisitIDs_From_Team_IDs_And_VisitIDs(_strVisitIDs, intLabourID, intLabourTypeID).ToString();
                            }
                            catch (Exception) 
                            {
                                continue;
                            }

                            // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments. Can't use custom MapiMailMessage class as it will only allow one email to be opened so use Outlook interop instead //
                            try
                            {
                                string strBody = "";
                                Outlook.Application outlookApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                                Outlook.Inspector oInspector = oMailItem.GetInspector;
                                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                                // Get email addresses from original select screen //
                                GridView view = (GridView)fChildForm.gridControl1.MainView;
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                                    {
                                        if (Convert.ToInt32(view.GetRowCellValue(i, "OwnerID")) == intLabourID && Convert.ToInt32(view.GetRowCellValue(i, "OwnerTypeID")) == intLabourTypeID)
                                        {
                                            Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(view.GetRowCellValue(i, "Email").ToString());
                                            oRecip.Resolve();
                                        }
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                                {
                                    //Add CC
                                    Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                    oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                    oCCRecip.Resolve();
                                }

                                // Create PDF File //
                                string strPDFName = SaveReport(fProgress, strVisitIDs, 0, 3, intLabourID, strDateTime, intLabourID, intLabourTypeID);
                                int intCounter = 0;  // Check if PDF file has completed writting //
                                do
                                {
                                    System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                                    intCounter++;
                                    if (intCounter >= 60) break;
                                } while (!File.Exists(@strPDFName));
                                if (intCounter >= 60)
                                {
                                    if (fProgress != null)
                                    {
                                        fProgress.Close();
                                        fProgress = null;
                                    }
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                                oMailItem.Attachments.Add(@strPDFName);
                                oMailItem.Subject = strSubject;
                                oMailItem.Body = strBody;
                                oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //

                                outlookApp = null;
                                oMailItem = null;
                                oInspector = null;
                                oRecips = null;
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            intPosition++;
                        }
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                    }
                    break;
                case DialogResult.Yes: // Separate PDF files per visit per team //
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        int intUpdateProgressThreshhold = intUniqueLabourCount / 10;
                        int intUpdateProgressTempCount = 0;

                        intPosition = 0;
                        foreach (string strLabourID in strArrayPersons)
                        {
                            intLabourID = Convert.ToInt32(strLabourID);
                            intLabourTypeID = Convert.ToInt32(strArrayPersonTypes[intPosition]);

                            intUpdateProgressTempCount++;  // Update Progress Bar //
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                if (fProgress != null) fProgress.UpdateProgress(10);
                            }

                            try
                            {
                                var GetVisits = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                                GetVisits.ChangeConnectionString(strConnectionString);
                                strVisitIDs = GetVisits.sp06425_OM_Get_VisitIDs_From_Team_IDs_And_VisitIDs(_strVisitIDs, intLabourID, intLabourTypeID).ToString();
                            }
                            catch (Exception)
                            {
                                continue;
                            }

                            // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments. Can't use custom MapiMailMessage class as it will only allow one email to be opened so use Outlook interop instead //
                            try
                            {
                                string strBody = "";
                                Outlook.Application outlookApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                                Outlook.Inspector oInspector = oMailItem.GetInspector;
                                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                                // Get email addresses from original select screen //
                                GridView view = (GridView)fChildForm.gridControl1.MainView;
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                                    {
                                        if (Convert.ToInt32(view.GetRowCellValue(i, "OwnerID")) == intLabourID && Convert.ToInt32(view.GetRowCellValue(i, "OwnerTypeID")) == intLabourTypeID)
                                        {
                                            Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(view.GetRowCellValue(i, "Email").ToString());
                                            oRecip.Resolve();
                                        }
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                                {
                                    //Add CC
                                    Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                    oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                    oCCRecip.Resolve();
                                }

                                // Create PDF Files //
                                string[] strArray = strVisitIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                Array.Sort(strArray, StringComparer.InvariantCulture);  // Sort by VisitID //
                                foreach (string strElement in strArray)
                                {
                                    string strPDFName = SaveReport(fProgress, strElement + ",", Convert.ToInt32(strElement), 2, intLabourID, strDateTime, intLabourID, intLabourTypeID);
                                    int intCounter = 0;  // Check if PDF file has completed writting //
                                    do
                                    {
                                        System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                                        intCounter++;
                                        if (intCounter >= 60) break;
                                    } while (!File.Exists(@strPDFName));
                                    if (intCounter >= 60)
                                    {
                                        if (fProgress != null)
                                        {
                                            fProgress.Close();
                                            fProgress = null;
                                        }
                                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }

                                    if (string.IsNullOrWhiteSpace(strPDFName))
                                    {
                                        if (fProgress != null)
                                        {
                                            fProgress.Close();
                                            fProgress = null;
                                        }
                                        return;
                                    }
                                    oMailItem.Attachments.Add(@strPDFName);

                                    intUpdateProgressTempCount++;  // Update Progress Bar //
                                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                    {
                                        intUpdateProgressTempCount = 0;
                                        if (fProgress != null) fProgress.UpdateProgress(10);
                                    }
                                }

                                oMailItem.Subject = strSubject;
                                oMailItem.Body = strBody;
                                oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //

                                outlookApp = null;
                                oMailItem = null;
                                oInspector = null;
                                oRecips = null;
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            intPosition++;
                        }
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                    }
                    break;
                default:
                    return;
            } */
        }

        private void bbiEditLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strReportFileName = "GrittingCalloutCompletionSheet.repx";
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var rptReport = new rpt_WM_Callout_Completion_Sheet(this.GlobalSettings, _strGrittingCalloutID, _strMapImagePath, _strSignaturePath, _strPicturePath, _ShowMaps);
                try
                {
                    rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                if (!splashScreenManager.IsSplashFormVisible)
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    this.splashScreenManager = splashScreenManager1;
                    this.splashScreenManager.ShowWaitForm();
                }

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, _ReportLayoutFolder, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                form.ShowDialog();
                panel.CloseReport();
            }
        }

 

        void form_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            //if (loadingForm != null) loadingForm.Close();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);

            }
        }
       
        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }            
         }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Data...", "Report Printing");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //
            }
        }

        /*
                public class SaveCommandHandler : ICommandHandler
                {
                    XRDesignPanel panel;

                    public string strFullPath = "";

                    public string strFileName = "";

                    public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
                    {
                        this.panel = panel;
                        this.strFullPath = strFullPath;
                        this.strFileName = strFileName;
                    }

                    public virtual void HandleCommand(ReportCommand command, object[] args, ref bool handled)
                    {
                        if (!CanHandleCommand(command)) return;
                        Save();  // Save report //
                        handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
                    }

                    public virtual bool CanHandleCommand(ReportCommand command)
                    {
                        // This handler is used for SaveFile, SaveFileAs and Closing commands.
                        return command == ReportCommand.SaveFile ||
                            command == ReportCommand.SaveFileAs ||
                            command == ReportCommand.Closing;
                    }

                    void Save()
                    {
                        // Custom Saving Logic //
                        Boolean blSaved = false;
                        panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                        // Update existing file layout //
                        panel.Report.DataSource = null;
                        panel.Report.DataMember = null;
                        panel.Report.DataAdapter = null;
                        try
                        {
                            panel.Report.SaveLayout(strFullPath + strFileName);
                            blSaved = true;
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            Console.WriteLine(ex.Message);
                            blSaved = false;
                        }
                        if (blSaved)
                        {
                            panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                        }
                    }
                }
        */
        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }




    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_GC_Team_Gritting_Information_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intContractorID = 0;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        #endregion
        
        public frm_GC_Team_Gritting_Information_Edit()
        {
            InitializeComponent();
        }

        private void frm_GC_Team_Gritting_Information_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 400008;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            this.sp04276_GC_Missing_Job_Sheet_Numbers_Delivery_Methods_With_BlankTableAdapter.Fill(this.dataSet_GC_DataEntry.sp04276_GC_Missing_Job_Sheet_Numbers_Delivery_Methods_With_Blank);

            sp04026_Contractor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04026_Contractor_List_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04026_Contractor_List_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04035_GC_Self_Billing_Frequency_Descriptors_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04036_GC_Stock_Depots_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04036_GC_Stock_Depots_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04036_GC_Stock_Depots_With_Blank, "");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04033_GC_Team_Gritting_Information_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Populate Main Dataset //
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04033_GC_Team_Gritting_Information_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["SubContractorID"] = intContractorID;
                        drNewRow["GrittingActive"] = 1;
                        drNewRow["IsDirectLabour"] = 0;
                        drNewRow["HoldsStockCheckEdit"] = 0;
                        drNewRow["SelfBillingInvoice"] = 0;

                        drNewRow["StartingGritAmount"] = 0.00;
                        drNewRow["TransferredInGritAmount"] = 0.00;
                        drNewRow["TransferredOutGritAmount"] = 0.00;
                        drNewRow["OrderedInGritAmount"] = 0.00;
                        drNewRow["CalloutUsedGritAmount"] = 0.00;

                        drNewRow["CurrentGritLevel"] = 0.00;
                        drNewRow["WarningGritLevel"] = 0.00;
                        drNewRow["UrgentGritLevel"] = 0.00;
                        drNewRow["StockSuppliedByDepotID"] = 0;
                        drNewRow["SelfBillingFrequencyDescriptorID"] = 0;
                        drNewRow["SelfBillingFrequency"] = 0;
                        drNewRow["MissingJobSheetDeliveryMethodID"] = 0;
                        drNewRow["AutoAllocateGritJobs"] = 0;
                       this.dataSet_GC_DataEntry.sp04033_GC_Team_Gritting_Information_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04033_GC_Team_Gritting_Information_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SubContractorID"] = 0;
                        this.dataSet_GC_DataEntry.sp04033_GC_Team_Gritting_Information_Edit.Rows.Add(drNewRow);
                        this.dataSet_GC_DataEntry.sp04033_GC_Team_Gritting_Information_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp04033_GC_Team_Gritting_Information_EditTableAdapter.Fill(this.dataSet_GC_DataEntry.sp04033_GC_Team_Gritting_Information_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_GC_DataEntry.sp04033_GC_Team_Gritting_Information_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Team Gritting Information", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        SubContractorIDGridLookUpEdit.Focus();

                        SubContractorIDGridLookUpEdit.Properties.ReadOnly = true;
                        StartingGritAmountSpinEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        SubContractorIDGridLookUpEdit.Focus();

                        SubContractorIDGridLookUpEdit.Properties.ReadOnly = true;
                        StartingGritAmountSpinEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        GrittingActiveCheckEdit.Focus();

                        SubContractorIDGridLookUpEdit.Properties.ReadOnly = true;
                        StartingGritAmountSpinEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        GrittingActiveCheckEdit.Focus();

                        SubContractorIDGridLookUpEdit.Properties.ReadOnly = true;
                        StartingGritAmountSpinEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            Set_Control_Readonly_Status("");
            Set_Grit_Warning_Label();
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_GC_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_GC_Team_Gritting_Information_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_GC_Team_Gritting_Information_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp04033GCTeamGrittingInformationEditBindingSource.EndEdit();
            try
            {
                this.sp04033_GC_Team_Gritting_Information_EditTableAdapter.Update(dataSet_GC_DataEntry);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp04033GCTeamGrittingInformationEditBindingSource.Current;
                if (currentRow != null) 
                {
                    strNewIDs = Convert.ToInt32(currentRow["SubContractorGritInformationID"]) + ";";
                    // Switch mode to Edit so than any subsequent changes update this record //
                    this.strFormMode = "edit";
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            frm_Core_Contractor_Manager fParentForm;
            foreach (Form frmChild in this.ParentForm.MdiChildren)
            {
                if (frmChild.Name == "frm_Core_Contractor_Manager")
                {
                    fParentForm = (frm_Core_Contractor_Manager)frmChild;
                    fParentForm.UpdateFormRefreshStatus(2, "", "", "", "", "", strNewIDs, "", "", "", "", "", "");
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_GC_DataEntry.sp04033_GC_Team_Gritting_Information_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_GC_DataEntry.sp04033_GC_Team_Gritting_Information_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                //if (!dxValidationProvider1.Validate())
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Set_Control_Readonly_Status("");
                    Set_Grit_Warning_Label();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            //foreach (Control c in dxValidationProvider.GetInvalidControls())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    //strErrorMessages += "--> " + item.Text.ToString() + "  " + dxValidationProvider.GetValidationRule(c).ErrorText + "\n";
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void SubContractorIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int? intValue = Convert.ToInt32(glue.EditValue);
            if (this.strFormMode != "blockedit" && intValue == 0)
            {
                dxErrorProvider1.SetError(SubContractorIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SubContractorIDGridLookUpEdit, "");
            }
        }

        private void HoldsStockCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("HoldsStock");
        }

        private void SelfBillingInvoiceCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("SelfBillingInvoice");
        }
        
        private void WarningGritLevelSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void WarningGritLevelSpinEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_ignoreValidation) Set_Grit_Warning_Label();
            ibool_ignoreValidation = true;
        }

        private void UrgentGritLevelSpinEdit_DragOver(object sender, DragEventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void UrgentGritLevelSpinEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_ignoreValidation) Set_Grit_Warning_Label();
            ibool_ignoreValidation = true;
        }

        private void IsDirectLabourCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("IsDirectLabour");
        }
        
        private void StartingGritAmountSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void StartingGritAmountSpinEdit_Validated(object sender, EventArgs e)
        {
            if (!ibool_ignoreValidation) Calculate_Current_Salt_Level();
            ibool_ignoreValidation = true;
        }
        
        #endregion


        private void Set_Control_Readonly_Status(string strCheckWhat)
        {
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "HoldsStock")
            {
                if (HoldsStockCheckEdit.Checked)
                {
                    WarningGritLevelSpinEdit.Properties.ReadOnly = false;
                    UrgentGritLevelSpinEdit.Properties.ReadOnly = false;
                    StockSuppliedByDepotIDGridLookupEdit.Properties.ReadOnly = false;
                    if (this.strFormMode != "blockedit")
                    {
                        StartingGritAmountSpinEdit.Properties.ReadOnly = false;
                    }
                    else
                    {
                        StartingGritAmountSpinEdit.Properties.ReadOnly = true;
                    }
                }
                else
                {
                    WarningGritLevelSpinEdit.Properties.ReadOnly = true;
                    UrgentGritLevelSpinEdit.Properties.ReadOnly = true;
                    StockSuppliedByDepotIDGridLookupEdit.Properties.ReadOnly = true;
                    StartingGritAmountSpinEdit.Properties.ReadOnly = true;
                }
            }
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "SelfBillingInvoice")
            {
                if (SelfBillingInvoiceCheckEdit.Checked)
                {

                    SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties.ReadOnly = false;
                    SelfBillingFrequencySpinEdit.Properties.ReadOnly = false;
                }
                else
                {
                    SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties.ReadOnly = true;
                    SelfBillingFrequencySpinEdit.Properties.ReadOnly = true;
                }
            }
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "IsDirectLabour")
            {
                if (IsDirectLabourCheckEdit.Checked)
                {

                    HourlyProactiveRateSpinEdit.Properties.ReadOnly = false;
                    HourlyReactiveRateSpinEdit.Properties.ReadOnly = false;
                }
                else
                {
                    HourlyProactiveRateSpinEdit.Properties.ReadOnly = true;
                    HourlyReactiveRateSpinEdit.Properties.ReadOnly = true;
                }
            }
        }

        private void Set_Grit_Warning_Label()
        {
            //  Set warning message if Grit stocks too low //
            if (strFormMode == "blockedit")
            {
                WarningLabel.Text = "";
                ItemForWarningLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                WarningLabel.BackColor = Color.Transparent;
                return;
            }
            DataRowView currentRow = (DataRowView)sp04033GCTeamGrittingInformationEditBindingSource.Current;
            if (currentRow != null)
            {
                if (Convert.ToInt32(currentRow["HoldsStock"]) == 1 && Convert.ToDecimal(currentRow["CurrentGritLevel"]) < Convert.ToDecimal(currentRow["UrgentGritLevel"]))
                {
                    WarningLabel.Text = "       Warning: Current Salt Level Below Urgent Level";
                    WarningLabel.BackColor = Color.Red;
                    ItemForWarningLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                }
                else if (Convert.ToInt32(currentRow["HoldsStock"]) == 1 && Convert.ToDecimal(currentRow["CurrentGritLevel"]) < Convert.ToDecimal(currentRow["WarningGritLevel"]))
                {
                    WarningLabel.Text = "       Warning: Current Salt Level Below Warning Level";
                    ItemForWarningLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    WarningLabel.BackColor = Color.Orange;
                }
                else
                {
                    WarningLabel.Text = "";
                    ItemForWarningLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    WarningLabel.BackColor = Color.Transparent;
                }
            }
        }

        private void Calculate_Current_Salt_Level()
        {
            DataRowView currentRow = (DataRowView)sp04033GCTeamGrittingInformationEditBindingSource.Current;
            if (currentRow == null || this.strFormMode == "blockedit") return;

 		    decimal? decStartingGritAmount = (decimal)0.00;
		    decimal? decTransferredInGritAmount = (decimal)0.00;
		    decimal? decTransferredOutGritAmount = (decimal)0.00;
		    decimal? decOrderedInGritAmount = (decimal)0.00;
            decimal? decCalloutUsedGritAmount = (decimal)0.00;

            decStartingGritAmount = Convert.ToDecimal(StartingGritAmountSpinEdit.EditValue ?? 0.00);
            decTransferredInGritAmount = Convert.ToDecimal(TransferredInGritAmountSpinEdit.EditValue ?? 0.00);
            decTransferredOutGritAmount = Convert.ToDecimal(TransferredOutGritAmountSpinEdit.EditValue ?? 0.00);
            decOrderedInGritAmount = Convert.ToDecimal(OrderedInGritAmountSpinEdit.EditValue ?? 0.00);
            decCalloutUsedGritAmount = Convert.ToDecimal(CalloutUsedGritAmountSpinEdit.EditValue ?? 0.00);

            // Required values in variables so perform calculation //
            currentRow["CurrentGritLevel"] = decStartingGritAmount + decTransferredInGritAmount + decOrderedInGritAmount - decTransferredOutGritAmount - decCalloutUsedGritAmount;
            this.sp04033GCTeamGrittingInformationEditBindingSource.EndEdit();
            Set_Grit_Warning_Label();
        }


    }
}

